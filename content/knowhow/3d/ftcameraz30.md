---
layout: "file"
title: "ftcameraz30"
date: 2018-01-24T11:21:42+02:00
file: "ftcameraz30.zip"
hidden: true
konstrukteure: 
- "Torsten Stuehn"
uploadBy:
- "Torsten Stuehn"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/ftcameraz30.zip
imported:
- "2019"
---
<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/ftcameraz30.zip -->

Zahnrad Z30 zum Aufschieben auf die fischertechnik Camera (STL-File)
