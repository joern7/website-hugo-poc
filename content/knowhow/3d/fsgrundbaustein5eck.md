---
layout: "file"
title: "Fidget Spinner Grundbaustein Fünfeck"
date: 2017-08-15T11:26:14+02:00
file: "fsgrundbaustein5eck.stl"
hidden: true
konstrukteure: 
- "Bausteinphilosoph"
uploadBy:
- "Bausteinphilosoph"
license: "unknown"
legacy_id:
- "/data/downloads/3ddruckdateien/fsgrundbaustein5eck.stl"
imported:
- "2019"
---
<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/fsgrundbaustein5eck.stl -->

Mit dem FS Grundbaustein lassen sich kreative, hochwertige und individualisierte FS konstruieren.

Das Achsloch hat einen Durchmesser von 12,4 mm,
der Nut Durchmesser beträgt 4,4 mm und die Breite des Schaftes ist 3,4 mm.

Bei mir passen alle Bausteine, Verbinder und Achsen durch die vorgesehenen Aussparungen. (Ultimaker)

Als Kugellager passen 12mm Kugelager mit 4mm Achsloch (http://fischerfriendsman.de/index.php?p=6&sp=4#R6513)
oder
1x Seilrolle 12, mini (38258) mit z.B. Rastachse 30 mm (35063),
2x Hülse mit Scheibe 15 mm (35981) und 2x BSB Puffer schwarz (36132)
oder
3x Seilrolle 12, mini (38258), 2x Hülse mit Scheibe 15 mm (35981) und Achse 30 (6500).
Oder, oder...oder.

Lasst eurer Phantasie freien Lauf. 
