---
layout: "file"
title: "Makerbeam XL Endstück"
date: 2017-06-07T11:32:44+02:00
file: "makerbeamxlendstueck.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- "/data/downloads/3ddruckdateien/makerbeamxlendstueck.zip"
imported:
- "2019"
---
<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/makerbeamxlendstueck.zip -->
\*.stl und \*.ipt Inventor Datei  
Maker Beam XL Endstück 15x15x7,5 mm
