---
layout: "file"
title: "Flipperkeile links und rechts"
date: 2017-06-01T15:00:19+02:00
hidden: true
file: "flipperkeil.zip"
konstrukteure: 
- "Dirk Wölffel"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/flipperkeil.zip
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/flipperkeil.zip -->
*.stl und *.ipt Inventor Datei
Keile links und rechts für Flipper "Pirates of the Caribbian"
