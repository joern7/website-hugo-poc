---
layout: "wiki"
title: "Modulanlage für Tischtennisbälle"
hidden: true
date: 2017-01-29T09:54:42
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /wiki34c9.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wiki34c9.html?action=show&topic_id=41 -->
<!--
Wiki

Thema: Modulanlage für Tischtennisbälle

Aktuelle Version

von: H.A.R.R.Y.

am: 29.01.2017, 09:54:42 Uhr

   Historie:
Version 31 von H.A.R.R.Y. am 29.01.2017, 09:50:40 Uhr
Version 30 von H.A.R.R.Y. am 28.01.2017, 20:35:48 Uhr
Version 29 von H.A.R.R.Y. am 28.01.2017, 20:23:07 Uhr

...

Version 3 von H.A.R.R.Y. am 28.01.2017, 13:13:42 Uhr
Version 2 von H.A.R.R.Y. am 28.01.2017, 13:05:51 Uhr
Version 1 von H.A.R.R.Y. am 28.01.2017, 13:04:22 Uhr
-->

Die Modulanlage für Tischtennisbälle ist als offenes Projekt gedacht, bei dem Jeder mitmachen kann. Die Diskussion im Forum findest Du [hier](https://forum.ftcommunity.de/viewtopic.php?f=6&t=3984).


Jedes Modul wird individuell von einer Person oder einem Team entworfen und gebaut. Die Module sind untereinander austauschbar und ergeben je nach Anordnung ein unterschiedliches Bild der gesamten Anlage. Damit das funktioniert, müssen allerdings ein paar Regeln vereinbart und eingehalten werden.

1. Der **Bezugspunkt** für alle Höhenangaben in diesem Dokument ist die Oberkante der Tischplatte auf der das Modul steht.


2. Als **Transportgut** dienen [Tischtennisbälle](http://de.wikipedia.org/wiki/Tischtennisball) mit 40mm Durchmesser, alle 2 s wird ein Ball weitergereicht. Der Durchsatz beträgt somit 0,5 /s. Die Ballfarbe ist egal.

3. Der **Eingang** hat eine lichte Weite WE von mindestens 60 mm, eine lichte Höhe HE von mindestens 70 mm und eine Schwellenhöhe SE im Bereich von 77,5 mm bis 85,0 mm.

   ![Abb. 1](../Abb._1_-_Eingangsportal.jpg)
   Abb. 1 - Sicht auf die Eingangsseite

4. Der **Ausgang** hat eine lichte Weite WA von höchstens 60 mm, eine lichte Höhe HA von höchstens 70 mm und eine Schwellenhöhe SA im Bereich von 87,5 mm bis 92,5 mm.

   ![Abb. 2](../Abb._2_-_Ausgangsportal.jpg)
   Abb. 2 - Sicht auf die Ausgangsseite

5. Für ein **Standardmodul** ist eine rechteckige Grundfläche reserviert. Es besitzt je einen Eingang und einen Ausgang an gegenüberliegenden Seiten. Die maximal nutzbare Tiefe T beträgt 540 mm, sie muss nicht ausgeschöpft werden. Die nutzbare Breite B beträgt genau ganzzahlige Vielfache von 390 mm. Eingang und Ausgang liegen symmetrisch zur Mittellinie der Grundfläche. Die Mittellinie liegt bei der halben Tiefe T, also bei T/2 = 270 mm.

   ![Abb. 3](../Abb._3_-_Grundfläche_Standard.jpg)
   Abb. 3 - Sicht von oben

6. Für ein **Eckmodul** ist eine quadratische Grundfläche mit der Kantenlänge T = 540 mm reserviert. Die Kantenlängen müssen nicht voll ausgeschöpft werden. Der Eingang und der Ausgang sind im Modul an einander benachbarten Seiten angeordnet. Eingang und Ausgang liegen symmetrisch zur jeweiligen Mittellinie der Grundfläche. Die Mittellinien liegen jeweils bei der halben Seitenlänge T, also jeweils bei 270 mm.

   ![Abb. 4](../Abb._4_-_Linksecke.jpg)
   Abb. 4 - Linksecke - Sicht von oben

   ![Abb. 5](../Abb._5_-_Rechtsecke.jpg)
   Abb. 5 - Rechtsecke - Sicht von oben

Die Regeln für Standardmodul und Eckmodul sind so gestaltet, dass es möglich ist ein Kombimodul zu bauen das wahlweise geradeaus oder aber auch um eine Ecke, oder gar beide Ecken, fördern kann. Sogar Abzweige oder auch Einmündungen können auf dieser Basis gestaltet werden, derzeit ist das jedoch nicht vorgesehen.

Abb. 6 und Abb. 7 zeigen jeweils ein regelgerechtes Ausführungsbeispiel für Eingangs- und Ausgangsportal, montiert auf einer [Grundplatte 1000](http://ft-datenbank.de/details.php?ArticleVariantId=3ba4e119-4c38-4a71-a614-fe01ae8e3428). Die liegenden, roten Bauplatten sind mittels Winkelsteinen um 7,5° geneigt und ihre Außenkanten liegen recht genau über dem Rand der 1000er Grundplatte.

![Abb. 6](../Abb._6_-_Eingangsportal.jpg)
Abb. 6 - Eingangsportal ([ft-Designer file](../ttbmoduleingang.ftm))

![Abb. 7](../Abb._7_-_Ausgangsportal.jpg)
Abb. 7 - Ausgangsportal ([ft-Designer file](../ttbmodulausgang.ftm))

Die Verwendung von irgendwelchen Grundplatten ist nicht zwingend vorgeschrieben.

Dieser Inhalt steht auch als [pdf-Datei](../TTBB-Regeln_-_DE.pdf) zur Verfügung.
