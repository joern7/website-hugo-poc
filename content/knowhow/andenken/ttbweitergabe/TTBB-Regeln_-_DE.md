---
layout: "file"
hidden: true
title: "TTBB-Regeln_-_DE"
date: 2020-06-16T17:02:37+02:00
file: "TTBB-Regeln_-_DE.pdf"
konstrukteure: 
- "H.A.R.R.Y"
uploadBy:
- "Website-Team"
license: "unknown"
---
Tischtennisball-Weitergabe: Zusammenfassung der Regeln als PDF
