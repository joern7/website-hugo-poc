---
layout: "file"
hidden: true
title: "txtAudioManager v0016"
date: "2018-03-10T00:00:00"
file: "txtaudiomanagerv0016ftc.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/txtaudiomanagerv0016ftc.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/txtaudiomanagerv0016ftc.zip -->
txtAudioManager v0016

The txtAudioManager has been developed to exchange or add sounds on the RTXTC. Wav files for RTXTC can be converted from any mp3 or wav file. These files can then be played back via the fischertechnik programming environment RoboPro for the RTXTC. This applies to the sound identifiers 01 ... 29.

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail