---
layout: "file"
hidden: true
title: "bootloader to upload firmware to ATMega through I2C interface on TX"
date: "2014-09-12T00:00:00"
file: "i2cbootloader.zip"
konstrukteure: 
- "Ad van der Weiden"
uploadBy:
- "Ad van der Weiden"
license: "unknown"
legacy_id:
- /data/downloads/software/i2cbootloader.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/i2cbootloader.zip -->
A bootloader for ATMega based peripherals, a RoboPro application to send the firmware and a utility to convert hexfiles to .CSV files that can be loaded into Robopro lists.