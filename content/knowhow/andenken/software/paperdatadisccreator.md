---
layout: "file"
hidden: true
title: "Paper Data Disc Creator"
date: "2016-01-10T00:00:00"
file: "paperdatadisccreator.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/paperdatadisccreator.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/paperdatadisccreator.zip -->
Dear friends of fischertechnik models and automation!

This program creates your notes or song as a Paper Data Disc. It can be used with an player like "https://www.youtube.com/watch?v=Tqc9J1QLcfs". 

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail