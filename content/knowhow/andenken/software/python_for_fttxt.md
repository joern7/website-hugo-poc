---
layout: "file"
hidden: true
title: "python2.7 fuer den TXT"
date: "2015-07-12T00:00:00"
file: "python_for_fttxt.zip"
konstrukteure: 
- "Torsten"
uploadBy:
- "Torsten"
license: "unknown"
legacy_id:
- /data/downloads/software/python_for_fttxt.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/python_for_fttxt.zip -->
Die Installation dieses Paketes auf dem TXT sollte nur von erfahrenen Linux Usern durchgeführt werden.
Das Python-Binary und alle Libraries wurden mit dem von der
fischertechnik GmbH zur Verfuegung gestellten Paket
   ft_TXT_toolchain
mit der Firmwareversion 4.1.6 Cross-Compiliert.
Die Binaries wurden ausserdem auch mit der Version 4.1.8 erfolgreich getestet.
Python kann komplett mit den Rechten des Users ROBOPro installiert und verwendet werden.
Root-Rechte sind nicht notwendig.
Die Anleitung befindet sich in der README-Datei.