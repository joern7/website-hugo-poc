---
layout: "file"
hidden: true
title: "Universalinterface mit Microkontroller gesteuert"
date: "2014-09-27T00:00:00"
file: "parint_c.zip"
konstrukteure: 
- "xbach"
uploadBy:
- "xbach"
license: "unknown"
legacy_id:
- /data/downloads/software/parint_c.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/parint_c.zip -->
Programme ( in C) und Layout zum Umbau Universalinterface mit PIC Microkontroller.