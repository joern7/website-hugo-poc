---
layout: "file"
hidden: true
title: "Roboproprogramm zur Akkumessstation"
date: "2010-07-29T00:00:00"
file: "akku.4.10.rpp"
konstrukteure: 
- "Fabian M."
uploadBy:
- "Fabian M."
license: "unknown"
legacy_id:
- /data/downloads/software/akku.4.10.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/akku.4.10.rpp -->
Teilweise vielleicht nicht ganz ordentlich Programmiert - aber es funktioniert einwandfrei!
Zuerst wird überprüft ob sich der Spannungsbereich im Vergleich zum letzten Durchlauf verändert hat.Wenn ja leuchtet(oder halt nicht) die entsprechende Lampe auf.