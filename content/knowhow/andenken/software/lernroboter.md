---
layout: "file"
hidden: true
title: "Teach-In Roboter aus FT Computing für C64"
date: "2010-02-10T00:00:00"
file: "lernroboter.zip"
konstrukteure: 
- "Hannes1971"
uploadBy:
- "Hannes1971"
license: "unknown"
legacy_id:
- /data/downloads/software/lernroboter.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/lernroboter.zip -->
Neuprogrammierung des Teach-In Ronoters aus FT der Ersten Generation für C64 in ROBO Pro.