---
layout: "file"
hidden: true
title: "Schräg-Aufzug"
date: "2008-03-08T00:00:00"
file: "schrgaufzug.zip"
konstrukteure: 
- "Laserman"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/software/schrgaufzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/schrgaufzug.zip -->
Ein sehr einfaches Programm mit Meldung der jeweiligen Tätigkeit.