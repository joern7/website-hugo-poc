---
layout: "file"
hidden: true
title: "RoboTXC Logik Analyser v100 VisualBasic 2010"
date: "2015-07-12T00:00:00"
file: "robotxclogikanalyserv100.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/robotxclogikanalyserv100.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/robotxclogikanalyserv100.zip -->
The files shown here are an example to program the RoboTX Controller with Fischertechnik Microsoft VisualBasic 2010 Express. Feel free to check it out. You can copy or distribute the project, but you must pay attention to the rights of Microsoft, Fischertechnik and others.

 To publish your results would be appreciated by the community, e.g. under www.ftcommunity.de

 No guarantee, all on your own risk.
