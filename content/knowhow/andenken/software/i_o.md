---
layout: "file"
hidden: true
title: "Simple I/O"
date: "2006-04-07T00:00:00"
file: "i_o.zip"
konstrukteure: 
- "thkais"
uploadBy:
- "thkais"
license: "unknown"
legacy_id:
- /data/downloads/software/i_o.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/i_o.zip -->
Die Programmierung der Ein- und Ausgänge des RoboInterface in C ist etwas kryptisch. In der Zip-Datei befinden sich Subroutinen, die die Ansteuerung vereinfachen. 