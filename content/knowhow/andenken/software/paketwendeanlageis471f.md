---
layout: "file"
hidden: true
title: "Paketwendeanlage + IR-Detektor-ISF471F IR-LED type L-934F3BT 940nm "
date: "2010-11-22T00:00:00"
file: "paketwendeanlageis471f.rpp"
konstrukteure: 
- "Peter Poederoyen NL"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/software/paketwendeanlageis471f.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/paketwendeanlageis471f.rpp -->
Die Paketwendeanlage hat die Aufgabe, den Strichcode (=weisses Etiket) des Paketes zu erkennen und das Paket danach zur weiteren Bearbeitung weiterzugeben. Wird den Strichcode (=weisses Etiket) nicht erkennt, dan muss das Paket gewendet werden. Nur beim erkennen der Strichcode (=weisses Etiket) mit dem IR-Detektor-ISF471F gibt es bei I7 ein 1 ----> 0 Übergang.   Laufkatze-Motor EM1-M4 kommt wieder zum Einsatz und beginnt auch das Förderband das Paket in Richtung Anlagenende zu befördern.

