---
layout: "file"
hidden: true
title: "Industry Robots 3Achser"
date: "2009-08-15T00:00:00"
file: "steurung2.1.5.rpp"
konstrukteure: 
- "Laurens Wagner"
uploadBy:
- "Laurens Wagner"
license: "unknown"
legacy_id:
- /data/downloads/software/steurung2.1.5.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/steurung2.1.5.rpp -->
Dieses Programm ist ein Steuerungsprogramm für den 3-Achs Roboter aus dem Industry robots Set.mit Lageregler und Co.