---
layout: "file"
hidden: true
title: "RoboTXTC link to VisualBasic2010 digital I/O camera access and bar code reader"
date: "2016-07-01T00:00:00"
file: "rtxtc_vb_barcode.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/rtxtc_vb_barcode.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/rtxtc_vb_barcode.zip -->
This software is an example how to link the fischertechnik RoboTXT controller to MS Visual Basic (VB) 2010 or higher. So VB gets access to the digital I/O connectors of the controller and the video device. Bar code encoding is also implemented.

Feel free to check it out. You can copy or distribute the programm together with this information, 

but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail