---
layout: "file"
hidden: true
title: "FT Datenlogger Beta"
date: "2011-01-04T00:00:00"
file: "ftdatenloggerbeta.zip"
konstrukteure: 
- "Martin"
uploadBy:
- "Martin"
license: "unknown"
legacy_id:
- /data/downloads/software/ftdatenloggerbeta.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftdatenloggerbeta.zip -->
Datenloggen für Ultraschallsensor am 1.Port des TX-Controllers.
Eine Version für das Robo Interface und andere Sensoren an andere Ports ist in Bearbeitung.
