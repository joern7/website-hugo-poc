---
layout: "file"
hidden: true
title: "Ada 1438 motorshield on the TXT Controller"
date: "2018-09-24T00:00:00"
file: "ada1438_drivertxtcontroller.rpp"
konstrukteure: 
- "fotoopa"
uploadBy:
- "fotoopa"
license: "unknown"
legacy_id:
- /data/downloads/software/ada1438_drivertxtcontroller.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ada1438_drivertxtcontroller.rpp -->
Ada 1438 motorshield on the TXT Controller. Onlie-mode 4 servos and 4 motors. User Interface on the PC screen. Motor speed setting control, servo postition control.