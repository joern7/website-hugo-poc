---
layout: "file"
hidden: true
title: "serial.c simple demo of the serial interface in C"
date: "2009-03-13T00:00:00"
file: "serial.c"
konstrukteure: 
- "Ad van der Weiden"
uploadBy:
- "Ad van der Weiden"
license: "unknown"
legacy_id:
- /data/downloads/software/serial.c
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/serial.c -->
This example program shows how to use the serial interface
   on the RoboInterface in the simplest way. That means no interrups,
   no buffering, nothing. By way of example we output a memory range
   as a motorola S file to a the PC. If you have a terminal emulator like 
   Putty (freeware) you can capture the output and save it to a .mot file.
   This file you can inspect in a text editor or, when you remove the 
   header line from putty, even load it as a module in the Renesas debugger!
   This will open a whole new world of possibilities.