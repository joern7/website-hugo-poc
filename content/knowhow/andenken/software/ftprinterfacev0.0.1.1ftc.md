---
layout: "file"
hidden: true
title: "ftPrinterFace v0.0.1.1"
date: "2018-06-13T00:00:00"
file: "ftprinterfacev0.0.1.1ftc.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/ftprinterfacev0.0.1.1ftc.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftprinterfacev0.0.1.1ftc.zip -->
ftPrinterFace v0.0.1.1

software interface for a fischertechnik thermal printer, with picture preparation.

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail