---
layout: "file"
hidden: true
title: "Eucalypta Hexe"
date: "2007-01-13T00:00:00"
file: "eucalyptascenario1.rpp"
konstrukteure: 
- "Peter Damen (Poederoyen, Holland)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/software/eucalyptascenario1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/eucalyptascenario1.rpp -->
Meine Hexe hat die folgende Funktionen:

Kopf in die höhe (pneumatisch)
Nein-Bewegung (electrisch) 
Ja-Bewegung (electrisch) 
Zunge Bewegung (electrisch) 
Augebraue Bewegung(pneumatisch) 
Wange Bewegung(pneumatisch) 
Finger Bewegung(pneumatisch) 
Hand schwingen(electrisch) 

Für alle Bewegungen habe ich ein Unterprogramm. 

Ich möchte aber in das Hauptprogram gerne verschiedene Unterprogrammen zugleich funktionieren lassen. Wie schaffe ich dass ?...............
