---
layout: "file"
hidden: true
title: "univInt2PIC"
date: "2014-09-27T00:00:00"
file: "parint2pic.zip"
konstrukteure: 
- "xbach"
uploadBy:
- "xbach"
license: "unknown"
legacy_id:
- /data/downloads/software/parint2pic.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/parint2pic.zip -->
c-Programme und Layout für Microkontrollersteuerung eines
alten FT-Interfaces  