---
layout: "file"
hidden: true
title: "Vier Gewinnt als Visual Studio .NET 2003-Projekt"
date: "2005-11-19T00:00:00"
file: "VierGewinnt.zip"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/software/VierGewinnt.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/VierGewinnt.zip -->
Das hier ist ein kleines Progrämmchen incl. Quellcode, was ich mal zum .NET-Üben geschrieben habe. Man kann gegen den Rechner Vier Gewinnt spielen. Es hat nichts mit ft zu tun.