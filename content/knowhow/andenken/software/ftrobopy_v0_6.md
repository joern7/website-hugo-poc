---
layout: "file"
hidden: true
title: "ftrobopy"
date: "2016-01-17T00:00:00"
file: "ftrobopy_v0_6.zip"
konstrukteure: 
- "Torsten"
uploadBy:
- "Torsten"
license: "unknown"
legacy_id:
- /data/downloads/software/ftrobopy_v0_6.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftrobopy_v0_6.zip -->
Neue ftrobopy-Version 0.6 mit Kamera-Support und verbesserter Kommunikation mit dem TXT. Diese Version kann sowohl mit Python2.7 als auch mit Python3.x verwendet werden und unterstützt jetzt auch Sessions in einer interaktiven Python-Konsole (z.B. fürs Debugging).