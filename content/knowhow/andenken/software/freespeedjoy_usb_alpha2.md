---
layout: "file"
hidden: true
title: "FreeSpeedJoy - USB"
date: "2007-04-24T00:00:00"
file: "freespeedjoy_usb_alpha2.zip"
konstrukteure: 
- "Markus Mack"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/software/freespeedjoy_usb_alpha2.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/freespeedjoy_usb_alpha2.zip -->
Da doch ab und zu jemand danach fragt: Hier eine Vorab-Version meiner Joystick-Steuerungssoftware, die auch über USB, und theoretisch sogar mit dem Data Link per Funk, funktionieren sollte.
Achtung: Soweit ich weiß funktioniert die Endtaster-Abschaltung mit dieser Version nicht.
Enthält den umfish-Treiber von Ulrich Müller, http://www.ftcomputing.de