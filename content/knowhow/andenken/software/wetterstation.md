---
layout: "file"
hidden: true
title: "Wetterstation"
date: "2007-03-05T00:00:00"
file: "wetterstation.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/software/wetterstation.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/wetterstation.rpp -->
A1->Solarzelle
I1->Taster mit Impulsrad, an "Windmühle" angeschlossen
AX->Temperatursensor
mit Max&Min-Werte-Speicherung
