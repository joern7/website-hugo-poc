---
layout: "file"
hidden: true
title: "TX-Treiber"
date: "2014-11-16T00:00:00"
file: "tx.dll"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/tx.dll
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/tx.dll -->
Treiber für Controller ab firmware 1.30,   ftMscLib.dll (13.11.2012) erforderlich
