---
layout: "file"
hidden: true
title: "PolarFish - Ansteuerung des Plotters von 1985"
date: "2009-12-30T00:00:00"
file: "polarfish0.1.3.zip"
konstrukteure: 
- "Marcus Schulz"
uploadBy:
- "Marcus Schulz"
license: "unknown"
legacy_id:
- /data/downloads/software/polarfish0.1.3.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/polarfish0.1.3.zip -->
Mit der Software lässt sich derzeit der Plotter ansteuern. Zwei vordefinierte Shapes lassen sich plotten. Befindet sich in er Entwicklung, wird ständig erweitert.
Die aktuellste Version finder sich immer unter:

http://polarfish.sourceforge.net