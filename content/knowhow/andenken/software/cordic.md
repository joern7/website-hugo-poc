---
layout: "file"
hidden: true
title: "CORDIC processor for sin, cos arctan, magnitude"
date: "2008-02-03T00:00:00"
file: "cordic.rpp"
konstrukteure: 
- "Ad"
uploadBy:
- "Ad"
license: "unknown"
legacy_id:
- /data/downloads/software/cordic.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/cordic.rpp -->
Two subroutines that can be used for several trigonometry calculations