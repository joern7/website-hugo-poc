---
layout: "file"
hidden: true
title: "Paketwendeanlage"
date: "2010-11-07T00:00:00"
file: "paketwendeanlage.rpp"
konstrukteure: 
- "Peter Poederoyen NL"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/software/paketwendeanlage.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/paketwendeanlage.rpp -->
Die Paketwendeanlage hat die Aufgabe, den Strichcode des Paketes zu erkennen und das Paket danach zur weiteren Bearbeitung weiterzugeben.  Die Anlage besteht aus 8 Motoren, von denen 7 Motoren in Rechts- und Linkslauf und variabler Drehzahl betrieben werden müssen. Des Weiteren sind 2 elektropneumatische Ventile zur Steuerung des Geifers und diverse Endtaster montiert.