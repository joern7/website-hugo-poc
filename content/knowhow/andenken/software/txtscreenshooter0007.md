---
layout: "file"
hidden: true
title: "Robo TXT Screen Shot"
date: "2017-12-24T00:00:00"
file: "txtscreenshooter0007.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/txtscreenshooter0007.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/txtscreenshooter0007.zip -->
txtScreenShot (txtScreenShooter)

This (Windows) software makes it possible to catch a screen shot from the fischertechnik Robo TXT controller. The bitmap of the TXT-display is transfered to the PC and converted from RGB565 to standard RGB 24bit format.

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail