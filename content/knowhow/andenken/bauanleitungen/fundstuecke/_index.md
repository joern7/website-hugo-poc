---
title: "Fundstücke"
weight: 1
---
Hier werden einzelne Bauanleitungen gesammelt,
die sich in der [ft-datenbank](https://ft-datenbank.de)
nicht gefunden haben.
