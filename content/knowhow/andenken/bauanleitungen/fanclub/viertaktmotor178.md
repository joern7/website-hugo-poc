---
layout: "file"
hidden: true
title: "ft-Club Bauanleitung 1 von 1978 "
date: "2007-02-17T00:00:00"
file: "viertaktmotor178.pdf"
konstrukteure: 
- "Walter Golz"
uploadBy:
- "Walter Golz"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/viertaktmotor178.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/viertaktmotor178.pdf -->
Hier wird ein Einzylinder-Viertaktmotor gebaut mit Antrieb, Nockenwelle und Ventilen.
