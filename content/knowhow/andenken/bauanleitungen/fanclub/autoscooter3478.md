---
layout: "file"
hidden: true
title: "ft-Club Bauanleitung 3/4 von 1978 "
date: "2007-02-17T00:00:00"
file: "autoscooter3478.pdf"
konstrukteure: 
- "Walter Golz"
uploadBy:
- "Walter Golz"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/autoscooter3478.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/autoscooter3478.pdf -->
Seit mehr als 40 Jahren und bis heute gibt es auf jedem größeren Rummelplatz 
den AutoScooter. Das Modell verfügt über eine Halle mit zwei selbstfahrenden Fahrzeugen, die
Stromabnehmer wie auf der Kirmes besitzen. An Fremdmaterial wird metallisches
Fliegengitter und Alufolie benötigt.
