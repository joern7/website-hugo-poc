---
layout: "file"
title: "Minimalistischer Präzisionsplotter"
hidden: "true"
date: "2012-07-07T00:00:00"
file: "plotter.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "con.barriga"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/plotter_dfox_01.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/plotter_dfox_01.pdf -->
Bauanleitung zum von Dirk Fox konstruierten "Minimalistischer Präzisionsplotter".
