---
layout: "file"
hidden: true
title: "Bandsäge"
date: "2013-04-25T00:00:00"
file: "bandsge.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/software/bandsge.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/bandsge.zip -->
Kleines Modell einer Bandsäge. Kann man mit den Kästen "Fisichertechnik in der Schule" bauen. (Grundschule) mit PDF-Anleitung zum Ausdrucken 
