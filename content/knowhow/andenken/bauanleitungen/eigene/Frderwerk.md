---
layout: "file"
title: "Kettenförderwerk"
hidden: "true"
date: "2005-10-21T00:00:00"
file: "Frderwerk.pdf"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/Frderwerk.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/Frderwerk.pdf -->
Dabei handelt es sich um ein Modul, welches in meinem Industriemodell zur Convention 2004 eigebaut war. Es dient zur Beförderung von Werkstücken, die in einer Taktstraße an verschiedenen Stationen bearbeitet werden. Das Laufwerk kann bautechnisch an jede Situation individuell angepaßt werden.
