---
layout: "file"
hidden: true
title: "Instructables Scorpion"
date: "2010-07-27T00:00:00"
file: "nook_scorpion_cute.pdf"
konstrukteure: 
- "Richard Mussler-Wright"
uploadBy:
- "Richard Mussler-Wright"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/nook_scorpion_cute.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/nook_scorpion_cute.pdf -->
This is the scorpion model I developed for the Instructables Author Gift Exchange in a pdf.