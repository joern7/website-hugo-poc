---
layout: "file"
title: "Präzisionsplotter"
hidden: "true"
date: "2012-07-07T00:00:00"
file: "praezisionsplotter.zip"
konstrukteure: 
- "con.barriga"
uploadBy:
- "con.barriga"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/plotter_dfox_teil1.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/plotter_dfox_teil1.pdf -->
Bauanleitung Minimalistischer Präzisionsplotter von Dirk Fox
