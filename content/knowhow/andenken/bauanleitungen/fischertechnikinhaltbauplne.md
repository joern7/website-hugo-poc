---
layout: "file"
hidden: true
title: "Inhalt Hobby-Bücher"
date: "2008-01-07T00:00:00"
file: "fischertechnikinhaltbauplne.xls"
konstrukteure: 
- "Andreas - Laserman"
uploadBy:
- "Andreas - Laserman"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/hobbybcher/fischertechnikinhaltbauplne.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/hobbybcher/fischertechnikinhaltbauplne.xls -->
Erst mal ein Dank an Thomas Kaiser, für sein gigantisches Werk, 18 Bücher einzuscannen mit jeweils 100 Seiten. Das macht 1800 Seiten!!!

Damit hat er viele schöne Modelle der Nachwelt erhalten. 

{Hier kann man sich die Bücher runterladen: 
http://www.ft-fanpage.de/FT/hobby_.htm.  
Außerdem den gleichen Dank an Edgar Hofer, dessen Werk man sich hier im Forum runterladen kann unter ==> Downloads ==> Bauanleitungen ==> Hobbybücher.   
Anmerkung des Site-Admins: Die angegebenen Links funktionieren jetzt (Stand Juni 2019) nicht mehr. PDF-Scans der Hobbybücher sind aktuell in der [FTDB](http://ft-datenbank.de) zu finden.}

In meiner Datei finden sich nun alphabetisch geordnet die 887 Modelle aus den Büchern. Vom Addierenden Zählwerk bis hin zur Zeitmeß-Anlage. Mit Zuordnung, in welchem Buch man das Modell findet.

Vieles was man sucht, wurde schon einmal ca. 1960-1970 von Arthur Fischer vorgestellt!!! 

Man versteht Dinge aus dem Alltag, wie z.B. KFZ-Technik, Elektronik-Schaltungen, und vieles vieles mehr!!!
