---
layout: "file"
hidden: true
title: "ftdb-Datenbestand Januar 2013"
date: "2013-02-01T00:00:00"
file: "ftdb_20130201.zip"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/ftdb_20130201.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/ftdb_20130201.zip -->
Diese ZIP-Datei enthält den vollständigen ft:db-Datenbestand Januar 2013 als CSV-Dateien:

a) "Flache Stückliste.csv" ist für einfache Bedürfnisse gedacht. Darin sind zu den Kästen und enthaltenen Teilen jeweils die älteste und jüngste Artikelnummer aufgeführt.

b) Im Unterordner "ftdb" findet sich ein Satz CSV-Dateien mit dem vollständigen Datenbankinhalt (bis auf Bilder und Dokumente selbst). Im Unterornder "Doku" befindet sich eine HTML-Datei mit der vollständigen Dokumentation der Datenbank von SF Parts, dem Windows-Programm für die Inhaltspflege der ft:db. Darin sind auch einige Tabellen wie Wiedervorlagen etc. enthalten, die die ft:db selbst nicht enthält, sowie viele interne Detailangaben, die ihr nicht benötigen werden - bitte einfach mit Mut zur Lücke verwenden.

Bei allen Fragen am besten im Forum posten.