---
layout: "file"
hidden: true
title: "Einzelteilliste des ft-Baukastens 30401 Multikit"
date: "2006-10-24T00:00:00"
file: "30401multikit.xls"
konstrukteure: 
- "Oliver Recktenwald"
uploadBy:
- "Oliver Recktenwald"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/30401multikit.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/30401multikit.xls -->
