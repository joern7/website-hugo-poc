---
layout: "file"
hidden: true
title: "fischertechnik-Datenbank 2.0 Auszug Stand 2010-08-28"
date: "2010-08-28T00:00:00"
file: "fischertechnikdatenbank2.0auszugstand20100828.zip"
konstrukteure: 
- "steffalk"
uploadBy:
- "steffalk"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/fischertechnikdatenbank2.0auszugstand20100828.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/fischertechnikdatenbank2.0auszugstand20100828.zip -->
Eine Microsoft Excel 97 - 2003 Datei mit folgenden Auszügen aus der ftdb 2.0: Artikelnummern (auch bei mehreren Nummern für denselben Artikel), Stücklisten, und Informationen über Bilder einschließlich Link direkt zum Bild auf http://www.ft-datenbank.de