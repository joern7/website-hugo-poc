---
layout: "file"
hidden: true
title: "30097 Jumbo-Jet.xls"
date: "2006-03-04T00:00:00"
file: "30097_Jumbo-Jet.xls"
konstrukteure: 
- "Hans-Werner Petersen"
uploadBy:
- "Hans-Werner Petersen"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/30097%20Jumbo-Jet.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/30097%20Jumbo-Jet.xls -->
Excel-Einzelteilübersicht, importierbar in der "großen" Einzelteiltabelle.
von Hans-Werner Petersen, soweit nicht anders angegeben.