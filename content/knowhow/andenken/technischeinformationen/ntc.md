---
layout: "file"
hidden: true
title: "Zusammenhang IF-NTC"
date: "2008-04-13T00:00:00"
file: "ntc.xls"
konstrukteure: 
- "equester"
uploadBy:
- "equester"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/ntc.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/ntc.xls -->
Durch 65 Messwerte mit dem NTC habe ich den Zusammenhang zwischen Interface-Wert und der entsprechenden Temperatur, bzw. zwischen dem Interface-Wert und dem entsprechenden Widerstand ermittelt.