---
layout: "file"
hidden: true
title: "NTC-Widerstand"
date: "2008-01-30T00:00:00"
file: "36437ntcwiderstand_k164_15k_.pdf"
konstrukteure: 
- "swalter"
uploadBy:
- "swalter"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/36437ntcwiderstand_k164_15k_.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/36437ntcwiderstand_k164_15k_.pdf -->
Umrechnungstabelle der Zuordnung
Temperatur --> Widerstandswert