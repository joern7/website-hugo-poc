---
layout: "file"
hidden: true
title: "ft:pedia werbeplakat"
date: "2011-04-13T00:00:00"
file: "ftpediawerbeplakat.pdf"
konstrukteure: 
- "M. Endlich"
uploadBy:
- "M. Endlich"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/ftpediawerbeplakat.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/ftpediawerbeplakat.pdf -->
ft:pedia werbeplakat zum aufhängen in schulen, läden....