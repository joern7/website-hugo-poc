---
layout: "file"
hidden: true
title: "ELO Sonderheft 255"
date: 2020-04-23T09:58:02+02:00
file: "ELO_Sonderheft_255_V2.pdf"
konstrukteure: 
- "-?-"
uploadBy:
- "website-team"
license: "unknown"
---
**"Roboter selbst steuern - Komplizierte Technik einfach erklärt"**

Sonderheft Nr. 255 der Zeitschrift ELO aus dem Jahr 1987.
Erschienen bei Franzis-Verlag GmbH, München.

Danke an Reiner (Sulu007) für den Scan. 
