---
layout: "file"
hidden: true
title: "ChipInside-Fischertechnik-Buch.zip"
date: "2013-10-20T00:00:00"
file: "chipinsidefischertechnikbuch.zip"
konstrukteure: 
- "Volker-James Münchhof"
uploadBy:
- "Volker-James Münchhof"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/chipinsidefischertechnikbuch.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/chipinsidefischertechnikbuch.zip -->
Scan des Buches von Horst F. Haupt "fischertechnik PROFI COMPUTING Modellsteuerung mit dem Computer" von 1992 und Kopie der zugehörigen Software. Mit freundlicher Genehmigung des Verlages "Vogel Business Media GmbH & Co. KG". Bereitgestellt von qincym und FtfanClub. Zum Download aufbereitet von qincym.

Buch       :  ChipInside-Fischertechnik-Buch.zip
Software
Floppy     :  ChipInside-Fischertechnik-Floppy.zip
installiert:  ChipInside-Fischertechnik-CHIP1292.zip