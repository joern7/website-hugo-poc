---
layout: "file"
hidden: true
title: "Excel-Datei für Conrad Sichtlagerkästen"
date: "2008-09-10T00:00:00"
file: "fischertechnik_neu.ods"
konstrukteure: 
- "Daniel"
uploadBy:
- "Daniel"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/fischertechnik_neu.ods
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/fischertechnik_neu.ods -->
Eine Liste von fast allen ft-Bauteilen
für die Sichtlagerkästen von Conrad.Wenn man sie gut ausschneidet passen sie mit etwas Freiraum an den Seiten in die Kästen.
Link zu den Kästen:
http://www.conrad.de/goto.php?artikel=813112