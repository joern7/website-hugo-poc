---
layout: "file"
hidden: true
title: "Preisauschreiben 1978 Statik-Aktion"
date: "2007-02-17T00:00:00"
file: "preisausschreibenstatik.pdf"
konstrukteure: 
- "Walter Golz"
uploadBy:
- "Walter Golz"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/preisausschreibenstatik.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/preisausschreibenstatik.pdf -->
Flyer für das 
Preisauschreiben von 1978 
es gab eine Statik-Aktion
jeder Gewinner bekam einen Gutschein