---
layout: "file"
hidden: true
title: "zyklisch variables Getriebe / Pedalantrieb"
date: "2008-05-07T00:00:00"
file: "pedalantriebfotosschema.pdf"
konstrukteure: 
- "Wihelm Klopmeier"
uploadBy:
- "Wihelm Klopmeier"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/pedalantriebfotosschema.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/pedalantriebfotosschema.pdf -->
(Aus einer email von Wilhelm Klopmeier):
Mit dem Getriebe soll die Leistungsabgabe des menschlichen Körpers verbessert werden. Nach meinen Überlegungen und nach einer Recherche sportmedizinischer Untersuchungen kann eine Verbesserung von ca. 10% erreicht werden. Problematisch ist aber noch eine praxistaugliche Konstruktion - die auftretenden Kräfte sind erheblich. Die Unterlagen halte ich jedoch zunächst zurück, damit eine Diskussion nicht beeinflußt wird.