---
layout: "file"
hidden: true
title: "Rätselsammlung"
date: "2010-04-03T00:00:00"
file: "ftraetsel.zip"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/ftraetsel.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/ftraetsel.zip -->
Rätsel und Knobelleien rund um fischertechnik.

Viel Spaß beim lösen!