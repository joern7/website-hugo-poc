---
layout: "file"
hidden: true
title: "Das selbsteinparkende Auto"
date: "2006-04-24T00:00:00"
file: "selbsteinparkendes_auto.pdf"
konstrukteure: 
- "joerka"
uploadBy:
- "joerka"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/selbsteinparkendes_auto.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/selbsteinparkendes_auto.pdf -->
"Schüler experimentiert"-Projekt. Langfassung + Bilder (PDF)