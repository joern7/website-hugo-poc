---
layout: "file"
hidden: true
title: "Lanz-Bulldog Sammelprospekt 1937"
date: "2012-08-26T00:00:00"
file: "lanzbulldogsammelprospekt_1937.pdf"
konstrukteure: 
- "Peter Poederoyen NL"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/lanzbulldogsammelprospekt_1937.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/lanzbulldogsammelprospekt_1937.pdf -->
Der Lanz Bulldog ist ein vom deutschen Hersteller Lanz entwickelter Traktortyp. Der vom deutschen Ingenieur Fritz Huber konstruierte Traktor hatte einen liegenden Einzylinder-Zweitakt-Glühkopfmotor. Durch den Einsatz dieses Einzylinders hatte er einen charakteristischen Klang, eben den typischen Bulldog-Sound: Das Stampfen im Leerlauf und
das mächtige Tuckern beim Fahren.

De Lanz Bulldog is een type tractor ontwikkeld door de Duitse fabrikant Lanz.
Ontworpen door de Duitse ingenieur Fritz Huber, had de tractor een liggende één-cilinder tweetakt gloeikopdieselmotor. Door gebruik van deze ééncilinder had die een karakteristiek geluid, een staccato "boem-boem" geluid.
