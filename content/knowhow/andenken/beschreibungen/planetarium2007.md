---
layout: "file"
hidden: true
title: "Planetarium"
date: "2007-09-21T00:00:00"
file: "planetarium2007.pdf"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/planetarium2007.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/planetarium2007.pdf -->
Begleittexte und
Grafiken meines
Modells von 2007
- Bilder in der
Galerie