---
layout: "file"
hidden: true
title: " fischertechnik Brückenlegepanzer M48 mit Scherenfaltbrücke"
date: "2008-05-06T00:00:00"
file: "fischertechnikbrugleggertankm48metschaarbrug.pdf"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/fischertechnikbrugleggertankm48metschaarbrug.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/fischertechnikbrugleggertankm48metschaarbrug.doc -->

Ich habe versucht, diesen Brückenlegepanzer M48 mit Scherenfaltbrücke mit f ischertechnik nachzubauen. Über Google war es extrem einfach, ein sehr detailliertes technisches Handbuch innerhalb von 3 Minuten herunterzuladen. Dann habe ich angefangen. Die großen wirkenden Kräfte sowie die Einschränkungen bestimmter Fischertechnik-Teile waren der
größte Stolperstein bei der Konstruktion des Brückenlegepanzers M48 mit Scherenfaltbrücke.

### Fischertechnik Bruglegger-tank M48 met schaarbrug 

Ik heb geprobeerd deze bruglegger-tank M48 met schaarbrug met Fischertechnik na te bouwen. Via Google was het buitengewoon eenvoudig binnen 3 minuten een zeer gedetailleerde technische handleiding te downloaden. Vervolgens ben ik aan de gang gegaan. De grote optredende krachten, alsmede de beperkingen van bepaalde Fischertechnik-onderdelen zijn het grootste struikelblok geweest bij de bouw van de bruglegger-tank M48 met schaarbrug. 
