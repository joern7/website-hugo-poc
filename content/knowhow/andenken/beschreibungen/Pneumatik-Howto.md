---
layout: "file"
hidden: true
title: "Selbstbau Pneumatikzylinder"
date: "2005-05-20T00:00:00"
file: "Pneumatik-Howto.pdf"
konstrukteure: 
- "Michael Orlik"
uploadBy:
- "Michael Orlik"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/Pneumatik-Howto.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/Pneumatik-Howto.pdf -->
Michael Orlik baut selber Pneumatikzylinder, die ins Fischertechnik-Raster passen, jedoch deutlich mehr Hub haben.
Hier hat er eine ausführliche Anleitung geschrieben wie er diese herstellt. Die Anleitung sollte es jedem, der über die notwendigen Maschinen verfügt, möglich machen sich selber Zylinder zu bauen.