---
layout: "file"
hidden: true
title: "Präzisionsplotter"
date: "2005-05-20T00:00:00"
file: "Praezisionsplotter.pdf"
konstrukteure: 
- "Martin Romann"
uploadBy:
- "Martin Romann"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/Praezisionsplotter.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/Praezisionsplotter.pdf -->
Bau und Gedanken über den Bau eines hochpräzisen Plotters aus Fischertechnik