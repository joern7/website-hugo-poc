---
layout: "file"
hidden: true
title: "Bahnkreisende oszillierende steigende schiefe Ebene"
date: "2011-11-03T00:00:00"
file: "frderschnecke.pdf"
konstrukteure: 
- "Stefan Barth"
uploadBy:
- "Stefan Barth"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/frderschnecke.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/frderschnecke.pdf -->
Eine andere Möglichkeit um Waren von unten nach oben zu befördern.