---
layout: "file"
hidden: true
title: "Codescheibe"
date: "2008-08-08T00:00:00"
file: "codescheibe_4.bmp"
konstrukteure: 
- "fitec"
uploadBy:
- "fitec"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/codescheibe_4.bmp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/codescheibe_4.bmp -->
Das ist eine Codescheibe für die Gabellichtschranke CNY37. Sie ist für alle fischertechnik-Motoren geeignet, da sie nur 8 Flanken hat (4schwarze Striche und 4 weiße Striche). Somit kann man auch die Impulse an einem 8:1 Power-Motor zählen. Der Achsendurchmesser ist 4mm.
