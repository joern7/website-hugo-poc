---
layout: "file"
hidden: true
title: "Statikhilfe"
date: "2006-02-05T00:00:00"
file: "statikhilfe.pdf"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/statikhilfe.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/statikhilfe.pdf -->
Wer schon einmal verstrickte Statikmodelle konstruiert hat, wird sich oftmals
daran gestört haben, immer und immer wieder mit dem Taschenrechner eine
bestimmte Strebenlänge für irgendeine Diagonale berechnen zu müssen. Ich
habe diesbezüglich vor geraumer Zeit eine Tabelle zusammengestellt, in der
sämtliche Kombinationen aus Streben und Laschen aufgeführt sind. Darüber
hinaus werden im 2. Teil alle möglichen Diagonalabstände in vielen erdenkli-
chen Varianten korrekt berechnet und chronologisch dargestellt.