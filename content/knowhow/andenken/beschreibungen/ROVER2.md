---
layout: "file"
hidden: true
title: "Mars Rover"
date: "2005-05-20T00:00:00"
file: "ROVER2.pdf"
konstrukteure: 
- "Holger Howey"
uploadBy:
- "Holger Howey"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/ROVER2.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/ROVER2.pdf -->
Holger Howey hat ein Modell des "Sojurner" gebaut, das auch einfach zum Nachbauen ist.
Er hat hier eine sehr schöne und bilderreiche Anleitung mit genauen Teilelisten erstellt.