---
layout: "file"
hidden: true
title: "Glockenspiel - Info und Dokumentation"
date: "2010-04-17T00:00:00"
file: "CarillonGlockenspiel.pdf"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/carillon.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/carillon.doc -->
Carillon - Info und Dokumentation als Grundlage für ein fischertechnik-Glockenspiel mit 8 Stahlglocken C3-C4. Das Hauptprogramm von Robo-Pro enthält nacheinander verschiedene Melodien. Ich habe für jede Melodie und jeden Ton ein eigenes Unterprogramm erzeugt. Ich habe die Glocken direkt mit Hilfe von Elektromagneten und 4 mm verchromten Stahlachsen mit einem Messing-Hammerstück zum Laufen gebracht. Für jeden Ton wird ein Elektromagnet für 0,08 Sekunden eingeschaltet, gefolgt von einer Standardwartezeit von 0,4 Sekunden. Für jede Melodie füge ich bei Bedarf zusätzliche Wartezeiten hinzu.

Carillon-info en documentatie als basis voor Fischertechnik -Carillon met  8 stalen klokken C3-C4. 
Robo-Pro-hoofdprogramma bevat achter elkaar verschillende melodieën. Voor elk melodie en toon heb ik een apart subprogramma. Ik heb de klokken direct middels elektro-magneten en 4mm verchroomde stalen stelen met een messing hamerstuk werkend gekregen. Voor elke toon wordt een electromagneet 0,08 sec bekrachtigd gevolgd door een standaard wachttijd van 0,4 sec.  Per melodie voeg ik aanvullende wachttijden toe waar dit nodig is.
