---
title: "Bauteildaten"
weight: 2
---
Wenn aus kleinen Spielkindern große Spielkinder werden, wachsen auch die
Ansprüche.
Aus vielen Kinderzimmer-"fischertechnikern" wuchsen im Laufe der Jahre
Ingenieure heran.
Und so ist es nicht verwunderlich, dass der Bedarf an Wissen um die
verwendeten Bauteile mitgewachsen ist.

Nun ist es wohl eher die
[Elektronik](../elektronik)
und die
[Computerei](../computing)
, die einen nie enden wollenden Informationsdurst auszeichnet.
Aber es gibt da auch noch die etwas grundlegenderen Bauteile wie zum Beispiel
Lämpchen und Motore, die bei falschem Einsatz Schaden leiden.
Pneumatik oder gar Hydraulik 
sind Themen, mit denen sich der interessierte fischertechniker
auch befassen kann.
Sogar zu den mechanischen fischertechnik-Bausteinen selbst gibt es manche
verblüffende Erkenntnis, die nicht von [fischertechnik](https://www.fischertechnik.de) stammt.

<!-- https://www.ftcommunity.de/wiki1b8f.html?action=show&topic_id=40 (Uffi)
=> erledigt -->
<!-- https://www.ftcommunity.de/wikicec6.html?action=show&topic_id=39 (Magier)
=> erledigt -->
