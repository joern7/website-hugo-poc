---
layout: "file"
hidden: true
title: "Motordaten XM-Motor"
date: "2013-12-09T00:00:00"
file: "ft135485_neu.pdf"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/ft135485_neu.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/ft135485_neu.pdf -->
Kennlinien und -werte für XM-Motor (135485) an 9V. Version ohne Tipfehler.