---
layout: "file"
hidden: true
title: "Motordaten S-Motor mit U-Getriebe"
date: "2013-12-09T00:00:00"
file: "ft32293ft31078ft31082_neu.pdf"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/ft32293ft31078ft31082_neu.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/ft32293ft31078ft31082_neu.pdf -->
Kennlinien und -werte
für S-Motor (32293) mit
U-Getriebe (31078 &
31082) an 9V. Version ohne Tipfehler.