---
layout: "file"
hidden: true
title: "Schaltschema Robo TXT Controller I2C mit 5 Volt Sensoren"
date: 2016-01-10T00:00:00
file: "schaltschemarobotxtcontrolleri2cmit5voltsensoren.pdf"
konstrukteure: 
- "Dirk Wölffel"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/schaltplne/schaltschemarobotxtcontrolleri2cmit5voltsensoren.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/schaltschemarobotxtcontrolleri2cmit5voltsensoren.pdf -->
Robo TXT Controller mit RoboPro 4.2.3 Ansteuerung von I2C 5 Volt Sensoren.

{Ergänzung durch Site Admin, 25.5.2019}  
Fans haben festgestellt, dass die vorgestellte Lösung zwei Probleme aufweist:

*  Der 3,3V-Anschluß des TXT ist keine Versorgung, sondern ein Ausgang.
   Genauer: Die serielle Konsole sendet hier. Das kann den I²C-Betrieb stören.
*  Der Elektronik im TXT könnte beschädigt werden, weil die Sendeleitung der
   seriellen Konsole keinen nennenswerten Strom liefern kann.

Eine [verbesserte Lösung](../schaltschemarobotxtcontrolleri2cmit5voltsensoren-1),
die die beschriebenen Probleme umgeht, ist ab sofort verfügbar.
