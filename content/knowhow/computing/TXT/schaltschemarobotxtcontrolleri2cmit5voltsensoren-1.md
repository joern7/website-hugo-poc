---
layout: "file"
hidden: true
title: "Schaltschema Robo TXT Controller I2C mit 5 Volt Sensoren - 2019"
date: 2019-05-25T06:28:32+0100
file: "schaltschemarobotxtcontrolleri2cmit5voltsensoren-1.pdf"
konstrukteure: 
- "Till Harbaum"
uploadBy:
- "MoG"
license: "unknown"
---
Nach Diskussionen im [Forum](https://forum.ftcommunity.de/viewtopic.php?f=8&t=5310)
zu [Dirk Wölffels Beitrag aus 2016](../schaltschemarobotxtcontrolleri2cmit5voltsensoren)
gibt es einen neuen Vorschlag für den Level-Shifter.    

In der Arduino- und Raspberry-Szene gibt es Levelshifter, die auch bei ft eingesetzt werden können. Technisch benötigen die Levelshifter zwei Spannungsversorgungen. Da es beim TXT weder eine nutzbare 3.3V- noch eine 5V-Versorgung gibt, muss diese für den Levelshifter erzeugt werden. Dieses Modell erzeugt die 3.3V mit einem Spannungsregler selbst, so dass es nur eine 5V-Versorgung benötigt.

Wer nicht löten möchte, kann mit dem [ft-Extender Projekt](https://github.com/elektrofuzzis/ftExtender) auf eine fertige Plug&Play-Lösung zurückgreifen. 

