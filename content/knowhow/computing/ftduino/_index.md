---
title: "ftDuino (2017)"
weight: 70
---
Warum soll nicht auch ein gängiger und kostengünstiger Arduino genutzt werden können,
um einfach fischertechnik anzusteuern?
Eben, es gibt nix was dagegen spricht.
Hier geht es also rund - um den ftDuino.

[Mehr Informationen zum ftDuino](https://harbaum.github.io/ftduino/www/de/).
