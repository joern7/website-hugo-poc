---
layout: "file"
hidden: true
title: "Nachbildung E-Tec Programme"
date: "2007-01-22T00:00:00"
file: "etecmodule_prugramme.rpp"
konstrukteure: 
- "Clemens (Clemy)"
uploadBy:
- "Clemens (Clemy)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/etecmodule_prugramme.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/etecmodule_prugramme.rpp -->
Diese Bibileothek enthält Nachbildungen der Programme aus dem E-Tec.
Leider habe ich das Programm "Wechselblinker" nicht hinbekommen