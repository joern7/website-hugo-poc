---
layout: "file"
hidden: true
title: "UP Impulsfahrt"
date: "2010-08-24T00:00:00"
file: "upimpulsfahrt.rpp"
konstrukteure: 
- "Marco Heilmann"
uploadBy:
- "Marco Heilmann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/upimpulsfahrt.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/upimpulsfahrt.rpp -->
RoboPro UP, das zwei Motoren synchronisiert eine vorgegebene Impulszahl mit vorgegebener Geschwindigkeit laufen lässt. Die genaue Beschreibung findet sich im UP selbst.