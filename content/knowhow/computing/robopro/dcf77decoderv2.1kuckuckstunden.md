---
layout: "file"
hidden: true
title: "DCF77 RoboPro-Programm Funkuhr mit LED-Displays + Motorantrieb 2 Kuckucks-Blasebälge mit Pfeife "
date: "2017-02-03T00:00:00"
file: "dcf77decoderv2.1kuckuckstunden.rpp"
konstrukteure: 
- "Peter Poederoyen NL"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoderv2.1kuckuckstunden.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoderv2.1kuckuckstunden.rpp -->
Für 1 mal "Kuckuck" reicht eine Achse-Umdrehung mit Nocke + Schalter.

- Bei CLK-Minuten-Var = 15 oder 30 oder 45 ---> eine Achse-Umdrehung für 1x "Kuckuck" : funktioniert o.k.

- Bei CLK-Stunden-Var Abfragen 1 bis zum 24 ? .....bei CLK-Stunden-Var = 13 = 1x "Kuckuck" funktioniert leider noch nicht einwandfrei.