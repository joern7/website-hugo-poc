---
layout: "file"
hidden: true
title: "RepStrap-Testprogramm"
date: "2017-02-03T00:00:00"
file: "reprap.rpp"
konstrukteure: 
- "Frank Jakob"
uploadBy:
- "Frank Jakob"
license: "unknown"
legacy_id:
- /data/downloads/robopro/reprap.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/reprap.rpp -->
Selbst entwickelter Schrittmotortreiber
für 3 Achsen.Die Achsen lassen sich nur manuell Verfahren. Die Anschlußbelegung entspricht dem Original von Andreas Rozek.