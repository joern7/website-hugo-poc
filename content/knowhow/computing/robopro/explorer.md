---
layout: "file"
hidden: true
title: "Explorer"
date: "2017-02-03T00:00:00"
file: "explorer.rpp"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
uploadBy:
- "Stefan Lehnerer (StefanL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/explorer.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/explorer.rpp -->
Das Programm zu meinem aktuellen Explorer (http://www.ftcommunity.de/categories.php?cat_id=1338&page=2)