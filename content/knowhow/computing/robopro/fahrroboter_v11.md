---
layout: "file"
hidden: true
title: "Fahrroboter mit GPS-Modul Navigatron und Compass CMPS10 v1.1"
date: "2017-02-03T00:00:00"
file: "fahrroboter_v11.zip"
konstrukteure: 
- "Mark Oude Elberink & Georg Stiegler (fantogerch)"
uploadBy:
- "Mark Oude Elberink & Georg Stiegler (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/fahrroboter_v11.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/fahrroboter_v11.zip -->
Programm für einen autonomen Fahrroboter wie hier vorgestellt <a href="http://forum.ftcommunity.de/viewtopic.php?f=6&t=2034&p=15246#p15264" target="_blank">GPS-Navigation mit dem TX</a>. Zusätzlich ein Programm zum Kalibrieren des Compass-Moduls CMPS10.

_MOT -> Programm für "normale" Motoren

_ENC -> Programm für Encoder-Motoren
