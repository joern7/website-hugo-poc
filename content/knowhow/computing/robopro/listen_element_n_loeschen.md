---
layout: "file"
hidden: true
title: "löscht ein beliebiges Element einer Liste"
date: "2007-11-12T00:00:00"
file: "listen_element_n_loeschen.rpp"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/listen_element_n_loeschen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/listen_element_n_loeschen.rpp -->
Realisierung als UP mit Ein- und Ausgang für eine Liste zur einfachen Verwendung in einem eigenen Programm (funktioniert sowohl mit dem ersten als auch mit dem letzten Element einer Liste!)