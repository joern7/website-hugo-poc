---
layout: "file"
hidden: true
title: "Digitaluhr Version 2"
date: "2017-02-03T00:00:00"
file: "digitaluhrversion2.rpp"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/digitaluhrversion2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/digitaluhrversion2.rpp -->
Dies ist das RoboPro-Programm für die Digitaluhr mit der lesbaren Anzeige von 2008. Anschlussbelegung und Beschreibungen finden sich im Programm.