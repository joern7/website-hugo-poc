---
layout: "file"
hidden: true
title: "Robo TX/TXT mit Tablet fernsteuern"
date: "2015-03-16T00:00:00"
file: "robo_tx_txt_tabletsteuerung.rpp"
konstrukteure: 
- "DirkW"
uploadBy:
- "DirkW"
license: "unknown"
legacy_id:
- /data/downloads/robopro/robo_tx_txt_tabletsteuerung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/robo_tx_txt_tabletsteuerung.rpp -->
Mit den RoboPro Bedienelementen und einem Win 8.1 Tablet mit
Bluetooth, könnt ihr bequem euer Modell fernsteuern.