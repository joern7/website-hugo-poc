---
layout: "file"
hidden: true
title: "Fischertechnik Autowaschstrasse "
date: "2017-02-03T00:00:00"
file: "autowaschstrasse.rpp"
konstrukteure: 
- "Peter  Poederoyen NL"
uploadBy:
- "Peter  Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/autowaschstrasse.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/autowaschstrasse.rpp -->
Meine FT-Autowaschstraße simuliert eine vollautomatische Wasch- und Trocknungsanlage für PKW, wie sie beispielsweise an Tankstellen und in Werkstätten eingesetzt wird. Die Autowaschstraße besteht aus einem fahrbaren Portal, an dem ein Gebläsetrockner, eine Horizontalbürste, zwei Vertikalbürsten und zwei Felgenbürsten beweglich angebracht sind. Im Simulationsablauf wird ein PKW-Modell gewaschen und getrocknet. 
