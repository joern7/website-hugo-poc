---
layout: "file"
hidden: true
title: "I2C Treiber für den Ultraschall Entfernungsmesser SRF10"
date: "2017-02-03T00:00:00"
file: "usrangefindersrf10.rpp"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/usrangefindersrf10.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/usrangefindersrf10.rpp -->
Dies ist der I2C-Treiber für den Ultraschall Entfernungsmesser SRF10 von Devantech.
Das Modul kann z.B. hier: http://www.shop.robotikhardware.de
... gekauft werden.