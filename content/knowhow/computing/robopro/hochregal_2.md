---
layout: "file"
hidden: true
title: "Hochregallager"
date: "2017-02-03T00:00:00"
file: "hochregal_2.rpp"
konstrukteure: 
- "Pascal Jan (bauFischertechnik)"
uploadBy:
- "Pascal Jan (bauFischertechnik)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/hochregal_2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/hochregal_2.rpp -->
Mit diesem Programm kann man mein Modell eines Hochregallagers steuern.