---
layout: "file"
hidden: true
title: "ftDirk"
date: "2017-02-03T00:00:00"
file: "cmps03_1.rpp"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/cmps03_1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/cmps03_1.rpp -->
Anschluss eines Kompassmoduls Devantech CMPS03 an den TX-C mit Darstellung der Kompass-Peilung.
