---
layout: "file"
hidden: true
title: "Schritt"
date: "2017-02-03T00:00:00"
file: "schritt.zip"
konstrukteure: 
- "Holger Howey (fishfriend)"
uploadBy:
- "Holger Howey (fishfriend)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/schritt.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/schritt.zip -->
#### Ansteuerung von Schrittmotoren

Dies sind zwei Grundprogramme mit UPs für einen Schrittmotor. Auch diese gebe ich hiermit frei. Das erste Programm dreht den Motor nur um ein winzigen Winkel. Das zweite Programm läßt ihn dauernd rechts drehen. Weitere Motoren können nach dem selben Schema angeschlossen werden. Entweder den zweiten Schrittmotor an M3 und M4 oder in der Spaarversion an M2 und M3. So hat man noch einen weiteren M-Ausgang für andere Dinge frei.
BTW den dritten Schrittmotor kann man in der Spaarvariante an M1 und M3 geben.
Einziges Manko ist, das die anderen Motoren etwas knurren was aber nichts macht, auch nicht an der Genauigkeit.

Wie kopiere ich nun andere Unterprogramme in Robopro bzw in mein eigenes Programm? Ganz einfach. Z.B zuerst das Programm laden das ich bearbeiten möchte, dann das Programm laden, was das Unterprogramm enthält das ich kopieren möchte.
Nun auf das + vor "Geladene Programme" klicken. Nun auf den Namen des Programms (in dem Fall das untere). Es werden nun alle Unterprogramme angezeigt.
Nun auf das eigene erste Programm umschalten, über "Fenster" und die Namen unten (in dem Fall der obere Name). Nun einfach das gewünschte Unterprogramm in das eigene rüberziehen. Fertig.
