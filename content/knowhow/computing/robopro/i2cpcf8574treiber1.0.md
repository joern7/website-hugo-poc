---
layout: "file"
hidden: true
title: "I²C-Treiber LED Ansteuerung PCF8574"
date: "2017-01-12T00:00:00"
file: "i2cpcf8574treiber1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cpcf8574treiber1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cpcf8574treiber1.0.rpp -->
Dieser I2C Treiber kann bis zu 7 LEDs ansteuern (ein-, ausschalten oder blinken), welche am Ausgang von dem Baustein PCF8574 angeschlossen sind, oder die Ausgänge können als digitale Eingänge verwendet werden.
Programmiert in RoboPro V4.2.3