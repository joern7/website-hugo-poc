---
layout: "file"
hidden: true
title: "Taschenrechner version 0.5"
date: "2017-02-03T00:00:00"
file: "taschenrechnerversion0.5.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/taschenrechnerversion0.5.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/taschenrechnerversion0.5.rpp -->
Jetzt kann der Taschenrechner Wurzel rechnen.
Der Taschenrechner rechnent die Wurzel so aus wie ein normaler Taschenrechner. 