---
layout: "file"
hidden: true
title: "I²C-Treiber für 6-achsen MotionTracking Sensor"
date: "2017-02-03T00:00:00"
file: "i2cmpu9150accelerationsensorversion1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cmpu9150accelerationsensorversion1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cmpu9150accelerationsensorversion1.0.rpp -->
The triple-axis MEMS in the MPU-91550 includes a wide range of features:

Gyroscope Features:
&#8226; Digital-output X-, Y-, and Z-Axis angular rate sensors (gyroscopes) with a user-programmable fullscale range of ±250, ±500, ±1000, and ±2000°/sec

Accelerometer Features
&#8226; Digital-output 3-Axis accelerometer with a programmable full scale range of ±2g, ±4g, ±8g and ±16g
&#8226; Orientation detection and signaling
&#8226; Tap detection

Magnetometer Features
&#8226; 3-axis silicon monolithic Hall-effect magnetic sensor with magnetic concentrator
&#8226; Output data resolution is 13 bit (0.3 &#956;T per LSB)
&#8226; Full scale measurement range is ±1200 &#956;T
