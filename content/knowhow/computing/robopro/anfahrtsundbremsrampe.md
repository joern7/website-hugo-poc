---
layout: "file"
hidden: true
title: "Funktionstüchtige Anfahrts- und Brems-"
date: "2007-05-20T00:00:00"
file: "anfahrtsundbremsrampe.rpp"
konstrukteure: 
- "ffcoe / chris"
uploadBy:
- "ffcoe / chris"
license: "unknown"
legacy_id:
- /data/downloads/robopro/anfahrtsundbremsrampe.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/anfahrtsundbremsrampe.rpp -->
Die Positionierungseinhiet von RoboPro wurde so erweiter, dasnun die Motoren am anfang der Strecke Beschleunigt und am Ende der Strecke abgebremst werden können. Eine ausführliche Beschreibung zu jedem Unterprogramm findet ihr in dessen Beschreibung im Programm.
