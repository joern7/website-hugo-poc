---
layout: "file"
hidden: true
title: "I2C-Treiber für Nunchuk"
date: "2017-01-12T00:00:00"
file: "nunchuktreiberv1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/nunchuktreiberv1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/nunchuktreiberv1.0.zip -->
Treiber, um den Nunchuk (Nintendo-Wii-Steuergerät) am I2C-Port des TX zu betreiben (siehe Foto in der [ft:c](http://www.ftcommunity.de/details.php?image_id=36074); Beschreibung folgt).
