---
layout: "file"
hidden: true
title: "Matrix robot"
date: "2018-07-09T00:00:00"
file: "matrixrobotfinal2018_07_02.rpp"
konstrukteure: 
- "fotoopa"
uploadBy:
- "fotoopa"
license: "unknown"
legacy_id:
- /data/downloads/robopro/matrixrobotfinal2018_07_02.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/matrixrobotfinal2018_07_02.rpp -->
Robot to fill 8x8 matrix pattern. Construction parts : Fischertechnik. External hardware via the I2C connector of the TXT Controller. Only very simple commands are needed to control the motors. All inputs and outputs are located on the external hardware and can be read via the Robo software on the Robo TXT Controller. Rotary encoders are home-made and also include a home point for exact positioning. 