---
layout: "file"
hidden: true
title: "Mittelwerte ausrechnen"
date: "2017-02-03T00:00:00"
file: "mittelwert.rpp"
konstrukteure: 
- "Clemy"
uploadBy:
- "Clemy"
license: "unknown"
legacy_id:
- /data/downloads/robopro/mittelwert.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/mittelwert.rpp -->
Dieses Programm kann Mittelwerte ausrechnen. (Die Lösung für fitecs Problem ;))
Es schreibt die Ergebnisse in eine Liste, die dann zur weiteren Verarbeitung dient. Nach jedem einlesen des Analogwertes wird der Mittelwert neu berechnet.