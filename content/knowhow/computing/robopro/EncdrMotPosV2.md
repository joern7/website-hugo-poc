---
layout: "file"
hidden: true
title: "RoboPro Library zur Encodermotor-Positionierung"
date: 2020-06-14T20:32:16+02:00
file: "EncdrMotPosV2.rpp"
konstrukteure: 
- "Helmut (hamlet)"
uploadBy:
- "website-team"
license: "unknown"
---

### Features:

* Die Positionierung jedes Motors läuft auf einem eigenen unabhängigen Prozess. Die Positionierungen können gleichzeitig zueinander und gleichzeitig zum eigentlichen Hauptprogramm-Prozessen erfolgen.
* Einfaches Interface, das der erweiterten Robopro Motorsteuerung entspricht:  
    "Positionsanforderung" und "Warten auf Positionierung"
* Automatischer Spielausgleich bei Richtungsänderungen
* Keine Fortpflanzung kleiner Positionsfehler, wenn der Encoder mal einen Encoder-Tic zu weit gefahren ist, da aufeinanderfolgenden Positionierungen stets basierend auf der gemessenen Position erfolgen.
* Umlauf-Positionierung von Drehtischen: Die Library wählt automatisch den kürzesten Weg zur Zielposition.
* Akkurates Anfahren des Nullpositionsschalters mit separat wählbarer langsamer Geschwindigkeit.
* Der Referenzpositionsschalter lässt sich mit einem Offset virtuell verschieben um die mechanische Justage schwer zu erreichender Referenzpositionsschalter zu vereinfachen.
    
## RoboPro library for encoder motor positioning.

### Features:

* The positioning of each motor runs on its own independent process. The positioning can take place simultaneously to each other and simultaneously to the main program processes.
* The interface is simple and corresponds to the extended RoboPro motor control:  
    "Position request" and "Waiting for positioning"
* Automatic backlash compensation when changing direction
* No propagation of small position errors if the encoder has moved one encoder tic too far, since successive positioning is always based on the measured position.
* Positioning of rotary tables: The library automatically selects the shortest route to the target position.
* Accurate approach to the zero position switch with separately selectable slow speed.
* The reference position switch can be virtually moved with an offset to simplify the mechanical adjustment of reference position switches that are difficult to reach.

Testaufbau ![Testaufbau](../EncoderMotPos_TestModel.jpg)
