---
layout: "file"
hidden: true
title: "Licht an und aus"
date: "2008-05-29T00:00:00"
file: "lichtanundaus.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/lichtanundaus.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/lichtanundaus.rpp -->
Das ist ein Programm das wenn man einen Taster drückt die Lampe schnell an,aus,an, usw. geht und wenn man noch mal auf dem Taster drauf drückt endgültig das licht aus geht.