---
layout: "file"
hidden: true
title: "Niveau-regeling voor 4 stuwen (2x schuif- en 2x segment-)"
date: "2017-02-03T00:00:00"
file: "4stuwenniveauregeling.rpp"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
uploadBy:
- "Peter Damen, Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/4stuwenniveauregeling.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/4stuwenniveauregeling.rpp -->
Niveau-regeling voor 4 stuwen:
-Stuw-1: Ultrasound spanningsuitgang A1,
-Stuw-2: US-FT-D1,
-Stuw-3: US-FT-D2,
-Stuw-4: Potmeter-vlotter weerstand: AY