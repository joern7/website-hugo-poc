---
layout: "file"
hidden: true
title: "quadrature decoder using spursucher and segmentscheibe"
date: "2017-02-03T00:00:00"
file: "quadr.rpp"
konstrukteure: 
- "Ad van der Weiden"
uploadBy:
- "Ad van der Weiden"
license: "unknown"
legacy_id:
- /data/downloads/robopro/quadr.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/quadr.rpp -->

