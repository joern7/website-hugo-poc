---
layout: "file"
hidden: true
title: "DCF77-Decoder v1.0"
date: "2017-02-03T00:00:00"
file: "dcf77decoderv1.0.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoderv1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoderv1.0.rpp -->
Eine erste Version des DCF77-Decoders; zeigt im TX-Display Uhrzeit (Stunden:Minuten) und das Datum (Tag.Monat.Jahr) an. Synchronisiert sich minütlich neu. (Siehe den Thread im Forum unter http://forum.ftcommunity.de/viewtopic.php?f=8&t=1421).