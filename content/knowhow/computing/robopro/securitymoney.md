---
layout: "file"
hidden: true
title: "Tresor-Steuerung"
date: "2017-02-03T00:00:00"
file: "securitymoney.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/robopro/securitymoney.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/securitymoney.rpp -->
Tür wird über Magnetventile und Elektromagnet verriegelt, bei Eingabe von 25963 öffnet sie sich