---
layout: "wiki"
title: "FAQ: RoboPro"
date: 2007-07-08T19:29:31
konstrukteure: 
- "ffcoe"
uploadBy:
- "ffcoe"
license: "unknown"
legacy_id:
- /wiki1d59.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wiki1d59.html?action=show&topic_id=26 -->
<!--
Wiki

Thema: FAQ: RoboPro

Aktuelle Version

von: ffcoe

am: 08.07.2007, 19:29:31 Uhr

Historie:

Version 4 von ffcoe am 08.07.2007, 19:29:12 Uhr
Version 3 von ffcoe am 08.07.2007, 19:28:37 Uhr
Version 2 von ffcoe am 08.07.2007, 19:18:36 Uhr
Version 1 von sven am 24.03.2007, 09:45:09 Uhr
-->


### Prozesseinstellungen im Eigenschaftsfenster:


Anders als LLWin arbeitet ROBOPro wie übliche prozedurale Programmiersprachen
(zum Beispiel C++ oder Pascal) und benötigt für jeden laufenden Prozess einen
so genannten Stackspeicher. Man kann in ROBOPro nur so viele Prozesse gleichzeitig
laufen lassen, wie Stackspeicher vorhanden sind. Da die Stackspeicher viel
Speicherplatz benötigen kann es insbesondere im Downloadmodus eng werden, da
dort nur etwa 40kByte Speicher für Stacks vom Prozessor adressiert werden können.

Normalerweise legt ROBOPro 5 Stacks an, so dass 5 Prozesse gleichzeitig laufen
können. Die Standardgröße der Stacks beträgt 4kByte im Downloadmodus und 64kByte
im Onlinemodus. Im Downloadmodus wird durch die Stacks mit 5x4kByte die Hälfte
des für Stacks und Variablen adressierbaren RAMs verbraucht.

Wenn man mehr als 5 Prozesse gleichzeitig laufen lassen möchte, muss man die
"Mindestzahl Prozesse" im Eigenschaftsfenster eines Unterprogramms vergrößern.
Es spielt dabei keine Rolle in welchem Unterprogramm man die Einstellung vornimmt.
ROBOPro verwendet den Maximalwert der "Mindestanzahl Prozesse" von allen
Unterprogrammen. Auf diese Weise kann man Bibliotheksunterprogrammen, die sehr
viele Prozesse erzeugen, bereits eine geeignete "Mindestzahl Prozesse" mitgeben.

Die Einstellung "Zusätzliche Prozesse" ist nur für Bibliotheksunterprogramme
gedacht, die globale Prozesse anlegen, die auch laufen, wenn das Unterprogramm
selbst gar nicht ausgeführt wird. Die Summe der "Zusätzlichen Prozesse" aller
Unterprogramme wird zum Maximalwert der "Mindestzahl Prozesse" hinzugezählt um
die benötigte Anzahl von Prozessen zu ermitteln.

Ein Beispiel:

    Hauptprogramm
    Mindestzahl. Prozesse: 7
    Zusätzliche. Prozesse: 0
    
    Unterprogramm.A.
    Mindestzahl. Prozesse: 5
    Mindestzahl. Prozesse: 2
    
    Unterprogramm.B.
    Mindestzahl. Prozesse: 5
    Zusätzliche. Prozesse: 1
    
    Maximalwert= 7
    Summe= 3

In diesem Beispiel werden insgesamt 7+3=10 Stacks angelegt, so dass maximal 10
Prozesse gleichzeitig laufen können.

Wie viele Prozesse gleichzeitig laufen kann erst festgestellt werden, wenn das
Programm läuft. Im Onlinemodus erhält man eine entsprechende Fehlermeldung,
wenn die Anzahl an Prozessen zu klein ist. Im Downloadmodus bricht das
Programm ab und die rote Error Lampe blinkt 1-mal, wenn ein Prozess gestartet
werden soll, aber kein Stack mehr frei ist.

Da im Downloadmodus der Speicher für Stacks und Variablen nur 40kByte beträgt,
wird es im Downloadmodus mit 10 Stacks zu je 4kByte schon sehr eng. Die meisten
Programme benötigen jedoch nicht so viel Stackspeicher. In den meisten Fällen
reichen auch 2kByte Stackspeicher pro Prozess aus. Wie viel Stackspeicher benötigt
wird kann erst festgestellt werden, wenn das Programm läuft. Im Onlinemodus
erhält man eine entsprechende Fehlermeldung, wenn der Stack zu klein ist. Im
Downloadmodus bricht das Programm ab und die Errorlampe blinkt 2-mal wenn der
Stackspeicher zu klein ist.

Die Größe des Stackspeichers kann man im Eigenschaftsfenster für den
Online- und den Downloadmodus getrennt einstellen. Die Stackgröße im
Onlinemodus wird man in der Regel bei 64kByte belassen, außer man möchte
Programme mit tief rekursiven Funktionen oder großen lokalen Listen laufen
lassen. Die Größe des Stackspeichers im Downloadmodus kann man verkleinern,
wenn man viele Prozesse benötigt (das Minimum sind etwa 1280 Bytes) oder
für komplexe rekursive Funktionen auch vergrößern.

Mindestspeicher pro Prozess (download) = Stackgröße im Download Modus
Mindestspeicher pro Prozess (online) = Stackgröße im Online Modus

### Initialisierung von globalen Variablen und Listen

Nachdem alle globalen Variablen oder Listen mit gleichen Namen sich auf dieselbe
Variable oder Liste beziehen, kann man nur einen Initialisierungswert angeben.
Daher werden die Initialisierungswerte an alle gleichnamigen und gleichartigen
Elemente automatisch übertragen, wenn man Initialisierungseigenschaften für
globale Variablen oder Listen ändert.

### Gibt es bei der Anzahl der Timer eine Limitierung ?

Nein. Jeder Timer hängt sich in eine seiner Zeitauflösung entsprechende
Eventwarteschlange ein. Die größe dieser Warteschlangen ist nur durch den zur
Verfügung stehenden Speicher begrenzt. Es können also ziemlich viele
Timervariablen angelegt werden.

### Wie viel Speicher steht für Arrays zur Verfügung ?

Bisher nutzen die Arrays, Variablen und Stacks zusammen den direkt 16-bit
addressierbaren Speicherbereich. Davon läßt die Firmware etwa 40kByte übrig.
In der Regel werden 20kByte mit Stacks belegt, so dass nur 20 kByte übrig bleiben.
Es ist jedoch geplant die Arraydaten zumindest von gloabeln Arrays auch in den
erweiterten RAM legen zu können, so daß praktisch der ganze RAM (256 kByte) zur
Verfügung steht. (Bei RAM Programmen gehen noch 64kByte für das Programm ab, bei
Flash Programmen nicht).

Im Onlinemodus steht für globale Arrays der gesamte Speicher des PC zur Verfügung.


### Kann man .csv Dateien in mehreren Stücken laden ?

Nein, dass ist nicht möglich. Das Array wird mit den Daten aus der .csv Datei
initialisiert. Die .csv Datei wird sozusagen nicht vom Programm, sondern vom
Compiler gelesen und in Initialsierungen umgewandelt. Das ist notwendig, da die
.csv Initialisierung ja auch im Download Modus funktionieren muss.

Demnächst wird es aber möglich sein, die serielle Schnittstelle des Interface vom
Programm aus anzusprechen und so das Programm mit Daten zu versorgen, bzw Daten
vom Interface an den PC zu senden.
