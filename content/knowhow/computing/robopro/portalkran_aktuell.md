---
layout: "file"
hidden: true
title: "Portalkran"
date: "2017-02-03T00:00:00"
file: "portalkran_aktuell.rpp"
konstrukteure: 
- "Pascal Jan (bauFischertechnik)"
uploadBy:
- "Pascal Jan (bauFischertechnik)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/portalkran_aktuell.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/portalkran_aktuell.rpp -->
Mit diesem Programm kann man mein Portalkranmodell steuern.