---
layout: "wiki"
title: "Analyse des Robo upload Protokolls"
date: 2007-08-11T15:18:04
konstrukteure: 
- "Defiant"
uploadBy:
- "Defiant"
license: "unknown"
legacy_id:
- /wiki9093.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wiki9093.html?action=show&topic_id=30 -->
<!--
Wiki

Thema: Analyse des Robo upload Protokolls

Aktuelle Version

von: Defiant

am: 11.08.2007, 15:18:04 Uhr

Historie:
Version 10 von Defiant am 11.08.2007, 15:14:29 Uhr
Version 9 von Defiant am 11.08.2007, 15:02:26 Uhr
Version 8 von Defiant am 11.08.2007, 14:57:21 Uhr

...

Version 3 von Defiant am 11.08.2007, 14:37:43 Uhr
Version 2 von Defiant am 11.08.2007, 14:37:10 Uhr
Version 1 von Defiant am 11.08.2007, 14:31:49 Uhr
-->


Hochgeladen wurde hier die Datei edgetest.hex in den RAM unter dem Namen edgetest

### Datei edgetest.hex


    :020000025000AC
    :10010000FC00010882003200000000000000000036
    :1001100000000000000000000000000000010002DC
    :100120000003000400050006000700080000000AA4
    :100130000000CA010000003DCA080000003D809B8D
    :10014000CA01000000CA00000000CAB80B00003D50
    :10015000A7CA010000003DCA000000003D8087CA18
    :1001600000000000E9CA000000003A3D007ECA021B
    :100170000000003DCA080000003D8082008BCA03D9
    :100180000000003DCA080000003D80CA881300003E
    :060190003D960000000096

Die Datei edgetest.hex ist im Intel-Hex Format.
Details zu diesem Format gibt es [hier](http://www.schulz-koengen.de/biblio/intelhex.htm).

### Protokollanalyse:


    [0 ms] UsbSnoop compiled on Jan 18 2003 22:41:32 loading
    [0 ms] UsbSnoop - DriverEntry(f77d9c40) : Windows NT WDM version 1.32
    [18 ms] UsbSnoop - AddDevice(f77d9f50) : DriverObject 8925fa10, pdo 89156de8
    [18 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (0x00000018)
    [18 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (0x00000018)
    [20 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_RESOURCE_REQUIREMENTS)
    [20 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_RESOURCE_REQUIREMENTS)
    [20 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_FILTER_RESOURCE_REQUIREMENTS)
    [20 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_FILTER_RESOURCE_REQUIREMENTS)
    [20 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_START_DEVICE)
    [20 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_START_DEVICE)
    [20 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_INTERFACE)
    [20 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_INTERFACE)
    [21 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_CAPABILITIES)
    [21 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_CAPABILITIES)
    [21 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_PNP_DEVICE_STATE)
    [21 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_PNP_DEVICE_STATE)
    [21 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [21 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [22 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_SYSTEM_CONTROL
    [33224 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33224 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33224 ms] >>> URB 1 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 00000012
    TransferBuffer = b967e02c
    TransferBufferMDL = 00000000
    Index = 00000000
    DescriptorType = 00000001 (USB_DEVICE_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33227 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=8970a9a8, IRQL=2
    [33227 ms] <<< URB 1 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000012
    TransferBuffer = b967e02c
    TransferBufferMDL = 8940ab20
    00000000: 12 01 10 01 00 00 00 40 6a 14 01 00 01 00 01 02
    00000010: 03 01
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 00 01 00 00 12 00
    [33227 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33227 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33227 ms] >>> URB 2 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 000000ff
    TransferBuffer = f7a71770
    TransferBufferMDL = 00000000
    Index = 00000003
    DescriptorType = 00000003 (USB_STRING_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33232 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=8970a050, IRQL=2
    [33232 ms] <<< URB 2 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000012
    TransferBuffer = f7a71770
    TransferBufferMDL = 8940ab20
    00000000: 12 03 30 00 30 00 30 00 30 00 37 00 33 00 32 00
    00000010: 38 00
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 03 03 00 00 ff 00
    [33232 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33232 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33232 ms] >>> URB 3 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 000000ff
    TransferBuffer = b967b770
    TransferBufferMDL = 00000000
    Index = 00000004
    DescriptorType = 00000003 (USB_STRING_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33237 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=896028e8, IRQL=2
    [33237 ms] <<< URB 3 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000002a
    TransferBuffer = b967b770
    TransferBufferMDL = 8940ab20
    00000000: 2a 03 31 00 2e 00 37 00 31 00 2e 00 30 00 2e 00
    00000010: 30 00 34 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 04 00 00 00 47 00 01 00
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 04 03 00 00 ff 00
    [33237 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33237 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33237 ms] >>> URB 4 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 000000ff
    TransferBuffer = b9681770
    TransferBufferMDL = 00000000
    Index = 00000002
    DescriptorType = 00000003 (USB_STRING_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33243 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=89120c90, IRQL=2
    [33243 ms] <<< URB 4 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000005c
    TransferBuffer = b9681770
    TransferBufferMDL = 8940ab20
    00000000: 5c 03 66 00 69 00 73 00 63 00 68 00 65 00 72 00
    00000010: 74 00 65 00 63 00 68 00 6e 00 69 00 6b 00 20 00
    00000020: 52 00 6f 00 62 00 6f 00 20 00 49 00 6e 00 74 00
    00000030: 65 00 72 00 66 00 61 00 63 00 65 00 20 00 28 00
    00000040: 4b 00 6e 00 6f 00 62 00 6c 00 6f 00 63 00 68 00
    00000050: 20 00 47 00 6d 00 62 00 48 00 29 00
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 02 03 00 00 ff 00
    [33243 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33243 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33243 ms] >>> URB 5 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 000000ff
    TransferBuffer = baed9770
    TransferBufferMDL = 00000000
    Index = 00000001
    DescriptorType = 00000003 (USB_STRING_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33248 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=8919ea70, IRQL=2
    [33248 ms] <<< URB 5 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000001c
    TransferBuffer = baed9770
    TransferBufferMDL = 8940ab20
    00000000: 1c 03 4b 00 6e 00 6f 00 62 00 6c 00 6f 00 63 00
    00000010: 68 00 2d 00 47 00 6d 00 62 00 48 00
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 01 03 00 00 ff 00
    [33248 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33248 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33248 ms] >>> URB 6 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 000000ff
    TransferBuffer = baedd770
    TransferBufferMDL = 00000000
    Index = 00000005
    DescriptorType = 00000003 (USB_STRING_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [33253 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=8970a9a8, IRQL=2
    [33253 ms] <<< URB 6 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000001e
    TransferBuffer = baedd770
    TransferBufferMDL = 8940ab20
    00000000: 1e 03 52 00 6f 00 62 00 6f 00 20 00 49 00 6e 00
    00000010: 74 00 65 00 72 00 66 00 61 00 63 00 65 00
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 05 03 00 00 ff 00
    [33253 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [33253 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885f7348, IRQL=0
    [33253 ms] >>> URB 7 going down >>>
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x00000000 (unconfigure)
    [33256 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885f7348, Context=8970a050, IRQL=0
    [33256 ms] <<< URB 7 coming back <<<
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x00000000 (unconfigure)
    [36198 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36198 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36198 ms] >>> URB 8 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 00000009
    TransferBuffer = 89867ef8
    TransferBufferMDL = 00000000
    Index = 00000000
    DescriptorType = 00000002 (USB_CONFIGURATION_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [36201 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8970a050, IRQL=2
    [36201 ms] <<< URB 8 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000009
    TransferBuffer = 89867ef8
    TransferBufferMDL = 89614288
    00000000: 09 02 2e 00 01 01 05 c0 32
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 00 02 00 00 09 00
    [36201 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36201 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36201 ms] >>> URB 9 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 0000002e
    TransferBuffer = 89408fd0
    TransferBufferMDL = 00000000
    Index = 00000000
    DescriptorType = 00000002 (USB_CONFIGURATION_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [36205 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=890f4b78, IRQL=2
    [36205 ms] <<< URB 9 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000002e
    TransferBuffer = 89408fd0
    TransferBufferMDL = 89614288
    00000000: 09 02 2e 00 01 01 05 c0 32 09 04 00 00 04 ff ff
    00000010: ff 04 07 05 81 03 40 00 05 07 05 82 02 40 00 0a
    00000020: 07 05 01 03 40 00 05 07 05 02 02 40 00 0a
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 00 02 00 00 2e 00
    [36205 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36205 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36205 ms] >>> URB 10 going down >>>
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x89408fd0 (configure)
    ConfigurationDescriptor : bLength = 9
    ConfigurationDescriptor : bDescriptorType = 0x00000002
    ConfigurationDescriptor : wTotalLength = 0x0000002e
    ConfigurationDescriptor : bNumInterfaces = 0x00000001
    ConfigurationDescriptor : bConfigurationValue = 0x00000001
    ConfigurationDescriptor : iConfiguration = 0x00000005
    ConfigurationDescriptor : bmAttributes = 0x000000c0
    ConfigurationDescriptor : MaxPower = 0x00000032
    ConfigurationHandle = 0x00000000
    Interface[0]: Length = 96
    Interface[0]: InterfaceNumber = 0
    Interface[0]: AlternateSetting = 0
    [36320 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8970a9a8, IRQL=0
    [36320 ms] <<< URB 10 coming back <<<
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x89408fd0 (configure)
    ConfigurationDescriptor : bLength = 9
    ConfigurationDescriptor : bDescriptorType = 0x00000002
    ConfigurationDescriptor : wTotalLength = 0x0000002e
    ConfigurationDescriptor : bNumInterfaces = 0x00000001
    ConfigurationDescriptor : bConfigurationValue = 0x00000001
    ConfigurationDescriptor : iConfiguration = 0x00000005
    ConfigurationDescriptor : bmAttributes = 0x000000c0
    ConfigurationDescriptor : MaxPower = 0x00000032
    ConfigurationHandle = 0x886423c8
    Interface[0]: Length = 96
    Interface[0]: InterfaceNumber = 0
    Interface[0]: AlternateSetting = 0
    Interface[0]: Class = 0x000000ff
    Interface[0]: SubClass = 0x000000ff
    Interface[0]: Protocol = 0x000000ff
    Interface[0]: InterfaceHandle = 0x8942aa18
    Interface[0]: NumberOfPipes = 4
    Interface[0]: Pipes[0] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[0] : EndpointAddress = 0x00000081
    Interface[0]: Pipes[0] : Interval = 0x00000005
    Interface[0]: Pipes[0] : PipeType = 0x00000003 (UsbdPipeTypeInterrupt)
    Interface[0]: Pipes[0] : PipeHandle = 0x8942aa34
    Interface[0]: Pipes[0] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[0] : PipeFlags = 0x00000000
    Interface[0]: Pipes[1] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[1] : EndpointAddress = 0x00000082
    Interface[0]: Pipes[1] : Interval = 0x0000000a
    Interface[0]: Pipes[1] : PipeType = 0x00000002 (UsbdPipeTypeBulk)
    Interface[0]: Pipes[1] : PipeHandle = 0x8942aa54
    Interface[0]: Pipes[1] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[1] : PipeFlags = 0x00000000
    Interface[0]: Pipes[2] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[2] : EndpointAddress = 0x00000001
    Interface[0]: Pipes[2] : Interval = 0x00000005
    Interface[0]: Pipes[2] : PipeType = 0x00000003 (UsbdPipeTypeInterrupt)
    Interface[0]: Pipes[2] : PipeHandle = 0x8942aa74
    Interface[0]: Pipes[2] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[2] : PipeFlags = 0x00000000
    Interface[0]: Pipes[3] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[3] : EndpointAddress = 0x00000002
    Interface[0]: Pipes[3] : Interval = 0x0000000a
    Interface[0]: Pipes[3] : PipeType = 0x00000002 (UsbdPipeTypeBulk)
    Interface[0]: Pipes[3] : PipeHandle = 0x8942aa94
    Interface[0]: Pipes[3] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[3] : PipeFlags = 0x00000000
    [36320 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36320 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36320 ms] >>> URB 11 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa34 [endpoint 0x00000081]
    [36320 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=89120c90, IRQL=2
    [36320 ms] <<< URB 11 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [36320 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36320 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36320 ms] >>> URB 12 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa54 [endpoint 0x00000082]
    [36320 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8969d6d0, IRQL=2
    [36320 ms] <<< URB 12 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [36320 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36320 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36320 ms] >>> URB 13 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa74 [endpoint 0x00000001]
    [36320 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8933fa70, IRQL=2
    [36320 ms] <<< URB 13 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [36320 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36320 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36320 ms] >>> URB 14 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa94 [endpoint 0x00000002]
    [36320 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=891732f0, IRQL=2
    [36320 ms] <<< URB 14 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [36320 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36320 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36320 ms] >>> URB 15 going down >>>
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x00000000 (unconfigure)
    [36322 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=890f58f0, IRQL=0
    [36322 ms] <<< URB 15 coming back <<<
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x00000000 (unconfigure)
    [36323 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36323 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36323 ms] >>> URB 16 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 00000009
    TransferBuffer = 89867ef8
    TransferBufferMDL = 00000000
    Index = 00000000
    DescriptorType = 00000002 (USB_CONFIGURATION_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [36326 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=891288b0, IRQL=2
    [36326 ms] <<< URB 16 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000009
    TransferBuffer = 89867ef8
    TransferBufferMDL = 89614288
    00000000: 09 02 2e 00 01 01 05 c0 32
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 00 02 00 00 09 00
    [36326 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36326 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36327 ms] >>> URB 17 going down >>>
    -- URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
    TransferBufferLength = 0000002e
    TransferBuffer = 89408fd0
    TransferBufferMDL = 00000000
    Index = 00000000
    DescriptorType = 00000002 (USB_CONFIGURATION_DESCRIPTOR_TYPE)
    LanguageId = 00000000
    [36330 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=89815240, IRQL=2
    [36330 ms] <<< URB 17 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000002e
    TransferBuffer = 89408fd0
    TransferBufferMDL = 89614288
    00000000: 09 02 2e 00 01 01 05 c0 32 09 04 00 00 04 ff ff
    00000010: ff 04 07 05 81 03 40 00 05 07 05 82 02 40 00 0a
    00000020: 07 05 01 03 40 00 05 07 05 02 02 40 00 0a
    UrbLink = 00000000
    SetupPacket =
    00000000: 80 06 00 02 00 00 2e 00
    [36330 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36331 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36331 ms] >>> URB 18 going down >>>
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x89408fd0 (configure)
    ConfigurationDescriptor : bLength = 9
    ConfigurationDescriptor : bDescriptorType = 0x00000002
    ConfigurationDescriptor : wTotalLength = 0x0000002e
    ConfigurationDescriptor : bNumInterfaces = 0x00000001
    ConfigurationDescriptor : bConfigurationValue = 0x00000001
    ConfigurationDescriptor : iConfiguration = 0x00000005
    ConfigurationDescriptor : bmAttributes = 0x000000c0
    ConfigurationDescriptor : MaxPower = 0x00000032
    ConfigurationHandle = 0x00000000
    Interface[0]: Length = 96
    Interface[0]: InterfaceNumber = 0
    Interface[0]: AlternateSetting = 0
    [36445 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8976b8d8, IRQL=0
    [36445 ms] <<< URB 18 coming back <<<
    -- URB_FUNCTION_SELECT_CONFIGURATION:
    ConfigurationDescriptor = 0x89408fd0 (configure)
    ConfigurationDescriptor : bLength = 9
    ConfigurationDescriptor : bDescriptorType = 0x00000002
    ConfigurationDescriptor : wTotalLength = 0x0000002e
    ConfigurationDescriptor : bNumInterfaces = 0x00000001
    ConfigurationDescriptor : bConfigurationValue = 0x00000001
    ConfigurationDescriptor : iConfiguration = 0x00000005
    ConfigurationDescriptor : bmAttributes = 0x000000c0
    ConfigurationDescriptor : MaxPower = 0x00000032
    ConfigurationHandle = 0x886423c8
    Interface[0]: Length = 96
    Interface[0]: InterfaceNumber = 0
    Interface[0]: AlternateSetting = 0
    Interface[0]: Class = 0x000000ff
    Interface[0]: SubClass = 0x000000ff
    Interface[0]: Protocol = 0x000000ff
    Interface[0]: InterfaceHandle = 0x8942aa18
    Interface[0]: NumberOfPipes = 4
    Interface[0]: Pipes[0] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[0] : EndpointAddress = 0x00000081
    Interface[0]: Pipes[0] : Interval = 0x00000005
    Interface[0]: Pipes[0] : PipeType = 0x00000003 (UsbdPipeTypeInterrupt)
    Interface[0]: Pipes[0] : PipeHandle = 0x8942aa34
    Interface[0]: Pipes[0] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[0] : PipeFlags = 0x00000000
    Interface[0]: Pipes[1] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[1] : EndpointAddress = 0x00000082
    Interface[0]: Pipes[1] : Interval = 0x0000000a
    Interface[0]: Pipes[1] : PipeType = 0x00000002 (UsbdPipeTypeBulk)
    Interface[0]: Pipes[1] : PipeHandle = 0x8942aa54
    Interface[0]: Pipes[1] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[1] : PipeFlags = 0x00000000
    Interface[0]: Pipes[2] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[2] : EndpointAddress = 0x00000001
    Interface[0]: Pipes[2] : Interval = 0x00000005
    Interface[0]: Pipes[2] : PipeType = 0x00000003 (UsbdPipeTypeInterrupt)
    Interface[0]: Pipes[2] : PipeHandle = 0x8942aa74
    Interface[0]: Pipes[2] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[2] : PipeFlags = 0x00000000
    Interface[0]: Pipes[3] : MaximumPacketSize = 0x00000040
    Interface[0]: Pipes[3] : EndpointAddress = 0x00000002
    Interface[0]: Pipes[3] : Interval = 0x0000000a
    Interface[0]: Pipes[3] : PipeType = 0x00000002 (UsbdPipeTypeBulk)
    Interface[0]: Pipes[3] : PipeHandle = 0x8942aa94
    Interface[0]: Pipes[3] : MaxTransferSize = 0x00001000
    Interface[0]: Pipes[3] : PipeFlags = 0x00000000
    [36445 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36445 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36445 ms] >>> URB 19 going down >>>
    -- URB_FUNCTION_RESET_PIPE:
    PipeHandle = 8942aa74 [endpoint 0x00000001]
    [36448 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=89815240, IRQL=0
    [36448 ms] <<< URB 19 coming back <<<
    -- URB_FUNCTION_RESET_PIPE:
    [36448 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36448 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36448 ms] >>> URB 20 going down >>>
    -- URB_FUNCTION_RESET_PIPE:
    PipeHandle = 8942aa34 [endpoint 0x00000081]

Ok, an dieser Stelle wurden alle endlich alle Informationen zum Interface abgerufen und wir können beginnen


    [36451 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8933fa70, IRQL=0
    [36451 ms] <<< URB 20 coming back <<<
    -- URB_FUNCTION_RESET_PIPE:
    [36451 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36451 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36451 ms] >>> URB 21 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000002
    Index = 00000000
    [36455 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8914a338, IRQL=2
    [36455 ms] <<< URB 21 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fd 28 73 00 00 01 00 00 00 28 73 00 00 02
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 02 00 00 00 0e 00

Zum warm werden:
Gesendet wird hier also Ein Kontroll-Paket, mit den Werten 0xc0 0xf0 0x02.
Die Antwort ist 0xfd 0x28 0x73 0x0 0x0 0x1
Ein Blick in die Datei Robo-If-Ser-Codes_FW_035.pdf der ftlib
verrät uns, daß 0xf0 0x2 die Abfrage der Seriennummer ist und die Antwort 0xfd + Seriennummer ist.


    [36455 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36455 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36455 ms] >>> URB 22 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000002
    Index = 00000000
    [36459 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=890f58f0, IRQL=2
    [36459 ms] <<< URB 22 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fd 28 73 00 00 01 00 00 00 28 73 00 00 02
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 02 00 00 00 0e 00
    [36459 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36459 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36459 ms] >>> URB 23 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000005
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000001
    Index = 00000000
    [36463 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=89173298, IRQL=2
    [36463 ms] <<< URB 23 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000005
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fe 04 00 47 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 01 00 00 00 05 00

Hier wird 0xf0 0x01 gesendet. Laut Dokumentation Abfrage der Firmware.


    [36463 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36463 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36463 ms] >>> URB 24 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000020
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000011
    Index = 00000000
    [36467 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=891288b0, IRQL=2
    [36467 ms] <<< URB 24 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000020
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: 00 01 08 ff ff 09 00 01 0a ff ff 0b 00 01 05 ff
    00000010: ff 07 00 40 00 ff ff 00 00 06 00 ff 11 00 ee 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 11 00 00 00 20 00

Was 0xf0 0x11 macht, habe ich ehrlich gesagt keine Ahnung - Aber ohne geht es auch ;)


    [36473 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36473 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36473 ms] >>> URB 25 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [36477 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8933fa70, IRQL=2
    [36477 ms] <<< URB 25 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00

0xf0 0x3 fragt laut Robo-If-Ser-Codes_FW_035.pdf den Zustand des Interfaces ab.
Antwort 0xfx 0x0 == Online Modus


    [36477 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36477 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36477 ms] >>> URB 26 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000000
    Index = 00000000
    [36735 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8914a338, IRQL=2
    [36735 ms] <<< URB 26 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: 01 69 6e 74 65 72 70 72 65 74 65 72 56 30 2e 39
    00000010: 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 00 00 00 00 51 00

Auch hier hilft glücklicherweise das pdf weiter: 0xfa 0x0 fragt nach dem Namen des Programs im Flash1.


    [36735 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36735 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36735 ms] >>> URB 27 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [36739 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=890f58f0, IRQL=2
    [36739 ms] <<< URB 27 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [36739 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36739 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36739 ms] >>> URB 28 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000001
    Index = 00000000
    [36748 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=89173298, IRQL=2
    [36748 ms] <<< URB 28 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: 01 46 3a 5c 52 6f 62 6f 49 6e 74 5f 42 61 73 69
    00000010: 63 5f 43 6f 6d 70 69 6c 65 72 5c 54 65 73 74 70
    00000020: 72 6f 67 72 61 6d 6d 65 5c 66 6f 72 2d 6e 65 78
    00000030: 74 2e 68 65 78 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 01 00 00 00 51 00
    [36748 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36748 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36748 ms] >>> URB 29 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [36752 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=891288b0, IRQL=2
    [36752 ms] <<< URB 29 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [36752 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [36752 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=8912dca8, IRQL=0
    [36752 ms] >>> URB 30 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000002
    Index = 00000000
    [36757 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=8912dca8, Context=8933fa70, IRQL=2
    [36757 ms] <<< URB 30 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 89614288
    00000000: f3 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 02 00 00 00 51 00
    [61958 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [61958 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [61958 ms] >>> URB 31 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [61961 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8969d6d0, IRQL=2
    [61961 ms] <<< URB 31 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00

Bis hierhin wurden abwechselnd der Status des Interfaces abgefragt und der Name im flash2 und im RAM.

Nun zum interessanteren Teil:

    [61961 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [61961 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [61961 ms] >>> URB 32 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000020
    Index = 00000000
    [61965 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8913e370, IRQL=2
    [61965 ms] <<< URB 32 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 20 00 00 00 01 00

Ah ha. 0xf0 0x20 ist undokumentiert. Alleine deswegen also interessant.
An dieser Stelle können wir noch nicht sagen, was dieser Befehl macht.
Wir wissen nur: Die Antwort ist 1.


    [61965 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [61965 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [61965 ms] >>> URB 33 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000002 (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000050
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 65 64 67 65 74 65 73 74 00 00 00 00 00 00 00 00
    00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000010
    Value = 00000102
    Index = 00000002

0x65 0x64 0x67 0x65 0x74 0x65 0x73 0x74 ist eine uns bekannte Zeichenkette.
hier handelt es sich um den Namen des Programms: edgetest in Hexadezimaler Form.
Der Befehl 0x10 0x102 leitet also den Download ein.
Das erste Argument ist hier übrigens das Ziel des Transfers.
0x100 ist flash1, 0x200 flash1 Autostart, 0x101 flash2 und 0x102 der RAM.


    [62636 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=896fc078, IRQL=2
    [62636 ms] <<< URB 33 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000a (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000050
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    SetupPacket =
    00000000: 40 10 02 01 02 00 50 00
    [62636 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62636 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62636 ms] >>> URB 34 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000020
    Value = 00000000
    Index = 00000000
    [62640 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8913e370, IRQL=2
    [62640 ms] <<< URB 34 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 02
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 20 00 00 00 00 01 00

Schon wieder 0x20.
Dieses mal mit der Antwort 0x2.
Die Antwort hier ist anscheinend die Zieladresse.
Beim Flash1 ist die Antwort 0x0, Flash2 0x1 und Ram 0x2.


    [62640 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62640 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62640 ms] >>> URB 35 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000002 (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000080
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fc 00 01 08 82 00 32 00 00 00 00 00 00 00 00 00
    00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 02
    00000020: 00 03 00 04 00 05 00 06 00 07 00 08 00 00 00 0a
    00000030: 00 00 ca 01 00 00 00 3d ca 08 00 00 00 3d 80 9b
    00000040: ca 01 00 00 00 ca 00 00 00 00 ca b8 0b 00 00 3d
    00000050: a7 ca 01 00 00 00 3d ca 00 00 00 00 3d 80 87 ca
    00000060: 00 00 00 00 e9 ca 00 00 00 00 3a 3d 00 7e ca 02
    00000070: 00 00 00 3d ca 08 00 00 00 3d 80 82 00 8b ca 03
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000011
    Value = 00000001
    Index = 00003094

0xfc 0x00 0x01 0x08 0x82 0x00 0x32... ist definitiv das Programm (siehe oben). Und zwar die ersten 128 Bit.
0x11 0x1 0x3094 sendet also das Programm.


    [62646 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8976b8d8, IRQL=2
    [62646 ms] <<< URB 35 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000a (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000080
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    SetupPacket =
    00000000: 40 11 01 00 94 30 80 00
    [62646 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62646 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62646 ms] >>> URB 36 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000020
    Value = 00000000
    Index = 00000000
    [62650 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=89173298, IRQL=2
    [62650 ms] <<< URB 36 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 20 00 00 00 00 01 00

Mal wieder 0x20 mit Antwort 0x1.


    [62650 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62650 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62650 ms] >>> URB 37 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000002 (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000080
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 00 00 00 3d ca 08 00 00 00 3d 80 ca 88 13 00 00
    00000010: 3d 96 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000011
    Value = 00000002
    Index = 0000a875

Mit 0x11 0x2 0xa875 werden also die letzten bits des Programms übertragen.
Da wir weniger als 128 bit zum übertragen haben, wird der Rest mit 0en aufgefüllt.
Es stellt sich heraus: Der zweite Wert (hier: 0x2) ist die Sequenznummer von 0x1, 0x2, 0x3... bis 0xffff und die letzte ist 0x0!
Der letzte Wert (hier: 0xa875) ist eine [CRC-CCITT, Anfangswert 0xFFFF](http://www.lammertbies.nl/comm/info/crc-calculation.html)


    [62659 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8914a338, IRQL=2
    [62659 ms] <<< URB 37 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000a (USBD_TRANSFER_DIRECTION_OUT, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000080
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    SetupPacket =
    00000000: 40 11 02 00 75 a8 80 00
    [62659 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62659 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62659 ms] >>> URB 38 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000020
    Value = 00000000
    Index = 00000000
    [62663 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=896fc078, IRQL=2
    [62663 ms] <<< URB 38 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 20 00 00 00 00 01 00

Und wieder 0x20 mit Antwort 0x1. 0x20 fragt wohl ob der letzte Upload erfolgreich war und eine andere Antwort als 1 ist ein Fehler. (Unbestätigt)

    
    [62663 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62663 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62663 ms] >>> URB 39 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000002
    Index = 00000000
    [62667 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8914a338, IRQL=2
    [62667 ms] <<< URB 39 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 0000000e
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fd 28 73 00 00 01 00 00 00 28 73 00 00 02
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 02 00 00 00 0e 00

Gesendet wird hier wieder 0xf0 0x2. Was wir ganz weit oben schon hatten.
Gefragt wird hier wieder nach der Seriennummer.
Der Upload Prozess ist also schon fertig.


    [62667 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62667 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62667 ms] >>> URB 40 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000005
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000001
    Index = 00000000
    [62671 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8913e370, IRQL=2
    [62671 ms] <<< URB 40 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000005
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fe 04 00 47 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 01 00 00 00 05 00
    [62672 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62672 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62672 ms] >>> URB 41 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000020
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000011
    Index = 00000000
    [62675 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=891288b0, IRQL=2
    [62675 ms] <<< URB 41 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000020
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 00 01 08 ff ff 09 00 01 0a ff ff 0b 00 01 05 ff
    00000010: ff 07 00 40 00 ff ff 00 00 06 00 ff 11 00 ee 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 11 00 00 00 20 00
    [62678 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62678 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62678 ms] >>> URB 42 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [62681 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=89173298, IRQL=2
    [62681 ms] <<< URB 42 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [62681 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62681 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62681 ms] >>> URB 43 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000000
    Index = 00000000
    [62939 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=89190d30, IRQL=2
    [62939 ms] <<< URB 43 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01 69 6e 74 65 72 70 72 65 74 65 72 56 30 2e 39
    00000010: 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 00 00 00 00 51 00
    [62940 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62940 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62940 ms] >>> URB 44 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [62943 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8914a338, IRQL=2
    [62943 ms] <<< URB 44 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [62943 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62943 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62943 ms] >>> URB 45 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000001
    Index = 00000000
    [62952 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=8913e370, IRQL=2
    [62952 ms] <<< URB 45 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01 46 3a 5c 52 6f 62 6f 49 6e 74 5f 42 61 73 69
    00000010: 63 5f 43 6f 6d 70 69 6c 65 72 5c 54 65 73 74 70
    00000020: 72 6f 67 72 61 6d 6d 65 5c 66 6f 72 2d 6e 65 78
    00000030: 74 2e 68 65 78 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 01 00 00 00 51 00
    [62952 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62952 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62952 ms] >>> URB 46 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [62956 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=891288b0, IRQL=2
    [62956 ms] <<< URB 46 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [62956 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [62956 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=891b39f0, IRQL=0
    [62956 ms] >>> URB 47 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000fa
    Value = 00000002
    Index = 00000000
    [62963 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=891b39f0, Context=89173298, IRQL=2
    [62963 ms] <<< URB 47 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000051
    TransferBuffer = 00000000
    TransferBufferMDL = 8940ab20
    00000000: 01 65 64 67 65 74 65 73 74 00 00 00 00 00 00 00
    00000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    00000050: 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 fa 02 00 00 00 51 00
    [81882 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [81882 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885e4460, IRQL=0
    [81882 ms] >>> URB 48 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8986b118
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 000000f0
    Value = 00000003
    Index = 00000000
    [81886 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885e4460, Context=89173298, IRQL=2
    [81886 ms] <<< URB 48 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000003
    TransferBuffer = 00000000
    TransferBufferMDL = 8986b118
    00000000: fc 00 00
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 f0 03 00 00 00 03 00
    [81886 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [81886 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=885e4460, IRQL=0
    [81886 ms] >>> URB 49 going down >>>
    -- URB_FUNCTION_VENDOR_DEVICE:
    TransferFlags = 00000003 (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8986b118
    UrbLink = 00000000
    RequestTypeReservedBits = 00000000
    Request = 00000012
    Value = 00000002
    Index = 00000000
    [81892 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=885e4460, Context=8976b8d8, IRQL=2
    [81892 ms] <<< URB 49 coming back <<<
    -- URB_FUNCTION_CONTROL_TRANSFER:
    PipeHandle = 89356bc0
    TransferFlags = 0000000b (USBD_TRANSFER_DIRECTION_IN, USBD_SHORT_TRANSFER_OK)
    TransferBufferLength = 00000001
    TransferBuffer = 00000000
    TransferBufferMDL = 8986b118
    00000000: 01
    UrbLink = 00000000
    SetupPacket =
    00000000: c0 12 02 00 00 00 01 00
    [232047 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [232047 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [232047 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [232047 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_QUERY_DEVICE_RELATIONS)
    [232047 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [232047 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=886fb240, IRQL=0
    [232047 ms] >>> URB 50 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa34 [endpoint 0x00000081]
    [232562 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=886fb240, Context=8976b8d8, IRQL=2
    [232562 ms] <<< URB 50 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [232562 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [232562 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=886fb240, IRQL=0
    [232562 ms] >>> URB 51 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa54 [endpoint 0x00000082]
    [233078 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=886fb240, Context=896213d0, IRQL=2
    [233078 ms] <<< URB 51 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [233078 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [233078 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=886fb240, IRQL=0
    [233078 ms] >>> URB 52 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa74 [endpoint 0x00000001]
    [233593 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=886fb240, Context=8933fa70, IRQL=2
    [233593 ms] <<< URB 52 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [233593 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_INTERNAL_DEVICE_CONTROL
    [233593 ms] UsbSnoop - MyDispatchInternalIOCTL(f77d8e80) : fdo=89156de8, Irp=886fb240, IRQL=0
    [233593 ms] >>> URB 53 going down >>>
    -- URB_FUNCTION_ABORT_PIPE:
    PipeHandle = 8942aa94 [endpoint 0x00000002]
    [234109 ms] UsbSnoop - MyInternalIOCTLCompletion(f77d8db0) : fido=88642948, Irp=886fb240, Context=890fa0e0, IRQL=2
    [234109 ms] <<< URB 53 coming back <<<
    -- URB_FUNCTION_ABORT_PIPE:
    [234109 ms] UsbSnoop - DispatchAny(f77d7610) : IRP_MJ_PNP (IRP_MN_SURPRISE_REMOVAL)
    [234109 ms] UsbSnoop - MyDispatchPNP(f77d9ee0) : IRP_MJ_PNP (IRP_MN_SURPRISE_REMOVAL)


