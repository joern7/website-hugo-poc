---
layout: "file"
hidden: true
title: "Training Roboter II"
date: "2017-02-03T00:00:00"
file: "trainingrobii.4.rpp"
konstrukteure: 
- "Marspau"
uploadBy:
- "Marspau"
license: "unknown"
legacy_id:
- /data/downloads/robopro/trainingrobii.4.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/trainingrobii.4.rpp -->
RoboPro program to drive the training roboter II