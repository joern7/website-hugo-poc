---
layout: "file"
hidden: true
title: "Kettenfahrzeug Version2"
date: "2017-02-03T00:00:00"
file: "ftjan.rpp"
konstrukteure: 
- "anonymus"
uploadBy:
- "anonymus"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ftjan.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ftjan.rpp -->
Programmverbesserung zum vorherigen Programm von ft-Jan.
Erläuterungen zum Programm stehen im Fenster Beschreibung.