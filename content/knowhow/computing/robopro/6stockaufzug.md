---
layout: "file"
hidden: true
title: "6Stockaufzug"
date: "2017-02-03T00:00:00"
file: "6stockaufzug.rpp"
konstrukteure: 
- "michael K."
uploadBy:
- "michael K."
license: "unknown"
legacy_id:
- /data/downloads/robopro/6stockaufzug.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/6stockaufzug.rpp -->
Das ist ein ganz einfaches Steuerungsprogramm für einen 6Stockauzug. Es ist praktisch beliebig erweiterbar.