---
layout: "file"
hidden: true
title: "Liste durchsuchen"
date: "2012-08-10T00:00:00"
file: "listedurchsuchen.rpp"
konstrukteure: 
- "Jan Werner (werner)"
uploadBy:
- "Jan Werner (werner)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/listedurchsuchen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/listedurchsuchen.rpp -->
Dieses kleine Unterprogramm durchsucht eine Liste nach einem bestimmten Wert. Je nachdem, ob die Suche erfolgreich war oder nicht, verzweigt das Unterprogramm zu dem einen oder anderen Ausgang. War die Suche erfolgreich, wird der erste Index, also die Zeile in der das Programm den Wert zuerst gefunden hat, ausgegeben.

Das ist zum Beispiel beim Programmieren von Hochregallagern sehr nützlich: Man kann daduch schnell ermitteln in welchem Fach sich angeforderte Ware befindet oder wo noch ein leeres Fach zum Einlagern ist.

Außerdem enthält das Programm eine erweiterte Zählschleife.