---
layout: "file"
hidden: true
title: "Neuhold 3-Achsen Beschleunigungs-Senor-Module  + I2C-Bus LED Display "
date: "2017-02-03T00:00:00"
file: "gkrachtmetingmaxminledi2c.rpp"
konstrukteure: 
- "Peter  (Poederoyen NL)"
uploadBy:
- "Peter  (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/gkrachtmetingmaxminledi2c.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/gkrachtmetingmaxminledi2c.rpp -->
Neuhold 3-Achsen Beschleunigungs-Senor-Module (=300mV/g)  + I2C-Bus LED Display Conrad # 198344