---
layout: "file"
hidden: true
title: "Sortieranlage"
date: "2017-02-03T00:00:00"
file: "sortiermaschine.rpp"
konstrukteure: 
- "fitec"
uploadBy:
- "fitec"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sortiermaschine.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sortiermaschine.rpp -->
Das ist das Programm meiner Sortieranlage.