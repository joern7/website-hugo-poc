---
layout: "file"
hidden: true
title: "PID Regler mit RoboPro"
date: "2017-05-23T00:00:00"
file: "pidreglerv008ftc.rpp"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pidreglerv008ftc.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pidreglerv008ftc.rpp -->
Beispiel zeigt einen PID Regler der ausgehen von einem Drucksensor 2 Kompressoren ansteuert.
Getestet auf Robo TXT.

No guarantee, all on your own risk.