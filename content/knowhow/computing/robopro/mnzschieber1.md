---
layout: "file"
hidden: true
title: "Fehlerhafter Münzschieber"
date: "2012-06-30T00:00:00"
file: "mnzschieber1.rpp"
konstrukteure: 
- "Bastel-Freak2"
uploadBy:
- "Bastel-Freak2"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/computing/mnzschieber1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/computing/mnzschieber1.rpp -->
Bei meinen Münzschieber funkioniert einiges nicht.
So sollte er funktionieren: Geld einwerfen. Der Schieber fährt 3mal hin und her und bleibt danach stehen. Fällt ein Geldstück hinunter, blinkt ein Licht. Für ein erneutes Spiel ein weiteres Geldstück einwerfen.