---
layout: "file"
hidden: true
title: "Pixy I2C Objekterkennung"
date: "2017-03-26T00:00:00"
file: "pixypantilt.rpp"
konstrukteure: 
- "Dirk Wölffel"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pixypantilt.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pixypantilt.rpp -->
Mit diesem Programm könnt Ihr über die Pixy Kamera I2C Hex Adresse 0x14
ein vorher erlerntes Objekt auslesen. 
Es wird in RoboPro M1 links-rechts und M2 auf-ab angesteuert.
Das Modell dazu findet ihr unter: https://www.youtube.com/watch?v=9Rk8K6oQ-xQ  