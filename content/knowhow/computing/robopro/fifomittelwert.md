---
layout: "file"
hidden: true
title: "Gleitender Mittelwert 10 Werte"
date: "2017-02-03T00:00:00"
file: "fifomittelwert.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/robopro/fifomittelwert.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/fifomittelwert.rpp -->
10 berechnete Temperaturwerte werden nach dem FiFo-Prinzip gespeichert ,gemittelt und ausgegeben.Die gemessenen Widerstandswerte(Counts) des Temperatursensors werden über eine Liste in Temperaturen umgewandelt.