---
layout: "file"
hidden: true
title: "RoboPro-I²C-Teiber für DC-Motoren, LEDs etc, DRV8830"
date: "2017-03-25T00:00:00"
file: "i2cdrv8830minimototreiber1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cdrv8830minimototreiber1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cdrv8830minimototreiber1.0.rpp -->
- Eine Last ansteuerbar
- 2,75V bis 6,8V Spannungsversorgung
- 1A max Strom
- Elektrische Schutzfunktionen (Strom-, Spannungs- und Temperaturlimit)
- Standby, Vorlauf, Rücklauf und Bremse