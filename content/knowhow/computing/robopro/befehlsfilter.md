---
layout: "file"
hidden: true
title: "Befehlsfilter"
date: "2007-02-04T00:00:00"
file: "befehlsfilter.rpp"
konstrukteure: 
- "michael K."
uploadBy:
- "michael K."
license: "unknown"
legacy_id:
- /data/downloads/robopro/befehlsfilter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/befehlsfilter.rpp -->
Wertet Fahrbefehle für einen 2-Rad Roboter aus und besitzt noch ein paar mehr funktionen als das Beispielprogramm von Robopro