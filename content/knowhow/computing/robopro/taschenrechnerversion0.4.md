---
layout: "file"
hidden: true
title: "Taschenrechner (version 0.4)"
date: "2017-02-03T00:00:00"
file: "taschenrechnerversion0.4.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/taschenrechnerversion0.4.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/taschenrechnerversion0.4.rpp -->
Jetzt kann der Taschenrechner Patenzen ausrechnen.