---
layout: "file"
hidden: true
title: "Nicht fuktionierendes tic tac toe Unterprogramm"
date: "2012-10-25T00:00:00"
file: "tictactoeunterprogramm.rpp"
konstrukteure: 
- "Zahnrädchen 001"
uploadBy:
- "Zahnrädchen 001"
license: "unknown"
legacy_id:
- /data/downloads/robopro/tictactoeunterprogramm.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/tictactoeunterprogramm.rpp -->
Das ist das Unterprogramm, das eine 2er-Reihe finden und vervollständigen soll. Jedes Feld hat eine eigene Variable. Im Bedienfeld habe ich eingezeichnet, welches Feld welche Nummer hat.