---
layout: "file"
hidden: true
title: "Streben Sortierer"
date: "2017-02-03T00:00:00"
file: "strebensortierer.rpp"
konstrukteure: 
- "Fischli"
uploadBy:
- "Fischli"
license: "unknown"
legacy_id:
- /data/downloads/robopro/strebensortierer.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/strebensortierer.rpp -->
Das Modell findet ihr unter http://www.ftcommunity.de/categories.php?cat_id=2018 