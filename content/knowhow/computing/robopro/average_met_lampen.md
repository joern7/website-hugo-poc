---
layout: "file"
hidden: true
title: "Using the distance sensor with 'anti-Geräusche'"
date: "2017-02-03T00:00:00"
file: "average_met_lampen.rpp"
konstrukteure: 
- "Richard R. Budding"
uploadBy:
- "Richard R. Budding"
license: "unknown"
legacy_id:
- /data/downloads/robopro/average_met_lampen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/average_met_lampen.rpp -->
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=989

How to use the ultra sonic distance sensors.

If connected to the A1 or 2 eingang the signal has too much 'Geräusche'. In order to stablize and utilze it better you'll have to use some average algorithm.

Herby the solution...

have fun,

richard