---
layout: "file"
hidden: true
title: "Raupensteuerung für RoboIF"
date: "2007-03-19T00:00:00"
file: "irmanualraupensteuerung.1.rpp"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
uploadBy:
- "Martin Giger (Ma-gi-er)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/irmanualraupensteuerung.1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/irmanualraupensteuerung.1.rpp -->
Dieses Programm wurde ursprünglich für einen Rasenmäher gemacht, ist aber eigentlich wie mit dem IR-Empfänger. im Bedienfeld findet man, wenn man keine Fehrnsteuerung hat, eine Steuerungsoberfläche.