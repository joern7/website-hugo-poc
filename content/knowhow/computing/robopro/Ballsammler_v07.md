---
layout: "file"
hidden: true
title: "Ballsammler"
date: 2020-03-10T15:40:33+01:00
file: "Ballsammler_v07.rpp"
konstrukteure: 
- "Techum"
uploadBy:
- "Website-Team"
license: "unknown"
---

RoboPro Programm für einen autonomen Ballsammler - im Kern leuchtet dieser weiße Tischtennisbälle mit einem Linienlaser an und gewichtet die Bälle nach Entfernung und Lage, um eine Richtungskorrektur zu berechnen und die Bälle einzusammeln. Dabei wird der TXT mit Kamera eingesetzt und mehrere parallele Liniensensoren ausgewertet. Nahe zentrale Bälle haben dabei mehr Gewicht als ferne, außenliegende. Liegen also zwei Bälle vergleichbarere Entfernung rechts der Mitte und einer links, dreht der Ballsammler rechts ab. Um die Entfernung zu bestimmen, wird die Parallaxe genutzt. Ich habe versucht, das Konzept hier vorzustellen: <https://youtu.be/zeO-ms4kd9c>
Leider zeigt sich, dass es das das Neon- bzw. Halogenlicht in einer echten Halle dem Ballsammler recht schwer macht, die Bälle zu identizieren - Tipp: der Laser muss die Basis des Balls anleuchten, da dort ein minimaler Binnenschatten entsteht. In zu heller Umgebung geht nicht mehr viel. Außerdem muss man wegen der automatischen Kontrastkorrektur der Kamera ggf. mehrere Startversuche machen, bis etwas erkannt wird.
 
Das Programm enthält bereits den Versuch, auch einer Wand zu folgen - schließlich soll der Roboter den Spielern nicht zwischen den Füßen herumfahren - dieses Feature funktioniert aber nicht nicht, sollte also ausgeschaltet bleiben. Zwar funktioniert die Ermittlung des Wandabstands per rotierendem Ultraschallsensor ganz gut - die weichen Kunststoffabtrennungen die im Tischtennis Verwendung finden, liefern leider keine verlässlichen Distanzwerte.
Auch zeigt sich, dass die Nutzung des Kompasses nicht unbedingt hilft - die im Hallenboden verlegten Kabel bzw. Armierung irritiert leicht. 
 
Außerdem gibt es im Programm verschiedende Schalter, die der Fehlerfindung dienen bis hin zur manuellen Fahrsteuerung über Wlan.
 
Vielleicht kann das Program oder Elemente daraus dem einen oder anderen als Orientierung dienen.  
