---
layout: "file"
hidden: true
title: "I2C Treiber für das Kompassmodul CMPS03"
date: "2017-02-03T00:00:00"
file: "compasscmps03.rpp"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/compasscmps03.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/compasscmps03.rpp -->
Dies ist der I2C Treiber für das CMPS03 Kompassmodul.
Da der CMPS03 ein Vorläufer des CMPS09 ist, ist dieser Treiber eigentlich nur eine "abgespeckte" Version des CMPS09 Treibers, der mit RoboPro mitgeliefert wird.