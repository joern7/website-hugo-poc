---
layout: "file"
hidden: true
title: "Geldwechsel-Automat"
date: "2017-02-03T00:00:00"
file: "geldwechselautomat.zip"
konstrukteure: 
- "Laserman"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/robopro/geldwechselautomat.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/geldwechselautomat.zip -->
Das Programm für den Geldwechsel-Automat. Im Bedienfeld des Hauptprogramms kann man den utomaten bedienen. Es ist auch eine kleine Beschreibung im Programm.