---
layout: "file"
hidden: true
title: "Würfel im Tx-Display"
date: "2011-05-16T00:00:00"
file: "wrfel0x.rpp"
konstrukteure: 
- "Fuzzi 52"
uploadBy:
- "Fuzzi 52"
license: "unknown"
legacy_id:
- /data/downloads/robopro/wrfel0x.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/wrfel0x.rpp -->
Die maximale Zahl (x) kann verändern 
der Würfel zählt von 0 bis x und wenn x überschritten
ist wird die Zahl auf 0 zurückgesetzt