---
layout: "file"
hidden: true
title: "Tag-Nacht-Temperatur-Roboter"
date: "2017-02-03T00:00:00"
file: "tagnachttemperaturroboter.rpp"
konstrukteure: 
- "Fuzzi 52"
uploadBy:
- "Fuzzi 52"
license: "unknown"
legacy_id:
- /data/downloads/robopro/tagnachttemperaturroboter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/tagnachttemperaturroboter.rpp -->
Das Programm von meinem Tag-Nacht-Temperatur-Roboter