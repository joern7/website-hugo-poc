---
layout: "wiki"
hidden: true
title: "RoboPro: Kommunikation via serieller Schnittstelle mit dem PC"
date: 2007-09-21T17:11:46
konstrukteure: 
- "thkais"
uploadBy:
- "thkais"
license: "unknown"
legacy_id:
- /wikib061.html
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/wikib061.html?action=show&topic_id=31 -->
<!--
   Wiki

   Thema: RoboPro: Kommunikation via serieller Schnittstelle mit dem PC

   Aktuelle Version

   von: thkais

   am: 21.09.2007, 17:11:46 Uhr

   Historie:
   Version 6 von thkais am 21.09.2007, 17:05:21 Uhr
   Version 5 von thkais am 21.09.2007, 16:57:23 Uhr
   Version 4 von thkais am 21.09.2007, 14:45:16 Uhr
   Version 3 von thkais am 21.09.2007, 14:44:29 Uhr
   Version 2 von thkais am 21.09.2007, 14:43:16 Uhr
   Version 1 von thkais am 21.09.2007, 07:36:08 Uhr
Das hier wäre dann Version 7, die auf .md umgestellt und etwas aufgehübscht
wurde.
-->

_**ACHTUNG**: Es fehlen einige Bilder, die zugehörigen Links sind HTTP404!!!
Die Bilder sind im www verschollen, nicht auf dem ftc-Server zu finden._


Mit der am RoboInterface vorhandenen seriellen Schnittstelle lassen sich
während der Laufzeit eines RoboPro-Programmes Daten zu einem PC oder anderen
geeigneten Gerät übertragen. Momentan ist es allerdings nicht möglich,
beliebige Daten zu verwenden, man muss das von RoboPro vorgegebene
7-Byte-Protokoll verwenden.
Dieses Protokoll setzt sich wie folgt zusammen:

1. Byte.... 0x90 .... Startbyte, muss immer diesen Wert haben
2. Byte.... 0x00 .... Hardware-ID (war bislang immer 0)
3. Byte.... 0x00 .... Sub-ID (In RoboPro die Funkrufnummer bzw. Gruppe)
4. Byte.... ...... .... Befehl Low-Byte
5. Byte.... ...... .... Befehl High-Byte
6. Byte.... ...... .... Daten Low-Byte
7. Byte.... ...... .... Daten High-Byte

Mit eben diesem Protokoll ist es auch möglich, Daten zum RoboInterface
zurückzusenden.

WICHTIG: Diese Art der seriellen Kommunikation funktioniert ausschließlich mit
RoboPro-Programmen, die auf das RoboInterface heruntergeladen und dort
gestartet wurden!

Hier nun zwei kleine Beispiele, wie man mit einem PC eine Datenübertragung
aufbauen könnte:

Beispiel 1

Senden vom PC zum RoboInterface.
Das PC-Programm ist in C geschrieben und soll nur als Beispiel dienen.
Am besten kopiert man sich den Beispielcode in einen Editor, er ist dann um
einiges besser lesbar.

Das Programm sendet den Befehl "=" einmal mit dem Wert 30 und einmal mit dem
Wert 20. Mit dem zugehörigen RoboPro-Programm wird für eine Sekunde die
Leuchte an O8 leuchten (die 20 wurde nicht gesendet), dann für eine weitere
Sekunde O8 + O7 (die 20 wurde gesendet).

```
// Beispiel 1: Senden
// Senden vom PC zum RoboInterface
//

#include <stdio.h>
#include <stdlib.h>

#define uchar unsigned char
#define uint unsigned int

#define RoboRight 0xFFFE
#define RoboLeft 0xFFFD
#define RoboStop 0xFFFC
#define RoboOn 0xFFFB
#define RoboOff 0xFFFA
#define RoboEqual 0xFFF9
#define RoboPlus 0xFFF8
#define RoboMinus 0xFFF7
#define RoboAdd 0xFFF6
#define RoboDelete 0x2184 // Nobody knows, why its not 0xFFF5
#define RoboChange 0xFFF4

void fputint (int value, FILE *Handle)
{
fputc(value & 0xFF, Handle);
fputc(value >> 8, Handle);
}

void fputrobopro (uchar HwID, uchar SubID, int command, int value, FILE *Handle)
{
fputc(0x90,Handle); // Startbyte
fputc(HwID,Handle); // Hardware-ID
fputc(SubID,Handle);
fputint(command,Handle);
fputint(value,Handle);
}

int main (void)
{
FILE *StreamHandle;

StreamHandle = fopen("COM5:38400,8,N,1","w"); // Öffnen der seriellen Schnittstelle, muss angepasst werden !
printf("r: %d",StreamHandle);

fputrobopro (0,9,RoboEqual,30,StreamHandle); // sende Befehl "=" und Wert 30 ans RoboInterface
fputrobopro (0,9,RoboEqual,20,StreamHandle); // sende Befehl "=" und Wert 20 ans RoboInterface

fclose(StreamHandle);
}
```

Und hier das zugehörige RoboPro-Programm (ich habe die Eigenschaften des
Empfänger-Bausteins aufgeklappt, damit man die Einstellungen sehen kann)

http://www.ftcommunity.de/~eggdrop/wiki_lcd/Empfaenger.jpg


Beispiel 2

Senden vom RoboInterface zum PC

```
// Beispiel2 - Empfangen
// Empfangen von Daten vom RoboInterface
//

#include <stdio.h>
#include <stdlib.h>

#define uchar unsigned char
#define uint unsigned int

#define RoboRight 0xFFFE
#define RoboLeft 0xFFFD
#define RoboStop 0xFFFC
#define RoboOn 0xFFFB
#define RoboOff 0xFFFA
#define RoboEqual 0xFFF9
#define RoboPlus 0xFFF8
#define RoboMinus 0xFFF7
#define RoboAdd 0xFFF6
#define RoboDelete 0x2184 // Nobody knows, why its not 0xFFF5
#define RoboChange 0xFFF4

int fgetc_wait (FILE *Handle) // Wartet, bis ein Zeichen seriell empfangen wurde
{
int result;
do
{
result = fgetc(Handle);
}
while (result < 0); // "result" ist -1, solange kein Zeichen empfangen wird

return result;
}

int fgetint (FILE *Handle) // Liest einen Int-Wert vom RoboInterface
{
int result;

result = fgetc_wait(Handle);
result |= (fgetc_wait(Handle) << 8);

return result;
}

int fgetrobopro (FILE *Handle)
{
while (fgetc_wait(Handle) != 0x90); // Auf Startbyte warten
fgetc_wait(Handle); // Dummy-Read der Hardware-ID
fgetc_wait(Handle); // Dummy-Read der Sub-ID
if (fgetint(Handle) == RoboEqual) // Befehlscode auswerten
return fgetint(Handle); // Falls Befehlscode "=", dann Daten zurückgeben
else
{
fgetint(Handle); // Dummy-Read
return 0; // Evtl. einen anderen Fehlercode benutzen
}
}

int main (void)
{
FILE *StreamHandle;
int result;

StreamHandle = fopen("COM5:38400,8,N,1","r");

printf("r: %d",StreamHandle);

printf ("Daten empfangen: %d rn", fgetrobopro (StreamHandle));

fclose(StreamHandle);
}
```

und ein entsprechendes Testprogramm für RoboPro:

http://www.ftcommunity.de/~eggdrop/wiki_lcd/Sender.jpg
