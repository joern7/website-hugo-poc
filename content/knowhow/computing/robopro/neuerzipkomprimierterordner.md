---
layout: "file"
hidden: true
title: "Schreibmaschine"
date: "2015-03-19T00:00:00"
file: "neuerzipkomprimierterordner.zip"
konstrukteure: 
- "Nicolas"
uploadBy:
- "Nicolas"
license: "unknown"
legacy_id:
- /data/downloads/robopro/neuerzipkomprimierterordner.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/neuerzipkomprimierterordner.zip -->
man kann mit demkleinen Programm Texte und ähnliches aufschreiben und über CSV schreiben