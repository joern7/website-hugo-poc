---
layout: "file"
hidden: true
title: "Besonderes Lauflicht"
date: "2010-05-13T00:00:00"
file: "blauflicht.rpp"
konstrukteure: 
- "Heini009"
uploadBy:
- "Heini009"
license: "unknown"
legacy_id:
- /data/downloads/robopro/blauflicht.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/blauflicht.rpp -->
Ein Lauflicht, dass die 4 Motoraugänge des IF braucht.
Die Geschwindigkeit kann man im Unterprogramm time einstellen.