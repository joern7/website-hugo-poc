---
layout: "file"
hidden: true
title: "Eucalypta Hexe"
date: "2017-02-03T00:00:00"
file: "eucalyptascenario1.rpp"
konstrukteure: 
- "Peter Damen (Poederoyen, Holland)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/eucalyptascenario1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/eucalyptascenario1.rpp -->
Meine Hexe Eucalypta hat die folgende Funktionen:

Kopf in die höhe (pneumatisch)
Nein-Bewegung (electrisch) 
Ja-Bewegung (electrisch) 
Zunge Bewegung (electrisch) 
Augebraue Bewegung(pneumatisch) 
Wange Bewegung(pneumatisch) 
Finger Bewegung(pneumatisch) 
Hand schwingen(electrisch) 

Für alle Bewegungen habe ich ein Unterprogramm. 

Ich möchte aber in das Hauptprogram gerne verschiedene (aber nicht alle) Unterprogrammen zugleich funktionieren lassen. Wie schaffe ich dass ?...............

