---
layout: "file"
hidden: true
title: "Doppelaufzug"
date: "2017-02-03T00:00:00"
file: "Doppelaufzug.zip"
konstrukteure: 
- "Uwe Timm"
uploadBy:
- "Uwe Timm"
license: "unknown"
legacy_id:
- /data/downloads/robopro/Doppelaufzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/Doppelaufzug.zip -->
Für das Hauptprogramm und etliche Unterprogramme sind unter "Beschreibung" Hinweise und Kommentare hinterlegt.

Genauere Infos zum Modell gibt es im Forum.