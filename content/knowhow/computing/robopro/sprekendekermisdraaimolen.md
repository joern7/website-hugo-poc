---
layout: "file"
hidden: true
title: "Sprekende Kermis-draaimolen met voice- en soundmodules"
date: "2017-02-03T00:00:00"
file: "sprekendekermisdraaimolen.rpp"
konstrukteure: 
- "Peter Damen (Poederoyen, Holland)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sprekendekermisdraaimolen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sprekendekermisdraaimolen.rpp -->
Sprekende Kermis-draaimolen met voice- en soundmodules