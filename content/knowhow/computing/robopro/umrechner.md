---
layout: "file"
hidden: true
title: "Umrechner"
date: "2015-03-27T00:00:00"
file: "umrechner.rpp"
konstrukteure: 
- "Nicolas"
uploadBy:
- "Nicolas"
license: "unknown"
legacy_id:
- /data/downloads/robopro/umrechner.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/umrechner.rpp -->
dieses kleine Programm kann die große des FT-discovery-Set Balls in eine cm angabe umrechnen. Das Programm wurde zwischen 5-70 cm getestet. Damit es besser klappt sollte die Kamera nicht auf zu buntem Hintergrund verwendet werden. Sonst wird der Ball überhaupt nicht erkannt.