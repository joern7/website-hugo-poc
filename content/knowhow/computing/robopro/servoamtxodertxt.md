---
layout: "file"
hidden: true
title: "Servo am TX oder TXT"
date: "2017-12-29T00:00:00"
file: "servoamtxodertxt.zip"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/servoamtxodertxt.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/servoamtxodertxt.zip -->
Robo Pro Programm und Anleitung zum Regeln von Servos direkt am TX oder TXT.
Am TXT kann der Servo via IR Control ferngesteuert werden. Benötigt wird ein Getriebemotor mit Potentiometer als Winkelsensor. 