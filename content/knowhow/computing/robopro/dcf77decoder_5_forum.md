---
layout: "file"
hidden: true
title: "DCF77-Decoder V5"
date: "2017-02-03T00:00:00"
file: "dcf77decoder_5_forum.zip"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoder_5_forum.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoder_5_forum.zip -->
Nach längerer Zeit gibt es nun auch meinen DCF77-Decoder V5, den ich in der V4 schon am 10. August 2012 vorgestellt hatte.

Eine Neuauflage wurde mit den neuen ROBOPro Versionen (4.1, 4.23) erforderlich: Das Timing des Decoders (Kalibrierung) musste angepasst werden.
Bei der Gelegenheit habe ich noch ein paar Details verändert.

Erfolgreich habe ich den DCF77-Decoder V5 jetzt auch mit dem TXT Controller getestet.
