---
layout: "file"
hidden: true
title: "RoboPro Segmentwehre"
date: "2017-02-03T00:00:00"
file: "niveauregeling.rpp"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/niveauregeling.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/niveauregeling.rpp -->
Ich versuche mit ein Pot-Meter und RoboPro in 2 Wasserbecken eine stabile verstelbare Wasserstand zu erreichen.

Mit kurze Motorbetriebzeiten (<0,1sec) ist eine stabile Wasserstand möglich: nur +/-1 cm. 
Statt die Impuls-schalter nutze ich <0,1 sec für eine kleine Wehr-Motor-Bewegung. 
Jede 0,5 sekunden wirt die Wasserstand mit einem US-Sensor gemessen.
Wenn es eine niedrige Wasserstand gibt als mit einem Pot-meter eingefuhrt und berechnet, bewegt die Wehre ein wenig nach Unten. Mit langere (>0,1sec) und deswegen grosse Wehr-Bewegungen wird dass Prozess Instabil.

Im [Bilderpool](http://www.ftcommunity.de/details.php?image_id=14926)
