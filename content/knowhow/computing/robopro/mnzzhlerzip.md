---
layout: "file"
hidden: true
title: "Münzzähler für Münzsortierer von Mirose"
date: "2017-02-03T00:00:00"
file: "mnzzhler.zip"
konstrukteure: 
- "Laserman"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/robopro/mnzzhler.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/mnzzhler.zip -->
Man muß lediglich 8 Fototransistoren und 8 Lämpchen mit Störlichtkappe in die 8 Fallschlitze einbauen. Und zwar die Lämpchen immer in Rollrichtung der Münzen. Und schon werden die Münzen nicht nur sortiert, sondern auch gezählt. 
Sie dürfen lediglich nicht ganz so schnell hintereinander kommen, sonst kann mal die ein- oder andere Münze nicht erfaßt werden.
Man kann es auch ausprobieren, ohne die Fototransistoren. Einfach in RoboPro mit dem Interface-Test jeweils einen Eingang schließen und wieder öffnen. Vorher bei "Com" auf Simulation stellen.
