---
layout: "file"
hidden: true
title: "Insektensteuerung"
date: "2017-02-03T00:00:00"
file: "insekt5.rpp"
konstrukteure: 
- "Patrick P."
uploadBy:
- "Patrick P."
license: "unknown"
legacy_id:
- /data/downloads/robopro/insekt5.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/insekt5.rpp -->
Ein Programm, das einen sechsbeinigen Roboter ansteuert.
Die Beine "denken" jedoch so, wie beim echten Insekt alleine und geben nur Befehle weiter, wann das nächste Bein anfangen soll.