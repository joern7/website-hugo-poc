---
layout: "file"
hidden: true
title: "Tic Tac Toe Programm"
date: "2017-02-03T00:00:00"
file: "tictactoe.rpp"
konstrukteure: 
- "Stefan Lehnerer"
uploadBy:
- "Stefan Lehnerer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/tictactoe.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/tictactoe.rpp -->
In diesem Programm kann man Tic Tac Toe gegen den Computer spielen. Er erkennt alle möglichkeiten wenn der Gegner schon 2 Steine in einer Reihe hat. Man kann nur mit Tricks gewinnen. Der Computer macht, wenn er grad nicht verlieren kann, nach Zufallsprinzip iergendwo ein Stein hin. Also gewinnt man selber oder es ist untentschieden, dass der Computer gewinnt ist sehr unwarscheinlich. 