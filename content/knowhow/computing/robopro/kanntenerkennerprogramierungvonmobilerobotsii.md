---
layout: "file"
hidden: true
title: "Kantenerkenner (Mobile RobotsII)"
date: "2017-02-03T00:00:00"
file: "kanntenerkennerprogramierungvonmobilerobotsii.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/kanntenerkennerprogramierungvonmobilerobotsii.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/kanntenerkennerprogramierungvonmobilerobotsii.rpp -->
Das ist das Programm was ich für den Kanntenerkenner geschrieben habe.