---
layout: "file"
hidden: true
title: "Taschenrechner 2"
date: "2017-02-03T00:00:00"
file: "taschenrechner.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/taschenrechner.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/taschenrechner.rpp -->
Das ist ein Taschenrechner den ich mit Robo Pro Programmiert habe.  

Der Taschenrechner 2 kann jetzt auch Kommazahlen ausrechnen. Aber man kann noch nicht Kammozahlen eintippen
