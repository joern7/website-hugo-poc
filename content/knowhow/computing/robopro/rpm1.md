---
layout: "file"
hidden: true
title: "Umdrehungsberechner"
date: "2017-02-03T00:00:00"
file: "rpm1.rpp"
konstrukteure: 
- "Kacker303"
uploadBy:
- "Kacker303"
license: "unknown"
legacy_id:
- /data/downloads/robopro/rpm1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/rpm1.rpp -->
Dieses Programm berechnet die Umdrehungen pro Minute wobei der Messungszeitraum zwischen 5-10 sekunden einstellbar ist.