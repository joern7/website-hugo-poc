---
layout: "file"
hidden: true
title: "I²C-Treiber v1.0 für Grove - Touch-Sensor Board mit MPR121 "
date: "2017-02-03T00:00:00"
file: "mpr121_touchsensor_v1.0.zip"
konstrukteure: 
- "Georg Stiegler (fantogerch)"
uploadBy:
- "Georg Stiegler (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/mpr121_touchsensor_v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/mpr121_touchsensor_v1.0.zip -->
RoboPro-Treiber für das I²C Touch-Sensor Board von Grove für das Ansteuern von bis zu 12 Touch-Sensoren mittels MPR121. Zwei Treiber-Dateien: die "abgespeckte Minimal-Version" verzichtet dabei auf Filtereinstellungen ...