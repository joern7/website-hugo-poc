---
layout: "file"
hidden: true
title: "Riesenrad "
date: "2017-02-03T00:00:00"
file: "riesenrad_3.rpp"
konstrukteure: 
- "Pascal Jan (bauFischertechnik)"
uploadBy:
- "Pascal Jan (bauFischertechnik)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/riesenrad_3.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/riesenrad_3.rpp -->
Mit diesem Programm kann man mein Riesenradmodell "London-Eye" steuern.