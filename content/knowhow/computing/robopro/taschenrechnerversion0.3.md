---
layout: "file"
hidden: true
title: "Taschenrechner (version 0.3)"
date: "2017-02-03T00:00:00"
file: "taschenrechnerversion0.3.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/taschenrechnerversion0.3.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/taschenrechnerversion0.3.rpp -->
Taschenrechner der jetzt auch % ausrechnen kann.