---
layout: "file"
hidden: true
title: "Licht/Spursucher m. Kanten/Hinderniserkennung"
date: "2017-02-03T00:00:00"
file: "lichtspursucherm.kantenhinderniserkennung.rpp"
konstrukteure: 
- "Michael K."
uploadBy:
- "Michael K."
license: "unknown"
legacy_id:
- /data/downloads/robopro/lichtspursucherm.kantenhinderniserkennung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/lichtspursucherm.kantenhinderniserkennung.rpp -->
Programm für: Licht/Spursucher m. Kanten/Hinderniserkennung. Bilder gibt es im Bilderpool