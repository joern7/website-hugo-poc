---
layout: "file"
hidden: true
title: "BCD-Ausgabe"
date: "2009-03-13T00:00:00"
file: "bcdausgabe.zip"
konstrukteure: 
- "Laserman"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/robopro/bcdausgabe.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/bcdausgabe.zip -->
Mit diesem Programm wird gezeigt, wie man eine Zahl (kann im Programm mit dem Schieber eingestellt werden) auf eine externe 7-Segment Anzeige bringen kann.
Man schließt einen BCD to 7-segment Decoder an den 4 Ausgängen des Interfaces an (Typ 4511 - geht auch für Suche bei Conrad). Dieser wandelt die 4 Ausgänge (Binär - 1,2,4,8) in 10 Zahlen (0 bis 9) von EINER 7-Segment Anzeige um. 
Ausgang 1 vom Interface geht dann auf Anschluß A des IC.
Ausgang 2 vom Interface geht dann auf Anschluß B des IC.
Ausgang 3 vom Interface geht dann auf Anschluß C des IC.
Ausgang 4 vom Interface geht dann auf Anschluß D des IC.
Hier gibt es eine genauere Beschreibung des ICs:
http://www.doctronics.co.uk/4511.htm