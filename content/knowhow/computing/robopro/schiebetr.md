---
layout: "file"
hidden: true
title: "Schiebetür"
date: "2017-02-03T00:00:00"
file: "schiebetr.rpp"
konstrukteure: 
- "fish"
uploadBy:
- "fish"
license: "unknown"
legacy_id:
- /data/downloads/robopro/schiebetr.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/schiebetr.rpp -->
Dies ist ein Programm zum pneumatischen öffnen und schließen der Tür. Die schiebetür öffnet sich sobald eine Lichtschranke unterbrochen ist. Steht beim Schließen ein Gegenstand im Weg geht die Tür sofort wieder auf.