---
layout: "file"
hidden: true
title: "I2C-Bus Scanner v1-5"
date: "2017-01-12T00:00:00"
file: "i2cidentification_v15.rpp"
konstrukteure: 
- "Georg Stiegler  (fantogerch)"
uploadBy:
- "Georg Stiegler  (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cidentification_v15.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cidentification_v15.rpp -->
Programm zum identifizieren der I2C-Adresse eines I2C-Moduls

(@Admin: bitte die Datei vom 12.09.2014 ersetzen oder löschen)