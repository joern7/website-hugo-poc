---
layout: "file"
hidden: true
title: "Tic-Tac-Toe-Roboter"
date: "2017-02-03T00:00:00"
file: "tictactoeroboter.rpp"
konstrukteure: 
- "Jan Käberich"
uploadBy:
- "Jan Käberich"
license: "unknown"
legacy_id:
- /data/downloads/robopro/tictactoeroboter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/tictactoeroboter.rpp -->
Hier das Programm des Tic-Tac-Toe-Roboters.
Er kann keine diagonalen Zwickmühlen erkennen, sonst wäre er fast unbesiegbar.