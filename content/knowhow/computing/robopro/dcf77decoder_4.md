---
layout: "file"
hidden: true
title: "DCF77-Decoder V4"
date: "2017-02-03T00:00:00"
file: "dcf77decoder_4.zip"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoder_4.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoder_4.zip -->
Bei der Arbeit zum nächsten ft:pedia Artikel sind ZWEI DCF77-Decoder parallel entstanden. Hier seht ihr mein Decoder-Beispiel V4, das die Aufgabe etwas anders umsetzt, als die tolle Version von Dirk Fox. Es ist gut getestet. Der eigentliche Decoder hat die Version 0.97, d.h. kann sicher noch verbessert werden (Zählen gültiger Telegramme, ggf. automatische Reparatur von defekten Telegrammen ...), bis er die Version 1.00 verdient.
Viel Spaß damit!
