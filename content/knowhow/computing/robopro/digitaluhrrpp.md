---
layout: "file"
hidden: true
title: "Digitaluhr"
date: "2017-02-03T00:00:00"
file: "digitaluhr.rpp"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/digitaluhr.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/digitaluhr.rpp -->
Das ist das RoboPro-Programm für die Digitaluhr so wie sie auf der Convention 2007 ausgestellt war.
