---
layout: "file"
hidden: true
title: "Programm zu meinem Auto"
date: "2017-02-03T00:00:00"
file: "ulltraschallauto.rpp"
konstrukteure: 
- "Johannes Deppert"
uploadBy:
- "Johannes Deppert"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ulltraschallauto.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ulltraschallauto.rpp -->
Das ist dass Programm zu meinem Auto das nicht gegen die Wand fährt.

Anschlüsse:

Ulltraschallsensor D1
Motor M1
Summer M1