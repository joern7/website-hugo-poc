---
layout: "file"
hidden: true
title: "Kartenmischersteuerer"
date: "2017-02-03T00:00:00"
file: "kartenmischer.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/robopro/kartenmischer.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/kartenmischer.rpp -->
Das Programm zu meinem Kartenmischer.
es soll die Motoren so regeln, das die Karten so fallen:links-rechts-links und
NICHT links-rechts-rechts-links-links -stau-kaputt ;)