---
layout: "file"
hidden: true
title: "Bagger"
date: "2017-02-03T00:00:00"
file: "bagger.rpp"
konstrukteure: 
- "fish"
uploadBy:
- "fish"
license: "unknown"
legacy_id:
- /data/downloads/robopro/bagger.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/bagger.rpp -->
Der Bagger wird über Bedienfeld oder IR-Fernbedienung gesteuert. Das für pneumatische Bagger ausgelegte Programm steuert:
-Die Räder oder Ketten (Vor, Rück, Kurve rechts/links und Drehen rechts/links)
-Den Baggerarm (Arm hoch/runter, Arm vor/zurück und die Schaufel aufnehmen/auskippen)
-Als kleines Extra kann das Licht ein und ausgeschaltet werden und die Geschwindigkeit angepasst werden.

Ein interresantes Programm, nicht nur für Bagger!