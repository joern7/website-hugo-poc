---
layout: "file"
hidden: true
title: "Betrachtung zum Element "
date: "2012-05-23T00:00:00"
file: "2012_05_22_zeitenfehlerrobopro.rpp"
konstrukteure: 
- "anonymus"
uploadBy:
- "anonymus"
license: "unknown"
legacy_id:
- /data/downloads/robopro/2012_05_22_zeitenfehlerrobopro.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/2012_05_22_zeitenfehlerrobopro.rpp -->
In 4 Tasks werden die Programmdurchläufe gezählt, eigentlich sollten die Zählwerte multipliziert mit ihren jeweiligen Wartezeiten zu annähernd gleichen Ergebnissen führen, aber ...