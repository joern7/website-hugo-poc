---
layout: "file"
hidden: true
title: "Interface Test"
date: "2008-07-12T00:00:00"
file: "interfacetest.rpp"
konstrukteure: 
- "Paul Nehlich"
uploadBy:
- "Paul Nehlich"
license: "unknown"
legacy_id:
- /data/downloads/robopro/interfacetest.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/interfacetest.rpp -->
Sämtliche Sensoren aus dem Robo Explorer werden überprüft, die Daten werden angezeigt und alle 10 Minuten in eine nicht definierte Tabelle gespeichert. auch habe ich eine uhr eingebaut, wozu auch immer.