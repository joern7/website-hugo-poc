---
layout: "file"
hidden: true
title: "RoboPro-I²C-Teiber AD / DA-Wandler PCF8591"
date: "2017-03-25T00:00:00"
file: "i2cpcf8591treiber1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cpcf8591treiber1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cpcf8591treiber1.0.rpp -->
RoboPro-I²C-Teiber für den  AD / DA-Wandler PCF8591.
- 4x Analogeingänge
- 1x Analogausgang
- Auflösung: 8 Bit