---
title: "Computing"
weight: 3
---
Viele Fans erwecken ihre ft-Modelle mit Computern zum Leben.
Wir sind da richtig kreativ und es gibt nichts, was nicht geht.
Während die Modelle selbst im
[Bilderpool](../../bilderpool)
dokumentiert sind, finden sich hier viele Infos rund um die verwendeten
Computer, Interfaces und die Programme für die verschiedensten Plattformen.

Da fischertechnik sogar das erste Konstruktionsspielzeug mit
"Computeranschluss" ist, ist das hier auch ein bisschen ein "Geschichtsbuch"
geworden.
Also wundere Dich nicht, wenn Du hier Dokus bis weit ins letzte Jahrtausend
findest.
Die "uralte" Technik erfreut sich immer noch großer Beliebtheit.
