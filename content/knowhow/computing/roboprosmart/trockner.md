---
layout: "wiki"
title: "Händetrockner"
hidden: true
date: 2020-03-26T15:51:36+01:00
konstrukteure: 
- "EstherM"
uploadBy:
- "EstherM"
license: "unknown"
---

Der Motor in einem Händetrockner soll 8 Sekunden laufen, wenn ein Taster gedrückt wird. Danach soll der Motor wieder ausgehen und das Programm enden.

Ein *Algorithmus* ist übrigens eine Vorschrift, nach der der Computer eine Handlung ausführt.

![Trockner1](../Trockner1.jpg)

Im nächsten Beispiel ist der Trockner immer aktiv, weil unten am Ende des Programmes eine Verbindung nach oben geht, so dass der Ablauf wieder von oben nach unten läuft. 

![Trockner1a](../Trockner1a.jpg)

Wie bei einem richtigen Händetrockner gibt es jetzt eine Lichtschranke statt dem Tasters. Das Licht dafür kommt von M1. Der Motor ist daher jetzt an M2.

![Trockner1b](../Trockner1b.jpg)
