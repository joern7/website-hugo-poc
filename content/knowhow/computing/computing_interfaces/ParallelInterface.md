---
layout: "file"
hidden: true
title: "Schaltplan"
date: "2005-05-20T00:00:00"
file: "ParallelInterface.zip"
konstrukteure: 
- "Holger Howey"
uploadBy:
- "Holger Howey"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/ParallelInterface.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/ParallelInterface.zip -->
Holger Howey hat die Schaltpläne des "alten" parallelen Interface nachgezeichnet und sehr datailliert erklärt.