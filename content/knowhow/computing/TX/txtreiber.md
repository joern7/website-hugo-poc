---
layout: "file"
hidden: true
title: "TX-Treiber"
date: "2016-01-17T00:00:00"
file: "txtreiber.rar"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/txtreiber.rar
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/txtreiber.rar -->
Die Treibersammlung unterstützt den TX Controller V1.30. Ergänzt wurden Befehle für das GPS-Modul Navigatron und die Kamera Pixy-CMUcam5 am i2c-Port (EXT2). Enthalten sind Wrapper-Dateien für Java, Dateien zur Einbindung in c++ und eine Spracherweiterung für TerrapinLogo. Weitere Informationen enthält die Seite von Ulrich Müller unter Sonstiges.