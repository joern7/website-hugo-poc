---
layout: "file"
hidden: true
title: "umfish-Treiber"
date: "2006-02-25T00:00:00"
file: "umfish.zip"
konstrukteure: 
- "Ulrich Müller"
uploadBy:
- "Ulrich Müller"
license: "unknown"
legacy_id:
- /data/downloads/software/umfish.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/umfish.zip -->
für die FreeSpeed-Programme.
Es handelt sich hierbei um eine ältere Version; Für aktuelle Versionen siehe http://www.ftcomputing.de (für Eigenentwicklungen; FreeSpeed läuft nur mit der hier angebotenen Version)