---
layout: "file"
hidden: true
title: "3D-Tisch XYZ.zip"
date: "2012-05-21T00:00:00"
file: "3dtischxyz.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/3dtischxyz.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/3dtischxyz.zip -->
3D-Tisch XYZ von Jürgen Fischer mit Hubgetrieben und E-Magnet