---
layout: "file"
hidden: true
title: "Materialaufzug.zip"
date: "2012-05-21T00:00:00"
file: "materialaufzug.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/materialaufzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/materialaufzug.zip -->
Materialaufzug. Original von Fischertechnik Experimenta Computing Schulprogramm.