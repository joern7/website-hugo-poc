---
layout: "file"
hidden: true
title: "Radlader"
date: "2015-01-11T00:00:00"
file: "schaufellader.ftm"
konstrukteure: 
- "david"
uploadBy:
- "david"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/schaufellader.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/schaufellader.ftm -->
Nachbau meines Radladers mit dem fischertechnik Designer (http://ftcommunity.de/categories.php?cat_id=3006)
Dank Kinematik lässt sich das Fahren animieren.
