---
layout: "file"
hidden: true
title: "Pneumatische Drehflügeltür mit Animation.zip"
date: "2012-05-21T00:00:00"
file: "pneumatischedrehflgeltrmitanimation.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatischedrehflgeltrmitanimation.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatischedrehflgeltrmitanimation.zip -->
Pneumatische Drehflügeltür. Original von Fischertechnik Pneumatik.