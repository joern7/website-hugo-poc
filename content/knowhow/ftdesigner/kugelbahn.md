---
layout: "file"
hidden: true
title: "Kugelbahn mit neuen Teilen aus Mobil-Kästen für FT-Designer"
date: "2013-04-24T00:00:00"
file: "kugelbahn.zip"
konstrukteure: 
- "Johannes Visserr"
uploadBy:
- "Johannes Visserr"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/kugelbahn.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/kugelbahn.zip -->
Komplexes Modell mit Teilen aus dem Dynamik-Kasten (Kugelbahn)
Besonderheit: Die Teile sind im FT-Designer aktuell noch gar nicht untersützt. Ich hab die Teile selber modellierit und in den FT-Designer eingefügt. Man muss die Bauteile aus den Ordnern im Zip-File in die entsprechenden Ordner des FT-Designers kopieren und dann sind die Teile da. 
Sollte Herr Samek (der Autor) die Teile irgendwann mal offiziell rausgeben, müsste man die Files wieder löschen, sonst tuts der Update nicht. 
Dabei ist auch eine Anleitung zum drucken und ein kleines WMV-Video, dass das Modell in Aktion zeigt. 