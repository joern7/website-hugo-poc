---
layout: "file"
hidden: true
title: "Dampfwalze.zip"
date: "2012-05-21T00:00:00"
file: "dampfwalze.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/dampfwalze.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/dampfwalze.zip -->
Dampfwalze. Original von Fischertechnik Limited Edition (2000 Stück). FT-Designer Datei von Michael Samek.