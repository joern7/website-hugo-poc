---
layout: "file"
hidden: true
title: "Gleichlauf-Gelenk.zip"
date: "2012-05-21T00:00:00"
file: "gleichlaufgelenk.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/gleichlaufgelenk.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/gleichlaufgelenk.zip -->
Gleichlauf-Gelenk. Ein sehr faszinierendes Gelenk. Original von Fischertechnik Hobby 2_3 Mechanik und KFZ-Technik.