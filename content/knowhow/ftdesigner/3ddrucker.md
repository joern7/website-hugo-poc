---
layout: "file"
hidden: true
title: "3D-Drucker.zip"
date: "2012-05-21T00:00:00"
file: "3ddrucker.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/3ddrucker.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/3ddrucker.zip -->
3D-Drucker mit Heißkleber. 
Eigentlich gibt es ihn auch hier: https://reprap.org/wiki/FTIStrap . 
Nur der Vollständigkeit halber. Original von Andreas Rozek.
