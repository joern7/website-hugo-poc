---
layout: "file"
hidden: true
title: "Kugelbahn mit neuen Teilen aus Dynamik-Kästen für FT-Designer / Aktualisierung"
date: "2013-04-25T00:00:00"
file: "kugelbahn2.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/kugelbahn2.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/kugelbahn2.zip -->
Komplexes Modell mit Teilen aus dem Dynamik-Kasten (Kugelbahn)
Besonderheit: Die Teile sind im FT-Designer aktuell noch gar nicht untersützt. Ich hab die Teile selber modellierit und in den FT-Designer eingefügt. Man muss die Bauteile aus den Ordnern im Zip-File in die entsprechenden Ordner des FT-Designers kopieren und dann sind die Teile da.
Sollte Herr Samek (der Autor) die Teile irgendwann mal offiziell rausgeben, müsste man die Files wieder löschen, sonst tuts der Update nicht.
Dabei ist auch eine Anleitung zum Drucken und ein kleines WMV-Video, dass das Modell in Aktion zeigt. 
Neue Teile 121641 Flexprofil blau, 38247 30x30 Platte rot 2 Zapfen, 38252 Kupplungsstück rot 5 Löcher, 143234 FlexProfili grün,  144262 Stahlkugel
