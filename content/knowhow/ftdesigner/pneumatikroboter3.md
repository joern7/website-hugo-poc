---
layout: "file"
hidden: true
title: "Pneumatik Roboter 3.zip"
date: "2012-05-21T00:00:00"
file: "pneumatikroboter3.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatikroboter3.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatikroboter3.zip -->
Pneumatik Roboter 3 aus dem Experimenta Schulprogramm.