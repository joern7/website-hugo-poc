---
layout: "file"
hidden: true
title: "Codekartenleser mit automatischem Einzug.zip"
date: "2012-05-21T00:00:00"
file: "codekartenlesermitautomatischemeinzug.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/codekartenlesermitautomatischemeinzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/codekartenlesermitautomatischemeinzug.zip -->
Codekartenleser mit automatischem Einzug. Original von Fischertechnik Profi Computing. Codekarte zum Ausdrucken anbei.