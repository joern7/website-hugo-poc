---
layout: "file"
hidden: true
title: "Bahnhofsdach.zip"
date: "2012-05-21T00:00:00"
file: "bahnhofsdach.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/bahnhofsdach.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/bahnhofsdach.zip -->
Bahnhofsdach aus Fischertechnik Hobby-Buch 1-5