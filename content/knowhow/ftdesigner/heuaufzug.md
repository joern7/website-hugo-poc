---
layout: "file"
hidden: true
title: "Heu-Aufzug.zip"
date: "2012-05-21T00:00:00"
file: "heuaufzug.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/heuaufzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/heuaufzug.zip -->
Heuaufzug. Man kann damit Heuballen hochziehen, die dann automatisch in den Heuboden fahren. Dann kommt der Haken automatisch wieder runter. Original von Fischertechnik Hobby 1-2.