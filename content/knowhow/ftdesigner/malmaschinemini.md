---
layout: "file"
hidden: true
title: "Malmaschine Mini.zip"
date: "2012-05-21T00:00:00"
file: "malmaschinemini.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/malmaschinemini.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/malmaschinemini.zip -->
Malmaschine Mini. Man kann damit Lissajous-Figuren (drehende Ellipsen) wie beim Spirograph zeichnen. Original von Frank Jakob.