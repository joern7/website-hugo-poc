---
layout: "file"
hidden: true
title: "Gleichlauf-Winkelkupplung.zip"
date: "2012-05-21T00:00:00"
file: "gleichlaufwinkelkupplung.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/gleichlaufwinkelkupplung.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/gleichlaufwinkelkupplung.zip -->
Gleichlauf-Winkelkupplung. Ein sehr faszinierendes Gelenk um die Ecke. Original von Fischertechnik Hobby 2-2.