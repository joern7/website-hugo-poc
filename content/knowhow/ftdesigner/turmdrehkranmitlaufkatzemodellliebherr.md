---
layout: "file"
hidden: true
title: "Turmdrehkran mit Laufkatze - Modell Liebherr.zip"
date: "2012-05-21T00:00:00"
file: "turmdrehkranmitlaufkatzemodellliebherr.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/turmdrehkranmitlaufkatzemodellliebherr.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/turmdrehkranmitlaufkatzemodellliebherr.zip -->
Turmdrehkran. Original von Fischertechnik Starlifters Master Plus.