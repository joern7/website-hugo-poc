---
layout: "file"
hidden: true
title: "Geldwechsel-Automat mit Animation.zip"
date: "2012-05-21T00:00:00"
file: "geldwechselautomatmitanimation.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/geldwechselautomatmitanimation.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/geldwechselautomatmitanimation.zip -->
Geldwechsel-Automat. Original von Fischertechnik Profi Computing.