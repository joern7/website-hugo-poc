---
layout: "file"
hidden: true
title: "Kleines Flugzeug"
date: "2013-03-27T00:00:00"
file: "kleinesflugzeug.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/kleinesflugzeug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/kleinesflugzeug.zip -->
Kleines Modell für jüngere und angehende FT-ler. 
Wir bieten nachmittags für die Kindern der Offenen Ganztagsschule Fischertechnik an. Die Teile dazu stammen aus dem Kasten "Fischertechnik in der Schule, Basiskasten"