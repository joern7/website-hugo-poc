---
layout: "file"
hidden: true
title: "3D-Konstruktion der Bahnhofs-Uhr"
date: "2012-06-30T00:00:00"
file: "uhr05.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/uhr05.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/uhr05.zip -->
Konstruktion der "Bahnhofs"-Uhr (siehe Fotos im [Bilderpool](http://www.ftcommunity.de/categories.php?cat_id=2580) als ft-Designer-Datei inklusive Kinematik-Simulation.
