---
layout: "file"
hidden: true
title: "FT-Designer / DEF-Maker"
date: "2013-09-01T00:00:00"
file: "defmaker.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/defmaker.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/defmaker.zip -->
DEF-Maker von Michael Samek. 

Dieses Tool dient als Hilfsmittel zur Erstellung der DEF-Datei (zentrales Element des FT-Designers) wenn man eigene Teile dem FT-Designer hinzufügen will. Das Tool wird wird in den Hauptordner des FT-designers kopiert und kann von dort gestartet werden. 
Michael Samek hat uns dieses Tool netterweise zur Verfügung gestellt unter folgendem Vorbehalt: 
Das Tool ist "as ist is". Es besteht kein Anspruch auf Vollständigkeit, Fehlerfreiheit. Die Anwendung geschieht auf eigenes Risiko. 

Eine detaillierte Anleitung ist in der FT:Pedia 3/2013 enthalten. 