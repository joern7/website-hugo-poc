---
layout: "file"
hidden: true
title: "Tieflader / LKW / Zugmaschine"
date: "2013-02-16T00:00:00"
file: "tieflader_junior.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/tieflader_junior.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/tieflader_junior.zip -->
Kleine Modelle mit Teilen aus den neuen Junior-Kästen
Inklusive pdf Anleitung. 