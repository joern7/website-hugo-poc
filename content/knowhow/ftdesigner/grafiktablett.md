---
layout: "file"
hidden: true
title: "Grafiktablett.zip"
date: "2012-05-21T00:00:00"
file: "grafiktablett.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/grafiktablett.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/grafiktablett.zip -->
Grafiktablett. Diente früher (80er bis 90er Jahre) zum digitalisieren. Original von Fischertechnik Computing.