---
layout: "file"
hidden: true
title: "Pneumatik Roboter 2.zip"
date: "2012-05-21T00:00:00"
file: "pneumatikroboter2.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatikroboter2.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatikroboter2.zip -->
Pneumatik Roboter 2 aus dem Experimenta Schulprogramm.