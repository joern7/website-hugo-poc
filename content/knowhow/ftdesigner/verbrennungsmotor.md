---
layout: "file"
hidden: true
title: "Verbrennungsmotor.zip"
date: "2012-05-21T00:00:00"
file: "verbrennungsmotor.zip"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/verbrennungsmotor.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/verbrennungsmotor.zip -->
4-Takt Verbrennungsmotor. Original von Fischertechnik Club Modell 1/1978. FT-Designer Datei von Michael Samek.