---
title: "Akkubox"
date: 2020-07-10T14:05:54+02:00
---

Im Jahr 2017 begann 
eine [Diskussion](https://forum.ftcommunity.de/viewtopic.php?f=15&t=4223) über 
die Vor- und Nachteile eines Selbstbau-Akkus. 
Hier gibt es eine Anleitung für einen solchen Selbstbau-Akku und die notwendigen 3D-Druck-Dateien. 

![Bild der Box](Akkubox.jpg)

Herzlichen Dank an Bello (Robert) für den Entwurf und die Anleitung.

**Wir übernehmen keine Verantwortung für eventuelle Schäden beim Betrieb dieses Akkus.**
