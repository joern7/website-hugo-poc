---
layout: "file"
hidden: true
title: "07_Lid_for_ABS_print"
date: 2020-07-10T14:23:07+02:00
file: "07_Lid_for_ABS_print.stl"
konstrukteure: 
- "Bello"
uploadBy:
- "Website-Team"
license: "by-nc"
---
Quelle: https://www.thingiverse.com/thing:2895198
