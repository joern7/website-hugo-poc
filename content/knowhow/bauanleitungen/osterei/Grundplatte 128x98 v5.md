---
layout: "file"
hidden: true
title: "stl-Datei Grundplatte 128x98"
date: 2020-04-06T11:43:51+02:00
file: "Grundplatte 128x98 v5.stl"
konstrukteure: 
- "Jan (juh)"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Die Grundplatte 128x98 (Grundplatte 125) von [Jan (juh) siehe Thingiverse](https://www.thingiverse.com/thing:4226554)
