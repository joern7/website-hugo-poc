---
layout: "file"
hidden: true
title: "ft-Designer-Datei Hafenkran"
date: 2020-04-06T11:43:38+02:00
file: "01_Hafenkran.ftm"
konstrukteure: 
- "Peter Habermehl"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Die Hafenkran-Datei für den ft-Designer
