---
layout: "file"
hidden: true
title: "ft-Designer-Datei Portalkran"
date: 2020-04-06T11:43:38+02:00
file: "03_Portalkran.ftm"
konstrukteure: 
- "Peter Habermehl"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Die Portalkran-Datei für den ft-Designer
