---
title: "Ostern 2020"
date: 2020-04-06T11:22:16+02:00
summary: "Der Community-Bausatz Box 125/1 Advanced Mini Cranes"
---
### Der Community-Bausatz "Box 125/1 Advanced Mini Cranes"

![Bild der Box 125/1 mit Anleitung](ftc_Box_mit_Anleitung.jpg "Die Box 125/1 mit Anleitung")

Als kleine Osterüberraschung für alle fischertechnik Fans haben wir einen kleinen Baukasten für drei Kranmodelle zusammengestellt.
Alle Teile lassen sich in der 3D-Druck-Sortierwanne 125 mit Grundplatte 125 (Dateien s. unten) verpacken. 

![Bild der gepackten Box](ftc_BoxGepackt.JPG "Der Teilesatz in der Box") 

Natürlich kann man die Modelle auch ohne 3D-Drucker bauen, denn es werden ansonsten nur Standard-fischertechnik-Teile verwendet.

![Bild des Deckblatts](ftc_Cover_Box125-1.jpg "Das Deckblatt der Box")

Vielen Dank an Jan (juh) für die Konstruktion der Grundplatte! Mehr Informationen zu den Druckdaten für Box und Grundplatte finden sich in [diesem](https://forum.ftcommunity.de/viewtopic.php?f=38&t=5919) und [in diesem Forumsthread](https://forum.ftcommunity.de/viewtopic.php?f=38&t=5904).

Wir wünschen allen ein geruhsames Osterfest und ... Gesundheit!

Für die ft-community
Peter Habermehl
