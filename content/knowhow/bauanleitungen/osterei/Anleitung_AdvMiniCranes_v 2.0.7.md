---
layout: "file"
hidden: true
title: "Bauanleitung zur Box 125/1"
date: 2020-04-06T11:43:20+02:00
file: "Anleitung_AdvMiniCranes_v 2.0.7.pdf"
konstrukteure: 
- "Peter Habermehl"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Das wichtigste Dokument des Community-Modellbausatzes „Box 125/1 Advanced Mini Cranes“.
Einzelteileliste, Bauanleitungen für die drei Modelle und der Packplan, um alle Teile in die 3D-Druck-Sortierwanne 125 zu packen.
