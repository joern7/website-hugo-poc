---
layout: "file"
hidden: true
title: "Anleitung h4 Grundbaustein"
date: 2010-07-02T00:00:00
file: "nachbauh4gb.pdf"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/nachbauh4gb.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/nachbauh4gb.pdf -->
Nachbau Hobby 4 Elektronik Grundbaustein in drei Versionen mit ausführlicher Erklärung.
