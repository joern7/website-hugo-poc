---
layout: "file"
hidden: true
title: "Gleichrichter"
date: 2005-12-31T00:00:00
file: "gleichrichter.brd"
konstrukteure: 
- "Thomas Kaiser"
uploadBy:
- "Thomas Kaiser"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/platinenlayoutsbrd/gleichrichter.brd
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/platinenlayoutsbrd/gleichrichter.brd -->
