---
layout: "file"
hidden: true
title: "Mono Flop"
date: 2005-12-31T00:00:00
file: "mono_flop.brd"
konstrukteure: 
- "Thomas Kaiser"
uploadBy:
- "Thomas Kaiser"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/platinenlayoutsbrd/mono_flop.brd
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/platinenlayoutsbrd/mono_flop.brd -->
