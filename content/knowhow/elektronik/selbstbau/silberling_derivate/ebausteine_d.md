---
layout: "file"
hidden: true
title: "Deutsche Übersetzung der Anleitung"
date: 2005-06-06T00:00:00
file: "ebausteine_d.pdf"
konstrukteure: 
- "Peter Meurer"
uploadBy:
- "Peter Meurer"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/ebausteine_d.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/ebausteine_d.pdf -->
Deutsche Übersetzung der der Anleitung für den Nachbau der E-Bausteine.  
*Anm. d. Red: Dieser Download gehört zu
[Nachbau der E-Bausteine (holländisch)](../ebausteine_holland)*
