---
layout: "file"
hidden: true
title: "OR NOR Baustein"
date: 2005-06-06T00:00:00
file: "ornor.pdf"
konstrukteure: 
- "Michael Becker"
uploadBy:
- "Michael Becker"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/schaltplne/ornor.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/ornor.pdf -->
Schaltplan "OR NOR Baustein" (36481) - Bestückungsvariante vom
["AND NAND Baustein"](../andnand)!
