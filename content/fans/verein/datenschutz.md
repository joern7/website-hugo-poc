---
title: "Datenschutzerklärung"
date: 2019-04-09T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm4e46.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/ftcomm4e46.html?file=datenschutz -->

#### Datenschutzerklärung für die Webseiten:
www.ftcommunity.de inklusive forum.ftcommunity.de und der Weiterleitung www.ft-modellbau.de
sowie von www.ft-datenbank.de
(im folgenden "Internetseiten")
betrieben durch den ftc Modellbau e.V., Auf der Horst 28, 30900 Wedemark, vorstand@ftcommunity.de

Wir erheben, speichern und verarbeiten personenbezogene Daten nur im Rahmen des Rechtes und auf dem Staatsgebiet der Bundesrepublik Deutschland. Im Folgenden erklären wir dies im Detail:

##### Automatische Erhebung von Daten
Jeder Zugriff auf unsere Internetseiten und jeder Abruf einer Datei von unserem Server wird von diesem protokolliert. Das dient einzig internen systembezogenen und statistischen Zwecken.
Folgende Daten werden dabei erhoben:

* IP-Adresse,
* Datum und Uhrzeit der Aufrufs,
* Aufgerufene Seite oder Datei,
* Browser-Typ und - Version sowie
* Betriebssystem-Typ und -Version.

Eine Zuordnung dieser Daten zu einer bestimmten Person ist für uns ohne weiteres nicht möglich. Nach einer anonymen, statistischen Auswertung werden die Daten nach spätestens 7 Tagen automatisch gelöscht.

##### Erhebung weiterer, freiwilliger Daten
Sofern eine freiwillige Registrierung auf unseren Internetseiten erfolgt, werden außerdem folgende personenbezogene Daten gespeichert:

* der selbst gewählte Benutzername,
* die angegebene E-Mail-Adresse und
* ggf. weitere, freiwillig anzugebende Daten.

Diese Daten sind zum Teil öffentlich bzw. von registrierten Benutzern einsehbar. Die Angabe eines Pseudonyms ist möglich, Klarnamen sind nicht erforderlich.

Unsere Internetseiten verwenden Cookies. Ein Cookie ist eine kleine Textdatei, die beim Besuch einer Internetseite auf dem Computer des Benutzers gespeichert wird. Dies dient bei uns ausschließlich dem Komfort des Benutzers ("automatischer Login") und wird zu keinen weiteren Zwecken verwendet. Wenn der Benutzer die Verwendung von Cookies unterbinden möchte, so kann er dies in seinem Browser (Programm, das die Internetseiten darstellt) einstellen.

##### Weitergabe von personenbezogenen Daten
Die von uns erfassten personenbezogenen Daten werden ohne die Zustimmung des jeweils betroffenen Benutzers nicht an Dritte weitergegeben, es sei denn, dass wir dazu gesetzlich oder richterlich verpflichtet wären.

##### Auskunftsrecht
Der Benutzer kann jederzeit Informationen über die bei uns von ihm gespeicherten Daten anfordern. Die Anfrage ist an oben genannte (E-Mail-) Adresse zu richten.

##### Widerruf, Löschung von Daten
Der Benutzer kann seine datenschutzrechtliche Einwilligung jederzeit widerrufen sowie jederzeit seine eingegebenen, personenbezogenen Daten verändern oder Löschen (siehe Profil). Das Löschen eines kompletten Benutzerprofils kann jederzeit auf Anfrage bei uns erfolgen, die Anfrage ist an die oben genannte (E-Mail-)Adresse zu richten.

Stand: August 2019 
