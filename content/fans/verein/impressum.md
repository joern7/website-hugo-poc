---
title: "Impressum"
date: 2019-04-11T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm006c.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/ftcomm006c.html?file=impressum -->

für die Webseite ftcommunity.de inklusive forum.ftcommunity.de, für ft-datenbank.de
und die Weiterleitung www.ft-modellbau.de

#### Server & Hosting, inhaltlich verantwortlich

ftc Modellbau e.V.      
Auf der Horst 28   
30900 Wedemark  
Telefon: 05130/379875  
vorstand@ftcommunity.de  
  
  
Vertretungsberechtiger Vorstand: Ralf Geerken (Vorsitzender), Stefan Falk (Schriftführer), Holger Bernhardt (Kassenwart)  
Eingetragen im Vereinsregister am Amtsgericht Hannover, VR203188

