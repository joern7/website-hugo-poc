---
title: "Freunde und Nachbarn"
weight: 50
date: 2019-04-04T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /links.html
- /links0851.html
- /links2790.html
- /linksf0e2.html
- /links3524.html

---

### fischertechnikclub in den Niederlanden

* [fischertechnikclub.nl](http://www.fischertechnikclub.nl)

### Seiten von Fans

* [der fischertechnik-blog, … damit aus fischertechnikern Ingenieure werden](https://fischertechnikblog.wordpress.com)
* [fischertechnik-Roboter mit Arduino](https://fischertechnik-roboter-mit-arduino.de/)
* [Technikgeschichte mit fischertechnik](https://technikgeschichte-mit-fischertechnik.de/)
* [fischertechnik-AG am Bismarck-Gymnasium Karlsruhe](http://fischertechnik-ag.de/)
* [fischertechnik Museum in der Schweiz](http://www.fischertechnik-museum.ch/)
* [Joachim Jacobi](http://www.jojos-homepage.de)
* [virtuelles fischertechnikmuseum](http://www.cc-c.de/fischertechnik/homepage.htm)
* [Fan Archiv](http://www.ft-fanarchiv.de/)
* [Stef Dijkstra (NL)](http://stef.fthobby.nl/)
* [Fischertechnik in der Schule](https://www.ltam.lu/fischertechnik/)
* [Dave, Jay und Tyrone Gabeler (NL)](http://www.dgabeler.nl/ft/index.htm)
* [fischertechnik & mehr....](https://hobby-endlich.weebly.com/index.html)

### Kommerzielle Angebote

* [Offizielle Website der fischertechnik GmbH](https://www.fischertechnik.de/de-de)
* [Fischerfriendsman Ihr unabhängiger fischertechnik Handel](http://www.fischerfriendsman.de/)
* [fischertechnik Shop santjohanser](https://santjohanser.de/)
* [Shop für Zubehör wie Sensoren, Aktuatoren und Elektronik](https://gundermann-software.de/shop/)
* [fischertechnik designer](http://www.3dprofi.de/de/)
* [fischertechnik Education Baukästen für Schulen | Christiani](https://www.christiani.de/schule/fischertechnik-education/)
* [fischertechnik Webshop NL](https://www.fischertechnikwebshop.com)
* [Entwicklung und Produktion - Knobloch GmbH](https://knobloch-gmbh.de/de/)
