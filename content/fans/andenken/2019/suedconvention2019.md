---
title: "Südconvention 2019"
layout: "event"
date: 2019-06-02T00:00:00
uploadBy:
- "Esther"
location: "im Erlebnismuseum für Fördertechnik in Sinsheim"
termin: 2019-09-21
flyer: "Plakat_verysmall.jpg"
---
Am 21. September 2019 findet die fischertechnik Südconvention erstmals in Sinsheim statt. Veranstalter ist das Erlebnismuseum Fördertechnik in Kooperation mit dem ftc Modellbau e.V.
Neben der Modellausstellung wird ein *buntes Rahmenprogramm für Groß und Klein* mit Action Parcour sowie einen *Baukastenverkauf* und ein *Flohmarkt* angeboten. Gemeinsam mit eurer Hilfe möchten wir außerdem eine *[Vortragsreihe](../vortraege-suedconvention-2019)* organisieren, in der fischertechnik Fans ihr Wissen an andere fischertechnik-Begeisterte (und die, die es noch werden wollen) weitergeben können.
Der Eintritt für das Fördertechnik Museum mit Indoor Spielplatz + den Erlebnistag + Modellausstellung beträgt 5€ pro Person (Kinder unter 3 Jahren sind frei). Für das leibliche Wohl ist gesorgt.
Weitere Informationen zu den Attraktionen des Fördertechnik Museums gibt es [hier](https://www.erlebnismuseum-fördertechnik.de/).

#### Termin:
**21. September 2019 von 10-17 Uhr**

#### Anfahrt:
Die Convention findet statt im:
**Erlebnismuseum Fördertechnik**
**Untere Au 4**
**74889 Sinsheim**
Das Museum befindet sich direkt neben dem Technik Museum Sinsheim. Parkplätze sind vorhanden.

[![Poster Südconvention 2019](../Plakat_verysmall.jpg)](../veranstaltungen/Plakat_large.pdf)  

#### Kuchenspenden:
Im Namen des Museums möchten wir außerdem um Kuchenspenden bitten. Von den Erlösen sollen fischertechnik Sets gekauft werden, mit denen Kinder immer Sonntags im Museum unter Beaufsichtigung experimentieren dürfen.

Wir freuen uns über Kuchenspenden jeglicher Art, gerne auch Sahnetorten, da Kühlmöglichkeiten vor Ort vorhanden sind. Wir bitten darum, uns per Mail an [suedconvention@ftcommunity.de](mailto:suedconvention@ftcommunity.de) kurz mitzuteilen, welche Art von Kuchen gespendet wird. Aussteller können dies darüber hinaus auch im Fragebogen angeben.

## Anmeldung für Aussteller
Wie jedes Jahr erheben wir keine Standgebühr für Aussteller. Aussteller sowie deren Begleitpersonen erhalten zudem kostenfreien Eintritt ins Museum. Wir heißen alle Modellbauer von jung bis alt als Aussteller willkommen! Insbesondere freuen wir uns auch über Großmodelle, eine Ausstellungsfläche im Freien ist vorhanden.

Wenn ihr als Aussteller dabei sein wollt, befolgt bitte das Anmeldeverfahren:
1. Zur *Voranmeldung* sendet ihr uns bitte unverbindlich eine kurze, formlose **E-Mail mit eurem Namen** an [suedconvention@ftcommunity.de](mailto:suedconvention@ftcommunity.de).
2. Wir antworten zeitnah auf eure Mail und wir werden euch bitten, einen **Online-Fragebogen** auszufüllen, sodass wir mit eurem Besuch der Convention besser planen können.
3. Etwa eine Woche vor der Convention werden wir alle Aussteller nochmals per Mail über die wichtigsten organisatorischen Abläufe informieren.

*Hinweis:* Modelle können mit Fahrzeugen bis direkt an die Halle gebracht werden. Der Auf- bzw. Abbau ist auch mehrere Tage vor bzw. nach der Convention möglich (z.B. für Großmodelle oder Gemeinschaftsprojekte).

**Anmeldeschluss** ist der **31. August 2019**. Wir freuen uns auf eure Modelle!

## **NEU:** Anmeldung für Vorträge
Neu im Rahmenprogramm einer Convention ist dieses Jahr unsere Vortragsreihe, die wir mit euren Beiträgen gestalten wollen. Ziel der Vortragsreihe ist die Weitergabe von Wissen  von fischertechnik Fans für fischertechnik Fans. In den Räumlichkeiten des Erlebnismuseum Fördertechnik steht uns hierfür ein Seminarraum mit Beamer zur Verfügung.
Gefragt sind nun spannende Themen wie z.B.:

* Modellbau mit fischertechnik im Allgemeinen
* ein spannendes Modell, über das die Besucher mehr erfahren sollten
* Elektronik: Arduino, ftDuino, TXT, selbstgebaute Elektronikprojekte, etc
* Software: RoboPro, Python, Community Firmware, etc

Die ist nur eine kleine Liste von Themenvorschlägen, gerne könnt ihr weitere Vorschläge einbringen. Wenn ihr Interesse habt, einen Vortrag vor den Besuchern der Convention zu halten, sendet uns bitte eine E-Mail an [suedconvention@ftcommunity.de](mailto:suedconvention@ftcommunity.de). Gemeinsam können wir dann das weitere Vorgehen besprechen und euch bei der Organisation des Vortrages unterstützten. 

## Anmeldung zum Flohmarkt / Tauschbörse
Ihr möchtet euch von (einem Teil) eurer fischertechnik Sammlung trennen? Oder habt ihr beispielsweise in den letzten Monaten an einem Elektronikprojekt gearbeitet und möchtet einige übrige Exemplare an Interessierte aus der Community verkaufen? Oder habt ihr einzigartige 3D-Drucke angefertigt und möchtet diese Raritäten mit der Community teilen?
Für diese und weitere Fällen möchten wir den Flohmarkt bzw. die Tauschbörse einrichten. Wir erheben auch hierfür keine Standgebühr. Ob ihr verkaufen, tauschen, verschenken oder versteigern wollt obliegt eurer Entschiedung.
Bei Fragen oder zur Anmeldung bitte eine Mail an [suedconvention@ftcommunity.de](mailto:suedconvention@ftcommunity.de) schreiben. Für diejenigen, die sich auch als Aussteller anmelden, reicht es aus, dies im Fragebogen anzugeben.

**Ansprechpartner:** *Tilo Rust* *(ClassicMan)* und *David Holtz* *(davidrpf)*

Euer Organisationsteam
Tilo, David, Stefan, Andreas, Torsten


