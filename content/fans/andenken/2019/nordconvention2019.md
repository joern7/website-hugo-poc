---
title: "Nordconvention 2019"
layout: "event"
date: 2019-04-04T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm8a9b.html
imported:
- "2019"
location: "im Schulzentrum in Mellendorf/ Wedemark"
termin: 2019-04-27
flyer: "a6_Postkarte-05.jpg"
---
<!-- http://ftcommunity.de/ftcomm8a9b.html?file=nordconvention -->
***Wedemark / Mellendorf bei Hannover***

Die Nordconvention 2019 findet im Schulzentrum in Mellendorf/ Wedemark statt.

Der Ausrichter ist der Verein zur Förderung des Richard-Brandt-Heimatmuseums Wedemark e.V.
in Kooperation mit dem Verein ftc Modellbau e.V. .

Das genaue Datum ist ***Samstag, 27.04.2019 von 10-17 Uhr***

#### Aussteller-Anmeldung
Eine Standgebühr oder Eintritt wird nicht erhoben, Spenden an den Ausrichter werden aber gerne entgegengenommen. Der Aufbau kann am Freitag, den 26.04. ab 17:00 Uhr und am Samstag ab 7:00 Uhr beginnen. Die Voranmeldung erfolgt per E-Mail. Wenn ihr dabei sein wollt, dann schreibt eine E-Mail mit folgenden Informationen:

* Name
* Nickname
* Großmodell geplant? (Höhe oder Breite mehr als 3m, Tiefe mehr als 1,5m)

an [Ralf Geerken](mailto:ralf_geerken@yahoo.de). Ihr bekommt ca 4 Wochen vor der Ausstellung einen detaillierten Fragebogen, um genauere Daten (z.B. Abmessungen) anzugeben. 
Anmeldeschluss ist **der 13.04.2019** !

#### Anreise
Die Convention findet statt im
**Forum Campus W Schulzentrum Mellendorf**  
Fritz-Sennheiser-Platz 2-3  
30900 Wedemark  

Wir freuen uns auf eure Anmeldung.

Holger Bernhardt und Ralf Geerken

[Postkarte Nordconvention 2019 A6](../a6_Postkarte-05.jpg)  
[Poster Nordconvention 2019 A4](../Plakat_2019-A4_300dpi.jpg)  
[Poster Nordconvention 2019 A2](../Plakat_2019-A2_300dpi.jpg)  

[Video von der Nordconvention 2017](https://www.youtube.com/watch?v=a0LDKPaTB1E&amp;t=917s)
