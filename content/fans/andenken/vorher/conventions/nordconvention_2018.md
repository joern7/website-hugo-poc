---
hidden: true
layout: "file"
title: "2018 Nord-Convention (Plakat)"
date: 2018-03-11T00:00:00
file: "nordconvention_2018.pdf"
konstrukteure:
- "Holger Bernhardt"
- "Ralf Geerken"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
legacy_id:
- /data/downloads/conventionplakate/a3_plakat.pdf
- /data/downloads/conventionplakate/a2_plakat.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/conventionplakate/a3_plakat.pdf -->
Plakat IdeA Nordconvention 2018
