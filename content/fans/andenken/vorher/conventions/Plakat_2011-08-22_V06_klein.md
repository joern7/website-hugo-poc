---
hidden: true
layout: "file"
title: "2011 Convention"
date: 2019-04-13T00:00:00
publishDate: 2019-04-21T00:00:00
file: "Plakat_2011-08-22_V06_klein.pdf"
konstrukteure:
- "Knobloch GmbH"
uploadBy:
- "Zapfenkiller"
license: "unknown"
---
Convention 2011
