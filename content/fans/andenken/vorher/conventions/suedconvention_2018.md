---
hidden: true
layout: "file"
title: "2018 Süd-Convention"
date: 2018-07-22T00:00:00
file: "suedconvention_2018.pdf"
konstrukteure:
- "Martin W."
uploadBy:
- "Masked"
license: "unknown"
legacy_id:
- /data/downloads/conventionplakate/plakat2018_a4.pdf
- /data/downloads/conventionplakate/plakat2018_a3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/conventionplakate/plakat2018_a4.pdf -->
Plakat zur Convention Dreieich 2018
