---
hidden: true
layout: "file"
title: "2018 Nord-Convention (Flyer)"
date: 2018-03-11T00:00:00
file: "nordconvention_2018_a6.pdf"
konstrukteure:
- "Holger Bernhardt"
- "Ralf Geerken"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
legacy_id:
- /data/downloads/conventionplakate/a6_flyerideanordconvention2018.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/conventionplakate/a6_flyerideanordconvention2018.pdf -->
Flyer IdeA Nordconvention 2018
