---
title: "Adventskalender 2012"
weight: 45
date: 2019-07-13T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /adventskalender/2012/index.html

---
<!-- https://ftcommunity.de/adventskalender/2012/index.html -->
 
Im Jahr 2012 gab es diesen Adventskalender, dessen Inhalt wir hier zur Verfügung stellen.

![Adventskalender 2012](hg_advent.png)

"Öffne jeden Tag ein Türchen, es gibt spannendes zu entdecken.
Das Team der ft:c wünscht allen eine schöne Adventszeit!

Wir danken allen Sponsoren und Helfern für Ihre freundliche Unterstützung!"

| | |
| --- | --- |
| 1. Dezember | Am ersten Tag des Adventskalenders gibt es gleich eine [Bauanleitung](Adventskranz.pdf). ![Vorschau 1](vorschau_1.png)|
| 2. Dezember | Verlosung eines Carts- und eines Aircraft-Kasten ![Aircraft](aircraft.png) ![Carts](carts.png)|
| 3. Dezember | Heute mal ein bisschen was für unsere kleinen ft Freunde: eine [Malvorlage](Malvorlage_Airbus.pdf). Danke an Marius Seider!|
| 4. Dezember | Und wieder etwas [zum bauen](Mini_Stapler.pdf). Danke an Jan Werner! ![Vorschau 2](vorschau_2.png) |
| 5. Dezember | Ein neuer Tag, eine neue [Bauanleitung](Konvektionskühler.pdf). Danke an Ingo Herschel! ![Vorschau 3](vorschau_3.png) |
| 6. Dezember | Nikolaustag: Sonderverlosung eines Cars&Drive-Kastens ![Cars&Drive](carsanddrives.png) |
| 7. Dezember | Kennt ihr schon die kleinste Kugelbahn der Welt? Nein? Dann nichts wie ran an ft und bauen. Hier [die Anleitung](Kleinste_Kugelbahn_der_Welt.pdf) Danke an Bernhard Lehner! ![Vorschau 4](vorschau_4.png) |
| 8. Dezember | Braucht Ihr noch einen Kerzenständer? Wie wäre es dann mit einem aus fischertechnik? Hier gibts die [Anleitung](Kerzenständer.pdf)! Danke an Marcel Endlich! ![Vorschau 5](vorschau_5.png) |
| 9. Dezember | Verlosung eines Carts- und eines Aircraft-Kasten ![Aircraft](aircraft.png) ![Carts](carts.png)|
| 10. Dezember | Heute gibt es wieder eine [Malvorlage](Malvorlagen.pdf) für unsere kleinen ft-Fans. Danke an Marius Seider!
| 11. Dezember | Und wieder etwas zum bauen. Diesmal die Anleitung für den Micro Truck von Thomas004. Hier gibts die [Anleitung](Microtruck.pdf). Danke an Thomas004 und Ingo Herschel! ![Vorschau 6](vorschau_6.png) |
| 12. Dezember | Heute gibts mal was nützliches [zum selber bauen](Brillenleuchte.pdf), allerdings mit der Einschränkung dass es nur Brillenträger nutzen können. Danke an Bernhard Lehner! ![Vorschau 12](vorschau_12.png) |
| 13. Dezember | Heute machen wir mal einen Ausflug in die Vergangenheit. Eine [Anleitung](Wagenheber.pdf) aus dem Hobbybuch 1-1 aber mit aktuellen ft-Teilen. Danke an Andreas Gürten! ![Vorschau 7](vorschau_7.png)|
| 14. Dezember | Heute gibts mal wieder ein wenig was Weihnachtliches. Dazu haben wir das alte [Clubheft](Weihnachtsbaum.pdf) 4/1979 rausgeholt. ![Vorschau 11](vorschau_11.png) |
| 15. Dezember | [Und heute gibts was zum rätseln.](Großrätsel.pdf) Danke an Thomas Habig!|
| 16. Dezember | Verlosung eines Carts- und eines Aircraft-Kasten ![Aircraft](aircraft.png) ![Carts](carts.png) |
| 17. Dezember | [Und noch ein wenig Rätselspaß!](Magische_Spirale.pdf) Danke an Thomas Habig! |
| 18. Dezember | Jeder kennt das leidige Problem des Einstaubens unserer Modelle, vor allem dann wenn sie bei schrittweiser Fortentwicklung oder einem etappenweisen Aufbau und natürlich für Ausstellungen längere Zeit aufgebaut bleiben. Ingo Herschel hat hier die [Lösung](Klebevorrichtung.pdf). Danke an Ingo Herschel! ![Vorschau 10](vorschau_10.png)|
| 19. Dezember | Und wieder eine Reise in die Vergangenheit. Die Freaks unter uns erinnern sich sicherlich noch an einen der ersten ft Computing Baukästen: Der Trainingsroboter. Hier die [Bauanleitung](Trainingsroboter.pdf) mit einer Modifizierung, damit man den Trainingsroboter ohne die nicht mehr verfügbaren Lichtschranken bauen kann. Danke an Andreas Gürten! ![Vorschau 9](vorschau_9.png) |
| 20. Dezember | Für unsere kleinen ft-Fans gibt wieder ein bisschen was zu [malen](Malvorlagen.pdf)! Danke an Marius Seider!|
| 21. Dezember | [Und weiter gehts mit 2 Rätseln](Querfeldein_Zwickmühle.pdf) am heutigen Tage! Danke an Thomas Habig!|
| 22. Dezember | Und wieder ein nettes [Modell zum Nachbauen](Malmaschine.pdf): die Malmaschine. Danke an Frank Jakob und Andreas Gürten! ![Vorschau 8](vorschau_8.png)|
| 23. Dezember | Verlosung von zwei Carts-Kästen ![Carts](carts.png) |
| 24. Dezember | Verlosung einer ftDesigner-Lizenz ![ftDesigner](ftdesigner.jpg) |




