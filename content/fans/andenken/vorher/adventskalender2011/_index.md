---
title: "Adventskalender 2011"
weight: 50
date: 2019-07-19T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm0ad4.html

---
<!-- https://ftcommunity.de/ftcomm0ad4.html?file=Adventskalender2011 -->
 
Für alle, die den Adventskalender 2011 verpasst haben, hier die Inhalte.

| | |
| --- | --- |
| 1. Dezember | [Rätsel 1](raetsel_1.pdf)|
| 2. Dezember | [Bauanleitung: Weihnachtspyramide - Teil 1](weihnachtspyramide_teil_1.pdf)|
| 3. Dezember | [Bauanleitung: Weihnachtspyramide - Teil 2](weihnachtspyramide_teil_2.pdf)|
| 4. Dezember | Verlosung fischertechnik Bikes Baukasten|
| 5. Dezember | [Rätsel 2](raetsel_2.pdf) |
| 6. Dezember | Verlosung 1x fischertechnik Bikes Baukasten, 2x fischertechnik Carts Baukasten  |
| 7. Dezember | [Rätsel 3](raetsel_3.pdf) |
| 8. Dezember | [Bauanleitung: Swibbogen](swibbogen.pdf)  |
| 9. Dezember | [Bauanleitung: Renntier mit Schlitten](renntier_schlitten.pdf)|
| 10. Dezember | [Rätsel 4](raetsel_4.pdf) |
| 11. Dezember | Verlosung fischertechnik Carts Baukasten |
| 12. Dezember | [Rätsel 5](raetsel_5.pdf) |
| 13. Dezember | [Bauanleitung: Kugelbahnaufzug](kugelbahnaufzug.pdf) |
| 14. Dezember | [Bauanleitung: Mini Truck](mini_truck.pdf) |
| 15. Dezember | [Bauanleitung: Anhänger für Mini Truck](anhaenger_mini_truck.pdf) |
| 16. Dezember | [Rätsel 6](raetsel_6.pdf) |
| 17. Dezember | [Bauanleitung: Kreuztisch](kreuztisch.pdf) + [ft-Designerdatei](kreuztisch.ftm)|
| 18. Dezember | Verlosung fischertechnik Bikes Baukasten |
| 19. Dezember | [Rätsel 7](raetsel_7.pdf) |
| 20. Dezember | [Rätsel 8](raetsel_8.pdf) |
| 21. Dezember | [Bauanleitung: Smart](smart.pdf) |
| 22. Dezember | [Rätsel 9](raetsel_9.pdf) |
| 23. Dezember | [Deckblatt ft:pedia 4/2011](deckblatt_ftpedia_4_2011.png) |
| 24. Dezember | Verlosung: fischertechnik Dynamic Baukasten |

An dieser Stelle noch mal vielen Dank an fischertechnik für das Sponsorn der Baukästen für die Verlosungen und natürlich an alle die Helfer für die Inhalte.

Hier noch die Auflösungen zu den Rätseln zum Herunterladen:
