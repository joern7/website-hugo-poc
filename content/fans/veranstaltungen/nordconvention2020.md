---
title: Nordconvention 2020
layout: "event"
date: 2019-11-06T15:45:52+01:00
uploadBy: EstherM
location: "abgesagt!!!"
termin: 2020-03-07T00:00:00+01:00
flyer: a6_Postkarte2020-01-Seite001.jpg
---
Die Nordconvention 2020 findet am

**Samstag, 7. März 2020**

im Schulzentrum in Mellendorf/Wedemark bei Hannover als 

**IdeenAusstellung mit fischertechnik** 

statt.

**Nach einem Corona-Fall in der Region Hannover muss die Nordconvention leider kurzfristig abgesagt werden.**

--------

Der Ausrichter ist der **Verein zur Förderung des Richard-Brandt-Heimatmuseums Wedemark e.V.** in Kooperation mit dem **ftc Modellbau e.V.**

[![Poster Nordconvention 2020](../a6_Postkarte2020-01 -Seite001.jpg)](../a6_Postkarte2020-01-Seite001.jpg)

----

## Aussteller-Anmeldung

Eine Standgebühr oder Eintritt wird nicht erhoben, Spenden an den Ausrichter werden aber gerne entgegengenommen. 

Der Aufbau kann am Freitag, den 6. März. ab 17:00 Uhr und am Samstag ab 7:00 Uhr beginnen. Wenn ihr dabei sein wollt, dann schreibt eine E-Mail mit folgenden Informationen:

* Name
* Nickname
* Großmodell geplant? (Höhe oder Breite mehr als 3 m, Tiefe mehr als 1,5 m)

an [nordconvention@ftcommunity.de](mailto:nordconvention@ftcommunity.de).

Ihr bekommt ca. 2 Wochen vor der Ausstellung einen detaillierten Fragebogen, um genauere Daten (z. B. benötigter Platz, Abmessungen) anzugeben.

**Anmeldeschluss ist der 23.02.2020**

----

## Anreise
 
Die Convention findet statt im Forum Campus W Schulzentrum Mellendorf  
Fritz-Sennheiser-Platz 2-3  
30900 Wedemark

Wir freuen uns auf eure Anmeldung.  
Holger Bernhardt und Ralf Geerken
