---
title: "Veranstaltungen"
weight: 1
date: 2019-04-04T00:00:00
uploadBy:
- "Esther"
---

Die fischertechnik-Community trifft sich regelmäßig zu Stammtischen und
Conventions sowie auf Modellbau-Messen und Maker Faires. Alle sind
herzlich eingeladen.

### Termine 

19\. Januar 2020 [Große Modellschau in Münster-Hiltrup](modellschau2020)

29\. Februar - 1. März 2020 [Modellbau Schleswig-Holstein Neumünster](https://www.messeninfo.de/Modellbau-Schleswig-Holstein-M2534/Neumuenster.html)

7\. März 2020 [Nordconvention](nordconvention2020/): **abgesagt!!!**

[![Plakat Modellschau Münster](Plakat-Fischertechnik-MS-2020.png)](Plakat-Fischertechnik-MS-2020.png)
[![Poster Nordconvention 2020](a6_Postkarte2020-01-Seite001.jpg)](a6_Postkarte2020-01-Seite001.jpg)


### Veranstaltungsübersicht der fischertechnik GmbH

[fischertechnik GmbH](https://www.fischertechnik.de/de-de/ueber-uns/termine)


