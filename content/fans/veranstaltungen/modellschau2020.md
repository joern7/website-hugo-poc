---
title: Modellschau 2020
layout: "event"
date: 2019-11-06T15:45:52+01:00
uploadBy: EstherM
location: "im Kardinal-von-Galen Gymnasium Münster-Hiltrup"
termin: 2020-01-19T00:00:00+01:00
flyer: Plakat-Fischertechnik-MS-2020.png
---
fischertechnik - Große Modellschau

**Sonntag, 19. Januar 2020**  
10:00 - 17:00 Uhr  

Aula des Kardinal-von-Galen Gymnasiums  
Zum Roten Berge 25  
48164 Münster-Hiltrup  

**Eintritt frei**

Lötkurse für Kinder  
Basteln von elektronischen Schaltungen  

Waffeln, Würstchen, Spieleecke  

Personen, Schulen, usw., die auf der Modellschau ausstellen möchten, können sich gerne bei Roland Keßelmann melden (kesselmann@bistum-muenster.de).


[![Plakat Modellschau 2020](../Plakat-Fischertechnik-MS-2020.png)](../Plakat-Fischertechnik-MS-2020.pdf)


