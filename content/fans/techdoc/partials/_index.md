﻿---
title: "Partials"
layout: "techdoc"
weight: 50
stand: "25. Oktober 2019"
---
Einige Seitenfunktionen sind so gebaut, dass sie an verschiedenen Stellen
wiederverwendet werden können. Das heißt bei Hugo "Partial".

Diese Partials (die, die wir selbst geschrieben 
oder grundlegend überarbeitet haben)
werden in dieser Ecke dokumentiert. Sie liegen im Verzeichnis
`/layouts/partials/` oder in einem Unterverzeichnis dort.
