---
title: "Ex-Wiki-Datei Seitenansicht"
topic: "wiki.html"
layout: "techdoc"
weight: 40
stand: "8. Juni 2020"
---


### Wiki-Seitenansicht (wiki.html)

Die Darstellung der Seiten mit dem Layout `layout: wiki` ist sehr ähnlich zu 
der Darstellung der Download-Seiten, die im [Kapitel über _file.html_](../file-html) 
beschrieben ist. Daher werden hier nur die Unterschiede dargestellt.
Das Format der "Wiki-Seiten" ist [hier](../wiki) beschrieben.

Der Beginn der Seite ist genau wie dort beschrieben.
Auch die Prüfungen des Frontmatters auf Vollständigkeit und korrekte Darstellung
sind wie [dort](../file-html/#prüfung-auf-pflichtangaben-und-korrekte-listen) beschrieben. 
Nur die Abfrage, ob der Name des Download-Files angegenben ist, entfällt hier naturgemäß.

Auf der fertigen Seite gibt es ebenfalls einen "Haftungsausschluss".
Ein Bestandteil dabei ist die Möglichkeit per E-Mail auf einen inhaltlichen
Fehler der dargestellten Seite aufmerksam zu machen.
Als puren Luxus für den Nutzer, und zur Unterstützung der Admins, gibt es
einen vordefinierten Text, der den Link zur bemängelten Seite enthält.
Dieser Text ist im Zusammenbau nicht ganz trivial und wird daher bereits
vorab erzeugt.
Das macht den späteren Code für den Seitenaufbau übersichtlicher.
````
   {{/* --- Vorgeplänkel --- */}}
   {{/* --- Prologue --- */}}

   {{ $email_subject := "Da stimmt was nicht!" }}
   {{ $scratch := newScratch }}
   {{ printf "Hallo ihr Lieben,\n\n" | $scratch.Set "email_body" }}
   {{ printf "ich möchte Euch auf ein Problem mit der ftc-Seite hinweisen. " | $scratch.Add "email_body" }}
   {{ printf "Betroffen ist die Seite:\n%s\n" .Permalink | $scratch.Add "email_body" }}
   {{ printf "\n<Problembeschreibung>" | $scratch.Add "email_body" }}
````

Mittels `$scratch := newScratch` wird eine spezielle Variable für den späteren
Text der eMail aufgemacht.
`$scratch.Set` weist direkt den ersten Textteil zu, per `$scratch.Add` werden
weitere Teile angehängt.

In dem String `"Betroffen ist die Seite:\n%s\n"` dient das `%s` als Platzhalter
für den folgenden Parameter `.Permalink`. 
In diesem Fall wird ein Link auf die entsprechende Seite geliefert.

Das `|` leitet die Ausgabe
einer Anweisung (hier von `printf`) an die folgende Anweisung (hier
`$scratch.Add`) weiter.

Ein Klick auf den späteren Link öffnet das e-mail-Programm des Besuchers und
bietet die Vorbelegung in einer neu zu schreibenden e-mail an.
Das sieht dann so aus (ungefähr, der Link variiert mit dem Seitennamen):

````
Hallo ihr Lieben,

ich möchte Euch auf ein Problem mit der ftc-Seite hinweisen. Betroffen ist die Seite:
/knowhow/elektronik/selbstbau/ex_wiki_rc5_codes_fr_ir_contol_set_30344/

<Problembeschreibung>
````

Jetzt wird es produktiv.
Es folgt Code, der den Seitenaufbau im Browser steuert und sichtbare
Inhalte erzeugt.
````
   {{/* --- HTML-Seite zusammenbauen --- */}}
   {{/* --- Assemble the HTML site --- */}}

   <div class="padding highlightable">
      {{ partial "topbar.html" . }}

      <div id="body-inner">
         <h2>{{.Title}}</h2>

         <p>
            {{ .Content }}
         </p>
````
Zunächst gibt es den Start der Seite und die einheitliche Kopfzeile.
Für die Kopfzeile gibt es so eine Art "Unterprogramm", das im Framework
an anderer Stelle vordefiniert ist.
Das ist die Zeile `{{ partial "topbar.html" . }}`.
Danach kommt sofort der Seitentitel in Form von `<h1>{{.Title}}</h1>`.
Das HTML-tag `<h2>` startet die Überschrift, `</h2>` beendet sie.
Wie sie genau aussieht, ist irgendwo im Framework 'versteckt' - global für
alle Seiten.
Der Inhalt der Seite (also der Text des Wiki-Eintrags) wird direkt aus der
Datei geholt.
Das erledigt `.Content`, eingerahmt von den zwei HTML-tags für einen eigenen
Absatz (`<p>`und `</p>`).
Dieser `.Content` ist der Markdownteil unterhalb des Frontmatters.

Als nächstes erfährt der Besucher, wer den Inhalt erstellt und hochgeladen hat.
Der Code wurde einfach vom 'file.html' übernommen und ist dort [dokumentiert](../file-html/#angabe-von-autoren-uploader-und-lizenz).
Bei Bedarf muss hier die Angabe einer Lizenz noch nachgetragen werden.

Es folgt nun ein weiterer Absatz in kleiner Schrift.
````
   <p>
      <small>
````
Hier erscheint ein Text, der mit einem fett gesetzten 'Hinweis:'
beginnt (das ist der Bereich zwischen den HTML-tags `<b>`und `</b>`).
````
   <b>Hinweis:</b>
   Wir vertrauen auf die Sachkunde und Sorgfalt unserer Nutzer.
   Trotzdem könnten sich Fehler eingeschlichen haben.
   Eine Haftung für die Richtigkeit der Inhalte können wir nicht
   übernehmen.
   Falls Du einen Fehler findest, kontaktiere bitte
````
Ein 'uns' beendet den Satz den Satz.
Das 'uns' ist mit einem Hyperlink hinterlegt.
Hier wird der Versand einer e-mail an das Betreuungsteam
vorbereitet.
````
         <a href="mailto:{{$wir_email}}?subject={{$email_subject}}&body={{$scratch.Get "email_body"}}">uns</a>.
````
`$wir_email` gibt den Adressaten an, `$email_subject` liefert den Betreff und
`$scratch.Get "email_body"` enthält dann die Nachricht.
Das ist der lange Text aus dem Vorgeplänkel oben "_Hallo ihr Lieben, ..._".

Der Absatz mit dem Hinweistext wird beendet (Kleinschrift aus und Absatzende).
````
      </small>
   </p>
````

Die **Navigation** ist bei den Download-Files [beschrieben](../file-html/#navigation).

