---
title: "Inhalte für die ft:pedia"
topic: "content/ftpedia"
weight: 10
stand: "6. Juni 2020"
---
Die ft:pedia ist also der Bereich, in dem die gleichnamige "Zeitschrift" zum
Herunterladen angeboten wird.
Die Sortierung erfolgt nach Jahrgang, der jüngste Jahrgang ist immer oben
(im Menü).
Pro Jahrgang gibt es 4 Ausgaben, bei einigen Ausgaben gibt es zusätzliche
Downloads.
Im Gegensatz zur alten ftc-Website werden die Downloads zu den Heften nicht
irgendwo in einer Download-Kategorie angeboten, sondern gleich in der Nähe
des zugehörigen Heftes.

Die Verwaltung der Inhalte wird in den folgenden Abschnitten beschrieben.

Aus taktischen Gründen startet die Beschreibung mit dem Teaser, auch
wenn man in der Navigation zunächst auf die Gesamtübersicht stoßen wird.


## Inhalt

<p><a href="#teaser">Teaser</a></p>
<p><a href="#einzelheft">Ein einzelnes Heft</a></p>
<p><a href="#heftbegleitende-downloads">Heftbegleitende Downloads</a></p>
<p><a href="#jahrgang">Jahrgang (4 Hefte)</a></p>
<p><a href="#jahrgangsbersicht">Jahrgangsübersicht (alle Hefte nach Jahrgang)</a></p>
<p><a href="#gesamtinhaltsverzeichnis">Gesamtinhaltsverzeichnis</a></p>
<p><a href="#übersicht-extras-zum-heft">Übersicht über die 'Extras zum Heft'</a></p>

## Teaser

Prinzipiell ist der Teaser ein Einzelheft, allerdings noch ohne die pdf-Datei
der Ausgabe. Und in der Historie der Ausgabe kommt er, mit einer Lebensdauer
von wenigen Tagen, vor dem Heft.

Der Teaser ist sozusagen der Platzhalter für die eigentliche Ausgabe und
eröffnet spätestens den Ordner dafür, beispielsweise
`content/ftpedia/2018/2018-1`.

In diesem Ordner befinden sich mindestens zwei Dateien:

* Teaser-Titelbild (Thumbnail, heißt **immer** 'titelseite.png')
* _index.md (**mit dem Unterstrich davor**, also nicht 'index.md'!)

Die Datei `_index.md` im Ordner `/2018-1` (generell `/yyyy-n`) enthält nun alle
nötigen Angaben, um aus dem Ordnerinhalt einen Teaser zu bauen.

Durch die Ablage unterhalb `/content/ftpedia` ist bereits geklärt, um was für
ein Objekt es sich prinzipiell handelt.
Weitere Angaben im Frontmatter können so auf ein Minimum reduziert werden.

Die Datei `_index.md` besteht für den Teaser immer aus diesem Frontmatter:
<table>
   <tr>
      <th>Codezeile</th>
      <th>Angabe</th>
      <th>Erläuterung</th>
   </tr>
   <tr>
      <td>`---`</td>
      <td></td>
      <td>Leitet den Frontmatter-Block ein</td>
   </tr>
   <tr>
      <td><b>layout</b>: "teaser"</td>
      <td><b>Pflicht</b></td>
      <td>Das Layout für einen ft:pedia Teaser heißt "teaser".</td>
   </tr>
   <tr>
      <td><b>title</b>: "1 / 2018"</td>
      <td><b>Pflicht</b></td>
      <td>Der Titel, der als Überschrift oben erscheint</td>
   </tr>
   <tr>
      <td><b>launchDate</b>: 2018-03-31T00:00:00+0100</td>
      <td><b>Pflicht</b></td>
      <td>
         Das Datum, zu dem die Ausgabe erscheinen wird. Natürlich liegt es ein
         paar Tage in der Zukunft.
         <br/>Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm
         <br/>Das ist manuell einzutragen und bitte die Zeitzone (`+0100` MEZ
         bzw. `+0200` MESZ nicht vergessen)!
      </td>
   </tr>
   <tr>
      <td><b>date</b>: 2018-03-25T00:00:00+0100</td>
      <td><b>Empfohlen</b></td>
      <td>
         Das Datum des uploads - wird automatisch erzeugt.
         <br/>Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm<br/>
         Beim manuellem Erstellen neuer Seiten bitte die Zeitzone (`+0100` MEZ
         bzw. `+0200` MESZ nicht vergessen)!
         Es wird empfohlen vorhandene Archetypes zu nutzen. Hugo kümmert sich
         dann um das korrekte Datum mitsamt der Zeitzone. Für legacy-Dateien,
         die älter als 2 h sind, ist die Angabe der Zeitzone unerheblich und
         kann entfallen. Für alles Aktuelle soll sie rein; sonst wird die Seite
         von Hugo nicht (zur richtigen Zeit) gebaut.
      </td>
   </tr>
   <tr>
      <td><b>uploadBy</b>:<br/>- "ft:pedia-Redaktion"</td>
      <td><b>Empfohlen</b></td>
      <td>Wer hat die Datei hochgeladen?</td>
   </tr>
   <tr>
      <td>`---`</td>
      <td></td>
      <td>Schließt den Frontmatter-Block ab.</td>
   </tr>
</table>

Ein fiktiver Teaser sieht also beispielhaft so aus:
````md
---
hidden: true
layout: "teaser"
title: "3 / 2021"
launchDate: 2021-09-25T00:00:00+0100
date: 2021-09-20T00:00:00+0100
uploadBy:
- "ft:pedia-Redaktion"
---
````

Zur Werbung für das Heft wird ein thumbnail der kommenden Titelseite benötigt.
Dieses **Teaser**-Bild muss als `.png` Datei in der **Breite 180 px** und
**Höhe 255 px** vorliegen.
_Die Größe muss exakt der des späteren Titelbildes entsprechen. In der nasS
gab es zwei leicht verschiedene Größen für die eigentliche
ft:pedia-Auswahlseite sowie das Seitenmenu. In der modernen Version verwenden
wir eine einheitliche Größe. Siehe auch unten beim Einzelheft._
Die Datei mit dem Thumbnail wird <u>ausschließlich</u> unter dem Namen
`titelseite.png` in den entsprechenden Ordner gespeichert.

Für das Anlegen der `_index.md` ist ein Archetyp behilflich. Siehe
[Archetype "teaser.md"](../archetypes/#archetype-teaser-md).

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

## Einzelheft

Für ein Einzelheft gibt es genau einen Ordner.
Er beschreibt mit seinem Namen sinnigerweise schon, welche Ausgabe sich dort
befindet, beispielsweise `content/ftpedia/2018/2018-1`.

In diesem Ordner befinden sich mindestens drei Dateien.

* Heft (z. B. 'ftpedia-1-2018.pdf')
* Titelbild (Thumbnail, heißt **immer** 'titelseite.png')
* _index.md (**mit dem Unterstrich davor**, also nicht 'index.md'!)

Die Datei `_index.md` im Ordner `/2018-1` (generell `/yyyy-n`) enthält nun alle
nötigen Angaben, um aus dem Ordnerinhalt eine schmucke Ansicht zu bauen.

Durch die Ablage unterhalb `/content/ftpedia` ist bereits geklärt, um was für
ein Objekt es sich prinzipiell handelt.
Weitere Angaben im Frontmatter können so auf ein Minimum reduziert werden.

Die Datei `_index.md` beginnt immer mit dem Frontmatter:
<table>
   <tr>
      <th>Codezeile</th>
      <th>Angabe</th>
      <th>Erläuterung</th>
   </tr>
   <tr>
      <td>`---`</td>
      <td></td>
      <td>Leitet den Frontmatter-Block ein</td>
   </tr>
   <tr>
      <td><b>hidden</b>: true</td>
      <td><b>Empfohlen</b></td>
      <td>
         Die Seite wird im Menü nicht angezeigt.
         Und falls doch mal jemand auf dieser Seite landet, kümmert sich
         `/layouts/ftpedia/list.html` um eine anständige Anzeige der
         angebotenen Information.
      </td>
   </tr>
   <tr>
      <td><b>layout</b>: "issue"</td>
      <td><b>Pflicht</b></td>
      <td>Das Layout für eine ft:pedia Einzelausgabe heißt "issue".</td>
   </tr>
   <tr>
      <td><b>title</b>: "1 / 2018"</td>
      <td><b>Pflicht</b></td>
      <td>Der Titel, der als Überschrift oben erscheint</td>
   </tr>
   <tr>
      <td><b>file</b>: "ftpedia-2018-1.pdf"</td>
      <td><b>Pflicht</b></td>
      <td>
         Name der Datei mit dem Heftinhalt.
      </td>
   </tr>
   <tr>
      <td><b>publishDate</b>: 2018-03-31T00:00:00+0100</td>
      <td><b>Pflicht</b></td>
      <td>
         Vorgabe des Veröffentlichungsdatums (in der Zukunft).
         Erlaubt es dem Redakteur, die Datei vorab einzustellen und
         zeitgesteuert zu veröffentlichen.
         <br/>Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm
         (<i>Gilt das dann wegen `_index.md` auch für den restlichen Ordnerinhalt? 
         Ja!</i>)
         <br/>Und bitte an die Zeitzone denken (siehe vorherigen Eintrag),
         wenn das Datum manuell eingetragen wird. Hugo kann (noch) nicht
         selbst ermitteln wann es so weit sein wird ...
         Aus diesem Feld wird auch die Angabe 'Erschienen am' gespeist.
      </td>
   </tr>
   <tr>
      <td><b>date</b>: 2018-03-31T00:00:00+0100</td>
      <td><b>Empfohlen</b></td>
      <td>
         Das Datum des uploads - wird automatisch erzeugt.
         <br/>Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm<br/>
         Beim manuellem Erstellen neuer Seiten bitte die Zeitzone (`+0100` MEZ
         bzw. `+0200` MESZ nicht vergessen)!
         Es wird empfohlen, vorhandene Archetypes zu nutzen. Hugo kümmert sich
         dann um das korrekte Datum mitsamt der Zeitzone. Für legacy-Dateien,
         die älter als 2 h sind, ist die Angabe der Zeitzone unerheblich und
         kann entfallen. Für alles Aktuelle soll sie rein; sonst wird die Seite
         von Hugo nicht (zur richtigen Zeit) gebaut.
      </td>
   </tr>
   <tr>
      <td><b>uploadBy</b>:<br/>- "wer war es?"</td>
      <td><b>Empfohlen</b></td>
      <td>
         Macht das hier Sinn?
         Ich denke ja, um zu sehen, wer das Heft eingestellt hat.
         Aber zwingend muss es nicht sein, oder?
         Ist halt noch unklar wegen "wie kommt das Heft in die ftc".
       </td>
   </tr>
   <tr>
      <td><b>legacy_id:</b></td>
      <td>Option</td>
      <td>
         Falls jemand mit einem "alten" Link herkommt, sorgt dieser Eintrag
         für ein Download-Erlebnis anstelle HTTP-404.
         Falls `imported` gesetzt ist, muss `legacy_id` auch angegeben sein
         (was nicht weiter schwer fällt, weil es eine gibt).
         Für die neueren Hefte ab 2019 entfällt dieser Eintrag,
         wenn nicht später mal die Struktur der Webseite wieder geändert wird.
      </td>
   </tr>
   <tr>
      <td><b>imported</b>:<br/>
         - "2019"
      </td>
      <td>Option</td>
      <td>
         Bis incl. 2018 kommen alle Dateien aus der alten ftc.
         Da macht es durchaus Sinn, `imported:` zu setzen.
         Für neu eingestellte Ausgaben entfällt dieser Schlüssel ersatzlos.
      </td>
   </tr>
   <tr>
      <td>`---`</td>
      <td></td>
      <td>Schließt den Frontmatter-Block ab.</td>
   </tr>
</table>

Das Frontmatter sieht also für die Ausgabe 1 / 2018 in deren `_index.md` so
aus:
````md
---
hidden: true
layout: "issue"
title: "1 / 2018"
file: "ftpedia-2018-1.pdf"
publishDate: 2018-03-31T00:00:00
date: 2018-03-31T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2018-1.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-1.pdf -->
````
Im HTML-Kommentar ist nochmal der vollständige alte Link angegeben, man kann
ja nie wissen.

Eine neue Ausgabe hat entsprechend weniger Einträge, zum Beispiel so:
````md
---
hidden: true
layout: "issue"
title: "3 / 2021"
file: "ftpedia-2021-3.pdf"
publishDate: 2021-09-25T00:00:00+0200
date: 2021-09-19T12:37:29+0200
uploadBy:
- "ft:pedia-Redaktion"
---
````

Zusätzlich zum Heft wird für dessen Präsentation ein Thumbnail der Titelseite
benötigt.
Dieses **Titelbild** muss als `.png` Datei in der **Breite 180 px** und **Höhe 255 px**
vorliegen.
_Die Größe ist historisch festgelegt. Eine Änderung kann nur erfolgen, wenn
alle älteren Titelbilder in der neuen Größe generiert werden!_
Die Datei mit dem Thumbnail wird <u>ausschließlich</u> unter dem Namen
`titelseite.png` in den entsprechenden Ordner gespeichert.

Der [Archetyp "ftpedia.md"](../archetypes/#archetype-ftpedia-md) kann beim
Anlegen des `_index.md` helfen.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

### Heftbegleitende Downloads

Wie schon angedeutet, werden die Downloads zum Heft direkt hier mit aufgelistet.
Jede dieser Dateien wird in den Ordner abgelegt, in dem sich die bereits
beschriebenen drei Dateien befinden; zum Beispiel `/2018-1`.

Dabei ist sowohl deren Name als auch deren Typ unerheblich.
Hugo haben wir auf Grund des Ablageortes beigebracht, diese Dateien als
zusätzliches Angebot zum Heft zu erkennen.
Dazu benötigt allerdings jede Download-Datei auch eine eigene Beschreibung
per `.md`-Dokument. Dieses Dokument ist in der 
[Dokumentation zum Know-How-Bereich](../../knowhow/download) beschreiben.

Damit wäre das Rüstzeug für ein Einzelheft samt zugehörigen Downloads
vorhanden:

* Mandatory
   - _index.md zur Beschreibung der jeweiligen ft:pedia
   - .pdf der betreffenden Ausgabe
   - titelseite.png
* Optional
   - Dateipärchen je Download zum Heft

<u>Interessant:</u>
Alle Dateien können schon vor dem Erscheinen der Ausgabe im jeweiligen Ordner
"geparkt" werden.
Sie werden erst sichtbar (und erreichbar), wenn das _index.md existiert!
Das eröffnet einen Mechanismus für den Teaser.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

## Jahrgang

Ein einzelner Jahrgang ft:pedia besteht aus 4 Ausgaben.
Die Verzeichnisstruktur ist einfach gehalten, es gibt für jede vorhandene
Ausgabe ein Unterverzeichnis.
````
_index.md
2017-1/
2017-2/
2017-3/
2017-4/
````

Um die Jahrgangseite für Hugo als solche zu definieren, ist ein `_index.md`
vorhanden. Es enthält lediglich dieses Frontmatter und sonst nichts:
````md
---
title: "2017"
weight: -2017
---
````
Das Beispiel stammt vom Jahrgang 2017; alle anderen Jahrgänge sind baugleich.
`title` entstpricht dem Jahr und `weight` sorgt für die richtige Sortierung
im Menü. Beide Einträge sind Pflicht.

Ohne die explizite Angabe `layout:` behandelt
`themes/website-layout/layouts/ftpedia/list.html`
die Seite als Auflistung des Jahrgangs (alle Hefte mit deren Downloads). Siehe
auch [ft:pedia: Script - 'list.html'](../ftpedia-list-html/).

Für einen neuen Jahrgang ist es lediglich erforderlich, einen neuen Ordner
mit passendem Jahr anzulegen (z. B.: /content/ftpedia/2019) und mit einem
`_index.md` zu bestücken.
Das Frontmatter ist sinngmäß anzupassen.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

## Jahrgangsübersicht

Folgt man dem Verzeichnisbaum zu `/content/ftpedia`, findet man sich auf der
Übersichtsseite mit den Jahrgängen wieder.
Für jeden
<a href="#jahrgang">Jahrgang</a>
gibt es einen eigenen Unterordner.
Weiterhin findet sich hier im Ordner auch noch das
<a href="#gesamtinhaltsverzeichnis">Gesamtinhaltsverzeichnis</a> und 
die [Übersicht über die Extras](#übersicht-extras-zum-heft).

````
_index.md
2011/
2012/
2013/
2014/
2015/
2016/
2017/
2018/
2019/
ftPedia_Artikeluebersicht.csv
overview.md
````

In der Datei `_index.md` ist die Beschreibung der Rubrik ft:pedia zu finden.
````md
---
title: "ft:pedia"
weight: 70
layout: "ftpediaAll"
legacy_id:
- ftcomm2409.html
---
<!-- https://www.ftcommunity.de/ftcomm2409.html?file=ftpedia -->
````
`title: "ft:pedia"` ergibt den Titel der Rubrik.
`weight: 70` legt die Position im Seitenmeü fest.
`layout: "ftpediaAll"` erklärt diese Seite zur Hauptseite der ft:pedia-Rubrik.
Mittels des Scriptes `themes/website-layout/layouts/ftpedia/list.html` wird
für diese Seite die Liste aller bisher erschienenen Ausgaben aus den
Jahrgängen und Einzelheften gebaut.

Die Zeilen zu `legacy_id:` sind für all diejenigen, die mit dem Link
zur alten ftc-Seite ankommen.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

## Gesamtinhaltsverzeichnis

Die Seite mit dem Gesamtinhaltsverzeichnis wird durch die Datei `overview.md`
definiert.
````md
---
title: "Gesamtinhalt"
weight: -9000
layout: "ftptoc"
legacy_id:
- /ftcommc482.html
---
````
Der Titel wird mittels `title:` vorgegeben, `weight: -9000` würde für die
Platzierung ganz oben im Menü sorgen, wenn die Seite dort dargestellt würde.
Auch für den Gesamtinhalt gab es in der alten ftc eine entsprechende URL, die
per `legacy_id:` zur neuen Seite umgeleitet wird.

`layout: "ftptoc"` veranlasst das Script
`themes/website-layout/layouts/ftpedia/ftptoc.html`, alle Informationen
zusammenzustellen und die Seite zu bauen.

Das große Gesamtinhaltsverzeichnis der ft:pedia wird aus einer 
Comma-Separated-Value-Datei (.csv) umgesetzt.
Der Name der Datei sei immer `ftPedia_Artikeluebersicht.csv` und ihr Ablageort
ist generell in `content/ftpedia/`.
Bereitgestellt wird diese Datei von der Redaktion der ft:pedia.

Der Inhalt von `ftPedia_Artikeluebersicht.csv` folgt exakt dieser Struktur:
````
Ausgabe;Autoren;Rubrik;Titel;Seiten;Abstract
"2019-1";"Dirk Fox";"Editorial";"„Muss man das verstehen?“";"2";""
"2019-1";"Christian Bergschneider, Stefan Fuss";"Wissenschaft";"Minimodelle schreiben Geschichte";"5–6";"Die Höhlenmalerei im südfranzösischen Lasceaux enthält einige der ältesten bildlichen Darstellungen der Menschheitsgeschichte. Aufgrund der jüngsten Auswertungen dieser Bilder müssen wahrscheinlich wesentliche Kapitel der Geschichte überarbeitet und teilweise neu geschrieben werden."
"2019-1";"Rüdiger Riedel, Stefan Falk";"Modell";"Spielereien mit ft und Magneten";"7–11";"Durch die Dynamic-Kästen haben wir Stahlkugeln, seit der „Technikgeschichte mit fischertechnik“ [1] haben wir kleine Stabmagnete. Was lässt sich damit noch anfangen?"
````
Nach der letzten Zeile darf ein Zeilenvorschub kommen, dieser ist optional.

Eine neue Zeile beginnt mit einem Zeilenumbruch.
Das Trennzeichen zwischen den Spalten einer Zeile ist das Semikolon (;) .
Die Felder werden mit Gänsefüßchen (") begrenzt.
Möchte man in einer Zeile ein Gänsefüßchen haben, 
so werden im csv zwei Gänsefüßchen nacheinander ("") gesetzt.
Typografische Anführungszeichen sind davon nicht betroffen.

Als eine besondere Eigenart von Hugo muss das allererste Feld der ersten Zeile
_ohne_ Gänsefüßchen vorgegeben werden. Aus Gründen der Vereinfachung werden
die Gänsefüßchen in der gesamten ersten Zeile weggelassen.

Die Spaltenreihenfolge entspricht derjenigen aus der alten Seite ('nasS').
Sie muss eingehalten werden, da `ftptoc.html` auf genau diese Reihenfolge
ausgelegt wurde.

Die Spaltenüberschriften werden allerdings aus der ersten Zeile der .csv
übernommen. 'Abstract' kann so von der Redaktion problemlos durch 'Auszug'
ersetzt werden.

Damit sieht der Kopf dieser Datei etwa so aus (wenn man das .csv mit Excel
öffnet, aber Excel unterdrückt auch die Gänsefüßchen!).

| Ausgabe  | Autoren | Rubrik | Titel | Seiten | Auszug |
|----------|---------|--------|-------|--------|--------|
| "2019-1" | "Dirk Fox" | "Editorial" | "„Muss man das verstehen?“" | "2" |  |
| "2019-1" | "Christian Bergschneider, Stefan Fuss" | "Wissenschaft" | "Minimodelle schreiben Geschichte" | "5–6" | "Die Höhlenmalerei im südfranzösischen Lasceaux enthält einige der ältesten bildlichen Darstellungen der Menschheitsgeschichte. Aufgrund der jüngsten Auswertungen dieser Bilder müssen wahrscheinlich wesentliche Kapitel der Geschichte überarbeitet und teilweise neu geschrieben werden." |
| ...      |         |        |       |        |        |

Zwischen dem Jahr und der Nummer der Ausgabe steht ein `minus` (Unicode u+002D)!
Das ist insofern wichtig, da `ftptoc.html` aus dieser Zeichenkette die
Ordnernamen zusammenbraut. Die heißen nicht umsonst genau so. Sollten die
Redakteure der ft:pedia hier Änderungsbedarf sehen, ist unbedingt auch das
Script anzupassen. Mein Rat: Lasst es wie es ist.

Bei den Seitenangaben steht kein `minus`, sondern ein `en-dash` (Unicode u+2013) zwischen den
Ziffern. Das Script ist so gebaut, dass es da alles als Trennung akzeptiert.

Das Manko der `minus`: Da kann ein Zeilenumbruch dazwischen kommen. Falls sich
das eines Tages doch als Problem darstellt, ist mein Vorschlag: Das Script
`ftptoc.html` ersetzt die jeweiligen `minus` durch `en-dash` für die Ausgabe -
letzterer wird nicht umgebrochen.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>


## Übersicht 'Extras zum Heft'

Die Seite mit der Übersicht über alle ft:pedia-Extras wird durch die Datei
`ftp-extras.md` bereitgestellt.
````md
---
title: "Übersicht Extras"
hidden: "true"
weight: -8999
layout: "ftp-extras"
legacy_id:
- downloadsa5ef.html
---
<!-- https://www.ftcommunity.de/downloads.html?kategorie=ft%3Apedia+Dateien -->
<!-- https://www.ftcommunity.de/downloadsa5ef.html?kategorie=ft%3Apedia+Dateien -->
````
Der Titel wird mittels `title:` vorgegeben, `weight: -8999` würde für die
Platzierung eins unter dem Gesamtinhaltsverzeichnis oben im Menü sorgen. Allerdings
ist diese Seite per `hidden: "true"` von der Anzeige im Menü ausgeschlossen.
(Anmerkung: inzwischen werden einzelne Seiten ohnehin nicht im Menü angezeigt).

In der nasS gibt es die Seite, auf der viele - aber nicht alle - dieser Extra-
Dateien zu finden sind. Es erscheint logisch deren `legacy_id` hierher
weiterzuleiten, falls mal jemand mit dem alten Link unsere neue Site besucht.

`layout: "ftp-extras"` veranlasst das Script
`themes/website-layout//layouts/ftpedia/ftp-extras.html`, alle relevanten
Dateien zusammenzusuchen und die Seite zu bauen.

<p><a href="#inhalt">Zurück zur Inhaltsübersicht</a></p>

