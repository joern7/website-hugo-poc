---
title: "FAQ: Chat"
weight: 80
date: 2020-01-20T00:00:00
uploadBy:
- "Stefan Falk"
legacy_id:
- /knowhow/umzugsgut/wiki/ex_wiki_faq_chat/
- /wikibf0b.html
- /php/wiki/8
- /ftcomm8cb8.html
- /php/ftcomm/chat
- 
---

### Worum geht es?
Im Chat können wir uns direkt miteinander unterhalten – jedenfalls die, die gerade „da“ sind. 
Der fischertechnik-Chat ist wie alles auf ftcommunity.de kostenlos; jede und jeder sind herzlich eingeladen.

### Wie komme in den Chat?
Der Chat ist auf mehrere Arten erreichbar:

* Über „[IRC](https://de.wikipedia.org/wiki/Internet_Relay_Chat)": 
Dazu braucht ihr einen „IRC-Client“. Oft verwendet wird [Nettalk](http://www.ntalk.de/Nettalk/). 
Wer mag, kann auch den rudimentären, nur für den ftc-Chat geschriebenen Client [SF Chat](https://www.ct-systeme.com/Lists/Downloads/DispForm.aspx?ID=31) verwenden. 
Mit einem IRC-Client eurer Wahl geht ihr dann auf den Server *chat.ftcommunity.de* (auf Port 6667, falls die Chat-Software euch das fragt), 
und dort in den Channel (Kanal) *#ftcommunity*. 
Ihr braucht dort gar keine Anmeldung, aber ihr könnt eine erstellen, wenn ihr möchtet. 
Damit Umlaute korrekt dargestellt werden, wählt bitte als Zeichensatz „UTF-8“, sofern euer IRC-Client eine Wahl anbietet.

* Über „[Matrix](https://matrix.org)“: Unser Matrix-Homeserver heißt *chat.ftcommunity.de* und bietet den Raum *#ftcommunity* an. 
Der Raum ist im kompletten Matrix-Netzwerk sichtbar.
Ihr könnt dafür ein Konto von jedem beliebigen Matrix-Server nutzen, der am Matrix-Verbund teilnimmt.
Wenn ihr bereits im Forum angemeldet seid, 
könnt ihr euch auch mit eurer Forums-Anmeldung – aber alles in Kleinbuchstaben – bei unserem Homeserver anmelden. 
Beim ersten Anmelden wird dann automatisch ein passendes Matrix-Konto für euch angelegt[^1]. 
In Matrix sind für Benutzernamen nur die Kleinbuchstaben a-z, die Ziffern 0-9 sowie die Sonderzeichen =_-./ zugelassen.
Wenn euer Benutzername andere Zeichen (z.B. Umlaute, Leerzeichen usw.) enthält, könnt ihr den Chat leider nicht mit dem Forums-Login nutzen.
Der vollständige Anmeldename mit einem Forums-Konto lautet dann *@anmeldename:chat.ftcommunity.de*, 
wobei *anmeldename* für den in Kleinbuchstaben geschriebenen ftc-Forums-Anmeldenamen steht. Das Kennwort ist dasselbe wie im Forum.

    Ihr könnt auch einen „Matrix-Client“ nutzen. 
    Ein Beispiel ist <https://riot.im/app>. 

Das Beste ist: Egal ob ihr Matrix oder IRC verwendet, ihr seid immer in beidem sichtbar
und könnt alles lesen und darauf antworten, was in einem der beiden Systeme geschrieben wird.

### Was muss ich noch wissen?
Nichts. Wenn etwas unklar ist, fragt einfach im Chat nach, oder bei Bedarf im Forum. 
Im Chat erklären euch die alten Hasen auch, was man da noch so tun kann (z.B. „private“ Chats zwischen zwei Teilnehmern führen).

Die Regeln im Chat sind grundsätzlich dieselben wie im Forum. 
Wir sind höflich zueinander, jeder darf alles fragen, ob nun direkt zu fischertechnik-Themen oder auch zu anderen.

Oft sind den ganzen Tag über bis in den späten Abend hinein Leute im Chat, und sei es nur „passiv“. 
Manchmal sind Leute im Chat, können aber gerade nicht lesen oder schreiben, weil sie anderes zu tun haben. 
Habt also bitte Geduld. Sagt einfach mal nett guten Tag, fragt was ihr fragen möchtet  und fangt ruhig eine Diskussion über etwas Interessantes an.

[^1]: Unser automatisch angelegter "Forums-Chat-Account" ist übrigens ebenfalls ein "vollwertiger" Matrix-Account, d.h. man kann mit dem dann auch auf Chats zugreifen, die bei matrix.org bzw. anderen "federated" Servern gehostet werden.
