---
layout: "image"
title: "Katamaran"
date: "2007-06-18T09:17:43"
picture: "kata2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10879
- /details30da.html
imported:
- "2019"
_4images_image_id: "10879"
_4images_cat_id: "985"
_4images_user_id: "557"
_4images_image_date: "2007-06-18T09:17:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10879 -->
hier die lenkung und das kardangelenk für die schiffschraube.oben wurde eine scheibe mit dam bauklotz verbunden und das zahnrad da drauf wird angetrieben