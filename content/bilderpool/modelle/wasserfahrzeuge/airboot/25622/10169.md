---
layout: "comment"
hidden: true
title: "10169"
date: "2009-11-02T03:55:01"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Der Spruch "Wasser hat keine Balken" ist hier wörtlich zu nehmen. Denn bereits
allerkleinste Abweichungen im (mittleren) Schwerpunkt der Längsachse können
dazu führen, daß das Boot nicht mehr geradeaus fährt, weil eine Seite minimal
(auch wenn's weniger als 1 mm ist) schwerer im Wasser liegt. Natürlich spielt da
auch der "Wellengang" und die Winddrift mit eine Rolle.

All diese Punkte würde ein Bootsfahrer (im richtigen Leben) sofort ausgleichen
können. Bei einem Modell, wo ein Kurs fest eingestellt wird, ist dieses weitaus
schwieriger zu gestalten, da sehr viele Faktoren die Richtung beeinflussen.

Auf einem Teich (im Garten?) würde ich das dahingehend einstellen, daß das
Boot einigermaßen im Kreis fährt, um möglichst lange fahren zu können. Kann
ja nix passieren.

Gruß, Thomas