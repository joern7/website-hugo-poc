---
layout: "image"
title: "Außenbord01"
date: "2011-05-29T20:45:13"
picture: "aussenbord01.jpg"
weight: "1"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30707
- /details4dfb.html
imported:
- "2019"
_4images_image_id: "30707"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30707 -->
Wird der S-Motor ft# 32293 seitlich mit 2 Bauplatten 15x90 mit Zapfen rot ft# 39245 versehen, stellt sich zwischen den beiden Bauplatten ein Zwischenraum von 30 mm ein, der, wie die Abbildung zeigt, mit einem Baustein 15 ohne Zapfen grau ft# 35003 und zwei Bausteinen 7,5 rot ft# 37468 ausgefüllt werden kann.

Der Baustein 15 ohne Zapfen grau ft# 35003 wird gewählt, weil der das Getriebe "störende" Zapfen nicht vorhanden ist und sich zwei der vier Längs-Nuten und das Loch des Zapfens genau auf der Mittellinie des Motorabtriebes befindet. Die Mittellinien der jeweils gegenüberliegenden Längs-Nuten des Bausteines 15 haben ebenso genau den Abstand, der für das 2:1-Getriebe erfoderlich ist.

Wenn ein Baustein 15 ohne Zapfen grau nicht verfügbar ist, kann dieser - die fischertechnik-Puristen mögen mir verzeihen - mit etwas "Gewalt" auch aus einem Baustein 15 mit 1 Zapfen grau ft# 31005 selbst hergestellt werden (Zapfen einfach absägen). Das geht natürlich auch mit Bausteinen 15 anderer Farbe.

Mit den Bausteinen 7,5 rot ft# 37468 links und rechts des Bausteines 15 lassen sich zwei Wangen für den Gelenk-, Getriebe- und Schiffsschrauben-Träger herstellen.
