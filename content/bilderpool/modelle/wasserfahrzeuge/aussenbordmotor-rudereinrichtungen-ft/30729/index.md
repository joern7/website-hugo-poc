---
layout: "image"
title: "Außenbord23"
date: "2011-05-29T20:45:13"
picture: "aussenbord23.jpg"
weight: "23"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30729
- /detailsc4bc-2.html
imported:
- "2019"
_4images_image_id: "30729"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30729 -->
Das Bild zeigt wie das Antriebsmodul bis zum "Anschlag" auf das Verbindungsstück des oberen Trägers des oberen Gelenkes des Außenbordmotors aufgeschoben worden ist. Das Ritzel Z10 greift nun in den inneren Ring Z32 des Zahnrades Z40/32 und sichert die Kraftübertragung.