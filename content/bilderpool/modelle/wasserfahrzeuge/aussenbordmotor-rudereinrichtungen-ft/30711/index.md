---
layout: "image"
title: "Außenbord05"
date: "2011-05-29T20:45:13"
picture: "aussenbord05.jpg"
weight: "5"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30711
- /details20ce.html
imported:
- "2019"
_4images_image_id: "30711"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30711 -->
Der 2:1-Getriebebaustein des Schiffsschraubenträgers wurde in der linken Längsnut mit 1 Zahnrad Z28 mit Rastachse m 0,5 schwarz ft# 31082 und 1 Rastkegelzahnrad mit Hülse m 1 schwarz ft# 35062 bestückt.