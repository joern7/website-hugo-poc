---
layout: "image"
title: "Außenbord25"
date: "2011-05-29T20:45:13"
picture: "aussenbord25.jpg"
weight: "25"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30731
- /detailsc3cc.html
imported:
- "2019"
_4images_image_id: "30731"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30731 -->
Die Boote mit den beiden verschiedenen Rudereinrichtungen, fertig verkabelt, mit IR-Empfängern und Stromversorgung ausgerüstet und mit "schmückendem" Beiwerk versehen noch im "Trockendock".