---
layout: "image"
title: "Außenbord02"
date: "2011-05-29T20:45:13"
picture: "aussenbord02.jpg"
weight: "2"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30708
- /details1ff7.html
imported:
- "2019"
_4images_image_id: "30708"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30708 -->
Eine Abschätzung zur Länge der seitlichen Wangen (15 mm Gelenkträger, 15 mm 2:1-Getriebeträger und 15 mm Schiffsschraubenträger) ergibt eine Wangenlänge von zunächst 45 mm, die sich mit 3 Bausteinen 7,5 rot ft# 37468 und 2 Verbindungsstücken 45 rot ft# 31330 zur Versteifung herstellen lässt.