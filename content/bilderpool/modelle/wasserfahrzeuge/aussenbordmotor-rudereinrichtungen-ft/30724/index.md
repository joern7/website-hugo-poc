---
layout: "image"
title: "Außenbord18"
date: "2011-05-29T20:45:13"
picture: "aussenbord18.jpg"
weight: "18"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30724
- /details2f18.html
imported:
- "2019"
_4images_image_id: "30724"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30724 -->
Aus den Teilen 1 S-Motor 6-9 V (mit kleiner Metallschnecke) schwarz ft# 32293, 1 U-Getriebe mini schwarz ft# 31078, 1 U-Achse 60 mit Zahnrad Z28 schwarz ft# 31063, 2 Seiltrommeln für 4mm Achse rot ft# 31016 und 1 Baustein 30 mit Bohrung grau ft# 31004, 1 Baustein 5 rot ft# 37237 und 1 Federnocken rot ft# 31982 als Stütze unter dem S-Motor sowie 1 Bauplatte 15*15 (zum Clipsen) rot ft# 31506 als Abdeckung wir der Antrieb des Seilzugen errichtet. 

Die Abbildung zeigt auf der rechten Bildseite das fertige Modul.