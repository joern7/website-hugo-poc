---
layout: "image"
title: "Außenbord26"
date: "2011-05-29T20:45:13"
picture: "aussenbord26.jpg"
weight: "26"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30732
- /details7045-2.html
imported:
- "2019"
_4images_image_id: "30732"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30732 -->
Hier sieht man die Boote mit den beiden verschiedenen Rudereinrichtungen nach dem Stapellauf auf Kreuzfahrt im "heimischen" Gewässer. Die Boote schwimmen gut, sind austariert und folgen den Befehlen der IR-Fernsteuerung.