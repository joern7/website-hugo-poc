---
layout: "image"
title: "Außenbord07"
date: "2011-05-29T20:45:13"
picture: "aussenbord07.jpg"
weight: "7"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30713
- /details142f.html
imported:
- "2019"
_4images_image_id: "30713"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30713 -->
Aus den Teilen:

1 Gelenkwürfel-Klaue rot ft# 31436 mit 1 Lagerhülse schwarz ft# 36819, montiert auf 1 Baustein 5 rot ft# 37237, und die auf 1 Kegelzahnrad mit Rastachse m 1 schwarz ft# 35061 mit aufgedrehtem M4-Gewinde aufgeschraubte Schiffsschraube Robbe 1-1455

wird die Schiffsschraube mit Antriebswelle, ihrem Lager und Lagerbock, die Schiffsschraubeneinheit, hergestellt.