---
layout: "image"
title: "Außenbord10"
date: "2011-05-29T20:45:13"
picture: "aussenbord10.jpg"
weight: "10"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30716
- /detailsa2f4.html
imported:
- "2019"
_4images_image_id: "30716"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30716 -->
Aus den Teilen:

1 Schubstange 30 ft# 37276, 1 Getriebeachse mit Ritzel Z 14 m 0,5 aus Kunststoff schwarz ft# 32912 und etwas weichem Silikonschlauch 

wird die kraftschlüssige Verbindung zwischen Schiffsschraubenantrieb und Schiffsschraubenträger hergestellt.

Die Spitze der Schubstange wird zunächst in eine produktionsbedingte Öffnung in der Mitte des Ritzels der Getriebeachse gepresst. Diese Öffnung kann zuvor noch mit einem Bohrer mit 2 mm Durchmesser gerundet und somit verbessert werden.

Um ein Schlagen der eingepressten Schubstange zu vermeiden, ist eine exakte achsiale Ausrichtung der beiden Komponenten erforderlich. Zu diesem Zweck wurde eine Ausrichtungsschablone gebaut. Links im Bild sieht man die geöffnete Schablone mit der eingelegten Getriebeachse mit Ritzel. In der Mitte des Bildes ist die Schablone geschlossen und die Schubstange (ist mit der nach unten zeigenden Spitze) bereits ebenfalls vorhanden. Die rechte Seite des Bildes zeigt nun die Situation nach dem Einpressen der Schubstange in das Ritzel der Getriebeachse. Jetzt sind beide Komponenten vorbildlich achsial ausgerichtet. Nichts kann mehr schlagen.