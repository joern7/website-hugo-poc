---
layout: "image"
title: "boot"
date: "2007-05-07T15:58:58"
picture: "PICT0017.jpg"
weight: "1"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10340
- /detailsadb7.html
imported:
- "2019"
_4images_image_id: "10340"
_4images_cat_id: "937"
_4images_user_id: "590"
_4images_image_date: "2007-05-07T15:58:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10340 -->
Hier sieht man den Antrieb und das Ruder.