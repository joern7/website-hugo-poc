---
layout: "overview"
title: "Außenborder"
date: 2020-02-22T08:26:48+01:00
legacy_id:
- /php/categories/1807
- /categories271b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1807 --> 
Schneller, elektrischer Außenbordmotor, kann als Antriebsmotor genutzt werden, wenn die Universalteil Kiste seetauglich gemacht werden soll