---
layout: "image"
title: "Rückansicht"
date: "2009-11-12T11:50:28"
picture: "ausssenborder2.jpg"
weight: "3"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25765
- /details61af.html
imported:
- "2019"
_4images_image_id: "25765"
_4images_cat_id: "1807"
_4images_user_id: "1019"
_4images_image_date: "2009-11-12T11:50:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25765 -->
Die Schiffsschraube ist mithilfe von Draht und Klebeband an der Achse befestigt. Dazu habe ich den Draht um ein Ende der Achse gewickelt und ein Stück nach hinten überstehen lassen.
Auf dieses abstehende Dahtende habe ich die Schraube geschoben und sie noch zusätzlich mit Klebeband fixiert.