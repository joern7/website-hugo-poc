---
layout: "image"
title: "Geöffneter Motor"
date: "2009-11-12T11:50:29"
picture: "ausssenborder5.jpg"
weight: "6"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25768
- /details780e-2.html
imported:
- "2019"
_4images_image_id: "25768"
_4images_cat_id: "1807"
_4images_user_id: "1019"
_4images_image_date: "2009-11-12T11:50:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25768 -->
Das gehäuse kann ganz einfach geöffnet werden, falls man einmal an den Motor muss.