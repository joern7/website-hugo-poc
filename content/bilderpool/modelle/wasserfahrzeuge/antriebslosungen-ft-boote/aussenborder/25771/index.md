---
layout: "image"
title: "Motor"
date: "2009-11-12T11:50:30"
picture: "ausssenborder8.jpg"
weight: "9"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25771
- /details6060-2.html
imported:
- "2019"
_4images_image_id: "25771"
_4images_cat_id: "1807"
_4images_user_id: "1019"
_4images_image_date: "2009-11-12T11:50:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25771 -->
Der Motor macht schon ordentlich schub, allerdings konnte ich den Außenborder bis jetzt nur bei maximal 1/4 Gas in der Badewanne austesten. Werde ihn demnächst mal auf dem See vom Modellbootverein Fahren lassen.