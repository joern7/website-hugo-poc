---
layout: "image"
title: "Seitenansicht"
date: "2007-02-11T17:06:55"
picture: "Ruderanlage03b.jpg"
weight: "3"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8969
- /details1df9.html
imported:
- "2019"
_4images_image_id: "8969"
_4images_cat_id: "815"
_4images_user_id: "488"
_4images_image_date: "2007-02-11T17:06:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8969 -->
