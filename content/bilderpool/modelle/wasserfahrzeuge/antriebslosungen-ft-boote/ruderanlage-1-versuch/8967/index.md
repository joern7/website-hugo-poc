---
layout: "image"
title: "Gesamtansicht 1"
date: "2007-02-11T17:06:55"
picture: "Ruderanlage01b.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8967
- /details3820-2.html
imported:
- "2019"
_4images_image_id: "8967"
_4images_cat_id: "815"
_4images_user_id: "488"
_4images_image_date: "2007-02-11T17:06:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8967 -->
