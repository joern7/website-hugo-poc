---
layout: "image"
title: "VSP-0x02.JPG"
date: "2007-08-09T18:44:38"
picture: "VSP-0x02.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11310
- /detailscc9e-2.html
imported:
- "2019"
_4images_image_id: "11310"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11310 -->
Hier treffen sich die Streben auch nicht in einem Punkt. Dafür ist die Lösung "reinrassig" ohne Fremdteile.