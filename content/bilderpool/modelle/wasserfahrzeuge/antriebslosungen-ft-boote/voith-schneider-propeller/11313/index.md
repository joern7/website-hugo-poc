---
layout: "image"
title: "VSP-0y01.JPG"
date: "2007-08-09T18:56:30"
picture: "VSP-0y01.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11313
- /detailsc4e6-2.html
imported:
- "2019"
_4images_image_id: "11313"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:56:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11313 -->
Die Blätter des Propellers sollen auf einer Kreisbahn umlaufen. Zuerst denkt man dabei natürlich an die ft-Drehscheibe, aber dieser Weg führt auf Probleme bei Lagerung und Antrieb.

Das ft-Speichenrad bietet sich als Alternative an.

Die Speichen sind nicht voll-rund, sondern an einer Seite abgeflacht. Das kann man ausnutzen, um ft-Steine flach draufzuschieben und dann um 90° zu schwenken. Diese Idee ist nicht von mir, sondern ist irgendwo in einer Anleitung zur Classic-Line versteckt.