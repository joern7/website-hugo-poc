---
layout: "image"
title: "Grundelement"
date: "2007-03-01T16:56:00"
picture: "bootselemente1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9194
- /details9891.html
imported:
- "2019"
_4images_image_id: "9194"
_4images_cat_id: "850"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9194 -->
Man sollte die Deckel mit Klebestoff oder sowas abdichten.