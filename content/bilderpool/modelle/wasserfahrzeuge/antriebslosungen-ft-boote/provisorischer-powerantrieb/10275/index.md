---
layout: "image"
title: "Schiffsschraube"
date: "2007-05-02T17:54:58"
picture: "bootsantrieb3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/10275
- /details0c02.html
imported:
- "2019"
_4images_image_id: "10275"
_4images_cat_id: "932"
_4images_user_id: "558"
_4images_image_date: "2007-05-02T17:54:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10275 -->
