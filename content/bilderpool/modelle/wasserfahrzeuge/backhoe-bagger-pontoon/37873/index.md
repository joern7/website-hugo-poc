---
layout: "image"
title: "Spud Aufrechten 2"
date: "2013-12-02T12:57:36"
picture: "backhoe03.jpg"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37873
- /details3319.html
imported:
- "2019"
_4images_image_id: "37873"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37873 -->
Die Spuds drehen sich durch ein Rechteckiges Loch im Ponton