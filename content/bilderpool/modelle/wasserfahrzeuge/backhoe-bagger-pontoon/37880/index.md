---
layout: "image"
title: "'Lauf Spud' Aufrechten"
date: "2013-12-02T12:57:36"
picture: "backhoe10.jpg"
weight: "10"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37880
- /details0b87.html
imported:
- "2019"
_4images_image_id: "37880"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37880 -->
Dies ist der "Lauf Spud". Der kann aber nicht vollig Wagerecht liegen wegen die Winde und die Laufschiene.
Dieser Spud wird dan auch aufegerichtet (normalerweise wird diese als Erste aufgerichtet).
