---
layout: "image"
title: "Innenleben Backhoe 3"
date: "2013-12-29T19:23:04"
picture: "backhoe4.jpg"
weight: "22"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37947
- /details5c0d.html
imported:
- "2019"
_4images_image_id: "37947"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-29T19:23:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37947 -->
Ventil fur den Bucket. (Nach die Idee von Harald: http://www.ftcommunity.de/details.php?image_id=37200).
