---
layout: "image"
title: "Gesamtansicht ohne Rampe"
date: "2009-01-30T19:38:47"
picture: "segelbootmirrampe21.jpg"
weight: "21"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17204
- /detailsa463-3.html
imported:
- "2019"
_4images_image_id: "17204"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:47"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17204 -->
Hier sieht man das Boot ohne Rampe.