---
layout: "image"
title: "Rampe"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe18.jpg"
weight: "18"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17201
- /details2c08.html
imported:
- "2019"
_4images_image_id: "17201"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17201 -->
Das ist die Rampe.