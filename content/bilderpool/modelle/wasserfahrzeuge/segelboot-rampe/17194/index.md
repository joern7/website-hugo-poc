---
layout: "image"
title: "Boot"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe11.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17194
- /details9d1c.html
imported:
- "2019"
_4images_image_id: "17194"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17194 -->
