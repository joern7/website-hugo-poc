---
layout: "image"
title: "Boot"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe13.jpg"
weight: "13"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17196
- /details7493-2.html
imported:
- "2019"
_4images_image_id: "17196"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17196 -->
