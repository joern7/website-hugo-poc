---
layout: "image"
title: "Steuer"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17191
- /detailsf19e.html
imported:
- "2019"
_4images_image_id: "17191"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17191 -->
Hier sieht man das Ruder.