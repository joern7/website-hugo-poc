---
layout: "image"
title: "Rampe von Seite"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe20.jpg"
weight: "20"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17203
- /detailsc86f.html
imported:
- "2019"
_4images_image_id: "17203"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17203 -->
