---
layout: "image"
title: "segelbootmirrampe07.jpg"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe07.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17190
- /details7ce8-2.html
imported:
- "2019"
_4images_image_id: "17190"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17190 -->
