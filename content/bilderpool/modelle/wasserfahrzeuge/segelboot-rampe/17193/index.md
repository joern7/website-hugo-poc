---
layout: "image"
title: "Gesamtansicht"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe10.jpg"
weight: "10"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17193
- /detailsd641-2.html
imported:
- "2019"
_4images_image_id: "17193"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17193 -->
