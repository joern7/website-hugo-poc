---
layout: "image"
title: "Boot- von der Seite"
date: "2011-02-18T14:12:43"
picture: "boot01.jpg"
weight: "1"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30055
- /detailsbd41.html
imported:
- "2019"
_4images_image_id: "30055"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30055 -->
Mein Bruder hat in der letzten Zeit an diesem Schiff hier gebaut. Es hat eine Beleuchtung, zwei Motoren,  und einen Summer. 
Aber zu den einzelnen Bilder gibt es dann eine Beschreibung.
Die Verkleidung besteht aus Pappe, die entsprechend zurecht geschnitten wurde.