---
layout: "image"
title: "Boot- Fahne"
date: "2011-02-18T14:12:43"
picture: "boot06.jpg"
weight: "5"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30060
- /details62f9.html
imported:
- "2019"
_4images_image_id: "30060"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30060 -->
Hier kann man sehr schön die Heimatfahne sehen, die ich gemacht habe ;).