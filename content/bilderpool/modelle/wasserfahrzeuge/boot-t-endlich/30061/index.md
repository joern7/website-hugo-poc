---
layout: "image"
title: "Boot- Schrauben"
date: "2011-02-18T14:12:43"
picture: "boot07.jpg"
weight: "6"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30061
- /details9c1b.html
imported:
- "2019"
_4images_image_id: "30061"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30061 -->
Hier sieht man die Schrauben, die das Boot antreiben. Sie sind etwas versetzt, da  sie sich sonst berühren.