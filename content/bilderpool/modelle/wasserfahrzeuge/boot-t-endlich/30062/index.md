---
layout: "image"
title: "Boot- Schrauben"
date: "2011-02-18T14:12:43"
picture: "boot08.jpg"
weight: "7"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30062
- /details3503-2.html
imported:
- "2019"
_4images_image_id: "30062"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30062 -->
Hier die Schrauben im Detail