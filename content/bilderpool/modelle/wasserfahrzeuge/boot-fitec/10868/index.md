---
layout: "image"
title: "Boot in Aktion"
date: "2007-06-14T20:34:31"
picture: "Boot24.jpg"
weight: "24"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10868
- /details0f05-3.html
imported:
- "2019"
_4images_image_id: "10868"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10868 -->
