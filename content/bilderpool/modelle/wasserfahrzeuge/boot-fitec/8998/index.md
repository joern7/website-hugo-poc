---
layout: "image"
title: "Boot"
date: "2007-02-12T17:47:07"
picture: "Boot1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8998
- /details21b5.html
imported:
- "2019"
_4images_image_id: "8998"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8998 -->
Das ist mein erstes Boot mit ft. es hat 2 Schaufelräder. Ich hab für den Bootsrumpf folgendes benutzt:
2x0,5Liter Flasche
2x0,33Liter Flasche
Ich werde es aber noch verbessern, weil etwas Wasser an die Motoren spritzt.