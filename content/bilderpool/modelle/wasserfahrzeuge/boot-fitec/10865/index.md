---
layout: "image"
title: "Antrieb"
date: "2007-06-14T20:34:31"
picture: "Boot21.jpg"
weight: "21"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10865
- /details1e9a.html
imported:
- "2019"
_4images_image_id: "10865"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10865 -->
Der Antrieb mit Schiffsschraube.