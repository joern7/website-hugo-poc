---
layout: "image"
title: "Schiffschraube hinten"
date: "2010-12-01T22:17:03"
picture: "motorbootfish6.jpg"
weight: "6"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/29400
- /detailsd489.html
imported:
- "2019"
_4images_image_id: "29400"
_4images_cat_id: "2135"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29400 -->
Hinten sind die beiden Schiffsschrauben. Sie verschwinden komplett im Wasser. Man kann sowohl die zweiflügligen Schrauben (36559) wie auch die vierflügligen Schrauben (36337) verwendet werden. Die Vierflügligen haben Vorteile besonders im Flachwasser...