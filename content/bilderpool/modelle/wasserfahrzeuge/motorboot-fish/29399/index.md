---
layout: "image"
title: "Getriebe seite"
date: "2010-12-01T22:17:03"
picture: "motorbootfish5.jpg"
weight: "5"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/29399
- /detailsa72e-2.html
imported:
- "2019"
_4images_image_id: "29399"
_4images_cat_id: "2135"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29399 -->
Zunächst ist ein großes Zahnrad angeschlossen danach folgen vier Kleine. Das ergibt eine Übersetzung von 50:3 (mit Motor).