---
layout: "image"
title: "Schiff vorne"
date: "2010-12-01T22:17:03"
picture: "motorbootfish3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/29397
- /details9e9c-3.html
imported:
- "2019"
_4images_image_id: "29397"
_4images_cat_id: "2135"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29397 -->
Hier sieht man die beiden Akkus (einen für die Stromversorgung, den anderen als Ballast)