---
layout: "image"
title: "Kompressor"
date: "2008-04-17T17:56:49"
picture: "bootmitluftstrahlantrieb2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14272
- /details40c2.html
imported:
- "2019"
_4images_image_id: "14272"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14272 -->
Hier sieht man den Kompressor und die zwei Luftspeicher.