---
layout: "image"
title: "Die Rampe"
date: "2007-10-27T14:49:58"
picture: "bootmitladestaionundcar11.jpg"
weight: "11"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- /php/details/12329
- /details4ce6-3.html
imported:
- "2019"
_4images_image_id: "12329"
_4images_cat_id: "1102"
_4images_user_id: "642"
_4images_image_date: "2007-10-27T14:49:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12329 -->
Ermöglicht Landgänge des Fahrzeugs