---
layout: "image"
title: "Delfin12.JPG"
date: "2004-11-21T13:37:35"
picture: "Delfin12.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Boot", "Jacht"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3300
- /details8c9c-2.html
imported:
- "2019"
_4images_image_id: "3300"
_4images_cat_id: "313"
_4images_user_id: "4"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3300 -->
Seetüchtig ist sie auch.

Auf das Oberdeck gehören allerdings noch zwei bis drei junge Damen.