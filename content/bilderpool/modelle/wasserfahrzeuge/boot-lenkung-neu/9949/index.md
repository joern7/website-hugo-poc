---
layout: "image"
title: "Boot neu"
date: "2007-04-04T10:29:45"
picture: "bootneu1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9949
- /details5f90-2.html
imported:
- "2019"
_4images_image_id: "9949"
_4images_cat_id: "899"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9949 -->
Gesamt. interface höher, anderer Antrieb, neue Spitze