---
layout: "image"
title: "FT-Bateau En détail 07"
date: "2009-08-04T18:13:36"
picture: "ftbateau11.jpg"
weight: "11"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/24711
- /details25da.html
imported:
- "2019"
_4images_image_id: "24711"
_4images_cat_id: "1697"
_4images_user_id: "136"
_4images_image_date: "2009-08-04T18:13:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24711 -->
