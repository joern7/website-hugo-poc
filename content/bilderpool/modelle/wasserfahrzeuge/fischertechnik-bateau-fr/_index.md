---
layout: "overview"
title: "Fischertechnik-bateau-(FR)"
date: 2020-02-22T08:27:14+01:00
legacy_id:
- /php/categories/1697
- /categoriesebc0.html
- /categoriesfcd9.html
- /categoriesd60b.html
- /categoriesb9aa.html
- /categories4ea7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1697 --> 
Un bateau fait avec le fischertechnik.

Créé en 2009 à Guéret (Greuze, France) et à Vic sur Cère (Cantal France) pendant mes vacances.

Pour les fans de fischertechnik en France et le monde Francophonie.






[i]Edit von Masked: Übersetzung ins Deutsche


Ein Schiff aus fischertechnik. Gebaut 2009 in Guéret und Vic sur Cére (beides Frankreich) während meiner Ferien/meines Urlaubs. Für die Fischertechnik-fans in Frankreich und in der frankophonen (=französischen, französisch-anhänigigen) Welt.[/i]