---
layout: "overview"
title: "Schaufelradschiff"
date: 2020-02-22T08:27:16+01:00
legacy_id:
- /php/categories/1937
- /categories9617.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1937 --> 
Ferngesteuertes Schiff mit zwei motorenisierten Schaufelrädern. Steuerung über Interface oder IR Control Set.