---
layout: "image"
title: "Schaufelrad seite"
date: "2010-12-01T22:17:02"
picture: "schaufelradschifffish3.jpg"
weight: "6"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/29393
- /detailsd25c.html
imported:
- "2019"
_4images_image_id: "29393"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29393 -->
Ein Schaufelrad mit Zugehör.