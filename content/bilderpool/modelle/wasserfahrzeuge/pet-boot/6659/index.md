---
layout: "image"
title: "Lenkungsdetails"
date: "2006-08-06T13:02:30"
picture: "Hovercraft_lenkungsdetails.jpg"
weight: "4"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6659
- /details0b5c.html
imported:
- "2019"
_4images_image_id: "6659"
_4images_cat_id: "575"
_4images_user_id: "426"
_4images_image_date: "2006-08-06T13:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6659 -->
Tja wie ihr seht habe ich hier meine super Funkfernsteuerung von Fischertechnik eingesetzt weil ich denke mal mit der Infrarot-Fernsteuerung muß man dann erst noch lernen übers Wasser zu gehen! :-)