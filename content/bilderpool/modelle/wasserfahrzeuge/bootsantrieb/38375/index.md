---
layout: "image"
title: "Unterseite"
date: "2014-02-23T18:04:06"
picture: "bootsantrieb2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38375
- /details9b56.html
imported:
- "2019"
_4images_image_id: "38375"
_4images_cat_id: "2853"
_4images_user_id: "104"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38375 -->
Dies ist die Unterseite der einfach in den Bootskörper eingelegten Antriebsgruppe. Der IR-Empfänger und (mit einem Federnocken) das Batteriegehäuse sitzen nur mit je einem Zapfen in der mittleren kleinen Nut der blauen Bauplatten.