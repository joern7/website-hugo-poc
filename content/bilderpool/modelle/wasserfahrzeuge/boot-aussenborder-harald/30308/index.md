---
layout: "image"
title: "Bootsrumpf-1"
date: "2011-03-23T17:31:55"
picture: "boot_4737.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30308
- /details92c9.html
imported:
- "2019"
_4images_image_id: "30308"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:31:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30308 -->
Das Gerippe des Bootsrumpfes. Da wird noch etwas Plastikfolie drum gewickelt und festgeklebt.