---
layout: "image"
title: "Außenborder"
date: "2011-03-23T17:27:03"
picture: "32912_32917_37276_37283_494.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30307
- /details71e8.html
imported:
- "2019"
_4images_image_id: "30307"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:27:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30307 -->
Ein Außenbordmotor, der *richtig* abgeht.
Die Schraube hier ist ein Modell "Robbe 1455" (2-Blatt,&#65279; 30 mm), und die braucht eine Untersetzung 2:1, damit sie richtig an den S-Motor angepasst ist. Die Teile dazu sind:
32912 oder  32917 -- Achsstummel mit Z14 (kurz / lang, aus dem 'neuen' Stufengetriebe)
37276 oder  37283 -- Schubstange 30 / 60 vom Hubgetriebe
Die Schubstange steckt mit ihrem spitzen Ende in der stirnseitigen Bohrung des Z14-Achsstummels.

Mit einer Graupner 2307.35 (3-Blatt, 35 mm) geht's auch ohne die Untersetzung und die Getriebeteile, man nehme dann eine 50mm Rastachse und verbinde sie per Silikonschlauch mit dem Motor.

Beide Schrauben haben ein 4mm-Messinggewinde. Mit einem BS15-Loch als Führung kann man die Schrauben einfach über eine Rastachse drüberschrauben und das passende Gewinde damit herstellen (hier gemacht mit dem Differenzial-Antriebsrad unten im Bild).


Video: http://www.youtube.com/watch?v=7YTA257HZJA