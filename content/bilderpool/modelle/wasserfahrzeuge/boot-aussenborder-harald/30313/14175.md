---
layout: "comment"
hidden: true
title: "14175"
date: "2011-04-25T23:27:34"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
Ja sicher, man kann den Rumpf auch aus dem Vollen arbeiten. Nur fehlt dieses Volumen dann für andere Einbauten und Funktionen, und 'echt' ist es ja auch nicht. Es gibt auch wohlfeile fertige Rümpfe im Modellbauladen. Ist aber alles nicht so prickelnd! 

Gruß,
Harald