---
layout: "image"
title: "Bootsrumpf-4"
date: "2011-03-23T17:43:13"
picture: "boot_4743.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30311
- /details9721.html
imported:
- "2019"
_4images_image_id: "30311"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:43:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30311 -->
Was beim Hochbau die "Angsteisen" sind (zusätzliche Armierungen im Beton, über die Berechnungen hinaus, damit auch ja nichts schief geht), das sind hier noch ein paar zusätzliche Lagen Klebeband, damit der Rumpf nicht so leicht aufgeschlitzt werden kann.