---
layout: "comment"
hidden: true
title: "13898"
date: "2011-03-24T09:56:11"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
einfach sensationell. Das toppt den ft-Rumpf um Längen! Wenn meine Söhne das entdecken, muss ich nicht nur die Quietscheentchen in Sicherheit bringen, sondern auch die Enten unseres Lieblingsteichs evakuieren lassen...
Eine Alternative zum Servo wären vielleicht zwei (starre) Außenborder - dass könnte dem Geschoss auch noch etwas mehr Vortrieb verleihen - und dann muss man wahrscheinlich auch die Schwäne in die Evakuierung einschließen und vorne einen Spritzschutz ergänzen.
Gruß, Dirk