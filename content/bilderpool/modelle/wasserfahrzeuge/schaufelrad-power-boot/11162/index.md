---
layout: "image"
title: "erster Versuch - Bild 2"
date: "2007-07-20T21:25:11"
picture: "schaufelradpowerboot2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11162
- /detailsc9e4-2.html
imported:
- "2019"
_4images_image_id: "11162"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T21:25:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11162 -->
da hilft nur umbauen, alles etwas weiter vor und hinten leichter bauen