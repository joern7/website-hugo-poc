---
layout: "image"
title: "Rahmen - Bild 2"
date: "2007-07-20T22:39:03"
picture: "schaufelradpowerboot1_9.jpg"
weight: "11"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11171
- /detailsccb9.html
imported:
- "2019"
_4images_image_id: "11171"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:39:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11171 -->
die roten Platten wurden bewusst nicht eingeklebt, montiert sich so besser und ist nicht wirklich nötig