---
layout: "image"
title: "Rahmen - Bild 1"
date: "2007-07-20T22:34:43"
picture: "schaufelradpowerboot1_8.jpg"
weight: "10"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11170
- /details351f-2.html
imported:
- "2019"
_4images_image_id: "11170"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:34:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11170 -->
hält alles auch ohne Boot, könnte so auch als spaciger Raumglteier durchgehen :)