---
layout: "image"
title: "Rahmen - Bild 3"
date: "2007-07-20T22:39:03"
picture: "schaufelradpowerboot2_2.jpg"
weight: "12"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11172
- /detailsebd1-2.html
imported:
- "2019"
_4images_image_id: "11172"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:39:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11172 -->
das "Horn" musste sein, wer weiß wie voll der Teich ist :)