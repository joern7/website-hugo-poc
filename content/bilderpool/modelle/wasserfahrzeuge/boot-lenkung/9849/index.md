---
layout: "image"
title: "Boot mit Antrieb und Lenkung"
date: "2007-03-29T17:34:25"
picture: "bootmitlenkung5.jpg"
weight: "5"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9849
- /details1852-3.html
imported:
- "2019"
_4images_image_id: "9849"
_4images_cat_id: "888"
_4images_user_id: "557"
_4images_image_date: "2007-03-29T17:34:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9849 -->
Gesamtansicht Heck