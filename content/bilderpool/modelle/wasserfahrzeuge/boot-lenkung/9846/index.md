---
layout: "image"
title: "Boot mit Antrieb und Lenkung"
date: "2007-03-29T17:34:25"
picture: "bootmitlenkung2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9846
- /details2c40.html
imported:
- "2019"
_4images_image_id: "9846"
_4images_cat_id: "888"
_4images_user_id: "557"
_4images_image_date: "2007-03-29T17:34:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9846 -->
Front. Lufttank, da sonst Gefahr des Untergehens.