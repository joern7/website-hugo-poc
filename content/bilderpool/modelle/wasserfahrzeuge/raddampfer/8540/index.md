---
layout: "image"
title: "Gesamt im Wasser"
date: "2007-01-20T16:46:18"
picture: "schiff13.jpg"
weight: "13"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8540
- /details4c13.html
imported:
- "2019"
_4images_image_id: "8540"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8540 -->
Hier ist ein Bild wie es vorwärts fährt.
Wenn ich ein passenden GartenTeich finde werde ich auch ein Video reinstellen.