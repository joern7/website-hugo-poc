---
layout: "image"
title: "Gesamt"
date: "2007-01-20T16:45:44"
picture: "schiff08.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8535
- /details6f55.html
imported:
- "2019"
_4images_image_id: "8535"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8535 -->
