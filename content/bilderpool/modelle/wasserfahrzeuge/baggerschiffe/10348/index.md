---
layout: "image"
title: "baggerschiff"
date: "2007-05-07T18:38:28"
picture: "baggerschiff2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10348
- /detailsa43f.html
imported:
- "2019"
_4images_image_id: "10348"
_4images_cat_id: "942"
_4images_user_id: "557"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10348 -->
hier wollte ich ein modul bauen, das den inhalt der baggerschaufel auffängt, aber die schaufel durchlassen kann...(siehe auch nächstes bild)