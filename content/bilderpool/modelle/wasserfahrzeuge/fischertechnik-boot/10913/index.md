---
layout: "image"
title: "Schaufelrad-Dampfer (le)"
date: "2007-06-25T00:32:31"
picture: "GrennderungDSC03057.jpg"
weight: "3"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/10913
- /details684b.html
imported:
- "2019"
_4images_image_id: "10913"
_4images_cat_id: "948"
_4images_user_id: "115"
_4images_image_date: "2007-06-25T00:32:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10913 -->
FT-Boats trifft Classic-Line  auf dem Schiffchenteich am Juister Kurplatz