---
layout: "image"
title: "Bootantrieb"
date: "2007-05-13T19:11:40"
picture: "Mein_Boot.jpg"
weight: "2"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: ["Schiffsantrieb", "Bootantrieb", "Boot", "Schiff"]
uploadBy: "Wolly2.0"
license: "unknown"
legacy_id:
- /php/details/10406
- /detailsfa8d.html
imported:
- "2019"
_4images_image_id: "10406"
_4images_cat_id: "948"
_4images_user_id: "570"
_4images_image_date: "2007-05-13T19:11:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10406 -->
Das ist mein Antriebssystem: auf einer umgedrehten Drehscheibe auf der sich auch der Drehmotor befindet sitzt der Powermotor der mit der Kette mit meiner Schiffschraube verbunden ist.