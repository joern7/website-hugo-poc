---
layout: "image"
title: "Katamaran01.JPG"
date: "2007-09-24T18:45:28"
picture: "Katamaran01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11942
- /details8603.html
imported:
- "2019"
_4images_image_id: "11942"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T18:45:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11942 -->
Nach der Convention ist vor der Convention...

Dieser Katamaran sollte eigentlich in Mörshausen zu Wasser gelassen werden, aber mit der Rumpfverschalung oder -Beplankung (oder was auch immer) ist eben nix gescheits geworden.