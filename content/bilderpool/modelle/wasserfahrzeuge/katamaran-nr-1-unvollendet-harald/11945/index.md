---
layout: "image"
title: "Katamaran04.JPG"
date: "2007-09-24T18:58:32"
picture: "Katamaran04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11945
- /details26c6-3.html
imported:
- "2019"
_4images_image_id: "11945"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T18:58:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11945 -->
Schraube (Marke Graupner) mit Adaptierung an 4 mm Messingrohr.

Damit das passt, habe ich eine m4-Schraube geköpft und zur Hälfte auf das Innenmaß des Messingrohrs abgedreht, diese Seite dann eingeklebt und die Schraube plus Kontermutter aufs restliche Gewinde geschraubt. 

Die senkrechte Antriebswelle muss durch ein Loch im Rumpf geführt werden. Da muss eine Dichtung hin, oder der Durchtritt muss oberhalb der Wasserlinie erfolgen. Selbst wenn das klappt, ist immer noch die Befestigung von Ruder und Getriebe, die irgendwie durch die Rumpfwand hindurch muss. Hmm....