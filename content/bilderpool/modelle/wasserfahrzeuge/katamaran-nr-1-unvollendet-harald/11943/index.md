---
layout: "image"
title: "Katamaran02.JPG"
date: "2007-09-24T18:47:25"
picture: "Katamaran02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11943
- /details3ed1.html
imported:
- "2019"
_4images_image_id: "11943"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T18:47:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11943 -->
Das Oberdeck kann durchgehend eben gemacht werden (bis auf die hervorstehenden Motoren).