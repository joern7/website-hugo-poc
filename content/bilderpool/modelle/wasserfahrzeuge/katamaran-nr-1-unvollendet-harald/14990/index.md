---
layout: "image"
title: "Rumpf glatt"
date: "2008-08-03T11:02:19"
picture: "P7310006_Rumpf_glatt.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14990
- /details8c53.html
imported:
- "2019"
_4images_image_id: "14990"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T11:02:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14990 -->
Die fertigen Abschnitte werden auf zwei Messingstäbe aufgespießt und mit Schmirgelpapier geglättet.