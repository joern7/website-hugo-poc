---
layout: "comment"
hidden: true
title: "14173"
date: "2011-04-25T22:23:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Oh ja, gute Frage!

Das Prinzip funktioniert.

Zwei Rümpfe aus diesem Pappmaché liegen irgendwo hinten im Regal herum, und haben niemals die Nähe von Wasser erlebt.
Zum Einen bringt so ein Rumpf nicht viel mehr als 1 kg nutzbarem Auftrieb (mit Reserve für Seegang), und mit 2 kg Gesamtgewicht hat man leider noch kein anständiges ft-Boot auf die Beine gestellt. 
Zum Anderen müssen die Rumpfteile stabil miteinander verbunden werden. Da fehlen beim Pappmaché die Punkte zum Festmachen.
Dann muss ja noch eine wasserdichte Außenhaut ums Ganze herum gestrickt werden. Mit der Lösung "Mülltüte" klappt das bei einem einfachen Rumpf sehr gut, aber beim  Doppelrumpf kommt man nicht weit, weil das Ganze ständig herum rutscht und der Halt an der Außenkante eben nur an der Außenkante wirkt. Das Problem ist die Einschnürung zwischen den Rümpfen.
Hinterher betrachtet, sag ich mal: es sollte machbar sein, wenn man die "Mülltüte" mit ein paar Gummiseilen (so etwa wie die Hosenträger einer Lederhose) hinzurrt und mit Panzerband drüber klebt. Die Wege für diese Gummiseile muss man aber rechtzeitig eingeplant haben.

Ja, und schließlich fehlt noch der Antrieb. Da wollte ich eine Antriebsachse senkrecht aus dem Heck nach unten führen und dann ins Waagerechte umleiten. Dafür fehlen wieder Befestigungspunkte, und der Tiefgang will auch berücksichtigt sein.

Nach den bisherigen Erfahrungen bin ich auch sehr skeptisch gegenüber einem wasserlöslichen Schiffsrumpf geworden. Irgendwo sickert oder tröpfelt immer mal ein Tröpfchen durch, und dann geht's abwärts. 

Das Bestreichen mit Bootslack wäre natürlich noch ein Gedanke, den ich noch nicht weiter verfolgt habe. Damit kann man sich das Gemansche mit dem Pappmaché gleich sparen und direkt auf der Rumpfschalung plus Frischhaltefolie anfangen. Auf absehbare Zeit wird das aber bei mir nichts werden. Diesen Ball kicke ich mal ungezielt in Richtung Spielfeldmitte :-)

Gruß,
Harald