---
layout: "image"
title: "Kata_6624.JPG"
date: "2011-11-01T17:35:26"
picture: "IMG_6624_mit.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33377
- /details6e1f.html
imported:
- "2019"
_4images_image_id: "33377"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:35:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33377 -->
Das Kleben mit der grünen Dampfsperrenfolie ist eine Kunst für sich. Das Zeug klebt dermaßen *ekelhaft* gut, dass man schon verloren hat, wenn eine Bahn beim Abziehen von der Rolle irgendwo Kontakt findet.

Auf keinen Fall, gar niemals nie nicht, sollte man damit direkt auf ft-Teilen kleben, die man später wieder verwenden möchte!

Die Eigenschaft "ekelhaft" findet sich auch im Preis wieder: man rechne mit ca. 1 Euro pro lfd. Meter Klebeband in 6 cm Breite.