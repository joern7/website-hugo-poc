---
layout: "image"
title: "IMG_6626.JPG"
date: "2011-11-01T17:39:49"
picture: "IMG_6626_mit.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33378
- /details41f5-2.html
imported:
- "2019"
_4images_image_id: "33378"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:39:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33378 -->
Das Oberdeck trägt einen Joystick für die Steuerung des Monitors (das ist der Fachbegriff für die Wasserspritze), einen schwenk- und drehbaren Suchscheinwerfer. Mangels einer Wasserpumpe ist der Monitor allerdings nur Dummy.