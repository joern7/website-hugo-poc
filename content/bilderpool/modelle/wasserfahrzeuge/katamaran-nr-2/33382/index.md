---
layout: "image"
title: "Kata5249.JPG"
date: "2011-11-01T17:52:32"
picture: "IMG_5249_mit.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33382
- /detailsf99f-2.html
imported:
- "2019"
_4images_image_id: "33382"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:52:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33382 -->
Der Rumpf musste noch etwas vergrößert werden, um den Auftrieb zu erhöhen. Deswegen ist jetzt der Kiel um einen BS15 tiefer gelegt.