---
layout: "image"
title: "Luftschraubenboot (3)"
date: "2009-04-26T19:08:26"
picture: "luftschraubenboot3.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/23813
- /detailse685.html
imported:
- "2019"
_4images_image_id: "23813"
_4images_cat_id: "1626"
_4images_user_id: "592"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23813 -->
Als Rumpf habe ich zwei Sortierwannen aus der Box 1000 verwendet. Ich habe einfach noch ein bisschen Frischhaltefolie drumgeklebt.