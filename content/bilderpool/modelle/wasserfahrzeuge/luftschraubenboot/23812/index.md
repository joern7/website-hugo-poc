---
layout: "image"
title: "Luftschraubenboot (2)"
date: "2009-04-26T19:08:25"
picture: "luftschraubenboot2.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/23812
- /detailsce33.html
imported:
- "2019"
_4images_image_id: "23812"
_4images_cat_id: "1626"
_4images_user_id: "592"
_4images_image_date: "2009-04-26T19:08:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23812 -->
Zusätzlich zu den beiden großen Antriebspropellern habe ich noch zwei zur Seite gebaut, damit kann das Boot seitlich einparken :-)
Der Accu ist verschiebbar, damit kann man den Schwerpunkt je nach Fahrstil verändern.