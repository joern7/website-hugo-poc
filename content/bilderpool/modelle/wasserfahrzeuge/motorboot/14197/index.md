---
layout: "image"
title: "Akku"
date: "2008-04-07T07:56:04"
picture: "motorboot04.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14197
- /detailsf16d-3.html
imported:
- "2019"
_4images_image_id: "14197"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14197 -->
In der Mitte des Bootes befindet sich der Akku.