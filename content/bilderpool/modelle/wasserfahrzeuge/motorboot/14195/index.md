---
layout: "image"
title: "Antrieb"
date: "2008-04-07T07:56:04"
picture: "motorboot02.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14195
- /details6104-3.html
imported:
- "2019"
_4images_image_id: "14195"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14195 -->
Hier sieht man die Propeller, die das Schiff antreiben.