---
layout: "image"
title: "Ruder"
date: "2008-04-07T07:56:04"
picture: "motorboot06.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14199
- /details5ed7.html
imported:
- "2019"
_4images_image_id: "14199"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14199 -->
Auf diesem Bild sieht man das Ruder.