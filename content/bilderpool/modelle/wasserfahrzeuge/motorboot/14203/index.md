---
layout: "image"
title: "Linke Seite des Bootes"
date: "2008-04-07T07:56:04"
picture: "motorboot10.jpg"
weight: "10"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14203
- /details89c3.html
imported:
- "2019"
_4images_image_id: "14203"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14203 -->
Und hier sieht man die linke Seite.