---
layout: "image"
title: "Das Boot von Rechts"
date: "2008-04-07T07:56:04"
picture: "motorboot09.jpg"
weight: "9"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14202
- /details923a.html
imported:
- "2019"
_4images_image_id: "14202"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14202 -->
Hier sieht man die rechte Seite des Bootes.