---
layout: "comment"
hidden: true
title: "6211"
date: "2008-04-13T17:06:19"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da hätte ich drei Fragen:

a) Wie hält die Schraube auf der Achse?

b) Zieht die Kette nicht ziemlich viel Spritzwasser mit hoch?

c) Ist die Achse direkt beim Motor nicht etwas stark gebogen?

Gruß,
Stefan