---
layout: "image"
title: "Arm"
date: "2009-05-27T18:14:02"
picture: "meiselbagger06.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24110
- /details7203.html
imported:
- "2019"
_4images_image_id: "24110"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24110 -->
