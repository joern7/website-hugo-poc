---
layout: "image"
title: "Gesamtansicht"
date: "2009-05-27T18:14:02"
picture: "meiselbagger01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24105
- /detailsf344.html
imported:
- "2019"
_4images_image_id: "24105"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24105 -->
