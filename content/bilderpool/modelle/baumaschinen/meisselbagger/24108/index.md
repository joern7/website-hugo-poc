---
layout: "image"
title: "Meisel_vorne"
date: "2009-05-27T18:14:02"
picture: "meiselbagger04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24108
- /details7026.html
imported:
- "2019"
_4images_image_id: "24108"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24108 -->
Das ist der Excenter.