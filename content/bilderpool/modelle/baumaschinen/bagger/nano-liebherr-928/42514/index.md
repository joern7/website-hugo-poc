---
layout: "image"
title: "... und trägt auch den leichten Arm."
date: "2015-12-09T12:11:32"
picture: "nanobagger5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42514
- /details459e.html
imported:
- "2019"
_4images_image_id: "42514"
_4images_cat_id: "3160"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42514 -->
Wie im Original kann er alles...