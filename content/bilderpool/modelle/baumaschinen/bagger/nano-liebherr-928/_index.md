---
layout: "overview"
title: "Nano-Liebherr 928"
date: 2020-02-22T08:12:42+01:00
legacy_id:
- /php/categories/3160
- /categories8b68.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3160 --> 
Wie klein kann man einen voll funktionsfähigen Bagger bauen? Die Nylon-Seilsteuerrung ist der Schlüssel zu dem Nano-Liebherr 928.