---
layout: "image"
title: "Montage"
date: "2015-12-09T12:11:32"
picture: "nanobagger3.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42512
- /detailsdb95.html
imported:
- "2019"
_4images_image_id: "42512"
_4images_cat_id: "3160"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42512 -->
Das Drehgelenk unten erlaubt die Lenk-Steuerung.