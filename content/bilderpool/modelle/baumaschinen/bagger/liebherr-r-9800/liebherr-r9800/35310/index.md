---
layout: "image"
title: "Blick in die Innerreien des Baggers"
date: "2012-08-11T20:38:30"
picture: "liebherrr04.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35310
- /details0352-2.html
imported:
- "2019"
_4images_image_id: "35310"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35310 -->
-