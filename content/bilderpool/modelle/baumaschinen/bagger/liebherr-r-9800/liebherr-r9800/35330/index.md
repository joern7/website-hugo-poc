---
layout: "image"
title: "Schaufel"
date: "2012-08-11T20:38:30"
picture: "liebherrr24.jpg"
weight: "24"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35330
- /details69f0.html
imported:
- "2019"
_4images_image_id: "35330"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35330 -->
Die Schaufel kann über zwei Minimotoren aufgeklappt werden.