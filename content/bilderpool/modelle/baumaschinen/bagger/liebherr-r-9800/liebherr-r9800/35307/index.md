---
layout: "image"
title: "Gesamtansicht des Baggers + des Ft-Konstrukteur"
date: "2012-08-11T20:38:30"
picture: "liebherrr01.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35307
- /details9b34.html
imported:
- "2019"
_4images_image_id: "35307"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35307 -->
Verbaut sind:

-4 Powermotoren zum Hochheben des kompletten Arms
-2 Powermotoren zum Hochheben des Mittelteiles des Arms
-1 Powermotor zum Bewegen der Schaufel
-1 Powermotor zum Drehen des Baggers
-1 Minimotor zum Hoch-und Runterfahren der Leiter
-2 Minimotor zum Öffnen der Schaufel
- etwa100 Stunden Arbeit

Der Bagger ist im Ft-Männchen-Maßstab gebaut.