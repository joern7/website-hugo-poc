---
layout: "image"
title: "Mittelstück des Arms"
date: "2012-08-11T20:38:30"
picture: "liebherrr18.jpg"
weight: "18"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35324
- /details090c.html
imported:
- "2019"
_4images_image_id: "35324"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35324 -->
Das Bild zeigt auch den Kardanantrieb für den Schnecken-Zylinder, der die Schaufel bewegt.