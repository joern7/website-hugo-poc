---
layout: "image"
title: "Arm-Unterteil"
date: "2012-08-11T20:38:30"
picture: "liebherrr16.jpg"
weight: "16"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35322
- /detailsc000-3.html
imported:
- "2019"
_4images_image_id: "35322"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35322 -->
In dem Unterteil des Arms sind die Motoren für das Mittelstück und für die Schaufel untergebracht.