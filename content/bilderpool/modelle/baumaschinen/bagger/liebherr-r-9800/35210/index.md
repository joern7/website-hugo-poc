---
layout: "image"
title: "Der Arm-Antrieb"
date: "2012-07-24T22:12:29"
picture: "liebherrr3.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35210
- /detailsc4b4.html
imported:
- "2019"
_4images_image_id: "35210"
_4images_cat_id: "2610"
_4images_user_id: "1122"
_4images_image_date: "2012-07-24T22:12:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35210 -->
Bisher habe ich 6 Power-Motoren und 3 Mini-Motren verbaut. Es kommen noch zwei Powermotoren dazu.
4 Power-Motoren, um den kompletten Arm zu heben und zwei Power-Motoren, um den Mittelteil des Arms zu bewegen.
Der eine, der noch nicht verbauten Power-Motoren wird die Schaufel bewegen, und der andere ist zum Drehen des Oberwagens notwendig.
