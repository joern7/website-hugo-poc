---
layout: "image"
title: "Blick auf den Rohbau"
date: "2012-07-24T22:12:29"
picture: "liebherrr4.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35211
- /details3e35.html
imported:
- "2019"
_4images_image_id: "35211"
_4images_cat_id: "2610"
_4images_user_id: "1122"
_4images_image_date: "2012-07-24T22:12:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35211 -->
-