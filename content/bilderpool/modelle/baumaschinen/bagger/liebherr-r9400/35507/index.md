---
layout: "image"
title: "Liebherr R9400"
date: "2012-09-12T21:32:11"
picture: "P9120059.jpg"
weight: "39"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: ["Hubspindel"]
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/35507
- /details0538-2.html
imported:
- "2019"
_4images_image_id: "35507"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-09-12T21:32:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35507 -->
De dubbel D M8 moer zit er gewoon ingeslagen met een hamer en komt er echt niet meer uit. Het voordeel van de hoge moer is dat hij er altijd recht in gaat. En wanneer het draadeind helemaal uitgeschoven/ gedraaid is knikt deze niet.
Voorin het M8 draad zit een 4,5mm gat. 4mm is te krap nu kan alles toch nog iets leven / bewegen.

Hoop dat dit alles wat verduidelijkt.