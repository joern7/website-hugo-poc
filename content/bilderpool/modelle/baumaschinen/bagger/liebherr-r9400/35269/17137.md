---
layout: "comment"
hidden: true
title: "17137"
date: "2012-08-26T22:19:20"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Wieder so ein tolles Scale-Modell. Irre, dass Du den riesigen und "gewichtigen" Arm (selbst ohne Last...) mit ft-Motoren gehoben bekommst...
Gruß, Dirk