---
layout: "image"
title: "aandrijving"
date: "2012-08-07T08:03:14"
picture: "PICT3777.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/35266
- /detailsd596.html
imported:
- "2019"
_4images_image_id: "35266"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-08-07T08:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35266 -->
Aandrijving hoofd hefcilinders door 2 power motoren (is wel nodig de arm is nog al zwaar en heeft een grote bewegings hoek)