---
layout: "comment"
hidden: true
title: "17217"
date: "2012-09-10T20:49:32"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Ruurd,

Elegante oplossing !

- Heb je de dubbele M8 moer half-boven in de cilinder aan elkaar gelast en met een schroef-tap iets ruimer gemaakt zodat het draadeind lichter loopt ?

- De M4-draad netjes uitklokken/centreren is volgens mij relevant om de RVS-cilinder niet (zichtbaar) te laten slingeren ?.........

Grüss,

Peter
Poederoyen NL