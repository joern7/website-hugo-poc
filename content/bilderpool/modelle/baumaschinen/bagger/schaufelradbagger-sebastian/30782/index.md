---
layout: "image"
title: "Kran"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger6.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30782
- /detailsd898.html
imported:
- "2019"
_4images_image_id: "30782"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30782 -->
Man sieht den Kran, der voll funktionsfähig ist. Er wird über Taster gesteuert.