---
layout: "image"
title: "Gesamtansicht"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30777
- /detailsbb73.html
imported:
- "2019"
_4images_image_id: "30777"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30777 -->
Der Schaufelradbagger ist 170cm lang, 80cm hoch und 20 cm breit. Der Ausleger kann mittels Controlset gehoben und gesenkt werden. Der Bagger lässt sich drehen. Wenn sich das Schaufelrad dreht hört man aus dem Soundmodul Geräusche.