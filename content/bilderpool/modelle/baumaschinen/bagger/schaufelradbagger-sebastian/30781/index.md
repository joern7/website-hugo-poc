---
layout: "image"
title: "Arbeiter"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger5.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30781
- /details5542.html
imported:
- "2019"
_4images_image_id: "30781"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30781 -->
Hier sieht man die fleißigen Arbeiter.