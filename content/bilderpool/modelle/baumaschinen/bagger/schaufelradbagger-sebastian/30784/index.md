---
layout: "image"
title: "Plakate"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger8.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30784
- /details7273.html
imported:
- "2019"
_4images_image_id: "30784"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30784 -->
Hier sind die Plakate von den Umweltschützern zu sehen.