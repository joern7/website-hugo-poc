---
layout: "image"
title: "Seilbagger 010"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_010.JPG"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2829
- /details5bee-4.html
imported:
- "2019"
_4images_image_id: "2829"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2829 -->
von Claus-W. Ludwig