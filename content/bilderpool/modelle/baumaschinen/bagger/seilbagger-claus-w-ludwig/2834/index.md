---
layout: "image"
title: "Seilbagger 015"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_015.JPG"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2834
- /detailsfe06-2.html
imported:
- "2019"
_4images_image_id: "2834"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2834 -->
von Claus-W. Ludwig