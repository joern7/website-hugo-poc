---
layout: "image"
title: "Bagger 16"
date: "2008-03-24T10:35:19"
picture: "baggerstefanl16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14084
- /details9287-2.html
imported:
- "2019"
_4images_image_id: "14084"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:19"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14084 -->
