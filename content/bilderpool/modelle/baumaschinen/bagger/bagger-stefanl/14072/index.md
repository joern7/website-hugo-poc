---
layout: "image"
title: "Bagger 4"
date: "2008-03-24T10:35:18"
picture: "baggerstefanl04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14072
- /detailsfeb8.html
imported:
- "2019"
_4images_image_id: "14072"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14072 -->
