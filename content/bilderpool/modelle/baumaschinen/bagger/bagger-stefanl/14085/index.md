---
layout: "image"
title: "Bagger 17"
date: "2008-03-24T10:35:19"
picture: "baggerstefanl17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14085
- /detailsf698.html
imported:
- "2019"
_4images_image_id: "14085"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:19"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14085 -->
Der Fahrer hat noch ein paar Steuerhebel bekommen.