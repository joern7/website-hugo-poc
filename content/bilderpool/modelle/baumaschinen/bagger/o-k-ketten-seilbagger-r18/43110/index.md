---
layout: "image"
title: "O&K Bagger R18 (9) die Kettenauflage"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr09.jpg"
weight: "9"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43110
- /details92d4-2.html
imported:
- "2019"
_4images_image_id: "43110"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43110 -->
Die Leiträder (Antriebsräder) sind gegenüber der Rollenwagen etwas höher angebaut, damit sie nicht so einer hohen Belastung am Boden außgesetzt sind.