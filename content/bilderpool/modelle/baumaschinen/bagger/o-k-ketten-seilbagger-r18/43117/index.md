---
layout: "image"
title: "O&K Bagger R18 (16) Seitenansicht 1"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr16.jpg"
weight: "16"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43117
- /detailse0d5.html
imported:
- "2019"
_4images_image_id: "43117"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43117 -->
Die Schiebetüren lassen sich über eine Metallachse locker schieben. An der hinteren Tür kann man die 3 Taster sehen, mit denen die einzelnen Elektronikkomponenten ein u. ausgeschaltet werden.Das Schwungrad läuft nur leer mit und  ist wegen der Optik des Orginals eingebaut.