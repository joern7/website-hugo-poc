---
layout: "image"
title: "Fahrgestell (Version 1)"
date: "2010-05-08T20:05:20"
picture: "bagger3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27204
- /detailsf410.html
imported:
- "2019"
_4images_image_id: "27204"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27204 -->
Das Fahrgestell ist ein etwas umgebautes Basismodell aus "Robo Mobile Set". In der Mitte ist das Interface und rechts sind die Schwungräder der Kompressoren. Dazwischen liegt die Batterie und der Lufttank.