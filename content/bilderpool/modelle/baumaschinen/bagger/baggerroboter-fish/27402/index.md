---
layout: "image"
title: "Baggerarm angezogen"
date: "2010-06-06T21:36:58"
picture: "baggerfishv3.jpg"
weight: "8"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27402
- /details9cd2.html
imported:
- "2019"
_4images_image_id: "27402"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27402 -->
Baggerarm mit beladener Schaufel.