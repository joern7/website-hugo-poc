---
layout: "image"
title: "Fahrgestell von der Seite (Version 2)"
date: "2010-06-06T21:36:58"
picture: "baggerfishv4.jpg"
weight: "9"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27403
- /details1a06.html
imported:
- "2019"
_4images_image_id: "27403"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27403 -->
Man sieht die Ketten, den Anfang des Arms und die Kompressoren.