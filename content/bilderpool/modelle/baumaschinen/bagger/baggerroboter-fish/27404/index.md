---
layout: "image"
title: "Bagger von vorne (Version 2)"
date: "2010-06-06T21:36:59"
picture: "baggerfishv5.jpg"
weight: "10"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27404
- /detailsf6e8.html
imported:
- "2019"
_4images_image_id: "27404"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27404 -->
In der Mitte ist der Farbsensor, rechts und links sind die Lampen und Kabel (USB-Kabel, Netzkabel). Der Bagger läuft aber auch mit Akku und IR-Fernbedienung.