---
layout: "image"
title: "Lenkung"
date: "2006-06-20T21:35:52"
picture: "DSCN0790.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6446
- /details1a07-2.html
imported:
- "2019"
_4images_image_id: "6446"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6446 -->
hier ist die vorhin angesprochene Metallachse zu erkennen.
Die beiden parallel verlaufenden Gewindstangen bewegen den Lenkhebel. Auf Grund der grossen Kräfte hat eine nicht ausgereicht.
Auch zu erkennen die beiden grauen Schalter für die Endabschaltung des Lenkmotors.