---
layout: "image"
title: "Gesamtansicht"
date: "2006-07-06T15:33:41"
picture: "DSCN0841.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6599
- /detailsc49e.html
imported:
- "2019"
_4images_image_id: "6599"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-06T15:33:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6599 -->
- ja, so könnte er mal aussehen - wenn er dann fertig wird.
Das Ding ist jetzt schon tierisch schwer.
Und die ganze Motorisierung für den Oberbau fehlt immer noch!