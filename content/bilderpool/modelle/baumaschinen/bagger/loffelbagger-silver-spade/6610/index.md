---
layout: "image"
title: "Die Energie"
date: "2006-07-10T17:38:39"
picture: "DSCN0863.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6610
- /details8b1d.html
imported:
- "2019"
_4images_image_id: "6610"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-10T17:38:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6610 -->
die der Bagger benötigt, wird mittels Kabel übertragen. Damit ich die nicht an den Motoren selbst befestigen muss (und somit das Modell unhandlich wird) habe ich mir etwas einfallen lassen. Dazu habe ich den Baustein 15 an den Ecken durchbohrt und jeweils eine von den Rohrhülsen eingeschoben.
Jetzt kann ich die Kabel direkt anschliessen und abziehen, ohne das ich an die Motoren heran muss.