---
layout: "image"
title: "Drehkranz Unterteil"
date: "2006-06-20T21:35:52"
picture: "DSCN0792.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6448
- /details5cdb-2.html
imported:
- "2019"
_4images_image_id: "6448"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6448 -->
Ansicht von oben.
Auch hier die beiden Endabschalter für die Drehrichtung.
Das Original hatte auch nur einen Bewegungsradius von 180 Grad.