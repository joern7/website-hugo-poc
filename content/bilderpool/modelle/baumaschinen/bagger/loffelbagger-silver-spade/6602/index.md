---
layout: "image"
title: "Oberer Teil"
date: "2006-07-06T15:33:42"
picture: "DSCN0843.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6602
- /details9118.html
imported:
- "2019"
_4images_image_id: "6602"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-06T15:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6602 -->
Die Spitze des Schwarzen Gerüstes liegt 78cm oberhalb der Tischkante. So in meinem Kopf funktioniert der "Schiebemechnismus" schon, nur halt im Modell noch nicht.