---
layout: "image"
title: "Drehkranz Unterteil"
date: "2006-06-20T21:35:52"
picture: "DSCN0788.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6444
- /details2fde.html
imported:
- "2019"
_4images_image_id: "6444"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6444 -->
mit den Antrieben für die Lenkung und dem Drehkranz.
Der Power Motor sollte eigentlich nicht so weit herausragen - das gefällt mir so ganz und gar nicht ...