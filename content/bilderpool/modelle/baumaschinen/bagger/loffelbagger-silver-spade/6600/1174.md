---
layout: "comment"
hidden: true
title: "1174"
date: "2006-07-06T18:22:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Du liebe Güte, was ein Monsterteil. Da stecken sicher viele Stunden drin. Mal eine Frage: Warum verbaust Du eigentlich so gerne so viele Grundbausteine anstatt Statik oder Alus? Das macht das Modell und die Arbeit der Antriebe doch sicher unheimlich schwer, oder nicht?