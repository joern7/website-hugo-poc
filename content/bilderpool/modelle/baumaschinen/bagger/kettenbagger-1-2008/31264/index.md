---
layout: "image"
title: "Baggerkabine"
date: "2011-07-14T11:58:24"
picture: "kettenbagger22.jpg"
weight: "21"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31264
- /detailsa867.html
imported:
- "2019"
_4images_image_id: "31264"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31264 -->
