---
layout: "image"
title: "Baggerarm"
date: "2011-07-14T11:58:24"
picture: "kettenbagger13.jpg"
weight: "13"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31255
- /detailsf2d3.html
imported:
- "2019"
_4images_image_id: "31255"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31255 -->
Auch Baggerarm. Gelenk wird mit einer Schnecke bewegt