---
layout: "image"
title: "Oberwagen"
date: "2011-07-14T11:58:24"
picture: "kettenbagger12.jpg"
weight: "12"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31254
- /details45a9.html
imported:
- "2019"
_4images_image_id: "31254"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31254 -->
Die Schnecke, die den Arm bewegt, die Streben führen zum Baggerarm