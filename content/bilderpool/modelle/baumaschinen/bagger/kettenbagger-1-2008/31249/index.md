---
layout: "image"
title: "Kettenbagger"
date: "2011-07-14T11:58:24"
picture: "kettenbagger07.jpg"
weight: "7"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31249
- /detailsbd1a.html
imported:
- "2019"
_4images_image_id: "31249"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31249 -->
der Arm kann senkrecht nach oben gestellt werden