---
layout: "image"
title: "Oberwagen"
date: "2011-07-14T12:04:42"
picture: "kettenbagger06.jpg"
weight: "6"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31279
- /details0809.html
imported:
- "2019"
_4images_image_id: "31279"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31279 -->
