---
layout: "image"
title: "Baggerarm"
date: "2011-07-14T12:04:43"
picture: "kettenbagger12.jpg"
weight: "12"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31285
- /detailsb19d-2.html
imported:
- "2019"
_4images_image_id: "31285"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:43"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31285 -->
