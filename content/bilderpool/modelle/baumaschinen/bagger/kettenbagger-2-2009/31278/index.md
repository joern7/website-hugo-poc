---
layout: "image"
title: "Oberwagen, Technik"
date: "2011-07-14T12:04:42"
picture: "kettenbagger05.jpg"
weight: "5"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31278
- /detailsd128-2.html
imported:
- "2019"
_4images_image_id: "31278"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31278 -->
Diesmal sitzt der Drehkranz Motor im Oberwagen (Powermot.)