---
layout: "image"
title: "Oberwagen, Technik"
date: "2011-07-14T12:04:42"
picture: "kettenbagger04.jpg"
weight: "4"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31277
- /details03e4.html
imported:
- "2019"
_4images_image_id: "31277"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31277 -->
Der Powermot treibt die Schnecke über zwei Kegelzahnräder an. Der Vorteil der beweglichen, nach oben gerichteten Schnecke ist, dass der Oberwagen nach hinten hin nciht so lang wird. Und die Schnecke Gleichzeitg länger sein kann.