---
layout: "image"
title: "Draaikrans"
date: "2016-05-25T10:12:52"
picture: "P3280061.jpg"
weight: "14"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43428
- /details60af.html
imported:
- "2019"
_4images_image_id: "43428"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43428 -->
De draaikrans zelf met het vaste tandwiel in het midden