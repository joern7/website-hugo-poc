---
layout: "image"
title: "Kabel geleiding"
date: "2016-05-25T10:12:52"
picture: "P3280059.jpg"
weight: "12"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43426
- /detailsc265-2.html
imported:
- "2019"
_4images_image_id: "43426"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43426 -->
Kabelgeleiding via een krukas systeem voor het mooier oprollen van de kabel op de drum