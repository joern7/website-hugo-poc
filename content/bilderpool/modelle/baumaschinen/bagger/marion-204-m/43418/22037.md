---
layout: "comment"
hidden: true
title: "22037"
date: "2016-05-28T08:40:28"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Klasse!

Die Geometrie der Hebel und Gelenke geht so ein kleines bisschen ( :-) ) über die Viergelenkmechanik des hobby2 hinaus. Das muss man als Video gesehen haben:
https://www.youtube.com/watch?v=ygT06zVKcjY

... und diese ft-Seite findet sich gleich als vierte Google-Fundstelle :-)

Gruß,
Harald