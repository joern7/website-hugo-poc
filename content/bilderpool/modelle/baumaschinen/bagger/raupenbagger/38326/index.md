---
layout: "image"
title: "Unteransicht Oberwagen"
date: "2014-02-21T20:18:43"
picture: "raupenbagger11.jpg"
weight: "11"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38326
- /details1454.html
imported:
- "2019"
_4images_image_id: "38326"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:43"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38326 -->
Im Gehäuse des Oberwagen stecken zur Zeit nur 2 Akku-Packs und der Schalter. Empfänger für RC kommt später noch. Platz dafür ist genügend.
Sollte ich später mal den Baggerarm über Pneumatik bewegen wollen, wird es allerdings eng im Motorraum!
Außerdem werde ich die Akkus ganz nach hinten verlegen, um einen besseren Hebel für das Gegengewicht zu haben.