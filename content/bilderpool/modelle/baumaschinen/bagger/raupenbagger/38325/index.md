---
layout: "image"
title: "Gesamtansicht von unten"
date: "2014-02-21T20:18:35"
picture: "raupenbagger10.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38325
- /details4a41-2.html
imported:
- "2019"
_4images_image_id: "38325"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38325 -->
In diesem Bild lässt sich mangels 3D der Oberwagen nicht richtig vom Unterwagen unterscheiden