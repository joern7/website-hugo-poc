---
layout: "image"
title: "Bagger von hinten"
date: "2014-02-21T20:18:35"
picture: "raupenbagger07.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38322
- /details7bcb.html
imported:
- "2019"
_4images_image_id: "38322"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38322 -->
Hier sieht man gut, wie unscheinbar die Motoren und Achsen im Modell integriert sind.
Auch der Drehkranz wird von unten angetrieben. Ich verwende im Moment einen Mini/Micromotor. Der schafft es aber nicht immer, den Drehkranz zu drehen. Mein Exemplar ist auch etwas schwergängig. Wahrscheinlich tausche ich den Drehkranz nochmal aus.
Der Motor ist sowieso nicht die beste Lösung. Eigentlich wollte ich einen S-Motor verwenden (für den genug Platz wäre). Dann komme ich aber mit dem Abstand vom Getriebe zum Drehkranz nicht hin (zumindest nicht mit Rastachsen).
Bei Metallachsen habe ich festgestellt, daß das Z10 mit Spannzange irgendwie eine andere Form von Zähnen hat. Diese greifen nicht gut in den Drehkranz.
Man sieht auch noch ein bisschen Kabelsalat. Ich wollte aber die Drähte des Schleifrings (noch) nicht kürzen.

Warum ich den Bagger für die Bilder aufgebockt habe, erkläre ich in einem der nächsten Bilder
