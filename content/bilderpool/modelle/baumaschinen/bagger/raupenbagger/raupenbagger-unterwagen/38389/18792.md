---
layout: "comment"
hidden: true
title: "18792"
date: "2014-02-27T13:19:48"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hi Harald,
danke für den Tipp!
ich habe mal in den Kisten gestöbert und tatsächlich 3 verschiedene Stecker-Varianten gefunden mit unterschiedlich langen Stiften, mit Plastikgehäuse in 3 verschiedenen Längen und 2 Breiten. Allerdings bei allen diesen Varianten ist das Loch nicht durchgehend. Da scheint es mindestens eine weitere Variante zu geben, auf die Du Dich beziehst.
Aber egal...ich möchte die Motoren aus Stabilitätsgründen anders herum einbauen, dann habe ich für Stecker sowieso keinen Platz mehr. Deswegen werde ich die Stecker ersetzen durch Ader-Endhülsen als Stiftkontakte.