---
layout: "image"
title: "Stecker-Modding"
date: "2014-02-26T08:17:56"
picture: "raupenbaggerunterwagen11.jpg"
weight: "11"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38389
- /details0289.html
imported:
- "2019"
_4images_image_id: "38389"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38389 -->
Ein kleines Detail am Rande:

Mit einem normalen Stecker würden die Drähte doch sehr sichtbar sein und den Gesamteindruck des Modells stören.
Den roten Stecker habe ich deshalb spezial behandelt. Mit einem kleinen Bohrer habe ich das Sack-Loch, in welchem der Draht normalerweise sitzt, durchgebohrt.
Den Draht kann ich dadurch von der anderen Seite einfädeln (durch das Loch, in dem man normalerweise einen weiteren Stecker anschliessen kann).
Das ist etwas hakelig, da der Draht um 90° rum muß. Man schiebt den Draht aber bis ans Ende, um ihn mit der Schraube festklemmen zu können
