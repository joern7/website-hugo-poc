---
layout: "image"
title: "Unterwagen Unteransicht"
date: "2014-06-10T06:55:41"
picture: "Unteransicht.jpg"
weight: "13"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Raupenbagger", "Unterwagen", "Drehkranz", "Schleifring"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38926
- /details4893.html
imported:
- "2019"
_4images_image_id: "38926"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38926 -->
Von unten sind fast nur glatte Oberflächen zu sehen.

Man sieht hier einen offenen Punkt. Die zwei Motoren je Seite möchte ich miteinander koppeln.
Nach vorne zu den Kettenfahrwerken möchte ich auf jeden Fall mit Rastachsen arbeiten. Allerdings kann ich die beiden Motoren nicht mit Rastachsen koppeln.
Mit Metallachsen müsste ich sägen und eine Verbindung zu den Rastachsen-Kardangelenk schaffen. Hat irgend jemand eine gute Idee?