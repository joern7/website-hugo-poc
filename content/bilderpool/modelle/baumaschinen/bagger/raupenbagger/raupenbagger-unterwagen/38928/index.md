---
layout: "image"
title: "Belastungstest"
date: "2014-06-10T06:55:41"
picture: "Belastungstest.jpg"
weight: "15"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Raupenbagger", "Unterwagen", "Drehkranz", "Schleifring"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38928
- /detailsf4e4-2.html
imported:
- "2019"
_4images_image_id: "38928"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38928 -->
Hauptziel war es ja, den Unterwagen stabiler zu machen.
Meine erste Version machte die Grätsche, wenn der Oberwagen draufgebaut war.
Bevor ich alles zusammenbaue, habe ich einen Belastungstest gemacht.
Die grauen Steine auf dem Bild sind nur Hilfsmittel, stattdessen sind später die Kettenfahrwerke befestigt
Eine 1,5 kg Wasserflasche drückt den ganzen Wagen nur um ca. 2 mm durch. Ich denke, ich habe mein Ziel erreicht.
Allerdings...mein ebenfalls neu konstruierter Oberwagen bringt 2 kg auf die Waage.