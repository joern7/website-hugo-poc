---
layout: "image"
title: "Kettenantrieb ältere Variante"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen03.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38381
- /details23f7.html
imported:
- "2019"
_4images_image_id: "38381"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38381 -->
Vor-Version:
Diese Variante war bereits meine 3. oder 4. Version. Eigentlich war die Lösung ganz gut. Sehr stabil, gute Kraftübertragung.
Der Nachteil: Die Untersetzung reicht nicht aus. Die Ketten laufen zu schnell. Unter Belastung kommen die S-Motoren an ihre Grenze.
Außerdem sind die Bewegungen des Baggers nicht sehr realitätsnah, wenn er über den Boden flitzt.