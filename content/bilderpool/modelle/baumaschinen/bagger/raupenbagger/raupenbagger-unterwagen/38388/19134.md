---
layout: "comment"
hidden: true
title: "19134"
date: "2014-06-10T16:42:29"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
ok, die Z10 von TST kenne ich, mir war aber nicht bewusst, daß er die aus Rast-Z10 macht!
das wäre eine gute Idee, das Problem zu lösen.
Allerdings habe ich es erst mal vertagt. Den Unterwagen hab ich neu gemacht (hab neue Bilder gepostet). Den grauen Motor (alt/6V, übersteuert mit 9V) habe ich mit einem aktuellen 9V Minimotor ersetzt. Der scheint ein besseres Drehmoment zu haben. Außerdem habe ich den Drehkranz ersetzt mit einem, der leichter dreht. (bei fischerfriendsmen bestellen und dazu mitteilen: "extra leichtgängig für Motorbetrieb". Ich muß noch neu verdrahten, dann mache ich erste Tests. Ich hoffe, daß der Minimotor reicht, sonst würde es mein neues Konstruktionskonzept in Gefahr bringen!