---
layout: "image"
title: "Antriebskopf mit Zahnräder"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen06.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38384
- /details502b-2.html
imported:
- "2019"
_4images_image_id: "38384"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38384 -->
Alles montiert:
- Das Z20 ganz links treibt die Kette an
- Das Z10 dient zur Untersetzung und wird von der anderen Seite über die Kegelzahnräder/Kardangelenk angetrieben
- Dann kommen die Z15 als Kettenlaufräder
