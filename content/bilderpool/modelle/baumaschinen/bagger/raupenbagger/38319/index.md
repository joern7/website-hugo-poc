---
layout: "image"
title: "Seitenansicht"
date: "2014-02-21T20:18:35"
picture: "raupenbagger04.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38319
- /detailsdf40-3.html
imported:
- "2019"
_4images_image_id: "38319"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38319 -->
In der Seitenansicht sieht man die Proportionen des Baggers am Besten. Ich habe mich dabei an Bildern von echten Baggern orientiert, ohne wirklich nachzumessen. Es ist also kein echtes Scale-Modell, kommt aber ziemlich gut hin.
Der ganze Oberwagen ist eigentlich nur entstanden, um vernünftige Bilder machen zu können. Den habe ich auch ziemlich schnell zusammengebaut und kann mit Sicherheit noch verbessert werden.
Mein Schwerpunkt lag auf dem Unterwagen mit dem Drehkranz/Schleifring-Thema. 
Viel Arbeit und mehrere Anläufe habe ich in das Kettenfahrwerk gesteckt. Das werde ich noch detailliert in einem Unterverzeichnis vorstellen.
Es ist sehr filigran geworden, ist aber ziemlich stabil und funktioniert echt Klasse. Die Kette läuft sehr leichtgängig, so daß ich in dieser ersten Version den Bagger nur mit 2 S-Motoren bewegen kann