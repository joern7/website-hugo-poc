---
layout: "image"
title: "Unteransicht Unterwagen"
date: "2014-02-21T20:18:43"
picture: "raupenbagger12.jpg"
weight: "12"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38327
- /details5ef3.html
imported:
- "2019"
_4images_image_id: "38327"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:43"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38327 -->
Das Unterwagengestell besteht eigentlich nur aus Motoren und Getriebe.
In dieser Ausbaustufe habe ich 4 S-Motoren, wobei zur Zeit nur 2 in Betrieb sind. Ich werde diese noch mechanisch aneinanderkoppeln, um noch mehr Kraft auf die Kette zu bringen.
Leider gibt es kein Rastachsen-Getriebezahnrad, welches an beiden Seiten eine Anschlußmöglichkeit hat. Deswegen müsste ich dafür Metallachsen verwenden. Um von der Metallachse dann aber irgendwie wieder auf die Rastachsen zu kommen, muß ich noch was basteln.

In einem Unterverzeichnis zeige ich auch eine frühere Variante, die weniger Motoren hatte. Aber ich wollte in dem Modell zeigen, was alles geht.
