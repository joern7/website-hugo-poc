---
layout: "image"
title: "Gesamtansicht"
date: "2014-06-10T06:55:49"
picture: "mit_Baggerarm_und_Oberwagen.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Bagger", "Raiupenbagger", "Oberwagen"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38930
- /detailsc154.html
imported:
- "2019"
_4images_image_id: "38930"
_4images_cat_id: "2855"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38930 -->
Hier eine kleine Vorschau auf meinen neuen Raupenbagger Oberwagen. Der ist noch nicht ganz fertig mangels Zeit und noch ein paar fehlenden Teilen.
Der Baggerarm ist im Vergleich zur ersten Version nur geringfügig verändert.