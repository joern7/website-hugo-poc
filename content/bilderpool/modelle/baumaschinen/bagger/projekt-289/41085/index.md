---
layout: "image"
title: "E-Halle im Bau"
date: "2015-05-27T22:48:39"
picture: "WP_20141202_001.jpg"
weight: "8"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41085
- /detailsba18.html
imported:
- "2019"
_4images_image_id: "41085"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-27T22:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41085 -->
Hier die Halle kurz vor Fertigstellung und deren Einbau. Alles Original FT, NEU und unvergilbt. Sehr schwer, da so dranzukommen. Ich habe zuletzt eine Charge von 3000 Standardsteinen erworben (die allerletzten) für stramme 81 Cent pro Stück!!! Die Rechnung war 4 (VIER!!!)-stellig. Und meine liebe Frau ist alles andere als begeistert...hehe!
PS: Die Bauplatten sind lediglich mit Alufolie (eher Kartigrarphieblech beklebt)