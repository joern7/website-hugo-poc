---
layout: "image"
title: "E-Halle im Bau"
date: "2015-05-26T14:41:38"
picture: "WP_20141218_001.jpg"
weight: "3"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41039
- /details3f61.html
imported:
- "2019"
_4images_image_id: "41039"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41039 -->
Bislang das komplexeste - die E-Halle, die später im Ballastausleger eingesetzt wird