---
layout: "image"
title: "E-Halle mit Dach"
date: "2015-06-08T21:30:57"
picture: "WP_20150526_002.jpg"
weight: "10"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41151
- /details8ad0-2.html
imported:
- "2019"
_4images_image_id: "41151"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41151 -->
Das vordere starre Dachelement (im Bild rechts) und das mittlere bewegliche Schiebedach im Aufbau. Innen liegen bereits die beiden Antriebseinheiten für die Verschiebung. Das hintere Dachelement soll sich bis zum Ballastgebäude (unterhalb der Windenplattform) verschieben lassen.