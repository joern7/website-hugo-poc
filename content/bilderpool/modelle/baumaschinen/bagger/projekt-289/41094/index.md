---
layout: "image"
title: "Ballastausleger"
date: "2015-05-29T21:34:14"
picture: "WP_20141020_014.jpg"
weight: "9"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41094
- /detailsa490-2.html
imported:
- "2019"
_4images_image_id: "41094"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-29T21:34:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41094 -->
Das also ist die erste von insgesamt 7 Baugruppen. Sie allein ist so groß im Original, das es nicht leicht ist, alles auf ein Foto zu bekommen. Im Modell ist sie 180 cm lang. Man sieht die Geometrie und die Anbauten wie E-Halle, Windenplattform, Begehung u.s.w.
Aus statischen Gründen kommen an die Knotenpunkte (im FT-Raster mit 7,5 Grad -Winkel - Schritten) wie im Original auch Knotenpunktbleche aus 2 mm Aluminium. Diese verdecken dann überwiegend die roten Winkelsteine und Gelenksteine.