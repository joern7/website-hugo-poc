---
layout: "comment"
hidden: true
title: "9826"
date: "2009-09-10T22:31:09"
uploadBy:
- "Claus"
license: "unknown"
imported:
- "2019"
---
Detailansicht des Seiltrommeln-Getriebes. Hier noch zu erwähnen ist das Differenzialgetriebe. Dieses ist mit Metallachsen ausgestattet, um die Stabilität zu erhohen. Die Achsen sind spezielle Anfertigungen von TST.

Claus