---
layout: "comment"
hidden: true
title: "9835"
date: "2009-09-11T15:41:26"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Ich vermute, dass die Übertragung erst mit einem Rastkegelrad (heisst das so? Eben die kleinen, mit denen man zwei senkrecht zueinander stehende Achsen verbinden kann) auf ein Z10 (kann man auf dem Bild schon erahnen) geht. Dieses Z10 treibt dann das Z20 an.
Wenn das stimmen sollte: Läuft die Kraftübertragung gut? bei kleinen Zahnrädern und großen Kräften hatte ich schon oft Probleme.