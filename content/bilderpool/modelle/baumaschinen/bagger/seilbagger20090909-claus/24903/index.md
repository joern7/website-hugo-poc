---
layout: "image"
title: "[4/4] Unteransicht"
date: "2009-09-10T21:27:15"
picture: "seilbaggerclaus4.jpg"
weight: "4"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24903
- /details462b.html
imported:
- "2019"
_4images_image_id: "24903"
_4images_cat_id: "1715"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24903 -->
