---
layout: "image"
title: "[1/4] Diagonalansicht links"
date: "2009-09-10T21:27:15"
picture: "seilbaggerclaus1.jpg"
weight: "1"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24900
- /details6dd2.html
imported:
- "2019"
_4images_image_id: "24900"
_4images_cat_id: "1715"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24900 -->
