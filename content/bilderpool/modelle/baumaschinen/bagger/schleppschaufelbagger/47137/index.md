---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr30.jpg"
weight: "29"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47137
- /detailsb5f7-2.html
imported:
- "2019"
_4images_image_id: "47137"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47137 -->
technik im detail (drehwerk und laufantrieb) um arbeiten (baggern) zu können werden die füße immer hochgeklappt