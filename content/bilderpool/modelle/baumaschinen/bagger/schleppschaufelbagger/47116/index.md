---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:23"
picture: "bagegr09.jpg"
weight: "8"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47116
- /details0b3e.html
imported:
- "2019"
_4images_image_id: "47116"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47116 -->
rolle um den hauptmast anzuheben