---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr17.jpg"
weight: "16"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47124
- /details98d2-3.html
imported:
- "2019"
_4images_image_id: "47124"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47124 -->
heckpartie des baggers