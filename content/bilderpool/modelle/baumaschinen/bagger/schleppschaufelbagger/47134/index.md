---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr27.jpg"
weight: "26"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47134
- /details08cd.html
imported:
- "2019"
_4images_image_id: "47134"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47134 -->
blick in die schaufel