---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr11.jpg"
weight: "10"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47118
- /detailsb8de-2.html
imported:
- "2019"
_4images_image_id: "47118"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47118 -->
ich finde eine schöne Seilführung. kraftvoll ist sie außerdem auch noch