---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr13.jpg"
weight: "12"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47120
- /details8009.html
imported:
- "2019"
_4images_image_id: "47120"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47120 -->
schöne draufsicht. leider kann ich den bagger nicht im ganzen fotografieren. Mein zimmer oder die kamera ist zu klein.