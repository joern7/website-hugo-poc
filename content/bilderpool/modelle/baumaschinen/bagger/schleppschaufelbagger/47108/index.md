---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:23"
picture: "bagegr01.jpg"
weight: "1"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47108
- /details27a8-2.html
imported:
- "2019"
_4images_image_id: "47108"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47108 -->
ein kräftiges rollensystem an der schaufel sorgt für vile hubkraft