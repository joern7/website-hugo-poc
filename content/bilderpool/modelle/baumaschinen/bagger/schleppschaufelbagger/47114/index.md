---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:23"
picture: "bagegr07.jpg"
weight: "6"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47114
- /details9a56.html
imported:
- "2019"
_4images_image_id: "47114"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47114 -->
die zwei masten