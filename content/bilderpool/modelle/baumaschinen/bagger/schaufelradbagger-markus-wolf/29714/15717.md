---
layout: "comment"
hidden: true
title: "15717"
date: "2011-11-19T11:35:14"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Super! Ubernehmen die Rollen die radiale Lagerung? Wie werden die Momenten über die horizontale Asche aufgenommen? Ich meine: alle Bausteinen des Kranzes haben die Nuten in vertikale Richtung. Das Teil wird nur kraftschlussig im Form gehalten. Wie hast du das geschafft?

Tolle Lösung für den Antrieb! Ich werde mich das behalten für mein eigenes Drehkranzprojekt!