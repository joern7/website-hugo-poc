---
layout: "image"
title: "schaufelradbagger12.jpg"
date: "2011-01-17T21:34:58"
picture: "schaufelradbagger12.jpg"
weight: "12"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29714
- /detailsc61a-2.html
imported:
- "2019"
_4images_image_id: "29714"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29714 -->
Die Scheibe besteht aus 15 mm Sperrholz und hat eine umlaufende angefaste PVC Kante. Ein Rollenkranz oben und unten .Angetrieben wird das ganze von 2 Getriebemotoren die mittels Gummizug an die umlaufende Kette gedrückt werden.