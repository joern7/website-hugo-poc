---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger18.jpg"
weight: "31"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33562
- /details3b18-3.html
imported:
- "2019"
_4images_image_id: "33562"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33562 -->
Der Ständer mit der Rollenkranzaufnahme von unten betrachtet.
In der Mitte das Loch wo das Material durch den Trichter aufs Förderband fällt.