---
layout: "image"
title: "Löffel mit seilzug"
date: "2006-06-24T19:38:11"
picture: "ft_lffelbagger_002.jpg"
weight: "2"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/6575
- /details44fb-2.html
imported:
- "2019"
_4images_image_id: "6575"
_4images_cat_id: "567"
_4images_user_id: "368"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6575 -->
