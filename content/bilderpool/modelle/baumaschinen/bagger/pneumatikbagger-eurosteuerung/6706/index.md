---
layout: "image"
title: "Lenkung"
date: "2006-08-21T19:42:50"
picture: "IMG_0651.jpg"
weight: "49"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/6706
- /detailsf450.html
imported:
- "2019"
_4images_image_id: "6706"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-08-21T19:42:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6706 -->
Zwar nicht die eleganteste Version zum Lenken, aber was besseres ist mir nicht eigefallen...