---
layout: "image"
title: "Neue Zylinder"
date: "2006-06-05T00:37:13"
picture: "Bagger1.jpg"
weight: "45"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/6421
- /details80d6.html
imported:
- "2019"
_4images_image_id: "6421"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-06-05T00:37:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6421 -->
Ein im Durchmesser abgedrehter Zylinder mit schwarzen Kappen, da sonst die Dichtungsabdeckung weiß war (Baumarktteil) mußte ich mir hier selber was drehen. Macht nur was fürs Auge her, von der Funktion bleibt es beim Alten.

Das ist ein Zylinder mit 20mm Kolben und einem Aussendurchmesser von 22mm.