---
layout: "image"
title: "Messingrohre"
date: "2004-01-31T16:30:23"
picture: "IMG_0515.jpg"
weight: "33"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2092
- /details65a8.html
imported:
- "2019"
_4images_image_id: "2092"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2004-01-31T16:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2092 -->
Umbau von Schlauchleitungen auf Rohrleitungen wie beim Original.