---
layout: "image"
title: "Gesamtansicht"
date: "2006-03-10T13:56:03"
picture: "IMG_0288.jpg"
weight: "44"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5848
- /details9d9a.html
imported:
- "2019"
_4images_image_id: "5848"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-03-10T13:56:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5848 -->
Es fehlen noch die Zylinder, dann sollte alles wieder beim Alten sein, nur besser.