---
layout: "image"
title: "Kraft"
date: "2004-01-20T20:34:30"
picture: "IMG_0494.jpg"
weight: "31"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2074
- /details0564.html
imported:
- "2019"
_4images_image_id: "2074"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2004-01-20T20:34:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2074 -->
Man könnte meinen dies Bild sei gestellt, aber dank der tollen Zylinder von Harald und dem guten Kompressor ist es möglich das der Bagger sich selber hochdrückt.
Dies tun Baggerfahrer z.B. wenn sie auf engem Raum wenden wollen. Also Bagger nach oben drücken und dann links oder rechts schwenken.