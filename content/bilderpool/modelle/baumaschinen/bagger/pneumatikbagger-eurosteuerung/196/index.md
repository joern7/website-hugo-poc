---
layout: "image"
title: "IMG 0378"
date: "2003-04-21T19:48:18"
picture: "IMG_0378.jpg"
weight: "7"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/196
- /details8f7e.html
imported:
- "2019"
_4images_image_id: "196"
_4images_cat_id: "23"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=196 -->
