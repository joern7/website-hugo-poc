---
layout: "image"
title: "Ausleger"
date: "2005-02-16T13:43:43"
picture: "Ausleger.jpg"
weight: "36"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/3641
- /detailsd67d-2.html
imported:
- "2019"
_4images_image_id: "3641"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2005-02-16T13:43:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3641 -->
So sieht das Ganze mittlerweilen mit den neuen Zylindern aus.