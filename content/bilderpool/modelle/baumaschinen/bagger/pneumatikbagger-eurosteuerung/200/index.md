---
layout: "image"
title: "IMG 0384"
date: "2003-04-21T19:48:18"
picture: "IMG_0384.jpg"
weight: "11"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/200
- /detailsadc4.html
imported:
- "2019"
_4images_image_id: "200"
_4images_cat_id: "23"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=200 -->
