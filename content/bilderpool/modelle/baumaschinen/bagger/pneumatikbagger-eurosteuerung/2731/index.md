---
layout: "image"
title: "Einbau der Lemo-Pumpe"
date: "2004-10-20T20:12:00"
picture: "IMG_0959ftc.jpg"
weight: "35"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2731
- /details47ec.html
imported:
- "2019"
_4images_image_id: "2731"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2004-10-20T20:12:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2731 -->
So passen zwei Airtanks rein und die Pumpe hat ein wirklich tolle Leistung. Festgemacht habe ich sie an einer Winkelachse, dazu muß man die an der Pumpe befindlichen Ösen ein wenig weiten, das geht aber Problemlos.