---
layout: "image"
title: "Ich habe fertig...."
date: "2006-08-20T14:06:32"
picture: "Bagger.jpg"
weight: "46"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/6700
- /details66df.html
imported:
- "2019"
_4images_image_id: "6700"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-08-20T14:06:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6700 -->
Leider ist er ohne den geplanten Allradantrieb, aber ansonsten ganz gut gelungen, oder?

Einzelbilder zur Technik kommen noch.