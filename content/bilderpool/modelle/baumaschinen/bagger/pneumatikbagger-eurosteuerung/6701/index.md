---
layout: "image"
title: "Technik"
date: "2006-08-20T20:05:17"
picture: "Bagger_2.jpg"
weight: "47"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/6701
- /details8945-2.html
imported:
- "2019"
_4images_image_id: "6701"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-08-20T20:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6701 -->
Zu sehen ist der Steuerblock mit den Modellbauservos und den Ventilen. Die schwarzen Kunststoffhülsen sind zugeklebt und dienen als Luftspeicher, drunter befindet sich der Fahrtregler mit dem ich das Schwenkwerk steuere.

Der IR Empfänger am Fahrwerk steuert den Fahrmotor, das Schiebeschild und die Lenkung, weiterhin ist am Fahrwerk der Accu und die Stromübertragung zum Oberwagen übernimmt ein Schleifring von Harald.