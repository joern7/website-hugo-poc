---
layout: "image"
title: "Ausleger oben"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger12.jpg"
weight: "16"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39036
- /detailsb8fd.html
imported:
- "2019"
_4images_image_id: "39036"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39036 -->
Etwas unrealistisch...