---
layout: "image"
title: "Gesamtansicht"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger07.jpg"
weight: "11"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39031
- /details9178.html
imported:
- "2019"
_4images_image_id: "39031"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39031 -->
