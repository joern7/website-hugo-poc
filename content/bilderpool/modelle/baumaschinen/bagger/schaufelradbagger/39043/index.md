---
layout: "image"
title: "Schaufelrad"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger19.jpg"
weight: "23"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39043
- /details0072-2.html
imported:
- "2019"
_4images_image_id: "39043"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39043 -->
Komplett ohne "Spezialteile"