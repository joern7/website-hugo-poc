---
layout: "image"
title: "Bagger aus Vogelperspektive"
date: "2014-05-02T16:40:08"
picture: "Bagger1.jpg"
weight: "1"
konstrukteure: 
- "majus"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/38738
- /details5cd8.html
imported:
- "2019"
_4images_image_id: "38738"
_4images_cat_id: "2893"
_4images_user_id: "1239"
_4images_image_date: "2014-05-02T16:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38738 -->
Mein selbst entworfener Bagger nach dem Vorbild des Liebherr Litronic 974. 

Folgende Funktionen sind voll funktionstüchtig:
- Schaufel bewegen
- Arm heben und senken
- Oberer Arm heben und senken
- Drehen nach rechts und links
- Vorwärts/Rückwärts fahren
- Rechts fahren (auf der Stelle drehen)
- Links fahren (auf der Stelle drehen)

Das Interface bildet die Fernsteuerung, das Extension ist im Bagger verbaut und steuert die Motoren und Zylinder. Die Kabel hinten am Bagger sind das Datenkabel von der Fernsteuerung, Druckluft und zwei Kabel, die mangels nur 4 Motorenausgänge am Interface angeschlossen sind.