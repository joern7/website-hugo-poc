---
layout: "image"
title: "Raupenfahrwerk (1)"
date: "2014-07-11T14:18:05"
picture: "pbagger1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39011
- /detailsf9fe-2.html
imported:
- "2019"
_4images_image_id: "39011"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39011 -->
Er fährt vorwärts und rückwärts, dahin, wohin das Gelände ihn treibt, denn zu einer Lenkung hat es nicht gereicht.