---
layout: "image"
title: "unterer Anschlag"
date: "2014-07-11T14:18:06"
picture: "pbagger8.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39018
- /detailsf66a.html
imported:
- "2019"
_4images_image_id: "39018"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39018 -->
Das Loch kann in der Tiefe gut die Länge eines U-Trägers ausmachen.