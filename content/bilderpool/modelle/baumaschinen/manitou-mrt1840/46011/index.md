---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0914.jpg"
weight: "14"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/46011
- /details01c6-2.html
imported:
- "2019"
_4images_image_id: "46011"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46011 -->
Achter aanzicht.
Geheel is redelijk in evenwicht. Er zit geen contra gewicht in de kont. De draaikrans (speciaal) is totaal spelings vrij. Er hangt en buigt niks door, in welke stand de mast ook staat.