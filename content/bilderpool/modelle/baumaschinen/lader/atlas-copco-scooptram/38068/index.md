---
layout: "image"
title: "Sturen"
date: "2014-01-13T13:02:35"
picture: "P1130046.jpg"
weight: "11"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38068
- /detailscf7c.html
imported:
- "2019"
_4images_image_id: "38068"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38068 -->
Dit is zo'n zwarte freewheel naaf voorzien van een M4 helicoil. Die trekt het M4 draadeind heen en weer voor het sturen. Dit kan veel kracht genereren daar het model best wel zwaar is. Sturen gaat zeer goed zelfs wanneer het model stilstaat.