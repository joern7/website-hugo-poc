---
layout: "image"
title: "Demontabel"
date: "2014-01-13T13:02:35"
picture: "P1130044.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38066
- /detailsef39.html
imported:
- "2019"
_4images_image_id: "38066"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38066 -->
Zoals het hoort kan de gehele bovenkant eraf