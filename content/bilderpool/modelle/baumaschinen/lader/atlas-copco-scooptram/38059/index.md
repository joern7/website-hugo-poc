---
layout: "image"
title: "Zijaanzicht"
date: "2014-01-13T13:02:35"
picture: "P1130037.jpg"
weight: "2"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38059
- /details1fd9.html
imported:
- "2019"
_4images_image_id: "38059"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38059 -->
Voorzien van 2x XM motor voor het rijden. 1x Powermotor 9:1 voor het sturen en 2x een powermotor 20:1 voor het heffen en neigen.