---
layout: "image"
title: "Detail foto lader frame"
date: "2014-01-13T13:02:35"
picture: "P1130042.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38064
- /details72f5.html
imported:
- "2019"
_4images_image_id: "38064"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38064 -->
4 WD plus eindvertraging. Het zicht op de hef motor. Alles is voorzien van kogellagers.