---
layout: "image"
title: "Radlader 6"
date: "2007-07-15T17:49:00"
picture: "radladerstefanl06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11089
- /details6041.html
imported:
- "2019"
_4images_image_id: "11089"
_4images_cat_id: "1005"
_4images_user_id: "502"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11089 -->
