---
layout: "image"
title: "Radlader 1"
date: "2007-07-15T17:49:00"
picture: "radladerstefanl01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11084
- /details3a9b.html
imported:
- "2019"
_4images_image_id: "11084"
_4images_cat_id: "1005"
_4images_user_id: "502"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11084 -->
Alle Funktionen sind motorisiert. Die Schaufel hab ich selbst gebaut. Mal ein paar Daten: Breite der Schaufel: 30cm; Gesamtlänge mit Schaufel: 70cm; Breite des Fahrzeugs: 24cm