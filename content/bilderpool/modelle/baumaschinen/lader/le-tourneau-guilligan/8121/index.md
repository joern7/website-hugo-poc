---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:39:46"
picture: "baumaschinen07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8121
- /details0c1e.html
imported:
- "2019"
_4images_image_id: "8121"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:39:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8121 -->
Schaufel