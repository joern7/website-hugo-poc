---
layout: "image"
title: "wielen"
date: "2011-04-30T20:58:18"
picture: "950h_011.jpg"
weight: "28"
konstrukteure: 
- "Ruurd"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30510
- /detailsb996.html
imported:
- "2019"
_4images_image_id: "30510"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-30T20:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30510 -->
Het voorste bouwsel voor de wiel bevestiging is beter dan de achterste