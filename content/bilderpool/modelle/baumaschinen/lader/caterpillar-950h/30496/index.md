---
layout: "image"
title: "onderaanzicht"
date: "2011-04-27T22:02:43"
picture: "pivot_015.jpg"
weight: "14"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30496
- /detailsd3b7.html
imported:
- "2019"
_4images_image_id: "30496"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-27T22:02:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30496 -->
Zicht op de onderkant, meeste aluminium vanwege de sterkte