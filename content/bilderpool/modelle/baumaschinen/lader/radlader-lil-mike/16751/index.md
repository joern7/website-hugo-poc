---
layout: "image"
title: "lenkung"
date: "2008-12-26T21:36:39"
picture: "radlader06.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/16751
- /details7d19.html
imported:
- "2019"
_4images_image_id: "16751"
_4images_cat_id: "1514"
_4images_user_id: "822"
_4images_image_date: "2008-12-26T21:36:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16751 -->
zu sehen sind:
links die leiter und rechts der antriebsmotor für die schnecke, die den drehkranz bewegt.