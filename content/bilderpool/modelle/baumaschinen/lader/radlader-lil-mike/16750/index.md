---
layout: "image"
title: "von hinten"
date: "2008-12-26T21:36:39"
picture: "radlader05.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/16750
- /details5735.html
imported:
- "2019"
_4images_image_id: "16750"
_4images_cat_id: "1514"
_4images_user_id: "822"
_4images_image_date: "2008-12-26T21:36:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16750 -->
in der mitte deutlich zu sehen ist die seilwinde die von einen motor angetrieben wird.
rechts und links habe ich wiederum LED's angebracht.