---
layout: "image"
title: "beleuchtung (vorn)"
date: "2008-12-26T21:36:39"
picture: "radlader03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/16748
- /details072b.html
imported:
- "2019"
_4images_image_id: "16748"
_4images_cat_id: "1514"
_4images_user_id: "822"
_4images_image_date: "2008-12-26T21:36:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16748 -->
das kleine weiße in der mitte ist eine LED die ich in einen Reedkontakthalter eingeklebt habe.