---
layout: "image"
title: "teleskoplader"
date: "2008-12-07T01:33:30"
picture: "DSC01022.jpg"
weight: "6"
konstrukteure: 
- "lil mike"
fotografen:
- "lil mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/16565
- /detailsbbae.html
imported:
- "2019"
_4images_image_id: "16565"
_4images_cat_id: "1554"
_4images_user_id: "822"
_4images_image_date: "2008-12-07T01:33:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16565 -->
