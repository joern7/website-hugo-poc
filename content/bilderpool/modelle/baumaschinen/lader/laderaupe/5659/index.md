---
layout: "image"
title: "von vorne"
date: "2006-01-25T16:42:58"
picture: "DSCN0562.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5659
- /details1d73.html
imported:
- "2019"
_4images_image_id: "5659"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5659 -->
Zu erkennen die beiden Schnecken die den Ladearm heben und senken und der Powermotor für die Schaufel.