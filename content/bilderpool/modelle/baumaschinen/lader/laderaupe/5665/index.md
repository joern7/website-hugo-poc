---
layout: "image"
title: "Ladearm in Aktion (1)"
date: "2006-01-25T16:42:58"
picture: "DSCN0566.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5665
- /details343c.html
imported:
- "2019"
_4images_image_id: "5665"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5665 -->
na ja, die Schaufel habe ich mir aus Pappe gebaut weil es keine entsprechende Größe von ft gibt.