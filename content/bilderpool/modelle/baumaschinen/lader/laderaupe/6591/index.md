---
layout: "image"
title: "neu überabeitet (3)"
date: "2006-07-01T21:32:21"
picture: "DSCN0827.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6591
- /detailsa119.html
imported:
- "2019"
_4images_image_id: "6591"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-07-01T21:32:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6591 -->
... und noch mal im Detail