---
layout: "image"
title: "neu überabeitet (1)"
date: "2006-07-01T21:32:21"
picture: "DSCN0824.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6589
- /detailsd16c.html
imported:
- "2019"
_4images_image_id: "6589"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-07-01T21:32:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6589 -->
Auch an alten "Baustellen" wird noch gearbeitet. Ich habe jetzt endlich diesen dicken Power Motor gegen einen "schöneren" ersetzen können.
Das Gehäuse und der Adapter sind von Andreas "TST".

Die Ladeschaufel wird auch bald geändert. Wahrscheinlich in schwarz.