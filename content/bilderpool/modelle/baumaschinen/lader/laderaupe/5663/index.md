---
layout: "image"
title: "Hebe Motor (3)"
date: "2006-01-25T16:42:58"
picture: "DSCN0545.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5663
- /details8b03.html
imported:
- "2019"
_4images_image_id: "5663"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5663 -->
In die Bausteine 30 (mit Loch) wird der Hebemotor eingesetzt und etwas nach hinten verschoben. So bildet sich dann mit dem 31436 ein Drehpunkt.