---
layout: "image"
title: "Fahrer"
date: "2006-01-25T16:43:07"
picture: "DSCN0585.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5673
- /detailsb527.html
imported:
- "2019"
_4images_image_id: "5673"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5673 -->
Eigentlich sollte er das Ding fahren. Aber als ich die Mechanik für den Aufreißer und das Dach montiert hatte war kein Platz mehr da.