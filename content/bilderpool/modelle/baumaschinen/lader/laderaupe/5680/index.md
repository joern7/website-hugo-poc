---
layout: "image"
title: "Schaufelsteuerung (2)"
date: "2006-01-26T16:04:20"
picture: "DSCN0604.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5680
- /detailsbbaf-2.html
imported:
- "2019"
_4images_image_id: "5680"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-26T16:04:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5680 -->
Hier noch mal im Detail