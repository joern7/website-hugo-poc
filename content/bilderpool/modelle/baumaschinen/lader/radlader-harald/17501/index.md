---
layout: "image"
title: "ExplorerMk2-25.JPG"
date: "2009-02-24T11:20:11"
picture: "ExplorerMk2-25.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17501
- /details511a.html
imported:
- "2019"
_4images_image_id: "17501"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:20:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17501 -->
Die Schaufel hebt gerade eben einen PowerMotor als Ladung, bei zweien reicht die Kraft nicht mehr.