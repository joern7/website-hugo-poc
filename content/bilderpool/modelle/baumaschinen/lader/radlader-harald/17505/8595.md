---
layout: "comment"
hidden: true
title: "8595"
date: "2009-02-25T16:02:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas, 
Naja, ich habe wohlweislich bisher nur die Schoko-Seite fotografiert. Das Heck ist noch nicht so weit...

Richtig, die Idee von Reus stand hier Pate. Gegen Bruch sollen eigentlich die Federn dienen, so dass der Servo immer da hin kann, wo er möchte, während Federn und Taster den P-Motor nachführen.

Hmm - ist der Plastikgetriebe-Servo wirklich schon von gestern? Ich mein, ich hätte noch keine Zusage von ft gehört, dass sie was stabileres machen wollen.

Gruß,
Harald