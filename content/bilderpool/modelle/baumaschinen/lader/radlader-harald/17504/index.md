---
layout: "image"
title: "ExplorerMk2-30.JPG"
date: "2009-02-24T11:30:48"
picture: "ExplorerMk2-30.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Servolenkung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17504
- /details5f7f.html
imported:
- "2019"
_4images_image_id: "17504"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:30:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17504 -->
Das ist die Servolenkung.
Genau genommen ist es eine Servo-Servolenkung :-)
Der kleine ft-Servo ist natürlich viel zu schmächtig, um die Knicklenkung zu bewegen. Deshalb steuert er nur die Minitaster, die mit der Vorderachse drehbar gelagert sind. Die Minitaster schalten den P-Motor, dessen erste Schnecke unten links im Bild zu sehen ist.

Die Federn sind wichtig, um Bruch und Überlast zu vermeiden, weil der Servo schneller drehen möchte als der P-Motor kann.