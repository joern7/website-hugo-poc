---
layout: "image"
title: "Caterpillar-950H-2.0  mit Dreh-Zylinder und -Antrieb Info"
date: "2015-01-24T21:42:56"
picture: "caterpillar16.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40412
- /details0184.html
imported:
- "2019"
_4images_image_id: "40412"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40412 -->
Schaal 1 : 14

Dreh-Zylinder Antrieb Alternativen
Schau mal :     http://www.ftcommunity.de/details.php?image_id=39290

Conrad-art.nrs: 
297313 Messing buis buitenØ5,0 binnenØ4,1 mm,  500mm 
221795 Messing buis buitenØ6,0 binnenØ4,0 mm,  500mm 
297321-62 Messing-Rohr-Profil  6 mm buiten + 5.1 mm binnen , 500 mm
225410 Stelringset 4mm (10 stuks) 
225428 Stelringset 5mm (10 stuks) 
225436 Stelringset 6mm (10 stuks) 
295612 KOGELLAGER inw. 6mm x 12 x 4 
RVS-Draadeinden :           https://www.rvspaleis.nl/
221786 Draadeind 500mm M4 Messing 
221787 Draadeind 500mm M5 Messing 
183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm 
Adapter 4mm auf 5mm Andreas Tacke Münster. Ich mache davon M4 auf 5mm.

Interessante sehr kleine (10 x 12 x26mm) Getriebemotoren passend im FT-Raster gibt es bei : 

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1 

http://www.pololu.com/product/2218 

Overview 

These tiny brushed DC gearmotors are intended for use at 6 V, though in general, these kinds of motors can run at voltages above and below this nominal voltage, so they should comfortably operate in the 3 - 9 V range (rotation can start at voltages as low as 0.5 V). Lower voltages might not be practical, and higher voltages could start negatively affecting the life of the motor. The micro metal gearmotors are available in a wide range of gear ratios-from 5:1 up to 1000:1-and offer a choice between three different motors: high-power (HP), medium-power (MP), and standard. With the exception of the 1000:1 gear ratio versions, all of the micro metal gearmotors have the same physical dimensions, so one version can be easily swapped for another if your design requirements change. 

Ook interessant in 12V -uitvoering (levering vanuit Frankrijk) :
http://www.robotshop.com/eu/en/12v-460rpm-20oz-in-micro-gearmotor.html
 
12 6 - 12 5:1 4900  RB-Sct-805 Servo City 
12 6 - 12 10:1 2600  RB-Sct-553 Servo City 
12 6 - 12 30:1 900  RB-Sct-749 Servo City 
12 6 - 12 50:1 460  RB-Sct-773 Servo City 
12 6 - 12 100:1 270  RB-Sct-722 Servo City 
12 6 - 12 150:1 175  RB-Sct-171 Servo City 
12 6 - 12 210:1 130  RB-Sct-810 Servo City 
12 6 - 12 250:1 110  RB-Sct-697 Servo City 
12 6 - 12 298:1 90  RB-Sct-169 Servo City