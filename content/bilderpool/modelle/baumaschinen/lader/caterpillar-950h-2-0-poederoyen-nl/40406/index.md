---
layout: "image"
title: "Caterpillar-950H-2.0  -onder met pendelende achteras en 4 wielaandrijving"
date: "2015-01-24T21:42:56"
picture: "caterpillar10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40406
- /detailsc42e.html
imported:
- "2019"
_4images_image_id: "40406"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40406 -->
pendelende achteras en 4 wielaandrijving

Voor de pendelende achteras (1), de laadbak (2) en de stuur-unit -onder en boven (3) heb ik totaal 4st Bodenplatte Führerhaus 75x90  (31889) gebruikt.