---
layout: "image"
title: "Caterpillar-950H-2.0  mit Dreh-Zylinder   -fertig"
date: "2015-01-24T21:42:56"
picture: "caterpillar15.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40411
- /details2c04.html
imported:
- "2019"
_4images_image_id: "40411"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40411 -->
Schaal 1 : 14

Voor de pendelende achteras (1), de laadbak (2) en de stuur-unit -onder en boven (3) heb ik totaal 4st Bodenplatte Führerhaus 75x90 (31889) gebruikt.


M1 = Drehzylinder- Antrieb -Laadbak  dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen
M2 = Drehzylinder-Antrieb -Giek  dmv 2x Schwarze XM-Motor + Ketten-Antrieb + M5 zum hoch und nach unten
M3 = Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4  zum Links und Rechts
Servo =  Fahr-Antrieb (2x Powermotor 20:1) mit Thor4 am servo-Ausgang zum Vor- und -Ruckwärts-gang