---
layout: "image"
title: "Drehzylinder Antrieb-Prinzip mit 10 x 12 x 26mm Getriebemotor Pololu geeignet für M4 oder M5"
date: "2015-01-25T20:02:28"
picture: "caterpillardetails11.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40423
- /details7fcc.html
imported:
- "2019"
_4images_image_id: "40423"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40423 -->
Messing Rohr geeignet für M4 oder M5


Zum Einbau der Getriebemotor ist interessant : 

U-Profile mit Streifen - grau 15x15mm 
http://www.ftcommunity.de/details.php?image_id=33987 

Diese Bauteile gibt es bei: 

LPE: 
http://lpe.lpe-b2b.de/index.php?page=product&info=2770


10 x 12 x26mm Getriebemotoren passend im FT-Raster gibt es bei : 

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1 

http://www.pololu.com/product/2218 

Overview 

These tiny brushed DC gearmotors are intended for use at 6 V, though in general, these kinds of motors can run at voltages above and below this nominal voltage, so they should comfortably operate in the 3 &#8211; 9 V range (rotation can start at voltages as low as 0.5 V). Lower voltages might not be practical, and higher voltages could start negatively affecting the life of the motor. The micro metal gearmotors are available in a wide range of gear ratios&#8212;from 5:1 up to 1000:1&#8212;and offer a choice between three different motors: high-power (HP), medium-power (MP), and standard. With the exception of the 1000:1 gear ratio versions, all of the micro metal gearmotors have the same physical dimensions, so one version can be easily swapped for another if your design requirements change.