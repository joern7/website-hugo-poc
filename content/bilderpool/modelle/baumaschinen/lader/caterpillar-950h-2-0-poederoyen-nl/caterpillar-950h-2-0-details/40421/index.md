---
layout: "image"
title: "Drehzylinder Antrieb-Prinzip mit 10 x 12 x 26mm Getriebemotor Pololu geeignet für M4 oder M5"
date: "2015-01-25T20:02:28"
picture: "caterpillardetails09.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40421
- /details758b-2.html
imported:
- "2019"
_4images_image_id: "40421"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40421 -->
Entfernungen wegen Zahnrad-10 notwendig

Z10 mit 6mm Bohrung