---
layout: "comment"
hidden: true
title: "21283"
date: "2015-11-13T14:55:32"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
De meest elegante combinatie bestaat uit een 4mm óf 5mm draadeinde met:
 
297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen, 500mm 

221795 Messing buis buiten Ø 6,0mm + binnenØ4,0 500mm    tbv 4mm draadeind
297321-62 Messing-Rohr-Profil 6 mm buiten + 5.1 mm binnen , 500 mm   tbv  5mm draadeind

225410 Stelringset 4mm (10 stuks) 
225428 Stelringset 5mm (10 stuks) 
225436 Stelringset 6mm (10 stuks) 

295612 KOGELLAGER inw. 6mm x 12 x 4 

RVS-Draadeinden : 
https://www.rvspaleis.nl/

221786 Draadeind 500mm M4 Messing 
221787 Draadeind 500mm M5 Messing 
221789 Draadeind 500 mm M6 Messing 

183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm 

Adapter 4mm auf 5mm Andreas Tacke Münster. 
Ich habe Andreas gefragt Adapter 3,3mm auf 5mm zu machen. Ich mache dann selbst davon M4 auf 5mm