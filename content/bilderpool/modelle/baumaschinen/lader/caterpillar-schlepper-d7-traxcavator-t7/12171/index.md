---
layout: "image"
title: "Fertig"
date: "2007-10-08T21:42:17"
picture: "DSCN1687.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12171
- /details957d.html
imported:
- "2019"
_4images_image_id: "12171"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-08T21:42:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12171 -->
