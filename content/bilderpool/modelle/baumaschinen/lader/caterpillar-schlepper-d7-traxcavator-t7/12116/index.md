---
layout: "image"
title: "Schaufel"
date: "2007-10-03T11:19:38"
picture: "DSCN1642.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12116
- /details57a1.html
imported:
- "2019"
_4images_image_id: "12116"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-03T11:19:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12116 -->
