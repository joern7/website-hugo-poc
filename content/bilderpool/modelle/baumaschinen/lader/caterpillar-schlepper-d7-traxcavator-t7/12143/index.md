---
layout: "image"
title: "Detail"
date: "2007-10-05T22:34:52"
picture: "DSCN1668.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12143
- /detailsb7d5.html
imported:
- "2019"
_4images_image_id: "12143"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-05T22:34:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12143 -->
Die Führung des Seiles, welches die Schaufel kippt, war gar nicht mal so einfach. Schließlich darf die Schaufel nicht kippen wenn der Arm gehoben wird. Deswegen muß das Seil so weit wie möglich parallel zum Arm geführt werden.