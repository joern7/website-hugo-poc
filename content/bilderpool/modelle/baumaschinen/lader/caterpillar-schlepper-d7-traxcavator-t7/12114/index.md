---
layout: "image"
title: "Schaufel"
date: "2007-10-03T11:19:38"
picture: "DSCN1641.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12114
- /details6212.html
imported:
- "2019"
_4images_image_id: "12114"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-03T11:19:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12114 -->
Die Maße der Schaufel zu benennen ist nicht so einfach.
Deswegen habe ich sie mit der roten 120ér ft Schaufel aufgenommen.