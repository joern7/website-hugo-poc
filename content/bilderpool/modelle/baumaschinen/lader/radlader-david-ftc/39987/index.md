---
layout: "image"
title: "Schaufel angehoben"
date: "2014-12-26T16:00:29"
picture: "rld3.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39987
- /details8a7c.html
imported:
- "2019"
_4images_image_id: "39987"
_4images_cat_id: "3006"
_4images_user_id: "2228"
_4images_image_date: "2014-12-26T16:00:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39987 -->
Die Schaufel kann wie beim Original angehoben und gekippt werden. Dies erfolgt durch Ventile am Heck des Fahrzeugs. Um die Schaufel zu kippen, habe ich eine Z-Kinematik verwendet.