---
layout: "image"
title: "Ansich von unten"
date: "2014-12-26T16:00:29"
picture: "rld5.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39989
- /detailsfdc2.html
imported:
- "2019"
_4images_image_id: "39989"
_4images_cat_id: "3006"
_4images_user_id: "2228"
_4images_image_date: "2014-12-26T16:00:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39989 -->
Gut zu erkennen ist hier der Allradantrieb: Die Kardanwelle wird durch ein Differential entlastet, damit sie nicht bricht. Zum Einsatz kommt das insbesondere dann, wenn der Radlader Steigungen überwinden muss.