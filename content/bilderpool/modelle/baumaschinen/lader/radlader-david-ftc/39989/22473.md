---
layout: "comment"
hidden: true
title: "22473"
date: "2016-08-27T17:19:39"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Zur Lenkung:
1. Ja, die Steuerung des Fahrzeugs (ohne Schaufel) erfolgt über das Control Set
2. Die Steuerung ist identisch mit der in meinem Modell "Dumper": https://ftcommunity.de/details.php?image_id=39170
 - durch Antippen des Joysticks nach links oder rechts schlägt der Lader ein kleines Stück nach links bzw. rechts ein
 - die Mittelstellung findet er meistens wieder von selber, ansonsten muss man kurz den Joystick antippen
das erklärt auch schon 3.: Zwischenstufen gibt es, allerdings ist nur der Maximaleinschlag auf beiden Seiten eindeutig festgelegt.

In der Praxis ist die Lenkung feinfühlig, aber nicht proportional wie man es von der Servolenkung gewohnt ist.

Gruß
David