---
layout: "image"
title: "000670"
date: "2008-03-20T15:18:00"
picture: "BILD0670.jpg"
weight: "14"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13976
- /details1a87.html
imported:
- "2019"
_4images_image_id: "13976"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13976 -->
