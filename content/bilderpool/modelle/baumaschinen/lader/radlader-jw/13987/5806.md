---
layout: "comment"
hidden: true
title: "5806"
date: "2008-03-21T11:25:55"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Nur mit Kabel und 230V. Ich habe zwar eine Graupner Fernsteuerung mit allem was man dazu benötigt, aber diese mit ft zu koppeln habe ich noch nicht in Erwägung gezogen.
Gruß Jürgen