---
layout: "image"
title: "000675"
date: "2008-03-20T15:18:00"
picture: "BILD0675.jpg"
weight: "16"
konstrukteure: 
- "JW"
fotografen:
- "JW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13978
- /details0a09.html
imported:
- "2019"
_4images_image_id: "13978"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13978 -->
Die Kinematik war schwerer als erwartet. Die Originalkinematik vom VOLVO habe ich nicht realisieren können.