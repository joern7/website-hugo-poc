---
layout: "image"
title: "00701"
date: "2008-03-20T15:23:41"
picture: "BILD0701.jpg"
weight: "24"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13986
- /details8ccf.html
imported:
- "2019"
_4images_image_id: "13986"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:23:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13986 -->
