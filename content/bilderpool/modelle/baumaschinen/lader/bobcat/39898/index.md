---
layout: "image"
title: "interieur"
date: "2014-12-08T17:05:15"
picture: "PC080199.jpg"
weight: "5"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39898
- /details5c45-2.html
imported:
- "2019"
_4images_image_id: "39898"
_4images_cat_id: "2997"
_4images_user_id: "838"
_4images_image_date: "2014-12-08T17:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39898 -->
Binnenkant cabine nu ook met stoel!!