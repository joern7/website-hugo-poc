---
layout: "image"
title: "Mini-Radlader 1"
date: "2009-03-29T19:07:17"
picture: "Radlader_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23543
- /details9aaa.html
imported:
- "2019"
_4images_image_id: "23543"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23543 -->
Hier mein kompakter ferngesteuerter Radlader, der alles das kann, was ein ferngesteuerter Radlader alles können muss:

- Antrieb an der Vorderachse per Mini-Motor
- Lenkung der Hinterachse per Servo
- Drucklufterzeugung durch Standard-FT-Kompressor
- Heben und Senken der Schaufel per motorisiertem Handventil mit Mini-Motor
- Kippen der Schaufel per motorisiertem Handventil mit Mini-Motor

Ein Hebel rechts neben dem Fahrersitz, der vom Fahrer nach vorn umgelegt werden kann, startet den Empfänger und den Kompressor, der immer läuft. Alles andere wird über die Fernsteuerung bedient.

Auf einen Druckluftspeicher habe ich aus zwei Gründen verzichtet: Die Optik des sehr kompakten Fahrzeugs hätte gelitten. Und außerdem mag ich bei derartigen Pneumatik-Anwendungen (Bagger, Lader, etc.), wenn die Zylinder langsam aus- und einfahren und ich das "Arbeiten" des Kompressors hören kann.