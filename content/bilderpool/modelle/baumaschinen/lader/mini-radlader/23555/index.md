---
layout: "image"
title: "Mini-Radlader 13"
date: "2009-03-29T19:07:24"
picture: "Radlader_13.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23555
- /detailsf980.html
imported:
- "2019"
_4images_image_id: "23555"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23555 -->
Für die kompakte Darstellung der unteren Lagerung der beiden Hebe-Zylinder habe ich je zwei Abstandsringe rot (31597) in eine Gelenkwürfel-Klaue rot (31436) gepresst. Funktioniert wunderbar und bietet mir oft eine Alternative zur klassischen Darstellung mit Lagerhülse schwarz (36819), da noch Kleinteile wie Klemmbuchsen 5 rot (37679) dazwischenpassen. Dadurch kann die Welle kurz gehalten werden.