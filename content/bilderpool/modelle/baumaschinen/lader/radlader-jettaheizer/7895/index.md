---
layout: "image"
title: "Lenkmechanik und Antrieb"
date: "2006-12-12T21:57:02"
picture: "Radlader10b.jpg"
weight: "10"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7895
- /detailsa5f2.html
imported:
- "2019"
_4images_image_id: "7895"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-12T21:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7895 -->
Am meisten stört mich diese Konstruktion aus zwei Winkelsteinen 60°, einem BS 7,5 und einem BS 5. Das will ich auf jeden Fall noch ändern.