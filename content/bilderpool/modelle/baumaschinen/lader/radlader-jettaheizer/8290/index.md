---
layout: "image"
title: "Radlader"
date: "2007-01-03T19:25:23"
picture: "Radlader21b.jpg"
weight: "21"
konstrukteure: 
- "Franz Osten"
fotografen:
- "Franz Osten"
keywords: ["Knicklenkung", "Allrad", "Monsterreifen", "Vorgelege"]
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8290
- /details162c.html
imported:
- "2019"
_4images_image_id: "8290"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8290 -->
So weit, so gut.
Endlich steht er auf seinen eigenen Rädern, auch wenn er noch lange nicht fertig ist.