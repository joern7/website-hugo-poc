---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader25b.jpg"
weight: "25"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8294
- /detailsa5ba-2.html
imported:
- "2019"
_4images_image_id: "8294"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8294 -->
Nochmal eine Detailansicht des Hinterachsantriebes. Oben die Welle, die vom oberen Mitteldifferential zu dem Gebilde auf dem vorherigen Bild führt. Unten die Welle vom zweiten Mitteldifferential, die über zwei Ketten (ging leider nicht anders, ich mußte irgendwie den Versatz nach vorne hinkriegen) zur zweiten Antriebswelle der Hinterachse läuft.