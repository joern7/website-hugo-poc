---
layout: "image"
title: "Antrieb neu"
date: "2006-12-13T21:55:01"
picture: "Radlader12b.jpg"
weight: "12"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7897
- /details1bd1.html
imported:
- "2019"
_4images_image_id: "7897"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-13T21:55:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7897 -->
Mußte zwar auch hier wieder mit BS 5 und BS 7,5 die Achslänge ausgleichen, aber das paßt so schon viel besser.