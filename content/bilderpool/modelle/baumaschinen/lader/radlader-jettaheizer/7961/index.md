---
layout: "image"
title: "Pendelhinterachse"
date: "2006-12-19T21:20:02"
picture: "Radlader19b.jpg"
weight: "19"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7961
- /detailsb8f2.html
imported:
- "2019"
_4images_image_id: "7961"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-19T21:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7961 -->
Die komplette Pendelachse mit Rädern (260mm Durchmesser) und noch nicht gekürzter Messingrohrachse, die hier irgendwie überhaupt nicht zu sehen ist...