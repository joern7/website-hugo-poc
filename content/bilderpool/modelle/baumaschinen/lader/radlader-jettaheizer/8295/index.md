---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader26b.jpg"
weight: "26"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8295
- /details5c44.html
imported:
- "2019"
_4images_image_id: "8295"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8295 -->
Detailaufnahme des Vorderachsantriebes, hintere Welle. Ging aus Platzgründen auch wieder nur über Kette. Rechts sieht man gerade noch so die beiden Drehkränze der Lenkmechanik.