---
layout: "image"
title: "Vorlage für den Radlader"
date: "2006-12-07T22:48:31"
picture: "Radlader05b.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7717
- /detailsc7b7-2.html
imported:
- "2019"
_4images_image_id: "7717"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-07T22:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7717 -->
Hier mal die Vorlage für den großen Radlader.
Ist schon ziemlich mitgenommen, vor allem die Arme der Schaufel hat´s schon fast komplett zerbröselt. Daher kann ich dieses Modell nur als ganz groben Anhaltspunkt nutzen.