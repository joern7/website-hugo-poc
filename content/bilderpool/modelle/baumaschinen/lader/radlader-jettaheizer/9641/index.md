---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:52"
picture: "Radlader64b.jpg"
weight: "64"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9641
- /detailse78c.html
imported:
- "2019"
_4images_image_id: "9641"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9641 -->
Hier jetzt wie schon länger versprochen ein paar Bilder vom hinteren Teil, so wie er jetzt im Moment aussieht und so, wie er hoffentlich auch wieder aussehen wird, wenn ich ihn zerlegt und das Differential getauscht hab...