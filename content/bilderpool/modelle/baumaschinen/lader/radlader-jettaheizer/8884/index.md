---
layout: "image"
title: "Pendelhinterachse neu"
date: "2007-02-04T19:01:19"
picture: "Radlader54b.jpg"
weight: "54"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8884
- /detailsce1b.html
imported:
- "2019"
_4images_image_id: "8884"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-04T19:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8884 -->
Und das ist die Unterseite. Die Z 40 verschwinden nachher hinter den großen Rädern.