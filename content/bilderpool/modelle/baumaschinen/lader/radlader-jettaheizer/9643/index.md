---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:53"
picture: "Radlader66b.jpg"
weight: "66"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9643
- /details1d59.html
imported:
- "2019"
_4images_image_id: "9643"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9643 -->
Die Flachträger 120 sind vorne und hinten mit S-Riegeln an Winkel- bzw. U-Trägern befestigt und in der Mitte zusätzlich mit je drei Streben 75 Loch verstärkt.