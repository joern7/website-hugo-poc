---
layout: "comment"
hidden: true
title: "1683"
date: "2006-12-08T22:25:06"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Jürgen,

so verrückt ist die Idee garnicht, ich habe gestern auch schon bei IhBäh nach Bollerwagen und diversen anderen Fahrzeugen geschaut... ;o)
Was mich dabei nur immer stört, ist das Profil oder das Material, viele der Räder sind nämlich aus Kunststoff.
Es gibt einen Tret-Traktor von Rolly mit Luftbereifung und passendem Profil, allerdings sind die Räder 310mm im Durchmesser. Das ist dann doch etwas zu groß.

Gruß,
Franz