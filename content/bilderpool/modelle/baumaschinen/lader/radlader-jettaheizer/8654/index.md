---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:37:35"
picture: "Radlader37b.jpg"
weight: "37"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8654
- /detailsd93c-3.html
imported:
- "2019"
_4images_image_id: "8654"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8654 -->
Die Arme der Schaufel, inzwischen aufgedoppelt und verstärkt. Obendrauf ist der Kippantrieb der Schaufel zu sehen