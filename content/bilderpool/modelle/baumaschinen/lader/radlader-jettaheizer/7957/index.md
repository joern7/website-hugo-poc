---
layout: "image"
title: "Pendelhinterachse"
date: "2006-12-19T21:20:01"
picture: "Radlader15b.jpg"
weight: "15"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7957
- /details90b7.html
imported:
- "2019"
_4images_image_id: "7957"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-19T21:20:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7957 -->
Hier die versprochenen Bilder der Pendelachse. Es fehlen nur noch zwei Rast-Z10 an den Antriebswellen, ich hoffe, die sind morgen da.