---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:27"
picture: "Radlader32b.jpg"
weight: "32"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8301
- /detailsd13a.html
imported:
- "2019"
_4images_image_id: "8301"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8301 -->
Hier nochmal die mittlerweile verkleinerte Schaufel im Größenvergleich. Ach ja, die Arme der Schaufel sind auch inzwischen 60mm kürzer. Schwer genug ist der ganze Kram aber immernoch.