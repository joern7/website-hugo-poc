---
layout: "comment"
hidden: true
title: "1862"
date: "2007-01-03T20:50:34"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
Hallo Franz,
tolle Arbeit. 
Was hällst Du davon, den Radlader mit Styropor zu verkleiden? Wiegt wenig und kostet wenig...

Gruß Jürgen Warwel (jw-stg)