---
layout: "image"
title: "Pendelhinterachse (Detail)"
date: "2006-12-19T21:20:02"
picture: "Radlader16b.jpg"
weight: "16"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7958
- /details200e.html
imported:
- "2019"
_4images_image_id: "7958"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-19T21:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7958 -->
Hier eine Detailaufnahme einer der Lager- und Antriebswellen. Um die Pendelachse ohne Teleskope zu realisieren, kam ich nicht umhin, die Antriebswellen auch gleich als Lagerwellen zu nutzen. Deswegen sind die beiden Wellen als einzige aus Metall.