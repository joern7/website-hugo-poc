---
layout: "image"
title: "Lenkmechanik und Antrieb"
date: "2006-12-12T21:57:01"
picture: "Radlader06b.jpg"
weight: "6"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7891
- /detailsf4fc.html
imported:
- "2019"
_4images_image_id: "7891"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-12T21:57:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7891 -->
Habe heute mein Power Motor Set bekommen und gleich mal den Antrieb konstruiert.
So ganz glücklich bin ich mit der Konstruktion allerdings noch nicht.