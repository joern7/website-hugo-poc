---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader28b.jpg"
weight: "28"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8297
- /details5c65.html
imported:
- "2019"
_4images_image_id: "8297"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8297 -->
Hier nochmal eine Detailaufnahme der vorderen Radaufhängung.