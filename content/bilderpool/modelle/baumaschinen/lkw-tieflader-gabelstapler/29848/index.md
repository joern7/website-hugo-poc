---
layout: "image"
title: "Der Auflieger"
date: "2011-02-03T19:50:41"
picture: "lkwmittiefladerundgabelstabler07.jpg"
weight: "7"
konstrukteure: 
- "Tim_Fischer"
fotografen:
- "Tim_Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tim_Fischer"
license: "unknown"
legacy_id:
- /php/details/29848
- /details242e.html
imported:
- "2019"
_4images_image_id: "29848"
_4images_cat_id: "2198"
_4images_user_id: "1244"
_4images_image_date: "2011-02-03T19:50:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29848 -->
Der Auflieger ist zum Beladen bereit .
