---
layout: "image"
title: "Gabelstabler + Schaufel"
date: "2011-02-03T19:51:17"
picture: "lkwmittiefladerundgabelstabler16.jpg"
weight: "16"
konstrukteure: 
- "Tim_Fischer"
fotografen:
- "Tim_Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tim_Fischer"
license: "unknown"
legacy_id:
- /php/details/29857
- /detailsc766.html
imported:
- "2019"
_4images_image_id: "29857"
_4images_cat_id: "2198"
_4images_user_id: "1244"
_4images_image_date: "2011-02-03T19:51:17"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29857 -->
