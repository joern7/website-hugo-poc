---
layout: "overview"
title: "Raupe mit Kran"
date: 2020-02-22T08:13:16+01:00
legacy_id:
- /php/categories/1537
- /categories497f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1537 --> 
Dies ist eine Raupe mit Kran und Licht (Schild kommt noch). Ich habe diese Raupe entwikelt weil im Set \"Powerbuldozers\" die raupe nicht funktionierte! Das liegt daran, dass die Powermotoren zu schwach sind. In dieser Raupe habe ich deswegen ein Schneckengetriebe eingebaut. Die Raupe fährt da zwar langsamer aber sie kann sich jetzt auch richtig auf der Stelle drehen.