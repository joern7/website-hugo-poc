---
layout: "image"
title: "Raupenhälfte oben"
date: "2009-01-22T21:34:21"
picture: "raupemitkran1.jpg"
weight: "1"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17122
- /detailsfb0d.html
imported:
- "2019"
_4images_image_id: "17122"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17122 -->
Die Raupe besteht aus 2 dieser Hälften (eine dvon spiegelverkehrt)!