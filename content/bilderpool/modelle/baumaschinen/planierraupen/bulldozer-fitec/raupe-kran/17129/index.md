---
layout: "image"
title: "Schaltplan"
date: "2009-01-22T21:34:22"
picture: "raupemitkran8.jpg"
weight: "8"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17129
- /details7fb3-3.html
imported:
- "2019"
_4images_image_id: "17129"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17129 -->
So habe ich die Motoren und Lampen verbunden.