---
layout: "image"
title: "Raupenhälfte unten"
date: "2009-01-22T21:34:22"
picture: "raupemitkran2.jpg"
weight: "2"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17123
- /detailsb6ad.html
imported:
- "2019"
_4images_image_id: "17123"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17123 -->
Die Raupe besteht aus 2 dieser Hälften (eine dvon spiegelverkehrt)!