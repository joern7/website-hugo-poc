---
layout: "image"
title: "Bulldozer Vorderansicht"
date: "2006-11-05T14:39:25"
picture: "Bulldozer1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7403
- /details8bf9.html
imported:
- "2019"
_4images_image_id: "7403"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7403 -->
Das ist ein Bulldozer. Er hat ein Schiebeschild das per M-Mot senkbar/hebbar ist. Außerdem hat er ein Gleichlaufgetriebe, welches man unter Kettenfahrzeuge/Gleichlaufgetriebe findet.