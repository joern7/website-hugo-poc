---
layout: "image"
title: "Schiebeschild"
date: "2006-11-05T14:39:25"
picture: "Bulldozer4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7406
- /detailsb891.html
imported:
- "2019"
_4images_image_id: "7406"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7406 -->
Hier sieht man den Antrieb vom Schiebeschild.