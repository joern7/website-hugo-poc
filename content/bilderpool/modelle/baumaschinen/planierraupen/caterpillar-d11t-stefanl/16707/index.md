---
layout: "image"
title: "Caterpillar D11T"
date: "2008-12-23T15:26:44"
picture: "caterpillardt6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/16707
- /detailsa3b8.html
imported:
- "2019"
_4images_image_id: "16707"
_4images_cat_id: "1512"
_4images_user_id: "502"
_4images_image_date: "2008-12-23T15:26:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16707 -->
Die beiden äußeren Motoren treiben die Ketten an, der links-unten den Aufreißer.