---
layout: "image"
title: "Caterpillar D11R Schaufel"
date: "2006-04-27T13:15:37"
picture: "10_Caterpillar_D11R_Schaufel_.jpg"
weight: "10"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6151
- /details5bb4.html
imported:
- "2019"
_4images_image_id: "6151"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6151 -->
