---
layout: "image"
title: "Caterpillar D11R Klaue detail"
date: "2006-04-27T13:15:36"
picture: "06_Caterpillar_D11R_klaue_detail.jpg"
weight: "6"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6147
- /details5d41.html
imported:
- "2019"
_4images_image_id: "6147"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6147 -->
