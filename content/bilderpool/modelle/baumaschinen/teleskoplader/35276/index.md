---
layout: "image"
title: "Teleskoplader006"
date: "2012-08-10T17:56:37"
picture: "teleskoplader06.jpg"
weight: "6"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35276
- /detailse7da.html
imported:
- "2019"
_4images_image_id: "35276"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35276 -->
Alle Lichter sind eingeschaltet. So kann auch in der Nacht gearbeitet werden.