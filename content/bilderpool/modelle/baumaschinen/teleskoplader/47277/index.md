---
layout: "image"
title: "Lenkung"
date: "2018-02-14T16:38:04"
picture: "telbly09.jpg"
weight: "31"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47277
- /detailsa739-2.html
imported:
- "2019"
_4images_image_id: "47277"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47277 -->
Streng genommen ist das ein Lenkparallelogramm. Die Verbindung des Servolenkhebel mit der Radaufhängung (Strebe 45) sowie die Parallelführung der Radaufhängungen ergeben nur angenähert ein gleichseitiges Dreieck, das vom Lenkeinschlag unabhängis ist. Aufgrund des relativ hohen Gewicht des Fahrzeugs und der großen Reibung der Räder mit dem Boden muss die Radaufhängung stabil aufgebaut sein, sodass sich die Achse lenken lässt.