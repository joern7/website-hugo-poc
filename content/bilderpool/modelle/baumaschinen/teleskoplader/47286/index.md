---
layout: "image"
title: "Kotflügel"
date: "2018-02-14T16:38:04"
picture: "telbly18.jpg"
weight: "40"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47286
- /details5801-2.html
imported:
- "2019"
_4images_image_id: "47286"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47286 -->
Die Kotflügel können einfach aufgesteckt werden