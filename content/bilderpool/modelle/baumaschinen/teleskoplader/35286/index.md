---
layout: "image"
title: "Teleskoplader 016"
date: "2012-08-10T17:56:38"
picture: "teleskoplader16.jpg"
weight: "16"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35286
- /details264a-3.html
imported:
- "2019"
_4images_image_id: "35286"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35286 -->
Gut versteckt hinter der Kabinentür befindet sich der Antriebsmotor 
für die Hebemechanik. Es ist ein Powermotor mit Übersetzung 1:20.