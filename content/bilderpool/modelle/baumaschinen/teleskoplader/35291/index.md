---
layout: "image"
title: "Teleskoplader 021"
date: "2012-08-10T17:56:38"
picture: "teleskoplader21.jpg"
weight: "21"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35291
- /detailsb904.html
imported:
- "2019"
_4images_image_id: "35291"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35291 -->
Weil die Verdrahtung der ganzen Elektronik sehr schwierig war,
kamen auch Kabelbinder zum Einsatz!!! :-)