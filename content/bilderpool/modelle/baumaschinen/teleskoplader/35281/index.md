---
layout: "image"
title: "Teleskoplader 011"
date: "2012-08-10T17:56:38"
picture: "teleskoplader11.jpg"
weight: "11"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35281
- /detailsc945-2.html
imported:
- "2019"
_4images_image_id: "35281"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35281 -->
Hinterradantrieb:

Der XM-Mot treibt über zwei Z20 die Hinterachse an.
Ein Differential habe ich absichtlich nicht eingebaut, da dieses Fahrzeug nur für langsame
Geschwindigkeiten ausgelegt ist. Neben den Zahnrädern sind noch der Akku
und die Blinkelektronik eingebaut.

