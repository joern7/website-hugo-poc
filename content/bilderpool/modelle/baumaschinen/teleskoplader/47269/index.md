---
layout: "image"
title: "Teleskoplader"
date: "2018-02-14T16:38:04"
picture: "telbly01.jpg"
weight: "23"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47269
- /details6005-2.html
imported:
- "2019"
_4images_image_id: "47269"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47269 -->
Video zum Modell: https://youtu.be/CptDiqSlyyc

Funktionen:
- Fernsteuerung über das Smartphone (iOS / Android) per Wifi mit der App Blynk (Empfänger: Particle Photon: https://youtu.be/jBZQri6zQZs)
- Geländegängig dank Allradantrieb (XM Elektromotor)
- Allradlenkung (2 Lenkservos) mit verschiedenen Lenkprogrammen: Gleichlauflenkung, Hundeganglenkung, Hinterachslenkung
- Pneumatisch anhebbarer Ausleger
- LED Fahrlicht