---
layout: "image"
title: "Teleskoplader 018"
date: "2012-08-10T17:56:38"
picture: "teleskoplader18.jpg"
weight: "18"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35288
- /details8a10.html
imported:
- "2019"
_4images_image_id: "35288"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35288 -->
Jetzt befindet sich der Teleskoparm auf seinem höchsten Punkt...
Er ist ganz hoch- und ausgefahren. Der gesamte Ausleger erreicht so eine Höhe von etwa 40cm.
