---
layout: "image"
title: "7"
date: "2009-01-17T13:23:39"
picture: "rhvonterex06.jpg"
weight: "5"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/17023
- /details1913-2.html
imported:
- "2019"
_4images_image_id: "17023"
_4images_cat_id: "1530"
_4images_user_id: "822"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17023 -->
