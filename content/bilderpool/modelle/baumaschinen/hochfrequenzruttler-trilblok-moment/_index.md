---
layout: "overview"
title: "Hochfrequenzrüttler ('Trilblok') mit einstellbarem Moment"
date: 2020-02-22T08:13:28+01:00
legacy_id:
- /php/categories/1708
- /categoriesac9f.html
- /categoriesf6ea.html
- /categoriesdb2a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1708 --> 
Trilblok met instelbaar variabel excentrisch moment.



Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.



Voordelen: 



minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk