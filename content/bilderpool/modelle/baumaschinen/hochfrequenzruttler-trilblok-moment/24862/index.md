---
layout: "image"
title: "Fischertechnik Trilblok met instelbaar variabel excentrisch moment."
date: "2009-08-30T09:28:51"
picture: "fttrilblokmetinstelbaarvariabelexcentrischmoment14.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24862
- /detailsd6f5.html
imported:
- "2019"
_4images_image_id: "24862"
_4images_cat_id: "1708"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24862 -->
Trilblok met instelbaar variabel excentrisch moment.

Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.

Voordelen: 

minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk
