---
layout: "image"
title: "Fischertechnik Trilblok met instelbaar variabel excentrisch moment."
date: "2009-08-30T09:28:50"
picture: "fttrilblokmetinstelbaarvariabelexcentrischmoment01.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24849
- /details7342.html
imported:
- "2019"
_4images_image_id: "24849"
_4images_cat_id: "1708"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24849 -->
Trilblok met instelbaar variabel excentrisch moment.

Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.

Voordelen: 

minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk
