---
layout: "image"
title: "Greifer"
date: "2006-04-27T13:15:47"
picture: "IMG_4567.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6158
- /detailsf29c.html
imported:
- "2019"
_4images_image_id: "6158"
_4images_cat_id: "543"
_4images_user_id: "389"
_4images_image_date: "2006-04-27T13:15:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6158 -->
