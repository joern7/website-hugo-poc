---
layout: "image"
title: "Schaufel1"
date: "2006-05-07T15:16:33"
picture: "IMG_4572.jpg"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: ["Guilligan", "Schaufel", "Bagger", "Baumaschine"]
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6237
- /details5f70.html
imported:
- "2019"
_4images_image_id: "6237"
_4images_cat_id: "543"
_4images_user_id: "389"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6237 -->
