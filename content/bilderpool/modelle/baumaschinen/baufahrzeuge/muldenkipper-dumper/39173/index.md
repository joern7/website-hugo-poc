---
layout: "image"
title: "Detailansicht: Gelenk"
date: "2014-08-08T21:21:23"
picture: "dumper07.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39173
- /detailsd3cc.html
imported:
- "2019"
_4images_image_id: "39173"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39173 -->
Über zwei Kardangelenke wird die Kraft des Motors auch auf die hinteren Achsen übertragen. Da ein Gelenk sehr schnell "ausbricht", (d.h. die Gelenkklauen lösen sich bei geringer Belastung von ihrem Würfel), habe ich zwei Gelenke verwendet, sodass sich die Kraft verteilt. Diese Stelle bereitet seitdem keine Probleme mehr beim Fahren.