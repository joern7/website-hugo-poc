---
layout: "image"
title: "Mulde"
date: "2014-08-08T21:21:23"
picture: "dumper10.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39176
- /details0627.html
imported:
- "2019"
_4images_image_id: "39176"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39176 -->
Das Design der Mulde orientiert sich an dem des Original.