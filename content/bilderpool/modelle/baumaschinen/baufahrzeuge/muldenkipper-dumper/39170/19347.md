---
layout: "comment"
hidden: true
title: "19347"
date: "2014-08-09T19:28:54"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wenn beide Magnetventile unbetätigt sind, sind dann aber alle Zylinderseiten mit Abluft verbunden, oder? Verdreht sich dann die Lenkung nicht von alleine beim Fahren? Oder lässt Du doch irgendwie alle Zylinder von beiden Seiten zwischen Druckluft einspannen?
Gruß, Stefan