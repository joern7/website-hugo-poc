---
layout: "image"
title: "geöffnete Motorhaube"
date: "2014-08-08T21:21:23"
picture: "dumper13.jpg"
weight: "13"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39179
- /details72aa-2.html
imported:
- "2019"
_4images_image_id: "39179"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39179 -->
Ganz unten im Motorbereich befindet sich der Powermotor, darüber sind der Empfänger und der Kompressor angebracht.