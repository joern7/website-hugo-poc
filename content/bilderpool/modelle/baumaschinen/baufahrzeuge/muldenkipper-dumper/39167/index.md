---
layout: "image"
title: "Dumper von vorne"
date: "2014-08-08T21:21:23"
picture: "dumper01.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39167
- /detailsd203.html
imported:
- "2019"
_4images_image_id: "39167"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39167 -->
Der Muldenkipper verfügt über Allradantrieb und eine pneumatische Lenkung. Die Mulde lässt sich pneumatisch kippen.