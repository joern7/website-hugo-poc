---
layout: "image"
title: "Ansicht unten + Allradantrieb"
date: "2014-08-08T21:21:23"
picture: "dumper12.jpg"
weight: "12"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39178
- /details2de8-2.html
imported:
- "2019"
_4images_image_id: "39178"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39178 -->
Der Dumper wird über einen Power Motor 50:1 angetrieben, der sich unter der Motorhaube befindet. Das Fahrzeug verfügt über Allradantrieb, was ein stabiles Fahrverhalten bewirkt.