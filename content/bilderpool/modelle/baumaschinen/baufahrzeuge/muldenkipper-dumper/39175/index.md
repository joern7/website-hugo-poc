---
layout: "image"
title: "Ansicht von hinten auf das Führerhaus"
date: "2014-08-08T21:21:23"
picture: "dumper09.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39175
- /details25b5.html
imported:
- "2019"
_4images_image_id: "39175"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39175 -->
Ansicht ohne Mulde