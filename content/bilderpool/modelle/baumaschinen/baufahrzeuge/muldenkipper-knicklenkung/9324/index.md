---
layout: "image"
title: "Bell-B30D-57.JPG"
date: "2007-03-06T19:39:58"
picture: "Bell-B30D-57.JPG"
weight: "1"
konstrukteure: 
- "Fa. Bell, Südafrika"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9324
- /detailsd0e3.html
imported:
- "2019"
_4images_image_id: "9324"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:39:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9324 -->
Dieser Muldenkipper stand letzten Sommer so aufreizend an der Isar, da musste ich einfach mit dem Fotoapparat hin.