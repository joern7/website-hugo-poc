---
layout: "image"
title: "Bell_B30D_16.JPG"
date: "2007-03-06T20:15:26"
picture: "Bell_B30D_16.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Zentraldifferenzial", "Knicklenkung", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9333
- /details8e42.html
imported:
- "2019"
_4images_image_id: "9333"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T20:15:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9333 -->
Das Gleichlaufgetriebe: der gleichsinnige Antrieb der Vorderräder, und zugleich Antrieb der Hinterachsen, kommt über das rote Differenzial in Bildmitte. Darauf folgt ein Vorgelege, um mit 1:4 Untersetzung auf das Rad zu gehen.
Der gegensinnige Antrieb erfolgt durch die beiden schwarzen Differenziale weiter vorn im Fahrzeug. Das untere ist wirklich ein Differenzial und wird von einem Motor angetrieben. Beim oberen wird der Käfig durch zwei ft-Kufen festgehalten; es wirkt daher nur als Drehrichtungsumkehr (dank der drei Kegelzahnräder im Inneren). Auch der gegensinnige Antrieb erfolgt mit 1:4 Untersetzung auf die Räder.


Beim Fahren muss man zwischen Vorwärts- und Rückwärtsfahrt unterscheiden: beim Vorwärtsfahren muss man den Vorderwagen einmal in Wunschrichtung schwenken und dann den Lenkmotor ausschalten - der Vorderwagen fährt dann schnurgerade weiter, der Hinterwagen kommt einfach hinterher. Bei Rückwärtsfahrt muss aber ständig nachgelenkt werden, da sonst das Fahrzeug bis zur Grenze einknickt und irgendwann etwas zu Bruch geht.



Das Zentraldifferenzial habe ich mir wie beim Original erspart. Das könnte zu Verspannungen im Antriebsstrang führen (insbesondere auf gut haftendem Grund), aber es gibt eine "Rutschkupplung" an jedem Rad: die verwendeten Conrad-Reifen sitzen von hause aus nicht übermäßig fest auf den ft-Felgen und rutschen ggf. einfach darauf herum.