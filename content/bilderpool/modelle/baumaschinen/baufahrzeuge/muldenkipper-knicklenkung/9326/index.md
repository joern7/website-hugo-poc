---
layout: "image"
title: "Bell-B30D-72.JPG"
date: "2007-03-06T19:50:04"
picture: "Bell-B30D-72.JPG"
weight: "3"
konstrukteure: 
- "Fa. Bell"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9326
- /detailsd19d.html
imported:
- "2019"
_4images_image_id: "9326"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:50:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9326 -->
Das ist der Wartungsplan. Nummer 17 ist die Bremse, die über die Antriebswelle auf die Räder wirkt.


Wie man sieht, sieht man ---- kein Zentraldifferenzial. Zwischen den beiden Hinterachsen gibt es auch keins; da geht eine Antriebsachse direkt von der ersten zur zweiten.