---
layout: "image"
title: "Telesc-lr-81"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran42.jpg"
weight: "42"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41439
- /details12f0-3.html
imported:
- "2019"
_4images_image_id: "41439"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41439 -->
Alternatives Fahrgestell.