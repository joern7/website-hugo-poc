---
layout: "image"
title: "Telesc-22a-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran14.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41411
- /detailsb06c-2.html
imported:
- "2019"
_4images_image_id: "41411"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41411 -->
Seilführung wie Originell.