---
layout: "image"
title: "Telesc-72-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran39.jpg"
weight: "39"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41436
- /detailsf6c4-2.html
imported:
- "2019"
_4images_image_id: "41436"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41436 -->
Kabelführung.