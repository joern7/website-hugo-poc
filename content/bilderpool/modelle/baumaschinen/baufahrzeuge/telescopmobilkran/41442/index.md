---
layout: "image"
title: "Telesc-lr-92"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran45.jpg"
weight: "45"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41442
- /detailsa166.html
imported:
- "2019"
_4images_image_id: "41442"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41442 -->
