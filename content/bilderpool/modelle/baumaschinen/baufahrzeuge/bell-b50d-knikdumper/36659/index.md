---
layout: "image"
title: "De bak"
date: "2013-02-21T18:56:48"
picture: "P2180007.jpg"
weight: "5"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/36659
- /details3fbe.html
imported:
- "2019"
_4images_image_id: "36659"
_4images_cat_id: "2718"
_4images_user_id: "838"
_4images_image_date: "2013-02-21T18:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36659 -->
De bak met een paar nep cilinders, ik kon de aandrijving hier niet mooi genoeg wegwerken zodoende is deze verhuist naar achteren. Maar toch een paar nep cilinders voor de look. 
De achteras is volledig onafhankelijk opgehangen net zoals bij de echte,