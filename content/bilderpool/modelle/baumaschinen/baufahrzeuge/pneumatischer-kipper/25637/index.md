---
layout: "image"
title: "ausgefahren"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25637
- /detailsfc11.html
imported:
- "2019"
_4images_image_id: "25637"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25637 -->
