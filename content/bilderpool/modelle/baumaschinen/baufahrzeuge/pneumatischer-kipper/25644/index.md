---
layout: "image"
title: "Laderampe"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper11.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25644
- /detailscefd-2.html
imported:
- "2019"
_4images_image_id: "25644"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25644 -->
