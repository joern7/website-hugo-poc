---
layout: "image"
title: "onderaanzicht"
date: "2011-02-13T17:51:43"
picture: "pivot_007.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29959
- /detailsc60e.html
imported:
- "2019"
_4images_image_id: "29959"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29959 -->
onderste schuif is hier van de grond, deze kan ook op de grond zakken voor het innemen van zand