---
layout: "image"
title: "Bovenaanzicht"
date: "2011-02-13T17:51:43"
picture: "pivot_005.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29957
- /detailsf0a3-2.html
imported:
- "2019"
_4images_image_id: "29957"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29957 -->
Boven aanzicht laadbak met klep en losschuif