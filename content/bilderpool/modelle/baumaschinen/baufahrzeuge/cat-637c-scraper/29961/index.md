---
layout: "image"
title: "powermotor"
date: "2011-02-13T17:51:43"
picture: "pivot_009.jpg"
weight: "8"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29961
- /detailsec68.html
imported:
- "2019"
_4images_image_id: "29961"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29961 -->
De powermotor met diff zorgt voor het laten zakken en heffen