---
layout: "image"
title: "Vooraanzicht"
date: "2011-06-20T21:57:11"
picture: "TD24_013.jpg"
weight: "13"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30880
- /details21da.html
imported:
- "2019"
_4images_image_id: "30880"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-20T21:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30880 -->
De motorkap er weer op