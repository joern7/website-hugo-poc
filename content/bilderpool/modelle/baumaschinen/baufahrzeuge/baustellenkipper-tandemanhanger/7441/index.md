---
layout: "image"
title: "Getriebegangschaltung"
date: "2006-11-08T23:03:21"
picture: "lkw1.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/7441
- /details6b61.html
imported:
- "2019"
_4images_image_id: "7441"
_4images_cat_id: "140"
_4images_user_id: "6"
_4images_image_date: "2006-11-08T23:03:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7441 -->
Auch hier leider wieder eine Sonderanfertigung. Die Idee mit dem Motor kommt von Schnaggels, das Adapterstück for die Welle hab ich selber gemacht. Funktioniert ganz gut.