---
layout: "image"
title: "Lenkstange"
date: "2006-11-06T21:30:33"
picture: "kipper2.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/7439
- /detailsae05.html
imported:
- "2019"
_4images_image_id: "7439"
_4images_cat_id: "140"
_4images_user_id: "6"
_4images_image_date: "2006-11-06T21:30:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7439 -->
Da es absolut keine passende Strebe gab, hab ich kurzerhand selber was gedreht.