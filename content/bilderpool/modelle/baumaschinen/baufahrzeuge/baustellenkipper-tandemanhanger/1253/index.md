---
layout: "image"
title: "Kipper"
date: "2003-07-23T22:13:04"
picture: "IMG_0199.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/1253
- /details21e1.html
imported:
- "2019"
_4images_image_id: "1253"
_4images_cat_id: "140"
_4images_user_id: "6"
_4images_image_date: "2003-07-23T22:13:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1253 -->
