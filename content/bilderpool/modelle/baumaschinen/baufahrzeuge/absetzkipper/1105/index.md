---
layout: "image"
title: "Bauschutt & Co (Container01)"
date: "2003-05-04T20:52:43"
picture: "Container01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1105
- /details0152.html
imported:
- "2019"
_4images_image_id: "1105"
_4images_cat_id: "26"
_4images_user_id: "4"
_4images_image_date: "2003-05-04T20:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1105 -->
