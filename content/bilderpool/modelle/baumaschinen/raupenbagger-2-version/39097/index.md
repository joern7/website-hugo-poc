---
layout: "image"
title: "Raupenbagger IV"
date: "2014-07-29T07:12:28"
picture: "raupenbaggerversion4.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/39097
- /detailscfd5.html
imported:
- "2019"
_4images_image_id: "39097"
_4images_cat_id: "2922"
_4images_user_id: "1729"
_4images_image_date: "2014-07-29T07:12:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39097 -->
Ansicht von vorne.
Leider bin ich etwas über das Ziel hinaus geschossen. Bei meiner ersten Variante saß der Oberwagen zu hoch; jetzt ist er zu niedrig. Nur wenige Millimeter, und die Ketten würden am Oberwagen schleifen.
Das muß ich nochmal überarbeiten und eine halbe Bausteinhöhe höher legen.
Man sieht auch im Vergleich zum Vorgänger die viel bessere Stabilität des Unterwagens: im Vergleich: http://ftcommunity.de/details.php?image_id=38324#col3
der neue Unterwagen biegt sich nur noch ein bisschen durch, obwohl der Oberwagen viel größer und schwerer als vorher ist.

Fortsetzung folgt, es gibt noch ein bisschen zu tun:
- Oberwagen höher setzen
- Baggerschaufel Seitenwände
- eventuell mach ich den Oberwagen auch noch eine Bausteinhöhe höher. Im Moment wirkt er mir ein bisschen zu flach.
- und irgendwann vielleicht mal die Motorisierung des Baggerarms.