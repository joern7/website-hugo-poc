---
layout: "image"
title: "Raupenbagger I"
date: "2014-07-29T07:12:28"
picture: "raupenbaggerversion1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/39094
- /details07dd.html
imported:
- "2019"
_4images_image_id: "39094"
_4images_cat_id: "2922"
_4images_user_id: "1729"
_4images_image_date: "2014-07-29T07:12:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39094 -->
Der Semi-finale Stand meines Raupenbaggers.
Den Unterwagen, auch die neue Version, hab ich ja schon im Detail beschrieben in http://ftcommunity.de/categories.php?cat_id=2854
Der Oberwagen ist auch neu. Größer als der alte, flacher, und in Gelb. Im Vergleich dazu: http://ftcommunity.de/details.php?image_id=38373

Die Proportionen passen jetzt besser. Das Kettenfahrwerk ist etwas kürzer als vorher, der Oberwagen länger und breiter.