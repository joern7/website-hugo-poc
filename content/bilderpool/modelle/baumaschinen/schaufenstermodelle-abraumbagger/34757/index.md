---
layout: "image"
title: "Abräumbagger_08"
date: "2012-04-06T23:17:30"
picture: "abraumbagger08.jpg"
weight: "8"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34757
- /detailsf88e.html
imported:
- "2019"
_4images_image_id: "34757"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34757 -->
Hier das Kettengestell mit dem Drehkranz.