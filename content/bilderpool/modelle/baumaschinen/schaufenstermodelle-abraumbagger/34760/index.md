---
layout: "image"
title: "Abräumbagger_11"
date: "2012-04-06T23:17:42"
picture: "abraumbagger11.jpg"
weight: "11"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34760
- /detailsf2c2.html
imported:
- "2019"
_4images_image_id: "34760"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34760 -->
Ein Blick auf das Schaufelrad.