---
layout: "image"
title: "Abräumbagger_10"
date: "2012-04-06T23:17:30"
picture: "abraumbagger10.jpg"
weight: "10"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34759
- /details8100.html
imported:
- "2019"
_4images_image_id: "34759"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34759 -->
Hier der Antrieb des Schaufelrades.