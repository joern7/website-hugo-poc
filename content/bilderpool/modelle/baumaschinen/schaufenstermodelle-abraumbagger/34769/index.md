---
layout: "image"
title: "Abräumbagger_20"
date: "2012-04-06T23:17:43"
picture: "abraumbagger20.jpg"
weight: "20"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34769
- /details594d.html
imported:
- "2019"
_4images_image_id: "34769"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34769 -->
Ein Blick auf die Anbringung des Polwendeschalters.