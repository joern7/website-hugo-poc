---
layout: "image"
title: "Abräumbagger_18"
date: "2012-04-06T23:17:43"
picture: "abraumbagger18.jpg"
weight: "18"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34767
- /details4e63.html
imported:
- "2019"
_4images_image_id: "34767"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34767 -->
Hier ist sehr schon die Befestigung der beiden "Seile" zu erkennen.