---
layout: "image"
title: "Abräumbagger_19"
date: "2012-04-06T23:17:43"
picture: "abraumbagger19.jpg"
weight: "19"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34768
- /details4576-2.html
imported:
- "2019"
_4images_image_id: "34768"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34768 -->
So sieht die Verkabelung aus.