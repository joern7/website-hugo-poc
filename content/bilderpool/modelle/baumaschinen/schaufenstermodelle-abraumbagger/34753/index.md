---
layout: "image"
title: "Abräumbagger_04"
date: "2012-04-06T23:17:30"
picture: "abraumbagger04.jpg"
weight: "4"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34753
- /detailsc0af.html
imported:
- "2019"
_4images_image_id: "34753"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34753 -->
Ein Blick von schräg-vorn.