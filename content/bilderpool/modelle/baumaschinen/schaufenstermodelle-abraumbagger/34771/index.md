---
layout: "image"
title: "Abräumbagger_22"
date: "2012-04-06T23:17:54"
picture: "abraumbagger22.jpg"
weight: "22"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34771
- /detailsc203-2.html
imported:
- "2019"
_4images_image_id: "34771"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:54"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34771 -->
Ein weiteres Foto wie der Polwendeschalter durch das Zahnrad betätigt wird.