---
layout: "image"
title: "Abräumbagger_05"
date: "2012-04-06T23:17:30"
picture: "abraumbagger05.jpg"
weight: "5"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34754
- /details2a56.html
imported:
- "2019"
_4images_image_id: "34754"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34754 -->
Hier der Blick auf die beiden Seilwinden.