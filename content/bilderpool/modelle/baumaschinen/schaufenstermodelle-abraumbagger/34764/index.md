---
layout: "image"
title: "Abräumbagger_15"
date: "2012-04-06T23:17:43"
picture: "abraumbagger15.jpg"
weight: "15"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34764
- /details86b5-2.html
imported:
- "2019"
_4images_image_id: "34764"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34764 -->
Ein Blick auf den Drehkranz.