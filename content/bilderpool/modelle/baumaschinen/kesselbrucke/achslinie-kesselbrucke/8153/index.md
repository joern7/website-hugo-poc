---
layout: "image"
title: "diverse Achskonstruktion für die Kesselbrücke"
date: "2006-12-26T15:40:18"
picture: "achsliniefuerdiekesselbruecke08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8153
- /detailsb4b8.html
imported:
- "2019"
_4images_image_id: "8153"
_4images_cat_id: "753"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8153 -->
schlechte Konstruktion - da zu breit