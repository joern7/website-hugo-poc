---
layout: "image"
title: "Kesselbrücke"
date: "2006-12-26T15:40:09"
picture: "kesselbruecke06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8137
- /details37ec.html
imported:
- "2019"
_4images_image_id: "8137"
_4images_cat_id: "752"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8137 -->
