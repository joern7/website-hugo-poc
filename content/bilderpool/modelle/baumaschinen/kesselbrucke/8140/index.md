---
layout: "image"
title: "Kesselbrücke"
date: "2006-12-26T15:40:09"
picture: "kesselbruecke09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8140
- /details4b3c.html
imported:
- "2019"
_4images_image_id: "8140"
_4images_cat_id: "752"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8140 -->
Traversenkonstruktion mit Winkelträgern