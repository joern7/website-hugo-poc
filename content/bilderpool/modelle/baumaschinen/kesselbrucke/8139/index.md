---
layout: "image"
title: "Kesselbrücke"
date: "2006-12-26T15:40:09"
picture: "kesselbruecke08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8139
- /details1319.html
imported:
- "2019"
_4images_image_id: "8139"
_4images_cat_id: "752"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8139 -->
Traversenkonstruktion mit Winkelträgern