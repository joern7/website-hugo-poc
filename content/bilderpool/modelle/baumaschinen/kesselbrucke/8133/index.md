---
layout: "image"
title: "Kesselbrücke"
date: "2006-12-26T15:40:01"
picture: "kesselbruecke02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8133
- /detailsd2c9.html
imported:
- "2019"
_4images_image_id: "8133"
_4images_cat_id: "752"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8133 -->
