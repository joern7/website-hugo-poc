---
layout: "image"
title: "Kabel"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran14.jpg"
weight: "14"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31225
- /details3be9.html
imported:
- "2019"
_4images_image_id: "31225"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31225 -->
Ich führe die Kabel immer durch das Loch in der MItte des Drehkranzes.