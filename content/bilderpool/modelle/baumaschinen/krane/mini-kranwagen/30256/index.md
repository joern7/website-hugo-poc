---
layout: "image"
title: "von hinten (inkl. Antrieb für die Seilwinde)"
date: "2011-03-15T18:50:59"
picture: "minikranwagen4.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30256
- /details2c91.html
imported:
- "2019"
_4images_image_id: "30256"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30256 -->
links sieht man den Motor der für die Seilwinde zuständig ist