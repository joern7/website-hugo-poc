---
layout: "image"
title: "der Motor zum Hochfahren des Auslegers"
date: "2011-03-15T18:50:59"
picture: "minikranwagen7.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30259
- /details57cb.html
imported:
- "2019"
_4images_image_id: "30259"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30259 -->
Der Ausleger ist 25cm lang