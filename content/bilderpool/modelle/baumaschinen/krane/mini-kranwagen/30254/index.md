---
layout: "image"
title: "von der anderen Seite"
date: "2011-03-15T18:50:59"
picture: "minikranwagen2.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30254
- /detailsaf07.html
imported:
- "2019"
_4images_image_id: "30254"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30254 -->
Blick auf den Motor zum Drehen des Auslegers