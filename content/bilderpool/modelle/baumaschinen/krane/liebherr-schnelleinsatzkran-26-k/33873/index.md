---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:56"
picture: "IM003398.jpg"
weight: "9"
konstrukteure: 
- "Rheingauer01"
fotografen:
- "Rheingauer01"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33873
- /detailsb992.html
imported:
- "2019"
_4images_image_id: "33873"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33873 -->
Hauptflaschenzug. Der untere Teil dient zum Aufrichten des Turmes, der obere Teil zum Ausziehen des Turmes.