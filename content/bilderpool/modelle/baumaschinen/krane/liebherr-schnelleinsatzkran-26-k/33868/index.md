---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:55"
picture: "IM003409.jpg"
weight: "4"
konstrukteure: 
- "Rheingauer01"
fotografen:
- "Rheingauer01"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33868
- /detailsc4b1-2.html
imported:
- "2019"
_4images_image_id: "33868"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33868 -->
Anschließend wird die Auslegerspitze mit einer 2. Seilwinde (Handbetrieb) heruntergelassen.