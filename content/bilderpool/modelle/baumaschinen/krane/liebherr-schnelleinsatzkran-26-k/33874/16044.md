---
layout: "comment"
hidden: true
title: "16044"
date: "2012-01-09T10:56:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr beeindruckend! Diese Aufstell-Kräne faszinieren mich immer wieder, ich wüsste gar nicht, wie ich so ein Modell anfangen sollte. Eine Frage aber: In echt sind die ganzen Aufstellvorgänge bestimmt von Motoren unterstützt, oder gibt's da im Original tatsächlich Handkurbeln? Man verzeihe mir, wenn die Frage doof ist, aber ich weiß es wirklich nicht.

Gruß,
Stefan