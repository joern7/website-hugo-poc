---
layout: "image"
title: "Der Drehkranz"
date: "2013-03-07T13:30:38"
picture: "20111216_102816_Apple_iPhone_4_IMG_1608.jpeg"
weight: "5"
konstrukteure: 
- "Marc Stephan Tauchert"
fotografen:
- "Marc Stephan Tauchert"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mst"
license: "unknown"
legacy_id:
- /php/details/36727
- /detailse398.html
imported:
- "2019"
_4images_image_id: "36727"
_4images_cat_id: "2724"
_4images_user_id: "1621"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36727 -->
