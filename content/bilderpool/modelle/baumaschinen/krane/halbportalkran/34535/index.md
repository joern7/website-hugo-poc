---
layout: "image"
title: "7"
date: "2012-03-03T21:35:26"
picture: "halbportalkran07.jpg"
weight: "7"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34535
- /detailse263.html
imported:
- "2019"
_4images_image_id: "34535"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34535 -->
Detail vom angetriebenen Rad an der Stütze - 2
Die Laufräder sind wie beim großen Kran mit zwei Spurkränzen ausgeführt.