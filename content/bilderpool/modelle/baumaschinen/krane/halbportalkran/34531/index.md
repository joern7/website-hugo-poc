---
layout: "image"
title: "3"
date: "2012-03-03T21:35:26"
picture: "halbportalkran03.jpg"
weight: "3"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34531
- /details9733.html
imported:
- "2019"
_4images_image_id: "34531"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34531 -->
Seitenansicht der Stütze.