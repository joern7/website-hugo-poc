---
layout: "image"
title: "17"
date: "2012-03-03T21:35:39"
picture: "halbportalkran17.jpg"
weight: "17"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34545
- /details1b10-2.html
imported:
- "2019"
_4images_image_id: "34545"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34545 -->
Laufrad an der Stütze.