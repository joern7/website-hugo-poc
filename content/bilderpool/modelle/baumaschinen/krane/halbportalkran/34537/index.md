---
layout: "image"
title: "9"
date: "2012-03-03T21:35:26"
picture: "halbportalkran09.jpg"
weight: "9"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34537
- /details0a58.html
imported:
- "2019"
_4images_image_id: "34537"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34537 -->
Katze von oben.