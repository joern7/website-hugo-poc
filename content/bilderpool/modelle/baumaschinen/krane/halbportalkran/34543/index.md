---
layout: "image"
title: "15"
date: "2012-03-03T21:35:39"
picture: "halbportalkran15.jpg"
weight: "15"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34543
- /detailsb039-2.html
imported:
- "2019"
_4images_image_id: "34543"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34543 -->
Laufrad oberer Kopfträger.