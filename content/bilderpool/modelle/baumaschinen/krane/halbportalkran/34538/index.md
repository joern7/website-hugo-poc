---
layout: "image"
title: "10"
date: "2012-03-03T21:35:26"
picture: "halbportalkran10.jpg"
weight: "10"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34538
- /details59fc.html
imported:
- "2019"
_4images_image_id: "34538"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34538 -->
Katze Antrieb und obere Seilumlenkung (Oberflasche).