---
layout: "image"
title: "1"
date: "2012-03-03T21:35:26"
picture: "halbportalkran01.jpg"
weight: "1"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34529
- /details04d4.html
imported:
- "2019"
_4images_image_id: "34529"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34529 -->
Zweiträgerhalbportalkran Gesamtansicht.
Als Stützen für die hintere Kranbahn dienen ein alter Lautsprecher und zwei 1000er Boxen.