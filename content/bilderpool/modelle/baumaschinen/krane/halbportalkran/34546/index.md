---
layout: "image"
title: "18"
date: "2012-03-03T21:35:39"
picture: "halbportalkran18.jpg"
weight: "18"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34546
- /details0cf5.html
imported:
- "2019"
_4images_image_id: "34546"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34546 -->
Detail angetriebes Laufrad an der Stütze. Die "unkonventionelle" Anordnung des Kegelrades war notwendig um die richtige Drehrichtung beizubehalten.