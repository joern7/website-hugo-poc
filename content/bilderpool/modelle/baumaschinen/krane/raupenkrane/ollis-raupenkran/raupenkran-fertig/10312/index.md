---
layout: "image"
title: "Gegengewichte"
date: "2007-05-05T21:04:15"
picture: "raupenkranolli20.jpg"
weight: "20"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10312
- /detailsfa29-3.html
imported:
- "2019"
_4images_image_id: "10312"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:15"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10312 -->
Ungefähr 2,6kg Gegengewicht.