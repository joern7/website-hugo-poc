---
layout: "image"
title: "Führerhaus"
date: "2007-05-05T21:04:14"
picture: "raupenkranolli14.jpg"
weight: "14"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10306
- /details011a.html
imported:
- "2019"
_4images_image_id: "10306"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:14"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10306 -->
Führerhaus mit Kranführer.