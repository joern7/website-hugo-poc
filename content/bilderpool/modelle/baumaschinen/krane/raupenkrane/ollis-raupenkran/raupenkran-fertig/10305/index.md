---
layout: "image"
title: "???"
date: "2007-05-05T21:04:14"
picture: "raupenkranolli13.jpg"
weight: "13"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10305
- /details5cc8-2.html
imported:
- "2019"
_4images_image_id: "10305"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10305 -->
Wie nennt man diese Dinger?