---
layout: "image"
title: "Kran von oben"
date: "2007-05-06T21:37:12"
picture: "raupenkran3.jpg"
weight: "26"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10330
- /detailsc582.html
imported:
- "2019"
_4images_image_id: "10330"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-06T21:37:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10330 -->
Vom Mast runterfotografiert.