---
layout: "image"
title: "Wippspitze"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli06.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10298
- /details380f.html
imported:
- "2019"
_4images_image_id: "10298"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10298 -->
Die 1,10m lange Wippspitze.