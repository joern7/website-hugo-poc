---
layout: "image"
title: "Ansicht"
date: "2007-05-19T09:12:25"
picture: "raupenkranolli2.jpg"
weight: "28"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10456
- /details44f1-2.html
imported:
- "2019"
_4images_image_id: "10456"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10456 -->
Hier nochmal der Kran.