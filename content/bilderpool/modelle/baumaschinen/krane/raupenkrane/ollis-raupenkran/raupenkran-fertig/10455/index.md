---
layout: "image"
title: "3. Version"
date: "2007-05-19T09:12:24"
picture: "raupenkranolli1.jpg"
weight: "27"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10455
- /detailsb9e7.html
imported:
- "2019"
_4images_image_id: "10455"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-19T09:12:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10455 -->
Das hier ist eine weitere Version meines Raupenkrans. Hier ist der Hauptausleger 1,2m lang und der Wippausleger 1,65m. Den Kranhaken hab ich diesmal nicht angehängt, weil das Seil sich sonst wieder so verdreht hätte.