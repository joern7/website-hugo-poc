---
layout: "image"
title: "Fahrgestell von Raupenkran"
date: "2007-02-24T13:52:51"
picture: "DSCI0058.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Raupenkran"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9141
- /details2594.html
imported:
- "2019"
_4images_image_id: "9141"
_4images_cat_id: "832"
_4images_user_id: "504"
_4images_image_date: "2007-02-24T13:52:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9141 -->
Das ist das Fahrgestell. Ach ja der Kran ist natürlich noch nicht vollendet! Ist noch in der Bauphase. Man kann ihn aber schon aufbauen.