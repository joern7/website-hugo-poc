---
layout: "image"
title: "Raupenkran"
date: "2007-02-24T13:52:51"
picture: "DSCI0056.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Raupenkran"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9139
- /details5324.html
imported:
- "2019"
_4images_image_id: "9139"
_4images_cat_id: "832"
_4images_user_id: "504"
_4images_image_date: "2007-02-24T13:52:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9139 -->
Das ist der Ausleger mit Wippspitze von meinem  Raupenkran. Sieht dem Liebherr LR 1160 ähnlich ist aber nicht ganz maßstabsgetreu. Also kein Scale Modell.