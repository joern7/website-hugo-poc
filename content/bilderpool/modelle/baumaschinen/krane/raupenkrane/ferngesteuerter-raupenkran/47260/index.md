---
layout: "image"
title: "Ferngesteuerter Raupenkran (Draufsicht seitlich)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-15.jpg"
weight: "15"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47260
- /details5581.html
imported:
- "2019"
_4images_image_id: "47260"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47260 -->
Beschreibung siehe andere Bilder.