---
layout: "image"
title: "Ferngesteuerter Raupenkran (Front/Detail)"
date: "2018-02-05T21:19:06"
picture: "Raupenkran-4.jpg"
weight: "4"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47249
- /detailseb46.html
imported:
- "2019"
_4images_image_id: "47249"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T21:19:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47249 -->
Hier ist der vordere Taster der horizontalen Maximalposition gut zu erkennen, sowie das eigentliche Lager des Auslegers.
Der vordere Polwendeschalter dient zum Umschalten zwischen der Kippfunktion des Auslegers und der Krandrehung. In Ermangelung eines fünften Kanals auf der Fernsteuerung war diese Notlösung erforderlich.
Im Fahrerhäuschen sitzt ein Brio-Männchen :-)