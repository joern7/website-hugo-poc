---
layout: "image"
title: "Ferngesteuerter Raupenkran (Unterseite/Details)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-11.jpg"
weight: "11"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47256
- /detailsa4ba.html
imported:
- "2019"
_4images_image_id: "47256"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47256 -->
Links die vordere Achsenaufhängung.
Mittig ist der S-Motor zu sehen, welcher über eine Schnecke den Drehkranz antreibt.
Der Motor hatte mit dem schweren Kranaufbau gut zu tun...