---
layout: "image"
title: "Raupenkran mit Fernsteuerung (Technik)"
date: "2018-02-05T21:19:01"
picture: "Raupenkran-2.jpg"
weight: "2"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47247
- /details7e7f.html
imported:
- "2019"
_4images_image_id: "47247"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T21:19:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47247 -->
Übersicht über die Krantechnik.
Der Ausleger ist kippbar gelagert und kann so in seinem Winkel variiert werden.