---
layout: "image"
title: "Ferngesteuerter Raupenkran (Rückansicht)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-7.jpg"
weight: "7"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47252
- /detailsacf2.html
imported:
- "2019"
_4images_image_id: "47252"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47252 -->
Die beiden Polwendeschalter dienen dem Schalten der Beleuchtung:
Einmal Signalleuchten und einmal Arbeitsscheinwerfer (etwa nach einem Drittel des Auslegers).
Oben ist der erwähnte Summer zu sehen.