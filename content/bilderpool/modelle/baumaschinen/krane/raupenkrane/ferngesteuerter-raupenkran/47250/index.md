---
layout: "image"
title: "Ferngesteuerter Raupenkran (Front)"
date: "2018-02-05T21:19:06"
picture: "Raupenkran-5.jpg"
weight: "5"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47250
- /details9701.html
imported:
- "2019"
_4images_image_id: "47250"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T21:19:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47250 -->
