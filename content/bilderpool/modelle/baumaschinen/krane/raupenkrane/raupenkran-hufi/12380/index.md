---
layout: "image"
title: "Belastungsproben"
date: "2007-11-03T12:52:36"
picture: "021107B.jpg"
weight: "17"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12380
- /detailsd6ae.html
imported:
- "2019"
_4images_image_id: "12380"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-03T12:52:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12380 -->
Hoffe ich bekomme keine Klage vom Verein der Kranschützer oder so aber getestet wurde bis etwas krachte.
Das Erste was sich auflöste waren die FT Haken die bei 5 Kg auf 2 Haken nachgaben.
Auch ist mir einmal die Schnur an der das Gewicht hängt gerissen.
Ab 6.5Kg (ca. dem Eigengewicht des Krans)
habe ich dann aufgegeben da ich nicht mehr ausschliessen konnte das es evtl. zu Schäden kommt bei denen ich das halbe Gerät abbauen muss. Aber mit 5 Kg am "Haken" ist noch ganz gut zu arbeiten. Es funktionieren dann noch alle Funktionen bis auf das Ausfahren des Auslegers (hat nur ca. 2,5Kg Schub) sehr gut.