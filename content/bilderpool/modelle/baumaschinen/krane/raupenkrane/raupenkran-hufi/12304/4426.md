---
layout: "comment"
hidden: true
title: "4426"
date: "2007-10-27T16:41:40"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Erst recht spannend wird es, wenn der Kran über seine Schwerpunktlage informiert ist und sein Gegengewicht verschieben kann.