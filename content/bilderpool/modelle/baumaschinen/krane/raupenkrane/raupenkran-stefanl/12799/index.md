---
layout: "image"
title: "Raupenkran 22"
date: "2007-11-24T12:18:23"
picture: "raupenkran01.jpg"
weight: "22"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12799
- /detailsa1d1.html
imported:
- "2019"
_4images_image_id: "12799"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12799 -->
Hier jetzt die Steuerung des Krans. Mit dem Kippschalter auf der rechten Seite schaltet man zwischen Kran-und Raupenfunktion um. Der untere Steuerknüppel ist für die Drehfunktion zuständig.