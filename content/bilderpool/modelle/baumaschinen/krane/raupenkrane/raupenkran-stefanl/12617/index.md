---
layout: "image"
title: "Raupenkran 17"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12617
- /detailsf489.html
imported:
- "2019"
_4images_image_id: "12617"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12617 -->
Damit der Haken auch schön runter hängt hat er ein zusätzliches Gewicht dran.