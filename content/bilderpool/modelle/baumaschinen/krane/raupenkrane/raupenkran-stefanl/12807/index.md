---
layout: "image"
title: "Raupenkran 30"
date: "2007-11-24T12:18:23"
picture: "raupenkran09.jpg"
weight: "30"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12807
- /details8703-2.html
imported:
- "2019"
_4images_image_id: "12807"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12807 -->
