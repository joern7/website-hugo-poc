---
layout: "image"
title: "Raupenkran 24"
date: "2007-11-24T12:18:23"
picture: "raupenkran03.jpg"
weight: "24"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12801
- /detailsab60.html
imported:
- "2019"
_4images_image_id: "12801"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12801 -->
Durch die 2 Taster an jeder Seite lassen sich die Motoren in 2 Geschwindigkeiten ansteuern.