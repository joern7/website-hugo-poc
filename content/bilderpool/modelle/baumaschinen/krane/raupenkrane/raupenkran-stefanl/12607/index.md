---
layout: "image"
title: "Raupenkran 7"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12607
- /detailsef29.html
imported:
- "2019"
_4images_image_id: "12607"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12607 -->
