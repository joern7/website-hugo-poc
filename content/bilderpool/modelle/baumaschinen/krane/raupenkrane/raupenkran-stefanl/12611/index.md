---
layout: "image"
title: "Raupenkran 11"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12611
- /detailsb27c.html
imported:
- "2019"
_4images_image_id: "12611"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12611 -->
Die Kette von der 2. Winde wird über viele Achsen nach hinten geleitet wo der Antriebsmotor sitzt.