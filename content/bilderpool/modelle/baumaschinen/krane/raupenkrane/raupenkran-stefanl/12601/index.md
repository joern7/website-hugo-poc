---
layout: "image"
title: "Raupenkran 1"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12601
- /details3cba.html
imported:
- "2019"
_4images_image_id: "12601"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12601 -->
Hier das Fahrgestell von meinem etwas größeren Kran.