---
layout: "image"
title: "Balastkorb"
date: "2009-07-07T18:23:47"
picture: "20090628-150428-Balastkorb-Komplett.jpg"
weight: "6"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24502
- /details25a6.html
imported:
- "2019"
_4images_image_id: "24502"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T18:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24502 -->
Der Balastkorb wiegt in der Form 5.6 kg