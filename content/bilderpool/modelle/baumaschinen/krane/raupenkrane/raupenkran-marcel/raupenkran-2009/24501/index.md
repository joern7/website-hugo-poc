---
layout: "image"
title: "Der Balastkorb"
date: "2009-07-07T18:23:47"
picture: "20090628-143610-Balast.jpg"
weight: "5"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24501
- /details7d70.html
imported:
- "2019"
_4images_image_id: "24501"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T18:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24501 -->
