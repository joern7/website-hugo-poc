---
layout: "image"
title: "von vorne"
date: "2009-07-08T15:38:58"
picture: "153239-Gesamt.jpg"
weight: "12"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24514
- /detailsfd27-2.html
imported:
- "2019"
_4images_image_id: "24514"
_4images_cat_id: "1685"
_4images_user_id: "979"
_4images_image_date: "2009-07-08T15:38:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24514 -->
Hier sieht man, dass der Kran etwas schief steht. Das liegt aber an dem Schiefen Untergrund.