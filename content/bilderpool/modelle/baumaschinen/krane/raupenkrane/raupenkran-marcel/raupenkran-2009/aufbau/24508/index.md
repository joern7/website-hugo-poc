---
layout: "image"
title: "Wippspitze eingehängt"
date: "2009-07-07T18:23:48"
picture: "150005-Aufbau.jpg"
weight: "6"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24508
- /detailsab1a.html
imported:
- "2019"
_4images_image_id: "24508"
_4images_cat_id: "1685"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T18:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24508 -->
