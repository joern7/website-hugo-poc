---
layout: "image"
title: "Raupenkran"
date: "2009-07-07T15:56:19"
picture: "20090628-164038-Gesamt.jpg"
weight: "1"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24497
- /detailsc8d4.html
imported:
- "2019"
_4images_image_id: "24497"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T15:56:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24497 -->
So sieht der Kran aktuell aus.
Allerdings ist die Mastspitze schon wieder modifiziert
Der Kran wiegt so 7,8 kg.