---
layout: "image"
title: "Am 28.06.08 hab ich den Kran mal draussen aufgebaut"
date: "2009-07-07T18:23:47"
picture: "20090628-140443-Aufbau.jpg"
weight: "1"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24503
- /details5aad.html
imported:
- "2019"
_4images_image_id: "24503"
_4images_cat_id: "1685"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T18:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24503 -->
