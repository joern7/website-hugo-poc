---
layout: "image"
title: "Gesamtansicht des Krans"
date: "2009-06-12T19:41:11"
picture: "cn01.jpg"
weight: "1"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24316
- /details4f07.html
imported:
- "2019"
_4images_image_id: "24316"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24316 -->
Eine Gesamtansicht des Krans bei vollständiger Größe.