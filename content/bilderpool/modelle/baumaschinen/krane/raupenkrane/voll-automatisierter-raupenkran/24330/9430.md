---
layout: "comment"
hidden: true
title: "9430"
date: "2009-06-15T22:05:44"
uploadBy:
- "ChiemgauN"
license: "unknown"
imported:
- "2019"
---
Das Alufolienproblem hat sich von sich aus gelößt, da ich noch einen kleinen Spiegel gefunden habe, der zufällig genau auf eine rote Bauplatte passt (38241).
Jetzt muss ich den nurnoch einbauen...
Vielleicht morgen oder übermorgen ;-)
Dann kommen neue Bilder

PS: Unter Selbstbauspiegel in Exoten/Zubehör/Schnitzereien etc. kann man das Teil schon einmal bewundern, ist gerade erst hoch geladen worden, sodass es vielleicht noch nicht sichtbar ist (darum auch kein Link)