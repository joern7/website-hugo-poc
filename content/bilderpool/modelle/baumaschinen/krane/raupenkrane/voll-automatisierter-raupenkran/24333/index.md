---
layout: "image"
title: "Und noch einer"
date: "2009-06-12T19:41:21"
picture: "cn18.jpg"
weight: "18"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24333
- /details121d.html
imported:
- "2019"
_4images_image_id: "24333"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24333 -->
Der Seilzug mit den Stabilisierungs-Seilen (rechts und links).