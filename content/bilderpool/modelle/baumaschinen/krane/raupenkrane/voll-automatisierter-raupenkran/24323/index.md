---
layout: "image"
title: "Das Herzstück des Arms"
date: "2009-06-12T19:41:12"
picture: "cn08.jpg"
weight: "8"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24323
- /details2efb.html
imported:
- "2019"
_4images_image_id: "24323"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24323 -->
Der Arm kann hoch, bzw. runter gefahren werden, sowie eine Lauf-Katze auf ihm bewegt werden, die einen Haken hoch und runter fahren kann.