---
layout: "image"
title: "Das Gegengewicht"
date: "2009-06-12T19:41:21"
picture: "cn19.jpg"
weight: "19"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24334
- /details5835-2.html
imported:
- "2019"
_4images_image_id: "24334"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24334 -->
An diesem Arm befindet sich das Gegengewicht