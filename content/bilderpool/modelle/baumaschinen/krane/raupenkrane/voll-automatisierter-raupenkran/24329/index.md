---
layout: "image"
title: "Eine Lampe"
date: "2009-06-12T19:41:21"
picture: "cn14.jpg"
weight: "14"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24329
- /details19c6.html
imported:
- "2019"
_4images_image_id: "24329"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24329 -->
Diese Lampe dient als Signal für den Fototransistor (siehe nächstes Bild), damit der Haken nicht zu hoch gezogen wird. Sie ist an zwei Gelenken befestigt, damit sie auch immer Senkrecht zur Hubrichtung steht. Zuvor hatte ich die legidlich mit einem Winkel befestigt, doch dann stellte sich das Problem, dass der Transistor nicht getroffen wird, wenn ich die Stellung des Armes verändere. Mit den Gelenken funktioniert es.