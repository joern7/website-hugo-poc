---
layout: "image"
title: "Die Steuerungszentrale"
date: "2009-06-12T19:42:07"
picture: "cn26.jpg"
weight: "26"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24341
- /detailscc8c-2.html
imported:
- "2019"
_4images_image_id: "24341"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:07"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24341 -->
Mittels Interface wird der Kran "lebendig". Alles lässt sich komfortabel am PC steuern. Einziges Problem: Ich habe zu wenig Motoranschlüsse, sodass ich einige Anschlüsse zweifach besetzen muss. Nachteil: Ich muss wenn ich zwischen Arm-Bewegung und Haken-Bewegung, bzw. zwischen Katzen-Bewegung und  Haken-Endschalter (die Lampe war's) umschalten will einen Schalter am Interface umlegen. Hier sieht man den Schalter ganz links im Bild, direkt neben dem Interface. (Den anderen Schalter kann man auf Grund der Perspektive nicht sehen.)