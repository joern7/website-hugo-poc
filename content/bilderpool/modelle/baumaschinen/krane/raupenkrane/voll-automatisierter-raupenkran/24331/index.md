---
layout: "image"
title: "Der Seilzug der Katze"
date: "2009-06-12T19:41:21"
picture: "cn16.jpg"
weight: "16"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24331
- /detailsed2a-2.html
imported:
- "2019"
_4images_image_id: "24331"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24331 -->
Mit dem widerspänstigen, aber dennoch starken Motor wird der Haken nach oben gezogen.