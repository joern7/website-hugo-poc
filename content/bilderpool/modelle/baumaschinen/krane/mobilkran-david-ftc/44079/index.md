---
layout: "image"
title: "Steuerung"
date: "2016-08-01T19:00:30"
picture: "mlkn11.jpg"
weight: "11"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44079
- /details8251.html
imported:
- "2019"
_4images_image_id: "44079"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44079 -->
Selbstbau IR Empfänger als Steuerung für den Kran, mehr zum Empfänger: https://ftcommunity.de/categories.php?cat_id=3219 oder in der ft:pedia 2/2016: https://ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-2.pdf
