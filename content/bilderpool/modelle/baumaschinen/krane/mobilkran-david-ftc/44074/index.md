---
layout: "image"
title: "Hydraulik"
date: "2016-08-01T19:00:30"
picture: "mlkn06.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44074
- /details9e77.html
imported:
- "2019"
_4images_image_id: "44074"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44074 -->
Der Kranarm lässt sich pneumatisch anheben (im Original natürlich hydraulisch).