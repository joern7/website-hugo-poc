---
layout: "image"
title: "Mitteldifferentialgetriebe"
date: "2016-08-01T19:00:30"
picture: "mlkn18.jpg"
weight: "18"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44086
- /details251f.html
imported:
- "2019"
_4images_image_id: "44086"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44086 -->
Der Motor (Encodermotor) bzw. Traktormotor treibt das Fahrzeug an. Er überträgt die Kraft mit der Übersetzung 10:20 auf das Mitteldifferentialgetriebe, das wiederum die Kraft zur Vorderachse und den hinteren Achsen bringt