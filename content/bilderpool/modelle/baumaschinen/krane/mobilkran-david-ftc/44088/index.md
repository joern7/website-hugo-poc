---
layout: "image"
title: "Vorderachse ohne Reifen"
date: "2016-08-01T19:00:30"
picture: "mlkn20.jpg"
weight: "20"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44088
- /details0e67.html
imported:
- "2019"
_4images_image_id: "44088"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44088 -->
Bei abgenommen Reifen wird die Sicht auf das Kardangelenk frei, das sich in einem um 7.5 mm aufgeborten Baustein 15 befindet.