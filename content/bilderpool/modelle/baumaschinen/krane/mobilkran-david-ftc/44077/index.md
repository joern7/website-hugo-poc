---
layout: "image"
title: "Kabine"
date: "2016-08-01T19:00:30"
picture: "mlkn09.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44077
- /detailscc4a-2.html
imported:
- "2019"
_4images_image_id: "44077"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44077 -->
Kabine mit Tür zum Einstieg