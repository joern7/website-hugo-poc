---
layout: "image"
title: "Kranwagen mit Aufbau"
date: "2016-08-01T19:00:30"
picture: "mlkn13.jpg"
weight: "13"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44081
- /details87d6.html
imported:
- "2019"
_4images_image_id: "44081"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44081 -->
