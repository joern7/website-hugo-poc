---
layout: "image"
title: "Unterseite - Überblick"
date: "2016-08-01T19:00:30"
picture: "mlkn16.jpg"
weight: "16"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44084
- /details29d0.html
imported:
- "2019"
_4images_image_id: "44084"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44084 -->
Blick auf den Antrieb und die Lenkung: Alle Achsen werden über einen Motor angetrieben, jede Achse besitzt ein Differentialgetriebe und kann über einen eignen Servomotor gelenkt werden