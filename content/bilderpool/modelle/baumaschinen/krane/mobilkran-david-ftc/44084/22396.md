---
layout: "comment"
hidden: true
title: "22396"
date: "2016-08-07T12:36:59"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Marten,

über die Fernsteuerung gibt man den Lenkwinkel vor, über die DIP Schalter wählt man den Lenkmodus. Je nach Lenkmodus ist im Arduino Programm ein Parameter hinterlegt, wie stark und in welche Richtung jede Achse einschlagen soll. In der Praxis sieht das so aus (Bsp):

Lenkmodus: Gleichlauflenkung
Lenkwinkel: +15 (voller Einschlag nach rechts)

Das Programm sucht dann aus einer Liste die Parameter der Achsen für "Gleichlauflenkung"
Achse 1: +100 % Einschlag nach rechts im Verhältnis 1:1 zum Joystick
Achse 2: - 50 % Einschlag nach links im Verhältnis 1:-2 zum Joystick
Achse 3: -100 % Einschlag nach links im Verhältnis 1:-1 zum Joystick

Die Paramter werden mit dem Lenkwinkel vom Control Set verrechnet:
Achse 1: 100% * 15 = 15
Achse 2: -50% *15 = -7.5
Achse 3: -100% * 15 = -15

Abschließend werden die Lenkwinkel noch mal 4/3 multipliziert, sodass man einen maximalen Lenkeinschlag von 20 ° je Achse erhält.

Gruß
David