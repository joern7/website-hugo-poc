---
layout: "image"
title: "Marspau"
date: "2009-06-15T22:51:45"
picture: "Kran_4_006.jpg"
weight: "30"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24388
- /details1ee6.html
imported:
- "2019"
_4images_image_id: "24388"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24388 -->
This is the same model,with the
addition of a security red flasher.

Das ist das gleiche Modell, mit der
Hinzufügen von eine rote Sicherheit flasher