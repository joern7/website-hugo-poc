---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:28"
picture: "Kranr_3_003.jpg"
weight: "28"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Global", "view", "of", "the", "underside", "of", "the", "kran", "showing", "the", "carriage", "the", "hook", "and", "the", "chain", "mechanism"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24099
- /details78ba.html
imported:
- "2019"
_4images_image_id: "24099"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24099 -->
Global view of the underside of the kran showing the carriage the hook and the chain mechanism.

Globale Ansicht der Unterseite der Kran zeigt der Beförderung der Haken und der Kette-Mechanismus