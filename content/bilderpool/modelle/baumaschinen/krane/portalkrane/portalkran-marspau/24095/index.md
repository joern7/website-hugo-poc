---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Krane_2_020.jpg"
weight: "24"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Motor", "and", "mechanism", "that", "actuate", "the", "screw"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24095
- /detailsb150-2.html
imported:
- "2019"
_4images_image_id: "24095"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24095 -->
Motor and mechanism that actuate the screw.


Motor und Mechanismus, die  Schraube zu betätigen