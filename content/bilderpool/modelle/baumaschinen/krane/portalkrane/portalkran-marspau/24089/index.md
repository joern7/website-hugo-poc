---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:23"
picture: "Krane_2_008_2.jpg"
weight: "18"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Detail", "of", "the", "HOOK", "UP", "limit", "switch."]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24089
- /detailse3c3.html
imported:
- "2019"
_4images_image_id: "24089"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24089 -->
Detail of the HOOK UP limit switch.

Detail des Schalters Limit HOOK UP.