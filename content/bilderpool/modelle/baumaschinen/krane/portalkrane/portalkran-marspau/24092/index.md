---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:23"
picture: "Krane_2_012.jpg"
weight: "21"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["General", "view", "of", "the", "top"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24092
- /details10bb-2.html
imported:
- "2019"
_4images_image_id: "24092"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24092 -->
General view of the top.

Translation;

Allgemeine Ansicht von oben