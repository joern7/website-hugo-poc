---
layout: "image"
title: "Portal"
date: "2009-05-22T20:56:48"
picture: "Krane_2_004_2.jpg"
weight: "14"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Photo", "of", "one", "end", "the", "carriege", "track", "structure", "and", "the", "chain", "mecanism"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24085
- /details40d0.html
imported:
- "2019"
_4images_image_id: "24085"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T20:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24085 -->
