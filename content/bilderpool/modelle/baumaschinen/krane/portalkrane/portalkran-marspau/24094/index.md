---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Krane_2_014.jpg"
weight: "23"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Top", "view", "showing", "the", "Hook", "positioning", "screw", "with", "the", "phototransistor", "and", "lamp"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24094
- /details0189-2.html
imported:
- "2019"
_4images_image_id: "24094"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24094 -->
Top view showing the Hook positioning screw with the phototransistor and lamp.

Ansicht von oben zeigt der Hook Positionierung Schraube mit der Phototransistor und der Lampe