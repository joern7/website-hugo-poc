---
layout: "image"
title: "PortalKrane"
date: "2009-05-22T20:56:48"
picture: "Krane_2_005_2.jpg"
weight: "15"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["One", "of", "the", "two", "limit", "switches", "that", "make", "the", "carriage", "change", "direction."]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24086
- /detailsbefa.html
imported:
- "2019"
_4images_image_id: "24086"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T20:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24086 -->
One of the two limit switches that make the carriage change direction.

Eines der beiden Endlagenschalter, die die Beförderung Textrichtung ändern