---
layout: "image"
title: "PortalKran (Marspau) update"
date: "2009-06-15T22:51:45"
picture: "Kran_4_007.jpg"
weight: "31"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24389
- /details569e-2.html
imported:
- "2019"
_4images_image_id: "24389"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24389 -->
The same portalkran,but I replaced the
old M motor,which consume a lot of current,by aPower Motor,8:1,
which is stronger,consume less power.
But it is more difficult to put in place.


Die gleichen Portalkran, aber ich ersetzt die
alte M Motor, die eine Menge Strom, durch die aMacht Motor, 8: 1, verbrauchen
die stärkere, weniger Strom verbrauchen.
Aber es ist schwieriger zu schaffen.