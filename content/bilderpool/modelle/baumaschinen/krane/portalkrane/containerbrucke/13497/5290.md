---
layout: "comment"
hidden: true
title: "5290"
date: "2008-02-12T18:33:07"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Hallo Jan,

wenn Du den Verschlussmechanismus des Kegelrades (Klaue) durch die Statikplatte steckst und dann um 90° drehst bleibt sie beim herausziehen leicht stecken. Um ausreichende Sicherheit gegen lösen zu garantieren musst Du die Klauen theoretisch etwas aufweiten. Diese Aufgabe übernimmt der Streichholzkeil. Nun brauchst Du nur sicher zu stellen, dass die Klaue in ihrer aufgeweiterten Position bleibt. Auch diese Aufgabe übernimmt der Keil.

P.S. Es schadet nicht wenn die Klauen in Ihrer Tiefe mit einem Messer nachgearbeitet werden. 

Gruß Jürgen