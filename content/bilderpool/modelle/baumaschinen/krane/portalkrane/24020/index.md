---
layout: "image"
title: "Molenkran Kopfstand"
date: "2009-05-14T16:34:28"
picture: "FTC-Kopfstand.jpg"
weight: "6"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/24020
- /details21b6.html
imported:
- "2019"
_4images_image_id: "24020"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24020 -->
Für nachträgliche Arbeiten am unteren Fahrwerk habe ich ein separates Stützgestell
aufgebaut. Jedoch müssen die Gegengewichte am hinteren Ausleger hierfür entfernt
werden. Der Kopfstand an sich ist damit zwar unproblematisch, allerdings ist jetzt der
Schwerpunkt relativ ungünstig (Kippgefahr).