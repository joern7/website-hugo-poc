---
layout: "image"
title: "Portalkran 1"
date: "2007-01-12T17:00:21"
picture: "portalkran1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8374
- /details11f9.html
imported:
- "2019"
_4images_image_id: "8374"
_4images_cat_id: "772"
_4images_user_id: "502"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8374 -->
