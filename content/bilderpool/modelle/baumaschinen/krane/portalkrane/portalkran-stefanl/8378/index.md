---
layout: "image"
title: "Portalkran 5"
date: "2007-01-12T17:00:32"
picture: "portalkran5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8378
- /details881d.html
imported:
- "2019"
_4images_image_id: "8378"
_4images_cat_id: "772"
_4images_user_id: "502"
_4images_image_date: "2007-01-12T17:00:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8378 -->
