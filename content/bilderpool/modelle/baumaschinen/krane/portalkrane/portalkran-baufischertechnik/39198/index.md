---
layout: "image"
title: "Sotieranlage"
date: "2014-08-08T21:21:23"
picture: "DSC00230.jpg"
weight: "10"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
keywords: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39198
- /details0243.html
imported:
- "2019"
_4images_image_id: "39198"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39198 -->
Die Rutsche endet am Förderband der Sotieranlage. Man kann entscheiden ob die Kugel "Müll" ist oder verarbeitet wird.