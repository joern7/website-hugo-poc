---
layout: "image"
title: "Gewichtskontrolle"
date: "2014-08-08T21:21:23"
picture: "DSC00232.jpg"
weight: "3"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
keywords: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39191
- /details380a.html
imported:
- "2019"
_4images_image_id: "39191"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39191 -->
Dieser Taster schützt den Kran vor Überlastung, indem der beim Hubversuch einer zu schweren Last den Magneten abschaltet.