---
layout: "overview"
title: "Portalkran (baufischertechnik)"
date: 2020-02-22T08:11:28+01:00
legacy_id:
- /php/categories/2931
- /categoriesdb7c.html
- /categoriesf12b.html
- /categories2fda.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2931 --> 
Hier habe ich den Portalkran aus Super Cranes modifiziert und motorisiert.
Modifikationen: Neue Seilzüge, Gewichtskontolle für Lasten, Energieketten zur Kabelführung