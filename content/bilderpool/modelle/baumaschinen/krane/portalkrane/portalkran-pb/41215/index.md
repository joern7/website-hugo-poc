---
layout: "image"
title: "Containerkr-lr-16"
date: "2015-06-24T14:11:15"
picture: "containerkran11.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41215
- /details90ea.html
imported:
- "2019"
_4images_image_id: "41215"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41215 -->
