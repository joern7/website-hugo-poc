---
layout: "image"
title: "Containerkr-lr-54"
date: "2015-06-24T14:11:15"
picture: "containerkran37.jpg"
weight: "37"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41241
- /detailsa707-2.html
imported:
- "2019"
_4images_image_id: "41241"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41241 -->
Fahrgestell. Zwei Mal zu bauen. Den 50:1 Powermotor ist besser Geeignet, met den Schwarzen ist der Kran ein Bischen unruhig beim Fahren, wodurch den Greifer mit Container ziemlich viel Schwenkt.