---
layout: "image"
title: "Containerkr-lr-22"
date: "2015-06-24T14:11:15"
picture: "containerkran17.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41221
- /details923b-3.html
imported:
- "2019"
_4images_image_id: "41221"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41221 -->
Befestigung vom Ausleger. Später kommt noch Zwei Stück Seil dazu.