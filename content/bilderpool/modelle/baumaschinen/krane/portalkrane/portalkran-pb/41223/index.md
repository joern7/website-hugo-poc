---
layout: "image"
title: "Containerkr-lr-24"
date: "2015-06-24T14:11:15"
picture: "containerkran19.jpg"
weight: "19"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41223
- /details8864-2.html
imported:
- "2019"
_4images_image_id: "41223"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41223 -->
Ausleger.