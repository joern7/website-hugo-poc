---
layout: "image"
title: "Containerkr-lr-01"
date: "2015-06-24T14:11:15"
picture: "containerkran01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41205
- /detailsd3d7.html
imported:
- "2019"
_4images_image_id: "41205"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41205 -->
Erst Mal einige Gesamtansichte.