---
layout: "image"
title: "Containerkr-lr-57"
date: "2015-06-24T14:11:15"
picture: "containerkran38.jpg"
weight: "38"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41242
- /details85a2.html
imported:
- "2019"
_4images_image_id: "41242"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41242 -->
Ich hatte zuerst den normalen Powerrmotor eingesetzt, aber wie gesagt: den Roten (50:1) wirkt hier besse.