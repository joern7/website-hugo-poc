---
layout: "comment"
hidden: true
title: "20759"
date: "2015-06-24T14:30:25"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist ja eine ganze Bauanleitung - für ein tolles Modell in perfekten Bildern. Gratuliere!

Um den Zapfen in dem BS 7,5 sorge ich mich etwas. Vielleicht würde ich da einen dieser 5x15x30-Steine verwenden, damit das auch nach oben geht und so weniger Hebelkräfte aushalten muss.

Gruß,
Stefan