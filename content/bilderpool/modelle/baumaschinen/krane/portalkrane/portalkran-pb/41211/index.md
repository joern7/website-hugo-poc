---
layout: "image"
title: "Containerkr-lr-10"
date: "2015-06-24T14:11:15"
picture: "containerkran07.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41211
- /details54e8-2.html
imported:
- "2019"
_4images_image_id: "41211"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41211 -->
Die grauen Lamp-steine nutze ich oft um Kabel zu verlengern und besser modular Bauen zu können. De Kran fäfhrt auch, und wird angetrieben von Zwei Powermotoren.