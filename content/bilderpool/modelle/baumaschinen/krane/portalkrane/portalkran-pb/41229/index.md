---
layout: "image"
title: "Containerkr-lr-32"
date: "2015-06-24T14:11:15"
picture: "containerkran25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41229
- /details27c3.html
imported:
- "2019"
_4images_image_id: "41229"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41229 -->
Einfach dazwichen zu schieben.