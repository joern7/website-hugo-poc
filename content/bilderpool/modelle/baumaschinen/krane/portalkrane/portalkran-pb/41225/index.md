---
layout: "image"
title: "Containerkr-lr-27"
date: "2015-06-24T14:11:15"
picture: "containerkran21.jpg"
weight: "21"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41225
- /details003c.html
imported:
- "2019"
_4images_image_id: "41225"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41225 -->
Ausleger mit Schienen und befestigt.