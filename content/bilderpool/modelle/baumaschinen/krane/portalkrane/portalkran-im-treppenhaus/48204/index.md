---
layout: "image"
title: "Portalkran von schräg oben"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-8.jpg"
weight: "8"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/48204
- /details1973.html
imported:
- "2019"
_4images_image_id: "48204"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48204 -->
