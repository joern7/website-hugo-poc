---
layout: "image"
title: "Molenkran komplett"
date: "2009-05-14T16:34:26"
picture: "FTC-Totale.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/24015
- /detailsa9c1-3.html
imported:
- "2019"
_4images_image_id: "24015"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24015 -->
Hier ist der Molenkran als Ganzes zu sehen. Die auffälligste Änderung ist das
portale Tragwerk. Der Unterbau verkürzt sich dabei auf den Hauptrahmen und
die schrägen Stützen. Dort sind der Antrieb des Fahrwerks, die Drehmechanik
sowie die drehbare Stromübertragung untergebracht. Einzige Verbindung ist
die 6-polige Stromzufuhr via Energiekette in einer Kombination aus Gleich-
und Wechselspannung.

Der Oberbau des Krans entspricht weitgehend dem Original. Nur die Abspann-
werke der beiden Ausleger sind aus Streben gefertigt. Und mit insgesamt vier
Motoren ausgestattet ist der Kran via manueller Steuerung vollständig mobil.