---
layout: "image"
title: "Greifer"
date: "2013-11-29T21:55:24"
picture: "IMG_2897.jpg"
weight: "10"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- /php/details/37851
- /detailse9bd.html
imported:
- "2019"
_4images_image_id: "37851"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37851 -->
Hier um ca. 90 Grad gedreht. Antrieb mit Mini-Motor.