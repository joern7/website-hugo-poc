---
layout: "image"
title: "39 Drehkranz"
date: "2012-04-01T17:12:16"
picture: "kran6.jpg"
weight: "39"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34741
- /details2c39-2.html
imported:
- "2019"
_4images_image_id: "34741"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-04-01T17:12:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34741 -->
und ohne Aufsatz