---
layout: "image"
title: "29 Steurpult"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell29.jpg"
weight: "29"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34489
- /details9257.html
imported:
- "2019"
_4images_image_id: "34489"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34489 -->
nochmal gesamt