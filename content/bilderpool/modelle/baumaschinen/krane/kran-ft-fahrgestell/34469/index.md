---
layout: "image"
title: "09 Gelenk"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34469
- /detailsad6c.html
imported:
- "2019"
_4images_image_id: "34469"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34469 -->
Das ist das Gelenk - Im Vergleich zum Mast ist es relativ klein und deshalb auch nicht ganz so stabil, aber es hält