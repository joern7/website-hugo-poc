---
layout: "image"
title: "18 Drehkranz"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell18.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34478
- /detailsc7ca.html
imported:
- "2019"
_4images_image_id: "34478"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34478 -->
max. 90° auf beiden Seiten - es wären aber auch mehr möglich, wenn man alles auf dem Drehkranz einen Baustein höher baut.