---
layout: "image"
title: "07 Rückseite"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34467
- /details4dd9-2.html
imported:
- "2019"
_4images_image_id: "34467"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34467 -->
von hinten - unten ist der Anschluss für das Steuerpult