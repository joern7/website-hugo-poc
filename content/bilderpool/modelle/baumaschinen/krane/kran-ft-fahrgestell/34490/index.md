---
layout: "image"
title: "30 Baustein"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell30.jpg"
weight: "30"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34490
- /detailseff6.html
imported:
- "2019"
_4images_image_id: "34490"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34490 -->
Dieser nette Baustein hat mir erst ermöglicht, den Kran so zu bauen. Auch wenn ich ihn noch nie irgendwo anders gesehen habe, dachte ich mir, dass sich doch damit etwas bauen lässt. Er lässt sich in jeden beliebigen Winkel drehen. 
Hat dazu jemand vielleicht mehr Informationen ?