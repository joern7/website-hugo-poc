---
layout: "image"
title: "05 Seite"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34465
- /details705d.html
imported:
- "2019"
_4images_image_id: "34465"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34465 -->
von der Seite - der Akku dient nur als Balast