---
layout: "image"
title: "20 Beleuchtung"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell20.jpg"
weight: "20"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34480
- /detailsb610.html
imported:
- "2019"
_4images_image_id: "34480"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34480 -->
im Dunkeln