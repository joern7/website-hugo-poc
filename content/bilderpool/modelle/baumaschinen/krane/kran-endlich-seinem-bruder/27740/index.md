---
layout: "image"
title: "Kran 01"
date: "2010-07-13T15:40:03"
picture: "kran01.jpg"
weight: "1"
konstrukteure: 
- "Endlich & sein Bruder"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27740
- /details1656.html
imported:
- "2019"
_4images_image_id: "27740"
_4images_cat_id: "1998"
_4images_user_id: "1162"
_4images_image_date: "2010-07-13T15:40:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27740 -->
Hier sieht man den Kran, den mein Bruder und ich zusammen gebaut haben. Dieser Kran kann fahren, und über den E-Magneten kann er Metalle anziehen. Über den Mini-Motor kann er das Seil hoch und runter lassen. die Lampe (noch nicht funktionsfähig) kann er seinen "Arbeitsplatz" beleuchten. Der eingebaute Akku diehnt zusätzlich noch als Gegengewicht. 

Als Werkstücke, die der Kran heben kann, haben wir auf die gelben Tonnen von FT Unterlegscheiben geklebt, und ie Tonnen können so über den E-Magnet angezogen werden.

Viel Spaß beim Ansehen der Bilder

Endlich