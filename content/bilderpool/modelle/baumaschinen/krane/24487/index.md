---
layout: "image"
title: "Hafenkran"
date: "2009-06-29T23:27:03"
picture: "Hafenkran_1981.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/24487
- /details53dc.html
imported:
- "2019"
_4images_image_id: "24487"
_4images_cat_id: "609"
_4images_user_id: "968"
_4images_image_date: "2009-06-29T23:27:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24487 -->
