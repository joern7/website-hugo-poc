---
layout: "comment"
hidden: true
title: "9497"
date: "2009-06-30T04:12:13"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

An so eine "höhen"-versetzte Konstruktion habe ich bei meinem Molenkran
auch gedacht. Hab' sie aber wieder verworfen, da diese Art Brücken meist
in Verbindung mit Gebäuden errichtet werden. Und dann könnte der Kran
sich gar nicht mehr richtig drehen; der Oberbau hätte (zumindest in der Re-
alität rein hypotetisch) keinen vollen Drehradius zur Verfügung.

Alles in allem finde ich deinen Kran aber super gelungen!

Gruß, Thomas