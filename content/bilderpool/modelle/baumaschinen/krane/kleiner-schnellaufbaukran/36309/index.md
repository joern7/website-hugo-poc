---
layout: "image"
title: "Seilwinde"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran25.jpg"
weight: "25"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36309
- /detailsbe92.html
imported:
- "2019"
_4images_image_id: "36309"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36309 -->
Das Schwarze im Bild ist der Mini Motor zum Antrieb.