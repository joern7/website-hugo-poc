---
layout: "image"
title: "Der Aufbau"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran12.jpg"
weight: "12"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36296
- /details8eca.html
imported:
- "2019"
_4images_image_id: "36296"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36296 -->
-