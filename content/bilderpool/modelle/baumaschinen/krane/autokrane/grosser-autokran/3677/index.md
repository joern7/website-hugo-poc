---
layout: "image"
title: "Rahmendetail Oberwagen"
date: "2005-03-04T14:31:33"
picture: "Oberwagen_Rahmendetail.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3677
- /detailsdef4.html
imported:
- "2019"
_4images_image_id: "3677"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-03-04T14:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3677 -->
Hier dasselbe Prinzip wie unten:M4-Schraube durch die Platten ins Alu ergibt für mich bisher ungeahnte Festigkeit