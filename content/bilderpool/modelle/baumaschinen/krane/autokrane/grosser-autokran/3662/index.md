---
layout: "image"
title: "Gesamtansicht2"
date: "2005-02-28T12:29:30"
picture: "Gesamtansicht2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3662
- /detailsd08f.html
imported:
- "2019"
_4images_image_id: "3662"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-02-28T12:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3662 -->
Vorgaben von mir:Robust,einfach zu handhaben,wenigstens 2 Motorfunktionen.Ergebnis:Rahmen von Ober- und Unterwagen mit Baumarktalu´s verstärkt,keine Stützen,keine Lenkung,Haken und Ausleger motorisiert.