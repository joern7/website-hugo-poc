---
layout: "image"
title: "Der Oberwagenrahmen"
date: "2005-03-04T14:31:33"
picture: "Oberwagen_Rahmen.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3673
- /details4cc9-3.html
imported:
- "2019"
_4images_image_id: "3673"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-03-04T14:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3673 -->
Der Oberwagen ist durch 2 längslaufende Alus verstärkt welche durch die Löcher der Bauplatten verschraubt sind