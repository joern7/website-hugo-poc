---
layout: "image"
title: "Rahmen des Unterwagens"
date: "2005-02-28T12:29:30"
picture: "Unterwagen_Rahmen.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3668
- /detailsa6cb.html
imported:
- "2019"
_4images_image_id: "3668"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-02-28T12:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3668 -->
Hier sieht man den Aluträger welcher sich durch den ganzen Unterbau zieht und für enorme Stabilität sorgt.Nicht besser als FT-Alu´s aber viel billiger.