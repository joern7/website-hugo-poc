---
layout: "image"
title: "Faltkran34"
date: "2005-03-14T14:45:05"
picture: "Faltkran34.JPG"
weight: "34"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3845
- /detailsed7e.html
imported:
- "2019"
_4images_image_id: "3845"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3845 -->
