---
layout: "image"
title: "Faltkran41"
date: "2005-03-14T14:45:05"
picture: "Faltkran41.JPG"
weight: "41"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3852
- /detailsd11b-2.html
imported:
- "2019"
_4images_image_id: "3852"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3852 -->
