---
layout: "image"
title: "Faltkran06"
date: "2005-03-12T19:48:41"
picture: "Faltkran06.JPG"
weight: "6"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3768
- /details4d28.html
imported:
- "2019"
_4images_image_id: "3768"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-12T19:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3768 -->
