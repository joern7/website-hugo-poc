---
layout: "image"
title: "Autokran 13"
date: "2007-02-03T12:18:38"
picture: "autokran05.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8805
- /details9317-3.html
imported:
- "2019"
_4images_image_id: "8805"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-03T12:18:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8805 -->
