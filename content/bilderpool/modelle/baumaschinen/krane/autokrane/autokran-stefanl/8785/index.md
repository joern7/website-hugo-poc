---
layout: "image"
title: "Autokran 5"
date: "2007-02-02T21:23:13"
picture: "autokran5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8785
- /detailscc5f.html
imported:
- "2019"
_4images_image_id: "8785"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8785 -->
