---
layout: "image"
title: "Autokran 6"
date: "2007-02-02T21:23:13"
picture: "autokran6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8786
- /detailsf63f.html
imported:
- "2019"
_4images_image_id: "8786"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8786 -->
