---
layout: "image"
title: "Faltkran01"
date: "2007-06-09T22:56:32"
picture: "faltkran3.jpg"
weight: "26"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10780
- /details4207.html
imported:
- "2019"
_4images_image_id: "10780"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T22:56:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10780 -->
Hier ist der Kran in voller Größe aufgebaut. Höhe ca 180cm(Ausleger)