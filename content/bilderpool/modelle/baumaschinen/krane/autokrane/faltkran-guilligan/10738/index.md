---
layout: "image"
title: "Grundmodell Faltkran"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan02.jpg"
weight: "2"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10738
- /details9815.html
imported:
- "2019"
_4images_image_id: "10738"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10738 -->
