---
layout: "image"
title: "Faltkran00"
date: "2007-06-09T22:56:32"
picture: "faltkran2.jpg"
weight: "25"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10779
- /details4e08.html
imported:
- "2019"
_4images_image_id: "10779"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T22:56:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10779 -->
Hier sieht man den Drehmechanismus