---
layout: "image"
title: "Ausleger00"
date: "2007-06-17T22:19:51"
picture: "kran1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10873
- /details97f3.html
imported:
- "2019"
_4images_image_id: "10873"
_4images_cat_id: "984"
_4images_user_id: "389"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10873 -->
Hier sieht man das Gelenk wo die beiden Ausleger mit einander verbunden sind. Es sind Bausteine 15 mit roten Kopf die drehbar mit dem Baustein 30 verbunden sind. Die Bausteine 30 sind mit den Schienen verbunden.

Außerdem ist hier die Konstruktion des Auslegers gut zu erkennen. Ich habe einfach 120er Winkelträger mit den Schienen per S-Riegel verbunden und oben drauf noch Diagonalstreben verbaut.