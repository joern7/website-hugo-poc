---
layout: "image"
title: "Faltkran03"
date: "2007-06-09T22:56:32"
picture: "faltkran5.jpg"
weight: "28"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10782
- /details0647.html
imported:
- "2019"
_4images_image_id: "10782"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T22:56:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10782 -->
Auf diesem Bild kann man sehr gut die Neigung des Auslegers erkennen. Sie ist wie beim Orginal stufenlos zu verstellen. Selbst bei 45° kann die Laufkatze problemlos vor und zurück gefahren werden.