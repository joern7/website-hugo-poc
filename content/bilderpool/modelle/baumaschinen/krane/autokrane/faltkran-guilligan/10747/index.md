---
layout: "image"
title: "Aufbau Oberwagen"
date: "2007-06-09T14:58:57"
picture: "faltkranguilligan11.jpg"
weight: "11"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10747
- /details717a.html
imported:
- "2019"
_4images_image_id: "10747"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:57"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10747 -->
