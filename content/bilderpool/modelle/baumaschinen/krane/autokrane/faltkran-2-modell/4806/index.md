---
layout: "image"
title: "Faltkran Model 2-24"
date: "2005-09-25T14:02:42"
picture: "Faltkran_2-34.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4806
- /details7f60.html
imported:
- "2019"
_4images_image_id: "4806"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4806 -->
