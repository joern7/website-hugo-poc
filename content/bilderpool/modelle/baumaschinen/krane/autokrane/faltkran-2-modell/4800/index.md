---
layout: "image"
title: "Faltkran Model 2-18"
date: "2005-09-25T14:02:34"
picture: "Faltkran_2-21.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4800
- /details0b8d.html
imported:
- "2019"
_4images_image_id: "4800"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4800 -->
