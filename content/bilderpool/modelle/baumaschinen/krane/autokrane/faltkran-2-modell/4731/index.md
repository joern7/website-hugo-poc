---
layout: "image"
title: "Faltkran Model 2-05"
date: "2005-09-25T09:00:49"
picture: "Faltkran_2-06.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4731
- /detailsc546.html
imported:
- "2019"
_4images_image_id: "4731"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T09:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4731 -->
