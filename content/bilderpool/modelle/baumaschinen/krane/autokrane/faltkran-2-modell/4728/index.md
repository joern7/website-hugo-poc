---
layout: "image"
title: "Faltkran Model 2-02"
date: "2005-09-25T09:00:49"
picture: "Faltkran_2-03.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4728
- /details53b8-2.html
imported:
- "2019"
_4images_image_id: "4728"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T09:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4728 -->
