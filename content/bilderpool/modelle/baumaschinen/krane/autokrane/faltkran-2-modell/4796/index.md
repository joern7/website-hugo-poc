---
layout: "image"
title: "Faltkran Model 2-14"
date: "2005-09-25T14:02:34"
picture: "Faltkran_2-16.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4796
- /details827f-2.html
imported:
- "2019"
_4images_image_id: "4796"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4796 -->
