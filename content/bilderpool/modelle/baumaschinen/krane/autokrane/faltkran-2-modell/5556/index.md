---
layout: "image"
title: "Alle Rader"
date: "2006-01-02T23:29:20"
picture: "Faltkran_2-46.jpg"
weight: "36"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/5556
- /detailse5a4.html
imported:
- "2019"
_4images_image_id: "5556"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2006-01-02T23:29:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5556 -->
