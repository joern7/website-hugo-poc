---
layout: "image"
title: "Wippspitze 1"
date: "2006-10-29T19:02:13"
picture: "Kran03a.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7277
- /detailsdcfd.html
imported:
- "2019"
_4images_image_id: "7277"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7277 -->
Hier eine etwas nähere Aufnahme der Wippspitze. Die maximale Höhe des Krans beträgt derzeit ca. 2,50m zzgl. Fahrgestell. Die Seillängen betragen ca. 18m für den Hauptmast, 20m für die Wippspitze und 22m für den Haken. Geplant ist aber noch ein zweiter, kleinerer Haken.