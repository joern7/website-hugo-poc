---
layout: "image"
title: "Detail Mastgelenk"
date: "2006-11-19T13:35:02"
picture: "Oberwagenrahmen04b.jpg"
weight: "9"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7494
- /details9daa.html
imported:
- "2019"
_4images_image_id: "7494"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-11-19T13:35:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7494 -->
Hier eine Detailaufnahme des Hauptgelenkes für den Mast. Wesentlich massiver und stabiler gebaut als vorher.
Was mir noch nicht gefällt, ist die Befestigung des SA-Bocks nur mit zwei wackeligen Gelenken, da muß ich mir auch noch was einfallen lassen. Die könnten unter Last die Grätsche machen.