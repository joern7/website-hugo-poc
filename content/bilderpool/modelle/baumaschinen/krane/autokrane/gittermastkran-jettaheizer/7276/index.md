---
layout: "image"
title: "Gesamtansicht 1"
date: "2006-10-29T19:02:13"
picture: "Kran01a.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7276
- /detailsa69b.html
imported:
- "2019"
_4images_image_id: "7276"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7276 -->
Erste Gesamtansicht des Gittermastes mit Wippspitze und Haken. Den Haken habe ich bei Trucker abgeschaut (siehe auch unter "Großer Autokran" Seite 6). Mit Ausnahme der Schnüre besteht der gesamte Mast aus originalen, unveränderten FT-Teilen.