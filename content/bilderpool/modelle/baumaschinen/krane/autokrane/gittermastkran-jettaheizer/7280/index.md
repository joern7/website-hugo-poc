---
layout: "image"
title: "Oberwagen 2"
date: "2006-10-29T19:02:13"
picture: "Kran09a.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7280
- /details29bb-2.html
imported:
- "2019"
_4images_image_id: "7280"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7280 -->
Noch eine Ansicht des Oberwagens. Hier erkennt man gerade noch so die drei Motoren sowie die Gegengewichte in Form zweier Batterieboxen. Die Kassetten auf den Batterieboxen sind momentan noch leer, da kommt später noch Gewicht rein und ein Deckel drauf. Bei Bedarf können auch noch weitere Kassetten darauf gesetzt werden.