---
layout: "image"
title: "Gesamtansicht"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran01.jpg"
weight: "1"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/7476
- /details6816.html
imported:
- "2019"
_4images_image_id: "7476"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7476 -->
Gesamtansicht meines Gittermastkrans, mit dem ich zwar lange nicht an Guilligan ran komme, aber immerhin...
Maximale Höhe 1,55 m
Länge des Hauptauslegers 1,35 m
Länge des Nebenauslegers 80 cm
Bauzeit ungefähr 30 Stunden

2 Minimotoren (Last auf-ab und Hauptausleger auf-ab)
2 PowerMotoren (Fahrwerk) Versorgung des Fahrwerks über Schleifring
2 Akkupacks (1 für Licht, 1 für IR)
IR Control Set für die Motoren