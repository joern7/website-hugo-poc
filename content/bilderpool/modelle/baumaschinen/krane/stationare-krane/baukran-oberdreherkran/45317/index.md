---
layout: "image"
title: "baukran37.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran37.jpg"
weight: "49"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45317
- /details544b-2.html
imported:
- "2019"
_4images_image_id: "45317"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45317 -->
