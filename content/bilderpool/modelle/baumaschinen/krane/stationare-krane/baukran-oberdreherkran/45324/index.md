---
layout: "image"
title: "baukran44.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran44.jpg"
weight: "56"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45324
- /detailsd115.html
imported:
- "2019"
_4images_image_id: "45324"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45324 -->
Seilführung. Ich hätte vielleicht noch ein Gelenkstein mit Feder nutzen oder etwas Ähnliches mussen om das Seil stets gut angespannt zu halten.