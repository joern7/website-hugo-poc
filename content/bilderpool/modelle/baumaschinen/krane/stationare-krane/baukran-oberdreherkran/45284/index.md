---
layout: "image"
title: "baukran04.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran04.jpg"
weight: "16"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45284
- /details4e6e.html
imported:
- "2019"
_4images_image_id: "45284"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45284 -->
