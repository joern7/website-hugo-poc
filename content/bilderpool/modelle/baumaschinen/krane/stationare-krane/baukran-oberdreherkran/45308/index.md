---
layout: "image"
title: "baukran28.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran28.jpg"
weight: "40"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45308
- /details274c.html
imported:
- "2019"
_4images_image_id: "45308"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45308 -->
Teil mit Gegengewicht