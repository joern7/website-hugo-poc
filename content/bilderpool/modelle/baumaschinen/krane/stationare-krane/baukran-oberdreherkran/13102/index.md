---
layout: "image"
title: "Kranmontage 02"
date: "2007-12-18T17:32:59"
picture: "kranmontage02.jpg"
weight: "4"
konstrukteure: 
- "Alphawolf"
fotografen:
- "Alphawolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- /php/details/13102
- /details3616.html
imported:
- "2019"
_4images_image_id: "13102"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13102 -->
