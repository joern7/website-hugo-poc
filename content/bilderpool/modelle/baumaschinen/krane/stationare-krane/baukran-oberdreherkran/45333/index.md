---
layout: "image"
title: "baukran53.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran53.jpg"
weight: "65"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45333
- /details3269.html
imported:
- "2019"
_4images_image_id: "45333"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45333 -->
