---
layout: "image"
title: "baukran07.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran07.jpg"
weight: "19"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45287
- /details12f8-2.html
imported:
- "2019"
_4images_image_id: "45287"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45287 -->
