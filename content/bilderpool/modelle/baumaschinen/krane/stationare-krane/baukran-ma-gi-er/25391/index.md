---
layout: "image"
title: "Gewicht von oben mit Fernsteuerung"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25391
- /details4778-2.html
imported:
- "2019"
_4images_image_id: "25391"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25391 -->
Dies ist ganz am ende des Krans hinten bei den Gewichten. rechts neben der Fernsteuerung erkennt man unten den Motor zum Antrieb des Wägelchens, welches vorne am ausleger hin und her fährt.