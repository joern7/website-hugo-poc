---
layout: "image"
title: "Seilbefestigung/-rückführung am Ende des Auslegers"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier03.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25393
- /detailsa888-2.html
imported:
- "2019"
_4images_image_id: "25393"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25393 -->
Das Seil, an dem der Haken befestigt ist, endet hier. Diese befestigung ist unten am Ausleger. Das Seil zur Bewegung des Wägelchens wird zurückgeführt.