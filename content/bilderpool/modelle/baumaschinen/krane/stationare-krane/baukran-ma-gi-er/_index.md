---
layout: "overview"
title: "Baukran von Ma-gi-er"
date: 2020-02-22T08:11:17+01:00
legacy_id:
- /php/categories/1779
- /categories8693.html
- /categories6f29.html
- /categories6d58.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1779 --> 
Dieser Kran ist 117.5 cm hoch und mit dem Gewichtsausgleich 98 cm breit. Der Ausleger sleber ist 66.5cm lang. Gearbeitet habe ich an dem Kran etwa 2 Tage, danach war er fertig. Der Mast ist so stabil, dass ich mich darauf setzten kann, ohne, dass er einknickt oder so.Er steht nicht super Stabil, da er auf vier Platten steht, welche eine leichte Rundung haben. P.S.:Alle Bilder habe ich mit meinem Handy geknipst!