---
layout: "image"
title: "Führerhäuschen"
date: "2009-09-27T23:59:15"
picture: "kranvonmagier11.jpg"
weight: "11"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25401
- /details9016-4.html
imported:
- "2019"
_4images_image_id: "25401"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25401 -->
