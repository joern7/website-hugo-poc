---
layout: "image"
title: "Kraftübertragung"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier04.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25394
- /detailsb2a3.html
imported:
- "2019"
_4images_image_id: "25394"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25394 -->
Diese übertragung ist die Hilfe, damit die Gewichte die Herunterziehen vorne den ausleger heraufziehen.