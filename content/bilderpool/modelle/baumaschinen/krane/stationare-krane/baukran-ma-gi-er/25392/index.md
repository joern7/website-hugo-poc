---
layout: "image"
title: "Wägelchen und Haken"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier02.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25392
- /details24f3.html
imported:
- "2019"
_4images_image_id: "25392"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25392 -->
Auf diesem Bild ist im oberen Teil das Wäglechen erkennbar, welches ähnlich des ft Baukastens auf einer Schiene fährt. Ich habe es jedoch etwas anderst gelöst, so, dass ich grosse Rollen verwenden konnte. Dieses System der Seilführung verhindert einen grossen teilder Verdrehungen des Seils. Auschliessen lässt es sich jedoch nicht. Ausserdem ist es relativ aufwändig das Seil wieder in die Rollen zu bringen, wenn es eimal herausgeflogen ist.