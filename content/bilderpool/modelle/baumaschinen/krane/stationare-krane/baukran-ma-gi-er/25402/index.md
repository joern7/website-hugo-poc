---
layout: "image"
title: "Gewicht von hinten"
date: "2009-09-27T23:59:15"
picture: "kranvonmagier12.jpg"
weight: "12"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25402
- /detailsca5c-2.html
imported:
- "2019"
_4images_image_id: "25402"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25402 -->
Da ich die Gelben Kästchen nie gefüllt habe, musste ich dies mit Achsen guttun.