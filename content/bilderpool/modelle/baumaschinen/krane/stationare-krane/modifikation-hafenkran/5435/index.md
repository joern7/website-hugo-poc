---
layout: "image"
title: "Ausleger"
date: "2005-11-30T17:28:46"
picture: "Oberteil_2.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/5435
- /details2d08-3.html
imported:
- "2019"
_4images_image_id: "5435"
_4images_cat_id: "645"
_4images_user_id: "59"
_4images_image_date: "2005-11-30T17:28:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5435 -->
