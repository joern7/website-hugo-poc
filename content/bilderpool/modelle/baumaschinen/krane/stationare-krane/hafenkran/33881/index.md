---
layout: "image"
title: "Wippkran"
date: "2012-01-11T18:34:32"
picture: "wippkran2.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33881
- /details08b6.html
imported:
- "2019"
_4images_image_id: "33881"
_4images_cat_id: "144"
_4images_user_id: "1361"
_4images_image_date: "2012-01-11T18:34:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33881 -->
Ansicht schräg von vorne