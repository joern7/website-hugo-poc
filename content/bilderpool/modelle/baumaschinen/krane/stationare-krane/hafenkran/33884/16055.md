---
layout: "comment"
hidden: true
title: "16055"
date: "2012-01-11T20:14:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
*Sehr* schön und ansprechend gebaut, Klasse!

Auch für Dich der Tipp: Bitte füll im Publisher mal Extras/Optionen aus. Dann ist beim Veröffentlichen gleich Konstrukteur und Fotograf ausgefüllt, und wir können z. B. nach allen Bildern von Dir suchen. Ich glaub, ich muss beim Start mal eine Erinnerung in den P. einbauen ;-)

Gruß,
Stefan