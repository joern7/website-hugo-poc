---
layout: "image"
title: "Ausleger-Verbindung unten"
date: "2008-03-21T15:41:45"
picture: "gittermastkranx4.jpg"
weight: "13"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/13992
- /details0399.html
imported:
- "2019"
_4images_image_id: "13992"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2008-03-21T15:41:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13992 -->
Unter den Bauplatten unten verbirgt sich ein ft-drehkranz. Der hält das Ganze sogar ausnahmsweise.