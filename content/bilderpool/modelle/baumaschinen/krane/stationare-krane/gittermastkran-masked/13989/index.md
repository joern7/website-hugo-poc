---
layout: "image"
title: "Gittermastkran die x-te"
date: "2008-03-21T15:41:44"
picture: "gittermastkranx1.jpg"
weight: "10"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/13989
- /details47ba.html
imported:
- "2019"
_4images_image_id: "13989"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2008-03-21T15:41:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13989 -->
Mal wieder ein Gittermastkran von mir. Eigentlich wollte ich garkeine Bilder Hochladen, aber Steffalk hat mich gezwungen. ;-)
Einen Kranhaken gibt es nicht, das war mir zu viel Arbeit. Eigentlich war der ganze Kran nur ne Übergangsbeschäftigung bis ich was Anderes gefunden hatte. Das habe ich jetzt, also wird er wieder demontiert.