---
layout: "image"
title: "Verstärkung"
date: "2007-10-12T21:26:45"
picture: "gittermastkran9.jpg"
weight: "9"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/12195
- /details1df2.html
imported:
- "2019"
_4images_image_id: "12195"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12195 -->
Zur Verstärkung sind hinter die Statikträger Aluprofile geschraubt. Mit reinem ft würde sich das endlos durchbiegen und überhaupt nicht verwendbar sein.