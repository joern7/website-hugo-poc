---
layout: "image"
title: "Antriebsblock"
date: "2007-10-12T21:26:45"
picture: "gittermastkran8.jpg"
weight: "8"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/12194
- /details76c4.html
imported:
- "2019"
_4images_image_id: "12194"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12194 -->
Für zwei Seiltrommeln, jeweils mit einem 8:1er PowerMotor angetrieben. Die Untersetzung ist bei beiden identisch.