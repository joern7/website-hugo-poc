---
layout: "image"
title: "Antriebsblock"
date: "2007-10-12T21:26:45"
picture: "gittermastkran6.jpg"
weight: "6"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/12192
- /detailsb2f4.html
imported:
- "2019"
_4images_image_id: "12192"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12192 -->
Das eigentlich schöne an diesem Kran ist meiner Meinung nach der Antriebsblock. Der ist mir hier das erste Mal zufriedenstellend gelungen.