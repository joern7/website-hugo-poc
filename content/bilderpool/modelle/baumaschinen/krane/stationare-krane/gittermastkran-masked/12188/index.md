---
layout: "image"
title: "Erster Versuch"
date: "2007-10-12T21:26:45"
picture: "gittermastkran2.jpg"
weight: "2"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/12188
- /detailsfcdd.html
imported:
- "2019"
_4images_image_id: "12188"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12188 -->
So groß sollter der Kran eigentlich werden. Doch er hielt nur, wenn man ihn oben auf dem Regal abgelegt hat. Mit neuer Abspannung (siehe nächstes Foto) war das Problem des Durchbiegens dann geklärt, doch dafür reichte das Gegengewicht (6,5kg) nicht.