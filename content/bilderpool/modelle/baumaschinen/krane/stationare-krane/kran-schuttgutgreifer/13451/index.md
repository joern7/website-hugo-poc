---
layout: "image"
title: "Kran03"
date: "2008-01-27T17:30:27"
picture: "kranmitschuettgutgreifer3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13451
- /details3c31.html
imported:
- "2019"
_4images_image_id: "13451"
_4images_cat_id: "1225"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13451 -->
Detail Auslegerverstellung