---
layout: "image"
title: "Kran01"
date: "2008-01-27T17:30:27"
picture: "kranmitschuettgutgreifer1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13449
- /detailse19c.html
imported:
- "2019"
_4images_image_id: "13449"
_4images_cat_id: "1225"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13449 -->
Kran von hinten