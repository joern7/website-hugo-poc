---
layout: "image"
title: "Gesamtansicht Seilkran"
date: "2006-03-12T18:38:51"
picture: "Gesamtansicht.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/5882
- /details3cd3.html
imported:
- "2019"
_4images_image_id: "5882"
_4images_cat_id: "508"
_4images_user_id: "373"
_4images_image_date: "2006-03-12T18:38:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5882 -->
