---
layout: "image"
title: "Spielen in Dreieich"
date: "2016-10-01T22:12:29"
picture: "IMG_0500.jpg"
weight: "1"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44484
- /detailsecbe.html
imported:
- "2019"
_4images_image_id: "44484"
_4images_cat_id: "3290"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44484 -->
AUf der Ausstellung durften Kinder unter Anleitung meines Sohnes Jan (links) damit spielen