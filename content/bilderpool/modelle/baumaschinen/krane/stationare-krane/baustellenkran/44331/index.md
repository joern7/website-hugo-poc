---
layout: "image"
title: "Führerhaus und Endtaster"
date: "2016-09-03T11:07:00"
picture: "baustellenkran3.jpg"
weight: "3"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44331
- /detailsd9fb-2.html
imported:
- "2019"
_4images_image_id: "44331"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44331 -->
Das Führerhaus. 
Links unterhalb davon sieht man einen der beiden Endlagentaster und die dazugehörige Diode (siehe FT-Pedia 2011-3 / Motorsteuerungen Teil 3)