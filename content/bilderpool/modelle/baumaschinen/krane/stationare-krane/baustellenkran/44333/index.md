---
layout: "image"
title: "Ausleger und Drehkranz"
date: "2016-09-03T11:07:00"
picture: "baustellenkran5.jpg"
weight: "5"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44333
- /details1d3d.html
imported:
- "2019"
_4images_image_id: "44333"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44333 -->
Auf dem Drehkranz sind zwei Aluprofile montiert, darauf dann nochmal zwei Platten 120x60x7,5. Auf denen sitzen dann die Seilwinden + die entsprechenden Motoren, hinten im Bild zu erkennen. Vorne sieht man vier Gelenksteine, auf die der Ausleger gebaut ist, durch die vier Gelenke geht noch ein Metallachse damit sich die Gelenke nicht zerlegen können (zum Bsp. wenn man denn Kranausleger während dem Bau aus Versehen loslässt...  :-( )
Die Kabel  gehen einfach durch die Mitte des Drehkranzes nach unten zur Steuerung.