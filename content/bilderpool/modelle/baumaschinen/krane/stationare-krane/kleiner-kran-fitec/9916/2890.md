---
layout: "comment"
hidden: true
title: "2890"
date: "2007-04-03T18:20:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich vermute dass beim Verfahren der Laufkatze sich der Hanken senkt bzw. hebt? Um das abzustellen, müsste das Seil um eine weitere Umlenkrolle geführt und am vorderen Ende des Kranarms befestigt werden. Dann könnte man die Laufkatze und die Hakenhöhe unabhängig voneinander steuern.

Gruß,
Stefan