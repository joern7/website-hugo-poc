---
layout: "image"
title: "Gesamtansicht"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran16.jpg"
weight: "16"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9920
- /details23bd.html
imported:
- "2019"
_4images_image_id: "9920"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9920 -->
Hier die Gesamtansicht meines Krans.