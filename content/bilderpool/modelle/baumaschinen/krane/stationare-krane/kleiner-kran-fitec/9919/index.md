---
layout: "image"
title: "Kran"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran15.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9919
- /detailsda78.html
imported:
- "2019"
_4images_image_id: "9919"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9919 -->
Das ist jetzt wieder mein Kran. Ich habe Gelenke an die Ausleger gemacht. So kann ich meinen Kran zusammenklappen und einfach wo mitnehmen z.B.: zu meinem Freund. Ich werde wohl noch einen Anhänger für den Kran bauen.