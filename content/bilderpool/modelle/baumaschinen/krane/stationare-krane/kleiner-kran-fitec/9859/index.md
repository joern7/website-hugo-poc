---
layout: "image"
title: "Kleiner Kran"
date: "2007-03-31T16:08:28"
picture: "Kleiner_Kran1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9859
- /details0a5b.html
imported:
- "2019"
_4images_image_id: "9859"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-03-31T16:08:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9859 -->
Das ist mein erster Kran. Er ist nicht sehr groß geworden, weil es mir sehr an Statik mangelt. Im Mai wird sich das ändern dann wird Super Cranes angeschafft.
Außerdem habe ich noch andere Statik im Gewächshaus, Kettenfahrwerk und in einem Allradauto.
Höhe: 44cm
Länge: 60cm