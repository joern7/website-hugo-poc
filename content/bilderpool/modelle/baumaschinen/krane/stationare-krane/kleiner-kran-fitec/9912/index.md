---
layout: "image"
title: "Kräne"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec/timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9912
- /details9762.html
imported:
- "2019"
_4images_image_id: "9912"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9912 -->
Hier ist mein Kran (links) und der Kran meines Freundes (rechts).