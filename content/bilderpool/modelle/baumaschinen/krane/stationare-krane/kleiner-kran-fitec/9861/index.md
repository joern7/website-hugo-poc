---
layout: "image"
title: "Kleiner Kran"
date: "2007-03-31T16:08:28"
picture: "Kleiner_Kran3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9861
- /details0e53.html
imported:
- "2019"
_4images_image_id: "9861"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-03-31T16:08:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9861 -->
