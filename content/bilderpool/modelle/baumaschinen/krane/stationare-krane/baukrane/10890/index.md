---
layout: "image"
title: "Kran Frontansicht"
date: "2007-06-18T21:56:52"
picture: "IMG_0262.jpg"
weight: "2"
konstrukteure: 
- "markus397"
fotografen:
- "markus397"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus397"
license: "unknown"
legacy_id:
- /php/details/10890
- /detailscfad-2.html
imported:
- "2019"
_4images_image_id: "10890"
_4images_cat_id: "986"
_4images_user_id: "51"
_4images_image_date: "2007-06-18T21:56:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10890 -->
