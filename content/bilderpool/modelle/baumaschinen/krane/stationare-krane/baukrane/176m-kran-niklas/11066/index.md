---
layout: "image"
title: "Kran 1,76m"
date: "2007-07-15T17:48:59"
picture: "kran4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11066
- /details3b50-3.html
imported:
- "2019"
_4images_image_id: "11066"
_4images_cat_id: "1002"
_4images_user_id: "557"
_4images_image_date: "2007-07-15T17:48:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11066 -->
und hinten ;)