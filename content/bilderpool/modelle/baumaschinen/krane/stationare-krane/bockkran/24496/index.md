---
layout: "image"
title: "Bockkran"
date: "2009-07-03T09:11:46"
picture: "20020102-123831-Gesamt-k.jpg"
weight: "1"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24496
- /details9920-2.html
imported:
- "2019"
_4images_image_id: "24496"
_4images_cat_id: "1683"
_4images_user_id: "979"
_4images_image_date: "2009-07-03T09:11:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24496 -->
Das ist ein Kran, den ich vor langer Zeit mal gebaut habe.