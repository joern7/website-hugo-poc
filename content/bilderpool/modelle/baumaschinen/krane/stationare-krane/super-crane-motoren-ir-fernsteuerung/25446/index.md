---
layout: "image"
title: "Antriebsmotor vom Super Crane"
date: "2009-10-01T19:18:52"
picture: "dersupercranemitmotorenundderirfernsteuerung4.jpg"
weight: "4"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25446
- /details2eec.html
imported:
- "2019"
_4images_image_id: "25446"
_4images_cat_id: "1781"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25446 -->
