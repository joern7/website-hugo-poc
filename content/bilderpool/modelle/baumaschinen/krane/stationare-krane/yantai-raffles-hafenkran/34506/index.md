---
layout: "image"
title: "Das Original,..."
date: "2012-03-02T14:25:00"
picture: "yantairafflesinkleinerversion13.jpg"
weight: "13"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34506
- /details9532-2.html
imported:
- "2019"
_4images_image_id: "34506"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:25:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34506 -->
... hat ein Hubgewicht von 20.000 Tonnen!
Hier sieht man auch nochmal den kleinen Kran auf der Spitze des Kranes,
der bei meinem kleinen Modell noch keinen Motor für die Seilwinde hat.