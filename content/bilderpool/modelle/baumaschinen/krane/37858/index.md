---
layout: "image"
title: "Detail Kran mit Greifer"
date: "2013-11-29T21:55:24"
picture: "IMG_2906.jpg"
weight: "17"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- /php/details/37858
- /details35fb.html
imported:
- "2019"
_4images_image_id: "37858"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37858 -->
