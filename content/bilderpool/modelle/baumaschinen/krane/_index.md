---
layout: "overview"
title: "Krane"
date: 2020-02-22T08:10:18+01:00
legacy_id:
- /php/categories/609
- /categories6034.html
- /categories1932.html
- /categories5074.html
- /categoriesd0f2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=609 --> 
Große Krane siehe unter "Scale-Modelle": http://www.ftcommunity.de/categories.php?cat_id=128

Anmerkung: Umgangssprachlich werden die Baumaschinen als "Kräne" bezeichnet. Nach einer [Diskussion im Forum](https://forum.ftcommunity.de/viewtopic.php?f=23&t=5277&p=38613&hilit=krane#p38578) haben wir uns jetzt entschlossen, hier den fachsprachlichen Ausdruck zu verwenden.
