---
layout: "image"
title: "Drehkranz klein Detailansicht von unten"
date: "2006-11-14T22:58:19"
picture: "Drehkranz07.jpg"
weight: "36"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7468
- /details7e1c.html
imported:
- "2019"
_4images_image_id: "7468"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7468 -->
Hier jetzt noch die Detailansicht von der Unterseite. In den Rollenböcken sitzen Räder 14. Die WS30 haben keine mechanische Verbindung u den Rollenböcken, aber durch die stramm anliegende Kette stützen sie sich dagegen ab.
Die Kette ist übrigens nur mit drei Förderkettengliedern an drei der 6 BS15 befestigt, weil ich sonst mit dem Umfang nicht hinkam. Hätte ich 6 Förderkettenglieder benutzt, hätte die Kette nicht stramm angelegen.

Vielleicht werde ich die Jumbo-Räder auf der Oberseite noch durch Räder 14 ersetzen, dann brauche ich die Scheibe nicht aufbohren und die Sache könnte noch etwas stabiler sein, weil die Gummiräder vom Jumbo immer etwas nachgeben.