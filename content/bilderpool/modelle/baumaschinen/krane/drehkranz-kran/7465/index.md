---
layout: "image"
title: "Drehkranz klein Innenleben"
date: "2006-11-14T22:58:19"
picture: "Drehkranz04.jpg"
weight: "33"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7465
- /details00a3.html
imported:
- "2019"
_4images_image_id: "7465"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7465 -->
Hier seht ihr das Innenleben des Drehkranzes.
Es besteht aus einer Rohrhülse 30 x 45 mit einem flachen Deckel mit Loch und einer Rohr-Adapterplatte (38848). Auf die Rohrhülse aufgeschoben ist eine Papphülse (das zweite und damit letzte Fremdteil), weil ich 32mm Durchmesser brauchte.
Die Metallachse ist eine 80er. Auf der Grundplatte 180x90 sind Bauplatten aufgeschoben, um die Grundplatte eben zu bekommen. Den gleichen Zweck erfüllt übrigens die gelbe Scheibe aus Platinenmaterial, weil ich es beim besten Willen nicht geschafft habe, die ft-Drehscheibe nur mit ft-Teilen eben genug für eine Lauffläche zu bekommen.