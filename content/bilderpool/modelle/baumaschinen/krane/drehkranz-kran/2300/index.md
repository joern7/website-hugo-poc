---
layout: "image"
title: "Drehkranz klein"
date: "2004-03-07T15:21:25"
picture: "drehkranz_klein.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/2300
- /details6ae0-2.html
imported:
- "2019"
_4images_image_id: "2300"
_4images_cat_id: "214"
_4images_user_id: "14"
_4images_image_date: "2004-03-07T15:21:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2300 -->
nach dem grossen vorbild, benötigt rohr mit 40 mm durchmesser alu platte 105 mm und alu platte durchmesser 85 mm