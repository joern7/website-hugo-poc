---
layout: "image"
title: "Ansicht"
date: "2006-05-11T15:01:30"
picture: "DSCN0733.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6256
- /detailsb43e.html
imported:
- "2019"
_4images_image_id: "6256"
_4images_cat_id: "214"
_4images_user_id: "184"
_4images_image_date: "2006-05-11T15:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6256 -->
Mittels der Bauplatte (15x45 mit Zapfen beidseitig) wird der 4-eckige Rahmen auf den oberen Ring montiert. Das was auf dem Foto unten ist, ist beim Modell oben.