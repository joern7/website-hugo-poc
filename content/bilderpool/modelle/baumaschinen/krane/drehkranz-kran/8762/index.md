---
layout: "image"
title: "Seitenansicht"
date: "2007-01-30T19:24:52"
picture: "drehkranzmartin2.jpg"
weight: "38"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8762
- /detailse732.html
imported:
- "2019"
_4images_image_id: "8762"
_4images_cat_id: "214"
_4images_user_id: "373"
_4images_image_date: "2007-01-30T19:24:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8762 -->
Die Ansicht von der Seite. Von Unten nach Oben: Grundplatte 90, Plexiglas, Drehkranz, Plexiglas und wieder eine Grundplatte 90. In der Mitte eine Stück 4mm Gewindestange.