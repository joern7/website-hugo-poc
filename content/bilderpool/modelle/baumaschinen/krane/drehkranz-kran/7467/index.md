---
layout: "image"
title: "Drehkranz klein Detailansicht von oben"
date: "2006-11-14T22:58:19"
picture: "Drehkranz06.jpg"
weight: "35"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7467
- /detailsdece-2.html
imported:
- "2019"
_4images_image_id: "7467"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7467 -->
Zu guter letzt noch das Herzstück, hier die Oberseite.
Der Drehkranz besteht im Wesentlichen aus einem Ring aus 12 WS30 und 12 BS15. Daran sitzen abwechselnd 6 Rollenböcke (innen) und 6 Radachsen (oben). Die schwarzen Rädchen sind die Räder vom alten Jumbo-Jet (Seilrolle 12 schwarz mit Gummiring drumherum). Auf den Radachsen sitzen außerdem Riegelscheiben, um den Laufkreisdurchmesser zu verkleinern.
Einziges Manko sind die Radachsen, weil sie die Rädchen  nicht wirklich gut festhalten. Aber da im Drehkranz (hoffentlich) keine seitlichen Bewegungen auftreten, sollte das so genügen.