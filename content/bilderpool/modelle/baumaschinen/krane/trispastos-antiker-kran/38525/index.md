---
layout: "image"
title: "Trispastos - Flaschenzug (oben)"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38525
- /details8a8c.html
imported:
- "2019"
_4images_image_id: "38525"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38525 -->
Detailansicht des oberen Teils des Flaschenzugs.