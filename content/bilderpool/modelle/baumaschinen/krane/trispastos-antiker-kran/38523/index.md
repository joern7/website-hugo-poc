---
layout: "image"
title: "Trispastos - Detailansicht Haspel, Sperre, Hebel"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38523
- /details4eea.html
imported:
- "2019"
_4images_image_id: "38523"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38523 -->
Hier sieht man die Haspel, eine einfache Sperre und das Drehrad mit dem großem Hebel.