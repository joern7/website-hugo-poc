---
layout: "overview"
title: "Trispastos (antiker Kran)"
date: 2020-02-22T08:11:49+01:00
legacy_id:
- /php/categories/2875
- /categories9299.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2875 --> 
Griechisch-römischer Kran mit 3-Rollen-Flaschenzug, der etwa 400-300 v.Chr. entwickelt und von dem römischen Ingenieur Vitruv ca. 100 v.Chr. dokumentiert wurde.