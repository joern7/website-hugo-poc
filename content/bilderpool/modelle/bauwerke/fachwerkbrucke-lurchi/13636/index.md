---
layout: "image"
title: "Draufsicht des Dachtragwerks"
date: "2008-02-10T22:34:51"
picture: "IMG_0613.jpg"
weight: "10"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/13636
- /details9f04-2.html
imported:
- "2019"
_4images_image_id: "13636"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-10T22:34:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13636 -->
