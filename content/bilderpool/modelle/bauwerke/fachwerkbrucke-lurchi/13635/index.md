---
layout: "image"
title: "Ansicht des ersten Brückenbogens"
date: "2008-02-10T22:34:51"
picture: "IMG_0620.jpg"
weight: "9"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/13635
- /detailsb4cf.html
imported:
- "2019"
_4images_image_id: "13635"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-10T22:34:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13635 -->
