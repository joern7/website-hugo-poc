---
layout: "comment"
hidden: true
title: "9162"
date: "2009-05-06T07:45:12"
uploadBy:
- "sulu007"
license: "unknown"
imported:
- "2019"
---
Hallo,
sehr schönes Modell, sehr detailreich und sogar mit Strommasten.
Pass blos auf wenn du an den Schrank kommst, der steht jetzt unter Strom :-).
Schön wäre es noch wenn du immer eine Beschreibung, sowie die Felder Fotograf und Konstrukteur füllst.
Um das bequem hinzubekommen kannst du dir hier unter Publisher den ftCommunity Publisher herunterladen und mit dessen hilfe Bilder hochladen.