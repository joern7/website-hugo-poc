---
layout: "image"
title: "Gesamtansicht"
date: "2009-05-05T16:44:02"
picture: "strommast10.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/23883
- /detailscb0d.html
imported:
- "2019"
_4images_image_id: "23883"
_4images_cat_id: "1632"
_4images_user_id: "791"
_4images_image_date: "2009-05-05T16:44:02"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23883 -->
