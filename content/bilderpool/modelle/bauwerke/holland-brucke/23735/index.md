---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:05"
picture: "hollandbruecke5.jpg"
weight: "5"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23735
- /detailsbc05.html
imported:
- "2019"
_4images_image_id: "23735"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23735 -->
Gelenk der Brücke.