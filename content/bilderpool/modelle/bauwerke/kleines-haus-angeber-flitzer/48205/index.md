---
layout: "image"
title: "Gesamtansicht"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48205
- /detailsc136.html
imported:
- "2019"
_4images_image_id: "48205"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48205 -->
Ein ähnliches Haus (aber ich meine nur jeweils 1 BS hoch anstatt 2) fand sich in einem der ersten fischertechnik-Prospekte, und das kleine Auto ist sehr nahe an einer Vorlage des Ur-ft-100-Kastens. Das Dach verwendet die guten alten Giebel- und Schornstein-Bauteile, und die Tür- und Fensterelemente stammen ebenfalls aus den frühen fischertechnik-Jahren.