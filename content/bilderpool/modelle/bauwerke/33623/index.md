---
layout: "image"
title: "3. Advent"
date: "2011-12-10T10:56:06"
picture: "Pyramide_02_w.jpg"
weight: "8"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33623
- /details272c.html
imported:
- "2019"
_4images_image_id: "33623"
_4images_cat_id: "656"
_4images_user_id: "1404"
_4images_image_date: "2011-12-10T10:56:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33623 -->
...zum 3. Advent gibt es die Pyramide aus fischertechnik mit Teelichter angetrieben...
...durch die Drehung der Pyramide geht etwas an Schärfe verloren...
solange die Teelichter brennen, läuft die Pyramide, ohne elektrischen Antrieb