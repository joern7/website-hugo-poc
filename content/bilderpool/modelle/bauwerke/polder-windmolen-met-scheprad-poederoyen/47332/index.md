---
layout: "image"
title: "Getrapte bemaling + molengang (=rij molens)"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen24.jpg"
weight: "24"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47332
- /details7d2f.html
imported:
- "2019"
_4images_image_id: "47332"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47332 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/molens-algemeen