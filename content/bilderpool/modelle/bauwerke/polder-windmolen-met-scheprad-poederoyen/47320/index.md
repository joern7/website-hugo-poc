---
layout: "image"
title: "Kruiwerk -detail"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47320
- /detailsed78.html
imported:
- "2019"
_4images_image_id: "47320"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47320 -->
