---
layout: "image"
title: "Onderrondsel -Koningsspil   + Waterwiel (onderwiel)"
date: "2018-03-09T22:19:45"
picture: "polderwindmolen04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47312
- /detailsbe1b.html
imported:
- "2019"
_4images_image_id: "47312"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:19:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47312 -->
