---
layout: "image"
title: "Rechsts:  Onderrondsel  + Waterwiel (-onderwiel) + Scheprad (-geheel links)"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen29.jpg"
weight: "29"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47337
- /detailsa144.html
imported:
- "2019"
_4images_image_id: "47337"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47337 -->
Youtube-link : 

https://www.youtube.com/watch?v=UliZqs6p7HM&t=0s