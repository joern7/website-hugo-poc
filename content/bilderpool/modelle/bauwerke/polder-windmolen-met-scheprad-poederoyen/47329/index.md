---
layout: "image"
title: "Achtkantige Poldermolen constructie-details"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen21.jpg"
weight: "21"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47329
- /details3da1.html
imported:
- "2019"
_4images_image_id: "47329"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47329 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant