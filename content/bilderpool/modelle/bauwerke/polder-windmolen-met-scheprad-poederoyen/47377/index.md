---
layout: "image"
title: "Romp achtkantmolen met benamingen"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen69.jpg"
weight: "69"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47377
- /detailsec7a.html
imported:
- "2019"
_4images_image_id: "47377"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47377 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant