---
layout: "image"
title: "Polder- Windmolen met scheprad    Poederoyen NL"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen35.jpg"
weight: "35"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47343
- /details5441-3.html
imported:
- "2019"
_4images_image_id: "47343"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47343 -->
Schaal: 1:25,
Modelhoogte:  1,30m 
Modelbreedte: 1,15m 

Youtube-link : 

https://www.youtube.com/watch?v=UliZqs6p7HM&t=0s

Instagram account van Peter Paul Klapwijk 
https://www.instagram.com/ppkhm/ 
#lifeatawindmill 

https://www.instagram.com/explore/tags/lifeatawindmill/