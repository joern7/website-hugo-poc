---
layout: "image"
title: "Achtkantige Polder Windmolen met scheprad te Zuilichem"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen83.jpg"
weight: "83"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47391
- /details926c.html
imported:
- "2019"
_4images_image_id: "47391"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47391 -->
