---
layout: "image"
title: "(Ver-) Nieuwbouw Molens"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen81.jpg"
weight: "81"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47389
- /detailsd362.html
imported:
- "2019"
_4images_image_id: "47389"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47389 -->
Interessante Links mite viel Details : 

https://www.verbij.nl/molens/ 
https://www.verbij.nl/molens/3d-ontwerp