---
layout: "image"
title: "Literatuur tbv details"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen68.jpg"
weight: "68"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47376
- /details3266-3.html
imported:
- "2019"
_4images_image_id: "47376"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47376 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/molens-algemeen

http://www.molenbiotoop.nl/content.php?page=2.2.1