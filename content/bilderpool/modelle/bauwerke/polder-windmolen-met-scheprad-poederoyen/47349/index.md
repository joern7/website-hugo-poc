---
layout: "image"
title: "Wieken -verloop goed zichtbaar"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen41.jpg"
weight: "41"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47349
- /details8b11.html
imported:
- "2019"
_4images_image_id: "47349"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47349 -->
Schaal: 1:25,
Modelhoogte:  1,30m 
Modelbreedte: 1,15m