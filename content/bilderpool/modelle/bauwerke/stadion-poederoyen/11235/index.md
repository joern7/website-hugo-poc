---
layout: "image"
title: "FT-Diafragma-Stadion Poederoyen"
date: "2007-08-03T16:12:02"
picture: "FT-Diafragma-dak-constructie_036.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11235
- /details045c-2.html
imported:
- "2019"
_4images_image_id: "11235"
_4images_cat_id: "1029"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11235 -->
