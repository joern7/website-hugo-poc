---
layout: "image"
title: "haussiemens02.jpg"
date: "2010-08-22T13:21:08"
picture: "haussiemens02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27875
- /detailsd269.html
imported:
- "2019"
_4images_image_id: "27875"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27875 -->
LED's werden anzeigen ob Boiler heiss oder kalt ist.