---
layout: "image"
title: "haussiemens09.jpg"
date: "2010-08-22T13:21:08"
picture: "haussiemens09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27882
- /detailsf135.html
imported:
- "2019"
_4images_image_id: "27882"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27882 -->
Lichtsensor auf dem Garagendach fuer die Aussenbeleuchtung