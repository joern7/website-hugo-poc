---
layout: "image"
title: "haussiemens04.jpg"
date: "2010-08-22T13:21:08"
picture: "haussiemens04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27877
- /details47dd.html
imported:
- "2019"
_4images_image_id: "27877"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27877 -->
Motor und Getriebe fuer das Tor