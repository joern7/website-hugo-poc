---
layout: "image"
title: "Windmühle"
date: "2010-04-16T23:26:05"
picture: "windmuehle1.jpg"
weight: "1"
konstrukteure: 
- "mike"
fotografen:
- "mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26939
- /details77cd.html
imported:
- "2019"
_4images_image_id: "26939"
_4images_cat_id: "1933"
_4images_user_id: "1051"
_4images_image_date: "2010-04-16T23:26:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26939 -->
Vorlage war ein Bild des Gallerieholländers Sprengel unweit von Schneverdingen mit Windrose, die ich hier nicht einbaute ebenso die Drehmechanik weil ich es so nicht erkennen konnte. Ich werde mich aber noch schlauer machen.