---
layout: "image"
title: "Windmühle"
date: "2010-04-16T23:26:06"
picture: "windmuehle3.jpg"
weight: "3"
konstrukteure: 
- "mike"
fotografen:
- "mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26941
- /detailsc527.html
imported:
- "2019"
_4images_image_id: "26941"
_4images_cat_id: "1933"
_4images_user_id: "1051"
_4images_image_date: "2010-04-16T23:26:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26941 -->
Kuppel - noch in einer anderen Variante