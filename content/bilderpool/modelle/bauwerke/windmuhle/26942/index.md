---
layout: "image"
title: "Windmühle"
date: "2010-04-16T23:26:06"
picture: "windmuehle4.jpg"
weight: "4"
konstrukteure: 
- "mike"
fotografen:
- "mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26942
- /detailsc270.html
imported:
- "2019"
_4images_image_id: "26942"
_4images_cat_id: "1933"
_4images_user_id: "1051"
_4images_image_date: "2010-04-16T23:26:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26942 -->
Kuppel - nicht laminiert verkleidet.