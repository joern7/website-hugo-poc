---
layout: "image"
title: "hausemodelfuersiemens03.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens03.jpg"
weight: "3"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27836
- /details5a0b.html
imported:
- "2019"
_4images_image_id: "27836"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27836 -->
