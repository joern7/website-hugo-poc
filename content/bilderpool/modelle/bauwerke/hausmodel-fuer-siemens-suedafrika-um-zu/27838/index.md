---
layout: "image"
title: "hausemodelfuersiemens05.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens05.jpg"
weight: "5"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27838
- /detailsbed9.html
imported:
- "2019"
_4images_image_id: "27838"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27838 -->
