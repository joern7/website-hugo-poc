---
layout: "image"
title: "hausemodelfuersiemens15.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens15.jpg"
weight: "15"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27848
- /details6ed1.html
imported:
- "2019"
_4images_image_id: "27848"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27848 -->
