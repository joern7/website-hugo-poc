---
layout: "image"
title: "hausemodelfuersiemens11.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens11.jpg"
weight: "11"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27844
- /detailse6be.html
imported:
- "2019"
_4images_image_id: "27844"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27844 -->
