---
layout: "overview"
title: "Hausmodel fuer Siemens Suedafrika um Hausautomatisierung zu zeigen"
date: 2020-02-22T08:38:06+01:00
legacy_id:
- /php/categories/2015
- /categories0903.html
- /categories5342.html
- /categories5ff9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2015 --> 
Haus wird mit einer Siemens LOGO! angesteuert.

Automatisierung von:
Licht
Alarm
Garagenmotor
Tormotor
Beuler
Solarzellen
Schwimmbeckenbeleuchtung und Pumpe

Alles wird auch steuerbar sein via GPRS Modul