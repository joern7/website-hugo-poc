---
layout: "image"
title: "hausemodelfuersiemens09.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens09.jpg"
weight: "9"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27842
- /detailsef69.html
imported:
- "2019"
_4images_image_id: "27842"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27842 -->
