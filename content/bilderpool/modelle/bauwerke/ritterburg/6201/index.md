---
layout: "image"
title: "Ritterburg3"
date: "2006-05-05T14:30:03"
picture: "Burg3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/6201
- /details68a5.html
imported:
- "2019"
_4images_image_id: "6201"
_4images_cat_id: "535"
_4images_user_id: "381"
_4images_image_date: "2006-05-05T14:30:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6201 -->
