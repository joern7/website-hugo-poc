---
layout: "comment"
hidden: true
title: "19472"
date: "2014-08-30T12:49:19"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Sehr schönes Modell.
Wenn man unter der Schranke steht, lernt man bald, warum der "Schlagbaum" so heißt.
Gruß, Dirk