---
layout: "image"
title: "Ansicht von unten."
date: "2008-09-08T17:45:22"
picture: "bruecke3_2.jpg"
weight: "35"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15212
- /details1e95-2.html
imported:
- "2019"
_4images_image_id: "15212"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-08T17:45:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15212 -->
So siehts aus wenn man drunter steht, bzw. in diesem Fall drunter liegt.
Ich seh gerade dass da noch einige Streben fehlen, sofort ans Werk begeb...