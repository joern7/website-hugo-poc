---
layout: "image"
title: "Der Bogen ...."
date: "2008-05-30T22:39:36"
picture: "bruecke11.jpg"
weight: "32"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14610
- /detailsadd0.html
imported:
- "2019"
_4images_image_id: "14610"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14610 -->
...war leider mit Bogenstücken nicht hinzubekommen. Eine Sorte war zu klein, die andere zu groß.