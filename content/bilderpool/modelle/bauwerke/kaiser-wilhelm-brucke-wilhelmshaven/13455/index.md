---
layout: "image"
title: "Kaiser- Wilhelm- Brücke"
date: "2008-01-29T16:30:45"
picture: "kwbruecke1.jpg"
weight: "1"
konstrukteure: 
- "u. a. Oberbaurat Ernst Troschel, MAN Nürnberg"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13455
- /details263f.html
imported:
- "2019"
_4images_image_id: "13455"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-01-29T16:30:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13455 -->
Hier ein paar Originalbilder von der KW Brücke in Wilhelmshaven. Die möchte ich gerne nachbauen. Es handelt sich hier nicht um eine stinknormale Brücke sondern um eine doppelte Drehbrücke mit Hubvorrichtung. Die KW-Brücke wurde von 1906 bis 1907 als die damals größte Drehbrücke Deutschlands mit einer Spannweite von 150 m und einer Höhe von 9 m erbaut und 1907 in Betrieb genommen.