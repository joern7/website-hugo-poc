---
layout: "image"
title: "Modell der Kaiser-Wilhelm-Brücke"
date: "2008-04-09T21:09:35"
picture: "bruecke05.jpg"
weight: "16"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14208
- /details6b83.html
imported:
- "2019"
_4images_image_id: "14208"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-04-09T21:09:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14208 -->
Innenansicht des unteren Teiles. Muss da jetzt aber noch links und rechts Streben einbauen die den oberen teil verstärken. Habe ich gestern aber erst entdeckt. Manche Sachen entdecke ich immer etwas später auf den Zeichnungen.