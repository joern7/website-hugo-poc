---
layout: "image"
title: "Die Fahrbahn"
date: "2008-09-08T17:45:23"
picture: "bruecke5_2.jpg"
weight: "37"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15214
- /detailsd7c8.html
imported:
- "2019"
_4images_image_id: "15214"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-08T17:45:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15214 -->
Hier hab ich mal probeweise die Fahrbahn aufgelegt. Passt auf den Millimeter genau herein. Die roten Platten sind der Gehsteig für die Fussgänger. Ein Geländer muss eigentlich auch noch dran aber ob ich das alles noch bis zur Convention dieses Jahr schaffe....Vorne im Bild fehlen noch die Auflagen für die Fahrbahn. Kommt noch...wenn ichs schaffe.