---
layout: "image"
title: "Kaiser-Wilhelm-Brücke"
date: "2008-01-29T16:30:45"
picture: "kwbruecke4.jpg"
weight: "4"
konstrukteure: 
- "u. a. Oberbaurat Ernst Troschel, MAN Nürnberg"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13458
- /detailsecd0.html
imported:
- "2019"
_4images_image_id: "13458"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-01-29T16:30:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13458 -->
Hier sieht man die von der Hafenseite aus rechte Anschlagsseite. In einem von den Häuschen hat der Brückenwart sein Büro. Geöffnet und geschlossen wird die Brücke jedoch von einem Steuerstand auf der Brücke.