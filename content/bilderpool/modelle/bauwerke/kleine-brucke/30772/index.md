---
layout: "image"
title: "Vogelperspektive"
date: "2011-06-03T19:21:14"
picture: "bruecke4.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30772
- /detailsee37.html
imported:
- "2019"
_4images_image_id: "30772"
_4images_cat_id: "2296"
_4images_user_id: "1162"
_4images_image_date: "2011-06-03T19:21:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30772 -->
