---
layout: "image"
title: "Turm 15"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm15.jpg"
weight: "15"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27557
- /detailsffbb.html
imported:
- "2019"
_4images_image_id: "27557"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27557 -->
Hier kommt von rechts (vom Turm) die Seilbahn, und man kann in dem Fahrstuhl die letzten zwei Meter hinab fahren.