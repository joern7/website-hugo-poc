---
layout: "image"
title: "Turm 10"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm10.jpg"
weight: "10"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27552
- /details0c0a.html
imported:
- "2019"
_4images_image_id: "27552"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27552 -->
Schräg von unten mit den Abspannungen.