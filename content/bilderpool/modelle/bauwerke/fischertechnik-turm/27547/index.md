---
layout: "image"
title: "Turm 5"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm05.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27547
- /detailsf451.html
imported:
- "2019"
_4images_image_id: "27547"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27547 -->
Noch mal das Solar-Karussell von nah dran. Der schöne blaue Hintergrund ist der Himmel