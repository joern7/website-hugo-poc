---
layout: "image"
title: "Turm 1"
date: "2010-06-23T22:18:56"
picture: "fischertechnikturm01.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27543
- /details44ce.html
imported:
- "2019"
_4images_image_id: "27543"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27543 -->
Hier kann man den ganzen Turm abklappen, sehr praktisch zum Auf und Abbau. Als Verriegelung steckt eine Stange in den roten Bausteinen.