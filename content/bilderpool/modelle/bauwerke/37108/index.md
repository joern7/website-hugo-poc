---
layout: "image"
title: "windkraft2.jpg"
date: "2013-06-14T06:51:30"
picture: "IMG_8471.JPG"
weight: "10"
konstrukteure: 
- "Thorsten Stock"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37108
- /detailsfa0d.html
imported:
- "2019"
_4images_image_id: "37108"
_4images_cat_id: "656"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T06:51:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37108 -->
Sowas passiert, wenn Kinder zu Besuch kommen und in meinem Bastelzimmer allein gelassen werden :-)

Die Antriebsrichtung drehen wir beim nächsten Mal um auf "Wind --> Anlage".
Die Drehkränze von MisterWho sind aus dem 3D-Drucker (http://forum.ftcommunity.de/viewtopic.php?f=4&t=1560&p=10396&hilit=Drucker#p10396 )