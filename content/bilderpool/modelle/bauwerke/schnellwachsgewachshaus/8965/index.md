---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-02-11T15:18:39"
picture: "Schnellwachsgewchshaus34.jpg"
weight: "34"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8965
- /details69b2.html
imported:
- "2019"
_4images_image_id: "8965"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8965 -->
Der Ventilator ist über, nicht unter der Lüftungsklappe, weil im Gewächshaus die Luftfeuchtigkeit zu groß ist. Ich glaub der Tipp kommt von schnaggels.