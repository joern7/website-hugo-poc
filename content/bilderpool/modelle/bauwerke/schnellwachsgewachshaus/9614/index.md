---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-03-19T20:27:22"
picture: "Schnellwachsgewchshaus45.jpg"
weight: "45"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9614
- /detailsb136.html
imported:
- "2019"
_4images_image_id: "9614"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-03-19T20:27:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9614 -->
Hier sind die Töpfchen mit Watte und mit Samen gefüllt.