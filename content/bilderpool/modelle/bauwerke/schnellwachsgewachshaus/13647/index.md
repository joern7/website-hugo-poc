---
layout: "image"
title: "Ventilsteuerung"
date: "2008-02-14T14:28:25"
picture: "Schnellwachsgewchshaus76.jpg"
weight: "69"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13647
- /details1ea3.html
imported:
- "2019"
_4images_image_id: "13647"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-14T14:28:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13647 -->
Das ist meine neue Ventilsteuerung. Sie hat 4 Ventile, je mit einem Mini-Motor. Zur Positionierung habe ich Potis verbaut. Jedes Poti ist an einen taster angeschlossen(noch nicht im Bild) und der taster arbeitet als Öffner. Diese sind dann alle an AX angeschlossen. In Grundposition ist der Wert also 1023. Wenn ich dann ein Ventil öffne schließt der Taster und das jeweilige Poti gibt einen Wert. Man kann halt immer nur ein Ventil auf einmal steuern, aber das reicht ja.