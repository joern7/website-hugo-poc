---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-03-19T20:27:22"
picture: "Schnellwachsgewchshaus47.jpg"
weight: "47"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9616
- /details6690-2.html
imported:
- "2019"
_4images_image_id: "9616"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-03-19T20:27:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9616 -->
Hier habe ich einen Zettel geschrieben wo welche Sensoren/Aktoren angeschlossen werden müssen. Das ist sehr hilfreich, wenn man so viele Sensoren/Aktoren hat.