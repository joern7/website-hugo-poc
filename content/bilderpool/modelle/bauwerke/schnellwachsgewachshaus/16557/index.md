---
layout: "image"
title: "Steuerung"
date: "2008-12-06T12:02:40"
picture: "Gewchshaus9.5.jpg"
weight: "88"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/16557
- /details9866.html
imported:
- "2019"
_4images_image_id: "16557"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-12-06T12:02:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16557 -->
Hier die Platine. Die Lochrasterplatine hat den großen Vorteil, dass man sie immer beliebig erweitern kann.
-ATMega32, 16Mhz
-6 Motorausgänge (3x L293D)
-10pol. ISP-Buchse
-I2C Anschluss für 2x LM75
-restliche Ports für servos und digital/analog Eingänge.
Es muss hier heftig getrickst werden, damit die Ports reichen (Schieberegister 74HC595 usw.)