---
layout: "image"
title: "Seite"
date: "2008-02-20T19:54:17"
picture: "Schnellwachsgewchshaus80.jpg"
weight: "73"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13712
- /details6378.html
imported:
- "2019"
_4images_image_id: "13712"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-20T19:54:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13712 -->
Die Magnetventile sind noch nicht "verschlaucht", aber sie werden den Zylinder für die Lüftungsklappe steuern. Die Kabel, die rausgucken sind, das Flachbandkabe vom Extension und ein 26pol. Flachbandkabel für das IF.