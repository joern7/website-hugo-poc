---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T16:58:32"
picture: "Schnellwachsgewchshaus1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8624
- /detailsd850.html
imported:
- "2019"
_4images_image_id: "8624"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T16:58:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8624 -->
Das ist ein Gewächshaus, das gegossen, beheizt, beleuchtet und klimatisiert werden soll. Es ist noch nicht fertig, weil RoboPro bei mir gerade eine Macke hat, aber der RoboproEntwickler hilft mir bei dem Problem. Außerdem hab ich noch keinen NTC für die Temperaturmessung.