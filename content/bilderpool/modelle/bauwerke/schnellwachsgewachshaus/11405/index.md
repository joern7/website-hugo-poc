---
layout: "image"
title: "Spitzpaprika"
date: "2007-08-25T12:44:10"
picture: "Schnellwachsgewchshaus72.jpg"
weight: "67"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11405
- /detailsc617.html
imported:
- "2019"
_4images_image_id: "11405"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-08-25T12:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11405 -->
Das ist eine Spitzpaprika.