---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-31T16:49:40"
picture: "Schnellwachsgewchshaus30.jpg"
weight: "30"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8767
- /details624d-2.html
imported:
- "2019"
_4images_image_id: "8767"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-31T16:49:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8767 -->
Hier die Beleuchtung. Die Lampen sind mit doppelseitigem Klebeband festgeklebt.