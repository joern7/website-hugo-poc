---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-02-19T16:15:55"
picture: "Schnellwachsgewchshaus40.jpg"
weight: "40"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9062
- /detailsf3cb.html
imported:
- "2019"
_4images_image_id: "9062"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-02-19T16:15:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9062 -->
Hier habe ich die Ventilsteuerung etwas umgebaut. Ich habe überall einen Verbinder 30 angebaut. An jeden dieser Verbinder 30 kommt ein Taster (siehe nächste Bilder). Dies ist dringend nötig um die Mittelstellung feststellen zu können, weil die reine Zeitsteuerung zu ungenau ist.