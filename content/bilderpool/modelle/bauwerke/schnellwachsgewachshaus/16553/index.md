---
layout: "image"
title: "Neu 2009!!!"
date: "2008-12-06T12:02:39"
picture: "Gewchshaus9.1.jpg"
weight: "84"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/16553
- /details799f.html
imported:
- "2019"
_4images_image_id: "16553"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-12-06T12:02:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16553 -->
So, hier nun Update 2009.
Das Gewächshaus soll 1.Februar 2009 In Betrieb genommen werden, ich hoffe ich schaff es bis dahin.
Neueheiten:
-statt IF eine slbstgebaute Platine mit ATMega32
-2*16 Display (Matrixtastatur geplant)
-Temperaturmessung mit LM75, im gewächshaus und außen
-Lüften über sehr leisen PC-Lüfter
-Öffnen der Lüfterklappen mit Servos
-Ventilsteuerung ohne Taster, nur mit Potis

Sonst bleibt eigentlich großteils alles so wie sonst.