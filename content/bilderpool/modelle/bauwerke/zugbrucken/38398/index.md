---
layout: "image"
title: "automatische Zugbrücke, Blick auf die Steuerelektronik"
date: "2014-03-01T15:55:18"
picture: "2048.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Relais", "Fototransistor", "Atmel", "ATTiny26", "Schwellwertschalter", "Leistungsstufe", "PWM", "MOSFET", "Puls", "Spitzenstrom"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38398
- /details6df1.html
imported:
- "2019"
_4images_image_id: "38398"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38398 -->
Die Steuerelektronik besteht aus einer selbstgefertigten Platine mit einem Atmel ATTiny26 Microcontroller (Fernbedienbar über Infrarotempfänger, mit pwm-gesteuertem Leistungs-MOSFET zur Motoransteuerung und einem MOSFET zur Relaisumschaltung für die Fahrtrichtung des Motors), einem Nachbau des Fischertechnik Elektronik-Schwellwertschalters (Eingänge für die angeschlossenen Infrarot-Fototransistoren der beiden Lichtschranken) und der FT-Leistungsstufe (Ausgang für zwei Infrarot-LEDs, die mit 50µs-Pulsen auf 1A Spitzenstrom gebracht werden, und einem Fischertechnik Relais-Baustein II (Umschaltung der Fahrtrichtung des Zugmotors).