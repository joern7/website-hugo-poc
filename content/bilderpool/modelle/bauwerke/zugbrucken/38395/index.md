---
layout: "image"
title: "automatische Zugbrücke in Autofahrtrichtung"
date: "2014-03-01T15:55:18"
picture: "2038.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38395
- /details7d32.html
imported:
- "2019"
_4images_image_id: "38395"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38395 -->
Blick entlang der Fahrbahnrichtung. Rechts ist eine der Autoverkehrsampeln zu sehen (Selbstbau mit LEDs auf Platine).