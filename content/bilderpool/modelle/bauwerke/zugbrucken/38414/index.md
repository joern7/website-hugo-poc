---
layout: "image"
title: "Seilzugmechanismus"
date: "2014-03-02T18:43:51"
picture: "2059.jpg"
weight: "14"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38414
- /detailsb306.html
imported:
- "2019"
_4images_image_id: "38414"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38414 -->
