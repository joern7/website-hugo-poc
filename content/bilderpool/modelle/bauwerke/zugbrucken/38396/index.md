---
layout: "image"
title: "automatische Zugbrücke - Strassenebene mit Auto und Ampel"
date: "2014-03-01T15:55:18"
picture: "2043.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38396
- /detailsb33a.html
imported:
- "2019"
_4images_image_id: "38396"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38396 -->
Die Zugbrücke ist abgesenkt, die Autoverkehrsampel auf grün.