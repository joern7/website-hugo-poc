---
layout: "image"
title: "Eiffelturm im Anfang"
date: "2010-09-25T20:38:37"
picture: "Vastgelegd_2008-3-9_00000A.jpg"
weight: "6"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/28232
- /detailsefdd.html
imported:
- "2019"
_4images_image_id: "28232"
_4images_cat_id: "656"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T20:38:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28232 -->
Fuß von Eiffelturm