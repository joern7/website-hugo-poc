---
layout: "image"
title: "Laura's House"
date: "2008-06-11T06:55:32"
picture: "L_House_b.jpg"
weight: "2"
konstrukteure: 
- "Laura"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Laura", "House"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14672
- /detailsd7ca.html
imported:
- "2019"
_4images_image_id: "14672"
_4images_cat_id: "656"
_4images_user_id: "585"
_4images_image_date: "2008-06-11T06:55:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14672 -->
This is another view of a house that Laura constructed during a meeting.