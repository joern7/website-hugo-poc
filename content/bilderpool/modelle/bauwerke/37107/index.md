---
layout: "image"
title: "windkraft1.jpg"
date: "2013-06-14T06:47:35"
picture: "IMG_8470.JPG"
weight: "9"
konstrukteure: 
- "Thorsten Stock"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37107
- /detailsd615.html
imported:
- "2019"
_4images_image_id: "37107"
_4images_cat_id: "656"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T06:47:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37107 -->
Was passiert, wenn Kinder zu Besuch kommen und in meinem Bastelzimmer allein gelassen werden? 
Lange Zeit hört man keinen Mucks, und irgendwann kommt eine Windkraftanlage heraus :-)