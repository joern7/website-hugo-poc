---
layout: "image"
title: "Bergstation"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman06.jpg"
weight: "6"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31293
- /detailsf519.html
imported:
- "2019"
_4images_image_id: "31293"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31293 -->
