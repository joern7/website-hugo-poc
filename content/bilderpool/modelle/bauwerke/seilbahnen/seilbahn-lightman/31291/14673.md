---
layout: "comment"
hidden: true
title: "14673"
date: "2011-07-17T23:39:30"
uploadBy:
- "lightman"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

die Idee hört sich nicht schlecht an! In echt ist oben noch eine Rolle, welche in der Station von einer kleinen Schiene angehoben bzw. heruntergedrückt wird und so die Klemme löst oder klemmt. Mein größtes Problem war aber einfach die Größe und der Platz. Ich hab keine Idee, wie ich sowas auf diesem kleinen Raum realisieren soll.

Viele Grüße, Ingo