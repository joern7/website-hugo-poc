---
layout: "image"
title: "Bergstation Einfahrt Kollisionsvermeidung"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman12.jpg"
weight: "12"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31299
- /details528d-2.html
imported:
- "2019"
_4images_image_id: "31299"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31299 -->
Mittels der Taster verhindert die Software, dass während der Einfahrt eines Sessels ein Mitnehmer auf der Kette im Weg ist und den Sessel womöglich rauswirft. An echten Bahnen sind die Mitnehmer gegen solche Fälle nach vorne wegklappbar.