---
layout: "image"
title: "Bergstation Bügelschließer"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman17.jpg"
weight: "17"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31304
- /detailsd830-2.html
imported:
- "2019"
_4images_image_id: "31304"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31304 -->
