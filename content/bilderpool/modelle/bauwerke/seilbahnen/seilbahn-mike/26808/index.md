---
layout: "image"
title: "Seilbahn 2"
date: "2010-03-23T21:06:12"
picture: "seilbahn2.jpg"
weight: "2"
konstrukteure: 
- "Michael Biehl und Fabian"
fotografen:
- "Michael Biehl und Fabian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26808
- /details88a4.html
imported:
- "2019"
_4images_image_id: "26808"
_4images_cat_id: "1915"
_4images_user_id: "1051"
_4images_image_date: "2010-03-23T21:06:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26808 -->
Talstation mit BSB Diesellok