---
layout: "image"
title: "Seilbahn 5"
date: "2010-03-23T21:06:12"
picture: "seilbahn5.jpg"
weight: "5"
konstrukteure: 
- "Michael Biehl und Fabian"
fotografen:
- "Michael Biehl und Fabian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26811
- /details78e6.html
imported:
- "2019"
_4images_image_id: "26811"
_4images_cat_id: "1915"
_4images_user_id: "1051"
_4images_image_date: "2010-03-23T21:06:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26811 -->
Berstation