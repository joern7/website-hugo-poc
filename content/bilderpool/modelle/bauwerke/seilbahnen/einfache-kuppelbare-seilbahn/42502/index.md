---
layout: "image"
title: "Detail schräg oben"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn08.jpg"
weight: "8"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42502
- /details08fb.html
imported:
- "2019"
_4images_image_id: "42502"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42502 -->
Die Führungen des Trageseils kurz vor dem Aufliegen war wichtig, weil damit die Justage präziser wird.