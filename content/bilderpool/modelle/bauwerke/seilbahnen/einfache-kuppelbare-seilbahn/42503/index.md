---
layout: "image"
title: "Versteifungen"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn09.jpg"
weight: "9"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42503
- /detailscdc5.html
imported:
- "2019"
_4images_image_id: "42503"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42503 -->
Die Station wird mit einem dicken Buch gehalten.