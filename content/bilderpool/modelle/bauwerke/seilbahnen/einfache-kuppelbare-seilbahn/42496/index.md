---
layout: "image"
title: "Aufsicht"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn02.jpg"
weight: "2"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42496
- /detailsf110.html
imported:
- "2019"
_4images_image_id: "42496"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42496 -->
Die Ausirchtung der Kette zum Seil ist wichtig, nur mit dem richtigen Abstand wird die Gondel sauber ab- und aufgelegt.