---
layout: "image"
title: "Detail von oben"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn07.jpg"
weight: "7"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42501
- /detailse397.html
imported:
- "2019"
_4images_image_id: "42501"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42501 -->
Gerade beim Ablegen auf das Trageseil muss es millimetergenau passen, weil sonst der 7.5mm Baustein der Gondel nicht auf dem Seil zu liegen kommt.