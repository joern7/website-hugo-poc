---
layout: "image"
title: "Detail von der Seite"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn06.jpg"
weight: "6"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42500
- /details713d.html
imported:
- "2019"
_4images_image_id: "42500"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42500 -->
Gut zu sehen ist die verkippte Konstruktion von Raupenkette und Trageseil