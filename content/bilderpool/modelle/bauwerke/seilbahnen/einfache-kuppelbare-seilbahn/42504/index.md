---
layout: "image"
title: "Optimierte Steigung"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn10.jpg"
weight: "10"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42504
- /detailsbc15.html
imported:
- "2019"
_4images_image_id: "42504"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42504 -->
Nur mit den richtigen Winkeln funktioniert das aufnehmen und ablegen.