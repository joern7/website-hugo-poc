---
layout: "image"
title: "Stütze mit 4 Rollen"
date: "2011-09-27T23:24:31"
picture: "Sessellift_Sttze_4_Rollen_06.jpg"
weight: "19"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/32959
- /details6bf0.html
imported:
- "2019"
_4images_image_id: "32959"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32959 -->
Der Taster (eigentlich sind es je 4 Rollen 3 Taster) soll eine Schleife unterbrechen, wenn das Seil unzulässigerweise die Rollen verläßt, um den Antrieb sofort abzustellen. Alle Taster auf den Stützen sind in Serie geschaltet.