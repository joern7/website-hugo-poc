---
layout: "image"
title: "Station der Seilbahn"
date: "2011-12-02T21:37:41"
picture: "DSCF7786.jpg"
weight: "7"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33595
- /details7a1d.html
imported:
- "2019"
_4images_image_id: "33595"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-02T21:37:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33595 -->
Hier könnt ihr die neue und deutlich funktionssicherere Seilbahn sehen. 
Auf diesem Bild sieht man besonders die Rolle, welche bei meinem letzten System nicht ganz ausgereift war. Eine breitere Rolle ersetzt diese. Außerdem ist das gesamte System breiter und die Station ist nicht schräg um 7,5° geneigt, sondern komplett eben. Daher sind Berg- und Talstation diesesmal identisch, was auch ein besseres "Design" möglich macht.