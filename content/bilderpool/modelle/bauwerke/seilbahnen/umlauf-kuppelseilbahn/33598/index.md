---
layout: "image"
title: "Blick von unten"
date: "2011-12-02T21:37:41"
picture: "DSCF7792.jpg"
weight: "10"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33598
- /details1b0f-2.html
imported:
- "2019"
_4images_image_id: "33598"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-02T21:37:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33598 -->
Man kann hier gut die verbesserte Antriebsrolle sehen. Der Power-Motor treibt das Seil direkt an, die Kraft wird trotzdem übersetzt, da die Rolle kleiner ist. 
Die Gondeln werden übrigens (siehe altes System: http://www.ftcommunity.de/details.php?image_id=29442#col3)
an der Stelle, wo sich Kette und Seil überschneiden ein- ausgekuppelt.
P.S.: Nützliche Kommentare finde ich übrigens immer gut, schließlich ist auch bei diesem nicht alles perfekt und ich kann noch etwas dazulernen.