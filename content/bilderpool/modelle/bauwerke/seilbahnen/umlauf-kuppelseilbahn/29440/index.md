---
layout: "image"
title: "Übersicht Talstation"
date: "2010-12-08T21:53:46"
picture: "Kuppelseilbahn-Web-1.1.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/29440
- /detailsc620.html
imported:
- "2019"
_4images_image_id: "29440"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2010-12-08T21:53:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29440 -->
Auf diesem Bild sieht man die Talstation einer von mir gebauten Umlauf - Kuppelseilbahn.