---
layout: "image"
title: "Gondeln der Bahn"
date: "2011-12-02T21:37:41"
picture: "DSCF7789.jpg"
weight: "8"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33596
- /detailsf00d.html
imported:
- "2019"
_4images_image_id: "33596"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-02T21:37:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33596 -->
Eine weitere Neuerung sind die Gondeln, statt einfachen Batterienkästen sind sie aus Papier mit einem Ausgleichsgewicht zur Stabilisierung während der Fahrt im Inneren. Der Haken oben ist der Selbe geblieben. Dieses System hat sich nach mehrfachen Tests (min. 15 Versuche, ein Auskuppeln in der Station mit anderen Systemen zu finden!!!) als Bestes erwiesen.