---
layout: "image"
title: "Lagerung des Linsenkorbs (2)"
date: 2020-05-01T11:16:20+02:00
picture: "2019-10-25 Leuchtturm8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die andere Seite der Lagerung.