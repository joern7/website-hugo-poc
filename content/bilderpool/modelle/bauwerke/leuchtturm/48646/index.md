---
layout: "image"
title: "Antrieb (4)"
date: 2020-05-01T11:16:24+02:00
picture: "2019-10-25 Leuchtturm5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein halbes Stockwerk höher wird die Achse locker geführt.