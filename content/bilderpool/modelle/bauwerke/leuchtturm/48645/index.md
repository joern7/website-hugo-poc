---
layout: "image"
title: "Antrieb (5)"
date: 2020-05-01T11:16:22+02:00
picture: "2019-10-25 Leuchtturm6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Über zwei Kardangelenke geht es schließlich zum Rast-Z10, dass das Z40 des Linsenkorbs antreibt