---
layout: "comment"
hidden: true
title: "9624"
date: "2009-07-22T21:06:16"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

Das kleine Differential Planetenrad 10 (31412) habe ich erst aufbebohrt nach 5mm.  
Dann klemmt dieses über ein 5mm Rohr (etwa 17mm lang) mit M4-innen-Gewinde. 

Beim drehen der Kurbel dreht sich dass 5mm Rohrchen mit M4-innen-Gewinde. 
Die Gewindestange-M4 dreht nicht, sondern bewegt nur nach oben oder unten, abhänglich der Drehrichtung.

Gruss, 

Peter Damen 
Poederoyen NL