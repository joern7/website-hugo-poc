---
layout: "image"
title: "Inundatie-Waaiersluis"
date: "2009-07-19T17:21:20"
picture: "FT-Inundatie-Waaiersluis_043.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poedeoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24624
- /detailsf153-2.html
imported:
- "2019"
_4images_image_id: "24624"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:21:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24624 -->
Een (inundatie-) waaiersluis is een speciale sluis, die middels omloopriolen tegen de waterdruk in geopend en gesloten kan worden. Dit type sluis is uitgevonden door Jan Blanken (NL 1755-1838)