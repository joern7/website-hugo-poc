---
layout: "overview"
title: "Inundatie-waaiersluis"
date: 2020-02-22T08:38:34+01:00
legacy_id:
- /php/categories/1692
- /categoriesac41-2.html
- /categories04a7.html
- /categories524c.html
- /categoriesc24e.html
- /categoriesf014.html
- /categoriesc2fd-2.html
- /categories3a3b-2.html
- /categories658e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1692 --> 
Eine Waaierschleuse ist eine spezielle Schleuse, die gegen den Wasserdruck geöffnet und geschlossen werden kann. Dieser Schleusentyp wurde von Jan Blanken (1755-1838) erfunden.

Een waaiersluis is een speciale sluis, die als voornaamste eigenschap heeft dat hij tegen de waterdruk in geopend en gesloten kan worden.
Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.