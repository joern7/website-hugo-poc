---
layout: "image"
title: "Neubau Schiebe- und Segmentwehre für dem Sommer"
date: "2009-02-01T19:35:38"
picture: "SegmentSchuifstuwen-2009_023.jpg"
weight: "33"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/17242
- /details790c.html
imported:
- "2019"
_4images_image_id: "17242"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17242 -->
Neubau Schiebe- und Segmentwehre für dem Sommer