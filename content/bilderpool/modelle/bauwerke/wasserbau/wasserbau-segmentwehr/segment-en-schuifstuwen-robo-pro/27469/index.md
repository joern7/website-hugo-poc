---
layout: "image"
title: "Wasserniveau-Messung mit Drucksensor  RVAQ050GB2 (Sensor-Technics)"
date: "2010-06-13T11:39:50"
picture: "2010-4-Stuwen-Drucksensor_RVAQ050GB2_018.jpg"
weight: "32"
konstrukteure: 
- "Peter, Poederoyen NL"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/27469
- /detailsf9ca.html
imported:
- "2019"
_4images_image_id: "27469"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27469 -->
Versuch mit Drucksensor  RVAQ050GB2 (Sensor-Technics)  zum bessere und stabielere Wasserniveau-Messung als mit dem US-Sensoren:   

Gluck oder Ungluck ?......……gleiche Ergebnisse.

Grüss,

Peter, Poederoyen NL