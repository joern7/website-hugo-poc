---
layout: "image"
title: "Segment- en Schuifstuwen, naast Robo-Pro-niveau-sturing nu ook met handbediening."
date: "2010-04-25T19:48:52"
picture: "SchuifSegment-Stuwen-AutomHanbediening_013.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26988
- /details29af.html
imported:
- "2019"
_4images_image_id: "26988"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2010-04-25T19:48:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26988 -->
Handbedienung macht die Kinder Antonie & Annemieke viel mehr Spass !