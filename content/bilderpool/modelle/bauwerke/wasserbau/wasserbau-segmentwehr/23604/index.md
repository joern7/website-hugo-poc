---
layout: "image"
title: "Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen"
date: "2009-04-05T22:44:38"
picture: "Schuif-_Segment-_en_Klepstuwen_-Fischertechnik_002.jpg"
weight: "66"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23604
- /details0620-2.html
imported:
- "2019"
_4images_image_id: "23604"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-04-05T22:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23604 -->
Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen

Das RoboPro-Programm für meine 5 Wehren gibt es im FT-Community / Downloads.
Ich versuche mit einer Pot-Meter und RoboPro in 5 Wasserbecken eine stabile verstelbare Wasserstand zu erreichen.

Mit kurze Motorbetriebzeiten  ist eine stabile Wasserstand möglich:  [+/-1 cm] 
Statt die Impuls-schalter nutze ich 0,02-1,0 sec für eine kleine Wehr-Motor-Bewegungen. 
Jede 0,3 - 0,5 Sekunde wirt die Wasserstand mit einem US-Sensor (oder Potmeter) gemessen.
Wenn es eine niedrige Wasserstand gibt als mit dem Pot-meter eingefuhrt und berechnet, bewegt die Wehre ein wenig nach Unten damit eine hohere Wasserstand erreicht wird. 


Ik kan elke stuwen onafhankelijk bedienen, waarbij in elke stuwpand (=goot) de waterstand middels de instelling van een pot-meter geregeld kan worden. Het werkt prachtig en m'n zoon Antonie (9) en dochter Annemieke spelen er ook graag mee !...........
 
Zoals je weet werk ik zelf bij Waterschap Rivierenland en heb in het verleden ook veel met stuwen te maken gehad. In de praktijk worden 1 of 2 x per jaar de grote A-watergangen "geveegd" met een maaiboot. Alle waterplanten e.d. worden dan onder water afgemaaid. Vervolgens worden de stuwen open gezet en vervolgens snel volledig geopend om het maaisel af te voeren. Een en ander zoals je ook een WC doortrekt......
 
Voor dichtslibbende havens wordt soms hetzelfde principe toegepast. Water vasthouden om er vervolgens met voldoende stroomsnelheid het afgezette slib weg te spoelen. Vroeger gebeurde dit bijvoorbeeld middels een spuikom (nu jachthaven) ook in Willemstad.
In Frankrijk bouwt men momenteel driftig aan een getijdendam met segmentstuwen om de aanslibbing rond Mont-Saint-Micheal in de toekomst weer te niet te doen.