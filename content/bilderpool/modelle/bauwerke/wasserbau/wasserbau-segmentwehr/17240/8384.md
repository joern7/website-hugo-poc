---
layout: "comment"
hidden: true
title: "8384"
date: "2009-02-01T19:50:30"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Schiebewehre-nr.1 hat ein US-entfernungsmesser "System Richard Budding" mit Analog A1-output. 

Schau mal unter: 
http://www.ftcommunity.de/categories.php?cat_id=602 

Weil das Robo-Interface nur 2 Entfernungsmesser-Anschlusse D1 und D2 hat, war dies eine schöne Trick. 
Die original FT-Entfernungsmesser nutze ich schon für Segmentwehre-nr2 und Segmentwehre-nr3. 

Gruss, 

Peter Damen 
Poederoyen NL