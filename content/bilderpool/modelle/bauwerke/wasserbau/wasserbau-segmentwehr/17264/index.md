---
layout: "image"
title: "Virtuele Sommer-2009........ (mit alte Segment-Wehrantrieb-2008)"
date: "2009-02-01T19:35:39"
picture: "2008-aug-Afgedamde_Maas_001.jpg"
weight: "55"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/17264
- /details78b7.html
imported:
- "2019"
_4images_image_id: "17264"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-02-01T19:35:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17264 -->
Virtuele Sommer-2009........ (mit alte Segment-Wehrantrieb-2008)

Draussen ist es -5 grad Celcius...

Drinnen: 
Fischertechnik Neubau Schiebe- und Segmentwehre für dem Sommer.......