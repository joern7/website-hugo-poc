---
layout: "comment"
hidden: true
title: "10587"
date: "2010-01-23T14:56:24"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Die Maxon Encoder-Getriebemotoren funktionieren auch mit dem TX mit "Channel A" oder "Channel B" als analogen I1 10V-Spannungs-eingang.

Als direkter 5V-Spannungsversorgung für die Encoder nutze ich TX-PIN-Belegung-EXT2 1=GND und 2=5V DC-out.

Anschluss am schnellen Impulszähler C1-4 funktioniert leider nicht. 

Link zum Encoder der Maxon-Getriebemotor:
 
http://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/encoder.pdf

Grüss,

Peter Poedeoyen NL