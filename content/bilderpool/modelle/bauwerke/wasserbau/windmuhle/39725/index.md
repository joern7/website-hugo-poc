---
layout: "image"
title: "Detail Stirnrad"
date: "2014-10-20T21:59:39"
picture: "Detail_Stirnrad.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39725
- /details6d30.html
imported:
- "2019"
_4images_image_id: "39725"
_4images_cat_id: "2980"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39725 -->
