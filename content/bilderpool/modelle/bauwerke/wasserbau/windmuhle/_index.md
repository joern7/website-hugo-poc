---
layout: "overview"
title: "Windmühle"
date: 2020-02-22T08:38:41+01:00
legacy_id:
- /php/categories/2980
- /categories9de4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2980 --> 
Diente früher zur Landgewinnung in den Niederlanden.
Eigentlich ist es eine Pumpe, die mit Hilfe von Wind angetrieben wurde.