---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:30"
picture: "tbm19.jpg"
weight: "19"
konstrukteure: 
- "Herrenknecht"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46142
- /details681f.html
imported:
- "2019"
_4images_image_id: "46142"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:30"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46142 -->
Weblink :

https://www.youtube.com/watch?v=kyXQfxsrymg&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ&t=9s