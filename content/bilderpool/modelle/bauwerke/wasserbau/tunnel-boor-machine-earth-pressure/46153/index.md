---
layout: "image"
title: "FFM 3/2-Wege Magnetventilen. Kompaktere Maße, zusätzlich drosselbare Entlüftungsmöglichkeit + billiger"
date: "2017-08-22T19:52:01"
picture: "tbm30.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46153
- /details21bf.html
imported:
- "2019"
_4images_image_id: "46153"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46153 -->
