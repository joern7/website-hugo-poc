---
layout: "image"
title: "Segment-Tubing-Transport TBM"
date: "2017-08-22T19:51:15"
picture: "tbm09.jpg"
weight: "9"
konstrukteure: 
- "Herrenknecht"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46132
- /details41cb.html
imported:
- "2019"
_4images_image_id: "46132"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46132 -->
