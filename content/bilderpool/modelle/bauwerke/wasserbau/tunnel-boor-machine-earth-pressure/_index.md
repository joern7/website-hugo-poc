---
layout: "overview"
title: "Tunnel-Boor-Machine + Earth Pressure Balance Shield"
date: 2020-02-22T08:38:43+01:00
legacy_id:
- /php/categories/3427
- /categoriese18d.html
- /categoriesbc8d.html
- /categoriesfed9.html
- /categoriesbddb.html
- /categoriesd91d.html
- /categories814e.html
- /categories2662-2.html
- /categories83b6.html
- /categories2383.html
- /categories7c81.html
- /categories5651.html
- /categories8d9c.html
- /categoriesbe14.html
- /categories15e3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3427 --> 
In weichen, bindigen Böden werden bevorzugt Vortriebsmaschinen mit Erddruckstützung eingesetzt. Bei den sogenannten Erddruckschilden (engl. Earth Pressure Balance Shield, kurz EPB) dient ein Erdbrei aus abgebautem Material als plastisches Stützmedium. Dies ermöglicht den nötigen Ausgleich der Druckverhältnisse an der Ortsbrust, verhindert ein unkontrolliertes Eindringen des Bodens in die Maschine und schafft die Voraussetzung für einen schnellen und weitestgehend setzungsfreien Vortrieb.