---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield"
date: "2017-08-22T19:52:01"
picture: "tbm47.jpg"
weight: "47"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46170
- /detailse9e4.html
imported:
- "2019"
_4images_image_id: "46170"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46170 -->
Innen :  Tubing Transport