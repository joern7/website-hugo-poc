---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:30"
picture: "tbm18.jpg"
weight: "18"
konstrukteure: 
- "Herrenknecht"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46141
- /detailsd579-3.html
imported:
- "2019"
_4images_image_id: "46141"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46141 -->
