---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen21.jpg"
weight: "21"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41365
- /detailsd4ba.html
imported:
- "2019"
_4images_image_id: "41365"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41365 -->
Instroom-opening met centrifugaal-pomp (= klein wormwiel)