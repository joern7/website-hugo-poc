---
layout: "image"
title: "Bosman B4 Windwatermolen -werking mech. automatische instelbare peil-beheersing"
date: "2015-07-01T18:05:08"
picture: "windwatermolen05.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41349
- /details1c66.html
imported:
- "2019"
_4images_image_id: "41349"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41349 -->
Als het water hoog in de sloot staat, staat de hoofdvaan verticaal en de hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 

Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.
