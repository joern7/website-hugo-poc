---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41356
- /details09ae-2.html
imported:
- "2019"
_4images_image_id: "41356"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41356 -->
Aandrijving centrifugaalpomp door molenwieken + automatische windvaan-verstelling door vlotter

Hoofdvaan verticaal : Windmolen draait.