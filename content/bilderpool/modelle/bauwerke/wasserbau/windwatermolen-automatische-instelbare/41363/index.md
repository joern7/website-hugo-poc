---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen19.jpg"
weight: "19"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41363
- /details4a38.html
imported:
- "2019"
_4images_image_id: "41363"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41363 -->
- Boven vooraanzicht (= laagwaterzijde)

Hoofdvaan verticaal: Windmolen draait.