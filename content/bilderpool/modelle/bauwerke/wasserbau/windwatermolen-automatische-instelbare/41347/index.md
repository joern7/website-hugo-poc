---
layout: "image"
title: "Bosman B4 Windwatermolen in Noordwaard Biesbosch  -geschiedenis"
date: "2015-07-01T18:05:08"
picture: "windwatermolen03.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41347
- /details5741.html
imported:
- "2019"
_4images_image_id: "41347"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41347 -->
Het Bosman-molentje is een kleine poldermolen die in 1929 door Bas Bosman in de Nederlandse plaats Piershil is ontwikkeld en daarna overal in de wereld toegepast. 
Ze worden nog steeds gemaakt in Piershil.

Bij de eerste types windmolens werd de haakse overbrenging vervaardigd uit de cardan van een T-Ford. 
Deze molen werkte maar half automatisch. Hij draaide zich automatisch op de wind door middel van het staartblad als windvaan, maar moest handmatig in en uit de wind worden gezet door deze vaan met de hand een kwart slag te draaien, tot hij plat lag en geen wind meer ving. 

De werking is daarna geautomatiseerd ten opzichte van het waterpeil in de sloot door de toevoeging van de vlotter en de hulp- of bijvaan, die haaks op de hoofdvaan staat. 
Latere types werkten met een cardan uit de A-Ford (tot ca. 1960) (type A-Ford) en weer later die uit de Ford V8 (tot 1977) (type V-8). 
Na de oorlog werden de cardanassen uit legerdumps verkregen. Vanaf 1977 maakte Bosman de overbrengingen zelf. 
Deze molens kregen de aanduiding 'type 77'.

In 2006 is het nieuwste model op de markt gebracht. Hierin zijn vele verbeteringen doorgevoerd. Dit type wordt als 'type B4' verkocht.
In 2010 ontwierp Gijsbert Koren als afstudeerproject aan de TU Delft een Bosman-molentje, waarbij de bladen van de wieken zijn uitgevoerd in bamboe. Naast milieuvriendelijker, is deze molen lichter in gewicht, efficiënter in gebruik en goedkoper te produceren.

De windmolens zijn altijd geleverd met prefab putten. De eerste van Bottenberg, daarna van Bodegom, totdat Bosman deze zelf is gaan produceren. Doordat de molens compleet met fundering werden geleverd konden ze snel worden geplaatst.

Aan de werking ervan komt geen mens te pas. 
Er drijft een vlotter op het water, die via een stang de stand van beide vanen instelt. Van de hoofdvaan, die recht achter de wieken zit en van de hulp- of bijvaan, die opzij staat. 
Als het water hoog in de sloot staat, staat de hoofdvaan verticaal en de hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 
Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.
De draaiende wieken drijven een centrifugaalpomp aan, die het water uit de sloot maalt. Als het water genoeg is gezakt, heeft de vlotter inmiddels de vanen zover gedraaid, dat de wieken uit de wind draaien en de bemaling stopt. 

De pomp biedt voldoende weerstand om bij storm het op hol slaan van de molen te voorkomen. 

Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. 
Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.
