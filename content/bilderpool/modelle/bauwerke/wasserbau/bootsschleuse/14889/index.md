---
layout: "image"
title: "Blick in die Schleusenkammer"
date: "2008-07-15T22:35:42"
picture: "0006_Blick_in_Schleusenkammer.jpg"
weight: "4"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14889
- /details7540.html
imported:
- "2019"
_4images_image_id: "14889"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14889 -->
Wir blicken praktisch flußabwärts