---
layout: "image"
title: "Schleuse"
date: "2008-07-15T22:35:42"
picture: "0003_Schleuse.jpg"
weight: "1"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14886
- /detailsd746.html
imported:
- "2019"
_4images_image_id: "14886"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14886 -->
Unten sieht man das Intelligent Interface mit Erweiterungs-Modul.

Gesteuert wird die Schleuse von mir mit RoboPro.

Rechts sieht man den Kompressor.