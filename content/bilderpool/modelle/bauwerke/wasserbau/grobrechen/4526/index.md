---
layout: "image"
title: "krooshekreiniger"
date: "2005-07-30T14:12:03"
picture: "Fischertechnik-krooshekreiniger_001.jpg"
weight: "10"
konstrukteure: 
- "&NBSP,"
fotografen:
- "peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4526
- /details8c05.html
imported:
- "2019"
_4images_image_id: "4526"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-07-30T14:12:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4526 -->
