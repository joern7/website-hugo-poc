---
layout: "image"
title: "krooshekreiniger"
date: "2005-07-30T14:12:03"
picture: "Fischertechnik-krooshekreiniger_001_2.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4535
- /detailsc343-2.html
imported:
- "2019"
_4images_image_id: "4535"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-07-30T14:12:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4535 -->
