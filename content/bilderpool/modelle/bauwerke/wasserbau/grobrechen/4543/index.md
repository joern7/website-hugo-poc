---
layout: "image"
title: "krooshekreiniger"
date: "2005-08-12T13:08:58"
picture: "Fischertechnik-Krooshekreiniger_in_bedrijf_005.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4543
- /details4aca.html
imported:
- "2019"
_4images_image_id: "4543"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-08-12T13:08:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4543 -->
