---
layout: "image"
title: "Befestigung der Aufzugsschiene (1)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47092
- /detailsdf07-2.html
imported:
- "2019"
_4images_image_id: "47092"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47092 -->
Die Aufhängung kommt ohne 3D-gedrucktes Teil aus ;-)