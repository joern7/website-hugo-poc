---
layout: "image"
title: "Plattform 1"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47100
- /detailsed8f.html
imported:
- "2019"
_4images_image_id: "47100"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47100 -->
Kurz über dem Grund....