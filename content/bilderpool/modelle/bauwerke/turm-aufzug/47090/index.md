---
layout: "image"
title: "Antennen (1)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47090
- /detailse9b6.html
imported:
- "2019"
_4images_image_id: "47090"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47090 -->
Wer das nicht kennt: Das sind original fischertechnik-Pappsegmente, wie sie im Ur-ft-200 (und ich glaube auch im ft 400) enthalten waren. Die sind genau für so einen Radarschirm gedacht gewesen - man vergleiche das Radar-Modell in den alten Grundkasten-Anleitungen.