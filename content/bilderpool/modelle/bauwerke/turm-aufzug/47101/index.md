---
layout: "image"
title: "Prototyp der Steuerung"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47101
- /details578d.html
imported:
- "2019"
_4images_image_id: "47101"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47101 -->
Mit diesem Aufbau wurde die während der Wartezeit bei einem Zahnarzt (wirklich!) aufgemalte Schaltung für die Aufzugssteuerung getestet.