---
layout: "image"
title: "Steuermodul (1)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47095
- /detailsa0e2.html
imported:
- "2019"
_4images_image_id: "47095"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47095 -->
Hier die Rückseite der Aufzugssteuerung. Zwei ft-em-5-Relais, ein Ein-/Aus-Schalter und je ein Taster für "fahre zum nächsten Stockwerk hoch" und "fahre zum nächsten Stockwerk runter" genügen.