---
layout: "image"
title: "Lichtschranke"
date: "2007-12-03T23:30:28"
picture: "kieswerk2.jpg"
weight: "15"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12989
- /details95b5.html
imported:
- "2019"
_4images_image_id: "12989"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-12-03T23:30:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12989 -->
S-Riegel6 vor den Lichtschranken. Die Löcher befinden sich knapp über dem Förderband