---
layout: "image"
title: "Förderbänder 2"
date: "2007-11-28T21:04:56"
picture: "kieswerk09.jpg"
weight: "9"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12869
- /details909e.html
imported:
- "2019"
_4images_image_id: "12869"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12869 -->
Seitliche Ansicht