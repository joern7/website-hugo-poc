---
layout: "image"
title: "Einlagern"
date: "2008-01-02T21:29:31"
picture: "kieswerk3.jpg"
weight: "3"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13203
- /details1687.html
imported:
- "2019"
_4images_image_id: "13203"
_4images_cat_id: "1192"
_4images_user_id: "424"
_4images_image_date: "2008-01-02T21:29:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13203 -->
16 Regale stehen zur Einlagerung zur Verfügung