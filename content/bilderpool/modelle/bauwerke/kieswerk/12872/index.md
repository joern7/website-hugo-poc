---
layout: "image"
title: "Sortieranlage 2"
date: "2007-11-28T21:05:13"
picture: "kieswerk12.jpg"
weight: "12"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12872
- /details07a2-2.html
imported:
- "2019"
_4images_image_id: "12872"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:05:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12872 -->
Probleme die gelöst werden mussten. Abstand, Lage, abtasten der Länge, auffangen der fliegenden Riegel usw.