---
layout: "image"
title: "Aufzug 3"
date: "2007-11-28T21:04:56"
picture: "kieswerk05.jpg"
weight: "5"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12865
- /detailsf498.html
imported:
- "2019"
_4images_image_id: "12865"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12865 -->
Mulde ganz angehoben. Wagen wird am Endschalter gestoppt