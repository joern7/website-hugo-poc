---
layout: "image"
title: "Rüttler"
date: "2007-11-28T21:04:56"
picture: "kieswerk07.jpg"
weight: "7"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12867
- /details5914.html
imported:
- "2019"
_4images_image_id: "12867"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12867 -->
Hier werden die Riegel schon ein bisschen getrennt, das heisst Sie fallen mit einem gewissen Abstand auf das Förderband.