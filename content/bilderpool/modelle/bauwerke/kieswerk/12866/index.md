---
layout: "image"
title: "Aufzug Ansicht"
date: "2007-11-28T21:04:56"
picture: "kieswerk06.jpg"
weight: "6"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12866
- /detailsaffe.html
imported:
- "2019"
_4images_image_id: "12866"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12866 -->
Aufzug mit Rüttler und Förderbänder