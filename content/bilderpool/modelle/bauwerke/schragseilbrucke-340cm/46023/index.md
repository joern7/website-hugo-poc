---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 7)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-17.jpg"
weight: "47"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Antrieb", "Gleichlaufgetriebe", "Spule", "Schneckengetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46023
- /detailsf479.html
imported:
- "2019"
_4images_image_id: "46023"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46023 -->
Antrieb mit fertigen Spulen.

DIe Räder (gummiräder aus den Klassik Kästen) berühren sich nicht! (gerade 1mm Platz, der normalerweise von Gummi Schlupfringen überbrückt wird. Bin ich Fan davon.)

Gut zu erkennen ist, dass es mir bei der ganzen Konstruktion um Stabilität geht. Auf die Bahn wirken später einige Kräfte und der Leichtlauf soll gesichert sein.

Die roten Platten sind übrigens das Dach des späteren Betriebsgebäudes und werden noch weitere Funktion haben (2. Obergeschoss)