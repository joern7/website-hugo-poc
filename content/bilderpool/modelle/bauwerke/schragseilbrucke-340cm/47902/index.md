---
layout: "image"
title: "Fahrtrichtungsgeber Version 2 Prototyp"
date: "2018-09-23T13:24:04"
picture: "Fahrtrichtungsgeber-entwicklung03.jpg"
weight: "80"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Reedkontakt"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47902
- /detailsefca-2.html
imported:
- "2019"
_4images_image_id: "47902"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47902 -->
HIer nun der fertige Funktionsprototyp.
Zu erkennen nun wie das Z40 über den Reedkontakten hängt und der darunter befestigte Magnet diese auslöst. Die Stellung soll mit dem weißen Dreieck verdeutlicht werden.

In das Z40 greift nachher ein Z10 von der Welle der Spule. So ist die Anzeige langsamer und flackert nicht so.

Die Anzeige wird später auf der Fernsteuerung angebracht sein und besteht aus 4 Lampen die in der Reihenfolge nach links oder rechts als Lauflicht blinken, je nachdem in welche Richtung die Gondel fährt.