---
layout: "image"
title: "Kubus"
date: 2020-08-30T16:23:01+02:00
picture: "Kubus.jpg"
weight: "134"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Unter dem Schacht, in dem das Spanngewicht läuft ist ein „Kubus“. Dieser zeigt die Fahrpreise: einfache Fahrt = 1,50 fischertaler
hin & rück = 2,50 ft
Kinder, Studenten, Anwohner 0,80 fischertaler
(Die Maße des Bauwerks sind leicht verändert worden im Laufe der Entwicklung)