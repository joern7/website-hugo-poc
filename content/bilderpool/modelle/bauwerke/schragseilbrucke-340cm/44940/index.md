---
layout: "image"
title: "Beleuchtungstest (Nacht) Turm 1"
date: "2016-12-28T12:29:46"
picture: "IMG_20161226_232252.jpg"
weight: "32"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["elektrik", "schrägseilbrücke", "statik", "LED", "lampe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44940
- /details2362-2.html
imported:
- "2019"
_4images_image_id: "44940"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44940 -->
Zum ersten Mal teste ich die Beleuchtung der Brückenkonstruktion. Das ganze Wohnzimmer (Weihnachten) ist nur von der Brücke erleuchtet.

Gut zu erkennen ist die 3,40m lange Fahrbahn der Hängebrücke (führt rechts aus dem Bild heraus zum Turm 2
Im Hintergrund auf der weißen Holzgrundplatte steht die Talstation (Turm 1).
Dazwischen spannt sich über der Fahrbahn das Tragseil der Beleuchtung.
Gut zu sehen sind nun auch die Schrägseile, welche die Fahrbahn halten.

Es leuchten:
- 5 weiße LED zur Fahrbahnbeleuchtung am Tragseil (schön zu sehen ist die Lochblende)
- 2 blinkende rote LED an der Turmspitze zur Kollisionsbeleuchtung
- 2 rote LED am Tragseil über der Fahrbahnbeleuchtung
- je 1 rote LED an der hinteren Strebe zur Kollisionsbeleuchtung
- je 2 weiße LED in unterschiedlichen Winkeln an jedem Turm (ca. halbe Höhe) zur Fahrbahnbeleuchtung (eine davon blendet hier schön)
- je 1 weiße LED am Lagerblock, die den Turm von schräg unten beleuchtet (schönes Schattenspiel an der Decke leider nicht zu sehen)


Das Bild wurde nur mit Handy gemacht - wenn alles fertig ist werde ich im Garten (Sommer) Bilder mit HDR zur blauen Stunde machen, wenn die Brücke mit Fahrzeug über dem Gartenteich steht....