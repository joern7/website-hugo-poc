---
layout: "comment"
hidden: true
title: "22858"
date: "2016-12-31T12:29:14"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Ja, richtig. Aber hier ist die senkrechte Last (Gewicht) der Fahrbahn sehr gering. Es kommt also vor allem auf die Richtungs-Führung der Fahrbahn an.

P.S.: alle Tests die ich jetzt gemacht habe erfüllen exakt meine Erwertungen. Selbst die Last mit einer Bahn und zusätzlichen 1kg Gesicht an allen Stellen der Fahrbahn. Ich denke, dass 2kg Fahrbahnbelastung kein Problem sein werden.