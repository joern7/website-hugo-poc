---
layout: "image"
title: "Getriebeblock Seilantrieb (Funktionsprototyp)"
date: "2016-10-01T22:12:29"
picture: "GOPR9187b.jpg"
weight: "3"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Getriebe", "Differential", "Seil", "Antrieb", "Prototyp"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44489
- /details8fad.html
imported:
- "2019"
_4images_image_id: "44489"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44489 -->
Der Prototyp zur Funktionsprüfung des Antriebes.
Die Hängbahn wird von einem Seil angetrieben, das nicht umlaufend ist, da die Reibungskräfte nicht ausreichen würden um einwandfrei zu funktionieren.
Daher haben wir uns für einen Antrieb mit zwei gegenläufigen Spulen entschieden. (Im Prototyp etwa Bildmitte).
Im Fahrbetrieb treibt der Fahrmotor (oben) die Spulen gleichschnell aber gegenläufig an.
Durch Abrollen einer Spule verringert sich der Spulendurchmesser und es wird weniger Seil pro Zeit abgegeben. Beim Aufrollen entsprechend andersrum.
Daraus ergibt sich eine schwankende Seillänge und daraus schwankende Seilspannung. Diese wird zwar mit einem Seilspanner konstant gehalten, doch bei zu starken Spannungen muss im Betrieb nachgespant werden. Dies geschieht mit dem Motor unten rechts, der auf die Gegenläufigen Spulen eine gleichlaufende Ab- oder Aufrollbewegung addiert.
Um die Bahn später schnell auf- und abzubauen muss das komplette Seil schnell ab- und aufgerollt werden. Hierzu übernimmt der Schnelllaufmotor (links unten) die Spulenbewegungen synchron.
Genaue Funktionsweise im Video.

Erklärung und Video mit Funktionstest hier: https://youtu.be/-EDMZNxTwsE