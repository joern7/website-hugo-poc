---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 7)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-13.jpg"
weight: "46"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Antrieb", "Gleichlaufgetriebe", "Schneckengetriebe", "Differenzialgetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46022
- /details6559-2.html
imported:
- "2019"
_4images_image_id: "46022"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46022 -->
Andere Seite: die Abtriebsseite. Hier ist auch die querliegende Schnecke zu sehen, die durch den Seillängen-Verstellmotor angetrieben wird. (Entspricht bei Kettenfahrzeugen der Lenkung).

Unter den Z10 sind Baustein 15 mit Loch zu sehen - hier stecken die Achsen der Spulen drin (nicht wie vorher auf den Aluschienen.)
Grund dafür ist, dass sich herausstellte, dass das Seil zu langsam läuft, wenn ich im Abtrieb Z30 habe (wie auf den anderen Bildern im Test zu erkennen). Schließlich soll es ein "XXL Express" werden, wie mein Sohn Jan (7) bestellt hat und keine "XXL Schnecke". Final sind die Spulen also mit einem Z10 angeschlossen und somit 3 Mal so schnell.