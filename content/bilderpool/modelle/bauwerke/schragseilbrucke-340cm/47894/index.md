---
layout: "image"
title: "Traverse 2 Prototyp"
date: "2018-09-23T13:24:04"
picture: "traverse01.jpg"
weight: "72"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47894
- /detailsd39b.html
imported:
- "2019"
_4images_image_id: "47894"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47894 -->
Ich wiederhole mich ungern - aber wer mich kennt weiß .....
ich LIEBE Funktionsprototypen.

Und so hat mich das Problem  mit der Traverse für die Endabschaltung nicht in Ruhe gelassen und ich habe diesen Prototyp gebaut.

Zu erkennen ist das Ende der Gondel (links im Bild) das nun nicht mehr die schwarzen Statik 15er hat, sondern Winkelbausteine.
Diese heben die zwei Räder an, welche an einem Balken hängen, der "Gabel". Diese gibt dann Minitaster frei.

Vorteil dieser Kontruktion: die Gondel kann sich noch weiter bewegen, wenn der Motor nicht gleich geschaltet wird. (Oder das Seil zu arg gespannt wurde.)

Die rechte Konstruktion ist in den Demiensionen entsprechend der Fahrbahnaufhängngung (oberes Gerätehaus) am Trum nachgebaut. Der liegende 30er mit Loch simuliert hier die Fahrbahn. Die Flachbausteiene 30 sind am Turm 60er.

Nachteil / Problem dieser Konstruktion: leider ist die Gabel mit Achse und Rädern nicht schwer genug, um die Minitaster verlässlich zu betätigen. Wenn die Gondel wieder abfährt kann es sein, dass nur einer der Taster auslöst.