---
layout: "image"
title: "Zusammengeklappt"
date: 2020-08-30T16:23:13+02:00
picture: "Transport02.jpg"
weight: "125"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Türme lassen sich für den Transport zusammenklappen. Dazu sind die Gelenke, welche dem Oberturm auch die Möglichkeit verleihen, nach hinten zu kippen und das Gewicht gegen die Fahrbahn aufzubringen, bestens geeignet.
Zur Transportsicherung wird dann die Spitze mit einem Alu-Profil (nicht ft) gestützt. Der Turm ist mit Spax auf einer Holzplatte montiert, der Stoff ebenfalls festgeschraubt.
So zusammengepackt besteht die Anlage aus 5 Teilen: zwei Türme, Fahrbahn, Fernbedienung und Kleinkram (in Holzkisten als Unterbau verstaut)