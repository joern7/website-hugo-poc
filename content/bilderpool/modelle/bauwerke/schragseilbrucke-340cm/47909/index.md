---
layout: "image"
title: "Fernbedienung Abdeckung Zwischenschritt"
date: "2018-09-23T13:24:04"
picture: "Bedienung-mit-Abdeckung.jpg"
weight: "87"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Magnete"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47909
- /details6252-3.html
imported:
- "2019"
_4images_image_id: "47909"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47909 -->
Fast fertig: die Pappe passt nun auf die Fernbedienung.
Zwar fehlen noch einige Löcher (Schalter) aber es geht bei dem Test darum, dass die Klappe gut geschlossen werden kann und alles verdeckt ist, das ich nicht sehen möchte.

Was mich stört ist der Magnet an der Klape (unterer Rand) der heraus schaut, da der Zapfen einfach zu lang ist. Hier hat Fischertechnik meiner Meinung nach total Schrott gebaut, da weder der Magnet genau im Raster ist, noch sauber aufgebracht, mit den Zacken am Rand sinnlos und zwei Magnete zusammen auch keine Rasterhöhe haben. Vielleicht der Tipp an die Werke: Neuauflage der Magneten als richtig kräftige Neodym-Magnete im Raster 15x15 mit passender Höhe gesamt 7,5mm.