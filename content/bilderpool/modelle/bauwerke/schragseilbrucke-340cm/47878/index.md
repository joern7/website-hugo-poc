---
layout: "image"
title: "Gewicht Kontuktion"
date: "2018-09-17T19:41:07"
picture: "gewicht-konstruktion.jpg"
weight: "56"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Seilspannung"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47878
- /detailsb281.html
imported:
- "2019"
_4images_image_id: "47878"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-17T19:41:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47878 -->
Die Frage ist:: wie schafft man es, auf engem Raum ein Gewicht (zur Seilspannung) zu konstruieren, das zwar gut geführt wird, aber dabei nicht hängenbleibt oder zu großen Reibungswiderstand hat?

(links) Die erste Idee war es, zwei Schachteln zu nehmen und so ein großes und quadratisches Gewicht zu haben, das durch die schwarzen Steine geführt wird.
Leider war das Gewicht zu groß um unter den Turm zu passen (siehe nachfolgende Bilder)

Da ich Funktionsprototypen liebe habe ich diese gebaut.

(mitte) Die zweite Version besaß Räder, die in Schienen laufen. Das reduzierte den Laufwiderstand. Problem: das Gewicht verkanntet sich.
Es hat bereits die Mitnehmer-Stifte angebracht um die Kette später mitzunehmen.

(rechts) Die endgültige Version hat nur noch Metallstangen, die (siehe nächstes Foto) in einer Metallstange laufen. Reibung = 0