---
layout: "image"
title: "Gartendekoration"
date: 2020-08-30T16:23:26+02:00
picture: "Garten01.jpg"
weight: "115"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Am Tag ist das Bauwerk in unserem kleinen Garten ein Spielzeug für Groß und Klein. Wenn man bedenkt, dass die Brücke eigentlich den kleinen Abschnitt des Teiches überspannen sollte und dazu nur 90cm lang sein bräuchte – und das mit den 340cm Spannweite heute vergleicht ….
Auf meinem YouTube Channel „ClassicMan“ habe ich übrigens die Anlage hier im Garten mit einigen Impressionen dokumentiert.