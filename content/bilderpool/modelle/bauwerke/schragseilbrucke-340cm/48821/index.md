---
layout: "image"
title: "Die Talstation"
date: 2020-08-30T16:23:16+02:00
picture: "Talstation01.jpg"
weight: "123"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Turm der Talstation ist in ganzer Größe zu sehen. Das Maschinenhaus ist mit den Metallplatten durch den Stoff auf einer Holzplatte fixiert.
Unter dem Turm hängt eine rote „Box“ diese ist das Endlager des Spannungsgewichtes. Darin die Notabschaltung und die Führung der Steuerkette.
Daneben ist Artur auf seiner Arbeitsplattform und sorgt für Licht.
Schön zu sehen auch, dass die Kabel der Fernbedienung (dicke Leitung) und des Seriellen Kabels (Versorgung der Bergstation) im Maschinenhaus am Hauptanschluss eingesteckt sind und links daneben die schwarze „Steckdose“ für Arturs Handlampe.