---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20161002_171049a.jpg"
weight: "18"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Brücke", "Schrägseilbrücke", "Hängebrücke", "Seilwinde"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44520
- /detailsfb35.html
imported:
- "2019"
_4images_image_id: "44520"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44520 -->
Lagerblockin Funktion von oben.
Von links kommen die Seile vom Turm herunter (die Fahrbahn ist links noch im Ansatz zu erkennen). Alle 4 Halteseile werden im Lagerblock aufgespult. Die Riegel blockieren die Zahnräder und somit die Spulen. Außerdem werden die Kräfte der Haltestreben aufgenommen und in die Grundplatte eingeleitet. Nach tests zeigt sich: das System ist absolut perfekt und arbeitet millimetergenau.