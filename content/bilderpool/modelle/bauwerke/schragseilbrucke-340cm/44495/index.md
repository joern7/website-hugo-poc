---
layout: "image"
title: "Biegesteifigkeit der Fahrbahn"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_182919a.jpg"
weight: "8"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Statik", "Turm", "Brücke", "Hängebrücke", "Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44495
- /detailsc494.html
imported:
- "2019"
_4images_image_id: "44495"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44495 -->
Noch einmal der Vergleich beier Türme von vorne (links neue Version).
Die Fahrbahn ist dank der 2. Laufschiene nun deutlich biegestabiler und braucht keine Torsionsbelastung mehr aufnehmen, da sie auf beiden Seiten der Spannseile belastet wird. Zu erkennen auch, dass die Fahrbahn deutlich höher geworden ist.
Derzeit ist die Fahrbahn (jeweils eine Hälfte pro Turm) nur provisorisch mit einem Seil abgespannt. Daher biegt sie sich deutlich (zum testen). WIe sich die Fahrbahn verhält wenn sie pro Turm mit 4 Abspannungen gehalten wird ist noch unklar.