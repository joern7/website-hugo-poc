---
layout: "image"
title: "ft:express - neue Version"
date: "2018-09-23T13:24:04"
picture: "express-2.jpg"
weight: "78"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47900
- /details8579.html
imported:
- "2019"
_4images_image_id: "47900"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47900 -->
Die Gondel auf der Laufschiene.

Die endgültige Version zeigen wir auf der ft:con in Dreieich 2018

Zu erkennen nun der Aufkleber.
Außerdem sieht man die Rampen am Ende des oberen Alu-Profil. Sie sorgen auch bei geneigter Fahrbahn (Steigung oder Gefälle) für das zuverlässige Auslösen der Endschalter.

Wer genau hinschaut sieht links, direkt unter der roten Laufschiene das blau-weiße und rechts das rot-weiße Zugseil. Sie führen in die Gondel hinein und werden in die Radhalter-Steine eingehängt. Das ist für Auf- und Abbau ausreichend schnell und zuverlässig, da ständig durch das Gewicht auf Zug gehalten.

Unter der Laufschiene schaut in Bögen das Kabel (Serielles PC Kabel) hervor. Es versorgt den Turm 2 (Bergstation) mit Strom und übertägt die Signale.