---
layout: "image"
title: "Transport"
date: 2020-08-30T16:23:14+02:00
picture: "Transport.jpg"
weight: "124"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Natürlich sollte man ein so großes Modell auch lagern und transportieren können. Hier also die Fracht auf dem Weg zur Süd:Convention.
In den Holzkästen, die als Unterbau dienen, ist noch Kleinkram drin. Die Cola-Kästen dienen dazu das Modell weiter anzuheben., so dass man es gut betrachten kann.