---
layout: "image"
title: "Warnung am Stecker"
date: 2020-08-30T16:23:08+02:00
picture: "Warnung01.jpg"
weight: "129"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Am Stecker der Anlage ist ein Warnhinweis. Da die Motoren sofort anlaufen, wenn der Strom eingesteckt wird, muss die Anlage aus dem Automatikbetrieb genommen werden. Andernfalls könnte es dazu kommen, dass das Getriebe Schaden nimmt.
Übrigens: die ganze Brücke wird mit einem einzigen alten Trafo versorgt, alle LED (mehr als 30 Stück) und beide Powermotoren.