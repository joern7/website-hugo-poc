---
layout: "image"
title: "Kabelchaos 4"
date: "2018-09-23T13:24:04"
picture: "gehuse05.jpg"
weight: "93"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47915
- /details0606.html
imported:
- "2019"
_4images_image_id: "47915"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47915 -->
Nicht Harry Potter, sondern der Ingenieur sorgt für verschwundene Kabel am Turm 2.

Der serielle Stecker wird im oberen Betriebsgebäude eingessteckt und die Lampen über die Streben zur Abspannung versorgt. So sieht das aufgeräumt aus und lässt die Architektur des Turmes noch viel besser gelten.

Das bin ich meinem Sohn Jan (9) schuldig, der immerhin vor bereits fast 2 Jahren den Turm entwickelt hat (selbständig!) und die Verstrebungen und das Design wirklich klasse gemacht hat. Meine Arbeit war "nur" die Blöcke (hier im Bild rechts) und das Bisschen Technik zu machen.