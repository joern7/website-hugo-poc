---
layout: "image"
title: "Motorraum-Leuchte"
date: 2020-08-30T16:23:11+02:00
picture: "Verkabelung07.jpg"
weight: "127"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Kabelchaos? Nein!
Alle Kabel, die aus dem Centronics und Seriellen Stecker kommen sind als Kabelpeitsche ausgeführt. Unterschiedliche Farben der Kabel und Stecker / Buchsen sorgen für System. Die Verbindungen sind zusätzlich mit Isolierband (ebenfalls in systemischer Farbe) gesichert und beschriftet. (Z.B. gelbe Schilder für Beleuchtungen, L = Lampe ….) Zusammen mit dem Schaltplan lässt sich ein Fehler in Minutenschnelle finden.
Der Clou bei diesem Bild: die Kabel müssen etwas zur Seite gedrückt werden und sind hier in besonderer Weise angeordnet.
Schließt man nämlich die Abdeckung des Maschinenhauses, so muss die Lampe, die sich automatisch einklappt (rechtes Bild – Pfeil) in die Aussparung der Kabel legen. Sie wird wie im Kühlschrank automatisch ausgeschaltet.
Also: kein Chaos, sondern hohe Ordnung.