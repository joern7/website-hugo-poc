---
layout: "image"
title: "Süd:Convention 2019"
date: 2020-08-30T16:23:32+02:00
picture: "Ausstellung.png"
weight: "110"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Süd:Convention 2019 im Erlebnismuseum Fördertechnik, Sinsheim.
Das ganze Bauwerk mit 540cm Länge ist kaum auf ein Bild zu packen. Vor dem schwarzen Vorhang ist es wenigstens schön in Szene gesetzt. Die weißen Schrägseile lassen sich nun gut erkennen.
Dazu zeigt ein Monitor (am Boden liegend) einige Detailaufnahmen und Erläuterungen. 