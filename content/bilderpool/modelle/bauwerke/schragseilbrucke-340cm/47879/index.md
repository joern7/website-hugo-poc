---
layout: "image"
title: "Gewicht Laufschienen"
date: "2018-09-23T13:24:03"
picture: "gewicht-konstruktion-laufschienen.jpg"
weight: "57"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Seilspannung", "Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47879
- /details6a2c.html
imported:
- "2019"
_4images_image_id: "47879"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47879 -->
HIer sieht man die Funktionsprototypen der Laufschienen für das Gewicht zur Seilspannung.

(links) Das Gewicht passt wunderbar (auch optisch) in die Schienen. Leider ist der Reibungswiderstand extrem hoch, was diese Version sofort disqualifizierte.

(mitte / vorne) Das Gewicht der zweiten Entwicklungsstufe passt in den Führungsbau des ersten Gewichts. Leider verkanntete das Gewicht, da das Seil nie genau zentrisch läuft.

(rechts) die Endversion hat viele Vorteile. Sie läuft quasi reibungsfrei. Das Gewicht kann nicht kannten und a-zentrisch gezogen werden. Die Konstruktion lässt Platz für Kette, Seil, Kabel und Schalter. (Das war mein Glück - wie sich später rausstellen sollte.)