---
layout: "image"
title: "Maschinenhaus"
date: 2020-08-30T16:23:38+02:00
picture: "Wartung03.jpg"
weight: "106"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der Detaillierte Blick in das Maschinenhaus.
Links oben der Block für die Seilspannung der Brücke. Er ist baugleich zur Bergstation.
Links unten in der Ecke (unter dem Kippschalter, welcher die Handlampe von Artur einschaltet – daran oberhalb ein kleines schwarzes Kästchen ist das Blink-Relais, welches die Turmspitzen im Gleichtakt leuchten lässt. Im Gegensatz zu allen anderen Blink-LEDs, die ohne Takt arbeiten) befindet sich der Selbstbau-Silberling „Relais“ mit 5 X UM plus Sperrkontakt.
Direkt rechts daneben (man erkennt noch gelbe Stecker, die nach unten zeigen) der Gleichrichter-Silberling.
Dann die Steckdose für die Handlampe Arturs  (schwarz) und die Anschlüsse der Fernbedienung (im Bereich der frei sichtbaren Grundplatte) und Serielles Kabel.
Weiter rechts mit gelb abgeschrankt die drei Z20 des Gleichlaufgetriebes, das sich nach oben erstreckt (verborgen durch den querliegenden Alu-Baustein) Darüber zwei Powermotoren (mit Lila und orangenem Isolierband beschriftet).
Rechts oben in der Ecke der Bauplatte ein Schalter der mit einem Federfuß gedrückt wird. Er schaltet die „Kühlschrenkbeleuchtung“ wenn das Dach geöffnet wird. (Das Scharnier für das Dach ist die lange Achse die rechts am Bildrand entlang geht.)
Links neben dem Taster der Fahrtrichtungsgeber. Er wird mit dem Z40 angetrieben.
Noch ein Stückchen links davon sieht man die zwei blauen Kappen der „Wartung“-Schalter. Und wieder links daneben versteckt den alten Trafo – die einzige Stromquelle im Modell.
Unscharf am unteren Ende der „Wartung“ Schalter: die klappbare Beleuchtung zeigen (etwa Bildmitte), die sich aufstellt, wenn das Dach hochgeklappt wird. Sie ist an geflochtenem Kabel / Seil befestigt und wird dadurch hoch gezogen.
Das zentrale Kabel-Chaos ist gut beschriftet und mit Isolierband gesichert.