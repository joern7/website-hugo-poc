---
layout: "image"
title: "Abendstimmung"
date: 2020-08-30T16:23:25+02:00
picture: "Garten02.jpg"
weight: "116"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Am Abend mit voller Beleuchtung der Fahrbahn und des Bauwerks ist der Teich zu einer Sehenswürdigkeit in Schifferstadt geworden. Dank der beleuchteten Fernbedienung und des Maschinenraums lässt sich alles auch  im Dunkeln sicher steuern. Die blinkenden Kollisionslampen und roten Warnlichter sorgen dafür, dass auch Fledermäuse ihren Flugbetrieb sicher versehen können.