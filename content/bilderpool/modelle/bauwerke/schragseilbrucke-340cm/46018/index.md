---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 4)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-5.jpg"
weight: "42"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Gleichlaufgetriebe", "Antrieb", "Motor", "Differenzialetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46018
- /details5263.html
imported:
- "2019"
_4images_image_id: "46018"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46018 -->
Hier von der Abtriebsseite gesehen.
Zu erkennen sind nun die Seilspulen und das Zusammenspiel von Motor, Schneckengetriebe und dem Abtrieb.