---
layout: "image"
title: "Turm Version 3 (Detail des Lagers von der Seite)"
date: "2016-10-23T20:57:10"
picture: "IMG_20161021_161913a.jpg"
weight: "24"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Statik", "Kranausleger", "Hängebrücke", "schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44664
- /details28c5.html
imported:
- "2019"
_4images_image_id: "44664"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44664 -->
Version 3 = links (Bahn wird nach rechts laufen, einghängt in die 30er mit Loch unten)
Version 2 = rechts (Bahn läuft nach links, eingehängt in die oberen 30er mit Loch)

Eine weitere Verbesserung des Turmes in der Version 3 ist die Lagerung des oberen Auslegers.
Er wurde neu konstruiert und leichter, sowie schmaler gebaut.
Die Lagerung besteht nun aus 3 Scharnieren, die miteinander verstärkt wurden.

Zusätzlich wurde der untere Montagepunkt nach vorne in Richtung der Bahn verschoben (hier Richtung Bildmitte) und um einen 15er erhöht. (unteres Pfeilpaar)

Die oberen Montagepunkte der Scharniere wurden neu gestaltete und brachten einen weiteren Gewinn an Turmhöhe und Stabilität (oberes Pfeilpaar)

Sinn ist, dass die Anschlagpunkte der Schrägseile am oberen Ausleger weiter nach oben kommen und weiter nach vorne zur Bahn, sowie der Ausleger stabiler und leichter.