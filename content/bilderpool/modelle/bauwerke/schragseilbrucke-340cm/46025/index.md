---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 8)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-9.jpg"
weight: "49"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Elektromechanik", "Taktscheibe", "Minitaster"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46025
- /details7d34.html
imported:
- "2019"
_4images_image_id: "46025"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46025 -->
Detail der Taktgeber.
Zu sehen ist die Ansicht von oben.

Die zwei Taktscheiben sind mit der einen Spule fest verbunden.
Sie geben je einen Minitaster frei. (Im Bild ist links unten jener freigegeben).
Durch 4 Lampen in der richtigen Reihenfolge erscheint ein Lauflicht.