---
layout: "image"
title: "Maschinenhaus Bergstation"
date: 2020-08-30T16:23:22+02:00
picture: "Kabelchaos01.jpg"
weight: "119"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Von unten gelangt das Serielle Kabel in den Turm. Hier spreizt es sich auf in einer Kabelpeitsche. Nicht zu erkennen: sobald man das Dach des Maschinenhaus (Talstation) öffnet wird auch dieser Schaltraum mit Licht versorgt. So ist es möglich auch im Dunkeln Wartungsarbeiten vorzunehmen.