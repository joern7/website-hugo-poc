---
layout: "image"
title: "Zweiseil-Betrieb"
date: 2020-08-30T16:23:18+02:00
picture: "Seilrollen01.jpg"
weight: "122"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Nachdem klar war, dass ein umlaufendes Seil zum Antrieb der Gondel nicht funktioniert sind wir auf einen Zweiseil-Betrieb gekommen. Hierbei laufen die Spulen über ein Gleichlaufgetriebe gleich schnell in die gleiche Richtung.
Während die blaue Rolle Seil abgibt, nimmt die rote die gleiche Länge wieder auf. Das geht nur so lange, wie die Rollendurchmesser gleich groß sind. Aber nach einiger Zeit wird die blaue Rolle kleiner (da zu viel Seil abgegeben ist) und die rote größer im Durchmesser. Nun sind die Längen der abgegebenen und aufgenommen Strecke unterschiedlich und müssen ausgeglichen werden (rechte Darstellung).
Der Trick: im Gleichlaufgetriebe gibt es einen zweiten Motor. Dieser verdreht die Spulen gegeneinander. Damit wird eine Spule schneller (blau) und die andere langsamer (rot). Die Steuerung dieses Motors wird durch das Relais geregelt. Dieses über einen Reedkontakt aktiviert. Jener über einen Magneten an einer Steuerkette geführt. Und diese ist lose (mit Hysterese) am Spanngewicht der Seile befestigt.
Wenn also das Seil länger wird, senkt sich das Gewicht, nimmt die Kette mit, aktiviert den Reedkontakt, schaltet den Motor auf „Seilspannung erhöhen“. Dieser bremst blau und beschleunigt rot.
Ich habe die Spulen übrigens absichtlich so dimensioniert, dass möglichst viel nachgeregelt werden muss, denn das soll mit der Anlage gezeigt werden.
Außerdem habe ich bewusst keine TXT oder ftDuinos eingesetzt, da ich zeigen möchte, dass solche Aufgaben auch in den 70ern schon mit Schütz-Technik machbar waren. (Übrigens leichter und schneller, sicherer und günstiger als mit Controller.)