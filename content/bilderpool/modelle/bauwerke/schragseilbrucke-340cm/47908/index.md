---
layout: "image"
title: "Fernbedienung Abdeckung"
date: "2018-09-23T13:24:04"
picture: "Bedienung-Planung-Abdeckung.jpg"
weight: "86"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47908
- /detailsc762.html
imported:
- "2019"
_4images_image_id: "47908"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47908 -->
Hier die Entwicklung der Abdeckung mit Beschriftung dür die Fernbedienung. AUsgemessen und in Pappe geschnitten passt diese später auf die Schalter und kann beschriftet werden.

Gar nicht so einfach das Ding.