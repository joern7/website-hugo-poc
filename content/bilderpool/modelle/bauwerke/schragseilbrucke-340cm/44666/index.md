---
layout: "image"
title: "Turm Version 3 (Detail der Verbindung von oben)"
date: "2016-10-23T20:57:10"
picture: "IMG_20161016_144822a.jpg"
weight: "26"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["statik", "schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44666
- /details946b.html
imported:
- "2019"
_4images_image_id: "44666"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44666 -->
Teilemangel :-)
Leider sind uns die Teile ausgegangen - dachten wir, bis wir die letzte Kiste gefunden hatten - jetzt haben wir genug.

Um die geraden Doppelstatikteile zu erhalten haben wir die gerade Verbindung der Turmfundamente (unterer Bildrand) durch eine geschwungene Version ersetzt (oberer Bildrand). Damit erhalten wir die geraden 120er Statikteile um die Laufschiene der Hängebahn zu verlängern.
Zudem bekommt die Konstruktion mehr geschwungene Elemente und wird daruch ästhetischer.

Im Bild: der Turm (linker Bildrand Mitte) mit seinen zwei Füßen (rote Platten) und dem hinteren Widerlager (graue Platte). Von oben liegt der obere Ausleger nach hinten (=rechts) um und die Seile sind schlaff. Später wird die Bahn im Turm eingehängt und würde im Bild nach links aus dem Bild führen.