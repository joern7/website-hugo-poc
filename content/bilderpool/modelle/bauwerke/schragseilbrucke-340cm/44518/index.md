---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20160918_194653a.jpg"
weight: "16"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Seil", "Brücke", "Schrägseilbrücke", "Hängebrücke", "Seilwinde"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44518
- /details2a2b-2.html
imported:
- "2019"
_4images_image_id: "44518"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44518 -->
Lagerblock vor dem Einbau von unten gesehen.
Gut zu erkennen ist, dass später die Last über 7 Nocken in die Bodenplatte eingeleitet wird. Das sollte reichen.

Ziel ist es, 4 blockierbare Spulen auf kleinstem Raum unterzubringen und das ganze möglichst zu versteifen. Die komplette Last der Schrägseilbrücke wird später hier in die Bodenplatte eingeleitet. Die Seile verstellen die Biegung der Brücke exakt.