---
layout: "image"
title: "Selbstbau-Silberlinge"
date: 2020-08-30T16:23:36+02:00
picture: "Relais01.jpg"
weight: "108"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Selbstbau-Silberlinge in Großaufnahme. Einer der zentralen Technik-Highlights in diesem Bauwerk.
Rechts ist der Gleichrichter-Baustein. Er besteht aus zwei starken Brückengleichrichtern und zwei riesigen Kondensatoren. Diese glätten nicht nur die Gleichspannung, sondern versorgen die Anlage auch bei kurzen Lastspitzen.
Beide Kreise sind komplett galvanisch getrennt. Später laufen darüber fast 30 LEDs und die Motoren.
Versorgt werden sie mit dem Lichtanschluss (seitlich) des alten Trafos. In der Anlage selbst ist nur ein einziger Trafo eingebaut, der alles versorgt!
Links befindet sich der Relais-Baustein. Er besteht aus zwei (ebenfalls komplett galvanisch getrennten) Relais 6 X UM.
5 Kontakte sind nach außen geleitet. Der 6. Kontakt dient als gegenseitiger Sperrkontakt, der aber auch einen Override besitzt. Wenn ein  Relais über den Sperrkontakt angesteuert wird, kann das andere Relais nicht schalten – es sei denn man steuert es über den Override an.
Alternativ kann der Kontakt auch als Selbsthaltekontakt verwendet werden.
Das Kabelgewirr ist groß, denn es sind insgesamt 36 Buchsen verbaut. 
Hier im Antrieb der Gondel übernimmt das Relais die Steuerung der Seilspannung: da ich die Spannung mit einem Reedkontakt abgreife (jeweils für „Seilspannung zu hoch“ oder „zu niedrig“) könnte ich damit nur eine Phase schalten. Ich muss aber die Pole des Motors, der die Spannung nachlässt oder erhöht umpolen und eine Nullstellung (= „Seilspannung OK“) haben. Dies sind die ersten 2 Kontakte des Relais. Nun muss ich diese gegeneinander Sperren und die Kontakte trennen, damit kein Kurzschluss entsteht. Das sind die nächsten 2 Kontakte. Der 5. Kontakt schaltet dann eine LED, die in der Fernbedienung den Zustand signalisiert.
Beide Bausteine sind so erdacht, dass sie auch in anderen Modellen wieder eingesetzt werden können. Mir persönlch gefallen die grauen Kästchen zum Bau von „Silberlingen“.