---
layout: "image"
title: "Klapplicht"
date: 2020-08-30T16:23:03+02:00
picture: "Klapplicht.jpg"
weight: "133"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die LED Lampe (grüner Pfeil), die sich beim Schließen des Daches vom Maschinenhaus selbständig einklappt gut zu sehen. Sie liegt über dem „Kabel-Chaos“ und wird an ihrem Kabel, das mit einer Zugentlastung (weißes Seil) geflochten ist vom Dach hochgezogen. Sie ist so hell, dass man Fehler auch in totaler Dunkelheit beheben kann.
Außerdem im Bild: links unten der Schalter für Arturs Handlampe. (Kann auch bei geschlossenem Dach betätigt werden.)
Links oben der Block, welcher die Seile der Brücke spannt. Daran eine LED mit Störlichtkappe für die Seilbeleuchtung.
Direkt rechts daneben der alte Trafo, der die ganze Anlage versorgt. Und weiter rechts die Schalter für die Wartung der Motoren.