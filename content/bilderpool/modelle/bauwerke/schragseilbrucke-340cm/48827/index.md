---
layout: "image"
title: "Gondel und Details"
date: 2020-08-30T16:23:24+02:00
picture: "Gondel-und-Details.jpg"
weight: "117"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Gondel an der Fahrbahn von unten gesehen.
Die Fahrbahn selbst hängt an den Schrägseilen (grüne Pfeile). Unter ihr ist versteckt das Serielle Kabel  angebracht. Dieses versorgt die Bergstation mit Strom und übermittelt die Schaltsignale (u.a. „Gondel Ende Bergstation“) an die Talstation und weiter an die Fernbedienung. Die Stecker des Kabels sind an den Maschinenhäusern (Talstation „Hauptanschluss“) eingesteckt und werden abgezogen, wenn die Anlage transportiert werden muss. Es bleibt an der Fahrbahn, die in der Mitte zusammengelegt werden kann.
Die Gondel selbst läuft auf zwei mal drei Rollen. (Zu erkennen an den Enden des Alu-Trägers.) Das „Dach“ des Fahrwerks ist dazu da, um in den Stationen die Gabel zu bedienen und damit die Endposition zu bestimmen.
Gezogen wird die Gondel von zwei gegenläufigen Seilen (blauer und roter Pfeil, das Seil ist mit farbigen Strichen markiert). Zu sehen ist auch im Innern der Gondel, wie die Seile mit Ösen in den Achsen eingehängt sind. Zum Transport hängen wir diese aus und erhöhen manuell die Seilspannung immer weiter, bis die Spulen in der Talstation das ganze Seil aufgenommen haben.
Die „Stoßdämpfer“ sind nur Deko.