---
layout: "image"
title: "Traverse 3 Prototyp"
date: "2018-09-23T13:24:04"
picture: "traverse02.jpg"
weight: "73"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47895
- /detailsb074.html
imported:
- "2019"
_4images_image_id: "47895"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47895 -->
Verbesserte Version des Funktionsprototypen der Traverse mit Endabschaltung.

Nun befinden sich die Taster oberhalb der Gabel.
Das Gewicht der Gabel spielt also keine Rolle mehr um die Taster auszulösen.

Problem hierbei: fährt die Gondel weiter, so verspannt sich die Gabel sehr. Gefahr für die Konstruktion.
Und wenn die Fahrbahn leicht nach unten geht, reicht die Höhe nicht aus, um die Taster zu betätigen.

In den Tests ist diese Konstruktion daher verworfen worden. Außerdem ist die Aufhängung der Taster unelegant und wackelig.