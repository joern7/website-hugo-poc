---
layout: "image"
title: "Neue Seilführung"
date: "2018-09-23T13:24:04"
picture: "seilfhrung-neu.jpg"
weight: "68"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Powermotor"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47890
- /details4fee.html
imported:
- "2019"
_4images_image_id: "47890"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47890 -->
Die Powermotoren und einigen andere Bauten bedingten, dass ichdie Seilführung optimieren musste.
Dieses Foto zeigt die Anlage sozusagen von der Rückseite.

Das obere Seil (rot-weiß) geht links hoch zum obren Gerätehaus (siehe vorheriges Foto - dort käme es dann am linken Bildrand über die Rolle). Danach geht es über das Gewicht weiter in die Strecke, hinüber zum Turm 2 (Bergstation) um eine weitere Rolle, zurück zur Gondel. Wird an diesem Seil gezogen, fährt die Gondel Richtung Turm 2.

An der Gondel beginnt dann das zweite Seil (blau-weiß) und geht unterhalb der Fahrbahn (frei hängend, aber durch das Gewicht gespannt) unter dem oberen Gerätehaus zurück, schräg nach unten - und kommt hier im Foto von links herein. Wird am ihm gezogen fährt die Bahn Richtung Turm 1 "Tal".

Beide Seile werden nach unten geführt und dann auf die Spulen aufgerollt.

Jetzt versteht man auch, warum ich das Gleichlaufgetriebe benötige: wenn die Seile auf- und abgerollt werden, ergeben sich durch unterschiedliche Spulendurchmesser unterschiedliche Geschwindigkeiten.
Da die Spannung durch das Gewicht aufrecht gehalten wird, muss sich dieses nach oben und unten bewegen. Leider reicht die Länges des Schachtes bei Weitem nicht aus, um die Seillängenunterschiede auszugleichen. Daher muss der Seilspannungsmotor die Spulen gegenläufig laufen lassen, um das Seil zu verlängern oder zu verkürzen.
Und genau diese Steuerung wird mit den Reedkontakten ausgelöst.