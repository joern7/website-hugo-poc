---
layout: "image"
title: "Traverse 6 Vergleich"
date: "2018-09-23T13:24:04"
picture: "traverse-vergleich.jpg"
weight: "76"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Minitaster", "Endschlater"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47898
- /detailsc7e2.html
imported:
- "2019"
_4images_image_id: "47898"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47898 -->
HIer der Vergleich der alten Traverse (oben) und der neu entwickelten Version:

- zuverlässiger
- kein Anschlag / Blockade
- kein Konflikt mit dem Zugseil (im oberen Bild sieht man, wie das Seil nach oben weg gedrückt wird, da die Traverse mit einem Verbinder direkt auf der Laufschiene / Aluprofil aufgebracht ist. Das sorgt für Reibung, manchmal sogar für verklemmtes Seil
- Traverse jetzt Bestandteil des Turms
- - bessere Kabelführung
- - stabilere Bauweise
- - keine Demontage für Transport nötig (großer Vorteil)

Ich benötige übrigens 2 Minitaster, da der Eine den Motor stoppt und der andere die Meldelampe an der Fernbedienung auslöst und beide auf unterschiedlichen Stromkreisen laufen müssen.