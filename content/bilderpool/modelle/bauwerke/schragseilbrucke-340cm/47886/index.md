---
layout: "image"
title: "Reedkontakte für Seilspannung 2"
date: "2018-09-23T13:24:04"
picture: "reed02.jpg"
weight: "64"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Reedkontakt"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47886
- /details5552.html
imported:
- "2019"
_4images_image_id: "47886"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47886 -->
HIer noch einmal eine Nahaufnahme, an der man erkennt, wie die Reedkontakte mit HIlfe kleiner Kabelbinder an den Statikstreben befestigt sind. Das ist nicht nur ungenau zu Positionieren, sondern macht auch bei jedem Neuaufbau ein Problem, da sie wieder neu justiert werden müssen.

Die Folge (hier nicht erkennbar): einer der Kontakte ist gebrochen und funktioniert nicht mehr.
Zum Glück habe ich zwei Kontakte parasslel geschaltet. Daher lief die Anlage noch.
Problematisch wurde es erst, als ich mcih am Glaskörper schnitt und mir ein Kontakt nach dem anderen zerbröselte.