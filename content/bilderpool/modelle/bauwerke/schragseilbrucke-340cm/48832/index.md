---
layout: "image"
title: "Fahrtrichtungsgeber"
date: 2020-08-30T16:23:30+02:00
picture: "Fahrtrichtungsgeber03.jpg"
weight: "112"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Zur Anzeige der Fahrt und der Fahrtrichtung der Gondel sind auf der Fernbedienung 4 LED Lampen in einer Reihe angebracht. (Dazu jeweils eine Endkontrolle für den Bahnhof in der Station).
Um diese Lampen in der Reihenfolge 1-2-3-4 oder rückwärts blinken zu lassen ist dieser Fahrtrichtungsgeber zuständig.
Das Rad23 trägt dazu zwei Neodym-Magnete, die jeweils die vier Reedkontakte (schwarz) aktivieren. Und zwar in der entsprechenden Reihenfolge der Lampen je nach Drehrichtung. Die Achse selbst ist die Verlängerung einer der Spulen.
Das ganze ist außerdem entsprechend abgeschrankt. (Deko)
Links im Bild ist ein Taster zu sehen. Sobald man das Dach des Maschinenraumes geöffnet hat wird die Wartungsbeleuchtung eingeschaltet wie bei einem Kühlschrank. Das ermöglicht auch Reparaturen mitten in der Nacht. (Eine Lampe sieht mal als hellen Fleck links neben dem Rad23)
Ebenfalls zu sehen sind die zwei blauen Lampen rechts oben in der Ecke. Das sind die Anzeigen zur Wartung der Motoren. Zur Fehlersuche können diese deaktiviert werden und deren Betrieb mir nur mit diesen blauen Lampen signalisiert.