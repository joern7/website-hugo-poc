---
layout: "image"
title: "Gabel / Endanschlag"
date: 2020-08-30T16:23:00+02:00
picture: "Gabel.jpg"
weight: "135"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Am Turm der Talstation sind einige Datails zu sehen:
Die Fahrbahn wird später in die Achsen 90° (gelber Pfeil) eingeführt (sie käme von unten ins Bild hinein).
Wenn die Gondel dann vom Bergturm kommt, fährt sie mit ihrem Fahrwerk unter die „Gabel“. (Das ist die Achse mit den zwei Rad23 am Ende). Diese wird angehoben und betätigt die beiden Minitaster (grüne Pfeile). Diese wiederum halten den Fahrmotor an und geben Signal „Ankunft Talstation“ auf der Fernbedienung.
Man beachte auch die Kabelführung – sie sind kaum zusehen. Da lege ich Wert drauf.
Die Blaue Feder (Stoßdämpfer) sorgt dafür, dass der Anpressdruck der Gabel auf die Taster groß genug ist, um sicher auszulösen.
Die gleiche Technik ist auch in der Bergstation verbaut.
Der Haken (unter dem rechten grünen Pfeil) ist einer jener Schrägseile, an dem die Fahrbahn hängt.