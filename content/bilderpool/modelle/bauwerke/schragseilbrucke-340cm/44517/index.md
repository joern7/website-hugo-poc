---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20160918_194636a.jpg"
weight: "15"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Hängebrücke", "Schrägseilbrücke", "Seilzug"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44517
- /details8ee2.html
imported:
- "2019"
_4images_image_id: "44517"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44517 -->
Lagerblock vor dem Einbau von vorne (aus dem Blickwinkel aus dem die Seile ankommen) gesehen.
Ziel ist es, 4 blockierbare Spulen auf kleinstem Raum unterzubringen und das ganze möglichst zu versteifen. Die komplette Last der Schrägseilbrücke wird später hier in die Bodenplatte eingeleitet. Die Seile verstellen die Biegung der Brücke exakt.