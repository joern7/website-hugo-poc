---
layout: "image"
title: "Brückentheorie 2"
date: 2020-08-30T16:23:43+02:00
picture: "Kleines-Modell.png"
weight: "102"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

An diesem kleinen Modell des Modells erkennt man wie die Brücke als Bauwerk funktioniert.
Die zwei „Kräne“ halten die Fahrbahn wie ihren Ausleger. Alle Kräfte werden über die Türme in den Boden geleitet, die Gegengewichte (rot) tragen die Fahrbahn.
Trennt man die Fahrbahn in der Mitte (mittleres Bild), so passiert – nichts! Die Kräne sind davon unbeeindruckt und halten die Ausleger weiter.
Der Gegenversuch (Bild unten) gelingt ebenfalls: löst man die Loselager der Fahrbahn an den Türmen, schwebt diese ebenfalls. Die Zugkräfte, die bei einem einzelnen Kran die Fahrbahn gegen das Loselager am Turm ziehen heben sich nun auf. Die Fahrbahn ist nun ausschließlich auf Zug belastet.