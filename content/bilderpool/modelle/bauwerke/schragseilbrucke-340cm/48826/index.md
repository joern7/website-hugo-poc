---
layout: "image"
title: "Hauptanschluss"
date: 2020-08-30T16:23:23+02:00
picture: "Hauptanschluss01.jpg"
weight: "118"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Hauptsteuerungsanschluss am Maschinenraum der Talstation ist ein technisches Wunderwerk, das gut versteckt ist.
Es enthält (oben) einen Centronics-Stecker. Dieser versorgt die Kabel-Fernbedienung mit Strom und allen Signalen. Hinter der Verkleidung fächert er sich auf in einer Kabelpeitsche. Das verursacht innen jenes „Kabelchaos“, das in Wirklichkeit gut geordnet und beschriftet ist.
Darunter befindet sich der Anschluss des Seriellen Kabels. Dieses wird unter die Fahrbahn geführt und versorgt die Bergstation mit den Schaltsignalen und Strom für die Beleuchtung.
Die Steckdose für die Handlampe ist für Artur vorgesehen, der mir beim Basteln Licht gibt.
Links neben dem Feld ist der Gleichrichter. Einer meiner „Selbstbau-Silberlinge“. Darin befinden sich zwei große Brückengleichrichter und fette Kondensatoren. (Deren Kapazität reicht aus, um die Warnbeleuchtung fast 60 Sekunden nachblinken zu lassen.) Sie glätten die Gleichspannung und die Motoren schnurren wunderbar. So ziehe ich aus dem alten Trafo aus dem Anschluss für Licht fast meine ganze Leistung.
Noch weiter links (unscharf) ist der zweite Selbstbau-Silberling, der Relaisstein.
Darüber zu erkennen der Lichtschalter für Artur.
Rechts am Rand noch das 20er Ritzel für den Antrieb.
Das Dach des Maschinenhauses lässt sich wie eine Motorhaube hochklappen.