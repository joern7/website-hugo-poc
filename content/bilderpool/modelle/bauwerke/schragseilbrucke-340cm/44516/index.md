---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20160918_194616a.jpg"
weight: "14"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Schrägseilbrücke", "Hängebrücke", "Brücke", "Lagerblock", "Seilwinde"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44516
- /details627e-2.html
imported:
- "2019"
_4images_image_id: "44516"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44516 -->
Lagerblock vor dem Einbau von der Seite gesehen.
Ziel ist es, 4 blockierbare Spulen auf kleinstem Raum unterzubringen und das ganze möglichst zu versteifen. Die komplette Last der Schrägseilbrücke wird später hier in die Bodenplatte eingeleitet. Die Seile verstellen die Biegung der Brücke exakt.