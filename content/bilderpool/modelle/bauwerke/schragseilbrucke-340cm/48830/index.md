---
layout: "image"
title: "Unterseite Fernbedienung "
date: 2020-08-30T16:23:27+02:00
picture: "Fernbedienung04.jpg"
weight: "114"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Auch von unten ist die Fernbedienung verkleidet. Die Beschriftungen sind mit Bedacht gewählt.
Gut zu sehen ist rechts die Ausführung des Telefonkabels. Es ist leider sehr steif. In Zukunft werde ich versuchen ein anderes zu verwenden.