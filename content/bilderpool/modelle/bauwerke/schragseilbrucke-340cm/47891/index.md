---
layout: "image"
title: "Seilfärben"
date: "2018-09-23T13:24:04"
picture: "seilfrben.jpg"
weight: "69"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47891
- /details9d83.html
imported:
- "2019"
_4images_image_id: "47891"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47891 -->
Bevor mich jemand fragt, wo man gestrichelte Seile her bekommt: Selbermachen!

Mit dieser einfachen Papplehre und einem Stift habe ich die Seile gleichmäßig in blau-weiß und rot-weiß gefärbt.

Sinn der Sache?
- man sieht die Bewegung der Seile viel besser.
- man kann erkennen, dass es sich nicht um ein umlaufendes Seil handelt, sondern um zwei Seile, die sich unterschiedlich bewegen.

Auch im Bild: die Seilspule mit dem Z10, das im vorigen Bild die rechte Spule antreibt.