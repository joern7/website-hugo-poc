---
layout: "image"
title: "Hängebahn Zugtest"
date: "2018-09-17T19:41:06"
picture: "traverse-zugtest.jpg"
weight: "55"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47877
- /detailsb70a-2.html
imported:
- "2019"
_4images_image_id: "47877"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-17T19:41:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47877 -->
An der Brücke soll ein Fahrzeug hängen. Dieses hängt an einer Traverse.

Natürlich haben Rollen und Seile einen gewissen Laufwiderstand.
Das Gewicht zum Spannen des Seils muss also deutlich kräftiger ziehen als diese Widerstände sind.

Um einen Eindruck der Kräfte zu erhalten habe ich versucht, diese zu messen.
Natürlich mit der ft Federwaage / Kraftmesser. (Classic eben!)
Hier sind es in Fahrt ca. 15 Pond = 15 Gramm
Bei Anfahrt gingen die Werte auf bis zu 40 Pond hoch.

Das Gewicht sollte also mindestens 1,5 fache Sicherheit = 60 Gramm schwer sein.
Da es später an einem Flaschenzug 1:2 hängt wird das endgültige Gewicht 120 Gramm betragen.