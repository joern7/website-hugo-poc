---
layout: "image"
title: "Traverse 1"
date: "2018-09-23T13:24:04"
picture: "traverse00.jpg"
weight: "71"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Minitaster", "Endschalter"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47893
- /details1c1f.html
imported:
- "2019"
_4images_image_id: "47893"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47893 -->
Die Traverse mit den Minitastern zur Detektion "Gondel Ankunft Turm".

Die alte Version, die ich auch 2017 auf der Convention zeigte sieht nicht nur doof aus, sondern ist auch unpraktisch.
Nicht immer werden die Schalter gleichmäßig von den schwarzen Statik 15ern ausgelöst. So kann es passieren, dass die Gondel anstößt und mechanisch Probleme macht, noch bevor der Motor stoppt. Das war nicht schlimm, so lange ich die alten Klassik-Motoren benutzte. Aber mit denneuen Powermotoren .... naja, die haben eben richtig "wumms".
Es konnte auch passieren, dass die Gondel am Turm war, aber am Bedienpult (siehe spätere Fotos) nicht angezeigt wurde.

Mit anderen Worten: Fehlkonstruktion.

Anm.: das dicke Kabel, das hier durchhängt ist die Signalleitung, die unter der Fahrbahn hängt und die Traverse und andere Dinge am Turm 2 versorgt.