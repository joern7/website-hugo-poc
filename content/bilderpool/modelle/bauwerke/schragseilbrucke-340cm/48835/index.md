---
layout: "image"
title: "Artur"
date: 2020-08-30T16:23:34+02:00
picture: "Artur.jpg"
weight: "109"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Natürlich darf auch Deko nicht fehlen.
Artur hilft mir mit seiner Handlampe.
Das Besondere: die Leitung ist so lang, dass sie auch zur Bergstation reicht. Der Anschluss befindet sich in der Talstation am Hauptanschlussfeld. Ein Schalter über den Relais ist dazu versteckt.