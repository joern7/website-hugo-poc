---
layout: "image"
title: "Brückentheorie"
date: 2020-08-30T16:23:46+02:00
picture: "Brueckentheorie01.png"
weight: "101"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Schaubild zeigt die Kräfte und Entwicklung von einer einfachen Schrägseilbrücke zu dem Modell von uns.
Es besteht aus zwei gleichen Teilen, die sich in der Fahrbahnmitte treffen.
Das Gegengewicht wird kleiner, wenn der Oberbau des Turms sein Gewicht mit hinzugibt.
Letztlich handelt es sich um einen Kran.
In unserem Falle sind die Seile noch in besonderer Weise auf den Oberbau verteilt.
Alle Kräfte werden so in den Turm eingeleitet. Sie bestehen aus einem senkrechten Anteil und einer Schubkraft, die von der Fahrbahnmitte auf den Turm wirkt. Wenn sich jedoch die zwei Kräne gegenüberstehen (also die Brücke mit beiden Türmen fertig ist), dann verschwinden diese Schubkräfte, da sich die beiden Hälften der Fahrbahn gegenseitig in die Mitte ziehen.
Zu diesem Thema habe ich übrigens in der ftPedia 3/2020 einen Artikel geschrieben.