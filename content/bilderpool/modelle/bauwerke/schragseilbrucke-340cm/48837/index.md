---
layout: "image"
title: "Unter der Haube"
date: 2020-08-30T16:23:37+02:00
picture: "Wartung02.jpg"
weight: "107"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick unter die geöffnete Maschinenhausabdeckung zeigt, was dahinter steckt. Viel Technik.
Die gelben Lampen an der Abdeckung blinken als Warnung (asynchron, da es Blink-LEDs sind, das spart das Blinkmodul).
Die Beleuchtung hat sich aufgeklappt (weiße LED in der Mitte)
In diesem Maschinenhaus befindet sich zum einen der Trafo (tief unter den Kabeln), der Antrieb mit Getriebe, die Spulen, die Energieversorgung (Silberlinge), Fahrtrichtungsanzeiger und Regelung. Herausgeführt sind die Anschlüsse für Fernbedienung und Bergstation. Ebenso der Block, welcher die Seile der Bücke spannt.
Die Philosophie war, dass alle Technik in der Talstation verstaut sein muss. Die Bergstation sollte „passiv“ sein.
Details folgen.