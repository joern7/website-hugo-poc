---
layout: "image"
title: "Seilspulen"
date: 2020-08-30T16:23:40+02:00
picture: "Spulen.jpg"
weight: "105"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Bei geöffneter Haube des Maschinenhauses lassen sich unter den Powermotoren (querliegend) die eigentlichen Seilspulen des Antriebs finden. Die Seile kommen parallel zum grünen Pfeil von unten ins Bild. (Sie kommen von der Umlenkung in Höhe der Fahrbahn).
Unter dem unteren Motor ist eine weitere Umlenkung. Dann aber die rote Spule (Spitze grüner Pfeil) und hinter dem oberen Motor ist die blaue Spule zu sehen.
Die Seile sind absichtlich farbig markiert, damit ich zeigen kann, dass es kein umlaufendes Seil ist, sondern zwei gegenläufige.
Ebenfalls zu erkennen: die Achse der roten Spule ist nach rechts verlängert, um über das Ritzel 10 das 40er anzutreiben – dies ist der Geber für die Fahrtrichtungsanzeiger.