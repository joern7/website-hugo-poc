---
layout: "image"
title: "Schönheits-OP (Teil 1) vorher"
date: "2017-07-29T19:32:19"
picture: "hintere_Seile_vertauscht01.jpg"
weight: "52"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["Schrägseilbrücke", "Knoten", "Seil"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46100
- /details9f46.html
imported:
- "2019"
_4images_image_id: "46100"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-29T19:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46100 -->
Die Seile zum Abspannen der Hängebrücke werden hinter dem Turm (im Bild rechts) nach unten zu einem Block geführt (siehe http://www.ftcommunity.de/details.php?image_id=44516 und folgende Bilder).

Leider sind bei unserern mehreren Veränderungen des Turm und der Bahn die Seile anders als geplant abgespannt worden. Daher scheinen sie verdreht zu sein.
Eine Schönheits Operation soll das beheben.

Problem: die Blöcke sind sehr eng und stabilgebaut und die Seile waren an den Achsen mit Hilfe von Knoten an den verschraubten Rädern fixiert. Kleine Finger, viel Geduld und Know-How waren gefragt.