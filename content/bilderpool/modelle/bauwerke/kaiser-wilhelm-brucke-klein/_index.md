---
layout: "overview"
title: "Kaiser-Wilhelm-Brücke (klein)"
date: 2020-02-22T08:39:05+01:00
legacy_id:
- /php/categories/2269
- /categories6f61.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2269 --> 
Die "kleine Schwester" des Modells der Wilhelmshavener Kaiser-Wilhelm-Brücke von stephan (http://www.ftcommunity.de/categories.php?cat_id=1227).