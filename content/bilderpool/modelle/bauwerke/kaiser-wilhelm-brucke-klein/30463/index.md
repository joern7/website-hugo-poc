---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Seitenansicht)"
date: "2011-04-16T18:30:46"
picture: "kaiserwilhelmbrueckewilhelmshavenklein2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30463
- /detailsf9bf-2.html
imported:
- "2019"
_4images_image_id: "30463"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30463 -->
Wir haben versucht, uns eng an der Originalbrücke zu orientieren, die Stephan so eindrucksvoll dokumentiert hat (http://www.ftcommunity.de/details.php?image_id=13642#col3).