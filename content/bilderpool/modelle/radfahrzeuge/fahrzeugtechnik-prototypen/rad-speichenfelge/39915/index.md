---
layout: "image"
title: "Rad im Alufelgen-Look"
date: "2014-12-09T21:34:16"
picture: "Rad_klein.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: ["Rad", "Alufelge"]
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39915
- /details0153-2.html
imported:
- "2019"
_4images_image_id: "39915"
_4images_cat_id: "2998"
_4images_user_id: "2321"
_4images_image_date: "2014-12-09T21:34:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39915 -->
Rad im Alufelgen-Look mit freier Durchsicht