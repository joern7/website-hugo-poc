---
layout: "image"
title: "Rad mit einem Anstz für die Lenkung"
date: "2014-12-13T19:42:17"
picture: "radimalufelgenlook3.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39921
- /details2d1a-3.html
imported:
- "2019"
_4images_image_id: "39921"
_4images_cat_id: "2998"
_4images_user_id: "2321"
_4images_image_date: "2014-12-13T19:42:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39921 -->
Man sieht es aus dieser Perspektive vielleicht nicht gut, aber wie ich ursprünglich beabsichtigt hatte, verschwindet der Gelenkwürfel ganz im Rad. Dadurch kommt der Drehpunkt ungefähr auf dem Standpunkt zu liegen und das Rad schert beim Lenken nicht mehr aus. Eine erste Idee für die Spurstange sieht man auch schon.