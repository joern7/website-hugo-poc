---
layout: "image"
title: "Einrasten des Zahnrades"
date: "2014-12-13T19:42:17"
picture: "radimalufelgenlook1.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39919
- /detailsab92.html
imported:
- "2019"
_4images_image_id: "39919"
_4images_cat_id: "2998"
_4images_user_id: "2321"
_4images_image_date: "2014-12-13T19:42:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39919 -->
Das Ritzel hält ziemlich gut an der Sternlasche, aber das Einrasten ist etwas fummelig. Am Besten geht es, wenn man erst eine Zacke zwischen zwei Zähnen fixiert und dann das Ritzel so hindrückt ("aus dem Bild heraus"), dass die Nachbarzacke hinter dem Zahn, den sie berührt einrastet. Dann geht der Rest fast automatisch.