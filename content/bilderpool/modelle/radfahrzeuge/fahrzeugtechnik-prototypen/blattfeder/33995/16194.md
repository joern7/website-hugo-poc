---
layout: "comment"
hidden: true
title: "16194"
date: "2012-01-23T18:11:26"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Ludger,
leider ist der Aufbau der Blattfedern nicht korrekt. Sie müssen in der Achsenmitte gegen Verdrehen gebündelt (verspannt) werden und nach beiden Enden hin aneinander frei gleiten können. Das Verstiften an den Enden der Streben 60 sollte dann wegfallen können. Das jeweils ein Ende der Federpakete frei gleiten kann ist ok. Schau dazu mal bei mir unter http://www.ftcommunity.de/details.php?image_id=14157 und Folgebild einer ersten Studie meinerseits zum Thema Kfz-Blattfeder.
Gruß, Udo2