---
layout: "comment"
hidden: true
title: "16208"
date: "2012-01-25T12:08:11"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Zu ergänzen wäre noch, daß die Achsenführung über die Enden der Blattfedern erfolgt. Diese sind in der Regel an einer Seite als horizontales Gelenklager und am anderen als Loslager wegen der Längenänderung durch das Einfedern ausgelegt. Brücken haben ja an jedem Ausleger z.B. wegen der temperaturbedingten Längenänderung auch je ein Fest- und ein Loslager.