---
layout: "comment"
hidden: true
title: "16197"
date: "2012-01-24T12:58:39"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Super, das schaut optisch schon viel besser aus und ist auch funktional näher am Original!

Aber die beiden senkrechten schwarzen Achsen dienen noch nur dazu, die kürzeren Streben am Verdrehen zu hindern, oder? Leider macht das den mittleren Bereich der Blattfeder immer noch unnötig steif! Ersetze die schwarzen Achsen doch durch kurze Achsen und verzichte komplett auf die zusätzliche Befestigung (alle Bausteine 30x15 und 7,5 weg)! Dann wär's aus meiner Sicht optimal.

Gruß, Thomas