---
layout: "image"
title: "andere Konstruktion"
date: "2012-01-24T11:51:12"
picture: "Blattfeder_6.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34001
- /details51c8.html
imported:
- "2019"
_4images_image_id: "34001"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-24T11:51:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34001 -->
Hier habe ich einige Änderungen vorgenommen.
Die Befestigung der einzelnen "Federn" ist nur noch in der Mitte vorhanden.
Auf die Längslänker kann ich hier verzichten.

Übrigens.....
So wie ich das lese ist ein sanftes aneinandergleiten gar nicht erwünscht.
Denn so kann man auf Schwingungsdämpfer verzichten.
http://www.kfz-tech.de/Blattfeder.htm

Gruß ludger