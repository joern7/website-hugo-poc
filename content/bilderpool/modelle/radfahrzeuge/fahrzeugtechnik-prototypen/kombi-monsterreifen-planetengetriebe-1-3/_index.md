---
layout: "overview"
title: "Kombi Monsterreifen Planetengetriebe 1:3"
date: 2020-02-22T07:53:03+01:00
legacy_id:
- /php/categories/2832
- /categories48e2.html
- /categoriesb89a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2832 --> 
die Notwendigkeit, ein Planetengetriebe für eine Untersetzung einzubauen, kam durch mein MonsterTruck Projekt, bei dem ich Probleme mit dem Antrieb hatte: http://ftcommunity.de/details.php?image_id=38075
Der entscheidende Hinweis zur Verbesserung kam von Stefan: "...Du musst *spät* untersetzen..."

Es wurden ja eigentlich schon Lösungen für dieses Thema gepostet.
Ich möchte aber trotzdem meine Lösung vorstellen, die ein bisschen anderes aufgebaut ist und ein paar Vorteile hat:
- fällt eingebaut im Modell fast nicht auf
- kann man komplett mit Original ft Teilen aufbauen ohne Schnitzereien
- extrem platzsparend
- trotzdem stabil, da die Reifen nach wie vor innen und außen von 2 Drehscheiben gehalten werden