---
layout: "image"
title: "Einzelteile"
date: "2014-01-17T15:27:08"
picture: "Einzelteile.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38080
- /details49ce.html
imported:
- "2019"
_4images_image_id: "38080"
_4images_cat_id: "2832"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T15:27:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38080 -->
Um die Einzelteile alle zu sehen, habe ich das Innenzahnrad Z30 abgenommen.
Das Z30 ist mit 6 Stück Statik Strebenadaptern auf der Drehscheibe der Monsterreifen-Radnabe fixiert.
Das hält erstaunlich gut. Wenn es doch abgehen sollte, dann werde ich die mit den schwarzen Zapfen (siehe Pfeil) austauschen. Allerdings müsste ich dann doch ein bisschen feilen.
Die Drehscheiben der Radnabe (innen und außen) haben übrigens eine Freilaufnabe. Die ist bei diesem Prinzip notwendig, da ja die zentrale Achse 3 mal schneller dreht als das Rad selbst (außerdem in entgegengesetzer Richtung)
Auf diesem Bild sieht man auch das innere Z10. Naja, die Photos habe ich bei der schon optimierten Variante gemacht. Das Z10 hat einen Metallkern (TST). Das könnte man aber mit einem normalen Z10 mit Spannzange tauschen (für ft Puristen), aber dann wird der Aufbau etwas breiter.
Genauso verhält es sich mit den Klemmbuchsen. Die ft Teile habe ich mit Metall-Klemmringen mit Madenschraube getauscht.