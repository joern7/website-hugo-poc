---
layout: "image"
title: "Radnabe"
date: "2014-01-17T15:27:08"
picture: "Radnabe.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38081
- /detailse021-3.html
imported:
- "2019"
_4images_image_id: "38081"
_4images_cat_id: "2832"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T15:27:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38081 -->
der Vollständigkeit halber noch die Felge für die Monsterreifen.
2 Drehscheiben, 2 Freilaufnaben, eine Achsfixierung. Die Bausteine 15x15 dienen als Abstandshalter zwischen den Drehscheiben; sind aber nicht unbedingt notwendig.
Weiterhin sieht man, daß die Strebenadapter die Nut der Drehscheiben nicht unterbrechen.
Dadurch hält die Gummilippe der Reifen immer noch sehr gut in der Nut.