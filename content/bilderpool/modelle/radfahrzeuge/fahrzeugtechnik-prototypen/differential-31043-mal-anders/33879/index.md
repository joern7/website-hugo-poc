---
layout: "image"
title: "mit Antrieb"
date: "2012-01-09T16:34:50"
picture: "DSCN4237.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/33879
- /details7614-2.html
imported:
- "2019"
_4images_image_id: "33879"
_4images_cat_id: "2506"
_4images_user_id: "184"
_4images_image_date: "2012-01-09T16:34:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33879 -->
einen Motor kann man natürlich auch direkt anbauen.
Gedacht hatte ich es aber als Kettenantrieb.
So kann man auch größere Kräfte über die Z40 auf das Diff übertragen.
Fals Platzmangel bestehen sollte kann man die Z40 auch gegen Z30 tauschen.