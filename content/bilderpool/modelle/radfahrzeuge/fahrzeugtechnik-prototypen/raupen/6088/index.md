---
layout: "image"
title: "Raupenfahrwerk (Bauweise mit Z30)"
date: "2006-04-14T18:57:50"
picture: "DSCN0697.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6088
- /detailse409.html
imported:
- "2019"
_4images_image_id: "6088"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-14T18:57:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6088 -->
Seitenansicht.
Die roten Bauteine 5 und 7,5 dienen zum Ausgleich der Kettenlänge. Leider passt das 15´ner Raster hier nicht genau.