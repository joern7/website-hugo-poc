---
layout: "image"
title: "Raupenfahrwerk (Bauweise mit Z30) 2.Teil"
date: "2006-04-14T18:57:50"
picture: "DSCN0703.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6091
- /detailsbc71-2.html
imported:
- "2019"
_4images_image_id: "6091"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-14T18:57:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6091 -->
Jetzt aber richtig.
Hier mal ein paar Daten:
Gesamtbreite ca. 27 cm, Breite zwischen den Raupen ca. 15 cm, Raupenbreite 6 cm,
Jede Kette besteht aus 46 Einzelgliedern und hat eine Länge von 69 cm, Radstand (Vorder-Hinterachse) ist 27 cm.
Trotz der sehr großen Auflagefläche (Gleitfläche auf den Ketten) von 24 cm gleitet das Fahrwerk erstaunlich leicht dahin.