---
layout: "image"
title: "Hier eine Variante mit Z30"
date: "2006-04-05T11:41:21"
picture: "DSCN0690.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6021
- /detailsbdc1.html
imported:
- "2019"
_4images_image_id: "6021"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-05T11:41:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6021 -->
Ihr fragt euch bestimmt, was hat er denn da zwischen die Zahnräder gebaut??
Das muss so sein weil das Fahrwerk sonst ein wenig "hoppelt".
Jedesmal wenn die Zahnräder über eine Metallachse laufen wird das Fahrwerk etwas angehoben. Das habe ich mit der Konstruktion verhindert.