---
layout: "image"
title: "raupen_1"
date: "2005-06-19T16:06:51"
picture: "raupen_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/4476
- /detailsc077.html
imported:
- "2019"
_4images_image_id: "4476"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:06:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4476 -->
Am moment bescheftige ich mit den bau der DEMAG CC12000. Dies schon zum driten mahl, aber jetst im maßstab 1:25.

Länge der Raupen: 68cm (16,50m)
Breite der Raupen: 9cm (15,75m)
Breite über den Raupen: 65,5cm (2,40m)