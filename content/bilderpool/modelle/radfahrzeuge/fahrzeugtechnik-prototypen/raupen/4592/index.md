---
layout: "image"
title: "Raupe-Vorschlag"
date: "2005-08-14T22:40:16"
picture: "Raupe02.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Rolltor", "Raupe"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4592
- /detailsabdd.html
imported:
- "2019"
_4images_image_id: "4592"
_4images_cat_id: "367"
_4images_user_id: "4"
_4images_image_date: "2005-08-14T22:40:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4592 -->
Eigentlich gedacht als Vorschlag für ein Rolltor eines Feuerwehr-Gerätehauses, aber als Raupe geht sowas natürlich auch.