---
layout: "image"
title: "raupen_12"
date: "2005-06-19T16:07:01"
picture: "raupen_12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/4487
- /details813d-2.html
imported:
- "2019"
_4images_image_id: "4487"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4487 -->
Für meine Demag CC12000 brauch ich für eine Raupe:

31982 x 148 Federnocken
36263 x 296 Kettenglied
36248 x 148 Rastkettenglied
37237 x 148 Baustein 5
37468 x 148 Baustein 7,5
38245 x 74  Bauplatte 15x90

Macht zusamen 962 einselteilen.
Für zwei Raupen also 1924 einselteilen!!!!