---
layout: "image"
title: "Raupenfahrwerk (Bauweise mit Z30) 2.Teil"
date: "2006-04-14T18:57:50"
picture: "DSCN0708.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6095
- /detailsed34-2.html
imported:
- "2019"
_4images_image_id: "6095"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-14T18:57:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6095 -->
Und hier das lange Raupenband, 69 cm.