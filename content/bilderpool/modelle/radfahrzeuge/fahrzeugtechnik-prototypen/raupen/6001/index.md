---
layout: "image"
title: "Ansicht (Detail)"
date: "2006-04-01T12:38:21"
picture: "DSCN0686.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6001
- /detailsb8be.html
imported:
- "2019"
_4images_image_id: "6001"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-01T12:38:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6001 -->
hier eine Variante mit Z40