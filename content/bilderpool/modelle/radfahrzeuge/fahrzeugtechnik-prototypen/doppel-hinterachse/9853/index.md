---
layout: "image"
title: "Hinterachse04.JPG"
date: "2007-03-30T17:11:37"
picture: "Hinterachse04.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9853
- /detailsa839.html
imported:
- "2019"
_4images_image_id: "9853"
_4images_cat_id: "889"
_4images_user_id: "4"
_4images_image_date: "2007-03-30T17:11:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9853 -->
Der Träger für die Räder einer Seite, von unten. Er wird mit drei Federnocken mit der Bauplatte 30x90 verbunden.