---
layout: "image"
title: "Hinterachse05 (klein).JPG"
date: "2007-05-02T15:33:01"
picture: "Hinterachse05_klein.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/10272
- /detailsdad7.html
imported:
- "2019"
_4images_image_id: "10272"
_4images_cat_id: "889"
_4images_user_id: "4"
_4images_image_date: "2007-05-02T15:33:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10272 -->
Das ist das gleiche Prinzip, nur eine Nummer kleiner (Reifen 50).