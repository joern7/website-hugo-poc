---
layout: "image"
title: "Kipper"
date: "2003-10-08T17:44:14"
picture: "tiefkipper.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/1807
- /details4f14.html
imported:
- "2019"
_4images_image_id: "1807"
_4images_cat_id: "185"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1807 -->
