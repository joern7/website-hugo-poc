---
layout: "image"
title: "Linkskurve"
date: "2005-10-26T15:33:06"
picture: "Zwangslenkung_-_6.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5122
- /details0565.html
imported:
- "2019"
_4images_image_id: "5122"
_4images_cat_id: "185"
_4images_user_id: "9"
_4images_image_date: "2005-10-26T15:33:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5122 -->
Da ist auch noch mehr drin, was den Lenkeinschlag angeht. Den Kurvenradius der (ebenfalls zwangsgelenkten) Zugmaschine macht der Hänger allerdings nicht mit, man darf über eine Begrenzung des Lenkeinschlags nachdenken. 

Bisher merkt man den maximalen Lenkeinschlag daran, dass die Reifen ans Chassis schlagen. Aber hey, es ist ft, noch nichtmal computergesteuert, da darf man das :-)