---
layout: "image"
title: "20050422 Fischertechnik Ackerschlepper mit Antrieb 09"
date: "2005-04-25T20:35:22"
picture: "20050422_Fischertechnik_Ackerschlepper_mit_Antrieb_09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/4086
- /details04ff.html
imported:
- "2019"
_4images_image_id: "4086"
_4images_cat_id: "349"
_4images_user_id: "5"
_4images_image_date: "2005-04-25T20:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4086 -->
