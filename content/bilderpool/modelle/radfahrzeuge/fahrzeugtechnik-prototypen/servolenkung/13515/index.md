---
layout: "image"
title: "Spannung"
date: "2008-02-03T20:45:25"
picture: "spannung.jpg"
weight: "5"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- /php/details/13515
- /detailse53d-2.html
imported:
- "2019"
_4images_image_id: "13515"
_4images_cat_id: "1238"
_4images_user_id: "708"
_4images_image_date: "2008-02-03T20:45:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13515 -->
Damit der Motor einem nicht das Lenkrad aus der Hand reisst, hab ich die Spannung verringert. Man könnte den Motor auch mit einem Getriebe verlangsamen, aber dann wäre die Lenkung bei einem Strom- oder Motorausfall nicht mehr von Hand bedienbar (zu schwergängig).