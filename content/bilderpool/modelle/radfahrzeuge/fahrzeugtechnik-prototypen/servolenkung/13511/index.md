---
layout: "image"
title: "Gesamtansicht"
date: "2008-02-03T20:45:24"
picture: "gesamt.jpg"
weight: "1"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- /php/details/13511
- /details2b11.html
imported:
- "2019"
_4images_image_id: "13511"
_4images_cat_id: "1238"
_4images_user_id: "708"
_4images_image_date: "2008-02-03T20:45:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13511 -->
Servolenkung mit Standardbauteilen ohne Elektronik. Man beachte, dass die obere Achse nicht durchgehend ist, sondern aus zwei Achsen besteht (links Achse mit Lenkrad, rechts Motor). Das kann man aber besser auf der Ansicht von oben (siehe Bild) erkennen.