---
layout: "image"
title: "Sperrbares Zentraldifferenzial"
date: "2007-02-25T18:59:44"
picture: "Sperrbares_Zentraldifferenzial1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9148
- /details18a3.html
imported:
- "2019"
_4images_image_id: "9148"
_4images_cat_id: "844"
_4images_user_id: "456"
_4images_image_date: "2007-02-25T18:59:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9148 -->
Das ist ein sperrbares Zentraldifferenzial. Es wird mit einem Mini-Motor gesperrt.