---
layout: "image"
title: "Sperrbares Zentraldifferenzial"
date: "2007-02-25T18:59:44"
picture: "Sperrbares_Zentraldifferenzial3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9150
- /details7612.html
imported:
- "2019"
_4images_image_id: "9150"
_4images_cat_id: "844"
_4images_user_id: "456"
_4images_image_date: "2007-02-25T18:59:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9150 -->
Man sieht die beiden Schnecken, welche die Achse verschieben. Verbunden sind die Schnecken über eine Kette. Der Power-Motor wird mit nach vorne und hinten gezogen. In dieser Position ist das Differenzial nicht gesperrt, d.h. dass es ganz normal ist. Wäre das Differnzial gesperrt, wäre es wie eine durchgehende achse.