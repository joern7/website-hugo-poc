---
layout: "image"
title: "Fertig..."
date: "2009-01-01T13:24:33"
picture: "bild6.jpg"
weight: "6"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/16833
- /detailsbdbd.html
imported:
- "2019"
_4images_image_id: "16833"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:24:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16833 -->
Hier nochmal eine Ansicht von hinten. Das Kegelrad wird nicht von den Verbindungsstücken behindert.
Jetzt nur noch das Differential wieder montieren und es kann los gehen!