---
layout: "image"
title: "Teleskop-Kardan.jpg"
date: "2012-01-01T22:19:17"
picture: "Tele-Kardan1.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33828
- /details04e7.html
imported:
- "2019"
_4images_image_id: "33828"
_4images_cat_id: "297"
_4images_user_id: "4"
_4images_image_date: "2012-01-01T22:19:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33828 -->
Immer wenn man eine Antriebsachse gelenkig lagert und per längs verlaufender Welle antreiben will, fehlen ein Kardangelenk und ein Teleskop-Stück, das den Längenausgleich beim Einfedern bewirkt. Das Kardangelenk gibt es, beim Teleskopstück muss man schon ein paar Verrenkungen anstellen... 

oder...

man kriegt beides gleichzeitig und sogar noch kompakter als das ft-Rastkardan, mittels
- 1x Rastachse mit Platte, 130593
- 1x V-Stein 15x15x15, 31934
- 1x Impulszahnrad 4, 37157