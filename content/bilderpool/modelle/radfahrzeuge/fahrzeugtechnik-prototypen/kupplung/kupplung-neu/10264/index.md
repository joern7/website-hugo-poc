---
layout: "image"
title: "Kupplung von oben"
date: "2007-05-01T19:08:42"
picture: "kupplungneu1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10264
- /detailsda8b-2.html
imported:
- "2019"
_4images_image_id: "10264"
_4images_cat_id: "930"
_4images_user_id: "445"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10264 -->
Diesmal habe ich den Motor direkt hinter das Zahnrad getan.