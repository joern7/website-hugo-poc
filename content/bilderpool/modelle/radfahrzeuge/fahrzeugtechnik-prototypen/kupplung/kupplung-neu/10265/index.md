---
layout: "image"
title: "Kupplung von der Seite"
date: "2007-05-01T19:08:42"
picture: "kupplungneu2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10265
- /details17ae.html
imported:
- "2019"
_4images_image_id: "10265"
_4images_cat_id: "930"
_4images_user_id: "445"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10265 -->
Der Mini Motor würde nicht als richtiger motor dienen, aber für das reicht er ganz gut.