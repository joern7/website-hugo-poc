---
layout: "image"
title: "Blick auf die Unterseite"
date: "2007-05-28T19:32:44"
picture: "allradprototyp10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10551
- /details1d42-3.html
imported:
- "2019"
_4images_image_id: "10551"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10551 -->
Man sieht die fast schon primitive, aber höchst wirkungsvolle Hinterachskonstruktion, deren Antrieb wie bei der Vorderachse über Reibräder geschieht. Von links nach rechts folgen das Getriebe, der MiniMot zum Schalten des Getriebes, die Vorderachskonstruktion sowie der MiniMot für die automatische Niveauregulierung vorne.