---
layout: "image"
title: "Radaufhängung vorne"
date: "2007-05-28T19:33:55"
picture: "allradprototyp12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10553
- /detailsb36e.html
imported:
- "2019"
_4images_image_id: "10553"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:33:55"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10553 -->
Die beiden Vorderräder hängen an je drei Querlenkern (zwei oben, einer unten), die in der Mitte auf gemeinsamen langen Achsen aufgehängt sind. Die Lenkbewegungen sind möglich, weil unten eine drehbare Lagerung (S-Riegel) und oben ein Kardangelenk angebracht sind. Nur an diesen zwei Elementen stützt sich das Fahrzeug letztlich ab.