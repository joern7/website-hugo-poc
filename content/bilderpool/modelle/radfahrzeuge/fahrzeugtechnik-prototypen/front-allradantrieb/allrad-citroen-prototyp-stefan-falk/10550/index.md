---
layout: "image"
title: "Detailblick 2 auf das Getriebe"
date: "2007-05-28T19:32:44"
picture: "allradprototyp09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10550
- /details5d15.html
imported:
- "2019"
_4images_image_id: "10550"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10550 -->
Der MiniMot zieht am Gummiring (Original ft, für einen älteren Reifen 45 gedacht) und drückt damit die verschiebliche Getriebeachse gegen den Federdruck (die Feder stammt aus dem alten ft Elektromechanikprogramm).