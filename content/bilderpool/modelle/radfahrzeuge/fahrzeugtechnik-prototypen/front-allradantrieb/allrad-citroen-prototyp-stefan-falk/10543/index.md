---
layout: "image"
title: "Blick auf die Frontpartie"
date: "2007-05-28T19:32:43"
picture: "allradprototyp02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10543
- /details98d2-2.html
imported:
- "2019"
_4images_image_id: "10543"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10543 -->
Der "Motorraum" wird komplett von der Technik für die angetriebene, gelenkte und gefederte Vorderachse ausgefüllt. Der Antriebsmotor fand hier keinen Platz mehr, sondern sitzt weiter hinten. Die vorne zentral herausstehende Achse ist nur irrtümlich herausgerutscht; ich hatte das beim Fotografieren gar nicht gemerkt.

Lenkung: Man sieht oben die von dem MiniMot am linken Bildrand gedrehte Schnecke, die die Lenkung ausführt.

Federung: Die zwei langen Metallachsen sind in der Nähe der Schnecke gelenkig gelagert und stützen sich links und rechts vorne gegen (nach oben gedrückte) Federn aus dem ft Elektromechanikprogramm. Dadurch wird eine übermäßige Seitenneigung vermieden. Die Federkraft wird durch Berührung der Seiltrommel-Zahnräder mit den in der Mitte aufgehängten Radaufhängungen übertragen.

Niveauausgleich: Die vorne unten von einem MiniMot angetriebene Achse wickelt ein Gummi mehr oder weniger auf und bewirkt so eine mehr oder weniger starke Federwirkung. Die Kontakte für die Messung der Höhe fehlen in diesem Prototypen noch.

Antrieb: Der Antrieb kommt für das linke Rad (im Bild rechts oben) von hinten, für das rechte Rad durch eine der Achsen, an denen das Fahrwerk selbst aufgehängt ist vorne an. Über die Kardanwellen gelangt die Kraft bis zu den über den Vorderrädern angebrachten Reibrädern, die mit den Rädern federn und einlenken.