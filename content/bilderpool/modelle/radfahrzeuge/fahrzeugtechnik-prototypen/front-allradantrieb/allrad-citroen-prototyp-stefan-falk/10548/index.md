---
layout: "image"
title: "Nochmal Gangschaltung"
date: "2007-05-28T19:32:44"
picture: "allradprototyp07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10548
- /detailsa440-3.html
imported:
- "2019"
_4images_image_id: "10548"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10548 -->
Die unteren Zahnräder (Z10 und Z15) werden für die Umschaltung zwischen den 2 verfügbaren Gängen in Achsrichtung verschoben.