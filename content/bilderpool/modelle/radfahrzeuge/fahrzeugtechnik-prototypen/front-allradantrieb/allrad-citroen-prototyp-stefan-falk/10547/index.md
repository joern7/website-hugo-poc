---
layout: "image"
title: "Ein unscharfer Blick auf die Gangschaltung"
date: "2007-05-28T19:32:44"
picture: "allradprototyp06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10547
- /details7511-2.html
imported:
- "2019"
_4images_image_id: "10547"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10547 -->
Man sieht ansatzweise die Gangschaltung (die roten Zahnräder links im Bild).