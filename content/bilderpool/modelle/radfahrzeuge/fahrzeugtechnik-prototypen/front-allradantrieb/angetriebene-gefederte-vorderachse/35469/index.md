---
layout: "image"
title: "Angetriebene gefederte Vorderachse 3"
date: "2012-09-08T15:23:52"
picture: "Angetriebene_gefederte_Vorderachse_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35469
- /details2e3e.html
imported:
- "2019"
_4images_image_id: "35469"
_4images_cat_id: "2630"
_4images_user_id: "328"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35469 -->
Die Rückseite der Achse. Die Feder ist eine Druckfeder 30*5*0,3 (35796), die wohl schon lange in keinem Kasten mehr drin war, aber bei Knobloch lieferbar ist.