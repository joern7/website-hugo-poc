---
layout: "image"
title: "Angetriebene gefederte Vorderachse 4"
date: "2012-09-08T15:23:52"
picture: "Angetriebene_gefederte_Vorderachse_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35470
- /details40c9.html
imported:
- "2019"
_4images_image_id: "35470"
_4images_cat_id: "2630"
_4images_user_id: "328"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35470 -->
Die maximal mögliche Einfederung. Das Federbein schlägt oben am Kupplungsstück an und verhindert so, dass das Rad seitlich schleift.

Wie bereits geschrieben: Das funktioniert in der Hand wirklich prächtig, aber in einem Fahrzeug verbaut fällt alles auseinander ...