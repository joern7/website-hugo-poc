---
layout: "image"
title: "Kraftverteilung für die Vorderräder"
date: "2006-05-21T17:52:59"
picture: "Allrad3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6280
imported:
- "2019"
_4images_image_id: "6280"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6280 -->
Das hier sichtbare Differential wird angetrieben von dem hier nicht sichtbaren Differential, welches die Kraft zwischen Vorder- und Hinterachse verteilt. Das sichtbare Differential hier teilt diese Kraft zwischen linkem Vorderrad (der hier "rechte" Ausgang des Differentials) und rechtem Vorderrad (der hier "hintere" Ausgang des Differentials). Über mehrere Zwischen-Z10 gelangt diese Kraft zum Vorderbau.