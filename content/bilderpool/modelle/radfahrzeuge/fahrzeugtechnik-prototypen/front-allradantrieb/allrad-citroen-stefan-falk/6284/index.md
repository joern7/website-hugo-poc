---
layout: "image"
title: "Radaufhängung vorne"
date: "2006-05-21T17:52:59"
picture: "Allrad7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6284
imported:
- "2019"
_4images_image_id: "6284"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6284 -->
Die beiden Vorderräder hängen an je drei Querlenkern (zwei oben, einer unten), die in der Mitte auf gemeinsamen langen Achsen aufgehängt sind. Die Lenkbewegungen sind möglich, weil unten eine drehbare Lagerung (S-Riegel) und oben ein Kardangelenk angebracht sind. Nur an diesen zwei Elementen stützt sich das Fahrzeug letztlich ab.