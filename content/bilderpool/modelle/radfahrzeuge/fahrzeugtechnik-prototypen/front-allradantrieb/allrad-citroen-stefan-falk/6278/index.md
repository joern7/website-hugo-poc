---
layout: "image"
title: "Gesamtansicht"
date: "2006-05-21T17:52:59"
picture: "Allrad1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6278
imported:
- "2019"
_4images_image_id: "6278"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6278 -->
Die einzigen nicht von fischertechnik stammenden Teile sind zwei handelsübliche Gummiringe. Das Fahrwerk ist ansonsten ausschließlich aus unveränderten ft-Teilen aufgebaut.