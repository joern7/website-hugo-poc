---
layout: "image"
title: "Details"
date: "2011-11-13T18:14:36"
picture: "vorderachse5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33471
- /details03db.html
imported:
- "2019"
_4images_image_id: "33471"
_4images_cat_id: "2481"
_4images_user_id: "1376"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33471 -->
Etwas unscharf, aber man erkennt zumindest das meiste.