---
layout: "image"
title: "Lenkung von der Seite"
date: "2007-04-30T18:47:24"
picture: "vorderradlenkung6.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10244
- /details1d8e.html
imported:
- "2019"
_4images_image_id: "10244"
_4images_cat_id: "929"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10244 -->
Unten das Kardan und oben ,genau über dem knickpunkt ein Gelenk.