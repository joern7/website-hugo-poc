---
layout: "overview"
title: "Vorderradlenkung mit Antrieb und Federung (Ma-gi-er)"
date: 2020-02-22T07:51:58+01:00
legacy_id:
- /php/categories/964
- /categories192f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=964 --> 
Ich habe im Chat mal gesagt, dass Vorderradlenkung mit Antrieb und Federung einfach sei. Die Funktionalität kann man fast nicht mehr überbieten. Aber dass sie etwas kompakter ist, schon.