---
layout: "image"
title: "Von hinten"
date: "2007-06-01T20:08:58"
picture: "vorderradlenkungmitantriebundfederung4.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10649
- /details5776.html
imported:
- "2019"
_4images_image_id: "10649"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10649 -->
unten an der Clipachse muss man noch ein "Sternchen" befestigen.