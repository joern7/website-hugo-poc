---
layout: "image"
title: "Von vorne"
date: "2007-06-01T20:08:58"
picture: "vorderradlenkungmitantriebundfederung3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10648
- /details4a9a.html
imported:
- "2019"
_4images_image_id: "10648"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10648 -->
An die Achse, die heraussteht (kann auch eine Metall Achse sein) käme das Rad.