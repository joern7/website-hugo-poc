---
layout: "image"
title: "Befestigung Differential"
date: "2007-06-01T20:08:58"
picture: "vorderradlenkungmitantriebundfederung6.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10651
- /details2c54-2.html
imported:
- "2019"
_4images_image_id: "10651"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10651 -->
Ich habe einen Reed-Kontaktbefestigungsstein mit zwei Rollen ausgestatten (sie hineingeschoben) so das ich die Achse gut durchschieben konnte.