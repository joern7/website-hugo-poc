---
layout: "comment"
hidden: true
title: "3679"
date: "2007-07-21T10:44:40"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Hi,
mal ne Frage:
Wieso baust du das Differenzial so schrecklich kompliziert ein wenn es doch vorne auch reinpasst? Du hast vorne ein 30er und ein 15er Baustein, ein 30er würde sogar reihen in der Breite. Wieso machst du es vorne dann nicht einen 15er-Baustein schmaler und baust das Diff vorne ein. Das wäre doch viel einfacher.

Etwas verwirrt,
fitec