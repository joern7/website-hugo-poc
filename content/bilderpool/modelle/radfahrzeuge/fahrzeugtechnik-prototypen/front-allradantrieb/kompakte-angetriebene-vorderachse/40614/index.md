---
layout: "image"
title: "Radaufhängung"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung3.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40614
- /detailsbc3f-2.html
imported:
- "2019"
_4images_image_id: "40614"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40614 -->
Die Radaufhängung ist ein S-15. Die Federung ist an der Radaufhängung nicht richtig befestigt, sie hält nur dadurch, dass die Federkraft die Rastachse von schräg oben in das Loch des Statikbausteins drückt.
Die Kardangelenke habe ich mit einem Federnocken "kurzgekuppelt".