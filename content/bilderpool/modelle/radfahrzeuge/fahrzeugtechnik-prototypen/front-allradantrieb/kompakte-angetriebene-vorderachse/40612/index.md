---
layout: "image"
title: "Gesamtansicht"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung1.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40612
- /details37c0-2.html
imported:
- "2019"
_4images_image_id: "40612"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40612 -->
Stefan Falks Erfindung http://www.ftcommunity.de/categories.php?cat_id=1012 hat mich fasziniert. Damit ist der Drehpunkt der Achsschenkel über das Parallelgestänge nach innen "kopiert" und dort ist erst die Aufhängung. Zusätzlich verschiebt sich das Trapez beim Lenken etwas nach vorne und hinten, wodurch der effektive Drehpunkt sogar unter das Rad kommt. Das Ganze noch mit Differential und Federung seht ihr hier.