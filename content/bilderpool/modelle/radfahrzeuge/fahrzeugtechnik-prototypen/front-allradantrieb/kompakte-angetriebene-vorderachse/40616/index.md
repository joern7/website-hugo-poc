---
layout: "image"
title: "Rechtseinschlag"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung5.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40616
- /detailsf0fa.html
imported:
- "2019"
_4images_image_id: "40616"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40616 -->
Beim Vergleich mit dem nächsten Bild (Stefan, das habe ich bei dir abgeguckt), sieht man den Effekt der schrägen Haltestrebe. Bei Stefan kommt die Längsverschiebung des Trapezes dadurch zustande, dass die drei Achsen der Strebenaufhängung nicht genau in einer Reihe sind. Das ergibt natürlich eine super Symmetrie. Hier ist die Längsverschiebung nach vorne und hinten nicht genau gleich, dafür sind die Querträger fix und ich konnte das Differential einbauen.