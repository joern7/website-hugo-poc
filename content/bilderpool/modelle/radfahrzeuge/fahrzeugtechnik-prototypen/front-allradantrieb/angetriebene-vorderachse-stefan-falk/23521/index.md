---
layout: "image"
title: "Ansicht von oben"
date: "2009-03-26T21:25:13"
picture: "vorderachse2.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23521
- /detailscfd8.html
imported:
- "2019"
_4images_image_id: "23521"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23521 -->
Der Motor hängt schräg im Gestell. Fest sitzt er tatsächlich nur an den Achsen des Differentials. Die Aufhängung sieht schwer abenteuerlich aus, funktioniert aber gut und kommt vor allem mit nichts Anderem ins Gehege. Der Motor wird nämlich beim Lenken leicht nach links oder rechts ausgelegt und braucht deshalb Platz.

Die Achskonstruktion ist nur an zwei K-Achsen mit den Flachträgern verbunden, deren obere Klemmringe man etwa an den Differentialenden sieht.