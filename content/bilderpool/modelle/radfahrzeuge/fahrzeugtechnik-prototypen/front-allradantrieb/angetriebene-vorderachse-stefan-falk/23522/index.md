---
layout: "image"
title: "Schrägansicht"
date: "2009-03-26T21:25:13"
picture: "vorderachse3.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23522
- /detailsfdb1-2.html
imported:
- "2019"
_4images_image_id: "23522"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23522 -->
Hier sieht man die Geometrie auch ganz gut. Der Motor gleitet beim Lenken auf der Statikstrebe unten entlang.