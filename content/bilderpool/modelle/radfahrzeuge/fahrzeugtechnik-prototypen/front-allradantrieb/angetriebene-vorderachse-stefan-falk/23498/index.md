---
layout: "image"
title: "Ansicht von unten"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23498
- /detailsb8a1.html
imported:
- "2019"
_4images_image_id: "23498"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23498 -->
Da der Antrieb von oben kommt, bleibt viel Bodenfreiheit übrig (die Portalachse ruft wieder ;-) Wenn ich mich recht erinnere, war es nicht so einfach, den Antrieb nach unten abzuleiten, weil sich beim Lenken immer alles in die Quere kommt.