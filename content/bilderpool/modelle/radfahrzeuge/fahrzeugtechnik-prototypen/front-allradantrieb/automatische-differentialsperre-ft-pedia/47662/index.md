---
layout: "image"
title: "Sperre für Mitteldifferential 1"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47662
- /detailsd630-2.html
imported:
- "2019"
_4images_image_id: "47662"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47662 -->
So, das wird jetzt komplizierter. Ich bin ja immer mal wieder in Sachen Allrad unterwegs, und das hier soll eine automatische Sperre für ein Mitteldifferential werden, die auf einem ähnlichen Prinzip basiert wie die in der ft:pedia beschriebene.

Da meine Konstruktionen für die Fliehkraftbremse für den praktischen Einsatz entweder zu sperrig oder zu unzuverlässig waren, habe ich die Anforderungen für das Aktivieren der Bremse heruntergeschraubt: Bei meinen Tests von Allradantrieben an der 45°-Rampe drehten immer die Vorderräder durch, weil sich der Schwerpunkt der Fahrzeuge nach hinten verschob. Nur diese Situation soll die neue Differentialsperre verhindern, in allen anderen braucht sie nicht zu wirken.