---
layout: "image"
title: "Sperre für Mitteldifferential 6"
date: "2018-05-18T18:40:13"
picture: "sperrefuermitteldifferential08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47667
- /detailsac07.html
imported:
- "2019"
_4images_image_id: "47667"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47667 -->
Das ist die Situation bei Rückwärtsfahrt. Der Hebel wird angehoben und die Differentialsperre ist unwirksam.