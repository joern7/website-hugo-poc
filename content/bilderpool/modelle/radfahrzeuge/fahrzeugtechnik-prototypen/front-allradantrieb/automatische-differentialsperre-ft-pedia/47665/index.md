---
layout: "image"
title: "Sperre für Mitteldifferential 4"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47665
- /detailsd37b.html
imported:
- "2019"
_4images_image_id: "47665"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47665 -->
Wenn sich Vorder- und Hinterachse gleich schnell drehen, also weder eine Kurve gefahren wird, noch eine Achse durchdreht, drehen sich beide Differentiale und beide Z20 (eins ist verdeckt) gleich schnell. Bei Vorwärtsfahrt wird dabei der Hebel, bestehend aus den beiden S-Laschen, heruntergedrückt. Das geschieht durch die Rechtsdrehung der Motorwelle und wird verstärkt durch die Drehung der sechs linken Riegelscheiben, die sich an der einzelnen Riegelscheibe am Rollenblock nach unten "abdrücken".