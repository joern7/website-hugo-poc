---
layout: "image"
title: "Sperre für Mitteldifferential, eingebaut in MB-Trac 2"
date: "2018-05-18T18:41:08"
picture: "sperrefuermitteldifferential13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47672
- /detailse065.html
imported:
- "2019"
_4images_image_id: "47672"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:41:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47672 -->
Ich muss gestehen: Der Effekt der Differentialsperre verpufft leider. Ich hatte gehofft, dass der MB-Trac die 45°-Rampe ohne durchdrehende Vorderräder schafft - aber es sieht leider auch mit Differentialsperre fast genau so aus wie in
https://www.youtube.com/watch?v=lpau8PAM0Nw , oder manchmal noch schlechter. Scheinbar entsteht durch die Differentialsperre so viel zusätzliche Reibung, dass er nur mit frisch vollgeladenen Akkus ansatzweise hochkommt.
