---
layout: "image"
title: "Allrad_4703.JPG"
date: "2011-02-25T16:41:08"
picture: "Allrad_4703.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30126
- /details48ff.html
imported:
- "2019"
_4images_image_id: "30126"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2011-02-25T16:41:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30126 -->
Die Hinterachse mitsamt Motor ist in einer Schwinge in Fahrzeugmitte gelagert. Die Federung der Hinterachse beruht auf vier Stück "Federfuß 31307". Die Vorderachse hängt da nur mittels zweier V-Platten 15x90 dran, die noch zusätzliche Weichheit mitbringen. An deren vorderen Enden sitzen Verbinder 35262, weil die Nuten der Bausteine in der Vorderachse nicht weit genug nach außen reichen.