---
layout: "image"
title: "Mini-Allrad-R45-06.jpg"
date: "2009-03-11T16:39:36"
picture: "Mini-Allrad-R45-06.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23446
- /details75f0-2.html
imported:
- "2019"
_4images_image_id: "23446"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-11T16:39:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23446 -->
Die U-Träger-Adapter haben genau die richtige Breite: Differenzial und Kegelzahnrad sind ordentlich eingerastet.
Das Spurstangengelenk 35068 ist das zentrale Element, sein Kopf (hier gerade jenseits des Bildrandes) stützt das antreibende Kegelrad.