---
layout: "image"
title: "Allrad-R45-01.jpg"
date: "2009-03-06T09:54:38"
picture: "Allrad-R45-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23376
- /detailsf0b2.html
imported:
- "2019"
_4images_image_id: "23376"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-06T09:54:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23376 -->
Leider ein wenig unscharf geworden, aber hier sieht man wenigstens, wie die Spurstangen (es gibt zwei davon) angebracht sind.