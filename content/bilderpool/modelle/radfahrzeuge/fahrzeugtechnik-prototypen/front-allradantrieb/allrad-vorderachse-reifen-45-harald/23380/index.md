---
layout: "image"
title: "Allrad-R45-24.jpg"
date: "2009-03-06T10:14:51"
picture: "Allrad-R45-24.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23380
- /detailsba07-2.html
imported:
- "2019"
_4images_image_id: "23380"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-06T10:14:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23380 -->
Der Lenkeinschlag wird derzeit dadurch begrenzt, dass die Spurstange irgendwann klemmt. Hier geschieht das ziemlich früh; es ist aber noch nicht aller Tage Abend.