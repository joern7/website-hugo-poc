---
layout: "comment"
hidden: true
title: "8714"
date: "2009-03-08T14:17:34"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Dienstag erst? Das wird aber ein langweiliger Montag, höhöhö... 

Aber mal im Ernst, das sind so Sachen, die seit November in der Ecke herumliegen und erst jetzt etwas Zeit kriegen, weil ich mal ein paar Überstunden nehmen kann.

Schmaler bauen ginge ja noch, unter Wegfall der 80er Reifen und der kurzen Achsen am Differenzial. Nur ist dann der Reifen 45 kleiner als der Klotz, mit dem er festgehalten wird. Sieht nicht so überzeugend aus, und die Bodenfreiheit ist auch nicht üppig.

Gruß,
Harald