---
layout: "image"
title: "front8423.JPG"
date: "2012-10-21T18:27:29"
picture: "front8423.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36030
- /detailse9bf.html
imported:
- "2019"
_4images_image_id: "36030"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2012-10-21T18:27:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36030 -->
Ein bisschen Karosseriebau außen herum und ... irgendwie wird das langweilig, wo die wesentliche Technik schon komplett vorne unter der Haube steckt.