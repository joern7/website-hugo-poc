---
layout: "image"
title: "Allrad_4704.JPG"
date: "2011-02-25T16:44:29"
picture: "Allrad_4704.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30127
- /details5e10.html
imported:
- "2019"
_4images_image_id: "30127"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2011-02-25T16:44:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30127 -->
Das ganze kann sich sehr weit verwinden. Die Metallachsen sind nur Ballast, damit die Hinterachse mehr Bodenhaftung bekommt. Der Lenkservo oder -motor fehlt noch; hier gings erst einmal nur um den Steigfähigkeitstest auf der Rampe, http://www.ftcommunity.de/categories.php?cat_id=2213