---
layout: "image"
title: "Allrad-R45-53.JPG"
date: "2009-03-08T11:39:03"
picture: "Allrad-R45-53.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23419
- /details8c19.html
imported:
- "2019"
_4images_image_id: "23419"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-08T11:39:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23419 -->
Kleine Änderung: die unteren BS30 sind ewas auseinander gerückt, damit noch ein BS5 dazwischen passt und das Ganze symmetrisch wird. Somit kann die Antriebswelle erstmal senkrecht nach unten geführt werden. Von dort geht der Antriebsstrang mit zwei Kegelzahnrädern ums Eck und nach hinten weiter.