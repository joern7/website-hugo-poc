---
layout: "image"
title: "Vorderachse Detailaufnahme"
date: "2005-11-01T19:53:08"
picture: "Vorderachse_010.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5173
- /details59a3.html
imported:
- "2019"
_4images_image_id: "5173"
_4images_cat_id: "642"
_4images_user_id: "104"
_4images_image_date: "2005-11-01T19:53:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5173 -->
Hier soll noch folgendes verbessert werden (aber ich werde wohl kaum vor Weihnachten dazu kommen): 1. Der obere Teil muss noch fester mit dem BS15 unten verbunden werden, damit das dauerbetriebsfähig wird. 2. Der BS30 und die 2 BS15, die mitgelenkt werden, müssen noch durch Statik ersetzt werden. Vor allem, damit die untere Radaufhängung durch eine zweite ergänzt werden kann und das Kardan oben weniger Scherlast tragen muss, außerdem wird dadurch aber auch die ungefederte Masse kleiner. Insgesamt sollte das dazu führen, dass man ein Auto mit Front- oder Allradantrieb bauen kann - schau'n wir mal!