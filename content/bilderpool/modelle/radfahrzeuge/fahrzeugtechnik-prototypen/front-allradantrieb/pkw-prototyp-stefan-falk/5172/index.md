---
layout: "image"
title: "Vorderachse von oben"
date: "2005-11-01T19:53:08"
picture: "Vorderachse_009.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5172
- /detailsaca7.html
imported:
- "2019"
_4images_image_id: "5172"
_4images_cat_id: "642"
_4images_user_id: "104"
_4images_image_date: "2005-11-01T19:53:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5172 -->
Hier sieht man die beiden Antriebswellen (die natürlich noch von einem Differential bedient werden müssen). Hier will ich noch folgende Änderungen probieren: 1. Die Antriebswellen quer zur Fahrtrichtung (eine vor, die andere hinter der gegenüberliegenden Radaufhängung vorbei). Dadurch würde das ganze kompakter bauen. Mal sehen, wie ich die beiden dann noch angetrieben bekomme. 2. Anstatt sechs Gelenke in der Mitte nur drei: Beide Querlenkergruppen hängen in der Mitte dann an denselben drei Gelenken. Dadurch wird das ganze nochmal 30 mm schmaler. Diese Gelenke sollen sich dann an zwei Metallachsen festhalten, die in der Mitte durch sie laufen und geeignet mit dem Fahrzeug verbunden sein sollen.