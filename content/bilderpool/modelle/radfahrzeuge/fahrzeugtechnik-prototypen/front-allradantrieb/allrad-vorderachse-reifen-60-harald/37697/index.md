---
layout: "image"
title: "Allrad-R60-9888.JPG"
date: "2013-10-08T21:01:30"
picture: "Allrad-R60-9888.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37697
- /details171a-2.html
imported:
- "2019"
_4images_image_id: "37697"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T21:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37697 -->
Die Unterseite. Wo am Fahrzeug hinten und wo vorne sein soll, kann man sich heraussuchen und muss dann nur noch die Spurstange "hinten" anbauen (und ggf. einen weiteren Anlenkhebel zum Hubgetriebe auf der Hinterseite anbringen). Hier ist "hinten" = oben im Bild.


"... So hab' ich es mir gewünscht. ..."