---
layout: "comment"
hidden: true
title: "16005"
date: "2012-01-06T14:26:36"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

da hat bei ft garantiert niemand dran gedacht. Das äußerste der Gefühle ist bei denen erreicht, wenn ein Fahrzeug in der Breite auf 90 mm oder knapp drüber kommt. Dabei wird ft-Fahrzeugbau erst richtig interessant, wenn man 180 mm (Statikplatte 180x90 quer) als Breite anpeilt.

Gruß,
Harald