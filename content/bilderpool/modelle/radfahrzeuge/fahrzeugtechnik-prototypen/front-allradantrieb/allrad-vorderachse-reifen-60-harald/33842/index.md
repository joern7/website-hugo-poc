---
layout: "image"
title: "Allrad-R60-80.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad4.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33842
- /details52be.html
imported:
- "2019"
_4images_image_id: "33842"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33842 -->
Das ist eine angetriebene Achse, die dank "Rastachse mit Platte" 130593 und dem Rastadapter mit allen Teilen im Raster ist.