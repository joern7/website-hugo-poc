---
layout: "image"
title: "Allrad-R60-03.JPG"
date: "2009-04-08T11:28:34"
picture: "Allrad-R60-03.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23640
- /details8eba-2.html
imported:
- "2019"
_4images_image_id: "23640"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2009-04-08T11:28:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23640 -->
Das Rad hat eine (gelbe) Freilaufnabe und sitzt auf einer Rastachse mit Platte 130593. Links und rechts vom Differenzial sitzen Kugellager in den Innenseiten der Schneckenmuttern, deshalb kann die Rastkupplung der einen Seite und das Abtriebsrad der anderen Seite etwas hineinragen.