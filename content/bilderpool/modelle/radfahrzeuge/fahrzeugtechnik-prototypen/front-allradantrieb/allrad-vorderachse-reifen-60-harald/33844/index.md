---
layout: "image"
title: "Allrad-R60-82.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad6.jpg"
weight: "11"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33844
- /details46f8-2.html
imported:
- "2019"
_4images_image_id: "33844"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33844 -->
Die "Rastachse mit Platte" ist auf dem besten Wege, der Führungsplatte 32455 den Rang als mein Favoritenteil abzulaufen. Hier sind gleich vier verbaut, am 'Jumping' waren es 10, und ein in der Ferne liegendes Projekt kommt auf mindestens 12 davon.