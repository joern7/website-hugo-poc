---
layout: "image"
title: "Allrad-R60-02.JPG"
date: "2009-04-08T11:24:31"
picture: "Allrad-R60-02.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23639
- /details6d04.html
imported:
- "2019"
_4images_image_id: "23639"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2009-04-08T11:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23639 -->
Noch ne Methode, diesmal mit dem Kegelkranz des Rast-Differentials als Umlenkmechanismus. Die Räder sind hier etwas nach außen abgezogen. Wenn sie richtig sitzen, ist die Verzahnung sauber.