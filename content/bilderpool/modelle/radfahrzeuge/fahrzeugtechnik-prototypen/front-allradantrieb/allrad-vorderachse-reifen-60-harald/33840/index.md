---
layout: "image"
title: "Allrad-R60-73.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad2.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33840
- /details1b4b.html
imported:
- "2019"
_4images_image_id: "33840"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33840 -->
Von unten. Als Spurstangengelenke dienen die Kardans. Die Lenkung hat ziemlich viel Spiel, was mich aber im Moment noch nicht sehr stört. Zur Not kann man kleine Papierschnipsel überall hinein klemmen, dann ist da auch Ruhe drin.