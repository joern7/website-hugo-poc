---
layout: "image"
title: "Allrad-R60-83.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad7.jpg"
weight: "12"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33845
- /details7608.html
imported:
- "2019"
_4images_image_id: "33845"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33845 -->
Das Zentralgelenk ist zweiteilig: In Fahrtrichtung vorn die Lenkung, hinten die Verschränkung. Die Antriebswelle läuft durch die grauen Gelenksteine hindurch.