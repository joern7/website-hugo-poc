---
layout: "image"
title: "Seite"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk3.jpg"
weight: "3"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33309
- /detailsa440-2.html
imported:
- "2019"
_4images_image_id: "33309"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33309 -->
Ups - das Statikteil rechts vor dem Vorderreifen ist mir wohl etwas verrutscht.

Wie man sieht, ist die Bodenfreiheit nicht soooo bombenmäßig. Ist aber auch egal - es sollte ja ein etwas sportlicher PKW werden, und kein SUV oder gar Geländewagen.