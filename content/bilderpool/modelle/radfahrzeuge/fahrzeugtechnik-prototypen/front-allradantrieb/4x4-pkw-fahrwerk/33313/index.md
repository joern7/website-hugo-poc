---
layout: "image"
title: "Antrieb"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk7.jpg"
weight: "7"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33313
- /details7d38-2.html
imported:
- "2019"
_4images_image_id: "33313"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33313 -->
Links die 2 XMs, in der Mitte mal wieder meine Rutschkupplung als Überlastungsschutz und unten, im Bild über der Achse zum Überlastungsschutz der Reedkontakthalter.