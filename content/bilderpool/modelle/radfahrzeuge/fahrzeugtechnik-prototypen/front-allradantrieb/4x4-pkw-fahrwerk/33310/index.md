---
layout: "image"
title: "Hinten"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk4.jpg"
weight: "4"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33310
- /detailsef6e-3.html
imported:
- "2019"
_4images_image_id: "33310"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33310 -->
Nun ja, das Heck schaut doch eher nach SUV aus. Aber das kann sich ja noch ändern. Wie schon erwähnt, war es mein Ziel, möglichst viel Innenraum zu schaffen, was meiner Meinung nach auch halbwegs gelungen ist. Der Boden bietet nahezu unbegrenzte Anbaumöglichkeiten. Über dem Mitteldifferenzial sitzt mein erwähnter Überlastungsschutz - die Kardans in der Vorderachse freuen sich :)