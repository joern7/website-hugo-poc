---
layout: "image"
title: "Allradantrieb 13"
date: "2007-05-01T13:32:43"
picture: "allradantrieb01.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10251
- /details2e2c.html
imported:
- "2019"
_4images_image_id: "10251"
_4images_cat_id: "1589"
_4images_user_id: "502"
_4images_image_date: "2007-05-01T13:32:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10251 -->
Leider bekommt man die Übersetzungen nicht gleich hin, deshalb läuft der hintere Antrieb etwas schneller. Eine Möglichkeit wäre die Motoren per IF zu steuern und den hinteren dann eine Geschwindigkeit lansamer laufen zu lassen, dann passt es fast genau.