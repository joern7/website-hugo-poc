---
layout: "image"
title: "Allradantrieb 7"
date: "2007-04-29T19:59:10"
picture: "allradantrieb1_2.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10193
- /detailsd331.html
imported:
- "2019"
_4images_image_id: "10193"
_4images_cat_id: "1589"
_4images_user_id: "502"
_4images_image_date: "2007-04-29T19:59:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10193 -->
Jetzt mit Motor und Lenkantrieb. Nur leider fehlen mir jetzt die Teile um den Rest dranzubauen :-( 
Wenn ich jetzt den hinteren Antrieb mit nem anderen Motor machen will wie find ich dann raus was für ne Übersetzung ich brauch?