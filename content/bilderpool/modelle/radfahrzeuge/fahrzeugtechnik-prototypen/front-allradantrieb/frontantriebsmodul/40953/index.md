---
layout: "image"
title: "Frontatrieb von vorne unten"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40953
- /details57ff.html
imported:
- "2019"
_4images_image_id: "40953"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40953 -->
Endlich kann ich mal die Statik-Knotenplatten verwenden.