---
layout: "image"
title: "Frontantrieb aufgeklappt von hinten"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40957
- /details8285.html
imported:
- "2019"
_4images_image_id: "40957"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40957 -->
Ganz aufgeklappt. Ziemlich tricky war die Befestigung der linken S-Strebe 60 für den Differentialkäfig. Weil der BS30 mit Lock ja an der Linken Seite den Zapfen hat, habe ich das mit einer Winkellasche gelöst. Man muss nur aufpassen, dass das Winkelsahnrad des Differentials auf der anderen Seite ist, damit das Winkelgetriebe kein Spiel hat und rattert.