---
layout: "image"
title: "Frontantrieb aufgeklappt von hinten"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40956
- /detailsb680.html
imported:
- "2019"
_4images_image_id: "40956"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40956 -->
Aufgeklappt wie ein "Butterbrot" sieht man, dass auf der Spurstange Lagerhülsen stecken, die den Winklelstein 15° in der Mitte fixieren. Im Differential stecken Rastachsen 30, in den Ausfallenden dann die Kardangelenke wie in http://www.ftcommunity.de/details.php?image_id=40627. Zur Stabilisierung sind noch rote Distanzhülsen darüber geschoben.