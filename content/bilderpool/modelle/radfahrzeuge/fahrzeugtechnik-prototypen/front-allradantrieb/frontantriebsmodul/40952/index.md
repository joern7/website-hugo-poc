---
layout: "image"
title: "Frontantrieb von vorne unten"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40952
- /detailsa6dd-2.html
imported:
- "2019"
_4images_image_id: "40952"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40952 -->
Das Bild ist gedreht für eine Sicht von unten auf das Modul. Weil das Ganze für einen Geländewagen verwendet werden soll, habe ich unterhalb der Achse möglichst wenig verbaut, um möglichst viel Bodenfreiheit zu erhalten. Nur der eine blöde S-Riegel steckt von der falschen Seite drin, das habe ich erst auf dem Bild bemerkt. Der muss natürlich, wie die anderen auch, von oben reingesteckt werden. Ist er mittlerweile auch.