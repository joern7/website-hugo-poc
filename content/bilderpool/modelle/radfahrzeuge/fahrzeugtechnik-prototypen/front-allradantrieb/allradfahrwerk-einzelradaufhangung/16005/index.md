---
layout: "image"
title: "Von Oben"
date: "2008-10-18T12:58:13"
picture: "verkuertzt2.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/16005
- /detailsc170-2.html
imported:
- "2019"
_4images_image_id: "16005"
_4images_cat_id: "1453"
_4images_user_id: "558"
_4images_image_date: "2008-10-18T12:58:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16005 -->
