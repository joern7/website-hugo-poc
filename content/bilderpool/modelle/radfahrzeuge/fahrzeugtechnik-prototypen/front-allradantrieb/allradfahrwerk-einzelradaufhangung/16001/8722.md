---
layout: "comment"
hidden: true
title: "8722"
date: "2009-03-08T20:02:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
als wacker gesprochen zu haben kann ich meinen Text aber nicht ansehen. Wir wissen alle, dass das auch nicht annähernd mit ft-Standardteilen machbar ist. Es sah aber hier in der ftCommunity schon eine vorgestellte Umgehungslösung, wobei beim Lenkeinschlag der Achsstand an den Vorderrädern entsprechend verändert wird wird. Leider habe ich das auf die Schnelle nicht wieder finden können. Die Öffentlichkeit des Forums wird dazu aber bestimmt noch hilfreich sein. Derartiges meinte ich als ein Anspruch für Modellvorhaben. Und ein PKW-Modell z.B. mit doch engen Radkastenausschnitten der Karosserie wäre dann auch erst mit einer derartigen Lösung möglich.
Gruss, Ingo