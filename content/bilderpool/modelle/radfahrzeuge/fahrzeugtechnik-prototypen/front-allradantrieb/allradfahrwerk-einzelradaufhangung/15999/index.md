---
layout: "image"
title: "Einzelrad Aufhängung"
date: "2008-10-17T16:31:51"
picture: "allrad2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/15999
- /details2d8e.html
imported:
- "2019"
_4images_image_id: "15999"
_4images_cat_id: "1453"
_4images_user_id: "558"
_4images_image_date: "2008-10-17T16:31:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15999 -->
Komplettansicht, wobei nur links eine Lenkung eingebaut ist.