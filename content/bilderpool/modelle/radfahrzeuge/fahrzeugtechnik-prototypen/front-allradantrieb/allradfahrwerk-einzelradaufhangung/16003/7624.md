---
layout: "comment"
hidden: true
title: "7624"
date: "2008-10-17T19:51:36"
uploadBy:
- "Svefisch"
license: "unknown"
imported:
- "2019"
---
Das Loch ist vor ca. zehn Jahren in den Kardanwürfel gekommen und gabs dann im Pneumatik-Kasten (Vorgänger des Pneumatik 2). Das war wieder mal eine Bauteilverbesserung, die hier eine prima Idee ermöglicht. gefällt mir wirklich gut, die Aufhängung mit Lenkung. Soll man erst einmal drauf kommen.

Gruß
Holger