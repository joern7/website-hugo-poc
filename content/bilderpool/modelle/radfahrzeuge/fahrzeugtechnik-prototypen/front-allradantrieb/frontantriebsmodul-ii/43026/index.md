---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Lenkung 3"
date: "2016-03-10T12:37:42"
picture: "98_nochmal_verbesserte_Lenkung_3.jpg"
weight: "67"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43026
- /details0439-3.html
imported:
- "2019"
_4images_image_id: "43026"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43026 -->
Der gelbe Stern ist das Firmenlogo und hat keine Funktion.
Beachtenswert sind vielleicht noch die BS7,5, die ich jetzt auf beiden Seiten für die Befestigung der Federn verwende. Vorher war zu wenig Platz zum Einfedern für die Statiklaschen.