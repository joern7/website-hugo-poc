---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung-Details 3"
date: "2016-01-27T21:37:09"
picture: "80_verbesserte_Radaufhngung_-_Details_3.jpg"
weight: "48"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42828
- /details8ea4-4.html
imported:
- "2019"
_4images_image_id: "42828"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42828 -->
Das Lagerstück-Unterteil ist ein wenig nach unten geschoben, weil das Z7 kleiner ist als ein Z10, welches in das 15mm-Raster passen würde.