---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul28.jpg"
weight: "28"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42602
- /detailsc4f0.html
imported:
- "2019"
_4images_image_id: "42602"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42602 -->
Die Spurstangen sind in der Mitte an einem doppelten Statkiadapter befestigt. Dieser wird mit den Statikstreben geführt und über den BS7,5 vom Servo hin- und hergeschoben.