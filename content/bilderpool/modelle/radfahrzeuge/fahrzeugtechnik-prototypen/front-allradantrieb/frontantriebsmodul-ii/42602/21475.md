---
layout: "comment"
hidden: true
title: "21475"
date: "2015-12-30T20:41:54"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Marten,
der Trick ist ein Kettenglied, welches in der Gelenkklaue unter dem Kardan liegt. Dadurch kommt das Zahnrad höher. Siehe auch drei Bilder weiter hinten: http://www.ftcommunity.de/details.php?image_id=42605#col3
Martin