---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 7"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul31.jpg"
weight: "31"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42605
- /detailsd23f.html
imported:
- "2019"
_4images_image_id: "42605"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42605 -->
Man beachte das Kettenglied in der Gelenkwürfel-Klaue unter dem Kardangelenk, damit das Z10 genug Abstand zum BS15 bekommt.