---
layout: "image"
title: "Frontantrieb II, Radaufhängung 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul17.jpg"
weight: "17"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42591
- /detailsde87.html
imported:
- "2019"
_4images_image_id: "42591"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42591 -->
Die Felge ist ein Poti-Flachstein, das Radlager ein Lagerstück1 ohne Anschlag. Das Lager hat viel Spiel, aber durch feine Höhenjustage des Lagerstücks kann man es so einstellen, dass die Zahnräder (oben im Reifen schon zu erahnen) immer Eingriff haben. Ein harter Dauerbetreib wird dennoch nicht möglich sein. Von außen passt eine Flachnabe gut in den Flachstein, vielleicht lässt sich damit was verbessern.