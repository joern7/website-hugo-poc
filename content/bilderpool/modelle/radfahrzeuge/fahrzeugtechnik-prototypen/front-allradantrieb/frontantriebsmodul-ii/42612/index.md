---
layout: "image"
title: "Frontantrieb II, Servo-Montage 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul38.jpg"
weight: "38"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42612
- /detailsd220.html
imported:
- "2019"
_4images_image_id: "42612"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42612 -->
Zwischen den Rädern und den Federn ist kaum ein Millimeter Platz.