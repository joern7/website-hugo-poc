---
layout: "image"
title: "Frontantrieb II verbessert, Lenkung 1"
date: "2016-02-15T17:21:21"
picture: "96_verbesserte_Lenkung_1.jpg"
weight: "55"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42872
- /details0751-3.html
imported:
- "2019"
_4images_image_id: "42872"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-02-15T17:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42872 -->
Bei den Praxistests hat sich gezeigt, dass die Lenkung zu weich ist. Desegen kommt hier eine Variante, die fester ist und präziser arbeitet.