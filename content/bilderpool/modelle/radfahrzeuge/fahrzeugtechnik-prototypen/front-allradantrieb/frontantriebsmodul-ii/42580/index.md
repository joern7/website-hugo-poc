---
layout: "image"
title: "Frontantrieb II, Bodenfreiheit 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42580
- /details11f5.html
imported:
- "2019"
_4images_image_id: "42580"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42580 -->
Der schwarze Hebel des Lenkservos steckt in einem Klemmadapter (nicht zu sehen). Die zwei Riegelscheiben dazwischen verhindern, dass sich der Hebel an dem BS7,5 verhakt.