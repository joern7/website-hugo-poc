---
layout: "image"
title: "Frontantrieb II verbessert, Zahnrad-Variante"
date: "2016-01-27T21:37:09"
picture: "90_verbessertes_Zahnrad_-_Variante_1.jpg"
weight: "52"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42832
- /details0de7.html
imported:
- "2019"
_4images_image_id: "42832"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42832 -->
Man kann die Kettenglieder anstelle der Schnur auch mit einem Stück Pneumatikschlauch fixieren. Es ist aber schwer, den Schlauch durch das Loch zu fummeln.