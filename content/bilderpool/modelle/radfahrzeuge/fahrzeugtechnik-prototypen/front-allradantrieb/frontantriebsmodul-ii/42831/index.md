---
layout: "image"
title: "Frontantrieb II verbessert, Zahnrad"
date: "2016-01-27T21:37:09"
picture: "85_verbessertes_Zahnrad_1.jpg"
weight: "51"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42831
- /details019b.html
imported:
- "2019"
_4images_image_id: "42831"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42831 -->
Hier das Z7 als Explosionszeichnung :-) Die Klemme hat im Loch des halben Gelenksteins etwas Spiel, aber nicht allzuviel. Die Schnur ist einmal durch das Querloch und einmal durch die Klauen des Kardangelenks geführt.