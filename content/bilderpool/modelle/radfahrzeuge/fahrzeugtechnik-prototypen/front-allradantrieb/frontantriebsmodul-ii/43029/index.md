---
layout: "image"
title: "Frontantrieb II nochmal verbessert, im Einsatz"
date: "2016-03-10T12:37:42"
picture: "99_verbesserte_Version_im_Einsatz.jpg"
weight: "70"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43029
- /detailsaf06-2.html
imported:
- "2019"
_4images_image_id: "43029"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43029 -->
So sieht das dann im Einsatz aus.