---
layout: "image"
title: "Frontantrieb II verbessert, Lenkung 2"
date: "2016-02-15T17:21:21"
picture: "96_verbesserte_Lenkung_2.jpg"
weight: "56"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42873
- /detailsb819-2.html
imported:
- "2019"
_4images_image_id: "42873"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-02-15T17:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42873 -->
Von dem Servo-Lenkhebel verwende ich jetzt das Loch. Dadurch ist der Hebel kürzer, der Servo muss weiter drehen und die Lenkung wird präziser.