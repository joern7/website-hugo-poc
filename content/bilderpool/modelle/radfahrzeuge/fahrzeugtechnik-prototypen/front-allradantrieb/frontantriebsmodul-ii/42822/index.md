---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung 2"
date: "2016-01-27T21:37:09"
picture: "70_verbesserte_Radaufhngung_2.jpg"
weight: "42"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42822
- /detailse1b3.html
imported:
- "2019"
_4images_image_id: "42822"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42822 -->
Der Reifen wird noch ein wenig weiter aufgespannt, und als Felge dient eine Sternlasche. Sie ist mit zwei Scheiben auf der Achse befestigt, einem Lagerstück-Unterteil ohne Anschlag.