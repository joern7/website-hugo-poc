---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul27.jpg"
weight: "27"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42601
- /details250b.html
imported:
- "2019"
_4images_image_id: "42601"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42601 -->
Wie schon gesagt, die drei oberen drei Förder-Kettenglieder geben dem Rad im oberebBereich Führung, indem sie zwischen den Zahnrädern laufen.
Auch auf dieser Seite ist auf halber Höhe ein Förder-Kettenglied (nach links zeigend) zur Fixierung der Kette an der Gelenkwürfel-Klaue. Das Förder-Kettenglied darunter (nach rechts zeigend) hat keine Funktion und kann durch eion normales ersetzt werden.
Auf dieser Seite ist die Kett unten mit dem normalen Ende im BS15. Dieses Ende ist ja etwas schmaler als das andere und deswegen kann es komplett in die Nut geschoben werden. Der S-Riegel sichert die Kette gegen verschieben.