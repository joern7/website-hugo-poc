---
layout: "image"
title: "Frontantrieb II, Bodenfreiheit 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42579
- /details80c0.html
imported:
- "2019"
_4images_image_id: "42579"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42579 -->
Portalachse für monstermäßige Bodenfreiheit.