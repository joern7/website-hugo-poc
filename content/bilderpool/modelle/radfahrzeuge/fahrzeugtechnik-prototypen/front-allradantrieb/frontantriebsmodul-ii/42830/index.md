---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung-Details 5"
date: "2016-01-27T21:37:09"
picture: "80_verbesserte_Radaufhngung_-_Details_5.jpg"
weight: "50"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42830
- /detailsf666-3.html
imported:
- "2019"
_4images_image_id: "42830"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42830 -->
Die Spurstange ist jetzt aus drei Pleuelstangen zusammengesetzt und kann mit Hilfe der Riegelscheiben beliebig eingestellt werden. Mit einem Verbindungsstopfen ist sie in der Nut des zentralen BS7,5 befestigt. Nicht grade schön, aber sehr platzsparend.