---
layout: "image"
title: "Frontantrieb II, Zahnrad 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul33.jpg"
weight: "33"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42607
- /detailsa57a-2.html
imported:
- "2019"
_4images_image_id: "42607"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42607 -->
Oben noch mal die Einzelteile. Weil das Loch im Kardangelenk rechteckig ist, spreizt sich das dünne Ende des Stecker-Innenteils im Pneumatikschlauc etwas, und gibt im Kardangelenk recht guten Halt. Das Loch im Z10 ist ja quadratisch, und weil das Stecker-Innenteil am dicken Ende auch nicht überall rund ist (bei den Querlöchern), hält das auch ganz gut. Insgesamt also eine fast formschlüssige Verbindung.
Unten ist noch eine Alternative: Wenn man es schafft, ein längeres Stück Pneumatikschlauch auch über das dickere Ende des Stecker-Innenteils zu schieben (sehr mühsam), und dann noch zwei Lagen Tesafilm spendiert, kann man auch das Z10 aus einem Freilauf-Zwischenzahnrad verwenden.