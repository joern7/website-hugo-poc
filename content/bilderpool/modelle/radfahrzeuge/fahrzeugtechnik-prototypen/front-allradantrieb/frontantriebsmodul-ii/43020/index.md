---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Radaufhängung 2"
date: "2016-03-10T12:37:42"
picture: "97_nochmal_verbesserte_Radaufhngung_2.jpg"
weight: "61"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43020
- /detailsdab0-2.html
imported:
- "2019"
_4images_image_id: "43020"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43020 -->
Ein schwarzes V-Rad sorgt dafür, dass die Freilafnabe nicht abrutscht.