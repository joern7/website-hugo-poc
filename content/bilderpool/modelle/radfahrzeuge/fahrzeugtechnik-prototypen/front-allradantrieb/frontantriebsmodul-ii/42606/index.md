---
layout: "image"
title: "Frontantrieb II, Zahnrad 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul32.jpg"
weight: "32"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42606
- /details1ef1-3.html
imported:
- "2019"
_4images_image_id: "42606"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42606 -->
So, jetzt sind wir in der Kreativkiste am Boden angelangt. Das Z10 passt stramm auf das dicke Ende eines Stecker-Innenteils. Das dünne Ende steckt in einem Stück Pneumatikschlauch und dann stramm in dem Kardangelenk.