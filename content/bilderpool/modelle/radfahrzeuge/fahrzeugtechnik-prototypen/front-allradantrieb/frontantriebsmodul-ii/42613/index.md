---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Variante 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul39.jpg"
weight: "39"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42613
- /details87b8-2.html
imported:
- "2019"
_4images_image_id: "42613"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42613 -->
Mit dieser Variante der Radaufhängung habe ich angefangen, aber damit war mir die Spurbreite und der Lenkrollradius noch zu groß. Ein Vorteil ist allerdings, dass die Felge besser gelagert ist.