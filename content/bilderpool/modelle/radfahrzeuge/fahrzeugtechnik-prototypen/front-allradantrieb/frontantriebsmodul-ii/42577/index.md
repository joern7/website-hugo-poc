---
layout: "image"
title: "Frontantrieb II, Gesamtansicht 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42577
- /details1beb-2.html
imported:
- "2019"
_4images_image_id: "42577"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42577 -->
Man sieht schon auf den ersten Blick, dass die Radnaben eine etwas ungewöhnliche Konstruktion sind. Allgemein sind viele Teile nicht in der Art und Weise zusammengebaut, wie von ft vorgesehen - teilweise stammen die Kombinationen tief aus der Kreativkiste.