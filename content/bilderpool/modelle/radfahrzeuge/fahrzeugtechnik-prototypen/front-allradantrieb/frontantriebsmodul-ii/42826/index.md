---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung-Details 1"
date: "2016-01-27T21:37:09"
picture: "80_verbesserte_Radaufhngung_-_Details_1.jpg"
weight: "46"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42826
- /details1c40-2.html
imported:
- "2019"
_4images_image_id: "42826"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42826 -->
Das ist die Radaufhängung an sich. Im Zentrum ein halber Gelenkstein, und ein Lagerstück-Unterteil ohne Anschlag als Radlager, Die schwarze Statikstrebe ist am Gelenkstein unbeweglich fixiert.