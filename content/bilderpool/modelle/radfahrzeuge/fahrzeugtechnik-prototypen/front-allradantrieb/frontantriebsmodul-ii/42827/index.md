---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung-Details 2"
date: "2016-01-27T21:37:09"
picture: "80_verbesserte_Radaufhngung_-_Details_2.jpg"
weight: "47"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42827
- /detailsac2b.html
imported:
- "2019"
_4images_image_id: "42827"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42827 -->
Als Zahnrad verwende ich jetzt das Z7 aus der fischertechnik-Trickfibel von Triceratops: http://www.ftcommunity.de/data/downloads/beschreibungen/trickfibel.pdf. Die Kettenglieder stzen zwar relativ stramm, sind aber noch mit einem Stück Schnur gesichert.