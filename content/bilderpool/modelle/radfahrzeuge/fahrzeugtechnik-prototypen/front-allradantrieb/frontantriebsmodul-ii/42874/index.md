---
layout: "image"
title: "Frontantrieb II verbessert, Lenkung 3"
date: "2016-02-15T17:21:21"
picture: "96_verbesserte_Lenkung_3.jpg"
weight: "57"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42874
- /details3aad.html
imported:
- "2019"
_4images_image_id: "42874"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-02-15T17:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42874 -->
Die Führung des Lenkbalkens erfolgt oben durch den Servo selber, unten durch zwei herausragende Verbinder, nach vorne hin durch den Servo-Lenkhebel und insgesamt noch einmal durch die Metallachse, auf der die Statikstrebe hin und her gleitet.