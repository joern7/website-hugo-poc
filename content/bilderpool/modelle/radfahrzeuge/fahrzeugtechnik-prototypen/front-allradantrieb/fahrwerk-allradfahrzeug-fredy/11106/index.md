---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug01.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11106
- /details954c.html
imported:
- "2019"
_4images_image_id: "11106"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11106 -->
Das Allrad Fahrzeug hat zwei Differenziale, ein vorne in der Lenkung und eins an der hinteren Achse. Der Power Motor im Heck treibt das hintere Differenzial an von dort aus wird die Drehbewegung, über eine Karadan Welle auf das vordere Diff übertragen.
Die Lenkung wird mit einem Mini Motor angetrieben,