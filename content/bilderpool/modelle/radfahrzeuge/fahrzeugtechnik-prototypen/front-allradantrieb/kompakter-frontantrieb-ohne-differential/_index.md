---
layout: "overview"
title: "Kompakter Frontantrieb ohne Differential"
date: 2020-02-22T07:52:15+01:00
legacy_id:
- /php/categories/2227
- /categoriesb71b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2227 --> 
Kleiner, stabiler Frontantrieb für die großen Traktorräder. Weil diese Vorderachse später für ein geländegängiges Fahrzeug dienen soll, wurde auf ein Differential verzichtet.