---
layout: "image"
title: "Vorderachse von unten"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11199
- /details495f.html
imported:
- "2019"
_4images_image_id: "11199"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11199 -->
Die grauen Streben bewegen sich nur durchs Federn, bleiben aber beim Lenken in konstanter Position.