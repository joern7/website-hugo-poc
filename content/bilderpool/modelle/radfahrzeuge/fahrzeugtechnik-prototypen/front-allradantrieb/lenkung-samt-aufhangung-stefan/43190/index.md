---
layout: "image"
title: "Aufhängepunkte"
date: "2016-03-21T13:33:37"
picture: "lenkung20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43190
- /details0583.html
imported:
- "2019"
_4images_image_id: "43190"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43190 -->
Auch wenn es auf dem Foto villeicht nicht so ausschaut, ist das Ganze doch recht stabil!