---
layout: "image"
title: "Reifen"
date: "2016-03-21T13:33:37"
picture: "lenkung17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43187
- /details7ea7.html
imported:
- "2019"
_4images_image_id: "43187"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43187 -->
Wie Martin Wanke habe auch ich ein Innenzahnrad Z30 in den Reifen gebaut. Dieses ist fix mit der Drehscheibe verbunden, welche eine vernünfige Lagerung durch die Freilaufnabe bietet. Außen am Z30 sind "V-Achsen 20 Rastachsen" mit jeweils zwei Abstandshülsen 3 angebracht, die den Reifen in seiner Positon sichern (einer ist links unten im Bild zu erkennen).