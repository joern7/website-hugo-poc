---
layout: "image"
title: "Reifen-2"
date: "2016-03-21T13:33:37"
picture: "lenkung18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43188
- /details4348-2.html
imported:
- "2019"
_4images_image_id: "43188"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43188 -->
Hier zu erkennen sind zwei von den Kunststoffachsen, die den Reifen recht stabil machen und einen sicheren Fahrbetrieb ermöblichen ;-).