---
layout: "image"
title: "Spurstange 2"
date: "2016-03-21T13:33:37"
picture: "lenkung07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43177
- /details3011-2.html
imported:
- "2019"
_4images_image_id: "43177"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43177 -->
Die Lenkung ist hier in ihrer Endpositon und zwischen Servohebel und Spurstange ist kein Spalt mehr...