---
layout: "image"
title: "Achsschenkel 6"
date: "2016-03-21T13:33:37"
picture: "lenkung15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43185
- /detailsa094-2.html
imported:
- "2019"
_4images_image_id: "43185"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43185 -->
Ein Abstandsring 3, der ca. zur Hälfte im Gelenkwürfel steckt (einfach bis zum Anschlag hineingeschoben) dient der richtigen Positon des weißen Kegelzahnrades.