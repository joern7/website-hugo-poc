---
layout: "image"
title: "Achsschenkel 4"
date: "2016-03-21T13:33:37"
picture: "lenkung13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43183
- /details2dba.html
imported:
- "2019"
_4images_image_id: "43183"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43183 -->
Mit ein Bisschen Herumspielen kommt man auf diese Lösung mit Winkelstein 15, BS 7,5, Klemmhülse 7,5... Ob alles wirklich perfekt ins Raster passt habe ich nicht nachgerechnet, aber beim Zusammenstecken klemmte nichts, daher muss es doch recht "genau" hinkommen.