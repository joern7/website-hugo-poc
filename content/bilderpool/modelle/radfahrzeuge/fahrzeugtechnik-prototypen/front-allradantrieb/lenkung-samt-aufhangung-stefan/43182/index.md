---
layout: "image"
title: "Achsschenkel 3"
date: "2016-03-21T13:33:37"
picture: "lenkung12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43182
- /details621b.html
imported:
- "2019"
_4images_image_id: "43182"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43182 -->
Die Statilstrebe 15 links verhindert, dass sich die Spurstange bei Belastung verschiebt. Die Statikstrebe 15, die vom Z10 zum BS 7,5 geht, dient der besseren Stabilität.