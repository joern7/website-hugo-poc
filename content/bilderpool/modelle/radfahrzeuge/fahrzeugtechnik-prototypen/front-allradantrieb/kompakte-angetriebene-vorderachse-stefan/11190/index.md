---
layout: "image"
title: "Gesamtansicht"
date: "2007-07-22T18:50:00"
picture: "kompakteangetriebenevorderachse1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11190
- /detailsd4c4.html
imported:
- "2019"
_4images_image_id: "11190"
_4images_cat_id: "1012"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T18:50:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11190 -->
Das Rastachsen- und Rastkardan-Gefummelt ist ja meist sowohl zu breit als auch zu schwach, ergo bin ich beim Reibradantrieb geblieben. Diese Variante baut aber niedriger, weil keine Räder 14 als Reibrad genutzt werden. Wenn ich es noch schaffe, diese Konstruktion so innerhalb eines Vorderwagens aufzuhängen, dann ist die Achse mitsamt Reifen 60 auch nur ca. 150 mm breit. Die Räder werden an vier Streben gehalten und gelenkt. Die beiden Manihots müssten dann in Serie geschaltet werden, wodurch sich ein elektrisches Differential ergibt.