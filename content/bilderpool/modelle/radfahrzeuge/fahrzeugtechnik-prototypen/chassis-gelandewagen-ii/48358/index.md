---
layout: "image"
title: "Vorderradaufhängung 7"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii37.jpg"
weight: "37"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48358
- /detailsd1b6.html
imported:
- "2019"
_4images_image_id: "48358"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48358 -->
Die Pleuelstange wird nur von der Klemme 5 gegen den Zug nach innen gehalten. Das ist leider eine echte Schwachstelle. Der Servo kann die Spurstange aus der Vorderradaufhängung herausziehen, wenn die Lenkkraft zu groß wird.