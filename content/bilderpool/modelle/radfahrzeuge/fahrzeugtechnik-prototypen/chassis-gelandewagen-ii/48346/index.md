---
layout: "image"
title: "Vorderachse 2"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii25.jpg"
weight: "25"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48346
- /detailsd81f-2.html
imported:
- "2019"
_4images_image_id: "48346"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48346 -->
Es sind etliche rote Kleinteile verbaut, die das Lager für das Differential stablilisieren. Und das funktioniert sehr gut, da ratscht und springt nichts, auch unter Last.