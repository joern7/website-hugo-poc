---
layout: "image"
title: "Vorderachse 5"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii28.jpg"
weight: "28"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48349
- /details5a73.html
imported:
- "2019"
_4images_image_id: "48349"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48349 -->
Ein erster Blick auf die Innereien der Vorderräder.