---
layout: "image"
title: "Rampe 2"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48329
- /details71cd.html
imported:
- "2019"
_4images_image_id: "48329"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48329 -->
Allerdings ist die Rampe mit Gummi-Raupenbändern bespannt. Gummiräder auf Gummi-Raupenbändern reiben besser als Gummiräder auf der Plasik-Platte, und ich denke, so kommt man einer Unimog-Teststrecke etwas näher, wo ja Gummi auf Beton reibt.
Das Video gibt's unter https://youtu.be/Q2r4f7Y7fNI