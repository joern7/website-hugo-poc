---
layout: "image"
title: "Nachtrag Wellenversteifung"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii39.jpg"
weight: "39"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48360
- /details1358-2.html
imported:
- "2019"
_4images_image_id: "48360"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48360 -->
So, hier noch die Verbesserung am Mitteldifferential, Die Welle kann jetzt nicht mehr durchbiegen, und die Kette springt nicht mehr über die Zahnräder (das war im Video ja deutlich zu hören).)