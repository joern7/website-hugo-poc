---
layout: "image"
title: "Vorderachse 4"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii27.jpg"
weight: "27"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48348
- /details762f.html
imported:
- "2019"
_4images_image_id: "48348"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48348 -->
Widmen wir uns jetzt der Vorderräder.