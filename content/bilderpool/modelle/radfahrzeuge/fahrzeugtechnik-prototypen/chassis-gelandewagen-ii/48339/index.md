---
layout: "image"
title: "Bodenfreiheit links"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii18.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48339
- /detailsaef0.html
imported:
- "2019"
_4images_image_id: "48339"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48339 -->
Die Bodenfreiheit beträgt ca. 25mm.