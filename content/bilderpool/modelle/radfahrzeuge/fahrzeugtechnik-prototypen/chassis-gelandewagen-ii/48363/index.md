---
layout: "image"
title: "Versuch mit S-Motor - Differentialsperre"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii42.jpg"
weight: "42"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48363
- /details6cfc-2.html
imported:
- "2019"
_4images_image_id: "48363"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48363 -->
Dafür war aber noch Platz für eine Differentialsperre am Mitteldifferential. War aber auch nur ein Versuch. Die Scheibe sitzt schräg und sollte über den Hebel mit dem grauen Scharnier und dem roten Zwischenstück die beiden Z10 an das Differential und das Z20 drücken, und somit das Differential "kurzschließen". Hat aber nicht funktioniert.