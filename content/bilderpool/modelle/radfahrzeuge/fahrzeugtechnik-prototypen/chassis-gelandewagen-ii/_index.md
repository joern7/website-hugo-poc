---
layout: "overview"
title: "Chassis für Geländewagen II"
date: 2020-02-22T07:53:19+01:00
legacy_id:
- /php/categories/3542
- /categoriesbcd7.html
- /categories86ed.html
- /categoriesedf4.html
- /categories49a9-2.html
- /categoriesbf73.html
- /categories3562.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3542 --> 
Allradantrieb mit Portalachsen und den Reifen 65, alles ft-pur.
Erklimmt die 45°-Rampe.
Video unter https://youtu.be/Q2r4f7Y7fNI