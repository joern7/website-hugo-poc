---
layout: "image"
title: "Vorderradaufhängung 6"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii36.jpg"
weight: "36"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48357
- /detailsf907.html
imported:
- "2019"
_4images_image_id: "48357"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48357 -->
Hier ist die Vorderradaufhängung wieder angebaut, aber ohne Rad.