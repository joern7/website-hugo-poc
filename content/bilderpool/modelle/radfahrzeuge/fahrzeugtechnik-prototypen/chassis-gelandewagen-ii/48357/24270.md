---
layout: "comment"
hidden: true
title: "24270"
date: "2018-11-05T21:36:26"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Mit ft verbrachte Zeit ist ja niemals verlorene Zeit - aber das sind schon eher Monate als Wochen, die für dieses Modell ins Land gegangen sind. Mit Unterbrechung sogar mehr als ein Jahr. Schade nur, dass die langen BSB-Schubstangen so selten sind.