---
layout: "image"
title: "Hinterachse 3"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii22.jpg"
weight: "22"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48343
- /details3430.html
imported:
- "2019"
_4images_image_id: "48343"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48343 -->
Die etwas rausgeschobenen BS7,5 und BS5 verihndern, dass sich der senkrecht stehende BS7,5 und damit die Radaufhängung verschiebt.