---
layout: "image"
title: "Gesamtansicht 1"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48322
- /details7208.html
imported:
- "2019"
_4images_image_id: "48322"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48322 -->
Nachdem ich es mit den Traktorreifen 80 schon hinbekommen habe, wollte ich mal versuchen, mit den Reifen 65 einen Allradantrieb mit Portalachsen zu bauen.
Es ist gelungen, auch halbwegs im ft-typischen Maßstab zu bleiben, aber es hat wieder einiges an Fantasie bedurft, um alles ft-pur zu halten. 
Zusätzlich soll die 45°-Rampe aus dem Wettbewerb http://forum.ftcommunity.de/viewtopic.php?f=19&t=441 gemeistert werden.