---
layout: "image"
title: "Sperrdifferential(die ander Seite)"
date: "2006-03-12T10:51:05"
picture: "P3110003.jpg"
weight: "2"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/5855
- /details3647.html
imported:
- "2019"
_4images_image_id: "5855"
_4images_cat_id: "505"
_4images_user_id: "366"
_4images_image_date: "2006-03-12T10:51:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5855 -->
Ich habe hier auf die schnelle mal ein motorisiertes Sperrdifferential gebaut. Das hat einfach den Vorteil, das man das D. z.B.über den Computer steuern kann. Leider ist es aber nur eine 0%auf100%-sperre.