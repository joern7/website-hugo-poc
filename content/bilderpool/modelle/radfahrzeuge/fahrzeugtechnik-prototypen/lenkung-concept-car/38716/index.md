---
layout: "image"
title: "Lenkung zusammengebaut"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar02.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38716
- /details638c.html
imported:
- "2019"
_4images_image_id: "38716"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38716 -->
So sieht es dann als fertige Lenkung aus. Hinzugekommen ist noch der Lenkhebel und das Lenkgestänge.