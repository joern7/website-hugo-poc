---
layout: "image"
title: "03"
date: "2008-03-22T20:53:45"
picture: "03.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14011
- /detailseef2.html
imported:
- "2019"
_4images_image_id: "14011"
_4images_cat_id: "1139"
_4images_user_id: "327"
_4images_image_date: "2008-03-22T20:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14011 -->
Ursprünglich wollte ich einfach nur einen (großen) Anhänger mit drei Achsen bauen, aber dazu musste ich mir erst mal eine möglichst tricky Federung ausdenken. Aus dem Anhänger ist bis jetzt nichts geworden, ich bin immer noch am forschen nach der besten Federung... Naja, ich bin halt eher ein Prototypen/Fahrwerkskomponenten-Konstrukteuer...