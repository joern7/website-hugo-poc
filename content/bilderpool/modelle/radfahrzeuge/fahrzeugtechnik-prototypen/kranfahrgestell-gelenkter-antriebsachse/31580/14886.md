---
layout: "comment"
hidden: true
title: "14886"
date: "2011-08-18T22:45:40"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Hallo Harald ... richtig, das ft-Kardan ist nicht so pralle. Im Leerlauf reicht es, ich muss mal sehen, wie es mit ein bisschen Belastung fährt. 

Bei eBay verkauft  ein Chinese auch "richtige" Kardangelenke (http://cgi.ebay.de/lIl-CNC-Wellenkupplung-Kardan-voll-Metal-4mm-4mm-Welle-/180539097044?pt=RC_Modellbau&hash=item2a08f7ffd4) ... falls es sich lohnen sollte, werd ich mich damit noch eindecken. Aber gleich sechs Stück kaufen für drei Achsen, das tu ich dann doch nicht auf Verdacht.