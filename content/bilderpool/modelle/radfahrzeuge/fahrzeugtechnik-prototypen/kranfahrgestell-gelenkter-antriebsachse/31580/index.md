---
layout: "image"
title: "Skizze: Angetriebene Lenkachse, Breite 150mm"
date: "2011-08-15T01:21:24"
picture: "kranfahrgestell1.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/31580
- /details6078.html
imported:
- "2019"
_4images_image_id: "31580"
_4images_cat_id: "2353"
_4images_user_id: "9"
_4images_image_date: "2011-08-15T01:21:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31580 -->
So könnte es aussehen. 

Die beiden Achsen sind Clipsachsen 30mm.