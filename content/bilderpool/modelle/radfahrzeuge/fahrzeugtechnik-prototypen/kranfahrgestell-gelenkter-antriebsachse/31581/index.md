---
layout: "image"
title: "Skizze: Angetriebene Lenkachse, Breite 150mm"
date: "2011-08-15T01:21:24"
picture: "kranfahrgestell2.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/31581
- /details138f.html
imported:
- "2019"
_4images_image_id: "31581"
_4images_cat_id: "2353"
_4images_user_id: "9"
_4images_image_date: "2011-08-15T01:21:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31581 -->
So könnte es aussehen. 

Die beiden Streben sind I-30 und X-42,4 mit einer halben Lasche 21,6mm. Dank an Triceratops' Streben-Tabelle. Morgen geht's weiter.