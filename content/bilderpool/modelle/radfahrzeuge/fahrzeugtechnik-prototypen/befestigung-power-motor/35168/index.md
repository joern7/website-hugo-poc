---
layout: "image"
title: "Extra Befestigung Power-Motor 2"
date: "2012-07-15T19:48:00"
picture: "bevestigungpowermotor2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/35168
- /detailsf740.html
imported:
- "2019"
_4images_image_id: "35168"
_4images_cat_id: "2606"
_4images_user_id: "144"
_4images_image_date: "2012-07-15T19:48:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35168 -->
Werend eine schlaflozen nacht, kam mir die idee:
wie auf einfacherweise der Power-Motor eine extra befestigung geben.

- man prest 1 Abstanring 3,75 in der Gelenkwürfel Klaue
- steckt der Zwischenstecker durch
- prest ein zweiten Abstandsring in der andere zeite der Klaue
- schiebt diesen kombi auf ein Baustein 7,5
- schiebt dies auf ein Baustein 5
- schiebt das ganse mit ein Federnocken auf ein Baustein oder bauplatte