---
layout: "image"
title: "8"
date: "2010-07-05T16:39:58"
picture: "solarflitzer8.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27685
- /details58dc.html
imported:
- "2019"
_4images_image_id: "27685"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27685 -->
In voller Fahrt!