---
layout: "image"
title: "15"
date: "2010-07-06T17:38:34"
picture: "solarflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27705
- /detailsf006.html
imported:
- "2019"
_4images_image_id: "27705"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27705 -->
Und dann einmal von unten. Man sieht, es ist kaum was dran, nur Solarzellen, Motor, Räder und Drahtgestell.