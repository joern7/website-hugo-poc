---
layout: "image"
title: "13"
date: "2010-07-06T17:38:34"
picture: "solarflitzer4.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27703
- /detailsdf31-3.html
imported:
- "2019"
_4images_image_id: "27703"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27703 -->
Das Schneckengetriebe, nicht von FT, sondern  aus dem Set für 15 Euro von www.artefact.de, in dem auch die vier Solarzellen und der Motor enthalten waren. Schluckt aber viel Kraft, Ich baue es wieder ab.