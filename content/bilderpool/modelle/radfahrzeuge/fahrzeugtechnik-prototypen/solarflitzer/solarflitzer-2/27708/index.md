---
layout: "image"
title: "18"
date: "2010-07-06T17:38:34"
picture: "solarflitzer9.jpg"
weight: "9"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27708
- /detailsbd8d.html
imported:
- "2019"
_4images_image_id: "27708"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27708 -->
Das leider nicht so gut funktionierende Schneckengetriebe.