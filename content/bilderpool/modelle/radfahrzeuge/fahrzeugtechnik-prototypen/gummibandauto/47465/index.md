---
layout: "image"
title: "Der Prototyp"
date: "2018-04-19T20:19:22"
picture: "gummibandauto1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47465
- /detailscc85.html
imported:
- "2019"
_4images_image_id: "47465"
_4images_cat_id: "3506"
_4images_user_id: "1557"
_4images_image_date: "2018-04-19T20:19:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47465 -->
Die Aufgabe für die 5. Klasse lautet: "Baut innerhalb von 7 Tagen aus einem Tetra-Pak ein Auto. Ein Tetra-Pak muss verwendet werden, alles Andere ist freigestellt. Ein Antrieb bringt eine Zusatznote, muss aber nicht sein." Gut, das sieht zunächst nach einer entspannten Hausarbeit aus: 4 Holzscheiben, 1 Schaschlik-Spieß und der Getränkekarton, fertig ist die Rostlaube. Bleibt "nur" noch der Antrieb. Fotos aus dem Internet dienen als Ideenspender und Konstruktionsvorlage. Um herauszufinden wie gut das mit dem Gummibandmotor prinzipiell funktioniert, wird zunächst ein Prototyp improvisiert - fischertechnik ist da ganz naheliegend.

Das ist nun der Prototyp. Schnell ein paar Teile gegriffen und halbwegs brauchbar zusammengesteckt. Die Antriebsachse klemmt grundsätzlich etwas (ist wohl einen Hauch zu dick, die 170er Achse) und trotzdem kommt das Gestell mit derart "schleifender Handbremse" auf Anhieb etwa 4 Meter weit, bei etwa 0,8 Meter Beschleunigungsweg. Der Nachwuchs ist mit Funktion und Fahrleistung zufrieden.

Zur Funktion: Die Klemmhülse (nicht Klemmbuchse!) auf der Antriebsachse ist der Mitnehmer für das Gummi. Das wird von Hand eingehakt und anschließend die Achse rückwärts gedreht bis das Gummiband schön straff gespannt ist. Lässt man das Auto auf dem Boden los, beschleunigt das gespannte Gummi die Fuhre vorwärts und fällt dann nahezu entspannt vom Mitnehmer runter. Ab jetzt rollt das Wägelchen frei und wird nennenswert nur durch die Lagerreibung gebremst. Auf der Antriebsachse sollten griffige Reifen montiert sein.