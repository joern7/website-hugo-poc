---
layout: "comment"
hidden: true
title: "24040"
date: "2018-04-20T13:49:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das wäre doch in der Tat ein schöner Konstruktionswettbewerb! Welches Auto, bestehend aus wie wenigen Teilen, fährt mit einem Gummi möglichst weit? Müssen wir da die Gummis "normieren"?

Gruß,
Stefan