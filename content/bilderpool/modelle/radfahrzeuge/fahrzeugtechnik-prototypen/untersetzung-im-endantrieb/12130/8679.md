---
layout: "comment"
hidden: true
title: "8679"
date: "2009-03-06T10:29:31"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Genau so isses. Die äußeren Lagersteine ersetzen durch Gelenkwürfel, und dann nur noch eine hinreichend stabile Befestigung für die Antriebswelle finden.

Gruß,
Harald