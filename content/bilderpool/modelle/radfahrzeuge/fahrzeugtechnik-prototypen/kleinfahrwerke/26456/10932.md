---
layout: "comment"
hidden: true
title: "10932"
date: "2010-02-17T08:41:43"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich wusste, dass die flexiblen durchsichtigen Prüfriegel nicht mehr hergestellt werden, aber die 8-mm-Riegel auch nicht mehr? Es gibt oder gab zumindest mal S-Riegel 4 mm ohne Griff, ganz flach, sowie die schwarzen Statikstopfen. Hab ich was vergessen?

Gruß,
Stefan