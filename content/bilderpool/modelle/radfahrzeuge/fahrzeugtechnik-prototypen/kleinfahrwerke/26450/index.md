---
layout: "image"
title: "Allradantrieb, gefedert"
date: "2010-02-15T21:05:56"
picture: "kleinfahrwerke6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26450
- /detailsa4b1-3.html
imported:
- "2019"
_4images_image_id: "26450"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26450 -->
Der MiniMot treibt das feine Ritzel eines Z10 an, dessen grobe Zähne die unten durchgehende Antriebswelle betreibt.