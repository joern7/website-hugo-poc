---
layout: "image"
title: "Eine angetriebene Achse, ungefedert"
date: "2010-02-15T21:05:55"
picture: "kleinfahrwerke4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26448
- /details8af5.html
imported:
- "2019"
_4images_image_id: "26448"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26448 -->
Hier sieht man, wie die Zahnräder ineinander greifen. Die Räder unterhalb des Motors sind tatsächlich nur über einen BS30 befestigt, der an nichts anderem hängt als über einen BS5 mit zwei Zapfen am Motor selbst. Hält. :-)