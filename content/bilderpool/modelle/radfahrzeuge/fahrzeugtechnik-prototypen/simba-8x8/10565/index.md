---
layout: "image"
title: "Simba02.JPG"
date: "2007-05-30T19:03:46"
picture: "Simba02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/10565
- /details4a80.html
imported:
- "2019"
_4images_image_id: "10565"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2007-05-30T19:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10565 -->
Wo ich gerade dabei bin, kann ich ja auch gleich alle vier Achsen lenkbar machen...