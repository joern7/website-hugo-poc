---
layout: "image"
title: "Simba13.jpg"
date: "2008-08-03T11:49:12"
picture: "Simba13.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14993
- /details031b-2.html
imported:
- "2019"
_4images_image_id: "14993"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T11:49:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14993 -->
Das Lenkgetriebe bei ausgebautem Antriebsstrang.

Die Räder an den äußeren Achsen müssen beim Lenken schwächer einschlagen als die inneren. Deshalb gibt es zwei Lenkwellen; eine für die äußeren und eine für die inneren Achsen, die hier links und rechts zu sehen sind. Zwischen den Lenkwellen wird mit 3:4 übersetzt, was halt ungefähr der Geometrie des ganzen Fahrzeugs entspricht: Vom Mittelpunkt der einen Fahrzeughälfte aus gesehen sind die Achsen der anderen Hälfte etwa im Verhältnis 3/4 entfernt.

Damit die Räder gegenläufig einschlagen, muss die Drehrichtung der Lenkwellen zwischen vorderer und hinterer Hälfte umgekehrt werden. Das wird mit den Kegelradsätzen erledigt, die links unten teilweise zu sehen sind.