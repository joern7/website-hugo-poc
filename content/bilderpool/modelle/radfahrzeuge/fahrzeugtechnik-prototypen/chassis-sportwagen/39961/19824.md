---
layout: "comment"
hidden: true
title: "19824"
date: "2014-12-22T23:06:06"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Boah, Hammer! Super raffiniert und kreativ! Vom Lenkradantrieb über die Kegelräder bis zum Elektromechanik-"Mitnehmer". Sehr stark.
Gruß, Stefan