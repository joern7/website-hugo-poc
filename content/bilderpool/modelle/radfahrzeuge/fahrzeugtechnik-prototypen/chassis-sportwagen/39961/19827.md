---
layout: "comment"
hidden: true
title: "19827"
date: "2014-12-23T08:20:51"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Stümmt! Hammer! Jetzt sag bloß noch, dass der Federkontakt bei Geradeauslauf zart einrastet.

Bei mir wäre das Taschenmesser schon längst in der Ecke gelandet und ich hätte irgendwas mit der Kettensäge passend gemacht. 

Gruß,
Harald