---
layout: "image"
title: "Sitz"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39964
- /details8ede.html
imported:
- "2019"
_4images_image_id: "39964"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39964 -->
Die Sitze sollten sportlich aussehen.