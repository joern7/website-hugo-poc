---
layout: "image"
title: "mit Powermotor, noch mal von oben"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_3_klein.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40082
- /details37fc.html
imported:
- "2019"
_4images_image_id: "40082"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40082 -->
Die meisten Teile sind jetzt auch gegen verrutschen gesichert.