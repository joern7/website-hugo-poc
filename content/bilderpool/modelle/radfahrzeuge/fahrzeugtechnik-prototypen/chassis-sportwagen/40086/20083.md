---
layout: "comment"
hidden: true
title: "20083"
date: "2015-01-21T13:02:05"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Martin,
auch dieses Lenkgetriebe von dir ist eine interessante Idee. Ich habe es nachdem mein ftD wieder läuft für mein Archiv mal nachgebaut. Da einzelne 3D-Teile - hier z.B. Rastschnecke und Differentialrad des ftD geometrisch und rastend masslich fehlerhaft sind, ist diese Lösung mit dem ftD allein nicht findbar ...
Gruß Udo2