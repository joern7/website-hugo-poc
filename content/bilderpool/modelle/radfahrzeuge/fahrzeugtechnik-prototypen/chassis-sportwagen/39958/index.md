---
layout: "image"
title: "Lenkung maximaler Linkseinschlag"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39958
- /detailse7ab.html
imported:
- "2019"
_4images_image_id: "39958"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39958 -->
Die Räder berühren grade so eben die Feder des Minnimotors. Das ist blöd, aber im nächsten Bild gibt es vielleicht einen Lösungsansatz dafür.