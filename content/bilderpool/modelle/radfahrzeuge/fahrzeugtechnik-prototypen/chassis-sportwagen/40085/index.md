---
layout: "image"
title: "mit Powermotor, noch mal von der Seite"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_6_klein.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40085
- /details4234.html
imported:
- "2019"
_4images_image_id: "40085"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40085 -->
Das flache Profil ist erhalten geblieben.