---
layout: "image"
title: "Lenkstellungsabgriff mit Potentiometer"
date: "2014-03-16T17:58:36"
picture: "S1060002.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38463
- /details9baa.html
imported:
- "2019"
_4images_image_id: "38463"
_4images_cat_id: "2868"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38463 -->
Das Poti hat endlich einen guten Platz gefunden. Angetrieben wird es von der Lenkmecahnik unter dem Fahrzeug.