---
layout: "image"
title: "Einzelradaufhängung parallel gefedert 02"
date: "2008-03-26T21:50:31"
picture: "02.jpg"
weight: "2"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14118
- /detailseb7a-2.html
imported:
- "2019"
_4images_image_id: "14118"
_4images_cat_id: "1297"
_4images_user_id: "327"
_4images_image_date: "2008-03-26T21:50:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14118 -->
Hier sieht man schön die Feder, sie ich hier zusätzlich zwischen den Parallellenkern angebaut habe.

Wenn man das Ganze noch kompakter haben will, kann man die Feder natürlich auch weglassen und die ganze Federwirkung den Pneumatikzylinder übernehmen lassen.

Die Achsgeometrie ist über den Anlenkpunkt Rollenlager/Zylinder veränderbar.

In der gezeigten Einstellung schwingt das ganze fast exact parallel, dafür ist der Federweg aber etwas geringer als im oberen oder linken Loch.