---
layout: "image"
title: "Drehstab1.jpg"
date: "2004-09-09T23:17:23"
picture: "Drehstab1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Federung", "Achse"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2575
- /detailsb493.html
imported:
- "2019"
_4images_image_id: "2575"
_4images_cat_id: "279"
_4images_user_id: "4"
_4images_image_date: "2004-09-09T23:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2575 -->
Burgerman hat da nach einer Achse mit Drehstabfederung gefragt...