---
layout: "image"
title: "06"
date: "2008-03-24T20:28:32"
picture: "06.jpg"
weight: "6"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14093
- /detailsf366-2.html
imported:
- "2019"
_4images_image_id: "14093"
_4images_cat_id: "1292"
_4images_user_id: "327"
_4images_image_date: "2008-03-24T20:28:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14093 -->
