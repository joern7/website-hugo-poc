---
layout: "image"
title: "Detail"
date: "2014-05-09T17:28:26"
picture: "protofrontachse3.jpg"
weight: "3"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38772
- /detailsf013.html
imported:
- "2019"
_4images_image_id: "38772"
_4images_cat_id: "2898"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38772 -->
Es ist noch nicht deutlich was oben und was unten ist