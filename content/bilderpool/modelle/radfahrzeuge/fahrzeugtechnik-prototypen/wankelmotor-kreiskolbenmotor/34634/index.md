---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor2.jpg"
weight: "2"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/34634
- /details6073.html
imported:
- "2019"
_4images_image_id: "34634"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34634 -->
hier ist alles Beschriftet was man von Forne sehen kann. Exzenterwelle und ähnliche Bauteile hat der Motor natürlich auch, nur aus dieser Perspektive kan man sie nciht sehen.