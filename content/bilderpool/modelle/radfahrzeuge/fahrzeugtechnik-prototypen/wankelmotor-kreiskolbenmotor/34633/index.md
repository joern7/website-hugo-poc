---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor1.jpg"
weight: "1"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/34633
- /details41c3.html
imported:
- "2019"
_4images_image_id: "34633"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34633 -->
Die Motorteile sind aus Holz gebaut. Auf den Vier Schildern stehen die Arbeitstakte, denn der Wankel ist ein viertaktmotor und neben dem jeweiligen Schild ist ein Lämpchen, das Leuchtet wenn der Takt gerade stattfindet. Die Arbeitstakte finden immer am selben Ort satt, daher können die childer auch an dem Punkt angebracht sein, wo sich der Arbeitstakt abspielt. Die Arbeitstakte (AT) finden in allen drei Kammern immer gleichzeitig statt. Mit meiner Anzeige der AT beziehe ich mcih aber nur auf eine Kammer, das gleiche gilt für die Schaltung der Zündkerze. Ansonsten würde der Beobachter völlig durcheinander kommen.
So wird man den Motor in der Prüfung sehen!