---
layout: "comment"
hidden: true
title: "3572"
date: "2007-06-28T16:27:11"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Rutscht der bearbeitete BS7,5 nicht bei Belastung der Lenkung, durch z.B. einen Stein vor einem Rad weg? Ich würde es eventuell etwas mit Heißklebe befestigen.

Gruß fitec