---
layout: "image"
title: "Mulde gekippt 1"
date: "2007-06-29T17:59:35"
picture: "DSCN1373.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10963
- /details7fb5.html
imported:
- "2019"
_4images_image_id: "10963"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-06-29T17:59:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10963 -->
Das Kippen geht nur im Handbetrieb, vorläufig.