---
layout: "image"
title: "Vorderansicht"
date: "2007-06-30T15:51:01"
picture: "DSCN1402.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10971
- /details8d2d.html
imported:
- "2019"
_4images_image_id: "10971"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10971 -->
Die Befestigung der Radträger ist ähnlich wie beim 2 - achsigen Fahrgestell.