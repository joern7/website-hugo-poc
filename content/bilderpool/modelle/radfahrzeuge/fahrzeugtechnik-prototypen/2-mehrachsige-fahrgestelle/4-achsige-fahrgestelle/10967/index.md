---
layout: "image"
title: "Mechanik der hinteren Vorderräder"
date: "2007-06-30T15:51:01"
picture: "DSCN1397.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10967
- /details848f.html
imported:
- "2019"
_4images_image_id: "10967"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10967 -->
Die hintren beiden Vorderräder brauchen nicht so stark einlenken wie die vorderen. Deswegen habe ich hier ein Z10 gewählt.