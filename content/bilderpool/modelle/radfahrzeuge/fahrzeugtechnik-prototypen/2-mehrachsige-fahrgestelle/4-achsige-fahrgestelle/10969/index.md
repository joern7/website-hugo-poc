---
layout: "image"
title: "Spurstangenbefestigung"
date: "2007-06-30T15:51:01"
picture: "DSCN1399.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["32455"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10969
- /detailsbc19.html
imported:
- "2019"
_4images_image_id: "10969"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10969 -->
erste Versuche