---
layout: "image"
title: "radträger mit montierten Vorderrädern"
date: "2007-06-28T14:15:48"
picture: "DSCN1324.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10952
- /detailse63a-2.html
imported:
- "2019"
_4images_image_id: "10952"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-06-28T14:15:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10952 -->
Das kann man hier gut erkennen.