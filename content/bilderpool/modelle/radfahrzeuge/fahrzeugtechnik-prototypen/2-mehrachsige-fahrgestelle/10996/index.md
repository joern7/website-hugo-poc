---
layout: "image"
title: "Lenkung"
date: "2007-07-01T12:32:56"
picture: "DSCN1416.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10996
- /details16b5-2.html
imported:
- "2019"
_4images_image_id: "10996"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-07-01T12:32:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10996 -->
Habe noch einen Gimmick mit eingebaut. Sobald der Lenkmotor in Bewegung kommt dreht sich das Lenkrad mit.