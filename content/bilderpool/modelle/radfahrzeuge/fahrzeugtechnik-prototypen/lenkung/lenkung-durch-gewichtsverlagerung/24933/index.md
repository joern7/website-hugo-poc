---
layout: "image"
title: "Neigunsgausgleich"
date: "2009-09-19T21:24:10"
picture: "lenkungdurchgewichtsverlagerung4.jpg"
weight: "4"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/24933
- /details4516-2.html
imported:
- "2019"
_4images_image_id: "24933"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24933 -->
Durch den Lenkeinschlag der Hinterräder neigt sich das Fahrzeug auf die entsprechende Seite. Wären die Vorderräder starr mit dem Fahrzeig verbunden, würden sie auf der Kurven-Innenseite den Bodenkontakt verlieren. Um das zu verhinden, sind die Vorderräder und der Rest des Fahrzeuges mit einer Achse verbunden. Die Vorderräder gleichen so die Neigung automatisch aus.