---
layout: "image"
title: "Federung"
date: "2009-09-19T21:24:11"
picture: "lenkungdurchgewichtsverlagerung5.jpg"
weight: "5"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/24934
- /detailsfd94-2.html
imported:
- "2019"
_4images_image_id: "24934"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24934 -->
Die Vorderrad-Konstruktion ist mit einer horizontalen Achse im Gehäuse aufgehängt. Um den Bewegsungsspielraum zu begrenzen, fungiert eine schwarze Kunststoff-Rastachse (mit Unterstüzung durch zwei Federn) als Federung.