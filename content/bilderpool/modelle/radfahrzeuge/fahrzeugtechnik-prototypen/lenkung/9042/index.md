---
layout: "image"
title: "lenkung07.jpg"
date: "2007-02-16T18:29:43"
picture: "lenkung07.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9042
- /details9fd2-2.html
imported:
- "2019"
_4images_image_id: "9042"
_4images_cat_id: "824"
_4images_user_id: "453"
_4images_image_date: "2007-02-16T18:29:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9042 -->
Hier sieht man die beiden Zahnräder die, die Räder antreiben.
Im Hintergrund sieht man die Gewindestange die den Lenkeinschlag zuständig ist.