---
layout: "image"
title: "Rennwagen-Chassis 5"
date: "2010-03-29T19:03:25"
picture: "Rennwagen-Chassis_5.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26839
- /details9dd8.html
imported:
- "2019"
_4images_image_id: "26839"
_4images_cat_id: "1919"
_4images_user_id: "328"
_4images_image_date: "2010-03-29T19:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26839 -->
Die Ansicht von unten.

Die beiden Bausteine V 15 Eck rot (38240) hinter der Vorderachse sind für den notwendigen Freiraum für die Stecker zum Anschluss des Power-Motors erforderlich.

Die beiden Winkelsteine 10x15x15 rot (38423) vor dem Power-Motor verhindern, dass sich der Motor unter Last nach vorn und vom Differenzial wegschiebt. Der Motor wird im vorderen Bereich nur durch die Statikstrebe gehalten.