---
layout: "comment"
hidden: true
title: "11281"
date: "2010-03-30T08:56:27"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Alfred, es ist ein etwas komplexes Thema, welche Felgen 23 auf welchen Achsen halten. Meine Erfahrungen sind die folgenden:

- Speichenfelge 23 hält fest auf Metallachsen und auf allen Kunststoffachsen

- Rad 23 hält fest nur auf Metallachsen, nicht auf Kunststoffachsen

In diesem Modell hält die Speichenfelge sehr fest auf den Rastachsen. Ich habe verschiedene Achsen ausprobiert, und bei manchen klemmt es besser, bei manchen weniger. Hier im Modell hält es bei mir bombenfest.

Das Rad 23 wie in meinem Kleinwagen-Modell hält viel schlechter auf Kunststoffachsen. Da funktionieren eigentlich nur Metallachsen...

Gruß, Thomas