---
layout: "image"
title: "Getriebe 15"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen35.jpg"
weight: "35"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41032
- /details0314-3.html
imported:
- "2019"
_4images_image_id: "41032"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41032 -->
... und von hinten.