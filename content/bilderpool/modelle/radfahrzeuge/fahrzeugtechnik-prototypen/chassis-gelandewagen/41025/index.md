---
layout: "image"
title: "Getriebe 08"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen28.jpg"
weight: "28"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41025
- /detailsa4bc-2.html
imported:
- "2019"
_4images_image_id: "41025"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41025 -->
Hier ist der hintere davon ausgebaut. Der Abdeckstein 3,75 hinten "füllt" nur die Lücke und definiert dadurch das hier angewendete Raster.