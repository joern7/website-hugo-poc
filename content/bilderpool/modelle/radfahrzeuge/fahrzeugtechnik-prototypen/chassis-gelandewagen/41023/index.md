---
layout: "image"
title: "Getriebe 06"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen26.jpg"
weight: "26"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41023
- /details9791.html
imported:
- "2019"
_4images_image_id: "41023"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41023 -->
So ist auf der rechten Seite der Batteriekasten mit dem Chassis verbunden.