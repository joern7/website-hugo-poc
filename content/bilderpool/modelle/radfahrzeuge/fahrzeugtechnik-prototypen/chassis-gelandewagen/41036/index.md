---
layout: "image"
title: "Fernsteuerung 01"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen39.jpg"
weight: "39"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41036
- /details0717-2.html
imported:
- "2019"
_4images_image_id: "41036"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41036 -->
Ach ja - die Fernbedienung.