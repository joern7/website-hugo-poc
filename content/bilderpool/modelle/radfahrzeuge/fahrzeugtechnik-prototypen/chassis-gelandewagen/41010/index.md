---
layout: "image"
title: "Kabelanschluss"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41010
- /detailsf727-3.html
imported:
- "2019"
_4images_image_id: "41010"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41010 -->
Wenig berauschende Basteleien: 
- Kabelanschluss mit Zimmermannsnägeln (keine Angst, sie sind nicht reingehämmert) - ich wollte keinen Platz für Stecker spendieren.
- Verbindung zwischen dem Ritzel aus dem Winkelgetriebe und dem Schneckenende mit schwarzer Lagerbuchse, in die ich eine Plastikfolie geklemmt habe.