---
layout: "image"
title: "ABS 02"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen16.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41013
- /details50b5.html
imported:
- "2019"
_4images_image_id: "41013"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41013 -->
Hier noch mal ohne Ankerplatte. Die Bremswirkung ensteht hauptsächlich dadurch, dass der Spalt in der sich drehenden Klemme der Ankerplatte erlaubt, bis ganz an den Magnet heranzukommen und dann das Drehmoment groß sein muss und erst den Anker wieder vom Magnet losreißen muss, damit sich die Klemme sich weiter drehen kann.