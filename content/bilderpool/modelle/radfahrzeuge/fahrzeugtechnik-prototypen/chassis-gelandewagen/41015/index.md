---
layout: "image"
title: "Hinterradschwinge 01"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen18.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41015
- /details5e45-3.html
imported:
- "2019"
_4images_image_id: "41015"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41015 -->
Die Hinterachs-Schwinge besteht aus einem starren Differentialkäfig, der dann mit zwei Statikstreben als Längslenker sehr flexibel mit dem Chassis verbunden ist. Dadurch wird die Hinterachse sehr beweglich in vertikaler Richtung und für Drehbewegungen um die Längsachse, aber recht stabil gegen Längsverschiebungen. Und der Differentialkäfig kann sich unabhängig um die Hinterachse drehen, so dass das Kardangelenk auch nicht ganz genau in einer Flucht mit den Lagern der Längslenker liegen muss.