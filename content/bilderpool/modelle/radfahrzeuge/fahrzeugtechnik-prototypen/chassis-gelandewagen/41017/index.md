---
layout: "image"
title: "Hinterradschwinge 03"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen20.jpg"
weight: "20"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41017
- /details95a7.html
imported:
- "2019"
_4images_image_id: "41017"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41017 -->
Die Federn liegen nur lose am Batteriekasten an, damit die Achse noch weicher wird und damit das Chassis für eine gefälligere Optik hinten nicht zu hoch kommt (würde allerding etwas mehr Bodenfreiheit bringen...).