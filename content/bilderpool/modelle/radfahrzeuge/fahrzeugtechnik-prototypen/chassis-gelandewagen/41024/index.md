---
layout: "image"
title: "Getriebe 07"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen27.jpg"
weight: "27"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41024
- /detailsba49-2.html
imported:
- "2019"
_4images_image_id: "41024"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41024 -->
Die Achse 50 aus dem Winkelgetriebe ist in zwei Winkelsteinen 60° gelagert.