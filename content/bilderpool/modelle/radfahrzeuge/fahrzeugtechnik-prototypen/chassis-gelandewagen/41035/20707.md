---
layout: "comment"
hidden: true
title: "20707"
date: "2015-06-02T23:56:09"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Das funktioniert eigentlich ganz gut, beim ersten Bild dieser Serie ist ein Link zum Video, wo man das sehen kann. Das Drehmoment der Klemmverbindungen auf der Motorwelle reicht wahrscheinlich, weil der Durchmesser der Riegelscheibe einfach klein genug ist, Bei einem größeren Ritzel bräuchte man festere Verbindungen. 
Die Fein-Zahnräder musste ich tatsächlich sorgfältig gegenlagern, vorher haben sie unter Last "gerattert".