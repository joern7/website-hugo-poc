---
layout: "image"
title: "Getriebe 05"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen25.jpg"
weight: "25"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41022
- /details2912-2.html
imported:
- "2019"
_4images_image_id: "41022"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41022 -->
Das abgebaute Teil ist elementar. Es ist das Gegenlager für die Motorwelle, damit die Riegelscheibe immer fest auf dem Zahnrad sitzt. Der Distanzring auf der Motorwelle ganz rechts stützt sich in Längsrictung ab und verhindert, dass sich der Selklemmstift aus der Klemmkupplung löst.