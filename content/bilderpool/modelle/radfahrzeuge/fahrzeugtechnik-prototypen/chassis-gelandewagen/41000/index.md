---
layout: "image"
title: "Chassis für Geländewagen 03"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41000
- /detailsb75d-2.html
imported:
- "2019"
_4images_image_id: "41000"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41000 -->
Stromversorgung mit ftPowerblock mit 6 Mignonzellen. Der Powerblock ist integraler Bestandteil des Chassis.