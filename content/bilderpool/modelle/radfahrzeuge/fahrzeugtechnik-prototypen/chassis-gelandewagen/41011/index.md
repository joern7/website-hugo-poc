---
layout: "image"
title: "Von unten"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen14.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41011
- /details730e-2.html
imported:
- "2019"
_4images_image_id: "41011"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41011 -->
Hier der Unterboden. Die 120er-Strebe mit der L-Lasche sollen das Lager des Mitteldifferentials im Raster halten.Die rote Plastikachse stabilisiert die Aufhängungen der Hinterachs-Schwinge und das Kardangelenk.