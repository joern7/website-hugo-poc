---
layout: "comment"
hidden: true
title: "18438"
date: "2013-10-25T18:26:27"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tja, so geht das mit dem technischen Fortschritt!
Jetzt gehört nur noch ein elastisches Element irgendwo hinein, damit man das Lenkrad schon weiter drehen kann, während der Motor noch unterwegs ist.

Gruß,
Harald