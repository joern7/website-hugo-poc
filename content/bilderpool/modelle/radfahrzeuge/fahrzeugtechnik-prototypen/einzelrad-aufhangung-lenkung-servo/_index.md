---
layout: "overview"
title: "Einzelrad-Aufhängung, Lenkung mit Servo-Unterstützung"
date: 2020-02-22T07:53:02+01:00
legacy_id:
- /php/categories/2806
- /categoriesec36.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2806 --> 
gefedertes Lastwagen-Chassis, Vorderachse mit Einzelrad Aufhängung und Lenkung mit Servo Unterstützung