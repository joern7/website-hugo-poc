---
layout: "image"
title: "Vorderachse mit Einzelradaufhängung und Lenkung mit Servounterstützung 4"
date: "2013-10-25T00:40:03"
picture: "4_IMG_0331.jpg"
weight: "4"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Lenkung", "Servounterstützung", "Einzelradaufhängung", "Radaufhängung", "Dreieckslenker"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37770
- /detailsdeea.html
imported:
- "2019"
_4images_image_id: "37770"
_4images_cat_id: "2806"
_4images_user_id: "1752"
_4images_image_date: "2013-10-25T00:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37770 -->
Die Streben unten und oben sind so verbaut, dass sich ein ‚leichter‘ Sturz für die Vorderräder ergibt. Mit Gewicht auf dem Teil (Karrosserie etc) sieht die Sache jedoch schon weit weniger dramatisch aus.