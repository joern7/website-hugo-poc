---
layout: "image"
title: "Vorderachse mit Einzelradaufhängung und Lenkung mit Servounterstützung 1"
date: "2013-10-25T00:40:03"
picture: "1_IMG_0326.jpg"
weight: "1"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Lenkung", "Servounterstützung", "Einzelradaufhängung", "Radaufhängung", "Dreieckslenker"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37767
- /details9f34.html
imported:
- "2019"
_4images_image_id: "37767"
_4images_cat_id: "2806"
_4images_user_id: "1752"
_4images_image_date: "2013-10-25T00:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37767 -->
Prototyp für ein Lastwagen-Chassis. Die Vorderachse hat Einzelradaufhängung, und eine Servo unterstützte Lenkung. Gemeint ist, dass jede Bewegung am Lenkrad, direkt durch den Lenkmotor unterstützt wird. 
Die Hinterrad Aufhängung ist weniger perfekt, da gibt’s natürlich wesentlich augereiftere Versionen hier im Bilderpool..