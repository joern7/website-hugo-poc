---
layout: "image"
title: "EstherM"
date: "2018-07-20T18:39:16"
picture: "P1050045_klein.jpg"
weight: "7"
konstrukteure: 
- "Mausefallenauto: Antriebsfaden"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/47727
- /details90d8.html
imported:
- "2019"
_4images_image_id: "47727"
_4images_cat_id: "297"
_4images_user_id: "2781"
_4images_image_date: "2018-07-20T18:39:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47727 -->
Hier ist im Detail dargestellt, wie der Antriebsfaden auf der Achse aufgewickelt ist. Das ist ein bisschen Fummelei, ermöglicht aber einen "Freilauf", wenn der Faden abgewickelt ist.