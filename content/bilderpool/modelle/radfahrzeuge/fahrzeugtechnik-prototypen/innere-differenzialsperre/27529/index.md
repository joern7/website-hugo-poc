---
layout: "image"
title: "Innere Differenzialsperre"
date: "2010-06-17T20:22:48"
picture: "Innere_Differenzialsperre.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/27529
- /details6f1c-2.html
imported:
- "2019"
_4images_image_id: "27529"
_4images_cat_id: "1974"
_4images_user_id: "328"
_4images_image_date: "2010-06-17T20:22:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27529 -->
Oftmals wünsche ich mir einen klassischen Achsantrieb über Kardanwelle, Kegelrad und das normale Differenzial, allerdings sollten die Räder STARR auf der Achse sitzen, das Differenzial also INTERN gesperrt sein. Gerade bei Geländewagen benötige ich das oft, wo es auf saubere Kurvenfahrt nicht so ankommt.

Meinen Wunsch gab es schon vor 5 Jahren, und wir diskutierten damals im Forum darüber. Da ging es u.a. um Modifikationen von FT-Teilen und um das Zukleistern des gesamten Innenlebens. Eine "richtige" Lösung war für mich leider nicht dabei:

http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=117

Heute bin ich das Thema noch mal angegangen, und siehe da, die Lösung ist SO EINFACH UND NAHELIEGEND:

Man press einfach seitlich 2 weitere Differential-Planetenräder weiß (31412) neben die 2 normalen Planetenräder, so dass ein Verbund aus 4 ineinander verzahnten Planetenrädern entsteht! Der Platz dafür ist vorhanden. Beim Einbau ist zu beachten, dass beide zusätzlichen Räder von beiden Seiten gleichzeitig und synchron neben die beiden anderen Räder gepresst werden! Es spannt ein wenig, geht aber komplett ohne Werkzeug.

Jetzt kann man ganz normal und wie immer an beiden Seiten die 2 Differential-Abtriebsräder Z 10 weiß (31413) mit Rastachsen reinstecken. Durch den Verbund aus nun 6 ineinander verzahnten Kegelrädern entsteht ein fester Klotz im inneren des Differenzialkäfigs, der eine starre Verbindung zwischen Käfig und Seitenwellen bzw. Rädern herstellt!

Alles nach dem FT-Reinheitsgebot! Keine Modifikationen, Fremdteile oder Kleber! Einfach genial, wie ich finde.