---
layout: "image"
title: "Gesamtansicht von oben"
date: "2015-04-03T10:00:06"
picture: "Foto_12.jpg"
weight: "11"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40705
- /detailscd0b-2.html
imported:
- "2019"
_4images_image_id: "40705"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40705 -->
Deutlich zu erkennen, die alte Achse ist sehr massiv und schwer.