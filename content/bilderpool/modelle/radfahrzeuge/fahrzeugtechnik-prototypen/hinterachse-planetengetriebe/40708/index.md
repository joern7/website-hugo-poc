---
layout: "image"
title: "Draufsicht"
date: "2015-04-03T10:00:06"
picture: "Foto_15.jpg"
weight: "14"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40708
- /details9a7d-2.html
imported:
- "2019"
_4images_image_id: "40708"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40708 -->
