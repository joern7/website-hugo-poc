---
layout: "image"
title: "Leichtigkeit"
date: "2015-04-03T10:00:06"
picture: "Foto_14.jpg"
weight: "13"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40707
- /detailsbb13-2.html
imported:
- "2019"
_4images_image_id: "40707"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40707 -->
Die Gewindestangen erhöen die Stabilität der Achse.