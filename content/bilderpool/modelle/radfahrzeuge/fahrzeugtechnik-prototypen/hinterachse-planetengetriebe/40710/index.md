---
layout: "image"
title: "TSTs Powermotorplatte mal anders"
date: "2015-04-03T10:00:06"
picture: "Foto_17_2.jpg"
weight: "16"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40710
- /detailsc446-2.html
imported:
- "2019"
_4images_image_id: "40710"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40710 -->
Die intelligente Nutzung der Einzelteile ermöglicht eine sehr kompakte und schlanke Bauweise.