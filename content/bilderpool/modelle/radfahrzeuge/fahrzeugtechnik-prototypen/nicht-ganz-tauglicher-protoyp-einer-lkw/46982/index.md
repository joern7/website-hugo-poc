---
layout: "image"
title: "Federung (2)"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46982
- /detailsec02.html
imported:
- "2019"
_4images_image_id: "46982"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46982 -->
Ein Blick von oben auf die Mechanik. Die Gummis stelle man sich bitte um die Seilrollen laufend vor, das habe ich beim Fotografieren übersehen.