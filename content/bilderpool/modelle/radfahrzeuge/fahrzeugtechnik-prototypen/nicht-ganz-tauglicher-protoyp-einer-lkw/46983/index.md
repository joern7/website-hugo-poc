---
layout: "image"
title: "Kettenführung"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46983
- /details81ed-2.html
imported:
- "2019"
_4images_image_id: "46983"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46983 -->
Auf diesem Bild ist das Lenk-Zeugs abmontiert, sodass man einen Blick auf die unabhängig laufenden Ketten bekommt.