---
layout: "image"
title: "Schneeräumfahrzeug"
date: "2016-10-24T21:20:51"
picture: "schneeraeumfahrzeug09.jpg"
weight: "9"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/44679
- /detailsf12a-2.html
imported:
- "2019"
_4images_image_id: "44679"
_4images_cat_id: "3325"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44679 -->
