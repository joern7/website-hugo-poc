---
layout: "image"
title: "Schneeräumfahrzeug"
date: "2016-10-24T21:20:51"
picture: "schneeraeumfahrzeug07.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/44677
- /detailsdaec.html
imported:
- "2019"
_4images_image_id: "44677"
_4images_cat_id: "3325"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44677 -->
Heben und senken über Spindelantrieb und Mikromotor von RC-Bruder.