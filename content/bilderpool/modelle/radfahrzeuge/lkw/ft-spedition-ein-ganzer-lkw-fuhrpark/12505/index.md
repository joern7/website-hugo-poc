---
layout: "image"
title: "Sattelschlepper"
date: "2007-11-05T16:42:29"
picture: "DSCN1877.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12505
- /detailsb6dd-2.html
imported:
- "2019"
_4images_image_id: "12505"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T16:42:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12505 -->
Sattelzug