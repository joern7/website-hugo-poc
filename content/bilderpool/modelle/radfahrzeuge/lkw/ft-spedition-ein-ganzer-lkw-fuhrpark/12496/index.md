---
layout: "image"
title: "kleiner LKW mit Kippmulde"
date: "2007-11-05T15:54:02"
picture: "DSCN1964.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12496
- /detailsaa91.html
imported:
- "2019"
_4images_image_id: "12496"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12496 -->
