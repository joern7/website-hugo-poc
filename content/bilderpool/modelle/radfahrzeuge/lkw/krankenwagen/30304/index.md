---
layout: "image"
title: "Die Trage"
date: "2011-03-21T18:35:36"
picture: "krankenwagen10.jpg"
weight: "9"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30304
- /detailse93e.html
imported:
- "2019"
_4images_image_id: "30304"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30304 -->
Die schräge Streben dienen zur Stabilität