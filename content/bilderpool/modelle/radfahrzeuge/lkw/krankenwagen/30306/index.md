---
layout: "image"
title: "Schalter Warnblinker"
date: "2011-03-21T18:35:36"
picture: "krankenwagen12.jpg"
weight: "11"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30306
- /details9e68-3.html
imported:
- "2019"
_4images_image_id: "30306"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30306 -->
Die Kabel sind mit "Tesa-Film" angeklebt, damit mehr Platz ist