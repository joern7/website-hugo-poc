---
layout: "image"
title: "In dem Krankenwagen"
date: "2011-03-21T18:35:36"
picture: "krankenwagen09.jpg"
weight: "8"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30303
- /details1e66.html
imported:
- "2019"
_4images_image_id: "30303"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30303 -->
hat sich wohl einer verletzt