---
layout: "image"
title: "Hinten"
date: "2011-03-21T18:35:36"
picture: "krankenwagen04.jpg"
weight: "3"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30298
- /details98df.html
imported:
- "2019"
_4images_image_id: "30298"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30298 -->
Die Türen sind offen und die Rücklichter leuchten