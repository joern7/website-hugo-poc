---
layout: "image"
title: "Blaulicht"
date: "2011-03-21T18:35:36"
picture: "krankenwagen05.jpg"
weight: "4"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30299
- /details2b91.html
imported:
- "2019"
_4images_image_id: "30299"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30299 -->
Die Lampen werden von einem E-Tec Modul gesteuert