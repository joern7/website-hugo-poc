---
layout: "image"
title: "Micro-RC-LKW 05"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_05.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31787
- /details26f2.html
imported:
- "2019"
_4images_image_id: "31787"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31787 -->
Das aufwendige und enge Package unter den Verkleidungsplatten macht es notwendig, dass die Platten auf beiden Seiten unterschiedlich aufgeteilt sind.

Hier auf dem Foto ist der maximale Lenkeinschlag zu sehen.

Im Cockpit der Ein/Aus-Schalter.