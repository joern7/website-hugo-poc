---
layout: "image"
title: "Micro-RC-LKW 11"
date: "2011-09-13T18:28:29"
picture: "Micro-RC-LKW_11.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31793
- /detailsd6f6-2.html
imported:
- "2019"
_4images_image_id: "31793"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:28:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31793 -->
Die Verbindung zwischen Servo-Ritzel und FT-Welle erfolgt über eine Klemmkupplung 31024. Das hält für diese Art von Anwendung wirklich gut! Bei größeren Lenkkräften rutscht die Kupplung aber auf dem Ritzel, was aber hier nicht passiert.

Ich hatte zunächst nach einer Lösung ohne Achsenverschraubung 38843 gesucht, also eine Rastachse direkt in die Klemmkupplung. Das passte aber vom Rastermaß nicht zufriedenstellend. Außerdem beschädigt die Klemmkupplung die Rastnasen an den Kunststoffachsen.