---
layout: "image"
title: "Micro-RC-LKW 04"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31786
- /details60e1.html
imported:
- "2019"
_4images_image_id: "31786"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31786 -->
Baut etwas zu hoch für die Breite, aber es geht noch ...