---
layout: "image"
title: "lkw10.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw10.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45598
- /details0fb7.html
imported:
- "2019"
_4images_image_id: "45598"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45598 -->
