---
layout: "image"
title: "lkwmehr1.jpg"
date: "2017-03-23T14:27:57"
picture: "lkwmehr1.jpg"
weight: "42"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45638
- /details0b70.html
imported:
- "2019"
_4images_image_id: "45638"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-23T14:27:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45638 -->
Eine Alternative, kürzeren Auflieger