---
layout: "image"
title: "lkw29.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw29.jpg"
weight: "29"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45617
- /detailsae0f-2.html
imported:
- "2019"
_4images_image_id: "45617"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45617 -->
