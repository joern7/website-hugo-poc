---
layout: "image"
title: "lkw33.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw33.jpg"
weight: "33"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45621
- /details6ad9.html
imported:
- "2019"
_4images_image_id: "45621"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45621 -->
