---
layout: "image"
title: "lkw23.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw23.jpg"
weight: "23"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45611
- /detailsf0a9-2.html
imported:
- "2019"
_4images_image_id: "45611"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45611 -->
