---
layout: "image"
title: "Lage des Servos"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34449
- /detailse6ca-2.html
imported:
- "2019"
_4images_image_id: "34449"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34449 -->
Der Servo hängt vorne mittig unter dem Führerhaus zwischen den (nicht angeschlossenen) Lampen.