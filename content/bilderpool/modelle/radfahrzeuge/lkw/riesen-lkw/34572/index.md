---
layout: "image"
title: "Kettenauffangkorb"
date: "2012-03-05T12:56:13"
picture: "05_Auffangkorb.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34572
- /details4360.html
imported:
- "2019"
_4images_image_id: "34572"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-03-05T12:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34572 -->
Ich habe hier zum ersten mal meine schwarzen Kettenglieder verbaut.
Sie sehen hier besser aus als die roten (denke ich).