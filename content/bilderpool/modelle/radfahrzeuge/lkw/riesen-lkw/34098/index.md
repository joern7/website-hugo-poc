---
layout: "image"
title: "Antrieb"
date: "2012-02-05T20:01:41"
picture: "17_Antrieb.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34098
- /details2eda.html
imported:
- "2019"
_4images_image_id: "34098"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34098 -->
So sieht es im innern aus. Die hinteren Differentiale mit der Zwischenverbindung. Nach rechts geht es zum Antrieb und der Vorderachse.

Ich habe noch mehr Bilder, wenn ihr etwas spezielles sehen möchtet sagt es mir.