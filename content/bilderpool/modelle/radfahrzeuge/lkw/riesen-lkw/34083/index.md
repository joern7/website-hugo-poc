---
layout: "image"
title: "Vorderansicht"
date: "2012-02-05T20:01:27"
picture: "02_Vorderansicht.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34083
- /detailsd146.html
imported:
- "2019"
_4images_image_id: "34083"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34083 -->
Gut zu erkennen die Seilwinde, die Beleuchtung und das freistehende Lenkrad.
Der Antrieb für die Seilwinde wird am Differential der Vorderachse nach hinten vorbeigeführt.
Unter der Motorhaube war nicht genug Platz dafür....