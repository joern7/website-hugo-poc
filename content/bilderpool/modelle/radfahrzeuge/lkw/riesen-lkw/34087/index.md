---
layout: "image"
title: "alte Version"
date: "2012-02-05T20:01:27"
picture: "06_Kupplung_0.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34087
- /details6704.html
imported:
- "2019"
_4images_image_id: "34087"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34087 -->
Kupplung offen