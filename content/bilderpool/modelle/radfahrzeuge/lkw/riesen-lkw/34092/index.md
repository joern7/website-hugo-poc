---
layout: "image"
title: "Heck rechts"
date: "2012-02-05T20:01:41"
picture: "11_Heck_Detail_mit_Beleuchtung.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34092
- /details3c65-2.html
imported:
- "2019"
_4images_image_id: "34092"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34092 -->
Hier sieht man den rechten hinteren Teil des LKW.
Jedes Hinterrad ist mit 10 Federn ausgestattet. 30 mm Federweg sind schon vorhanden.
Und der Antrieb funktioniert dann immer.
Natürlich ist die Beleuchtung voll funktionstüchtig.
Wird das Licht eingeschaltet leuchten die roten Lampen, fährt der LKW rückwärts gehen zusätzlich noch die Rückfahrscheinwerfer an.
Und der Blinker funktioniert automatisch (richtig).