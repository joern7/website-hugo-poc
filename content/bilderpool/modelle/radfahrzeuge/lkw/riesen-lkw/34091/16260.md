---
layout: "comment"
hidden: true
title: "16260"
date: "2012-02-06T10:46:52"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hallo Ludger!

Danke. Aber nur weil bei Dir einige Federn etwas weicher sind als andere, heißt das doch nicht, dass es offiziell seitens FT mal 2 Varianten dieser Federn gab! Laut Datenbank gibt es eine Feder 26 schwarz, und keine 2 Varianten.

Gruß, Thomas