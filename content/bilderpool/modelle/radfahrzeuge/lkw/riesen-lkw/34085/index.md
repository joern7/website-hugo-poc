---
layout: "image"
title: "Seitenansicht"
date: "2012-02-05T20:01:27"
picture: "04_Seitenansicht_0.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34085
- /detailsb705-3.html
imported:
- "2019"
_4images_image_id: "34085"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34085 -->
In der Bildmitte die Mechanik für die Klauenkupplung.