---
layout: "image"
title: "Radträger"
date: "2012-02-05T20:01:41"
picture: "15_Radtrger_2.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34096
- /details5889-2.html
imported:
- "2019"
_4images_image_id: "34096"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34096 -->
Möglichst kompakt wollte ich zwei Naben auf einer Achse befestigen. Dieses habe ich so gelöst.
In den beiden Schneckenmuttern befindet sich jeweils ein Kugellager damit alles leicht läuft.
Jedes Rad ist übrigens mit 5 Kugellagern bis zum Differential ausgestattet.