---
layout: "image"
title: "zweiter Versuch"
date: "2012-02-05T20:01:41"
picture: "13_Diff_2-1.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34094
- /details14de.html
imported:
- "2019"
_4images_image_id: "34094"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34094 -->
Hier ein Differential in der zweiten, verbesserten, Version.
Andreas hat mir die Z40 abgedreht so das nur noch der Innenkranz mit den Zähnen stehen blieb.
So konnte ich die Differentiale weiter verkleinern.
Hier sieht man auch wie ich die gekürzten Rast Z10 eingebaut habe.
Sie befinden sich zwischen den liegen roten Zahnrädern.