---
layout: "image"
title: "Radträger"
date: "2012-02-05T20:01:41"
picture: "14_Radtrger_1.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34095
- /details5683-2.html
imported:
- "2019"
_4images_image_id: "34095"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34095 -->
Die großen Reifen haben einen Nachteil wenn es darum geht diese anzutreiben. Die Metallachsen neigen dann dazu durchzudrehen.
Deswegen habe ich einen Radträger gebaut der das verhindern soll.