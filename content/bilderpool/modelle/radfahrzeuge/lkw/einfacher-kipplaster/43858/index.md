---
layout: "image"
title: "Montagedetail - Frontwand"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster14.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43858
- /details9e34-3.html
imported:
- "2019"
_4images_image_id: "43858"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43858 -->
Vier Federnocken knapp innerhalb der halben Nutlänge passen schon so ungefähr zu den Nuten des Rahmens.