---
layout: "overview"
title: "Einfacher Kipplaster"
date: 2020-02-22T07:51:30+01:00
legacy_id:
- /php/categories/3249
- /categories1212.html
- /categorieseb7f.html
- /categoriese07e.html
- /categories2213.html
- /categories71bf.html
- /categories4d7a.html
- /categories141a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3249 --> 
Erst "Einfacher Lastwagen mit Anhänger" (http://www.ftcommunity.de/categories.php?cat_id=3162) - dort ist die Grundkonstruktion des Fahrzeugrahmens zu finden. Und das feste Führerhaus.
Dann "Einfacher Abschleppwagen" (https://www.ftcommunity.de/categories.php?cat_id=3197) - von dem stammt das verwendete klappbare Führerhaus. Mit dem festen Führerhaus geht das aber auch.
Und nun "Einfacher Kipplaster" - der Abschleppkran ist durch eine Kippmulde ersetzt.

Wie man sieht ist das einfache Lasterchen durchaus wandelbar und das ist gut so. Junior's neuester Wunsch ließ sich auf die Art einigermaßen schnell erfüllen.