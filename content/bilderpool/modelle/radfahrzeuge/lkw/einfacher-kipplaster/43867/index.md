---
layout: "image"
title: "Bei der Arbeit"
date: "2016-07-09T08:24:57"
picture: "einfacherkipplasterii5.jpg"
weight: "23"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43867
- /details5147-2.html
imported:
- "2019"
_4images_image_id: "43867"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-09T08:24:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43867 -->
So macht er nun auch auf der Baustelle eine gute Figur. Beladen per Bagger geht problemlos.