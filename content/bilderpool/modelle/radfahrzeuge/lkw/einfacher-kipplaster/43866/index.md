---
layout: "image"
title: "Verbesserung - 'Facelift'"
date: "2016-07-09T08:24:57"
picture: "einfacherkipplasterii4.jpg"
weight: "22"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43866
- /details4d7e.html
imported:
- "2019"
_4images_image_id: "43866"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-09T08:24:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43866 -->
Der Junior wird älter und die Ansprüche steigen! Fehlende  Scheinwerfer wurden reklamiert und die grünen Teile sind das, was sich in der Sammlung als nächstgelegener Farbtupfer (nicht rot) mit halbwegs passender Form fand: Hydraulik-Rastgelenk (31849).

Bei der Gelegenheit sind die gelben Teile am Führerhaus ("Einfacher Abschleppwagen" - https://www.ftcommunity.de/categories.php?cat_id=3197) nun durch rote Teile ersetzt. Das bringt die "Scheinwerfer" etwas stärker zur Geltung und wirkt insgesamt ruhiger. Wer gelbe BS5 sein eigen nennt, kann die ja mal als "Blinker" testen.