---
layout: "image"
title: "Detail - Heckpartie"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster18.jpg"
weight: "18"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43862
- /detailsb8a8.html
imported:
- "2019"
_4images_image_id: "43862"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43862 -->
Wie schon erwähnt, die WS30° können durch andere Bausteine ersetzt werden. Sie sollen nur verhindern, daß die Gelenkwürfel nach innen rutschen. Insgesamt wird die Mulde so auch gegen seiltiches Verschieben stabil.
Die dunklen Gelenkwürfelklauen zusammen mit den hellen Gelenkwürfelzungen stammen aus meinem Universal II und sind damals OVP in genau diesen Farben angeliefert worden. Also bitte keine Anzüglichkeiten deswegen ;-)