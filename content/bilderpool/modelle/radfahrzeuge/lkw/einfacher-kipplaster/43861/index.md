---
layout: "image"
title: "Detail - Muldenauflage und Radachse"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster17.jpg"
weight: "17"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43861
- /detailseea6-3.html
imported:
- "2019"
_4images_image_id: "43861"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43861 -->
Auf dem vorderen Verschlußriegel liegt die gelbe Bodenplatte auf. Die Mulde liegt dadurch fast waagerecht. Die unterschiedliche Dicke von Grundplatte 90 x 90 (die rote mit 5,5 mm) und Bodenplatte 30 x 90 (die gelbe mit 7,5 mm) stört hier überhaupt nicht.

An der Achse ist innen zuerst eine Riegelscheibe und dann eine schwarze Hülse 15 mm aufgesteckt. Ohne die Riegelscheibe sitzen die Räder etwas arg eng am Rahmen und am Führerhaus scheuert es dann etwas. Mit Riegelscheibe paßt es.

Mit längerem Radstand sieht der Laster auch nicht schlecht aus, einfach die Metallachse ein (besser) oder zwei (nicht mehr so toll) Löcher weiter hinten einhängen. Versuch macht klug => der eigene Geschmack entscheidet.