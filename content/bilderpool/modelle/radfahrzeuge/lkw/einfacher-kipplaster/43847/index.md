---
layout: "image"
title: "Und von schräg hinten"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster03.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43847
- /details4940-2.html
imported:
- "2019"
_4images_image_id: "43847"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43847 -->
Hier hätten wir die Kippmulde etwas mehr im Vordergrund. Vorne ist "quick & dirty" ein stabiler Rahmen aus grauen Grundbausteinen montiert. Die Seitenwände bestehen aus Grundplatten 120 x 60 und sind vorne am Rahmen befestigt.