---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 21"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert21.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37055
- /detailsf7d8.html
imported:
- "2019"
_4images_image_id: "37055"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37055 -->
Radaufhängung und Federung der beide Hinterachsen