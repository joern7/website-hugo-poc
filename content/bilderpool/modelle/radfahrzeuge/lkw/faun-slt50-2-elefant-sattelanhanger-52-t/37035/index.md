---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 1"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37035
- /detailsa13c.html
imported:
- "2019"
_4images_image_id: "37035"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37035 -->
Länge: 630mm
Breite: 225
Gewicht: keine anung. Habs nicht gewogen, aber zu viel.