---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 1"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert37.jpg"
weight: "37"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37071
- /detailscbcc-2.html
imported:
- "2019"
_4images_image_id: "37071"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37071 -->
Unterseite: Verstelbare Stutzen und 8 gelenkte Räder