---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 14"
date: "2013-06-11T22:27:47"
picture: "sattelanhaengerkaessbohrert10.jpg"
weight: "61"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37095
- /details4fe1-3.html
imported:
- "2019"
_4images_image_id: "37095"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-11T22:27:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37095 -->
