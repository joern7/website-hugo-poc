---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 5"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert41.jpg"
weight: "41"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37075
- /detailsef46-2.html
imported:
- "2019"
_4images_image_id: "37075"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37075 -->
Länge: 935mm
Breite: 225mm
Ladefläche: 560 x 225mm