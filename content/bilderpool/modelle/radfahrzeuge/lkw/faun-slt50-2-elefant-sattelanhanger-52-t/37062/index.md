---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 28"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert28.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37062
- /details081a.html
imported:
- "2019"
_4images_image_id: "37062"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37062 -->
