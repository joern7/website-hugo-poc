---
layout: "image"
title: "Zugmaschine - v.2 / Aufleger"
date: "2008-09-06T09:04:29"
picture: "DSCN0048_800.jpg"
weight: "11"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15190
- /details9422.html
imported:
- "2019"
_4images_image_id: "15190"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15190 -->
Der Vollständigkeit musste ich natürlich noch einen Aufleger bauen.
Selbstverständlich voll gefedert, leider nicht Pneumatisch.