---
layout: "image"
title: "Aufleger - Klappe hinten"
date: "2008-09-06T09:04:29"
picture: "DSCN0056_800.jpg"
weight: "13"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15192
- /detailsa5a0-2.html
imported:
- "2019"
_4images_image_id: "15192"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15192 -->
