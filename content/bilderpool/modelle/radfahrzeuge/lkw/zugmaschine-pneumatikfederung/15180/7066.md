---
layout: "comment"
hidden: true
title: "7066"
date: "2008-09-05T16:20:32"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Stark! Endlich machts mal einer! Bitte mehr Detailfotos, auch von der Vorderachse. Ist die auch gefedert? Und momentan sieht es mir so aus, als ob die Zylinder noch nicht unter Druckluft stehen - wie funktioniert die Federung?

Gruß,
Stefan