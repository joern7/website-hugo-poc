---
layout: "comment"
hidden: true
title: "7067"
date: "2008-09-05T18:10:07"
uploadBy:
- "zeuz"
license: "unknown"
imported:
- "2019"
---
Vorderachse ist gefedert, aber nicht pneumatisch.
Sehr richtig erkannt, leider habe ich im Moment keine funktionierenden Zylinder, und auch meine Airtanks sind undicht.
Die geplante Funktion ist wie folgt:
ein kleiner Kompressor, der in den Luftspeicher pump.
Der Luftspeicher bildet geschlossenen Kreislauf mit dem oberen Zylinder teil.
Durch ein Ventil kann Druck abgelassen werden und dutch Aktivierung des Kompressor kann Druck nachgepumpt werden.

Gruß
Norbert