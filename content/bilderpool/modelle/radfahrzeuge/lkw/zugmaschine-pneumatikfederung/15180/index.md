---
layout: "image"
title: "Zugmaschine - v.1 - vorne links"
date: "2008-09-05T16:16:49"
picture: "IMG_1467_sp_800.jpg"
weight: "1"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15180
- /detailsb268-2.html
imported:
- "2019"
_4images_image_id: "15180"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T16:16:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15180 -->
Mein Kindheitstraum, die 80ger jahre Trucks .. zu spät, es gab nur noch Master.
Das ist mein erster Versuch heute mal so etwas ähnliches nachzubauen .. hoffentlich bald mit Proportionaler ft Fernsteuerung.