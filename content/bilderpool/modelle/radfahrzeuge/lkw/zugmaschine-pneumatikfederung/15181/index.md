---
layout: "image"
title: "Zugmaschine - v.1 - vorne oben"
date: "2008-09-05T16:16:49"
picture: "IMG_1464.jpg"
weight: "2"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15181
- /details5995.html
imported:
- "2019"
_4images_image_id: "15181"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T16:16:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15181 -->
