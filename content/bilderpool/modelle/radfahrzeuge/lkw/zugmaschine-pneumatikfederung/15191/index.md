---
layout: "image"
title: "Zugmaschine - v.2 / Aufleger - Geländetauglichkeit"
date: "2008-09-06T09:04:29"
picture: "DSCN0050_800.jpg"
weight: "12"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15191
- /detailsba41.html
imported:
- "2019"
_4images_image_id: "15191"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15191 -->
Man kann hier schön erkennen wie sich alle Räder den Bodengegebenheiten anpassen.