---
layout: "comment"
hidden: true
title: "16572"
date: "2012-03-06T07:38:12"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Schönes Design und gute Proportionen! Klasse, wie Du die Sitzbausteine in den Rahmen integriert hast.

Gruß, Thomas