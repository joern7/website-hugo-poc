---
layout: "image"
title: "sattelzug_komplett_2"
date: "2012-03-04T19:50:12"
picture: "sattelzug_2.jpg"
weight: "3"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34549
- /details8513-2.html
imported:
- "2019"
_4images_image_id: "34549"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34549 -->
