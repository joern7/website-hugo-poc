---
layout: "image"
title: "zugmaschiene 5"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_05.jpg"
weight: "8"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34554
- /details7c4d.html
imported:
- "2019"
_4images_image_id: "34554"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34554 -->
