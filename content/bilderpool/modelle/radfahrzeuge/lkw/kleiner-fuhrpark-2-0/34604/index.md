---
layout: "image"
title: "Sattelzug in der Totalen 1"
date: "2012-03-06T15:40:49"
picture: "sattelzug_totale_tuer_geschlossen.jpg"
weight: "32"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34604
- /detailsa5b9.html
imported:
- "2019"
_4images_image_id: "34604"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-06T15:40:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34604 -->
