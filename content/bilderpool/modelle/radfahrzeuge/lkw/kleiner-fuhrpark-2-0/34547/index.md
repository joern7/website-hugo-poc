---
layout: "image"
title: "anhaenger_solo"
date: "2012-03-04T19:50:12"
picture: "anhaenger_1.jpg"
weight: "1"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34547
- /detailsf0f0.html
imported:
- "2019"
_4images_image_id: "34547"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34547 -->
Der Anhänger des Sattelzuges. Aufbau bfindet sich noch in Arbeit