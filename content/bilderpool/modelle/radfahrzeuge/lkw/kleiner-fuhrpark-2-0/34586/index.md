---
layout: "image"
title: "bug3"
date: "2012-03-05T22:09:42"
picture: "buggy_03.jpg"
weight: "14"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34586
- /detailsaccc.html
imported:
- "2019"
_4images_image_id: "34586"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-05T22:09:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34586 -->
