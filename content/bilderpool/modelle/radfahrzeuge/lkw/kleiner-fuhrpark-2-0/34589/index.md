---
layout: "image"
title: "bug6"
date: "2012-03-05T22:09:42"
picture: "buggy_06.jpg"
weight: "17"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34589
- /detailsecec.html
imported:
- "2019"
_4images_image_id: "34589"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-05T22:09:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34589 -->
Der Versuch, ein recht "geländegängiges" Fahrzeug zu bauen. Aufgrund der Größe bezeichne ich es mal als Kreuzung aus LKW/Buggy