---
layout: "image"
title: "Aufhaengung 2. Achse, Blick seitlich"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine08.jpg"
weight: "8"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30792
- /details56c4.html
imported:
- "2019"
_4images_image_id: "30792"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30792 -->
Hier die "Blattfederung" fuer Achse zwei. Die beiden parallelen Streben in Fahrtrichtung schleppen die Achse, die oben aber nochmal am Rahmen befestigt ist. Weiter oben die beiden Anlenkstreben, quer zur Fahrtrichtung die Spurstangen.