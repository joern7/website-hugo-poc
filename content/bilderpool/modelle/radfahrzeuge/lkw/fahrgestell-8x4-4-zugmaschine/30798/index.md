---
layout: "image"
title: "Geringster Lenkrollradius?"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine14.jpg"
weight: "14"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30798
- /details4084.html
imported:
- "2019"
_4images_image_id: "30798"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30798 -->
Lenkrollradius ist der Abstand von der Drehachse des Rades zu seinem mittleren Aufstandspunkt auf der Strasse. Der sollte nahe null sein, ist bei fischertechnik aber immer riesig, weil die Drehachse ausserhalb des Reifens verlaeuft. Das erfordert elend lange Radkaesten.

Diese Lenkung will es besser machen, und verlegt auf Kosten der Belastbarkeit den Drehpunkt an den Rand der Felge. Weiter nach innen geht es nur, wenn man die Nut der Strebenadapter auf einer Seite absaegt. Ginge aber auch.

Der Winkelstein 15 sorgt fuer ein grosszuegig abgeschaetztes Lenktrapez. 

Die obere Verbindung zum Rahmen ist entfernt, der zweite Strebenadapter ist die Anlenkung der zweiten Achse. Dort kommt also nicht zweimal die gleiche Strebe hin!