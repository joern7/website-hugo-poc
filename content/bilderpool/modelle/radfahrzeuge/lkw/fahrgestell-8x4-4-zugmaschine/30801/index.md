---
layout: "image"
title: "Hinterachsaufhaengung: Pendelachse"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine17.jpg"
weight: "17"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30801
- /details3c8d.html
imported:
- "2019"
_4images_image_id: "30801"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30801 -->
Die Verkleidung wurde entfernt, damit es ein bisschen heller wurde. Wir sehen den schoenen Teil der Pendelachsaufhaengung. Der Baustein 30 hinten nimmt auch die Anhaengerkupplungs-Attrappe auf. Das Kupplungsstueck, das die Pendelachse in der Mitte festhaetl, wird also beim Rueckwaertsfahren ganz schoen verbogen, weil die angetriebene Hinterachse gegen den Baustein 30 stoesst.