---
layout: "image"
title: "Hinterachsaufhaengung 2"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine16.jpg"
weight: "16"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30800
- /details247a-3.html
imported:
- "2019"
_4images_image_id: "30800"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30800 -->
Hier die drei Streben, die im vorigen Bild im Winkeltraeger 15 versteckt waren. Die Welle im Bild ist mit der Kardanwelle verbunden; die drei Streben sind also auch nicht direkt mit dem Wagen verbunden. Hmmpf.