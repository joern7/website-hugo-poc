---
layout: "image"
title: "Frontansicht, Kuehlergrill"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine03.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30787
- /detailsb089-2.html
imported:
- "2019"
_4images_image_id: "30787"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30787 -->
Hinter dem Kuehler sind selbstverstaendlich die Motoren angebracht.

Die Front wurde im Laufe der Zeit ein bisschen asymmetrisch.

Wenn das Ding nicht ganz und gar rot werden soll, braucht man schwarze "Zierstreifen" oder Akzente ... die einzubauen, ist gar nicht so leicht. Die zwei unverkleideten Bausteine 30 sind ein Versuch.