---
layout: "image"
title: "Fahrgestell, leer"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine21.jpg"
weight: "21"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30805
- /details9774-3.html
imported:
- "2019"
_4images_image_id: "30805"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30805 -->
Der im Laufe der Fotosession immer weiter entleerte Rahmen. Man kann gut erahnen, wo die Vorderachse hingehoert. Ihre obere Aufhaengung ist in der Mitte noch zu sehen -- nicht die stabilste, koennte man aber leicht aendern. Interessanterweise sitzt der Motorblock oberhalb dieser Bausteingruppe -- ich konnte es auch nicht glauben.