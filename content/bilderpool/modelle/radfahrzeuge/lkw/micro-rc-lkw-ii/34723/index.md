---
layout: "image"
title: "Micro-RC-LKW II 02"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34723
- /details6fbf.html
imported:
- "2019"
_4images_image_id: "34723"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34723 -->
Der kleine Hebel seitlich am Führerhaus lässt sich bewegen, denn es ist der Ein/Aus-Schalter.