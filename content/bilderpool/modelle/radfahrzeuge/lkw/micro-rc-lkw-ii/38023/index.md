---
layout: "image"
title: "Micro-RC-LKW II 14"
date: "2014-01-08T23:15:04"
picture: "DSC09192.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38023
- /details2a9f.html
imported:
- "2019"
_4images_image_id: "38023"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38023 -->
Ich habe gerade festgestellt, dass ich euch noch Detailaufnahmen des Innenlebens schulde ...

Hier die Draufsicht aufs Innenleben, das Dach ist abgenommen.