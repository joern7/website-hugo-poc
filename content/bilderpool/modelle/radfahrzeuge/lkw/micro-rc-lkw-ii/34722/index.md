---
layout: "image"
title: "Micro-RC-LKW 01"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34722
- /details5b55-2.html
imported:
- "2019"
_4images_image_id: "34722"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34722 -->
Zuwachs in meiner Micro-RC-Serie: Der Micro-RC-LKW II, eine Neuinterpretation des Micro-RC-LKW, den ich hier bereits vorgestellt hatte:

http://www.ftcommunity.de/categories.php?cat_id=2372

Dieser war zwar schön minimalistisch und kompakt, aber die Proportionen gefielen mir nie so - er war einfach zu hoch für seine geringe Breite. Auch die Lenkung über einen simplen Drehschemel war nicht der Weisheit letzter Schluss ...

Mein neues Modell macht alles wesentlich besser, bleibt aber ein klassischer Micro-RC, also ein voll ferngesteuertes Fahrzeug mit maximal 50 mm Breite und natürlich gebaut nach dem FT-Reinheitsgebot (keine Fremdteile, Modifikationen, Kleber).

Es ist natürlich ein Mercedes geworden, wie man am Stern vorn am Führerhaus sieht.

Die Details des Modells:

- Antrieb per XS-Motor
- Achsschenkel-Lenkung per Servo (ohne Hebel)
- Ein/Aus-Schalter im Führerhaus
- Stromversorgung per 9V-Blockakku
- Fernsteuerung per Control Set
- moderne Optik mit Junior-Führerhaus
- Aufbau nur 3 Grundbausteine hoch

Gegenüber meinem ersten Modell sind die Proportionen deutlich stimmiger: Das Führerhaus ist größer und der Aufbau einen ganzen Grundbaustein flacher. Es fährt viel schneller, und die kleine Achsschenkel-Lenkung ist natürlich auch deutlich realistischer als ein Drehschemel.

Ich denke, damit sollte das Ende der Fahnenstange hinsichtlich miniaturisierter ferngesteuerter FT-Fahrzeuge erreicht sein! Kleiner kann ich mir ein FT-Fahrzeug mit Control Set und ohne Modifikationen nicht mehr vorstellen. Aber man weiß ja nie ... ;o)

Ein kleines Video gibt es hier:

http://www.youtube.com/watch?v=stSLDacL9cQ