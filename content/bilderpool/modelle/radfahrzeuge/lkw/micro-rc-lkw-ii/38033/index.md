---
layout: "image"
title: "Micro-RC-LKW II 24"
date: "2014-01-08T23:15:05"
picture: "DSC09214.jpg"
weight: "24"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38033
- /detailsef7f.html
imported:
- "2019"
_4images_image_id: "38033"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38033 -->
Die Befestigung des Servos.