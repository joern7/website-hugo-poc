---
layout: "image"
title: "Micro-RC-LKW II 21"
date: "2014-01-08T23:15:05"
picture: "DSC09208.jpg"
weight: "21"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38030
- /detailsaf04.html
imported:
- "2019"
_4images_image_id: "38030"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38030 -->
Das Junior-Führerhaus lässt sich einfach nach oben abziehen.

Links im Bild der Hebel-Schalter, der über den Mini-Taster den Empfänger an und aus schaltet. Die Herausforderung war zunächst, in diesem kleinen Fahrzeug überhaupt noch einen Mini-Taster inkl. Kabelanschlüssen unterzubringen. Das gelang mir im Führerhaus. Diesen aber dann noch von außen schaltbar zu machen, war wirklich nicht einfach ...