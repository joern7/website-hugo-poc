---
layout: "overview"
title: "Tieflader-Zugmaschine mit IR Control"
date: 2020-02-22T07:51:27+01:00
legacy_id:
- /php/categories/3128
- /categories1be2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3128 --> 
Replik der Zugmaschine aus dem Modellbaukasten Tieflader (Art. 30475) mit aktueller IR-Fernbedienung.