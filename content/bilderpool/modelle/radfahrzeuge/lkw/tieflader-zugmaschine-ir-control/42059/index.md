---
layout: "image"
title: "Vorderachse mit Lenkung"
date: "2015-10-06T17:22:16"
picture: "DSC08206-sc.jpg"
weight: "6"
konstrukteure: 
- "Fischertechnik Modellbaukasten 30475 / Änderungen PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42059
- /details8d05.html
imported:
- "2019"
_4images_image_id: "42059"
_4images_cat_id: "3128"
_4images_user_id: "2488"
_4images_image_date: "2015-10-06T17:22:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42059 -->
Die Vorderachse, Aufhängung ebenfalls diskret aufgebaut, mit Micro-Servo.