---
layout: "image"
title: "Tieflader-Zugmaschine von vorne"
date: "2015-10-06T17:22:16"
picture: "DSC08201-sc.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik Modellbaukasten 30475 / Änderungen PHabermehl"
fotografen:
- "PHabermehl"
keywords: ["Modellbaukasten", "Tieflader", "Zugmaschine", "Sattelschlepper", "Fernsteuerung", "IR", "Control"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42054
- /details69ba.html
imported:
- "2019"
_4images_image_id: "42054"
_4images_cat_id: "3128"
_4images_user_id: "2488"
_4images_image_date: "2015-10-06T17:22:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42054 -->
Eine Sattelzugmaschine, prinzipiell eine Replik der Zugmaschine aus dem Modellbaukasten Tieflader (Art. 30475).

Statt Radantriebsgehäuse wurden 60mm längere Rahmen-Aluprofile verbaut und zwei Werkzeugkästen 60x30x30 angebaut.

Antrieb erfolgt über den Traktormotor auf beide Hinterachsten, Fernsteuerung über das IR Control Set.