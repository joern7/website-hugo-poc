---
layout: "image"
title: "büssing05.jpg"
date: "2013-07-20T19:23:31"
picture: "IMG_9063imit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37180
- /detailsd448-2.html
imported:
- "2019"
_4images_image_id: "37180"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:23:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37180 -->
Der Antriebsmotor (P-Motor, 50x) sitzt oben im Chassis, die Antriebswelle aber unten, deswegen wird die Kette gebraucht.