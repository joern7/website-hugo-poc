---
layout: "image"
title: "büssing02.jpg"
date: "2013-07-20T19:17:38"
picture: "IMG_9057mit.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37177
- /detailsb837-2.html
imported:
- "2019"
_4images_image_id: "37177"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:17:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37177 -->
