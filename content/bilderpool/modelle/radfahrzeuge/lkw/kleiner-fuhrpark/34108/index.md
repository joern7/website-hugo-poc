---
layout: "image"
title: "4achser 6"
date: "2012-02-07T19:33:25"
picture: "4achser_06.jpg"
weight: "6"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34108
- /details7470-2.html
imported:
- "2019"
_4images_image_id: "34108"
_4images_cat_id: "2523"
_4images_user_id: "1443"
_4images_image_date: "2012-02-07T19:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34108 -->
