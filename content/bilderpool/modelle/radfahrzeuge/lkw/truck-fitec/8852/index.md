---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:01"
picture: "Truck10.jpg"
weight: "10"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8852
- /details9d9a-2.html
imported:
- "2019"
_4images_image_id: "8852"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8852 -->
Der Antrieb besteht aus einem 8:1 P-Mot und einer zusätzlichen 2:1 Untersetzung.