---
layout: "comment"
hidden: true
title: "2298"
date: "2007-02-07T22:52:14"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo fitec,

ok, dann hübsche Hausschuhe ;o))

Wenn Du mit den Proportionen ein "Problem" hast (sprich wenn Du Schwierigkeiten hast, die richtig hinzubekommen), kannst Du dich ja auch an Fotos von realen Fahrzeugen orientieren. Hier im speziellen sind natürlich für den großen LKW die Räder recht klein geraten. Aber wenn die Hinterachse etwas weiter vorne sitzen würde, sähe das schon wesentlich "vorbildlicher" aus.
Aber Proportionen sind nicht alles, die Technik, die drinsteckt, ist eigentlich viel interessanter.

Gruß,
Franz