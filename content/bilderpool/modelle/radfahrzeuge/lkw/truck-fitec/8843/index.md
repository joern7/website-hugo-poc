---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:01"
picture: "Truck1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8843
- /details78fc-3.html
imported:
- "2019"
_4images_image_id: "8843"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8843 -->
Das ist mein Truck. Er hat Hekantrieb, Achsschenkellenkung, Hinterradfederung.