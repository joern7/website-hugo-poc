---
layout: "image"
title: "Road-Train - Übersicht"
date: "2008-03-16T18:44:28"
picture: "road-train1.jpg"
weight: "3"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Road", "Train", "Fernsteuerung"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/13928
- /detailsce98.html
imported:
- "2019"
_4images_image_id: "13928"
_4images_cat_id: "205"
_4images_user_id: "41"
_4images_image_date: "2008-03-16T18:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13928 -->
Dies ist der Road-Train, den ich in Mörshausen 2007 dabei hatte.
Er besteht im wesentlich aus Teilen des Super-Trucks Baukastens. Die Zugmaschine wird mit einem Power-Motor 1:8 angetrieben, der nochmals 1:2 untersetzt wird - das ergibt ausreichend Kraft. Gesteuert wird das Gespann mit einer selbstgebauten Funkfernsteuerung. 
(Schnellschuss-Fotos, leider etwas dunkel)