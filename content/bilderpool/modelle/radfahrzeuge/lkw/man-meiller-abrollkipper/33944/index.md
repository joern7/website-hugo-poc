---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:58:23"
picture: "4.jpg"
weight: "4"
konstrukteure: 
- "Johannes Weber"
fotografen:
- "Johannes Weber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33944
- /details766c.html
imported:
- "2019"
_4images_image_id: "33944"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33944 -->
Beim Abkippen wird das andere Gelenk gesperrt (im Bild leider vergessen zu sperren) durch die beiden bearbeiteten "Bausteine 5". Sie sollen ein Einknicken des Armes beim Abkippen verhindern.