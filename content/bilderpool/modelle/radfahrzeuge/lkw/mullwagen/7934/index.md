---
layout: "image"
title: "muellwagen14.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen14.jpg"
weight: "14"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7934
- /detailse507-3.html
imported:
- "2019"
_4images_image_id: "7934"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7934 -->
