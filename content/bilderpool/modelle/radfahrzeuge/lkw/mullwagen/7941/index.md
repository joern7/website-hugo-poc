---
layout: "image"
title: "muellwagen21.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen21.jpg"
weight: "21"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7941
- /details5201.html
imported:
- "2019"
_4images_image_id: "7941"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7941 -->
