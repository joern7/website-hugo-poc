---
layout: "image"
title: "muellwagen31.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen31.jpg"
weight: "31"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7951
- /details011e.html
imported:
- "2019"
_4images_image_id: "7951"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7951 -->
