---
layout: "image"
title: "muellwagen19.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen19.jpg"
weight: "19"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7939
- /details1a5b.html
imported:
- "2019"
_4images_image_id: "7939"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7939 -->
