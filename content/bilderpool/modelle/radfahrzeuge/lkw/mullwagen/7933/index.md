---
layout: "image"
title: "muellwagen13.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen13.jpg"
weight: "13"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7933
- /details5edd-2.html
imported:
- "2019"
_4images_image_id: "7933"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7933 -->
