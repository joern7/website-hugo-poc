---
layout: "image"
title: "stuurarm"
date: "2010-06-14T14:39:13"
picture: "P6130013.jpg"
weight: "6"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/27497
- /detailsb4b2.html
imported:
- "2019"
_4images_image_id: "27497"
_4images_cat_id: "1972"
_4images_user_id: "838"
_4images_image_date: "2010-06-14T14:39:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27497 -->
Stuurarm met wat bochten om langs de band te komen