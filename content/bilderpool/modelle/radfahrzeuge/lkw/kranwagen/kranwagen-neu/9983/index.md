---
layout: "image"
title: "Kranwagen Neu"
date: "2007-04-04T17:42:41"
picture: "kranwagenneu4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9983
- /detailsea45.html
imported:
- "2019"
_4images_image_id: "9983"
_4images_cat_id: "901"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T17:42:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9983 -->
Ausfahrmechanismus