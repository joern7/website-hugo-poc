---
layout: "image"
title: "Kranwagen Neu"
date: "2007-04-04T17:42:41"
picture: "kranwagenneu6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9985
- /details1270.html
imported:
- "2019"
_4images_image_id: "9985"
_4images_cat_id: "901"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T17:42:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9985 -->
Ausgefahren und hochgefahren