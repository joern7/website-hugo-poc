---
layout: "image"
title: "Kranwagen Neu"
date: "2007-04-04T17:42:41"
picture: "kranwagenneu1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9980
- /detailsc782.html
imported:
- "2019"
_4images_image_id: "9980"
_4images_cat_id: "901"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T17:42:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9980 -->
von oben. Ausfahrbarer Kranarm, Höhenverstelllbar, Drehbar