---
layout: "image"
title: "Kranwagen"
date: "2007-04-03T17:33:56"
picture: "kranwagen3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9906
- /details7628-2.html
imported:
- "2019"
_4images_image_id: "9906"
_4images_cat_id: "896"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9906 -->
Seite mit Kran