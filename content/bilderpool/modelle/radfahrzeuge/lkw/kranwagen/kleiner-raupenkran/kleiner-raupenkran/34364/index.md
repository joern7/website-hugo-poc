---
layout: "image"
title: "Hilfsausleger"
date: "2012-02-23T21:06:55"
picture: "kleinerraupenkran01.jpg"
weight: "1"
konstrukteure: 
- "1234567890"
fotografen:
- "0987654321"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34364
- /detailsd4f5.html
imported:
- "2019"
_4images_image_id: "34364"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:06:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34364 -->
Das Seil darf nur einmal gewickelt sein,weil der Hilfsausleger sonst zu leicht währe und das Seil durchhängen würde.