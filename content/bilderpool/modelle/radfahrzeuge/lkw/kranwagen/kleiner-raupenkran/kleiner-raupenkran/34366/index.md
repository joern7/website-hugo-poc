---
layout: "image"
title: "Hauptausleger"
date: "2012-02-23T21:06:55"
picture: "kleinerraupenkran03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34366
- /detailsc60e-2.html
imported:
- "2019"
_4images_image_id: "34366"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:06:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34366 -->
Hier ist das Seil zum Aufstellen des Hauptauslegers dreifach gewickelt.