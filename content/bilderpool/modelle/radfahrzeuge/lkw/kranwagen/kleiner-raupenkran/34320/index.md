---
layout: "image"
title: "Oberteil"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran05.jpg"
weight: "5"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34320
- /detailsfecb.html
imported:
- "2019"
_4images_image_id: "34320"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34320 -->
Im Oberteil befinden sich folgende Motoren:
1. Motor: Zum Drehen des Oberteils
2. Motor: Seilwind zum Hochziehen des Mastes
3. Motor. Seilwinde um den Hacken hoch und runter zu lassen