---
layout: "image"
title: "Motor"
date: "2012-02-20T21:16:19"
picture: "kleinerraupenkran13.jpg"
weight: "13"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34328
- /details0537.html
imported:
- "2019"
_4images_image_id: "34328"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:16:19"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34328 -->
Das ist der Motor der für die Bewgung des Hilfsauslegers zuständig ist.