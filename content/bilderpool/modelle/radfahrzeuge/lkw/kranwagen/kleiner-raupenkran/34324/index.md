---
layout: "image"
title: "Kran"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran09.jpg"
weight: "9"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34324
- /details7229.html
imported:
- "2019"
_4images_image_id: "34324"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34324 -->
Hier auf dem Bild ist der Kran fast zusammengebaut, ohen den Ausleger.