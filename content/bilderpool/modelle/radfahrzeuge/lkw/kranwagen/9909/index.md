---
layout: "image"
title: "Kranwagen"
date: "2007-04-03T17:33:56"
picture: "kranwagen6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9909
- /details56f4.html
imported:
- "2019"
_4images_image_id: "9909"
_4images_cat_id: "896"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9909 -->
von unten