---
layout: "image"
title: "Kranwagen"
date: "2007-05-19T09:12:25"
picture: "kranwagen2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10467
- /details883b.html
imported:
- "2019"
_4images_image_id: "10467"
_4images_cat_id: "954"
_4images_user_id: "557"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10467 -->
von hinten, ventile für hoch/runter,kurbel für lenkung