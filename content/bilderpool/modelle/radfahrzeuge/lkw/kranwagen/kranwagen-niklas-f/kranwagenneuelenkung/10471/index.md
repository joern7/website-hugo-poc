---
layout: "image"
title: "Kranwagen"
date: "2007-05-19T09:12:25"
picture: "kranwagen6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10471
- /detailse8da-2.html
imported:
- "2019"
_4images_image_id: "10471"
_4images_cat_id: "954"
_4images_user_id: "557"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10471 -->
stütze