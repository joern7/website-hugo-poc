---
layout: "image"
title: "Kranwagen"
date: "2007-04-30T09:00:18"
picture: "kranwagen2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10225
- /details0718.html
imported:
- "2019"
_4images_image_id: "10225"
_4images_cat_id: "926"
_4images_user_id: "557"
_4images_image_date: "2007-04-30T09:00:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10225 -->
Ausfahrmechanismus