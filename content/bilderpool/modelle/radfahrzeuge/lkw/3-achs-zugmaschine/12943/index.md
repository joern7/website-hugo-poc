---
layout: "image"
title: "mit 'offenem Verdeck'"
date: "2007-11-30T19:45:05"
picture: "achszugmaschine3.jpg"
weight: "3"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12943
- /detailsd579-2.html
imported:
- "2019"
_4images_image_id: "12943"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12943 -->
Den vorderen Teil des Führerhauses habe ich klappbar gestaltet. Hier findet die Elektrik (Empfänger, Fahrtregler) Platz. Die "Schlafkabine" des Führerhauses ist groß genug, um den Akku (8 AA-Zellen) aufzunehmen).