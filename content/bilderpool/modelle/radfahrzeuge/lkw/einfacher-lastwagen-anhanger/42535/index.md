---
layout: "image"
title: "Bahn frei!"
date: "2015-12-20T13:46:57"
picture: "einfacherlastwagenmitanhaenger02.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42535
- /detailsb9ca.html
imported:
- "2019"
_4images_image_id: "42535"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42535 -->
So sieht es von vorne aus. Eine gelbe Bauplatte für einen gewissen Akzent.