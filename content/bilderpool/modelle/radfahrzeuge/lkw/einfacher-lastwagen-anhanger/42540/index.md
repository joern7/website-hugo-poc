---
layout: "image"
title: "Die Anhängerkupplung"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger07.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42540
- /detailsd534.html
imported:
- "2019"
_4images_image_id: "42540"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42540 -->
Altbekannte Technik.