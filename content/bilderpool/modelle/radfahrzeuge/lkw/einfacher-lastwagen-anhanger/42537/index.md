---
layout: "image"
title: "Die Zugmaschine"
date: "2015-12-20T13:46:57"
picture: "einfacherlastwagenmitanhaenger04.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42537
- /details1228-2.html
imported:
- "2019"
_4images_image_id: "42537"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42537 -->
Schlicht und kindgerecht.