---
layout: "image"
title: "Anhänger von unten"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger12.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42545
- /details4ce6-2.html
imported:
- "2019"
_4images_image_id: "42545"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42545 -->
Das ist auch schon alles. Deichsel aus W7,5° und Bauplatte 90x90 sind die Hauptelemente. Die Kleinteile sollten auch alle zu erkennen sein. Die Achse geht durch.

... und hier geht es weiter:
https://www.ftcommunity.de/categories.php?cat_id=3197