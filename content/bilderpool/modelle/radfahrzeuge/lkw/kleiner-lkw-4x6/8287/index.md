---
layout: "image"
title: "LKW 4x6 5"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_06.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8287
- /detailsf1ec.html
imported:
- "2019"
_4images_image_id: "8287"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8287 -->
Hier sieht man - etwas zerlegt - den Vorderwagen mit der kompakten Vorderachse.

Die Aufhängung des Mini-Motors über eine Strebe 30 und eine Adapterlasche 31674 verläuft durch den Kettentrieb.