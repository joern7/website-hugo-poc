---
layout: "image"
title: "LKW 4x6 2"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8284
- /details9b9f.html
imported:
- "2019"
_4images_image_id: "8284"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8284 -->
Die Ansicht täuscht ein wenig durch den Blitz; der Akku-Pack schliesst nahezu plan mit der Oberseite des Dachs ab und ist somit (fast) unsichtbar.