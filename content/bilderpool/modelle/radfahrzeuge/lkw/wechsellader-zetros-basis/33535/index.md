---
layout: "image"
title: "Wechsellader-Fahrzeug"
date: "2011-11-21T22:08:23"
picture: "wlfzetros01.jpg"
weight: "1"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/33535
- /details6eae.html
imported:
- "2019"
_4images_image_id: "33535"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33535 -->
Bis jetzt gab es sowas noch garnicht auf der ft:c. Ich hab mich schon die ganze Zeit gefragt, wieso. Eigentlich ist es eine schöne mechanische Idee. Ich sage eigentlich, da ich jetzt seit März dran sitze. Massenweise Ideen für die Teleskopmechanik habe ich probiert, Nichts funktionierte. Jetzt habe ich endlich eine Lösung, siehe die weiteren Bilder.

Das Ganze soll eine gewisse Ähnlichkeit mit dem Mercedes Zetros haben. Ob mans sieht, ich weiß es nicht. 
Eigentlich war auch der Motor mal ganz originalgetreu unter der Fronthaube untergebracht, das machte dann aber Platzprobleme. Der Maßstab passt nahezu zum Originalfahrzeug, wenn man die Standard-Reifengröße annimmt.

Und natürlich wird bei mir aus jedem größeren Fahrzeug ein Feuerwehr- oder Katastrophenschutzfahrzeug.