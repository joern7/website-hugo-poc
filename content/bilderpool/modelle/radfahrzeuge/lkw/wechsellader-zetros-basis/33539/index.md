---
layout: "image"
title: "noch weiter geklappt"
date: "2011-11-21T22:08:23"
picture: "wlfzetros05.jpg"
weight: "5"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/33539
- /detailsb410-3.html
imported:
- "2019"
_4images_image_id: "33539"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33539 -->
...dabei wird der "Container" weiter nach hinten geschoben. Bei wenig Platz fährt der Fahrer gleichzeitig noch nach vorne(bzw. lässt den LKW einfach vom Gewicht rollen), um die Bewegung auszugleichen.