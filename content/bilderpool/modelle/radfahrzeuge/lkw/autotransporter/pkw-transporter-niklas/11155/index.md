---
layout: "image"
title: "PKW-Transporter"
date: "2007-07-20T16:55:57"
picture: "pkwtrans1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11155
- /detailsc697.html
imported:
- "2019"
_4images_image_id: "11155"
_4images_cat_id: "1010"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T16:55:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11155 -->
von oben. Das Unterteil ist früher zu sehen:  http://www.ftcommunity.de/categories.php?cat_id=1009
