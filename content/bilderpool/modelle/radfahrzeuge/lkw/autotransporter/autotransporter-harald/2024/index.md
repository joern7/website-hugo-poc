---
layout: "image"
title: "0x6Tr45.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr45.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2024
- /details4618.html
imported:
- "2019"
_4images_image_id: "2024"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2024 -->
Fahrgestell (geländegängig) und hintere Stütze des Aufliegers.