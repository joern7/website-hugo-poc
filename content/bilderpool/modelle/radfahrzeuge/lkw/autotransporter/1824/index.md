---
layout: "image"
title: "Autotransporter_004"
date: "2003-10-12T22:38:57"
picture: "AutoTransporter04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Autotransporter", "Sattelschlepper"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1824
- /details0cf1.html
imported:
- "2019"
_4images_image_id: "1824"
_4images_cat_id: "193"
_4images_user_id: "61"
_4images_image_date: "2003-10-12T22:38:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1824 -->
IR-Ferngesteuerter Autotransporter