---
layout: "image"
title: "Unterseite 1"
date: "2009-09-22T18:47:58"
picture: "lkwmitladeflaeche04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/25059
- /details5219-2.html
imported:
- "2019"
_4images_image_id: "25059"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25059 -->
Der Kipper wird mit den Tastern auf der rechten Seite gesteuert, mehr dazu gibts später.