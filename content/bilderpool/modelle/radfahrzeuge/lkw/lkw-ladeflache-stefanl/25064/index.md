---
layout: "image"
title: "Steuerung"
date: "2009-09-22T18:47:59"
picture: "lkwmitladeflaeche09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/25064
- /details2c65.html
imported:
- "2019"
_4images_image_id: "25064"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25064 -->
Der Kipper wird mit den beiden linken Tastern bedient. Mit dem linken wird umgepolt und mit dem rechten eingeschaltet. 
Rechts gibts nochmal so eine Steuerungseinheit und in der Mitte befinden sich noch zwei Taster um z.b. das Licht anzuschalten.