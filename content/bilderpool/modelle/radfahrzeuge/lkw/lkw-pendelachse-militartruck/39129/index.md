---
layout: "image"
title: "Doppelpendelachse Aufhängung Ausgebaut 2"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse12.jpg"
weight: "12"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39129
- /detailsb905.html
imported:
- "2019"
_4images_image_id: "39129"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39129 -->
Noch einmal im Detail: Die Kupplung zwischen den zwei Antriebseinheiten.