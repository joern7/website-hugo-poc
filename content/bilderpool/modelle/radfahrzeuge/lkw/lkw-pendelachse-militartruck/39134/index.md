---
layout: "image"
title: "Aufbau Doppelachse Teil 1: 2.Perspektive"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse17.jpg"
weight: "17"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39134
- /detailsa85b.html
imported:
- "2019"
_4images_image_id: "39134"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39134 -->
Noch mal eine andere Perspektive