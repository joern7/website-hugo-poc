---
layout: "image"
title: "Aufbau Doppelachse Teil 1"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse16.jpg"
weight: "16"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39133
- /details747b.html
imported:
- "2019"
_4images_image_id: "39133"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39133 -->
Hier sieht man den ersten Teil ohne Aufhängung