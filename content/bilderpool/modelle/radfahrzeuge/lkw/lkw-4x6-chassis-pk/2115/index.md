---
layout: "image"
title: "LKW4x6-13.JPG"
date: "2004-02-15T20:20:31"
picture: "LKW4x6-13.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2115
- /detailsd177.html
imported:
- "2019"
_4images_image_id: "2115"
_4images_cat_id: "430"
_4images_user_id: "4"
_4images_image_date: "2004-02-15T20:20:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2115 -->
