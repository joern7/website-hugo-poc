---
layout: "image"
title: "LKW4x6-15.JPG"
date: "2004-02-15T20:20:31"
picture: "LKW4x6-15.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2116
- /detailsdd0d.html
imported:
- "2019"
_4images_image_id: "2116"
_4images_cat_id: "430"
_4images_user_id: "4"
_4images_image_date: "2004-02-15T20:20:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2116 -->
So richtig was für Frickler :-)