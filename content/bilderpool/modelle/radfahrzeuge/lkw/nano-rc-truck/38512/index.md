---
layout: "image"
title: "Nano-RC-Truck 14"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_14.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38512
- /details5d4e-3.html
imported:
- "2019"
_4images_image_id: "38512"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38512 -->
Die Radaufhängung ist etwas unkonventionell, aber es gibt leider keine V-Achse (mit Rast-Enden) in der gewünschten Breite.

Die Spurweite der Zugmaschine entspricht exakt der des Aufliegers.