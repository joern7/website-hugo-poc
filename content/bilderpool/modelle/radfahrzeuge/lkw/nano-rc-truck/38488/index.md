---
layout: "image"
title: "Nano-RC-Truck 3"
date: "2014-03-25T16:31:24"
picture: "Nano-RC-Truck_4.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38488
- /detailsf8c4.html
imported:
- "2019"
_4images_image_id: "38488"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-25T16:31:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38488 -->
Ich denke, die Proportionen sind gelungen ...