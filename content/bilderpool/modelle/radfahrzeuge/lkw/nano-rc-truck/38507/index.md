---
layout: "image"
title: "Nano-RC-Truck 9"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_9.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38507
- /details85a0-2.html
imported:
- "2019"
_4images_image_id: "38507"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38507 -->
Der Motor passt haarscharf unter den Empfänger. Er ist nur über den Baustein 7,5 am Getriebe befestigt. Hält aber prima, denn die auftretenden Kräfte sind ja auch sehr gering.

Die beiden Stecker mussten ihrer Umhüllung beraubt werden, denn es fehlten ein paar Zehntelmillimeter in der seitlichen Maßkette.