---
layout: "image"
title: "4-Achs-Zugmaschine 5"
date: "2007-05-30T15:12:59"
picture: "LKW_-_5.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10558
- /details158f.html
imported:
- "2019"
_4images_image_id: "10558"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:12:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10558 -->
