---
layout: "image"
title: "4-Achs-Zugmaschine Kabine"
date: "2007-05-30T15:14:31"
picture: "LKW_-_8.jpg"
weight: "8"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10561
- /details6e19.html
imported:
- "2019"
_4images_image_id: "10561"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:14:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10561 -->
