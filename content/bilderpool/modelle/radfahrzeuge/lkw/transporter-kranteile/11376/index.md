---
layout: "image"
title: "Arretierung"
date: "2007-08-13T17:04:05"
picture: "transporterkranteile10.jpg"
weight: "10"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11376
- /details6e6d.html
imported:
- "2019"
_4images_image_id: "11376"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11376 -->
Vorne (die Stahlachse muss natürlich noch durch die Halterung).