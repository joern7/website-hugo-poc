---
layout: "image"
title: "Transporter(von vorne)"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile02.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11368
- /detailsc652.html
imported:
- "2019"
_4images_image_id: "11368"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11368 -->
Von vorne.