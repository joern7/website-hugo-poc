---
layout: "image"
title: "Nachläufer 2"
date: "2007-08-13T17:04:05"
picture: "transporterkranteile07.jpg"
weight: "7"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11373
- /detailse9cb.html
imported:
- "2019"
_4images_image_id: "11373"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11373 -->
Hier ist zu sehen wie die beiden Achsen sich bewegen können. die hintere Achse kann natürlich auch über den Stein fahren.