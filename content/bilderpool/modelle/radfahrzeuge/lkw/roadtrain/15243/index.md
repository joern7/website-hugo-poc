---
layout: "image"
title: "Mein Nachbau"
date: "2008-09-16T18:20:58"
picture: "003.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15243
- /details15a5.html
imported:
- "2019"
_4images_image_id: "15243"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15243 -->
