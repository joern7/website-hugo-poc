---
layout: "image"
title: "Ansicht"
date: "2008-09-16T18:21:00"
picture: "018.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15258
- /details9d52-2.html
imported:
- "2019"
_4images_image_id: "15258"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15258 -->
Hier mit geöffner Kippermulde.
Später wird die Kippermulde mittels Motor hochgestellt und pneumatisch geöffnet.