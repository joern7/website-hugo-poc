---
layout: "image"
title: "Korb"
date: "2008-09-19T07:59:26"
picture: "029.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15291
- /details1e45.html
imported:
- "2019"
_4images_image_id: "15291"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-19T07:59:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15291 -->
