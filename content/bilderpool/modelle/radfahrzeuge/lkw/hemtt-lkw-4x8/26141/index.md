---
layout: "image"
title: "Größen Vergleich mit 1L Flasche"
date: "2010-01-24T22:22:18"
picture: "DSCN0236.jpg"
weight: "8"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26141
- /details608c.html
imported:
- "2019"
_4images_image_id: "26141"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-24T22:22:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26141 -->
