---
layout: "image"
title: "Frontansicht"
date: "2010-01-25T19:29:24"
picture: "DSCN0238.jpg"
weight: "9"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26142
- /details0e93-2.html
imported:
- "2019"
_4images_image_id: "26142"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26142 -->
Die zwei blauen Pneumatik T-Stücke sind die "Schnellkupplungen" für Anbaugeräte.