---
layout: "image"
title: "Vorderansicht"
date: "2010-01-24T20:32:03"
picture: "DSCN0214.jpg"
weight: "2"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26135
- /detailsa3e7.html
imported:
- "2019"
_4images_image_id: "26135"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-24T20:32:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26135 -->
