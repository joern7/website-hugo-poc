---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine - Montagedetail unter der Fahrerkabine"
date: "2016-03-06T19:14:38"
picture: "einfacherabschleppwagen11.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42990
- /details9208.html
imported:
- "2019"
_4images_image_id: "42990"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42990 -->
Detail unter der Kabine (weggeklappt) mit dem Verschlußriegel. Normale S-Riegel passen nicht unter das Kabinenseitenteil.