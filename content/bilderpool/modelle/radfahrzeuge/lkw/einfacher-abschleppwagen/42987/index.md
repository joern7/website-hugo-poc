---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Anhängerkupplung mit Hakenhalter"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen08.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42987
- /detailsdfc8.html
imported:
- "2019"
_4images_image_id: "42987"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42987 -->
Wo macht man den Haken fest, wenn er nicht gebraucht wird? An der Anhängerkupplung zum Beispiel.