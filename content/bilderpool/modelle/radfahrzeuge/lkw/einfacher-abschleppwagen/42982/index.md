---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Die Befestigungspunkte des Abschleppkrans"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen03.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42982
- /detailsb25d.html
imported:
- "2019"
_4images_image_id: "42982"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42982 -->
Die Verbindung zum Chassis wird an den Streben mit WT30 erledigt. WT15 würden sich unter der Zuglast verdrehen. Der Mast hinten ist mittels BS30 und 2 Federnocken an der senkrechten Heckplatte befestigt.