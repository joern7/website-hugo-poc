---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Mastspitze"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen06.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42985
- /details6e17.html
imported:
- "2019"
_4images_image_id: "42985"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42985 -->
Ein BS30 quer nimmt die Lager der Stützrolle auf. Deren Achse trägt auch die beiden Zugstreben 169.6. Insgesamt ergibt sich ein stabiler Ausleger.