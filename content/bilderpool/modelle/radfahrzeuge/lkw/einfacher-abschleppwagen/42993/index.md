---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine - Front mit Kühlergrill"
date: "2016-03-06T19:14:38"
picture: "einfacherabschleppwagen14.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42993
- /details2e5d-2.html
imported:
- "2019"
_4images_image_id: "42993"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42993 -->
Und so sieht er jetzt von vorne aus.

... eine ungeplante Fortsetzung:
https://www.ftcommunity.de/categories.php?cat_id=3249