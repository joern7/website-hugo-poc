---
layout: "image"
title: "Ansicht von unten"
date: "2015-10-25T14:30:02"
picture: "DSC08323_1.jpg"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42124
- /details01f8-2.html
imported:
- "2019"
_4images_image_id: "42124"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42124 -->
In der Ansicht von unten sieht man, daß als Antrieb ein XM-Motor mit 2:1 Untersetzung eingebaut wurde. Die Abtriebsseite des Motors ist nach vorne gerichtet und es sitzt ein Z10 auf der Welle. Dieses treibt über ein Z20 die Antriebswelle an, die an die Hinterachse geht.

Die Hinterachskonstruktion ist ungefedert.