---
layout: "image"
title: "Die Magnetventile für die Kipperfunktion"
date: "2015-10-26T11:27:24"
picture: "DSC08332_1.jpg"
weight: "9"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42143
- /details7501.html
imported:
- "2019"
_4images_image_id: "42143"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T11:27:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42143 -->
