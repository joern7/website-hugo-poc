---
layout: "overview"
title: "Kleiner Kipper mit Doppelachsanhänger, Pneumatik und IR Fernsteuerung"
date: 2020-02-22T07:51:27+01:00
legacy_id:
- /php/categories/3137
- /categories61a9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3137 --> 
Kleiner  Kipper-LKW mit Doppelachs-Kippanhänger und IR Fernsteuerung im Stil der 80er Modellbaukästen.