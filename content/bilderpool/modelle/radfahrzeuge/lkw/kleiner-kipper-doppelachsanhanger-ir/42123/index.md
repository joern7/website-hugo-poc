---
layout: "image"
title: "Seitenansicht LKW"
date: "2015-10-25T14:30:01"
picture: "DSC08322_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: ["LKW", "Modellbaukasten", "IR-Control", "Fernsteuerung", "Pneumatik", "Kipper", "Anhänger"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42123
- /detailsd486.html
imported:
- "2019"
_4images_image_id: "42123"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-25T14:30:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42123 -->
Zur Fernsteuerung dient das aktuelle IR Control Set, daß sich auch in die 80er-Jahre-Konstruktion der Vorderachse problemlos integrieren läßt.

Zur Betätigung der Kippeinrichtung sowohl von LKW als auch des Anhängers wurde der aktuelle Kompressor integriert. Zwei Zubehör-Magnetventile (kompakter als die FT-Originale und auch etwas unterspannungsfester) sitzen in einem 60x30x30er V-Stein mit Klappe auf der rechten Fahrzeugseite.

Zur Ansteuerung von Kompressor und Magnetventilen:
Der Kompressor wurde an einen Motorausgang des IR Empfängers angeschlossen.
Beide Magnetventile sind parallel zum Kompressor angeschlossen, wobei in der Zuleitung jeweils eine Sperrdiode sitzt.
Die Dioden sind entgegengesetzt gepolt.
Das heißt, bei Ansteuerung des Motorausgangs läuft der Kompressor immer, aber abhängig von der "Drehrichtung", also der Polung des Ausgangs, wird entweder das erste oder das zweite Ventil betätigt.
Damit können über einen Ausgang also abwechselnd die LKW-Pritsche oder der Anhänger gekippt werden.