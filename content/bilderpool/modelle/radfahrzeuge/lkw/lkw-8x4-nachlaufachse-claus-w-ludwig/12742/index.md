---
layout: "image"
title: "modellevonclauswludwig82.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig82.jpg"
weight: "8"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12742
- /details256a.html
imported:
- "2019"
_4images_image_id: "12742"
_4images_cat_id: "1146"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12742 -->
