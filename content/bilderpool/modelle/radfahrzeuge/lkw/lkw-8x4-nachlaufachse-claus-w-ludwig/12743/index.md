---
layout: "image"
title: "modellevonclauswludwig83.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig83.jpg"
weight: "9"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12743
- /detailsed25.html
imported:
- "2019"
_4images_image_id: "12743"
_4images_cat_id: "1146"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12743 -->
