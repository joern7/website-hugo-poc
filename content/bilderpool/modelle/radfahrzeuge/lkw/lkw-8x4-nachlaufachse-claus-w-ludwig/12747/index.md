---
layout: "image"
title: "modellevonclauswludwig87.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig87.jpg"
weight: "12"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12747
- /details0057-2.html
imported:
- "2019"
_4images_image_id: "12747"
_4images_cat_id: "1146"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12747 -->
