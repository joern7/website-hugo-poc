---
layout: "image"
title: "modellevonclauswludwig75.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig75.jpg"
weight: "1"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12735
- /details9a4c.html
imported:
- "2019"
_4images_image_id: "12735"
_4images_cat_id: "1146"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12735 -->
