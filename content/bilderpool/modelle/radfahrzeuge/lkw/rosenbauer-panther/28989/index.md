---
layout: "image"
title: "02 Front"
date: "2010-10-14T17:59:02"
picture: "rosenbauerpanther02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28989
- /detailscf2f.html
imported:
- "2019"
_4images_image_id: "28989"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28989 -->
Das ungewöhnliche Design ist mir nur teilweise gelungen.