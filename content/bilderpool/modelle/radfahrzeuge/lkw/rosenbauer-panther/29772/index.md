---
layout: "image"
title: "34 Wasserwerfer"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther10_3.jpg"
weight: "34"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29772
- /detailsf382-2.html
imported:
- "2019"
_4images_image_id: "29772"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29772 -->
Hier kann man jetzt auch erkennen, wie der Wasserwerfer gedreht wird.