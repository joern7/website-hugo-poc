---
layout: "image"
title: "01 Rosenbauer Panther (Flughafenlöschfahrzeug)"
date: "2010-10-14T17:59:01"
picture: "rosenbauerpanther01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28988
- /detailsa64b-3.html
imported:
- "2019"
_4images_image_id: "28988"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28988 -->
Der Panther (Flughafenlöschfahrzeug) der Firma Rosenbauer aus Österreich. Ich habe versucht, ihn etwa im Maßstab 1:22 zu bauen, er ist allerdings ein paar Zentimeter zu hoch und zu lang. Ich konnte auch nicht alle Funktionen des Original übernehmen, so fehlt ihm bspw. der Frontwasserwerfer, der Löschschaumfunktion, die Federung, etc.

In ihm verbaut sind:
- Interface + Extension
- Wassertank
- Pumpe/Kompressor
- Ventil fürs Wasser
- Beleuchtung + Blinklicht
- zwei angetriebene Achsen (hinten) + Doppelachslenkung (vorne)