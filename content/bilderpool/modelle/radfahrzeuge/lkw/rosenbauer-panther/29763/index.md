---
layout: "image"
title: "25 Gesamt"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther01_3.jpg"
weight: "25"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29763
- /details01df-2.html
imported:
- "2019"
_4images_image_id: "29763"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29763 -->
Nochmal ein paar Bilder während des Auseinanderbauens