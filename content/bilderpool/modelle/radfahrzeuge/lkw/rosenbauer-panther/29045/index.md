---
layout: "image"
title: "24 Schieber"
date: "2010-10-19T18:24:56"
picture: "rosenbauerpanther13.jpg"
weight: "24"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29045
- /details4389-2.html
imported:
- "2019"
_4images_image_id: "29045"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29045 -->
Auf dem BS 7,5 ist normalerweise eine Platte, die verschiebbar ist: http://www.ftcommunity.de/details.php?image_id=28996