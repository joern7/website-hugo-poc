---
layout: "image"
title: "09 USB-Anschuss"
date: "2010-10-14T17:59:03"
picture: "rosenbauerpanther09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28996
- /details58af-3.html
imported:
- "2019"
_4images_image_id: "28996"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28996 -->
Die eine Platte auf dem Dach kann man zur Seite schieben, um an die USB-Schnittstelle des Interfaces ranzukommen.