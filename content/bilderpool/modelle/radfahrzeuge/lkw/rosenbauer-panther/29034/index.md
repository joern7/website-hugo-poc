---
layout: "image"
title: "13 unten"
date: "2010-10-19T18:24:55"
picture: "rosenbauerpanther02_2.jpg"
weight: "13"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29034
- /details8840.html
imported:
- "2019"
_4images_image_id: "29034"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29034 -->
Der Motor in der Mitte ist für die Lenkung.