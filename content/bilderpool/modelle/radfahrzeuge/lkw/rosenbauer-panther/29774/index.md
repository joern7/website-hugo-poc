---
layout: "image"
title: "36 Schlauchsystem"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther12_2.jpg"
weight: "36"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29774
- /details94c8.html
imported:
- "2019"
_4images_image_id: "29774"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29774 -->
Das Schlauchsystem quasi "unplugged"
Links oben ist das Ventil, unten der Kompressor.