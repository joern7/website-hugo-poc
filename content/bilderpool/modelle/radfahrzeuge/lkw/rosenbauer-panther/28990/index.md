---
layout: "image"
title: "03 Seite"
date: "2010-10-14T17:59:02"
picture: "rosenbauerpanther03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28990
- /detailsbc2f-2.html
imported:
- "2019"
_4images_image_id: "28990"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28990 -->
Die Fahrerhaustür ist übrigens rausklappbar.