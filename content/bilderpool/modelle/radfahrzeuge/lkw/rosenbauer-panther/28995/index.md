---
layout: "image"
title: "08 Anschlüsse"
date: "2010-10-14T17:59:03"
picture: "rosenbauerpanther08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28995
- /detailsaf61.html
imported:
- "2019"
_4images_image_id: "28995"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28995 -->
In dem Batteriefach befindet sich ein Anschluss, an den man das Steuerpult anschließen kann. Die zwei Kontakte über dem Schalter führen zum Akku, hier kann man das Ladegerät anschließen.