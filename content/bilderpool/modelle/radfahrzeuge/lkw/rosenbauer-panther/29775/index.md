---
layout: "image"
title: "37 Ventil"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther13_2.jpg"
weight: "37"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29775
- /details9594-3.html
imported:
- "2019"
_4images_image_id: "29775"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29775 -->
Das ist zwar nicht die platzsparenste Variante, dafür funktioniert sie aber zuverlässig.