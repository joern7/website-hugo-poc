---
layout: "image"
title: "King of the Road mit Auflieger - Gesamtansicht"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion1.jpg"
weight: "1"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33292
- /details6a73.html
imported:
- "2019"
_4images_image_id: "33292"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33292 -->
Das Modell ist eine "gepimpte" Variante des "King of the Road" von 1996 (http://www.ft-datenbank.de/web_document.php?id=84bb5bf9-315b-4bac-8a96-f0134904a641) mit dem Sattelauflieger aus "Advanced Super Trucks" - mit Motorisierung, Fernsteuerung und LED-Beleuchtung.