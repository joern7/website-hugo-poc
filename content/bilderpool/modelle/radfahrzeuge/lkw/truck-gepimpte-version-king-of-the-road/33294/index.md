---
layout: "image"
title: "Zugmaschine - Vorderansicht"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion3.jpg"
weight: "3"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33294
- /detailse0f4.html
imported:
- "2019"
_4images_image_id: "33294"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33294 -->
Der Kühlergrill wurde mit Metallstangen aufgewertet, und damit das Zurücksetzen unfallfreier funktioniert ...