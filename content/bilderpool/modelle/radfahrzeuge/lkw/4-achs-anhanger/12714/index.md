---
layout: "image"
title: "modellevonclauswludwig54.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig54.jpg"
weight: "9"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12714
- /detailsbc7f.html
imported:
- "2019"
_4images_image_id: "12714"
_4images_cat_id: "1138"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12714 -->
