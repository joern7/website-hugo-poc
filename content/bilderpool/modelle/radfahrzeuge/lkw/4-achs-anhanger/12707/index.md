---
layout: "image"
title: "modellevonclauswludwig47.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig47.jpg"
weight: "2"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12707
- /detailsa7dd-2.html
imported:
- "2019"
_4images_image_id: "12707"
_4images_cat_id: "1138"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12707 -->
