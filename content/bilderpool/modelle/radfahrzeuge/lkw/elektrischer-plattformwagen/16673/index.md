---
layout: "image"
title: "Platformwagen 4"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_04_klein.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/16673
- /details4763-2.html
imported:
- "2019"
_4images_image_id: "16673"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16673 -->
Der 9V-Blockakku sitzt zwischen den Hinterrädern. Er vollbringt keine Wunder, reicht aber für etliche Minuten Fahrspaß.