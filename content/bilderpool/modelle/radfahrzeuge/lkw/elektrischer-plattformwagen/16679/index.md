---
layout: "image"
title: "Platformwagen 10"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_12_klein.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/16679
- /detailsac8b.html
imported:
- "2019"
_4images_image_id: "16679"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16679 -->
Hier die Anbindung der Servos von der anderen Seite. Die Bauplatte 15*45 mit Zapfen 38277 bringt das Servo exakt in die richtige Position und Höhe für die Verlängerung mit der Statikstrebe.

Ich liebe es, wenn alles im Raster sitzt, und hier passt es perfekt! :o)