---
layout: "image"
title: "Platformwagen 9"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_11_klein.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/16678
- /detailsf8d0.html
imported:
- "2019"
_4images_image_id: "16678"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16678 -->
Hier habe ich das Servo ein wenig nach oben verschoben, um die Verbindung zwischen Servohebel und Verlängerungsstrebe detailierter zu zeigen. Nun ist der Strebenadapter 31848 gut zu sehen.