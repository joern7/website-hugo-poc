---
layout: "image"
title: "Muhlwagen"
date: "2006-04-08T12:26:04"
picture: "DSCN4426.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6025
- /detailsf664-2.html
imported:
- "2019"
_4images_image_id: "6025"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6025 -->
