---
layout: "image"
title: "Antrieb Muhlpresse 2"
date: "2006-04-08T12:26:19"
picture: "DSCN4446.jpg"
weight: "21"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6045
- /details5732-2.html
imported:
- "2019"
_4images_image_id: "6045"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6045 -->
Mit dieser Motor werden 4 Ketten angetrieben (leider geht es etwas zu schwierig. Deswegen kratzen die Zahnrader doch immer ein bisschen).
Von diese 4 Ketten liegen 2 oben und 2 unten in Muhlwagen. 
Innen im Muhlwagen ist auf die Kette ein Wand montiert. Dieser Wand kann damit nach Vorne oder Hinter transportiert werden, damit das Muhl raus gedruckt werden kann wann es voll ist.