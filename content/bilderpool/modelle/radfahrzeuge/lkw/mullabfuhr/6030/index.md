---
layout: "image"
title: "Muhlwagen"
date: "2006-04-08T12:26:05"
picture: "DSCN4433.jpg"
weight: "6"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6030
- /detailsdac7.html
imported:
- "2019"
_4images_image_id: "6030"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6030 -->
