---
layout: "image"
title: "Messgerät Draufsicht"
date: "2012-07-06T10:46:19"
picture: "ftMessung2.jpg"
weight: "2"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- /php/details/35100
- /details37ea.html
imported:
- "2019"
_4images_image_id: "35100"
_4images_cat_id: "2604"
_4images_user_id: "1524"
_4images_image_date: "2012-07-06T10:46:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35100 -->
Ansicht von oben.
Oben Links sind die drei Taster für schnellen Links- und Rechtslauf zu sehen. In der Mitte wird der Start für die Messung ausgelöst. Weiterhin ist noch eine ft-Lampe zu sehen, welche ich mittels einer Platine zur LED umgebaut habe. Die LED kann mit Gleichspannung und Wechselspannung betrieben werden.