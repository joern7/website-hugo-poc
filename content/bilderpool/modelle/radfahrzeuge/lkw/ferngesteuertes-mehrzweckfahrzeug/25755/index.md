---
layout: "image"
title: "Fahrzeug Unterboden"
date: "2009-11-11T20:20:45"
picture: "ferngesteuerteselektromehrzweckfahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25755
- /details2241.html
imported:
- "2019"
_4images_image_id: "25755"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25755 -->
Hier kann man das Zahnrad sehen, dass auf der Kardanwelle sitzt.