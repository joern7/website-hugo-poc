---
layout: "image"
title: "Gesamtansicht"
date: "2009-11-11T20:20:45"
picture: "ferngesteuerteselektromehrzweckfahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25756
- /details2ca8-2.html
imported:
- "2019"
_4images_image_id: "25756"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25756 -->
Der Regler wurde mit Klebeband fixiert, damit er bei Kurven nicht hinausfällt. Ursprünglich war er auch wie die restliche Elektronik unter der Verkleidung untergebracht.
 Er hatte seinen Platz zwischen Motor und Servo.