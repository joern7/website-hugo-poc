---
layout: "image"
title: "Gesamtansicht 2"
date: "2009-11-11T20:20:46"
picture: "ferngesteuerteselektromehrzweckfahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25758
- /details5ce4-2.html
imported:
- "2019"
_4images_image_id: "25758"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25758 -->
Leider etwas dunkel gewodene 2. Gesamtansicht