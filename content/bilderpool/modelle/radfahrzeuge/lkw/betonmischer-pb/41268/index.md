---
layout: "image"
title: "Mixer-lr-41"
date: "2015-06-24T14:11:15"
picture: "betonmischer22.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41268
- /details0236-2.html
imported:
- "2019"
_4images_image_id: "41268"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41268 -->
