---
layout: "image"
title: "Mixer-lr-01c"
date: "2015-06-24T14:11:15"
picture: "betonmischer04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41250
- /detailsb744.html
imported:
- "2019"
_4images_image_id: "41250"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41250 -->
