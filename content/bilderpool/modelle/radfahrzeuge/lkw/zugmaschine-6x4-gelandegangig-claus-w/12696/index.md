---
layout: "image"
title: "modellevonclauswludwig36.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig36.jpg"
weight: "14"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12696
- /detailsd6c1-2.html
imported:
- "2019"
_4images_image_id: "12696"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12696 -->
