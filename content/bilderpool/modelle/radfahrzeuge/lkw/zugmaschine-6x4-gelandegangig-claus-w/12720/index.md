---
layout: "image"
title: "modellevonclauswludwig60.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig60.jpg"
weight: "24"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12720
- /detailsf732.html
imported:
- "2019"
_4images_image_id: "12720"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12720 -->
