---
layout: "image"
title: "modellevonclauswludwig34.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig34.jpg"
weight: "12"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12694
- /details92fd-3.html
imported:
- "2019"
_4images_image_id: "12694"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12694 -->
