---
layout: "image"
title: "[6/6] Vorderachse"
date: "2009-09-11T21:40:46"
picture: "zugmaschineclaus6.jpg"
weight: "6"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24914
- /detailse391.html
imported:
- "2019"
_4images_image_id: "24914"
_4images_cat_id: "1717"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24914 -->
