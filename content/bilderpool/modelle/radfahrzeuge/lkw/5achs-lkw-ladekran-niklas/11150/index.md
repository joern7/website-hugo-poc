---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw08.jpg"
weight: "8"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11150
- /details73ce.html
imported:
- "2019"
_4images_image_id: "11150"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11150 -->
Hier die Mechanik zum Hoch-/Runterfahren