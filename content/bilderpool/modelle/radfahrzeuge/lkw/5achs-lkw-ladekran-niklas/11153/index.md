---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw11.jpg"
weight: "11"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11153
- /details7ef2-2.html
imported:
- "2019"
_4images_image_id: "11153"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11153 -->
Zugmaschine von unten