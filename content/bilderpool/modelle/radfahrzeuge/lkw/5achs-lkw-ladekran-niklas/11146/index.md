---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw04.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11146
- /detailsa5ca-2.html
imported:
- "2019"
_4images_image_id: "11146"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11146 -->
und nochmal etwas größer, IF zu sehen