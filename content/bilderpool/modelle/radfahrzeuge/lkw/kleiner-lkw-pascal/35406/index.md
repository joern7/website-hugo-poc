---
layout: "image"
title: "Von Unten"
date: "2012-08-29T20:08:53"
picture: "kleinerlkw04.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35406
- /detailsa36a.html
imported:
- "2019"
_4images_image_id: "35406"
_4images_cat_id: "2627"
_4images_user_id: "1122"
_4images_image_date: "2012-08-29T20:08:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35406 -->
Der LKW wird angetrieben mit einem Mini-Motor.