---
layout: "image"
title: "Fahrerhaus"
date: "2012-08-29T20:08:53"
picture: "kleinerlkw08.jpg"
weight: "8"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35410
- /details2e44-2.html
imported:
- "2019"
_4images_image_id: "35410"
_4images_cat_id: "2627"
_4images_user_id: "1122"
_4images_image_date: "2012-08-29T20:08:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35410 -->
Der Empfänger sitzt, wie mann gut erkennen kann im Fahrerhaus.