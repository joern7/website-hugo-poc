---
layout: "image"
title: "02 Front"
date: "2012-07-17T16:34:37"
picture: "zugmaschine2.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35183
- /details4774-2.html
imported:
- "2019"
_4images_image_id: "35183"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35183 -->
Den Lampenbaustein konnte ich nirgens unterbringen, also habe ich einfach die LEDs so verbaut.