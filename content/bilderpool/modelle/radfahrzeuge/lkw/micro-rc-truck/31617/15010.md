---
layout: "comment"
hidden: true
title: "15010"
date: "2011-09-01T20:34:47"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Wie ich das schon bei einer IR-Lichtschranke feststellen konnte, scheinen die roten Verkleidungsplatten durchlässig für IR-Licht zu sein. Du könntest es also einfach probieren, ob die Fernsteuerung bei völlig geschlossenem Auflieger noch funktioniert. U. U. leidet allenfalls die Reichweite, was ich bei einem Modell dieser Größe nicht weiter schlimm finde. Das Empfangsmodul würde ich in diesem Fall ganz nach oben direkt unter die Verblendung plazieren.

Gruß, Thomas