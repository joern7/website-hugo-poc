---
layout: "image"
title: "Micro-RC-Truck 13"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_13.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31626
- /detailsa405-2.html
imported:
- "2019"
_4images_image_id: "31626"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31626 -->
Alle 4 Räder werden starr angetrieben. Die Räder 23 klemmen sehr fest auf den Metallachsen (mit Kunststoffachsen funktioniert es nicht).