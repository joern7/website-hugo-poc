---
layout: "comment"
hidden: true
title: "14906"
date: "2011-08-20T12:25:17"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Dadurch, dass hinten 4 Reifen starr angetrieben werden, ist der Vortrieb in der Ebene ganz ordentlich, siehe Video. Liegt auch am vergleichsweise hohen Gewicht des Aufliegers, und der XS-Motor hat ja auch nicht so viel "Bumms".

Gruß, Thomas