---
layout: "image"
title: "Micro-RC-Truck 11"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_11.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31624
- /details8b4e.html
imported:
- "2019"
_4images_image_id: "31624"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31624 -->
Der Auflieger ist vorn voll verkleidet, nur unten schaut das Servo raus.