---
layout: "image"
title: "Nano-Truck_09"
date: "2011-12-11T21:51:12"
picture: "N_Truck_bfr_w.jpg"
weight: "29"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33630
- /details9f6f-2.html
imported:
- "2019"
_4images_image_id: "33630"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-11T21:51:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33630 -->
...diese variante bringt es auf eine länge von 240 mm, in der höhe ist jetzt alles auf oberkante servo, ohne zusätzliche höhe lässt sich wenig noch einkürzen...