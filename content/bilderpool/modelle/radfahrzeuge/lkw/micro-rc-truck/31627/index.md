---
layout: "image"
title: "Micro-RC-Truck 14"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_14.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31627
- /detailse8a9-2.html
imported:
- "2019"
_4images_image_id: "31627"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31627 -->
Alle 5 Achsen mit den Z10 sind in vertikaler Richtung jeweils um 2,5 mm versetzt. Das schafft mehr Bodenfreiheit im Bereich des Motors. Und es sieht auch aufgelockerter aus, als wenn alle 5 Zahnräder hintereinander in einer Ebene angeordnet wären.

Den Versatz erreicht man fast automatisch, wenn man viel mit Bausteinen 5 und 7,5 und Kupplungsstücken arbeitet.