---
layout: "image"
title: "Nano RC Truck 07"
date: "2011-12-05T23:19:12"
picture: "07_M_Truck_a_w.jpg"
weight: "27"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33620
- /detailsa6a6.html
imported:
- "2019"
_4images_image_id: "33620"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33620 -->
...der Antrieb, eine Kombination aus neu und alt. Das graue Getriebe bringt genügend Kraft und Geschwindigkeit auf die Piste und ist eine super Kombi zu den neuen Komponenten...
Mit der einen angetriebenen Achsen kommt der kleine auf ansehnliche Geschwindigkeit.
Auf dem gezeigten Teppichboden natürlich keine Rennen. Die 23er Räder sind auf der Getriebeachse gesteckt und drehen nicht durch.