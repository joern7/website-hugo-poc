---
layout: "image"
title: "Nano RC Truck 02"
date: "2011-12-05T23:19:12"
picture: "02_M_Truck_v_w.jpg"
weight: "22"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33615
- /detailsbea6.html
imported:
- "2019"
_4images_image_id: "33615"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33615 -->
Frontalansicht aus leichter Vogelperspektive