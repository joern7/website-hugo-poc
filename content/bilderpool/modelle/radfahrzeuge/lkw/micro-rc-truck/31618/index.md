---
layout: "image"
title: "Micro-RC-Truck 05"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_05.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31618
- /detailsa50b.html
imported:
- "2019"
_4images_image_id: "31618"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31618 -->
