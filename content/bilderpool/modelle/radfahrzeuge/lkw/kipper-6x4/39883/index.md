---
layout: "image"
title: "Unterseite (weißen Hintergrund)"
date: "2014-11-23T19:12:24"
picture: "kipperx31.jpg"
weight: "31"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39883
- /details0ffc.html
imported:
- "2019"
_4images_image_id: "39883"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39883 -->
Unterseite (weißen Hintergrund)