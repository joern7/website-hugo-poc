---
layout: "image"
title: "Antriebsstrang"
date: "2014-11-23T19:12:24"
picture: "kipperx06.jpg"
weight: "6"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39858
- /detailsefb3.html
imported:
- "2019"
_4images_image_id: "39858"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39858 -->
Es gibt kein Mitteldifferential damit die Räder na zu einander stehen.
Die Belastung auf die rastachsen ist schon ziemlich Hoch. 
Ich hab ein Radsatz mit dem alten Differential fast fertig. Aber, um das ein zu bauen muss ich aber viel ändern.
