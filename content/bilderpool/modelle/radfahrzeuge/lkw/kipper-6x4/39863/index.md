---
layout: "image"
title: "Kipper (Unteransicht)"
date: "2014-11-23T19:12:24"
picture: "kipperx11.jpg"
weight: "11"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39863
- /detailsc8f5.html
imported:
- "2019"
_4images_image_id: "39863"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39863 -->
Endschalter mit Dioden. Der Motor kann ein lehre Kipper gut hochbringen.
Ich habe es mit ein wenig Ladung versucht. Das geht alles sehr gut, richtig schwere Ladung könnte der Motor vielleicht zerbrechen.
