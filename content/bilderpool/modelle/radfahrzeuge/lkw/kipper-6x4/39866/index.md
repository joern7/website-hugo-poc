---
layout: "image"
title: "Untergestell auf Montageständer"
date: "2014-11-23T19:12:24"
picture: "kipperx14.jpg"
weight: "14"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39866
- /details58a0.html
imported:
- "2019"
_4images_image_id: "39866"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39866 -->
Links ist Hinten, rechts ist Vorne