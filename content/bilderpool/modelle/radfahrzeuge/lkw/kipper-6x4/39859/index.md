---
layout: "image"
title: "Doppelte Hinterachse"
date: "2014-11-23T19:12:24"
picture: "kipperx07.jpg"
weight: "7"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39859
- /details4770.html
imported:
- "2019"
_4images_image_id: "39859"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39859 -->
Die zwei innere Kegelräder sind auf ein Rastachse 30 gesteckt. 
Das Kegelrad vom Motor ist frei Bewegbar aufgehängt.
