---
layout: "image"
title: "Fahrerhaus und Korbe mit Ersatzrad"
date: "2014-11-23T19:12:24"
picture: "kipperx23.jpg"
weight: "23"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39875
- /details53bd.html
imported:
- "2019"
_4images_image_id: "39875"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39875 -->
Fahrerhaus und Korbe mit Ersatzrad