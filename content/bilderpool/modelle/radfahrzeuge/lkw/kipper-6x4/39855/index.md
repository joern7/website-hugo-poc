---
layout: "image"
title: "Vorderachse mit Servo (Rückansicht)"
date: "2014-11-23T19:12:24"
picture: "kipperx03.jpg"
weight: "3"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39855
- /details1a06-2.html
imported:
- "2019"
_4images_image_id: "39855"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39855 -->
Am Moment der Foto gibt es Zwei unterschiedliche Feder. Das sind inzwischen zwei Gleiche.
Der Hebel der Servolenkung ist noch frei so daß während der Zusammensetzung kein Beschädigung statt findet.
