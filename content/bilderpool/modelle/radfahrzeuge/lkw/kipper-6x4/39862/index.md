---
layout: "image"
title: "Fahrerhaus"
date: "2014-11-23T19:12:24"
picture: "kipperx10.jpg"
weight: "10"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39862
- /details0abd.html
imported:
- "2019"
_4images_image_id: "39862"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39862 -->
Leider ist das Fahrerhaus in twotone (Dunkelrot) ausgeführt. 
