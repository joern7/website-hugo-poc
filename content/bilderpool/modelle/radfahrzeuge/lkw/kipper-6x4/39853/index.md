---
layout: "image"
title: "Kipper 6x4"
date: "2014-11-23T19:12:24"
picture: "kipperx01.jpg"
weight: "1"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39853
- /details3aca.html
imported:
- "2019"
_4images_image_id: "39853"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39853 -->
Das LKW Chassis von PK aus 2004 hat mir inspiriert auch ein LKW zu bauen. 
Powermot für Antireb
S-motor für Kipper
ft Servo für die Lenkung
Sender und Empfämger von Graupner
Motorregler von CTI (THOR4)