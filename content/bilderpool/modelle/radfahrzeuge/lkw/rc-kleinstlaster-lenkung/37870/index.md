---
layout: "image"
title: "RC-Kleinstlaster 03"
date: "2013-12-02T12:57:36"
picture: "rckleinstlaster4.jpg"
weight: "4"
konstrukteure: 
- "Dieter und Uli Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37870
- /details0d13-2.html
imported:
- "2019"
_4images_image_id: "37870"
_4images_cat_id: "2814"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37870 -->
Zeigt den maximalen Lenkeinschlag. Durch den starken Motor und dem direkten Getriebe geht es flott ums Eck. Durch Vorderachsenfederung fährt es auch geschmeidig über Unebenheiten.