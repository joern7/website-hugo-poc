---
layout: "image"
title: "RC-Kleinstlaster 02"
date: "2013-12-02T12:57:36"
picture: "rckleinstlaster3.jpg"
weight: "3"
konstrukteure: 
- "Dieter und Uli Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37869
- /detailsb31b-3.html
imported:
- "2019"
_4images_image_id: "37869"
_4images_cat_id: "2814"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37869 -->
Motor und Servo ist so eng wie möglich zusammengerutscht.