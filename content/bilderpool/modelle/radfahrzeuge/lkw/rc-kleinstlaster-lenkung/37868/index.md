---
layout: "image"
title: "RC-Kleinstlaster 02"
date: "2013-12-02T12:57:35"
picture: "rckleinstlaster2.jpg"
weight: "2"
konstrukteure: 
- "Dieter und Uli Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37868
- /details023f.html
imported:
- "2019"
_4images_image_id: "37868"
_4images_cat_id: "2814"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37868 -->
Das Kabelgewirr ist hinter den Seitenblechen. Akkupack ließe sich noch verkleinern, aber so kann man ihn gut ausschalten.