---
layout: "image"
title: "Salz-und Granulatstreuer"
date: "2008-12-01T17:55:45"
picture: "winterdienstfahrzeug03.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16534
- /details4e17.html
imported:
- "2019"
_4images_image_id: "16534"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16534 -->
