---
layout: "image"
title: "Salzbehälter"
date: "2015-02-15T20:38:19"
picture: "IMG_0496_b.jpg"
weight: "8"
konstrukteure: 
- "asdf"
fotografen:
- "asdf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "asdf"
license: "unknown"
legacy_id:
- /php/details/40546
- /details41e1-2.html
imported:
- "2019"
_4images_image_id: "40546"
_4images_cat_id: "3039"
_4images_user_id: "1490"
_4images_image_date: "2015-02-15T20:38:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40546 -->
Gefüllt mit Gewindestangen als Gegengewicht.