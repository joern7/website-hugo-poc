---
layout: "image"
title: "LKW Allrad Lenkung"
date: "2007-04-24T22:01:05"
picture: "lkw2_2.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10162
- /details98dc.html
imported:
- "2019"
_4images_image_id: "10162"
_4images_cat_id: "898"
_4images_user_id: "557"
_4images_image_date: "2007-04-24T22:01:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10162 -->
Fahrerhaus von vorne