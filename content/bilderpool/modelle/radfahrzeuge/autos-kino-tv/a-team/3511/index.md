---
layout: "image"
title: "ATeam12.JPG"
date: "2005-01-04T16:28:30"
picture: "ATeam12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3511
- /detailsdeea-2.html
imported:
- "2019"
_4images_image_id: "3511"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3511 -->
Beim Einschlagen der Lenkung drehen sich die Radlagerungen um eine Achse im Raum, die *schräg* durch die Anordnung verläuft. Der eine Drehpunkt liegt am Ende der gelben Strebe, der andere ist in der Mitte der Kunststofffeder.

Diese Schräglage der Lenkachse hat ein paar interessante Konsequenzen: 

Erstens, beim Einschlagen wird die Federung so stark aufgebogen, dass sich (wenn das Hubgetriebe nicht wäre) die Lenkung von allein wieder auf Geradeauslauf stellen würde. Das ist allerdings nur im ft-Modell zu beobachten, bei echten Autos gibts das nicht.

Zweitens wird der Platzbedarf für das Radgehäuse verringert, weil der Punkt, um den das Rad dreht, innerhalb der Bodenaufstandsfläche liegt. Bei der ft-Lenkung mittels Lenkklaue liegt die Drehachse neben dem Rad, so dass es beim Lenken einen Bogen um den Drehpunkt herum fährt.

Drittens sieht man, dass sich das innere Rad beim Einschlagen richtig schräg stellt. (Das tut das äußere zwar genauso, aber man sieht es nicht so). Dadurch läuft der Reifen auf der Schulter, der Wagenkasten wird angehoben und das Eigengewicht des Fahrzeugs versucht, den Geradeauslauf wieder herzustellen. Diesen Effekt gibt es auch und gerade bei echten Autos.

Insgesamt erhöht sich dadurch also die Spurstabilität und die Lenkung flattert nicht mehr.