---
layout: "image"
title: "ATeam04.JPG"
date: "2005-01-04T16:28:20"
picture: "ATeam04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3503
- /detailsbb8e.html
imported:
- "2019"
_4images_image_id: "3503"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3503 -->
Der Wagenboden ist eine durchgehend ebene Fläche. Die tief heruntergezogene Heckklappe sorgt für eine niedrige Ladekante. Zur Stromversorgung für die Rückleuchten gibts noch ein genaueres Bild.