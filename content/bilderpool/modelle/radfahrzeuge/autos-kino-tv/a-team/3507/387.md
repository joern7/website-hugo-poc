---
layout: "comment"
hidden: true
title: "387"
date: "2005-01-05T23:56:21"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wiedermal ein echtes Harald-Meisterstück!Einfach nur TOLL, wie schön kompakt Du so viel Technik baust! Absolut super! Höchster Respekt! Ich staune immer wieder, wie unglaublich kreativ Du vermeintlich altgewohnte Teile neuen Zwecken zuführst.

Gruß,
Stefan