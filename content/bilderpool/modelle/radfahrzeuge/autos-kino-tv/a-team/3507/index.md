---
layout: "image"
title: "ATeam08.JPG"
date: "2005-01-04T16:28:21"
picture: "ATeam08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3507
- /detailsfcbc-3.html
imported:
- "2019"
_4images_image_id: "3507"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3507 -->
Alle Räder sind gefedert, bei der Hinterachse ist die Schwinge "rückwärts" eingebaut und hinten gelagert. 

An den Zahnstangen des Hubgetriebes wurde nichts geschnippelt, die Statiksteine sitzen in den Original-Lücken der Befestigungsschienen.

Die Kontaktstifte für die Beleuchtung sind mit etwas Knetmasse (gelb und blau) gesichert.