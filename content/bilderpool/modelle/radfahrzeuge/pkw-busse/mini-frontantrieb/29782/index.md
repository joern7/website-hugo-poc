---
layout: "image"
title: "Mini-Frontantrieb 1"
date: "2011-01-23T20:20:33"
picture: "Mini-Frontantrieb_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29782
- /detailsecfc-3.html
imported:
- "2019"
_4images_image_id: "29782"
_4images_cat_id: "2189"
_4images_user_id: "328"
_4images_image_date: "2011-01-23T20:20:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29782 -->
Bei diesem Modell habe ich die angetriebene Vorderachse meines Classic-Unimogs genommen, um damit einen Kleinwagen mit Frontantrieb darzustellen. Während des Bauens entstand die Zielsetzung, das Modell absolut minimalistisch aufzubauen, also nur das Notwendigste an Teilen einzusetzen. Auch wollte ich den Radstand so gering wie möglich haben, also das Maximum an Kompaktheit.

Entstanden ist ein kleines englisches Stadt-Cabrio (wegen Rechtslenker). Es fährt und lenkt ferngesteuert und hat Licht. Die Verkabelung für die Lampen habe ich für die Fotos allerdings wieder abgebaut, denn das Modell bestand dadurch fast nur aus Kabeln.

Die Proportionen liefen ziemlich schnell aus dem Ruder, aber meine Freundin findet ihn wahrscheinlich gerade deshalb "sooo süß und niedlich" ... ;o)