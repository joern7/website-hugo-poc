---
layout: "image"
title: "Mini-Frontantrieb 9"
date: "2011-01-23T20:20:34"
picture: "Mini-Frontantrieb_9.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29790
- /details7f2a.html
imported:
- "2019"
_4images_image_id: "29790"
_4images_cat_id: "2189"
_4images_user_id: "328"
_4images_image_date: "2011-01-23T20:20:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29790 -->
Die selbe Vorderachse wie bei meinem Classic-Unimog. Die Drehachsen der Achsschenkel laufen absolut exakt durch die Mitte der Kardangelenke. Daher funktioniert die Achse wirklich gut!