---
layout: "image"
title: "Oldtimer"
date: "2011-02-26T18:08:18"
picture: "oldtimer3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30134
- /detailsecb8-2.html
imported:
- "2019"
_4images_image_id: "30134"
_4images_cat_id: "2228"
_4images_user_id: "453"
_4images_image_date: "2011-02-26T18:08:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30134 -->
