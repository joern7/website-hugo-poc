---
layout: "image"
title: "Lenkung"
date: "2011-08-22T15:46:51"
picture: "selbsttragendekarosserieauto3.jpg"
weight: "3"
konstrukteure: 
- "Barth Thomas"
fotografen:
- "Barth Thomas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "choco"
license: "unknown"
legacy_id:
- /php/details/31638
- /details05cc.html
imported:
- "2019"
_4images_image_id: "31638"
_4images_cat_id: "2359"
_4images_user_id: "1347"
_4images_image_date: "2011-08-22T15:46:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31638 -->
Mal was anderes die metallstangen dienen zur stabilisierung