---
layout: "image"
title: "ohne aufbau"
date: "2011-08-22T15:46:51"
picture: "selbsttragendekarosserieauto2.jpg"
weight: "2"
konstrukteure: 
- "Barth Thomas"
fotografen:
- "Barth Thomas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "choco"
license: "unknown"
legacy_id:
- /php/details/31637
- /details63bf.html
imported:
- "2019"
_4images_image_id: "31637"
_4images_cat_id: "2359"
_4images_user_id: "1347"
_4images_image_date: "2011-08-22T15:46:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31637 -->
