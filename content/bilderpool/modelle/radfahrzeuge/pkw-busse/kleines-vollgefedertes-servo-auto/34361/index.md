---
layout: "image"
title: "Antrieb"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34361
- /details337d.html
imported:
- "2019"
_4images_image_id: "34361"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34361 -->
Der S-Motor geht über ein mittig liegendes Winkelgetriebe nach unten und dort über ein weiteres zum Kardangelenk.