---
layout: "overview"
title: "Kleines vollgefedertes Servo-Auto"
date: 2020-02-22T07:53:53+01:00
legacy_id:
- /php/categories/2541
- /categoriesddf9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2541 --> 
Ein kleines, butterweich gefedertes Auto zum Rumfahren.