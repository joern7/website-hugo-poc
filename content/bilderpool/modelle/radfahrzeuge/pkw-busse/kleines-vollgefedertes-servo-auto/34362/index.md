---
layout: "image"
title: "Fensterlinie"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34362
- /detailsf95e.html
imported:
- "2019"
_4images_image_id: "34362"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34362 -->
Wenigstens passt alles unterhalb einer gedachten Fensterlinie. Da beneide ich thomas004 um seine Konstruktionen, die total wie richtige Autos aussehen...