---
layout: "image"
title: "Von vorne"
date: "2012-08-26T15:33:25"
picture: "mgtd2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/35358
- /details7c47.html
imported:
- "2019"
_4images_image_id: "35358"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35358 -->
Man kann den Kühlergrill, die Stoßstange und die fehlenden Lampen erkennen.