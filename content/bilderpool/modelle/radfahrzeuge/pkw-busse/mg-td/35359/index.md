---
layout: "image"
title: "Das Dach"
date: "2012-08-26T15:33:25"
picture: "mgtd3.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/35359
- /details3ef5-2.html
imported:
- "2019"
_4images_image_id: "35359"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35359 -->
Das Dach kann nicht weggeklappt werden sondern nur als ganzes um z.B. die Batterie auszuwechseln entfernt werden.