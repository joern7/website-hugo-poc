---
layout: "image"
title: "pickup07.jpg"
date: "2013-07-20T17:47:36"
picture: "IMG_9081mit.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37175
- /details2714.html
imported:
- "2019"
_4images_image_id: "37175"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:47:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37175 -->
Das ist der Rest, wenn das Vorderteil herausgezogen ist.