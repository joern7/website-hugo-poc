---
layout: "image"
title: "pickup06.jpg"
date: "2013-07-20T17:46:02"
picture: "IMG_9080mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37174
- /detailsd28a.html
imported:
- "2019"
_4images_image_id: "37174"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:46:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37174 -->
Das Vorderteil kann man einfach so heraus ziehen.