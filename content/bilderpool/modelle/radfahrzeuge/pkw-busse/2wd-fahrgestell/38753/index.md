---
layout: "image"
title: "Seite vorne"
date: "2014-05-09T17:28:26"
picture: "wdfahrgestell02.jpg"
weight: "2"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38753
- /detailsaadf-2.html
imported:
- "2019"
_4images_image_id: "38753"
_4images_cat_id: "2897"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38753 -->
Diese Fahrzeug soll weiter entwickelt werden und ist gemeint ein Grund zu sein um "Fernsteuer-Teile" zu kaufen
Es ist auch meine erste Eintrag im Bilderpool