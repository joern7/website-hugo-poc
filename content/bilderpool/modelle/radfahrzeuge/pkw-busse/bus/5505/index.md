---
layout: "image"
title: "Heck_1"
date: "2005-12-20T18:26:08"
picture: "Neuer_Ordner_002.jpg"
weight: "5"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5505
- /detailsddc1-2.html
imported:
- "2019"
_4images_image_id: "5505"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5505 -->
Hier sieht man die Federung der Hinterachse. Die Zwillingsreifen sind unbedingt notwendig!
Sie liefern den nötigen Grip, um den Bus zu bewegen. 2 Reifen würden durchdrehen.