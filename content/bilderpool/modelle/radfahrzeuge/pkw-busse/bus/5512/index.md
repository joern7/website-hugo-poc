---
layout: "image"
title: "Lenkmechanik"
date: "2005-12-20T18:26:09"
picture: "Neuer_Ordner_018.jpg"
weight: "12"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5512
- /details03d5-2.html
imported:
- "2019"
_4images_image_id: "5512"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5512 -->
Die Lenkung mit Schnecke: die bekannte Schaltung mit zwei Dioden wurde verwendet.