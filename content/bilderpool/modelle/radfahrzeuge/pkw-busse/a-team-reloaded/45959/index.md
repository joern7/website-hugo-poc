---
layout: "image"
title: "Akku"
date: "2017-06-18T18:00:37"
picture: "ateamreloaded08.jpg"
weight: "8"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45959
- /detailsa1c6.html
imported:
- "2019"
_4images_image_id: "45959"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45959 -->
Der Akku liegt schräg auf Motor und Getriebe und bringt damit sein Gewicht genau auf die angetriebene Achse.