---
layout: "image"
title: "von innen"
date: "2017-06-18T18:00:37"
picture: "ateamreloaded06.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45957
- /details2ffb.html
imported:
- "2019"
_4images_image_id: "45957"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45957 -->
Diese Ansicht verbirgt mehr, als sie offenbart. Vorne vor dem unteren Rast-Z20 sitzt der Antriebsmotor, hinter dem Armaturenbrett liegt das Differenzial, und noch davor sitzt der Akku. Am linken Vorderrad ist das weiße Kegelzahnrad zu erkennen, das von innen mit einem Rast-Z10 angetrieben wird. Der Fotograf steht auf dem großen Geheimfach in der Wagenmitte.