---
layout: "image"
title: "Vorderachse"
date: "2017-06-18T18:00:36"
picture: "ateamreloaded05.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45956
- /details6af0.html
imported:
- "2019"
_4images_image_id: "45956"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45956 -->
Die Radaufhängung ist die gleiche wie beim Jeep mit Frontantrieb
https://ftcommunity.de/categories.php?cat_id=3414 . Hier lenkt aber ein Hubgetriebe, der Motor ist ganz nach unten gewandert und das Differenzial sitzt oben drauf. Vom Differenzial kommen seitlich schwarze Rastkegelzahnräder, die je ein weißes Kegelzahnrad (aus einem ft-Differenzial entnommen) antreiben, das seinerseits im Zahnkranz des "alten" Reifen 60 kämmt. Das ist hier links komplett zu sehen.

Dank dem Hinweis von Thomas (geometer) kommt das Auto ganz ohne Modding aus. Der Jeep hatte in der Radaufhängung noch einen halbierten BS7,5 -- der aber ersetzbar ist durch die  Klemmhülse 35980, die hier im Reifen, unterhalb vom Gelenkstein zu sehen ist.

Damit die Zahnräder Z10 (innen unter dem Differenzial) frei drehen können, muss die Radhalterung gegenüber dem Motor "nur so ein kleines bisschen" abgesenkt werden. Das sind nur die zwei Millimeter, die hier als Dicke der weißen und blauen Platten auftreten, aber dieser Höhenversatz setzt sich nach vorn und hinten weiter fort und stört nach Kräften.