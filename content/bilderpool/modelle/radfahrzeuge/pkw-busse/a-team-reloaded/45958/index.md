---
layout: "image"
title: "Geheimfächer"
date: "2017-06-18T18:00:37"
picture: "ateamreloaded07.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45958
- /detailsda2d.html
imported:
- "2019"
_4images_image_id: "45958"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45958 -->
Die Wagenmitte hat gleich drei Stauräume nebeneinander. Den doppelten Boden kann man wie hier anheben und z.B. auch als Tisch für ein Camping-Mobil hernehmen. Der seitliche Wagenboden (im Bild oben) wird dann zur Sitzfläche.