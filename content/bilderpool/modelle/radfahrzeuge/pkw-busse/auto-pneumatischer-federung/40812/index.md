---
layout: "image"
title: "Gesamtansicht (3)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40812
- /details9028.html
imported:
- "2019"
_4images_image_id: "40812"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40812 -->
Praktisch alles unterhalb der Gürtellinie (einschließlich Innenraum) ist mit Technik für die pneumatische Federung ausgefüllt. Das Fahrzeug sieht man hier ausgeschaltet und flach wie eine Flunder fast auf dem Boden aufliegen.