---
layout: "image"
title: "Schaltbild"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40818
- /detailsab1b.html
imported:
- "2019"
_4images_image_id: "40818"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40818 -->
Bis auf den zentralen Ein-/Ausschalter für die Stromversorgung zeigt dieses Bild den Aufbau der Federung: Der Kompressor wird vom Akku gespeist und läuft permanent durch. Er liefert auch gar nicht genug Druckluft, als dass eine Überdruckabschaltung ansprechen könnte.

Solange ein Rad zu weit eingefedert ist, wird der Kontakt geschlossen, der das jeweilige Magnetventil betätigt. Dadurch strömt Druckluft gedrosselt und also langsam in einen Druckbehälter und schließlich an den einfachwirkenden Pneumatikzylinder, der das Rad ausfedert und also das Fahrzeug anhebt.

Sobald die Sollhöhe knapp überschritten wird, wird der Kontakt geöffnet, das Magnetventil fällt ab, und so kann die Druckluft in der Zylinder/Volumen-Kombination wiederum durch die Drossel langsam ab ins Freie strömen. Das Fahrzeug senkt sich wieder ab, bis es so tief liegt, dass der Kontakt wieder geschlossen ist.

Wichtige Parameter sind hier die Einstellung der Drossel sowie die Größe des Puffervolumens. Würde man die Druckluft ungedrosselt direkt auf den Zylinder leiten, würde das Fahrzeug wilde Bocksprünge vollführen. Die Luft muss also langsam hinein und darf auch nur langsam hinausströmen. Das bewirkt a) dass sich das Auto beim Einschalten tatsächlich so majestätisch anhebt wie z.B. eine Citroen DS, GS oder CX und b) dass die einmal erreichte Bodenfreiheit ohne größere Schwankungen beibehalten werden kann.

Die Tatsache, dass nur ein einziger Kontakt verwendet wird, und nicht etwa einer für "zu tief" und ein weiterer für "zu hoch", zwischen denen aber ein bestimmter Weg liegt, führt dazu, dass das Auto sein Niveau enorm genau einhält. Man hört zwar ständig die Ventile schalten, weil das Fahrzeug mehrere Male pro Sekunde die Kontakte schließt und wieder öffnet. Das geht aber durch die Drosselung und das Volumen so langsam, dass in den kurzen Zeitspannen sich das Niveau nur so winzig ändert, wie es gerade fürs Öffnen bzw. Schließen des Kontakts notwendig ist. Das sind winzige Bruchteile von Millimetern - fürs bloße Auge nicht erkennbar.

Die hier gezeigte Anordnung gibt es ein Mal je Vorderrad sowie ein drittes Mal gemeinsam für beide Zylinder der Hinterräder. Die Kontakte der Hinterräder sind ebenfalls einfach parallel geschaltet. Sobald einer davon Kontakt hat, strömt in beide Hinterrad-Zylinder Druckluft. Das hat zur Folge, dass die Seitenneigung durch die Vorderräder eingehalten wird (auf ebenem Grund steht das Fahrzeug auch eben), während die Hinterräder auf unebenem Grund durchaus auch bei stehendem Fahrzeug unterschiedlich weit ausgefedert sein können. Die beiden Vorderräder halten also die Bodenfreiheit beide genau ein, während die Hinterradfederung sicherstellt, dass die Bodenfreiheit der Hinterräder mindestens so groß wie verlangt ist, bei einem der Räder aber überschritten sein kann.

Das Puffervolumen darf allerdings nicht zu groß sein, sonst kommt ein einzelner ft-Kompressor nicht mehr nach. Zuerst hatte ich pro Vorderrad sowie für die Hinterachse tatsächlich je einen ft-Air-Tank eingebaut. Die Federung sprach damit wirklich *butterweich* an, ganz wundervoll. Je mehr Volumen zur Verfügung steht, desto weniger ändert sich ja der Luftdruck beim Einfedern und also Einschieben der Zylinder. Jedoch ging doch durch die ständige Abluftschaltung zu viel Druckluft verloren, und der Kompressor kam nicht mehr nach - das Fahrzeug kam nicht zuverlässig hoch. Deshalb ist das "Volumen" im Modell tatsächlich nur durch relativ viel Pneumatikschlauch realisiert. Das kann der ft-Kompressor noch bewältigen, und der Federungskomfort ist auch noch gut.