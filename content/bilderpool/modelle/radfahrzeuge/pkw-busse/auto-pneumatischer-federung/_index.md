---
layout: "overview"
title: "Auto mit pneumatischer Federung und automatischem Niveauausgleich"
date: 2020-02-22T07:54:06+01:00
legacy_id:
- /php/categories/3067
- /categoriese8c6.html
- /categories644a.html
- /categories63e3.html
- /categories32dc.html
- /categories3670.html
- /categoriesbcf7.html
- /categoriesa579.html
- /categories6ab2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3067 --> 
Dieses Fahrzeug verfügt über eine recht weiche Federung, die die Bodenfreiheit unabhängig vom Beladungszustand auf Bruchteile von Millimetern genau einhält. Es war auf der Convention 2014 dabei.