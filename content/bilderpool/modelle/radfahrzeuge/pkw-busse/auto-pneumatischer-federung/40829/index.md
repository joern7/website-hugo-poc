---
layout: "image"
title: "Lenkung"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40829
- /details8daa-2.html
imported:
- "2019"
_4images_image_id: "40829"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40829 -->
Ein Blick von unten auf die Lenkmechanik.