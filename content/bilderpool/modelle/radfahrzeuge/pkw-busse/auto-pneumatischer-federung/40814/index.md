---
layout: "image"
title: "Gesamtansicht (5)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40814
- /details74e7-2.html
imported:
- "2019"
_4images_image_id: "40814"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40814 -->
Das kesse Heck hat praktisch keinen Kofferraum (wie ein C6 halt ;-), aber immerhin eine Hutablage. Ob die Statikhalter unten als Diffusor durchgehen, mag jeder selbst entscheiden.