---
layout: "image"
title: "Hinterer Niveauausgleich"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40836
- /detailsc22e.html
imported:
- "2019"
_4images_image_id: "40836"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40836 -->
Auf den Längslenkern stecken Federgelenksteine aus dem alten Elektromechanik-Programm von ft. Federt eines der Räder zu weit ein, schließt sich der Kontakt, und beide (!) hinteren Zylinder bekommen über ein gemeinsames Magnetventil langsam Druckluft zugeführt.