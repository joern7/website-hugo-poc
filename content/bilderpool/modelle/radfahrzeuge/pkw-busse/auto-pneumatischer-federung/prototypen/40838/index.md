---
layout: "image"
title: "Vorderachse in erster Variante"
date: "2015-04-20T15:15:13"
picture: "prototypen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40838
- /details45c1.html
imported:
- "2019"
_4images_image_id: "40838"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40838 -->
Diese Variante baut noch deutlich höher als die letztlich im Fahrzeug verwendete, aber das Funktionsprinzip ist schon dasselbe. Anfangs hatte ich normale 45-mm-Zylinder mit Metallkolben verwendet, aber schließlich Kompressorzylinder. Die haben eine deutlich geringere Haftreibung und sprechen deshalb viel feinfühliger an.

Videos von Prototypen stehen unter https://www.youtube.com/watch?v=7H21qJAMytc