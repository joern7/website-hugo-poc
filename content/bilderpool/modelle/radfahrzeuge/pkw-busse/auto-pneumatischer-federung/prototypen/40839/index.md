---
layout: "image"
title: "Vorderachse in erster Variante"
date: "2015-04-20T15:15:13"
picture: "prototypen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40839
- /details72e4.html
imported:
- "2019"
_4images_image_id: "40839"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40839 -->
Man sieht, dass die Konstruktion - anders als die endgültige Version - über die Reifenhöhe hinaus ragt.