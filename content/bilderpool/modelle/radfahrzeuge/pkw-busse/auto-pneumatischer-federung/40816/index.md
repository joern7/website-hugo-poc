---
layout: "image"
title: "Wirkungsweise der Federung (2)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40816
- /details890a-3.html
imported:
- "2019"
_4images_image_id: "40816"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40816 -->
Sobald das Auto eingeschaltet wird, hebt es sich schön langsam hinten und vorne an, bis es die einjustierte Bodenfreiheit erreicht. Dieser Abstand wird dann so genau eingehalten, dass man mit bloßem Auge keinerlei Schwankung erkennen kann. Das Fahrwerk federt in diesem Zustand sehr weich ein und aus, und bei Beladungsänderung (z.B. mit einem Akkupack, der ganz hinten aufgelegt oder wieder weggenommen wird) stellt der automatische Niveauausgleich den eingestellten Bodenabstand wieder automatisch her.