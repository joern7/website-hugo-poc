---
layout: "image"
title: "Routemaster"
date: "2018-05-13T08:59:58"
picture: "Routemaster_-_14_of_14.jpg"
weight: "14"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47659
- /detailsbc24-2.html
imported:
- "2019"
_4images_image_id: "47659"
_4images_cat_id: "3513"
_4images_user_id: "2739"
_4images_image_date: "2018-05-13T08:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47659 -->
Rücklichter. Die Rückfahrscheinwerfer sind über eine Diode parallel zum Motor geschaltet und leuchten daher (nur) beim rückwärts fahren.