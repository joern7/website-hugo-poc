---
layout: "image"
title: "Routemaster"
date: "2018-05-13T08:59:58"
picture: "Routemaster_-_4_of_14.jpg"
weight: "4"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47649
- /details0384.html
imported:
- "2019"
_4images_image_id: "47649"
_4images_cat_id: "3513"
_4images_user_id: "2739"
_4images_image_date: "2018-05-13T08:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47649 -->
Es gibt zwei Beleuchtungs + Sound Modi. Hier: Normales Licht + Motoren Sound