---
layout: "image"
title: "Routemaster"
date: "2018-05-13T08:59:58"
picture: "Routemaster_-_5_of_14.jpg"
weight: "5"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47650
- /details2d6c-2.html
imported:
- "2019"
_4images_image_id: "47650"
_4images_cat_id: "3513"
_4images_user_id: "2739"
_4images_image_date: "2018-05-13T08:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47650 -->
Beleuchtungsmodus 2: Party Bus Beleuchtung mit Rainbow LEDs + passendem Sound :-)