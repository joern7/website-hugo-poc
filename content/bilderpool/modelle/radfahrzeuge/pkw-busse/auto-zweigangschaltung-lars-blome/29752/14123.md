---
layout: "comment"
hidden: true
title: "14123"
date: "2011-04-18T15:34:52"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Ich habe manchmal den Eindruck, das die Kontrolle erst hier auf der Seite erfolgt :o

Zu dunkel, falsch rotiert oder unscharf - sieht man alles eigentlich vorher am PC ;)