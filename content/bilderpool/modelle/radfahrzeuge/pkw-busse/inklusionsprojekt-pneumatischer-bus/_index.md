---
layout: "overview"
title: "Inklusionsprojekt: pneumatischer Bus mit eRolli"
date: 2020-02-22T07:54:15+01:00
legacy_id:
- /php/categories/3383
- /categoriesb853.html
- /categories8795.html
- /categoriesf456.html
- /categoriesfc66.html
- /categories9d28.html
- /categoriescaa7.html
- /categories104f.html
- /categories20ba.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3383 --> 
Behindertengerechter Bus mit pneumatischen Gleitfalttüren und Luftfederung mit Niveauregulierung. Der Bus kann sich absenken. Ein e-Rolli fährt an der Bushaltestelle in den Bus und steigt später wieder aus. Die Lage der Pneumatik-Zylinder für die Luftfederung wird für die Niveauregulierung über Potentiometer analog erfasst. Die Gleit-Falt-Türen wurden aus CD-Hüllen angefertigt.