---
layout: "image"
title: "eRolli mit Liniensensor von unten"
date: "2017-03-15T21:16:58"
picture: "IMG_8494.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Liniensensor"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45532
- /details64ea.html
imported:
- "2019"
_4images_image_id: "45532"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45532 -->
Der Liniensensor hat vier Elemente