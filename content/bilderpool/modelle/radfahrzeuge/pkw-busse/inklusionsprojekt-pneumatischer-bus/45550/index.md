---
layout: "image"
title: "Bus Vorderteil"
date: "2017-03-15T21:16:58"
picture: "IMG_8549.jpg"
weight: "27"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Fahrerkabine"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45550
- /details70e4.html
imported:
- "2019"
_4images_image_id: "45550"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45550 -->
Fahrerkabine