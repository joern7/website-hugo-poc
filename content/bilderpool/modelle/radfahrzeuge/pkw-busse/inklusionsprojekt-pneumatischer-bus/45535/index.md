---
layout: "image"
title: "Luftfederung mit Potentiometer zur Lagebestimmung"
date: "2017-03-15T21:16:58"
picture: "IMG_8515.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Niveauregulierung", "Lageerfassung", "Potentiometer", "Regelabweichung", "Kompressor"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45535
- /details9f4d.html
imported:
- "2019"
_4images_image_id: "45535"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45535 -->
Der Widerstandswert des Potentiometers wird analog erfasst und über die Regelabweichung der PWM Wert für den jeweiligen Kompressor bestimmt. Es gibt einen Kompressor für vorne und einen weiteren für hinten. Die Türen werden mit einem dritten Kompressor betätigt.