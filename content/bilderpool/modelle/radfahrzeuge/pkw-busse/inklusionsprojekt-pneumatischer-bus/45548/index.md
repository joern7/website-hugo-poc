---
layout: "image"
title: "Bus von vorne mit Kamera"
date: "2017-03-15T21:16:58"
picture: "IMG_8542.jpg"
weight: "25"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Kamera", "Ampel", "Bildverarbeitung"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45548
- /detailsc2f0.html
imported:
- "2019"
_4images_image_id: "45548"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45548 -->
Die Kamera wird für die Erkennung einer Verkehrsampel eingesetzt.