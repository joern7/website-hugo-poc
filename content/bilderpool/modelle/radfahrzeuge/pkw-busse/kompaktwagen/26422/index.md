---
layout: "image"
title: "Kompaktwagen 5"
date: "2010-02-14T14:01:03"
picture: "Kompaktwagen_05.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26422
- /details4f8a-3.html
imported:
- "2019"
_4images_image_id: "26422"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26422 -->
