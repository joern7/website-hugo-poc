---
layout: "image"
title: "Kompaktwagen 17"
date: "2010-02-14T17:45:59"
picture: "Kompaktwagen_17.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26436
- /details63eb-2.html
imported:
- "2019"
_4images_image_id: "26436"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T17:45:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26436 -->
Der klassische "thomas004-Drehschemel" in seiner vorerst letzten und besten Evolutionsstufe. Mehr Stabilität geht da - glaube ich - nicht mehr.