---
layout: "image"
title: "Kleinwagen 18"
date: "2010-03-20T18:00:09"
picture: "Kleinwagen_19.jpg"
weight: "18"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26769
- /detailse543.html
imported:
- "2019"
_4images_image_id: "26769"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26769 -->
Der Unterboden mit der kompakten Anordnung von Antriebsmotor mit Getriebe, 9V-Blockakku und den beiden Achsen. Kürzer ist das Fahrzeug meiner Meinung nach nicht möglich.