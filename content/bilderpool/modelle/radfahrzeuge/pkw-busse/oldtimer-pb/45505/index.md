---
layout: "image"
title: "oldtimer12.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer12.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45505
- /details5a62.html
imported:
- "2019"
_4images_image_id: "45505"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45505 -->
