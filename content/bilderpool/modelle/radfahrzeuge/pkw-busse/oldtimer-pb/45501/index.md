---
layout: "image"
title: "oldtimer08.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer08.jpg"
weight: "8"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45501
- /details4d8c-2.html
imported:
- "2019"
_4images_image_id: "45501"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45501 -->
Damals hatte ich nur 2 schwarze Streben I-30  :-)
Den Feder kommt später unter den Accu, damit die Antriebsachse nicht hoch kommt.