---
layout: "image"
title: "oldtimer09.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer09.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45502
- /details21ee-2.html
imported:
- "2019"
_4images_image_id: "45502"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45502 -->
Veränderungen in Vergleich mit den letzten Photo:
1. Doppelter Federung war gefordert wegen den Gewicht des Accus.
2. Zwei Hülse um die Antriebsachse sorgen dafür das der Achse frei drehen bleiben kann, auch wenn er den Boden touchieren würde.