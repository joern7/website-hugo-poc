---
layout: "image"
title: "oldtimer02.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45495
- /details4eec-2.html
imported:
- "2019"
_4images_image_id: "45495"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45495 -->
Die Türen können geöffnet werden (mit alte BS 15 mit 2 roten Zapfen), und am Heck kann man den Accu wechseln.