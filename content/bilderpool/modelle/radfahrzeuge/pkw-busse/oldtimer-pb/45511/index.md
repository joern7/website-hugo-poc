---
layout: "image"
title: "oldtimer18.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer18.jpg"
weight: "18"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45511
- /details4b9a-2.html
imported:
- "2019"
_4images_image_id: "45511"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45511 -->
Heckklappen und Kottflügel Hinter