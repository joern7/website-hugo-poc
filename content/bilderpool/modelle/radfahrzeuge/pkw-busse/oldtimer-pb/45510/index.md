---
layout: "image"
title: "oldtimer17.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer17.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45510
- /detailsdc15.html
imported:
- "2019"
_4images_image_id: "45510"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45510 -->
Türe und Einstieghilfe