---
layout: "image"
title: "oldtimer16.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer16.jpg"
weight: "16"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45509
- /details4d7b.html
imported:
- "2019"
_4images_image_id: "45509"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45509 -->
Den Kurbel sitzt an die Spitze eine mal abgebrochene Rastachse