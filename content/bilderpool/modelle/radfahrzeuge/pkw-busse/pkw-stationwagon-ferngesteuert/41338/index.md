---
layout: "image"
title: "PKW-station-24"
date: "2015-06-29T23:51:57"
picture: "pkwstation21.jpg"
weight: "21"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41338
- /detailsb30e-3.html
imported:
- "2019"
_4images_image_id: "41338"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41338 -->
Past alles Hinten drin.