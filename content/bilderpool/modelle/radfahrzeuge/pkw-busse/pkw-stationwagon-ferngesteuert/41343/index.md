---
layout: "image"
title: "pkwstation26.jpg"
date: "2015-06-29T23:51:57"
picture: "pkwstation26.jpg"
weight: "26"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41343
- /details6e06.html
imported:
- "2019"
_4images_image_id: "41343"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41343 -->
Und das war der Sedan Variant.