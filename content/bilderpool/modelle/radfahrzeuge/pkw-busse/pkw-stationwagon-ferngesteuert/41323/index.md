---
layout: "image"
title: "PKW-station-09"
date: "2015-06-29T23:51:40"
picture: "pkwstation06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41323
- /details7895.html
imported:
- "2019"
_4images_image_id: "41323"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41323 -->
