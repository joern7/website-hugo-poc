---
layout: "image"
title: "Strandbuggy 1"
date: "2012-04-29T13:34:18"
picture: "Strandbuggy_1.jpg"
weight: "1"
konstrukteure: 
- "asdf"
fotografen:
- "asdf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "asdf"
license: "unknown"
legacy_id:
- /php/details/34832
- /details8b96.html
imported:
- "2019"
_4images_image_id: "34832"
_4images_cat_id: "2577"
_4images_user_id: "1490"
_4images_image_date: "2012-04-29T13:34:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34832 -->
Hallo liebe Fischertechnik Community,

Nachdem wir (Rainer (genannt Papa) und Florian (6, genannt Flori) nun schon seit einiger Zeit Eure Modelle beobachten, wollen wir auch mal ein paar Bilder beitragen.

Wir haben ein kleines Auto mit Vorderradantrieb (und in einer älteren Version auch Allradantrieb) gebaut. Als es fertig war, hat es uns an einen Strandbuggy erinnert, auch wenn die sonst wohl Hinterradantrieb haben.