---
layout: "image"
title: "detail: beide hinterradaufhängungen"
date: "2009-12-31T13:11:36"
picture: "swingbuggy07.jpg"
weight: "7"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/25998
- /detailsefbd-2.html
imported:
- "2019"
_4images_image_id: "25998"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25998 -->
jede hinterradbefestigung ist über eine achse 90grad drehbar befestigt, so können die hinterräder die neigung des fahrzeuges beim lenken ausgleichen und behalten den bodenkontakt