---
layout: "image"
title: "seitenansicht federung"
date: "2009-12-31T13:11:52"
picture: "swingbuggy12.jpg"
weight: "12"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/26003
- /details393b.html
imported:
- "2019"
_4images_image_id: "26003"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:52"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26003 -->
mit entfernter seitenverkleidung