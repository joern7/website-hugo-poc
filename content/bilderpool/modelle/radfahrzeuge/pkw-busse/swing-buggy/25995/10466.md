---
layout: "comment"
hidden: true
title: "10466"
date: "2010-01-04T19:10:50"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Bernd,
und wieder das bereits dritte gelungene ft-Modell mit deiner konstruktiven Handschrift eines "musischen Designers". Die schiefwinklige Bauweise mit Grund- und Winkelbausteinen zeigt, was man in ft mit Fantasie aus unserer Gewohnheit ausbrechend alles machen kann. Ein "swing-buggy" dürfte z.B. bis auf die Lagerteile aber auch fast ausschliesslich aus Statikteilen möglich sein. Mit dem Modell "Lenkung durch Gewichtsverlagerung" hast du eine solche Bauweise ja schon angedeutet. Interessante Felder dazu finde ich auch bei Modellkonstruktionen von Maschinentechnik mit diesen beiden Bauweisen.
Gruss Ingo