---
layout: "image"
title: "Seitenansicht"
date: "2008-01-25T15:36:04"
picture: "DSCN2045.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13383
- /details6bcd.html
imported:
- "2019"
_4images_image_id: "13383"
_4images_cat_id: "1219"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13383 -->
Ich habe hier versucht einen PKW aus dem Automobile Kasten mit den neuen gelben Bausteinen nachzubauen. Auf Grund fehlender verschiedener Bauteile ist es nicht ganz gelungen.