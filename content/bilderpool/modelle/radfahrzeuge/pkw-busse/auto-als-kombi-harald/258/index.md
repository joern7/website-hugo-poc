---
layout: "image"
title: "kombi-01"
date: "2003-04-21T21:17:58"
picture: "kombi-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/258
- /details5347.html
imported:
- "2019"
_4images_image_id: "258"
_4images_cat_id: "429"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T21:17:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=258 -->
