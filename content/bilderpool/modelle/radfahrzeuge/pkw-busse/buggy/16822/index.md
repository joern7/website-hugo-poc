---
layout: "image"
title: "Unterseite"
date: "2008-12-31T19:10:24"
picture: "buggy05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16822
- /detailsed95-2.html
imported:
- "2019"
_4images_image_id: "16822"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16822 -->
Dasselbe von schräg aufgenommen.