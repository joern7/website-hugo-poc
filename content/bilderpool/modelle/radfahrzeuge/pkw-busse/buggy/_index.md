---
layout: "overview"
title: "Buggy"
date: 2020-02-22T07:53:39+01:00
legacy_id:
- /php/categories/1517
- /categories77d8.html
- /categories812c.html
- /categories8ab9.html
- /categoriesdbb4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1517 --> 
Da ich mein ft wieder für längere Zeit wegräumen musste, wollte ich wenigstens *irgendwas* zum Rumspielen machen. Daraus ist in aller Eile an einem kurzen Abend ein etwas verbogen aussehendes Fahrzeug entstanden. ;-)