---
layout: "image"
title: "Hinterachse von innen"
date: "2008-12-31T19:10:24"
picture: "buggy07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16824
- /details72fc.html
imported:
- "2019"
_4images_image_id: "16824"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16824 -->
Die beiden Kunststofffedern wirken a) federverstärkend und b) stabilisierend gegen Seitenneigung. Je nachdem, wie die Endpunkte gelagert werden, kann man das Heck auf schwache oder starke Beladung einstellen. Dazu muss man die BS5 an den Enden der Federn so wie hier einfach ihren Weg finden lassen (viel Bodenfreiheit) oder so hindrücken, dass sie direkt oberhalb der Federstäbe enden (wenig Bodenfreiheit).