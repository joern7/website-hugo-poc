---
layout: "image"
title: "Unterboden"
date: "2008-12-31T19:10:24"
picture: "buggy04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16821
- /detailsc9cc-2.html
imported:
- "2019"
_4images_image_id: "16821"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16821 -->
Die Federstäbe aus dem alten ft-Elektromechanikprogramm übernehmen die wesentliche Federung und wirken gegen zu starke Seitenneigung. Sie werden vorne und hinten von ft-Kunststofffedern unterstützt.