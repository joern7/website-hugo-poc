---
layout: "overview"
title: "Fernbedienbares Polizeiauto mit Antrieb, Lenkung, kompletter Beleuchtung und Flügeltüren"
date: 2020-02-22T07:54:00+01:00
legacy_id:
- /php/categories/2869
- /categories0b56.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2869 --> 
Aufbauend auf dem Antriebsstrang mit grauem M-Motor und altem Differential habe ich hier eine Karosserie mit Beleuchtung als Polizeifahrzeug aufgebaut. Antrieb, Lenkung und Beleuchtung sind über eine Infrarot-Fernbedienung steuerbar. Gesteuert wird das Ganze über ein selbst gebautes Mikrocontroller-Board mit einem AVR ATMEGA16 und zwei L293DNE Motortreiber ICs.