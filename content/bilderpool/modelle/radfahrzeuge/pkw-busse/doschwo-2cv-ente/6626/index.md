---
layout: "image"
title: "Ente54.JPG"
date: "2006-07-10T18:14:04"
picture: "Ente54.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6626
- /details0a11.html
imported:
- "2019"
_4images_image_id: "6626"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:14:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6626 -->
Der Unterboden komplett. Vielleicht werde ich die hinteren Radschwingen noch länger machen; beim Original sind die Räder auch sehr weit hinten.

Die Lenkung ist durch die beiden hintereinander geschalteten Schneckenantriebe (1x m1,5 rot, 1x m1 schwarz) sehr stark untersetzt.