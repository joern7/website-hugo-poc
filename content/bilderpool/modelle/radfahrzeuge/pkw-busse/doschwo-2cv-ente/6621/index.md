---
layout: "image"
title: "Ente35.JPG"
date: "2006-07-10T18:02:22"
picture: "Ente35.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6621
- /details6247-2.html
imported:
- "2019"
_4images_image_id: "6621"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:02:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6621 -->
Die Motorhaube wird durch zwei Streben I-75 in Form gebracht. Die Messingachse steckt nachher in zwei Winkel 30 an der A-Säule.