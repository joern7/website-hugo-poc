---
layout: "image"
title: "Ente52.JPG"
date: "2006-07-10T18:10:08"
picture: "Ente52.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6624
- /details5841-2.html
imported:
- "2019"
_4images_image_id: "6624"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:10:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6624 -->
Eigentlich ist das VW-Käfer-Technik (Heckmotor + Heckantrieb). Es hat aber nicht anders gepasst.