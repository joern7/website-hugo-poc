---
layout: "image"
title: "Ente53.JPG"
date: "2006-07-10T18:11:11"
picture: "Ente53.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6625
- /details240e-2.html
imported:
- "2019"
_4images_image_id: "6625"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:11:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6625 -->
Eigentlich ist das VW-Käfer-Technik (Heckmotor + Heckantrieb). Es hat aber nicht anders gepasst.