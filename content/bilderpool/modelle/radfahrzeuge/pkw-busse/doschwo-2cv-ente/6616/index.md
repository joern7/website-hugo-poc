---
layout: "image"
title: "Ente07.JPG"
date: "2006-07-10T17:48:31"
picture: "Ente07.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6616
- /detailsa171.html
imported:
- "2019"
_4images_image_id: "6616"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6616 -->
