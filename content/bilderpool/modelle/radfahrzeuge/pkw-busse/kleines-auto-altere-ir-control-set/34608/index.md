---
layout: "image"
title: "Leicht geänderte Front"
date: "2012-03-07T22:31:34"
picture: "servicepack2.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34608
- /detailsa7c3-2.html
imported:
- "2019"
_4images_image_id: "34608"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:31:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34608 -->
Außerdem wurden die V15-Eckbausteine unterhalb der Lampen gegen diese Winkel ausgetauscht, weil die Reifen beim Lenken mitunter etwas an den Eckbausteinen schleifte.