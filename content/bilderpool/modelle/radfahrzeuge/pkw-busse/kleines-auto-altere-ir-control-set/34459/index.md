---
layout: "image"
title: "Lenkung (4)"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34459
- /detailsa908.html
imported:
- "2019"
_4images_image_id: "34459"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34459 -->
Hier sieht man ganz gut, wie der MiniMot mit BS5 direkt mittig auf dem S-Motor sitzt.