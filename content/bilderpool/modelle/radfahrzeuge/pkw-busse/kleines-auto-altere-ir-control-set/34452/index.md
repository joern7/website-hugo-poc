---
layout: "image"
title: "3/4-Ansicht von vorne"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34452
- /details2f5c-2.html
imported:
- "2019"
_4images_image_id: "34452"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34452 -->
Noch ein bisschen Auto und Karosserie üben ;-) Dieses Auto ist klein, ungefedert, ohne Differential heckgetrieben, und gelenkt mit dem älteren IR-Control-Set, also ohne Servo.

thomas004 möge mir die Ähnlichkeit mit http://www.ftcommunity.de/categories.php?cat_id=1910 verzeihen, aber wenn man versucht, sowas so klein wie möglich zu bauen, ergeben sich ein paar Dinge wohl fast zwangsweise.

Die Lampen tun auch (M3 an der Fernbedienung). Beim Beifahrer sitzt der Ein-/Ausschalter. Beide Insassen wurden ihrer Arme beraubt, damit alles reinpasst - einschließlich der Batterie zwischen den zweien.