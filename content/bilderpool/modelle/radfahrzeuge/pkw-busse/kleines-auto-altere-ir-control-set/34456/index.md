---
layout: "image"
title: "Lenkung (1)"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34456
- /details8a59.html
imported:
- "2019"
_4images_image_id: "34456"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34456 -->
Der MiniMot treibt eine Riegelscheibe an, die zwischen zwei Klemmringen auf einer K-Achse steckt. Sie ist mit der hier sichtbaren Lagerlasche und vorne in einem roten BS15 mit Loch gelagert. Diese Riegelscheibe dreht beim Lenkanschlag einfach durch, sodass die Lenkgeometrie und insbesondere die Mittelstellungserkennung (siehe später) ungestört bleiben.