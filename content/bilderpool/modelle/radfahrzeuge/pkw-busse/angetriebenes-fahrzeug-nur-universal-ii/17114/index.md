---
layout: "image"
title: "Fahrzeug aus Universal II-Teilen (Lenkung)"
date: "2009-01-20T16:50:22"
picture: "IMG_5757.jpg"
weight: "4"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- /php/details/17114
- /details1696.html
imported:
- "2019"
_4images_image_id: "17114"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17114 -->
Die Achsschenkellenkung nur aus den zur Verfügung stehenden Teilen konstruiert. Ich habe lange herumprobiert mit Gelenken und Schanieren. Es sollte eine kompakte Lenkung werden, die Lenkachse sollte möglichst nahe an der Felge sitzen und der Lenkeinschlag groß sein. Die Lösung lag schließlich in der Verwendung der Statik-Teile. 
Man sieht auf dem Bild auch den vorne quer eingebauten MiniMot.