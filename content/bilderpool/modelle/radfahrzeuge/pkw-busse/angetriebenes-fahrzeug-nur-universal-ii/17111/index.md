---
layout: "image"
title: "Universal II-Fahrzeug"
date: "2009-01-20T16:50:21"
picture: "IMG_5750.jpg"
weight: "1"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- /php/details/17111
- /detailsd753-2.html
imported:
- "2019"
_4images_image_id: "17111"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17111 -->
Ein Fahrzeug nur aus Teilen des Universal II-Kasten mit MinoMot. Schön ist es nicht, aber es funktioniert.