---
layout: "image"
title: "Fahrzeug aus Universal II-Teilen"
date: "2009-01-20T16:50:22"
picture: "IMG_5751.jpg"
weight: "2"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- /php/details/17112
- /detailsb9e6.html
imported:
- "2019"
_4images_image_id: "17112"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17112 -->
Das Fahrtzeug aus einer anderen Perspektive. Auf die Stabilität habe ich besonderen Wert gelegt. Es hält auch das Spiel einer 2jährigen aus.