---
layout: "image"
title: "Gefedertes Auto"
date: "2004-04-24T14:49:41"
picture: "Gefedertes_Auto_001F.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2370
- /details1d7a.html
imported:
- "2019"
_4images_image_id: "2370"
_4images_cat_id: "220"
_4images_user_id: "104"
_4images_image_date: "2004-04-24T14:49:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2370 -->
Der Gag war eigentlich nur, dass es außer den großen Reifen ausschließlich aus den ft-Teilen meiner Tochter gebaut ist.