---
layout: "image"
title: "Vorderachse"
date: "2014-12-30T07:14:11"
picture: "tongabuggy08.jpg"
weight: "8"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/40062
- /details768f.html
imported:
- "2019"
_4images_image_id: "40062"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40062 -->
Vorderachse, Stoßstange demontiert.
Panhard-Stab zur Stabilisierung.
Die originale fischertechnik Feder sind oben demontiert. Nachdem die BIC-Feder im Platz sind haben die keine Funktion meher.