---
layout: "image"
title: "Heck, Kofferraum geöffnet"
date: "2014-12-30T07:14:11"
picture: "tongabuggy02.jpg"
weight: "2"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/40056
- /details6a5c-2.html
imported:
- "2019"
_4images_image_id: "40056"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40056 -->
Zum schliessen des Kofferraums ist ein 4-Gelenk konstruiert.