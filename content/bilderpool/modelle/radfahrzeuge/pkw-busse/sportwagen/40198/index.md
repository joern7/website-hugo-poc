---
layout: "image"
title: "Ansicht von schräg vorne"
date: "2015-01-07T22:44:05"
picture: "sportwagen02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40198
- /details4a4e.html
imported:
- "2019"
_4images_image_id: "40198"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40198 -->
Er ist ferngesteuert mit der IR-Control. Die Fronthaube ist an den Seiten etwas hochgebogen, um Platz  für die Vorderräder zu bekommen, gleichzeitig aber auch ganz vorne nach unten gezogen. Die Spoiler sind irgenwie zusammengefrickelt.