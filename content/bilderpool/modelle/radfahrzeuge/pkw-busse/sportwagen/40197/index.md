---
layout: "image"
title: "Sportwagen"
date: "2015-01-07T22:44:05"
picture: "sportwagen01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40197
- /details507a.html
imported:
- "2019"
_4images_image_id: "40197"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40197 -->
Ich möchte euch einen Sportwagen präsentieren. Er ist glaube ich ein Mischling aus verschiedenen Vorbildern, rot ist jedenfalls immer eine gute Farbe für so ein Auto.