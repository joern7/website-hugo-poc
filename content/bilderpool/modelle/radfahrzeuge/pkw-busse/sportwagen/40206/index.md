---
layout: "image"
title: "geöffnete Heckklappe"
date: "2015-01-07T22:44:05"
picture: "sportwagen10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40206
- /details2edd-3.html
imported:
- "2019"
_4images_image_id: "40206"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40206 -->
Wenn man die gezeigten Bauteile entfernt, kann man die Heckklappe öffnen, vor allem um die Akkus zu laden. Ich habe Federgelenksteine verwendet, dadurch geht die Klappe fast von alleine hoch. Das fühlt sich fast an als wenn Gasdruckfedern drin wären.