---
layout: "overview"
title: "Allrad-Citroën"
date: 2020-02-22T07:53:37+01:00
legacy_id:
- /php/categories/686
- /categories1323.html
- /categories5ca5.html
- /categories0aa7.html
- /categories5ae1.html
- /categoriesfc02.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=686 --> 
Ein Auto mit Allradantrieb, Lenkung, Federung, Einzelradaufhängung, vollautomatischem Niveauausgleich und einstellbarer Bodenfreiheit.