---
layout: "image"
title: "Von rechts"
date: "2006-10-02T16:16:38"
picture: "allradcitroen01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7083
- /details77c2.html
imported:
- "2019"
_4images_image_id: "7083"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7083 -->
Na ja, von der Form her erinnert es vielleicht nur entfernt an einen Citroën. Das Ziel war auch weniger Optik, sondern nur mit Standard-ft-Teilen ein gelenktes und gefedertes Allrad-Auto zu bauen. Dies ist das Modell so wie es auf der ft Convention 2006 ausgestellt war.