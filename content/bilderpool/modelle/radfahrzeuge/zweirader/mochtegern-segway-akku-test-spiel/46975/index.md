---
layout: "image"
title: "Gesamtansicht"
date: "2017-12-10T22:55:39"
picture: "moechtegernsegwayfuerakkutestundspiel1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46975
- /detailsf8ed-2.html
imported:
- "2019"
_4images_image_id: "46975"
_4images_cat_id: "3476"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:55:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46975 -->
Auf den letzten beiden Conventions sah ich so ein cooles schwarzes Teil ferngesteuert herumrasen. Das wollte ich nachbauen. Dies hier ist eine völlig ungeregelte Version: Je ein Powermotor treibt ein großes Speichenrad (mit je zwei alten fischertechnik-Gummiraupenbändern drauf) an. Dreht man an der IR-Fernsteuerung zu weit aus, überschlägt sich das Innere einfach. Man muss also gefühlvoll beschleunigen und bremsen. Dann aber kann das Teil langsam und sehr schnell fahren, auf der Stelle drehen, etc.