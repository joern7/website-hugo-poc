---
layout: "image"
title: "Unterseite"
date: "2017-12-10T22:55:39"
picture: "moechtegernsegwayfuerakkutestundspiel2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46976
- /details65ef-2.html
imported:
- "2019"
_4images_image_id: "46976"
_4images_cat_id: "3476"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:55:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46976 -->
In der Mitte steckt der von Robert Unger und Oliver-André Urban zum Test zur Verfügung gestellt selbstentwickelte Akkublock - und der funktionierte ganz hervorragend gut!

Die Motoren sitzen tief, damit der Schwerpunkt unterhalb der Drehachse liegt.