---
layout: "image"
title: "Umbau auf Reifen mit mehr Grip"
date: "2015-03-01T10:40:27"
picture: "IMG_0410.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["ebike", "bike", "Motorrad", "balancieren", "MPU6050"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40608
- /details4f99-2.html
imported:
- "2019"
_4images_image_id: "40608"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-03-01T10:40:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40608 -->
... und größerer Standfläche