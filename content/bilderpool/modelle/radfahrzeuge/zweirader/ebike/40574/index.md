---
layout: "image"
title: "GY-86 Huckpack auf Arduino-Nano"
date: "2015-02-19T18:44:53"
picture: "Huckepack.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Huckepack", "GY-86", "Arduino", "Nano", "Servo"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40574
- /details981c.html
imported:
- "2019"
_4images_image_id: "40574"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-02-19T18:44:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40574 -->
Hier sieht man die Montage des Boards GY-86 auf dem Arduino Nano sowie die Rückseite des Eigenbau-Boards mit dem Anschluss für den Servo-Motor