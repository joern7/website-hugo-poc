---
layout: "image"
title: "selbst balancierendes eBike"
date: "2015-02-03T16:49:34"
picture: "Bike1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Gyroskop", "Beschleunigungssensor", "IMU", "Interial", "Measurement", "Unit", "Kippwinkel", "Servo", "Bike", "Zweirad", "Balancieren"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40437
- /details79f9.html
imported:
- "2019"
_4images_image_id: "40437"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-02-03T16:49:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40437 -->
In das rote Gepäckteil kommt die Elektronik bestehend aus: Arduino Nano (18 x 45 mm), GY-86 mit MPU6050 (3-Achs-Gyro + Beschleunigungssensor) und Selbstbau-Board zum Ansteuern des Lenk-Servos und des Antriebsmotors. Kippt das Bike nach rechts, lenkt es nach rechts und beschleunigt etc...