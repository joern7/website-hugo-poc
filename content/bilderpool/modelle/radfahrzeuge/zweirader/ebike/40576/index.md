---
layout: "image"
title: "Ansicht von schräg oben"
date: "2015-02-20T18:29:18"
picture: "IMG_0374.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Modding", "Schrauben", "Servo", "Hebel", "Servohebel", "Vordergabel"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40576
- /detailscd99.html
imported:
- "2019"
_4images_image_id: "40576"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-02-20T18:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40576 -->
Hier sieht man, dass ich ein bisschen Modding mit Schrauben gemacht habe, um die Vordergabel am Servo-Hebel zu befestigen