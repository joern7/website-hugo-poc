---
layout: "image"
title: "Bicycle"
date: "2008-06-04T17:04:04"
picture: "bike1.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Bicycle"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14615
- /details298b.html
imported:
- "2019"
_4images_image_id: "14615"
_4images_cat_id: "2233"
_4images_user_id: "585"
_4images_image_date: "2008-06-04T17:04:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14615 -->
This was my entry into the www.instructables.com Bicycle Month contest. (I created several different versions of the bike). 

My entry can be found here: http://www.instructables.com/id/fischertechnik-Bicycle/
Please visit the page and vote for me! Thanks! 

Richard

***google translation: 

Dies war mein Einstieg in die www.instructables.com Fahrrad-Month-Wettbewerb. (Ich habe mehrere verschiedene Versionen des Fahrrads).

Mein Beitrag können Sie hier abrufen: http://www.instructables.com/id/fischertechnik-Bicycle/
Bitte besuchen Sie die Seite und stimmen Sie für mich! Danke!