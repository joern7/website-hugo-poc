---
layout: "image"
title: "Der Akku..."
date: "2011-11-06T22:46:33"
picture: "ueberschlagfahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/33423
- /details88a7-2.html
imported:
- "2019"
_4images_image_id: "33423"
_4images_cat_id: "2477"
_4images_user_id: "1122"
_4images_image_date: "2011-11-06T22:46:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33423 -->
...dient in diesem Fall als Gegengewicht.