---
layout: "image"
title: "Neues Programm vom Segway"
date: "2011-05-20T22:38:23"
picture: "Neues_Hauptprogramm_Segway.jpg"
weight: "3"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
keywords: ["Segway", "Programm", "Neu"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/30588
- /details3166-3.html
imported:
- "2019"
_4images_image_id: "30588"
_4images_cat_id: "2281"
_4images_user_id: "-1"
_4images_image_date: "2011-05-20T22:38:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30588 -->
verbessertes Programm von meinem Segway