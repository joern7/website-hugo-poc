---
layout: "image"
title: "Conference Bike mit den Machern des großen Bikes"
date: "2006-01-09T16:10:19"
picture: "Ft-CoBi-SoerenUndTil.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/5583
- /detailse9eb.html
imported:
- "2019"
_4images_image_id: "5583"
_4images_cat_id: "484"
_4images_user_id: "381"
_4images_image_date: "2006-01-09T16:10:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5583 -->
Das sind die beiden Macher des fahrbaren ConferenceBikes Sören Nossek und Til Sauerwein