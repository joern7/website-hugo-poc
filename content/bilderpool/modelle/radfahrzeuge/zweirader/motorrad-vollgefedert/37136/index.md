---
layout: "image"
title: "Motorrad (vordere Ansicht)"
date: "2013-07-02T08:30:08"
picture: "motorradvollgefedert3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/37136
- /detailsef0d.html
imported:
- "2019"
_4images_image_id: "37136"
_4images_cat_id: "2756"
_4images_user_id: "1112"
_4images_image_date: "2013-07-02T08:30:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37136 -->
Auch das vordere Rad hat eine Federung. Die Lenkergriffe sind nur lose eingeharkt.