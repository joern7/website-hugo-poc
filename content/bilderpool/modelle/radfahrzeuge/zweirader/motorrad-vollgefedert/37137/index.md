---
layout: "image"
title: "Motorrad (Gepäckfach)"
date: "2013-07-02T08:30:08"
picture: "motorradvollgefedert4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/37137
- /detailsf309.html
imported:
- "2019"
_4images_image_id: "37137"
_4images_cat_id: "2756"
_4images_user_id: "1112"
_4images_image_date: "2013-07-02T08:30:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37137 -->
Um eine gute Leistung zu bekommen, habe ich zwei Lithium-Polymer Akkus von meinem Hubschrauber verbaut. Diese sind leicht, haben eine hohe Leistung und sind sehr platzsparend. Die 9V Batterie wäre leider zu schwach.