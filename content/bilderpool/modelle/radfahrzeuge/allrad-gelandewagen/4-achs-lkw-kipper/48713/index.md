---
layout: "image"
title: "Fehlgeschlagene Vorversuche (2)"
date: 2020-05-10T11:38:53+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ebenfalls chancenlos: Hubgetriebe treibt nur waagerecht und schiebt Hebel hoch (der ebenfalls wieder kardanisch unter die Mitte der Mulde greift). Die notwendigen Kräfte sind nicht aufzubringen, wenn die Mulde flach liegt.

Immerhin fiel die Kaulquappe mit der Leuchtsteinabdeckung zwischen den beiden BS7,5 ab, die dieses Element gegen Druck sehr stabil macht (es ist in der ft:pedia beschrieben).