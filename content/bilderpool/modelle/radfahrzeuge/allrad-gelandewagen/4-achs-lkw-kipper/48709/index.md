---
layout: "image"
title: "Seitenansicht"
date: 2020-05-10T11:38:48+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Antrieb, Lenkung und das Anheben der Mulde werden per IR-Fernbedienung gesteuert. Für einen Akku wäre reichlich Platz im Führerhaus, aber ich habe das Modell direkt vom Netzgerät gespeist. Es ist auch so schon schwer genug.