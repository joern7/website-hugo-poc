---
layout: "image"
title: "Fehlgeschlagene Vorversuche (3)"
date: 2020-05-10T11:38:47+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Seilführung dient dazu, die weit auseinanderliegenden äußeren Enden kräftig zusammen zu ziehen.