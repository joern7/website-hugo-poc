---
layout: "image"
title: "Rückspiegel"
date: 2020-05-10T11:38:34+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Immerhin gibt es verstellbare Rückspiegel an beiden Seiten. Das sind h4-Spiegelbausteine, und man kann sie an dem Riegel, der im Statikträger und BS7,5 steckt, einstellen.