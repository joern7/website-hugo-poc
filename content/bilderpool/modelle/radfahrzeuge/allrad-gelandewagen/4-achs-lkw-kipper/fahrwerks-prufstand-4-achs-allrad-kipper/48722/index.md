---
layout: "image"
title: "5:2-Untersetzung"
date: 2020-05-10T11:39:26+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Über die 32 Zähne des inneren Zahnkranzes eines Z40, gegenüber den 40 Zähnen des Z40 bekommt man die "5" auf einfache Weise in ein Getriebe: 40 = 5 * 8 und 32 = 4 * 8, so haben wir da schon die 5:4.