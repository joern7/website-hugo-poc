---
layout: "image"
title: "Power-Motoren 50:1 innen"
date: 2020-05-10T11:39:30+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

6 der 8 Power-Motoren sind solche mit roter Kappe und eingebauter 50:1-Untersetzung. Die gehen direkt per Z10 auf je ein Z40, an dem das Exzenter sitzt. Die Elektromechanik-Schaltscheiben sind um einen Rad-Zacken versetzt angebracht, damit der Taster für genau die Hälfte der Umdrehung gedrückt ist. Die Taster sitzen unter den Schaltscheiben.

Die Exzenterachsen hier sind zwei getrennte. Sie treffen sich nur in der Mitte im BS15 mit Bohrung.