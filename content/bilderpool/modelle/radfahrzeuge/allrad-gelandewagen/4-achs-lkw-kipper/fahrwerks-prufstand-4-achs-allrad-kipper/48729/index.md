---
layout: "image"
title: "Die Radaufnahmen"
date: 2020-05-10T11:39:35+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die 8 Räder des LKW passen genau in diese Radaufnahmen. Die Räder könnten sich darin drehen und lenken. Wenn der Antrieb des LKW kräftig genug ist (bzw. wäre...), könnte man den LKW also "in voller Fahrt durchs Gelände jagen". Auch mit nicht eingeschaltetem Antriebsmotor wird das Fahrwerk aber recht beeindruckend malträtiert - und es hält das gut aus. Siehe das Video dazu.