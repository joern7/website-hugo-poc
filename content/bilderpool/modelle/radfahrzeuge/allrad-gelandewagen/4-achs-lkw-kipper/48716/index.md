---
layout: "image"
title: "Blick ins Fahrwerk"
date: 2020-05-10T11:38:57+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn der LKW zerlegt wird, liefere ich noch Detailaufnahmen nach.