---
layout: "image"
title: "Fehlgeschlagene Vorversuche (3)"
date: 2020-05-10T11:38:50+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick auf den halb ausgefahrenen Scherenhub.