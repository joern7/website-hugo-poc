---
layout: "image"
title: "Die Mulde"
date: 2020-05-10T11:38:40+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kippmulde ist relativ leicht gebaut und hat großes Volumen.