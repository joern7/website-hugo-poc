---
layout: "image"
title: "Kardanische Aufhängung"
date: 2020-05-10T11:38:54+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das hier ist ein ganz witziges Abfallprodukt: Damit ein Schieber eine Mulde nach drei Richtungen kippen kann, muss alles in alle Richtungen beweglich sein. Das leistet diese Baureihe vom BS7,5 über einen drehbaren Adapter, in den zwei Distanzscheiben eingebracht sind (sonst würde er an der Klemmhülse 7,5 haken), eine Achse15, eine Klemmkupplung 15 und einen normalen Gelenkstein.