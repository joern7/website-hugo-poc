---
layout: "comment"
hidden: true
title: "4315"
date: "2007-10-16T21:36:10"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Klar, echte Monstertrucks sind ja auch nicht fürs Gelände gemacht und entfalten ihre volle Wirkung auch nur mit viel Anlauf und Schwung! Von daher passt es schon. :o)

Alles in allem auf alle Fälle ein super Modell!

Gruß, Thomas