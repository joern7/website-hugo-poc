---
layout: "comment"
hidden: true
title: "3575"
date: "2007-06-28T17:39:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Nein nein, das "ganz ohne was" bezog sich auf eine Untersetzung **hinter** dem Kardangelenk, oder eine Verstärkung für die Gelenke selbst.

Und ganz richtig, wenn du hier noch Gehäuse und Verkleidung drumbaust, ist der Fahrspaß dahin. Das habe ich beim "Hummer" auch schon gesehen.

Gruß,
Harald