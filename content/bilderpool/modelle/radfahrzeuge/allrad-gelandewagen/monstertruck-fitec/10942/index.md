---
layout: "image"
title: "Heckantrieb"
date: "2007-06-27T18:34:59"
picture: "Monstertruck3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10942
- /detailsda2e.html
imported:
- "2019"
_4images_image_id: "10942"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10942 -->
Hier sieht man den Heckantrieb.