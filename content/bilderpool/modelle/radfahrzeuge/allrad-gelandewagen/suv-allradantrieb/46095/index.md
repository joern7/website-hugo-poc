---
layout: "image"
title: "und jetzt... Vorhang auf!"
date: "2017-07-12T23:40:59"
picture: "suvx12.jpg"
weight: "12"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46095
- /detailsb0f0.html
imported:
- "2019"
_4images_image_id: "46095"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46095 -->
Hier kommt die Begründung der überlangen Motorhaube: Das Fahrzeug begann als Erprobungsträger für einen Twin-Engine-Antrieb: 
die beiden Motoren werden erstmal über ein Differenzial kombiniert (hier mitten drin zu sehen). Von da geht es aufs Mittendifferenzial, und von dort in getrennten Strängen zu den beiden Achsen. Das Vorderachs-Differenzial liegt hier verdeckt unter dem Summier-Differenzial.

Durch gegenläufig gleich schnellen Antrieb erhält man den Fahrzustand "forced zero" oder "forced stop", bei dem das Fahrzeug steht, aber keineswegs weg rollen kann und insbesondere am Hang unter Last da bleibt, wo es ist. 

Mit Variation der Drehzahlen kann man von Null beginnend mit sanftem Anlauf alle Fahrgeschwindigikeiten bis zur Summe der Motordrehzahlen erreichen. Das hatte auch das Fahrgeschäft "Inferno" https://ftcommunity.de/details.php?image_id=4067 schon drinnen. Und auch das Problem beim Ganzen ist nicht neu (und war nur in Vergessenheit geraten):

es taugt nichts, weil der IR-Empfänger den Summenstrom der Motoren begrenzt. 

Der Bluetooth-Empfänger soll da ein bisschen besser sein, aber ... mir hat das gereicht, und das Auto hat jetzt nur einen Motor für den Antrieb.