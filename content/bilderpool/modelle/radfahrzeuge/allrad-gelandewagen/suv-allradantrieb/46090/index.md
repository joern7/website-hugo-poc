---
layout: "image"
title: "Heckpartie von unten"
date: "2017-07-12T23:40:59"
picture: "suvx07.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46090
- /details2d5f.html
imported:
- "2019"
_4images_image_id: "46090"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46090 -->
Hier mit montierten Rädern. Das Differenzial sitzt starr im Rahmen; die Räder schwingen jeweils um ihr Rast-Z10 herum.