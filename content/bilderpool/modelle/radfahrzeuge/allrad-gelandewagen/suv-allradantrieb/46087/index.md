---
layout: "image"
title: "Heckklappe geöffnet"
date: "2017-07-12T23:40:59"
picture: "suvx04.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46087
- /details4a89.html
imported:
- "2019"
_4images_image_id: "46087"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46087 -->
Der Akku liegt an seinem Platz. Die Ft-Federn haben etwas Silikonschlauch zur Verstärkung drinnen. Das taugt aber nicht viel, weil die ganze Feder unter Last weg knickt.