---
layout: "image"
title: "Versuchsträger mit Übersetzung 1:1 mit Z15"
date: "2017-07-12T23:40:59"
picture: "suvx11.jpg"
weight: "11"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46094
- /details26bf-2.html
imported:
- "2019"
_4images_image_id: "46094"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46094 -->
Es passt alles zusammen, aber die Zahnräder haben zu wenig Klemmung und rutschen durch.