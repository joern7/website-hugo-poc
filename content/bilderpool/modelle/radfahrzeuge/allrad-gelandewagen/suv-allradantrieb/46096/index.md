---
layout: "image"
title: "Twin-Engine"
date: "2017-07-12T23:40:59"
picture: "suvx13.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46096
- /details0441.html
imported:
- "2019"
_4images_image_id: "46096"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46096 -->
Eine üble Frickelei, und lehrreich war sie auch (sprich: alles schön und fein, taugt aber nicht). Die zwei Motoren der oberen Etage werden zusammengeführt, von da geht es aufs Mittendifferenzial (links am Bildrand); und von dort kommen die Antriebswellen für Vorder- und Hinterachsen.