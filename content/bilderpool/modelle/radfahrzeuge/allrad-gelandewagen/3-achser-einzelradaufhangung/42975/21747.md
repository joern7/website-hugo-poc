---
layout: "comment"
hidden: true
title: "21747"
date: "2016-03-03T13:04:15"
uploadBy:
- "Severin"
license: "unknown"
imported:
- "2019"
---
Danke dir!
Die Statikstreben hab ich zu beginn genutzt, jedoch verbiegen sie sich in ihre flache Richtung. Man kann dann das Rad sehr leicht parallel zum Chassis biegen. Die Verbindung über Federnocken ist erstaunlich stabil, lohnt sich auszuprobieren!