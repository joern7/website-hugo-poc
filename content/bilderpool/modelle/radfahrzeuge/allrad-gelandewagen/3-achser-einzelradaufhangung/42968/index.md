---
layout: "image"
title: "3-Achs Allrad-Fahrwerk mit Einzelradaufhängung"
date: "2016-03-02T12:54:17"
picture: "achseinzel1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/42968
- /detailsb2b1.html
imported:
- "2019"
_4images_image_id: "42968"
_4images_cat_id: "3195"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42968 -->
Meine letzte Tüftelei: Ein 3-Achsfahrwerk mit Einzelradaufhängung und Allradantrieb. Das Mittendifferenzial kann in drei verschiedene Modi gestellt werden: Klassischer Allrad mit Mittendifferenzial (Beide Z10 ganz links), Mit Differenzialsperre (Die Z10 mittig) und reiner Heckantrieb. (Die Z10 ganz rechts)
Fehlt "nur" noch eine Karosserie..