---
layout: "comment"
hidden: true
title: "21735"
date: "2016-03-02T14:12:02"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Aufhängung hat ja eine schicke Mechanik. Genügt da echt eine einzelne Feder pro Rad? Oder geht das nur, solange noch keine Karosserie drauf ist?

Gruß,
Stefan