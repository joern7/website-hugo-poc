---
layout: "image"
title: "Universalfahrzeug Seite / nah"
date: "2015-10-18T15:42:43"
picture: "universalfahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42088
- /detailsa3a6.html
imported:
- "2019"
_4images_image_id: "42088"
_4images_cat_id: "3132"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42088 -->
Detail der Bühne