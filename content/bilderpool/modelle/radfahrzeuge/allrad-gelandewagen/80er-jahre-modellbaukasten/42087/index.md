---
layout: "image"
title: "Universalfahrzeug von hinten"
date: "2015-10-18T15:42:43"
picture: "universalfahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42087
- /details029a.html
imported:
- "2019"
_4images_image_id: "42087"
_4images_cat_id: "3132"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42087 -->
Hebebühne von hinten