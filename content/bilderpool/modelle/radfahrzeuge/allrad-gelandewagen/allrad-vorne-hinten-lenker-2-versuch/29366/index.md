---
layout: "image"
title: "Erstes Viertel des neuen Unterbaus"
date: "2010-11-25T17:33:13"
picture: "allradvornehintenlenkerversuch1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29366
- /details50ba.html
imported:
- "2019"
_4images_image_id: "29366"
_4images_cat_id: "2131"
_4images_user_id: "381"
_4images_image_date: "2010-11-25T17:33:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29366 -->
Diesen Allrad-Vorne-Hinten-Lenker gab es schon auf 2 Conventions zu sehen.
Jetzt hatte ich aber eine Idee das Fahrzeug noch kompakter zu machen.
In der letzten Version hatte ich die beiden (alten) Differentiale für vorne und hinten in Längsrichtung eingebaut, um Platz zu sparen.
Das war aber nicht sehr stabil.
So gehts auch und ist noch ein bisschen schmaler.