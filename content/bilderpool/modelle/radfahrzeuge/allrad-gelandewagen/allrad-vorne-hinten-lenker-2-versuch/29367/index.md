---
layout: "image"
title: "Der Lenkmotor mit Schalter zum Geradeausfahren"
date: "2010-11-25T17:33:14"
picture: "allradvornehintenlenkerversuch2.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29367
- /details14ba.html
imported:
- "2019"
_4images_image_id: "29367"
_4images_cat_id: "2131"
_4images_user_id: "381"
_4images_image_date: "2010-11-25T17:33:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29367 -->
Durch diesen Schalter wird es möglich, dass das Fahrzeug auf Knopfdruck wieder geradeaus fährt.
Ist der Schalter bedämpft, dann wird so lange gelenkt, bis der Schalter nicht mehr bedämpft ist.
Dann wird in entgegengesetzter Richtung gelenkt, bis der Schalter wieder bedämpft ist.
In dieser Position fährt er dann geradeaus.
Die Lenkachse macht also eine Refernzfahrt, nach welcher das Fahrzeug geradeaus fährt.