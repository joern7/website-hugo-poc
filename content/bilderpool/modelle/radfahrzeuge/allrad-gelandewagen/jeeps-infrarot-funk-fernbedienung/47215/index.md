---
layout: "image"
title: "Infrarot-Fernbedienungsempfänger Detail"
date: "2018-01-30T16:23:37"
picture: "J3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["TSOP", "34538", "AGC5"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47215
- /details8d4c-2.html
imported:
- "2019"
_4images_image_id: "47215"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47215 -->
Hier sieht man den passenden Empfänger-IC auf dem selbstgebauten Nano-Shield:

- TSOP 34538 (AGC5)