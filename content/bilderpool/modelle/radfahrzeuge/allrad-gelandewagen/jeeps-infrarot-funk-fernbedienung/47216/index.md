---
layout: "image"
title: "Motortreiber MX1508"
date: "2018-01-30T16:23:37"
picture: "J4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["MX1508", "Motortreiber", "Nano", "Shield", "Arduino"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47216
- /details2cd9.html
imported:
- "2019"
_4images_image_id: "47216"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47216 -->
Auf der anderen Seite des Empfängers sitzt der Motortreiber MX1508. Diese roten Boards bekommt man für 0,6 Euro/Stück aus China. Damit können zwei Motoren betrieben werden (jeweils mit bis zu 10V und 1,5A).