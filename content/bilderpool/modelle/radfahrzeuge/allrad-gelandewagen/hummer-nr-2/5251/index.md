---
layout: "image"
title: "Hummer01.JPG"
date: "2005-11-07T18:58:39"
picture: "Hummer01.JPG"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "HMMWV", "Hummer", "Humvee"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5251
- /details2c55.html
imported:
- "2019"
_4images_image_id: "5251"
_4images_cat_id: "450"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T18:58:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5251 -->
Ähnlichkeiten mit dem Hummer No.1 ( http://www.ftcommunity.de/categories.php?cat_id=363 ) sind kein Zufall. Aber Peter hat deutlich stabiler gebaut und dem Gerüst ein ansprechendes Äußeres verliehen. Man beachte die Stahlfedern und andere Metallteile im Antriebsstrang.