---
layout: "image"
title: "Hummer"
date: "2007-10-23T19:02:01"
picture: "hummer1.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12287
- /details1e9b.html
imported:
- "2019"
_4images_image_id: "12287"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-23T19:02:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12287 -->
Mit Hilfe einiger Tipps von MisterWho, habe ich den Frontteil noch ein bisschen umgebaut und ich glaube so sieht es besser aus.
Die Lenkung ist nur rein gesteckt, sie wird noch ein bisschen tiefer gesetzt, weil der vordere Teil vom Hummer (noch) höher ist, 

Dadurch kommt das Vorder Rad noch ein bisschen Höher mit dem oberen Rand, Die Boden freiheit passt sich dem Heck an.