---
layout: "image"
title: "Hinterachse"
date: "2007-10-23T19:02:03"
picture: "hummer5.jpg"
weight: "12"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12291
- /details3d49.html
imported:
- "2019"
_4images_image_id: "12291"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-23T19:02:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12291 -->
Hier ist die Federung ganz entlastet, sie trägt also auch nicht das Gewicht von dem Hummer. Wie sie ist wenn das Auto sein normal Gewicht hat, kann ich noch nicht sagen.