---
layout: "image"
title: "Hinterachse"
date: "2007-10-23T19:02:02"
picture: "hummer2.jpg"
weight: "9"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12288
- /details5614.html
imported:
- "2019"
_4images_image_id: "12288"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-23T19:02:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12288 -->
Radkasten, der Platz um den Reifgen reicht zum Federn der Räder super aus.