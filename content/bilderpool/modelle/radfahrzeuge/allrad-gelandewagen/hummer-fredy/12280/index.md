---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:46"
picture: "ersteversuchefuereinenhummer1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12280
- /detailsa4b1-2.html
imported:
- "2019"
_4images_image_id: "12280"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12280 -->
Beschreibung:
Die Lenkung wird für einen Hummer, sie ist noch etwas zu schmal. Das heißt ich werde sie noch verbreidern.

Das Zahnrad im Vordergrund dient zum Antrieb, ein Stückchen darunter im Hintergrund erkennt man das Differenzial für die Lenkung.