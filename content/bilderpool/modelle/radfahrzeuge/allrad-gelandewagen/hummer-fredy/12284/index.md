---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:47"
picture: "ersteversuchefuereinenhummer5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12284
- /details9145-2.html
imported:
- "2019"
_4images_image_id: "12284"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12284 -->
Hier ist der Lenkmotor abgebaut.