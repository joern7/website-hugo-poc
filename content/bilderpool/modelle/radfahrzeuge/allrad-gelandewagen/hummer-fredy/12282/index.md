---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:47"
picture: "ersteversuchefuereinenhummer3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12282
- /details396a.html
imported:
- "2019"
_4images_image_id: "12282"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12282 -->
Unteransicht, 
Oben am Bildrand ist der Lenkhebel zu sehen.