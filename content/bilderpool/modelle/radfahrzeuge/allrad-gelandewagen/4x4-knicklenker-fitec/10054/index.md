---
layout: "image"
title: "4x4"
date: "2007-04-11T09:59:11"
picture: "4x48.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10054
- /detailsdf36.html
imported:
- "2019"
_4images_image_id: "10054"
_4images_cat_id: "908"
_4images_user_id: "456"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10054 -->
So ist die Hebemechanik hochgefahren.