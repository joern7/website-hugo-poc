---
layout: "image"
title: "Allrad 5"
date: "2006-03-15T21:07:36"
picture: "Allrad_5.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5895
- /details36ad.html
imported:
- "2019"
_4images_image_id: "5895"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5895 -->
Hier die nachträglich eingebaute Differenzialsperre der Vorderachse. Hinten ist auch noch eine drin. Schöner wäre es, wenn sie im Fahrbetrieb zuschaltbar wäre (mach ich dann beim nächsten mal...). Man kann sich gut vorstellen, wie schwer die Lenkung z.B. auf einem Glastisch geht, wenn beide Sperren drin sind, oder? Im Stand geht gar nix... ;o)