---
layout: "image"
title: "Allrad 1"
date: "2006-03-15T21:07:35"
picture: "Allrad_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5891
- /details3108.html
imported:
- "2019"
_4images_image_id: "5891"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5891 -->
Hier mein Beitrag zum beliebten Thema "Gelenkter Allrad". Die Knicklenkung scheint ne gute Lösung des Problems zu sein.

Der Antrieb aller 4 Räder erfolgt mit einem Power-Motor über 3 Differenziale. Die Knicklenkung wird von einem S-Motor übernommen. Zunächst ohne Differenzialsperren konzipiert, lässt sich das Fahrzeug sehr leicht lenken. Im Gelände (Sofa-Kissen-Landschaft ;o) mussten dann aber 2 Differenzialsperren vorn und hinten her. So ausgerüstet fährt und lenkt es sich auf glattem Boden sehr schwer, im Gelände wühlt er sich aber ÜBERALL durch!