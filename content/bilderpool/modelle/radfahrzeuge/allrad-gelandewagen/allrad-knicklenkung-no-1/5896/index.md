---
layout: "image"
title: "Allrad 6"
date: "2006-03-15T21:07:36"
picture: "Allrad_7.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5896
- /detailsfd78.html
imported:
- "2019"
_4images_image_id: "5896"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5896 -->
Hier noch ne Draufsicht teilzerlegt. Man sieht gut die Knicklenkung mit dem geklemmten Zahnrad und die hintere Differenzialsperre.