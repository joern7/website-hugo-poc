---
layout: "image"
title: "Linke Seite"
date: "2014-01-12T17:58:03"
picture: "pickup07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Reinmüller, Sebastian Erler"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38043
- /detailsa59a.html
imported:
- "2019"
_4images_image_id: "38043"
_4images_cat_id: "2829"
_4images_user_id: "1924"
_4images_image_date: "2014-01-12T17:58:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38043 -->
