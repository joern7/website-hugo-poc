---
layout: "image"
title: "Führerhausstufen"
date: "2014-01-12T17:58:03"
picture: "pickup21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Reinmüller, Sebastian Erler"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38057
- /details7518.html
imported:
- "2019"
_4images_image_id: "38057"
_4images_cat_id: "2829"
_4images_user_id: "1924"
_4images_image_date: "2014-01-12T17:58:03"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38057 -->
