---
layout: "image"
title: "Allradauto"
date: "2006-12-26T21:04:39"
picture: "Allradauto1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8157
- /details6c38.html
imported:
- "2019"
_4images_image_id: "8157"
_4images_cat_id: "748"
_4images_user_id: "456"
_4images_image_date: "2006-12-26T21:04:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8157 -->
Das ist ein Auto mit Allradantrieb, Knicklenkung, Licht und Federung.