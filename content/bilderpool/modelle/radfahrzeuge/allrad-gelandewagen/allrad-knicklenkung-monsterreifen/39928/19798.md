---
layout: "comment"
hidden: true
title: "19798"
date: "2014-12-15T23:33:48"
uploadBy:
- "Kay100"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan, mein Sohn, 8 Jahre, und ich sind hier in der Community noch ganz neu, aber von Deinem Allrader vollkommen begeistert. Speziell das Video, was Du heute veröffentlicht hast, ist der Hammer.
Wie ist der Allrader entstanden? Wolltest Du ihn direkt bauen, oder hast Du einfach ausprobiert und er ist es dann geworden....? 
Wir haben selber ein Mickymouseauto gebaut und worden total stolz auf die extreme Kompaktheit und die optimierte Kabelführung,aber Dein Fahrzeug ist unglaublich. Herzlichen Glückwunsch zu diesem einzigartigen Fahrzeug.
Kay