---
layout: "image"
title: "Gesamtansicht Hydraulik unten"
date: "2010-01-09T12:09:42"
picture: "gelaendewagenmitallradantriebundhydraulik08.jpg"
weight: "8"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- /php/details/26037
- /detailsb636-2.html
imported:
- "2019"
_4images_image_id: "26037"
_4images_cat_id: "1837"
_4images_user_id: "1052"
_4images_image_date: "2010-01-09T12:09:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26037 -->
Wenn die Hydraulik unten ist schleift der Kompressor am Rad...
Aber das Modell war nur ein Experiment