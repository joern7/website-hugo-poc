---
layout: "comment"
hidden: true
title: "29"
date: "2003-06-01T11:59:39"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
StifteÜber die Stifte hab ich mich auch gewundert! 
Ich hab die Leuchtsteine über ebay bezogen und die Stifte waren schon dran. Sie sind aus Vollmaterial und bilden mit der durchgehenden Löthülse/Buchse **ein** Teil. Da hat also niemand gefrickelt :-) Dank der Stifte kann man die Leuchtsteine direkt aneinander reihen, aber warum ft da so einen Aufwand getrieben hat, weiß ich nicht. Mit ein paar lose beiliegenden Metallfedern wäre das einfacher zu machen.