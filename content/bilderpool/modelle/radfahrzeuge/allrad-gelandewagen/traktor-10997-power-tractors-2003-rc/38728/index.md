---
layout: "image"
title: "Motorregler"
date: "2014-04-27T20:38:31"
picture: "IMG_0011.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38728
- /details24cb-2.html
imported:
- "2019"
_4images_image_id: "38728"
_4images_cat_id: "2891"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T20:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38728 -->
meine neueste Errungenschaft, ein Motorregler von XXL Modellbau,
EE11432  10A Fahrtregler 1A BEC 2S Lipo 4-7 NIxx Regler Car
13,47¤ zzgl. Versand (http://www.xxl-modellbau.de/)
der kann genau "unsere" Fischertechnik Akkuspannung vertragen, hat genug Reserve an Belastbarkeit,
und je nach Ausführung eine eingebaute Bremsfunktion. 
d.h. wenn ich bei voller oder halber Vorwärtsfahrt den Hebel nach hinten ziehe (in die Rückwärtsfahrt), wird der Motor elektrisch gebremst - normalerweise würde er ausrollen.
sobald ich den Steuerhebel wieder auf "0"-position setze, kann ich dann entweder wieder vorwärts oder rückwärts fahren. die Regelung arbeitet proportional zum Steuerknüppel, d.h. ich kann sehr sanft anfahren und gut langsame Geschwindikeiten fahren, sehr präzise..

Dank "BEC" kann der Empfänger aus dem Fahrakku versorgt werden.

und für das Geld - astrein! ;-)