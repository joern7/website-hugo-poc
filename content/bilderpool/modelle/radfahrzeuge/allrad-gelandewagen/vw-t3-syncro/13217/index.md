---
layout: "image"
title: "VW T3 Syncro 6"
date: "2008-01-03T18:19:58"
picture: "VW_T3_Syncro_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/13217
- /detailsa9c3-2.html
imported:
- "2019"
_4images_image_id: "13217"
_4images_cat_id: "1194"
_4images_user_id: "328"
_4images_image_date: "2008-01-03T18:19:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13217 -->
Durch die Reibung im vorderen Antriebsstrang ist die Drehmomentverteilung sehr hecklastig, was aber gerade bei Anstiegen gut funktioniert. Durch die hohe Masse auf der Vorderachse wird dies wieder etwas ausgeglichen.