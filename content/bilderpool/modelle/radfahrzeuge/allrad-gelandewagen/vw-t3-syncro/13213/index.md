---
layout: "image"
title: "VW T3 Syncro 2"
date: "2008-01-03T18:19:58"
picture: "VW_T3_Syncro_07.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/13213
- /details18d2.html
imported:
- "2019"
_4images_image_id: "13213"
_4images_cat_id: "1194"
_4images_user_id: "328"
_4images_image_date: "2008-01-03T18:19:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13213 -->
