---
layout: "comment"
hidden: true
title: "4903"
date: "2008-01-03T19:41:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Starkes Stück wiedermal, toll! Hättest Du von den interessanten Innereien wie der Lenkung noch ein paar Detailbilder? Wie immer Hut ab! Klasse!

Gruß,
Stefan