---
layout: "image"
title: "Ansicht von oben"
date: "2014-01-16T15:59:48"
picture: "TopView.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38073
- /details7196-2.html
imported:
- "2019"
_4images_image_id: "38073"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38073 -->
Hier kann man den maximalen Lenkeinschlag schön sehen. Die Lenkung ist voll funktionsfähig und ist aufgebaut nach dem Ackermannprinzip. Das führt dann dazu, daß das kurveninnere Rad stärker einlenkt als das kurvenäußere, da es ja auf der Kreisbahn einen kürzeren Weg zurücklegen muß.
In Verbindung mit einem Differential ist das das Grundprinzip aller heutigen PKWs. 
Wenn dies nicht beachtet wird, führt dies zu schwergängigem Lenken (das Fahrzeug will einfach nicht richtig durch die Kurve fahren, bzw. zum Rubbeln der Reifen auf dem Untergrund.