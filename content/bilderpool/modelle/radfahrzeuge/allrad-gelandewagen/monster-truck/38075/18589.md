---
layout: "comment"
hidden: true
title: "18589"
date: "2014-01-17T16:40:57"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
also, für die "späte Untersetzung" hab ich mir jetzt ein Planetengetriebe direkt am Reifen überlegt, welches 1:3 untersetzt, optisch fast nicht auffällt und auch die Stabilität nicht beeinträchtigt.
Da ich die Umsetzung ganz gut finde, habe ich die Lösung separat gepostet:
http://ftcommunity.de/categories.php?cat_id=2832

Leider kann ich es noch nicht am Fahrzeug ausprobieren, da mir dafür Teile für das 2. Rad fehlen