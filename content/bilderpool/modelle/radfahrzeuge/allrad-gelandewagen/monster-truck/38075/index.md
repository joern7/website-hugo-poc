---
layout: "image"
title: "... und die dazu passende Ansicht von unten"
date: "2014-01-16T15:59:48"
picture: "ChassisBottomView.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38075
- /details0a69.html
imported:
- "2019"
_4images_image_id: "38075"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38075 -->
von unten sieht man die Mechanik ein bisschen besser.
Die 2 Motoren liegen übereinander. Zu sehen ist hier nur der ft Powermotor der die Hinterräder antreibt. 
Der zweite Motor liegt darüber und ist etwas kleiner (kein ft-Motor). Dieser ist für die Lenkung zuständig, welche über eine Zahnstange bedient wird.
Details von der Lenkung sind allerdings schwer zu erkennen. Da mache ich mal Bilder, wenn ich das Ding wieder auseinandernehme (ich will da eh noch was optimieren).

Der Antriebsstrang ist einfach aufgebaut. Aufgrund des Schlupfes habe ich schon Teile mit Metallteilen ersetzt (Naben, Kardangelenke, Achsverbinder). Leider ist der Truck aber immer noch nicht fahrtüchtig. Die verbliebenen ft Teile im Antriebsstrang sind für die Belastungen nicht geeignet. Ich habe überwiegend Probleme mit dem Differential (die Zahnräder innerhalb des Differentials sind ja nur aus Plastik und drehen auf den Achsen durch bei größerer Belastung) und bei den Kegelzahnrädern (die Quetschkupplungen greifen auf den Metallachsen nicht ausreichend; außerdem drehen sich die Verbindungen im Rückwärtsgang auf). 
Das ganze Auto bringt satte 3 kg auf die Waage! Diese müssen erst mal bewegt werden. Ich fürchte, wenn ich die Probleme im Antriebsstang gelöst habe, stelle ich dann fest, daß die Leistung des Powermotors nicht ausreicht.