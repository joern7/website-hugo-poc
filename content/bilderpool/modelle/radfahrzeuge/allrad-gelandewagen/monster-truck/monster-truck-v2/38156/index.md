---
layout: "image"
title: "Monster Truck V2"
date: "2014-02-04T20:39:24"
picture: "MonsterTruckV2.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38156
- /details545c.html
imported:
- "2019"
_4images_image_id: "38156"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38156 -->
im Vergleich zur ersten Version fällt die Karosserie natürlich gleich auf. Diese ist übrigens 100% FischerTechnik. Ich bin eigentlich nicht so sehr der Designer, aber irgendwie ist mir die gelungen.
Beleuchtung hat er auch (Frontscheinwerfer und Rückleuchten). 
Neu ist auch die Hinterachse mit Planetengetrieben an jedem Rad.