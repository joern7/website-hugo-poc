---
layout: "image"
title: "Schaltung EIN/AUS"
date: "2014-02-04T20:39:24"
picture: "Schaltung.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38162
- /details4a59-2.html
imported:
- "2019"
_4images_image_id: "38162"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38162 -->
dieses Gimmik musste irgendwie sein...Die Schaltung ist nicht nur Deko! Mit dem Schaltknüppel schaltet man das Modell Ein und AUS.
Der Schalter dazu und das Schaltgestänge sitzen unter der Sitzbank. Von oben im zusammengebauten Modell ist dann nur der Schaltknüppel zu sehen.