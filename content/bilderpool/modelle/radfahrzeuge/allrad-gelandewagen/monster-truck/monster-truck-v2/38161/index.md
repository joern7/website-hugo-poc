---
layout: "image"
title: "Motorraum"
date: "2014-02-04T20:39:24"
picture: "Motorraum.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38161
- /detailsd302.html
imported:
- "2019"
_4images_image_id: "38161"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38161 -->
Im Motorraum findet der RC Empfänger seinen Platz. 
Auf diesem Bild sieht man meine Idee mit dem Kühlergrill am Besten: RAUPENBELÄGE !
naja, damit das eine saubere Frontansicht ergibt, muß die Kette stramm sitzen. Irgendwie passen aber die Kettenglieder nicht so richtig ins 15mm Raster, so daß ich hier ein bisschen tüfteln musste.