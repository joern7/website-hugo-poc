---
layout: "image"
title: "Chassis"
date: "2014-02-04T20:39:24"
picture: "ChassisDraufsicht.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38163
- /details1f89-2.html
imported:
- "2019"
_4images_image_id: "38163"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38163 -->
Das Chassis hat sich kaum geändert. 
Zwischen die Querstreben wird der Akkupack fixiert.