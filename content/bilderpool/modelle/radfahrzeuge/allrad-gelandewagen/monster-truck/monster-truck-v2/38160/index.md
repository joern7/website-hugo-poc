---
layout: "image"
title: "Karrosserie von unten"
date: "2014-02-04T20:39:24"
picture: "KarosserieVonUnten.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38160
- /details6a6a.html
imported:
- "2019"
_4images_image_id: "38160"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38160 -->
Die Karosserie lässt sich einfach vom Chassis lösen.
In der schwarzen Box ist ein alter RC-Empfänger, mit dem man zwei DC Motoren ansteuern kann. (einer für den Antrieb und einer für die Lenkung; diese Remote Control ist außerdem super für Kettenfahrzeuge !). Der Empfänger passt gut unter die Motorhaube.
Der Akku-Halter passt im Chassis zwischen die Streben (sieht man später auf einem anderen Bild). Ich verwende übrigens Nickel-Zink Akkus. Bei nominal 1,5 Volt komme ich damit bei 6 Akkus auf 9 Volt, was ideal ist für fischertechnik!