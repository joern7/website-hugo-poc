---
layout: "image"
title: "06-Fahrstand"
date: "2007-07-15T17:49:00"
picture: "06-Fahrstand.jpg"
weight: "6"
konstrukteure: 
- "unbek."
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11073
- /detailsf643-2.html
imported:
- "2019"
_4images_image_id: "11073"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11073 -->
Ohne Worte.

Eigentlich nur interessant der Stoßdämpfer. Das ist käufliche Industrieware, dessen Vorspannung und Dämpfungskonstante mit den Schrauben über seiner Länge eingestellt werden und mittels der angeschlossenen Druckpatrone. Der Rest ist ein passendes Zusammenspiel mit den langen Schraubenfedern. Der Federweg hier sind gut 30 cm. Die so schier unendlich weiche Federung begrenzt natürlich die Kräfte auf die Konstruktion und verhindert so den Achsbruch.