---
layout: "image"
title: "Details der Lenkung (1)"
date: "2010-02-11T18:38:49"
picture: "versucheineshummers1.jpg"
weight: "1"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26321
- /detailscb4b.html
imported:
- "2019"
_4images_image_id: "26321"
_4images_cat_id: "1872"
_4images_user_id: "1057"
_4images_image_date: "2010-02-11T18:38:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26321 -->
Der Motor ist dabei nich mal festgenaut,da er bei Lenkbewegungen immer nach vorne oder hinten geht