---
layout: "image"
title: "Land Rover 4x4 5"
date: "2006-12-08T10:42:04"
picture: "Land_Rover_4x4_06.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7722
- /details409e.html
imported:
- "2019"
_4images_image_id: "7722"
_4images_cat_id: "730"
_4images_user_id: "328"
_4images_image_date: "2006-12-08T10:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7722 -->
Hier sieht man gut, dass ich im Detail sehr viel wert auf Stabilität gelegt habe. Die gelbe Strebe zwischen den Rastachsen hält die Kette straff, und vorn an der Lenkachse halten 6 "Bausteine 5" in einem fetten Klotz die beiden Kegelräder des Vorderachsantriebes zusammen.

Im Gelände hält der Antrieb dann wirklich extrem gut! Wenn es zu steil wird und nicht mehr weitergeht, rutschen keine Zahnräder und nix fliegt auseinander, sondern der Power-Motor wird langsamer bis zum Stillstand.