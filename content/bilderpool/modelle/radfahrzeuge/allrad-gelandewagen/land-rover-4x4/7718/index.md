---
layout: "image"
title: "Land Rover 4x4 1"
date: "2006-12-08T10:42:04"
picture: "Land_Rover_4x4_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7718
- /details4b4f.html
imported:
- "2019"
_4images_image_id: "7718"
_4images_cat_id: "730"
_4images_user_id: "328"
_4images_image_date: "2006-12-08T10:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7718 -->
Hier von mir mal wieder der Versuch eines kleinen Allrades unter Einhaltung folgender Randbedingungen:

- 3 Differenziale
- Antrieb und Lenkung motorisiert
- autarke Stromversorgung an Bord
- so kompakt wie möglich
- sehr robuste/stabile Bauart fürs Gelände
- möglichst gutaussehend nach Vorbild Land Rover Defender

Es entstand das vorliegende Modell, was ich optisch und technisch recht gelungen finde. Perfektionisten werden eine Federung vermissen, aber die war bei diesem Maßstab nicht mehr drin...

Ist er nicht knuffig geworden?! ;o)

Viel Spaß beim Angucken!