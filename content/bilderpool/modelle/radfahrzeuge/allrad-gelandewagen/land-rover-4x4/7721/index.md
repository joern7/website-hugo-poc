---
layout: "image"
title: "Land Rover 4x4 4"
date: "2006-12-08T10:42:04"
picture: "Land_Rover_4x4_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7721
- /details334b.html
imported:
- "2019"
_4images_image_id: "7721"
_4images_cat_id: "730"
_4images_user_id: "328"
_4images_image_date: "2006-12-08T10:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7721 -->
Hier die Unterseite mit den 3 Differenzialen. Die beiden gelben Streben werden straff auf Zug belastet und halten den Unterbau und die Rastachsen vom Mitteldifferenzial zusammen.