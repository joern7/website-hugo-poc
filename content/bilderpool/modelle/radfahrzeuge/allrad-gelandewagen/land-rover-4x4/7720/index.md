---
layout: "image"
title: "Land Rover 4x4 3"
date: "2006-12-08T10:42:04"
picture: "Land_Rover_4x4_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7720
- /details23e7.html
imported:
- "2019"
_4images_image_id: "7720"
_4images_cat_id: "730"
_4images_user_id: "328"
_4images_image_date: "2006-12-08T10:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7720 -->
Als Perfektionist wollte ich unbedingt die Stromversorgung mit im Fahrzeug unterbringen. Für den Akkupack war leider gar kein Platz, so dass es der Batteriekasten geworden ist.

Funktioniert genauso gut wie mit dem Akkupack, allerdings nicht sehr lange (eher seeehr kurz ;o)