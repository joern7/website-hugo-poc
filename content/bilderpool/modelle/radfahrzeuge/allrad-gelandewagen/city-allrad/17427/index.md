---
layout: "image"
title: "City-Allrad 12"
date: "2009-02-15T18:22:44"
picture: "City-Allrad_08.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17427
- /details7b74.html
imported:
- "2019"
_4images_image_id: "17427"
_4images_cat_id: "1567"
_4images_user_id: "328"
_4images_image_date: "2009-02-15T18:22:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17427 -->
Ansicht auf die Lenkung (Fronthaube abgebaut). Gut zu sehen ist meine Verlängerung des Servohebels und die Mimik zum realistsichen Drehen des Lenkrads beim Lenken mit dem Servo. Einfach, aber wirkungsvoll.