---
layout: "image"
title: "City-Allrad 11"
date: "2009-02-15T18:22:44"
picture: "City-Allrad_07.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17426
- /details3c42.html
imported:
- "2019"
_4images_image_id: "17426"
_4images_cat_id: "1567"
_4images_user_id: "328"
_4images_image_date: "2009-02-15T18:22:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17426 -->
Der "Schalthebel" nach vorn gedrückt. Er betätigt den Mini-Taster, und der Empfänger ist angeschaltet. Sehr realistsich, oder? ;o)