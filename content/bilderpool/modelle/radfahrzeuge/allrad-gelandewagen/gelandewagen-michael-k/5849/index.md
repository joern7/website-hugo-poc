---
layout: "image"
title: "Fahrwerk"
date: "2006-03-11T11:09:10"
picture: "P3100001.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/5849
- /detailsa254.html
imported:
- "2019"
_4images_image_id: "5849"
_4images_cat_id: "504"
_4images_user_id: "366"
_4images_image_date: "2006-03-11T11:09:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5849 -->
Hier sieht man das Fahrwek für einen Geländewagen mit Allrad- antrieb,-Lenkun und  -Federung(Das Fahrzeug ist zur zeit falsch-herum)Die rote Kettediehnt zur stabilisierung. Als Räder habe ich die großen Traktorreifen verwendet. Mehr zu sehen gibt´s auf den Detail Fotos.