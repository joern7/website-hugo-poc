---
layout: "image"
title: "Radantrieb"
date: "2006-03-11T11:09:10"
picture: "P3100004.jpg"
weight: "4"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/5852
- /details3ef0-2.html
imported:
- "2019"
_4images_image_id: "5852"
_4images_cat_id: "504"
_4images_user_id: "366"
_4images_image_date: "2006-03-11T11:09:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5852 -->
Hier sieht man die (lenkbare)kraftübertragung von der Achse zum Rad. Es sind 3 Kegelzahnräder, das mittlere ist einfach in den Gelenkstein gesteckt