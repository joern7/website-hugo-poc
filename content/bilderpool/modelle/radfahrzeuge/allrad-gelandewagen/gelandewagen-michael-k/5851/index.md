---
layout: "image"
title: "Antrieb"
date: "2006-03-11T11:09:10"
picture: "P3100003.jpg"
weight: "3"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/5851
- /detailsdc36-3.html
imported:
- "2019"
_4images_image_id: "5851"
_4images_cat_id: "504"
_4images_user_id: "366"
_4images_image_date: "2006-03-11T11:09:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5851 -->
Hier sieht man den Antrieb für die Hinterachse Die Vorderachse wird in Prinzip genauso angetrieben.
Man kann auch noch die blauen Federn zur Federung erkennen.