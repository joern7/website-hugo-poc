---
layout: "image"
title: "Mini-Geländewagen 7"
date: "2010-02-10T18:50:55"
picture: "Gelndewagen_10.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26314
- /detailsd759.html
imported:
- "2019"
_4images_image_id: "26314"
_4images_cat_id: "1871"
_4images_user_id: "328"
_4images_image_date: "2010-02-10T18:50:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26314 -->
Der Starthebel im Cockpit, der den Empfänger anschaltet.