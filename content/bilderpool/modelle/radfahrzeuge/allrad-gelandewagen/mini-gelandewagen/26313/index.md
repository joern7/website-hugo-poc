---
layout: "image"
title: "Mini-Geländewagen 6"
date: "2010-02-10T18:50:55"
picture: "Gelndewagen_07.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26313
- /detailsea41.html
imported:
- "2019"
_4images_image_id: "26313"
_4images_cat_id: "1871"
_4images_user_id: "328"
_4images_image_date: "2010-02-10T18:50:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26313 -->
Der Lenkwinkel ist ganz ordentlich und macht das Fahrzeug schön wendig.