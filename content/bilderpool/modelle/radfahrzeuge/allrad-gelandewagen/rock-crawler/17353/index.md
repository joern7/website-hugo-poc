---
layout: "image"
title: "Rock Crawler 13"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_13_klein.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17353
- /details0f6c.html
imported:
- "2019"
_4images_image_id: "17353"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17353 -->
Blick von unten auf die Vorderachse. Die Spurweite habe ich aus Gründen der Stabilität etwas breiter als üblich gewählt.