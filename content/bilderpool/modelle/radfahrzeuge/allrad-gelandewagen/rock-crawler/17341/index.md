---
layout: "image"
title: "Rock Crawler 1"
date: "2009-02-09T22:00:14"
picture: "Rock_Crawler_01_klein.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17341
- /details2500.html
imported:
- "2019"
_4images_image_id: "17341"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17341 -->
Hier habe ich mich an einem klassischen Rock Crawler versucht. Rock Crawler sind allradgetriebene Fahrzeuge mit dem quasi einzigen Ziel, extrem schwieriges Gelände und sehr steile Anstiege zu bewältigen.

Mein Modell wird von einem Power-Motor angetrieben, der sein Drehmoment durch eine Schnecke extrem verstärkt über 3 Differenziale klassisch an alle 4 Räder abgibt. Die mögliche Verschränkung der Achsen ist enorm, so dass keine Differenzialsperren - wie bei Rock Crawlern sonst üblich - notwendig sind. Es sind immer alle 4 Räder am Boden, da das Fahrzeug sehr schwer ist.

Das Modell fährt sehr langsam und ist unglaublich stark. Es bewältigt nahezu jedes Hindernis. Zwar schneckenlangsam und wie in Zeitlupe, aber erfolgreich!

Gelenkt wird über einen Mini-Motor und eine Zahnstange mit 2 Endschaltern. Der Akku-Pack ist mit an Bord. Gesteuert wird der Rock Crawler über das neue Control Set.

Die Bodenfreiheit ist eher schlecht, aber das ist egal. Es geht einfach IMMER voran!