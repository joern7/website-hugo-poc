---
layout: "image"
title: "Rock Crawler 20"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_20_klein.jpg"
weight: "20"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17360
- /detailsbae9-2.html
imported:
- "2019"
_4images_image_id: "17360"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17360 -->
Das "klassische" und meiners Erachtens nach beste Prinzip, hohe Drehmomente bei einer Achsschenkellenkung zu übertragen.