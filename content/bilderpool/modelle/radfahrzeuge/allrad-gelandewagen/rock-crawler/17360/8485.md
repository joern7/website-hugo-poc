---
layout: "comment"
hidden: true
title: "8485"
date: "2009-02-10T09:07:57"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Naja... ich hab's auch schon erlebt, dass sich das äußere Rast-Kegelzahnrad weggebogen und dann geknattert hat. Da hilft dann auch kein Verblocken und Versteifen.

Gruß,
Harald