---
layout: "comment"
hidden: true
title: "8483"
date: "2009-02-10T08:41:52"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich glaube der springende Punkt ist einfach, dass es immer Schwierigkeiten geben wird, so viel Drehmoment (mehr noch bei noch größeren Rädern) über 4-mm-Achsen auf die Räder zu bringen. Realitätsnähe und viel Drehmoment schließen sich da irgendwann aus, und die Lösung sind dann eben diese Varianten, wo ein Zahnrad direkt mit dem Rad gekoppelt ist und weit außen angetrieben wird. Deine Variante liegt wohl ziemlich nah am Optimum dessen, was realitätsnah mit ft gemacht werden kann. Und offenbar funktioniert's ja prima. :-)

Gruß,
Stefan