---
layout: "image"
title: "Rock Crawler 18"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_18_klein.jpg"
weight: "18"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17358
- /detailsb435.html
imported:
- "2019"
_4images_image_id: "17358"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17358 -->
Die Vorderachse von vorn.