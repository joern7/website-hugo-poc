---
layout: "image"
title: "Rock Crawler 11"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_11_klein.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17351
- /detailsb932.html
imported:
- "2019"
_4images_image_id: "17351"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17351 -->
Blick seitlich auf die Vorderachse. Die Vertikalstrebe, die das Fahrzeuggewicht trägt, ist links gut zu erkennen.