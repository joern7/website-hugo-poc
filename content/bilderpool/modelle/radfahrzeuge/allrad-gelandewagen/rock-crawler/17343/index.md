---
layout: "image"
title: "Rock Crawler 3"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_03_klein.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17343
- /details7ebf.html
imported:
- "2019"
_4images_image_id: "17343"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17343 -->
Der Lenkeinschlag der Achsschenkel an der Vorderachse ist recht ordentlich...

Die gelben horizontalen Statikstreben an den Achsen außen verwinden sich beim Verschränken der Achsen.