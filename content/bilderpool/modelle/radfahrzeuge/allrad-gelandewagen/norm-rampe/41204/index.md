---
layout: "image"
title: "Rampensau1783_48,65.jpg"
date: "2015-06-22T18:58:39"
picture: "Rampe1783_4865.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41204
- /details8b77.html
imported:
- "2019"
_4images_image_id: "41204"
_4images_cat_id: "2213"
_4images_user_id: "4"
_4images_image_date: "2015-06-22T18:58:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41204 -->
Kaulquappe, ick hör dir trapsen: die Feuerwehrleitern sind wie gemacht für die Raupenbänder 146985.
Hier fährt dasselbe Fahrzeug auf einer nicht-Standard-Rampe aus den ft-Feuerwehrleitern. Da komme ich auf arctan(12,5 / 11) = 48,65° Neigung :-)) -- aber außer Konkurrenz.