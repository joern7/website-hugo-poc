---
layout: "image"
title: "Norm-Rampe 1"
date: "2011-02-17T21:29:19"
picture: "Norm-Rampe_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30006
- /details5ced.html
imported:
- "2019"
_4images_image_id: "30006"
_4images_cat_id: "2213"
_4images_user_id: "328"
_4images_image_date: "2011-02-17T21:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30006 -->
Diese Norm-Rampe soll dazu dienen, FT-Geländefahrzeuge bezüglich ihrer Steigfähigkeit zu testen, zu optimieren und für andere FT-Bauer vergleichbar und nachvollziehbar zu machen.

Die Rampe lässt sich aus 3 Bauplatten 500 (32985) und einigen Winkelsteinen aufbauen, muss aber noch etwas verstärkt werden, um sich bei Belastung nicht durchzubiegen. Die Winkelsteine ermöglichen eine genaue Festlegung des Steigungswinkels; hier im Bild sind es 3x15°, also 45°, was 100% Steigung entspricht. Wessen Fahrzeug das schaftt, der ist wirklich gut!

Die normalen FT-Winkelsteine ermöglichen Abstufungen im 7,5°-Abstand (also 7,5°, 15°, 22,5°, 30°, 37,5° und 45°). Zur Vereinfachung schlage ich vor, 15°-Abstände zu wählen, also 15° (sollte jeder schaffen), 30° (wird schon recht schwer) und 45°, die absolute Königsklasse!

Wer mehr als 45° schafft, kann das natürlich auch gern zeigen ... ;o)