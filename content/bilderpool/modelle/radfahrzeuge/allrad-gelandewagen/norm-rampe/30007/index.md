---
layout: "image"
title: "Norm-Rampe 2"
date: "2011-02-17T21:29:19"
picture: "Norm-Rampe_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30007
- /detailsb84e.html
imported:
- "2019"
_4images_image_id: "30007"
_4images_cat_id: "2213"
_4images_user_id: "328"
_4images_image_date: "2011-02-17T21:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30007 -->
Hier sind 45° Steigungswinkel dargestellt.