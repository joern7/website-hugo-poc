---
layout: "image"
title: "Radaufhängung vorn"
date: "2017-06-18T11:55:32"
picture: "jeepfa7.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45949
- /details3bff.html
imported:
- "2019"
_4images_image_id: "45949"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45949 -->
Der halbierte BS7,5 macht diese Lenkung überhaupt erst möglich. 


Das hoch gesetzte Differenzial ergibt noch ganz andere Möglichkeiten, Insbesondere passt jetzt ein M-Motor auch *darunter*, wenn man den mittig liegenden Längstrager ersetzt durch zwei außen liegende (da kommen wir später im Jahr noch drauf zurück).