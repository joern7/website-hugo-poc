---
layout: "image"
title: "Radaufhängung hinten"
date: "2017-06-18T12:03:12"
picture: "Jeep_Frontantrieb9.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45951
- /detailscd1a.html
imported:
- "2019"
_4images_image_id: "45951"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T12:03:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45951 -->
Die ft-Lenkklaue ist hier hochkant gestellt und wird mit zwei Streben I-30 zu einem Trapez ergänzt. Ein Gummiband aus dem Nähkästchen dient als Feder.