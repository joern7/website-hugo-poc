---
layout: "image"
title: "Motorraum"
date: "2017-06-18T11:55:32"
picture: "jeepfa4.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45946
- /detailsf44d.html
imported:
- "2019"
_4images_image_id: "45946"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45946 -->
Unter der Haube ist alles voll -- aber nur oben. Unten drunter wäre noch Platz etwa für eine Seilwinde, wenn nur der Längsträger nicht mittendrin im Weg stünde.