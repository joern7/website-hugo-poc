---
layout: "image"
title: "Wasserantrieb"
date: "2007-02-22T12:13:47"
picture: "002Seite_2.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Paul"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- /php/details/9122
- /details91d9-3.html
imported:
- "2019"
_4images_image_id: "9122"
_4images_cat_id: "803"
_4images_user_id: "459"
_4images_image_date: "2007-02-22T12:13:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9122 -->
Der neu gebaute Wasserantrieb ist hier zu sehen.