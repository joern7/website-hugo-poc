---
layout: "image"
title: "Amphiebienfahrzeug Vorne"
date: "2007-02-03T16:32:11"
picture: "Vorne001.jpg"
weight: "1"
konstrukteure: 
- "Paul und Tobias"
fotografen:
- "Paul"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- /php/details/8815
- /details61da-2.html
imported:
- "2019"
_4images_image_id: "8815"
_4images_cat_id: "803"
_4images_user_id: "459"
_4images_image_date: "2007-02-03T16:32:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8815 -->
Das Amphiebienfahrzeug von Vorne, unten die Lenkung, Darüber zwei Platten 500.
Die LEnkung wird mit einem Grauen Motor Angetrieben. Weiter oben ist ein Tageslicht Sensor, der die noch nicht Verkabelten Lichter einschaltet.
Als Schwimmer werden zwei Viereckige Flaschen unter dem Fahrzeug und zwei Colaflaschen neben dem Fahrzeug.
Achtung! Veraltet

Im Wasser bewegt es sich mit zwei Schaufelrädern.