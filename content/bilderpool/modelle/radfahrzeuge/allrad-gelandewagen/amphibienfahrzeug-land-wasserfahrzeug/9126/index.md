---
layout: "image"
title: "Neuer Rückschwimmmer"
date: "2007-02-22T12:13:47"
picture: "005neuer_Schwimmer.jpg"
weight: "10"
konstrukteure: 
- "Paul"
fotografen:
- "Kunstrukteur"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- /php/details/9126
- /details5980.html
imported:
- "2019"
_4images_image_id: "9126"
_4images_cat_id: "803"
_4images_user_id: "459"
_4images_image_date: "2007-02-22T12:13:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9126 -->
Der neue Schwimmer an der Rückseite hält den Motor über Wasser