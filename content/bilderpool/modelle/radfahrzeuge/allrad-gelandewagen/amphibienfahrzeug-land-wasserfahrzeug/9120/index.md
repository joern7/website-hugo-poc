---
layout: "image"
title: "Vorderansicht, neue Lenkung"
date: "2007-02-22T12:13:47"
picture: "001Vorne.jpg"
weight: "4"
konstrukteure: 
- "Paul"
fotografen:
- "Kunstrukteur"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- /php/details/9120
- /details45a6-2.html
imported:
- "2019"
_4images_image_id: "9120"
_4images_cat_id: "803"
_4images_user_id: "459"
_4images_image_date: "2007-02-22T12:13:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9120 -->
Die Vorderseite mit neuem Wasserantrieb und neuer Lenkung.