---
layout: "image"
title: "Amphi Explorer"
date: "2007-10-15T19:57:56"
picture: "Amphi8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12232
- /details26a9.html
imported:
- "2019"
_4images_image_id: "12232"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-15T19:57:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12232 -->
Man sieht vorn den Schlüsselschalter.