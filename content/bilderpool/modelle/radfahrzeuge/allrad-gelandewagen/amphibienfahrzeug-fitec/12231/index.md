---
layout: "image"
title: "Amphi Explorer"
date: "2007-10-15T19:57:56"
picture: "Amphi7.jpg"
weight: "7"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12231
- /details2ea4.html
imported:
- "2019"
_4images_image_id: "12231"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-15T19:57:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12231 -->
Von vorne. Man sieht die lampen. Das Führerhaus dient gleichzeitig als Überrollbügel.