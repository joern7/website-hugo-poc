---
layout: "comment"
hidden: true
title: "4039"
date: "2007-09-20T17:22:41"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Da gibts nur eins: Leichtbau. 

Versuch, mehr Winkelträger und weniger Bausteine zu benutzen, probier aus, ob es nicht noch leichtere Räder gibt, ob alle Verstärkungen wirklich notwendig sind ...

Das Modell ist das coolste, das bisher jemand aus dem Boats-Kasten gebaut hat. Viel Erfolg!