---
layout: "image"
title: "Neuer Antrieb"
date: "2007-09-29T20:34:56"
picture: "Amphi5.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12056
- /details84de.html
imported:
- "2019"
_4images_image_id: "12056"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-09-29T20:34:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12056 -->
Jetzt habe ich die Maße geändert und eine Strebe angebracht und alles stabilisiert. Der Antrieb funktioniert jetzt.