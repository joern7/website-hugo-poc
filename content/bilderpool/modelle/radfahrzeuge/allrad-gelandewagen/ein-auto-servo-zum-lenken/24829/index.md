---
layout: "image"
title: "Mein Feuerwehrauto"
date: "2009-08-28T21:12:25"
picture: "jpg1.jpg"
weight: "5"
konstrukteure: 
- "michelino"
fotografen:
- "michelino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "michelino"
license: "unknown"
legacy_id:
- /php/details/24829
- /detailsc64f.html
imported:
- "2019"
_4images_image_id: "24829"
_4images_cat_id: "884"
_4images_user_id: "876"
_4images_image_date: "2009-08-28T21:12:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24829 -->
Mit Blaulicht, Frontscheinwerfer, Rückleuchten, Soundmodul,Martinshorn, Fernsteuerung