---
layout: "image"
title: "Ferngesteuertes Auto"
date: "2007-03-27T14:05:50"
picture: "auto1.jpg"
weight: "1"
konstrukteure: 
- "1958230dermitdemhut"
fotografen:
- "1958230dermitdemhut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9799
- /detailse6de.html
imported:
- "2019"
_4images_image_id: "9799"
_4images_cat_id: "884"
_4images_user_id: "453"
_4images_image_date: "2007-03-27T14:05:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9799 -->
Hier ist eine Gesamtansicht des Autos.