---
layout: "image"
title: "landrover09.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover09_2.jpg"
weight: "64"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45521
- /detailse68a-2.html
imported:
- "2019"
_4images_image_id: "45521"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45521 -->
Bei Photo 33:
1. Das Block mit der Antriebsachse ist verstärkt worden mit 2 Steine 15x30x3,75, befestigt mit Klemmstücke 15mm. So schiebt da Nichts mehr und Fahrt er viel besser.
2. 2 Statika-ringen sind zugefügt am Hintenachse um Spiel zu verringern.