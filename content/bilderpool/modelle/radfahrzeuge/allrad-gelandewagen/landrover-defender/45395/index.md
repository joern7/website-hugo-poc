---
layout: "image"
title: "landrover06.jpg"
date: "2017-03-03T21:38:24"
picture: "landrover06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45395
- /details4eaf.html
imported:
- "2019"
_4images_image_id: "45395"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45395 -->
1. Vorseite
Das Model lasst Sich in Vier Teilen bauwen, Ich arbeite hier von Vorn nach Hinten, also Dies ist den Anfang von die Vorseite mit Lenkung, Federung, Seilwinde,Lampen und Haube