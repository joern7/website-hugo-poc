---
layout: "image"
title: "landrover25.jpg"
date: "2017-03-03T21:38:24"
picture: "landrover25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45414
- /detailsde12.html
imported:
- "2019"
_4images_image_id: "45414"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45414 -->
2. Vorne Mitte
Motor, Sitzen der Fahrer und Beireiter und ein Stück Boden