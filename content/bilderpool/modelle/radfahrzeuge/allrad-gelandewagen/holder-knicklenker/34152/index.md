---
layout: "image"
title: "Holder Knicklenker 1"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_02.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34152
- /details5747.html
imported:
- "2019"
_4images_image_id: "34152"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34152 -->
Für den kleinen Allrad zwischendurch ein Holder Knicklenker! Natürlich nicht maßstabsgetreu, aber doch deutlich inspiriert von den kleinen Knicklenkern für kommunale Anwendungen der schwäbischen Firma Holder:

http://www.max-holder.com

Das Fahrzeug hat permanenten Allradantrieb durch einen S-Motor über der Hinterachse. Die Kraft wird über ein Kardangelenk unter dem Knickgelenk an die Vorderachse übertragen.

Gelenkt wird per Servo, das direkt über dem Knickgelenk sitzt. Die Steuerung (Fahren, Lenken) erfolgt per Control Set.

Empfänger und Blinklicht auf dem Dach lassen sich über seperate Schalter unabhängig voneinander einschalten. Die Leuchten an Front und Heck sind nur Show und ohne Funktion (ließe sich aber problemlos verschalten), denn schon jetzt leidet die Optik des Fahrzeugs unter der Vielzahl von Kabeln. Wie so oft bei kleinen Modellen machen mir die unveränderlichen festen Kabelpeitschen von Empfänger, Servo, Batteriekasten und Blinkelektronik zu schaffen ...

Die Ladefläche ist verkleidet und gut nutzbar.

Durch den permanenten Allrad sind die Geländeeigenschaften sehr gut, und auch die Steigfähigkeit begeistert (37,5° an der Rampe). Die neuen Junior-Reifen haben auch auf glatten Flächen ordentlich Grip! Die neuen Felgen klemmen prima auf Kunststoffachsen.

Hier ein kleines Video, das die Steigfähigkeit von 37,5° an der Norm-Rampe zeigt. Recht beachtlich für so ein kleines Fahrzeug:

http://www.youtube.com/watch?v=1qI3PM0ZyzM

Und ganz nebenbei dürfte dies das erste Modell mit den neuen Junior-Reifen in der ftc sein ... ;o)