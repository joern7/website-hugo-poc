---
layout: "image"
title: "Holder Knicklenker 08"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_08.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34159
- /details373c.html
imported:
- "2019"
_4images_image_id: "34159"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34159 -->
Das Knickgelenk im Detail.

Das normale FT-Gelenk (15 mm lang) mit je 1 Baustein 7,5 davor und dahinter war mir für diese kompakte Anwendung viel zu instabil und verschob sich unter Last ständig. Daher habe ich mir diese neue Konstruktion aus 4 Kupplungsstücken 38253, 3 Scheiben 105195 und einem Klemmstift D4,1 107356 ausgedacht. Hält und funktioniert perfekt! Vor allem die Abstützung um eine gedachte achsparallele Achse durch die Mitte des Gelenks ist hervorragend. Zusätzlich stabilisiert das fest eingespannte Servo das Gelenk.

Servo-Drehachse, Knickgelenk und Kardangelenk liegen exakt übereinander in einer Achse.

Oben im Bild der Schalter für den Empfänger.