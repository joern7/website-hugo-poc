---
layout: "image"
title: "Holder Knicklenker 11"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_11.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34162
- /detailsaa1b.html
imported:
- "2019"
_4images_image_id: "34162"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34162 -->
Einblick in den Vorderwagen (teilzerlegt).