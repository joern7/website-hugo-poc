---
layout: "image"
title: "Holder Knicklenker 13"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_13.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34164
- /detailsed33-2.html
imported:
- "2019"
_4images_image_id: "34164"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34164 -->
Nochmals ein besserer Einblick ins Knickgelenk (teilzerlegt).