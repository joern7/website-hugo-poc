---
layout: "comment"
hidden: true
title: "2527"
date: "2007-02-27T20:48:12"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Also hier auf diesem Bild ist die Lenkung komplett abgebaut. Aber nebenan, in Bild Nr. 2, ist der Lenkhebel zu sehen, der von einem Z20 angetrieben wird. Dieses wird von einem Z10 angetrieben, und das Z10 steckt in einem schwarzen Etwas drinnen. Nach allem was ich sagen kann, muss das ein Stufengetriebe sein. Der Motor ist nicht mehr auf dem Bild, aber er wird wohl nach rechts weg (d.h. im Foto: ins Bild hinein, vom Betrachter weg) angebracht sein.

Gruß,
Harald (aka Sherlock Holmes)