---
layout: "image"
title: "Mini-Allrad 4"
date: "2007-03-02T08:54:40"
picture: "Mini-Allrad_05.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/9203
- /details1a98.html
imported:
- "2019"
_4images_image_id: "9203"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2007-03-02T08:54:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9203 -->
Auf Wunsch hier zusätzlich noch einige Ansichten, wo man den Lenkmotor gut sieht.