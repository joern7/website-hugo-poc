---
layout: "image"
title: "Mini-Allrad 1"
date: "2006-06-01T21:29:46"
picture: "Mini-Allrad_02.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/6339
- /detailsb859-3.html
imported:
- "2019"
_4images_image_id: "6339"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2006-06-01T21:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6339 -->
Hier der Versuch, einen gelenkten Allrad mit 2 Motoren, 3 Differenzialen und Akku-Pack so klein wie möglich aufzubauen. Die Proportionen sind nicht so optimal geworden, allerdings funktioniert alles prächtig. Sogar das Lenkrad dreht sich, und in die Mitte konnte eine neuartige variable/teilsperrende Differenzialsperre eingebaut werden!