---
layout: "image"
title: "Explorer34.jpg"
date: "2009-02-05T20:12:02"
picture: "Explorer34.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17310
- /detailsf42b.html
imported:
- "2019"
_4images_image_id: "17310"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17310 -->
