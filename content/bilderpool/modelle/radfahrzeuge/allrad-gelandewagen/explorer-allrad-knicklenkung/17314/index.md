---
layout: "image"
title: "Explorer41.jpg"
date: "2009-02-05T20:20:36"
picture: "Explorer41.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17314
- /detailsed7b-2.html
imported:
- "2019"
_4images_image_id: "17314"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:20:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17314 -->
Die Innereien der Vorderachse. Die Querstreben sind X-63, und damit passen die Rastachsen, auch wenn das Rastdifferenzial drin ist. In der Hinterachse ist die Abstand-Halterei etwas anders gelöst, mit je 2x BS5 zwischen inneren und äußeren BS30-Loch.