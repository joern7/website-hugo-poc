---
layout: "image"
title: "Explorer38.jpg"
date: "2009-02-05T20:16:45"
picture: "Explorer38.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17312
- /details0954.html
imported:
- "2019"
_4images_image_id: "17312"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:16:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17312 -->
Die Vorderachskonstruktion.

Mal sehen, ob da noch eine schaltbare Differenzialsperre hinein zu bringen ist...