---
layout: "image"
title: "Explorer14.JPG"
date: "2009-02-04T15:55:41"
picture: "Explorer14.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Knicklenkung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17304
- /details7710.html
imported:
- "2019"
_4images_image_id: "17304"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T15:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17304 -->
Der sieht nicht nur so aus, der IST auch für's Grobe geeignet. Die nächste Rally Paris-Dakar kann kommen.