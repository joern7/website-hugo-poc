---
layout: "image"
title: "Explorer35.jpg"
date: "2009-02-05T20:12:21"
picture: "Explorer35.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17311
- /details8475-3.html
imported:
- "2019"
_4images_image_id: "17311"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:12:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17311 -->
