---
layout: "image"
title: "Hummer-08.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-08.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4418
- /details7028-3.html
imported:
- "2019"
_4images_image_id: "4418"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4418 -->
Die Drehpunkte der Lenkung sind ziemlich weit weg von den Aufstandspunkten der Räder. Beim Lenken fahren diese also einen recht großen Bogen und verlangen viel Platz für die Radkästen.