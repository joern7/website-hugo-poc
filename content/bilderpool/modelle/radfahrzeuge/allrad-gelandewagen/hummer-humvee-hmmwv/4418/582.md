---
layout: "comment"
hidden: true
title: "582"
date: "2005-06-13T17:50:53"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
RäderJa, die sind von Conrad. Ich hab aber leider die Nummer nicht (dafür habe ich ja den Laden direkt in der Innenstadt erreichbar).
Die Reifen passen auf die Felge 45, als ob sie gerade dafür gemacht wären. Sie rutschen aber auf der Felge unter Last etwas früher als die Reifen 65. Im vorliegenden Fall ist mir das gerade recht so, denn wenn man gegen ein festes Hindernis fährt, muss ja irgendwo im Antriebsstrang jemand nachgeben.