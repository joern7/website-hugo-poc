---
layout: "image"
title: "Hummer-13.JPG"
date: "2005-06-12T14:29:14"
picture: "Hummer-13.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4423
- /details318f.html
imported:
- "2019"
_4images_image_id: "4423"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:29:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4423 -->
Die Schwinge für die Antriebswelle der Vorderachse. Das Differenzial-Antriebsrad Z14 (31414) musste auch etwas bearbeitet werden: außer dem Kegelkranz ist nichts mehr dran geblieben.

Die BS15 zur Lagerung der Räder müssen gegen Abwandern nach außen gesichert werden, dazu dienen hier die 2x2 Drähte (links+rechts oben+unten), die in senkrechten Bohrungen stecken und dann herumgebogen sind.