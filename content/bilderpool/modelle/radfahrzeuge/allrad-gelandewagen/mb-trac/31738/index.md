---
layout: "image"
title: "MB-Trac"
date: "2011-08-31T21:22:46"
picture: "MB-Trac_007.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31738
- /details7180.html
imported:
- "2019"
_4images_image_id: "31738"
_4images_cat_id: "2365"
_4images_user_id: "22"
_4images_image_date: "2011-08-31T21:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31738 -->
MB-Trac 
Eine Antrieb mit ein Z10 und ein Innenzahnrad gegen die Reifen funtioniert auch immer gut.