---
layout: "image"
title: "Golden Eagle Details 9"
date: "2015-06-16T21:06:00"
picture: "goldeneagle18.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41201
- /detailsf1c2-2.html
imported:
- "2019"
_4images_image_id: "41201"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41201 -->
Und so landet man schließlich beim Chassis.