---
layout: "image"
title: "Golden Eagle Gelände 2"
date: "2015-06-16T21:06:00"
picture: "goldeneagle05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41188
- /details8842.html
imported:
- "2019"
_4images_image_id: "41188"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41188 -->
Ein kleines Video dazu kommt noch.