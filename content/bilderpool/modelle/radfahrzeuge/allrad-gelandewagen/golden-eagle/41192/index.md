---
layout: "image"
title: "Golden Eagle Ansichten 4"
date: "2015-06-16T21:06:00"
picture: "goldeneagle09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41192
- /details776e-2.html
imported:
- "2019"
_4images_image_id: "41192"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41192 -->
...und von unten.