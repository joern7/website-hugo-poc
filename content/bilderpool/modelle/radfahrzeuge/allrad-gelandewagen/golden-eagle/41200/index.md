---
layout: "image"
title: "Golden Eagle Details 8"
date: "2015-06-16T21:06:00"
picture: "goldeneagle17.jpg"
weight: "17"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41200
- /details50e9.html
imported:
- "2019"
_4images_image_id: "41200"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41200 -->
Ballast, Kotflügel und Kühlergrill