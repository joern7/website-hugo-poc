---
layout: "image"
title: "Geländewagen 1"
date: "2007-03-23T19:26:04"
picture: "gelaendewagen01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9654
- /details2f84.html
imported:
- "2019"
_4images_image_id: "9654"
_4images_cat_id: "878"
_4images_user_id: "502"
_4images_image_date: "2007-03-23T19:26:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9654 -->
Ein Aufbau fehlt noch damits nach was aussieht aber dann wirds schwerer und schwerfälliger und das will ich nicht. Das Ziel war es ihn so kompakt und der Drehpunkt der Lenkung  so nah dran wie möglich zu bauen.