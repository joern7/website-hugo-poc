---
layout: "image"
title: "Geländewagen 5"
date: "2007-03-23T19:26:05"
picture: "gelaendewagen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9658
- /details8d8a.html
imported:
- "2019"
_4images_image_id: "9658"
_4images_cat_id: "878"
_4images_user_id: "502"
_4images_image_date: "2007-03-23T19:26:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9658 -->
Der Drehpunkt so nah am Rad wie möglich.