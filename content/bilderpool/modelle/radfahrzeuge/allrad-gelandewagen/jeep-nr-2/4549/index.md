---
layout: "image"
title: "Jeep2-03.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4549
- /detailsc704.html
imported:
- "2019"
_4images_image_id: "4549"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4549 -->
Ein Gepäcknetz (da waren noch kurz zuvor Zwiebeln drin) sorgt für sicheren Halt der Ladung.