---
layout: "image"
title: "Jeep2-01.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Jeep", "Geländewagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4547
- /detailsb075.html
imported:
- "2019"
_4images_image_id: "4547"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4547 -->
Jeep No.1 gibt es hier: http://www.ftcommunity.de/categories.php?cat_id=33

Gegenüber dem ersten Modell ist die Rücksitzbank einfacher geworden, eine motorbetriebene Seilwinde ist hinzugekommen und der Akku hat einen Platz unter der Ladefläche bekommen. Dazu wurde das Fahrzeug etwas verlängert.