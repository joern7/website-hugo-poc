---
layout: "image"
title: "Moster-Truck Chassis 5"
date: "2007-09-19T19:23:50"
picture: "mostertruckchassis5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11874
- /detailscfc8.html
imported:
- "2019"
_4images_image_id: "11874"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-19T19:23:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11874 -->
Da die blauen Federn zu schwach sind hab ich jeweils noch einen Gummi eingebaut.