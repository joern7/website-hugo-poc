---
layout: "image"
title: "Moster-Truck Chassis 15"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis07.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11892
- /detailsadf9-2.html
imported:
- "2019"
_4images_image_id: "11892"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11892 -->
