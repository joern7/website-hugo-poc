---
layout: "image"
title: "Monster-Truck Allrad 13"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/17152
- /details3e65.html
imported:
- "2019"
_4images_image_id: "17152"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17152 -->
Hier habe ich mal eine kleine Teststrecke aufgebaut, die er locker schafft.