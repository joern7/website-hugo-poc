---
layout: "image"
title: "Moster-Truck Chassis 10"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis02.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11887
- /details164e.html
imported:
- "2019"
_4images_image_id: "11887"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11887 -->
So hier eine Teststrecke.