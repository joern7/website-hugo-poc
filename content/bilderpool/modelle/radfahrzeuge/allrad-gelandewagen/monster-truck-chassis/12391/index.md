---
layout: "image"
title: "Moster-Truck Chassis 34"
date: "2007-11-03T22:08:57"
picture: "mostertruckchassis8_3.jpg"
weight: "34"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12391
- /detailsc0ca-3.html
imported:
- "2019"
_4images_image_id: "12391"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-11-03T22:08:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12391 -->
