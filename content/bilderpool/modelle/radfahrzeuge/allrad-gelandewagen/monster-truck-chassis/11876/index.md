---
layout: "image"
title: "Moster-Truck Chassis 7"
date: "2007-09-19T19:23:50"
picture: "mostertruckchassis7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11876
- /details8e2c.html
imported:
- "2019"
_4images_image_id: "11876"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-19T19:23:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11876 -->
