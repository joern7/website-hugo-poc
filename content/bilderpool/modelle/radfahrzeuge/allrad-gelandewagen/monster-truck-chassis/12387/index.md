---
layout: "image"
title: "Moster-Truck Chassis 30"
date: "2007-11-03T22:08:57"
picture: "mostertruckchassis4_3.jpg"
weight: "30"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12387
- /details2677-2.html
imported:
- "2019"
_4images_image_id: "12387"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-11-03T22:08:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12387 -->
