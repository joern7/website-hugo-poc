---
layout: "image"
title: "Das Ausfahren beginnt (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran51.jpg"
weight: "51"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33082
- /detailsd7eb.html
imported:
- "2019"
_4images_image_id: "33082"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33082 -->
Derselbe Zustand bei der Armspitze.