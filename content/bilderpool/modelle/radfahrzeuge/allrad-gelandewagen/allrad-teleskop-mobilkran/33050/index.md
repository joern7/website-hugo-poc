---
layout: "image"
title: "Hinterachsen"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33050
- /detailsbec4.html
imported:
- "2019"
_4images_image_id: "33050"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33050 -->
Das vorne (hier: links) sichtbare Z30 wird angetrieben, und dadurch die beiden Hinterachsdifferentiale. Deren Z10 gehen auf Z30, auf deren Achsen wiederum die Hinterräder sitzen. Von den Achsen in Bildmitte sitzt die hintere absichtlich nicht ganz in der Rastkupplung, der leichteren Drehbarkeit wegen.