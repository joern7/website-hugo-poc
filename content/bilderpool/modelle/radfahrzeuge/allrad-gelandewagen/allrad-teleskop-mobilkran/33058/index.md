---
layout: "image"
title: "Das Fahrzeugheck"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33058
- /details9335.html
imported:
- "2019"
_4images_image_id: "33058"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33058 -->
Hier gibt's viel zu sehen, von oben nach unten:

- Der M-Motor für den Antrieb der ausfahrbaren Kranarmsegmente
- Der S-Motor für den Antrieb der Seilwinde mit dem Überlastschutz darüber
- In Bildmitte rechts den Taster für die Endlage des komplett aufgestellten Kranarms
- Die Verbindung zwischen Plattform (schwarze Bauplatten mit Alus an den Seiten) und dem ganz normalen ft-Drehkranz darunter mittels 2 Reihen à 2 BS30, die mit Verbindern die Plattform stabil halten
- Etwas verdeckt der Powermotor zum Drehen der Plattform
- Das hintere Stützenpaar incl. S-Motor zum Antrieb sowie oberen (wieder über eine Strebe als Hebel) und unteren (direkt von der Stütze betätigten) Endschalter