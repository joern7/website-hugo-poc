---
layout: "image"
title: "Fahrantrieb"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33041
- /details4687-2.html
imported:
- "2019"
_4images_image_id: "33041"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33041 -->
Mittig im Fahrzeug sitzt quer eingebaut ein 1:20-Power-Motor (mit grauer Kappe). Er treibt ein Rast-Differential an, welches nach hinten (hier: rechts) in Richtung Hinterachsen und nach links über die Kette links im Bild zu den Vorderachsen führt. Der Antrieb der Vorderachsen kommt also von der Oberseite des Fahrzeugs.