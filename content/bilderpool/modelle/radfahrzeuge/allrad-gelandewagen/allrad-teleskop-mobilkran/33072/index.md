---
layout: "image"
title: "Aus- und Einfahrantrieb"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33072
- /details213f.html
imported:
- "2019"
_4images_image_id: "33072"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33072 -->
Dieser M-Motor genügt tatsächlich, den Arm auszufahren. Das funktioniert sogar unabhängig davon, ob der Arm gerade ganz flach liegt (hier ist die innere Reibung durch die Hebelwirkung eklig groß) oder fast senkrecht steht (hier muss der Motor das Gewicht der Segmente stemmen).

Über zwei Schnecken geht's auf zwei kräftig gelagerte Metallachsen, auf denen - wie bei den Stützen - direkt auf der Achse Seilzüge enden. Die Schnüre sind hier einfach mit einigen Klemmbuchsen fixiert.

Die unteren Seilzüge (in diesem Bild die beiden entfernt liegenden) dienen dem Ausfahren (Hinausziehen) der Segmente, die oberen (hier die beim Betrachter liegenden) dem Einfahren (Herunterziehen).

Der Taster auf der Unterseite (den man hier oben im Bild sieht) schaltet den Antrieb ab, sobald der Arm komplett eingefahren ist.