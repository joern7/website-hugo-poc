---
layout: "image"
title: "Hakenseilantrieb"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran61.jpg"
weight: "61"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33092
- /detailsb427-2.html
imported:
- "2019"
_4images_image_id: "33092"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33092 -->
Ein S-Motor ist fürs Hakenseil zuständig und wickelt das Seil per Direktantrieb auf die Seiltrommel.