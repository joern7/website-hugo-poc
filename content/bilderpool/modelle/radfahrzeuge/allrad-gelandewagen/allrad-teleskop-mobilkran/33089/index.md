---
layout: "image"
title: "Belastbarkeitstest (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran58.jpg"
weight: "58"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33089
- /details6343-2.html
imported:
- "2019"
_4images_image_id: "33089"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33089 -->
Auch in diesem Zustand ist die Maschine in der Lage, den Arm sauber aus- und ein zu fahren. Der Arm ist offenbar auch leicht genug, dass das Fahrzeug hier noch nicht kippt.

Ich staune allerdings, was der ft-Drehkranz unterhalb der Drehplattform aushält, ohne auseinander zu springen. Eher kippte das Fahrzeug, als das das passierte. Man sieht aber sehr schön, wie das Material hier "arbeitet".