---
layout: "image"
title: "Radaufhängung"
date: "2011-10-29T17:10:20"
picture: "bildervomabbaudesteleskopmobilkrans8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33361
- /detailsd8d8.html
imported:
- "2019"
_4images_image_id: "33361"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:10:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33361 -->
Das Kegelzahnrad sitzt nicht mittig über dem Z30, weil seine Achse sonst mit der des Reifens kollidierte. Deshalb geht der Antrieb auch in eine Richtung etwas besser als in die andere - aber beide Richtungen gehen hinreichend gut.