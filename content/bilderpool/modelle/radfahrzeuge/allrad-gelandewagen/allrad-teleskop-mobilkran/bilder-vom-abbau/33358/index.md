---
layout: "image"
title: "Antrieb und Lenkung"
date: "2011-10-29T17:04:58"
picture: "bildervomabbaudesteleskopmobilkrans5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33358
- /details0ddf.html
imported:
- "2019"
_4images_image_id: "33358"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:04:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33358 -->
Ein Blick auf Antrieb und Lenkung, auf dem man sieht, wie einfach der S-Motor die Lenkung betätigt.