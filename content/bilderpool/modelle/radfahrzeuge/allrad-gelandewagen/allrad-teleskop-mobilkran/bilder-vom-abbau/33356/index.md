---
layout: "image"
title: "Fahrantrieb"
date: "2011-10-29T17:04:57"
picture: "bildervomabbaudesteleskopmobilkrans3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33356
- /details270c.html
imported:
- "2019"
_4images_image_id: "33356"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:04:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33356 -->
Das Differential treibt letztlich über die unten waagerecht liegende Kette die Vorderräder an.