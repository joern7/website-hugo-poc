---
layout: "image"
title: "Radaufhängung"
date: "2011-10-29T17:10:20"
picture: "bildervomabbaudesteleskopmobilkrans7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33360
- /details4dc4-2.html
imported:
- "2019"
_4images_image_id: "33360"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:10:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33360 -->
Das Rad saß auf der hier nach rechts zeigenden Achse.