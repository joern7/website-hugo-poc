---
layout: "comment"
hidden: true
title: "15591"
date: "2011-10-30T19:47:47"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Rollenlager kann man ja eine Spur "zu tief" reinschieben, dann ist der BS7,5 fixiert. Das Verdrehen des BS7,5 war aber eh kein Problem, dann die Lenkung hatte durch die Statikträger-Löcher genug Spiel, dass sich notfalls die ganze Anordnung etwas eindrehen konnte (was natürlich auch nicht erwünscht ist). Wie gesagt, funktioniert das Fahrwerk ohne aufgesetzten Arm ganz hervorragend und sehr kräftig (es konnte völlig problemlos mit allen Rädern über meine auf den Tisch gelegte Hand fahren). Mit dem Arm obendrauf allerdings kam es gut nur noch über einzelne Finger.

Gruß,
Stefan