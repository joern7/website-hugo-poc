---
layout: "image"
title: "Hebeeffekt der Stützen (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33062
- /details1144.html
imported:
- "2019"
_4images_image_id: "33062"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33062 -->
Hier bis zum automatischen Abschalten der Stützen hochgedrückt - das Fahrzeug wird einige Millimeter angehoben, und die Stützen tragen einen Gutteil der Last.

Ohne Kranarm drauf können die vier Stützen das Fahrwerk so hoch heben, dass alle vier Hinterräder frei in der Luft hängen. Mit dem Kranarm drauf reicht's dafür aber nicht mehr.