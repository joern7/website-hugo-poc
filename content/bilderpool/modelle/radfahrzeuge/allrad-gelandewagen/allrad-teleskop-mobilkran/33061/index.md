---
layout: "image"
title: "Hebeeffekt der Stützen (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33061
- /details6252.html
imported:
- "2019"
_4images_image_id: "33061"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33061 -->
Trotz des recht hohen Gesamtgewichtes des Fahrzeugs tun die Stützen ihren Dienst. Hier ein Bild bei gerad noch nicht ganz abgesenkten Stützen. Man beachte den (geringen) Abstand zwischen Reifen und oberer gelber Zierleiste (eine sonstige Funktion hat die nämlich nicht).