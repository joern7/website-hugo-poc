---
layout: "image"
title: "Verseilung zum Einfahren (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran47.jpg"
weight: "47"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33078
- /details28e2.html
imported:
- "2019"
_4images_image_id: "33078"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33078 -->
Die Verseilung für die richtigen Ausfahrgeschwindigkeiten der einzelnen Segmente ist dieselbe wie auf der Armunterseite. Unter der oberen gelben Strebe sieht man gerade noch die rote Seilrolle zum Umlenken des roten Kranseils.

Das nach ganz oben laufende Kranseil läuft auf dieser Aufnahme versehentlich zu weit rechts; es läuft normalerweise einfach über das innerste Segment nach oben. Das ist wohl beim Heraustragen des Modells in den Sonnenschein zum Fotografieren oder während der Aufnahmen der Unterseite des Fahrwerks passiert (da lag das Fahrzeug auf der Seite).