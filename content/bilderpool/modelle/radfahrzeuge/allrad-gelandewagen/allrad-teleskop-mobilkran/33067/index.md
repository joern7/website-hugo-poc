---
layout: "image"
title: "Aufstell-Antrieb (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33067
- /detailsf65f-2.html
imported:
- "2019"
_4images_image_id: "33067"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33067 -->
Der PowerMotor mit der schwarzen Kappe läuft ja sehr schnell. Auch hier wird also hohe Drehzahl erst möglichst spät in hohe Kraft gewandelt, damit die Mechanik das überhaupt schafft und aushält. Mechaniken, wo sich um eine Welle (hier die des Z30) gleichzeitig noch etwas anderes dreht (hier das gerade noch sichtbare Z20 beim Z30, welches je nach Aufstellwinkel um das Z30 kreist), mag ich besonders gerne.