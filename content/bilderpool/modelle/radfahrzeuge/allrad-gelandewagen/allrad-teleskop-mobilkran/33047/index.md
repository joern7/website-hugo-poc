---
layout: "image"
title: "Federung der Vorderachse (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33047
- /detailsec66.html
imported:
- "2019"
_4images_image_id: "33047"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33047 -->
Die Kugelschreiberfedern haben eher nur einen optischen Effekt. Die ganze Last wird von den Federfüßen oben getragen (altes Elektromechanik-Programm von ft). Die senkrecht stehende Metallachse ist die zum Antrieb, aber auch gleichzeitig die, um die das Rad beim Lenken dreht, und die beim Einfedern, nur an waagerechter Verschiebung gehindert, nach oben drückt.

Die Achskonstruktion ist einer MacPherson-Achse sehr ähnlich (http://de.wikipedia.org/wiki/MacPherson-Federbein).