---
layout: "image"
title: "Das Ausfahren beginnt (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran50.jpg"
weight: "50"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33081
- /detailsf9e6.html
imported:
- "2019"
_4images_image_id: "33081"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33081 -->
Derselbe Zustand von der Oberseite gesehen. Das Zugseil zum wieder Einfahren ist hierbei, wie man sieht, sehr locker. Bei den schon nennenswerten Kräften im Arm sieht man daran die Dehnbarkeit des Zwirns.