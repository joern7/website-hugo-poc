---
layout: "image"
title: "Seitenansicht"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33033
- /details9d81.html
imported:
- "2019"
_4images_image_id: "33033"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33033 -->
Autokräne, die mal eben 100 t in 60 m Höhe heben können, haben mich schon immer schwer beeindruckt. Seit Jahren hab ich schon immer mal erfolglose Anläufe unternommen, so etwas nachzubauen.