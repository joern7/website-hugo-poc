---
layout: "image"
title: "Detail zum Flaschenzug"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran60.jpg"
weight: "60"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33091
- /details1666.html
imported:
- "2019"
_4images_image_id: "33091"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33091 -->
Die beiden Seilrollen an der Kranspitze sind absichtlich schräg gestellt. Das bewirkt bei dem doppelten Flaschenzug, dass der Haken selbst schön gerade hängt.