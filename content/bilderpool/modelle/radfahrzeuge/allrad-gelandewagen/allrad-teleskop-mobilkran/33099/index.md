---
layout: "image"
title: "Alles zusammen in der Transporthilfe"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran68.jpg"
weight: "68"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33099
- /detailsad8d.html
imported:
- "2019"
_4images_image_id: "33099"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33099 -->
Mit einer ganzen Reihe von Federnocken kann auch das Bedienpult fest an der Transporthilfe fixiert werden und stabilisiert es sogar noch.