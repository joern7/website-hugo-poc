---
layout: "image"
title: "Das Ausfahren beginnt (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran49.jpg"
weight: "49"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33080
- /details40ba.html
imported:
- "2019"
_4images_image_id: "33080"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33080 -->
Die Segmente ziehen und schieben sich, ausgehend vom zweitinnersten, gegenseitig schön gleichmäßig raus.