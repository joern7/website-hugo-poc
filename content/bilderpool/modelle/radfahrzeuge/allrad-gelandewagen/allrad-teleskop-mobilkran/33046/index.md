---
layout: "image"
title: "Seiteneinblick in die Vorderachse"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33046
- /detailsa7a9.html
imported:
- "2019"
_4images_image_id: "33046"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33046 -->
Wir blicken hier von knapp hinter der linken Fahrertür in die Vorderachskonstruktion hinein. Hier ist die Vorderachse gerade entlastet (der Kranarm war fürs Foto nämlich aufgestellt). Der "Normalzustand" ist bei waagerecht liegenden Streben erreicht.