---
layout: "image"
title: "Obere Endlagenabschaltung des vorderen Stützenpaares"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33057
- /details06b5.html
imported:
- "2019"
_4images_image_id: "33057"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33057 -->
Das Hochziehen der Stützen geschieht ja nur durch den Zug der ft-Spiralfeder. Das ist nicht kräftig genug, einen Taster direkt zu betätigen. Deshalb geht das hier über einen langen Hebel aus Statikstreben, der um ein Gelenk eines Statikadapters auf der Unterseite dieser vielen BS7,5 sitzt. Der andere Hebelarm ist kurz und betätigt dann den kurz vor dem Reifen noch sichtbaren Taster, der den vorderen Sützenmotor abschaltet, sobald die Stützen komplett eingefahren sind.