---
layout: "image"
title: "Kraftfluss zu den Vorderrädern"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33042
- /details96dc-2.html
imported:
- "2019"
_4images_image_id: "33042"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33042 -->
Wir blicken hier durch den Kranarm auf die Rückseite des Führerhauses. Die von rechts unten kommende Kette wird vom vorderen Abgang des Differentials beim Fahrmotor angetrieben und führt oben zum Differential zwischen den beiden Vorderrädern. Hier läuft noch alles mit relativ hoher Drehzahl, also recht wenig zu übertragendem Drehmoment. Deshalb halten das die Rast-Winkelzahnräder links und rechts des oberen Differentials aus.

Die Winkelzahnräder führen auf Rast-Z10, die per Kettenantrieb auf die waagerecht über den Vorderrädern liegenden Z30 gehen. Da die Z30 mit den Rädern ein- und ausfedern, dient diese Kette auch als Höhenausgleich. Die Z30 wiederum treiben die älteren, größeren Winkelzahnräder, die auf die Z30 an den Rädern gehen. Das ergibt eine angenehme Untersetzung, weil erst am Rad selbst ein kräftiges Drehmoment aufgebaut wird.

Die beiden E-Tecs sorgen für das Blinken der Lampen.