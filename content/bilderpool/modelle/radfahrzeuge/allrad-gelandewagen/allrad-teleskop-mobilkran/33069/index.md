---
layout: "image"
title: "Aufstellen des Kranarms"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33069
- /detailsfebf.html
imported:
- "2019"
_4images_image_id: "33069"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33069 -->
Dies ist stabil genug, die notwendigen Kräfte auf den Kranarm zu leiten. Die Metallachse ist eine 30 mm lange und steckt nur so weit im U-Träger, dass die inneren U-Träger sicher vorbeigleiten können.