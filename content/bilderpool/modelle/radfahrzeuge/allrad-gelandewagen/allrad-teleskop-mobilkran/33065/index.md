---
layout: "image"
title: "Drehplattformantrieb"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33065
- /details00db.html
imported:
- "2019"
_4images_image_id: "33065"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33065 -->
Das Z15 muss, je nachdem, wie weit der Arm ausgefahren und gesenkt ist, ganz schön Arbeit leisten. Mittlerweile ist es ganz schön malträtiert, insbesondere nach dem Dauerbetrieb den ganzen Convention-Tag über.