---
layout: "image"
title: "Knick-4x4-06.JPG"
date: "2006-03-26T12:33:16"
picture: "Knick-4x4-06.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5916
- /detailsf11a.html
imported:
- "2019"
_4images_image_id: "5916"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:33:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5916 -->
