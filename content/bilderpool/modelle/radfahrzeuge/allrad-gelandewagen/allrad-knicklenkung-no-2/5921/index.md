---
layout: "image"
title: "Knick-4x4-15.JPG"
date: "2006-03-26T12:36:26"
picture: "Knick-4x4-15.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5921
- /details64c6.html
imported:
- "2019"
_4images_image_id: "5921"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:36:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5921 -->
