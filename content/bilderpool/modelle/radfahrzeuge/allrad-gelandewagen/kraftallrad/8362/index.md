---
layout: "image"
title: "Seite-Lenkung"
date: "2007-01-11T18:56:54"
picture: "Kraftallrad2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8362
- /details6f06.html
imported:
- "2019"
_4images_image_id: "8362"
_4images_cat_id: "771"
_4images_user_id: "456"
_4images_image_date: "2007-01-11T18:56:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8362 -->
Seitenansicht. Hier sieht man gut die Lenkung. Sie funktioniert über ei  Hubgetriebe.