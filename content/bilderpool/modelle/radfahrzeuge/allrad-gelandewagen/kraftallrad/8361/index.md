---
layout: "image"
title: "Kraftallrad1"
date: "2007-01-11T18:56:54"
picture: "Kraftallrad1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8361
- /details4ddb.html
imported:
- "2019"
_4images_image_id: "8361"
_4images_cat_id: "771"
_4images_user_id: "456"
_4images_image_date: "2007-01-11T18:56:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8361 -->
Das ist mein Kraftallrad. Es ist ein ferngesteuertes Allradfahrzeug was viel Kraft hat.