---
layout: "image"
title: "Seite-Antrieb"
date: "2007-01-11T18:56:54"
picture: "Kraftallrad3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8363
- /detailsac5c-2.html
imported:
- "2019"
_4images_image_id: "8363"
_4images_cat_id: "771"
_4images_user_id: "456"
_4images_image_date: "2007-01-11T18:56:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8363 -->
Seitenansicht. Hier hab ich mal den Akku abgenommen damit man den Antrieb sehen kann. Man sieht sowohl das Mitteldifferenzial, als auch die Schnecke die das hintere Diff antreibt. Das vordere Diff wird genauso angetrieben wie das hintere.