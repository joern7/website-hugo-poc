---
layout: "image"
title: "Classic-Unimog 11"
date: "2011-01-15T20:46:47"
picture: "Classic-Unimog_14.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29698
- /details9fba-2.html
imported:
- "2019"
_4images_image_id: "29698"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29698 -->
In der Mitte vom Rahmen sitzt der neue Batteriekasten mit Schalter. Ein Segen für FT-Kleinmodelle, die nur von einem 9V-Block versorgt werden!!!