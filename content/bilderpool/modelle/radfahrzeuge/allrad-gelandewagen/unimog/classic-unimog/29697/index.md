---
layout: "image"
title: "Classic-Unimog 10"
date: "2011-01-15T20:46:47"
picture: "Classic-Unimog_13.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29697
- /detailsa355.html
imported:
- "2019"
_4images_image_id: "29697"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29697 -->
Die Vorderachse bei abgebauter Frontverkleidung und Fahrerhaus. Alles ganz einfach und schön im Raster!

Die Achsschenkel drehen sich jeweils um einen Achse mit Vierkant, rechts eine kurze rote, links eine längere schwarze, damit das Lenkrad noch drauf passt und sich mitdrehen kann. Der Vierkant steckt jeweils unten im Baustein 15.