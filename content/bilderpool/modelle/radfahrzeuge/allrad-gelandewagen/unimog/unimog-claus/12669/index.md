---
layout: "image"
title: "modellevonclauswludwig09.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig09.jpg"
weight: "24"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12669
- /detailse4d7.html
imported:
- "2019"
_4images_image_id: "12669"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12669 -->
