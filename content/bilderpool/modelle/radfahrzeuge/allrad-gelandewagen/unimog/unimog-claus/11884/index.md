---
layout: "image"
title: "cu048.JPG"
date: "2007-09-21T20:15:12"
picture: "cu048.JPG"
weight: "8"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11884
- /detailsa188.html
imported:
- "2019"
_4images_image_id: "11884"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11884 -->
