---
layout: "image"
title: "modellevonclauswludwig07.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig07.jpg"
weight: "22"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12667
- /details34e6-2.html
imported:
- "2019"
_4images_image_id: "12667"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12667 -->
