---
layout: "image"
title: "cu052.JPG"
date: "2007-09-21T20:23:20"
picture: "cu052.JPG"
weight: "11"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11897
- /details6d8c.html
imported:
- "2019"
_4images_image_id: "11897"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:23:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11897 -->
Auf der Rückseite gibt es ebenfalls zwei Steckdosen und einen Druckluftanschluss. Dazu kommen noch drei (!) Zapfwellen.