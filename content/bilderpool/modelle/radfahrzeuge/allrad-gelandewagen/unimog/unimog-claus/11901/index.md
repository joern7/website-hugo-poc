---
layout: "image"
title: "cu065.JPG"
date: "2007-09-21T20:37:39"
picture: "cu065.JPG"
weight: "15"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11901
- /detailsd40c.html
imported:
- "2019"
_4images_image_id: "11901"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:37:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11901 -->
Der Hubmotor für Frontanbaugeräte ist ausgeklappt, die Motorhaube ist geöffnet.