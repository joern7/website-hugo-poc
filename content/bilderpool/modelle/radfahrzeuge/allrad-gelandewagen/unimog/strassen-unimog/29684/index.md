---
layout: "image"
title: "Bodenfreiheit"
date: "2011-01-14T13:47:47"
picture: "strassenunimog6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- /php/details/29684
- /detailsef56.html
imported:
- "2019"
_4images_image_id: "29684"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29684 -->
Wie man sieht, hat der Unimog nur geringe Bodenfreiheit. Am Bild verstärkt sich der Eindruck, weil die Räder etwas ins Sofa "einsinken".