---
layout: "image"
title: "Lenkung"
date: "2011-01-14T13:47:47"
picture: "strassenunimog3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- /php/details/29681
- /details052e.html
imported:
- "2019"
_4images_image_id: "29681"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29681 -->
Hier ist die Übertragung auf die Lenkung etwas besser zu sehen.