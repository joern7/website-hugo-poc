---
layout: "image"
title: "Mini-Unimog 2"
date: "2007-04-28T15:22:26"
picture: "miniunimog2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10175
- /detailsc319.html
imported:
- "2019"
_4images_image_id: "10175"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-04-28T15:22:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10175 -->
