---
layout: "image"
title: "Mini-Unimog 12"
date: "2007-05-07T18:38:28"
picture: "miniunimog3_2.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10352
- /detailsb655.html
imported:
- "2019"
_4images_image_id: "10352"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10352 -->
