---
layout: "image"
title: "MB-trac"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44178
- /details9c41.html
imported:
- "2019"
_4images_image_id: "44178"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44178 -->
