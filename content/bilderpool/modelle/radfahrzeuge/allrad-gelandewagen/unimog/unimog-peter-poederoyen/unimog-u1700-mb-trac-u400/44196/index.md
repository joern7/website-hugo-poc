---
layout: "image"
title: "U400   Poederoyen NL"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu22.jpg"
weight: "22"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44196
- /details656a.html
imported:
- "2019"
_4images_image_id: "44196"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44196 -->
Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit.



Link zum Unimog-Museum : 

http://www.unimog-museum.com/ 

Unimog-Museum Betriebs GmbH 
An der B 462 
Ausfahrt Schloss Rotenfels 
76571 Gaggenau