---
layout: "image"
title: "Unimog  -U400,  -MB-trac  +  U1300  + U1700L     Poederoyen NL"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44175
- /detailsaf5a-2.html
imported:
- "2019"
_4images_image_id: "44175"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44175 -->
Link zum Unimog-Museum : 

http://www.unimog-museum.com/ 

Unimog-Museum Betriebs GmbH 
An der B 462 
Ausfahrt Schloss Rotenfels 
76571 Gaggenau