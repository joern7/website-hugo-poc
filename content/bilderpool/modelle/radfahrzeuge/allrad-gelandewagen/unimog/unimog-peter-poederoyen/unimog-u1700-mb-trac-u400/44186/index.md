---
layout: "image"
title: "U400"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44186
- /details2712-2.html
imported:
- "2019"
_4images_image_id: "44186"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44186 -->
Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit.