---
layout: "image"
title: "U400"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu15.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44189
- /details1d31.html
imported:
- "2019"
_4images_image_id: "44189"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44189 -->
Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit.