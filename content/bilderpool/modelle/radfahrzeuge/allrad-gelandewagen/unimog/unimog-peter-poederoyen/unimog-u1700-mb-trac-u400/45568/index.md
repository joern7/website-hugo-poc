---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T14:19:17"
picture: "unimogfuerrangierarbeiteneisenbahndraisine04.jpg"
weight: "26"
konstrukteure: 
- "Peter Peoderoyen"
fotografen:
- "Peter Peoderoyen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45568
- /details93c4.html
imported:
- "2019"
_4images_image_id: "45568"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T14:19:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45568 -->
Het ontwerp dateert van kort na de Tweede Wereldoorlog. De Unimog wordt tegenwoordig geleverd onder de merknaam Mercedes-Benz. De naam komt van het Duitse UNIversal MOtor Gerät ("universele motor machine"). De Unimog was aanvankelijk bedoeld als langzaam rijdend landbouwvoertuig waarmee men op het land kon werken, en werktuigen kon aandrijven en transporteren. Ook kon de Unimog als auto worden gebruikt. Hij heeft een zeer hoge bodemvrijheid en een flexibel frame, en kan in zeer oneffen terrein rijden. Daarom wordt hij ook door verschillende legers gebruikt. Albert Friedrich ontwierp het eerst model en tekende een overeenkomst met Erhard & Söhne in Schwäbisch Gmünd. De ontwikkeling begon op 1 januari 1946 en het eerste prototype was eind 1946 gereed. De spoorbreedte was zo groot als twee rijen gepote aardappelen. Vanaf 1947 werd de productie gestart, aanvankelijk met een dieselmotor van 25 pk. Het logo op deze eerste wagens werd gevormd door twee ossehoorns die een "U" vormden. Deze eerste serie werd door Boehringer gebouwd en wel om twee redenen. Allereerst hadden Erhard & Söhne niet de capaciteit om Unimogs te bouwen. Bovendien mocht Daimler-Benz destijds nog geen vierwielaangedreven voertuigen bouwen. In 1951 nam Mercedes-Benz de productie over.