---
layout: "comment"
hidden: true
title: "18345"
date: "2013-10-01T21:03:45"
uploadBy:
- "StefanL"
license: "unknown"
imported:
- "2019"
---
Danke, ich habe die Bilder mit unserer neuen Kamera gemacht. Die RAW-Dateien habe ich am PC entwickelt. Trotz (wie immer) schlechten Lichtverhältnissen sind die Bilder ganz gut geworden, im Gegensatz zu den Jpeg´s der Kamera. Wieso akzeptiert der Publisher eigentlich mein Passwort nicht mehr?
Gruß Stefan