---
layout: "image"
title: "Lenkung-1"
date: "2014-08-24T10:52:09"
picture: "unimog12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39274
- /detailsd8c3-2.html
imported:
- "2019"
_4images_image_id: "39274"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39274 -->
Das graue Teil, das man hier erkennen kann verhindert das Wegbiegen des Zahnrades zum Differential. So können ordentliche Kräfte problemlos übertragen werden.