---
layout: "image"
title: "Lenkung-4"
date: "2014-08-24T10:52:09"
picture: "unimog15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39277
- /detailsee9b.html
imported:
- "2019"
_4images_image_id: "39277"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39277 -->
Hier der zweite Lenkzylinder.