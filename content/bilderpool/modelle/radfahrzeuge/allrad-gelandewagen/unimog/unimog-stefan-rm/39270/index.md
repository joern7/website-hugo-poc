---
layout: "image"
title: "Heck"
date: "2014-08-24T10:52:09"
picture: "unimog08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39270
- /detailsb7a7.html
imported:
- "2019"
_4images_image_id: "39270"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39270 -->
Hier ist die Portalachse gut  zu sehen.