---
layout: "image"
title: "Unimog ohne Aufbau"
date: "2014-08-10T15:00:28"
picture: "IMG_0760.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: ["Unimog"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39229
- /details3b74.html
imported:
- "2019"
_4images_image_id: "39229"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39229 -->
Hier sieht man den Unimog ohne Aufbau.