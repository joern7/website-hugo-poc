---
layout: "image"
title: "Unimog mit Kompaktran (2)"
date: "2014-08-07T12:53:04"
picture: "u2.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39160
- /details94ed.html
imported:
- "2019"
_4images_image_id: "39160"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39160 -->
Hier sieht man den ausgeklappten Kompaktkran. Dieser lässt sich in etwa auf eine Höhe von 35-40 cm ausfahren. Die Seilwinde ist aus Platzgründen die einzige Funktion, die nicht von einem Motor betätigt wird.