---
layout: "image"
title: "Ansicht vorne / oben"
date: "2014-08-07T12:53:04"
picture: "u4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39162
- /detailsa550.html
imported:
- "2019"
_4images_image_id: "39162"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39162 -->
Hier noch einmal Unimog mit Scheinwerfern, Stützen und eingeklappten Kran