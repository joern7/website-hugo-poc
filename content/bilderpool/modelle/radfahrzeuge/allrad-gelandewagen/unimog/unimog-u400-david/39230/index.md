---
layout: "image"
title: "Kompaktkran"
date: "2014-08-10T15:00:28"
picture: "Kompaktkran_-_Kopie.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: ["Kompaktkran", "ft-Designer"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39230
- /details66a9-2.html
imported:
- "2019"
_4images_image_id: "39230"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39230 -->
Hier noch einmal ein Bild vom Kompaktkran, das mit dem ft-Designer konstruiert wurde.