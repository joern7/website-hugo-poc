---
layout: "image"
title: "Unimog von hinten"
date: "2010-10-16T21:50:51"
picture: "DSCF3623.jpg"
weight: "4"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29016
- /details182d.html
imported:
- "2019"
_4images_image_id: "29016"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29016 -->
Hinten ist eine Ladefläche. Diese hat aber einen Nachteil: Wenn ich Rückwerts einen Berg hochfahren will, drehen die Hinterräder durch. Da hilft nichts anderes als Gewicht drauf zu machen. Der Akku sitz übrigens unter dem Empfänger. Das sieht man auf dem letzten Bild. Ich habe ihn versucht möglichst tief unterzubringen wegen der Kippgefahr. Das hat allerdings den Nachteil, dass man ihn nicht mehr so gut herausbekommt. Man muss zuerst den Empfänger ab machen.