---
layout: "image"
title: "Unimog Gelände"
date: "2010-10-16T21:50:52"
picture: "DSCF3629.jpg"
weight: "8"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29020
- /detailsd548.html
imported:
- "2019"
_4images_image_id: "29020"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29020 -->
Das ist nur eine kleine Beanspruchung für den Unimog. Hoch und runter schafft er locker.