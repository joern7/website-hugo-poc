---
layout: "image"
title: "Fuhrpark"
date: "2010-12-22T15:27:27"
picture: "DSCF4575.jpg"
weight: "20"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29505
- /detailsfc90-3.html
imported:
- "2019"
_4images_image_id: "29505"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29505 -->
Hier sind beide Modell noch einmal zusehen. Den alten Unimog habe ich übrigens auch mit Federn ausgestattet.