---
layout: "image"
title: "Scheinwerfer"
date: "2010-12-22T15:27:26"
picture: "DSCF4564.jpg"
weight: "14"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29499
- /details489c-2.html
imported:
- "2019"
_4images_image_id: "29499"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29499 -->
Wie auch bei dem letzen Unimog habe ich LEDs verbaut. Diese dient als Arbeitsscheinwerfer.