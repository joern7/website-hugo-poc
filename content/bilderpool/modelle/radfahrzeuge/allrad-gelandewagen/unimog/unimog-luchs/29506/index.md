---
layout: "image"
title: "Anhänger"
date: "2010-12-22T15:27:27"
picture: "DSCF4576.jpg"
weight: "21"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29506
- /details49cf.html
imported:
- "2019"
_4images_image_id: "29506"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29506 -->
Der Anhänger hat eine Pendel-Tandemachse und ist somit auch in jedem Gelände zu gebrauchen. Er lässt sich mit einem weiteren Empfänger kippen.