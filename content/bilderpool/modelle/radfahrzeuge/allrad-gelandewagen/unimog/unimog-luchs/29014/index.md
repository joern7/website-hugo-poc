---
layout: "image"
title: "Unimog von Vorne"
date: "2010-10-16T21:50:51"
picture: "DSCF3620.jpg"
weight: "2"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29014
- /details39c8.html
imported:
- "2019"
_4images_image_id: "29014"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29014 -->
Hier sieht man gut die Seilwinde. Ich weiß, der Motor da vorne dran sieht nicht so schön aus, aber ich habe keine andere Lösung gefunden. Den Mercedesstern und Kühler habe ich auf Tesa mit einem Edding gemalt. Die LED Lampen, die ich in den Leuchtbaustein gelötet habe, habe ich am Dach befestigt, da es an der Seite keinen Platz mehr gab.