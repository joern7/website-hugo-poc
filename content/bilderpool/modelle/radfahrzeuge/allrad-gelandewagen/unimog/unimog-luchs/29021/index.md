---
layout: "image"
title: "Unimog mit Licht"
date: "2010-10-16T21:50:52"
picture: "DSCF3631.jpg"
weight: "9"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29021
- /details9f6b.html
imported:
- "2019"
_4images_image_id: "29021"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29021 -->
Hier sieht man das moderne und helle Licht der LED. Verbaut habe ich welche mit 20° Abstrahlwinkel und 18 000mcd. Hinten und an den Blinklichtern habe ich ein bisschen schwächere verbaut.