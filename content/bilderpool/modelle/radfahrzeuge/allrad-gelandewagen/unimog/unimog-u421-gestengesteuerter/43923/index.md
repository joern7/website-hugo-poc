---
layout: "image"
title: "Motortreiber: Monster Moto Shield mit VNH2SP30"
date: "2016-07-24T08:35:58"
picture: "Motortreiber.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Unimog", "U421", "Funkfernsteuerung", "NRF24", "Servolenkung", "Bewegungssteuerung", "Gestensteuerung", "Arduino", "Nano", "Servo-Shield", "Monster-Moto-Shield", "VNH2SP30", "MPU6050", "Kippwinkel"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43923
- /detailsf01b-2.html
imported:
- "2019"
_4images_image_id: "43923"
_4images_cat_id: "3254"
_4images_user_id: "579"
_4images_image_date: "2016-07-24T08:35:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43923 -->
Hier mein später Beitrag zum Thema Unimog. Er hat ein paar bekannte Klassiker: Allradantrieb, Federung, Servolenkung.

Und er hat ein paar Neuigkeiten:
- Funkfernsteuerung mit Gestenkontrolle: die Fahrgeschwindigkeit und die Lenkung werden über den Kipp- bzw Drehwinkel um zwei Achsen gesteuert: Kippen nach vorne = Vorwärtsfahrt, Kippen nach hinten = Rückwärtsfahrt, Kippen nach links = Lenkeinschlag nach links, Kippen nach rechts = Lenkeinschlag nach rechts. Alle diese Bewegungen werden mit feiner Auflösung von 8 bit übertragen, dadurch ist das Fahrzeug sehr fein steuerbar.
- Datenübertragung über NRF24 (Sender und Empfänger) jeweils an Arduino Nano Board
- Erfassung des Kipp- bzw. Drehwinkels über MPU6050 am Arduino Nano (Sender)
- Eigenbau-Servoshield am Arduino-Nano Empfänger
- Monster-Motoshield mit VNH2SP30 Motortreiber als Fahrtregler
- Zweiton-Hupe, die über Taster an der Fernbedienung betätigt wird
- Beleuchtung, Blinker, Bremslichter