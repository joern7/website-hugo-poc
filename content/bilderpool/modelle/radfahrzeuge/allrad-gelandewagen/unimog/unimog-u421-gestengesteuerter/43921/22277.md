---
layout: "comment"
hidden: true
title: "22277"
date: "2016-07-25T12:34:43"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Wenn ein Vorderrad abhebt, klingt das aber danach, wie wenn das andere gar nicht oder fast nicht zum Antrieb beiträgt, sondern halt nur so schnell mitdreht, wie das Fahrzeug fährt, und die komplette Antriebsleistung über die Hinterachse erbracht wird - oder?

Gruß,
Stefan