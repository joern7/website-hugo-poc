---
layout: "image"
title: "Führerhaus"
date: "2014-04-27T16:09:00"
picture: "unimog03.jpg"
weight: "3"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38670
- /detailsc284-2.html
imported:
- "2019"
_4images_image_id: "38670"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38670 -->
Das Führerhaus mit den Schaltern/Ventilen für den Kran und die Seilwinde. Wernbung auf der Motorhaube darf auch nicht fehlen...