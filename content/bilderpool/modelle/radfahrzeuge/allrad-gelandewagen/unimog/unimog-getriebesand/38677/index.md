---
layout: "image"
title: "Das Zweiganggetriebe"
date: "2014-04-27T16:09:00"
picture: "unimog10.jpg"
weight: "10"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38677
- /details6b10-3.html
imported:
- "2019"
_4images_image_id: "38677"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38677 -->
... ist eigentlich überflüssig, da der Unimog 2,8kg wiegt und im zweiten (schnelleren) nicht fahren kann.
Hier wird übrigens der Motor verschoben.