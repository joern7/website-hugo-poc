---
layout: "image"
title: "Unimog 4"
date: "2006-12-29T22:04:52"
picture: "unimogfastfertig4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8191
- /details2f30-2.html
imported:
- "2019"
_4images_image_id: "8191"
_4images_cat_id: "756"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T22:04:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8191 -->
