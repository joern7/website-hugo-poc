---
layout: "image"
title: "Unimog 1"
date: "2007-01-02T14:58:38"
picture: "unimog01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8261
- /details95ca-2.html
imported:
- "2019"
_4images_image_id: "8261"
_4images_cat_id: "763"
_4images_user_id: "502"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8261 -->
Jetzt ist er fertig. Hat einen Antrieb für den Kipper bekommen. Ein paar Daten: Gewicht: 3 kilo; länge: 40cm, höhe: 28cm; breite: 25cm.