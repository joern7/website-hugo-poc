---
layout: "image"
title: "Unimog 4"
date: "2006-12-29T20:31:02"
picture: "jetztistseinunimog4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8187
- /details2209-4.html
imported:
- "2019"
_4images_image_id: "8187"
_4images_cat_id: "755"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T20:31:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8187 -->
Der Antrieb besteht jetzt aus 2 Power Motoren 50:1