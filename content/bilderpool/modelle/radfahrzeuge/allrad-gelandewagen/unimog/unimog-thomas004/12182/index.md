---
layout: "image"
title: "Unimog 9"
date: "2007-10-10T19:44:37"
picture: "Unimog_20.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12182
- /detailsd29e.html
imported:
- "2019"
_4images_image_id: "12182"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12182 -->
Von oben sieht man gut den Antrieb der Lenksäule und die Lagerung der Zahnstange. Die beiden roten Achsstummel rechts und links sorgen für den Endanschlag der Zahnstange.