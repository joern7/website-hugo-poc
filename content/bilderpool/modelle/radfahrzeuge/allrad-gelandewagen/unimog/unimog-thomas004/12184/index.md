---
layout: "image"
title: "Unimog 11"
date: "2007-10-10T19:44:56"
picture: "Unimog_19.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12184
- /details51f0.html
imported:
- "2019"
_4images_image_id: "12184"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12184 -->
Das Getriebe in der Mitte untersetzt die Drehzahl des Standard-Power-Motors auf ein ziviles Maß (zweimal Z10 auf Z20).