---
layout: "image"
title: "Unimog 7"
date: "2007-10-10T19:44:37"
picture: "Unimog_15.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12180
- /details35c5-2.html
imported:
- "2019"
_4images_image_id: "12180"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12180 -->
Die Vorderachse von unten. Der Harald'sche Aufbau mit den Kegelradsätzen ist meiner Meinung nach der stabilst mögliche Aufbau für eine kompakte angetriebene Achse mit Achsschenkel-Lenkung. Besser gehts nicht, hält aber auch nicht immer. Gerade die Winkelsteine im Rad verschieben sich gerne unter Last, und dann rattert es unangenehm...

Auch zwischen Mittel-Differenzial und Vorderachse ist bewusst kein Rast-Kardangelenk eingebaut.

Die Vorderachse wird durch zwei Statik-Streben geführt. Den Power-Motor hält eine Kette in Lage.