---
layout: "image"
title: "Unimog V2 14"
date: "2012-09-03T10:24:20"
picture: "Unimog_16_2.jpg"
weight: "31"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35436
- /details716b.html
imported:
- "2019"
_4images_image_id: "35436"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35436 -->
Die Statiksteine als Lagerungen verringern die Reibung im System deutlich (links feht leider eine der Aufhängungen der Ladefläche).