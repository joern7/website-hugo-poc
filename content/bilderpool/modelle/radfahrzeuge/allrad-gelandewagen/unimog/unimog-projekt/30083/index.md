---
layout: "image"
title: "Unimog 17"
date: "2011-02-18T23:20:35"
picture: "Unimog_17.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30083
- /details8257.html
imported:
- "2019"
_4images_image_id: "30083"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30083 -->
Blick auf die Differenzialsperre von oben.

Zwischen dem oberen Z10 (kommt von den Motoren) und dem Differenzial unten sitzen 2 Z15 auf der über den XS-Motor und das Hubgetriebe seitlich verschiebbaren Welle.

Hier im Bild ist gut zu sehen, dass ich dort, wo es möglich ist, die Bausteine 30 des Rahmens durch Statikträger ersetzt habe, um Gewicht zu sparen.