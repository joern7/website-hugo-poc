---
layout: "image"
title: "Unimog 10"
date: "2011-02-18T23:20:35"
picture: "Unimog_09.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30076
- /detailsa2f0.html
imported:
- "2019"
_4images_image_id: "30076"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30076 -->
Blick auf das Führerhaus.

Die beiden Schalter sind für den Empfänger und das seperat schaltbare Licht.

Links ist zu sehen, dass das Fahrzeug in der Mitte stark zusammenklappt. Thema Gewicht mal wieder ... :o/

Das Lenkrad ist starr und nur Show.