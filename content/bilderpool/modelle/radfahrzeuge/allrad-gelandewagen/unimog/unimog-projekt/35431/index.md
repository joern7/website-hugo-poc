---
layout: "image"
title: "Unimog V2 09"
date: "2012-09-03T10:24:20"
picture: "Unimog_11_2.jpg"
weight: "26"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35431
- /details9875.html
imported:
- "2019"
_4images_image_id: "35431"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35431 -->
Die Heckklappe geöffnet.