---
layout: "image"
title: "Unimog V2 21"
date: "2012-09-03T10:24:21"
picture: "Unimog_23.jpg"
weight: "38"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35443
- /details8fd5-2.html
imported:
- "2019"
_4images_image_id: "35443"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35443 -->
Der Verzicht auf Kardangelenke vor und hinter dem Mittel-Differenzial hat keine Nachteile.