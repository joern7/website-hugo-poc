---
layout: "image"
title: "Unimog 08"
date: "2011-02-18T23:20:35"
picture: "Unimog_08.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30074
- /details3072-2.html
imported:
- "2019"
_4images_image_id: "30074"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30074 -->
Blick auf das Heck bei abgenommener Ladefläche.

Die Ladefläche wird über 4 Strebenadapter gehalten, die in die Löcher auf der Unterseite der Platte eingerastet werden! Das ist ursprünglich sicher nicht so gedacht, aber es rastet tatsächlich brauchbar fest ein (ich kann das Fahrzeug hinten an der Platte hochheben), und die Löcher sind auch im 5-mm-Raster.

Man kann auch andere Achsen mit Rast-Ende nehmen, um die Bauplatte 500 zu befestigen. Aber die Strebenadapter funktionieren jedenfalls perfekt.

Ein Riesenvorteil der Sache ist, dass man eine schöne große glatte Ladefläche hat, die man vollständig bebauen kann, man gleichzeitig aber die Ladefläche für Wartungsarbeiten oder Änderungen am Fahrzeug jederzeit problemlos nach oben abziehen kann!