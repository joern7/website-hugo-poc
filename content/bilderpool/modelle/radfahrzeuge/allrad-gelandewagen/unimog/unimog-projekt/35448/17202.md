---
layout: "comment"
hidden: true
title: "17202"
date: "2012-09-08T11:25:59"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ich hab das 32455 natürlich nicht nur verbaut, weil's schön aussieht oder ich Dich necken will, sondern weil es hier in beide Richtungen einen wunderbar kompakten Anschlag für den Starthebel bietet. Ein- und Ausstellung sind somit genau definiert - ich mag sowas. ;o)

Gruß, Thomas