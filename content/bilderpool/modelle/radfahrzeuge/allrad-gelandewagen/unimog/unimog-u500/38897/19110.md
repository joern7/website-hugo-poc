---
layout: "comment"
hidden: true
title: "19110"
date: "2014-06-04T12:31:27"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das sieht mir aus, wie wenn das Rad eben nach oben und unten kippt anstatt nur vertikal geführt wird, oder?
Gruß,
Stefan