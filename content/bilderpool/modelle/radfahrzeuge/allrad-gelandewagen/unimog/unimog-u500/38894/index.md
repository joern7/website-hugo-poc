---
layout: "image"
title: "Unimog U500"
date: "2014-06-01T15:26:33"
picture: "Foto.jpg"
weight: "8"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38894
- /details55bd.html
imported:
- "2019"
_4images_image_id: "38894"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-06-01T15:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38894 -->
Angetrieben wird der Unimog von 2 Encoder-Motoren.
Bei den alten ft Differentialen  wurden die Achsen gekürzt. Hierbei treibt ein Motor ein Differentialen an.