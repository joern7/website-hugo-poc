---
layout: "image"
title: "Die Achswelle"
date: "2015-04-03T10:00:06"
picture: "Foto_20.jpg"
weight: "21"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40699
- /detailsf139.html
imported:
- "2019"
_4images_image_id: "40699"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40699 -->
Die schwenkbare Achswelle benötigt nur ein Kardangelenk.