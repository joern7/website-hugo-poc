---
layout: "image"
title: "Die Antriebsmechanik"
date: "2015-04-03T10:00:06"
picture: "Foto_19.jpg"
weight: "20"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40698
- /details5c1b.html
imported:
- "2019"
_4images_image_id: "40698"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40698 -->
Wird mit zwei ft Kardangelenken gearbeitet, muss der Abstand zwischen dem Rad und dem Differentialgehäuse mindestens 60mm betragen. Bei der Verwendung von Kegelzahnrädern, statt des ft -Differentials, reicht ein Zwischenraum von nur 30mm.