---
layout: "comment"
hidden: true
title: "20419"
date: "2015-04-06T00:52:54"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Das sieht ja schnuckelig aus. Bei der Lenkgeometrie solltest Du vielleicht noch etwas nachjustieren - die Verlängerungen der beiden Lenkhebel sollten sich bei gerader Ausrichtung in der Mitte der Hinterachse treffen ("Ackermann-Bedingung"), sonst bekommst Du Schlupf in der Kurve.
Gruß, Dirk