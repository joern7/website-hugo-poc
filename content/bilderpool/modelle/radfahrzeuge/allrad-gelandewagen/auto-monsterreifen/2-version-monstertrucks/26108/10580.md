---
layout: "comment"
hidden: true
title: "10580"
date: "2010-01-22T13:02:07"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Jo, aber wenn man gerade nur zwei unterschiedliche Motoren hat, bzw. keinen Geschwindigkeitsregler, dann ist's ne clevere Lösung.

Vergiss nicht, wir haben es hier ja gerade nicht mit richtigem Maschinenbau zu tun ... deshalb sind ganz andere Lösungen möglich :-)