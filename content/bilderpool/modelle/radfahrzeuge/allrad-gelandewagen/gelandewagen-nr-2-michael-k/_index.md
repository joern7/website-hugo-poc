---
layout: "overview"
title: "Geländewagen Nr. 2 (Michael K.)"
date: 2020-02-22T07:54:36+01:00
legacy_id:
- /php/categories/757
- /categoriesdefe.html
- /categories5abe.html
- /categories8fe8.html
- /categoriesfe6f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=757 --> 
Dies ist ein Fahrzeug mit Allradantrieb und Allradlenkung. Es ist komplett gefedert.