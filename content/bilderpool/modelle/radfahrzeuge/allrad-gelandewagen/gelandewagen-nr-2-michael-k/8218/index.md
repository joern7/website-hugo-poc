---
layout: "image"
title: "antrieb"
date: "2006-12-30T09:56:36"
picture: "gelaendewagen11.jpg"
weight: "11"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8218
- /details77fb.html
imported:
- "2019"
_4images_image_id: "8218"
_4images_cat_id: "757"
_4images_user_id: "366"
_4images_image_date: "2006-12-30T09:56:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8218 -->
der antrieb des autos ist im entgültigen modell etws anders; die lenkung ist mit einem 8:1PM und der antrieb über einen einzelnen sehr starken nicht-ft motor.