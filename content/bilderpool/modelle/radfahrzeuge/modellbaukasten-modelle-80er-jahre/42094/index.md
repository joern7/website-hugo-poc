---
layout: "image"
title: "Hebebühne 30456 (1982)"
date: "2015-10-18T15:42:43"
picture: "IMG_1085_copy_copy_copy.jpg"
weight: "5"
konstrukteure: 
- "Jens bzw FT"
fotografen:
- "Jens"
keywords: ["Modellbaukasten", "Hebebühne", "30456", "(1982)", "und", "Geländebuggy", "30463", "(1982)"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42094
- /details04a9.html
imported:
- "2019"
_4images_image_id: "42094"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42094 -->
Modellbaukasten Hebebühne 30456 (1982)

mit aufgestelltem Geländebuggy 30463 (1982)