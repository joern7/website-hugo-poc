---
layout: "image"
title: "Federung kurze Version"
date: "2015-10-18T18:20:08"
picture: "IMG_1004.jpg"
weight: "6"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Supermodell", "Teleskop-Mobilkran", "30474", "(1983)", "Feder", "kurz"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42096
- /details5a47.html
imported:
- "2019"
_4images_image_id: "42096"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T18:20:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42096 -->
Teleskop-Mobilkran Detail 30474 (1983)

auf dem Bild erkennt man die Kunststofffeder 38630 (1983). diese war etwas kürzer als die normale Feder - damit wurde erreicht, das die 2. Hinterradachse etwas in der Luft "hing" - im unbelasteten Zustand und somit das Modell auch besser lenkbar war.