---
layout: "image"
title: "Universalfahrzeug (30481, 1984)  von hinten mit Hebebühne"
date: "2015-10-18T15:42:43"
picture: "IMG_1081_copy.jpg"
weight: "3"
konstrukteure: 
- "Jens bzw FT"
fotografen:
- "Jens"
keywords: ["Universalfahrzeug", "(30481", "1984)", "von", "hinten", "mit", "Hebebühne"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42092
- /detailsee07.html
imported:
- "2019"
_4images_image_id: "42092"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42092 -->
Universalfahrzeug (30481, 1984)  von hinten mit Hebebühne