---
layout: "image"
title: "Supermodell Teleskop Mobil-Kran 30474 (1983)"
date: "2015-10-18T18:20:08"
picture: "IMG_0998.jpg"
weight: "10"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Teleskop", "Mobil-Kran", "Supermodell"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42100
- /detailsa5a7-2.html
imported:
- "2019"
_4images_image_id: "42100"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T18:20:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42100 -->
Ein Original aus den 80er-Jahren.
Der Kasten gehörte zu den sog. "Supermodellen".
Der Teleskop Mobil-Kran 30474 (1983), der Truck 30477 (1983), der Tieflader 30475 (1983) und der Containerkran 30479 (1984) gehörten dazu. Alle Fahrzeugmodelle konnten mittels Radantrieb, Servo, Fernsteuerung, ggf. 2. Empfänger und weiteren Motoren nahezu komplett motorisiert & ferngesteuert werden.

Neben dem Containerkran der Teleskop-Kran wohl DAS Modell, welches seinerzeit die meisten und längsten Aluminiumprofile enthielt. Stolzer Preis war damals glaube ich rund 200DM, wenn ich recht erinnere ?!  
einige inzwischen sehr rare Teile waren enthalten, z.B. diese grauen Gelenke an den Stützen ( heute Rot) 31424 (1983) + 31425 (1983), graue Seilwinden-Halbschalen 32086 (1984) am Teleskop-Auszug, um ein Seil darum zu legen. Ebenso die grauen 20er Kunststoff-Federn 38630 (1983) für die 2. Hinterachse, und die graue S-Strebe 15 ArtNr 38577 (1982)