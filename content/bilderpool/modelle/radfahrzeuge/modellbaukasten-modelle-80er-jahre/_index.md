---
layout: "overview"
title: "Modellbaukästen Modelle der 80er Jahre"
date: 2020-02-22T07:57:35+01:00
legacy_id:
- /php/categories/3133
- /categoriescc17.html
- /categories54cf.html
- /categoriesba74.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3133 --> 
Hier sollen nach und nach alle (original) Modelle aus den 80er Jahren gezeigt werden, die von FT erhältlich waren - weitestgehend Original - jedoch sind leider nicht immer alle Teile vorhanden und es kann zu kleine Varianten kommen .