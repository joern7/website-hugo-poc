---
layout: "image"
title: "Umgebauter Abschlepper"
date: "2012-07-16T07:22:36"
picture: "S73F1403.jpg"
weight: "8"
konstrukteure: 
- "Thomas Kaltenbrunner"
fotografen:
- "Thomas Kaltenbrunner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35170
- /details5e8a.html
imported:
- "2019"
_4images_image_id: "35170"
_4images_cat_id: "122"
_4images_user_id: "1342"
_4images_image_date: "2012-07-16T07:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35170 -->
Es war anfangs ein Abschleppwagen aus noch grauer Fischertechnik.
Ich habe das Controlset eingebaut Servo als Lenkung.
Der Antrieb besteht aus 2 Powermotoren 50:1 man könnte aber auch schnellere verwenden.
Das Ziel war einen schnellen Leistungsfähigen Flitzer zu bauen.
Da 2 Motoren gleichzeitig antreiben und über 2 Ausgänge laufen ist die Fernbedienung etwas verändert.