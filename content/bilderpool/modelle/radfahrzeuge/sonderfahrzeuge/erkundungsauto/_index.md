---
layout: "overview"
title: "Erkundungsauto"
date: 2020-02-22T07:57:19+01:00
legacy_id:
- /php/categories/1704
- /categoriesc89d.html
- /categoriesc65d.html
- /categoriesad31.html
- /categoriesf768.html
- /categoriesda42.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1704 --> 
Dieses Auto hat vorne eine Sensorleiste um die Gegend zu erkunden. 2 Farbsensoren 1 Ultraschallabstandssensor 1 Fotowiderstand und einen Reedkontakt