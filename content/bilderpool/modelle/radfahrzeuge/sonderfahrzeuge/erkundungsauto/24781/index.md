---
layout: "image"
title: "Innengehäuse"
date: "2009-08-13T20:03:44"
picture: "erkundungsauto12.jpg"
weight: "12"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24781
- /detailscd19-2.html
imported:
- "2019"
_4images_image_id: "24781"
_4images_cat_id: "1704"
_4images_user_id: "987"
_4images_image_date: "2009-08-13T20:03:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24781 -->
zeigt das Innengehäuse