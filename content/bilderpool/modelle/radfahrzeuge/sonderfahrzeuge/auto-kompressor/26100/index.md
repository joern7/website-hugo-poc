---
layout: "image"
title: "Auto mit Kompressor (unten)"
date: "2010-01-15T20:38:44"
picture: "automitkompressor4.jpg"
weight: "4"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26100
- /details1cc4-2.html
imported:
- "2019"
_4images_image_id: "26100"
_4images_cat_id: "1847"
_4images_user_id: "1057"
_4images_image_date: "2010-01-15T20:38:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26100 -->
Ein Auto mit Kompressor und Druckluftabschaltung und 20:1 Motor