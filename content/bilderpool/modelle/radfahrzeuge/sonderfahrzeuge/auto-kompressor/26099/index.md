---
layout: "image"
title: "Auto mit Kompressor (kompressor und abschaltung)"
date: "2010-01-15T20:38:44"
picture: "automitkompressor3.jpg"
weight: "3"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26099
- /details46d8.html
imported:
- "2019"
_4images_image_id: "26099"
_4images_cat_id: "1847"
_4images_user_id: "1057"
_4images_image_date: "2010-01-15T20:38:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26099 -->
Ein Auto mit Kompressor und Druckluftabschaltung und 20:1 Motor