---
layout: "image"
title: "Baumstammgreifer 01"
date: "2012-12-01T18:27:05"
picture: "greifer1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36244
- /detailse417.html
imported:
- "2019"
_4images_image_id: "36244"
_4images_cat_id: "2689"
_4images_user_id: "453"
_4images_image_date: "2012-12-01T18:27:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36244 -->
Der etwas umgebaute Baumstammgreifer aus dem Pneumatik 3 Kasten, er hat einen Antriebsmotor, Lenkservo und Magnetventile bekommen, dadurch ist er voll ferngesteuert.
http://youtu.be/8obZKUHKtmE