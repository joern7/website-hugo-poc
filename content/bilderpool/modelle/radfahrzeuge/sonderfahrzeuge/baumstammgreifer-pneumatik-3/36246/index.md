---
layout: "image"
title: "Baumstammgreifer 03"
date: "2012-12-01T18:27:05"
picture: "greifer3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36246
- /details14f0-2.html
imported:
- "2019"
_4images_image_id: "36246"
_4images_cat_id: "2689"
_4images_user_id: "453"
_4images_image_date: "2012-12-01T18:27:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36246 -->
Steuerung