---
layout: "image"
title: "monowheel_6"
date: "2013-09-13T11:07:03"
picture: "monowheel_6.jpg"
weight: "6"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37383
- /details418e-2.html
imported:
- "2019"
_4images_image_id: "37383"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37383 -->
Der Hamster ohne Rad… , hinten die Kabel ‚Fernsteuerung‘ für Beschleunigen, Bremsen und Schalten