---
layout: "overview"
title: "monowheel"
date: 2020-02-22T07:57:28+01:00
legacy_id:
- /php/categories/2780
- /categories8c5f.html
- /categoriesf0ca.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2780 --> 
Fahrzeug bestehend aus einem einzigen Rad. Antrieb, Schaltung etc: alles integriert!