---
layout: "image"
title: "monowheel_1"
date: "2013-09-13T11:07:02"
picture: "monowheel_1_3.jpg"
weight: "1"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37378
- /details65b1-3.html
imported:
- "2019"
_4images_image_id: "37378"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37378 -->
Hier der Prototyp eines sogenannten Monowheel. 
Das Wheel wird angetrieben von einer einachsigen 'Laufkatze' (ich nenn das einfach mal so), mit 2 Antriebs (doppel) rädern, die das Wheel beim Beschleunigen nach vorn drückt, und so für den Vortrieb sorgt. Ausserdem ist ein 2-Gang Getriebe eingebaut, inklusive Leerlauf. Damait kann das Gerät, einmal in fahrt, auch  frei weiterrollen..
Der einzige Kontakt der Laufkatze mit dem Wheel sind die beiden Antriebsräder. Das Teil kann also frei nach vorn und hinten kippen, unten ist ist auch genug Platz dafür da.

Im Prinzip sollten mit so einem Teil - zumindest für FT-Modell 
Verhältnisse - hohe, bis sogar sehr hohe Geschwindigkeiten erreicht werden können, die Laufkatze läuft ja  quasi 'auf Schienen', resp. führt seine Schienen  immer gleich mit sich ..