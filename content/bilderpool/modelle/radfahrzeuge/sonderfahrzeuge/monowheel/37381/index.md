---
layout: "image"
title: "monowheel_4"
date: "2013-09-13T11:07:03"
picture: "monowheel_4.jpg"
weight: "4"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37381
- /detailsaf5b-2.html
imported:
- "2019"
_4images_image_id: "37381"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37381 -->
Neben dem Antriebsmotor sind die Stecker für den Akku, der eigentlich neben dem Motor platziert ist, ist gerade an der Ladestation..