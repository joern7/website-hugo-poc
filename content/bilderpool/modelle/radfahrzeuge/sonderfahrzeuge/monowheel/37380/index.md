---
layout: "image"
title: "monowheel_3"
date: "2013-09-13T11:07:03"
picture: "monowheel_3.jpg"
weight: "3"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37380
- /details0f10-2.html
imported:
- "2019"
_4images_image_id: "37380"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37380 -->
Im Prinpip könnte man die Laufkatze durch Abringen von zusätzlichen Rädern an der 'oberen' Wheelseite auch am freien Drehen um die eigene Achse hindern. Ich fands aber noch ganz witzig so, das heisst, der einzige Kontakt mit dem Wheel passiert über die 2 Räder, die auf der Antriebsachse sitzen. 
Dadurch entstehen beim Beschleunigen, Bremsen, und vor allem beim Schalten natürlich ziemlich 'interressante' Drehbewegungen. Beim Beschleunigen zb.kippt das Teil wegen dem Motordrehmoment nach vorn, beim Bremsen nach hinten. Ausserem hat das Wheel eine gewisse Trägheit, die bewirkt dass die Laufkatze beim Beschleunigen jedes Mal  ein Stück daran vorn 'hochläuft', bevor sie Mr. Newton wieder nach unten an den tiefsten Punkt befördet, was ja auch für den eigentlichen Antrieb sorgt. Umgekehrt ist es beim Abbremsen, wo das Teil  entsprechend hinten hochläuft.
Kurzum: das ganze Gefährt entwickelt teils eine recht überraschende und eigene Beweungsdynamk, aus der ich erst ehrlich gesagt noch nicht ganz schlau geworden bin ..
Das Steuerprogamm ist darum auch erstmal  in einer 'Basisversion' vorhanden: Bschleunigen, Bremsen, Gangwechsel hoch und runter, Schalten in den Leerlauf.
Ah ja, Monowheels gibts übrigens tatsächlich, einfach mal unter youtube nach 'monowheel' suchen ..