---
layout: "image"
title: "So klein wie ich konnte"
date: "2012-03-07T22:55:11"
picture: "automitmikrolenkung1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34609
- /details0153.html
imported:
- "2019"
_4images_image_id: "34609"
_4images_cat_id: "2554"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:55:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34609 -->
So, wenn thomas004 denkt, es geht nicht mehr schlimmer, kommt das hier :-) Aufgabenstellung: Servolenkung mit nur 1 Bausteinbreite zwischen den Achsschenkeln. Fahrer egal. Klein um jeden Preis.

Und überraschenderweise fährt das Ding gut! Ein Video folgt.