---
layout: "image"
title: "Lenkung"
date: "2012-03-07T22:55:11"
picture: "automitmikrolenkung5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34613
- /details91b7.html
imported:
- "2019"
_4images_image_id: "34613"
_4images_cat_id: "2554"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:55:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34613 -->
Hier sieht man die kleine Lenkung im Detail. Erst durch diese komische Befestigung der BS7,5 ergab sich der richtige Abstand zwischen den Kontaktstiften. Mit einem dritten BS7,5 dazwischen, der auf der Servoachse steckte, war es zu breit und die Lenkung bekam O-Beine.