---
layout: "image"
title: "1. Antriebsmodifikation"
date: "2011-09-20T17:04:50"
picture: "kehrmaschiene2.jpg"
weight: "13"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31865
- /details5a84.html
imported:
- "2019"
_4images_image_id: "31865"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31865 -->
Hier eine Nahaufname der Kraftübertragung zum Differential.