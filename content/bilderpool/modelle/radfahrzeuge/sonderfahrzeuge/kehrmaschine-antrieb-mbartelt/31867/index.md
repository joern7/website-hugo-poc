---
layout: "image"
title: "1. Antriebsmodifikation"
date: "2011-09-20T17:04:50"
picture: "kehrmaschiene4.jpg"
weight: "15"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31867
- /details0c00.html
imported:
- "2019"
_4images_image_id: "31867"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31867 -->
Frontansicht der 2. Antriebsmodifikation. So ist auch wieder Platz für den Fahrersitz und dem obligatorischen FT-Fahrer. Im nächsten Schritt werde ich mir nochmal die vordere Bürste annehmen, weil die dreht sich zu schnell wenn man etwas rasanter fährt. Ich denke ein Mini-Motor wäre nicht schlecht. Schade ist allerdings, daß das Control-Set keine Möglichkeiten hat, einen Motor ein-/auszuschalten. Wäre auch auch eine straßentaugliche Beleuchtung nicht schlecht.