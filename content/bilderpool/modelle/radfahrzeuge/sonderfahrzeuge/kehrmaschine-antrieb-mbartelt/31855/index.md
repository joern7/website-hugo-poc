---
layout: "image"
title: "Hintere Ansicht mit Lenkung"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine03.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31855
- /details083d.html
imported:
- "2019"
_4images_image_id: "31855"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31855 -->
Dieses Modell ist mit einer manuellen Lenkung der Hinterachse ausgestattet.