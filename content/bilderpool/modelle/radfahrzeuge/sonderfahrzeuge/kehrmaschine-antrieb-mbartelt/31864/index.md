---
layout: "image"
title: "1. Antriebsmodifikation"
date: "2011-09-20T17:04:49"
picture: "kehrmaschiene1.jpg"
weight: "12"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31864
- /details7533.html
imported:
- "2019"
_4images_image_id: "31864"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31864 -->
Der Kommentar, daß der Motor doch anders eingebaut werden könne hat mir keine Ruhe gelassen. Also überlegt, die Teile gesichtet die in Frage kömmen könnten und losgelgt. Das Fahrzeug wurde durch einen Baustein 30 verlängert. Akku, Fernbedienungsempfänger und Soundmodul müßen noch eingebaut werden.