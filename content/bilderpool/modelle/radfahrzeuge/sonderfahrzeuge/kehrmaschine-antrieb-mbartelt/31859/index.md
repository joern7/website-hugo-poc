---
layout: "image"
title: "Teilansicht Motoriesierung und Reinigungsbürste"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine07.jpg"
weight: "7"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31859
- /details60dd.html
imported:
- "2019"
_4images_image_id: "31859"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31859 -->
Hier sieht man, wie ich den Powermotoe eingebaut habe. Andersherung ist nicht möglich, da sich dort die rotierende Reinigungsbürste und der Auffangbehäter befindet. Die Befestigung der vorderen Reinigungsbürste wurde geändert sowie der Antrieb der Bürste. Im original besteht keine durchgehende Achse. Ich habe das Differential mit zwei unterschiedlich langen Steckachsen verbaut. Der Motor soll noch verkeidet werden.