---
layout: "image"
title: "von unten 2"
date: "2010-05-27T12:28:05"
picture: "dreiraedigerseitlichfahrer5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/27305
- /detailsc973-2.html
imported:
- "2019"
_4images_image_id: "27305"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27305 -->
hier sieht man die Schnecke die die 3 Achsen für die Lenkung antreibt.