---
layout: "image"
title: "Gesamt"
date: "2010-05-27T12:28:04"
picture: "dreiraedigerseitlichfahrer1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/27301
- /details6fc6.html
imported:
- "2019"
_4images_image_id: "27301"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27301 -->
Allradantrieb mit Allradlenkung - ein tolles Konzept: Hier in der Dreirad Variante.
Egal in welche Richtung das Fahrzeug fährt, der Fahrer schaut immer in die gleiche Richtung.