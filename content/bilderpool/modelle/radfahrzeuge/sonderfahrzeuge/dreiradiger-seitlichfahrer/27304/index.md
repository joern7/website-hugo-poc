---
layout: "image"
title: "von unten"
date: "2010-05-27T12:28:05"
picture: "dreiraedigerseitlichfahrer4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/27304
- /details7aa4.html
imported:
- "2019"
_4images_image_id: "27304"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27304 -->
dieser S-Motor ist für die Lenkung.
Über eine in der Mitte sitzende Schnecke (unter dem Z30) treibt er 3 Wellen mit Rast-Z10 an beiden Enden.
Das hinzubekommen war eigentlich das schwierigste.

Die Räder zeigen hier nicht ganz genau in eine Richtung, dass passiert auch manchmal während der Fahrt wenn man an eine Kante stösst. Das ist dann nicht so toll.
Aber eigentlich kann man ziemlich zuverlässig umhercruisen, nur die Steuerung ist gewöhnungsbedürftig ;)