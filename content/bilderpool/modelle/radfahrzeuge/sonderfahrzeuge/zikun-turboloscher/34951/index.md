---
layout: "image"
title: "09 Innen"
date: "2012-05-15T22:00:23"
picture: "turboloescher06.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34951
- /details32d7.html
imported:
- "2019"
_4images_image_id: "34951"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34951 -->
Viel erkennt man leider nicht.
Die Zahnräder gehören zum Ventil, das letztendlich den Wasserfluss öffnet oder schließt.
Das hier habe ich verwendet: http://www.ftcommunity.de/details.php?image_id=29743