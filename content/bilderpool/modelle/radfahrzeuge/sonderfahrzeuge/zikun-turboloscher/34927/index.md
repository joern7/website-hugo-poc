---
layout: "image"
title: "03 Zikun Turbolöscher"
date: "2012-05-11T12:54:43"
picture: "turboloescher3.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34927
- /details8973.html
imported:
- "2019"
_4images_image_id: "34927"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-11T12:54:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34927 -->
Die Turbinen habe ich hier durch zwei Räder simuliert, die ich auf zwei Motoren gesteckt habe. 
Es kommt nur aus einer Düse Wasser, da der Druck sonst zu gering wäre.

Video: http://www.youtube.com/watch?v=kqJoE2jjxog