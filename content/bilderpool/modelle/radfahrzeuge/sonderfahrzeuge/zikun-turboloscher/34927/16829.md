---
layout: "comment"
hidden: true
title: "16829"
date: "2012-05-12T17:37:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
"Gedacht" sind die für eine Luftschranke (das pneumatische Äquivalent zu einer Lichtschranke). Zwei von den Düsen gut fluchtend gegenüber. Druckluft auf die eine (mit einer Drossel vornedran). An die hintere einen Einfach-Betätiger, der ein Ventil betätigt (alles aus der ft-Festop-Pneumatik stammend). Damit kannst Du kurze (!) Entfernungen (mehr als 1 cm ist kaum drin) daraufhin überwachen, ob ein Gegenstand (dünne Bauplatte z. B.) zwischendrin liegt.

Gruß,
Stefan