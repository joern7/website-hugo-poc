---
layout: "image"
title: "15 Steuerpult"
date: "2012-05-15T22:00:23"
picture: "turboloescher12.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34957
- /details8b77-3.html
imported:
- "2019"
_4images_image_id: "34957"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34957 -->
Links ist ein Schieberegler, der die Geschwindigkeit des Fahrmotor in vier Stufen steuern kann. Daneben ein Schalter für vorwärts / rückwärts;  Rechts unten die Lenkung

Rechts ein weiterer Schieberegler, der zwei Stufen hat:
1. Bereitschaftszustand: Die Turbinen laufen langsam, der Kompressor läuft
2. Turbinen laufen schnell, Ventil wird geöffnet und Wasser läuft