---
layout: "image"
title: "12 Rückseite"
date: "2012-05-15T22:00:23"
picture: "turboloescher09.jpg"
weight: "12"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34954
- /details9c5b-2.html
imported:
- "2019"
_4images_image_id: "34954"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34954 -->
so sieht das ganze von hinten aus

Wasser kommt nur aus der rechten Düse.