---
layout: "image"
title: "07 Tankbefüllung"
date: "2012-05-15T22:00:23"
picture: "turboloescher04.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34949
- /details3901.html
imported:
- "2019"
_4images_image_id: "34949"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34949 -->
So wird der externe Tank angeschlossen:
1. Das untere Ventil wird geschlossen.
2. Das obere wird geöffnet und der Tank an den mittleren Eingang des Ventils angeschlossen.
3. Der andere Schlauch des Tanks wird unten an den anderen Eingang des Luftdrucktanks angeschlossen.

-> Der Kompressor pumpt jetzt Luft durch den Luftdrucktank in den externen Tank, dort wird das Wasser rausgedrückt und fließt durch das obere Ventil in den Wassertank.