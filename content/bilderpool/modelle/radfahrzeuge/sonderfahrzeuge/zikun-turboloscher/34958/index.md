---
layout: "image"
title: "16 Steuerpult"
date: "2012-05-15T22:00:23"
picture: "turboloescher13.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34958
- /detailsc1c5.html
imported:
- "2019"
_4images_image_id: "34958"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34958 -->
von links nach rechts:
1. Drehen
2. hoch / runter
3. Licht
4. Scheinwerfer
5. Blinklicht