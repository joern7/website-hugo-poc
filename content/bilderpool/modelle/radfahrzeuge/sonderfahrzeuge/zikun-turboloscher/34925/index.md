---
layout: "image"
title: "01 Zikun Turbolöscher"
date: "2012-05-11T12:54:43"
picture: "turboloescher1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34925
- /detailsbc41.html
imported:
- "2019"
_4images_image_id: "34925"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-11T12:54:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34925 -->
Da ich vor ca. zwei Jahren den Rosenbauer Panther gebaut hatte (http://www.ftcommunity.de/details.php?image_id=28988) und es mir das Modell sehr angetan hat, habe ich jetzt ein anderes Feuerwehr-Sonderfahrzeug gabaut, nämlich den Turbolöscher der Firma Zikun. Er hat hinten einen drehbaren Aufsatz, in dem sich zwei Turbinen und vier Düsen befinden. Diese erzeugen einen großen Wassernebel, der ein Feuer schnell und effektiv löschen kann. Mir war aber von vornherein klar, dass ich das nicht unsetzten kann, es ist ja aber auch nur ein Modell.

Ein Video gibt es hier: http://www.youtube.com/watch?v=kqJoE2jjxog

Weitere Bilder werden definitiv noch folgen, die drei habe ich nur schnell bei den Versuchen im Garten gamacht