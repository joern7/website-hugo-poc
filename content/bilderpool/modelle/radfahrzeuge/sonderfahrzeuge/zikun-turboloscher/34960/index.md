---
layout: "image"
title: "18 Steuerpult"
date: "2012-05-15T22:00:23"
picture: "turboloescher15.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34960
- /detailsd324-2.html
imported:
- "2019"
_4images_image_id: "34960"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34960 -->
Hier sieht man die vier Taster des Schiebereglers

Links ein Schalter, der die LEDs im Steuerpult in zwei Helligkeitsstufen ansteuern kann (deshalb auch zwei Widerstände)