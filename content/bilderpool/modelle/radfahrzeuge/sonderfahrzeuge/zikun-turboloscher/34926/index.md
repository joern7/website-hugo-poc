---
layout: "image"
title: "02 Zikun Turbolöscher"
date: "2012-05-11T12:54:43"
picture: "turboloescher2.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34926
- /detailsa942-3.html
imported:
- "2019"
_4images_image_id: "34926"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-11T12:54:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34926 -->
Funktionen:
- zwei angetriebene Achsen + Lenkung
- drehbare und höhenverstellbare Wasserkanone
- zwei Tanks, zwei Kompressoren + Ventil 
- halbautomatische Tankbefüllung
- Schnittstelle für Steuerpult
- Licht, Blinklicht, Scheinwerfer