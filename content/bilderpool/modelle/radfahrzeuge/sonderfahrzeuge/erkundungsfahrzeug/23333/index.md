---
layout: "image"
title: "Greifzange"
date: "2009-03-02T18:07:36"
picture: "erkundungsfahrzeug04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23333
- /detailsbb8e-2.html
imported:
- "2019"
_4images_image_id: "23333"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23333 -->
