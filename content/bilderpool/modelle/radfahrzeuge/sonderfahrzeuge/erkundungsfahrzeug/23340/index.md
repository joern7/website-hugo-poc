---
layout: "image"
title: "Unterboden"
date: "2009-03-02T18:07:36"
picture: "erkundungsfahrzeug11.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23340
- /details8aa6.html
imported:
- "2019"
_4images_image_id: "23340"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23340 -->
