---
layout: "image"
title: "Vorne"
date: "2010-06-06T20:34:44"
picture: "dreiradmitraupenantrieb1.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/27395
- /details8e1d.html
imported:
- "2019"
_4images_image_id: "27395"
_4images_cat_id: "1968"
_4images_user_id: "1122"
_4images_image_date: "2010-06-06T20:34:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27395 -->
Hier sind zwei Mini Motoren eingebaut.