---
layout: "image"
title: "Mit Laser 01"
date: "2011-02-06T15:26:10"
picture: "fahrzeugemitencodermotoren1.jpg"
weight: "1"
konstrukteure: 
- "Lars B."
fotografen:
- "Lars B."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29867
- /detailsad5f.html
imported:
- "2019"
_4images_image_id: "29867"
_4images_cat_id: "2203"
_4images_user_id: "1177"
_4images_image_date: "2011-02-06T15:26:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29867 -->
vorne is ein kleiner laser eingebaut, der über das cntrl. set angesteuert wird