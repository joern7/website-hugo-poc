---
layout: "image"
title: "Luftschraubenauto (6)"
date: "2009-03-06T21:42:09"
picture: "luftschraubenauto1_2.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/23397
- /detailsc17a.html
imported:
- "2019"
_4images_image_id: "23397"
_4images_cat_id: "1595"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T21:42:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23397 -->
Das Auto hat eine "Steckdose", um das Ladegerät anzuschließen. Man muss nichts an den Kabeln umstecken, um den Accu zu laden.