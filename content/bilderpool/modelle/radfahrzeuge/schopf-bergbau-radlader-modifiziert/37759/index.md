---
layout: "image"
title: "Schopf-Bergbau-Radlader   -modifiziert"
date: "2013-10-23T21:50:46"
picture: "bergbauradlader07.jpg"
weight: "7"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37759
- /details5e47-2.html
imported:
- "2019"
_4images_image_id: "37759"
_4images_cat_id: "2805"
_4images_user_id: "22"
_4images_image_date: "2013-10-23T21:50:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37759 -->
SCHOPF Mining - Lösungen für die  Tunnel- und Bergbau-Industrie
