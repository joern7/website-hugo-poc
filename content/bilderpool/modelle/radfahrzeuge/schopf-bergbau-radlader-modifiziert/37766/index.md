---
layout: "image"
title: "Schopf-Bergbau-Radlader   -modifiziert"
date: "2013-10-23T21:50:46"
picture: "bergbauradlader14.jpg"
weight: "14"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37766
- /details3357-2.html
imported:
- "2019"
_4images_image_id: "37766"
_4images_cat_id: "2805"
_4images_user_id: "22"
_4images_image_date: "2013-10-23T21:50:46"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37766 -->
SCHOPF Mining - Lösungen für die  Tunnel- und Bergbau-Industrie

Ferngesteuerter Schopf-Bergbau-Radlader mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung

modifiziert mit:

- pneumatik Ventilen 3-way, 2 position  NO + NC & Distributor 

- 15x15 Alu-Profilen OpenBeam

Inspiration:  Erbes-Büdesheim-2013   -Jörg und Erik Busch

http://www.ftcommunity.de/categories.php?cat_id=2785