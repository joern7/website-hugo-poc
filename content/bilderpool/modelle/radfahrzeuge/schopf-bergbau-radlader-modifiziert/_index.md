---
layout: "overview"
title: "Schopf-Bergbau-Radlader  -modifiziert"
date: 2020-02-22T07:57:31+01:00
legacy_id:
- /php/categories/2805
- /categoriese5ef.html
- /categories7045.html
- /categoriesa07b.html
- /categories88e3.html
- /categoriescb5f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2805 --> 
Inspiration:  Erbes-Budeheim-2013   -Jörg und Erik Busch

Ferngesteuerter Schopf-Bergbau-Radlader mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung

modifiziert mit:

- pneumatik Ventilen 3-way, 2 position  NO + NC & Distributor 

- 15x15 Alu-Profilen OpenBeam 
