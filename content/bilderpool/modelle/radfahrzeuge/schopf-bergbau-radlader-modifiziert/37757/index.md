---
layout: "image"
title: "Schopf-Bergbau-Radlader   -modifiziert"
date: "2013-10-23T21:50:46"
picture: "bergbauradlader05.jpg"
weight: "5"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37757
- /details2ae8.html
imported:
- "2019"
_4images_image_id: "37757"
_4images_cat_id: "2805"
_4images_user_id: "22"
_4images_image_date: "2013-10-23T21:50:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37757 -->
Direkter Link zum Pneumatik Ventilen
2-way or 3-way, 2 position valve (NO, NC & Distributor) 

http://www.sensortechnics.com/en/products/other-products/miniature-solenoid-valves/series-11/ 

http://www.sensortechnics.com/cms/upload/datasheets/DS_Standard-Series-11_E_11259.pdf 

