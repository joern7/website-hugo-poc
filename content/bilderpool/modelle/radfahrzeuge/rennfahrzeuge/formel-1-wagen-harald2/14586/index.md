---
layout: "image"
title: "F1b-15.JPG"
date: "2008-05-23T18:27:04"
picture: "F1b-15.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14586
- /details5ec3-2.html
imported:
- "2019"
_4images_image_id: "14586"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:27:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14586 -->
Die hintere Radaufhängung im Detail. Die S-Streben sind mit Strebenadaptern 31848 (ein Knubbel) und Statikadaptern 35975 (zwei Knubbel) befestigt. Der Motor wird einfach in die blaue Verkleidungsplatte eingeschoben und muss noch gegen Herausflutschen gesichert werden.