---
layout: "image"
title: "F1b-12.JPG"
date: "2008-05-23T18:23:33"
picture: "F1b-12.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14584
- /details4dd9-4.html
imported:
- "2019"
_4images_image_id: "14584"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:23:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14584 -->
Die Hinterachsaufhängung: drei S-Streben pro Rad reichen aus. Damit die beiden unteren unter Last nicht aus dem BS15/Loch herausrutschen (und zwecks Höhenausgleich), ist ein Statikadapter 35975 dazwischen angebracht.