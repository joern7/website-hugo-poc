---
layout: "image"
title: "f25.jpg"
date: "2017-03-24T06:51:52"
picture: "f25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45667
- /details31b4.html
imported:
- "2019"
_4images_image_id: "45667"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45667 -->
