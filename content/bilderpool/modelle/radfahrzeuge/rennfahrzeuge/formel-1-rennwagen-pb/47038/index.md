---
layout: "image"
title: "fbesser05.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser05.jpg"
weight: "75"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47038
- /detailsad72-2.html
imported:
- "2019"
_4images_image_id: "47038"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47038 -->
