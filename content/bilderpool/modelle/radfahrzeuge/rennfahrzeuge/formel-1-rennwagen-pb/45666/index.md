---
layout: "image"
title: "f24.jpg"
date: "2017-03-24T06:51:52"
picture: "f24.jpg"
weight: "24"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45666
- /detailsb535-2.html
imported:
- "2019"
_4images_image_id: "45666"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45666 -->
Die 2x BS5 halten den BS7,5 auf 'm Platz