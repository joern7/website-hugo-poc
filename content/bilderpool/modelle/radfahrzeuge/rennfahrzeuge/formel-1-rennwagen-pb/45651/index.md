---
layout: "image"
title: "f09.jpg"
date: "2017-03-24T06:50:47"
picture: "f09.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45651
- /details540c-2.html
imported:
- "2019"
_4images_image_id: "45651"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45651 -->
Und Rechts.
Die Lenkung hab ich erstmals mit den Servo probiert, aber ich fand Keine Weg den Lenkeinschlag befriedigend Groß zu kriegen. Also bin ich gewechselt und hab die kleinen Motor mit Hubgetriebe verwendet. Die Lenkeinschlag ist jetzt sehr gut, aber es dauert eine Weile biss mann am Ende der Einschlag ist. Und zurück dasselbe Problem. Für ein Racer ist das schon ein Bißchen Schade.