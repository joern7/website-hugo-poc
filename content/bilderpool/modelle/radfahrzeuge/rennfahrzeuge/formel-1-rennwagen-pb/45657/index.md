---
layout: "image"
title: "f15.jpg"
date: "2017-03-24T06:51:52"
picture: "f15.jpg"
weight: "15"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45657
- /detailseedf-2.html
imported:
- "2019"
_4images_image_id: "45657"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45657 -->
Ich habe bisher Keine Schwarzen I-streben 120 mit Löcher, und wollte unbedingt Keine Gelben verwenden. Das gab schon ein Bißchen Ärger beim Aufbau der Lenkung, aber Endlich hatts geklappt.