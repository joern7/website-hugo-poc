---
layout: "comment"
hidden: true
title: "23305"
date: "2017-03-25T11:02:03"
uploadBy:
- "PB"
license: "unknown"
imported:
- "2019"
---
@Stefan: Wenn ich die Bilder vom Original nochmal genau betrachtete, sah ich das das vorne Teil gar nicht gebogen ist! Nur ein Knick etwas vor den Reifen und dann Recht nach Unten. Mann sieht manchmal was mann sehen will...