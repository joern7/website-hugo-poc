---
layout: "image"
title: "Etwas kürzer 3"
date: "2017-03-25T13:34:58"
picture: "fetwaskuerzer3.jpg"
weight: "57"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45699
- /details844d-2.html
imported:
- "2019"
_4images_image_id: "45699"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-25T13:34:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45699 -->
