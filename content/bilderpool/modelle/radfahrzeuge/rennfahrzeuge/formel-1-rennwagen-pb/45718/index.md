---
layout: "image"
title: "fservo4.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo4.jpg"
weight: "66"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45718
- /detailsdf51.html
imported:
- "2019"
_4images_image_id: "45718"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45718 -->
Mit eine S-Strebe zwischen Servohebel und Spurstange klappte es nicht: den Kraft vom Servo war zu viel nach Vorn und zu wenig nach der Seite gerichtet. Mit das T-stück das beim Servo gehört, hatts doch geklappt, aber ein Schönheitspreis wird es hiervor wohl nicht geben.
Mann sieht hier auch den angepasster Befestigung der 2 gelben BS30 am Untenseite, die nötig war weil die Spurstange beim Lenken näher an die Radaufhängung kommt.