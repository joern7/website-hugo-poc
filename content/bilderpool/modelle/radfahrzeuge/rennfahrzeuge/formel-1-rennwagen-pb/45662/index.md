---
layout: "image"
title: "f20.jpg"
date: "2017-03-24T06:51:52"
picture: "f20.jpg"
weight: "20"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45662
- /detailsd101-2.html
imported:
- "2019"
_4images_image_id: "45662"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45662 -->
Der Nase