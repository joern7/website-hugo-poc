---
layout: "image"
title: "f26.jpg"
date: "2017-03-24T06:51:52"
picture: "f26.jpg"
weight: "26"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45668
- /details15d5.html
imported:
- "2019"
_4images_image_id: "45668"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45668 -->
