---
layout: "comment"
hidden: true
title: "23318"
date: "2017-03-28T23:31:10"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Hallo Paul,

Diese -45 Strebe muss nicht unbedingt direkt am -90 Strebe verbunden sein. Es könnte auch im anderen Loch von Baustein 15 montiert worden. Oder die 5mm Steine durch 1 BS15 ersetzen und die -45 Strebe damit verbinden . Das würde auch noch ein Bisschen Toe-in geben.

Viel Spass! MfrGr Marten