---
layout: "comment"
hidden: true
title: "23314"
date: "2017-03-28T16:35:31"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Entweder das schwarze Servo Teil (mit Nummer 02) länger machen, oder die Abstand zwischen Drehpunkt der Radaufhängung und der Punkt wo der Servo die Lenkung bewegt kurzer machen.

MfrGr Marten