---
layout: "image"
title: "f42.jpg"
date: "2017-03-24T06:51:52"
picture: "f42.jpg"
weight: "42"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45684
- /detailsed97.html
imported:
- "2019"
_4images_image_id: "45684"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45684 -->
Den Accu Klemmt einfach ein.