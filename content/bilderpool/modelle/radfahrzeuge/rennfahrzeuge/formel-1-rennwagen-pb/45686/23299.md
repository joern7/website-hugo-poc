---
layout: "comment"
hidden: true
title: "23299"
date: "2017-03-24T18:48:37"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Entschuldigung, Dirk, aber das ist genau nicht "wie aus dem ft-Baukasten". Beim ft-Baukasten wären da angeblich zwingend notwendige Möchtegern-"Design"-Teile drin. Dass es auch ohne dergleichen Firlefanz ganz fantastisch geht, zeigt der Paul heuer ja gerade regelmäßig.

Gruß,
Stefan