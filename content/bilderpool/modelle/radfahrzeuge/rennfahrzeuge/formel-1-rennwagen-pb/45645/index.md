---
layout: "image"
title: "f03.jpg"
date: "2017-03-24T06:50:47"
picture: "f03.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45645
- /detailsdb43.html
imported:
- "2019"
_4images_image_id: "45645"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45645 -->
