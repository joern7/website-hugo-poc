---
layout: "comment"
hidden: true
title: "291"
date: "2004-11-01T10:03:37"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Pulleralarm!(SCNR) :-)

So'n mächtiges Aggregat hätt ich ja auch gern in meinen Fendt eingebaut, aber den Platz gibts nicht dafür.

Den Auspuff darfst du gern zeigen, und wenn du schon dabei bist, dann bitte auch die Lenkung (funktioniert die wirklich) und den Antrieb!

Gruß, Harald