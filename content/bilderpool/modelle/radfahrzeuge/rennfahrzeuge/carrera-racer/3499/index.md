---
layout: "image"
title: "FT Carrera Racer Getriebe"
date: "2005-01-04T15:20:02"
picture: "FT_Carrera_Racer_003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Carrera", "Rennauto"]
uploadBy: "joker"
license: "unknown"
legacy_id:
- /php/details/3499
- /details14dc-2.html
imported:
- "2019"
_4images_image_id: "3499"
_4images_cat_id: "321"
_4images_user_id: "89"
_4images_image_date: "2005-01-04T15:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3499 -->
MotorTyp: Fox
Achsen 3mm in Messingbuchsen geführt.
Zahnräder: aus dem Slotraceing Bedarf