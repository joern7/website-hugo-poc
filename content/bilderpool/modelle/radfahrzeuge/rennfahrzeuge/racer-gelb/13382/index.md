---
layout: "image"
title: "Racer aus Cars und Trucks"
date: "2008-01-25T15:36:04"
picture: "DSCN2052.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing und ft"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13382
- /detailsbea0-2.html
imported:
- "2019"
_4images_image_id: "13382"
_4images_cat_id: "1218"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13382 -->
Nachgebaut mit möglichst vielen gelben Bausteinen