---
layout: "comment"
hidden: true
title: "18878"
date: "2014-03-26T14:31:18"
uploadBy:
- "Dieter Braun"
license: "unknown"
imported:
- "2019"
---
Falls der .mov nicht richtig gehen will, unter www.dieterb.de/ft/Rapid_Racer.avi gibts eine AVI-Variante. Oder am besten das Originalmovie anschauen, falls es klemmt: www.dieterb.de/ft/Rapid_Racer_full.MOV zu finden. Grüße, Dieter.