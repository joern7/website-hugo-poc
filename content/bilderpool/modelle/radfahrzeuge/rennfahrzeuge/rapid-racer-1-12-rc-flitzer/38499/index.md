---
layout: "image"
title: "Getriebekasten 3"
date: "2014-03-26T10:34:34"
picture: "rapidracer11.jpg"
weight: "11"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38499
- /detailsa0cc.html
imported:
- "2019"
_4images_image_id: "38499"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38499 -->
Getriebetausch ist einfach machbar. Noch sind keine Abnutzungen am Zahnrädchen zu sehen....