---
layout: "overview"
title: "Rapid Racer: 1:12 RC Flitzer"
date: 2020-02-22T07:56:50+01:00
legacy_id:
- /php/categories/2874
- /categoriesc5ad.html
- /categories5540.html
- /categories4a6c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2874 --> 
Kann man in Fischertechnik ein schnelles Modellauto bauen? Habe ich bisher nicht recht geglaubt. Anbei der Gegenentwurf. Der Racer heizt um sehr enge Kurven und hat eine sehr ordentliche Endgeschwindigkeit. In einer kompakten Geometrie wird jedes Hinterrad extra von einem Motor angetrieben. Damit entfällt das Differential als leidiger Powerschlucker. Um die Stromabschaltung des Empfängers etwas zu entlasten, fährt er im Raupenmodus und steuert die Motoren einzeln an. Das erlaubt auch, dass man den Racer in engen Kurven durch den damit möglichen Einzelradantrieb richtig schön driften lassen kann. Das Getriebe ist schon sehr wuchtig ausgeführt, was aber auch nötig ist. Somit knackt es nicht im Getriebekarton. Einzig die Stromabschaltung läßt einen mit blinkender Empfänger-LED bei ganz wilden Manövern mal hie und da im Regen stehen. Also unser Kleiner (und die Katze) ist ganz begeistert von der Kreation. Link zum Movie: www.dieterb.de/ft/Rapid_Racer.mov (5MB, Ton ist leider etwas asynchron).