---
layout: "image"
title: "Unterseite"
date: "2009-05-14T19:55:31"
picture: "DSC01007.jpg"
weight: "2"
konstrukteure: 
- "ich"
fotografen:
- "ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24023
- /details4230.html
imported:
- "2019"
_4images_image_id: "24023"
_4images_cat_id: "1647"
_4images_user_id: "920"
_4images_image_date: "2009-05-14T19:55:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24023 -->
Ansicht von unten. 4 Motoren treiben die Räder an, einer lenkt.