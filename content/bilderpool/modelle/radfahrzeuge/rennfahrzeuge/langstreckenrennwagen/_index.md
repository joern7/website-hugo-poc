---
layout: "overview"
title: "Langstreckenrennwagen"
date: 2020-02-22T07:56:52+01:00
legacy_id:
- /php/categories/3210
- /categoriesef9e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3210 --> 
Modell eines Langstreckenrennwagens beim 24-Stundenrennen von Le Mans. Ich habe das Modell bereits am Fischertechnik Fanclubtag 2015 ausgestellt.