---
layout: "image"
title: "Ansicht vorne"
date: "2016-04-02T17:06:56"
picture: "lsrph2.jpg"
weight: "2"
konstrukteure: 
- "Patrick"
fotografen:
- "Patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43223
- /details44b7.html
imported:
- "2019"
_4images_image_id: "43223"
_4images_cat_id: "3210"
_4images_user_id: "2228"
_4images_image_date: "2016-04-02T17:06:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43223 -->
Ansicht von vorne mit Blick auf die Scheinwerfer und das flache Design.