---
layout: "image"
title: "Batteriehalterung"
date: "2007-01-15T22:50:01"
picture: "mabi09.jpg"
weight: "80"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8478
- /details4af4-3.html
imported:
- "2019"
_4images_image_id: "8478"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8478 -->
Und da ist er jetzt eingebaut