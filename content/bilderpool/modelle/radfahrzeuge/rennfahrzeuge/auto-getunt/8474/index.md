---
layout: "image"
title: "Neu von der Seite"
date: "2007-01-15T22:50:01"
picture: "mabi05.jpg"
weight: "76"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8474
- /detailsd6b9-3.html
imported:
- "2019"
_4images_image_id: "8474"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8474 -->
Hier musste ich die Bauplatte 60 auf eine Bauplatte 30 zurückschrumpfen.