---
layout: "image"
title: "Grosse Auspüffe (zusammenbau)"
date: "2006-12-09T13:38:20"
picture: "gif21.jpg"
weight: "21"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7772
- /detailsf5f0.html
imported:
- "2019"
_4images_image_id: "7772"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:20"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7772 -->
