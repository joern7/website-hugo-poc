---
layout: "image"
title: "Baterie/Akkuhalter"
date: "2006-12-11T20:00:04"
picture: "magi1.jpg"
weight: "53"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7874
- /detailsf0eb.html
imported:
- "2019"
_4images_image_id: "7874"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-11T20:00:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7874 -->
Ich habe 1.2V Akkus drin, doch die reichen nicht immer aus.