---
layout: "image"
title: "F1a-03.JPG"
date: "2008-05-23T17:56:31"
picture: "F1a-03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14572
- /detailsa36b.html
imported:
- "2019"
_4images_image_id: "14572"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T17:56:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14572 -->
