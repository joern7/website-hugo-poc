---
layout: "image"
title: "Draufsicht auf die Front"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari03.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38653
- /details444e-3.html
imported:
- "2019"
_4images_image_id: "38653"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38653 -->
