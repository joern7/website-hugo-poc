---
layout: "overview"
title: "Formel 1 Bolide (Ferrari)"
date: 2020-02-22T07:56:51+01:00
legacy_id:
- /php/categories/2886
- /categories3434.html
- /categories34ab.html
- /categoriesc496.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2886 --> 
Eigentlich wollte ich nur einen Technik-Prototypen zur Veranschaulichung einer Lenkung mit "Spreizung" bauen, aber dann hatte ich über Ostern ein bisschen Zeit und dann kam eins zum anderen...
Die Lenkung und vor allem die Aufhängung hat mir so gut gefallen, daß ich ein Auto bauen wollte, bei der nicht die ganze Technik unter der "Karosserie" verschwindet. Deshalb kam ich auf einen Formel-1 Renner.
Leider ging jetzt nach Ostern die Zeit und auch die Teile zu Neige, so daß das Auto noch nicht ganz fertig ist.
Das Chassis ist eigentlich komplett. Akkus, Empfänger, Power-Motor, Servo, Lenkung ist alles da und funktionsfähig.
Die Nase ist auch fertig, da werd ich wahrscheinlich nichts mehr ändern, da sie eigentlich eine sehr schöne Form hat. Hinten rum fehlt noch das Blech, aber ganz am Ende ist der Heckflügel auch schon fertig (die sind ja sehr schmal in heutigen Formel 1 Wagen)

Insgesamt hat das Auto einen Status erreicht, bei dem man schon sehr gut erkennen kann, wo es hingehen soll