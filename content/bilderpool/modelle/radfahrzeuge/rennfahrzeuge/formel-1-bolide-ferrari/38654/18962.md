---
layout: "comment"
hidden: true
title: "18962"
date: "2014-04-24T15:42:09"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
das müssten die 237733-20 Kompletträder von Conrad sein. Der 4er Set kostet nur 9,95
War gerade drüben in der Conrad Filiale, da gibt es auch schöne extrem breite Slicks....die würden noch besser zum Modell passen, die waren mir aber zu teuer :(
Im Prinzip kannst Du über den Aluminium Hex Adapter von mir jedes beliebige Rad aus dem RC Modellbau an eine ft 4mm Achse schrauben, solange das Rad für einen 12 mm 6-Kant-Felgenmitnehmer vorgesehen ist. Die sind am meisten verbreitet. Bei Conrad gibt es alle möglichen Kompletträder von 50m bis 125mm, von günstig bis teuer.
Nur bei sehr großen Rädern (120...180 mm) sind 17 mm Felgenmitnehmer üblich.