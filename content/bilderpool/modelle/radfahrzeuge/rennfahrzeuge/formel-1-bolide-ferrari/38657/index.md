---
layout: "image"
title: "Achsschenkel von hinten"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari07.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38657
- /details1779.html
imported:
- "2019"
_4images_image_id: "38657"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38657 -->
Bei der Ansicht von vorne hat das "Luftleitblech" die Sicht etwas verdeckt. Von hinten sieht man, daß der eigentliche Achsschenkel nur aus 4 Einzelteilen aufgebaut ist (Schneckenmutter, 2 Winkelsteine, U-Stein)
Die Lenkhebel sind nur in die Nut der U-Steine geschoben und halten recht gut. Die schwarze Statikstrebe bildet das Lenkgestänge.