---
layout: "image"
title: "Gesamtansicht"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari01.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38651
- /details775d.html
imported:
- "2019"
_4images_image_id: "38651"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38651 -->
Von knapp hinter der Lenkung bis zur Hinterachse fehlt noch die Karosserie.
Der Antrieb ist eher schlicht. Ein Powermotor quer eingebaut überträgt über 3 Zahnräder die Kraft auf ein Differential. Alle drehenden Achsen sind in Schneckenmuttern kugelgelagert