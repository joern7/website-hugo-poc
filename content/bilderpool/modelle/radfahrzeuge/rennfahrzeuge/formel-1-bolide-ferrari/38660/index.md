---
layout: "image"
title: "Lenkungseinschlag Kurve Außenseite"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari10.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38660
- /detailsabe9-2.html
imported:
- "2019"
_4images_image_id: "38660"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38660 -->
Beim Einschlagen der Lenkung in die andere Richtung passiert im Prinzip das selbe. Der Sturzwinkel ist allerdings entgegengesetzt und das Rad stellt sich auf die Innenkante.
