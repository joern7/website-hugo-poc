---
layout: "image"
title: "Lenkungseinschlag Kurve Innenseite"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari09.jpg"
weight: "9"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38659
- /details02db.html
imported:
- "2019"
_4images_image_id: "38659"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38659 -->
Bei voll eingeschlagenen Rad sieht man, daß sich durch die Spreizung das Rad auf die Reifen-Aussenflanke stellt. Damit wird das Auto ein bisschen angehoben und es bildet sich eine Rückstellkraft.