---
layout: "image"
title: "Zwei Brushless Nabenmotoren hinten..."
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43771
- /details38a3.html
imported:
- "2019"
_4images_image_id: "43771"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43771 -->
... geben dem Teil viel Speed und viel Power. Beim Anfahren zuckeln die sensorlosen Fahrtenregler ein bißchen und der Motor rechts startet einen Tick früher als links, aber mit beherztem Gasgeben kommt die Fuhre schnell ins Rollen. Max-Speed ist nur durch Ausbrechen des Hecks und mangelnder Straßenlage der Vorderräder limitiert - Grob geschätzt sollte die Fuhre bei 370kV und 7.4V bis 2700 U/min auf der Hinterachse damit 370*7.4*0.06*3.14/60*3.6 = 30 km/h, aber mit 3S Akku ginge das auf 45km/h. Man muss halt die Speed erstmal auf unsere etwas unebene Straße bringen... :)