---
layout: "overview"
title: "Brushless Racer: RC-Car mit 30-45 km/h :)"
date: 2020-02-22T07:56:53+01:00
legacy_id:
- /php/categories/3240
- /categories0e59.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3240 --> 
Ein richtig flottes Modellauto mit ca. 40-50km/h max Speed? Ein Traum - nicht machbar mit Fischertechnik? Doch, geht! 

Straßenlage ist noch nicht optimal, aber ..... Wie es geht? Tja, 2x Brushless Nabenmotoren hinten, war mal der letzte Schrei bei Elektroautos... 

Nun, wie gesagt, Motoren und Getriebe sind das eigentliche Problem, der Rest ist ja von Fischertechnik sehr robust da. Und tatsächlich, es geht: man nehme einen möglichst langsam laufenden Multicopter-Motor (hier: 370KV, z.B. die hier http://www.hobbyking.com/hobbyking/store/uh_viewItem.asp?idProduct=47721) mit Außendurchmesser der großen Fischertechnikreifen und 4mm Nabe, sägt die Hälfte der Felge ab, schraubt es auf den Motor und schiebt die Reifen drauf. Kleben nicht notwendig. Die Andere Seite muss man mit 2 Schrauben an 2 Grundbausteine verschrauben. Hänge das an 2S Lipo mit zwei 25A Fahrtenreglern und los geht's. OK, kostet pro Motor leider 30 Euro und die Fahrtenregler kosten auch nochmal um die 15.- jeweils. Aber immer noch nicht super-teuer und .. supergeil!

Hier die Pics und ein Movie gibt's hier: 
www.dieterb.de/ft/brushless_racer_small.avi	

Bei hoher Speed wird die Straßenlage sehr empfindlich - da werden wir noch was nachlegen müssen. Liegt schon vielleicht auch an den Reifen, vielleicht auch am noch hohen Schwerpunkt. Auch durch gemähtes Gras geht es locker mit Schwung durch - bei kürzerem Chassis sind die Motoren so stark, dass sie einen Wheely erlauben. :)

Am Ende es Movies sieht man, was passiert, wenn man die Innenseiten der Metallachsen vorne (Plastik halten nicht lange durch) nicht mit Sekundenkleber sichert: beide Räder fliegen davon und unter großem Hallo fliegt er durch die Luft. Da ist übrigens außer ein paar Schrammen nichts passiert: sehr solide das Teil.

Einziger Schwachpunkt momentan: obwohl ich ein eigentlich solides Metallgetriebe-Lenk-Servo eingebaut habe (nicht Original-Fischertechnik), gabs im Lenkservo nach einer Weile doch Karies... war eben auch kein echtes Metallgetriebe: das einzige Plastikzahnrad hats gestrippt. Jetzt ists ausgetauscht und geht soweit gut - aber ein dickeres ist bestellt (15mm breit wäre gegangen, aber jetzt gehts erstmal aus Sicherheit gleich mit 20mm Breite und wir werden etwas fräsen an der Stelle).

Aber supertoll, dass man quasi sein Auto selbst zusammenfrickeln und modifizieren kann! Irgendwann vielleicht auch mit Frontantrieb? Oder gleich Allrad? Wir sind auf jeden Fall begeistert hier.

Grüße,
Dieter.
