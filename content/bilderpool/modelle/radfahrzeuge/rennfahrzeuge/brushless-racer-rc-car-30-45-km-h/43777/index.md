---
layout: "image"
title: "Gefederte Variante"
date: "2016-06-21T09:57:36"
picture: "D70_6633s.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43777
- /detailsddc3.html
imported:
- "2019"
_4images_image_id: "43777"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-21T09:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43777 -->
Jetzt ist er gefedert - das macht einen riesigen Unterschied: Straßenlage nun viel besser.... Siehe auch Movie: http://www.dieterb.de/ft/gefedert.avi

Grüße,
Dieter.