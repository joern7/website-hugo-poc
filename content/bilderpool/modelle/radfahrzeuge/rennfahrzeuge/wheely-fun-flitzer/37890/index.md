---
layout: "image"
title: "Hintenansicht: Antrieb"
date: "2013-12-02T12:57:36"
picture: "funflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37890
- /detailsa411-2.html
imported:
- "2019"
_4images_image_id: "37890"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37890 -->
Das ist kein Ersatzrad, sondern sitzt direkt auf einem großen Motor und treibt zuerst mit einer Übersetzung das untenliegende Differential an. Damit wird das Modell richtig schnell und beschleunig erst langsam mit einem sonoren Surren, bis nach ca. 2m die Höchstgeschwindigkeit erreicht ist. Motor wird trotzdem nach unserer Erfahrung nicht überlastet. Die hintere "Stoßstange" sorgt dafür, daß das Getriebe nicht rattert.