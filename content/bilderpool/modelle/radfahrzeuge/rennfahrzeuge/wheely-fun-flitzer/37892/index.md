---
layout: "image"
title: "Seitenansicht 1"
date: "2013-12-02T12:57:36"
picture: "funflitzer4.jpg"
weight: "4"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37892
- /details70c1.html
imported:
- "2019"
_4images_image_id: "37892"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37892 -->
Fernsteuerung liegt oben, damit sie eine gute Sicht hat. Kann man sicher noch besser machen.