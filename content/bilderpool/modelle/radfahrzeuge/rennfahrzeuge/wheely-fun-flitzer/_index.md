---
layout: "overview"
title: "Wheely-Fun-Flitzer"
date: 2020-02-22T07:56:49+01:00
legacy_id:
- /php/categories/2816
- /categoriesd23c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2816 --> 
Das Modell erreicht eine sehr hohe Geschwindigkeit und ist durch den hohen Schwerpunkt sehr witzig zu lenken: gibt man zuviel Gas, hebt sich die lenkbare Vorderachse vom Boden und das Modell wird unlenkbar :-) Im Rückwärtsgang wird es zusätzlich sehr wendig, dreht sich mitunter direkt um die Hinterachse. Das große Rad hinten sitzt direkt auf dem Motor und macht die hohe Geschwindigkeit möglich. Modell ist auf Robustheit getrimmt, nicht auf Optik. Es macht unseren Kids sehr viel Spaß, wir hatten mitunter zwei Modelle, welche sich dann im Auto-Scooter Duell aufeinander losgehen. FT at its best!

Viel Spaß beim Weiterbauen/Nachbauen!