---
layout: "image"
title: "ft Schneepflug (Basis 90239)"
date: "2012-02-04T23:46:01"
picture: "ft_Schneepflug.jpg"
weight: "6"
konstrukteure: 
- "ftDirk"
fotografen:
- "ftDirk"
keywords: ["Schneepflug", "90239"]
uploadBy: "ftDirk"
license: "unknown"
legacy_id:
- /php/details/34081
- /detailsfa5b.html
imported:
- "2019"
_4images_image_id: "34081"
_4images_cat_id: "122"
_4images_user_id: "1372"
_4images_image_date: "2012-02-04T23:46:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34081 -->
Wenn es um die Bekämpfung hoher Schneemengen geht, braucht man bullige Maschinen, wie diesen gigantischen ft Schneepflug. Der schafft was weg!