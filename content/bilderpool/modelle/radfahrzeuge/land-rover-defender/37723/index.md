---
layout: "image"
title: "IMG_9961.JPG"
date: "2013-10-19T16:05:15"
picture: "IMG_9961.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37723
- /details4a72.html
imported:
- "2019"
_4images_image_id: "37723"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37723 -->
Die Last auf der Hinterachse versucht, den quer liegenden Balken zu drehen. Dagegen musste mit einigen Versteifungen angegangen werden.