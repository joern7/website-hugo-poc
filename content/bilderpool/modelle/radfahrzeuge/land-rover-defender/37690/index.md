---
layout: "image"
title: "IMG_9193.JPG"
date: "2013-10-07T20:57:38"
picture: "IMG_9193mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37690
- /details304c.html
imported:
- "2019"
_4images_image_id: "37690"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T20:57:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37690 -->
Ohne die Vorderachse ist da wirklich nicht viel dran. Die Hinterachse ist gleich dreifach gefedert: über die schwarzen Spiralfedern, die längs liegenden Streben X-63, die von der quer liegenden X-106 nach oben gedrückt werden, und schließlich noch über die Flachträger 120, die den Boden der Fahrzeugzelle bilden.