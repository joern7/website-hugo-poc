---
layout: "image"
title: "IMG_9188.JPG"
date: "2013-10-07T20:41:48"
picture: "IMG_9188mit.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37687
- /detailsaf7d.html
imported:
- "2019"
_4images_image_id: "37687"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T20:41:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37687 -->
Unter der Haube geht es ziemlich geräumig zu. Die obere Etage mit IR-Empfänger und Motor hätte noch Platz für eine Seilwinde oder ähnliches. Die gelben Lochstreben sind gleichzeitig eine (eher mäßige) Federung.