---
layout: "image"
title: "Seilzüge"
date: "2014-10-02T16:38:42"
picture: "Pistenraupe04.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39496
- /details5a3b-2.html
imported:
- "2019"
_4images_image_id: "39496"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T16:38:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39496 -->
Die zwei Powermotoren 1:50 treiben je eine Seilwinde an. Motoren 1:20 gehen auch, aber meine beiden wurden in der Pistenraupe gebraucht.

Jedes Seil führt von der Trommel über die schwarzen längs liegenden Achsen (links und rechts am Ladebalken) auf die andere Seite hinüber, dort über die quer liegende Messingachse (die Seilrolle sorgt nur dafür, dass das Seil nicht weiter nach innen rutscht) wieder nach unten und zurück über dieselben schwarzen Achsen auf einen festen Punkt neben der Trommel. 

Die Anordnungen sind gespiegelt, weil der Ladebalken sofort auf eine Seite kippt, wenn die Trommeln auf der gleichen Seite montiert sind.