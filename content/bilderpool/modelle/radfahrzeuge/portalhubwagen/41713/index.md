---
layout: "image"
title: "von unten"
date: "2015-08-03T22:11:02"
picture: "straddlecarrier5.jpg"
weight: "21"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41713
- /detailsdcfc.html
imported:
- "2019"
_4images_image_id: "41713"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:11:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41713 -->
Ein Wimmelbild vom gesamten Fahrwerk. Die gelben U-Träger (rechts nur zu erahnen) sind Transportsicherungen.