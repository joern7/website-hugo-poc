---
layout: "comment"
hidden: true
title: "20980"
date: "2015-08-04T20:18:50"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die schwarzen Differenziale werden auf der einen Seite (hier rechts) mit einem nicht mehr ganz so exotischen Exoten festgehalten: die "BSB Spur N Grundplatte" 36093 gab es auf dem FanClub-Tag frisch aus der Spritzgussmaschine, zusammen mit dem ganzen Zug. 

Der Vollständigkeit halber sei noch gesagt: diese Grundplatte ist aber auch das einzige Teil der BSB-N, das mit dem restlichen ft kompatibel ist. Der Rest gibt nicht mal eine einzige Kaulquappe her.