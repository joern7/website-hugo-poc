---
layout: "image"
title: "Antrieb"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39471
- /detailsc45f.html
imported:
- "2019"
_4images_image_id: "39471"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39471 -->
Das Fahrzeug hat Allradantrieb. Jede Seite hat einen XM-Motor (in Bildmitte, aufrecht), der über das schwarze Differenzial die Antriebskräfte nach vorn und hinten verteilt. Von dort wird über die roten Differenziale auf die Räder 1+2 und 3+4 verteilt. Die Differenziale liegen horizontal, die Antriebsachsen aber vertikal. Dazwischen geschieht die 90°-Umlenkung über Kronenräder, die aus Z20 durch Umschlingen mit der schwarzen Kette entstehen.