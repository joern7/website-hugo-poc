---
layout: "comment"
hidden: true
title: "20981"
date: "2015-08-05T07:24:43"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
@Harald: Deine Ansicht bezüglich der Kompatibilität teile ich so nicht.

Die besagten Bauplatten haben so etwa 43mm x 22mm Aussenmaße. Passen also auch nur bedingt in die gewohnten Maße.

An vielen anderen Kleinteilchen des "Jubiläums-Zügle" sind auch Federn oder Zapfen dran. Also kompatibel sind die damit schon! Und für welche exotischen Zwecke sie sich eignen finden unsere KaulquappenzüchterInnen sicher noch raus. Bei den kleinen Pufferplatten fallen mir spontan so Stichworte wie "Endschalterbetätigung", und "Ösen für Seilzuganlenkungen" (vielleicht sogar für Flugzeugleitwerke) ein.

Grüße
H.A.R.R.Y.