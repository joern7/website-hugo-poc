---
layout: "comment"
hidden: true
title: "23197"
date: "2017-03-12T16:50:42"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Die Vorgehensweise nennt man auch "Nachfahren mit Schalthysterese" oder "Differenz-Steuerung". Simpel aber gut und bei vielen Steuerungen eingesetzt.
Cool. Schön gelöst.