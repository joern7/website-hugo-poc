---
layout: "image"
title: "panta rhei"
date: "2014-09-29T22:15:40"
picture: "portalhubwagen11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39477
- /details70f8.html
imported:
- "2019"
_4images_image_id: "39477"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:40"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39477 -->
"alles fließt" -- auch der Ladebalken darf in horizontaler Richtung hin und her baumeln. Dafür sorgt das Spiel in der Aufnahme des Blocks, der jetzt links daneben liegt: in den Nuten der BS30 haben die S-Riegel reichlich Spiel in Längsrichtung, währen die Metallachse in Querrichtung soviel Freiheit hat, wie der Innenraum zwischen den Fahrwerken zulässt.