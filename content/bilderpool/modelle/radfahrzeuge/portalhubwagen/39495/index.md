---
layout: "image"
title: "Kronenrad öffnen.jpg"
date: "2014-10-02T15:31:32"
picture: "IMG_1450mit.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39495
- /details0dd0.html
imported:
- "2019"
_4images_image_id: "39495"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T15:31:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39495 -->
Wie kriegt man so ein Kronenrad mit schwarzer Kette wieder auseinander?

1. mindestens an einem Kettenglied sollte die "Kralle", mit der der Steg im Nachbarglied umschlossen wird, nach innen zeigen.
2. an diesem Punkt setze ich die Spitze einer Pinzette von innen, ganz eng an der Fläche des Zahnrades, an, und drücke die Kralle nach außen.
3. entweder klickt an diesem Punkt das Kettenglied schon aus dem Nachbar heraus, oder man muss auf der anderen Seite noch ein bisschen nachhelfen. Dafür braucht man eigentlich drei Hände; mit etwas Geschick reichen zwei.