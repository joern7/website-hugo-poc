---
layout: "image"
title: "neuer Antrieb"
date: "2015-08-03T22:11:02"
picture: "straddlecarrier4.jpg"
weight: "20"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41712
- /detailseaba.html
imported:
- "2019"
_4images_image_id: "41712"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:11:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41712 -->
Der Antrieb verwendet jetzt eine umlaufende Kette (die untere), die alle Räder einer Seite gleich schnell antreibt. Sie laufen zwar nur paarweise (innnen und außen) auf dem gleichen Kreisbogen, aber davon merkt man nicht viel.

Die obere Kette ist nötig, um die Motorkraft auf mehreren Wegen auf die Antriebsachsen zu bekommen. Ursprünglich saß der Motor direkt auf der Achse des hintersten Z20. Dieses war aber (ohne Querbohren und Verstiften) nicht vom Durchrutschen abzubringen. 

In der Lenkung arbeitet auf jeder Seite ein Greifergetriebe von TST. Die Achsen darin haben einmal Links- und einmal Rechtsgewinde, so dass die zwei schwarzen BS15-Loch (natürlich auch mit Links- bzw. Rechtsgewinde) gleichzeitig zusammen oder auseinander gefahren werden.