---
layout: "image"
title: "Lenkung 2"
date: "2014-09-29T22:15:40"
picture: "portalhubwagen13.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39479
- /details0526.html
imported:
- "2019"
_4images_image_id: "39479"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:40"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39479 -->
Detailaufnahme: diese Lenkstrebe mit eingebautem Endanschlagstaster besorgt die Richtungsumkehr in der Lenkansteuerung zwischen den beiden hinteren und den beiden vordern Rädern auf einer Fahrzeugseite. Darunter ist der XM-Motor zu sehen, der die Räder der Fahrzeugseite antreibt.