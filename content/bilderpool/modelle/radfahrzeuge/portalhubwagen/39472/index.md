---
layout: "image"
title: "Containerschlösser"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39472
- /details27ea.html
imported:
- "2019"
_4images_image_id: "39472"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39472 -->
Der Container wird durch vier S-Riegel 8 gehalten. Hier sind zwei davon zu sehen: sie stecken in den gelben Winkelträgern 15, und ihre Griffe werden in Rastkupplungen gehalten. Die gelben Verbinder oben auf den Achsen zeigen die Stellung der S-Riegel an: stehen sie in Längsrichtung wie hier, sind die Schlösser offen.

Die rote Platte 90x180 und der U-Träger darüber gehören zur Transportsicherung und müssen vor Betrieb entfernt werden.