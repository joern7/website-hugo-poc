---
layout: "image"
title: "Gabel hoch"
date: "2007-04-06T15:10:45"
picture: "Gabelstapler_021.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Gabelstapler"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9994
- /detailsa869.html
imported:
- "2019"
_4images_image_id: "9994"
_4images_cat_id: "903"
_4images_user_id: "504"
_4images_image_date: "2007-04-06T15:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9994 -->
Das ist die höchste Position der Gabel.