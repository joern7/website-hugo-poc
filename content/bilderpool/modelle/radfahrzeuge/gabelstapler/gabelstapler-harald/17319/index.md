---
layout: "image"
title: "Stapler04.jpg"
date: "2009-02-05T21:28:10"
picture: "Stapler04.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17319
- /details22b1.html
imported:
- "2019"
_4images_image_id: "17319"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:28:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17319 -->
Die Unterseite vom Antrieb der beiden Schnecken.