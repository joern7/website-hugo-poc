---
layout: "image"
title: "Stapler10.jpg"
date: "2009-02-05T21:43:36"
picture: "Stapler10.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17322
- /details2596.html
imported:
- "2019"
_4images_image_id: "17322"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:43:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17322 -->
Der Teleskopschub ist am unteren Anschlag. So ganz befriedigend ist das noch nicht. Die Aufnahme sollte tiefer runter gehen und natürlich etwas stabiler sein als die Bauplatten 15x90.

Das Fahrwerk stammt vom "Explorer", http://www.ftcommunity.de/categories.php?cat_id=1555
. Da gibt es auch Detailfotos zum Antrieb.