---
layout: "image"
title: "Stapler07.jpg"
date: "2009-02-05T21:41:16"
picture: "Stapler07.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17321
- /details4a03.html
imported:
- "2019"
_4images_image_id: "17321"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:41:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17321 -->
Fast bis zum oberen Anschlag angehoben.