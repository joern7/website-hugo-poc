---
layout: "overview"
title: "Gabelstapler Franks Version"
date: 2020-02-22T07:53:26+01:00
legacy_id:
- /php/categories/1528
- /categoriesa3f4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1528 --> 
Nachbau von HLGR\'s Stapler mit 2 Mini-Motoren und einem Powermotor. Ferngesteuert mit dem neuen Control Set.