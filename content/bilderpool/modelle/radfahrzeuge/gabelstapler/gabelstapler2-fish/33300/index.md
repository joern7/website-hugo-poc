---
layout: "image"
title: "Gabelstapler vorne"
date: "2011-10-23T16:00:07"
picture: "gabelstaplerfish2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/33300
- /details0f45.html
imported:
- "2019"
_4images_image_id: "33300"
_4images_cat_id: "2464"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33300 -->
Die Gabel wird von einem Minimoter gehoben und gesenkt, welcher mit den beiden Endtastern gestoppt wird (über das Interface). Eine lange Metallstange dient als Führung und über die gleich lange Rastachse wird die Schnecke mit der verbunden ist.