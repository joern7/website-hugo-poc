---
layout: "overview"
title: "Gabelstapler2 (fish)"
date: 2020-02-22T07:53:30+01:00
legacy_id:
- /php/categories/2464
- /categories4d17.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2464 --> 
Ein normaler Gabelstapler mit Farbsensor, Endtastern für die Gabel und einem RoboInterface welches über Python vom PC aus gesteuert wird.