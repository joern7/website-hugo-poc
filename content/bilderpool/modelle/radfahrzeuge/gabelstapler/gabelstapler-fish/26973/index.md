---
layout: "image"
title: "Gabelstapler"
date: "2010-04-20T21:29:28"
picture: "gabelstapler1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/26973
- /details1d41.html
imported:
- "2019"
_4images_image_id: "26973"
_4images_cat_id: "1938"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26973 -->
Gabelstapler mit Palette.