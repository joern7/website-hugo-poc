---
layout: "image"
title: "Gabelstabler2"
date: "2005-05-15T17:28:52"
picture: "Gabelstabler_002.jpg"
weight: "2"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4145
- /detailsb404.html
imported:
- "2019"
_4images_image_id: "4145"
_4images_cat_id: "1556"
_4images_user_id: "332"
_4images_image_date: "2005-05-15T17:28:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4145 -->
Dies ist die Ansicht von  oben. Hier läst sich deutlich erkennen, dass der IR Empfänger links etwas überstecht. Dies lies sich nicht vermeiden, da der Empfänger vier Zapfen hat, die eine Nute des Motors aber ziemlich genau in der Mitte liegt.