---
layout: "image"
title: "Gabelstabler5"
date: "2005-05-15T17:48:16"
picture: "Gabelstabler_006.jpg"
weight: "5"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4148
- /details3d78.html
imported:
- "2019"
_4images_image_id: "4148"
_4images_cat_id: "1556"
_4images_user_id: "332"
_4images_image_date: "2005-05-15T17:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4148 -->
Die Frontansicht basiert überwiegend auf dem ft-Baukasten.