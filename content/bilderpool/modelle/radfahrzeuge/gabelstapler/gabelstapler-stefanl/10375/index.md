---
layout: "image"
title: "Gabelstapler 2"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10375
- /details12e4.html
imported:
- "2019"
_4images_image_id: "10375"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10375 -->
