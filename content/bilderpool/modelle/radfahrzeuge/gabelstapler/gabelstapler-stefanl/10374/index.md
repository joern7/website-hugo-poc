---
layout: "image"
title: "Gabelstapler 1"
date: "2007-05-12T15:08:45"
picture: "gabelstaplerstefanl01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10374
- /details53ef.html
imported:
- "2019"
_4images_image_id: "10374"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10374 -->
Der Anfang eines Gabelstaplers mit Teleskoparm.