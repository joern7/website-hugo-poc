---
layout: "image"
title: "Gabelstabler 15"
date: "2007-05-12T21:51:43"
picture: "gabelstabler1.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10388
- /details43b3-2.html
imported:
- "2019"
_4images_image_id: "10388"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T21:51:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10388 -->
