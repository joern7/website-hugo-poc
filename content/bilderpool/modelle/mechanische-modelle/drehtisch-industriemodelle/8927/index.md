---
layout: "image"
title: "Drehtisch Detail unten"
date: "2007-02-11T12:23:26"
picture: "drehtisch2.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/8927
- /details6d75.html
imported:
- "2019"
_4images_image_id: "8927"
_4images_cat_id: "810"
_4images_user_id: "1"
_4images_image_date: "2007-02-11T12:23:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8927 -->
