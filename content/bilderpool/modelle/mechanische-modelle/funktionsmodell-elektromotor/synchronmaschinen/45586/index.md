---
layout: "image"
title: "Synchronmotor mit 600 U/min aus drei Drehscheiben - Seite"
date: "2017-03-21T17:35:36"
picture: "Synchron600UproMin2.jpg"
weight: "7"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/45586
- /detailsef2b-2.html
imported:
- "2019"
_4images_image_id: "45586"
_4images_cat_id: "3374"
_4images_user_id: "1088"
_4images_image_date: "2017-03-21T17:35:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45586 -->
Hier die Seitenansicht, an der man gut erkennen kann, dass die drei Drehscheiben gegeneinander verdreht sind und dass in einigen der radialen Bohrungen Magneten stecken.

Empfohlen werden zur Sicherheit kräftige Gummiringe/-bänder auf den Drehscheiben! Nachbau auf EIGENE GEFAHR!