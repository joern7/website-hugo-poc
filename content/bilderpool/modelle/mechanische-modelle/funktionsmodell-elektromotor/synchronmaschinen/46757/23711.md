---
layout: "comment"
hidden: true
title: "23711"
date: "2017-10-19T15:48:14"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das hier inspirierte mich übrigens, so eine vier-Phasen-Uhr damit zu bauen. Prototyp läuft. Die Gummis gehen, sehr stramm, auch über ein Z30.

Gruß,
Stefan