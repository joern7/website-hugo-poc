---
layout: "image"
title: "10-poliger Synchronläufer"
date: "2017-10-11T16:59:09"
picture: "Synchronlufer_p5.jpg"
weight: "16"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Synchron", "Synchronmotor", "Synchronmaschine", "Polpaarzahl5", "10-polig"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46757
- /details5e4b.html
imported:
- "2019"
_4images_image_id: "46757"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-10-11T16:59:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46757 -->
Zwei Zahnräder Z20, 10 Stabmagnete mit wechselnder Polung und ein Gummiring vom Spurkranz ergeben den Läufer einer Synchronmaschine mit der Polpaarzahl 5. Sie läuft einwandfrei mit 600 Umdrehungen pro Minute.