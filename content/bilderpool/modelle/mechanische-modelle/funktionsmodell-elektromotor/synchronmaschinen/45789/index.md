---
layout: "image"
title: "10-polige Synchronmaschine, Einzelteile des Läufers"
date: "2017-04-25T16:54:26"
picture: "Einzelteile_des_Lufers.jpg"
weight: "10"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45789
- /detailsc7f6.html
imported:
- "2019"
_4images_image_id: "45789"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-04-25T16:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45789 -->
Die Streben des Läufers werden durch Klemmbuchsen und einen Mitnehmer (31712) kraftschlüssig mit der Achse verbunden.