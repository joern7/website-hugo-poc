---
layout: "image"
title: "10-polige Synchronmaschine"
date: "2017-04-25T16:54:26"
picture: "10-polige_Synchronmaschine.jpg"
weight: "12"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Synchronmaschine", "10-polig", "p=5"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45791
- /detailsd4a1.html
imported:
- "2019"
_4images_image_id: "45791"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-04-25T16:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45791 -->
Der Läufer besteht aus einem &#8222;fast-Kreis&#8220;, die Magnete sind mit wechselnder Polung von vorne und hinten in die Löcher der WS60 gesteckt (es sind also 20 Magnete) und halten durch die gegenseitige Anziehung. Das ist absolut zuverlässig! Die Synchrondrehzahl ist 600 U/min.