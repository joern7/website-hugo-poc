---
layout: "image"
title: "Synchronmotor mit 600 U/min aus drei Drehscheiben - oben"
date: "2017-03-21T17:35:36"
picture: "Synchron600UproMin4.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/45588
- /details0cc1.html
imported:
- "2019"
_4images_image_id: "45588"
_4images_cat_id: "3374"
_4images_user_id: "1088"
_4images_image_date: "2017-03-21T17:35:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45588 -->
Auf diesem Bild kann man gut erkennen, wie die Drehscheiben gegeneinander verdreht sind. Die Bohrungen/Nuten der rechten Drehscheibe liegen genau auf der Mitte zwischen den Bohrungen/Nuten der linken Drehscheibe. Die Drehscheibe in der Mitte wird durch eine der Nabenmuttern so fixiert, dass ihre Bohrungen in der Mitte zwischen einer Bohrung links und einer Bohrung rechts liegt. Dann werden die Magneten entsprechend der vorigen Abbildung gesteckt.

Empfohlen werden zur Sicherheit kräftige Gummiringe/-bänder auf den Drehscheiben! Nachbau auf EIGENE GEFAHR!