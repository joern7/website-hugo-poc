---
layout: "image"
title: "4-polige Synchronmaschine mit 'Trommelanker'"
date: "2017-10-11T16:59:09"
picture: "Trommelanker_p2.jpg"
weight: "17"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Synchron", "Synchronmotor", "Synchronmaschine", "4-polig", "Polpaarzahl2"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46758
- /details78a9.html
imported:
- "2019"
_4images_image_id: "46758"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-10-11T16:59:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46758 -->
Legt man 4 Stabmagnete mit wechselnder Polung in den BS15 ein, erhält man die Polpaarzahl 2. Die Maschine läuft mit dem alten, grauen Netzgerät an 6 V Wechselstrom mit 1500 Umdrehungen pro Minute und treibt dabei den Propeller an.