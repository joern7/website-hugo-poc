---
layout: "image"
title: "Dynamo in grau"
date: "2013-07-19T20:20:07"
picture: "dynamo800_2.jpg"
weight: "1"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: ["Dynamo", "vintage", "alt", "grau"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/37166
- /details393c.html
imported:
- "2019"
_4images_image_id: "37166"
_4images_cat_id: "2631"
_4images_user_id: "427"
_4images_image_date: "2013-07-19T20:20:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37166 -->
Ein Dynamomodell nach einer alten ft-Vorlage. Für die Magnete wurden 4 Relais geschlachtet. Ich hatte nicht so viele ft-Elektromagnete. Das Messgerät  dient als Indikator,
die 2 weissen LEDs 'just for fun'.