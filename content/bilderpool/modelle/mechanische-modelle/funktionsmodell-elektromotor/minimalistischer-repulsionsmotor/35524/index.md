---
layout: "image"
title: "Programm Drehzahlmessung"
date: "2012-09-16T20:52:47"
picture: "Programm_Drehzahlmessung.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35524
- /details4c18.html
imported:
- "2019"
_4images_image_id: "35524"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35524 -->
Hier mein kleines RoboPro-Programm zur Drehzahlmessung. Es eignet sich auch für die Drehzahlmessung von ft-Motoren.
Die Messung startet, wenn der Taster betätigt wird. Die Drehzahlanzeige bleibt bis zu einer erneuten Betätigung des Tasters stehen.