---
layout: "image"
title: "Motor im Betrieb"
date: "2012-09-09T20:58:19"
picture: "Dokumentation5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35476
- /details318e.html
imported:
- "2019"
_4images_image_id: "35476"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-09T20:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35476 -->
Die Geschwindigkeit des Motors ist bei sauberer Justierung (Abstand der Elektromotoren von den Permanentmagneten, Abstand des Tasters vom Schleifring, Winkel des Schleifrings zum Baustein 15) ganz beachtlich - das Drehmoment allerdings eher moderat. Er läuft auch nicht so flüsterleise wie die 50-Hz-Motoren von steffalk (http://www.ftcommunity.de/details.php?image_id=25286) oder geometer (http://www.ftcommunity.de/details.php?image_id=32765). Zur Veranschaulichung des Funktionsprinzips eines Elektromotors erscheint er mir aber ganz geeignet - und ist auch zügig aufgebaut.