---
layout: "image"
title: "Übersicht"
date: "2017-02-06T17:22:59"
picture: "smma1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45127
- /details6c8a.html
imported:
- "2019"
_4images_image_id: "45127"
_4images_cat_id: "3363"
_4images_user_id: "2228"
_4images_image_date: "2017-02-06T17:22:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45127 -->
50 Hertz Synchronmotor mit 6 Magnetpolpaaren, daraus ergibt sich eine Drehzahl von 500 Umdrehungen pro Minute

Der Rotor wird von einem Encodermotor auf die Zieldrehzahl beschleunigt. Diese wird vom TX Controller geregelt und angezeigt. Sobald diese erreicht wurde, kann man den Anlasser vom Rotor abkuppeln und der Synchronmotor läuft selbstständig. Ein externer Motor wird dann zum Anlassen eines Synchronmotors verwendet, wenn der Motor eher groß ist.

Ein Video des Motors und der Funktion des Anlassers gibt es hier: https://www.youtube.com/watch?v=dXnRAa9-eoo