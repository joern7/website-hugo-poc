---
layout: "image"
title: "Anlasser ausgekuppelt"
date: "2017-02-06T17:22:59"
picture: "smma6.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45132
- /details7f69.html
imported:
- "2019"
_4images_image_id: "45132"
_4images_cat_id: "3363"
_4images_user_id: "2228"
_4images_image_date: "2017-02-06T17:22:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45132 -->
Der Encodermotor lässt sich um etwa 7mm zurückschieben, sodass man ihn vom Rotor entkoppeln kann.