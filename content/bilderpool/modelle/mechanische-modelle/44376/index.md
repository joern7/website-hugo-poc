---
layout: "image"
title: "BILD 3 einphasiger Schrittmotor"
date: "2016-09-14T22:14:35"
picture: "BILD_3_einphasiger_Schrittmotor.jpg"
weight: "7"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Einphasiger", "Schrittmotor"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44376
- /details0439.html
imported:
- "2019"
_4images_image_id: "44376"
_4images_cat_id: "127"
_4images_user_id: "2635"
_4images_image_date: "2016-09-14T22:14:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44376 -->
Der Schrittmotor erweitert um ein Getriebe 1/12 mit "Sekundenzeiger"