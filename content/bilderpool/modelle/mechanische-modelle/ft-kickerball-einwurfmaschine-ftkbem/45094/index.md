---
layout: "image"
title: "Gesamtansicht"
date: "2017-01-29T14:06:53"
picture: "IMG_1078.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["ft", "Kickerball", "Einwurfmaschine", "(ftKBEM)"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45094
- /details701c.html
imported:
- "2019"
_4images_image_id: "45094"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45094 -->
die Maschine wurde zur Entwicklung auf einer Grundplatte aufgestellt.
Ein Kickerball wird mit dem Aufzug (minimot / Zahnstangenantrieb) nach oben gefahren. In der Endposition wird das Ventil betätigt und der Ball ins Spiel eingeworfen. Am oberen Ende ist die Maschine so konstruiert, dass diese direkt am Kickertisch eingehangen werden kann.