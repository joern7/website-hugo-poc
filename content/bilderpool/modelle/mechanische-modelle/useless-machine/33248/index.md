---
layout: "image"
title: "useless4"
date: "2011-10-19T16:55:27"
picture: "DSC01620.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33248
- /details76e9.html
imported:
- "2019"
_4images_image_id: "33248"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:55:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33248 -->
Hier seht ihr die gesamte Technik. Hier nochmal die Beschreibung: Der Arm ist mit einer Nockenscheibe verbunden, deren antrieb von einem Taster unterbrochen wird, betätigt man nun den Schalter wird der Taster überbrückt und der Arm kommt heraus macht den Schalter aus. Da der Taster nur dann betätigt wird, wenn der Arm innen ist, fährt der Arm wieder hinein.