---
layout: "image"
title: "useless5"
date: "2011-10-19T16:55:27"
picture: "DSC01621.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33249
- /details814f.html
imported:
- "2019"
_4images_image_id: "33249"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:55:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33249 -->
Hier seht Ihr die Statikstrebe, mit der der Arm mit der Nockenscheibe verbunden ist.