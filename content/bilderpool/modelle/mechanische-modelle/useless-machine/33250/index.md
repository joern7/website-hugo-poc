---
layout: "image"
title: "useless6"
date: "2011-10-19T16:55:27"
picture: "DSC01623.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33250
- /details3cc8.html
imported:
- "2019"
_4images_image_id: "33250"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:55:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33250 -->
So sieht es aus, wenn der Arm den Schalter umlegt.