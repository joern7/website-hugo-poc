---
layout: "image"
title: "Gespannt mit Flieger"
date: "2008-02-27T18:13:00"
picture: "papierfliegerabschussrampe6.jpg"
weight: "6"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/13802
- /details5ef5.html
imported:
- "2019"
_4images_image_id: "13802"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:13:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13802 -->
