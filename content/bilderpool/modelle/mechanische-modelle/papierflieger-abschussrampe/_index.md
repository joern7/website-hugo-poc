---
layout: "overview"
title: "Papierflieger-Abschußrampe"
date: 2020-02-22T08:18:18+01:00
legacy_id:
- /php/categories/1266
- /categories0f38.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1266 --> 
Nuja, eigentlich mein Einstand (wenn man den Hund ned mitzählt *G*)



Vorgabe war, die Mechanik auf relativ wenig Platz zu verwirklichen. Könnte man wohl noch besser machen.



Die Rampe verfügt über einen Schlitten (1 Kettenglied) der über eine Schnur und Gummizug gespannt wird.

Version 2.0 verfügt über eine drehbare und kippbare Rampe, dessen Drehpunkt genau über der Seilrolle liegt.



Seilspannung und Abschuß ist Programmgesteuert. Die Ausrichtung der Rampe über Joystick.



Je nach Fliegermodell sind 7 bis 8 Meter drin.



Versuche mit Alternativen laufen (evtl Kinder-Dartpfeil (die mit dem Klettverschluß *G*)