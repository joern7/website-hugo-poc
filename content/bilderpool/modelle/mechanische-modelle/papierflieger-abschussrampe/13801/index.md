---
layout: "image"
title: "Detail Spannmechanik im gespannten Zustand"
date: "2008-02-27T18:13:00"
picture: "papierfliegerabschussrampe5.jpg"
weight: "5"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/13801
- /details69d7.html
imported:
- "2019"
_4images_image_id: "13801"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:13:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13801 -->
oben sieht man das Kettenglied