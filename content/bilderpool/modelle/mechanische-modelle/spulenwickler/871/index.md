---
layout: "image"
title: "Spulenwickler 1"
date: "2003-04-27T17:23:18"
picture: "DSCF0006.jpg"
weight: "1"
konstrukteure: 
- "Süwütrütt"
fotografen:
- "Süwua"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/871
- /details9f5b.html
imported:
- "2019"
_4images_image_id: "871"
_4images_cat_id: "16"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:23:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=871 -->
