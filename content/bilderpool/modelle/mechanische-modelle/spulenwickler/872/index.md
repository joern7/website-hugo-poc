---
layout: "image"
title: "Spulenwickler 2"
date: "2003-04-27T17:23:44"
picture: "DSCF0007.jpg"
weight: "2"
konstrukteure: 
- "dsa"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/872
- /details8396.html
imported:
- "2019"
_4images_image_id: "872"
_4images_cat_id: "16"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:23:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=872 -->
