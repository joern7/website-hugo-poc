---
layout: "image"
title: "Funktionsweise (1)"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28760
- /details1af4.html
imported:
- "2019"
_4images_image_id: "28760"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28760 -->
So liegt ein Papierstapel im Fach - an jeder Ecke unterhalb der Eckknotenplatten (hinten) bzw. Laschen 45 (vorne).