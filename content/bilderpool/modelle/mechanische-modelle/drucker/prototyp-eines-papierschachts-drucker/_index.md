---
layout: "overview"
title: "Prototyp eines Papierschachts für einen Drucker"
date: 2020-02-22T08:20:09+01:00
legacy_id:
- /php/categories/2094
- /categories9009.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2094 --> 
Dies soll ein Papierfach für einen Drucker mit automatischer Einzelblattzufuhr werden. Dies ist die nachträgliche Dokumentation über den Prototypen, den ich auf der Convention 2010 dabei hatte.