---
layout: "image"
title: "Der Spiegel"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28774
- /details16b9.html
imported:
- "2019"
_4images_image_id: "28774"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28774 -->
Der verwendete Spiegel ist tatsächlich ein Original-fischertechnik-Teil aus den 1970er Jahren. Er kann mit seiner Unterseite auf Nuten geklipst werden, ähnlich der älteren Fassungen der Verkleidungsplatten. Solche Spiegel gab es in den Kästen le-1 (Lichtelektronik, 1969) und hobby-4.