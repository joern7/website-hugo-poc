---
layout: "overview"
title: "Prototyp einer Duplexeinheit für einen Drucker"
date: 2020-02-22T08:20:10+01:00
legacy_id:
- /php/categories/2095
- /categories2154.html
- /categoriesae6d.html
- /categoriesbe34.html
- /categories79f3-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2095 --> 
Für einen Drucker, der Vorder- und Rückseite bedrucken können soll: Dies ist die nachträgliche Dokumentation für das Modell, das ich auf der Convention 2010 dabei hatte.