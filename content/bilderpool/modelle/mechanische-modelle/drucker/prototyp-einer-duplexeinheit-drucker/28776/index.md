---
layout: "image"
title: "Der Dreiecksbalken (1)"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28776
- /detailsb3ab-2.html
imported:
- "2019"
_4images_image_id: "28776"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28776 -->
Dies ist nun das Herzstück der Einheit, ein dreieckiger Balken. Er wird nur außen an zwei Punkten festgehalten und darf ansonsten keinerlei Noppen oder Nuten haben, an denen sich das Papier verfangen könnte.