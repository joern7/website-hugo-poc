---
layout: "image"
title: "Gesamtansicht"
date: "2010-09-29T20:02:42"
picture: "prototypeinerduplexeinheitfuereinendrucker01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28767
- /detailsaad6.html
imported:
- "2019"
_4images_image_id: "28767"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28767 -->
Die Duplexeinheit ist in 120°-Schritten rotationssymmetrisch aufgebaut (bis auf die Lichtschranke und die Befestigung außen). Hier von vorne muss das Papier zugeführt werden, und zwar waagerecht zwischen die Reifen 30 und die darunterliegenden Vorstuferäder, so dass es zwischen die Führungsbleche gelangt.

Ein Video gibt es hier: http://www.youtube.com/watch?v=sSJGsg5Nnjk