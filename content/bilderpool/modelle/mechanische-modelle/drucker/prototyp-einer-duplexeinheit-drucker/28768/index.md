---
layout: "image"
title: "Symmetrie"
date: "2010-09-29T20:02:42"
picture: "prototypeinerduplexeinheitfuereinendrucker02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28768
- /detailse6d4.html
imported:
- "2019"
_4images_image_id: "28768"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28768 -->
Hier ein Blick von der Seite auf die Duplexeinheit. Die drei identischen Einheiten sind um das zentrale Element herum angeordnet: Ein durchgehender dreieckiger Balken, der das Papier in die jeweils benötigte Richtung umlenkt. Die Motoren laufen immer paarweise und werden jeweils umgepolt - je nach Lage des Papiers und gewünschter Bewegungsrichtung.