---
layout: "image"
title: "Lichtschranke (1)"
date: "2010-09-29T20:02:42"
picture: "prototypeinerduplexeinheitfuereinendrucker06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28772
- /details36c9.html
imported:
- "2019"
_4images_image_id: "28772"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28772 -->
Eine auf der Oberseite angebrachte Lampe strahlt durch zwei der Papierwege hindurch auf einen Spiegel auf der anderen Seite.