---
layout: "image"
title: "Lichtschranke (3)"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28775
- /details701a.html
imported:
- "2019"
_4images_image_id: "28775"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28775 -->
Das Gelbe ist die Fotozelle, auf der das Licht nach Durchgang aller drei Papierbahnen ankommt. Dadurch kann mit nur einer Lichtschranke für jede Bahn festgestellt werden, ob das Papier sich schon in der Einheit befindet und ob es schon weit genug heraus gezogen wurde (nämlich wenn die Lichtschranke frei wird und die Motoren ca. 0,6 s weitergelaufen sind), um wieder zurück geschoben zu werden.