---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:10"
picture: "stempelmaschine09.jpg"
weight: "9"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34253
- /detailseaf5.html
imported:
- "2019"
_4images_image_id: "34253"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34253 -->
Teile des Transportrollensystems.