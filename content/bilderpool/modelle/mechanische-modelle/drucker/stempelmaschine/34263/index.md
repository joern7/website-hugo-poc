---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine19.jpg"
weight: "19"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34263
- /detailsd8f7.html
imported:
- "2019"
_4images_image_id: "34263"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34263 -->
Die Steuerung der Stempelmechanik.