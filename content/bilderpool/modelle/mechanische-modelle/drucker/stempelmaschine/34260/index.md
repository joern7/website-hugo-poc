---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine16.jpg"
weight: "16"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34260
- /details70be-3.html
imported:
- "2019"
_4images_image_id: "34260"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34260 -->
Die Stempelmechanik in der Ausgangstellung.