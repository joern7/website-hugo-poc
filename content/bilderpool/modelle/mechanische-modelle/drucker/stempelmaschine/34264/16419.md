---
layout: "comment"
hidden: true
title: "16419"
date: "2012-02-19T11:42:52"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Elmar,
sind die blauen Fäden "Rutschhemmer"? Halten die schwarzen Räder sonst nicht fest genug auf der Metallachse?
Gruß, Dirk