---
layout: "image"
title: "Befestigung der Einzelbilder"
date: "2005-01-02T15:36:50"
picture: "Bildbefestigung.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3435
- /details8079.html
imported:
- "2019"
_4images_image_id: "3435"
_4images_cat_id: "318"
_4images_user_id: "103"
_4images_image_date: "2005-01-02T15:36:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3435 -->
Hier sieht man daß ich die Bilder einfach in die Nuten der Bausteine 30 eingeschoben habe.Einzelbilder kann man einfach selbst malen mit einer kleinen Bildfolge abhängig von der Größe des Rades.Ein Rad aufgebaut aus Winkelsteinen 7,5° würde Platz für 48 Bilder schaffen und somit ca.4 Sekunden Kino ermöglichen.