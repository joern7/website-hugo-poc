---
layout: "image"
title: "Durchsicht durch die Blende"
date: "2005-01-02T15:36:50"
picture: "DurchsichtBlende.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3434
- /details46e9-2.html
imported:
- "2019"
_4images_image_id: "3434"
_4images_cat_id: "318"
_4images_user_id: "103"
_4images_image_date: "2005-01-02T15:36:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3434 -->
Durch diesen Schlitz muß man das gegenüberliegende Bild bei Drehung de Trommel beobachten.Die Blende sollte möglichst schmal(max 5mm)gehalten werden da zu große Schlitze das Bild unscharf erscheinen lassen.In diesem Fall ist er wg. Winkelstein 30° leider zu groß.Evtl kann man die Bilder breiter schneiden.