---
layout: "image"
title: "Draufsicht"
date: "2005-01-02T15:36:50"
picture: "Draufsicht.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3433
- /detailsb285.html
imported:
- "2019"
_4images_image_id: "3433"
_4images_cat_id: "318"
_4images_user_id: "103"
_4images_image_date: "2005-01-02T15:36:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3433 -->
Pro Sekunde sollten nicht weniger als 10-15 Bilder durchlaufen was bei dieser Trommel eine Drehzahl von ca.1U/sec ergibt.