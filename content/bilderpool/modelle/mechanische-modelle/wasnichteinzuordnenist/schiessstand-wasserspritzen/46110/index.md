---
layout: "image"
title: "Grundstellung"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/46110
- /detailsadff.html
imported:
- "2019"
_4images_image_id: "46110"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46110 -->
Die Fallziele sind aufgerichtet und einsatzbereit. Man ziele mit dem Wasserstrahl auf die roten Bauplatten. Entfernung: Je nach Wasserspeier - ausprobieren.