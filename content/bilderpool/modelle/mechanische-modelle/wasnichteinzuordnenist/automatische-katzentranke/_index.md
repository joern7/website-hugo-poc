---
layout: "overview"
title: "Automatische Katzentränke"
date: 2020-05-09T17:24:24+02:00
---

Eine unserer zwei verrückten Katzen trinkt nur fließendes Wasser: Sie besteht darauf, ins Waschbecken gehoben zu werden, um dann ihren Kopf unters leicht fließende Wasser zu halten und das über ihre Nase herunterlaufende Wasser aufzuschlabbern. Stehendes Wasser straft sie mit Verachtung. Da sie das aber auch 2 x nachts will, musste eine Lösung her!