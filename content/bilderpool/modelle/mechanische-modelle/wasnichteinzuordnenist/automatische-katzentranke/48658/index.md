---
layout: "image"
title: "Die Pumpstation"
date: 2020-05-09T17:24:26+02:00
picture: "2020-05-05 Automatische Katzentränke2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Damit das Pumpgeräusch die Katze nicht vertreibt, steht die Steuereinheit um die Ecke.

Die Katze hat die neue Tränke sofort angenommen und kapiert. Hach, was haben wir ne schlaue Katze *hust*.