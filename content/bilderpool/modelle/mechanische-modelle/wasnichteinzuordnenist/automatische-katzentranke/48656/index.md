---
layout: "image"
title: "Die Elektronik"
date: 2020-05-09T17:24:24+02:00
picture: "2020-05-05 Automatische Katzentränke4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier habe ich endlich mal meine vor längerer Zeit gebraucht erworbene 80er-Jahre Elektronik eingesetzt. Von links nach rechts sind die vier Module:

- Spannungsversorgung (liefert geglättete 5 V für die ICs).

- Schwellwertschalter (am oberen Poti kann man die Empfindlichkeit der Lichtschranke einstellen)

- Monoflop (ein IC 74141; realisiert zusammen mit dem 470-µF-Kondensator und dem 39-kΩ-Widerstand die 20 - 30 Sekunden Laufzeit der Pumpe)

- Leistungsstufe (die obere der beiden treibt die Pumpe).

Der IC-Baustein (der dritte) stammt aus dem Kasten "IC Digital-Praktikum", die anderen aus dem Kasten "Elektronik".