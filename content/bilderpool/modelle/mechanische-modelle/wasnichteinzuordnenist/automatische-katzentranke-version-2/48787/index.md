---
layout: "image"
title: "Elektronik"
date: 2020-07-09T12:54:37+02:00
picture: "Katzentraenke2_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die gleichen Elektronikbausteine in gleicher Schaltung wie bei Version 1 (die das Wasser selbst pumpte) stehen hier auf Stelzen. Die Elektronik steuert hier das fischertechnik-Netzschaltgerät dahinter (deshalb läuft die Anlage nur mit 6 V Spannung). Im Vordergrund neben der Elektronik sieht man die LED, im Hintergrund gerade noch die Fotozelle der Lichtschranke.