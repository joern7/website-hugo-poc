---
layout: "image"
title: "Netzschaltgerät"
date: 2020-07-09T12:54:35+02:00
picture: "Katzentraenke2_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Diese Gerät hatte ich vor einigen Jahren gebraucht erstanden, und es funktionierte meiner Erinnerung nach auch beim Test. Heuer musste ich allerdings feststellen, dass es immer Strom liefert und nicht nur wenn es sollte. Das muss ich noch untersuchen. Version 3 wird dann wohl die gekaufte Katzentränke mit einem gut 10 € kostenden kleinen Bewegungsmelder werden...