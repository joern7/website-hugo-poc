---
layout: "image"
title: "Das Modell in Anwendung"
date: 2020-07-09T12:54:33+02:00
picture: "Katzentraenke2_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der Grundplatte steht nun eine gekaufte Katzentränke, die sehr leise läuft und Filter enthält, um Kalk, Katzenhaare und -speichel zu filtern. Die versauten mein pur-fischertechnik-Modell in Version 1 im Dauereinsatz über Wochen nämlich ganz gehörig. Man sieht das Netzteil der Katzentränke im Netzschaltgerät stecken.