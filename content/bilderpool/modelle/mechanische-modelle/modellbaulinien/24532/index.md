---
layout: "image"
title: "Rührmaschine in Eighties-Classic-Line"
date: "2009-07-09T17:19:13"
picture: "IMG_1395b.jpg"
weight: "11"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24532
- /details5864-2.html
imported:
- "2019"
_4images_image_id: "24532"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T17:19:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24532 -->
Rühr- bzw. Küchenmaschine in Eighties-Classic-Line.

Um auf die in dieser Epoche fehlenden Rastteile verzichten zu können, musste das komplette Rührwerk angepasst werden.