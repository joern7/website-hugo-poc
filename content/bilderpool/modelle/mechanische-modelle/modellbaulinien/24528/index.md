---
layout: "image"
title: "Balkenwaage in Freestyle-Line"
date: "2009-07-09T16:07:53"
picture: "IMG_1360b.jpg"
weight: "7"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24528
- /detailsdbd8.html
imported:
- "2019"
_4images_image_id: "24528"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T16:07:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24528 -->
In der Freestyle-Line dürfen alle Bauteile aller Generationen und Farben parallel eingesetzt werden.