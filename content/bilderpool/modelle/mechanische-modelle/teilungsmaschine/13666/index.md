---
layout: "image"
title: "Etwas näher ran..."
date: "2008-02-17T15:39:32"
picture: "02-Details.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13666
- /details56c6.html
imported:
- "2019"
_4images_image_id: "13666"
_4images_cat_id: "1256"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13666 -->
Im Vordergrund der Schneidschlitten. Die Frässpindel ist mit einem dünnen Sägeblatt ausgerüstet, welches mit hoher Drehzahl läuft. Die Spindel ist einfach mit Gummis festgezurrt, etwas Gummi untergelegt dämpft lästige Schwingungen. Geschoben wird der Schlitten von dem Exzenterantrieb rechts dahinter.

Das Seil an der Spindel hat die Aufgabe, das Getriebe, das die Spindel dreht, stets mit einem Drehmoment gespannt zu halten. Dadurch wird der Antrieb spielfrei und es gibt kein Gewackel im Winkel.

Der Winkelvorschub wird mit einem Inkrementalgeber gemessen. Dieser hat 1000 Striche Auflösung, so daß das RoboPro-Interface via Egde-#any-Befehl (Thkais-Basic) 4000 Flanken je Umdrehung zählt. Das löst sehr schön auf, die Spindel darf sich dann aber nur langsam drehen.

Wie man oben noch an dem grauen Kabel erkennen kann, ist der Drehaufnehmer direkt mit dem Interface verbunden.