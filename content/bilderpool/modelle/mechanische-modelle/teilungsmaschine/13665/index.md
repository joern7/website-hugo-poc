---
layout: "image"
title: "Übersicht Teilungsmaschine"
date: "2008-02-17T15:39:31"
picture: "01-Gesamtmodell.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13665
- /detailsea0f.html
imported:
- "2019"
_4images_image_id: "13665"
_4images_cat_id: "1256"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13665 -->
Die Maschine besteht aus den Baugruppen Spindel, Drehmomentgeber, Schneidschlitten und Steuerung

Sie dient dazu, Pappscheiben um exakte Winkelstücke zu drehen und dann einzuschneiden. So entsteht eine Teilscheibe, wie sie für Gabellichtschranken gebraucht wird.

Es ist mir halt nicht gelungen, solche Scheiben von Hand relativ sauber auszuschneiden, also muß eine Maschine ran.