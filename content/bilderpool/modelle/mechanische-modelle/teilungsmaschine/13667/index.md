---
layout: "image"
title: "Rückansicht"
date: "2008-02-17T15:39:32"
picture: "03-Rckansicht.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13667
- /detailse091.html
imported:
- "2019"
_4images_image_id: "13667"
_4images_cat_id: "1256"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13667 -->
Hier besser zu sehen, wie das Seil mittels Gewicht über einen Flaschenzug gespannt wird. Ich habe es viermal eingeschirrt, damit das Gewicht nicht so viel Weg macht und ich den Turm dann hätte so hoch bauen müssen.