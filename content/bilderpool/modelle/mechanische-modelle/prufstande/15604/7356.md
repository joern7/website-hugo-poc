---
layout: "comment"
hidden: true
title: "7356"
date: "2008-09-29T22:33:56"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Mit dieser Welle bin ich heute trotz bestehender Skepsis mit dem Power Motor 8:1
auf 1008 U/min gekommen. 
Die Konfiguration dazu: RoboPro-Steuerprogramm, Netzteil 9V unstab. mit Robo Interface, Steuerscheiben-Impulszähler, Laufzeit 1 min.
Eine spezifische Präparation der Welle um  Drehzahlspitzenwerte wurde hierzu nicht durchgeführt.
Mit einer externen Spannungsversorgung von 12V überschreitet der Motor 1.200 U/min.
Gruss, Udo2