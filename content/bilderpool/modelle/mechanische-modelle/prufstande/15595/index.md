---
layout: "image"
title: "Drehimpuls-Prüfstand (1/11)"
date: "2008-09-25T17:47:39"
picture: "pruefstanddrehimpulse01.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15595
- /details48ab.html
imported:
- "2019"
_4images_image_id: "15595"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15595 -->
Vorweg:
Eine Veröffentlichung zum Modell war noch nicht vorgesehen. Es sind auch zunächst noch einfache Anordnungen und Programme auf dem Weg der Modellkonfiguration "Vom Niederen zum Höheren". Im Fanclub-Forum gibt es aber zum Thema "Drehzahl- und Impulsmessung" nur mit verbaler Kommunikation Schwierigkeiten in der Verständigung. Also ist das hier mal dazu eine öffentliche Hilfestellung.
Modellthematik:
Ich wollte für die Konfigurationen meiner Modellvorhaben mal etwas über die Drehzahlbereiche der ft-Motoren und die verlässlichen Funktionsbereiche der aktuellen ft-Impulsbausteine aus eigener Erkenntnis erfahren. Da ich aber keine Vergleichsnormale hatte, baute ich mir diesen mechanischen Prüfstand. Der verliert wenigstens keine Drehzahlimpulse oder dichtet welche hinzu. So gesehen hat er eine Toleranz von Null.
Link ins fischertechnik-fanclubforum:
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3314