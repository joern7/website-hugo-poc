---
layout: "image"
title: "Drehimpuls-Prüfstand (5/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse05.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15599
- /detailsa761.html
imported:
- "2019"
_4images_image_id: "15599"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15599 -->
Lichtschranke:
Vor dem mechanischen Prüfstand war dies meine Hoffnung auf einfache Mess-Lösungen. Aber eigentlich weiss man ja, dass die Anordnung einer Drehscheibe 60 mit ein bis sechs beklebten Radialschlitzen bei höheren Drehzahlen wegen der sich verkürzenden Impulsdauer ohne spezifische Elektronik und an Universalanschlüssen am Interface der Realität bald hinterherläuft. So war es dann auch. Ich bekam sogar in bestimmmten Bereichen unter LLWin mehr Drehzahlen angezeigt, als da gelaufen waren. Also musste der mechanische Drehzahlzähler als Vergleichsmormal her. So näherte ich mich dieser Inkrementalöffnung, einem um fast das doppelte verlängerter Kreisbogenschlitz der Drehscheibe 60. Mit dieser Konfiguration zählt die ft-Lichtschranke ohne mechanische Übersetzung 1:1 zunächst bis an 1000U/min beprobt fehlerfrei die Drehzahlen.
Mit einer Fremdspannungsversorgung des schnellsten ft-Motors Power-Motor 8:1 ausserhalb von Programm und Interface wird man sehen wie weit abhängig von der max. Motordrehzahl die Lichtschranke da noch mitmacht.