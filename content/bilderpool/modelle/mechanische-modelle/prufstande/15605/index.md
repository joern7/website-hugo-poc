---
layout: "image"
title: "Drehimpuls-Prüfstand (11/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse11.jpg"
weight: "11"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15605
- /details2ac5.html
imported:
- "2019"
_4images_image_id: "15605"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15605 -->
Impulsbaugruppen:
Hier noch abschliesssend von links nach rechts von den Impuls-Baugruppen "Taster", "Lichtschranke" und "Impulsrad" rot eingefärbt die Geber und grün die Empfänger. Die Störlichtkappen der Lichtschranke sind ausgeblendet. Der Empänger der Lichtschranke ist hier der ft-Fototransistor.

So einfach kann das sein, wenn man alles immer nur sofort gleich wüsste ...