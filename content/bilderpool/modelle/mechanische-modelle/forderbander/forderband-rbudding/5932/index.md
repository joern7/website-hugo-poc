---
layout: "image"
title: "part5-2"
date: "2006-03-26T14:48:45"
picture: "fischertechnik_019.jpg"
weight: "10"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/5932
- /details2623-4.html
imported:
- "2019"
_4images_image_id: "5932"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5932 -->
