---
layout: "image"
title: "part4"
date: "2006-03-26T14:48:45"
picture: "fischertechnik_015.jpg"
weight: "7"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/5929
- /details575f.html
imported:
- "2019"
_4images_image_id: "5929"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5929 -->
