---
layout: "image"
title: "part6-motor"
date: "2006-03-26T14:53:33"
picture: "fischertechnik_026.jpg"
weight: "11"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/5933
- /detailsb67c.html
imported:
- "2019"
_4images_image_id: "5933"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5933 -->
