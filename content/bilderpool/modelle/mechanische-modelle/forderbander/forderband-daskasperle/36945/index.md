---
layout: "image"
title: "Förderband von DasKasperle - VORNE DETAILE 2"
date: "2013-05-26T09:50:17"
picture: "foerderbandvondaskasperle09.jpg"
weight: "9"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36945
- /detailsf129.html
imported:
- "2019"
_4images_image_id: "36945"
_4images_cat_id: "2746"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36945 -->
