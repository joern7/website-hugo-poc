---
layout: "comment"
hidden: true
title: "8174"
date: "2009-01-07T00:30:49"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Da fehlt noch die Zentriereinrichtung. Bisher bleiben Werkstücke, die leicht daneben liegen, am Baustein 7,5 und an der Bauplatte 15*90 hängen.