---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband04.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16907
- /details85d3.html
imported:
- "2019"
_4images_image_id: "16907"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16907 -->
