---
layout: "image"
title: "Förderband"
date: "2009-01-07T15:12:39"
picture: "foerderband05_2.jpg"
weight: "18"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16934
- /details18f5-5.html
imported:
- "2019"
_4images_image_id: "16934"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-07T15:12:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16934 -->
Hier sieht man den Baustein der auf den Endtaster drückt (Es geht auch ohne Schnitzereien - auf Staudingermodell guck).