---
layout: "image"
title: "Förderband"
date: "2009-01-07T15:12:39"
picture: "foerderband08_2.jpg"
weight: "21"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16937
- /detailsae71.html
imported:
- "2019"
_4images_image_id: "16937"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-07T15:12:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16937 -->
Als Sensor habe ich einen CNY70 ausgwählt.
Der Vorteil: Dafür muss nur an einer Seite des Förderbands der Sensor angebaut werden.
Bei einer Lichtschrank müsste man von der einen Seite den Sensor, an der anderen Seite die Lampe.
Ach arbeiten die CNY70 zuverlässiger!