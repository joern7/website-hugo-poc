---
layout: "image"
title: "FB11_01.JPG"
date: "2005-08-12T14:48:44"
picture: "FB11_01.jpg"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4589
- /details7a0a.html
imported:
- "2019"
_4images_image_id: "4589"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4589 -->
