---
layout: "image"
title: "FoeBa7_03.JPG"
date: "2004-11-15T17:29:21"
picture: "FoeBa7_03.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3168
- /detailsc562.html
imported:
- "2019"
_4images_image_id: "3168"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3168 -->
Die Kufenteile sind wichtig, da die schwarze Trommel etwas konisch ist und die Gummibänder sonst dauernd zur einen Seite hin rutschen. Die Achse ist kein Original-ft, sondern ein Kunststoff-Schweißdraht (wie er beim Verlegen von Linoleumplatten verwendet wird) mit etwas mehr als 4mm Stärke, der in der Bohrung der Walze gut klemmt.