---
layout: "image"
title: "FB10_03.JPG"
date: "2005-08-12T14:48:44"
picture: "FB10_03.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4588
- /detailsa425.html
imported:
- "2019"
_4images_image_id: "4588"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4588 -->
Das Rastzahnrad Z10 wurde auf der Drehmaschine bearbeitet, um den Schaft auf das Innenmaß des Reedkontakthalters 35969 zu bringen.