---
layout: "image"
title: "FB09_01.JPG"
date: "2005-08-12T14:48:44"
picture: "FB09_01.jpg"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4583
- /details86af.html
imported:
- "2019"
_4images_image_id: "4583"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4583 -->
