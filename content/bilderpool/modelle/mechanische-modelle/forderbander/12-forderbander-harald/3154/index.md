---
layout: "image"
title: "FoeBa2_03.JPG"
date: "2004-11-15T17:29:03"
picture: "FoeBa2_03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3154
- /details49da-2.html
imported:
- "2019"
_4images_image_id: "3154"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3154 -->
