---
layout: "image"
title: "FoeBa7_01.JPG"
date: "2004-11-15T17:29:21"
picture: "FoeBa7_01.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3166
- /detailsa8ed.html
imported:
- "2019"
_4images_image_id: "3166"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3166 -->
