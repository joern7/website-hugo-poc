---
layout: "image"
title: "Transportband mit Bearbeitungsstation"
date: "2010-06-25T18:20:41"
picture: "CIMG0325.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Ihrig"
fotografen:
- "Jürgen Ihrig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "juergen669"
license: "unknown"
legacy_id:
- /php/details/27570
- /details11a2.html
imported:
- "2019"
_4images_image_id: "27570"
_4images_cat_id: "278"
_4images_user_id: "1158"
_4images_image_date: "2010-06-25T18:20:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27570 -->
