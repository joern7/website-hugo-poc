---
layout: "image"
title: "Blick aufs Wasserrad"
date: 2020-04-28T17:24:39+02:00
picture: "2020-04-26 Brunnen-Wasserrad-Fahrrad3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Rad ist möglicherweise effizienter als das meiner älteren Brunnen-Wasserrad-Kugelbahn. Es dreht sehr schnell und treibt den Fahrradfahrer ganz schon zum Strampeln an. :-)