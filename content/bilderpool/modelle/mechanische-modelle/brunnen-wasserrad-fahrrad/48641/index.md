---
layout: "image"
title: "Gesamtansicht"
date: 2020-04-28T17:24:41+02:00
picture: "2020-04-26 Brunnen-Wasserrad-Fahrrad1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das Fahrrad ist an die tolle Vorlage von Rüdiger Riedel im fischertechnik-Forum angelehnt. Im gesamten Modell ist kein einziger normaler Baustein und auch sonst kein Teil mit Metall enthalten, das ja rosten könnte. Alles sind pure Kunststoffteile.

Das Wasserrad treibt die Pedale und die Beine des Fahrradfahrers an. Die Räder stehen allerdings in diesem Modell still.

Ein Video gibt es unter https://youtu.be/Rqmr_jfhVRc.