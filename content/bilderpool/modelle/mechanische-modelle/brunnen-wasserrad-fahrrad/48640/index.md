---
layout: "image"
title: "Blick aufs Fahrrad"
date: 2020-04-28T17:24:40+02:00
picture: "2020-04-26 Brunnen-Wasserrad-Fahrrad2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine stecken über die roten Klemmstifte 15 in den Löchern der Rändelräder der alten Seiltrommeln und werden auch von solchen über die Kardangelenke angetrieben.