---
layout: "image"
title: "Großdistanz"
date: "2004-11-21T13:37:35"
picture: "Grodistanz.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3305
- /details90d9.html
imported:
- "2019"
_4images_image_id: "3305"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3305 -->
1,5 m sind noch nicht viel. Aber es ist deutlich zu sehen, wie die Maschine trotz gleichbleibender Motorspannung bei leicht unterschiedlichen Drehzahlen unterschiedliche Abwurfwinkel einsteuert.