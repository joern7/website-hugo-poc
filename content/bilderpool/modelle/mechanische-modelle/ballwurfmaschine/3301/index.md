---
layout: "image"
title: "Projektil"
date: "2004-11-21T13:37:35"
picture: "Projektil.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Magnet", "Kugel", "Schwungrad"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3301
- /details6486.html
imported:
- "2019"
_4images_image_id: "3301"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3301 -->
Am Außenrand des Schwungrads hält der Elektromagnet die Kugel fest.