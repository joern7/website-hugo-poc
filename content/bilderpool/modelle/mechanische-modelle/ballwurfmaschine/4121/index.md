---
layout: "image"
title: "Wurfmaschine Motoranordnung"
date: "2005-05-10T23:27:34"
picture: "Motoranordnung.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kettenantrieb"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4121
- /details0345-2.html
imported:
- "2019"
_4images_image_id: "4121"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4121 -->
Die Kette wurde erheblich gekürzt und der Motor in das Schwungrad hineingenommen. Das Ritzle ragt tatsächlich in den Kranz. Der Antrieb ist dadurch sehr viel steifer geworden. Die Leistung des Motors ist sehr reichlich bemessen. Bei 12 Volt nimmt er 250 mA Strom und dreht das Schwungrad mit 860 U/min. Mehr habe ich mich dann doch noch nicht getraut. Ausgelegt ist der Motor für 24 Volt, 2 A.