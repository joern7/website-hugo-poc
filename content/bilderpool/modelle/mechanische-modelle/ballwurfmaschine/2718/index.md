---
layout: "image"
title: "Schleifring"
date: "2004-10-16T18:51:55"
picture: "06-Schleifring.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schleifring", "Elektromagnet", "Freilaufdiode"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2718
- /detailsc2e2.html
imported:
- "2019"
_4images_image_id: "2718"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2718 -->
Hier wird die Spannung an den mitrotierenden Elektromagnet übergeben. Ebenso erkennbar ist die Freilaufdiode.