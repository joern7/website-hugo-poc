---
layout: "image"
title: "Antrieb Drehteller"
date: "2004-05-31T19:50:19"
picture: "H2-08-Antrieb_Drehteller.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2438
- /detailscfbd.html
imported:
- "2019"
_4images_image_id: "2438"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2438 -->
Zwar läuft der Drehteller, ihm fehlt aber noch die notwendige Sensorik zum Nachmessen des Drehwinkels.