---
layout: "image"
title: "Steuerrechner"
date: "2004-05-31T19:50:20"
picture: "H2-18-Rechner.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2451
- /details05b2-2.html
imported:
- "2019"
_4images_image_id: "2451"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2451 -->
Der kleine Rechner mit 486er CPU muß mit seinen bescheidenen 66 MHz alles machen. Die grünen Kabel sind die 6 Taktbits, die gelben die 6 Richtungsbits. Blau ist Masseanschluß und Rot ist +5V für die Steuerelektronik.