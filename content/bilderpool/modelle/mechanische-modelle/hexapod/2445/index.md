---
layout: "image"
title: "Schrittmotorsteuerung1"
date: "2004-05-31T19:50:19"
picture: "H2-16-Schrittmotorsteuerung1.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2445
- /details95b3.html
imported:
- "2019"
_4images_image_id: "2445"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2445 -->
Die neue Schrittmotorsteuerung in der Baugröße 55 x 40 mm kann Voll-, Halb-, Viertel- und Achtelschrittbetrieb bis 500 mA ohne Kühlkörper. Daneben das IC von Allegro A3967SLB.