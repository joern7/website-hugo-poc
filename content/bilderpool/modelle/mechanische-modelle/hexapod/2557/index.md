---
layout: "image"
title: "Layout 6-fach Treiberbaustein"
date: "2004-08-19T11:01:54"
picture: "Layout_6fach-Treiber.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schrittmotor", "Treiberkarte"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2557
- /detailsee7f.html
imported:
- "2019"
_4images_image_id: "2557"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-08-19T11:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2557 -->
Für das Hexapod entsteht ein 6-fach Treiberbaustein, der auf eine halbe Europakarte soll. Ansteuerung über Parallelport, Halb-, Viertel- und Achtelschrittbetrieb über Jumper wählbar.