---
layout: "image"
title: "Pneumatisches Tier 7"
date: "2010-04-27T12:02:09"
picture: "P1080562_-_Kopie.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27005
- /details1a62.html
imported:
- "2019"
_4images_image_id: "27005"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-27T12:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27005 -->
Ein ziehmlicher Schlauchsalat. Auch wenn ich versucht habe, die Schläuche möglichst ordentlich zu verlegen :-)