---
layout: "image"
title: "Pneumatisches Tier 5"
date: "2010-04-27T12:02:09"
picture: "P1080551_-_Kopie.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27003
- /details1d37-2.html
imported:
- "2019"
_4images_image_id: "27003"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-27T12:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27003 -->
Hier die Steuerung, leider noch nicht elektrisch. Ich habe auch zurzeit keine längeren Schläuche, deshalb müssen die Ventile so dicht am Tier sein