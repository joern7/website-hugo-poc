---
layout: "image"
title: "Pneumatisches Tier 4"
date: "2010-04-27T12:02:08"
picture: "P1080549_-_Kopie.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27002
- /details0c18.html
imported:
- "2019"
_4images_image_id: "27002"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-27T12:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27002 -->
Ein Standart Fischertechnik Kompressor, zurzeit nicht vollständig. Er steht auf einem kleinen Wagen