---
layout: "image"
title: "Laufmaschine05"
date: "2009-01-31T00:06:29"
picture: "laufmaschinealienimfruehstadium5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17211
- /details2211.html
imported:
- "2019"
_4images_image_id: "17211"
_4images_cat_id: "1543"
_4images_user_id: "729"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17211 -->
Nahaufnahme der "Alien"-Beine :-)