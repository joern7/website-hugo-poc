---
layout: "image"
title: "Von oben"
date: "2007-09-13T15:41:07"
picture: "huumi1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11459
- /details5562.html
imported:
- "2019"
_4images_image_id: "11459"
_4images_cat_id: "1032"
_4images_user_id: "445"
_4images_image_date: "2007-09-13T15:41:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11459 -->
Gesammtansicht der Neukonstruktion.