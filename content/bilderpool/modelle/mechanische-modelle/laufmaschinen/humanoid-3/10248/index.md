---
layout: "image"
title: "Humanoid, fast fertig"
date: "2007-04-30T18:47:48"
picture: "DSC08035k.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10248
- /detailsf45d.html
imported:
- "2019"
_4images_image_id: "10248"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10248 -->
Die Arme sind noch nicht ganz fertig, und die Füsse etwas zu klein.