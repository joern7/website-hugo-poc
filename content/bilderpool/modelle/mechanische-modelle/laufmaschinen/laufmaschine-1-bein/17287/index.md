---
layout: "image"
title: "lm01"
date: "2009-02-03T00:59:29"
picture: "laufmaschinebein1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17287
- /detailsc928.html
imported:
- "2019"
_4images_image_id: "17287"
_4images_cat_id: "1550"
_4images_user_id: "729"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17287 -->
Seitenansicht auf ein Bein