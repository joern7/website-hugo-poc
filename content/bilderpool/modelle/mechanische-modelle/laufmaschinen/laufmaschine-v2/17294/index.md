---
layout: "image"
title: "LMv2-04"
date: "2009-02-03T00:59:29"
picture: "laufmaschinev4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17294
- /detailsc6d7.html
imported:
- "2019"
_4images_image_id: "17294"
_4images_cat_id: "1551"
_4images_user_id: "729"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17294 -->
Ansicht von rechts hinten