---
layout: "image"
title: "LMv2-05"
date: "2009-02-03T00:59:29"
picture: "laufmaschinev5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17295
- /details47b3.html
imported:
- "2019"
_4images_image_id: "17295"
_4images_cat_id: "1551"
_4images_user_id: "729"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17295 -->
Ansicht auf die Kurbelwelle