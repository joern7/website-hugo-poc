---
layout: "image"
title: "Gesamtansicht"
date: "2017-06-19T19:47:06"
picture: "spinne01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45972
- /details52f5.html
imported:
- "2019"
_4images_image_id: "45972"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45972 -->
Diese Spinne https://www.ftcommunity.de/details.php?image_id=41588 vom ft-Jubiläum 2015 und Peters Nachbau in https://www.ftcommunity.de/categories.php?cat_id=3401 versuchten mit, das ohne verbogene Achsen, nur mit purem ft hinzubekommen. Es gab einen Prototyp mit PowerMotoren, aber der war zu schwer. Diese Variante mit S-Motoren und Stromversorgung von außen funktioniert und läuft sogar, wenn auch nur langsam.

Die Spinne ist eher zu breit als zu lang - wo die grünen Lampen sind, ist vorne.