---
layout: "image"
title: "Zu schwerer Prototyp mit PowerMotoren (3)"
date: "2017-06-19T19:47:06"
picture: "spinne12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45983
- /details4e1b.html
imported:
- "2019"
_4images_image_id: "45983"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45983 -->
Der schwere Akku trug auch zum Scheitern bei.