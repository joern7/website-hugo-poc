---
layout: "image"
title: "Zu schwerer Prototyp mit PowerMotoren (4)"
date: "2017-06-19T19:47:06"
picture: "spinne13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45984
- /details36bd-2.html
imported:
- "2019"
_4images_image_id: "45984"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45984 -->
Ein detailblick auf die schräge Motorbefestigung.