---
layout: "image"
title: "Detailblick auf die Kinematik"
date: "2017-06-19T19:47:06"
picture: "spinne06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45977
- /detailsd518-2.html
imported:
- "2019"
_4images_image_id: "45977"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45977 -->
Das hier sollte zu einem eventuellen Nachbau genügen. Ein Video folgt noch.