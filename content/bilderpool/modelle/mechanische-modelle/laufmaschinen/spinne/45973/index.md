---
layout: "image"
title: "Detailblick auf den Antrieb von oben"
date: "2017-06-19T19:47:06"
picture: "spinne02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45973
- /detailsc7eb.html
imported:
- "2019"
_4images_image_id: "45973"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45973 -->
Die S-Motoren genügen tatsächlich, wenngleich man natürlich keine Wunder erwarten darf: Die Spinne hat's nicht so mit Eile, eher mit ner gaaanz ruhigen Kugel.