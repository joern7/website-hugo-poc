---
layout: "image"
title: "MickyW"
date: "2014-09-14T20:24:06"
picture: "Multiplizierer_Variante_1_Bild_1_publish.jpg"
weight: "1"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Multplizierer", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39361
- /detailsa7bd.html
imported:
- "2019"
_4images_image_id: "39361"
_4images_cat_id: "2950"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T20:24:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39361 -->
Die erste Version eines Multiplizierers.

Die orthogonalen Kärfte auf die Geradführungen machen geringe Gleitreibung und stabile Bauweise so gut wie zwingend erforderlich. Man beachte die Achsen in den vertikalen Pfosten an den vier Ecken und die Abstützung. (Der Kenner wird auch die Papierstreifchen bemerken).

Nachteilig ist bei dieser Konstruktion, dass die Rechenbereiche begrenzt sind und die Multilpikation nicht modulo erfolgt.

Ich werde dann noch ein wenig Erklärung dazufügen. Aber kurz erwähnen kann ich, dass diese Mimik auf dem Prinzip der "ähnlichen Dreiecke" beruht.