---
layout: "image"
title: "Sinus Variante 2"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_2_Bild_1_publish.jpg"
weight: "2"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39350
- /details3842.html
imported:
- "2019"
_4images_image_id: "39350"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39350 -->
Der Versuch, dass Ganze etwas tiefer zu legen, damit die Kräfte auf die Achse nicht so groß werden und die Gleitführung so zu gestalten, dass die Kraft näher an der Linie der Bewegung angreift (was das verkannten vermindern sollte).