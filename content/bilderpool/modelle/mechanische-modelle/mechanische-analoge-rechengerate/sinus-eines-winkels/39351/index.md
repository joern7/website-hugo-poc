---
layout: "image"
title: "Sinus Variante 2"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_2_Bild_2_publish.jpg"
weight: "3"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39351
- /details2abd.html
imported:
- "2019"
_4images_image_id: "39351"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39351 -->
Eine Seitenansicht zeigt, dass die Verbindung schräg zu vertikalen Achse verläuft. Nicht so schön.