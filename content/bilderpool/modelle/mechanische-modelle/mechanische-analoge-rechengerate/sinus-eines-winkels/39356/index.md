---
layout: "image"
title: "Sinus Variante 8 - Bessere Abstützung"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_8_Bild_1_publish.jpg"
weight: "8"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Geradführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39356
- /detailse144-2.html
imported:
- "2019"
_4images_image_id: "39356"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39356 -->
Ich überspringe mal ein paar Varianten, die nur "Vorstudien" zu dieser waren.
Hier ein roter Lochstein und die horizontale Achse deutlich abgestützt. Denn die Kräfte haben das Gestell doch deutlich verbogen, so daß die Kräfte noch schiefer an die Geradführung angriffen.