---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe - Prinzipskizze"
date: "2014-03-02T18:43:51"
picture: "SkizzeAddiererRaederkurbelgetriebe.jpg"
weight: "7"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38408
- /detailsc264-2.html
imported:
- "2019"
_4images_image_id: "38408"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38408 -->
Hier nun einige grundsätzliche Erläuterungen:

Zunächst müssen die Strecken a und c sowie die Strecken b und d paarweise gleich lang sein.

Das Verhältnis der Radien r1 und r2 hingegen kann beliebig gewähtl werden. Es wirkt sich allerdings auf die Skalierung aus, wie bei dem Bild der Skala angemerkt.

Dreht man nun den mit y gekennzeichneten Zeiger um einen gewissen Winkel phi und hält x fest, so dreht sich der Summenzeiger z um den Winkel phi * r2 / r1.

Dreht man hingegen den mit x gekennzeichneten Zeiger um einen Winkel phi und hält y fest, so dreht sich der Summenzeiger um den Winkel phi * (1 + r2 / r1). Dieses letztere Verhältnis gibt auch die oben schon erwähnte Skalierung von x im Verhältnis zu z (resp. y) an.