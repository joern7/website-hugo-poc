---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe (vorletzte Baustufe - noch zwei vorher)"
date: "2014-03-02T18:43:51"
picture: "AddiererRaedkurbelgetriebeVorletzteBaustufe.jpg"
weight: "3"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38404
- /details5908-2.html
imported:
- "2019"
_4images_image_id: "38404"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38404 -->
Das sind alle Elemente die nun zum Endprodukt zusammengesetzt werden, indem zunächst das linke Teil auf die rechte Achse gesteckt wird, so dass der "Anzeiger" (die 120er Achse) nach oben (im Bild) zeigt. Anschliessend wird der kürzere der beiden Schenkel durch die oberen beiden Löcher der Bausteine gesteckt. Dann das verbleibende Teil durch die beiden unteren Löcher der Bausteine. Voilà.