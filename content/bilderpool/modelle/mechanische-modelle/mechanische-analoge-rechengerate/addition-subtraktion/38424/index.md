---
layout: "image"
title: "Addierer/Subtrahierer mit Differential (31043)"
date: "2014-03-03T11:03:35"
picture: "AddiererVariante1_Bild_e_Variables_bearbeitet.jpg"
weight: "8"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38424
- /details9f90.html
imported:
- "2019"
_4images_image_id: "38424"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38424 -->
Es ist Euch zwar wahrscheinlich schon längst bekannt, aber man kann auch mit einem Differential addieren bzw. subtrahieren.

Im Bild sind a und der Umlaufradträger die Eingänge und c der Ausgang (wobei man c und a tauschen kann).

Ihr seht, ein Untersetzungsgetriebe 2:1 am Umlaufradträger, da der zu der Welle c (oder auch a) eine Übersetzungsverhältnis von 1 zu 2 hat. Damit wird dann die Summe c = -a + b (bei angenommenem gleichem Drehsinn bei gleichen Vorzeichen) gebildet.
Umgekehrt kann man natürlich auch die Differenz -a = c - b ablesen.

Als vorteilhaft empfinde ich, dass das sich alle Wellen ohne Anschlag drehen können und so eine Addition (Subtraktion) modulo durchführen.