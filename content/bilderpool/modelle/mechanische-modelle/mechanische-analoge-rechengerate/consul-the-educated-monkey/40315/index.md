---
layout: "image"
title: "Consul, The Educated Monkey - fischertechnik Designer"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey8.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40315
- /detailsd2bb.html
imported:
- "2019"
_4images_image_id: "40315"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40315 -->
Hier die Konstruktion im fischertechnik-Designer; die Datei findet ihr im [Download-Bereich](https://www.ftcommunity.de/data/downloads/ftdesignerdateien/educatedmonkey01.ftm) der ft:c.