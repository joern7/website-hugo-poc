---
layout: "image"
title: "Consul, The Educated Monkey - die 'Füße' zur Einstellung der Faktoren"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40310
- /detailse7a4-2.html
imported:
- "2019"
_4images_image_id: "40310"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40310 -->
Mit den "Füßen" des Rechenaffen werden die Faktoren der Multiplikation eingestellt.
Verwendet habe ich dafür Gelenkwürfel-Klauen (31436) mit Lagerhülse (36819), die auf einer Metallachse 260 (107436) verschoben werden.