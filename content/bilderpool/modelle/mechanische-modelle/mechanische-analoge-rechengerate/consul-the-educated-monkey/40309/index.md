---
layout: "image"
title: "Consul, The Educated Monkey - die 'Hände' zur Anzeige des Multiplikationsergebnisses"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40309
- /detailsebd1.html
imported:
- "2019"
_4images_image_id: "40309"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40309 -->
Die "Hände" des Rechenaffen zeigen das Ergebnis der Multiplikation mit einer Metallachse 30 (alternativ: Kunststoffachse 30) in einer Gelenkklaue an. 
Zwei Klemmbuchsen 5 verhindern das Herausrutschen.