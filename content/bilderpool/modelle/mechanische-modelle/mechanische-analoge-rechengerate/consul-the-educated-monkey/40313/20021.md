---
layout: "comment"
hidden: true
title: "20021"
date: "2015-01-12T11:21:17"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo,
ein ganz tolles Teil!
Ich möchte das nachbauen für meinen Sohn.....allerdings ist er erst 4 1/2 Jahre alt.
Er kann aber schon die Zahlen bis 20 (und auch lesen) und versucht sich schon an seinen ersten Kopfrechnungen....allerdings aufgrund des Alters nur Addieren.
Wäre es möglich, daß Du auch eine Tabelle für die Addition machst? 
Meinem Kleinen könnte ich damit sowohl den Spaß am Rechnen sowie den Spaß an fischertechnik vermitteln.