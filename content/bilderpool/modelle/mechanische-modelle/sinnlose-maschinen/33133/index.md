---
layout: "image"
title: "Deckel offen"
date: "2011-10-12T19:38:24"
picture: "sinnlosemaschineftfan08.jpg"
weight: "8"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33133
- /detailsb480.html
imported:
- "2019"
_4images_image_id: "33133"
_4images_cat_id: "2449"
_4images_user_id: "1371"
_4images_image_date: "2011-10-12T19:38:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33133 -->
Hier seht man in die Maschine oben das Interface, Rechts der Motor, inder Mitte der Arm, welcher den Hebel wieder auf die Stellung OFF zurückdrückt.