---
layout: "image"
title: "Hebel ohne Abdeckung"
date: "2011-10-12T19:38:24"
picture: "sinnlosemaschineftfan06.jpg"
weight: "6"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33131
- /details24d9.html
imported:
- "2019"
_4images_image_id: "33131"
_4images_cat_id: "2449"
_4images_user_id: "1371"
_4images_image_date: "2011-10-12T19:38:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33131 -->
Hier in OFF-Stellung
Die Lichtschranke meldet die Stellung an das Interface 1= OFF   0=ON