---
layout: "image"
title: "Der Taster für den Summer"
date: "2011-10-12T19:38:24"
picture: "sinnlosemaschineftfan04.jpg"
weight: "4"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33129
- /details5dc0.html
imported:
- "2019"
_4images_image_id: "33129"
_4images_cat_id: "2449"
_4images_user_id: "1371"
_4images_image_date: "2011-10-12T19:38:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33129 -->
Der Motor dreht den Stab => der Summer beginnt zu Quäcken