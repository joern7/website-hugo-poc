---
layout: "image"
title: "Schreiben eines Bits (3)"
date: "2009-03-28T16:15:47"
picture: "ram7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23532
- /detailsf67b-2.html
imported:
- "2019"
_4images_image_id: "23532"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23532 -->
Der Wedel ist wieder in seiner Endlage oben angekommen, der Motor muss dann wieder stehen. Der Baustein ist gekippt.

Das Setzen eines Bits in die andere Richtung bedingt nur die Drehung des Wedels in die andere Drehrichtung.

Sollte ein Bit bereits den gewünschten Zustand haben, wird durch eine solche Operation nichts verändert. Damit kann man also jedes Bit zuverlässig in den gewünschten Zustand bringen, egal welchen Zustand es vorher hatte.

Das Auslesen eines Bits wird durch die Lichtschranke gemacht, die jetzt vom nach links gekippten BS15 nämlich unterbrochen wurde.