---
layout: "image"
title: "Auslesen eines Bits (1)"
date: "2009-03-28T16:15:47"
picture: "ram8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23533
- /details05c7.html
imported:
- "2019"
_4images_image_id: "23533"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23533 -->
Das durchsichtige Teil ist ein Lichtleitstab aus dem alten Elektronikprogramm von fischertechnik. Den gab es z. B. im le-1 von 1969 und im hobby-4. Das von der Lampe kommende Licht fällt in die Fläche am Ende des Lichtleitstabes ein und wird durch Totalreflexion nahezu verlustfrei im Stab gehalten, bis es schließlich im Fotowiderstand mit einer entsprechenden 4-mm-Störlichtkappe ankommt. Das ermöglicht eine recht kompakte Bauweise.

Ist der aktuell adressierte Baustein 15 nach hinten gekippt, erhält der Lichtleitstab und damit der Fotowiderstand Licht, ist er nach vorne gekippt, wird die Lichtschranke unterbrochen.