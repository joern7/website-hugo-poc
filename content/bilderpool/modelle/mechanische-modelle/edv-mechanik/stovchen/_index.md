---
layout: "overview"
title: "Stövchen"
date: 2020-02-22T08:20:20+01:00
legacy_id:
- /php/categories/2824
- /categories7bbc.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2824 --> 
Fischertechnik Stövchen
Mechanik und Elektrik von Fischertechnik. Die Sensoren sind von Tinkerforge.
Die Temperaturregelung wird von einer Singleton Bean in einem JEE Container (Glassfish 4) übernommen: Wenn das Objekt vor dem „Temperature IR Bricklet“ die Solltemperatur erreicht hat wird das Teelicht zur Pos. „kühlen“ gefahren. Wenn die Solltemperatur unterschritten wird wird das Teelicht zur Pos. heizen gefahren.Ich wollte einfach mal die Kombination JEE, Fischertechnik und Tinkerforge Bricks ausprobieren