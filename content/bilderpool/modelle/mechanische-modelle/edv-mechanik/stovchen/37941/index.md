---
layout: "image"
title: "Stövchen von oben"
date: "2013-12-26T15:23:58"
picture: "stoecvchenVonOben_x800.jpg"
weight: "1"
konstrukteure: 
- "axel"
fotografen:
- "axel"
keywords: ["Motorsteuerung", "Brick", "Bricklet"]
uploadBy: "axel"
license: "unknown"
legacy_id:
- /php/details/37941
- /detailsfc5f.html
imported:
- "2019"
_4images_image_id: "37941"
_4images_cat_id: "2824"
_4images_user_id: "2056"
_4images_image_date: "2013-12-26T15:23:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37941 -->
Das Stövchen von oben. Hier ist die verwendete Elektronik gut zu sehen (von links): Digital In Bricklet, 2 Dual Relais zur Motorsteuerung und die Master Brick. Der Temperatursensor ist hier nicht zu sehen.