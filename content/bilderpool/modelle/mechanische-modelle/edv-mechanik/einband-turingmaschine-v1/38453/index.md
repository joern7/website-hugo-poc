---
layout: "image"
title: "Leseeinheit"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38453
- /details9f71.html
imported:
- "2019"
_4images_image_id: "38453"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38453 -->
Leseeinheit = Fotowiderstand.