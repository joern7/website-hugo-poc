---
layout: "image"
title: "Schreibschlitten von Hinten"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38452
- /details54f0.html
imported:
- "2019"
_4images_image_id: "38452"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38452 -->
Schreibschlitten von Hinten mit Motor (Schubstange ist gelb)