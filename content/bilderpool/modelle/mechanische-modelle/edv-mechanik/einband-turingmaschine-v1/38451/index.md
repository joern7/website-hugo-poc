---
layout: "image"
title: "Schreibmechanik"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38451
- /detailsd45b.html
imported:
- "2019"
_4images_image_id: "38451"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38451 -->
Schreibmechanik
Die beiden Rastadapter kippen die BS30 nach vorn oder hinten. {0,1} (hier im Bild rechts / links)
Die Rastadapter sitzen auf einem Schlitten (unten das schimmernde Alu) der vor und zurück bewegt wird.
Dank an Stefan Falk für die Idee mit den umklappenden BS. Link: http://ftcommunity.de/details.php?image_id=23531 - hatte das vorher mit Gelenksteinen versucht.
