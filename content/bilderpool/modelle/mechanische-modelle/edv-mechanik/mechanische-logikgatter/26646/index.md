---
layout: "image"
title: "Baustufe"
date: "2010-03-07T10:57:04"
picture: "Baustufe2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26646
- /detailsf57a.html
imported:
- "2019"
_4images_image_id: "26646"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T10:57:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26646 -->
Hier eine Zwischenbaustufe.