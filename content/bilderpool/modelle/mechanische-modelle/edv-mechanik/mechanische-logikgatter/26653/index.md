---
layout: "image"
title: "XOR aus drei NAND-Bausteinen 2"
date: "2010-03-07T11:12:24"
picture: "XOR01.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26653
- /detailsc9cf.html
imported:
- "2019"
_4images_image_id: "26653"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T11:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26653 -->
Hier ist A=0, B=1 und die Mechanik bildet A XOR B = 1.