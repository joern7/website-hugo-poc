---
layout: "image"
title: "XOR aus drei NAND-Bausteinen"
date: "2010-03-07T11:12:24"
picture: "XOR00.jpg"
weight: "8"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26652
- /detailsdee7-2.html
imported:
- "2019"
_4images_image_id: "26652"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T11:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26652 -->
Hier sind A=B=0 und die Mechanik bildet A XOR B = 0.