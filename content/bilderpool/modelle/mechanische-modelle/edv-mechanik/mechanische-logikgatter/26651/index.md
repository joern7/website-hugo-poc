---
layout: "image"
title: "3-fach UND"
date: "2010-03-07T10:57:05"
picture: "UND3.jpg"
weight: "7"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26651
- /detailse6a6.html
imported:
- "2019"
_4images_image_id: "26651"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T10:57:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26651 -->
Ein 3-fach UND aus zwei NAND-Grundbausteinen.