---
layout: "image"
title: "Siebensegmentanzeige - Anzeige von der Seite"
date: "2004-04-05T18:47:54"
picture: "22_-_Schaltwalze_3.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2337
- /detailsdbcc-2.html
imported:
- "2019"
_4images_image_id: "2337"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2337 -->
Gegenüber dem ersten Bild um 180 Grad  nach hinten gedreht (sorry, natürlich nicht %, wie im vorhergehenden Bild geschrieben).