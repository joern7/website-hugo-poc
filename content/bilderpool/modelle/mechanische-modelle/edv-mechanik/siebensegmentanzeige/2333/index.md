---
layout: "image"
title: "Siebensegmentanzeige - Anzeige von oben"
date: "2004-04-05T18:47:45"
picture: "18_-_Anzeige_von_oben.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2333
- /details7902.html
imported:
- "2019"
_4images_image_id: "2333"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2333 -->
