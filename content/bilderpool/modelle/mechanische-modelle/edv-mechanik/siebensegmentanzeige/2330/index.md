---
layout: "image"
title: "Siebensegmentanzeige - Ziffer d"
date: "2004-04-05T18:47:45"
picture: "15_-_ZifferD.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2330
- /details7dd6-2.html
imported:
- "2019"
_4images_image_id: "2330"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2330 -->
