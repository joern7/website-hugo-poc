---
layout: "image"
title: "Antrieb"
date: "2007-02-10T15:13:34"
picture: "lochkartenleser4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8899
- /detailsd69f.html
imported:
- "2019"
_4images_image_id: "8899"
_4images_cat_id: "807"
_4images_user_id: "453"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8899 -->
Hier sieht man den Antrieb.