---
layout: "image"
title: "Overzicht"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel11.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38355
- /details5283.html
imported:
- "2019"
_4images_image_id: "38355"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38355 -->
Afhankelijk van de instel-positie en/of het gekozen RoboPro-Programma van de Fin-Ray Lachtspiegel wordt aan de ene zijde de bolling en aan de andere zijde de holling van de spiegel groter, kleiner of omgekeerd.