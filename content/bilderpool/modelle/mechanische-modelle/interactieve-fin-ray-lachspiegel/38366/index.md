---
layout: "image"
title: "Interactieve Fin-Ray Lachspiegel"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel22.jpg"
weight: "22"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38366
- /detailsf174-2.html
imported:
- "2019"
_4images_image_id: "38366"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38366 -->
Voor m'n lachspiegel heb ik 2 spiegelplaten van 0,5 x 1m middels Fischertechnik I-spanten op regelmatige afstanden aan elkaar verbonden ten behoeve van het Fin-Ray-principe. 

Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + Polystyreen-Gold-Spiegel-Platten. 
http://www.kunststofshop.nl/index.php?item=&action=page&group_id=10000046&lang=NL 

http://www.kunststofshop.nl/index.php?action=home&lang=NL 




Afhankelijk van de instel-positie en/of het gekozen RoboPro-Programma van de Fin-Ray Lachtspiegel wordt aan de ene zijde de bolling en aan de andere zijde de holling van de spiegel groter, kleiner of omgekeerd.  

Youtube-link: 
http://www.youtube.com/watch?v=hk6pu6vlWG8&list=UUjudqZ8_PVUpZRRLsSIJCPA&feature=share 



Pijlstaartrog met Fin-Ray-principe : 
http://www.ftcommunity.de/categories.php?cat_id=2825 

http://www.youtube.com/watch?v=m1UJYAVVplU&feature=youtu.be&a

Adaptive Gripper met Fin-Ray-principe : 
http://www.ftcommunity.de/categories.php?cat_id=2775 

Festo - MultiChoiceGripper
Er kombiniert paralleles und zentrisches Greifen ohne aufwendigen Umbau. Seine adaptiven Finger mit Fin Ray®-Stuktur passen sich flexibel an die unterschiedlichsten Formen an.
https://www.youtube.com/watch?v=u4ZScJsaepg&feature=em-uploademail-ctrl

Fischertechnik-Qualle mit Fin-Ray-Effect : 
http://www.ftcommunity.de/categories.php?cat_id=2772