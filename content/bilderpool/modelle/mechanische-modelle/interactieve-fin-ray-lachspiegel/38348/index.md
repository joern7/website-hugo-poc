---
layout: "image"
title: "Interactief bewegende flexibele wanden"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38348
- /details15ea.html
imported:
- "2019"
_4images_image_id: "38348"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38348 -->
In 2009 waren bij de TU-Delft afd. Bouwkunde  door Festo onderstaande 7 interactief bewegende flexibele wanden achter elkaar  opgesteld.   
Bewegend ziet dat er heel mooi uit :  https://www.youtube.com/watch?v=PVz2LIxrdKc


