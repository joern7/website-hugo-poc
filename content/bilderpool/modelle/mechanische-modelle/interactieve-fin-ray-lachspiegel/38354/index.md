---
layout: "image"
title: "Rob van Baal kijkt of een en ander goed funktioneert...."
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38354
- /details9e9f.html
imported:
- "2019"
_4images_image_id: "38354"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38354 -->
De maximum uitslag (= amplitude) heb ik begrenst door I1 alleen te accepteren tussen 2500 en 3500.  De middenpositie heeft een I1-waarde van 3000.