---
layout: "image"
title: "Spiegeltje, spiegeltje aan de wand, wie is het schoonste van heel het land ?...."
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel13.jpg"
weight: "13"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38357
- /details33ee.html
imported:
- "2019"
_4images_image_id: "38357"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38357 -->
Bolle spiegel......