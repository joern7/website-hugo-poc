---
layout: "image"
title: "Fin-Ray Lachspiegel -Handinstelling met potmeter"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel20.jpg"
weight: "20"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38364
- /detailsfff9.html
imported:
- "2019"
_4images_image_id: "38364"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38364 -->
Een vaste instel-positie van de Fin-Ray Lachtspiegel geeft aan de ene zijde een vaste bolling en aan de andere zijde een vaste holle spiegeling.  
Middels de 5 K-Potmeter  (I2) kan de bolling worden ingesteld. 
De draai-arm-positie tussen de spiegels beweegt dan net zolang tot de Potmeter-weerstand-waarde I1 aan de draai-arm gelijk is aan de gewenste ingestelde I2 weerstand-waarde.
De maximum uitslag (= amplitude) heb ik begrenst door I1 alleen te accepteren tussen 2500 en 3500.  De middenpositie heeft een I1-waarde van 3000.
