---
layout: "image"
title: "Containergreifer 01"
date: "2008-07-26T16:23:12"
picture: "containergreifer01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14947
- /details7a35.html
imported:
- "2019"
_4images_image_id: "14947"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14947 -->
Ich habe mal wieder versucht einen Containergreifer zu bauen.
Ziel war es die Standardcontainergröße von ft zu greifen (leider hatte ich nicht
so viele blaue Statikplatten ---> rote Platten) die dann von einem Hafenkran
/ Doppellenker versetzt werden. 
Die Funktionen des Greifers werden anhand der nächsten Bilder erklärt.
