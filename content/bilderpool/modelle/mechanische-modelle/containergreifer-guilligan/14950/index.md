---
layout: "image"
title: "Containergreifer 04"
date: "2008-07-26T16:23:13"
picture: "containergreifer04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14950
- /details7cb7.html
imported:
- "2019"
_4images_image_id: "14950"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14950 -->
Detail der Verriegelungsmechanik