---
layout: "image"
title: "Containergreifer 05"
date: "2008-07-26T16:23:13"
picture: "containergreifer05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14951
- /detailsf21b.html
imported:
- "2019"
_4images_image_id: "14951"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14951 -->
Hier kann man die Funktionsweise besser erkennen. 
Ich habe einen Mini Motor mit einem vorgeschalteten Schnecke nocheinmal untersetzt und
gehe dann mit Hilfe eines Zahnrades auf das eigentlich Getriebe. Durch die Drehbewegung
greifen Metallstifte in die Öffnungen der Statikplatte und die Box kann verfahren werden.