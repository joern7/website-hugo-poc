---
layout: "image"
title: "'Abstauber'"
date: "2007-09-09T15:10:00"
picture: "d_bilder_004.jpg"
weight: "7"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/11445
- /details553b-2.html
imported:
- "2019"
_4images_image_id: "11445"
_4images_cat_id: "1031"
_4images_user_id: "637"
_4images_image_date: "2007-09-09T15:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11445 -->
Der Drehteller