---
layout: "image"
title: "'schlampige' Verkabelung :( von - oben -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded23.jpg"
weight: "23"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42198
- /detailsc2a6.html
imported:
- "2019"
_4images_image_id: "42198"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42198 -->
