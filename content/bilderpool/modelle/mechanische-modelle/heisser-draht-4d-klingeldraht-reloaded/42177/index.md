---
layout: "image"
title: ".... ersten Heißen Draht in 4D !  :)"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded02.jpg"
weight: "2"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42177
- /details3467.html
imported:
- "2019"
_4images_image_id: "42177"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42177 -->
