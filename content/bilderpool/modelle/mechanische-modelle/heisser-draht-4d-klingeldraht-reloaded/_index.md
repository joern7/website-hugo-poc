---
layout: "overview"
title: "Heißer Draht 4D - Klingeldraht ReLoaded"
date: 2020-02-22T08:20:55+01:00
legacy_id:
- /php/categories/3145
- /categoriesc910.html
- /categoriesc779.html
- /categories5a14.html
- /categoriesedcb.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3145 --> 
Der Heiße Draht bzw. Klingeldraht neu aufgelegt.
https://www.youtube.com/watch?v=u2X5_4yOIuc

Im Ursprungsspiel ist der Kontaktdraht (KD) NICHT bewegt.
Beim der Erweiterung der Spielidee bewegt sich der KD in verschiedenen Geschwindigkeiten und es gibt Richtung´s Änderungen
(Standard/Level1=Stillstand; Level 2=Langsam; Level3=Schnell; Level4=CrazyHorse[Verschiedene Geschwindigkeiten und Richtungswechsel] ). Level4 ist ein Hiddenpart. *lach&freu

Am Anfang des Spiels wählt man den Schwierigkeitsgrad durch drücken der entsprechenden Taste. Der gewählte Level wird durch das entsprechende Licht angezeigt. Sobald die Anzeige erloschen ist,  kann man das Spiel durch schließen des Stromkreises mittels Handstück und der Startkontaktfläche(Erster Alu-Abschnitt auf dem KD ) starten. (Je nach Level bewegt sich jetzt der KD)
Zu Beginn des Spiels hat man drei Leben. (angedeutet durch drei Lampen)
Bei jedem Kontakt zwischen Handstück und KD (Schließen des Stromkreislaufs), wird einem ein Leben abgezogen und es erklingt ein Summer als akustisches Signal.
Sind alle drei Leben verbraucht endet das Spiel und ein Audiomodul gibt eine "bedauernde & ermunternde" Ansage aus.
Erreicht man die ZielKontaktfläche vor dem Verbrauch aller Leben erkling eine Beglückwünschung. Hat sich der KD während des Spiels gedreht, wird er jetzt für die nächste Runde wieder in Ausgangs Situation gedreht.

Der Schwierigkeitsgrad kann durch die Form und Aufbau des KD und die Schlingengröße des Handstücks variiert werden.

Im Laufe der Zeit korrodieren der KupferKD und die Kupferschlaufe des Handstücks. Zwei rotierende ftRadnaben beklebt mit einem Stück von einem Schwammrücken auf einer Achse dienen als Polierhilfe. So kann man die bessere Leitfähigkeit der Kupferteile wieder herstellen. 

Idee & Umsetzung: Kai Baumgart

Konstruktionsmaterial von fischertechnik, MisterWho (Drehkranz 3DDruck mit Kugellager), TST (Wellenkupplung , Stellring 4mm & Verteilerplatte), Bauhaus (Kupferdraht/Erdungskabel, Kabelbinder & Schrumpfschlauch ) 
Musik: Mozart Requiem in d minor (https://www.youtube.com/watch?v=sPlhKP0nZII)