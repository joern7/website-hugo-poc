---
layout: "overview"
title: "Montierung fürs Teleobjektiv"
date: 2020-02-22T08:13:43+01:00
legacy_id:
- /php/categories/182
- /categories1b34.html
- /categoriesbca1.html
- /categories7e10.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=182 --> 
Auf wenige Bogensekunden genaue Drehung eines Teleobjektivs um zwei Achsen.