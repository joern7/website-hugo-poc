---
layout: "image"
title: "Variante des -etwas anderen Motors-"
date: "2016-09-25T16:29:00"
picture: "Variante_des_-etwas_anderen_Motors-.jpg"
weight: "8"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Hui-Motor"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44395
- /detailsae04.html
imported:
- "2019"
_4images_image_id: "44395"
_4images_cat_id: "127"
_4images_user_id: "2635"
_4images_image_date: "2016-09-25T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44395 -->
Dieser Hui-Motor hat einen Ringmagneten 10/4mm x 5 mm, der in einem halben Gelenkbaustein (35848) rotiert. Bitte beachten, dass der Magnet nur so weit in den Baustein ragt, dass er auch bei Achsverschiebung in der weiteren Öffnung bleibt. Das Drehmoment reicht für den Betrieb eines &#8222;richtigen&#8220; Propellers (35675) oder eines kleinen Getriebes über eine weiche Kupplung, ich habe eine Stück Pneumatik-Schlauch verwendet.