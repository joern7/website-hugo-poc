---
layout: "image"
title: "Planetarium"
date: "2016-01-10T17:44:13"
picture: "planetarium01.jpg"
weight: "1"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42700
- /details81ec.html
imported:
- "2019"
_4images_image_id: "42700"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42700 -->
An alternative to the Planetarium of Triceratops. I copied the design of Triceratops for the Earth and the moon - it is a perfect design! 
Instead of having all the mechanics on the rotating platform I decided to have the main motor drive and delay of the earth rotation around the sun outside the model. This requires to get all the forces from outside to inside the model. 

The main drive for the sun, earth and moon rotation is through an axis put inside an ALU profill. 

Since the purpose was to show some a class of childeren such model, I also decided to increase the speed of the model in order to make the rotations a bit more visible. Obviously this iimpacts the accuracy of the model. Moreover, I wanted to make sure I could decouple the orbit around the sun from the motor in order to show manually how earth and moon orbit around the sun and create seasons.