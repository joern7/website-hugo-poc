---
layout: "image"
title: "The Earth and Moon mechanics 2"
date: "2016-01-10T17:44:13"
picture: "planetarium05.jpg"
weight: "5"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42704
- /details6965-2.html
imported:
- "2019"
_4images_image_id: "42704"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42704 -->
Here you can see at the bottom how planetary gears connect to the earth and moon system