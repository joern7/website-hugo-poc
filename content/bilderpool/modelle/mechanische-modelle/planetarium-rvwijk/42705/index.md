---
layout: "image"
title: "Sun gears"
date: "2016-01-10T17:44:13"
picture: "planetarium06.jpg"
weight: "6"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42705
- /detailscd31.html
imported:
- "2019"
_4images_image_id: "42705"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42705 -->
Details gears for sun rotation