---
layout: "comment"
hidden: true
title: "6580"
date: "2008-06-07T17:34:51"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
@Schnaggels:
Zu 1.) Freut mich, daß es Dich freut...   ;c)
Zu 2.) Thkais hat recht. Das Interface ist nur ein Verteiler, bzw. Stromversorgung für den IC. Ich betreibe das Interface nur mit 6V. Das hält das IC für die LEDs gerade noch so aus... Die Daten für die Anzeige kommen aus dem Parallelport.
Zu 3.) So ist es. Ich kann daran meinen Plotter 85 anschließen. Oder den TrainingsRobot. Flachbandkabel halt. Muß man dann nicht immer umstöpslen. Ein (größeres) Modell - ein extra Flachbandkabel.
Zu 4.) Ich habe einen AMD Athlon mit 3 GHz. Da es unter Win XP ja leider keinen anständigen DOS-Modus gibt, läuft das Ganze mit einer Start-CD von Win 98. Da kann man noch direkt in einen echten (schnellen) DOS-Modus wechseln.

@Thkais:
Respekt zur Standalone-Version! Ich war schon froh, daß das Ganze (nach etlichem Experimentieren und Fehlschlägen) bei mir überhaupt läuft...   ;c)

Viele Grüße, Andreas.