---
layout: "image"
title: "Bild 01 - Lauftschrift-Generator"
date: "2008-05-16T07:15:05"
picture: "Bild_01_-_Laufschrift_Text_in_the_air.jpg"
weight: "1"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14514
- /detailscd64.html
imported:
- "2019"
_4images_image_id: "14514"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14514 -->
Laufschrift-Generator
"Text in the air"

Mit GWBAsic wird die Sequenz auf die sich drehenden LEDs gegeben. 
Ausgabe über LPT1-Schnittstelle im DOS-Modus (extrem schnell: 4 Mio. Durchläufe pro Sekunde)
Über die Lichtschranke fängt die Anzeige immer an der gleichen Stelle an.
Als Schleifkontakt dient ein 4-poliger Klinkenstecker.
Als Antrieb dienst ein ehemaliger Druckkopf-Antrieb aus einem HP-Drucker.
Ein BCD zu Dezimal Dekoder-IC (Typ CD4028BE) kombiniert mir die 3 Datenleitungen zu den 7 LEDs.