---
layout: "comment"
hidden: true
title: "6536"
date: "2008-05-27T20:26:01"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
Vor einiger Zeit habe ich auch mit solchen Laufschriften herumgespielt, bin aber schnell von der radialen zur axialen Darstellung gekommen, Stichwort: "Propeller clock". 
http://ftcommunity.de/details.php?image_id=1727
http://ftcommunity.de/details.php?image_id=1783
Allerdings ist das "Standalone" und benötigt keinen PC ;-)
Leider siehts in der Realität viel besser aus, als auf den Fotos.
@schnaggels: So, wie ich das sehe, macht das IntInt nicht sehr viel - man kann den Drähten nicht allen folgen, aber mir sieht das so aus, als wäre das Interface nur ein etwas groß geratener Verteiler. Die Buchse scheint ein "Schnellverbinder" ähnlich wie beim RoboInt zu sein.