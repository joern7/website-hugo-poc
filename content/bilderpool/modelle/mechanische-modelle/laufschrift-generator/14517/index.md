---
layout: "image"
title: "Bild 04 - Stromversorgung IC über Stange"
date: "2008-05-16T07:15:05"
picture: "Bild_04_-_Detail_Stromversorgung_IC_ber_Stange.jpg"
weight: "4"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14517
- /details5633.html
imported:
- "2019"
_4images_image_id: "14517"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14517 -->
Da ich nur 4 Kontakte über den Klinkenstecker führen kann, der IC aber Plus und Minus-Versorgung benötigt, habe ich die Plus-Leitung über die Stange geführt. Der obere Schleifkontakt dreht sich mit. Der untere bleibt stehen (wird über Kabel und Kabelhalter an seiner Position gehalten).
Die Schleif-Kontakte gibt es noch bei:
http://www.fischerfriendswoman.de/