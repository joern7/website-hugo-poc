---
layout: "comment"
hidden: true
title: "4917"
date: "2008-01-04T16:23:11"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Diesen Klemmstift 107356 kannte ich gar nicht. Wo war der denn mal drin, wofür ist der denn eigentlich gedacht und was für Abmessungen hat der denn (die Knoblochsche Bitmap ist da nicht gerade aussagekräftig)?