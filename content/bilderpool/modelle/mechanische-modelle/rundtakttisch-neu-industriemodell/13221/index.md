---
layout: "image"
title: "Rundtakttisch - vorne"
date: "2008-01-03T19:07:20"
picture: "rundtakttisch3.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/13221
- /detailsc323.html
imported:
- "2019"
_4images_image_id: "13221"
_4images_cat_id: "1196"
_4images_user_id: "1"
_4images_image_date: "2008-01-03T19:07:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13221 -->
