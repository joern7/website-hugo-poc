---
layout: "image"
title: "12/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus06.jpg"
weight: "6"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16179
- /details7733-2.html
imported:
- "2019"
_4images_image_id: "16179"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16179 -->
Spindelstock- und Getriebegehäuse, Diagonalansicht von links hinten