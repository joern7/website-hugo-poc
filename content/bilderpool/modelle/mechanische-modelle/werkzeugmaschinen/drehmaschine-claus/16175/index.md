---
layout: "image"
title: "07/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus02.jpg"
weight: "2"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16175
- /details61b2.html
imported:
- "2019"
_4images_image_id: "16175"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16175 -->
Spindelstock- und Getriebegehäuse mit 3-Backen-Futter.
Die Spindel dreht sich um die Drehachse C parallel zur Linearachse Z der Bettführung.