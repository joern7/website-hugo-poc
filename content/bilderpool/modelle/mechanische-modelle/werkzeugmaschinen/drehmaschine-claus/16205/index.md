---
layout: "image"
title: "42/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus32.jpg"
weight: "32"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16205
- /detailsc13f-2.html
imported:
- "2019"
_4images_image_id: "16205"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16205 -->
Werkzeugschlitten, Bildmitte vorn Verriegelung Planvorschub