---
layout: "image"
title: "43/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus33.jpg"
weight: "33"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16206
- /details2d22-3.html
imported:
- "2019"
_4images_image_id: "16206"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16206 -->
Werkzeugschlitten, Schloßplatte mit Bedienung von links:
- Rastritzel Z10: Drehwinkel Oberschlitten
- Reifen 30: Planzustellung von Hand
- Handkurbel: Umschaltung Längsvorschub maschinell und Längszustellung von Hand
- Zangenmutter/Spannzange: Planvorschub maschinell