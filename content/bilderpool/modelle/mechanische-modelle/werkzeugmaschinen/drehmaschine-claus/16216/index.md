---
layout: "image"
title: "53/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus43.jpg"
weight: "43"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16216
- /details6104-4.html
imported:
- "2019"
_4images_image_id: "16216"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16216 -->
Werkzeugschlitten von vorn oben
Die Pfeilspitzensymbole definieren die Spindeldrehrichtungen zur gewünschten Schlittenbewegung