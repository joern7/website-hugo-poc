---
layout: "image"
title: "46/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus36.jpg"
weight: "36"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16209
- /details3e4c-2.html
imported:
- "2019"
_4images_image_id: "16209"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16209 -->
Werkzeugschlitten, Ansicht der Schloßplatte von vorn unten