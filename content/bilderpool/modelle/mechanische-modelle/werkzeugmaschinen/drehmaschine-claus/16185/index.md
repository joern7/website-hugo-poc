---
layout: "image"
title: "18/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus12.jpg"
weight: "12"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16185
- /details218d-2.html
imported:
- "2019"
_4images_image_id: "16185"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16185 -->
Spindelgetriebe, Teilansicht hintere Stirnseite