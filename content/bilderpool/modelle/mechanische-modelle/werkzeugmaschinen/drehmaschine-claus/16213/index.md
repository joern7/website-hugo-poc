---
layout: "image"
title: "50/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus40.jpg"
weight: "40"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16213
- /details255b.html
imported:
- "2019"
_4images_image_id: "16213"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16213 -->
Spindelgetriebe, Teilansicht