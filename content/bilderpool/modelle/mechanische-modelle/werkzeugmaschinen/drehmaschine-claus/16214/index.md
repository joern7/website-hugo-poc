---
layout: "image"
title: "51/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus41.jpg"
weight: "41"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16214
- /detailsfad8-2.html
imported:
- "2019"
_4images_image_id: "16214"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16214 -->
Spindel- und Vorschubgetriebe von vorn, Teilansicht