---
layout: "image"
title: "27/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus21.jpg"
weight: "21"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16194
- /details3bd2-2.html
imported:
- "2019"
_4images_image_id: "16194"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16194 -->
3-Backen-Futter selbstzentrierend, Stirnansicht von vorn oben