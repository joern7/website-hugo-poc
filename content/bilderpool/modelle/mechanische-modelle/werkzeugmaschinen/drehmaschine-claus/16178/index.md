---
layout: "image"
title: "11/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus05.jpg"
weight: "5"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16178
- /detailsb132.html
imported:
- "2019"
_4images_image_id: "16178"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16178 -->
Spindelstock- und Getriebegehäuse, Stirnansicht von hinten