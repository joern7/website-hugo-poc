---
layout: "image"
title: "34/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus26.jpg"
weight: "26"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16199
- /detailse23f-2.html
imported:
- "2019"
_4images_image_id: "16199"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16199 -->
Reitstock
Pinolenbewegung von Hand und Führung von unten gesehen