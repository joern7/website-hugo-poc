---
layout: "image"
title: "40/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus30.jpg"
weight: "30"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16203
- /detailsb50b-2.html
imported:
- "2019"
_4images_image_id: "16203"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16203 -->
Werkzeugschlitten, Umschaltwellen unten links Längs- und oben rechts Planvorschub