---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (9/15)"
date: "2008-10-14T08:59:55"
picture: "bohrundfraesmaschinebf09.jpg"
weight: "9"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15975
- /details2d29.html
imported:
- "2019"
_4images_image_id: "15975"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15975 -->
Der Kreuzschlitten mit dem Arbeitstisch ermöglicht die Werkstückbewegung in den beiden Linearachsen X (steigende Bildkoordinate) und Y (fallende Bildkoordinate).