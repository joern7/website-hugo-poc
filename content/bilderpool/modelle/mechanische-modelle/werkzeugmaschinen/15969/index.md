---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (3/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf03.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15969
- /details7002.html
imported:
- "2019"
_4images_image_id: "15969"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15969 -->
Wie wir alle wissen, ist ein rechter Winkel mit einem Anbauwinkel bei einer Belastung kleiner/grösser 90° an der Verbinderseite nicht stabil. Da ich mit dem Modell auch Bearbeitungsversuche durchführen will und damit am Gestell auch aufbiegende Kräfte auftreten, habe ich vier Anbauwinkel entgegengesetzt angeordnet. Die Verbinder wurden erst eingeschoben, nachdem die Säule mit einem Winkel senkrecht ausgerichtet war. Das Modell hat inzwischen fast 9 Monate gestanden. Die Säule hat in dieser Zeit am Fuß "gearbeitet", was am Säulenkopf eine Neigung nach vorn von der Senkrechten um 0,4mm verursachte. Das "Arbeiten" ist eine Lageveränderung zwischen den Klemmflächen der Teile, weil die durch die elastischen Verbindungskonturen Zapfen/Nut erzeugte Haftreibung dort zu gering ist.