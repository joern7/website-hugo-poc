---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (1/15)"
date: "2008-10-14T08:59:53"
picture: "bohrundfraesmaschinebf01.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15967
- /detailsfccd.html
imported:
- "2019"
_4images_image_id: "15967"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15967 -->
Vorweg:
Dieses Modell einer Bohr- und Fräsmaschine der Heimwerkerklasse habe ich hier in der ftC schon mal im Januar 2008 vorgestellt, allerdings nur mit einer 2D-Ansicht aus dem 3D-Fenster und 3 unzureichenden Fotos. Ich versprach den Wünschen der Kommentatoren mit ausführlicheren und vor allem besseren Fotos nachzukommen. Das kann ich nun heute endlich einlösen. Das Modell wird nach dieser Vorstellung demnächst mit einer programmgesteuerten Motorisierung ausgestattet, die ich in 3D schon vorbereitet habe. Ich hoffe ihr seid einverstanden, daß dann damit das noch gewünschte Video folgt.
Funktionsumfang:
Spindelantrieb mit Power-Motor 8:1, 2-stufiges Rädergetriebe mit Leerlauf, Handvorschub zum Bohren und manuelle Fräszustellung. Das Grundmaschinenmodell besteht aus dem Maschinenkörper mit Fuß und Säule, dem schwenk- und höhenverstellbaren Spindelkopf sowie einem Kreuzschlitten mit Arbeitstisch. Diese Grundausstattung hat die Linearachsen X, Y und Z sowie die Drehachse B. Mit dem "Sonderzubehör" erweitern sich die Arbeitsachsen um die Rotationsachsen A und C auf insgesamt sechs. Die Achsen werden bei den folgenden Fotos beschrieben.
Ich darf darauf verweisen, daß die gewählte Modellgröße gerade noch die modellgemäße Umsetzung der realen Funktionen ermöglichte. Teilweise konnten konstruktiv "keine Teile mehr dazwischen" konzipiert werden, sodaß einzelne Modellkomponenten dann schon fast simpel erscheinen.
Nachtrag 21.10.2008:
Erste Veröffentlichung
http://www.ftcommunity.de/details.php?image_id=13342
Nachtrag 31.03.2009:
BF1 jetzt als BF1-2 mit 7 Motoren, 6-Achsen-Steuerung bis 20 Prozessdaten je Programmschritt
http://www.ftcommunity.de/details.php?image_id=23560