---
layout: "image"
title: "[10/11 Planscheibe mit 3 Spannbacken"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl10.jpg"
weight: "10"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28110
- /details177d.html
imported:
- "2019"
_4images_image_id: "28110"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28110 -->
Hier die vorläufige Planscheibe mit 3 Spannbacken für zylindrische Werkstücke. Eine mit vergleichbarem Aufbau und 4 Spannbacken für prismatische Werkstücke ist auch vorhanden. Planscheiben dieser Grösse über Bett ! haben keine Selbstzentrierung. Da muss jeder Spannbacken egal ob zentrisch oder exzentrisch zueinander einzeln eingestellt werden. Aus Stabilitätsgründen sind die Planscheiben hier auf der Arbeitsspindel 2-fach gelagert. Bei dieser läuft ein Experiment an der vorderen Lagerung mit einer Freilaufnabe ...
Dieses einfache allseitig überdrehte Testwerkstück ist aus Wachs (Kerzenreststück) und vor der bereits erfolgten Bearbeitung der zweiten Seite umgespannt und zentriert. Da es dabei etwas weiter heraus zu spannen war, wurde der erforderliche rückseitige Plananschlag zur Vermeidung einer Unwucht mit einem Rad 23 auf der Spindelspitze zentriert und als eigentliche Anlage an einem etwas aufgeschobenen Reifen 32,5 hergestellt.
Das Wachs lässt sich beispielsweise sehr gut zerspanen und die Späne sind mit einem weichen ! Pinsel leicht vom Modell entfernbar. Allerdings verhielt sich das Material zwischen den noch nicht präparierten Spannbackenflächen "aalglatt" und musste zunächst zusätzlich gesichert werden.
Günstig hierbei ist es, wenn man die Werkstücke vorher extern überdrehen kann, damit sie im Modell schon beim Einspannen rundlaufen und es so nicht durch Unwucht überlasten.