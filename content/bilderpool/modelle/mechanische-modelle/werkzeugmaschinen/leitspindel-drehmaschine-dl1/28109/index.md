---
layout: "image"
title: "[9/11] Bedienelemente"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl09.jpg"
weight: "9"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28109
- /details1544-2.html
imported:
- "2019"
_4images_image_id: "28109"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28109 -->
Die Bedienelemente Handrad/Handspindel für Support und Reitstock habe ich mit Verzahnung m0,5 ausgewählt. Die Zähne und Lücken nutze ich hilfsweise für definierte Zu- und Einstellungen als "Skalenstriche" wenn auch nur mit "krummen" Werten. Beim Querschlitten sind das z.B. auf den Werkstückdurchmesser pro Zahn 0,118 mm bzw. pro Zahn-Lücke 0,059 mm. Es ist hilfreich wegen der Kräfteverhältnisse und dem beschränkten Drehmoment der Arbeitsspindel solche definiert feinen  Zustellungen z.B. bei grossen Drehdurchmessern in der Spantiefe auch manuell vornehmen zu können. 
Der Oberschlitten steht hier übrigens nicht ganz parallel zur Z-Achse. Er kann auf dem Zahnkranz um 360° geschwenkt werden.