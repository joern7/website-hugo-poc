---
layout: "image"
title: "Bohrmaschine (Führung Koordinatentisch)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/41090
- /details5ccb.html
imported:
- "2019"
_4images_image_id: "41090"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41090 -->
Hier sieht man vorne den Baustein mit dem roten Zapfen, der somit die Position der Schneckenbefestigung fixiert. Der hintere Taster ist für das Abschalten des Motors in der Endposition. Die Taster links (nur einer zu sehen auf diesem Bild) für die Bewegung des Tisches nach links und rechts.