---
layout: "image"
title: "Bohrmaschine (gesamt)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/41086
- /detailsd239.html
imported:
- "2019"
_4images_image_id: "41086"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41086 -->
Unimat Bohrmaschine
FT Koordinatentisch und Bohrmaschinenführung
Proxon Schraubstock

Der Koordinatentisch kann elektrisch vorwärts/rückwärts und links/rechts bewegt werden. An jeder Endposition befinden sich Endschalter. Die Bohrmaschine kann elektrisch auf/ab bewegt werden. Hier gibt es den Endschalter allerdings nur in der oberen Position, weil die Bohrtiefe ja verschieden seien kann. Die tragenden Bausteine sind so gesteckt, dass es kein Verrutschen gibt. Das habe ich erreicht, indem ich den Baustein mit den roten Zapfen in das Loch der Bauplatte gesteckt habe und die anderen Bausteine dann links und rechts davon verankert wurden. Der Koordinatentisch mit der jeweiligen Schnecke ist mit einer leichten Druckspannnung verbunden. Somit beträgt die Genauigkeit insgesamt weit unter 1mm. Die Einstellung der Geschwindigkeit der jeweiligen Motoren ist mit dem vorderen Schiebeschalter in zwei Stufen möglich. Durch die Verkabelung der Endtaster mit drei Kabeln komme ich ohne Controller aus, so dass trotzdem die Abschaltung in den Endpositionen erfolgt.