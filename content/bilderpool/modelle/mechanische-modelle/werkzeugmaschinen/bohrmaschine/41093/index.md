---
layout: "image"
title: "Bohrmaschine (Beispiel Kontakte Powermotor)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine8.jpg"
weight: "8"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/41093
- /details1de1.html
imported:
- "2019"
_4images_image_id: "41093"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41093 -->
Hier sieht man die Vergrößerung der Anschlusskontakte nach dem Aufbohren. Jetzt passt die Schraube der FT Buchse durch das Loch und man kommt ohne Löten aus.