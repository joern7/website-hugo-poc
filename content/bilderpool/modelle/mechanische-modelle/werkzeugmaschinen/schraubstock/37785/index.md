---
layout: "image"
title: "Schraubstock"
date: "2013-11-01T14:03:14"
picture: "schraubstock7.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37785
- /detailse440.html
imported:
- "2019"
_4images_image_id: "37785"
_4images_cat_id: "2807"
_4images_user_id: "453"
_4images_image_date: "2013-11-01T14:03:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37785 -->
