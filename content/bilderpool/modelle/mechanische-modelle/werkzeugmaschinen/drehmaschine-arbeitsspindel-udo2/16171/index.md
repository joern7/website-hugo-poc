---
layout: "image"
title: "(8/9) Kopflagerkäfig"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel8.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16171
- /detailsf26e-2.html
imported:
- "2019"
_4images_image_id: "16171"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16171 -->
Hier die Lagerkäfige des Kopflagers mit den 2x6 Rollkörpern Rad 14.