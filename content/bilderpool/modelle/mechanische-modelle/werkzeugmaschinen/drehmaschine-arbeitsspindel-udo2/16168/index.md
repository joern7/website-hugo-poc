---
layout: "image"
title: "(5/9) Kopfflanschstirn"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16168
- /detailsa332.html
imported:
- "2019"
_4images_image_id: "16168"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16168 -->
Stirnansicht des Kopfflanschs:
Bohrungsflucht Stangendurchlass für Rund- und 6kt-Material bis 13,5mm