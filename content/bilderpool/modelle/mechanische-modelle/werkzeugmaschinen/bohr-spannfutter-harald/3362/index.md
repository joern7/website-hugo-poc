---
layout: "image"
title: "Futter03.JPG"
date: "2004-11-30T19:55:42"
picture: "Futter03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Bohrfutter"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3362
- /details94e7.html
imported:
- "2019"
_4images_image_id: "3362"
_4images_cat_id: "299"
_4images_user_id: "4"
_4images_image_date: "2004-11-30T19:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3362 -->
