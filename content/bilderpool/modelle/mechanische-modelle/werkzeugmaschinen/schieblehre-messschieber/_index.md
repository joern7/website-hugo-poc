---
layout: "overview"
title: "Schieblehre  Messschieber"
date: 2020-02-22T08:18:39+01:00
legacy_id:
- /php/categories/2892
- /categoriesdaf6-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2892 --> 
Mit Innenmaß, Außenmaß und Tiefenmaß + Feststellschraube.
Ohne Nonius da nur im mm Bereich genau.