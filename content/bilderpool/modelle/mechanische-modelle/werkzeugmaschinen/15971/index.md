---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (5/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf05.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15971
- /details2abb.html
imported:
- "2019"
_4images_image_id: "15971"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15971 -->
Der geöffnete Spindelkopf von rechts. Zu sehen sind die beiden Handhebel, der hintere für die Schaltung der zwei Drehzahlbereiche und des Leerlaufs sowie der vordere für wahlweise Handvorschub zum Bohren oder Fräszustellung.