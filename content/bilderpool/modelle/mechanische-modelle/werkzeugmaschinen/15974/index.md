---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (7/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf08.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15974
- /details9acf.html
imported:
- "2019"
_4images_image_id: "15974"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15974 -->
Eine kleine Wartungsöffnung am verschlossenen Spindelkopf auf der linken Seite.