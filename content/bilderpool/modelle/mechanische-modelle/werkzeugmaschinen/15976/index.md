---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (10/15)"
date: "2008-10-14T08:59:55"
picture: "bohrundfraesmaschinebf10.jpg"
weight: "10"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15976
- /details9f63.html
imported:
- "2019"
_4images_image_id: "15976"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15976 -->
Ansicht des Modells von oben auf Spindel- und Säulenkopf.