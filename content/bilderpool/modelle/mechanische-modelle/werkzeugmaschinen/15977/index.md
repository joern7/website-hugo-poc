---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (11/15)"
date: "2008-10-14T09:00:13"
picture: "bohrundfraesmaschinebf11.jpg"
weight: "11"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15977
- /details184b.html
imported:
- "2019"
_4images_image_id: "15977"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T09:00:13"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15977 -->
Der Spindelkopf kann nach beiden Richtungen bis 90° geschwenkt werden. Dabei dreht er sich um die Drehachse B, die parallel zur Linearachse Y liegt.