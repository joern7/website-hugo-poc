---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (6/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf06.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15972
- /details9dc8.html
imported:
- "2019"
_4images_image_id: "15972"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15972 -->
Der geöffnete Spindelkopf von links. Zu sehen ist das Rastschneckengetriebe, welches das Halten einer manuellen Fräszustellung ermöglicht. Die Einstellung der Fräszustellung erfolgt dann an der Stellradimitation am unteren Bildrand. Die Funktion des Handvorschubes zum Bohren ist bei dieser Funktion blockiert.