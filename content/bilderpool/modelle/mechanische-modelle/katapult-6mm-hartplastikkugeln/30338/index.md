---
layout: "image"
title: "Gesamtansicht"
date: "2011-03-30T16:49:54"
picture: "IMG_4057_small.jpg"
weight: "2"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/30338
- /details52b5.html
imported:
- "2019"
_4images_image_id: "30338"
_4images_cat_id: "2257"
_4images_user_id: "986"
_4images_image_date: "2011-03-30T16:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30338 -->
Mit diesem Katapult kann man sogenannte Softairkugeln (Ich hatte nie eine Softair, werde mir auch sicher nie ein kaufen, die Kugeln sind halt nur für "experimetelle Zwecke" recht praktisch :P) schießen kann. Reichweite liegt bei ~10m.