---
layout: "image"
title: "Tür 6"
date: "2007-03-09T19:00:21"
picture: "doppeltuer06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9358
- /detailsc308.html
imported:
- "2019"
_4images_image_id: "9358"
_4images_cat_id: "863"
_4images_user_id: "502"
_4images_image_date: "2007-03-09T19:00:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9358 -->
Die vordere Tür wird über die Ketten bewegt, die hintere über Schnüre die mit der vorderen Tür verbunden sind.