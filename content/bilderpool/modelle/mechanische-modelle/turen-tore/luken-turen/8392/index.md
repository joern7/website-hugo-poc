---
layout: "image"
title: "Tuer06-auf.JPG"
date: "2007-01-13T14:23:45"
picture: "Tuer06-auf.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8392
- /details47b2.html
imported:
- "2019"
_4images_image_id: "8392"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:23:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8392 -->
Hier sind es wieder 45 mm Öffnungsweite, allerdings mit etwas Behinderung durch die Klemmhülse 35980, die beim Öffnen in den Weg schwenkt. Die Klemmhülse sorgt dafür, dass man die Tür auch von außen öffnen kann, ohne sich die Fingernägel abzubrechen: wenn man von außen mit einer Achse o.ä. durch das Loch drückt, schwenkt die Tür auf.

Die Scharniere beruhen darauf, dass der Verschlussriegel 37232 prima in den Riegelstein hineinpasst.