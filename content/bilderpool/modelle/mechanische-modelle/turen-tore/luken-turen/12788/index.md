---
layout: "image"
title: "Tuer10-auf.JPG"
date: "2007-11-18T23:06:53"
picture: "Tuer10-auf.JPG"
weight: "38"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12788
- /detailsc1ee-2.html
imported:
- "2019"
_4images_image_id: "12788"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T23:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12788 -->
