---
layout: "image"
title: "Luke04-auf.JPG"
date: "2007-01-13T14:55:11"
picture: "Luke04-auf.JPG"
weight: "29"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8410
- /detailsa95c.html
imported:
- "2019"
_4images_image_id: "8410"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:55:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8410 -->
