---
layout: "image"
title: "Tuer05-zu.JPG"
date: "2007-01-13T14:16:40"
picture: "Tuer05-zu.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8389
- /details51b1-2.html
imported:
- "2019"
_4images_image_id: "8389"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:16:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8389 -->
Das Bild täuscht: diese Tür öffnet 60 mm weit (es fehlt eine V-Platte 15x90 links).
Die Rollenlager 37636 sitzen je auf zwei Zapfen, aber immer nur halb.