---
layout: "image"
title: "Tuer08-auf.JPG"
date: "2007-01-13T14:31:23"
picture: "Tuer08-auf.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8397
- /details446e.html
imported:
- "2019"
_4images_image_id: "8397"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:31:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8397 -->
Diese Tür hat 45 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden.
Die Tür ist so hoch gebaut, dass eine ft-Figur (hier: Thor mit dem Hammer) hindurchgehen kann. Das geht bei den Türen 01 bis 07 natürlich auch, wenn man die Ober/Unterteile auseinander setzt.