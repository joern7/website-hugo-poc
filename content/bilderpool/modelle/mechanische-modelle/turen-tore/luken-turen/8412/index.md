---
layout: "image"
title: "Luke05-zu.JPG"
date: "2007-01-13T15:00:43"
picture: "Luke05-zu.JPG"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8412
- /detailscd69.html
imported:
- "2019"
_4images_image_id: "8412"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:00:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8412 -->
Hier sind Teile quer durchs ft-Sortiment verbaut. Die Zahnstange wird durch "Führungsplatten E-Magnet 32455" gehalten. Die Scharniere sind aus der Zahnspurstange 38472 und zugehörigen Lenkhebeln aufgebaut. Die beiden Streben I-30 sind oben in einem Statikadapter 35975 gelagert.