---
layout: "image"
title: "Tuer01-auf.JPG"
date: "2007-01-13T14:02:45"
picture: "Tuer01-auf.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8382
- /detailsb2a2.html
imported:
- "2019"
_4images_image_id: "8382"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:02:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8382 -->
