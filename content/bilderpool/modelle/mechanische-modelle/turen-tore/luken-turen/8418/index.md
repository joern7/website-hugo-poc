---
layout: "image"
title: "Luke07-auf.JPG"
date: "2007-01-13T15:07:17"
picture: "Luke07-auf.JPG"
weight: "37"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8418
- /details0e4e-2.html
imported:
- "2019"
_4images_image_id: "8418"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:07:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8418 -->
Das ist noch ein Viergelenk-Getriebe mit der Gelenkkurbel 35088. Den rechten Deckel muss man sich dazudenken.