---
layout: "image"
title: "Tuer03-auf.JPG"
date: "2007-01-13T14:11:02"
picture: "Tuer03-auf.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8386
- /detailsf9ea.html
imported:
- "2019"
_4images_image_id: "8386"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:11:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8386 -->
