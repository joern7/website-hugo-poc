---
layout: "image"
title: "Luke06-auf.JPG"
date: "2007-01-13T15:05:12"
picture: "Luke06-auf.JPG"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8416
- /details955d.html
imported:
- "2019"
_4images_image_id: "8416"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:05:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8416 -->
Das ist wieder ein Viergelenk-Getriebe, diesmal mit der Gelenkkurbel 35088 aufgebaut. Den rechten Deckel muss man sich dazudenken.