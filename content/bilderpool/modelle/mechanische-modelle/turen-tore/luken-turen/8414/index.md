---
layout: "image"
title: "Luke05-auf3.JPG"
date: "2007-01-13T15:02:20"
picture: "Luke05-auf3.JPG"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8414
- /detailsf26a.html
imported:
- "2019"
_4images_image_id: "8414"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:02:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8414 -->
