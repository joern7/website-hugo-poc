---
layout: "image"
title: "Tuer07-zu.JPG"
date: "2007-01-13T14:26:32"
picture: "Tuer07-zu.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8393
- /details1a19-2.html
imported:
- "2019"
_4images_image_id: "8393"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:26:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8393 -->
Diese Tür hat wieder 45 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden (angedeutet durch das Kardangelenk).
Als Mitnehmer dienen zwei Klemmhülsen unter dem BS7,5 nebst den beiden Rastadaptern beiderseits davon.