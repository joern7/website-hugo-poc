---
layout: "image"
title: "Luke11"
date: "2011-06-19T18:21:54"
picture: "Luke11.JPG"
weight: "39"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30867
- /detailse9f2-2.html
imported:
- "2019"
_4images_image_id: "30867"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2011-06-19T18:21:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30867 -->
Da wurde im Forum nach einer Tür gefragt, die bündig in die ft-Verkleidung hineinpasst und 30 mm weit auf geht. Dieser hier fehlt noch ein Griff, damit sie auch von außen betätigt werden kann. Oder man dreht die Kurbel um, und setzt ein Zahnrad o.ä. auf den Achsstummel, der dann nach unten weg geht.

Forum: http://forum.ftcommunity.de/viewtopic.php?f=15&t=448