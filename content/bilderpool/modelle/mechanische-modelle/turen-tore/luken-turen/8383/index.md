---
layout: "image"
title: "Tuer02-zu.JPG"
date: "2007-01-13T14:06:08"
picture: "Tuer02-zu.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8383
- /detailsaf3c.html
imported:
- "2019"
_4images_image_id: "8383"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:06:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8383 -->
Gleiches Prinzip wie Tür 01, Öffnung 45 mm, aber mit anderen Bauteilen.