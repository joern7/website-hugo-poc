---
layout: "image"
title: "Teleskoptür07"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13594
- /details9a8e-3.html
imported:
- "2019"
_4images_image_id: "13594"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13594 -->
Geschlossene Tür