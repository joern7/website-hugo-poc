---
layout: "image"
title: "Teleskoptür05"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13592
- /detailsebfd.html
imported:
- "2019"
_4images_image_id: "13592"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13592 -->
Ansicht von oben. Türblattaufhängung