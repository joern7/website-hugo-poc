---
layout: "image"
title: "Schachttür03"
date: "2008-02-09T13:45:47"
picture: "schachttuer3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13611
- /details0ad6.html
imported:
- "2019"
_4images_image_id: "13611"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13611 -->
Ansicht vom "Schacht"