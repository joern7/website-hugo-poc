---
layout: "image"
title: "Schachttür05"
date: "2008-02-09T13:45:47"
picture: "schachttuer5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13613
- /details4eeb.html
imported:
- "2019"
_4images_image_id: "13613"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13613 -->
geöffnete Schachttür