---
layout: "image"
title: "Alles"
date: "2007-08-09T22:02:39"
picture: "lamellentor9.jpg"
weight: "9"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11332
- /details5740.html
imported:
- "2019"
_4images_image_id: "11332"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11332 -->
Hier das alte Interface mit einem entsprechend alten Notebook zum Programmieren...