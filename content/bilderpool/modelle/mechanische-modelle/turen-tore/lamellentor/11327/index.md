---
layout: "image"
title: "Zu(von Oben)"
date: "2007-08-09T22:02:25"
picture: "lamellentor4.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11327
- /details28f9.html
imported:
- "2019"
_4images_image_id: "11327"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11327 -->
Hier nochmal geschlossen von oben zu sehen.