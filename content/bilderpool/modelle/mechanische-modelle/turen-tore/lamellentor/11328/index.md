---
layout: "image"
title: "Ansicht"
date: "2007-08-09T22:02:25"
picture: "lamellentor5.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11328
- /detailse4a8.html
imported:
- "2019"
_4images_image_id: "11328"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11328 -->
Hier nochmal die Mechanik mit den Ketten.