---
layout: "image"
title: "Doppelfalttor 12"
date: "2007-08-11T17:21:31"
picture: "doppelfalttor2.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11350
- /details4049-2.html
imported:
- "2019"
_4images_image_id: "11350"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11350 -->
