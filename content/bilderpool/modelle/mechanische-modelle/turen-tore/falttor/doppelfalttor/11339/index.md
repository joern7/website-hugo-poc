---
layout: "image"
title: "Doppelfalttor  4"
date: "2007-08-10T17:07:05"
picture: "doppelfalttor04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11339
- /detailscca9.html
imported:
- "2019"
_4images_image_id: "11339"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11339 -->
Hier die Mechanik der 3. Tür