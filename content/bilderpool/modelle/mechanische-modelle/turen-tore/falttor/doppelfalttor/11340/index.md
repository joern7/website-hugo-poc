---
layout: "image"
title: "Doppelfalttor  5"
date: "2007-08-10T17:07:05"
picture: "doppelfalttor05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11340
- /details1d03-2.html
imported:
- "2019"
_4images_image_id: "11340"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11340 -->
