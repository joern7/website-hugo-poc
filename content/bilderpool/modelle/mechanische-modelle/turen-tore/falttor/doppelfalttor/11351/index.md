---
layout: "image"
title: "Doppelfalttor 13"
date: "2007-08-11T17:21:31"
picture: "doppelfalttor3.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11351
- /details98d2-4.html
imported:
- "2019"
_4images_image_id: "11351"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11351 -->
Da das Tor auf der linken Seite nicht ganz auf gegangen ist hab ich hier noch eine Mechanik eingebaut die es noch restlich aufdrückt.