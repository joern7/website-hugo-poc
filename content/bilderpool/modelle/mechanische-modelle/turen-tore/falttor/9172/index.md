---
layout: "image"
title: "Falttor 15"
date: "2007-02-28T19:26:00"
picture: "falttor15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9172
- /details72f0-2.html
imported:
- "2019"
_4images_image_id: "9172"
_4images_cat_id: "846"
_4images_user_id: "502"
_4images_image_date: "2007-02-28T19:26:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9172 -->
Die 2 Platten sorgen dafür dass das Tor gut geschlossen wird.