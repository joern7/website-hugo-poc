---
layout: "image"
title: "Falttor 5"
date: "2007-02-28T19:25:41"
picture: "falttor05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9162
- /detailsb5ed.html
imported:
- "2019"
_4images_image_id: "9162"
_4images_cat_id: "846"
_4images_user_id: "502"
_4images_image_date: "2007-02-28T19:25:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9162 -->
Hier drückt die Strebe auf den schwarzen Stein der direjt mit dem Tor verbunden ist um sie so ein wenig zu öffnen.