---
layout: "image"
title: "Tür 9"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9389
- /details589f.html
imported:
- "2019"
_4images_image_id: "9389"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9389 -->
