---
layout: "image"
title: "Tür 2"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9382
- /detailsce78.html
imported:
- "2019"
_4images_image_id: "9382"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9382 -->
