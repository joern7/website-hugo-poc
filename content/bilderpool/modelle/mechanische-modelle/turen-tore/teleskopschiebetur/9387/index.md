---
layout: "image"
title: "Tür 7"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9387
- /detailsb055.html
imported:
- "2019"
_4images_image_id: "9387"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9387 -->
