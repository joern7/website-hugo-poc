---
layout: "image"
title: "Tür 11"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9391
- /details07ad-2.html
imported:
- "2019"
_4images_image_id: "9391"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9391 -->
