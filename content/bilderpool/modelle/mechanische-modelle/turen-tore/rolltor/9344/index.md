---
layout: "image"
title: "Rolltor 3"
date: "2007-03-07T16:00:50"
picture: "rolltor3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9344
- /detailsc7f7.html
imported:
- "2019"
_4images_image_id: "9344"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9344 -->
