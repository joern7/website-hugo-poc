---
layout: "image"
title: "Rolltor 2"
date: "2007-03-07T16:00:50"
picture: "rolltor2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9343
- /detailsb65b-2.html
imported:
- "2019"
_4images_image_id: "9343"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9343 -->
Mit diesen Hebeln kann man das Rolltor in jede beliebige Position steuern.