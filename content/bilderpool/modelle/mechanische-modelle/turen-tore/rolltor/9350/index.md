---
layout: "image"
title: "Rolltor 9"
date: "2007-03-07T16:00:50"
picture: "rolltor9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9350
- /details63da.html
imported:
- "2019"
_4images_image_id: "9350"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9350 -->
