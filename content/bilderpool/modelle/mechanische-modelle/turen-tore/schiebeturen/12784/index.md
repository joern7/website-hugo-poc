---
layout: "image"
title: "Schiebetuer08.JPG"
date: "2007-11-18T23:00:37"
picture: "Schiebetuer08.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12784
- /detailsaba3-2.html
imported:
- "2019"
_4images_image_id: "12784"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T23:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12784 -->
Diese Version passt schon besser (überhaupt, und auch ins ft-Raster) als Version 07.
Die roten gewinkelten Teile gab es einmal als Zubehör zu den Clips-Verkleidungsplatten (31503 und andere). Das gelbe Teil müsste irgendwo zur BSB gehören und wurde von Knobloch als Restbestand günstigst verscherbelt. Da hab ich mir halt ein paar besorgt und endlich eine Anwendung gefunden.