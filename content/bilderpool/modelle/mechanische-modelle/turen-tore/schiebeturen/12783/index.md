---
layout: "image"
title: "Schiebetuer07.JPG"
date: "2007-11-18T22:55:54"
picture: "Schiebetuer07.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12783
- /detailsc7ee.html
imported:
- "2019"
_4images_image_id: "12783"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T22:55:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12783 -->
Das Flachstück 120 lässt sich mit der "Führungsplatte für E-Magnet" auch umfassen und damit als Schiebeführung verwenden -- wenn auch mit reichlich Spiel.