---
layout: "image"
title: "Schiebetuer05-auf.JPG"
date: "2007-01-14T12:57:44"
picture: "Schiebetuer05-auf.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8452
- /details7f1a.html
imported:
- "2019"
_4images_image_id: "8452"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:57:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8452 -->
