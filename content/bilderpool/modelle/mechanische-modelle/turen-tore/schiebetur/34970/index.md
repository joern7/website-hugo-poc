---
layout: "image"
title: "Ansicht oben offen"
date: "2012-05-18T00:39:49"
picture: "schiebetuerfabian5.jpg"
weight: "5"
konstrukteure: 
- "Fabian"
fotografen:
- "Fabian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-fabi"
license: "unknown"
legacy_id:
- /php/details/34970
- /details141f.html
imported:
- "2019"
_4images_image_id: "34970"
_4images_cat_id: "2588"
_4images_user_id: "1502"
_4images_image_date: "2012-05-18T00:39:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34970 -->
Die 4 BS-30-Steine sind zur stabilisation notwendig, da ich die Tür nicht weiter mit anderen "Gebäudeteilen" verbunden habe.
Wenn man die Türhälften jeweils mit anderen Teilen verbindet, kann man die 4 BS-30 weglassen :-)