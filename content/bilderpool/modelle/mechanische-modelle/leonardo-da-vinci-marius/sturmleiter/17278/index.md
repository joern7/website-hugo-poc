---
layout: "image"
title: "eingefahrener Zustand"
date: "2009-02-03T00:59:28"
picture: "sturmleiter1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17278
- /detailsa41e.html
imported:
- "2019"
_4images_image_id: "17278"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17278 -->
