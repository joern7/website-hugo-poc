---
layout: "image"
title: "Schnecke und Zahnrad von hinten"
date: "2009-02-03T00:59:29"
picture: "sturmleiter7.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17284
- /details9481.html
imported:
- "2019"
_4images_image_id: "17284"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17284 -->
Hier sieht man den Mini-Motor, der die Leiter kippt und hochfährt.