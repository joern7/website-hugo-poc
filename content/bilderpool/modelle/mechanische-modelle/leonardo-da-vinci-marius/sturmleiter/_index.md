---
layout: "overview"
title: "Sturmleiter"
date: 2020-02-22T08:18:49+01:00
legacy_id:
- /php/categories/1549
- /categories2466.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1549 --> 
Diese Version der Sturmleiter von Leonardo Da Vinci ist die \"Luxus\"-Ausführung. Die Leiter wird durch den Mini-Motor hoch-und runtergefahren.