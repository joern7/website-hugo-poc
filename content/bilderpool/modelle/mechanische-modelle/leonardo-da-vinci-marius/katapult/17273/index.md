---
layout: "image"
title: "katapult4.jpg"
date: "2009-02-03T00:59:28"
picture: "katapult4.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17273
- /detailsb87b.html
imported:
- "2019"
_4images_image_id: "17273"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17273 -->
