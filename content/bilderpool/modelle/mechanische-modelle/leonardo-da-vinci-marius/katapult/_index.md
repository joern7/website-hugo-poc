---
layout: "overview"
title: "Katapult"
date: 2020-02-22T08:18:49+01:00
legacy_id:
- /php/categories/1548
- /categoriesdcda.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1548 --> 
Mein Leonardo Da Vinci-Katapult schleudert 15er-Bausteine etwa 40-50 cm weit.