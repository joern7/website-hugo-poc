---
layout: "image"
title: "Vorderansicht"
date: "2009-02-01T19:35:38"
picture: "streitwagen2.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17237
- /details90cb.html
imported:
- "2019"
_4images_image_id: "17237"
_4images_cat_id: "1546"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17237 -->
