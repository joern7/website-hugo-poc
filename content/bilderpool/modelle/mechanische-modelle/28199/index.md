---
layout: "image"
title: "Ellipsenschablone"
date: "2010-09-19T21:07:47"
picture: "Ellipsenschablone.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/28199
- /details96dc.html
imported:
- "2019"
_4images_image_id: "28199"
_4images_cat_id: "127"
_4images_user_id: "724"
_4images_image_date: "2010-09-19T21:07:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28199 -->
Hiemit können Ellipsen gezeichnet werden, die auch in ihrer Größe verändert werden können.
Dazu muß einfach die Befestigung der Stangen auf der "Schiene" verschoben werden.
Inspiriert wurde ich durch eine Schablone für Oberfräsen.

Hier sieht man die Schablone aus Fischertechnik in Aktion:
http://www.youtube.com/watch?v=cDArHIJjizc

Video vom Original siehe hier:
http://www.youtube.com/watch?v=hOO6gwMuiUo