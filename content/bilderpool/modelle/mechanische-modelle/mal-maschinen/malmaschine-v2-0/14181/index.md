---
layout: "image"
title: "Malmaschine2-02"
date: "2008-04-07T07:56:03"
picture: "malmaschinev02.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14181
- /details9bce.html
imported:
- "2019"
_4images_image_id: "14181"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14181 -->
Gesamtansicht von allen Seiten