---
layout: "image"
title: "Malmaschine2-01"
date: "2008-04-07T07:56:03"
picture: "malmaschinev01.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14180
- /details1df7.html
imported:
- "2019"
_4images_image_id: "14180"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14180 -->
Gesamtansicht von allen Seiten