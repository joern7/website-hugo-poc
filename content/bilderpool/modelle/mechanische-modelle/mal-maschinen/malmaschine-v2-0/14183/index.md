---
layout: "image"
title: "Malmaschine2-04"
date: "2008-04-07T07:56:03"
picture: "malmaschinev04.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14183
- /details411f.html
imported:
- "2019"
_4images_image_id: "14183"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14183 -->
Gesamtansicht von allen Seiten