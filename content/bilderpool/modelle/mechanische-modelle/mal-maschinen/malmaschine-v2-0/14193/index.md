---
layout: "image"
title: "Malmaschine2-14"
date: "2008-04-07T07:56:04"
picture: "malmaschinev14.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14193
- /detailse6ba.html
imported:
- "2019"
_4images_image_id: "14193"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14193 -->
Zeichenergebnis 2