---
layout: "image"
title: "Malmaschine2-03"
date: "2008-04-07T07:56:03"
picture: "malmaschinev03.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14182
- /details5c81.html
imported:
- "2019"
_4images_image_id: "14182"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14182 -->
Gesamtansicht von allen Seiten