---
layout: "comment"
hidden: true
title: "6074"
date: "2008-03-25T22:41:09"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist also sozusagen eine Erweiterung der Zeichenmaschine aus dem alten Clubheft 1969-3 um gleich mehrere Freiheitsgrade. Sehe ich das richtig, dass Du also den Antrieb auf diesem Bild verstellen und damit die Auslenkungsgröße verändern kannst, und dass in der Mitte das ganze nochmal in die andere Richtung hin- und her-versetzt wird?

Gruß,
Stefan