---
layout: "image"
title: "Malmaschine09"
date: "2008-03-25T17:56:41"
picture: "malmaschine07.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14111
- /details9a65.html
imported:
- "2019"
_4images_image_id: "14111"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14111 -->
Getriebe X-Richtung