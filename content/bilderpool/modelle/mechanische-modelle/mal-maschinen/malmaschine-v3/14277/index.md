---
layout: "image"
title: "Malmaschine V3"
date: "2008-04-18T21:08:54"
picture: "malmaschinevderkompaktograph01.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14277
- /details5f59.html
imported:
- "2019"
_4images_image_id: "14277"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-18T21:08:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14277 -->
Ansicht von oben