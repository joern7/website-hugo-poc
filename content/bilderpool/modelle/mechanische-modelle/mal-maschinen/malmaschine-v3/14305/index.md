---
layout: "image"
title: "PICT3923 Kopie"
date: "2008-04-19T14:36:42"
picture: "PICT3923_Kopie.jpg"
weight: "16"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14305
- /detailsc2a2.html
imported:
- "2019"
_4images_image_id: "14305"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14305 -->
Eine Drehscheibe um 90° gedreht.