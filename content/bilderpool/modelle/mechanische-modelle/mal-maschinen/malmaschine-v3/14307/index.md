---
layout: "image"
title: "PICT3925 Kopie"
date: "2008-04-19T14:36:42"
picture: "PICT3925_Kopie.jpg"
weight: "18"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14307
- /detailsb592-3.html
imported:
- "2019"
_4images_image_id: "14307"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14307 -->
Einstellung wie im Bild davor, mit Tellerdrehung.