---
layout: "comment"
hidden: true
title: "6354"
date: "2008-04-28T23:36:07"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Die schwarze Figur ist eine Figur.
Der Drehtisch lief 4 mal schneller...
... oder langsamer.

Bei den meisten anderen Grafiken liefen alle Drehkränze Z58 synchron.

Voraussagen kann ich nichts.
Die Planetengetriebe stehen anfangs in gleicher Position. Danach drehe ich ein Getriebe um 90° oder 180°.

Gruß,
Frank