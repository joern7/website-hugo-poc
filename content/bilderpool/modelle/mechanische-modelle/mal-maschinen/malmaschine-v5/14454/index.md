---
layout: "image"
title: "Seitenansicht"
date: "2008-05-04T12:53:13"
picture: "PICT4090.jpg"
weight: "26"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14454
- /details2c01-2.html
imported:
- "2019"
_4images_image_id: "14454"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-05-04T12:53:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14454 -->
Seitenansicht auf das Planetengetriebe mit Standart-Zahnräder Z40 und Z10