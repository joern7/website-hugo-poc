---
layout: "comment"
hidden: true
title: "6388"
date: "2008-05-03T12:26:41"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Klar, ich mach noch ein paar Fotos. Der Rollenbock dient der Kabelbefestigung und sitzt auf dem Foto unter den Drehscheiben und das ist FALSCH.Die Drehscheiben sind frei beweglich und übertragen die Kraft vom linken auf's rechte Modul. Der U-Träger 150 ist genauso wackelig wie der Statikträger, das liegt an den "Statiklochungen". Ich verwende jetzt Alus für den Arm. Die Drehkränze haben am Eingriff der Schneckenwelle nur ein paar Zehntel Millimeter Spiel. Das Planetengetriebe muss Exakt mittig sein und etwas Spiel haben, sonst klemmt es.

Schönen Gruß
Frank