---
layout: "comment"
hidden: true
title: "6408"
date: "2008-05-05T12:04:32"
uploadBy:
- "Fredy"
license: "unknown"
imported:
- "2019"
---
Das sieht sehr gut aus !

Wie hast du ds mit den verschieden Farben hin bekommen ?
Hast du die Maschine mitten in einem Zeichenvorgang gestoppt und die Farbe gewechselt oder hast du zwei Durchgänge laufen lassen mit jeweils unterschiedlichen Farben ?

Gruß Frederik