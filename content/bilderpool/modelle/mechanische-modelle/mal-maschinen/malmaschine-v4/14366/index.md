---
layout: "image"
title: "Malmaschine V4"
date: "2008-04-23T17:06:42"
picture: "malmaschinevderplanetograph07.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14366
- /details13cd.html
imported:
- "2019"
_4images_image_id: "14366"
_4images_cat_id: "1329"
_4images_user_id: "729"
_4images_image_date: "2008-04-23T17:06:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14366 -->
Detail Planetengetriebemodul
Leider habe ich (momentan) nur einen Drehkranz Z58. So reicht's nur für ein Planetengetriebe.