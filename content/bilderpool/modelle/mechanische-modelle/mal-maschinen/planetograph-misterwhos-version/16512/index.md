---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:02"
picture: "bildergalerie05.jpg"
weight: "14"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Joachim Jacobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/16512
- /detailsb723.html
imported:
- "2019"
_4images_image_id: "16512"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16512 -->
Gleiche Einstellungen wie ein Bild weiter. Hier wurde gewartet bis der Stift einmal komplett "rum" ist.
Die unterschiedlichen Figuren kommen nur durch Änderung der Übersetzungen zu stande. Meistens ist die Drehzahl des Tisches geändert worden.