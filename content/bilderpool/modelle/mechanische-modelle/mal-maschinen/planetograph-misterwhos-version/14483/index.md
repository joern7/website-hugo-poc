---
layout: "image"
title: "Gesamtansicht"
date: "2008-05-06T16:31:08"
picture: "planetographmwversion4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/14483
- /details8e52.html
imported:
- "2019"
_4images_image_id: "14483"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-06T16:31:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14483 -->
