---
layout: "comment"
hidden: true
title: "6462"
date: "2008-05-10T07:44:22"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

äußerst saubere Linien erlauben Tuschestifte auf Transparentpapier. Dann braucht es aber spielfreie Stiftführungen, sonst sieht man das Umkehrspiel in der Linie gnadenlos.