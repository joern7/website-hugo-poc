---
layout: "image"
title: "Malmaschinev3 Mini-03"
date: "2009-01-07T21:22:47"
picture: "malmaschinevmini3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16951
- /details46e9.html
imported:
- "2019"
_4images_image_id: "16951"
_4images_cat_id: "1526"
_4images_user_id: "729"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16951 -->
von links hinten