---
layout: "image"
title: "Greifer"
date: "2010-10-01T14:51:17"
picture: "greifer1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: ["35977"]
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/28793
- /detailseaf0.html
imported:
- "2019"
_4images_image_id: "28793"
_4images_cat_id: "2096"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T14:51:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28793 -->
Wie in diesen Modellen verwendet:
http://www.ftcommunity.de/categories.php?cat_id=1169
http://www.ftcommunity.de/categories.php?cat_id=2006

Nochmal als Detail-Ansichten zum besseren Nachbauen