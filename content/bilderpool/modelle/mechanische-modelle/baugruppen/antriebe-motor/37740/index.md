---
layout: "image"
title: "Antrieb9519.jpg"
date: "2013-10-19T17:34:58"
picture: "IMG_9519.JPG"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37740
- /detailsa21f.html
imported:
- "2019"
_4images_image_id: "37740"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37740 -->
Getriebe 31069 und Drehschieber 31070 haben hier noch einmal zueinander gefunden.