---
layout: "image"
title: "Antrieb8862.JPG"
date: "2013-10-19T16:41:20"
picture: "IMG_8862.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37728
- /detailse7dd-2.html
imported:
- "2019"
_4images_image_id: "37728"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37728 -->
