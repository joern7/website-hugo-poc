---
layout: "image"
title: "xm_2051.JPG"
date: "2010-01-27T19:18:26"
picture: "xm_2051.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26171
- /details8c24-2.html
imported:
- "2019"
_4images_image_id: "26171"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:18:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26171 -->
Hier der XM-Motor mit dem Stufengetriebe des M-Motors. Wer genau hinschaut, sieht einen Achsstummel in Verlängerung des Z10 auf der Motorachse. Es geht auch ohne. Der Stummel stammt von einer Rastachse passender Länge und hat vorne (hier rechts) eine umlaufende Nut, so dass er sauber in die Aufnahme des Stufengetriebes einrastet und so leicht dreht wie die Zahnräder, die da sonst hinein gehören.