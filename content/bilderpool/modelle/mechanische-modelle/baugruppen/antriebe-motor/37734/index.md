---
layout: "image"
title: "Antrieb0021.JPG"
date: "2013-10-19T17:10:45"
picture: "IMG_0021.JPG"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37734
- /detailsa77e.html
imported:
- "2019"
_4images_image_id: "37734"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37734 -->
Das Getriebe 31069 fristet so ein Schattendasein in seiner Magazinbox. Hier sieht man auch, weshalb: es sind schon ziemliche Klimmzüge nötig, um damit irgendwas anzustellen. Obendrauf im Einsatz: ein Baustein 30x15x7,5 von Andreas (TST, siehe: http://www.ftcommunity.de/categories.php?cat_id=2623 ). Die zwei Teile unten links verhindern, dass das Getriebe unter Last weg rutscht.