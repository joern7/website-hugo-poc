---
layout: "image"
title: "Antrieb2107.JPG"
date: "2010-01-27T19:09:28"
picture: "IMG_2107.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26167
- /detailsd15e-2.html
imported:
- "2019"
_4images_image_id: "26167"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:09:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26167 -->
Noch flacher dürfte ein Antrieb für den Drehkranz kaum zu machen sein. S-Motor und Bauplatte kommen auf unter 25 mm Bauhöhe.