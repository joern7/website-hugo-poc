---
layout: "image"
title: "Antrieb2106b.jpg"
date: "2010-01-27T19:08:13"
picture: "IMG_2106b.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26166
- /details5123.html
imported:
- "2019"
_4images_image_id: "26166"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:08:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26166 -->
Ein Kurbelantrieb oder für sonst etwas langsames. Die Lasche rechts soll ein Wegbiegen unter Last verhindern, schafft es aber nicht so richtig.