---
layout: "image"
title: "S-Motor mit starker Untersetzung"
date: "2015-05-03T19:52:12"
picture: "S-Motor_mit_Untersetzung_01.jpg"
weight: "36"
konstrukteure: 
- "pinkepunk"
fotografen:
- "pinkepunk"
keywords: ["S-Motor", "Z44", "Untersetzung", "Getriebe"]
uploadBy: "pinkepunk"
license: "unknown"
legacy_id:
- /php/details/40942
- /details1824-2.html
imported:
- "2019"
_4images_image_id: "40942"
_4images_cat_id: "1855"
_4images_user_id: "2431"
_4images_image_date: "2015-05-03T19:52:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40942 -->
Gesucht war eine Lösung für einen leichten Motor mit einer starken Untersetzung bei gleichzeitig hoher Genauigkeit bzw. geringem Spiel. Die Lösung mit den Z44-Zahnrädern aus dem Getriebeblock passt augenscheinlich sehr gut ins Raster. Allerdings ist die Anordnung nicht für größere Lastmomente geeignet. Die Zahnräder rutschen ab einer gewissen Last durch.