---
layout: "image"
title: "Antrieb2110b.jpg"
date: "2010-01-27T19:14:41"
picture: "IMG_2110b.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26169
- /details07da.html
imported:
- "2019"
_4images_image_id: "26169"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:14:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26169 -->
Das ist z.B. etwas für Teleskoptüren. Der Schlitten zwischen den Zahnstangen fährt mit dem halben Tempo der einen davon. Das Zahnrad stammt aus dem Hubgetriebe (oder 75107 von Knobloch) und sitzt auf einer Hubstange. Das gelbe Teil ist auch von ft, wohl irgendwo aus der BSB-Ecke.