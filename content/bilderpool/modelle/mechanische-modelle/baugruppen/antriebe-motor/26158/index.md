---
layout: "image"
title: "Seilwinde2097.jpg"
date: "2010-01-27T18:56:57"
picture: "IMG_2097b.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["31069"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26158
- /details8e25-4.html
imported:
- "2019"
_4images_image_id: "26158"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T18:56:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26158 -->
Hier ging es darum, den Abstand zwischen Schnecke und Zahnrad passend hin zu kriegen. Hat geklappt.