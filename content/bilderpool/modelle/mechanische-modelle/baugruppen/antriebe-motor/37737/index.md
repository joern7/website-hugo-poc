---
layout: "image"
title: "Antrieb0022.JPG"
date: "2013-10-19T17:19:31"
picture: "IMG_0022.JPG"
weight: "30"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37737
- /details6f70.html
imported:
- "2019"
_4images_image_id: "37737"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:19:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37737 -->
Mit einigen Verrenkungen (und abgeschrägten Kanten) kriegt man ein Getriebe 31069 auch für brauchbare Antriebe hin. Der Klemmring 31020 hat zwei Bohrungen, durch die ein Stück Draht gefädelt ist.