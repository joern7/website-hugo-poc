---
layout: "image"
title: "IMG_0043v.JPG"
date: "2013-10-19T16:35:41"
picture: "IMG_0039mit.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37726
- /details503c.html
imported:
- "2019"
_4images_image_id: "37726"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:35:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37726 -->
