---
layout: "image"
title: "Antrieb2104.JPG"
date: "2010-01-27T19:05:12"
picture: "IMG_2104.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26164
- /details93b0.html
imported:
- "2019"
_4images_image_id: "26164"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:05:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26164 -->
Welch ein Gefriemel, bis die passenden Teile (alle im Raster montiert) gefunden waren.