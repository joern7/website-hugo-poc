---
layout: "comment"
hidden: true
title: "10632"
date: "2010-01-28T08:02:10"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Das ist wirklich gut, Respekt!

Deine anderen Baugruppen sind mir alle zu wenig im Raster. Das mag bei einer einzelnen Bauruppe noch passen und gut aussehen, aber später in den Modellen klemmt's dann meist überall.

Gruß, Thomas