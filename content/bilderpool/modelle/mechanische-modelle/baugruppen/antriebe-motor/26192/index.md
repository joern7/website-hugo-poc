---
layout: "image"
title: "XM Motor mit 3 Ganggetriebe"
date: "2010-01-31T18:37:40"
picture: "xmmotor1.jpg"
weight: "17"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26192
- /details82b6.html
imported:
- "2019"
_4images_image_id: "26192"
_4images_cat_id: "1855"
_4images_user_id: "182"
_4images_image_date: "2010-01-31T18:37:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26192 -->
Hier sieht man wie der XM Motor am 3 Ganggetrieb befestigt werden kann.
Die einzige Modifikation besteht in der Madenschraube in der Schnecke um diese auf der Achse des XM befestigen zu können.