---
layout: "image"
title: "xm_2114.jpg"
date: "2010-01-27T19:21:49"
picture: "xm_2114.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26173
- /detailsbcba.html
imported:
- "2019"
_4images_image_id: "26173"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:21:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26173 -->
Und hier der XM-Motor mit dem Stufengetriebe vom M-Motor. Das Stufengetriebe ist gemodded und hat stirnseitig genau in der Mitte ein 4mm-Loch.