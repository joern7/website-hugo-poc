---
layout: "image"
title: "Antrieb2105b.jpg"
date: "2010-01-27T19:06:27"
picture: "IMG_2105b.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26165
- /detailse7f6.html
imported:
- "2019"
_4images_image_id: "26165"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:06:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26165 -->
Noch so ein Teil, wo der richtige Abstand zwischen Schnecke und Zahnrad den Knackpunkt darstellt.