---
layout: "image"
title: "Nachbau Axiallager (4/7) Wälzkäfig"
date: "2008-10-03T09:42:11"
picture: "axiallagernachbau4.jpg"
weight: "4"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15804
- /detailsf315.html
imported:
- "2019"
_4images_image_id: "15804"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15804 -->
Der Wälzkäfig dient mit den roten Rädern 23 zu axialen Kraftübertragung zwischen den Laufringen und mit den schwarzen sowie weißen zur Zentrierung dieses unteren Laufrings mit dem hier noch nicht montierten oberen. Der Umlaufdurchmesser der schwarzen und weißen Räder beträgt Ø204mm. Beim Aufsetzen des Wälzkäfigs auf den unteren Laufring muß also ein Überdeckungsmaß von Ø204mm minus Ø202,5 gleich 1,5mm ausgeglichen werden. Es empfiehlt sich dabei, die schwarzen Räder genau zwischen die Bogenverbindungen zu setzen. Hier ist die Überdeckung dann nur Ø204mm minus Ø203,5mm gleich 0,5mm auf den Dmr. Beim Aufsetzen drückt man die schwarzen Räder mit den Fingern leicht nach innen. 
