---
layout: "image"
title: "Nachbau Axiallager (2/7) Laufringauffederung"
date: "2008-10-03T09:42:11"
picture: "axiallagernachbau2.jpg"
weight: "2"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15802
- /details2733.html
imported:
- "2019"
_4images_image_id: "15802"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15802 -->
Interessant wäre mal von anderen ft-Freunden zu erfahren, wie weit bei ihnen solche Kreisbogen auf waren, bevor sie geschlossen wurden.