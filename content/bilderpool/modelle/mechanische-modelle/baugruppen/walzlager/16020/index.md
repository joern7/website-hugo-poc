---
layout: "image"
title: "(1/8) Drehkranz, Gesamtansicht"
date: "2008-10-20T21:35:34"
picture: "drehkranz1.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16020
- /detailsdd01.html
imported:
- "2019"
_4images_image_id: "16020"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16020 -->
Gesamtansicht des aktuellen Entwicklungsstandes vom Axiallager zum Drehkranz. Die Kette läuft jetzt am Zylinderumfang. Der Führungsring aus der Vorstellung des Axiallagers mit Tauchantrieb ist weggefallen. Der Tauchantrieb wurde für eine größere Tauchtiefe geändert.

Nachtrag 21.10.2008:
Studie, einstellbares Axiallager
http://www.ftcommunity.de/details.php?image_id=15662
Nachbau des Axiallagers von Remadus
http://www.ftcommunity.de/details.php?image_id=15801
Tauchantrieb mit Führungsring zum Axiallager von Remadus
http://www.ftcommunity.de/details.php?image_id=15961