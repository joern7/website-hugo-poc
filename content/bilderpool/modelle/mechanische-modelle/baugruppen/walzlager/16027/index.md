---
layout: "image"
title: "(8/8) Drehkranz als Vertikallager"
date: "2008-10-20T21:35:35"
picture: "drehkranz8.jpg"
weight: "15"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16027
- /details78ee.html
imported:
- "2019"
_4images_image_id: "16027"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16027 -->
Der Drehkranz mit senkrechter Achse auf dem Kopf stehend im Probleauf.