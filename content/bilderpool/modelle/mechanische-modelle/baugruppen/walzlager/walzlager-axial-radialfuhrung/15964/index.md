---
layout: "image"
title: "(4/6) Tauchantrieb"
date: "2008-10-13T23:09:47"
picture: "waelzlageraxialradialfuehrung4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15964
- /detailsa411.html
imported:
- "2019"
_4images_image_id: "15964"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15964 -->
Das Ritzel dieses Tauchantriebes muß radial den Durchmesserunterschied zwischen "Eckenmaß" und "Flankenmaß" der Kette überwinden. Deshalb habe ich den Power-Motor 50:1 auf einen mit 4 Druckfedern 30x5x0,3 vorgespannten Radialschlitten gesetzt.