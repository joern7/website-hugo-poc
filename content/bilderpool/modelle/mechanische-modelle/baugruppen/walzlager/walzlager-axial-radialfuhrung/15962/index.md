---
layout: "image"
title: "(2/6) Laufring unten"
date: "2008-10-13T23:09:47"
picture: "waelzlageraxialradialfuehrung2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15962
- /detailsefd2.html
imported:
- "2019"
_4images_image_id: "15962"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15962 -->
Zunächst habe ich den unteren Laufring mit 2 Riegelsteinen 15x15 gegen Verdrehung gesichert und an den 6 Stoßverbindungen mit I-Streben 15 stabilisiert.
 
