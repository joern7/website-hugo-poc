---
layout: "image"
title: "Kraftheber3-01.JPG"
date: "2005-11-11T12:14:37"
picture: "Kraftheber3-01.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5303
- /detailsc396.html
imported:
- "2019"
_4images_image_id: "5303"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:14:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5303 -->
Die Kegelzahnräder 35062 passen auch mit der Schnecke zusammen. Am seitlichen Abstand muss man hier etwas justieren. Der Trick an diesem Aufbau ist, dass zwei Schnecken gegenläufig arbeiten und damit das Kegelzahnrad nicht aus der Position gedrückt wird.