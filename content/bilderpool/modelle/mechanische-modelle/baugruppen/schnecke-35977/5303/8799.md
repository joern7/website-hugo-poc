---
layout: "comment"
hidden: true
title: "8799"
date: "2009-03-20T11:09:19"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ich arbeite derzeit an einem Fahrzeug, wo ich genau dieses Prinzip als Radantrieb verwenden wollte (Schnecke auf Kegelrad). Ich hatte mich schon mächtig darauf gefreut!

Leider hat sich im praktischen Versuch gezeigt, dass es gar nicht zufriedenstellend funktioniert... :o(

Bei geringem seitlichen Abstand läuft das Kegelrad zwar rund, aber die Reibung ist sehr hoch und manchmal hakelt es an den Flanken der Schnecke, so dass diese mechanisch beansprucht und sogar beschädigt werden.

Bei größerem Abstand funktioniert es, aber die Bewegungsübertragung ist ruckelnd, da das Kegelrad zwischenzeitich keinen Kontakt zur Schnecke hat und stehenbleibt.

Harald, hast Du auch diese Erfahrungen gemacht?

Gruß, Thomas