---
layout: "image"
title: "Kraftheber1-01.JPG"
date: "2005-11-11T11:59:40"
picture: "Kraftheber1-01.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Schnecke", "35977"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5298
- /detailsdbb7-2.html
imported:
- "2019"
_4images_image_id: "5298"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T11:59:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5298 -->
Das könnte mal ein Front-Kraftheber für einen Traktor werden. Die Rast-Z20 sind aus Deckeln für das schwarze Differenzial entstanden (Diese Idee stammt von Frickelsiggi).