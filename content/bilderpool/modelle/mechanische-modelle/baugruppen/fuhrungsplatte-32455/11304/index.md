---
layout: "image"
title: "Vorderachse03.JPG"
date: "2007-08-05T17:18:38"
picture: "Vorderachse03.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11304
- /detailse289.html
imported:
- "2019"
_4images_image_id: "11304"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:18:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11304 -->
Das ist die Vorderseite. Das Z20 treibt über ein "Alternativ-Hubgetriebe" die Zahnstange an. Die Vorderachse pendelt um die Achse, auf der das Z10 sitzt.