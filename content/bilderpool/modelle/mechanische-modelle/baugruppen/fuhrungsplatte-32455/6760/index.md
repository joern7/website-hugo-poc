---
layout: "image"
title: "32455-Gleit01.JPG"
date: "2006-08-29T21:21:47"
picture: "32455-Gleit01.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6760
- /detailsc801.html
imported:
- "2019"
_4images_image_id: "6760"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:21:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6760 -->
Wenn es nur um eine Gleitführung (ohne Antrieb) geht, ist das Teil auch hilfreich.

Hier ist der dazu passende Faden im alten ft-Forum, zum Thema "Gabelstapler":

http://www.fischertechnik.de/de/fanclub/forumalt/topic.asp?TOPIC_ID=1131&FORUM_ID=12&CAT_ID=2&Topic_Title=RC+Gabelstapler&Forum_Title=Modellideen