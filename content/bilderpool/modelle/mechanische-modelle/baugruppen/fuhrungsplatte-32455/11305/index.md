---
layout: "image"
title: "Vorderachse04.JPG"
date: "2007-08-05T17:20:40"
picture: "Vorderachse04.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11305
- /details710c-2.html
imported:
- "2019"
_4images_image_id: "11305"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:20:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11305 -->
Immer diese Sucherei und Fummelei, um die Räder bei Geradeausfahrt halbwegs parallel zu kriegen.

Es wird allmählich Zeit, dass ft mal eine stufenlos (oder von mir aus auch ganz fein gestufte) variable Spurstange herausbringt.