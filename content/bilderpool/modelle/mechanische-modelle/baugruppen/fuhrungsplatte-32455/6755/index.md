---
layout: "image"
title: "32455-Hub07.JPG"
date: "2006-08-29T21:13:13"
picture: "32455-Hub07.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6755
- /details9407.html
imported:
- "2019"
_4images_image_id: "6755"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:13:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6755 -->
Fertig zusammengebaut.