---
layout: "image"
title: "32455-Hub02.JPG"
date: "2006-08-29T21:10:44"
picture: "32455-Hub02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6753
- /detailsab97.html
imported:
- "2019"
_4images_image_id: "6753"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:10:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6753 -->
Der Nagel von der Gegenseite dient dazu, die Zahnrädchen auszurichten, so dass ihre Nuten innen schön in Reihe ausgerichtet sind.

Die Führung für die ft-Zahnstangen ist hier schon fertig zusammengebaut.