---
layout: "image"
title: "32455-Heli01.JPG"
date: "2006-08-29T21:26:39"
picture: "32455-Heli01.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Rotorkopf", "Hubschrauber", "Taumelscheibe", "Speichenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6761
- /detailsafa7.html
imported:
- "2019"
_4images_image_id: "6761"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:26:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6761 -->
Mit noch ein, zwei verregneten Wochenenden könnte das der Kern für einen Hubschrauber werden.

Die Taumelscheibe sorgt je nach Neigung und mittlerer Höhe dafür, dass die Rotorblätter richtig angestellt werden. Die Rotorblätter müssen dazu drehfest auf der Achse gelagert und per Kipphebel mit den außen herum kreisenden Blöcken aus BS15-Loch und 32455 verbunden werden.