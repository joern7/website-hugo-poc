---
layout: "image"
title: "32455-Schalt01.JPG"
date: "2006-08-29T21:16:27"
picture: "32455-Schalt01.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6758
- /details7cb0.html
imported:
- "2019"
_4images_image_id: "6758"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:16:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6758 -->
Szenenwechsel. Hier dient das 32455, um den ft-Taster möglichst kompakt zu einem ft-Schalter zu machen.