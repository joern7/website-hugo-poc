---
layout: "image"
title: "Pneumatisch abschaltbare Handkurbel"
date: "2009-12-06T19:40:50"
picture: "pneukurbl1.jpg"
weight: "1"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/25913
- /details438b.html
imported:
- "2019"
_4images_image_id: "25913"
_4images_cat_id: "1822"
_4images_user_id: "430"
_4images_image_date: "2009-12-06T19:40:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25913 -->
Da mir gerade die Motoren ausgehen, suche ich nach alternativen. Wenn man zum Beispiel einen Portalroboter damit betreiben möchte kann man durch die grünen Lampen die Soll-Drehrichtung anzeigen lassen. Die roten Lampen würden dann aufleuchten, wenn man den Punkt erreicht hat. Zusätzlich würde dann noch ein Ventil geschaltet werden, was dann die Kurbel von der restlichen Anlage trennt. Somit kann dann der Punkt nicht überfahren werden. Lässt der pneumatische Druck wieder nach, drückt eine Feder die bewegliche Mechanik wieder nach unten.