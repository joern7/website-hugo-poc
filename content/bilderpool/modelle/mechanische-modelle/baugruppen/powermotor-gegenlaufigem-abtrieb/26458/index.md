---
layout: "image"
title: "Abtrieb"
date: "2010-02-16T11:28:38"
picture: "powermotormitgegenlaeufigemabtrieb2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26458
- /details47c0.html
imported:
- "2019"
_4images_image_id: "26458"
_4images_cat_id: "1882"
_4images_user_id: "104"
_4images_image_date: "2010-02-16T11:28:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26458 -->
Das Ritzel in der Mitte kommt vom PowerMotor, die beiden anderen sind die Abtriebe. Das ist extrem stabil gebaut und kann hohe Drehmomente abgeben.