---
layout: "image"
title: "Innerer Aufbau (1)"
date: "2010-02-16T11:28:38"
picture: "powermotormitgegenlaeufigemabtrieb4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26460
- /details5772.html
imported:
- "2019"
_4images_image_id: "26460"
_4images_cat_id: "1882"
_4images_user_id: "104"
_4images_image_date: "2010-02-16T11:28:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26460 -->
Und hier weitere Details. Man benötigt ein Verbindungsstück 15x30x5 in der alten Bauform mit zwei vollrunden Nuten.