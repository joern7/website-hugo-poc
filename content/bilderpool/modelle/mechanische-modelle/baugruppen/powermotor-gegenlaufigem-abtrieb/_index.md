---
layout: "overview"
title: "PowerMotor mit gegenläufigem Abtrieb"
date: 2020-02-22T08:14:13+01:00
legacy_id:
- /php/categories/1882
- /categoriesc699.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1882 --> 
Hier wird ein PowerMotor so verbaut, dass er extrem robust zwei gegenläufig drehende Achsen antreibt.