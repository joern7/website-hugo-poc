---
layout: "comment"
hidden: true
title: "12611"
date: "2010-10-25T19:41:22"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das mit den Abständen kommt mir aber komisch vor: Beide Typen sind doch dafür gedacht, ein Differential anzutreiben, müssen also doch dieselbe Geometrie haben?

Gruß,
Stefan