---
layout: "image"
title: "Innerer Aufbau (2)"
date: "2010-02-16T11:28:38"
picture: "powermotormitgegenlaeufigemabtrieb5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26461
- /details52bb-2.html
imported:
- "2019"
_4images_image_id: "26461"
_4images_cat_id: "1882"
_4images_user_id: "104"
_4images_image_date: "2010-02-16T11:28:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26461 -->
Hier die Innereien von der PowerMotor-Seite aus gesehen und teilzerlegt. Der PowerMotor hält sich an den vier Federnocken bombenfest.