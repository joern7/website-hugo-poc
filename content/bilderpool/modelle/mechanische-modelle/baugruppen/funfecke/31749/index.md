---
layout: "image"
title: "5Eck_A_5725"
date: "2011-09-05T19:58:23"
picture: "5eck-A_5725m.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31749
- /details531e.html
imported:
- "2019"
_4images_image_id: "31749"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T19:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31749 -->
Ein Blick von oben hinein. Die Haken und die Schwungscheibe waren nur Notbehelf, weil die Anordnung insgesamt zu weich ist und sich unter Last verbiegt.