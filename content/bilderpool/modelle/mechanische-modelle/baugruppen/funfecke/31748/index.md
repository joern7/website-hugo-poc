---
layout: "image"
title: "5Eck_A_5723"
date: "2011-09-05T19:56:39"
picture: "5eck-A_5723m.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31748
- /details81d0.html
imported:
- "2019"
_4images_image_id: "31748"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T19:56:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31748 -->
Die Geometrie mit den Streben ist nicht zu 100% aufgegangen, deshalb ist das Fünfeck nicht ganz symmetrisch, und an einer Seite muss die Strebe I-30 den Rest überbrücken. Oben rum passt aber alles