---
layout: "image"
title: "5Eck_6024"
date: "2011-09-05T20:12:51"
picture: "IMG_6024m.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31753
- /details0930.html
imported:
- "2019"
_4images_image_id: "31753"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T20:12:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31753 -->
... oder noch kleiner mit einer Schneckenspannzange 31717 innen drin.