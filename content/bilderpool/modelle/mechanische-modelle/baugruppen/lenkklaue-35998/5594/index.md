---
layout: "image"
title: "35998-05.JPG"
date: "2006-01-16T18:03:01"
picture: "35998-05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5594
- /details940f.html
imported:
- "2019"
_4images_image_id: "5594"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:03:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5594 -->
Wenn es einmal an Gelenkwürfeln mangelt...
Aber auch nicht nur dann: in Richtung "oben" und "unten" hat man hier Zapfen und kann weiter anbauen.