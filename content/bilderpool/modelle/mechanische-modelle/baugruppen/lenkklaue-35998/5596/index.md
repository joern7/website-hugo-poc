---
layout: "image"
title: "35998-07.JPG"
date: "2006-01-16T18:07:40"
picture: "35998-07.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Weitwinkel"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5596
- /details5703.html
imported:
- "2019"
_4images_image_id: "5596"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5596 -->
Dieses Gelenk hat über 270° Winkelbereich zwischen linkem und rechtem Anschlag. Beim Gelenkwürfel 31426+31436 sind es gerade einmal 180°.