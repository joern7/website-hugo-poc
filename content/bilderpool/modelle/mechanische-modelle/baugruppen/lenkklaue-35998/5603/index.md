---
layout: "image"
title: "Container-C01.JPG"
date: "2006-01-16T18:15:14"
picture: "Container-C01.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5603
- /details2ffa.html
imported:
- "2019"
_4images_image_id: "5603"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5603 -->
Wo ich gerade so im Schwung war... Variante "C" für ein Hubgeschirr.