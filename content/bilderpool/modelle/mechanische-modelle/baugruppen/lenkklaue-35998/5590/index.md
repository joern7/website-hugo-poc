---
layout: "image"
title: "35998-01.JPG"
date: "2006-01-16T17:55:13"
picture: "35998-01.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5590
- /detailsa26b.html
imported:
- "2019"
_4images_image_id: "5590"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T17:55:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5590 -->
Das Rad 23 (34994 etc.) passt in die Lenkklaue, als wären die beiden füreinander geschaffen.

(Siehe auch die Schubkarre unter
http://www.ftcommunity.de/details.php?image_id=3307 )