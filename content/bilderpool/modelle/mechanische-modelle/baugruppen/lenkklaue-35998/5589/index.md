---
layout: "image"
title: "35998-02.JPG"
date: "2006-01-16T17:53:55"
picture: "35998-02.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5589
- /details6845.html
imported:
- "2019"
_4images_image_id: "5589"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T17:53:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5589 -->
Das Zahnrad ist von der "alten" Seilwinde. Passen tut es prima, für eine konkrete Verwendung fehlt noch das Problem.