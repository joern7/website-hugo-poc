---
layout: "image"
title: "Container-A03.JPG"
date: "2006-01-16T18:12:06"
picture: "Container-A03.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5599
- /details1b1b-2.html
imported:
- "2019"
_4images_image_id: "5599"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:12:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5599 -->
Ausführung "A" des Container-Geschirrs von unten.