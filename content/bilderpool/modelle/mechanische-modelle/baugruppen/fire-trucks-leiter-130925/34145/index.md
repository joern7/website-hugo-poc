---
layout: "image"
title: "Befestigungsmöglichkeiten"
date: "2012-02-12T14:48:21"
picture: "Befestigung_2x155.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34145
- /detailsafdb.html
imported:
- "2019"
_4images_image_id: "34145"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34145 -->
Unter der Leiter ist gerade genug Platz für 2x15 und 1x5 Baustein. Sie wird gegen seitliches verrutschen fest gehalten.