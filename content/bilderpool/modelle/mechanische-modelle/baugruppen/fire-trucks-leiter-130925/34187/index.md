---
layout: "image"
title: "Befestigung"
date: "2012-02-15T14:35:41"
picture: "DSCN4552.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34187
- /details6ff8.html
imported:
- "2019"
_4images_image_id: "34187"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-15T14:35:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34187 -->
Zwei Leitern gleiten aufeinander.