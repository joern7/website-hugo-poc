---
layout: "image"
title: "Befestigung"
date: "2012-02-15T13:05:32"
picture: "DSCN4525.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34185
- /details9995.html
imported:
- "2019"
_4images_image_id: "34185"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-15T13:05:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34185 -->
Hier eine andere Art der Befestigung.