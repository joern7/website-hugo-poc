---
layout: "image"
title: "zwei Leitern als ausfahrbare Rampe"
date: "2012-02-24T18:19:38"
picture: "DSCN4608.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34391
- /details756e.html
imported:
- "2019"
_4images_image_id: "34391"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-24T18:19:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34391 -->
Hier habe ich zwei Leitern so eingesetzt das man sie als Rampe nutzen kann. Das tolle dabei ist, die Rampe ist ausfahrbar. Die obere Leiter gleitet auf der unteren nach vorn. Angetrieben durch einen Klemmring für die alte Seiltrommel. Dafür habe ich echt lange überlegen müssen. Zuerst wolle ich den Antrieb so gestalten wie auf den vorigen Bildern. Das klappte aber nicht. Der Antriebsstrang wurde zu lang. Und dann hakte es dauernd.