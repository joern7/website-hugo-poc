---
layout: "image"
title: "Kurze dicke Variante (1)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24697
- /detailsef93.html
imported:
- "2019"
_4images_image_id: "24697"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24697 -->
Wenn man in der Länge wenig Platz hat, dafür aber etwas in die Breite gehen kann, geht's auch so.