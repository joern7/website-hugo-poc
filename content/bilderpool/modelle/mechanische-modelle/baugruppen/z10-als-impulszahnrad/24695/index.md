---
layout: "image"
title: "Lange schmale Variante (2)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24695
- /details6005-3.html
imported:
- "2019"
_4images_image_id: "24695"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24695 -->
Auf dieser Seite kann natürlich anstatt des Rast-Winkelzahnrads auch alles andere mit Rastachsen angebracht werden.