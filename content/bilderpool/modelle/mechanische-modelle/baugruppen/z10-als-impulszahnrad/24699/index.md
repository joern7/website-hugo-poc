---
layout: "image"
title: "Kurze dicke Variante (3)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24699
- /detailsc21b.html
imported:
- "2019"
_4images_image_id: "24699"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24699 -->
Blick aufs Gelenk.