---
layout: "comment"
hidden: true
title: "17376"
date: "2012-10-05T11:55:49"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

dass die Kettenglieder so schön zur Führung in die Kugelbahnflexschienen passen, dürfte sich um ein echtes Zufallsprodukt handeln.
Umso mehr ist es eben aber auch genial, erst einmal darauf zu kommen und dies so umzusetzen!
Als weitere Anwendungsmöglichkeiten könnte man sich ein durch eine Kette geführtes Karussell oder eine zum Materialfluss dienende Kette (Kettenförderer), in einem Industriemodell, vorstellen.

Gruß

Lurchi