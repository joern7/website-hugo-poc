---
layout: "image"
title: "Ansicht"
date: "2007-10-08T14:12:09"
picture: "DSCN1675.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12168
- /details1fd0.html
imported:
- "2019"
_4images_image_id: "12168"
_4images_cat_id: "1089"
_4images_user_id: "184"
_4images_image_date: "2007-10-08T14:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12168 -->
Na ja, für ein Auto ist das nix. Aber wenn man mal eine Dampfwalze oder ähnliches bauen möchte dann geht das schon.
Leider rutschen die Z30 sehr gern aus den Innenzahnrädern heraus. Deswegen habe ich dort, auf beiden Seiten, diese 15er Streben angebracht. Da gibt es bestimmt schönere Lösungen.