---
layout: "image"
title: "Motor von Davenport 1837, Original und Funktionsmodell"
date: "2016-12-13T18:12:41"
picture: "Motor_von_Davenport_1837.jpg"
weight: "9"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["historische", "Elektromotore"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44897
- /detailse5e9.html
imported:
- "2019"
_4images_image_id: "44897"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-12-13T18:12:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44897 -->
Da Fischertechnik keine einzelnen Spulen zur Verfügung stellt, habe ich die Elektromagnete senkrecht eingebaut, so dass nur der obere Pol wirksam ist. Sie sind alle in Reihe geschaltet, die Fußpunkte sind jeweis mit einer Achse des Stromwenders (Kollektor, Kommutator) verbunden. Statt eines &#8222;großen Ringmagneten&#8220; im Original habe ich 2 kleine Magnete in N-S-Ausrichtung installiert. Der Stromwender besteht aus 4 Achsen, die Schleifkontakte aus 2 weiteren Achsen, die von 2 Scharnieren (36329) an den Stromwender gedrückt werden. Wegen der Reihenschaltung der Elektromagnete (es sind immer 2 aktiv) muss die Versorgungsspannung etwa 12 V betragen, damit der Motor gut läuft. Er erreicht dann etwa 200 U/min.
Und &#8211; ja, mir sind die Winkelsteine ausgegangen :-(