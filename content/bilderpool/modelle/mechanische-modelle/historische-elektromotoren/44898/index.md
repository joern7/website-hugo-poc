---
layout: "image"
title: "Motor von Davenport 1837, Schaltbild des Funktionsmodells"
date: "2016-12-14T16:53:58"
picture: "Schaltung_Davenport_Motor_800px.jpg"
weight: "10"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44898
- /detailsd0a0-2.html
imported:
- "2019"
_4images_image_id: "44898"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-12-14T16:53:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44898 -->
Ein &#8222;freihändiges&#8220; Schaltbild