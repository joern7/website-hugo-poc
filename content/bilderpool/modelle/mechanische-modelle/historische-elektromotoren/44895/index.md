---
layout: "image"
title: "Motor von Davenport 1837, Stromwender und Schleifkontakte"
date: "2016-12-13T18:12:41"
picture: "Stromwender_und_Schleifkontakte.jpg"
weight: "7"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44895
- /details7bac.html
imported:
- "2019"
_4images_image_id: "44895"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-12-13T18:12:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44895 -->
