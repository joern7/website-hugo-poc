---
layout: "comment"
hidden: true
title: "22765"
date: "2016-11-26T21:19:44"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
ja, alle 4 Statormagnete sind parallel geschaltet und werden über den separaten Umschalter mit Spannung versorgt.
Je 4 Kontakte des Stromwenders (Drehschalter) sind miteinander und einer Seite der parallel geschalteten Rotormagnete verbunden. Wichtig ist, dass die Schleifkontakte um 45° versetzt angeordnet werden.
Siehe auch mein Schaltbild, sobald es vom Admin freigeschaltet wird.

Viele Grüße
Rüdiger