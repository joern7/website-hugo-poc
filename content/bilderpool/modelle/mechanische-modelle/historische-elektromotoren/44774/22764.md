---
layout: "comment"
hidden: true
title: "22764"
date: "2016-11-26T12:04:07"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Beim Nachbau ist zu beachten, dass sich die Pole von Stator und Rotor alle 45° gegenüberstehen müssen .............

Sind alle 4 Magneten der Stator parallel geschaltet ?

Beim Rotor gibt es 8/2 = 4 gegen über stehende Kontakten. Hat jeder gegenüber stehende Kontakten-Paar eine Elektromagnet am Rotor ?

Gruss,

Peter Poederoyen NL