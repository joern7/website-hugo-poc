---
layout: "image"
title: "Codierung"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24444
- /detailsb719-2.html
imported:
- "2019"
_4images_image_id: "24444"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24444 -->
Die Codierung der Drehscheiben für die vier Taster ergibt sich so:

1. Das Problem bei einem Binärcode ist, dass zwischen zwei Stunden mehrere Taster ihren Zustand ändern müssten. Da die das wegen winziger Justageunterschiede nicht exakt zur gleichen Zeit tun werden, könnten falsche Zwischenwerte abgelesen und die Positionierung unzuverlässig werden.

2. Dieses Problem löst der Gray-Code, bei dem sich zwischen zwei Positionen immer nur genau ein Bit ändert. So kann es niemals falsche Zwischenzustände geben und die Positionserkennung ist immer zuverlässig.

3. Nun brauchen wir für eine 12-Stunden-Uhr nur 12 und nicht 16 verschiedene Werte. Nimmt man nun die *mittleren* 12 Positionen, ist immer noch beim Wechsel zwischen der letzten (11 Uhr) zurück zur ersten Position (0 Uhr) nur ein Bit umzuschalten: Nur Bit 3 ändert sich (von 1 zurück auf 0), aber die Bits 0 bis 2 bleiben unverändert. So ist also auch hier eine Falscherkennung unmöglich gemacht. (Hier endeten übrigens bis auf das Versetzen von Schritt 5 die Überlegungen, die zu meinem Vorgängermodell aus den 1980ern angestellt wurden.)

4. Da ja Drehscheiben mit Bausteinen 15 und Winkelsteinen 60 bestückt werden müssen, kann man recht viel Material sparen, wenn man den Code invertiert: Da wo ein Bit nicht gesetzt ist, kommt ein Stein hin, und da wo ein Bit gesetzt sein soll, kommt keiner hin. In der Summe braucht man so erheblich weniger Bausteine weil mehr gesetzte als ungesetzte Bits benötigt werden.

5. Da zwei Taster nicht direkt nebeneinander gebaut werden können, weil sonst kein Platz mehr für die Stecker wäre, wird immer abwechselnd ein Taster links und einer rechts von den Drehscheiben eingebaut. Deshalb müssen Bit 1 und Bit 3 um eine halbe Umdrehung, mithin also um 6 Positionen versetzt werden. Ist das gemacht, erkennt man, dass die beiden mittleren Schaltringe exakt identisch bestückt werden müssen. Man kommt also insgesamt mit nur drei anstatt 4 Bahnen aus; die mittlere Bahn wird von den Tastern für Bit 1 und (auf der anderen Seite) Bit 2 gemeinsam verwendet.