---
layout: "image"
title: "Stellen auf die volle Stunde (1)"
date: "2009-06-23T16:02:25"
picture: "selbststellendeanaloguhr14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24449
- /details0c43.html
imported:
- "2019"
_4images_image_id: "24449"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:25"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24449 -->
Nachdem per Motor auf die passende Stunde (bei aktueller Zeit 9:12 also etwa auf 8:45) gestellt wurde, muss per Magnet so oft eine Minute gestellt werden, bis zunächst die volle Stunde (also 9:00) erreicht ist. Erst dann weiß der Rechner genau, wo die Uhr steht. Der Taster hier wird genau zur vollen Stunde losgelassen.

Man sieht hier auch die beiden Winkelachsen zum minutenweisen Schalten: Die im Vordergrund stellt um einen Zahn weiter, die im Hintergrund ist fest angebracht und sorgt dafür, dass sich das Z30 niemals rückwärts drehen kann.