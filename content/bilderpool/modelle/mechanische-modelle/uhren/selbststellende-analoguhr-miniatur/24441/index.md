---
layout: "image"
title: "Stundenmessung (1)"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24441
- /details82da.html
imported:
- "2019"
_4images_image_id: "24441"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24441 -->
Auf der Achse des schwarzen Z30 sitzen Schaltwalzen in Form von mit 15-mm-Bausteinen und Winkelsteinen 60 bestückten Drehscheiben. Diese werden von insgesamt 4 Tastern abgetastet. Vorhandensein oder nicht-Vorhandensein eines Bausteins bzw. Winkelsteins ergeben 12 konfigurierbare Bit pro Umdrehung - entsprechend den 12 Stunden. Hier sieht man das erste Bit, welches alle 2 Stunden seinen Zustand wechselt: Immer zwei Stellungen sind bestückt, danach kommen zwei unbestückte.