---
layout: "image"
title: "Schnelles Stellen (1)"
date: "2009-06-23T16:02:19"
picture: "selbststellendeanaloguhr10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24445
- /details7471.html
imported:
- "2019"
_4images_image_id: "24445"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:19"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24445 -->
Beim Programmstart wird dieser Motor eingeschaltet, der über das dahinter sichtbare Freilaufgetriebe und ein Z40 das Z30 antreibt, welches wiederum das Uhrwerk treibt. Der Minutenzeiger dreht sich dabei mehrmals pro Sekunde. Der Motor läuft solange, bis die Stundenabtastung eine Stunde vor der aktuellen meldet. Ist es z. B. beim Programmstart 9:12 Uhr, läuft dieser Motor, bis der Stundenzeiger auf 8 Uhr irgendwas steht. Durch Justierung der Codierscheiben wird erreicht, dass dieser Zustand einige Minuten vor der nächsten Stunde erreicht ist, also wenn die Uhr etwa auf 8:45 steht. Dann hält der Motor an und die Feinjustage erfolgt. Das wird später noch beschrieben.