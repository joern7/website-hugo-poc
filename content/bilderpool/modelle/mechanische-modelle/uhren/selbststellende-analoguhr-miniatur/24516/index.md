---
layout: "image"
title: "Details zum Uhrwerk (1)"
date: "2009-07-08T21:11:43"
picture: "detailszumuhrwerk1.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24516
- /details0f58.html
imported:
- "2019"
_4images_image_id: "24516"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24516 -->
Zur Dokumentation reiche ich noch ein paar Detailaufnahmen nach, die beim Abbau der Uhr entstanden.

Man blickt hier von vorne unten unter die 12:1-Untersetzung.