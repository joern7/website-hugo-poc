---
layout: "image"
title: "Details zum Uhrwerk (3)"
date: "2009-07-08T21:11:44"
picture: "detailszumuhrwerk3.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24518
- /details6e9f-2.html
imported:
- "2019"
_4images_image_id: "24518"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24518 -->
Von vorne unten links betrachtet.