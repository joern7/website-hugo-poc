---
layout: "image"
title: "Uhrwerk (3)"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24440
- /detailsa79e.html
imported:
- "2019"
_4images_image_id: "24440"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24440 -->
Hier sieht man (links) das im vorigen Bild angesprochene zweite Z44, welches wiederum über ein auf derselben Achse befindliches Z22 das dritte Z44 antreibt. Das wiederum geht über das Z10 ganz links auf das Z30, an dem der Stundenzeiger angebracht ist.

Außerdem geht das schwarze Z10 auf ein schwarzes Z30 im Vordergrund. Auch das dreht sich also wie der Stundenzeiger ein Mal alle 12 Stunden.