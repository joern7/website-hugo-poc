---
layout: "image"
title: "Frontansicht"
date: "2009-06-23T16:02:17"
picture: "selbststellendeanaloguhr01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24436
- /details54f1.html
imported:
- "2019"
_4images_image_id: "24436"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24436 -->
Diese kleine Analoguhr wird von einem .NET-Programm gesteuert und stellt sich beim Programmstart automatisch auf die aktuelle Uhrzeit. Das Ziffernblatt ist recht klein, die Zeiger ganz primitiv (der kleine Stundenzeiger ist nur eine mit einem Statikstopfen am Z30 befestigte I-Strebe 15).