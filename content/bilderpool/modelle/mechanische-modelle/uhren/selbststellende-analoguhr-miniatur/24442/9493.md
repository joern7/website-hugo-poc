---
layout: "comment"
hidden: true
title: "9493"
date: "2009-06-26T13:23:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Dankeschön! Ich mag diese Taster auch sehr gerne, vor allem weil sie so leichtgängig sind. Natürlich haben sie mich früher auch immer fasziniert, und es war schon was, das erste Mal selber einen zu haben. ;-)

Gruß,
Stefan