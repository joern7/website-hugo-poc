---
layout: "image"
title: "Uhrwerk (1)"
date: "2009-06-23T16:02:17"
picture: "selbststellendeanaloguhr03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24438
- /details5058.html
imported:
- "2019"
_4images_image_id: "24438"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24438 -->
Die untenliegende Achse treibt das Uhrwerk an. Sie wird von einem Z30 zahnweise angesteuert und dreht sich bei 1 Zahn pro Minute also 2 Mal pro Stunde.

Die Z10-Ritzel haben 22 Zähne, die feinen Zahnräder des alten mot-2-Stufengetriebes 44. Das ergibt eine kompakte Untersetzung 2:1. Die in Bildmitte befindliche Metallachse dreht sich damit genau 1 Mal pro Stunde und trägt den Minutenzeiger

Die Riegelschreibe rechts im Bild fixiert nicht etwa irgendwas, sondern ist tatsächlich ein Zwischenzahnrad, um die richtigen Drehrichtungen zu erreichen. Es wird wieder von einem feinen Z22 angetrieben und geht auf ein dahinterliegendes feines Z44 (siehe nächstes Bild).

Raster? Welches Raster? ;-)