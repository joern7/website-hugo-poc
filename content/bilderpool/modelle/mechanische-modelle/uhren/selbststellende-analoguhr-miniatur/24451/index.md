---
layout: "image"
title: "Stellen auf die volle Stunde (3)"
date: "2009-06-23T16:02:25"
picture: "selbststellendeanaloguhr16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24451
- /details77e4.html
imported:
- "2019"
_4images_image_id: "24451"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:25"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24451 -->
Hier die andere Seite des Hebels. Nachdem auf die volle Stunde gestellt wurde, wird minutenweise so lange weitergestellt, bis die aktuelle Uhrzeit erreicht ist - und danach eben jede Minute einen Zahn weiter.