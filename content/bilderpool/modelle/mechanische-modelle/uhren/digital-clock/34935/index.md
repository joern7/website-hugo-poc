---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock08.jpg"
weight: "8"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34935
- /details10dd-2.html
imported:
- "2019"
_4images_image_id: "34935"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34935 -->
If you block a ring to prevent it from moving, and the axle turns, the blocked rings don't turn, whereas the free ones do. The clock has a system of levers that that block the rings. They are kept in a position to block the rings by an elastic.