---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:08"
picture: "digitalclock13.jpg"
weight: "13"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34940
- /detailsd627.html
imported:
- "2019"
_4images_image_id: "34940"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34940 -->
The axle with the seconds indicator. In order to fix them on the axle I used[ glue to fix a clip 5 and a locking washer to the building block 15 with bore.