---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock09.jpg"
weight: "9"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34936
- /details3b65.html
imported:
- "2019"
_4images_image_id: "34936"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34936 -->
When a levers is pushed by a cam plate (Nockenscheibe) they unblock the same-level rings at the 3 digits (minutes and unit hours) and then the motor can turn the axle and the unblocked rings will turn as well.