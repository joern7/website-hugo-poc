---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock02.jpg"
weight: "2"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34929
- /details3db1.html
imported:
- "2019"
_4images_image_id: "34929"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34929 -->
The back site