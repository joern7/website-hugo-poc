---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock07.jpg"
weight: "7"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34934
- /detailsb4f8.html
imported:
- "2019"
_4images_image_id: "34934"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34934 -->
The 5  rings forming one digit can move freely on the axle they are sitting on, but with some friction, created by clamping the rings between clips. When the axle is driven by a motor, the rings will turn as well, because of the friction.