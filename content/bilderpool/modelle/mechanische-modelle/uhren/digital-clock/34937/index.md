---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:07"
picture: "digitalclock10.jpg"
weight: "10"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34937
- /detailsb253-2.html
imported:
- "2019"
_4images_image_id: "34937"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34937 -->
5 cam plates on an axle, with 72 degrees phase difference to push all 5 levers in one full rotation.