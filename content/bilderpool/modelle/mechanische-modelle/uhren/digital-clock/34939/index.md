---
layout: "image"
title: "Digital Clock v2"
date: "2012-05-12T17:08:07"
picture: "digitalclock12.jpg"
weight: "12"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34939
- /detailsd390.html
imported:
- "2019"
_4images_image_id: "34939"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34939 -->
The position of the cam plates is controlled by a photo transistor seeing a light through the hole in the cam plate.