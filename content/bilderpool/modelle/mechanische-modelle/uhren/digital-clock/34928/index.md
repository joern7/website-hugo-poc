---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock01.jpg"
weight: "1"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34928
- /details0eaa.html
imported:
- "2019"
_4images_image_id: "34928"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34928 -->
Digital Clock driven by 6 S-motors. 4 Motors for the digits, one for the seconds indicator and one for the lever system that enables the rings to move selectively. And a TX controller to manage the thing. Each of the 4 digits consists of 5 triangular rings. By positioning the rings in different ways, all digits can be formed.

Video of clock in action: http://www.youtube.com/watch?v=0QyPH_z0ySE