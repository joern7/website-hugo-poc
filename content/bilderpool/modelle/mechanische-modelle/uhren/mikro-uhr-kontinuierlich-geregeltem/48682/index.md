---
layout: "image"
title: "Elektronik"
date: 2020-05-10T11:37:14+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Unten sitzt ein Netduino 3, und auf diesem befindet sich ein Adafruit Motor Shield V2. Auf dem Netduino läuft das .net nanoFramework (https://nanoframework.net, Open Source auf https://github.com/nanoframework). Darauf läuft das C#-Programm, das sich wiederum auf mein AbstractIO-Framework stützt (https://github.com/steffalk/AbstractIO).

----------

Electronics

At the bottom there is a Netduino 3 carrying an Adafruit motor shield V2. The Netduino runs .net nanoFramework (https://nanoframework.net, open source at https://github.com/nanoframework). On top of that the C# program runs, using my AbstractIO framework (https://github.com/steffalk/AbstractIO).