---
layout: "image"
title: "Schrägansicht von hinten"
date: 2020-05-10T11:36:59+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor, dessen Schnecke man in der Bildmitte gerade noch ausmachen kann, geht über drei 31069 Getriebebock mit Schnecke auf das feine Räderwerk. Der Motor läuft nur mit etwa 1 V Spannung und dreht deshalb langsam und vor allem leise.

----------

Skewed view on backside

The XS motor which you may recognize in this image's middle drives the fine-toothed gear over three 31069 gear blocks with worm gear. The 9 V motor is running at only about 1 V and therefore turns slowly and, most importantly, silently.