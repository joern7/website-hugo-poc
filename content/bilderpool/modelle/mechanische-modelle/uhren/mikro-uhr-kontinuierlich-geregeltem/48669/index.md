---
layout: "image"
title: "Abtastung (1)"
date: 2020-05-10T11:36:58+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Auf die waagerecht liegende metallene Schnecke wird durch den 31306 Federkontakt direkt Spannung gegeben. Auf der Vorderseite (links im Bild) tastet ein zweiter Federkontakt ab, ob die Schneckenwindung gerade vorbeikommt und also den Kontakt schließt. Auf diese Weise kann also an einem der Digital-Eingänge des Microcontrollers (eines Netduino 3) festgestellt werden, wann die mittlere der drei Schneckengetriebe eine Umdrehung vollendet hat. Das sollte laut Getriebe alle 3600 / 22 / 14 Sekunden, also gerundet etwa alle 11,69 Sekunden der Fall sein.

----------

Measurement (1)

Voltage is applied to the metallic, horizontal worm gear, directly through a 31306 spring contact. At the front (left side of this image), a second spring contact detects whether worm's coil is coming by and makes contact. This way it can be measured using a digital input of the micro controller (a Netduino 3) when the middle one of the three worm gears has completed its turn. This should ideally happen each 3600 / 22 / 14 seconds, which is roughly each 11,69 seconds.