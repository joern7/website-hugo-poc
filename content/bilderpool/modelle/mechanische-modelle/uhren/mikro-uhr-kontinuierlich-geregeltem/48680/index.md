---
layout: "image"
title: "Debug-Ausgabe"
date: 2020-05-10T11:37:12+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Software gibt bei jeder Umdrehungen eine Statuszeile aus, die live in der Visual Studio Debug-Ausgabe ankommt. Die Spalten sind:

n = Anzahl der Zyklen seit dem Start der Uhr. Ein Zyklus ist eine Umdrehung der gemessenen Schnecke (also die ca. 11,69 Sekunden).

bounces = Die Anzahl festgestellter und ignorierter Prell-Kontakte bei der Messung.

cycles = Die Anzahl Umdrehungen zwischen dieser und der vorherigen Messung. Das kann größer als 1 sein, wenn mal ein oder mehrere Kontakte ausbleiben.

vi = Die ideale PWM-Stärke (also der Anteil von 9V), die laut aktuellem Kenntnisstand des Programms für genau die Zykluszeit pro Umdrehung benötigt würden. Die wird per gleitendem Durchschnitt gemittelt, sodass Schwankungen durch Ungenauigkeiten der Messung selbst nicht gleich zu großen Ausschlägen führen, die Uhr sich aber sehr wohl an geänderte Bedingungen anpassen kann, etwa der vielleicht etwas leichtere Lauf, wenn der Minutenzeiger auf der rechten Seite der Uhr steht, oder eine Erschwernis durch angesammelten Staub im Getriebe.

t1 = Die Zeit (seit Programmstart) in HH.MM.SS, zu der der Kontakt idealerweise hätte anfallen sollen.

a1 = Die Zeit, zu der der Kontakt tatsächlich anfiel.

early/late by: Die Zeit, um die das Getriebe den idealen Zeitpunkt des Kontakts verfehlt hat, in Sekunden und in Prozent der Zykluszeit.

v = Die neue PWM-Stärke, mit der der Motor für den nächsten Zyklus angetrieben wird. Der wird, wie man sieht, permanent nachgeregelt, damit die Uhr dauerhaft mehr als hinreichend genau läuft.

----------

Debug Output

On each cycle, the software outputs a status line live to the Visual Studio debug output console. The columns are:

n = The number of cycles since the start of the clock. One cycle is one turn of the measured worm (thus the 11,69 seconds already mentioned).

bounces = The number of detected and ignored contact bounces which occurred in the measurement.

cycles = The number of turns between this and the previous measurement. This can be greater than 1 when one or more contacts missed.

vi = The ideal PWM value (that is, the portion of 9V) which, according to the current self-learning of the program, is needed to have one turn in ideal time. This gets averaged using running averages so that variations and inaccuracies in measurement do not lead to large changes, but at the same time the clock is able to adapt to changed conditions, for example due to the slightly easier run when the minutes hand is on the right side or obstacles coming from dust which settled on the gear.

t1 = The time (since program start) in HH.MM.SS at which the contact ideally should have happened.

a1 = The time at which the contact actually occurred.

early/late by: The time interval by which the gear missed the ideal contact time, in seconds and in percent of the cycle time.

v = The new PWM output by which the motor gets driven for the next cycle. As you see, this gets permanently adapted in order to have the clock lastly run more than precise enough.