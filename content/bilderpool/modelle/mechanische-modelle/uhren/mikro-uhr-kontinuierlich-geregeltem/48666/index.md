---
layout: "image"
title: "Abtastung (4)"
date: 2020-05-10T11:36:54+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Natürlich trifft der Motor den genauen Zeitpunkt nur mehr oder weniger genau. Wir haben beim Start der Uhr noch Zielabweichungen von über einer Sekunde, die aber nach wenigen Zyklen auf typisch unter 0,5 Sekunden und meist 0,1 Sekunden oder sogar wenige Millisekunden gesenkt werden können. Da der Algorithmus die Verspätungen, zu frühe Impulse und sogar durch mechanische Toleranzen gar nicht geschehene Kontakte ausgleicht, ergeben sich aber keine sich langfristig aufbauschenden Fehler. Letztlich geht die Uhr immer besser als auf eine Sekunde genau - so genau kann man die Zeiger eh nicht ablesen.

----------

Measurement (4)

Of course, the motor will reach the exact point of next contact only more or less precise. When the clock starts, it misses the goal with errors above one second, but after only a few cycles, the error is driven down to typically less than 0,5 seconds and mostly even only 0,1 seconds or even only a few milliseconds. Because the algorithm deals with pulses coming late, early, or even totally missing due to mechanical tolerances, there are no cumulative errors in the long run. In the end, the clock always runs with errors smaller than one second - a precision which cannot be watched on the hands anyway.