---
layout: "image"
title: "Uhrwerk ohne Beiwerk (3)"
date: 2020-05-10T11:37:03+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

... von rechts (die Federnocke im BS7,5 ist hier überflüssig) und...

----------

Clock gear without requisites (3)

.. from the right side (the small part in the BS7,5 is superfluous) and...
