---
layout: "image"
title: "Getriebe (5)"
date: 2020-05-10T11:37:15+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Damit das Gebilde mit dem Z10 nicht aus dem BS7,5 herausrutschen kann, befinden sich direkt vor dem Z10 eine schwarze 105195 Scheibe 4 15, die von einer klemmenden 35981 Hülse mit Schreibe fixiert wird. Die schwarze Scheibe stützt sich also an den Zähnen des Z15 ab.

----------

Gear (5)

To prevent the Z10 axis falling out of the BS7,5, there is a black 105195 washer 4 15 which gets fixed by a jamming 35981 shell with washer. The black washer stems against the teeth of the Z15.