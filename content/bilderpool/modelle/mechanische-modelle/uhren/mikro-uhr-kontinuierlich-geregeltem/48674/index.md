---
layout: "image"
title: "Uhrwerk ohne Beiwerk (2)"
date: 2020-05-10T11:37:04+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

... von schräg hinten, ...

----------

Clock gear without requisites (2)

... skewed from backside, ...