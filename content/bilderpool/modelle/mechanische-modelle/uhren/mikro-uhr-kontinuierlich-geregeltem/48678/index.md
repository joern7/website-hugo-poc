---
layout: "image"
title: "Prototypen (2)"
date: 2020-05-10T11:37:09+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das Getriebe entspricht aber schon der Endversion.

----------

Prototypes (2)

The gear is already the same as in the final version.