---
layout: "image"
title: "Abtastung (3)"
date: 2020-05-10T11:36:56+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Dabei geht der Algorithmus von einer Startgeschwindigkeit aus (11 % PWM-Anteil von 9 V) und lernt durch gleitende Durchschnittsbildung selbst, wie schnell sich der Motor bei einer gegebenen Spannung tatsächlich dreht und was also die Spannung ist, die ihn ideal schnell laufen lässt.

----------

Measurement (3)

The algorithm starts with an initial speed guess (11 % PWM of 9 V) and is self-learning using running averages, how fast the motor runs for a given voltage and so what the needed voltage is to have it run just as fast as needed.