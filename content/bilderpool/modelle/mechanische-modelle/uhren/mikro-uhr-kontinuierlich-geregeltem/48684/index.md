---
layout: "image"
title: "Getriebe (4)"
date: 2020-05-10T11:37:17+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Schließlich geht es über ein normales Z10 auf ein Klemmzahnrad Z15. Das ergibt die noch fehlende Untersetzung von 2:3, um aus der 1:8 die 1:12 zu bilden, die wir zwischen Minuten -und Stundenzeiger benötigen.

----------

Gear (4)

Finally, things go on over a standard Z10 to a jamming Z15. This give the missing of 2:3 to get 1:12 from the 1:8 reached so far. This is needed between the minutes and hours hand.