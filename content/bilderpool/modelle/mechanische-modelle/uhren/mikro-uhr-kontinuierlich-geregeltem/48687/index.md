---
layout: "image"
title: "Mikro-Uhr mit kontinuierlich geregeltem Gleichstrommotor"
date: 2020-05-10T11:37:20+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Analoguhr, wie sie analoger kaum sein könnte: Diese Uhr wird von einem ständig durchlaufenden XS-Motor angetrieben. Über die Messung der Drehgeschwindigkeit wird die Motorspannung permanent so nachgeregelt, dass die Uhr so genau geht wie die Zeitbasis des Controllers und dass insbesondere keine sich aufaddierenden Fehler entstehen. Die Software ist in C#/.net geschrieben und läuft auf dem nanoFramework direkt im Controller (https://nanoframework.net).

Die Uhr läuft seit Tagen perfekt durch und soll es noch lange Zeit tun. Einen BS15 mit Bohrung mitgezählt, enthält sie nur ganze 6 Grundbausteine.

----------

Micro clock with continuously regulated DC motor

An analog clock which could hardly be any more analog: This clock gets driven by a continuously running XS motor. By measurement of the turning speed, it gets permanently regulated so that the clock is running just as precise as the time base built into the controller, and especially so that there are no cumulative errors. The software was written in C#/.net and is running on nanoFramework directly inside the controller (https://nanoframework.net).

The clock is running just perfect since days now and shall continue running for a long time. Including a BS15 with hole, it contains only 6 basic fischertechnik building blocks.