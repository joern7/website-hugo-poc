---
layout: "image"
title: "Carriage (2)"
date: "2016-03-07T12:45:30"
picture: "segmentclock10.jpg"
weight: "10"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43011
- /details0a45.html
imported:
- "2019"
_4images_image_id: "43011"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43011 -->
When the Federnocken are turned to the outside, as shown in the picture, pushing the Taster in, they slide freely over the Achsadapter connected to the segment shafts, when the carriage moves. 
The Encoder is used to position the carriage with both Achsadapter eactly aligned. This works remarkably well and fast. When in position, the S-Motors are switched on, the Achsadapter with the Federnocken turns, and rotates the other Achsadapter as well, flipping the segment from red to yellow or vice verse. This is a somewhat crude process: the S-Motor just pushes for a while and assumes the segment is in position. This takes a bit of calibration to determine the PWM value and time to switch the S-Motor on. When done, the Achsadapter with Federnocken returns to the middle position, as indicated by the Taster, so that the carriage is free to move on to the next segment.