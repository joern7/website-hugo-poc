---
layout: "image"
title: "Clock"
date: "2016-03-07T12:45:30"
picture: "segmentclock14.jpg"
weight: "14"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43015
- /details8861-2.html
imported:
- "2019"
_4images_image_id: "43015"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43015 -->
With all cover installed.