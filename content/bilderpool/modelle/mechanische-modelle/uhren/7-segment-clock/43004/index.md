---
layout: "image"
title: "Segment drives"
date: "2016-03-07T12:45:30"
picture: "segmentclock03.jpg"
weight: "3"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43004
- /detailsf71a.html
imported:
- "2019"
_4images_image_id: "43004"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43004 -->
In order to drive 7 segments with a single motor, I ordered the drive shafts on the back in a vertical row, at equal distances. For the top, middle and bottom segment, and the two segments on one side, it is not too hard to do. In order to drive the two segments on the other side some transmission is needed. I tried several solutions, and ended up with a number of gear wheels that maintain a 1:1 ratio (Z10, Z30, Z10-Z15, Z15-Z10). It all barely fits, and one Z10 has to be cut-off to make it fit.