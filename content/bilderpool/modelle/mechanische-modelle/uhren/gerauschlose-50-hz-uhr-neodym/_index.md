---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Neodym-Sekundenwelle"
date: 2020-02-22T08:16:07+01:00
legacy_id:
- /php/categories/3468
- /categoriesf522.html
- /categoriesea52.html
- /categoriesdd76.html
- /categories4abb.html
- /categoriesc83b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3468 --> 
Eine kompakte Variante einer Uhr mit Direktantrieb des Sekundenzeigers unter Verwendung kleiner Neodym-Magnete.