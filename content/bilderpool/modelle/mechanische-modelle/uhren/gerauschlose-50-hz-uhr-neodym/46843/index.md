---
layout: "image"
title: "Im Echtbetrieb"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46843
- /details8e00.html
imported:
- "2019"
_4images_image_id: "46843"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46843 -->
Alle meine Zeigeruhren mit Vier-Phasen-Antrieb fanden hier schon ihr längerfristiges Plätzchen im Esszimmer. An der hier nicht sichtbaren Wand hängt eine Quarz-Uhr - die tickt jede Sekunde. Das stört mich immer etwas dabei, zu hören, wie man von diesen Uhren halt nichts hört ;-) Jedenfalls bekamen die Uhren hier immer die Genehmigung der besten aller Ehefrauen zum Dauerlauf :-)