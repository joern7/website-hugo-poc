---
layout: "image"
title: "Blicke ins Getriebe (3)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46838
- /details5b53-2.html
imported:
- "2019"
_4images_image_id: "46838"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46838 -->
Links unten, halbverdeckt, sitzt das Rast-Z20, das vom eben besprochenen Z10 direkt angetrieben wird. Das geht über die Rastachse nach vorne zum Z10, das man noch erahnen kann, auf das in Bildmitte deutlichsichtbare Z15 auf der Metallachse. In letzterem steckt schließlich das von vorne aus auch sichtbare Z10, das das Z30 des Stundenzeigers trägt und antreibt.

Der Statikträger 30 im Bild dient nur zur Erhöhung der Stabilität beim Herumtragen. Das komplette Uhrwerk ist ansonsten mit nichts anderem als der Frontplatte verbunden, und zwar an nur zwei Punkten links und rechts.

Genauere Bilder des Getriebeaufbaus will ich gerne nachreichen, wenn ich die Uhr irgendwann wieder zerlege.