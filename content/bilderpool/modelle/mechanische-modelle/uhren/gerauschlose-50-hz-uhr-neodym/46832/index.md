---
layout: "image"
title: "Schrägansicht (1)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46832
- /detailsbd9f-2.html
imported:
- "2019"
_4images_image_id: "46832"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46832 -->
Alle Achsen sind so leichtgängig wie möglich gelagert. Bis auf eine einzige, die in einem BS7,5 sitzt, werden alle Achsen von je einem Paar S-Kupplungen 15 2 (38253) getragen und geführt.

Die Sekundenachse treibt wie man sieht über eine Schnecke ein Z40 an. Damit haben wir schon mal eine 1:40-Untersetzung.

Der Taster rechts oben dient dem schnellen Stellen der Uhr - dazu später noch mehr.