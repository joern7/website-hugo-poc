---
layout: "image"
title: "Links unten"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46994
- /detailsb319.html
imported:
- "2019"
_4images_image_id: "46994"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46994 -->
Von links unten gesehen. Unten im Bild sieht man, wie durch Einfügen eines kleinen Abstandshalters vor den Klemmringen die Reibung zum Achslager weiter minimiert wurde.