---
layout: "overview"
title: "Detailbilder vom Uhrwerk"
date: 2020-02-22T08:16:07+01:00
legacy_id:
- /php/categories/3478
- /categories6f79.html
- /categoriesf4f8.html
- /categoriesc748.html
- /categories999b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3478 --> 
Bevor ich die Uhr nach monatelangem perfekten Lauf wieder abbaute, machte ich noch Detailbilder des Uhrwerks.