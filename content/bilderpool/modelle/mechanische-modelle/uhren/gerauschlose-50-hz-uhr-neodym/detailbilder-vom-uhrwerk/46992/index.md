---
layout: "image"
title: "Blick in die Seite"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46992
- /details528c-2.html
imported:
- "2019"
_4images_image_id: "46992"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46992 -->
Seitenblick ins Uhrwerk.