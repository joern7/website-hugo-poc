---
layout: "image"
title: "Z40 abgenommen"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46995
- /detailsd4f3.html
imported:
- "2019"
_4images_image_id: "46995"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46995 -->
Das Z40 ist hier abgenommen und gibt den Blick auf die Schnecke frei, in die es eingriff.