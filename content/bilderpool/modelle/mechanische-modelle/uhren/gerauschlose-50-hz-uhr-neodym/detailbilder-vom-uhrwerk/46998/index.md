---
layout: "image"
title: "Unterseite der Zeigeraufhängung"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46998
- /details3a83.html
imported:
- "2019"
_4images_image_id: "46998"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46998 -->
Hier sieht man, wie der Antrieb aufs Z30 funktioniert, und wie das Hemmgummi anliegt.