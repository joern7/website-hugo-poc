---
layout: "image"
title: "Frontplatte"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46989
- /detailse408.html
imported:
- "2019"
_4images_image_id: "46989"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46989 -->
So sieht die Frontplatte von hinten aus. Die BS7,5, die da in der Mitte herumliegen, gehören auf die BS5 mit zwei Zapfen nebendran und sind bis auf eine auf dem Trafo sitzende Stütze die *einzige* Verbindung des Uhrwerks mit dem Rest. Man kann das Uhrwerk vorsichtig nach hinten aus der Frontplatte herausziehen, wenn man diese beiden BS7,5 wegschiebt.