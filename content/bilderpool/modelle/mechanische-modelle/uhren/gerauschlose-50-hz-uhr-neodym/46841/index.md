---
layout: "image"
title: "Blicke ins Getriebe (6)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46841
- /details36c3.html
imported:
- "2019"
_4images_image_id: "46841"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46841 -->
Dasselbe von der anderen Seite aus: Unten der Antrieb des Stundenzeigers, oben Kette und Z15 des Minutenzeiger-Antriebs.