---
layout: "image"
title: "Detail vom Innenleben"
date: "2015-12-20T13:46:57"
picture: "ft_ZeitwortUhr_WS2812B.jpg"
weight: "15"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["WS2812B", "Verdrahtung", "Innenleben", "Details"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42531
- /details8520-2.html
imported:
- "2019"
_4images_image_id: "42531"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42531 -->
Hier sieht man die Verdrahtung vom Mikrocontroller durch die Rückseite zu den RGB LED-Streifen.