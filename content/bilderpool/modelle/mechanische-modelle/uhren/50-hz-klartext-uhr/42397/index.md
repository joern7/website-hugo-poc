---
layout: "image"
title: "Abschluss"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42397
- /details85fc.html
imported:
- "2019"
_4images_image_id: "42397"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42397 -->
Nach den Aufnahmen war es etwa fünf Minuten später als vorher. Genauer will man es nicht wissen :-)