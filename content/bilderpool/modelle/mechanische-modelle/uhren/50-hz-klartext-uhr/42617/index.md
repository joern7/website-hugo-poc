---
layout: "image"
title: "Blick von hinten ins Leuchtraster"
date: "2015-12-28T19:08:43"
picture: "klartextuhrnachtrag2.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42617
- /details4674.html
imported:
- "2019"
_4images_image_id: "42617"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42617 -->
Hier noch ein Blick von hinten in das Gerippe, was die 18 Lampen trägt.