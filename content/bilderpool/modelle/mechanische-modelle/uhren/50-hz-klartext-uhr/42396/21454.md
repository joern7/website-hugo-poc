---
layout: "comment"
hidden: true
title: "21454"
date: "2015-12-28T22:31:41"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Ganz tolles und fantasiereiches Modell - und sogar alltagstagstauglich. Beruhigend sind die Kabel, so sehen meine Kabelführungen auch immer aus ;-)  Und der Stundenverteiler macht so den Eindruck, als wenn er auch in einem 12-Zylinder als Zündverteiler eingesetzt werden könnte.