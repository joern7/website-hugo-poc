---
layout: "image"
title: "Mechanik - Strom für die zwölf Stundenlampen"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42395
- /details093f.html
imported:
- "2019"
_4images_image_id: "42395"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42395 -->
Die ältere ft-Feder (eine gut leitende Kugelschreiberfeder tut's evtl. auch) vorne im Bild klemmt mit einer Achse darin sehr brauchbar in der Statiklasche. Damit kommt Strom auf die Metallachse. Auf der sitzen das angetriebene Z20 sowie ein ähnlich aufgebauter Federkontakt. Der ist über die klemmende Kombination aus Z10 und der Anordnung von BS15 mit Bohrung und zwei BS7,5 verdreh-sicher auf der Achse fixiert.