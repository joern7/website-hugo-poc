---
layout: "image"
title: "Mechanik - Fünf-Minuten-Taster"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42393
- /detailsf8eb.html
imported:
- "2019"
_4images_image_id: "42393"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42393 -->
Der Motor hat ein Impulsrad. Wenn der graue Minitaster gedrückt ist, läuft der Motor, bis der Taster wieder freigegeben wird. Vom Monoflop über das letzte E-Tec als Treiberbaustein kommt ein kurzer Impuls zum Loslaufen, aber der Motor hält dann korrekt nach einer Viertelumdrehung wieder an.

Das geht über das Rast-Z10 aufs Z30. Die 1:3-Untersetzung, angesteuert in Viertelumdrehungen, ergibt also 3 * 4 = 12 Impulse für eine ganze Umdrehung. Das sind genau die 12 Fünf-Minuten-Intervalle für die Minutenlampen. Über die entsprechend bestückten Schaltwalzen werden nun die Taster in der richtigen Kombination gedrückt. Die Taster sind aus diesem Blickwinkel:

"fünf": vorne rechts
"zehn": hinten rechts
"viertel": vorne mittig
"nach": hinten mittig
"vor": vorne links
"halb": hinten links

Die zwölf so realisierten Wort-Kombinationen sind:

- nichts (bei der vollen Stunde nämlich)
- fünf nach
- zehn nach
- viertel nach
- zehn vor halb
- fünf vor halb
- halb
- fünf nach halb
- zehn nach halb
- viertel vor
- zehn vor
- fünf vor