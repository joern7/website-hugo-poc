---
layout: "overview"
title: "50-Hz-Klartext-Uhr"
date: 2020-02-22T08:15:48+01:00
legacy_id:
- /php/categories/3155
- /categoriesda17.html
- /categories1190.html
- /categoriesdc62.html
- /categories14b5.html
- /categories6b64.html
- /categories190a.html
- /categories1e8f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3155 --> 
Diese Uhr geht zwar genau (getaktet durch das 50-Hz-Stromnetz), zeigt aber die Uhrzeit sehr entspannend nur auf fünf Minuten genau und in ganzen Worten an.