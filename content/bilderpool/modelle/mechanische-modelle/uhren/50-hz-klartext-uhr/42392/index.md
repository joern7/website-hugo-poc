---
layout: "image"
title: "Mechanik - Überblick"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42392
- /detailsfdae.html
imported:
- "2019"
_4images_image_id: "42392"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42392 -->
Man sieht rechts den schwarzen XS-Motor. Der läuft so schön leise und treibt die Schaltwalzen für die Wörter fünf/zehn/viertel/nach/vor/halb und also die sechs Taster für die entsprechenden Lampen an.

Von dort geht es über eine Übertragsmechanik auf die Ansteuerung der zwölf Stunden-Lampen.