---
layout: "image"
title: "Umbau des Antriebs"
date: "2015-12-28T19:08:43"
picture: "klartextuhrnachtrag1.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42616
- /details6afe.html
imported:
- "2019"
_4images_image_id: "42616"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42616 -->
Da nach einigen Tagen Dauerbetrieb die Uhr eines Morgens um fünf Minuten nachging, und sich dieses Spiel auch nochmal wiederholte, habe ich den Antrieb durch Zwischenschalten einer Schnecke zwar etwas langsamer, aber dafür zuverlässig kräftig geändert.