---
layout: "image"
title: "Innenleben mit RGB LED Leuchtstreifen WS2812B"
date: "2015-12-20T13:46:57"
picture: "ft_ZeitwortUhr_Innenleben.jpg"
weight: "14"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["RGB", "LED", "WS2812B", "Streifen", "seriell"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42530
- /details41f8.html
imported:
- "2019"
_4images_image_id: "42530"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42530 -->
Die drei RGB LED Streifen sind alle seriell hintereinander geschaltet und werden von einem einzigen Pin am Mikrocontroller mit 800 kbit/s angesteuert.