---
layout: "image"
title: "Ansicht bei Nacht"
date: 2020-05-26T17:33:06+02:00
picture: "IMG_0547.JPG"
weight: "8"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Wo immer es geht, sind meine Uhren so gebaut, dass sie mit einer entsprechenden Beleuchtung auch ein schönes Schattenspiel an den umliegenden Wänden abgeben.
