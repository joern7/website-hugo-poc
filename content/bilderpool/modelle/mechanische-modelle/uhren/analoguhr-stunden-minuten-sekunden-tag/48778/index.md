---
layout: "image"
title: "Seitenansicht von links"
date: 2020-05-26T17:33:19+02:00
picture: "IMG_0538.JPG"
weight: "3"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Eine von mir gerne verwendete Konstruktion ist das kleine Winkelgetriebe (gut zu erkennen am schwarzen Z 10). Hier wird direkt über 90 Grand gedreht der Sekundenanzeiger angetrieben und gegenüber geht es weiter zum Getriebeteil für die Minutenanzeige.