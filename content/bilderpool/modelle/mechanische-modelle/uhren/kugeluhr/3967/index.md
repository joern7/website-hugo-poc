---
layout: "image"
title: "Kugeluhr Überblick"
date: "2005-04-08T20:54:59"
picture: "07-Dispenser-Wippe-Rcklauf.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3967
- /detailsabe8-2.html
imported:
- "2019"
_4images_image_id: "3967"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3967 -->
Hier nochmals im Überblick die Anlagenteile.