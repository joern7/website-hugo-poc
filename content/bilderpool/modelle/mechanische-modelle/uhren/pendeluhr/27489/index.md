---
layout: "image"
title: "17-Pendel"
date: "2010-06-13T14:58:59"
picture: "17-Pendel1.jpg"
weight: "17"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27489
- /details3d2d-2.html
imported:
- "2019"
_4images_image_id: "27489"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T14:58:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27489 -->
Nach einigen Umbauten hat die Uhr jetzt ein justierfähiges Pendel erhalten. Im Hintergrund die Auslöser und von oben kommend, das Antriebsgewicht.

Im Antriebsgewicht und im Pendel sind Stahlwalzen mit 42 mm Durchmesser verbaut. Das Stückgewicht ist 750 g. Mein Dank gilt meinem Arbeitgeber, der IBC Wälzlager GmbH, die mir diese Rollen zur Verfügung gestellt haben.

Mit allen Anbauten wiegt das Pendel knapp 3,5 kg. Startet man die Uhr neu, so braucht es einige Stunden, bis das Pendel seinen Sollbetrieb aufnimmt. Die Gesamtlänge des Pendels beträgt vom Aufhängepunkt bis zur Spitze unten 155 cm, die Pendeldauer ist 75/32 oder 2,34375 Sekunden. Die Pendelamplitude beträgt 0,8 Grad oder 22 mm nach jeder Seite. Mehr nicht.

Unten an der Pendelspitze habe ich einen Papierschnipsel angeklebt, der bei jedem Pendelhub die Gabellichtschranke unterbricht. Ein Computer zählt die Pendelschwingungen mit und schreibt alle 384 Schwingungen die Abweichung von der Sollzeit auf, d. h. alle 15 Minuten. Damit kann das Pendel einjustiert werden.