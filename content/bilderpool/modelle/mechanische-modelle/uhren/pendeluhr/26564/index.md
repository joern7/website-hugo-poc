---
layout: "image"
title: "07-Hemmung Teststand"
date: "2010-02-28T13:00:49"
picture: "07-Hemmung.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26564
- /detailsda46-2.html
imported:
- "2019"
_4images_image_id: "26564"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-28T13:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26564 -->
Hemmung und Pendel stehen jetzt auf deutlich soliderem Fundament und auch das Pendel ist vorerst auf Kugellagern reibungsarm aufgehängt.

Der kurze Hebel an der Hemmungswelle fasst mit einem Stift ganz zart in die Nut des Pendelstabs. Die höhere Steifigkeit des Pendels, die geringere Reibung der Pendelaufhängung und die stark erhöhte Steifigkeit des Rahmens haben den Leistungshunger der Maschine auf nunmehr 70 Mikrowatt gedrückt.

Allmählich erreiche ich damit das Ende des mit diesem Aufbau Machbaren, weil ich die Reibung in der Hemmung nicht weiter reduzieren kann. Evtl. hilft noch reinigen, bis keine Partikel mehr in der Mechanik stecken.

Das Pendel wird sehr lang werden. Wenn ich schon nicht die Reibung in der Hemmung mehr vermindern kann, kann ich aber noch die Drehzahl herabsetzen. Auch das spart Energie.

Immerhin: mit dem Meterkilogramm würde diese Mechanik jetzt schon 36 Stunden laufen.