---
layout: "image"
title: "23-Ziffernblatt"
date: "2010-08-03T22:06:25"
picture: "23-Ziffernblatt.jpg"
weight: "23"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27791
- /detailsaaf1.html
imported:
- "2019"
_4images_image_id: "27791"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-08-03T22:06:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27791 -->
Das Projekt "Hora Mechanica" geht zu Ende. Mit dem Ziffernblatt und einer ordentlichen Fassung ist die Uhr fertig.
Irene sagte einmal "...und sie bewegt sich doch" (lat.: Tamensi movetur). Daraus leite ich ihren Namen her. Ich nenne das Modell "Movetura"