---
layout: "image"
title: "03-Hemmung"
date: "2010-02-21T22:18:19"
picture: "03-Hemmung.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26502
- /detailse3a3-2.html
imported:
- "2019"
_4images_image_id: "26502"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26502 -->
Etwas näher heran: hier die etwas feineren Details. Die Klemmringe verhindern das Anstreifen der Rollen an den großen Rädern der Hemmung. Damit die Rollenwellen nicht anstoßen, müssen die Klemmring mit ihrer Öffnung genau zu den großen Scheiben ausgerichtet werden. Dann geht es.