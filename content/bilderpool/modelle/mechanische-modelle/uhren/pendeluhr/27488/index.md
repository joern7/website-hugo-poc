---
layout: "image"
title: "16-Auslöser"
date: "2010-06-13T14:58:58"
picture: "16-Auslser.jpg"
weight: "16"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27488
- /detailsb4a1.html
imported:
- "2019"
_4images_image_id: "27488"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T14:58:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27488 -->
Hier der Blick von hinten in den "Fußraum" der Uhr.

Wenn das große Gewicht unten ankommt und den linken Taster betätigt, kommt der automatische Aufzug ins Spiel. Der Taster schaltet den Strom für ein Relais und für einen Step-Up-Spannungswandler ein. Der Spannungswandler macht aus den 2,5 bis 5,2 Volt, die zur Verfügung stehen, konstante 5 Volt, die das Relais benötigt. Zieht das Relais an, dann schaltet es sich mit seinem Kontakt selbst die Spannung ein (Selbsthaltung). Mit dem Kontakt wird auch der Motor eingeschaltet.

Der Aufzugsmotor zieht das große Gewicht wieder nach oben, der erste Taster fällt wieder ab, aber das Relais bleibt aufgrund seiner Selbsthaltung eingeschaltet. Das kleine Gegengewicht senkt sich ab und betätigt den rechten Tastschalter. Dieser steht normalerweise auf Dauer-Ein, so daß dessen Betätigung den Stromkreis unterbricht. Das Relais fällt ab, der Motor steht, die Uhr ist aufgezogen. Das Gegengewicht kommt gerade oben links ins Bild (kleine rote Tonne).

Im Hintergrund ist noch der rote Würfel erkennbar. Darin ist das Relais, der Spannungswandler und ein großer Kondensator untergebracht. Der Kondensator hat die Kennwerte 5,4 Volt maximal bei 100 F! Voll geladen enthält der Kondensator damit 1400 Joule Energie. Damit könnte die Uhr theoretisch schier ewig laufen.

Der Kondensator bezieht seine Ladung von einer Solarzelle, die draußen auf dem Fensterbrett liegt. Damit läuft die Uhr umweltfreundlich mit Solarstrom. Das erklärt jetzt auch die stark unterschiedliche Spannung am Motor und die Verwendung eines Step-Up-Reglers. Der Motor läuft mit 2,5 Volt schon an, wenn er dann auch etwas gequält klingt, aber er kriegt das Gewicht hoch. Leider kann der Step-Up-Regler nicht auch noch den Motor versorgen.

Nach einer Woche Dauerbetrieb und den langen, hellen Tagen, hat sich die Betriebsspannung des Kondensators auf 4,6 bis 4,9 Volt stabilisiert.