---
layout: "image"
title: "09-Hemmung"
date: "2010-03-15T16:56:37"
picture: "09-Hemmung.jpg"
weight: "9"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26694
- /details16e8-2.html
imported:
- "2019"
_4images_image_id: "26694"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26694 -->
Die Situation bessert sich merklich, wenn die Drehachse des Hemmungsankers nicht mehr so dicht an der Drehachse des Hemmungsrades liegt. Ideal wäre es, wenn beide Achsen unendlich weit auseinanderlägen. Dann wären die rote und die grüne Linie im Bild zuvor parallel.

Die neue Hemmung ist erst einmal gewaltig abgespeckt worden. Das Hemmungsrad wiegt nur noch die Hälfte und hat 60% weniger Massenträgheitsmoment. Der Hemmungsanker ist von 160 g auf 20 g abgemagert und die Drehachse des Hemmungsankers hat 150 mm mehr Abstand gewonnen. Hinzugekommen sind allerdings zwei neue Achsen, um die ersten Übersetzungsstufen zu bauen. Vom Antrieb hinten auf das Hemmungsrad ist die Übersetzung schon mal 1:16.

Diese Hemmung arbeitet jetzt deutlich symmetrischer und weit weniger hakelig.