---
layout: "comment"
hidden: true
title: "14799"
date: "2011-08-07T20:25:31"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Zur Ergänzung:

Das Video habe ich inzwischen bei YouTube untergebracht. Es ist hier zu sehen:

http://www.youtube.com/watch?v=VJ2OkslQSzU

Leider kann ich viele Anregungen zu dem Video nicht mehr realisieren, obwohl ich das gerne getan hätte. Ich lasse es als Erstlingswerk so stehen, wie es ist und freue mich, dass ich es überhaupt zustande gebracht habe.

Bei allen, die mit Kommentaren und Anregungen zum Gelingen dieses Projekts beigetragen haben, möchte ich mich ganz herzlich bedanken.