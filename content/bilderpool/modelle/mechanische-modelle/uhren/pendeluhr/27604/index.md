---
layout: "image"
title: "21-Zeigerwerksgetriebe"
date: "2010-07-04T14:04:18"
picture: "21-Zeigerwerk.jpg"
weight: "21"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["complication"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27604
- /details8b76.html
imported:
- "2019"
_4images_image_id: "27604"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-07-04T14:04:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27604 -->
Hier das Zeigerwerksgetriebe en détail:

Hinten links das Antriebszahnrad vom Uhrwerk, soeben am Pendelstab vorbei. Dieses Zahnrad greift auf ein gleichgroßes Zahnrad der richtigen Minutenwelle, die sich damit richtig herum dreht. Das Zahnrad ist nur leicht aufgezogen, damit man am Minutenzeiger drehen kann, um die Uhr zu stellen.

Die neue Minutenwelle trägt vier Zahnräder: ein Z20 übernimmt die Minutendrehung, dann kommt ein Z10, dann ein Z30 auf einer Freilaufnabe und ein weiteres Z30, das fest auf der Achse sitzt und den Minutenzeiger dreht.

Die Zwischenwelle, die um 7,5 Grad schräg im Getriebe sitzt, ergibt mit der Übersetzung 1:4 und 1:3 insgesamt 1:12 auf den Stundenzeiger. Wie gesagt, das Z30, das den Stundenzeiger trägt, sitzt mit einer Freilaufnabe auf der Minutenwelle. So laufen die beiden Zeiger koaxial, ohne sich drehmomentenmässig zu stören.

Die Lagerung der Minutenwelle musste aus Platzgründen auf ein Kugellager verzichten.