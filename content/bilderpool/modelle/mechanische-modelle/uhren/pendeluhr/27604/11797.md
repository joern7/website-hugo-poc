---
layout: "comment"
hidden: true
title: "11797"
date: "2010-07-04T18:05:04"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Ja, genau darum geht es.

Je weniger Wellen und Zahnräder umso weniger Reibung im Getriebe. Auf die Schräge der Welle hätte ich verzichten können, wenn es eine ft-Zahnradübersetzung 1:Wurzel(12) gäbe. Die habe ich aber nicht gefunden und deshalb habe ich 1:(3x4) gebaut. 

Eine Übersetzung von so ungefähr 1:12 ist zwischen Minuten- und Stundenzeiger eher unbefriedigend.

Als dann
Remadus