---
layout: "image"
title: "04-Hemmung"
date: "2010-02-21T22:18:19"
picture: "04-Hemmung.jpg"
weight: "4"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26503
- /detailsbbbc.html
imported:
- "2019"
_4images_image_id: "26503"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26503 -->
Hier der Teststand. Gemessen wird die angehängte Masse, die Zeit und die Höhe, um die sich das angehängte Gewicht absenkt.

Das Gewicht zieht über einen Flaschenzug am Seil, welches sich von einer kleinen Seilrolle abwickelt. Die Seilrolle treibt das Zahnrad 40 und über die Kette ein Ritzel 10 auf der Welle des Hemmungsrades. Dieses System hat für die spätere Uhr viel zu viel Reibung und dient hier nur für erste Tests.

Noch trägt die Hemmung ein langes Stabpendel direkt auf ihrer Welle. Auf diesem Bild unterschreitet die Hemmung erstmals die 500 Mikrowatt-Grenze.