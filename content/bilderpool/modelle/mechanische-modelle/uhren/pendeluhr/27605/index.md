---
layout: "image"
title: "22-Zeigerwerk frontal"
date: "2010-07-04T14:04:18"
picture: "22-Zeigerwerk.jpg"
weight: "22"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27605
- /details1dfd.html
imported:
- "2019"
_4images_image_id: "27605"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-07-04T14:04:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27605 -->
Hier der Überblick: ganz oben die Pendellagerung an Blattfedern und darunter Uhrwerk und Zeigerwerk.

Der gelbe Ring erhält noch das eigentliche Zifferblatt. Zwar ist die Uhr dann nicht mehr ganz so transparent, aber hoffentlich etwas übersichtlicher.

Für den Einbau des Zeigerwerks hatte ich die Uhr knapp zwei Stunden stillgesetzt, aber sonst läuft sie ununterbrochen. So geht sie ihrer Vollendung entgegen...