---
layout: "image"
title: "24-Die Uhr"
date: "2010-08-03T22:06:25"
picture: "24-Die_Uhr.jpg"
weight: "24"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27792
- /detailse467-2.html
imported:
- "2019"
_4images_image_id: "27792"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-08-03T22:06:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27792 -->
Das ist sie.

Mit 175 cm Höhe überragt sie mich um einige Zentimeter.

Die letzte Gangkorrektur ist jetzt schon über eine Woche her. Seitdem zeigt sie die Zeit zuverlässig an.