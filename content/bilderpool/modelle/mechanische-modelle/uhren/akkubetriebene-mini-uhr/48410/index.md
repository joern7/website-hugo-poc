---
layout: "image"
title: "Frontansicht"
date: "2018-11-23T21:14:59"
picture: "akkubetriebeneminiuhr1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48410
imported:
- "2019"
_4images_image_id: "48410"
_4images_cat_id: "3546"
_4images_user_id: "104"
_4images_image_date: "2018-11-23T21:14:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48410 -->
Die Uhr ist ein recht kleines Paket und durch den Akkubetrieb völlig autonom. Links sieht man einen Netduino 3 mit aufgestecktem Adafruit Motor Shield v2.3, auf dem das nanoFramework - siehe https://nanoframework.net - läuft. Die Uhr ist von Visual Studio 2017 aus in .net/C# programmiert und steuert den Schrittmotor im Hintergrund an. Der geht über ein Z25 von Roland (ganz herzlichen Dank nochmals!) auf das praktisch unverändert von der vorherigen Fassung übernommene Getriebe mit feinen Zähnen.