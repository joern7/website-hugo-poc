---
layout: "image"
title: "Linke Seite"
date: "2018-11-23T21:15:00"
picture: "akkubetriebeneminiuhr2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48411
imported:
- "2019"
_4images_image_id: "48411"
_4images_cat_id: "3546"
_4images_user_id: "104"
_4images_image_date: "2018-11-23T21:15:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48411 -->
Dies ist die Controller-Seite. Der steht einfach auf dem Akku, wird rechts im Bild von einem Winkelbaustein und links von den Klemmringen der Metallachse gehalten, die den Schrittmotor fixieren.