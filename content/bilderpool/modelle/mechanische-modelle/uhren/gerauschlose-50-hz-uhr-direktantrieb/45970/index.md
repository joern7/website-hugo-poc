---
layout: "image"
title: "Elektronik 1 - Zähler"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45970
- /details65c5.html
imported:
- "2019"
_4images_image_id: "45970"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45970 -->
Der linke Electronics-Baustein dient als schneller (Poti nach rechts zum Ausschalten der Entprellung) Zähler 1:50, macht also aus den 50 Hz einzelne Sekunden. An E1 (links unten) kommen die 50 Hz an. Der Ausgang der ersten Zählstufe (O2, links oben), geht zum Eingang der zweiten (E6). Dessen normaler Ausgang O3 geht zum zweiten Modul, der halb so schnell laufende O4 steuert die Sekundenlampe an.

Das rechte Electronics-Modul ist als 1:120-Zähler verschaltet (wieder über zwei Zählstufen), macht also aus den Sekunden einen Takt von *zwei* (!) Minuten. Man beachte, dass der auf dem Kopf eingebaut ist, damit rechts die Stromversorgung ankommen kann. Die Eingänge liegen also oben, die Ausgänge unten. Der letzte Ausgang - 1 Takt pro Minute also - ist O1 ganz rechts unten, der auf die Elektronik der anderen Seite geleitet wird.