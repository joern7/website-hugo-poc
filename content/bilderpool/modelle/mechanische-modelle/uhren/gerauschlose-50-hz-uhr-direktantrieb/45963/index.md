---
layout: "image"
title: "Rückansicht 1"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45963
- /details64c2.html
imported:
- "2019"
_4images_image_id: "45963"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45963 -->
Das Getriebe beschränkt sich dadurch auf eine simple 1:12-Untersetzung zwischen Minuten- und Stundenzeiger. Der im Bild vordere Schalter ist der Ein-/Aus-Schalter, der andere schaltet die sekündlich aufleuchtende Lampe (oben fast im Rotor) ein oder - falls sie mal nerven sollte - aus.