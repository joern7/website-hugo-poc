---
layout: "image"
title: "Abgesenkte Ziffer von der Seite"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27284
- /detailsa8ec.html
imported:
- "2019"
_4images_image_id: "27284"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27284 -->
Hier sieht man die "6" im abgesenkten Zustand.