---
layout: "image"
title: "Seitenblick"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27281
- /details04c7.html
imported:
- "2019"
_4images_image_id: "27281"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27281 -->
Hier sieht man alle Ziffern 0 - 9 aufgehängt. Die 9 befindet sich gerade in der Absenkmechanik. Jede andere Ziffer kann ebenfalls dorthin verschoben werden.

So kam der komische Name "Hängedachuhr" zustande- mir ist nichts Besseres eingefallen. Sie wird von einem Rechner (RoboInt) gesteuert werden müssen.