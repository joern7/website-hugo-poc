---
layout: "image"
title: "Abgesenkte Ziffer von vorne"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27285
- /detailsc03d-3.html
imported:
- "2019"
_4images_image_id: "27285"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27285 -->
So sieht es aus, wenn eine Ziffer sichtbar wird. Alles oben drüber muss noch verkleidet sein, so dass nur die gewünschte Ziffer ins Freie gelangt.