---
layout: "image"
title: "Rückansicht"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27282
- /details866a.html
imported:
- "2019"
_4images_image_id: "27282"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27282 -->
Man sieht hier (oben, schwarz) den MiniMot mit Hubgetriebe, der über die Bausteinstange mit den zwei senkrecht nach unten ragenden schwarzen BS30 die ganze Zifferngruppe umschließt und also vor und zurück schieben soll. Das Z10 dient dem Antrieb der Absenkmechanik. Ein alter grauer ft-Motor passt hier genau dazu. Ganz vorne der Endlagenschalter, der gedrückt wird, wenn die vorderste Ziffer (die 9) in der Absenkeinheit hängt. Er muss noch durch einen zweiten Taster ergänzt werden, der je Ziffer gedrückt wird, um die Positionen mitzählen zu können.