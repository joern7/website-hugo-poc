---
layout: "image"
title: "Teilansicht des oberen Getriebeteils"
date: 2020-03-16T17:59:48+01:00
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

Ein Z42 bestehend aus Drehscheibe 60 und Rastkettengliedern. Erforderlich, da der Minutenring einZ84 ist und der Stundenring ein Z168.