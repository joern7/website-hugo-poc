---
layout: "image"
title: "Seitenansicht rechts"
date: 2020-03-16T17:59:56+01:00
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

