---
layout: "image"
title: "Ein Blick in den unteren Getriebeteil"
date: 2020-03-16T17:59:51+01:00
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

Über das rote Z40 geht es weiter zum Minuten-Antrieb, über das schwarze Z40 zum Stunden-Antrieb