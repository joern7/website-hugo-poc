---
layout: "image"
title: "Der Synchron-Motor"
date: 2020-03-16T17:59:45+01:00
picture: "bild6.jpg"
weight: "6"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

Ein Synchronmotor mit Polpaarzahl 12 und 250 U/min. Eine von mir gern verwendete Version, da sie in dieser Bauart und mit entsprechender Beleuchtung ein schönes Schattenspiel liefert. 