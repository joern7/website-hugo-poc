---
layout: "image"
title: "Getriebekonstruktion"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45092
- /detailsb3b9.html
imported:
- "2019"
_4images_image_id: "45092"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45092 -->
Das gesamte Getriebe sollte so leichtläufig wie möglich konstruiert werden: Alle Achsen müssen fluchten; jede Achse sollte an genau zwei Stellen reibungsarm gelagert werden. Als Lager sollten bevorzugt Kupplungsstücke (sehr geringe Reibung) verwendet werden. Auch Gelenkwürfelklauen mit Lagerhülse verursachen relativ wenig Reibung. Metallachsen sind Kunststoffachsen vorzuziehen, B15 mit Bohrung sollte man besser nicht für die Lagerung der Achsen einsetzen.