---
layout: "comment"
hidden: true
title: "22955"
date: "2017-01-29T16:32:29"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Danke für die Blumen...
@Stefan: Ketten habe ich bei allen meinen Uhrengetrieben auch immer gescheut - ich glaubte, die gehören da einfach nicht hin. Thomas hat dann erste Ketten eingesetzt. Als ich dann systematisch untersucht habe, mit welchen Konstruktionen ich die geringsten Reibungsverluste habe, waren die Ketten bei der Kraftübertragung ganz vorne. Die Anzeige ist dadurch einfach unglaublich leichtgängig geworden - und die Getriebekonstruktion wird ziemlich einfach (und flexibel zugleich).
Gruß, Dirk