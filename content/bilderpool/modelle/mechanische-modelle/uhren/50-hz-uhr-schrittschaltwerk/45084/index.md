---
layout: "image"
title: "Schwungrad des 50-Hz-Motors"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45084
- /detailsc16e.html
imported:
- "2019"
_4images_image_id: "45084"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45084 -->
Der 50-Hz-Motor besteht aus sechs Neodym-Stabmagneten 6 x 30 mm, die mit Reedkontakthaltern außen an einem Innenzahnrad Z30 befestigt sind, und einem Elektromagneten, angeschlossen am 50-Hz-Wechselstromausgang eines 40 Jahre alten grauen fischertechnik-Trafos (6,8V).

Das Innenzahnrad ist am Z30 mit drei schwarzen I-Streben 15 und sechs schwarzen Verbindungsstopfen montiert. Um die Neodymmagneten habe ich einen Streifen Tesafilm geklebt, damit sie fest in den Reedkontakthaltern sitzen.
Der Motor treibt die Achse mit 500 U/min an: 100 Polaritätswechsel des E-Magneten pro Sekunde = 6000 Polaritätswechsel pro Minute; bei 12 Magnetfeldern pro Umdrehung entspricht das einer Geschwindigkeit von 500 U/min.