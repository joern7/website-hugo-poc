---
layout: "image"
title: "Konstruktion Zeitanzeige"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45089
- /details03ee.html
imported:
- "2019"
_4images_image_id: "45089"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45089 -->
Die Drehscheibe 60 mit dem Stundenzeiger wird mit drei roten Rastachsen 20 und Klemmbuchsen 5 als Abstandshalter auf einem Z40 befestigt, das von einer Kette angetrieben wird. Das Z40 sitzt mit einer Freilaufnabe auf der Achse des Minutenzeigers.