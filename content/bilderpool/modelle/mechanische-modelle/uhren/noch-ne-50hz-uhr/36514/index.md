---
layout: "image"
title: "Rechte Seite Detail"
date: "2013-01-25T19:32:47"
picture: "althzuhr08.jpg"
weight: "8"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36514
- /detailsf7fa.html
imported:
- "2019"
_4images_image_id: "36514"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36514 -->
Vom linken z30 geht's über eine z10 auf z40 Untersetzung über eine ... schäbige Kardanwelle in den Stunden-Innenzahnkranz.
Für die Kardan-Fummelei hab ich meine ersten ft-Teile zerschnipselt. Mir ist aber keine bessere Lösung eingefallen: Zwischen den eng liegenden Antrieben der Innenzahnkränze, der zentralen Sekundenzeigerachse und dessen Antriebs-Kronrad will irgendwie das z40 und z30 des Stundentrieb nicht reinpassen. Die Kardan-Umlenkung wurde notwendig damit ich an den Flügeln der z30-Nabe vorbeikam.