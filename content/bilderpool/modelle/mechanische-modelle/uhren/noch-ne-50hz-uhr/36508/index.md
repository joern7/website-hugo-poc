---
layout: "image"
title: "Linke Seite"
date: "2013-01-25T19:32:47"
picture: "althzuhr02.jpg"
weight: "2"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36508
- /details930d.html
imported:
- "2019"
_4images_image_id: "36508"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36508 -->
Damit der Antrieb auch richtig zur Geltung kommt, hab ich die Mechanik des eigentlichen Uhrwerks auf die untere Hälfte beschränkt.