---
layout: "image"
title: "Sie läuft!"
date: "2013-01-25T19:32:47"
picture: "althzuhr01.jpg"
weight: "1"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36507
- /detailsc214.html
imported:
- "2019"
_4images_image_id: "36507"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36507 -->
Aufgrund der langen Belichtung sieht man den Sekundenzeiger nur schemenhaft.