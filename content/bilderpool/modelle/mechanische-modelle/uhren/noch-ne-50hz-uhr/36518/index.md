---
layout: "image"
title: "Und Action!"
date: "2013-01-25T21:53:44"
picture: "althzuhr12.jpg"
weight: "12"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36518
- /detailsf602-2.html
imported:
- "2019"
_4images_image_id: "36518"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T21:53:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36518 -->
Ein erster Lauf nach Umbau.