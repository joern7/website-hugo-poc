---
layout: "image"
title: "'Kugellager'"
date: "2013-01-25T19:32:47"
picture: "althzuhr06.jpg"
weight: "6"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36512
- /detailsae7c.html
imported:
- "2019"
_4images_image_id: "36512"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36512 -->
Ja, der Motor ist "kugelgelagert" (-; Mit ein wenig Silikon-Öl geschmiert, läuft er fast lautlos. Den meisten Lärm machen die Schneckentriebe wegen ihrer rauen Oberfläche. Mein erster Ansatz hatte eine Schnecke direkt auf der Motorachse. Das wurde aber zu laut, sobald der Motor leicht belastet wurde. Deshalb habe ich die  erste Schnecke zunächst mit der z10/z40 Kombination, dessen "Schlagen" bei guter Justage erträglich ist,  untersetzt. 
Die Geomag-Kugeln haben die gleiche Größe wie die von ft und sind viel besser verchromt.