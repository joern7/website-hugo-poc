---
layout: "overview"
title: "Und noch 'ne 50Hz Uhr"
date: 2020-02-22T08:15:45+01:00
legacy_id:
- /php/categories/2710
- /categories7c7f-2.html
- /categoriesfd2f.html
- /categoriesb15b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2710 --> 
Inspiriert on Andreas Püttmanns wunderschönen 50Hz-Uhr hab ich mich auch an einer Uhr mit Sekundenzeiger versucht. Sie ist ein wenig kompakter geraten und hat eine alternative Mechanik für die drei Zeiger. Die Mechanik erreicht nicht ganz die geniale Einfachheit von Andreas Uhr, manche Lösungen sind mir für meinen Geschmack etwas zu komplex geraten. Bin aber trotzdem recht stolz darauf. 