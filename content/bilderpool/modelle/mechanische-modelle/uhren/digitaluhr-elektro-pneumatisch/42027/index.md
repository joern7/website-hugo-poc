---
layout: "image"
title: "Digitaluhr"
date: "2015-10-01T18:18:47"
picture: "Digitaluhr.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42027
- /detailsb897.html
imported:
- "2019"
_4images_image_id: "42027"
_4images_cat_id: "3123"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42027 -->
Jede Minute wird eine neue Zffer aufgestellt.

Hier gibt es das Video dazu:
https://www.youtube.com/watch?v=m3SrK25q868