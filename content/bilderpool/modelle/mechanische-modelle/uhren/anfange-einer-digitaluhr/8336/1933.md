---
layout: "comment"
hidden: true
title: "1933"
date: "2007-01-09T17:16:49"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

beeindruckende Arbeit und eine grandiose Idee. Aber ich habe leichte Zweifel, was die Sichtbarkeit betrifft. Um durch die Folie sichtbar zu sein, müssen die Segmente schon mit einem leichten Druck anliegen. Sie werden aber vermutlich von der Folie minimal zurückgedrückt und sind somit nicht mehr klar erkennbar. Soweit meine Vermutung. Ich lasse mich aber gerne durch Bilder eines besseren belehren! ;o)

Gruß,
Franz