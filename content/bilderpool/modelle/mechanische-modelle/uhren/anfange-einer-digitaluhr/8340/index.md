---
layout: "image"
title: "Die Schiebemechanik von vorne"
date: "2007-01-08T17:16:10"
picture: "digitaluhrprototyp6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8340
- /details37d2.html
imported:
- "2019"
_4images_image_id: "8340"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8340 -->
Diese Mechanik soll eine Ziffer bedienen können und je nach Bedarf zur jeweils betreffenden Ziffer verfahren werden. Ansonsten bräuchte ich ja 28 Elektromagnete. Und es sollen nur E-Magnete werden, damit die Uhr möglichst leise läuft und im Wohnzimmer stehen darf ;-)

Die quer liegenden Lagerböcke sollen von oben auf die Klemmringe einer Ziffer abgesenkt werden und sie so umschließen, dass man die Segmente vor- und zurückfahren kann.

Dieses ganze Teil muss also noch in einer Ziffer zum Eingriff gebracht sowie zwischen den Ziffern verschoben werden können, um alle Ziffern anzusteuern. Nur alle 10 min muss die Mechanik aber die Einerstelle der Minuten verlassen - ich hoffe, das wird vom Betriebsgeräusch tolerierbar.