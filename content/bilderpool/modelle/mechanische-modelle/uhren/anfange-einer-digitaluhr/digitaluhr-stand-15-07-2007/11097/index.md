---
layout: "image"
title: "Zeitbasis-Elektronik"
date: "2007-07-16T16:20:56"
picture: "digitaluhr1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11097
- /detailsdee4.html
imported:
- "2019"
_4images_image_id: "11097"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11097 -->
Das ist die Elektronik, die die Zeitbasis liefern soll. Nicht nur der Gleichrichterbaustein, sondern auch der linke der beide Elektromagnete unten hängt an 50 Hz Wechselspannung aus dem seitlichen Ausgang eines alten ft-Trafos. Vorher hängen noch zwei Lampen als Schutzwiderstand. Zusammen mit dem rechten Elektromagneten ergibt das einen kleinen Transformator zwecks galvanischer Trennung. Im zweiten Elektromagneten wird also eine 50 Hz-Spannung induziert, die auf die beiden Eingänge E1 und E2 des Grundbausteins (einem Operationsverstärker) gegeben werden. Das Potentiometer des Grundbausteins dient hier als Schutzwiderstand. Dessen 50 Hz-Gezappel am Ausgang wird durch drei Flip-Flops auf 50 Hz / 8 = 6,25 Hz herunter geteilt. Das ergibt anstatt 50 * 60 = 3000 Schwingungen pro Minute nur noch 3000 / 8 = 375 Takte pro Minute. Ein Takt dauert dann 1 / 6,25 Hz = 160 ms. Das sollte vom Robo-Interface locker verarbeitet werden können. Noch ein Flip-Flop wäre etwas ungünstig, weil 375 nicht gerade ist und sich somit eine schlechter handhabbare Schwingungsanzahl pro Minute ergäbe.