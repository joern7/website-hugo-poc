---
layout: "image"
title: "Der Wagen"
date: "2007-07-16T16:20:57"
picture: "digitaluhr8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11104
- /details4b9b-2.html
imported:
- "2019"
_4images_image_id: "11104"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11104 -->
Das ist der derzeitige Stand des Wagens. Der befindet sich noch im Anfangsstadium. Er ist wie man sieht auf den Schienen verfahrbar. Das Gesamtgewicht wird auf 8 Seilrollen verteilt. Die Statikschienen selbst sitzen auf den schwarzen Grundplatten auf und werden also auf der ganzen Länge gleichmäßig von unten gestützt Die grauen BS15 halten sie nur fest und sorgen für einen definierten Abstand. Da die BS15 für beide Schienen "von vorne" kommen, ergibt sich auf einfache Art und Weise genau der richtige, rasterkonforme Abstand zwischen den Schienen. Die Hauptmechanik vom vorherigen Bild rollt nun auf den Rädern 14 nach vorne und hinten. Da werden noch weitere Räder auf der Außenseite hinzukommen, damit das Gewicht weiter verteilt und kein Drehmoment durch einseitige Belastung auf die zugehörigen Alustangen kommt. Zwischen den Rädern 14 sitzen Klips-Platten als Abstandshalter. Außerdem - das ist hier noch gar nicht realisiert - muss die Hauptmechanik auch noch gegen verdrehen und seitliches Verschieben auf dem Wagen bewahrt werden. Das kommt am nächsten Wochenende, wenn ich mal wieder Zeit für ft habe.