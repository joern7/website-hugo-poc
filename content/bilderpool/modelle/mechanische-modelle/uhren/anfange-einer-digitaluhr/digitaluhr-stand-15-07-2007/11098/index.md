---
layout: "image"
title: "Galvanische Trennung"
date: "2007-07-16T16:20:57"
picture: "digitaluhr2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11098
- /details46b8.html
imported:
- "2019"
_4images_image_id: "11098"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11098 -->
Hier noch eine Detailaufnahme des Selbstbau-Trafos. Durch Verschieben kann man den Abstand zwischen den beiden Elektromagneten gut justieren.