---
layout: "image"
title: "Gesamtansicht von hinten"
date: "2007-07-16T16:20:57"
picture: "digitaluhr3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11099
- /details0fe2-2.html
imported:
- "2019"
_4images_image_id: "11099"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11099 -->
So sieht das Ganze derzeit von Hinten aus. Die Segment-Verstell-Mechanik sitzt direkt hinter den Ziffern, die für das Endprodukt noch höher gesetzt werden müssen als sie jetzt angebaut sind. Die Mechanik kann auf den Schienen zu jeder der vier Ziffern verfahren werden, was die dritte Bewegungsachse darstellt - aber das alleine genügt noch nicht...