---
layout: "image"
title: "Der Wagen von unten"
date: "2007-07-16T16:20:57"
picture: "digitaluhr9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11105
- /details88bc.html
imported:
- "2019"
_4images_image_id: "11105"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11105 -->
So sieht der Wagen Stand heute Abend von unten aus. Da fehlt natürlich noch eine ganze Menge.