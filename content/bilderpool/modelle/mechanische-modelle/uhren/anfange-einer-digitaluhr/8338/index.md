---
layout: "image"
title: "Ein Segment von vorne."
date: "2007-01-08T16:53:45"
picture: "digitaluhrprototyp4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8338
- /detailsea55-3.html
imported:
- "2019"
_4images_image_id: "8338"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T16:53:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8338 -->
Die Segmente selbst sind mit so wenigen Bauteilen wie möglich aufgebaut, denn jedes verbaute Teil muss mit 28 (Segmenten insgesamt) multipliziert werden, und da wird's bei einigen Bauteilen schon dünn mit meinem Bestand.