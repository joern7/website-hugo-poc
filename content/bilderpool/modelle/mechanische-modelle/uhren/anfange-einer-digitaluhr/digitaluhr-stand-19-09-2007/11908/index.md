---
layout: "image"
title: "Die ganze Uhr von hinten"
date: "2007-09-23T11:13:28"
picture: "digitaluhr3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11908
- /details15a7.html
imported:
- "2019"
_4images_image_id: "11908"
_4images_cat_id: "1067"
_4images_user_id: "104"
_4images_image_date: "2007-09-23T11:13:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11908 -->
Knapp 3 m 30adriges Flachbandkabel führen von den RoboInt/Extensions zum Wagen. Am Wagen unten (schräg) sieht man den MiniMot für die 30 mm Vor/Zurück-Bewegung und den PowerMotor für das Anheben der 7 Magnete. Der Motor für die seitliche Verschiebung des Wagens zu den einzelnen Ziffern befindet sich auf der Unterseite des Wagens. Er läuft an der an der hier vorderen (auf diesem Bild hinten liegenden) Schiene befestigten Zahnstange entlang.