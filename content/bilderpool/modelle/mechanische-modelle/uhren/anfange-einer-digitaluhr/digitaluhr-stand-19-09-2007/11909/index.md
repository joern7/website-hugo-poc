---
layout: "image"
title: "Seitlicher Einblick"
date: "2007-09-23T11:13:28"
picture: "digitaluhr4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11909
- /details8c22-2.html
imported:
- "2019"
_4images_image_id: "11909"
_4images_cat_id: "1067"
_4images_user_id: "104"
_4images_image_date: "2007-09-23T11:13:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11909 -->
Ohne den Wagen könnte das ganze auch als eine Autowaschstraße durchgehen ;-)

Da sich ja letztlich doch herausgestellt hat, dass der nach Abzug allen Spiels erreichbare Netto-Hub der Segmente mit 1 - höchstens 3 mm einfach zu gering ausfällt, um einen ordentlichen optischen Effekt zu bewirken, und da mir Dirk Haizmann auf der Convention netterweise noch 3 Pneumatikzylinder und 3 Magnetventile vermacht hat, werde ich die Uhr - sollte ich mal wieder zum ft-Bauen kommen - wohl doch noch auf 7 Pneumatikzylinder anstatt 7 Elektromagnete umbauen.