---
layout: "image"
title: "7-Segment-Anzeige"
date: "2007-01-08T16:53:44"
picture: "digitaluhrprototyp1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8335
- /detailse245.html
imported:
- "2019"
_4images_image_id: "8335"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T16:53:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8335 -->
Dieses Bild ist mit Blitz gemacht worden, weil man da die Segmente besser sieht. Die Wirkungsweise der Anzeige wird auf den nächsten Bildern beschrieben. Die Uhr soll von einem Robo-Interface mit I/O-Extensions autonom gesteuert werden. Als Zeitbasis will ich die hervorragend genauen 50 Hz aus dem Wechselstromausgang des alten ft-Trafos auf einen h4 Grundbaustein lenken und über ein paar Flip-Flops eine Frequenz zum RoboInt leiten, die dieses locker neben der anderen Arbeit mitzählen kann. Naja, Detailprobleme wird es schon noch in hinreichender Anzahl geben ;-)