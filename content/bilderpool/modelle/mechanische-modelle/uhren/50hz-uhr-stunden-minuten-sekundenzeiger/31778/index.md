---
layout: "image"
title: "Vorderansicht"
date: "2011-09-10T17:53:45"
picture: "Vorderansicht.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/31778
- /detailsd71f-3.html
imported:
- "2019"
_4images_image_id: "31778"
_4images_cat_id: "2371"
_4images_user_id: "1088"
_4images_image_date: "2011-09-10T17:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31778 -->
Das Design ist so angelegt, daß man durch die Zeiger möglichst viel vom 50Hz-Motor hinten sieht.