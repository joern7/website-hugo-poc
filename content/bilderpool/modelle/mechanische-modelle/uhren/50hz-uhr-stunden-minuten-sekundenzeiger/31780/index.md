---
layout: "image"
title: "Linke Seite"
date: "2011-09-10T17:53:45"
picture: "LinkeSeite.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/31780
- /details44c4.html
imported:
- "2019"
_4images_image_id: "31780"
_4images_cat_id: "2371"
_4images_user_id: "1088"
_4images_image_date: "2011-09-10T17:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31780 -->
Das Schwungrad des Motors macht 150 Umdrehungen pro Minute (Erklärung zwei Fotos weiter). Mittels zwei Schnecken, einem Z15 und einem Z10 wird auf 1 Umdrehung pro Minute untersetzt. Diese Drehgeschwindigkeit hat die Abtriebswelle des Motors links. Der Aufbau des Uhrenkopfes mit den drei Zeigern wird an anderer Stelle erklärt.