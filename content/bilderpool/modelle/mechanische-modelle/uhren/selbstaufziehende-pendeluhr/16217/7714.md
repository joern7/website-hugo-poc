---
layout: "comment"
hidden: true
title: "7714"
date: "2008-11-07T17:15:11"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo flyingcat,

das sieht aus wie mit dem Publisher hochgeladen. Der hat neben den Bildern je eine Schaltfläche zum Drehen in 90°-Schritten und eine Beschneidungsfunktion, die die Details besser sichtbar macht. Nur ein Tipp.

Gruß,
Stefan