---
layout: "image"
title: "Frontansicht"
date: "2008-11-07T16:44:35"
picture: "selbstaufziehendependeluhr01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16217
- /details5219-3.html
imported:
- "2019"
_4images_image_id: "16217"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16217 -->
Ich habe die Uhr extra so geplant, dass die Hemmung unterhalb des Ziffernblattes sichtbar bleibt.