---
layout: "image"
title: "Gewichte 2"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16225
- /details5a3a.html
imported:
- "2019"
_4images_image_id: "16225"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16225 -->
Das linke Gewicht im Bild ist in der oberen Endlage und wartet auf seinen Einsatz. Das rechte Gewicht treibt die Uhr. Die Laufzeit eines Gewichts von oben bis unten dauert etwa 35 Minuten.