---
layout: "image"
title: "Regulierung"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16228
- /details2742.html
imported:
- "2019"
_4images_image_id: "16228"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16228 -->
Über die Schnecke kann die Lage des Pendelauges justiert werden und damit die Genauigkeit der Uhr eingestellt werden.