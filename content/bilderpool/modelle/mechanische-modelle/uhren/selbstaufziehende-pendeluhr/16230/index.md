---
layout: "image"
title: "Blockiereinheit"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16230
- /details30c9.html
imported:
- "2019"
_4images_image_id: "16230"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16230 -->
Noch ein Blick auf ein Blockade-Z20 und den Kontrolltaster dazu.