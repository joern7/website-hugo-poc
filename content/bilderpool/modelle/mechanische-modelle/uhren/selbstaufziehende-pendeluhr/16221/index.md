---
layout: "image"
title: "Blockade"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16221
- /detailsb7bd.html
imported:
- "2019"
_4images_image_id: "16221"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16221 -->
Hier ist einer der Zylinder zu sehen, der bei Bedarf ein Blockier-Z20 aus dem Weg räumt (hier gerade in Blockierstellung). Es wird nur ein Bruchteil des möglichen Zylinderwegs genutzt. Der Getriebeaufbau auf der rechten Seite bildet die Untersetzung zwischen Hemmung und Zeigern. Der Motor in der linken Bildhälfte zieht bei Bedarf ein Gewicht nach oben.