---
layout: "image"
title: "Gewichte"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16224
- /detailse574-3.html
imported:
- "2019"
_4images_image_id: "16224"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16224 -->
Die beiden Gewichte wiegen jeweils knapp 2 kg.