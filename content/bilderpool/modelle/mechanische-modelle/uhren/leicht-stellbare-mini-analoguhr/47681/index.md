---
layout: "image"
title: "Uhrwerk (3)"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47681
- /detailsa048.html
imported:
- "2019"
_4images_image_id: "47681"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47681 -->
Das zuletzt genannte Z22-Ritzel geht nochmal auf eine Z44-Achse (damit haben wir also von der Minutenachse zwei Mal eine Untersetzung 22:44 und also insgesamt 1:4).

----------

The last-named Z22 sprocket turns another Z44 axis (resulting in a gear reduction of two times 22:44, that is 1:4, counting from the minute axis).