---
layout: "image"
title: "Uhrwerk (1)"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47679
- /detailsc062.html
imported:
- "2019"
_4images_image_id: "47679"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47679 -->
Die Minutenachse steckt locker im Z10 des Schrittmotors und wird auch davon nochmal sauber geführt. Auf ihr sitzt ein Ritzel mit 22 feinen Zähnen. Das in der Bildmitte etwas verdeckte schwarze Zwischenzahnrad ist ein 31082 "Rastachse + Zahnrad Z28 m0,5" und dient nur der richtigen Drehrichtung des Stundenzeigers. Darüber geht es weiter auf eine 31050 "Metallachse 50 + Zahnrad Z44 m0,5", auf dem wiederum ein Z22-Ritzel sitzt.

----------

The minute axis sits lightly in he Z10 of the stepper motor for additional guiding. It carries a sprocket with 22 fine teeth. The not fully visible black intermediate gearwheel in the middle of this image is a 31082 "Rastachse + Zahnrad Z28 m0,5" and serves only for the correct turning direction of the hour hand. The gear continues with a 31050 "Metallachse 50 + Zahnrad Z44 m0,5", on whose axis there is a second Z22 sprocket.