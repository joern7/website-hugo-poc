---
layout: "overview"
title: "Leicht stellbare Mini-Analoguhr mit Schrittmotor"
date: 2020-02-22T08:16:11+01:00
legacy_id:
- /php/categories/3515
- /categories39b7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3515 --> 
Diese kleine Uhr wird von einem Schrittmotor gedreht, der alle 60 Sekunden von einem Netduino 3, der in C# (.net) programmiert ist, angesteuert wird. Man kann die Uhrzeit auch während des Laufs einfach einstellen.