---
layout: "comment"
hidden: true
title: "11535"
date: "2010-05-12T13:20:35"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch Ingo,

diese Geräusche wegen des Abriebs sollen genau dadurch vermieden oder zumindest hinreichend lange hinausgezögert werden, dass ich den Motor so langsam wie nur möglich laufen lasse. Die Uhr läuft momentan nicht mehr durch, weil unsere kriminellen Katzen sich eh einen Spaß draus machen, mit den Pfoten in den Motor zu hauen oder die Kette abzuheben ;-) Aber bis zur Convention wird's schon halten, und danach wird die Uhr eh wieder abgebaut. Wenns notwendig werden sollte, hab ich auch noch Blockflöten-Vaseline (aus meiner Schulzeit!) bei meinem ft-Bestand; die hat sich in solchen Fällen auch schon öfters bewährt.

Gruß,
Stefan