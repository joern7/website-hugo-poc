---
layout: "image"
title: "Ziffernbänder"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27018
- /details77e3-3.html
imported:
- "2019"
_4images_image_id: "27018"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27018 -->
In Word wurde in einem vierspaltigen Dokument in jeder Spalte der Seite je eine zweispaltige Tabelle mit 15 mm Spaltenbreite und der jeweils notwendigen errechneten Zeilenhöhe eingefügt, deren Gesamthöhe dem halben Walzenumfang entspricht. Hier sind, von links nach rechts, abgebildet (nicht in Originalgröße):

- Die zwei Streifen für das Sekundenrad,

- die zwei Streifen für 0 - 4 und 5 - 9 der Einerstelle der Minuten,

- zwei identische Streifen 0 - 5 für die Zehnerstelle der Minuten (andernfalls müsste das Rad doppelt so weit je Takt gedreht werden, was die Mechanik unnötig schwierig gemacht hätte) sowie

- die zwei Streifen für 00 - 11 und 12 - 23 des Stundenrades.