---
layout: "overview"
title: "Flüsterleise präzise mechanische Digitaluhr"
date: 2020-02-22T08:15:35+01:00
legacy_id:
- /php/categories/1943
- /categories4bfb.html
- /categories481d.html
- /categories160b.html
- /categoriesd215.html
- /categories1103.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1943 --> 
Eine fast unhörbar leise und deshalb absolut wohnzimmertaugliche mechanische Digitaluhr, die ihre Präzision aus dem 50-Hz-Takt des Stromnetzes bezieht - ganz ohne Computerei.