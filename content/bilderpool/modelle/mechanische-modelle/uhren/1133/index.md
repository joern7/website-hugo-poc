---
layout: "image"
title: "Uhrmitgewichten0659"
date: "2003-05-21T15:05:28"
picture: "klokftdetail0659.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/1133
- /details4cb7.html
imported:
- "2019"
_4images_image_id: "1133"
_4images_cat_id: "615"
_4images_user_id: "7"
_4images_image_date: "2003-05-21T15:05:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1133 -->
Detail vom Zahnrad einer Uhr mit Gewichten , komplett aus FT gebaut .
(Prüfmodell , hat aber gut gelaufen ).
 Das grosse "Zahnrad" mit die 12 Metallachsen  : hierin greift das échappement ein . (Das échappement ist dann selbst verbunden mit dem Pendel )