---
layout: "image"
title: "Uhr von links"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34851
- /details68e1.html
imported:
- "2019"
_4images_image_id: "34851"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34851 -->
Rechts kann man gut den Festo-Pneumatik-Rollenhebel erkennen, den ich als Sperre für das Z15 verwende - eine weitere Idee von Steffalk. Natürlich funktionieren auch andere Sperrklinken; der Rollenhebel ist aber besonders leichtgängig, sieht elegant aus - und funktioniert perfekt.