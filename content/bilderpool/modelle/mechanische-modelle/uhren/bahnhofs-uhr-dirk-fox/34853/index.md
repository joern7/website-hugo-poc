---
layout: "image"
title: "Rückansicht der Uhr"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34853
- /details150c.html
imported:
- "2019"
_4images_image_id: "34853"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34853 -->
Hier sieht man den Antrieb des Rast-Z10 über die Rastkurbel am U-Getriebe. Der Festo-Pneumatik-Rollenhebel dient auch hier als Sperre.
Natürlich kann hier auch ein anderer (präziserer und hübscherer) Antrieb als ein Mini-Motor verwendet werden.