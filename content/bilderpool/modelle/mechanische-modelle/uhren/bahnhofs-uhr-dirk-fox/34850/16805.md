---
layout: "comment"
hidden: true
title: "16805"
date: "2012-05-04T18:34:23"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,
Danke für Eure netten Anmerkungen!

@steffalk: ja, bitte ein Z25 (oder ein Z50)!

@Ingo: nein, bei mir ist nichts gekürzt - ich kann kein Blut sehen (Harald würde vermutlich sagen: mir fehlt die Gelassenheit...). Du meinst wahrscheinlich die Kunststoffachse 60 (38416).

@sven, Marcel: Mindestens ein Uhren-Modell (Getriebebau) gehört in einen der ft-Kästen - allein schon technikgeschichtlich; am liebsten zusammen mit geometers Planetarium.

@remadus: ja, für das Background-Know-How gibt es ja - zum Glück - die ft:pedia :-), freue mich schon riesig auf Deinen Beitrag!

Gruß, Dirk