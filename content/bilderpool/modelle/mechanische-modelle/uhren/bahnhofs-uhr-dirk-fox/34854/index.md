---
layout: "image"
title: "Uhr von hinten rechts"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34854
- /details95c2.html
imported:
- "2019"
_4images_image_id: "34854"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34854 -->
Das Getriebe von der linken Seite. Gut zu erkennen die Gelenkkurbel oben links, die im Minutentakt das Z15 weiterschaltet.