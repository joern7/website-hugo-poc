---
layout: "image"
title: "Vorderansicht der Uhr"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34856
- /details198a-2.html
imported:
- "2019"
_4images_image_id: "34856"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34856 -->
Die Uhr von vorne: Man sieht den Stunden- und Minutenzeiger, sowie oben links den separaten Sekundenzeiger.
Der Stundenzeiger wird von dem schwarzen Rast-Z10 unten links am Z40 - mit aufgesteckter Drehscheibe 60 und Freilaufnabe - angetrieben.

Der Mini-Motor steht ein wenig hässlich ab - er ist jedoch nur als Beispielantrieb gedacht. Ein 50-Hz-Motor oder ein Pendel stünden der Uhr deutlich besser zu Gesicht...