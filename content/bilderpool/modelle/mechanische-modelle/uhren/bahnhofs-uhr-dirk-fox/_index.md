---
layout: "overview"
title: "'Bahnhofs'-Uhr (Dirk Fox)"
date: 2020-02-22T08:15:44+01:00
legacy_id:
- /php/categories/2580
- /categories4d86.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2580 --> 
Uhr mit separatem Sekundenzeiger, die - wie eine Bahnhofsuhr - den Sekunden- und Minutenzeiger nicht kontinuierlich, sondern diskret weiterschaltet.