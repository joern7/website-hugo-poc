---
layout: "image"
title: "Das Getriebe"
date: 2020-04-14T17:29:14+02:00
picture: "gone-uhr-getriebe.JPG"
weight: "2"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das Getriebe besteht aus 2 Stufen.

1. Zwischen dem großen Zeiger und dem kleinen hängt eine Untersetzung von 100:1
2. von der Taktwelle (Minimotor – Taktscheibe) wird 100:1 untersetzt, um den kleinen Zeiger im Centi-Gone Takt zu bewegen. 