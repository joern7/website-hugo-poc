---
layout: "image"
title: "Getriebedetail"
date: 2020-04-14T17:29:12+02:00
picture: "gone-uhr-detail.JPG"
weight: "3"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Taktgeber:
Im ftDuino läuft eine einfache Schleife alle 8640 mSekunden, die einen kurzen Impuls auf den Minimotor geben. Dieser reicht aus, um den Minimotor so lange laufen zu lassen, bis die Taktscheibe (im Bild links unten) auf der Schneckenwelle den Taster zur Selbsthaltung aktiviert. Nun läuft der Motor so lange weiter, bis der Taster durch die Austastlücke der Impulsscheibe abfällt und zum Stehen kommt. Dies dauert kürzer als der Zeittakt (also weniger als 8,64 Sekunden).

8640 ms?
Auf die Takteinheit kommt man, indem die Getriebeuntersetzung auf einen Tag gerechnet wird:
Großer Zeiger = 1 Umdrehung / Tag
Kleiner Zeiger = 100 Umdrehungen / Tag = 100 Gone
Antrieb/Taktscheibe = 10 000 Umdrehungen / Tag = 100 Umdrehungen pro Gone = 100 * 100 pro Tag
24 Stunden * 60 Minuten * 60 Sekunden / 10000 = 8640 ms