---
layout: "image"
title: "Wagen (1)"
date: "2008-11-09T17:53:53"
picture: "digitaluhrvwagen01.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16243
- /details6417.html
imported:
- "2019"
_4images_image_id: "16243"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16243 -->
Dies ist der Wagen in der Gesamtansicht von seiner linken Seite. Rechts sieht man die Schubgelenke, links unten den Motor, der den Geräteträger nach vorne und hinten schiebt, und links oben die Pneumatikbaugruppe.