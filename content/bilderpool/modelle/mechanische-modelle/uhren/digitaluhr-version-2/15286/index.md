---
layout: "image"
title: "Elektronik (2)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15286
- /details28c1.html
imported:
- "2019"
_4images_image_id: "15286"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15286 -->
Die 50 Hz kommen hier rein: Aus einem alten fischertechnik-Trafo, der noch einen echten Wechselspannungsausgang hat, geht es über die zwei Lampen als Vorwiderstand auf den rechten dieser zwei Elektromagnete. Der bildet mit seinem Gegenstück links einen kleinen Transformator, damit das Signal potentialfrei gegenüber der Versorgungsspannung auf die Eingänge des Grundbausteins geführt werden können.