---
layout: "image"
title: "Gesamtansicht von Links"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15287
- /detailsc7e1.html
imported:
- "2019"
_4images_image_id: "15287"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15287 -->
Ein schöner Rücken kann auch entzücken. ;-)

Außer (gefühlten) zweieinhalb Millionen BS7,5 und Federnocken sind verbaut:

1 Robo Interface
2 Robo I/O Extensions
5 Silberlinge (1 Gleichrichter, 1 Grundbaustein, 3 Flip-Flops)
9 Elektromagnete (2 für die 50-Hz-Elektronik, 7 für die Ventilsteuerung)
21 Pneumatik-Ventile (7 Schließer, 14 Öffner - je 7 bei den Elektro-Ventilen und 7 bei der Ventilgruppe)
8 Betätiger (7 Doppelbetätiger für die Zylinder-Ventile, 1 Einfachbetätiger für die Druckkontrolle).
7 Pneumatik-Zylinder
1 30adriges Flachbandkabel, sonstige Kabel und ein paar Pneumatikschläuche ;-)