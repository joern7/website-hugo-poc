---
layout: "image"
title: "Wagen (13)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen13.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16255
- /details77be.html
imported:
- "2019"
_4images_image_id: "16255"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16255 -->
Zu guter Letzt das Einfachste: Die Unterseite.