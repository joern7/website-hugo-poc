---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-16T21:35:33"
picture: "digitaluhrv01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15268
- /details7483.html
imported:
- "2019"
_4images_image_id: "15268"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15268 -->
So sieht das Teil von vorne mit angebrachten Klarsichtfolien aus. Es ist jetzt gerade 17:39 Uhr. Die vier Siebensegmentanzeigen werden elektropneumatisch betätigt, indem gelbe Statikträger 60 hinter grauen Statikträgern versteckt und damit "ausgeschaltet" oder eben durch Verschieben sichtbar und damit "eingeschaltet" werden. Ein Video wird's auch geben.