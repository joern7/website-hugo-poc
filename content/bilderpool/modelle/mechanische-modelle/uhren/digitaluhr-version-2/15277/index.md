---
layout: "image"
title: "Die Stellmechanik (2)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15277
- /details766b-2.html
imported:
- "2019"
_4images_image_id: "15277"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15277 -->
Man blickt hier von schräg oben auf die Schiebemechaniken:

Die sieben Pneumatikzylinder betätigen je eine Kipphebel-Mechanik. Die schiebt die Angriffspunkte der Segmente in die gewünschte Richtung. Diese Angriffspunkte sind bei den senkrecht stehenden Segmenten Bausteine 7,5 und bei den waagerecht stehenden Segmenten etwas Kürzeres - deren Verschiebeachsen sitzen nämlich 7,5 mm näher an der Steuermechanik. Es wurden die Innereien von Bausteinen mit einem Zapfen, also die Zapfen selbst mit ihren Metallstiften, verwendet, von denen ich einige von kaputten Bausteinen habe.

Der Ablauf zum Umschalten einer Ziffer von einer Zahl auf eine andere ist damit wie folgt:

1. Der Wagen muss zur jeweiligen Ziffer gefahren werden, falls er dort nicht schon steht. Er muss die Einerstelle der Minuten ja z. B. nur alle zehn Minuten verlassen.

2. Die Kippmechanik wird durch die Ansteuerung der Elektromagnete genau *umgekehrt* eingestellt, wie die Ziffern später stehen soll. Damit kommen die Kipphebel immer von der Richtung, *aus* der sie die Segmente schieben müssen.

3. Der Geräteträger wird nach vorne in die Ziffer hinein gefahren.

4. Die Kippmechanik wird genau so eingestellt, wie die Ziffern stehen sollen: Die Segmente werden in die richtige Richtung bis zum Anschlag geschoben, falls sie nicht sowieso schon da standen.

5. Nach einer Sekunde Wartezeit werden die Kippmechaniken wieder umgekehrt eingestellt, damit sie keinen Kontakt mehr mit den Segmenten haben.

6. Jetzt wird der Geräteträger wieder nach hinten aus der Ziffer hinaus transportiert. Nichts hakt oder hängt, weil die Kippmechaniken ja wieder weg von den Angriffspunkten eingestellt sind.