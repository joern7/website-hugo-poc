---
layout: "image"
title: "Wagen (4)"
date: "2008-11-09T17:53:53"
picture: "digitaluhrvwagen04.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16246
- /detailscfa0.html
imported:
- "2019"
_4images_image_id: "16246"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16246 -->
Hier ein Überblick über die vor/zurück-Stellmechanik.