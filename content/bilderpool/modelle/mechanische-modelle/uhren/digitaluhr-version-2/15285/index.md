---
layout: "image"
title: "Elektronik (1)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15285
- /details32c9.html
imported:
- "2019"
_4images_image_id: "15285"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15285 -->
Die Elektronikbausteine links verarbeiten den 50-Hz-Takt zu 375 Impulsen pro Minute. Der Grundbaustein links unten, der als Operationsverstärker beschaltet ist, digitalisiert die 50 Hz. Das sind 50 Hz * 60 s = 3000 Impulse pro Minute. Die drei Flip-Flops-Bausteine rechts teilen das drei Mal durch 2, insgesamt also durch 8. Das ergibt 3000 / 8 = 375 Impulse pro Minute. Dieses Signal wird an einen Eingang des Robo-Interfaces geleitet. Das hat damit 160 ms Zeit, eine ganze Periode (zwei Flankenwechsel) mitzuzählen. Das schafft es locker während seiner anderen Arbeit.

Hier sieht man auch den Motor, der den ganzen Wagen von Ziffer zu Ziffer bewegt.