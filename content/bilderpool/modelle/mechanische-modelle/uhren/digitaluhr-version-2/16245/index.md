---
layout: "image"
title: "Wagen (3)"
date: "2008-11-09T17:53:53"
picture: "digitaluhrvwagen03.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16245
- /detailsa93d-2.html
imported:
- "2019"
_4images_image_id: "16245"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16245 -->
Auf der Rückseite sieht man oben die eine Hälfte der Pneumatikorgie: Rechts oben den Ventilblock, sowie drei der elektropneumatischen Ventile. Die anderen vier davon befinden sich auf der anderen Seite der roten Bauplatten 45 x 90.