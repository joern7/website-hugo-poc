---
layout: "overview"
title: "Digitaluhr Version 2"
date: 2020-02-22T08:15:19+01:00
legacy_id:
- /php/categories/1396
- /categories9888.html
- /categories1164.html
- /categories5d91.html
- /categoriesd431.html
- /categories5cba.html
- /categories7cc3.html
- /categories327e.html
- /categoriese81a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1396 --> 
Nachdem die Digitaluhr für die Convention 2007 ja nur \"fast\" funktionierte (man konnte die Ziffern nicht lesen), gibt es jetzt hier Version 2 mit lesbarer Anzeige.