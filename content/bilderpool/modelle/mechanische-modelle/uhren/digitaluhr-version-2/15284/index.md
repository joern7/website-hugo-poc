---
layout: "image"
title: "Verschieben des Geräteträgers (2)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15284
- /details472a-2.html
imported:
- "2019"
_4images_image_id: "15284"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15284 -->
Dieses Bild zeigt den Wagen von links. Man sieht den Motor, der den Geräteträger über Schnecke und Z15, auf dessen Achse die Exzenterkurbel sitzt, nach vorne und hinten schiebt.

Oben links sieht man zwei der elektropneumatischen Ventilbaugruppen.