---
layout: "overview"
title: "50-Hz-Electronics-Zeigeruhr mit Sekundenzeiger"
date: 2020-02-22T08:15:53+01:00
legacy_id:
- /php/categories/3172
- /categories3ba9.html
- /categoriesf1a5.html
- /categories645d.html
- /categories1937-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3172 --> 
Dieses Modell benutzt die Zähler-Funktionen des 2015er Electronics-Moduls zum Takten einer relativ kompakten Zeigeruhr mit Sekundenzeiger.