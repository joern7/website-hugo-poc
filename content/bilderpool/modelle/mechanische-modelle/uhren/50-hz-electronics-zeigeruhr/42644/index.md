---
layout: "image"
title: "Arbeitsweise (5) - Taster, Sekundenwelle und Übergang zum Minutenzeig4er"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42644
- /details6a44.html
imported:
- "2019"
_4images_image_id: "42644"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42644 -->
Arbeits- und Ruhekontakt des Tasters sind mit den beiden Ausgängen des E-Tec-Moduls verbunden. Über den Stelltaster und eine ft-Lampe als Vorwiderstand geht es dann vom Zentralkontakt des Tasters zum Motor, dessen anderer Pol einfach mit "-" verbunden ist. Bei jedem Umschalten des E-Tec, was ja wegen der 2 Sekunden Periodenlänge ein Mal pro Sekunde geschieht, bekommt der Motor also genau so lange Strom, bis der Taster die jeweils andere Stellung einnimmt. Diese Schaltungsart wird in der nächsten ft:pedia noch ausführlich beleuchtet.

Auf der Sekundenwelle sitzt die Schnecke. Über ein Winkelgetriebe geht es auf ein Z40. Damit haben wir eine Untersetzung von 1:10 * 1:4 = 1:40.