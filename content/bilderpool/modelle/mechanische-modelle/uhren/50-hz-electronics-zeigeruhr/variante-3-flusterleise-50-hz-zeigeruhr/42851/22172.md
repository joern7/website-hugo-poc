---
layout: "comment"
hidden: true
title: "22172"
date: "2016-06-26T16:30:33"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Diese Uhr hörte nach einer Reihe von Wochen im Dauerbetrieb auf, zu funktionieren. Der Sekundenzeiger konnte beim Stand um die 45 Sekunden nach der vollen Minute herum nicht mehr angehoben werden. Er hat ja kein Gegengewicht, und das Drehmoment des Selbstbaumotors genügte plötzlich nicht mehr.

Es folgten mehrere enttäuschende Versuche zur Ursachenforschung: Selbst 4 anstatt 2 Magnete und ein separates starkes 9V-Netzteil für die Gleichspannung genügten nicht. Die Uhr war schwergängig geworden. Ich befürchtete Staub in Achslagern - das hätte eine komplette Zerlegung der Uhr erfordert, und bis zur Convention sind's doch nur noch ein paar Monate ;-)

Just habe ich mich nochmal dransetzen können und herausgefunden: Die zentrale Achse des Sekundenzeigers war die schwergängige. Und nicht etwa wegen Staub oder einer verkanteten Achslagerung oder sowas. Nein, lediglich die beiden Klemmringe ganz vorne auf der Achse mussten einen Hauch lockerer eingestellt werden. Die sorgen dafür, dass die beiden lose eingelegten Rastachsen 30 für den Antrieb des Minutenzeigers durch die Drehscheibe hindurch nicht zu locker sitzt und dadurch der Minutenzeiger zu viel Spiel bekommt.

Nach genauer Justierung und etwas Probe läuft die Uhr jetzt wieder ganz normal durch. Ich werde natürlich beobachten, ob und wie lange das nun hilft.

Gruß,
Stefan