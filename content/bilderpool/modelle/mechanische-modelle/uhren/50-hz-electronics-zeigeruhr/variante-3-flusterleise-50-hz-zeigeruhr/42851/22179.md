---
layout: "comment"
hidden: true
title: "22179"
date: "2016-06-27T00:52:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hi H.A.R.R.Y.,

zwischen Klemmbuchsen und Rastadapter ist etwas Platz, da beißt sich nichts. Die nächsten Tage werden zeigen, ob es tatsächlich nur am zu strammen Klemmen lag.

Gruß,
Stefan