---
layout: "comment"
hidden: true
title: "22178"
date: "2016-06-26T20:11:03"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

es könnte auch am Schlitz der Klemmbuchsen liegen. In einer bestimmten Stellung drückt sich die Rundung des Rastachsadapter leicht in diesen Schlitz.

Eventuell beide Klemmbuchsen durch 3 bis 4 Riegelscheiben ersetzen, oder 1 Klemmbuchse durch einen Abstandsring 31597.

Nur so eine Idee.

Grüße
H.A.R.R.Y.