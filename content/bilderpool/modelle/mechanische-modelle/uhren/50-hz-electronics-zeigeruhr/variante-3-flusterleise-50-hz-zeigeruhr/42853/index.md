---
layout: "image"
title: "Elektromagnete"
date: "2016-01-30T17:54:49"
picture: "fluesterleisehzzeigeruhrmitsekundenzeiger3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42853
- /detailsebcc-2.html
imported:
- "2019"
_4images_image_id: "42853"
_4images_cat_id: "3186"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T17:54:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42853 -->
Die zwei Elektromagnete werden mit einer 4-Phasen-Ansteuerung betrieben, gerade wie in http://www.ftcommunity.de/details.php?image_id=27014. Die Magnete ziehen immer einen Dauermagneten zu genau einem der vier Pole der beiden Elektromagnete und halten ihn danach für den Rest der Sekunde sogar noch fest. Ein Überschwingen ist also nicht schlimm; der Rotor arbeitet total zuverlässig ohne kumulative Fehler - wie ein Schrittmotor.