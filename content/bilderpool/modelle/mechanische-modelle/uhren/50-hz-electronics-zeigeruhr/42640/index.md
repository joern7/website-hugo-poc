---
layout: "image"
title: "Arbeitsweise (1) - Wechselspannungseingang"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42640
- /details9eca.html
imported:
- "2019"
_4images_image_id: "42640"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42640 -->
Die leere Lampenfassung ist der Eingang für die 50 Hz-Wechselspannung aus einem alten fischertechnik-Trafo. Nach dem Einschalter geht das auf einen hobby-4-Gleichrichterbaustein, der geglättete 9 V Gleichspannung am Ausgang bereitstellt. Man beachte die zusätzliche Leitung am Wechselspannungseingang direkt links oben beim Schalter, die die Wechselspannung in die Zählelektronik führt.