---
layout: "image"
title: "Variante mit optischer Abtastung - Zusätzliche Untersetzung"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42670
- /details27a0.html
imported:
- "2019"
_4images_image_id: "42670"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42670 -->
Außerdem gibt es eine zusätzliche 1:3-Untersetzung, damit die Lichtschrankenauswertung genug Zeit hat, den nächsten Halbschritt zuverlässig festzustellen und den Motor auszuschalten.