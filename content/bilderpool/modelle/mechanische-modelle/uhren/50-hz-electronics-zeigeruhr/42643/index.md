---
layout: "image"
title: "Arbeitsweise (4) - Zahnabtastung"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42643
- /details28fa.html
imported:
- "2019"
_4images_image_id: "42643"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42643 -->
Am Gelenk ist eine Metallachse 30 mit zwei der kleinen Abstandsringe angebracht. Die muss sehr genau so justiert werden, dass sie die Zähne und Zahnzwischenräume abtasten kann. Die Verlängerung des Hebels betätigt einen älteren großen fischertechnik-Taster. Das Angenehme daran ist, dass der so leichtgängig ist. Ist gerade ein Zahn des Z30 bei der Abtastachse, wird er zuverlässig gedrückt. Liegt die Achse gerade im Zwischenraum zwischen zwei Zähnen, wird der Taster zuverlässig losgelassen. Die schwarze Kunststofffeder verringert das Betriebsgeräusch. Wenn der Taster den Hebel in einen Zahnzwischenraum drückt, wird der Hebel von dieser Feder etwas gedämpft aufgefangen.