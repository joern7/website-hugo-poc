---
layout: "image"
title: "Hora Movetura 01"
date: "2017-06-14T20:18:47"
picture: "IMG_7958II.jpg"
weight: "1"
konstrukteure: 
- "ist"
fotografen:
- "ist"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FTbyIST"
license: "unknown"
legacy_id:
- /php/details/45938
- /details9ebf.html
imported:
- "2019"
_4images_image_id: "45938"
_4images_cat_id: "3412"
_4images_user_id: "2145"
_4images_image_date: "2017-06-14T20:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45938 -->
Nach der Idee von Remadus nachgebaut und mit einem Schlagwerk ergänzt.