---
layout: "image"
title: "Hora Movetura 03"
date: "2017-06-14T20:18:47"
picture: "IMG_7952.jpg"
weight: "3"
konstrukteure: 
- "ist"
fotografen:
- "ist"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FTbyIST"
license: "unknown"
legacy_id:
- /php/details/45940
- /details1b9d-3.html
imported:
- "2019"
_4images_image_id: "45940"
_4images_cat_id: "3412"
_4images_user_id: "2145"
_4images_image_date: "2017-06-14T20:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45940 -->
Das Viertelstunden - Westminster - Schlagwerk. Vier durch den Arduino gesteuerte Servos mit Schlägeln schlagen auf die Klangstäbe einer alten Uhr. Zu jeder Viertelstunde mit einem weiteren Takt.