---
layout: "overview"
title: "Hora Movetura mit Westminsterschlagwerk"
date: 2020-02-22T08:16:03+01:00
legacy_id:
- /php/categories/3412
- /categories0f84.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3412 --> 
Klassik meets Moderne

Ich habe vor einigen Jahren die Standuhr von M. Rohmann nachgebaut. Inzwischen habe ich ein Westminsterschlagwerk hinzugefügt. Dieses wird von einem Arduino Uno gesteuert. Der Stundenschlag wird durch ein Stück Papier, welches durch eine Gabellichtschranke fährt, ausgelöst. Dieses Papierstück ist rückwärtig an der Hora Movetura angebracht. Ansonsten ist das Schlagwerk unabhängig von der Uhr.
 Die jeweiligen Viertelstundenschläge löst der Arduino anschliesend selbstständig aus und wartet nach der dritten Viertelstunde wieder auf das Signal der Lichtschranke.