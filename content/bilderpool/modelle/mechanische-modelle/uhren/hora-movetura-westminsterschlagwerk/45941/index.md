---
layout: "image"
title: "Hora Movetura 04"
date: "2017-06-14T20:18:47"
picture: "IMG_7955.jpg"
weight: "4"
konstrukteure: 
- "ist"
fotografen:
- "ist"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FTbyIST"
license: "unknown"
legacy_id:
- /php/details/45941
- /detailsa7d0.html
imported:
- "2019"
_4images_image_id: "45941"
_4images_cat_id: "3412"
_4images_user_id: "2145"
_4images_image_date: "2017-06-14T20:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45941 -->
Durch die Gabellichtschranke wird zur vollen Stunde durch die mechanische Uhr das Schlagwerk ausgelöst.
Sehen und hören kann man das Ganze hier: https://youtu.be/xm9n1IMXBu0