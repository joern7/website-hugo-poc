---
layout: "image"
title: "Innerer Aufbau"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46796
- /details62a2.html
imported:
- "2019"
_4images_image_id: "46796"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46796 -->
Die beiden kräftigen Elektromagnete sind ganz einfach angebracht, müssen aber hinreichend genau justiert werden, damit sie kräftig genug ziehen können und dennoch nirgendwo mit dem Rotor kollidieren.

Sämtliche Achsen, die durch die Bauplatten 500 gehen, sind direkt in den Nuten gelagert, weil das sehr reibungsarm geht. Eine erste Variante dieser Uhr hatte die Zeigerwelle und die beiden Achsen links und rechts in den Achslöchern der Platten, aber trotz Schmierung mit Vaseline blieb die Uhr damit nach gut einer Woche wegen zu großer Reibung stehen. Also baute ich sie nochmal um und lagerte sämtliche Achsen in den Nuten.