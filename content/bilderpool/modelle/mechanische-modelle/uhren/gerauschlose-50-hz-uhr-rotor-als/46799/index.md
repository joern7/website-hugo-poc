---
layout: "image"
title: "Rotor (2)"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46799
- /detailsc312.html
imported:
- "2019"
_4images_image_id: "46799"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46799 -->
Es ist genau derselbe Rotor wie in https://www.ftcommunity.de/categories.php?cat_id=3416 (er wurde tatsächlich aus der Vorgängeruhr einfach ausgebaut und übernommen).