---
layout: "image"
title: "Rotor (1)"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46798
- /detailsf4a8.html
imported:
- "2019"
_4images_image_id: "46798"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46798 -->
15 alte ft-Dauermagnete sind in einem nicht ganz kreisrunden Aufbau angeordnet.