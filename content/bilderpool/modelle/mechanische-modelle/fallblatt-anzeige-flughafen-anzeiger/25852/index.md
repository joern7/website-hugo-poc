---
layout: "image"
title: "Fallblatt-Anzeige Flughafen-Anzeiger"
date: "2009-11-28T14:39:58"
picture: "Fallblatt-Anzeige.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Fallblatt-Anzeige", "Flughafen-Anzeige", "Bahnhofs-Anzeige", "Klappertafel", "Split-Flap", "Display", "Anzeigetafel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/25852
- /detailsf4c5.html
imported:
- "2019"
_4images_image_id: "25852"
_4images_cat_id: "1815"
_4images_user_id: "724"
_4images_image_date: "2009-11-28T14:39:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25852 -->
Hier ein Nachbau der Bekannten Klapper-Tafeln.
Man kann die Anzeige durch Knopfdruck rotieren lassen.
Die Steuerung über die rechte Codewalze klappt auch prima. Diese wird mit 6 Lichtschranken ausgelesen.

Hier gibt es das Video von der Automatik:
http://www.youtube.com/watch?v=ezVrELNSTAk

Die Steuerung ist der Patentschrift nachempfunden:
http://freenet-homepage.de/laserman/fischertechnik/patentschrift_fallblattanzeige.pdf

Hier gibts ein Video vom gesamten Durchlauf:
http://www.youtube.com/watch?v=d4JTajiwMI0