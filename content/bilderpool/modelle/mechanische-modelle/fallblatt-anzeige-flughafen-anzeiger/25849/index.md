---
layout: "image"
title: "Detail Steuerung und Antrieb"
date: "2009-11-28T14:30:57"
picture: "Detail_Steuerung_und_Antrieb.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Fallblatt-Anzeige", "Flughafen-Anzeige", "Klappertafel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/25849
- /detailsb4b8-2.html
imported:
- "2019"
_4images_image_id: "25849"
_4images_cat_id: "1815"
_4images_user_id: "724"
_4images_image_date: "2009-11-28T14:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25849 -->
Angetrieben mit einem Mini-Mot. und einer dreifachen Untersetzung 4:1 ergibt sich eine Gesamt-Untersetzung von 64:1.
Der Motor dreht am Abgang (Universal-Getriebe) mit ca. 250 Umdr./Min. Durch die Untersetzung werden etwa 4 Umdr./Min. erreicht, was etwa 1 Umdrehung in 15 Sek. entspricht. 
Dadurch schafft es der Antrieb problemlos die 1 Kg schweren Buchstaben anzutreiben.

Hier gibts ein Video vom Antrieb:
http://www.youtube.com/watch?v=hPjJQKjkrMg