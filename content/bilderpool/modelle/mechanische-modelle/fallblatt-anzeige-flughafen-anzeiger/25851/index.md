---
layout: "image"
title: "Detail Buchstabe"
date: "2009-11-28T14:30:58"
picture: "Detail_Buchstabe.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Split-Flap", "Display", "Anzeigetafel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/25851
- /detailsa0e0-2.html
imported:
- "2019"
_4images_image_id: "25851"
_4images_cat_id: "1815"
_4images_user_id: "724"
_4images_image_date: "2009-11-28T14:30:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25851 -->
Insgesamt 39 Ziffern:
26 Buchstaben: A-Z
10 Zahlen: 0-9
3 Sonderzeichen: .(Punkt) /(Schrägstrich) und Leerzeichen