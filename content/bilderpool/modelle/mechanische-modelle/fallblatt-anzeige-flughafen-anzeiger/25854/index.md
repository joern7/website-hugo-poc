---
layout: "image"
title: "6-fach Reflex-Lichtschranke"
date: "2009-11-28T21:31:18"
picture: "6-fach_Reflexlichtschranke.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/25854
- /details42f7.html
imported:
- "2019"
_4images_image_id: "25854"
_4images_cat_id: "1815"
_4images_user_id: "724"
_4images_image_date: "2009-11-28T21:31:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25854 -->
Hier sieht man die Lichtschranke, die (mittlerweile) hinter der Codewalze montiert ist.
Eingekreist ist das Leuchten der Infrarot-LED, das von der Digitalkamera sichtbar gemacht wird.