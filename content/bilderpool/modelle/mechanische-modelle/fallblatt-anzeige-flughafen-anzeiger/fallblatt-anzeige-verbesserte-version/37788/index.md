---
layout: "image"
title: "Fallblatt Anzeige"
date: "2013-11-02T14:06:03"
picture: "Fallblatt-Anzeige.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Fallblatt-Anzeige", "Split", "flap", "display", "Flughafen", "Anzeige", "Airport", "Bahnhof", "Anzeigetafel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/37788
- /details0e6a.html
imported:
- "2019"
_4images_image_id: "37788"
_4images_cat_id: "2808"
_4images_user_id: "724"
_4images_image_date: "2013-11-02T14:06:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37788 -->
Funktionsweise Fallblatt-Anzeige auf Flughäfen und Bahnhöfen
Hier gibt es das Video:
http://youtu.be/NoyxD9BRxh4