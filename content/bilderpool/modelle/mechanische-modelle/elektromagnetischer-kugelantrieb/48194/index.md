---
layout: "image"
title: "Direkt geschalteter Linerarbeschleuniger 2"
date: "2018-09-30T20:40:30"
picture: "kl_Direkt_geschalteter_Linerarbeschleuniger_2.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Elektromagnet", "Kugel", "Beschleunigung"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/48194
- /detailsa3e6.html
imported:
- "2019"
_4images_image_id: "48194"
_4images_cat_id: "3535"
_4images_user_id: "2635"
_4images_image_date: "2018-09-30T20:40:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48194 -->
Die Versuchsstrecke besitzt 2 Elektromagnete und 2 Kontaktstellen. Links ist der Start, dort kann das Kabel mit Klemmkontakten (31338) angeschlossen werden. In der Mitte habe ich blanke Litze unter die Achsen geklemmt. Die Elektromagnete sind seitlich verschiebbar um den besten Ausschaltpunkt einzustellen.

Die im Artikel &#8222;Funktionsmodelle von Gleich- und Wechselstrommotoren&#8220; in der ft:pedia 2016-4 beschriebenen Schutzmaßnahmen empfehle ich dringend, sonst gibt es Schweißspuren auf den Metallachsen und der Kugel. Hier benutze ich 220-Ohm-Widerstände direkt an den Buchsen der E-Magnete.