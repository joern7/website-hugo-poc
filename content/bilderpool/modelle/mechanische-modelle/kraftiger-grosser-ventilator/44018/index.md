---
layout: "image"
title: "Von der Seite"
date: "2016-07-26T10:47:34"
picture: "ventilatorgross4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44018
- /details6c31.html
imported:
- "2019"
_4images_image_id: "44018"
_4images_cat_id: "3258"
_4images_user_id: "558"
_4images_image_date: "2016-07-26T10:47:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44018 -->
Dreht der Rotor schneller, wird der Ring aus Bausteinen gespannt und damit noch fester. Durch diesen Aufbau hält das Rotorblatt auch Drehzahlen eines PM 8:1 bei 12V aus, dann aber kräftig laut bei einem Lüftchen wie im Windkanal.