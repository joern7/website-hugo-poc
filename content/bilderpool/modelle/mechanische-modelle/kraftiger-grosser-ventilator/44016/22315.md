---
layout: "comment"
hidden: true
title: "22315"
date: "2016-07-27T18:24:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Naiver gebaut: Drehscheibe, WS30° drauf, Platten dran. Da sind die Nuten ja genau ungünstig. Aber helfen könnten dann zusätzliche Federnocken oder noch besser Seilklemmstifte in den WS30°. Für fette PowerMotoren allerdings dürfte das auch nicht reichen.

Gruß,
Stefan