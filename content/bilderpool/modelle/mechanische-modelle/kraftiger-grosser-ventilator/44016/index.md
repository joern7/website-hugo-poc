---
layout: "image"
title: "Rotor"
date: "2016-07-26T10:47:34"
picture: "ventilatorgross2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44016
- /details82bb-2.html
imported:
- "2019"
_4images_image_id: "44016"
_4images_cat_id: "3258"
_4images_user_id: "558"
_4images_image_date: "2016-07-26T10:47:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44016 -->
Die seltsame Ringkonstruktion löst das übliche Problem der wegfliegenden Rotorblätter, durch Fliehkraft bedingtes lösen von der Befestigung, ohne Klebstoff!