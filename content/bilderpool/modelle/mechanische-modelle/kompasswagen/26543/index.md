---
layout: "image"
title: "Kompassanhänger - Unterbau"
date: "2010-02-25T17:41:16"
picture: "Unterbau720.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26543
- /details30ed.html
imported:
- "2019"
_4images_image_id: "26543"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-25T17:41:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26543 -->
