---
layout: "image"
title: "Hohlrad-Kompasswagen -Versenktes Rastkegelzahnrad"
date: "2010-02-28T13:00:48"
picture: "VersenktesRastkegelzahnrad.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["modding"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26563
- /detailsf4b5.html
imported:
- "2019"
_4images_image_id: "26563"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-28T13:00:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26563 -->
Mit einem 8mm Bohrer und einem kleinen Seitenschneider kann man aus dem Baustein 15 mit Bohrung ganz einfach einen Baustein 15 machen, in dem das Rastkegelzahnrad versenkt ist und soweit herausschaut wie ein Kegelzahnrad aus einem unmodifizierten BS 15. Zuerst fand ich das etwas brutal, aber dann fiel mir ein, dass von authorisierter Seite (ich glaube in der em1-Anleitung) angewiesen wurde, für ein Modell aus einem Zahnrad 20 jeden zweiten Zahn zu entfernen. Jedenfalls löst das mein Problem beim Hohlradkompasswagen, und ich denke, dass ich den Baustein noch öfter benötigen kann.