---
layout: "image"
title: "Kompassanhänger Hauptansicht"
date: "2010-02-25T17:41:15"
picture: "Kompasswagen720.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Kompasswagen", "South-pointing-chariot", "Differentialgetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26539
- /detailsd06e.html
imported:
- "2019"
_4images_image_id: "26539"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-25T17:41:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26539 -->
Es sollte ein Modell werden, dass a) gut funktioniert und b) es erlaubt, die Funktionsweise auf einen Blick zu erfassen. Wenn man die Naben des Z30 und des Z40 nicht fest anzieht, läuft es noch etwas leichter. Die Einstellung des Spiels bei der Lagerung der Reifen 45 war weit unproblematischer, als ich gedacht hätte.