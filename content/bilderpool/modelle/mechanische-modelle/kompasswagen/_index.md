---
layout: "overview"
title: "Kompasswagen"
date: 2020-02-22T08:20:02+01:00
legacy_id:
- /php/categories/1891
- /categories034c.html
- /categoriesd6da.html
- /categoriesa733.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1891 --> 
Englisch: South pointing chariot. Zeigt trotz Bewegungen immer in die gleiche Richtung. Zur Funktionsweise siehe z.B.

http://www.odts.de/southptr/

http://www.aip.de/%7Elie/Publikationen/342.Kompasswagen.pdf