---
layout: "image"
title: "Geöffnete Aufzugtüre (mit Verkleidung)"
date: "2010-09-13T14:37:24"
picture: "aufzug12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28096
- /detailsf400-3.html
imported:
- "2019"
_4images_image_id: "28096"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28096 -->
Der Schiebetürenantrieb und die Lichtschranke verschwinden vollständig hinter einer Verkleidung (Bauplatten auf Winkelstein). 
An der oberen Verkleidung sind die Lampen für die Stockwerkanzeige an Winkelsteinen 60 befestigt (im Bild ohne Verkabelung).