---
layout: "image"
title: "Aufzugkabine (vorne: Magnet für Reed-Kontakt)"
date: "2010-09-13T14:37:22"
picture: "aufzug02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28086
- /details88eb.html
imported:
- "2019"
_4images_image_id: "28086"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28086 -->
Aufzugkabine mit Verkleidung. Vorne der Dauermagnet für die Reed-Kontakte (zur Stockwerk-Erkennung). 
An den beiden Haken wird die Kabine zur Sicherung über zwei Seilzüge mit dem Gegengewicht verbunden. Bewegt wird die Kabine über eine Kette (Mitte).
(Um Gewicht zu sparen, hatte ich ursprünglich an den vier Kanten graue Winkelträger verbaut; mit roten Bauplatten verkleidet sah die Kabine von innen jedoch nicht sehr ansprechend aus.)
