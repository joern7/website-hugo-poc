---
layout: "image"
title: "Aufzugkabine (vorne: Rad für Polwendetaster)"
date: "2010-09-13T14:37:22"
picture: "aufzug03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28087
- /detailsf26e.html
imported:
- "2019"
_4images_image_id: "28087"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28087 -->
Auf der rechten Kabinenseite erkennt man das schwarze Rad, das am oberen und unteren Ende des Aufzugschachts einen Minitaster für den Richtungswechsel betätigt.
(Ursprünglich hatte ich einen Winkelstein 60 verwendet; damit verkantete die Kabine aber gelegentlich. Das Rad betätigt den Schalter wesentlich geschmeidiger.)