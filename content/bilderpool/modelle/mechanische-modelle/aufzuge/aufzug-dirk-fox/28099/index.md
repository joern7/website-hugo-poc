---
layout: "image"
title: "Programm Aufzugsteuerung: Nebenprozess Stockwerk-Halt"
date: "2010-09-13T14:37:24"
picture: "aufzug15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28099
- /details3ee3-2.html
imported:
- "2019"
_4images_image_id: "28099"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28099 -->
Die drei Nebenprozesse sind fast identisch - sie unterscheiden sich nur darin, welche Etagen-Sensoren abgefragt werden (die Abbildung zeigt den Prozess für Etage 1).
Wurde die Ruftaste der Etage (hier: Etage 1) betätigt, wird der Wert der Variable "Ruf" um eins erhöht (war er vorher gleich Null, wird nun der Aufzug im Hauptprozess aus der Wartestellung gestartet). Sobald die Aufzugkabine den Reed-Kontakt der Etage (hier: Etage 1) passiert, wird das Unterprogramm "Stockwerk Halt" aufgerufen und anschließend der Wert der Variable "Ruf" wieder um eins verringert.
(Das Programm kann im Downloadbereich unter "Robo Pro" heruntergeladen werden.)