---
layout: "image"
title: "PICT4870"
date: "2010-02-24T21:35:37"
picture: "fangvorrichtung2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26536
- /detailsf351.html
imported:
- "2019"
_4images_image_id: "26536"
_4images_cat_id: "1890"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26536 -->
Fangvorrichtung an der Schiene im Normalzustand.
Wird abgestützt vom gelben Statikträger