---
layout: "image"
title: "Aufzug Ansicht von links vorne"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug05.jpg"
weight: "5"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31876
- /details10c1.html
imported:
- "2019"
_4images_image_id: "31876"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31876 -->
