---
layout: "image"
title: "Fahrkorb (links vorne)"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug10.jpg"
weight: "10"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31881
- /details9c55-2.html
imported:
- "2019"
_4images_image_id: "31881"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31881 -->
