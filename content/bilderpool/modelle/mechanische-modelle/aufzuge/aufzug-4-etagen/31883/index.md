---
layout: "image"
title: "Fahrkorb (vorne) mit Verkleidungsmaterial"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug12.jpg"
weight: "12"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31883
- /details6dcf-3.html
imported:
- "2019"
_4images_image_id: "31883"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31883 -->
