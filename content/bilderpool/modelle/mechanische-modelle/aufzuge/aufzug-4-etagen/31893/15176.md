---
layout: "comment"
hidden: true
title: "15176"
date: "2011-09-23T16:56:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Oh den sehe ich jetzt erst. Ich sage: Als Summer. Obendrüber ist eine Schwingfeder, und ich rate, die ist so verschaltet, dass der E-Magnet, wenn er sie anzieht, sich selbst den Strom wegnimmt.

Gruß,
Stefan