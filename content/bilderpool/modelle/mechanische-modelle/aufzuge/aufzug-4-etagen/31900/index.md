---
layout: "image"
title: "Stockwerks und Fahrtrichtungsanzeige"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug29.jpg"
weight: "29"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31900
- /details4359-2.html
imported:
- "2019"
_4images_image_id: "31900"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31900 -->
Oberer Taster: Endtaster Tür geschlossen
Lampe nach rechts strahlend: Lampe für Lichtschranke
Taster und Lampe vorne: Ruftaste mit anzeige