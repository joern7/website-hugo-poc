---
layout: "image"
title: "Stockwerksteilansicht von hinten"
date: "2011-09-23T11:45:36"
picture: "etagenaufzug33.jpg"
weight: "33"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31904
- /detailse1f3-2.html
imported:
- "2019"
_4images_image_id: "31904"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:36"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31904 -->
