---
layout: "image"
title: "Tastenfeld im Fahrkorb (ohne Abdeckung)"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug06.jpg"
weight: "6"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31877
- /detailsce5f.html
imported:
- "2019"
_4images_image_id: "31877"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31877 -->
