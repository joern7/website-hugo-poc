---
layout: "image"
title: "Fahrkorb (links) verkleidet"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug16.jpg"
weight: "16"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31887
- /detailsa6fe-2.html
imported:
- "2019"
_4images_image_id: "31887"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31887 -->
