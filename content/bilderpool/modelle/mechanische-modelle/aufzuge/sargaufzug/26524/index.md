---
layout: "image"
title: "18082009369"
date: "2010-02-24T21:35:37"
picture: "sargaufzug04.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26524
- /detailsdd87.html
imported:
- "2019"
_4images_image_id: "26524"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26524 -->
Detail der Umlenkrolle unter der Plattform