---
layout: "image"
title: "PICT4857"
date: "2010-02-24T21:35:37"
picture: "sargaufzug05.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26525
- /detailsf197.html
imported:
- "2019"
_4images_image_id: "26525"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26525 -->
Gesamtansicht von vorne links