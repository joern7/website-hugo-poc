---
layout: "image"
title: "PICT4866"
date: "2010-02-24T21:35:37"
picture: "sargaufzug14.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26534
- /detailsca50.html
imported:
- "2019"
_4images_image_id: "26534"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26534 -->
Ruhe in Frieden