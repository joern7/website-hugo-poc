---
layout: "comment"
hidden: true
title: "15519"
date: "2011-10-23T10:09:45"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Aber dann ist es doch eher zufällig, ob die Luftschraube die Tür verriegelt oder nicht. Oder wird die richtige Position noch irgendwie sichergestellt?
Gruß,
Stefan