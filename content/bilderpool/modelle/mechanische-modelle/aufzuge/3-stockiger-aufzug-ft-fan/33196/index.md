---
layout: "image"
title: "Türe im 2. Stockwerk offen"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan12.jpg"
weight: "11"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33196
- /detailseeb0.html
imported:
- "2019"
_4images_image_id: "33196"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33196 -->
Da die Kabine in diesem Stockwerk ist, kann man die Türe öffnen (Blaue Luftschraube)