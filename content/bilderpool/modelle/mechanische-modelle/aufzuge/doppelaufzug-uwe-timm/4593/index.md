---
layout: "image"
title: "Doppelaufzug 002"
date: "2005-08-15T22:11:36"
picture: "Doppelaufzug 002.jpg"
weight: "1"
konstrukteure: 
- "uwe timm"
fotografen:
- "uwe timm"
keywords: ["Aufzug", "Steuerung"]
uploadBy: "Chemikus"
license: "unknown"
legacy_id:
- /php/details/4593
- /detailsf81c-2.html
imported:
- "2019"
_4images_image_id: "4593"
_4images_cat_id: "371"
_4images_user_id: "156"
_4images_image_date: "2005-08-15T22:11:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4593 -->
Beschreibung und Kommentare gibt es <a href="http://www.fischertechnik.de/fanclub/forum/topic.asp?TOPIC_ID=3509&FORUM_ID=12&CAT_ID=2&Topic_Title=Doppelaufzug&Forum_Title=Modellideen">im Forum</a>.