---
layout: "image"
title: "Aufzug"
date: "2004-04-24T14:51:25"
picture: "Aufzug_001F.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2377
- /details87da.html
imported:
- "2019"
_4images_image_id: "2377"
_4images_cat_id: "221"
_4images_user_id: "104"
_4images_image_date: "2004-04-24T14:51:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2377 -->
Erdgeschoss und 3 Stockwerke darüber. Die Tür geht (schnell) auf und (langsam) zu. Sie öffnet sich auch, wenn jemand in die Lichschranke zwischen die Türen kommt. Gesteuert wurde das ganze von einem 286/16 unter MS-DOS 6.22 und Logitech/Multiscope Modula-2 4.0 via Parallel-Interface.