---
layout: "image"
title: "Kabine"
date: "2015-01-02T15:55:46"
picture: "aufzug09.jpg"
weight: "9"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40104
- /details1544.html
imported:
- "2019"
_4images_image_id: "40104"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40104 -->
Hier sind die 4 Ruftasten innerhalb des Fahrstuhl zu sehen.