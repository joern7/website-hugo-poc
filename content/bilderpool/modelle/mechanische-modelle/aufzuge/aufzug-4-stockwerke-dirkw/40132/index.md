---
layout: "image"
title: "Kabine oben"
date: "2015-01-02T15:55:46"
picture: "aufzug37.jpg"
weight: "37"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40132
- /details310b-2.html
imported:
- "2019"
_4images_image_id: "40132"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40132 -->
Links: Stromzufuhr über 2 Federkontakte seitlich.