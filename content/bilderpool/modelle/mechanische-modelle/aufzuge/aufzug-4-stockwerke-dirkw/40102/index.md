---
layout: "image"
title: "Erdgeschoß"
date: "2015-01-02T15:55:46"
picture: "aufzug07.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40102
- /detailsae4d.html
imported:
- "2019"
_4images_image_id: "40102"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40102 -->
Links der Ruftaster (beleuchtet). In der Mitte seht ihr die Lichtschranke außen und in der Kabine.
Damit eine zu- oder aussteigende Person nicht  in der Tür einklemmt wird.