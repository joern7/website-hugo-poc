---
layout: "image"
title: "Gegengewicht"
date: "2015-01-02T15:55:46"
picture: "aufzug41.jpg"
weight: "41"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40136
- /details8e8c-2.html
imported:
- "2019"
_4images_image_id: "40136"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40136 -->
Das Gegengwicht mit 4 Seilrollen als Führung.