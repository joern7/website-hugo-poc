---
layout: "image"
title: "Erdgeschoß"
date: "2015-01-02T15:55:46"
picture: "aufzug06.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40101
- /detailsbec6.html
imported:
- "2019"
_4images_image_id: "40101"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40101 -->
Grundstellung des Aufzugs.