---
layout: "image"
title: "Kabine oben"
date: "2015-01-02T15:55:46"
picture: "aufzug36.jpg"
weight: "36"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40131
- /details3270-2.html
imported:
- "2019"
_4images_image_id: "40131"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40131 -->
Sound Modul:: 
- öffen und schließen der Kabinentür  (Zischen)
- Stockwerk erreicht (Gong)
- Auf und Ab (Fahrgeräusch)

Mittig: 2 Seilrollen für die Seilaufhängung der Kabine.