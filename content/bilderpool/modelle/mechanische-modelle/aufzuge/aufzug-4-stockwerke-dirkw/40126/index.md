---
layout: "image"
title: "Kabine vorn"
date: "2015-01-02T15:55:46"
picture: "aufzug31.jpg"
weight: "31"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40126
- /details55de.html
imported:
- "2019"
_4images_image_id: "40126"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40126 -->
Eine Teleskoptür entfernt.