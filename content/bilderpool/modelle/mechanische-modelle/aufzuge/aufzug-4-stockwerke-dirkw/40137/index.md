---
layout: "image"
title: "Gegengewicht offen"
date: "2015-01-02T15:55:46"
picture: "aufzug42.jpg"
weight: "42"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40137
- /details44de.html
imported:
- "2019"
_4images_image_id: "40137"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40137 -->
3 Stahlplatten im Gegengewicht.