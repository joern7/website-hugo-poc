---
layout: "image"
title: "SAA1064"
date: "2015-01-02T15:55:46"
picture: "aufzug13.jpg"
weight: "13"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40108
- /detailsdbc1-2.html
imported:
- "2019"
_4images_image_id: "40108"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40108 -->
SAA1064 Treiber für die 7- Segmentanzeige. Dieser dient zur Anzeige der Ziffern außen am Stockwerk und im Fahrstuhl.