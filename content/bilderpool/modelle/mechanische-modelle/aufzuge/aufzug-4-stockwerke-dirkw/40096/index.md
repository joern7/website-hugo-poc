---
layout: "image"
title: "Aufzug vorne"
date: "2015-01-02T15:55:46"
picture: "aufzug01.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40096
- /detailsf16f-2.html
imported:
- "2019"
_4images_image_id: "40096"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40096 -->
Hier seht ihr den Auzug mit den 4 Stockwerken von vorne.