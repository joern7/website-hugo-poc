---
layout: "image"
title: "Aufzug hinten"
date: "2015-01-02T15:55:46"
picture: "aufzug04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40099
- /detailsd58f.html
imported:
- "2019"
_4images_image_id: "40099"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40099 -->
Mittig sieht man das Gegengewicht der Kabine. Unten links die 2 Robo TX Controller und darüber der SAA1064 Treiber für die 7- Segmentanzeige.