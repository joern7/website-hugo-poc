---
layout: "image"
title: "Kabine von oben"
date: "2015-01-02T15:55:46"
picture: "aufzug23.jpg"
weight: "23"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40118
- /details7835-2.html
imported:
- "2019"
_4images_image_id: "40118"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40118 -->
Kabine mit geöffneter Tür.