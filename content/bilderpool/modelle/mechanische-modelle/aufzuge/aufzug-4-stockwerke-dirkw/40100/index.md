---
layout: "image"
title: "Stockwerk seitlich"
date: "2015-01-02T15:55:46"
picture: "aufzug05.jpg"
weight: "5"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40100
- /details99a2.html
imported:
- "2019"
_4images_image_id: "40100"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40100 -->
Hier seht ihr das Flachbandkabel für die Ansteuerung der Stockwerke.