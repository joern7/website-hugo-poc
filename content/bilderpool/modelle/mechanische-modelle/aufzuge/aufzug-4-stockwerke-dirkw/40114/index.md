---
layout: "image"
title: "Kabine und Gegengewicht"
date: "2015-01-02T15:55:46"
picture: "aufzug19.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40114
- /detailse5b1-2.html
imported:
- "2019"
_4images_image_id: "40114"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40114 -->
