---
layout: "image"
title: "Stromversorgung"
date: "2015-01-02T15:55:46"
picture: "aufzug29.jpg"
weight: "29"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40124
- /detailse294-2.html
imported:
- "2019"
_4images_image_id: "40124"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40124 -->
2x Federkontakte als Stromversorgung für den TX Controller der Kabine.