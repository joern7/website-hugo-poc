---
layout: "image"
title: "fahrstuhl jan knobbe 2"
date: "2008-01-06T20:09:44"
picture: "fahrstuhlvonjanknobbe2.jpg"
weight: "2"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13281
- /detailsfc4f-2.html
imported:
- "2019"
_4images_image_id: "13281"
_4images_cat_id: "1200"
_4images_user_id: "713"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13281 -->
Die Kabine hält im "Hochparterre". Im Downloadbereich befindet sich der Schaltplan (Kategorie Software).