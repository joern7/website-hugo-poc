---
layout: "comment"
hidden: true
title: "9437"
date: "2009-06-16T22:30:32"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

vielen Dank schon mal an dieser Stelle für das positive Feedback!

Ich habe jetzt tatsächlich hinbekommen, einen Antrieb zu realisieren mit einem 1:50 Power-Motor *stolllz*

Hier kann man sich ein Bild davon machen:
http://www.youtube.com/watch?v=7vNyFbjQ9pA&feature=channel_page

Viele Grüße, Andreas.