---
layout: "image"
title: "IMG_0006_Detail Kabine.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0006_Detail_Kabine.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/34785
- /details805f.html
imported:
- "2019"
_4images_image_id: "34785"
_4images_cat_id: "2571"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34785 -->
