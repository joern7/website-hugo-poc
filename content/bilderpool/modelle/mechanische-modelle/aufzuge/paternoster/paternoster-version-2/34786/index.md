---
layout: "image"
title: "IMG_0005_Detail Antrieb.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0005_Detail_Antrieb.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/34786
- /details7b94.html
imported:
- "2019"
_4images_image_id: "34786"
_4images_cat_id: "2571"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34786 -->
Das Rast-Ritzel treibt die Drehscheibe an