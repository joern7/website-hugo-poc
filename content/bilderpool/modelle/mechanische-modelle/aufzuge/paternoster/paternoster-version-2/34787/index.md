---
layout: "image"
title: "IMG_0004_Detail Kraftübertragung.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0004_Detail_Kraftbertragung.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/34787
- /detailsf69a.html
imported:
- "2019"
_4images_image_id: "34787"
_4images_cat_id: "2571"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34787 -->
Ein Power-Motor 1:50 schickt seine Kraft schön gemächlich über Steck-Achsen, Kardan-Gelenke und Rast-Kegel-Zahnräder auf beide Seiten des Aufzugs.