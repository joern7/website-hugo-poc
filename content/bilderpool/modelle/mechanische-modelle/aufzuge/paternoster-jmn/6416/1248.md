---
layout: "comment"
hidden: true
title: "1248"
date: "2006-08-24T11:48:16"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Ich meinte du speicherst die Position mechanisch mit einem 2. Drehschalter, der über einen Motor angetrieben wird. Es würde aber reichen, die Position in einer Variable in RoboPro zu speichern. Das reicht mindestens bis zum Stromausfall :)

Zu den Widerständen: Du musst immer auf einen Bereich prüfen! Also Wert zwischen 450 und 490 => Position 1

Thomas