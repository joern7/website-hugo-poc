---
layout: "image"
title: "Plank und Antrieb"
date: "2006-06-04T12:56:41"
picture: "paternoster_5.jpg"
weight: "6"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6419
- /details4115.html
imported:
- "2019"
_4images_image_id: "6419"
_4images_cat_id: "556"
_4images_user_id: "162"
_4images_image_date: "2006-06-04T12:56:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6419 -->
