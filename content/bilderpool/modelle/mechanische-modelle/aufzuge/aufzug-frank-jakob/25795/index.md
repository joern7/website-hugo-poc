---
layout: "image"
title: "PICT4843"
date: "2009-11-17T21:32:24"
picture: "aufzugfrankjakob09.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25795
- /detailsf107-2.html
imported:
- "2019"
_4images_image_id: "25795"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25795 -->
Umlenkrollen, Gegengewichtsbahn