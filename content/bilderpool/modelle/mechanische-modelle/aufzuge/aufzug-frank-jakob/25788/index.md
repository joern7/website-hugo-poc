---
layout: "image"
title: "PICT4825"
date: "2009-11-17T21:32:24"
picture: "aufzugfrankjakob02.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25788
- /details36fd.html
imported:
- "2019"
_4images_image_id: "25788"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25788 -->
Aufzugskabine von hinten