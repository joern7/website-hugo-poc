---
layout: "image"
title: "Die USB-Hauptplatine"
date: "2008-09-15T16:13:56"
picture: "DSCF1684.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "me"
keywords: ["USB", "Platine", "Aufzug"]
uploadBy: "Strohi"
license: "unknown"
legacy_id:
- /php/details/15233
- /detailsfe86.html
imported:
- "2019"
_4images_image_id: "15233"
_4images_cat_id: "1394"
_4images_user_id: "763"
_4images_image_date: "2008-09-15T16:13:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15233 -->
Mit diesen beiden Platinen wird der Aufzug gesteuert. Die eine PLatine arbeitet ein C-PRogramm ab, die andere (die rechte) Steuert dann den Motor