---
layout: "image"
title: "Die beiden Steuerungsplatinen"
date: "2008-09-15T16:13:56"
picture: "DSCF1681.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "me"
keywords: ["Aufzug", "Platine"]
uploadBy: "Strohi"
license: "unknown"
legacy_id:
- /php/details/15232
- /detailsf35d-3.html
imported:
- "2019"
_4images_image_id: "15232"
_4images_cat_id: "1394"
_4images_user_id: "763"
_4images_image_date: "2008-09-15T16:13:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15232 -->
Mit diesen beiden Platinen wird der Aufzug gesteuert. Die eine PLatine arbeitet ein C-PRogramm ab, die andere (die rechte) Steuert dann den Motor