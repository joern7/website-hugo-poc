---
layout: "image"
title: "Kabine auf der 3. Ebene"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl06.jpg"
weight: "6"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13471
- /details5900.html
imported:
- "2019"
_4images_image_id: "13471"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13471 -->
