---
layout: "image"
title: "Kabine auf der 2. Ebene"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl05.jpg"
weight: "5"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13470
- /details7e4d.html
imported:
- "2019"
_4images_image_id: "13470"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13470 -->
