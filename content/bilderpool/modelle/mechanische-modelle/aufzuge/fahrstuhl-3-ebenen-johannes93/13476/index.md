---
layout: "image"
title: "Lichtschranke an der Tür"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl11.jpg"
weight: "11"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13476
- /detailsa49f-2.html
imported:
- "2019"
_4images_image_id: "13476"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13476 -->
