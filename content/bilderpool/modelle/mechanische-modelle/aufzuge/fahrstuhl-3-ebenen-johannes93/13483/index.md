---
layout: "image"
title: "Blick durch den Schacht"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl18.jpg"
weight: "18"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13483
- /details69a9.html
imported:
- "2019"
_4images_image_id: "13483"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13483 -->
