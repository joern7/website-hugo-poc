---
layout: "image"
title: "Sackaufzug in einer (Wasser-) Mühle (Gesamtansicht)"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug1.jpg"
weight: "1"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/44377
- /details3a46.html
imported:
- "2019"
_4images_image_id: "44377"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44377 -->
Im Sommerurlaub 2016 haben wir die 500 Jahre alte Wassermühle Willer in Harskirchen (Lothringen) besucht und wurden vom Müller durch die "Innereien" der Mühle geführt, die bis heute ausschließlich mit Wasserkraft betrieben wird (http://www.dailymotion.com/video/xhbcj9_harskirchen-le-moulin-de-willer_people). 
Konrad war besonders von dem Sackaufzug fasziniert - kurz am Seil gezogen wanderten die schweren Kornsäcke wie von Geisterhand bewegt nach oben... Das musste er nach unserer Rückkehr sofort nachbauen.