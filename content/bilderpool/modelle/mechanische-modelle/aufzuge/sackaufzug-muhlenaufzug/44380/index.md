---
layout: "image"
title: "Sackaufzug - angetrieben"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug4.jpg"
weight: "4"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/44380
- /details5d11-2.html
imported:
- "2019"
_4images_image_id: "44380"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44380 -->
Zieht man am Seil, wird der Transmissionsriemen gespannt; er greift und der Sackaufzug wird angetrieben.