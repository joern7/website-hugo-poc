---
layout: "image"
title: "Stufen im Detail"
date: "2016-07-26T18:01:04"
picture: "fahrtreppe5.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44023
- /details3c7a.html
imported:
- "2019"
_4images_image_id: "44023"
_4images_cat_id: "3259"
_4images_user_id: "4"
_4images_image_date: "2016-07-26T18:01:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44023 -->
Die rote und die gelbe Stufen sind hochkant geklappt, die weiße liegt richtig herum. Die Vorderkante jeder Stufe hat breite Spur und kleine Rollen; die Hinterkante hat schmale Spur und die Räder 23. Durch die unterschiedlichen Spurweiten kann man Vorder- und Hinterkante auf verschiedene Bewegungspfade zwingen. Dazu dienen Führungsschienen (die BS30 und das Unterteil des Handlaufs) und die Umlenkrollen (Reifen 45 und Malteserrad).