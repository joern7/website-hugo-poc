---
layout: "image"
title: "Stirnseite"
date: "2016-07-26T18:01:04"
picture: "fahrtreppe3.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44021
- /details4787.html
imported:
- "2019"
_4images_image_id: "44021"
_4images_cat_id: "3259"
_4images_user_id: "4"
_4images_image_date: "2016-07-26T18:01:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44021 -->
Da wo die Auflage endet (die schwarzen querliegenden Steine, auf denen die zwei weißen Rollen gerade laufen), klappen die Stufen mit dem "unteren" Ende nach unten. Deshalb kann diese Treppe nur nach oben fahren. Wenn man nur die Fahrtrichtung umkehrt, liegen die Stufen mit der Unterseite nach oben.