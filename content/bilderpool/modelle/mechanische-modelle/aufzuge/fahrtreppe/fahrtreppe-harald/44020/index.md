---
layout: "image"
title: "Antrieb"
date: "2016-07-26T18:01:04"
picture: "fahrtreppe2.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44020
- /details748e.html
imported:
- "2019"
_4images_image_id: "44020"
_4images_cat_id: "3259"
_4images_user_id: "4"
_4images_image_date: "2016-07-26T18:01:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44020 -->
Das Malteserrad wollte ich unbedingt mal verbaut haben. Es ginge auch mit der ft-Drehscheibe, mit kurzen Achsen in den nach außen zeigenden Nuten.