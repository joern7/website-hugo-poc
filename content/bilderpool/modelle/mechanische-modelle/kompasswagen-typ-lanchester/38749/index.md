---
layout: "image"
title: "Kompasswagen vom Typ Lanchester"
date: "2014-05-05T23:06:56"
picture: "Lancaster3c.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Kompasswagen", "Differentialgetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38749
- /detailsa60d-2.html
imported:
- "2019"
_4images_image_id: "38749"
_4images_cat_id: "2896"
_4images_user_id: "1088"
_4images_image_date: "2014-05-05T23:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38749 -->
Die Verwendung der Antriebskegelzahnräder des Rastdifferentials ermöglicht es, den Radabstand genau so einzustellen, dass die angezeigte Richtung auch bei mehreren Umdrehungen gehalten wird.