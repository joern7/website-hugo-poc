---
layout: "image"
title: "BILD 1 & 2, einphasiger Schrittmotor"
date: "2016-09-14T22:14:35"
picture: "BILD_1__2_einphasiger_Schrittmotor.jpg"
weight: "6"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Einphasiger", "Schrittmotor"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44375
- /details251b.html
imported:
- "2019"
_4images_image_id: "44375"
_4images_cat_id: "127"
_4images_user_id: "2635"
_4images_image_date: "2016-09-14T22:14:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44375 -->
Die 12 Magnete auf dem Rotor sind gleichsinnig ausgerichtet. Die beiden Magnete in den halben Gelenksteinen (31008 oder 31009) halten stramm ohne Zusatzmaßnahmen, sie fixieren jeweils einen Rotormagneten. Der Elektromagnet ist so angebracht, dass der nächste Rotormagnet knapp oberhalb zur Ruhe kommt, dadurch ergibt sich eine leichte Verdrehung des Rotors gegenüber den oberen Haltemagneten. Die Polung des Elektromagneten ist gleichsinnig mit den Rotormagneten, er stößt sie also immer ab. Mit dem Taster (31008) kann man nun von Hand einen kurzen Impuls auf den Elektromagneten geben, der nächste Rotormagnet wird nach oben gedrückt und der Rotor um 1/12 gedreht. Mit einem Impulsgenerator lässt sich Dauerbetrieb durchführen bis etwa 1 Umdrehung in 12 Sekunden, also 1 Impuls pro Sekunde. Ich bekomme das nicht genau hin weil ich einen elektromechanischen (!) Impulsgenerator verwende, bestehend aus zwei Relaisbausteinen (37683).
Mit einer 1/12 Untersetzung (1/3 und 1/4) wird daraus ein Sekundenzeiger-Antrieb.