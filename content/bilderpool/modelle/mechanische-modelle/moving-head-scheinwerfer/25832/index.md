---
layout: "image"
title: "kabeldurchführung"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer15.jpg"
weight: "15"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25832
- /detailsc36c.html
imported:
- "2019"
_4images_image_id: "25832"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25832 -->
ohne schleifkontakt