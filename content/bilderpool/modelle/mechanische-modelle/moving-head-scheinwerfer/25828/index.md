---
layout: "image"
title: "robo TX controller"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer11.jpg"
weight: "11"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25828
- /detailsde01-2.html
imported:
- "2019"
_4images_image_id: "25828"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25828 -->
controller