---
layout: "image"
title: "Display"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer17.jpg"
weight: "17"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25834
- /details9044-2.html
imported:
- "2019"
_4images_image_id: "25834"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25834 -->
Display zeigt Temperatur und zustand der lammelen
auserdem steuert man mit den tasten links und rechts die lammelen