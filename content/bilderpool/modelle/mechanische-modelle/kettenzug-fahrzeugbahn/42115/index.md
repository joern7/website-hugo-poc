---
layout: "image"
title: "Antrieb der zwei übereinanderliegenden Kurven (1)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42115
- /detailsf7a6.html
imported:
- "2019"
_4images_image_id: "42115"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42115 -->
An einer Seite liegen zwei Kurven übereinander. Da die Achse nicht einfach senkrecht durch die Fahrbahn darunter geführt werden kann (da sollen ja die Fahrzeuge ein Stockwerk tiefer durch), wird der Antrieb um ein Z40 versetzt geleitet...