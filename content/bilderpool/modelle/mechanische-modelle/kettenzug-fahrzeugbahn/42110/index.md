---
layout: "image"
title: "Antrieb (2)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42110
- /details29a8.html
imported:
- "2019"
_4images_image_id: "42110"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42110 -->
Der gefedert stehende Motorblock treibt über die zwei Z10 je ein Z15 auf den langen Achsen an.