---
layout: "image"
title: "Kardanwellen"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42111
- /details8505.html
imported:
- "2019"
_4images_image_id: "42111"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42111 -->
Zwei geeignet angebrachte große Kardangelenke leiten das Drehmoment nach oben zu den Schrägförder-Ketten. Sie ergeben (korrekt zueinander ausgerichtet) ein homokinetisches Gelenk. Die Drehbewegung ist also auf beiden Seiten der Kardankombination gleichförmig; nichts ruckelt.

Das Bild ist bei laufendem Modell aufgenommen. Man erahnt die schnelle Drehung der Antriebsachsen.