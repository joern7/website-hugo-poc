---
layout: "image"
title: "Fahrzeug 4 - Lastwagen"
date: "2015-10-24T21:46:36"
picture: "kettenzugfahrzeugbahn15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42121
- /details67e4.html
imported:
- "2019"
_4images_image_id: "42121"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:36"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42121 -->
Leider ein etwas unscharfes Bild - Sorry.