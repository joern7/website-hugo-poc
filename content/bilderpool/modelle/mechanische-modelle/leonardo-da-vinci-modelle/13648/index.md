---
layout: "image"
title: "Da Vinci Chariot"
date: "2008-02-15T19:04:10"
picture: "chariot_with_flails.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["da", "Vinci", "Chariot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13648
- /detailse666.html
imported:
- "2019"
_4images_image_id: "13648"
_4images_cat_id: "1252"
_4images_user_id: "585"
_4images_image_date: "2008-02-15T19:04:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13648 -->
Dieses Modell basiert auf-da-Vinci-Design für einen Krieg Wagen mit Dreschflegel.