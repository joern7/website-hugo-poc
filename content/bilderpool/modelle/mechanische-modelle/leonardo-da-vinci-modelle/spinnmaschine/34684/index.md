---
layout: "image"
title: "Detailansicht Kronrad und Triebstangen"
date: "2012-03-25T14:40:07"
picture: "Kronrad_und_Triebstockgetriebe.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34684
- /detailsfcb1.html
imported:
- "2019"
_4images_image_id: "34684"
_4images_cat_id: "2560"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T14:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34684 -->
Im Kronrad sind lediglich fünf von 12 möglichen Zapfen befestigt, die in die sechs Triebstangen greifen und die orthogonale Achse drehen.