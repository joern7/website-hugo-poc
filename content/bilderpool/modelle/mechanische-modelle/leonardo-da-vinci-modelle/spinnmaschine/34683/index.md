---
layout: "image"
title: "Schiebe-Getriebe mit Kronrad und Triebstangen"
date: "2012-03-25T14:40:07"
picture: "Gesamtansicht_Schiebe-Getriebe.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34683
- /details245c.html
imported:
- "2019"
_4images_image_id: "34683"
_4images_cat_id: "2560"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T14:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34683 -->
Besonders faszinierend ist das Getriebe, das die Spindel hin- und herschiebt, um eine gleichmäßige Verteilung des gesponnenen Garns auf der Spule zu erreichen. Das wird erreicht durch ein Kronrad mit Zapfen, die in Triebstangen greifen - da die Zapfen anders als Zähne eines Zahnrads nicht exakt greifen, wurden Triebstangen in einer Art "Laterne" verwendet, an denen die Zapfen entlang glitten.