---
layout: "comment"
hidden: true
title: "16657"
date: "2012-03-25T13:04:25"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Moin Dirk,
ich muss jetzt mal fragen: Woher kommen diese Bilder? Sind sie in irgendeiner Form freigegeben/CC/was auch immer, damit wir sie hier einfach so einbinden können?
Ansonsten müssten wir sie leider löschen.
Grüße,
Martin