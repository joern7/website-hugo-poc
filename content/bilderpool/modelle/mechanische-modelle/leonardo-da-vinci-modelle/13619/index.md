---
layout: "image"
title: "Sichelwagen (b)"
date: "2008-02-09T13:45:48"
picture: "Sensenwagen_Da_Vinci_Feb_08_2.jpg"
weight: "2"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13619
- /detailsb1f3.html
imported:
- "2019"
_4images_image_id: "13619"
_4images_cat_id: "1252"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T13:45:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13619 -->
