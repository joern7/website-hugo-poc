---
layout: "image"
title: "Pneumatik-Schiebetür mit Druckluftspeicher"
date: "2014-01-06T08:31:02"
picture: "img_663-4969eW.jpg"
weight: "6"
konstrukteure: 
- "IXI"
fotografen:
- "IXI"
keywords: ["Pneumatik", "Schiebetür", "Druckluftspeicher"]
uploadBy: "IXI"
license: "unknown"
legacy_id:
- /php/details/38007
- /detailsec7d-2.html
imported:
- "2019"
_4images_image_id: "38007"
_4images_cat_id: "1947"
_4images_user_id: "2076"
_4images_image_date: "2014-01-06T08:31:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38007 -->
Leicht modifizierte Schiebetür aus Pneumatik 3. Die Schläuche des rechten Zylinders sind unter der Bodenplatte verlegt, da ausreichend Bodenfreiheit vorhanden und der Durchgang dann frei ist. Als Druckluftspeicher dient eine 50ml-Spritze, deren 60 ml Volumen bei Vollauszug für drei vollständige Türbetätigungen reicht. Die Spritze passt leicht drehbar in die Kesselhalter aus dem Pneumatikpaket, die dort eigentlich als Greifer für die Spielmodelle dienen. Sie ist mehr als ausreichend druckfest, lediglich der Kolben muss fixiert werden, da er sonst bei vollem Druck herausschießt (lustig, aber nicht Sinn der Sache ;-).

Die Fixierung des Kolbens erfolgt hier mit einem einfachen Drahtbinder. Alternativ kann die Spritze samt Kolben am hinteren Ende durchbohrt, der Kolben mit einer Schraube gesichert und danach gekürzt werden. Eine weitere Möglichkeit besteht darin, den Kolben nicht starr, sondern mit einer geeignete Druckfeder zu fixieren und einen Taster einzubauen, der den Kompressor bei Maximaldruck abschaltet.

Der Schlauchanschluss ist hier über einen Adapter für Fahrradventile realisiert. Besser wäre eine stumpfe Kanüle, da sie exakt in das Gewinde der Spritze passt und daher nicht mit Kleber gesichert werden muss, wie der Adapter im Bild.