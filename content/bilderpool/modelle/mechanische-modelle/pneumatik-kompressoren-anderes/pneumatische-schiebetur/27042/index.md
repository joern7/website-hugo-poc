---
layout: "image"
title: "Schiebetür geschlossen"
date: "2010-05-02T22:08:20"
picture: "schiebetuer2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27042
- /details436d.html
imported:
- "2019"
_4images_image_id: "27042"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27042 -->
