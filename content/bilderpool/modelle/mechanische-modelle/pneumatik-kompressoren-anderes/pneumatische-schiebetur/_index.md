---
layout: "overview"
title: "Pneumatische Schiebetür"
date: 2020-02-22T08:14:54+01:00
legacy_id:
- /php/categories/1947
- /categories90c2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1947 --> 
Die Schiebetür pneumatische Schiebetür wird über ein Robo Interface gesteuert. Die Tür öffnet sich wenn eine Lichtschranke unterbrochen wird. Nach einer bestimmten Zeit schließt sich die Tür wieder. Steht dabei ein Hindernis im Weg springt die Tür sofort wieder auf.