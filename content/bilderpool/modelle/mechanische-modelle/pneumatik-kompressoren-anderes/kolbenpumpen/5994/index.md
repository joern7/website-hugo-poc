---
layout: "image"
title: "Kompressor_neu_3.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00610_resize.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5994
- /details7ef7-2.html
imported:
- "2019"
_4images_image_id: "5994"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5994 -->
Leichtgängigkeit und gute Kolbenschmierung erhöhen wesentlich die Laufruhe!