---
layout: "image"
title: "Kompressor_alt_1.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00617_resize.jpg"
weight: "4"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5995
- /details5e3a.html
imported:
- "2019"
_4images_image_id: "5995"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5995 -->
Hier der alte Kompressor aus dem Pneumatik+ bzw. dem Kompressor-Bausatz. Das Kompressor-Chassis wurde zweifach am großen M-Motor verbaut und bedient natürlich auch 2 Kolben wechselseitig. Die Pumpe liefert so zwar über 0,5 bar (je nach Akkuladung), neigt dann aber zum blockieren und ist daher eher für den kurzfristigen Betrieb gedacht.