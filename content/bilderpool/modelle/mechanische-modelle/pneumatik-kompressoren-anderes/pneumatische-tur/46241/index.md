---
layout: "image"
title: "Zylinder mit Kurbel"
date: "2017-09-13T22:13:16"
picture: "Zylinder_mit_Kurbel_community.jpg"
weight: "3"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46241
- /details1246.html
imported:
- "2019"
_4images_image_id: "46241"
_4images_cat_id: "3430"
_4images_user_id: "2770"
_4images_image_date: "2017-09-13T22:13:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46241 -->
hier sieht man gut den Zylinder mit der kurbel