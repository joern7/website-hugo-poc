---
layout: "image"
title: "Zylinder"
date: "2017-09-13T22:13:16"
picture: "Zylinder_comunity.jpg"
weight: "1"
konstrukteure: 
- "Awin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46239
- /detailsee74.html
imported:
- "2019"
_4images_image_id: "46239"
_4images_cat_id: "3430"
_4images_user_id: "2770"
_4images_image_date: "2017-09-13T22:13:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46239 -->
Der Zylinder dreht an der Kurbel und an der Kurbel ist die Tür befestigt