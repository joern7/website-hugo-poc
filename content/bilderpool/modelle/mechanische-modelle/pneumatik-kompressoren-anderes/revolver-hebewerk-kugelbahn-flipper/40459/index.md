---
layout: "image"
title: "Seitenansicht Zylinder und MAgnetventil"
date: "2015-02-08T12:54:27"
picture: "IMG_0007.jpg"
weight: "2"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40459
- /details9c6d.html
imported:
- "2019"
_4images_image_id: "40459"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40459 -->
Zylinder sind die alten Hydraulik-Zylinder. Die Magnetventile habe ich aus einem ausrangiertem  Blutdruckmonitor ausgeschlachtet. Schönes Detail an den Ventilen: 6-12V Betriebsspannung, ABSOLUT dicht(!) und eine echtes 3/2 Wege-Ventil!! (d.h Öffner und Schliesser bzw. "UM"- Scahltung in EINEM Ventil :-)))  - ich habe davon jetzt "Einige" im Bestand  - in Kürze "gönne" ich mir wohl eine echte Ventil-Insel..