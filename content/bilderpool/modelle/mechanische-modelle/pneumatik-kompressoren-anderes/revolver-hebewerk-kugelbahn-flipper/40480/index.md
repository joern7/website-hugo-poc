---
layout: "image"
title: "Schritt 3"
date: "2015-02-08T12:54:27"
picture: "IMG_0073.jpg"
weight: "18"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40480
- /details4316.html
imported:
- "2019"
_4images_image_id: "40480"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40480 -->
Die Kugel öffnet den Blockier-Hebel