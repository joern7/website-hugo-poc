---
layout: "image"
title: "Zylinder"
date: "2015-02-08T12:54:27"
picture: "IMG_0052.jpg"
weight: "11"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40468
- /details22d1.html
imported:
- "2019"
_4images_image_id: "40468"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40468 -->
Zylinder, Gummizug, Verriegelung, Ventil