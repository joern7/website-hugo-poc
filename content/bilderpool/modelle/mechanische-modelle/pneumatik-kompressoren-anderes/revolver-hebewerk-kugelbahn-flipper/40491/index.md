---
layout: "image"
title: "Kugel ist durch und wird gegen nach unten Fallen blockiert"
date: "2015-02-08T12:54:27"
picture: "IMG_0079.jpg"
weight: "24"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40491
- /details7708-2.html
imported:
- "2019"
_4images_image_id: "40491"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40491 -->
