---
layout: "overview"
title: "Powerkompressor (gummel87)"
date: 2020-02-22T08:14:50+01:00
legacy_id:
- /php/categories/1835
- /categories2e80.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1835 --> 
Kompakter, starker Kompressor mit Powermotor für anspruchsvolle Modelle.