---
layout: "image"
title: "3-Zylinder Druckluftmotor"
date: "2012-01-21T14:02:48"
picture: "zylinderdruckluftmotor5.jpg"
weight: "5"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33982
- /details914c-2.html
imported:
- "2019"
_4images_image_id: "33982"
_4images_cat_id: "2515"
_4images_user_id: "1361"
_4images_image_date: "2012-01-21T14:02:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33982 -->
Zylinder mit Führungen