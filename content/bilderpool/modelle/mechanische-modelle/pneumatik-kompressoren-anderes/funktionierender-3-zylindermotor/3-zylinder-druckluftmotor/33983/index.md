---
layout: "image"
title: "3-Zylinder Druckluftmotor"
date: "2012-01-21T14:02:48"
picture: "zylinderdruckluftmotor6.jpg"
weight: "6"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33983
- /details9ef4.html
imported:
- "2019"
_4images_image_id: "33983"
_4images_cat_id: "2515"
_4images_user_id: "1361"
_4images_image_date: "2012-01-21T14:02:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33983 -->
Schrägansicht. Mit dem Schalter im Vordergrund wird Aquarienpumpe (nicht sichtbar) zur Erzeugung der Druckluft eingeschaltet.