---
layout: "image"
title: "3-Zylinder Druckluftmotor"
date: "2012-01-21T14:02:48"
picture: "zylinderdruckluftmotor1.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33978
- /detailsbd89.html
imported:
- "2019"
_4images_image_id: "33978"
_4images_cat_id: "2515"
_4images_user_id: "1361"
_4images_image_date: "2012-01-21T14:02:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33978 -->
Gesamtansicht. Am linken Ende der Kurbelwelle befindet sich der Verteiler, am rechten das Schwungrad. Im Vordergrund erkennt man die 3 Magnetventile mit den Lampen, die jeweils die Aktivität anzeigen. Ein Video, das den Motor in Betrieb zeigt kann in Youtube unter "fischertechnik 3ZylDruckluftmotor" angesehen werden.