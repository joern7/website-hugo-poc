---
layout: "image"
title: "Kurz vor dem Zurückstellen"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42036
- /details48d0.html
imported:
- "2019"
_4images_image_id: "42036"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42036 -->
Beim rechten Z30 sieht man hier den Zustand, kurz bevor der Mitnehmer den Eckstein "verliert", weil dieser durch die schrägstehende Drehachse umso höher gelangt, je weiter er sich außen (hier: rechts) befindet.