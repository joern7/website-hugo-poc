---
layout: "image"
title: "Gesamtansicht"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42031
- /details906b.html
imported:
- "2019"
_4images_image_id: "42031"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42031 -->
Ordentlich Druckluft strömt durch zwei Düsen und trägt einen Tischtennisball. Durch die Mechanik darunter wird eine Düse so bewegt, dass sie den schwebenden Ball zur anderen trägt. Dort übernimmt der Luftstrahl der anderen Düse den Ball, denn die vorhergehende schnalzt per Gummizug zurück. Das Spiel wiederholt sich, und so wird der Ball also ständig zwischen den beiden Düsen hin und her übergeben, ohne jemals mit etwas Anderem als Luft in Berührung zu kommen.

Jedenfalls solange alles klappt ;-) Für den Fall, dass der Ball doch mal herunterfällt, ist da ja noch der große Auffangkorb drum rum.

Die Druckluft stammt von meinem 3,5 bar, 15 L/min Kompressor. Mit einem normalen fischertechnik-Kompressor ist da leider nichts auszurichten.

Habe ich schon erwähnt, dass schwarzes fischertechnik einfach *miserabel* zu fotografieren ist?