---
layout: "image"
title: "Antrieb"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42033
- /details68b0.html
imported:
- "2019"
_4images_image_id: "42033"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42033 -->
Der Antrieb wurde gegenüber dem ft-Jubiläum im Sommer durch eine weitere Untersetzung etwas verlangsamt, damit man die Ballbewegung besser studieren kann. Der XM-Motor wird nur mit 3 V Betriebsspannung betrieben und treibt über Untersetzung, Schnecke und das mittlere Z40 die beiden anderen Z40 an. Alle Z40 sitzen einfach per Achse 30 in stabil auf der Grundplatte befestigten BS15.