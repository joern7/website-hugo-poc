---
layout: "image"
title: "Schrägstehende Drehachsen (1)"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42038
- /details3cd8.html
imported:
- "2019"
_4images_image_id: "42038"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42038 -->
Durch das Schrägstellen der Drehachse und das Ausgleichen des Winkels oben stehen die Düsen in Ruhestellung zwar senkrecht...