---
layout: "image"
title: "Fuß zur Schalldämpfung"
date: "2009-05-17T17:56:43"
picture: "powerkompressor6.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24039
- /details255f-2.html
imported:
- "2019"
_4images_image_id: "24039"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24039 -->
Ohne diesen Fuß wäre der Kompressor noch lauter, weil er ständig vibriert und so auf den Untergrund schlägt.