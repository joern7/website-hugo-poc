---
layout: "image"
title: "Getriebe"
date: "2009-05-17T17:56:43"
picture: "powerkompressor2.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24035
- /details7258.html
imported:
- "2019"
_4images_image_id: "24035"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24035 -->
Durch die große Übersetzung wird der Kolben sehr schnell rein- und rausgezogen.