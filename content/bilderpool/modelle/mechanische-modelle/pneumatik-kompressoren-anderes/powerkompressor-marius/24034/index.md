---
layout: "image"
title: "Gesamtansicht"
date: "2009-05-17T17:56:43"
picture: "powerkompressor1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24034
- /details54de.html
imported:
- "2019"
_4images_image_id: "24034"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24034 -->
