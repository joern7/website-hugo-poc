---
layout: "image"
title: "mini Abschaltung für Kompressoren"
date: "2011-01-22T19:45:38"
picture: "Kompressor_Abschaltung.jpg"
weight: "9"
konstrukteure: 
- "werner"
fotografen:
- "werner"
keywords: ["Abschaltung", "Kompressor", "Federgelenk", "31308", "Betätiger", "36075", "36076"]
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/29753
- /details554f.html
imported:
- "2019"
_4images_image_id: "29753"
_4images_cat_id: "613"
_4images_user_id: "1196"
_4images_image_date: "2011-01-22T19:45:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29753 -->
Das Federgelenk (31308) sorgt für eine optimale Abschaltung bei ca. 0,4bar.