---
layout: "image"
title: "Kompressor"
date: "2007-02-10T23:44:57"
picture: "Kompressor2.jpg"
weight: "16"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8922
- /detailsbdd5.html
imported:
- "2019"
_4images_image_id: "8922"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-02-10T23:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8922 -->
Detail, Kurbel.