---
layout: "image"
title: "Kompressor"
date: "2007-02-10T23:44:57"
picture: "Kompressor1.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8921
- /details8fd6.html
imported:
- "2019"
_4images_image_id: "8921"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-02-10T23:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8921 -->
Das ist ein Kompressor, dessen Kurbelgröße einstellbar ist. Man kann so den ganzen Hubraum des Kompressorzylinders ausnutzen.