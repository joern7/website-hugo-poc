---
layout: "image"
title: "Schalter"
date: "2006-12-17T16:23:48"
picture: "Kompressorstation3.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7916
- /details6b13.html
imported:
- "2019"
_4images_image_id: "7916"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2006-12-17T16:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7916 -->
Mit diesem Polwendschalter kann man den Zylinder ein und ausfahren. Oben:Zylinder ausfahren/ unten:Zylinder einfahren.
Damit nicht beide Magnetventile gleichzeitig angehen, hab ich 2 Dioden eingebaut. Dies ist sehr nützlich, weil man für 2 Magnetventile nur einen Ausgang braucht. Wenn man eine Robo Interface hat, kann man auch das benutzen und die Magnetventile an 2 Lampenausgänge anschliesen. Aber das sollte halt nicht so groß werden.