---
layout: "image"
title: "Umgebauter Mini-Komp5"
date: "2007-01-21T14:05:17"
picture: "Umgebauter_Mini-Komp5.jpg"
weight: "14"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8587
- /details77eb-2.html
imported:
- "2019"
_4images_image_id: "8587"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-01-21T14:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8587 -->
