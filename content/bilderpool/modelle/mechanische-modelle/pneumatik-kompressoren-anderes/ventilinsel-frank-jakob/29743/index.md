---
layout: "image"
title: "Ventilinsel (4)"
date: "2011-01-22T16:48:49"
picture: "ventilinselfrankjakob4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29743
- /details1b41.html
imported:
- "2019"
_4images_image_id: "29743"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29743 -->
Ansicht von hinten rechts