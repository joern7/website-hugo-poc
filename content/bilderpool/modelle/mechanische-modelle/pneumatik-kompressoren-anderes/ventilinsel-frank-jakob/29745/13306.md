---
layout: "comment"
hidden: true
title: "13306"
date: "2011-01-22T20:38:03"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Hallo Ludger

Links und rechts ist doch gar kein Platz für eine Federnocke !?
Die alte Variante des BS7.5 habe ich nicht und ich weiß auch
nicht ob das was bringt. Ich habe schon möglichst schwergängige
Bauteile benutzt, aber nach 100 Bewegungen fliegt ein Teil ab.

Gruß,
Frank