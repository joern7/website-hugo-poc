---
layout: "comment"
hidden: true
title: "17431"
date: "2012-10-10T16:14:52"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Fantastisch!!! Das ist wirklich eine Neuerung! Und super kompakt umgesetzt!!!

Klar, es sind "leider nur" Magnetventile - das macht so einen Antrieb natürlich relativ einfach. Eine klassische Watt'sche Dampfmaschine mit mechanischem Ventilantrieb auf einem Fahrgestell - das wär's!

Dirk, Du bist dran ... ;o)

Gruß, Thomas