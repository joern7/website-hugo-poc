---
layout: "comment"
hidden: true
title: "9219"
date: "2009-05-09T23:52:23"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Schnaggels,
ich werde die Abwärtsbewegung noch eindämmen, indem ich dosiert Kabelbinder um die Schläuche mache.
Das Problem ist, daß ich keine 5 Drosseln habe.
Außerdem brauche ich mehr als 0,5 bar, damit die Seitenwände hoch gehen. Bei der doppelten Wand brauche ich sogar 1 bar. Wenn ich meinem normalen FT-Kompressor gut zurede, schafft der 0,45 bar. Das langt aber noch nicht. Der Pari-Inhalierboy drückt 1,3 bar.
Für einen 2. Zylinder habe ich keinen Platz mehr. Denn auf der Hauptplattform gehen 4 Zylinder in 4 Richtungen weg.
Viele Grüße, Andreas.