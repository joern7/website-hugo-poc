---
layout: "image"
title: "5 - PneumaCube Detail Ventilsteuerung"
date: "2009-05-08T23:41:20"
picture: "5_-_PneumaCube_Detail_Ventilsteuerung.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23941
- /details611c.html
imported:
- "2019"
_4images_image_id: "23941"
_4images_cat_id: "1643"
_4images_user_id: "724"
_4images_image_date: "2009-05-08T23:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23941 -->
Hier erkennt man schön die Rutschkupplung. Ist die Endlage des Handventils erreicht, dreht der Motor durch. Dadurch brauche ich keine 10 Endtaster.
Jedoch brauche ich ein erstaunlich hohes Drehmoment. Dieses erreiche ich, indem ich mit Kabelbindern die Seilrollen noch weiter zupresse, bevor ich die Stange einschiebe. 
Die Motoren versuchen sich mit gewaltiger Kraft zu verwinden. Dies wird durch die obere Reihe durchgehender Bausteine 30 verhindert, die mit den Motoren verbunden sind.