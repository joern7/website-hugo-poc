---
layout: "image"
title: "PneumaCube ohne Elektronic"
date: "2009-05-17T17:57:42"
picture: "2009-PneumaCube-ohne-Elektronic_005.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24046
- /details7d97.html
imported:
- "2019"
_4images_image_id: "24046"
_4images_cat_id: "1649"
_4images_user_id: "22"
_4images_image_date: "2009-05-17T17:57:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24046 -->
