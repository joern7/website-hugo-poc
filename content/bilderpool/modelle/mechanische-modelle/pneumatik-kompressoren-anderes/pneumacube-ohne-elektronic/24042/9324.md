---
layout: "comment"
hidden: true
title: "9324"
date: "2009-05-21T12:25:28"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Der Pneumatik-Steuerung ist eigentlich sehr einfach:

Jedes Ventil steuert nur die nächste Klappe: 1-->2-->3-->4-->5-->6-->1-->2-->3-->4-->5-->6-->1-->2-->3--> usw.....

Ich brauche "nur" 0,75 bar: ein Membrankompressor (max.1,2 bar) mit ein verstellbares Sicherheits-ventil.


Gruss,

Peter