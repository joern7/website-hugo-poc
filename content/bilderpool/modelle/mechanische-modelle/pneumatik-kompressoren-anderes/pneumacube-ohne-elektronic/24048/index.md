---
layout: "image"
title: "PneumaCube ohne Elektronic"
date: "2009-05-17T23:35:27"
picture: "2009-PneumaCube-ohne-Elektronic_007.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24048
- /detailsadf6.html
imported:
- "2019"
_4images_image_id: "24048"
_4images_cat_id: "1649"
_4images_user_id: "22"
_4images_image_date: "2009-05-17T23:35:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24048 -->
