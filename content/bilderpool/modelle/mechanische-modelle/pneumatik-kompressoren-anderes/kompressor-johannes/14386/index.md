---
layout: "image"
title: "Kompressor"
date: "2008-04-27T13:35:53"
picture: "kompressor1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14386
- /details89ef.html
imported:
- "2019"
_4images_image_id: "14386"
_4images_cat_id: "1331"
_4images_user_id: "747"
_4images_image_date: "2008-04-27T13:35:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14386 -->
Das ist ein Gesamtbild meines selbsterfundenen Kompressors.