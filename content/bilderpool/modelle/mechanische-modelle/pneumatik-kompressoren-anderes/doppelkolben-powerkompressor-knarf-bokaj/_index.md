---
layout: "overview"
title: "Doppelkolben-Powerkompressor (Knarf Bokaj)"
date: 2020-02-22T08:14:56+01:00
legacy_id:
- /php/categories/2169
- /categories7a35.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2169 --> 
Doppelkolben Kompressor mit 8:1 Powermotor und Endabschaltung auf 120*60 Grundplatte.