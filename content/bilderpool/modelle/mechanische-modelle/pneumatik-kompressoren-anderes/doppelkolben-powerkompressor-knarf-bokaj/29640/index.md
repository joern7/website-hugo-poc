---
layout: "image"
title: "Kompressor PICT5005"
date: "2011-01-09T15:18:49"
picture: "doppelkolbenpowerkompressorknarfbokaj1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29640
- /details204c.html
imported:
- "2019"
_4images_image_id: "29640"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-09T15:18:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29640 -->
Ansicht vorne rechts