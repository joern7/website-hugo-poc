---
layout: "image"
title: "Kompressor PICT5009"
date: "2011-01-09T15:18:50"
picture: "doppelkolbenpowerkompressorknarfbokaj5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29644
- /details1d81-2.html
imported:
- "2019"
_4images_image_id: "29644"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-09T15:18:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29644 -->
Frontalansicht