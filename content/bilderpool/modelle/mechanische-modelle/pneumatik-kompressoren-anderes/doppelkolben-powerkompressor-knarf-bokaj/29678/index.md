---
layout: "image"
title: "Kompressor v1.1"
date: "2011-01-12T18:25:33"
picture: "PICT5020.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: ["Kompressor", "Pneumatik"]
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29678
- /details80f4.html
imported:
- "2019"
_4images_image_id: "29678"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-12T18:25:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29678 -->
Kleine Verbesserungen: 
Kolbenbefestigung verstärkt
Kunststoff-Federn unter der Bauplatte als Entkopplung
Oberer Totpunkt 2,5mm höher gesetzt