---
layout: "image"
title: "Kompressoranhänger 5"
date: "2009-11-20T17:55:37"
picture: "kompressoranhaenger5.jpg"
weight: "5"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: ["Kompressor", "Pneumatik", "TST", "Betätiger", "Anhänger"]
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25809
- /detailsc228.html
imported:
- "2019"
_4images_image_id: "25809"
_4images_cat_id: "1810"
_4images_user_id: "1027"
_4images_image_date: "2009-11-20T17:55:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25809 -->
...ich habe die von Andreas Tacke (TST) hergestellte Bauplatte mit passender Exzenterscheibe verwendet. Damit lässt sich der Powermotor auf engem Raum zur Druckerzeugung nutzen. Das System baut den Druck in etwa 2 Sekunden auf (300 bis 400 mbar).