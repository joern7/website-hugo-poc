---
layout: "image"
title: "Pneumatik-Greifer 2"
date: "2008-01-29T23:14:23"
picture: "Pneumatik-Greifer_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/13463
- /details136b.html
imported:
- "2019"
_4images_image_id: "13463"
_4images_cat_id: "1228"
_4images_user_id: "328"
_4images_image_date: "2008-01-29T23:14:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13463 -->
