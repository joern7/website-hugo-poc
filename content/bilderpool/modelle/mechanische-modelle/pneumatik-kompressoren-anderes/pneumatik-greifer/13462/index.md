---
layout: "image"
title: "Pneumatik-Greifer 1"
date: "2008-01-29T23:14:23"
picture: "Pneumatik-Greifer_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/13462
- /details3077.html
imported:
- "2019"
_4images_image_id: "13462"
_4images_cat_id: "1228"
_4images_user_id: "328"
_4images_image_date: "2008-01-29T23:14:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13462 -->
Ergebnis meiner ersten Pneumatik-Versuche war dieser kleine Pneumatik-Greifer, den ich frei hängend an einem Bagger-Arm angebracht hatte. Trotz seiner geringen Größe greift er ganz ordentlich.

Sicherlich nix besonders Neues, aber ich habe so einen Greifer hier in der ftcommunity noch nicht gesehen.