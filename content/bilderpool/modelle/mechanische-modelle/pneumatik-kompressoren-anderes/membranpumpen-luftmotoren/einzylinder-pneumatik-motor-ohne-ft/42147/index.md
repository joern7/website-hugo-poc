---
layout: "image"
title: "Schwungrad"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42147
- /detailsf93c-2.html
imported:
- "2019"
_4images_image_id: "42147"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42147 -->
Mit der richtigen Masse der Schwungscheibe und richtier Einstellung der Abklemmpunkte kann man den Motor nach Anlegen der Druckluft prblemlos mit einem sachten Anschupser der Schwungscheibe starten.