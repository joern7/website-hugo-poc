---
layout: "image"
title: "4-Zylinder-Axialkolbenmotor + Festo -Ventilen"
date: "2017-11-06T18:13:53"
picture: "axiaalkolbenmotor4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46907
- /detailsc945.html
imported:
- "2019"
_4images_image_id: "46907"
_4images_cat_id: "3471"
_4images_user_id: "22"
_4images_image_date: "2017-11-06T18:13:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46907 -->
Ich habe die 4-Zylinder-Axialkolbenmotor nachgebaut  mit  eine: Antriebseinheit (4 pneumatikzylinder) + "Taumelkreuz" + Schwungrad. 
Statt das Ventilinsel habe ich die Festo 3/2-Wegeventilen + Nocken genutzt.
