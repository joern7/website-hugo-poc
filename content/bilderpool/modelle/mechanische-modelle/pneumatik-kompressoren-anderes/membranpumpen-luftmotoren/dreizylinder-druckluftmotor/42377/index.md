---
layout: "image"
title: "Zylinder und Mechanik (1)"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42377
- /details8389.html
imported:
- "2019"
_4images_image_id: "42377"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42377 -->
Dieses und die nächsten beiden Bilder zeigen den Aufbau der Antriebsmechanik. Es wurden Kompressorzylinder verwendet, weil die besonders leichtgängig sind. Auch bei den S-Riegel 8/Riegelscheiben-Kombinationen wurde auf Leichtgängigkeit geachtet.