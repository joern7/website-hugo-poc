---
layout: "comment"
hidden: true
title: "21171"
date: "2015-10-30T08:32:40"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Wenig Kraft ist das Stichwort. Viel Kraft schafft die "Kurbel" nicht.
Die Kompressorkurbel (36949) geht nicht wegen der Rastanschlüsse? Oder ist der KR zu klein?