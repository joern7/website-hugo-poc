---
layout: "comment"
hidden: true
title: "21176"
date: "2015-10-31T14:49:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Mit drei (einfach wirkenden) Kompressorzylindern will ich noch was versuchen. Was genau meinst Du mit der "ordentlichen" Kurbelwelle? Die vom Arbeitszylinder oder die für die Ventilsteuerung?

Gruß,
Stefan