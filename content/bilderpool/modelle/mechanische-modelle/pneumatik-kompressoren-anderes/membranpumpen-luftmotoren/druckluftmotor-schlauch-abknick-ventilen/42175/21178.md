---
layout: "comment"
hidden: true
title: "21178"
date: "2015-10-31T21:07:18"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Oh, jetzt bin ich gespannt!
Meine Definiton einer ordentlichen Kurbelwelle: möglichst stabil (gegen Verdrehung), damit man wirklich Drehmoment am Motorausgang abgreifen kann und natürlich wieder möglichst kompakt ;-)

Die Welle der Ventilsteuerung wird ja kaum belastet und es reicht vermutlich auch bei mehreren Zylindern eine "nicht ordentliche" Welle ...

Schöne Grüße,
Stefan