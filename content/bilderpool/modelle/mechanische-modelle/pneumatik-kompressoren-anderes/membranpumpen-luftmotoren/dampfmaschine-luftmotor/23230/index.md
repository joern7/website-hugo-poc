---
layout: "image"
title: "Dampfmaschine 5"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_07.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23230
- /details43b5.html
imported:
- "2019"
_4images_image_id: "23230"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23230 -->
