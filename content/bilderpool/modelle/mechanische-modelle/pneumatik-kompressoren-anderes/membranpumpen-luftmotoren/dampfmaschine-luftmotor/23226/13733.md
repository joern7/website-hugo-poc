---
layout: "comment"
hidden: true
title: "13733"
date: "2011-02-27T23:25:17"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Ein tolles Modell - einfach ideal, um Schülern die technischen Hintergründe einer Dampfmaschine zu vermitteln!
Sehr instruktiven "technikhistorischen Hintergrund" bietet in diesem Zusammenhang der 15-Minuten-Film aus der Serie "Meilensteine aus Naturwissenschaft und Technik" über James Watt und die Dampfmaschine:
http://www.youtube.com/watch?v=txUAoEWgxpE
Gruß, Dirk