---
layout: "image"
title: "Dampfmaschine 10"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_12.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23235
- /details7f94.html
imported:
- "2019"
_4images_image_id: "23235"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23235 -->
Hier ist gut die recht einfache Verschlauchung zu sehen. Rechts im Bild der Druckluftanschluss.