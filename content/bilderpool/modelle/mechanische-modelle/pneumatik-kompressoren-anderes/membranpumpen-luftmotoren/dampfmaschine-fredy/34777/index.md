---
layout: "image"
title: "Dampfmaschine 06"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine6.jpg"
weight: "6"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34777
- /detailsea0f-2.html
imported:
- "2019"
_4images_image_id: "34777"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34777 -->
Ventilsteuerung