---
layout: "image"
title: "Gesamtansicht"
date: "2016-03-21T13:33:37"
picture: "pneumatikmotor01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43194
- /details9710.html
imported:
- "2019"
_4images_image_id: "43194"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43194 -->
Die verbesserte Version des Einzylinder-Pneumatikmotors 2.0 (http://ftcommunity.de/categories.php?cat_id=3173).

Verbesserungen:
stabilere Kurbelwelle
bessere Ventile...
und zwei Zylinder

Dank der Zylinder mit Feder ist der Motor selbsanlaufend.

Ein kurzes Video gibt es unter: https://youtu.be/OiWAKAY1sT0