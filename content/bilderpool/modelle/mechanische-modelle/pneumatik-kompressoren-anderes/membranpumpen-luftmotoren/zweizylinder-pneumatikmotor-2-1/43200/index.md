---
layout: "image"
title: "Ventil 1"
date: "2016-03-21T13:33:55"
picture: "pneumatikmotor07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43200
- /details986d.html
imported:
- "2019"
_4images_image_id: "43200"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43200 -->
Das Ventil in seiner einen Endpositon.
Der Ventilhebel greift jetzt nicht mehr an der Kurbelwelle an, sondern am Zylinder. Somit ist das Ganze kompakter und der Motor läuft auch besser.