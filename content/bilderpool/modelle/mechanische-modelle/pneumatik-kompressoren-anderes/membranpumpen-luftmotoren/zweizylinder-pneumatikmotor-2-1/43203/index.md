---
layout: "image"
title: "Kurbelwelle"
date: "2016-03-21T13:33:55"
picture: "pneumatikmotor10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43203
- /details6007-2.html
imported:
- "2019"
_4images_image_id: "43203"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43203 -->
Die neue Kurbelwelle ist jetzt vorne und hinten gelagert und somit können ordentilche Kräfte problmelos übertragen werden.

Aufgebaut ist sie aus zwei Heblen, die in der Mitte mit einer Kunststoff-Rast-Achse 30 verbunden sind.