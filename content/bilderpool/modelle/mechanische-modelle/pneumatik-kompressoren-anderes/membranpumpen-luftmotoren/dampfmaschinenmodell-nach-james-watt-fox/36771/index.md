---
layout: "image"
title: "Kompressorstation (mit Abschaltautomatik)"
date: "2013-03-18T12:28:43"
picture: "Kompressorstation_01.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36771
- /details9334.html
imported:
- "2019"
_4images_image_id: "36771"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2013-03-18T12:28:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36771 -->
Der (neue) ft-Kompressor mit Tank und Abschaltautomatik arbeitet tatsächlich nur einen Bruchteil der Zeit. Wenn es doch die Betätiger aus den Festo-Pneumatik-Kästen noch gäbe...