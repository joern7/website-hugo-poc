---
layout: "image"
title: "Dampfmaschine nach James Watt (Draufsicht)"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35828
- /details2d09-2.html
imported:
- "2019"
_4images_image_id: "35828"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35828 -->
Hier sieht man das Schubkurbelgetriebe aus Kolben, Pleuelstange (bestehend aus zwei X-Streben 127,2) und Exzenter, sowie die gute alte Kurbelwelle, die die Druckluftzufuhr regelt.