---
layout: "comment"
hidden: true
title: "17410"
date: "2012-10-08T09:50:19"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Fredy ist bei seiner schuckeligen Dampfmaschine (http://www.ftcommunity.de/categories.php?cat_id=2569) mit einem deutlich kleineren Schwungrad ausgekommen; das klappt aber nur, wenn man die Festo-Ventile für die Druckluft-Umschaltung verwendet - bei denen gibt es praktisch keinen "Umschalt-Leerlauf".
Gruß, Dirk