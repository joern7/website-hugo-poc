---
layout: "image"
title: "Schwungrad der Dampfmaschine"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35829
- /details6a84-2.html
imported:
- "2019"
_4images_image_id: "35829"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35829 -->
Das Schwungrad hilft, die beiden "toten Punkte" des Schubkurbelgetriebes zu überwinden. Es hat die Funktion eines "Energiespeichers": Die Schubenergie des Kolbens wird auf das Schwungrad übertragen, dessen Massenträgheit das Weiterdrehen der Kurbelwelle bewirkt. Dabei kommt es vor allem auf das Gewicht am äußersten Ende des Schwungrads an: Je größer der Radius und je schwerer der "äußere Ring", desto gleichmäßiger läuft die Dampfmaschine. Montiert habe ich das Schwungrad auf ein Speichenrad (mit einem Raupenband 31057 dazwischen, damit es nicht rutscht).