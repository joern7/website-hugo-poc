---
layout: "image"
title: "Steuerung 4"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42661
- /details0f72.html
imported:
- "2019"
_4images_image_id: "42661"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42661 -->
Hier ist das Ventil von unten zu sehen.
Bei genauerem Hinschauen erkennt man die zwei Schlauchschleifen für Zu- und Abluft, die abwechselnd abgeknickt werden.