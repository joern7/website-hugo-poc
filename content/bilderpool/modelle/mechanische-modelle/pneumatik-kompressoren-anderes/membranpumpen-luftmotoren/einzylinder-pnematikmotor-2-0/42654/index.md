---
layout: "image"
title: "Vorderseite"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42654
- /detailsec01.html
imported:
- "2019"
_4images_image_id: "42654"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42654 -->
Ich habe den Pneumatikzyliner mit Feder verbaut, weil er sehr leichtgängig ist und somit wenig Schwungmasse benötigt wird.