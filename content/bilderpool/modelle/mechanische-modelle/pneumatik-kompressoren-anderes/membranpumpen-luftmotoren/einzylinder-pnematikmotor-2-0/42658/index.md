---
layout: "image"
title: "Steuerung"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42658
- /details8e5a-2.html
imported:
- "2019"
_4images_image_id: "42658"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42658 -->
Das Ventil ist so eng wie möglich an den Zylinder gebaut um Platz zu sparen.
Der Ventilhebel ist direkt durch eine Statikstrebe mit der Kurbelwelle verbunden.
Das Einstellen des Ventiles ist hauptsächlich durch die Länger der abgeknickten Schläuche möglich. Sprich ist einmal die richtige Schlauchlänge gewählt, läuft das ganze ohne Probleme.
