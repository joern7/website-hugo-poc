---
layout: "image"
title: "Steruerung 3"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42660
- /detailsadfc.html
imported:
- "2019"
_4images_image_id: "42660"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42660 -->
Hier ist der hintere Teil des Ventiles abgebaut (man kann in rechts unten im Bild erkennen) um bessere Sicht zu schaffen.