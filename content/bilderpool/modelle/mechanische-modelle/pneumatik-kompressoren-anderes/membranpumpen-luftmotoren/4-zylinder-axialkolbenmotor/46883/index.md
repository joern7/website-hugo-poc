---
layout: "image"
title: "Ventil"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46883
- /details7faf.html
imported:
- "2019"
_4images_image_id: "46883"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46883 -->
Die Ventile selbst habe ich weitgehend von meinen vorhergehenden Druckluftmotoren übernommen ( siehe http://www.ftcommunity.de/categories.php?cat_id=3208 oder http://www.ftcommunity.de/categories.php?cat_id=3173)