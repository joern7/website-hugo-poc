---
layout: "image"
title: "Drehbewegung3"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46870
- /detailsa629.html
imported:
- "2019"
_4images_image_id: "46870"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46870 -->
Durch die Anordung der Zylinder ist der Motor in jeder Position selbstanlaufend.