---
layout: "image"
title: "Drehbewegung1"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46868
- /detailsf6de-2.html
imported:
- "2019"
_4images_image_id: "46868"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46868 -->
Auf den folgenden Bildern kann man sich in Ruhe anschauen, wie sich die einzelnen Teile bei einer Umdrehung bewegen.