---
layout: "image"
title: "Antrieb Ventile"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46878
- /details7a7c.html
imported:
- "2019"
_4images_image_id: "46878"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46878 -->
Der Antrieb für die Ventile erfoglt über die hier zu sehende Kette.