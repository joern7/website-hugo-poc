---
layout: "image"
title: "Gesamt von oben"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46862
- /details3694.html
imported:
- "2019"
_4images_image_id: "46862"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46862 -->
Rechts unten: Antriebseinheit (4 Pneumatikzylinder)
Bildmitte unten: "Taumelkreuz"
Links unten: Schwungrad und Lagerung des Schwungrades

Oben: Ventilinsel + Druckluftspeicher