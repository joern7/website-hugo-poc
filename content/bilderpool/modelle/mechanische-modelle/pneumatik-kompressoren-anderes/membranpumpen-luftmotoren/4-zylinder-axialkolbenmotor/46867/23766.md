---
layout: "comment"
hidden: true
title: "23766"
date: "2017-11-05T14:11:10"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Der Ausgleich funktioniert auch unter Last (nicht perfekt, aber völlig ausreichend). Bis jetzt habe ich mit der Reibung an den Achsen kein Problem gehabt, ansonsten könnte ich sie immer noch schmieren... ;-).
Die drei Achsen machen das Ganze zudem stabiler und verhindern trotz erhöhter Reibung ein Verspannen.