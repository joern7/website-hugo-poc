---
layout: "image"
title: "Ventile"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46882
- /detailsbfe9.html
imported:
- "2019"
_4images_image_id: "46882"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46882 -->
