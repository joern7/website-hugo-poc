---
layout: "image"
title: "Gesamtansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46856
- /details5714.html
imported:
- "2019"
_4images_image_id: "46856"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46856 -->
Der Motor wäre stark genug um das ein oder andere Modell anzutreiben...