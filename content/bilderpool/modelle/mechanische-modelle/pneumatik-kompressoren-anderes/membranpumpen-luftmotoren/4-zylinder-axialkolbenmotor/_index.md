---
layout: "overview"
title: "4 Zylinder Axialkolbenmotor"
date: 2020-02-22T08:14:34+01:00
legacy_id:
- /php/categories/3469
- /categoriesc636.html
- /categories57bb.html
- /categoriesa949.html
- /categoriesc164.html
- /categories406c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3469 --> 
Fasziniert von Alfred Petteras 6-Zylinder-Axialkolbenmotor ( http://www.ftcommunity.de/details.php?image_id=1040#col3 ) habe ich mich entschlossen meinen eigenen Axialkolbenmotor zu bauen. Was dabei herausgekommen ist könnt ihr auf den nächsten Fotos genauer unter die Lupe nehmen ;-).

Allgemeines:
+Anzahl Zylinder: 4
+Gewicht: ca. 3,5 kg
+Bauzeit: ca. 2 Wochen

Video: https://youtu.be/6ziPNIWrqGw