---
layout: "image"
title: "Ventilinsel"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46879
- /details5cce-2.html
imported:
- "2019"
_4images_image_id: "46879"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46879 -->
Da die Zylinder mit Druckluft aus- und eingefahren werden, sind für die zwei Zylinderpaare jeweils zwei 3/2 Wegeventile erforderlich (sprich ein Ventil zum Ausfahren, das andere zum Einfahren des Zylinders).
Die Ventile eines Ventilpaares für ein Zylinderpaar sind auf dem Foto jeweils ober- und unterhalb der Ventilantriebsachse angeordnet.

Die Welle mit den Z10 sorgt dafür, dass die Exzenterbaugruppen immer synchron laufen.