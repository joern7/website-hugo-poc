---
layout: "image"
title: "Gesamtansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46855
- /detailsb21e.html
imported:
- "2019"
_4images_image_id: "46855"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46855 -->
Im "Normalbetrieb" (wie im Video zu sehen) reicht die Druckluftversorgung durch einen blauen ft-Kompressor. Die zwei Drucklufttanks wären nicht unbedingt notwendig.