---
layout: "comment"
hidden: true
title: "23761"
date: "2017-11-04T14:28:47"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Alter Schwede, das nenne ich heavy duty Maschinenbau vom Feinsten. Die Aufhängung hier ist Klasse, und das Detail mit den Klemmringen auf den Zylinderkolben kenne ich so auch noch nicht. Und dann noch die Schlauch-Ventile, ganz zu schweigen von den tollen Fotos und Beschreibungen. Spitzenmäßig!

Gruß,
Stefan