---
layout: "image"
title: "Mechanik"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46864
- /detailsc5be.html
imported:
- "2019"
_4images_image_id: "46864"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46864 -->
Funktionsweise:
Die lineare Bewegung der Antriebszylinder, welche parallel zur Hauptdrehachse des Motores erfolgt, wird über das taumelnde Kreuz in eine Drehbewegung umgewandelt.

Die vier Zylinder bringen das Kreuz (in Bildmitte) in eine taumelnde Bewegung. Das Kreuz ist so angebracht, dass dessen Mittelpunkt immer an der gleichen Position bleibt. Die Mitte des Kreuzes bildet ein BS 15 mit Loch, welcher mit Hilfe einer Rastachse mit Platte gelagert ist. Durch die taumelnde Bewegung beschreibt jeder Punkt, welcher sich auf der Mittelachse der Rastachse mit Platte befindet, eine Kreisbahn (dessen Mittelpunkt auf der Mittelachse des Schwungrades liegt). Die Rastachse mit Platte ist exzentrisch und in einem Winkel von 22,5° auf dem Schwungrad angebracht und überträgt somit die entstehende Drehbewegung.