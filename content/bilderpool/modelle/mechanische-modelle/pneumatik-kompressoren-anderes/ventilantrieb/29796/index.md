---
layout: "image"
title: "Ventilantrieb (6)"
date: "2011-01-24T19:23:40"
picture: "ventilantrieb6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29796
- /details21df.html
imported:
- "2019"
_4images_image_id: "29796"
_4images_cat_id: "2190"
_4images_user_id: "729"
_4images_image_date: "2011-01-24T19:23:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29796 -->
Detailansicht
Zwei Federnocken im BS7,5 sorgen für etwas mehr Festigkeit des Kabelhalters :-)