---
layout: "image"
title: "Ventilantrieb (1)"
date: "2011-01-24T19:23:40"
picture: "ventilantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29791
- /details954e.html
imported:
- "2019"
_4images_image_id: "29791"
_4images_cat_id: "2190"
_4images_user_id: "729"
_4images_image_date: "2011-01-24T19:23:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29791 -->
Kompakter Handventilantrieb. Wesentlich kleiner als bei meiner Ventilinsel, leider dauert es ca. 3 Sekunden bis die Endstellung erreicht ist. Naja, kein Vorteil ohne Nachteil ;-)