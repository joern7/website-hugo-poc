---
layout: "image"
title: "Momentaufnahme 3"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36879
- /details6887.html
imported:
- "2019"
_4images_image_id: "36879"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36879 -->
Schließlich wird der Ball auf der nur aus Streben konstruierten leicht abfallenden Ablaufbahn sanft abgesetzt.  Er rollt dann zurück, und rechtzeitig wird die Düse wieder in die Senkrechte zurückgestellt sein.