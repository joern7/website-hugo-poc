---
layout: "image"
title: "Gummirückzug"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36876
- /details08ba.html
imported:
- "2019"
_4images_image_id: "36876"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36876 -->
Ein normales Haushaltsgummi besorgt das Rückstellen der Mechanik, sobald die Segmentscheibe ihr Z40 verlassen hat.