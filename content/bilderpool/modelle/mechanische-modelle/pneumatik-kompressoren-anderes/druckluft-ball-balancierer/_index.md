---
layout: "overview"
title: "Druckluft-Ball-Balancierer"
date: 2020-02-22T08:15:04+01:00
legacy_id:
- /php/categories/2740
- /categories5554.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2740 --> 
Angeregt durch ein Festo-Modell im Technikmuseum Sinsheim, das einen Tischtennisball auf einem senkrecht nach oben zeigenden Luftstrahl balancierend transportierte, entstand dieses Modell.