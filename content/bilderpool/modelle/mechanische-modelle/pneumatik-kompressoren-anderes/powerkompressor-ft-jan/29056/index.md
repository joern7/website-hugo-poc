---
layout: "image"
title: "Übertragung"
date: "2010-10-25T19:15:04"
picture: "pkomp6.jpg"
weight: "6"
konstrukteure: 
- "Jan Hils"
fotografen:
- "Jan Hils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-Jan"
license: "unknown"
legacy_id:
- /php/details/29056
- /detailsc396-2.html
imported:
- "2019"
_4images_image_id: "29056"
_4images_cat_id: "2111"
_4images_user_id: "1181"
_4images_image_date: "2010-10-25T19:15:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29056 -->
Hier nochmal die Kette zwischen Kurbelwelle und Powermot.