---
layout: "image"
title: "ft-Arm Gelenk"
date: "2013-03-07T13:30:37"
picture: "bild07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36719
- /details0f2e.html
imported:
- "2019"
_4images_image_id: "36719"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36719 -->
Das Gelenk im Detail. Leider haben die P-Zylinder einen zu kleinen Hub, sodass der ft-Arm nicht so weit ausschwenken kann wie der menschliche.