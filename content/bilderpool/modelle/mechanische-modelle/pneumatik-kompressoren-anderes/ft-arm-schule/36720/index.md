---
layout: "image"
title: "ft-Arm Steuerpult"
date: "2013-03-07T13:30:38"
picture: "bild08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36720
- /details248b.html
imported:
- "2019"
_4images_image_id: "36720"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36720 -->
Mit diesen beiden Tasten konnte man den Arm nach oben bzw. nach unten bewegen.

Wusstet ihr, dass man den Drehknopf des Power-Sets herausziehen und durch eine ft-Rastachse ersetzen kann?