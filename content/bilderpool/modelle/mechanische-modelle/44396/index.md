---
layout: "image"
title: "Verkleinerter -etwas anderer Motor-"
date: "2016-09-25T16:29:00"
picture: "Verkleinerter_-etwas_anderer_Motor-.jpg"
weight: "9"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Hui-Motor"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44396
- /details4a31.html
imported:
- "2019"
_4images_image_id: "44396"
_4images_cat_id: "127"
_4images_user_id: "2635"
_4images_image_date: "2016-09-25T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44396 -->
Ich nenne ihn auch Hui-Motor!
Hier die verkleinerte Version mit einem Magneten als Achse (4x25 mm, alternativ auch 3 Stück 4x10 mm).