---
layout: "image"
title: "PICT4952"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26676
- /details053e.html
imported:
- "2019"
_4images_image_id: "26676"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26676 -->
Ansicht von vorne rechts