---
layout: "comment"
hidden: true
title: "16031"
date: "2012-01-08T19:18:47"
uploadBy:
- "McDoofi"
license: "unknown"
imported:
- "2019"
---
Hallo,

der Hut sieht echt klasse aus und auch die Mechanik ist nicht schlecht gelungen. Besonders gut gefällt mir die Steuerung des Summers und der Lampen.

MfG McDoofi