---
layout: "image"
title: "Unterseite"
date: "2012-01-08T18:31:42"
picture: "partyhut2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33862
- /details12cc.html
imported:
- "2019"
_4images_image_id: "33862"
_4images_cat_id: "2504"
_4images_user_id: "104"
_4images_image_date: "2012-01-08T18:31:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33862 -->
Die Drehscheibe berührt gerade so meine Haare. Sie treibt über die nach oben laufende Achse das Karussell und das Herz an. Über das Zwischenzahnrad zur Drehrichtungsumkehr wird die obere Drehscheibe in anderer Richtung angetrieben, die die Autos zum Fahren bringt. Die obere Drehscheibe läuft auf einer Freilaufnabe, und über den mitdrehenden Schleifring kommt der Strom zu den Lampen an den Autos. Der Schalter dient zum Ein-/Ausschalten des Stroms vom 9-V-Akku im Hintergrund.