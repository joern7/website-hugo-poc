---
layout: "image"
title: "Summer"
date: "2012-01-08T18:31:42"
picture: "partyhut4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33864
- /details9ae2.html
imported:
- "2019"
_4images_image_id: "33864"
_4images_cat_id: "2504"
_4images_user_id: "104"
_4images_image_date: "2012-01-08T18:31:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33864 -->
Der ach so toll verbaubare Summer ist zwischen zwei BS15 eingeklemmt und wird von zwei Platten 15*15 am Herausrutschen gehindert. Ich war überrascht, was für einen Radau der macht.

Man sieht aber auch noch, wie zwei Statikadapter halb in der oberen Drehscheibe stecken und den Strebenantrieb der Autos zum Mitdrehen bringen.