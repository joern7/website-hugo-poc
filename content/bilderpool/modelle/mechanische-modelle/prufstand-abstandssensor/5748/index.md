---
layout: "image"
title: "01-Übersicht über die Anlage"
date: "2006-02-11T14:07:02"
picture: "01-Gesamtanlage.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5748
- /details154f.html
imported:
- "2019"
_4images_image_id: "5748"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:07:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5748 -->
Die Schienen erlauben einen Verfahrweg von ca. 80 cm. Gleich dabei der Steuerrechner, der auch die Analogdaten aufnimmt, dazu die Schrittmotorsteuerung mit Auswertung der Endschalter.