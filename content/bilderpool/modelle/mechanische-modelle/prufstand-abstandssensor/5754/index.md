---
layout: "image"
title: "07-Tiefpaß"
date: "2006-02-11T14:21:45"
picture: "07-Tiefpasignal.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5754
- /details3dcb.html
imported:
- "2019"
_4images_image_id: "5754"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:21:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5754 -->
Ein passiver Tiefpaß, bestehend aus einem Widerstand von 33k und einem Kondensator von 1 µF glättet perfekt. So ist das Signal auswertbar.