---
layout: "image"
title: "Vorderansicht"
date: "2008-07-15T22:17:21"
picture: "zylindermotor02.jpg"
weight: "3"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14876
- /detailsc907.html
imported:
- "2019"
_4images_image_id: "14876"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14876 -->
Modell noch nicht ganz fertig