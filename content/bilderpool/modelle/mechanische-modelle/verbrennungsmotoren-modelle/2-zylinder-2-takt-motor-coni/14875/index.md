---
layout: "image"
title: "Verbindung für Luftpumpe"
date: "2008-07-15T22:17:21"
picture: "zylindermotor01.jpg"
weight: "2"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14875
- /details0c04.html
imported:
- "2019"
_4images_image_id: "14875"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14875 -->
Weil der Fischertechnikkompressor für meinen 2 Takter zu wenig druck hat nehme ich einfach eine Fahrradpumpe mit Balladapter.