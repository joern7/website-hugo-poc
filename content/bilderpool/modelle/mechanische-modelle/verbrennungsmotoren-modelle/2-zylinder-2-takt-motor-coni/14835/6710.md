---
layout: "comment"
hidden: true
title: "6710"
date: "2008-07-15T22:22:12"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr schön, besonders gefällt mir, wie Du die Zylinder von Querbelastungen entkoppelt hast.

Gruß,
Stefan