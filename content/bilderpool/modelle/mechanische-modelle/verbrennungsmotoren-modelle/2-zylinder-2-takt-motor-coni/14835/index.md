---
layout: "image"
title: "2 Zylindermotor"
date: "2008-07-15T22:11:59"
picture: "taktmotor1.jpg"
weight: "1"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14835
- /detailsec73.html
imported:
- "2019"
_4images_image_id: "14835"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:11:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14835 -->
Diese noch nicht ganz fertige Modell eines 2 Zylindermotors soll rein Mechanisch funktionieren.(ohne Magnetventile). 
Weiter infos bald unter http://erfinderhome.repage4.de