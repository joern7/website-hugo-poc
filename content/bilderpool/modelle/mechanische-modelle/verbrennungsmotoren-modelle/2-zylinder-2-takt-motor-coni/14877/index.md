---
layout: "image"
title: "Draufsicht"
date: "2008-07-15T22:17:21"
picture: "zylindermotor03.jpg"
weight: "4"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14877
- /detailsef66.html
imported:
- "2019"
_4images_image_id: "14877"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14877 -->
Das Schwungrad sitzt auf ein Stange die ich an die beiden Kegelzahnräder mit Sekunden kleber befestigt habe. (hält einwandfrei)