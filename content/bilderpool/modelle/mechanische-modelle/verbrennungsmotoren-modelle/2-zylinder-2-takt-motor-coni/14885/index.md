---
layout: "image"
title: "Draufsicht5"
date: "2008-07-15T22:17:21"
picture: "zylindermotor11.jpg"
weight: "12"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14885
- /details6809.html
imported:
- "2019"
_4images_image_id: "14885"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14885 -->
