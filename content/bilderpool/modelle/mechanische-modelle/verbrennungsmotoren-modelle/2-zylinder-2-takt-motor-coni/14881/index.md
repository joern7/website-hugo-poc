---
layout: "image"
title: "Draufsicht2"
date: "2008-07-15T22:17:21"
picture: "zylindermotor07.jpg"
weight: "8"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14881
- /details3740-3.html
imported:
- "2019"
_4images_image_id: "14881"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14881 -->
