---
layout: "image"
title: "Ein- und Auslassventil und die Zündkerze"
date: "2008-03-19T16:00:26"
picture: "kolbenmotor2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13957
- /details58f1.html
imported:
- "2019"
_4images_image_id: "13957"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13957 -->
Hier sieht man das Einlassventil, das Auslassventil und die Zündkerze.