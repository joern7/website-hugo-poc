---
layout: "image"
title: "2-Takt-Motor 5"
date: "2007-01-08T17:16:10"
picture: "taktmotor5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8347
- /detailsfa21.html
imported:
- "2019"
_4images_image_id: "8347"
_4images_cat_id: "768"
_4images_user_id: "502"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8347 -->
