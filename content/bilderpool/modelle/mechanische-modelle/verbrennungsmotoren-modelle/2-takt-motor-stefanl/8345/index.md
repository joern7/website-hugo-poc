---
layout: "image"
title: "2-Takt-Motor 3"
date: "2007-01-08T17:16:10"
picture: "taktmotor3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8345
- /detailsaf47.html
imported:
- "2019"
_4images_image_id: "8345"
_4images_cat_id: "768"
_4images_user_id: "502"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8345 -->
