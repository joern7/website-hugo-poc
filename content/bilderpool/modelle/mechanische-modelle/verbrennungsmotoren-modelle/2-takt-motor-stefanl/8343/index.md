---
layout: "image"
title: "2-Takt-Motor 1"
date: "2007-01-08T17:16:10"
picture: "taktmotor1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8343
- /details4762.html
imported:
- "2019"
_4images_image_id: "8343"
_4images_cat_id: "768"
_4images_user_id: "502"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8343 -->
