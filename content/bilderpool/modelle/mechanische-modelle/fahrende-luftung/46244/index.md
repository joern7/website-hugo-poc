---
layout: "image"
title: "Programm"
date: "2017-09-17T18:02:22"
picture: "fahrendelueftung1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46244
- /details533c.html
imported:
- "2019"
_4images_image_id: "46244"
_4images_cat_id: "3431"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46244 -->
Wenn eine Lichtschranke unterbrochen oder ein taster gedrückt wird fährt die Lüftung wieder in die andere Richtung .