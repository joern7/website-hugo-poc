---
layout: "image"
title: "Kartenmischer"
date: "2007-06-03T11:06:52"
picture: "kartenmischer1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10664
- /detailsc90c.html
imported:
- "2019"
_4images_image_id: "10664"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-03T11:06:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10664 -->
von oben.In der Mitte sind zwei 3*2-Platten an einem 60°-Winkel mit drei Rinnen angebracht.