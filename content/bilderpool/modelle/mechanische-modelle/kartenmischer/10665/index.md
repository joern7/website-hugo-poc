---
layout: "image"
title: "Kartenmischer"
date: "2007-06-03T11:06:52"
picture: "kartenmischer2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10665
- /detailsbd7b.html
imported:
- "2019"
_4images_image_id: "10665"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-03T11:06:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10665 -->
Der Karteneinzug, hier legt man die Karten rein