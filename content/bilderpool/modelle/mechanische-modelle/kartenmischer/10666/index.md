---
layout: "image"
title: "Kartenmischer"
date: "2007-06-03T11:06:52"
picture: "kartenmischer3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10666
- /detailse106-2.html
imported:
- "2019"
_4images_image_id: "10666"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-03T11:06:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10666 -->
Hier die bewegliche aufhängung, damit das rad immr an den Karten anliegt