---
layout: "image"
title: "Kartenmischer"
date: "2007-06-04T17:06:47"
picture: "v1.jpg"
weight: "7"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10712
- /details418e.html
imported:
- "2019"
_4images_image_id: "10712"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-04T17:06:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10712 -->
neue Version.Einzugsschächte zu sehen