---
layout: "image"
title: "Zoetrop - Innenansicht"
date: "2017-03-18T19:27:39"
picture: "Zoetrop_03.jpeg"
weight: "3"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Zoetrop", "Film", "Optik", "Geschichte"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/45559
- /details41ac.html
imported:
- "2019"
_4images_image_id: "45559"
_4images_cat_id: "3386"
_4images_user_id: "2179"
_4images_image_date: "2017-03-18T19:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45559 -->
Die Verstrebung wurde ähnlich wie bei einem Riesenrad verbaut. Ich glaube so hat man das schon mehrmals hier in dem Forum gesehen.