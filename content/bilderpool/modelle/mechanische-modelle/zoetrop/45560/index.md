---
layout: "image"
title: "Zoetrop - Lager"
date: "2017-03-18T19:27:39"
picture: "Zoetrop_04.jpeg"
weight: "4"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Zoetrop", "Film", "Optik", "Geschichte"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/45560
- /details2a4a-2.html
imported:
- "2019"
_4images_image_id: "45560"
_4images_cat_id: "3386"
_4images_user_id: "2179"
_4images_image_date: "2017-03-18T19:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45560 -->
Die obere Trommel wurde recht simpel auf mehreren dieser Rollen gelagert. Dadurch dreht sie sich auf dem unteren Ring recht leicht.