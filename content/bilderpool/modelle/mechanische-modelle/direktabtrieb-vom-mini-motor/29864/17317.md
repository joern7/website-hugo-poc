---
layout: "comment"
hidden: true
title: "17317"
date: "2012-10-01T13:05:46"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hm ... Ihr meint also, das bei längerem Betrieb unter Last die Seitenkräfte auf die Motorwelle über die Zahnräderflanken zu groß werden und die Welle bricht? :-O

Das habe ich natürlich nicht ausprobiert. Mein Modell ist ja nur eine Prinzipdarstellung. Aber wenn dem so ist, dann sollte vor dem Nachbau gewarnt werden!

Gruß, Thomas