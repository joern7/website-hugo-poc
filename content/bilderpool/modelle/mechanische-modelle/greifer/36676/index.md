---
layout: "image"
title: "Greifer (2) Hand"
date: "2013-02-24T16:00:37"
picture: "DSCN5036.jpg"
weight: "38"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36676
- /details8f8a-4.html
imported:
- "2019"
_4images_image_id: "36676"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-24T16:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36676 -->
Ich habe hier versucht eine Hand mit Daumen und drei Fingern zu bauen.
Der mittlere Finger läuft zusammen mit dem Daumen.
Die beiden äußeren werden separat gesteuert.
Wie kann ich noch nicht sagen .....