---
layout: "image"
title: "Mittlerer Fingen und Daumen geschlossen."
date: "2013-02-24T16:00:37"
picture: "DSCN5037.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36678
- /detailsa6ea.html
imported:
- "2019"
_4images_image_id: "36678"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-24T16:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36678 -->
