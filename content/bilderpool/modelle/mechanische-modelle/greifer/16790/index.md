---
layout: "image"
title: "'6-Finger-Greifer' pneumatisch"
date: "2008-12-30T16:07:44"
picture: "DSCN2603.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16790
- /details8518-2.html
imported:
- "2019"
_4images_image_id: "16790"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16790 -->
Das Prunkstück
Leider funktioniert es nicht.
Die Kompressoren liefern nicht genug Druckluft...
Sieht doch gigantisch gut aus, - oder?