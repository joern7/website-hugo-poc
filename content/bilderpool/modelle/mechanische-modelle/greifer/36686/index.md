---
layout: "image"
title: "Greifer (2) Hand"
date: "2013-02-24T16:00:37"
picture: "DSCN5048.jpg"
weight: "48"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36686
- /detailsac4e.html
imported:
- "2019"
_4images_image_id: "36686"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-24T16:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36686 -->
Sicht von hinten.