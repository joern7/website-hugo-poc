---
layout: "image"
title: "Gelenke Seitenansicht"
date: "2014-09-27T18:57:33"
picture: "hebegreifer3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/39369
- /detailsa713-2.html
imported:
- "2019"
_4images_image_id: "39369"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39369 -->
Der Greifer selber ist mehr oder weniger nach dem standard fischertechnik Muster gebaut, ausser der Befestigung. Unter dem Lagerungsgelenk des Greifers ist ein Baustein 5 15x30, auf welchen der Greifer aufliegt wenn er geöffnet ist, ansonsten würde er einfach so weit nach unten drehen, wie möglich.