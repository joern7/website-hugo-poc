---
layout: "image"
title: "Hebegreifer inmitten des Vorgangs"
date: "2014-09-27T18:57:33"
picture: "hebegreifer6.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/39372
- /detailsfe29.html
imported:
- "2019"
_4images_image_id: "39372"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:33"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39372 -->
Auf diesem Bild ist nun auch die Befestigung der Bauplatte 15x90 sichtbar. Die beiden Z30 sollten sich etwa gegenüber stehen, damit die Achse nicht schief ist. Natürlich könnten grössere Wege zum Beispiel mit einem Innenzahnrad Z30 erreicht werden.