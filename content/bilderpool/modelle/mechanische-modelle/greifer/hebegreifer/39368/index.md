---
layout: "image"
title: "Nahansicht Gelenke"
date: "2014-09-27T18:57:33"
picture: "hebegreifer2.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/39368
- /detailsff3a.html
imported:
- "2019"
_4images_image_id: "39368"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39368 -->
Der Greifer ist auf einem Gelenk gelagert, so dass er angehoben werden kann. Die Bauplatte 15x90 6Z ist auch über ein Gelenk am Greifer verbunden, um die Drehbewegung in eine (beinahe) lineare Bewegung umzuwandeln.