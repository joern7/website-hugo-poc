---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:42"
picture: "DSCN5093.jpg"
weight: "49"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36696
- /detailsd631.html
imported:
- "2019"
_4images_image_id: "36696"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36696 -->
Hier die neu überarbeitete Version V2.0.
Alle Finger und der Daumen werden einzeln mit Pneumatik Zylindern gesteuert.
Die Bauhöhe hat sich um 20mm verringert, dadurch liegen die Finger dichter zusammen.