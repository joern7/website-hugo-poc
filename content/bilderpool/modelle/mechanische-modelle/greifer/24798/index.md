---
layout: "image"
title: "Taster"
date: "2009-08-14T21:37:27"
picture: "fingergreifer7.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24798
- /details5d8b-2.html
imported:
- "2019"
_4images_image_id: "24798"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24798 -->
