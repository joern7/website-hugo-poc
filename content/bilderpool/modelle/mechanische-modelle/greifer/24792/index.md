---
layout: "image"
title: "Vorderansicht"
date: "2009-08-14T21:37:27"
picture: "fingergreifer1.jpg"
weight: "26"
konstrukteure: 
- "Mr Smith"
fotografen:
- "Mr Smith"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24792
- /details2b2c.html
imported:
- "2019"
_4images_image_id: "24792"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24792 -->
Ich hab den Greifer ein wenig überarbeitet, die Greifzangen gefedert. Der Grund: Bei den Versionen von Ludger wurden die einzelnen Greifzangen entweder unterschiedlich belastet, was zu einem verziehen des Greifers führt, oder der Luftdruck war zu schwach.