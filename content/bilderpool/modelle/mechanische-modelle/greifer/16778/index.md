---
layout: "image"
title: "erster Versuch"
date: "2008-12-30T16:07:43"
picture: "DSCN2584.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16778
- /detailse529-2.html
imported:
- "2019"
_4images_image_id: "16778"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16778 -->
Hier habe ich versucht einen Greifer zu bauen der von zwei Seiten greift. Er sollte in der Lage sein Schüttgut aufzunehmen.
War aber zu klobig