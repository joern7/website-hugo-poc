---
layout: "image"
title: "Endschalter"
date: "2009-08-14T21:37:27"
picture: "fingergreifer3.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24794
- /details248e.html
imported:
- "2019"
_4images_image_id: "24794"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24794 -->
Der Endschalter, der betätigt wird wenn der Greifer vollständig geöffnet ist