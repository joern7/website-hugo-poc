---
layout: "image"
title: "'6-Finger-Greifer' pneumatisch"
date: "2008-12-30T16:07:44"
picture: "DSCN2567.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16786
- /details937d.html
imported:
- "2019"
_4images_image_id: "16786"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16786 -->
Detail "Fingerbefestigung"