---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:43"
picture: "DSCN5068.jpg"
weight: "51"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36698
- /details5601.html
imported:
- "2019"
_4images_image_id: "36698"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36698 -->
seitliche Sicht in die Mechanik.