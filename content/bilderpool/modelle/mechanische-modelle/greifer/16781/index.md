---
layout: "image"
title: "'6-Finger-Greifer' mechanisch"
date: "2008-12-30T16:07:44"
picture: "DSCN2593.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16781
- /details3d9f.html
imported:
- "2019"
_4images_image_id: "16781"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16781 -->
Nachdem der erste Versuch verunglückt ist habe ich einen nächsten gestartet.
Bei diesem mechanischen Greifer wird das Gut mit 6 "Fingern" gehalten.
Leider halten die Rastachsen nicht so gut, deswegen darf die "Ladung" nicht zu schwer sein.