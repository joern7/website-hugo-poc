---
layout: "comment"
hidden: true
title: "20079"
date: "2015-01-19T23:16:09"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Die Baugruppe hat lange in der Schublade geschlummert. Ich wollte sie nicht auseinanderbauen, weil ich dachte, das krieg ich so nicht wieder hin. Dank dem Bilderpool ist es jetzt "verewigt" und ich kann die Teile weiterverwenden.