---
layout: "image"
title: "Modell 2 und 3: Zwei Pressen"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46735
- /detailscf7d.html
imported:
- "2019"
_4images_image_id: "46735"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46735 -->
Die erste, linke Presse geht sehr schnell hoch und runter, bleibt aber sehr lange unten (um den Pressdruck lange am Werkstück anliegen zu haben) und oben (um viel Zeit für den Werkstückwechsel zu bieten).

Die zweite, rechte Presse setzt noch einen drauf: Sie geht ganz normal herunter, bis etwas nach dem unteren Totpunkt ihres Exzenters. Danach geht sie ein Stückchen (ca. 60° am Exzenter) *zurück*, presst also *nochmal* auf das Werkstück, und geht dann wieder vor (presst also ein *drittes Mal* aufs Werkstück), um schließlich wieder nach oben zu fahren. Diese Presse presst also nach der ersten Pressung noch zwei Mal nach - mit einem ganz simplen Getriebe.