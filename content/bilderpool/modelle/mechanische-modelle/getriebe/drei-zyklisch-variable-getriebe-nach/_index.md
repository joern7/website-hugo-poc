---
layout: "overview"
title: "Drei zyklisch variable Getriebe nach Klopmeier"
date: 2020-02-22T08:19:44+01:00
legacy_id:
- /php/categories/3461
- /categoriesd827.html
- /categories6f7b.html
- /categories3d2e.html
- /categoriesa5e2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3461 --> 
Dieses Modell stand auf der Convention 2017 in Dreieich und stellt drei Varianten einfach aufgebauter ungleichförmig übersetzender Getriebe in Anwendung vor.