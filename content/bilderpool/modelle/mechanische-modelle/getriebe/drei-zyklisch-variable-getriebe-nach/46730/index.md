---
layout: "image"
title: "Modell 1: Ölförderpumpe"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46730
- /details4208.html
imported:
- "2019"
_4images_image_id: "46730"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46730 -->
Solche Pumpen mag der eine oder andere schon mal wo in Betrieb gesehen haben. Ein Exzenter treibt über zwei Hebel eine Auf-/Abwärtsbewegung der Pumpe an. Die Aufwärtsbewegung beim Pumpen darf nicht zu schnell erfolgen, weil die Pumpe sonst Luft anstatt Öl ziehen würde. Bei einem normalen Exzenter ginge dann aber die unproduktive Abwärtsbewegung nur genau so schnell.

Mit einem Klopmeier-Getriebe kann die Aufwärtsbewegung auf weiten Strecken nahe an der maximal möglichen Fördergeschwindigkeit gehalten werden, die Abwärtsbewegung aber schneller vonstattengehen. Dadurch erhöht sich die Effizienz der Pumpe enorm, und der Ölscheich steigert seinen Reibach ;-)