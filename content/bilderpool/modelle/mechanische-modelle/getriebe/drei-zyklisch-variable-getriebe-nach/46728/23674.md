---
layout: "comment"
hidden: true
title: "23674"
date: "2017-10-03T14:38:13"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Oh, das ging auch vorher schon ohne: http://www.ftcommunity.de/categories.php?cat_id=1374 Diese Varianten brauchen aber keine zwei der großen, schwergängigen Drehkränze und sind deshalb viel leichtgängiger  Auch braucht man keine Innenzahnräder, und man kann sie auch mit Z40 anstatt Z30 bauen.

Hat jemand mal wieder was von Herrn Klopmeier gehört? Ich hoffe, doch dass er sich mit seiner genialen Idee eine goldene Nase verdient hat?

Gruß,
Stefan