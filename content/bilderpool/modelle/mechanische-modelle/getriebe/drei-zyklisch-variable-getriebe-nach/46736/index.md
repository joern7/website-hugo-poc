---
layout: "image"
title: "Getriebe der ersten Presse"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46736
- /details83ab.html
imported:
- "2019"
_4images_image_id: "46736"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46736 -->
Dieses Getriebe ist ganz ähnlich aufgebaut wie das der Ölförderpumpe, nur dass das letzte Zahnrad ein Z20 anstatt ein Z10 ist. Das bewirkt, dass die ungleichförmige Bewegung (wie bei der Ölpumpe) auf zwei Halbzyklen erfolgt: Die Presse geht also zwei Hälften der Umdrehung des Exzenters vorne schnell, aber auch zwei Mal pro Umdrehung ganz langsam. Richtig justiert, geht deshalb die Presse sehr schnell herunter, bleibt dort lange (zum langen Pressen), geht dann sehr schnell hoch und bleibt auch dort lange (zum Werkstückwechsel). Die unproduktiven Auf-/Ab-Bewegungen aber gehen zeitsparend schnell vor sich.