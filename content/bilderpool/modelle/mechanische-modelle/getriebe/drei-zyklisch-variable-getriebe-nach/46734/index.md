---
layout: "image"
title: "Gemeinsamer Antrieb der beiden Pressen"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46734
- /detailsaba4.html
imported:
- "2019"
_4images_image_id: "46734"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46734 -->
Über die Mimik von den Winkelzahnrädern bis zur langen Kette werden die beiden Pressen angetrieben. Man beachte, dass die hier rechte (von vorne gesehen also linke) über ihr Z20 doppelt so schnell angetrieben wird wie die letzte über ihr Z40.