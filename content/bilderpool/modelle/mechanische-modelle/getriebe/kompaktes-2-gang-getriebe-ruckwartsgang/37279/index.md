---
layout: "image"
title: "Kompaktes 2-Gang-Getriebe + Rückwärtsgang (1)"
date: "2013-08-27T23:15:00"
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "lukas99h. / Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37279
- /details4da4.html
imported:
- "2019"
_4images_image_id: "37279"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37279 -->
Dieses Getriebe ähnelt der Anfangsversion 4, aber hier sind die BS15-Loch stabiler. Siehe nächstes Bild.
Gebaut von lukas99h. und phil