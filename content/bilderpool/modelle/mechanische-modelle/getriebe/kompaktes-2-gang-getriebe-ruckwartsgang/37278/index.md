---
layout: "image"
title: "Anfänge (4)"
date: "2013-08-27T23:15:00"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "lukas99h. / Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37278
- /detailsba2e.html
imported:
- "2019"
_4images_image_id: "37278"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37278 -->
Etwas anders.
Gebaut von lukas99h. und phil