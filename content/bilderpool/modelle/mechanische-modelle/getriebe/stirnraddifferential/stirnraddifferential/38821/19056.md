---
layout: "comment"
hidden: true
title: "19056"
date: "2014-05-18T21:43:00"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Stark! Das ist mal so ein richtiger "Was-bin-ich"-Klapperatismus. OK, die Bildüberschrift hat die Lösung ja schon verraten, aber bis man sich im Klaren darüber ist, wieso das jetzt ein Differenzial ist...

Etwas kompakter (ich denke mal in Richtung Gleichlaufgetriebe) geht das mit vier Klemm-Z15. Zwei BS15 in benachbarte Nuten der Drehscheibe einstecken und die Achsen durch deren Längsnut (und mit überschüssiger Länge durch die Schlitze der Drehscheibe) führen. Als Verfeinerung und zur Verwirrung des Betrachters kann man die mittleren Z15-Paare noch durch die "alten" ft-Differenziale ersetzen :-)

Gruß,
Harald