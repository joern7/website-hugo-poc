---
layout: "overview"
title: "Stirnraddifferential"
date: 2020-02-22T08:19:34+01:00
legacy_id:
- /php/categories/2900
- /categoriesd17d-2.html
- /categories5902.html
- /categories2de1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2900 --> 
Der geniale Entwurf von MickyW hat mich auf die Idee dieses Stirnraddifferentials gebracht.
Es ist stabil und lediglich etwas breiter als das [Selbstbaudifferential](http://www.ftcommunity.de/categories.php?cat_id=2873).