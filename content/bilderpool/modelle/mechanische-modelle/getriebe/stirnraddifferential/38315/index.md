---
layout: "image"
title: "Stirnraddifferential von der Seite"
date: "2014-02-21T12:11:49"
picture: "Stirnraddifferential_2_bearbeitet_klein.jpg"
weight: "2"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38315
- /details2f29-2.html
imported:
- "2019"
_4images_image_id: "38315"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-02-21T12:11:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38315 -->
Man muss auch ganz klar sagen, daß die Lagerung der oberen Achsen (wie üblich bei dieser Art und Weise) sehr schlabbrig ist.
Leider wirklich eine Kaulquappe und keine Prinzessin.