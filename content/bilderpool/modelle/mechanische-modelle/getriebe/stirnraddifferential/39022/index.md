---
layout: "image"
title: "Stirnraddifferential - rotationssymmetrisch - Z10"
date: "2014-07-21T20:31:28"
picture: "Stirnraddifferential_V6_Bild_1_bearbeitet.jpg"
weight: "5"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Stirnraddifferential", "rotationssymmetrisch"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39022
- /details8823.html
imported:
- "2019"
_4images_image_id: "39022"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-07-21T20:31:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39022 -->
Ich habe im März noch weitere Varianten gebaut.
Vor allem mit dem Ziel rotationssymmetrische und leichtgängige Formen zu entwicklen.
Die Grösse spielt bei mir nicht so sehr die Rolle, da diese Formen ohnehin für stationäre Modelle vorgesehen sind.

Dieses hier, ist also rotationssymmetrisch und daher nicht mehr unwuchtig - mit Klemm-Z10.