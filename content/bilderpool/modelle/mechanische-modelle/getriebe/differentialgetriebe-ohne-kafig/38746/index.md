---
layout: "image"
title: "Symmetrische Variante - Komplett"
date: "2014-05-05T23:06:56"
picture: "DifferentialSymmetrisch1.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Differential", "Getriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38746
- /detailsb256.html
imported:
- "2019"
_4images_image_id: "38746"
_4images_cat_id: "2895"
_4images_user_id: "1088"
_4images_image_date: "2014-05-05T23:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38746 -->
Hier das komplette Getriebe. Die Drehwinkel x,y der beiden Zahnräder Z40 und z der zentralen Achse erfüllen die Gleichung x + y = 2z. Eine genaue Begründung dafür erfolgt an geeigneter Stelle....