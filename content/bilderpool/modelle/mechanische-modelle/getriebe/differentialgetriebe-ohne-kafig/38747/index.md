---
layout: "image"
title: "Asymmetische Variante"
date: "2014-05-05T23:06:56"
picture: "DifferentialAsymmetrisch1.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Differential", "Getriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38747
- /details5f15.html
imported:
- "2019"
_4images_image_id: "38747"
_4images_cat_id: "2895"
_4images_user_id: "1088"
_4images_image_date: "2014-05-05T23:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38747 -->
Die asymmetrische Variante ist noch leichtgängiger und lässt sich in manchen Anwendungssituationen leichter lagern, da keine Rastachsen zum Einsatz kommen. Das Z40 auf der linken Seite ist fest mit der Achse verbunden, während sich das Z40 rechts mit einer Freilaufnabe um die Kunststoffachse mit Vierkant dreht. Die Drehwinkel x des Z40 links, y des Z40 rechts und z der Kunststoffachse mit Vierkant erfüllen wieder die Gleichung x + y = 2z.