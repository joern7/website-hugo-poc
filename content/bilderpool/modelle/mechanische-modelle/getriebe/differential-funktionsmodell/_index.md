---
layout: "overview"
title: "Differential (Funktionsmodell)"
date: 2020-02-22T08:19:37+01:00
legacy_id:
- /php/categories/2873
- /categoriesed9b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2873 --> 
Angeregt durch die "Monster-Differentiale" von Ludger haben wir ein Selbstbau-Differential konstruiert, das sich als Funktionsmodell eignet, aber auch in einem Fahrzeug eingesetzt werden kann - die Bauhöhe durfte daher zumindest den derzeit größten ft-Reifendurchmesser nicht übersteigen.