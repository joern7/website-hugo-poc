---
layout: "image"
title: "Kern des Getriebes: 4 Differenziale"
date: "2016-05-22T18:55:30"
picture: "pic1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/43407
- /details35d0-2.html
imported:
- "2019"
_4images_image_id: "43407"
_4images_cat_id: "3224"
_4images_user_id: "1729"
_4images_image_date: "2016-05-22T18:55:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43407 -->
Wer hat Lust, mitzuraten?
Wenn sich ein paar Interessenten finden, würde ich nach und nach Tipps und Bilder - entsprechend meinem Baufortschritt - posten. 
Es geht um die Frage, was dieses Getriebe für einen Sinn hat und was damit angetrieben werden soll.
Achtung: Ich hab es bisher noch nicht ausprobiert, ob dieses Getriebe wirklich funkioniert! Nur theoretisch durchdacht. Ich denke aber, es müsste funktionieren. Am Ende könnte es dann trotzdem heissen: "Ätsch...funktioniert nicht ;)