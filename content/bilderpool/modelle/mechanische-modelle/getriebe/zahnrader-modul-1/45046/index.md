---
layout: "image"
title: "Zahnräder Modul 1"
date: "2017-01-16T17:13:13"
picture: "Zahnrder_Modul_1.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Zahnrad", "Modul1", "m1", "Eitech"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45046
- /details4091-2.html
imported:
- "2019"
_4images_image_id: "45046"
_4images_cat_id: "3355"
_4images_user_id: "2635"
_4images_image_date: "2017-01-16T17:13:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45046 -->
Hallo Leute,
wozu hat die Rastkurbel 35071 ein Zahnrad Modul 1?
Um sie mit Eitech-Zahnrädern zu kombinieren? Die passen genau ins ft-Raster.