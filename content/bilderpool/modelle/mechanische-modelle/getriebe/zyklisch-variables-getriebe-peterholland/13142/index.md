---
layout: "image"
title: "Mit Kugellager und  Zahnrad-Z15 (35695) wie im Modell Wilhelm Klopmeier"
date: "2007-12-23T11:06:09"
picture: "Mit_kugellager_001.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13142
- /details2c00-2.html
imported:
- "2019"
_4images_image_id: "13142"
_4images_cat_id: "1185"
_4images_user_id: "22"
_4images_image_date: "2007-12-23T11:06:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13142 -->
Jetzt mit Lagerung, direkt montiert auf das Innenzahnrad Z30 (35694) mit Z15 Innen-Zahnrad.

Lagerungtype     : 1-rijig diepgroef kogellager - 6810 ZZ - 50x65x7 mm. (dxDxB).

Jetzt auch mit semi FT-Drehkrans mit Kugellager hergestellt durch Andreas Tacke.