---
layout: "image"
title: "Gesamtansicht"
date: "2008-08-24T09:35:03"
picture: "klopmeiergetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15077
- /details3944.html
imported:
- "2019"
_4images_image_id: "15077"
_4images_cat_id: "1374"
_4images_user_id: "104"
_4images_image_date: "2008-08-24T09:35:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15077 -->
Das Ganze ist sehr stabil aufzubauen. Bei der Abtriebswelle darf man es aber nicht übertreiben - die muss immer noch leichtgängig bleiben. Die zweite Grundplatte ist mit 12 BS 15 mit zwei Zapfen mit der ersten verbunden und dient der Versteifung. Ansonsten verwindet sich die Grundplatte durch die auftretenden Kräfte zu stark und das Getriebe läuft nicht mehr so glatt. Unterhalb der linken Drehscheibe sitzt ein PowerMotor und treibt deren schwarzes Zahnrad an. Exzentrisch darauf angebracht (um 15 mm außerhalb der Mitte) sitzt eine zweite Drehscheibe, auf der mittels 4 Strebenadapter ein Innenzahnrad angebracht ist. Das treibt das Z10 für die Abtriebswelle an. Der schwenkbare Balken hält einen Punkt des Innenzahnrads über eine Metallachse 30 fest. Erst dadurch funktioniert das Ganze.