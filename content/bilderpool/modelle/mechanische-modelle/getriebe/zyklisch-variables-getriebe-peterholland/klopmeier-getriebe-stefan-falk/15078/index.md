---
layout: "image"
title: "Detailansicht"
date: "2008-08-24T09:35:04"
picture: "klopmeiergetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15078
- /detailsd4b2.html
imported:
- "2019"
_4images_image_id: "15078"
_4images_cat_id: "1374"
_4images_user_id: "104"
_4images_image_date: "2008-08-24T09:35:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15078 -->
Der PowerMotor sitzt um eine Platte 15x30 unterhalb der Winkelstücke nach oben versetzt, damit er sauber ins Zahnrad eingreift. Es macht viel Spaß, das Getriebe beim Laufen zu betrachten und zu studieren.