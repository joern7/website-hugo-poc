---
layout: "overview"
title: "Klopmeier-Getriebe (Stefan Falk)"
date: 2020-02-22T08:19:11+01:00
legacy_id:
- /php/categories/1374
- /categories0b3a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1374 --> 
Hier habe ich auch mal versucht, ein Getriebe wie es von Herrn Wilhelm Klopmeier erfunden wurde, nachzubauen. Eine gleichförmige Drehbewegung wird in eine ungleichförmige übersetzt. Diese Variante verwendet relativ wenige Teile und zeigt das Prinzip. Die Drehgeschwindigkeit der Abtriebswelle geht von fast Null bis ca. drei Mal so schnell wie die Antriebswelle. Ein Video habe ich auch gemacht.