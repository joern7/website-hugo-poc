---
layout: "comment"
hidden: true
title: "4763"
date: "2007-12-15T12:29:50"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Ölpumpe (Pferdekopfpumpe) mit gleichmäßigem, nicht sinusförmigen Fördervolumen. Die Vermeidung von Förderspitzen ermöglicht eine generelle Erhöhung des Förderkapazität (da die maximalen Werte nie größer sein dürfen als das Nachfüllfähigkeit der Quelle).