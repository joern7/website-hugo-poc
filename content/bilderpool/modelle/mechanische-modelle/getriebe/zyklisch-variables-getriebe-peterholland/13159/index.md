---
layout: "image"
title: "Exzenterantrieb mit zyklisch variablem Getriebe"
date: "2007-12-26T18:26:57"
picture: "Schmiedepresse_mit_2x_kugellagerung_zyklisch_variablem_Getriebe_003.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13159
- /details4341-2.html
imported:
- "2019"
_4images_image_id: "13159"
_4images_cat_id: "1185"
_4images_user_id: "22"
_4images_image_date: "2007-12-26T18:26:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13159 -->
Exzenterantrieb mit zyklisch variablem Getriebe :
&#9679; ausgeprägte Rast im oberen Totpunkt
&#9679; kurze Druckberührzeit
&#9679; schneller Rückhub

Ich habe noch ein wenig gebastelt mit die Drehscheibe mit Kugellagerung (Entwickelung Andreas Tacke), das Innenzahnrad Z30 (35694) und Zahnrad Z15.

Es gibt jetzt mit dem Zahnrad Z15 ein optimiertes Exzenterantrieb mit zyklisch variablem Getriebe wie beim Erfinder Wilhelm Klopmeier : 

http://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/infovarpressenantriebe.pdf


Gruss, 

Peter Damen 
Poederoyen NL