---
layout: "image"
title: "Minimal viele Bauteile"
date: "2009-04-14T22:21:39"
picture: "freilaufgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23718
- /details4091.html
imported:
- "2019"
_4images_image_id: "23718"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T22:21:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23718 -->
Die Drehscheibe sitzt auf einer Freilaufnabe, ersatzweise auf einer nur schwach angezogenen Flachnabe. Das Z20 sitzt fest auf der Achse. Außerdem braucht man nur einen Verbinder 15 (mit einer flachen Seite), einen Baustein 7,5 und - das ist allerdings leider selten geworden - einen Federkontakt aus dem 1970er Elektromechanik-Programm von fischertechnik.

Vorteile:
- Sehr wenige Bauteile.
- Durch das Z20 relativ feine Auflösung (alle 18° rastet der Freilauf ein).

Nachteile:
- Recht schwergängig, weil der Federkontakt stark drückt.
- Der BS7,5 muss unbedingt noch weiter fixiert werden, sondern wird der leicht aus der Nut geschoben.
- Der Federkontakt wird nicht mehr hergestellt (man bekommt ihn aber noch gebraucht).
- Benötigt etwas mehr freien Umfang als nur die Drehscheibe.