---
layout: "image"
title: "Ansicht von oben"
date: "2009-04-14T23:33:26"
picture: "freilaufgetriebeb2.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23725
- /detailsb89d.html
imported:
- "2019"
_4images_image_id: "23725"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T23:33:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23725 -->
Die Achse ist entweder Antrieb und der Baustein 30 mit Loch mit seinen Winkelsteinen der Abtrieb, oder umgekehrt - wie's beliebt.