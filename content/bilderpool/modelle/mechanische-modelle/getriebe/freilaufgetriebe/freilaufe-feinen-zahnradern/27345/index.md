---
layout: "image"
title: "Freiläufe mit feinen Zahnrädern"
date: "2010-06-03T12:49:57"
picture: "freilaeufemitfeinenzahnraedern01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27345
- /details32eb.html
imported:
- "2019"
_4images_image_id: "27345"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27345 -->
Hier noch einige Vorschläge für Freilaufgetriebe, die mit Zahnrädern mit feiner Verzahnung funktionieren. Das hat Vorteile:

- Baut potentiell kompakter.
- Reagiert durch die vielen Zähne feiner.

Und Nachteile:

- Die feinen Zähne können nicht so viel ab wie die groben. Deshalb ist das übertragbare Drehmoment potentiell kleiner und der Verschleiß der Zähne potentiell größer.