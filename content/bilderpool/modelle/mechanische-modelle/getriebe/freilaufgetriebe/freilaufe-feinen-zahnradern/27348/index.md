---
layout: "image"
title: "Variante 2"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27348
- /details37ce.html
imported:
- "2019"
_4images_image_id: "27348"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27348 -->
Es geht noch etwas kleiner: Hier wird die MiniMot-Getriebeachse verwendet. Wieder dient ein Reifen-45-Gummi als das federnde Element. Damit das richtig spannt, dienen lange Klemmbuchsen als Abstandshalter. Die kann man mit etwas Mühe tatsächlich auf die Zapfen der Gelenkbausteine bringen. Ein BS5 oder eine Platte 15 * 15 mit Nut scheidet aus, weil dann die dreieckige Form verloren ginge, die in die Zähne einrasten muss.

Vorteile:

- Noch etwas leichterer Lauf
- Ordentlich Drehmoment