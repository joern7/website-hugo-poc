---
layout: "image"
title: "Variante 5"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27352
- /details8661.html
imported:
- "2019"
_4images_image_id: "27352"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27352 -->
Hier habe ich mal versucht, ohne einen BS30 auszukommen. Die Hoffnung war, noch schmaler zu bauen. Tatsächlich ist diese Baugruppe aber größer als die mit dem BS30.

Auf jeder Seite sind je ein Winkelstein 15° und ein 7,5° entgegengesetzt angebracht. Federndes Element ist wieder der Verbinder 30. Sieh auch das nächste Bild.