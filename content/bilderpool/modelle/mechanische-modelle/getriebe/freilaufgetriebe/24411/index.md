---
layout: "image"
title: "Noch kleiner und noch leichtgängiger (2)"
date: "2009-06-17T23:54:14"
picture: "freilaufnochkleinerundleichtgaengiger5.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24411
- /detailsd95b.html
imported:
- "2019"
_4images_image_id: "24411"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24411 -->
Die Platte wird nur extrem leicht angedrückt, aber die Geometrie sorgt dafür, dass sich der in der Platte befindliche Verbinder 15 richtig in das Z10 eingräbt.