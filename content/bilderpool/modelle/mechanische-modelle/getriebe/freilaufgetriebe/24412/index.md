---
layout: "image"
title: "Noch kleiner und noch leichtgängiger (3)"
date: "2009-06-17T23:54:14"
picture: "freilaufnochkleinerundleichtgaengiger6.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24412
- /details4cb4.html
imported:
- "2019"
_4images_image_id: "24412"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24412 -->
Die Justage ist weniger schlimm als es vielleicht aussieht: Der BS7,5 mit der Achse steckt praktisch genau am Ende des BS30 mit Loch, der BS5 mit dem Federkontakt ganz leicht draußen.