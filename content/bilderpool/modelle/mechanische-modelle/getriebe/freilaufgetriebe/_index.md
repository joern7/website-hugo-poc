---
layout: "overview"
title: "Freilaufgetriebe"
date: 2020-02-22T08:19:18+01:00
legacy_id:
- /php/categories/1620
- /categoriesdbda-2.html
- /categories6cb4.html
- /categoriesf049.html
- /categoriesbec8.html
- /categoriesa47b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1620 --> 
Hier ein paar Varianten, wie man Freilaufgetriebe (Getriebe, die Drehmoment nur in eine Richtung übertragen, in die andere aber frei laufen) bauen kann.