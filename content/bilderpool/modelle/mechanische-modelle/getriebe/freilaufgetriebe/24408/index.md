---
layout: "image"
title: "Kleiner und leichtgängiger (2)"
date: "2009-06-17T23:54:13"
picture: "freilaufnochkleinerundleichtgaengiger2.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24408
- /detailsc8c9.html
imported:
- "2019"
_4images_image_id: "24408"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24408 -->
Das Teil kann durchaus nennenswert Drehmoment übertragen, auch wenn es auf den ersten Blick nicht danach aussieht.