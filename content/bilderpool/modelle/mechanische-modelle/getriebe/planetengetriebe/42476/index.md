---
layout: "image"
title: "Übersetzungsverhältnis 5:2 mit Planetengetriebe"
date: "2015-11-29T16:51:42"
picture: "Uebersetzung5zu2.jpg"
weight: "4"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/42476
- /detailsf657.html
imported:
- "2019"
_4images_image_id: "42476"
_4images_cat_id: "2901"
_4images_user_id: "1088"
_4images_image_date: "2015-11-29T16:51:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42476 -->
Mit Planetengetrieben gelingt die Umsetzung bestimmter Übersetzungsverhältnisse oft kompakter und leichtgängiger als mit Differentialen. Ein Beispiel dafür ist die Übersetzung von 10:1 in meiner Rechenmaschine. Hier ist ein weiteres Beispiel. Die schwarzen Z15 werden arretiert, drehen sich also nicht. Wenn die Metallachse 5 Umdrehungen macht, macht die rote Achse mit Vierkant 2 Umdrehungen.