---
layout: "image"
title: "Mit abgezogenen zentralen Achsen."
date: "2014-05-22T14:33:36"
picture: "Planeten2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Planetengetriebe", "Mini-Zahnräder"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38831
- /detailsd7e5-2.html
imported:
- "2019"
_4images_image_id: "38831"
_4images_cat_id: "2901"
_4images_user_id: "1088"
_4images_image_date: "2014-05-22T14:33:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38831 -->
