---
layout: "image"
title: "Gesamtansicht (1)"
date: "2008-08-27T23:17:55"
picture: "harmonicdrive1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15116
- /detailsc6f7.html
imported:
- "2019"
_4images_image_id: "15116"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-27T23:17:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15116 -->
Dies ist ein ganz gut funktionierendes Harmonic Drive nur aus unmodifizierten ft-Teilen gebaut (allerdings sind einige ältere Teile verbaut, die nicht mehr hergestellt werden).

Ein Harmonic Drive ist normalerweise mit einem Innenzahnrad aufgebaut, aber es ist mir bis dato nicht gelungen, die notwendigen Teile alle innerhalb eines ft-Innenzahnrads funktionierend unterzubringen. Es geht aber genauso gut auch mit einem normalen Zahnrad.

Ein paar Links zur Funktionsweise:
http://de.wikipedia.org/wiki/Harmonic-Drive-Getriebe
http://en.wikipedia.org/wiki/Harmonic_drive
http://www.harmonicdrive.de/contenido/cms/front_content.php?idart=51&idcat=21
http://www.harmonicdrive.de/contenido/cms/front_content.php?idart=52
http://www.harmonicdrive.de/contenido/cms/front_content.php?idart=53

Folgende evtl. nicht mehr so einfach zur Verfügung stehenden Teile wurden verbaut:
2 Unterbrecherstücke doppelt (alte Elektromechanik)
6 Federstäbe (alte Elektromechanik)
Förderkettenglieder (12 Stück würden ausreichen)