---
layout: "comment"
hidden: true
title: "6970"
date: "2008-08-28T16:45:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hut ab!

An so ein Getriebe und die Kette hatte ich auch schon mal gedacht, aber den Mut zu einem "fliegenden" Aufbau nicht aufgebracht. Von diesem em-Teil ganz zu schweigen!

Aber stabiler machen müsste doch gehen. Wenn du nur 6 Förderglieder bestückst und da je einen BS7,5 draufsteckst, und von selbigen mit kurzen Kettenstücken (5 oder 6 Glieder) weiter auf eine Zacke am "Stern"? 

Gruß,Harald