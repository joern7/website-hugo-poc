---
layout: "image"
title: "Detailansicht (2)"
date: "2008-08-27T23:17:56"
picture: "harmonicdrive4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15119
- /details4cf9.html
imported:
- "2019"
_4images_image_id: "15119"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-27T23:17:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15119 -->
Jetzt muss die Drehbewegung der Kette nur noch abgegriffen und auf eine normale Achse übertragen werden. Da die Kette 60 Zähne lang ist, passen sechs Abnehmer wunderbar dazu. Die Abnehmer sind Federstäbe (ebenfalls aus dem alten ft-Elektromechanikprogramm). Die müssen a) die Hebebewegung mitmachen können und b) einen Rotationsausgleich zwischen den schon versetzten und den noch aufliegenden Kettensegmenten herstellen. Das gelingt gut, und heraus kommt eine hinreichend gleichmäßige, sehr langsame Drehbewegung.