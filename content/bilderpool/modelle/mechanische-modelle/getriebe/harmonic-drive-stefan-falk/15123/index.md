---
layout: "image"
title: "Service Pack 1"
date: "2008-08-28T21:12:51"
picture: "harmonicdriveservicepack2.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15123
- /detailsd5d7-3.html
imported:
- "2019"
_4images_image_id: "15123"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-28T21:12:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15123 -->
Die dritte Verbesserung ist eine leichte Hemmung der Abtriebswelle. Die hier angebrachte neue Drehscheibe ist unten (wie die auf der Antriebsseite) mit einem Verbinder 15 fixiert und gerade so weit angezogen, dass sich ein Optimum zwischen leichtem Lauf und ruhiger Drehung des "Sterns" ergibt.

Das ergibt eine sehr deutlich bessere Funktion und Gleichmäßigkeit.