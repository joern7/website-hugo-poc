---
layout: "image"
title: "hypozyk0568"
date: "2014-04-21T22:34:25"
picture: "IMG_0568mit_2.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38588
- /details93ed.html
imported:
- "2019"
_4images_image_id: "38588"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:34:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38588 -->
Die Bank ist verlängert, die erste Stufe ist komplett; die zweite gerade angefangen: auf dem Exzenter sitzen das schwarze Z40, die Drehscheibe und ein Z28, das als Kette um ein Drehschalter-Oberteil entstanden ist. Innen drin sitzen zwei Z15, die aber nur als schlanke Freilaufnaben dienen. Das Drehschalter-Oberteil ist mit zwei Stück V-Achse 4*17, 35404 (rot) an der Drehscheibe befestigt. Die Verbindungsstopfen 32316 sorgen für den richtigen Abstand des Z30 aus der nächsten Stufe.