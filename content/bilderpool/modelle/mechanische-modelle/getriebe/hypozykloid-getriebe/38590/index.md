---
layout: "image"
title: "hypozyk592.jpg"
date: "2014-04-21T22:41:37"
picture: "IMG_0592Oldham.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38590
- /details67c4.html
imported:
- "2019"
_4images_image_id: "38590"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38590 -->
Eine andere Möglichkeit, aus der ersten Stufe auszukoppeln, ist die Kreuzschieberkupplung (Oldham-Kupplung) : die dunkelrote Bauplatte 15x45 zusammen mit zwei BS5 sitzt lose in zwei rechtwinkligen Gleitführungen, kann in X- und Y-Richtung beliebig gleiten und damit das "Eiern" der mittleren Baugruppe aufheben, aber die Drehung wird übertragen.