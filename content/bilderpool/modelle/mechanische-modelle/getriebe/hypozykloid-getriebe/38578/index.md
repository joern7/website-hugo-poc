---
layout: "image"
title: "hypozyk594"
date: "2014-04-21T21:32:56"
picture: "IMG_0597.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38578
- /details0db4-3.html
imported:
- "2019"
_4images_image_id: "38578"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T21:32:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38578 -->
Das hier ist ein Hypozykloidgetriebe. Das ist ein Untersetzungsgetriebe, das mit wenig Elementen auskommt und beachtliche Übersetzungen erreicht. Der Antrieb erfolgt von links auf die schwarze Rastachse. Diese treibt eine Exzenterwelle an, auf der das Z40 und die Drehscheibe lose mitgenommen werden. Links befindet sich ein feststehendes Innenzahnrad Z42, das entstanden ist durch Umwickeln einer Drehscheibe mit einer schwarzen Kette. In die Rastaufnehmer-Stifte greift das Z40 ein. Beim Drehen des Antriebs eiert das Z40, und deshalb wandert der Eingriffspunkt des Z40 mit dem Z42 langsam im Kreis herum. Dabei rollt das Z40 auf dem Z42 ab. Dieses Abrollen, aber nicht die Eierei, soll am Ausgang abgegriffen werden. Dazu dienen die schwarzen Kettenbeläge, die von drei schwarzen Knubbeln (allesamt hier verdeckt) in der Drehscheibe mitgenommen werden.