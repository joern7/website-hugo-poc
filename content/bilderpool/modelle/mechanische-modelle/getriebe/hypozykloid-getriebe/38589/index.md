---
layout: "image"
title: "hypozyk575"
date: "2014-04-21T22:36:46"
picture: "IMG_0575.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38589
- /detailsbe78.html
imported:
- "2019"
_4images_image_id: "38589"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:36:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38589 -->
Die Teile der ersten Stufe, nebst Z28, das schon zur zweiten Stufe gehört. Der ganze Block rechts vom Z42 eiert auf dem Exzenterstück, das von hinten her kommend am Kabelbinder endet.