---
layout: "image"
title: "Centrifugaal Getriebe mit 3 Gänge und eine Freilauf"
date: "2010-01-07T08:22:38"
picture: "autotechnik09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26023
- /details80d8.html
imported:
- "2019"
_4images_image_id: "26023"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26023 -->
Detail des Centrifugaal Getriebe mit 3 Gänge und eine Freilauf