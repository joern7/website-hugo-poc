---
layout: "image"
title: "ABS-Antiblockiersystem"
date: "2010-01-07T08:22:39"
picture: "autotechnik15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26029
- /detailsbe71-2.html
imported:
- "2019"
_4images_image_id: "26029"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26029 -->
Ich habe eher ein Fischertechnik-ABS-Antiblockiersystem-Modell gebaut und ein Robopro-Programm ABS-2 geuploaded im FT-Community-downloads.
Mit eine Auslauf-Schleife (lokale Variable) über Geschwindigkeit M2 is das Modell etwas realistisch. 
In Praxis ist alles doch nicht so einfach, da auch der Brems(Luft)druck dosiert wird.

