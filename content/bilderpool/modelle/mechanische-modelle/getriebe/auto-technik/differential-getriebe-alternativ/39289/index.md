---
layout: "image"
title: "Fischertechnik + Lego-Reifen"
date: "2014-08-24T22:29:34"
picture: "differentialgetriebealternativ5.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen"
fotografen:
- "Peter Poederoyen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39289
- /detailsa9a2.html
imported:
- "2019"
_4images_image_id: "39289"
_4images_cat_id: "2939"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39289 -->
Lego-Reifen 

Innen Speichenrad 90   36916