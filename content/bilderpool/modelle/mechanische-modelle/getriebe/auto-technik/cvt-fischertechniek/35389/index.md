---
layout: "image"
title: "Fischertechnik CVT"
date: "2012-08-26T20:28:54"
picture: "cvt4.jpg"
weight: "4"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35389
- /detailsaede.html
imported:
- "2019"
_4images_image_id: "35389"
_4images_cat_id: "2625"
_4images_user_id: "22"
_4images_image_date: "2012-08-26T20:28:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35389 -->
This CVT enables to have :
.	A constant speed of the motor 
.	A infinity of variable ratio 
.	A variable speed 
.	A variable torque 
Of course, some torque are lost because of the friction pin but the torque is increased so the loss is negligible regarding the increase.
It enables a variable ratio on the output. The CVT chooses the most suitable ratio functions of the resistive torque on the output. The principle is based on two differentials which are connected side by side with a first gear : 1:1 and a second gear on the other side.  The second-gear-side has also a friction pin in order to limit the rotation.
