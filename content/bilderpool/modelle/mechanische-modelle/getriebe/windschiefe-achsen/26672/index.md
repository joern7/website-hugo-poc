---
layout: "image"
title: "Übertragung"
date: "2010-03-11T19:52:42"
picture: "InBewegung_2.jpg"
weight: "8"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26672
- /detailsf7c0-2.html
imported:
- "2019"
_4images_image_id: "26672"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26672 -->
Hier ein Foto in Bewegung. Man erkennt etwa, wie die beiden Hyperboloiden aneinander abschroten. Auch die Berührgerade sieht man halbwegs.