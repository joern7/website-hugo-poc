---
layout: "image"
title: "Auszug aus dem Buch 'Anschauliche Geometrie'"
date: "2010-03-11T19:52:42"
picture: "Hilbert.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26673
- /details23ab-3.html
imported:
- "2019"
_4images_image_id: "26673"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26673 -->
Hier eine Seite aus einem klassischen Geometriebuch von Hilbert und Cohn-Vossen zum Abschroten von Hyperboloiden. Man kann auch andere Übersetzungen als 1:1 erzielen, allerdings gleiten die Hyperboloiden dann in Richtung der Geraden, längs der sie sich berühren, aneinander ab, was Abrieb verursacht.