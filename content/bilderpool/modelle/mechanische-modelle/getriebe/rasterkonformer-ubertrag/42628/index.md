---
layout: "image"
title: "Detailblick 1"
date: "2015-12-28T19:08:43"
picture: "rasterkonformeruebertrag2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42628
- /details75c4.html
imported:
- "2019"
_4images_image_id: "42628"
_4images_cat_id: "3169"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42628 -->
Man wird das vielleicht weniger zum Zählen als für Schrittschaltwerke verwenden. Das Angenehme ist, dass alles im Raster liegt.