---
layout: "image"
title: "schräge Seitenansicht"
date: "2015-12-30T19:41:03"
picture: "getriebe3.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp"
fotografen:
- "Jens Lemkamp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42632
- /details53e9-2.html
imported:
- "2019"
_4images_image_id: "42632"
_4images_cat_id: "3170"
_4images_user_id: "1359"
_4images_image_date: "2015-12-30T19:41:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42632 -->
Seiltrommel 15  (31016) + Klemmring Z36 (31020) + Z28 (31082)

Das Z28 stammt aus dem Getriebe für den Mini-Motor XS .

Das Ganze passt herrlich ins Raster - und läuft "wie geschmiert"

ist es nun eine Kaulquappe, ein Frosch oder gar ein Prinz?