---
layout: "comment"
hidden: true
title: "21487"
date: "2016-01-01T22:55:19"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Die Drehrichtung ist hier aber m.E. wichtig. Denn falsch herum könnte die Rastnase der Seiltrommel nachgeben und den Klemmring rausdrehen, so daß keine Verbindung mehr besteht. Es geht zwar nichts kaputt, funktioniert aber dann nicht mehr.

Genau das ist mir nämlich 2010 bei meinem Karussell (Münster) passiert. Angetrieben direkt vom Minimotor (insgesamt an 4 Achsen) war die Antriebrichtung stets zwingend vorgegeben.

Gruß, Thomas