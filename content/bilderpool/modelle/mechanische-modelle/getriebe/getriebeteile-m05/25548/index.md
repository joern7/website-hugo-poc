---
layout: "image"
title: "[1/4] Getriebeteile m0,5 aktuell"
date: "2009-10-13T19:55:00"
picture: "getriebeteilem1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: ["m05"]
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/25548
- /detailsda26-3.html
imported:
- "2019"
_4images_image_id: "25548"
_4images_cat_id: "1794"
_4images_user_id: "723"
_4images_image_date: "2009-10-13T19:55:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25548 -->
Die kreative Beschäftigung mit dem Thema "ft-Getriebe in m0,5" möchte ich mit den vier folgenden Zusammenstellungen anregen. Ein Anspruch auf Vollständigkeit besteht aber noch nicht. Da im fischertechnik-Designer ein größeres Teilesortiment zu finden war als ich momentan schon besitze, habe ich mich zunächst hier für die 3D-Teile zur Darstellung entschieden. Außerdem lassen sie sich im Interesse der Informationsdichte in der Abbildung leichter "auffädeln". Es wäre schön wenn ihr hier außer mir Ergänzungen zu den Teilen bringt und bald weitere interessante Getriebelösungen folgen. Ich denke auch, dass sich unabhängig vom Wissen dazu dennoch ein breites Interesse finden könnte. Sicher werden dabei auch weitere Teile gefunden, die ursprünglich nicht als Getriebeteile gedacht oder eingeordnet waren. Und ft-Fans finden immer? Einbaulösungen für unveränderte? Teile.

Aufgeführt sind hier zeilenweise von links nach rechts (entnehmbare? Einzelteile weiss eingefärbt):

31079 Getriebhalter Schnecke mit Z10m1,5 und Z25m0,5 auf Schnecke m1,5
31063 U-Achse 60 mit Z28m0,5
31064 U-Achse 40 mit Z28m0,5
31082 Rastachse mit Z28m0,5
31078 U-Getriebe
31075 Getriebehalter Schnecke mit Z14m0,5 und Z25m0,5 auf Schnecke m1,5
137096 XS-Motor mit Schnecke m0,5
32293 Mini-Motor mit Schnecke m0,5
31020 Klemmring für Seiltrommel mit Z36m0,5
37351 Zahnstange 60 mit dreiseitiger Zahnstange 38Zm0,5
37457 Zahnstange 30 mit dreiseitiger Zahnstange 19Zm0,5 (in Einbaulage zum Zahnstangengetriebe gedreht)
37272 Zahnstangengetriebe m0,5 (Hubgetriebe)
36334 Riegelscheibe mit Z20m0,5
35113 Spannzange mit Z22m0,5
31915 Zangenmutter mit Z22m0,5
35695 Zahnrad Z15 anreihbar mit Z15m1,5 sowie innen und außen Z22m0,5
31998 Seilwindentrommel mit Z32m0,5

Eine  Aufreihung der Zähnezahlen in m0,5 (Zahnstangen in Klammern) ergibt Z14, (Z19), Z20, Z22, Z25, Z28, Z32, Z36 und (Z38)

Nachträgliche Ergänzung zu den weiss eingefärbten Teilen:

34109 Getriebeschnecke m1,5 Z25m0,5 rot
35575 Ritzel Z10m1,5 schwarz
?     Ritzel Z14m0,5 schwarz

Nachtrag 17.10.09 Berechnung des Achsabstandes:

Zugegebenermaßen ist beim ft-System mit einem Grundraster von 15mm die Realisierung des Achsabstandes oft eine Herausforderung.
Er berechnet sich für außenliegende Gegenräder mit a=m*(Z1+Z2)/2.
Beispiel mit Modul m=0,5, Z1=14 und Z2=44:  a=0,5*(14+44)/2=14,5mm
Ein erfolgreicher Weg erforderliche Achsabstände zu erreichen ist das Einstellen verschiebbarer Lager.