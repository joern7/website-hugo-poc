---
layout: "image"
title: "[4/4] Getriebteile m0,5 M-System"
date: "2009-10-13T19:55:02"
picture: "getriebeteilem4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: ["m05"]
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/25551
- /detailse040.html
imported:
- "2019"
_4images_image_id: "25551"
_4images_cat_id: "1794"
_4images_user_id: "723"
_4images_image_date: "2009-10-13T19:55:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25551 -->
Ein Thema das Wehmut? aufkommen lässt, der M-Motor 9V/5000U/min? mit seinen Getriebeteilen. Besteht doch bei vielen ft-Fans der Wunsch, dass ein mechanisch einstellbarer Getriebesatz mal wieder erhältlich wird.
Hier dazu die Zusammenstellung nach meinem vorläufigen Wissen:

Rechts oben die sieben Komponenten des M-Systems
32618 M-Motor mit Schnecke m1,5 Z25m0,5 
32619 M-Motor Getriebewinkel
Darunter die fünf Zahnradteile, die schon unter Bild [2/4] mit Art-Nr. aufgeführt sind.

Links oben der Grundaufbau M-Motor und -Getriebewinkel mit dem Schneckengetriebe 10:1 von m1,5 auf Z14m0,5 und der symmetrisch montierten Abgangsvariante 31049 M-Achse110  Z44m0,5.

Unten die drei Getriebeeinstellungen (gleichbleibende Teile weiss eingefärbt) von links nach rechts zum ein-, zwei- und dreistufigen Getriebe: schnell 32:1, mittel 77:1 und langsam 240:1

Weitere Einzelheiten dazu sind zu finden in der Bauanleitung ft30301 MASTER-MOTORMASTER, die hier in der ftC unter Downloads als pdf-file heruntergeladen werden kann.