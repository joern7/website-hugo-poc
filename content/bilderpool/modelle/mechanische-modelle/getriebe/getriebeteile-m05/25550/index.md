---
layout: "image"
title: "[3/4] Getriebeteile Übergang m0,5<=>m1,5"
date: "2009-10-13T19:55:02"
picture: "getriebeteilem3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: ["m05"]
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/25550
- /details2e41.html
imported:
- "2019"
_4images_image_id: "25550"
_4images_cat_id: "1794"
_4images_user_id: "723"
_4images_image_date: "2009-10-13T19:55:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25550 -->
Eine ausgewählte Zusammenstellung von Teilen aus Bild [1/4], die einen Modulübergang zwischen m0,5 und m1,5 im Getriebe oder an seinem Ein- bzw. Ausgang ermöglichen (entnehmbare? Einzelteile wieder weiss eingefärbt):

31079 Getriebehalter Schnecke mit Z10m1,5 und Z25m0,5 auf Schnecke m1,5
31063 U-Achse 60 mit Z28m0,5
31064 U-Achse 40 mit Z28m0,5
31082 Rastachse mit Z28m0,5
31075 Getriebehalter Schnecke mit Z14m0,5 und Z25m0,5 auf Schnecke m1,5
35695 Zahnrad Z15 anreihbar mit Z15m1,5 sowie innen und außen Z22m0,5

Darf ich mir hier eine detailierte Aufzählung der Möglichkeiten ersparen?