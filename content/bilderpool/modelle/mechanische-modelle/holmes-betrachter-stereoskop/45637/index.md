---
layout: "image"
title: "Holmes-Betrachter - Seitenansicht"
date: "2017-03-23T11:41:10"
picture: "Stereoskop_02.jpg"
weight: "2"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Optik", "stereo", "Stereogramm", "3D"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/45637
- /detailse350.html
imported:
- "2019"
_4images_image_id: "45637"
_4images_cat_id: "3390"
_4images_user_id: "2179"
_4images_image_date: "2017-03-23T11:41:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45637 -->
Hier kann man die einfache Mechanik sehen, mit der die Bühne leicht auf seine Sehstärke abgestimmt werden kann.
Natürlich nimmt man diesen Betrachter normaler Weise in die Hand. Hier steht er nur auf einer Platte, damit ich ihn besser fotografieren kann.