---
layout: "image"
title: "Generalschalter"
date: "2009-08-12T09:44:30"
picture: "abschussrampe18.jpg"
weight: "18"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24755
- /details279a.html
imported:
- "2019"
_4images_image_id: "24755"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24755 -->
Dieser Schalter Schaltet allen Strom an