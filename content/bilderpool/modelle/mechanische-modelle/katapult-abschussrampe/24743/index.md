---
layout: "image"
title: "Turm Von Oben"
date: "2009-08-12T09:44:30"
picture: "abschussrampe06.jpg"
weight: "6"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24743
- /detailsdf4b.html
imported:
- "2019"
_4images_image_id: "24743"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24743 -->
Dort Lagern die Bälle