---
layout: "image"
title: "Munition6"
date: "2009-08-12T09:44:30"
picture: "abschussrampe31.jpg"
weight: "31"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24768
- /details7e4b-2.html
imported:
- "2019"
_4images_image_id: "24768"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24768 -->
Mit diesen Bällen schiesst die Maschiene.