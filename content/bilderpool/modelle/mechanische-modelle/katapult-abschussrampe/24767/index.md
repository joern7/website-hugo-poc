---
layout: "image"
title: "Munition5"
date: "2009-08-12T09:44:30"
picture: "abschussrampe30.jpg"
weight: "30"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24767
- /details13cf.html
imported:
- "2019"
_4images_image_id: "24767"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24767 -->
Mit diesen Bällen schiesst die Maschiene.