---
layout: "image"
title: "Munition4"
date: "2009-08-12T09:44:30"
picture: "abschussrampe29.jpg"
weight: "29"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24766
- /details5764-2.html
imported:
- "2019"
_4images_image_id: "24766"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24766 -->
Mit diesen Bällen schiesst die Maschiene.