---
layout: "overview"
title: "Rechenmaschine"
date: 2020-02-22T08:20:48+01:00
legacy_id:
- /php/categories/3030
- /categoriesc8dc.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3030 --> 
Eine Vier-Spezies-Rechenmaschine mit kontinuierlichem Übertrag. Durch den kontinuierlichen Übertrag kann man beliebig viele der Additions-/Subtraktionseinheiten nebeneinander schalten. Der Genauigkeit sind also nur durch die Zahl der fischertechnik-Bauteile Grenzen gesetzt. Ein kontinuierlicher Übertrag mit Planetengetriebe wurde zuerst von Tschebyscheff im Jahr 1876 in seinem Arithmometer eingesetzt. Ein Video findet sich unter http://youtu.be/9a_us8rMczw