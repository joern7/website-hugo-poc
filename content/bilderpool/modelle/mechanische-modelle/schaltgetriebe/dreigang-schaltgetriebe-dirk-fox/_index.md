---
layout: "overview"
title: "Dreigang-Schaltgetriebe (Dirk Fox)"
date: 2020-02-22T08:17:12+01:00
legacy_id:
- /php/categories/2106
- /categoriesa2aa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2106 --> 
Funktionsmodell eines Dreigang-Schiebeschaltgetriebes mit geringer Bauhöhe und hoher Stabilität (daher auch für den Einsatz in Fahrzeugen geeignet)