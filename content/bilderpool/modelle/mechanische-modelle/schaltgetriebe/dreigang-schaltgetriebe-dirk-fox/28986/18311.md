---
layout: "comment"
hidden: true
title: "18311"
date: "2013-09-11T10:48:26"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hi,
mal ne frage zum Getriebe. ich habe das nachgebaut, bekomme das aber nur suboptimal hin.
mein problem: beim Schalten vom 1. auf den 2. Gang und umgekehrt greifen für einen Moment beide Übersetzungen, was zum kurzzeitigen Blockieren des Getriebes führt.
Dem Bild oben zufolge sollte das bei Dir eigentlich auch passieren. Oder konntest Du die Abstände so optimieren, daß das nicht passiert?