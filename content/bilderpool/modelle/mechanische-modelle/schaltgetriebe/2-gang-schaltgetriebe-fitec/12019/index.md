---
layout: "image"
title: "Neu"
date: "2007-09-26T15:07:38"
picture: "2-Gang-Getriebeneu4.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12019
- /detailsd48c.html
imported:
- "2019"
_4images_image_id: "12019"
_4images_cat_id: "972"
_4images_user_id: "456"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12019 -->
Gleiches Prinzip nur ein wenig kompakter.