---
layout: "overview"
title: "2-Gang-Schaltgetriebe (fitec)"
date: 2020-02-22T08:17:02+01:00
legacy_id:
- /php/categories/972
- /categories5d3a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=972 --> 
Besitzt 2 Gänge (2:1/1:2) und kann angetrieben sowie per Minimotor geschalten werden.