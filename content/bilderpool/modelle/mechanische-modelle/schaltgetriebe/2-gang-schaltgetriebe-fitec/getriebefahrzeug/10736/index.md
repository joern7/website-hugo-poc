---
layout: "image"
title: "Getriebe-Achse-Verbindung"
date: "2007-06-07T22:28:03"
picture: "Getriebefahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wolly2.0"
license: "unknown"
legacy_id:
- /php/details/10736
- /details2bf5.html
imported:
- "2019"
_4images_image_id: "10736"
_4images_cat_id: "973"
_4images_user_id: "570"
_4images_image_date: "2007-06-07T22:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10736 -->
Habe einfach eine Kette von oben nach unten gespannt, um Getriebe und Antriebsachse zu verbinden.