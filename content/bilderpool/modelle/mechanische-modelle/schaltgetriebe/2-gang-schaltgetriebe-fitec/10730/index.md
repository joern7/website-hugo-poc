---
layout: "image"
title: "2-Gang-Schaltgetriebe"
date: "2007-06-05T18:53:58"
picture: "2-Gang-Getriebe3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10730
- /details1208.html
imported:
- "2019"
_4images_image_id: "10730"
_4images_cat_id: "972"
_4images_user_id: "456"
_4images_image_date: "2007-06-05T18:53:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10730 -->
Die Führung für den Power-Motor.