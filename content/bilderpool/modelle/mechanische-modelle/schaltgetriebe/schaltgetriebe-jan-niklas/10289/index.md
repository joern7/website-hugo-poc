---
layout: "image"
title: "Getriebe mit Bremse"
date: "2007-05-05T08:38:34"
picture: "getriebe7.jpg"
weight: "7"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10289
- /details9bda-2.html
imported:
- "2019"
_4images_image_id: "10289"
_4images_cat_id: "935"
_4images_user_id: "557"
_4images_image_date: "2007-05-05T08:38:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10289 -->
Bremse