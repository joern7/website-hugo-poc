---
layout: "image"
title: "Getriebe mit Bremse"
date: "2007-05-05T08:38:34"
picture: "getriebe6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10288
- /detailsfdde.html
imported:
- "2019"
_4images_image_id: "10288"
_4images_cat_id: "935"
_4images_user_id: "557"
_4images_image_date: "2007-05-05T08:38:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10288 -->
Magnetventile