---
layout: "image"
title: "Von oben"
date: "2009-10-03T20:57:27"
picture: "minizahnraedergetriebe6.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25488
- /detailsa933.html
imported:
- "2019"
_4images_image_id: "25488"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25488 -->
Die ganze Konstruktion von oben. Es ist auf das linke Getriebe geschaltet.