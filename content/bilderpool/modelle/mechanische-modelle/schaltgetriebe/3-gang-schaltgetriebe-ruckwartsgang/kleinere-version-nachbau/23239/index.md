---
layout: "image"
title: "Gesamtansicht1"
date: "2009-02-28T10:27:06"
picture: "dsc00643.jpg"
weight: "1"
konstrukteure: 
- "ich"
fotografen:
- "ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23239
- /details1a0b.html
imported:
- "2019"
_4images_image_id: "23239"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-02-28T10:27:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23239 -->
Das ist mein Versuch, das Getriebe etwas kompakter zu machen. Der Vorteil ist, dass die Zahnräder nicht mehr tiefer als die Antriebswelle sind. Dadurch kann man das Getriebe leichter in Fahrzeuge einbauen.