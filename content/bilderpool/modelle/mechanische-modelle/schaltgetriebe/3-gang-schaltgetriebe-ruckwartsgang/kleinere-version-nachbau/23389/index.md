---
layout: "image"
title: "die Motoren"
date: "2009-03-06T19:34:34"
picture: "g5.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23389
- /details84eb.html
imported:
- "2019"
_4images_image_id: "23389"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23389 -->
Die Motoren für die Gangschaltung(die 3 unteren Zahnräder)  und den Antrieb.
