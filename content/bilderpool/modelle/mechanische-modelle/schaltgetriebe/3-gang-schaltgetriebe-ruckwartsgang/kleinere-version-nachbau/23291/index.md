---
layout: "image"
title: "Detailansicht1"
date: "2009-03-01T20:05:02"
picture: "DSC00648.jpg"
weight: "5"
konstrukteure: 
- "ich"
fotografen:
- "ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23291
- /detailsaa54-2.html
imported:
- "2019"
_4images_image_id: "23291"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-01T20:05:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23291 -->
Die Plastikachsen werden durch die Streben stabilisiert und an ihrer Position gehalten.
Die Abstände zwischen den Zahnrädern und den Streben sind 1-2mm groß, aber sie berühren sich auch bei hohen Belastungen oder Blockaden nicht.