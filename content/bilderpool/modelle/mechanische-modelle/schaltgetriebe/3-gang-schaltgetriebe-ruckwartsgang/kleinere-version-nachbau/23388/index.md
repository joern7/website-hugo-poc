---
layout: "image"
title: "Weitere Detailansicht"
date: "2009-03-06T19:34:34"
picture: "g4.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23388
- /details6ad8.html
imported:
- "2019"
_4images_image_id: "23388"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23388 -->
