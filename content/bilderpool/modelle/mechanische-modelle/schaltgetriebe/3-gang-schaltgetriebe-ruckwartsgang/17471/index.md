---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 03"
date: "2009-02-21T16:56:56"
picture: "porschemakus03.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17471
- /details98c5.html
imported:
- "2019"
_4images_image_id: "17471"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17471 -->
Ganz oben auf thronen die drei Z10 für den Rückwärtsgang.