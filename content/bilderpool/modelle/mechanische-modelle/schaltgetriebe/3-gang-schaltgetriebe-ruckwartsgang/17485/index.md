---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 17"
date: "2009-02-21T16:56:56"
picture: "porschemakus17.jpg"
weight: "17"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17485
- /detailsb0bf-3.html
imported:
- "2019"
_4images_image_id: "17485"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17485 -->
Dritter Gang eingelegt (Z20 ganz links)