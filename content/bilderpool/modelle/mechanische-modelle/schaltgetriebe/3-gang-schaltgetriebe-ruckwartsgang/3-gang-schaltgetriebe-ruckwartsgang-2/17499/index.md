---
layout: "image"
title: "20090223_3-Gang_Schaltgetriebe_mit_Rückwärtsgang_kompakter_07"
date: "2009-02-23T20:21:40"
picture: "ganggetriebekompakter7.jpg"
weight: "7"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17499
- /detailsedd4.html
imported:
- "2019"
_4images_image_id: "17499"
_4images_cat_id: "1571"
_4images_user_id: "327"
_4images_image_date: "2009-02-23T20:21:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17499 -->
Das Getriebe in Aktion