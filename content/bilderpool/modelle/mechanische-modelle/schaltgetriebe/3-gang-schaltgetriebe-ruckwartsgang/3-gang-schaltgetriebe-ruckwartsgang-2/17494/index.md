---
layout: "image"
title: "20090223_3-Gang_Schaltgetriebe_mit_Rückwärtsgang_kompakter_02"
date: "2009-02-23T20:21:40"
picture: "ganggetriebekompakter2.jpg"
weight: "2"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17494
- /details43af.html
imported:
- "2019"
_4images_image_id: "17494"
_4images_cat_id: "1571"
_4images_user_id: "327"
_4images_image_date: "2009-02-23T20:21:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17494 -->
Das Getriebe in Aktion