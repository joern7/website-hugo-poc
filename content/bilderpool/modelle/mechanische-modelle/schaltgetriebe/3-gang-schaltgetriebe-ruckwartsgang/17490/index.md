---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 22"
date: "2009-02-21T16:56:56"
picture: "porschemakus22.jpg"
weight: "22"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17490
- /details4b3e.html
imported:
- "2019"
_4images_image_id: "17490"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17490 -->
Leergang eingelegt