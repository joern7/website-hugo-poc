---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 06"
date: "2009-02-21T16:56:56"
picture: "porschemakus06.jpg"
weight: "6"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17474
- /detailsd1db.html
imported:
- "2019"
_4images_image_id: "17474"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17474 -->
Auf der Unterseite ist die Getriebenebenwelle mit einem Z10 und einem Z30 für den dritten Gang angebracht.

Weil die Z10 sehr in die Breite bauen, musste ich die Kontruktion entsprechend anpassen.