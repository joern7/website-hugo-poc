---
layout: "overview"
title: "3-Gang Schaltgetriebe mit Rückwärtsgang (Porsche-Makus)"
date: 2020-02-22T08:17:05+01:00
legacy_id:
- /php/categories/1570
- /categoriesf408.html
- /categories8fdc.html
- /categories0836.html
- /categories6a68.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1570 --> 
Hallo,



endlich ist es mir gelungen, ein Getriebe zu bauen! Hat sehr viele Anläufe gebraucht, aber jetzt bin ich mit dem Ergebnis sehr zufrieden.



Und so funktioniert das ganze:



Links (also da wo das Kardangelenk auf der Welle montiert ist) ist die Getriebeeingangswelle. Davor sitzt normalerweise noch eine Kupplung und dann der Motor.

Dann geht es über das Z20 der Getriebeeingangswelle weiter auf das Z40 (Hauptgetrieberad), wodurch schon mal eine Untersetzung erreicht wird. Auf der gleichen Welle sitzen nun von links nach rechts insgesamt vier Stück Z20:

1. 3. Gang (Z20 nicht auf Welle fixiert)

2. 2. Gang (Z20 nicht auf Welle fixiert)

3. 1. Gang (Z20 auf Welle fixiert)

4. Rückwärtsgang  (Z20 nicht auf Welle fixiert)



Die unterschiedlichen Übersetzungen werden durch die Nebengetriebewellen und insbesondere deren Zahnrädern erreicht. Konkret ist sind die Übersetzungen wie folgt gewählt:

1. Gang: Da dieses Z20 auf der Welle festgeschraubt ist, ist die Übersetzung Z20 -> Z40 -> Z20 -> Z40

2. Gang: Z20 -> Z40 -> Z20 -> Z40 -> Z20 -> Z40

3. Gang: Z20 -> Z40 -> Z10 -> Z30 Z20 -> Z40

Rückwärtsgang: Z20 -> Z40 -> Z10 -> Z10 -> Z10 -> Z20 -> Z40 (der Rückwärtsgang läuft etwas schneller als der 1. Gang)



Momentan ist der Leergang eingelegt, der sich sinnvollerweise zwischen dem ersten und dem Rückwärtsgang befindet. Auf den letzteren Bildern sind zur Verdeutlichung alle Gänge der Reihe nach (1., 2., 3., Rückwärts) eingelegt.



Das ganze geht somit also immer über ein Z20 auf das Z40 der Getriebeausgangswelle, wobei es kein Z40 sein muss, es können beliebige Zahnräder eingesetzt werden, je nach gewünschter Übersetzung.



Geschaltet wird das Ganze über die Getriebeausgangswelle, was auch wunderbar funktioniert, solange sich die Zahnräder drehen. Im Stillstand hakt es gerne, aber das ist bei einem Autogetriebe auch nicht anders. Die leicht abgeschrägten Zähne der Fischertechnik Zahnräder wirken so wie Synchronringe, d.h. wird langsam geschaltet, beschleunigt das Z40 der Getriebeausgangswelle durch die Reibung schon ein bisschen und irgendwann wird dann der richtige Moment erreicht und es lässt sich ganz leicht schalten.

