---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 09"
date: "2009-02-21T16:56:56"
picture: "porschemakus09.jpg"
weight: "9"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17477
- /details0938.html
imported:
- "2019"
_4images_image_id: "17477"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17477 -->
