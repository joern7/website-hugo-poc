---
layout: "image"
title: "automatik Getriebe 2"
date: "2007-07-13T12:03:14"
picture: "automatikgetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "StefanLehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11029
- /detailsfc8c.html
imported:
- "2019"
_4images_image_id: "11029"
_4images_cat_id: "997"
_4images_user_id: "502"
_4images_image_date: "2007-07-13T12:03:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11029 -->
Mit diesem Hebel kann hoch und runter schalten.