---
layout: "image"
title: "Frontansicht"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33927
- /detailsec4d.html
imported:
- "2019"
_4images_image_id: "33927"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33927 -->
Blick von vorne auf die Vorderachse mit Lenkung.