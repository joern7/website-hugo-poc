---
layout: "comment"
hidden: true
title: "16125"
date: "2012-01-16T13:49:33"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Stefan, für mich ist das hauptsächlich ein optischer Grund, weil die Ketten innen ja so rauh und technisch aussehen, außen dagegen so glatt und ästhetisch ... ;o)

Allerdings schließe ich bei kleinen und eng verbauten Kettentrieben die (straffe) Kette oft direkt am Zahnrad, und da geht es nur von außen.

Gruß, Thomas