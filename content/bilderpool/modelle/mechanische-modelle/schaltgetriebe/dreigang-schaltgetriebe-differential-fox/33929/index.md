---
layout: "image"
title: "AkkuMontage"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33929
- /details82c0.html
imported:
- "2019"
_4images_image_id: "33929"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33929 -->
Der Akku wird auf dem XM-Motor montiert, um ausreichend Gewicht auf die Vorderreifen zu bringen - sie haften sonst bei stärkerem Lenkeinschlag nicht, und das Fahrzeug rutscht geradeaus. 
Letzteres ließe sich wahrscheinlich durch ein Differential an der Hinterachse abschwächen, aber dafür wäre eine weitere Achse erforderlich, und das Getriebe geriete deutlich größer.