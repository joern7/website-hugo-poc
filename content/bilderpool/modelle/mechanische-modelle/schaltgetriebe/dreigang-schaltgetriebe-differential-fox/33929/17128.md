---
layout: "comment"
hidden: true
title: "17128"
date: "2012-08-26T00:18:21"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Es gibt mit 2 Differentiale noch mehr möglichkeiten:

http://www.nico71.fr/continuously-variable-transmission/

It enables a variable ratio on the output from 1:1 to 1:5. The CVT chooses the most suitable ratio functions of the resistive torque on the output. The principle is based on two differentials which are connected side by side with a first gear : 1:1 and a second gear on the other side : 1:5. The side with the 1:5 gear has also a friction pin in order to limit the rotation.

Das Pinzip gibt es auch bei dieser Link:

http://www.brickshelf.com/cgi-bin/gallery.cgi?i=4082578

Grüss,

Peter
Poederoyen NL