---
layout: "image"
title: "Zweiter Gang"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33924
- /detailsfa10.html
imported:
- "2019"
_4images_image_id: "33924"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33924 -->
Im zweiten Gang hat das Getriebe eine Untersetzung von 2:1 (der Getriebeteil mit dem Differential übersetzt 1:1), die Entfaltung liegt bei 12,75 cm.