---
layout: "image"
title: "2. Gang"
date: "2013-09-15T15:47:05"
picture: "2terGang.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37398
- /details64f0.html
imported:
- "2019"
_4images_image_id: "37398"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37398 -->
Im 2. Gang sind folgende Ritzel beteiligt:

1: Z20->Z20
2: Z20->Z20
3: Z20->Z20

Alle Z20 Ritzel sind genau in einer Ebene. Im 2. (mittleren) Gang überträgt das Getriebe 1:1