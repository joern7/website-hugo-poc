---
layout: "image"
title: "1. Gang"
date: "2013-09-15T15:47:05"
picture: "1terGang.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37397
- /detailsc474.html
imported:
- "2019"
_4images_image_id: "37397"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37397 -->
es ist etwas schwer zu erkennen, auch wegen der perspektivischen Verzerrung.
ich habe deswegen im Bild markiert, an welchen Stellen jeweils Zahnräder ineinandergreifen:
1: Z10->Z30
2: Z20->Z20
3: Z10->Z30