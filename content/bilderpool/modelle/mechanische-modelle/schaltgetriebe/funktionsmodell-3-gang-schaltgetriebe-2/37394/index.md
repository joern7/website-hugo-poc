---
layout: "image"
title: "Ansicht von Vorne/1.Stufe"
date: "2013-09-15T15:47:05"
picture: "VonVorne.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37394
- /details6559.html
imported:
- "2019"
_4images_image_id: "37394"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37394 -->
Von vorne sieht man die erste Getriebestufe. Diese ist fast mit dem Basisgetriebe aus 
http://www.ftcommunity.de/categories.php?cat_id=2779
identisch