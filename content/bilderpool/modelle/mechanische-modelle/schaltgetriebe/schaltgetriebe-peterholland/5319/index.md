---
layout: "image"
title: "Wechselgetriebe mit Differentialen und Elektromagneten"
date: "2005-11-12T22:15:17"
picture: "Wechselgetriebe_3_Vorwarts_1x_Zuruck_002.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5319
- /details9b2a-2.html
imported:
- "2019"
_4images_image_id: "5319"
_4images_cat_id: "640"
_4images_user_id: "22"
_4images_image_date: "2005-11-12T22:15:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5319 -->
Wechselgetriebe mit 4 Differentialen und 4 Elektromagneten als Kopplung.

Gangwechsel ohne "krssss -en" Problemlos von 1 nach 2 nach 3, und wieder zuruck.
Selbst von 3 direkt nach R (Ruckwarts) ist keine Problem.

Nur ein DifferentialDeckel habe ich umgebaut: geklebt mit ein durchbohrter Z-10-Rastritzel. Damit hat die Ruckwarts-Position ein langzames Drehzahl, und bleibt das Wechselgetriebe-Modell Kompakt.

Das Idee fur eine Wechselgetriebe mit Differentialen, kam von Max Buiting beim FT-Treffen in Schoonhoven. 
Seines Modell is aber ziemlich gross und hat Ketten. Max hat aber vor auch ein Wechselgetriebe ohne Ketten zu machen.

Gruss,

Peter Damen  (alias Peterholland)
Poederoyen
die Niederlände