---
layout: "image"
title: "Wechselgetriebe mit Differentialen und Elektromagneten"
date: "2005-11-12T22:15:18"
picture: "Wechselgetriebe_3_Vorwarts_1x_Zuruck_012.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5324
- /detailscc29.html
imported:
- "2019"
_4images_image_id: "5324"
_4images_cat_id: "640"
_4images_user_id: "22"
_4images_image_date: "2005-11-12T22:15:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5324 -->
