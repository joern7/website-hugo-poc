---
layout: "image"
title: "Antrieb 2"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40076
- /detailsf7ba.html
imported:
- "2019"
_4images_image_id: "40076"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40076 -->
Die zwei Kurbeln sind starr miteinander verbunden.