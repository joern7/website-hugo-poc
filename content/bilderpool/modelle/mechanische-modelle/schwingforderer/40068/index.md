---
layout: "image"
title: "Gesamtansicht"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40068
- /details9426.html
imported:
- "2019"
_4images_image_id: "40068"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40068 -->
Die Funktionsweise der Maschine: durch kleine Würfe wird das Fördergut nach vorne bewegt.
Der Antrieb erfolgt durch einen Encodermotor, der 1:4 überstezt wird.
Links ist die Zufahrt für Lkw`s... die ihr Gut in den Trichter abladen, das dann in die Förderrinne kommt.