---
layout: "comment"
hidden: true
title: "19878"
date: "2014-12-30T16:34:22"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein Ähnliches Prinzip zeigt auch der "Schrittförderer" vom Ur-ft 300 S:

http://ft-datenbank.de/search.php?keyword=Statik+100S-400S

In http://ft-datenbank.de/web_document.php?id=2bc64637-752f-4a94-b548-8ac80d5a95e7 ist das auf Seite 37. Da wird zwar nicht gerüttelt, aber auch via Schwinge Schritt für Schritt gefördert.

Neben der Mechanik hier finde ich den Aufbau des Trichters mit den vielen Winkelsteinen 60! interessant. Eine echt Fleißarbeit (ich hätte vermutlich einfach eine große Platte hingeklatscht ;-)

Gruß,
Stefan