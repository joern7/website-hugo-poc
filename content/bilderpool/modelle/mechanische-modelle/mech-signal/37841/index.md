---
layout: "image"
title: "8/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal8.jpg"
weight: "8"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- /php/details/37841
- /detailsab34.html
imported:
- "2019"
_4images_image_id: "37841"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37841 -->
Außenspannwerk