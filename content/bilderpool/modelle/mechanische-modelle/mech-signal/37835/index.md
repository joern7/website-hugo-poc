---
layout: "image"
title: "2/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal2.jpg"
weight: "2"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- /php/details/37835
- /detailse0f1-2.html
imported:
- "2019"
_4images_image_id: "37835"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37835 -->
mech. Signal