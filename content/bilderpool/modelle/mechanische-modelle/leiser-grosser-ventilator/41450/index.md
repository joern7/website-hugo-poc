---
layout: "image"
title: "Gefederte Lagerung"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41450
- /detailsfec9-2.html
imported:
- "2019"
_4images_image_id: "41450"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41450 -->
An allen vier Ecken sitzt der stabil gebaute obere Trageblock an solchen Kombinationen aus je drei 31307 Federfüßen aus dem alten Elektromechanik-Programm. Das filtert auch die letzten kleinen (sowieso kaum noch fühlbaren) Vibrationen weg und verhindert, dass die Tischplatte irgendein unerwünschtes Geräusch abgibt.