---
layout: "image"
title: "Antrieb"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41446
- /detailsf4df.html
imported:
- "2019"
_4images_image_id: "41446"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41446 -->
Der Antrieb besteht aus einem M-Motor aus den 1990er Jahren. Der läuft schön gleichmäßig (hat wohl mehr Spulen drin als der Ur-ft-Motor), kann auch sehr langsam sehr flüssig drehen und passt wunderbar ins ft-Raster.

Die Verbindung zur Ventilator-Metallachse besteht aus einer 31717 Schneckenspannzange und einer 31711 Schnecken-Kontermutter. Die müssen sauber fluchtend angebracht werden (eine frühere Modellversion hatte anstatt des BS15 direkt vor der Bauplatte 5x15x30 einen Gelenkstein, um das nicht-Fluchten auszugleichen, aber so ist's noch leiser).