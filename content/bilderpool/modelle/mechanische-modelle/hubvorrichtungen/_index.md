---
layout: "overview"
title: "Hubvorrichtungen"
date: 2020-02-22T08:18:09+01:00
legacy_id:
- /php/categories/1244
- /categories753a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1244 --> 
Maschinen zum Anheben von Sachen. Anders als bei einem Kran soll dabei die Sache nicht nach oben gezogen werden, sondern von unten nach oben gedrückt werden.