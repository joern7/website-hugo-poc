---
layout: "comment"
hidden: true
title: "5240"
date: "2008-02-08T16:29:15"
uploadBy:
- "bodo42"
license: "unknown"
imported:
- "2019"
---
Stimmt, einmal ist sogar das Getriebe vom MiniMot abgerissen, aber da war was verklemmt. Die Hebelwirkung ist also groß, aber es geht noch. Um die Aufhängung auf die andere Seite des Minimot zu legen müßte die ganze Konstruktion wieder wesentlich größer und schwerer werden, weil es ja nicht an der unteren Stebe befestigt werden kann.