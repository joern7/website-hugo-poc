---
layout: "image"
title: "Hebeplatform 2"
date: "2008-02-07T09:48:06"
picture: "DSCN0031.jpg"
weight: "2"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13581
- /details2855.html
imported:
- "2019"
_4images_image_id: "13581"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13581 -->
