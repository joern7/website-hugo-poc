---
layout: "image"
title: "Hebeplatform 8"
date: "2008-02-07T09:48:06"
picture: "DSCN0037.jpg"
weight: "8"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13587
- /details2eef.html
imported:
- "2019"
_4images_image_id: "13587"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13587 -->
Hier nochmal der zusammengefaltete Zustand. Leider ragen die Schnecken noch weit nach oben hervor, aber das ist wohl nicht zu vermeiden. Ein hydraulischer Antrieb hätte dieses Problem aber wohl nicht.