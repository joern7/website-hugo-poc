---
layout: "image"
title: "Hebeplatform 5"
date: "2008-02-07T09:48:06"
picture: "DSCN0034.jpg"
weight: "5"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13584
- /details0d49.html
imported:
- "2019"
_4images_image_id: "13584"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13584 -->
Am Rücken des Mini Motors wurde die Feder entfernt, damit er genau 15mm breit ist und zwischen die Streben passt.