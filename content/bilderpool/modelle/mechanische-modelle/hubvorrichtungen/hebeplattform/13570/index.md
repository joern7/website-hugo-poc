---
layout: "image"
title: "Hebeplatform 9"
date: "2008-02-06T17:15:06"
picture: "DSCN0016.jpg"
weight: "9"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13570
- /details2ddd.html
imported:
- "2019"
_4images_image_id: "13570"
_4images_cat_id: "1245"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13570 -->
Die Platform im ausgeklappten Zustand. Überwundener Höhenunterschied: 33cm.