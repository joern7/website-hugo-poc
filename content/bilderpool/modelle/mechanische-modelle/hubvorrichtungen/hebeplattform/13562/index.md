---
layout: "image"
title: "Hebeplatform 1"
date: "2008-02-06T17:15:05"
picture: "DSCN0009.jpg"
weight: "1"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13562
- /details739b.html
imported:
- "2019"
_4images_image_id: "13562"
_4images_cat_id: "1245"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13562 -->
Die Platform im eingeklappten Zustand. Höhe der kleinen roten Platte: 17cm.