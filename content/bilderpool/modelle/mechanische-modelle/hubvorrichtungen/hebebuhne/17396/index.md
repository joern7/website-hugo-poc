---
layout: "image"
title: "Hebemechanismus"
date: "2009-02-14T09:50:29"
picture: "hebebuehne05.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17396
- /details0486.html
imported:
- "2019"
_4images_image_id: "17396"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17396 -->
