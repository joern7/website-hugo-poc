---
layout: "image"
title: "Seitenansicht"
date: "2009-02-14T09:50:29"
picture: "hebebuehne02.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17393
- /detailsfbb4.html
imported:
- "2019"
_4images_image_id: "17393"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17393 -->
