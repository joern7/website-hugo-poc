---
layout: "image"
title: "Bühne"
date: "2009-02-14T09:50:29"
picture: "hebebuehne04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17395
- /detailsead0.html
imported:
- "2019"
_4images_image_id: "17395"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17395 -->
