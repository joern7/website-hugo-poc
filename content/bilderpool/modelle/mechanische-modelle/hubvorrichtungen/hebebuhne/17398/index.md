---
layout: "image"
title: "hochgefahren"
date: "2009-02-14T09:50:30"
picture: "hebebuehne07.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17398
- /details324b.html
imported:
- "2019"
_4images_image_id: "17398"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17398 -->
