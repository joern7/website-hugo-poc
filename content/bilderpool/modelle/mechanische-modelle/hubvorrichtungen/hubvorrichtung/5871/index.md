---
layout: "image"
title: "Hub01.JPG"
date: "2006-03-12T13:39:44"
picture: "Hub01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5871
- /detailse348.html
imported:
- "2019"
_4images_image_id: "5871"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:39:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5871 -->
Diese Vorrichtung hebt Gewichte, die man mit Pneumatik nicht hoch bekommt, und die ich dem ft-Hubgetriebe nicht zumuten möchte. Bei den ft-Schnecken hat man zwar enorm viel Kraft, muss sie aber irgendwie durch ein Gelenk hindurch bekommen.

Das Kernstück sind zwei Differenziale, zwischen die eine ft-Zahnstange locker hineinpasst.