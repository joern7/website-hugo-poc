---
layout: "image"
title: "Hub05.JPG"
date: "2006-03-12T13:48:37"
picture: "Hub05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5875
- /details686c-2.html
imported:
- "2019"
_4images_image_id: "5875"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:48:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5875 -->
