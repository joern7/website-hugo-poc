---
layout: "comment"
hidden: true
title: "909"
date: "2006-03-12T14:32:51"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Raffiniert!

Die Differentialmimik ist klasse. Die Idee, die Scheibenteile (keine Ahnung, wie die korrekt heißen) als Zahnstange ist auch genial - wenn man genug davon hat.

Sonst könntest Du doch auch mehrere ft-Zahnstangen zusammenkoppeln. Zwar in diesem Fall nicht mit normalen Bausteinen, weil die Zähne ja freiligen müssen. Aber gingen z. B. BS 7,5 nicht für diese Aufgabe? Die Stangen werden doch nur auf Druck beansprucht, wenn ich das richtig sehe.

Gruß und, wiedermal, ein Kompliment für den Ideenreichtum,
Stefan