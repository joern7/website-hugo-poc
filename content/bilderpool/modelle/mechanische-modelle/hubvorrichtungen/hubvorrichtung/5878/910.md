---
layout: "comment"
hidden: true
title: "910"
date: "2006-03-12T16:18:34"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das Anstückeln mit BS7,5 habe ich natürlich probiert gehabt ;-) aber die 2 oder 3 Millimeter Überlappung halten nicht so doll.

Von den "Hülse mit Scheibe" 35981 kannst du gerne welche abhaben (die habe ich mal in einem Sack "ft-Kiloware" erhalten).

Differenziale sind überhaupt faszinierende Teile. Womit ich mich aber schwer tue, ist, auszurechnen, welches Übersetzungsverhältnis die Mimik hier genau hat.

Gruß,
Harald