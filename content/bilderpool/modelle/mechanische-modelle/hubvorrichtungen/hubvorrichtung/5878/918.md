---
layout: "comment"
hidden: true
title: "918"
date: "2006-03-14T20:53:46"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hobby2!? Richtig, da war noch was!
Na gut, aber Getriebe mit zwei Differenzialen sind selbst da nicht drin, wenn ich richtig sehe.


Schnecken m1,5... mal sehen...

und siehe da: ja, es geht auch damit! Man muss die Differenziale etwas enger setzen, sonst schlüpfen/ruckeln sich die Schnecken (Schnecken in der Mehrzahl, weil ich auch hier zwei parallel verwende) dazwischen durch: streiche BS30, setze BS5 + BS7,5 + BS15 und die Sache passt.
Die Schnecken drehen sich unter Last (insbesondere wenn noch Vibration im Spiel ist). Das kann man abstellen, wenn man die Klemm-Muffen mit Klemm-Z10 versieht: dann können sie sich nur noch gegenläufig drehen, und das nicht sehr weit.