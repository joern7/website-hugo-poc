---
layout: "image"
title: "Scherenbühne 1"
date: "2008-02-06T17:15:06"
picture: "DSCN0021.jpg"
weight: "1"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13571
- /detailsbffb-2.html
imported:
- "2019"
_4images_image_id: "13571"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13571 -->
Die Scherenbühne im komplett eingeklapptem Zustand. Abstand der roten Platte zur Grundplatte: 10.5cm. Leider ist es nahezu unmöglich, diesen Zustand per Motor zu verlassen, weil die Kräfte dafür zu gross sind. Wenn die Bühne nicht ganz eingeklappt ist, geht es schon viel einfacher.