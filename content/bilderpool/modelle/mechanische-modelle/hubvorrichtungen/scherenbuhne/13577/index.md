---
layout: "image"
title: "Scherenbühne 7"
date: "2008-02-06T17:20:44"
picture: "DSCN0018.jpg"
weight: "7"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13577
- /details54ff.html
imported:
- "2019"
_4images_image_id: "13577"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13577 -->
Hier der Versuch, den größten Widerstand am Anfang der Bewegung zu reduzieren, indem die Schere mit Abstandshaltern daran gehindert wird, ganz nach unten zu klappen. Leider steigt dadurch auch die Grundhöhe, und die nötigen Kräfte sind immer noch zu hoch.