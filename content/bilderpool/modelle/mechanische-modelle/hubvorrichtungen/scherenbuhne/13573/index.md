---
layout: "image"
title: "Scherenbühne 3"
date: "2008-02-06T17:15:25"
picture: "DSCN0019.jpg"
weight: "3"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13573
- /detailse57f.html
imported:
- "2019"
_4images_image_id: "13573"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13573 -->
Die Scherenbühne in voll ausgeklapptem Zustand. Abstand der kleinen roten Platte zur Grundplatte: 57cm. Überwundener Höhenunterschied: 46.5cm.