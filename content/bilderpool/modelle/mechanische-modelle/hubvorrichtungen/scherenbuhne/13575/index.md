---
layout: "image"
title: "Scherenbühne 5"
date: "2008-02-06T17:20:43"
picture: "DSCN0023.jpg"
weight: "5"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13575
- /details65ea.html
imported:
- "2019"
_4images_image_id: "13575"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:20:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13575 -->
Der Teil, der sich links unter auf der Stange bewegt sollte evtl. durch einen Hydraulikzylinder oder Flaschenzug angetrieben werden. Leider sind die erforderlichen Kräfte extrem hoch, so dass ich mir noch nicht sicher bin, wie das mit einem FT Motor schaffbar ist. Eine Schnecke schafft es jedenfalls nicht, und ein Hubgetriebe schon gar nicht.