---
layout: "image"
title: "Scherenbühne (Vorlage)"
date: "2008-02-06T20:44:14"
picture: "Vorlage.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13578
- /detailsd35c.html
imported:
- "2019"
_4images_image_id: "13578"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T20:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13578 -->
Hier die Vorlage zur Scherenbühne vom Universalfahrzeug. Der Antrieb erfolgt über eine Seilwinde. Leider ist die Konstruktion nur einstufig, und der überwundene Höhenunterschied deshalb sehr gering.