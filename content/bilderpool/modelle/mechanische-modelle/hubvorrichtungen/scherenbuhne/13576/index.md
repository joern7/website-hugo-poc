---
layout: "image"
title: "Scherenbühne 6"
date: "2008-02-06T17:20:44"
picture: "DSCN0024.jpg"
weight: "6"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13576
- /detailsdf90.html
imported:
- "2019"
_4images_image_id: "13576"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13576 -->
