---
layout: "image"
title: "Gesamtansicht 3"
date: "2005-03-28T23:31:27"
picture: "Contact-Maschine_004.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3901
- /details51d0.html
imported:
- "2019"
_4images_image_id: "3901"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3901 -->
Insgesamt gibt es übrigens nur vier Metallachsen im Modell. Ich bin echt überrascht, was man mit den Rastachsen alles anstellen kann.