---
layout: "image"
title: "Gesamtansicht rechte Seite"
date: "2005-03-28T23:50:43"
picture: "Contact-Maschine_010.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3907
- /details90ef.html
imported:
- "2019"
_4images_image_id: "3907"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3907 -->
Man sieht, wie das Z20 das rote Z40 antreibt. Direkt links davon steckt ein Baustein 30, mittels zweier S-Riegel an den auch innen angebrachten Bogenstücken 60 fixiert, in dem die Achse mit den Z10 geführt wird.