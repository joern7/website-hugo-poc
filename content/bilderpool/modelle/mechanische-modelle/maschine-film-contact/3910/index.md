---
layout: "image"
title: "Antrieb des mittleren Rings"
date: "2005-03-28T23:50:43"
picture: "Contact-Maschine_013.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3910
- /detailsad7c-2.html
imported:
- "2019"
_4images_image_id: "3910"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3910 -->
Das Prinzip wieder wie beim äußeren Ring auch: Achse fixiert, Z40 angetrieben, aber lose auf der Achse und dafür fest mit dem mittleren Ring verbunden. Und zwar wie man hier sieht: Zwei der drei Stummel des Z40 passen gerade so an dem 15 mm breiten mittleren Ring vorbei, wenn je eine kleine Abdeckplatte montiert wird. Damit gibt's es kein nennenswertes Spiel.