---
layout: "image"
title: "Gesamtansicht 2"
date: "2005-03-28T23:31:27"
picture: "Contact-Maschine_003.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3900
- /details42c4.html
imported:
- "2019"
_4images_image_id: "3900"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3900 -->
Das ganze bringt's aber erst, wenn man es in Bewegung sieht. Der äußere Ring wird links und rechts zum Drehen gezwungen. Eben diese Drehung bewirkt, dass der mittlere Ring sich darin dreht. Und dies wiederum zwingt den inneren Ring sich, darin wieder zu drehen.