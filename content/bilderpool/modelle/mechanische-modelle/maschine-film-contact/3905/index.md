---
layout: "image"
title: "Übergang auf den mittleren Ring"
date: "2005-03-28T23:32:55"
picture: "Contact-Maschine_008.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3905
- /detailsd67a.html
imported:
- "2019"
_4images_image_id: "3905"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3905 -->
Dasselbe von der anderen Seite gesehen. Es ist übrigens für die richtige Drehrichtung wichtig, dass das Z10 links oben im Bild auf der entfernten Seite des Z40 eingreift, sonst wird die Bewegung viel weniger interessant.