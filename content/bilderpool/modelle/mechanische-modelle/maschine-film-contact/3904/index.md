---
layout: "image"
title: "Übergang auf den mittleren Ring"
date: "2005-03-28T23:31:49"
picture: "Contact-Maschine_007.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3904
- /detailsdb40.html
imported:
- "2019"
_4images_image_id: "3904"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3904 -->
Das gleiche Bild aus einem nur leicht anderen Blickwinkel gesehen.