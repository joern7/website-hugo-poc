---
layout: "image"
title: "Drehzylinder M5-Antrieb+ 5-6mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb7.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39296
- /details9719-2.html
imported:
- "2019"
_4images_image_id: "39296"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39296 -->
M5-Antrieb+ 5-6mm-Rohr