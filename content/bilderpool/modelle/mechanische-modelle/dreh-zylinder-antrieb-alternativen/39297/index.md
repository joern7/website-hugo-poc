---
layout: "image"
title: "Drehzylinder M5-Antrieb+ 5-6mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb8.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39297
- /detailsfac8.html
imported:
- "2019"
_4images_image_id: "39297"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39297 -->
M5-Antrieb+ 5-6mm-Rohr