---
layout: "image"
title: "Drehzylinder M4-Antrieb+ 6-4mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb6.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39295
- /details2110.html
imported:
- "2019"
_4images_image_id: "39295"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39295 -->
M4-Antrieb+ 6-4mm-Rohr