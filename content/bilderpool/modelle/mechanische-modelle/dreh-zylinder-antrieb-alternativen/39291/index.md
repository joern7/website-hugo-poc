---
layout: "image"
title: "Fischertechnik + Lego"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb2.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39291
- /details66bb.html
imported:
- "2019"
_4images_image_id: "39291"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39291 -->
Antrieb zum Fischertechnik