---
layout: "comment"
hidden: true
title: "19530"
date: "2014-09-26T08:40:34"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Interessante sehr kleine (10 x 12 x26mm) Getriebemotoren passend im FT-Raster gibt es bei :

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1

http://www.pololu.com/product/2218

Overview

These tiny brushed DC gearmotors are intended for use at 6 V, though in general, these kinds of motors can run at voltages above and below this nominal voltage, so they should comfortably operate in the 3 &#8211; 9 V range (rotation can start at voltages as low as 0.5 V). Lower voltages might not be practical, and higher voltages could start negatively affecting the life of the motor. The micro metal gearmotors are available in a wide range of gear ratios&#8212;from 5:1 up to 1000:1&#8212;and offer a choice between three different motors: high-power (HP), medium-power (MP), and standard. With the exception of the 1000:1 gear ratio versions, all of the micro metal gearmotors have the same physical dimensions, so one version can be easily swapped for another if your design requirements change.

Gruss,

Peter
Poederoyen NL