---
layout: "comment"
hidden: true
title: "19531"
date: "2014-09-26T08:47:21"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Zum Einbau der Getriebemotor ist interessant :

U-Profile mit Streifen - grau 15x15mm 
http://www.ftcommunity.de/details.php?image_id=33987

Diese Bauteile gibt es bei: 

LPE: http://lpe.lpe-b2b.de/index.php?page=product&info=3697
http://lpe.lpe-b2b.de/index.php?page=product&info=2770