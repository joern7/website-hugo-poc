---
layout: "image"
title: "Drehzylinder M6-Antrieb+ 6-8mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb9.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39298
- /details4c60.html
imported:
- "2019"
_4images_image_id: "39298"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39298 -->
Oben:  Drehzylinder M6-Antrieb+ 6-8mm-Rohr

Unten: M6-Antrieb