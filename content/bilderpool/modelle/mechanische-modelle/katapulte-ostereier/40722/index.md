---
layout: "image"
title: "Katapult 3"
date: "2015-04-03T20:27:39"
picture: "IMG_0012_3.jpg"
weight: "11"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40722
- /details3223.html
imported:
- "2019"
_4images_image_id: "40722"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T20:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40722 -->
