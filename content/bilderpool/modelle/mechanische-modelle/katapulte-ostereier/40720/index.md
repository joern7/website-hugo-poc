---
layout: "image"
title: "Hobby 1 Band 3 S. 12"
date: "2015-04-03T15:14:58"
picture: "katapult_3_hobby_1-3_S_12.jpg"
weight: "9"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40720
- /details6c0a.html
imported:
- "2019"
_4images_image_id: "40720"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T15:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40720 -->
