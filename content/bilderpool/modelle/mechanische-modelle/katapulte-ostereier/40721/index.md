---
layout: "image"
title: "Katapult 3"
date: "2015-04-03T15:14:58"
picture: "IMG_0011_2.jpg"
weight: "10"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40721
- /detailsf319.html
imported:
- "2019"
_4images_image_id: "40721"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T15:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40721 -->
