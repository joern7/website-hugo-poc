---
layout: "image"
title: "Hobby 1 Band 1 S. 36"
date: "2015-04-03T15:14:58"
picture: "IMG_0011.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Ostereier-Katapulte"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40716
- /detailsb214.html
imported:
- "2019"
_4images_image_id: "40716"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T15:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40716 -->
Ostereier-Katapulte