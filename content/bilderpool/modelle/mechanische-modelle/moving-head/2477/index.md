---
layout: "image"
title: "Moving Head Schleifringe"
date: "2004-06-06T10:30:08"
picture: "MH08-Schleifringe.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2477
- /detailsfe61-2.html
imported:
- "2019"
_4images_image_id: "2477"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2477 -->
Groß, aber funktionstüchtig, die Stromübergabe auf den drehenden Teil.