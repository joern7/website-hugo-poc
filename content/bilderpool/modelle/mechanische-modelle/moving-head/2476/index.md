---
layout: "image"
title: "Moving Head Gabel"
date: "2004-06-06T10:30:08"
picture: "MH05-Gabel.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2476
- /detailsf81f.html
imported:
- "2019"
_4images_image_id: "2476"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2476 -->
Noch ist die Mitte leer, aber da läßt sich bestimmt etwas sinnvolles montieren.