---
layout: "comment"
hidden: true
title: "990"
date: "2006-04-13T13:25:56"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Nicht schlecht, aber das Seil verläuft nun vom Motor weg, was an Kränen usw. sicher einiges an Bauraum verschwendet. Bei meiner Lösung verläuft das Seil zum Motor hin, was den Vorteil hat, dass sich das ganze Modul recht kompakt verbauen lässt, z.B. an kleinen Autokränen.

Geht das, die Winde um 90 Grad zum Motor hin zu verbauen (im Foto nach unten)? Ich besitze das rote Ritzel leider (noch) nicht, sonst würde ich es selbst probieren.