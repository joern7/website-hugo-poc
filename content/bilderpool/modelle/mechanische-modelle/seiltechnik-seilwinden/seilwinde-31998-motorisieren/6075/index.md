---
layout: "image"
title: "31998-02.JPG"
date: "2006-04-12T20:22:48"
picture: "31998-02.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6075
- /details9e95.html
imported:
- "2019"
_4images_image_id: "6075"
_4images_cat_id: "523"
_4images_user_id: "4"
_4images_image_date: "2006-04-12T20:22:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6075 -->
Da fiel das Stichwort "Ritzel", und ich hab's mal mit dem Zahnrad von der alten Seiltrommel probiert. Ziemlich schwergängig und der Baustein 15x30x5 sieht nicht nur in der Kamera-Optik verbogen aus, er ist es auch.