---
layout: "image"
title: "Seilantrieb"
date: "2008-10-25T21:35:43"
picture: "Seilantrieb-Det2.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/16076
- /details7f40.html
imported:
- "2019"
_4images_image_id: "16076"
_4images_cat_id: "2220"
_4images_user_id: "59"
_4images_image_date: "2008-10-25T21:35:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16076 -->
Die Lage des Zahnrades Z40 (auch Z30 wäre möglich) kann ohne weiteres an bau-
liche Gegebenheiten angepaßt werden. Ein Abstand zu den Drehscheiben ist nur
bei Antrieb via Kette erforderlich; die zusätzlichen Achsen verhindern das Durchrut-
schen in den Naben. Liegen alle Drehscheiben und das Zahnrad direkt aneinander,
so genügen Naben lediglich ganz außen; die innere Drehscheibe wird dann durch
die eingeschobenen Achsen fixiert.