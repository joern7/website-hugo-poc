---
layout: "image"
title: "verseilmaschine 4"
date: "2005-11-13T12:11:20"
picture: "verseilmaschine_4.jpg"
weight: "5"
konstrukteure: 
- "Sven Engelke & Ralf Gerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/5329
- /details99ce.html
imported:
- "2019"
_4images_image_id: "5329"
_4images_cat_id: "457"
_4images_user_id: "1"
_4images_image_date: "2005-11-13T12:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5329 -->
