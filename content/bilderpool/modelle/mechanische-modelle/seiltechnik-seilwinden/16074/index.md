---
layout: "image"
title: "Seilantrieb"
date: "2008-10-25T21:35:42"
picture: "Seilantrieb-Oben.jpg"
weight: "2"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/16074
- /details216a.html
imported:
- "2019"
_4images_image_id: "16074"
_4images_cat_id: "2220"
_4images_user_id: "59"
_4images_image_date: "2008-10-25T21:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16074 -->
Die U-Träger können auch weiter auseinander stehen. Die Seilführung läßt sich gut
daran anpassen. Der Abstand der angetriebenen Seile zueinander definiert sich aus
der Größe der Drehscheiben selbst und kann (ohne entsprechenden Aufwand) nur
geringfügig verändert werden. Bei drei Drehscheiben entsteht das Problem, daß ei-
ne zweite Umlenkrolle oben schlecht montiert werden kann, da diese leicht versetzt
zur ersten stehen muß und somit zwei unabhängige Lager erfordert.