---
layout: "image"
title: "Seilantrieb"
date: "2008-10-25T21:35:43"
picture: "Seilantrieb-Det1.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/16075
- /details6094.html
imported:
- "2019"
_4images_image_id: "16075"
_4images_cat_id: "2220"
_4images_user_id: "59"
_4images_image_date: "2008-10-25T21:35:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16075 -->
Besonderes Augenmerk gilt dem Baustein 15 mit rundem Zapfen, der durch einen
Winkelstein 15° fixiert ist. Falls kein solcher Baustein zur Verfügung steht, kann auch
ein normaler Baustein 15 verbaut werden (Zapfen zur Seite ausrichten). Doch dann
ist unbedingt auf beiden Seiten ein Winkelstein 15° zum Stützen erforderlich.