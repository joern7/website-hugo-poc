---
layout: "image"
title: "Das dritte Bild der 'Dampfmaschine'"
date: "2017-01-21T19:05:25"
picture: "bkl3.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45079
- /details290e.html
imported:
- "2019"
_4images_image_id: "45079"
_4images_cat_id: "3356"
_4images_user_id: "2635"
_4images_image_date: "2017-01-21T19:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45079 -->
