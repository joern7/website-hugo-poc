---
layout: "image"
title: "Eine elektrische &#8222;Dampfmaschine&#8220;"
date: "2017-01-21T19:05:25"
picture: "bkl1.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45077
- /details19af-2.html
imported:
- "2019"
_4images_image_id: "45077"
_4images_cat_id: "3356"
_4images_user_id: "2635"
_4images_image_date: "2017-01-21T19:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45077 -->
Dies war vor einem Monat mein erster Versuch, die Funktion einer Dampfmaschine mit den ft-Elektromagneten nachzubilden. Die Maschine lief sauber mit etwa 12 V Gleichspannung. 
Da sie nur einfach wirkend ist, muss sie zum Starten in jedem Fall leicht angeschubst werden. Zwei Stabmagnete mit 4 x 25 mm werden für den &#8222;Kolben&#8220; verwendet. Der Antrieb des Pleuels ist mechanisch ungünstig hoch angesetzt. Die sich ergebende erhöhte Reibung durch die Verkantung lässt sich ausgleichen durch die verschiebbaren Lagerachsen, die Haftreibung wird dadurch ausreichend verringert. Mir war der Vorteil des kompakten Aufbaus wichtig. Auch die lockere Lagerung des Pleuels im &#8222;Kolben&#8220; verbessert den Lauf der Maschine, also Gelenkwürfel-Klaue ohne Hülse. Die Steuerung ist kaum zu erkennen auf der Rückseite. Eine Gelenkwürfel-Zunge trägt einen kleinen Magneten (4x10 mm) und der regt einen Reedkontakt an, einmal je Umdrehung. 
Wenn man die 4 Bilder schnell hintereinander anschaut erhält man einen filmischen Effekt.
Weitere Bilder habe ich leider nicht aufgenommen und die Maschine ist bereits wieder demontiert für die nächsten Projekte :-).