---
layout: "image"
title: "Blick auf Dreieck"
date: "2015-02-14T19:15:14"
picture: "2_Turnover_Blick_auf_Dreieck.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40536
- /details23e8.html
imported:
- "2019"
_4images_image_id: "40536"
_4images_cat_id: "3038"
_4images_user_id: "724"
_4images_image_date: "2015-02-14T19:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40536 -->
