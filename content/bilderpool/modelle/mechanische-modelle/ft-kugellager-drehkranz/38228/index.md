---
layout: "image"
title: "oberes Drehteil abgenommen"
date: "2014-02-11T15:57:07"
picture: "DSCN5208.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/38228
- /detailsa066-2.html
imported:
- "2019"
_4images_image_id: "38228"
_4images_cat_id: "2847"
_4images_user_id: "184"
_4images_image_date: "2014-02-11T15:57:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38228 -->
Hier sieht man den Innenaufbau.
6 Achsen (hier 35mm) "oder wenn jemand Originalteile nehmen möchte Achse 60" in die Seitlichen Löcher der Drehscheibe gesteckt und mit Klemmbuchsen und kleinen Seilrollen versehen. Zur ersten Montage habe ich die Drehscheibe mit einer Nabe ausgestattet damit diese mittig liegt.