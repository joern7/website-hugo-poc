---
layout: "image"
title: "Eine weitere Ansicht"
date: "2008-11-16T23:15:14"
picture: "PB160037.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/16287
- /detailsfd79-2.html
imported:
- "2019"
_4images_image_id: "16287"
_4images_cat_id: "1469"
_4images_user_id: "381"
_4images_image_date: "2008-11-16T23:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16287 -->
