---
layout: "image"
title: "Detailblick auf das Getriebe"
date: "2006-06-18T19:44:25"
picture: "planetarium4.jpg"
weight: "4"
konstrukteure: 
- "Michael Samek"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6437
- /details0da3.html
imported:
- "2019"
_4images_image_id: "6437"
_4images_cat_id: "565"
_4images_user_id: "104"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6437 -->
Einige Maße des Getriebes lassen sich tatsächlich etwas einfacher exakt realisieren als in der SnapCon-Vorlage: Zwei BS30 mit Loch stellen die korrekten Zahnradabstände her, anstatt ein BS15 mit Loch und 2 BS5.