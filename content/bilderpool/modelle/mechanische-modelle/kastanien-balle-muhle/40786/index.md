---
layout: "image"
title: "Bälle- & Kastanienmühle Vorne Schaufelradachslager 2xBS15 mit Loch"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle19.jpg"
weight: "19"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40786
- /details784b.html
imported:
- "2019"
_4images_image_id: "40786"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40786 -->
