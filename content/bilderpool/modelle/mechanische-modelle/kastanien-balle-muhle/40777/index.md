---
layout: "image"
title: "Kastanienmühle Vorne 04"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle10.jpg"
weight: "10"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40777
- /details07b1-2.html
imported:
- "2019"
_4images_image_id: "40777"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40777 -->
