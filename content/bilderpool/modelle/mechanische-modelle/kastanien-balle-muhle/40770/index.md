---
layout: "image"
title: "Kastanienmühle Oben 01"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle03.jpg"
weight: "3"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40770
- /detailsa11b.html
imported:
- "2019"
_4images_image_id: "40770"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40770 -->
Im Sandkasten hatte bestimmt der Eine oder Andere im Kindesalter eine Sandmühle.
Oben wird rieselfähiger Sand eingefüllt. Dieser  fließt durch einen Trichter.
Der herunterfallende Sand treibt ein oder zwei Mühlräder an.

Das Prinzip, wurde einfach auf Kastanien und Bälle angewandt.

Anforderungen die erfüllt werden sollten:

+ es soll ein "Lern(äahhh) Spielzeug" für Kinder im Alter von 1-99 Jahren sein
+ hohe Stabilität
+ Unfallsicher (verschluckbare Kleinteile)
+ Umfallsicher (umsturzsicher)