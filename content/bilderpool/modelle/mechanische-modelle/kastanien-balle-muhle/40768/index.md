---
layout: "image"
title: "Kastanienmühle Schaufelrad 01"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle01.jpg"
weight: "1"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40768
- /detailsd9eb.html
imported:
- "2019"
_4images_image_id: "40768"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40768 -->
Der Große Reifen aus dem Pneumatik Traktor Set mit einer Metalachse bildet die Achseaufnahme für die 8 Schaufeln.