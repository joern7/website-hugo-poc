---
layout: "image"
title: "Kugeltreppe"
date: "2013-10-22T16:40:22"
picture: "kugeltreppe1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37744
- /detailsb25b.html
imported:
- "2019"
_4images_image_id: "37744"
_4images_cat_id: "2803"
_4images_user_id: "162"
_4images_image_date: "2013-10-22T16:40:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37744 -->
Hier einige Bilder von meine Kugeltreppe. Es gibt 12 Treppenstufen die durch Nockenscheiben nach unten/oben bewegen. Dadurch werden die Kugeln eins fur eins nach die nachste Treppenstufe gebracht.
Dieser Modell kann mit ein Motor ausgestattet werden oder mit der Hand bedient werden.