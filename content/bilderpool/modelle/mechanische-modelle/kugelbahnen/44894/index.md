---
layout: "image"
title: "Unterprogramme"
date: "2016-12-11T16:13:02"
picture: "PRG_Unterfunktion01_2.png"
weight: "25"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Software"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44894
- /details26dd.html
imported:
- "2019"
_4images_image_id: "44894"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-11T16:13:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44894 -->
Die beiden Subroutinen zur Steuerung der Kugelbahn