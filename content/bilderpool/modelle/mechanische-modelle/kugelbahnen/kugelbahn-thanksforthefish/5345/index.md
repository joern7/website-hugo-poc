---
layout: "image"
title: "Kugelbahn 3"
date: "2005-11-16T14:14:13"
picture: "Kugelbahn_3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/5345
- /detailsdef7.html
imported:
- "2019"
_4images_image_id: "5345"
_4images_cat_id: "459"
_4images_user_id: "381"
_4images_image_date: "2005-11-16T14:14:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5345 -->
