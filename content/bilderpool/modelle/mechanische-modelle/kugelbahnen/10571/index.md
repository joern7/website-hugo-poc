---
layout: "image"
title: "Kugelbahn mit Greifer"
date: "2007-05-31T09:43:06"
picture: "diverse3.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10571
- /detailsfcaa.html
imported:
- "2019"
_4images_image_id: "10571"
_4images_cat_id: "1030"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10571 -->
