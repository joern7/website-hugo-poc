---
layout: "image"
title: "Gesamtübersicht"
date: "2012-06-07T13:08:18"
picture: "kippaufzug01.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35040
- /detailsf94f-2.html
imported:
- "2019"
_4images_image_id: "35040"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35040 -->
http://youtu.be/MKkWHJbkXpI