---
layout: "image"
title: "Stufe 5"
date: "2012-06-07T13:08:18"
picture: "kippaufzug07.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35046
- /details8987.html
imported:
- "2019"
_4images_image_id: "35046"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35046 -->
