---
layout: "image"
title: "Stufe 2"
date: "2012-06-07T13:08:18"
picture: "kippaufzug03.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35042
- /details9b22.html
imported:
- "2019"
_4images_image_id: "35042"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35042 -->
