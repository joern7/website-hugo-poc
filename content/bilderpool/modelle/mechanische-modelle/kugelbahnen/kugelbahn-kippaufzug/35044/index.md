---
layout: "image"
title: "Gegengewicht"
date: "2012-06-07T13:08:18"
picture: "kippaufzug05.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35044
- /detailse99d-2.html
imported:
- "2019"
_4images_image_id: "35044"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35044 -->
Wenn der Aufzug wieder kippt, wird die Strebe durch das Gegengewicht nach oben bewegt.