---
layout: "image"
title: "Kugelbahn Version 1 Aufzug von oben"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv09.jpg"
weight: "9"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40867
- /details7b68.html
imported:
- "2019"
_4images_image_id: "40867"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40867 -->
Aufzug von oben mit Abgang zur Dynamic M Kugelbahn. Die zwei Flexschienen enden auf einer umgedrehten Weiche.