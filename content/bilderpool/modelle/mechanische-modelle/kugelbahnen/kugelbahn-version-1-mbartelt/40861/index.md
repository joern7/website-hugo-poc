---
layout: "image"
title: "Kugelbahn Version 1 Gesamtansicht #3"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv03.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40861
- /details2eee.html
imported:
- "2019"
_4images_image_id: "40861"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40861 -->
Hier sieht man, dass die Endabschaltung der BSB links und die Entladevorrichtung etwas überstehen.