---
layout: "image"
title: "Kugelbahn Version 1 Auffangbehälter Kugeln und Aufzug"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv08.jpg"
weight: "8"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40866
- /details820b.html
imported:
- "2019"
_4images_image_id: "40866"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40866 -->
Hier geschieht das Auffangen der entladenen Kugeln und der Transport nach oben.