---
layout: "image"
title: "Kugelaufnahme"
date: "2012-03-02T15:36:26"
picture: "kugelbahn5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34511
- /detailsb03b.html
imported:
- "2019"
_4images_image_id: "34511"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34511 -->
Die Strebe ist schon wieder oben und eine Kugel ist im Aufzug, so geht es jetzt nach oben.