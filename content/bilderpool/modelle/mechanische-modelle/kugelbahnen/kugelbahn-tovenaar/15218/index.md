---
layout: "image"
title: "zwei bällen !"
date: "2008-09-09T22:43:32"
picture: "DSC01573klein.jpg"
weight: "3"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15218
- /detailsd0de.html
imported:
- "2019"
_4images_image_id: "15218"
_4images_cat_id: "2342"
_4images_user_id: "814"
_4images_image_date: "2008-09-09T22:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15218 -->
