---
layout: "image"
title: "der Aufzug"
date: "2008-09-09T22:43:32"
picture: "DSC01575klein.jpg"
weight: "4"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15219
- /detailsa93c-2.html
imported:
- "2019"
_4images_image_id: "15219"
_4images_cat_id: "2342"
_4images_user_id: "814"
_4images_image_date: "2008-09-09T22:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15219 -->
