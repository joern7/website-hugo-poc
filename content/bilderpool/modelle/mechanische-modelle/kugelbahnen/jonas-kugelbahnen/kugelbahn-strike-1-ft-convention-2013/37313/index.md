---
layout: "image"
title: "Probleme beim Einladen der Kugelbahn STRIKE 1 ins Auto - 1"
date: "2013-09-07T12:59:47"
picture: "Probleme-beim-Einladen-1.png"
weight: "12"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37313
- /details2d75-2.html
imported:
- "2019"
_4images_image_id: "37313"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37313 -->
Nun wollte ich versuchen die Kugelbahn ins Auto zu stämmen, doch die Kugelbahn ist zu hoch (90 cm). Auf dem Foto sieht man auch mich, wie ich neben der Kugelbahn sitze.