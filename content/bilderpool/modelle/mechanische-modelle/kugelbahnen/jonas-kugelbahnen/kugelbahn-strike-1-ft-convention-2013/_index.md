---
layout: "overview"
title: "Kugelbahn STRIKE 1 - ft convention Kugelbahn 2013"
date: 2020-02-22T08:16:30+01:00
legacy_id:
- /php/categories/2777
- /categoriese30d.html
- /categoriesb45e.html
- /categories00fa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2777 --> 
Dies ist die Kugelbahn STRIKE 1. Sie ist noch größer und komplizierter als die Kugelbahn V5. Ich werde diese Kugelbahn bei der ft convention 2013 austellen.