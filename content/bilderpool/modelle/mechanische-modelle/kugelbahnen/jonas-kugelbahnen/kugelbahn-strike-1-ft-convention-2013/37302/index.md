---
layout: "image"
title: "Gesamtübersicht 1 der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:46"
picture: "Gesamtuebersicht-1.jpg"
weight: "1"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37302
- /details7364-3.html
imported:
- "2019"
_4images_image_id: "37302"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37302 -->
Die Gesamtübersicht der Kugelbahn STRIKE 1.