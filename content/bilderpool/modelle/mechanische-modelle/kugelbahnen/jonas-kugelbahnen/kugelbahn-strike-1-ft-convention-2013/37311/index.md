---
layout: "image"
title: "Weiche 1 der Kugelbahn Strike 1"
date: "2013-09-07T12:59:47"
picture: "Weiche-1.jpg"
weight: "10"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37311
- /details816f.html
imported:
- "2019"
_4images_image_id: "37311"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37311 -->
Die Weiche 1 der Kugelbahn Strike 1.
Die Weiche wird durch einen Pneumatik Zylinder gesteuert.