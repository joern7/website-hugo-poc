---
layout: "image"
title: "Weiche (umgekehrt) der Kugelbahn V5"
date: "2013-02-11T20:43:15"
picture: "Weiche_umgekehrt.jpg"
weight: "9"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Jonas", "Weiche"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36605
- /detailsf08c.html
imported:
- "2019"
_4images_image_id: "36605"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-11T20:43:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36605 -->
Oben kommen die Kugeln an und die Weiche leitet sie dann zur Schiene vorne.