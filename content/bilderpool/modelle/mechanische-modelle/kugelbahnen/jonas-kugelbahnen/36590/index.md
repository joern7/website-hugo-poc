---
layout: "image"
title: "Aufzug NR1 und erste Kurvenstrecke---Kugelbahn V5"
date: "2013-02-09T11:50:06"
picture: "Kugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Jonas", "Aufzug"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36590
- /details8561.html
imported:
- "2019"
_4images_image_id: "36590"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-09T11:50:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36590 -->
Hier ist der erste Aufzug der Kugelbahn.
Er ist ohne Kette gebaut.