---
layout: "image"
title: "Übersicht 2 der kugelbahn V5 (noch im Bau)"
date: "2013-02-11T20:43:15"
picture: "bersicht2.jpg"
weight: "8"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Übersicht", "Jonas"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36604
- /details1e53.html
imported:
- "2019"
_4images_image_id: "36604"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-11T20:43:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36604 -->
Die Übersicht der Kugelbahn V5.