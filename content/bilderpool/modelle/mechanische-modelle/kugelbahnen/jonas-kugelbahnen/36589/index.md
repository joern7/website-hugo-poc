---
layout: "image"
title: "Kugelbahn V5 (noch im Bau)"
date: "2013-02-09T11:50:05"
picture: "Kugelbahn1.jpg"
weight: "1"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Jonas"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36589
- /details7bc1.html
imported:
- "2019"
_4images_image_id: "36589"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-09T11:50:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36589 -->
Hier ist meine neue große Kugelbahn.
Dort will ich viel Pneumatic & co einbauen.
Ich habe bereits eine Zweispuriege Flexischienenstrecke gebaut, aber noch nicht fotografiert.