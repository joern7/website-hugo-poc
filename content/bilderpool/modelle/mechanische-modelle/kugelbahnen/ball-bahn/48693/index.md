---
layout: "image"
title: "Antrieb (1)"
date: 2020-05-10T11:37:58+02:00
picture: "2019-10-20 Ball-Bahn04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein 50:1-PowerMotor treibt den Aufzug an. Über die vielen Z20, die alle auf mit Vaseline leichtgängig gemachten Achsen laufen, geht der Antrieb auch auf die andere Turmseite zum zweiten Kettenpaar.

Mit nur einer Kette je Seite funktionierte der Aufzug gar nicht gut: Die Mitnehmer konnten sich durch nicht genau mittig sitzende Bälle neigen, an den Bällen vorbeirutschen und so den ganzen Aufzug verklemmen. Erst mit zwei Kettenpaaren waren die Mitnehmer hinreichend fix waagerecht.