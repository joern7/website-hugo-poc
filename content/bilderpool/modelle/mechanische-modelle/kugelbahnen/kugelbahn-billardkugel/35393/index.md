---
layout: "image"
title: "02 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35393
- /detailse86b-3.html
imported:
- "2019"
_4images_image_id: "35393"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35393 -->
Der untere Teil