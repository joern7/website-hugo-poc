---
layout: "image"
title: "03 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35394
- /detailsf1ce-3.html
imported:
- "2019"
_4images_image_id: "35394"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35394 -->
Das ist der Bereich, in den die Kugel fällt, wenn sie aus dem Lift kommt. 

Zum bessern Verständnis das Video schaun (ab 0:45): http://www.youtube.com/watch?v=dtplrOt1AjY