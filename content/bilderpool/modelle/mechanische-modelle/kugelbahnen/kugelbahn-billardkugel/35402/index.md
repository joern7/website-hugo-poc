---
layout: "image"
title: "11 Kugelbahn"
date: "2012-08-28T22:30:16"
picture: "kugelbahn11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35402
- /details7004.html
imported:
- "2019"
_4images_image_id: "35402"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35402 -->
Ein Video gibt es hier: http://www.youtube.com/watch?v=dtplrOt1AjY