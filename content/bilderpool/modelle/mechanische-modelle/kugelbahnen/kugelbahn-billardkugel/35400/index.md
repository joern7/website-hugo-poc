---
layout: "image"
title: "09 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35400
- /details91c7.html
imported:
- "2019"
_4images_image_id: "35400"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35400 -->
Interface + Motor