---
layout: "image"
title: "Gemeinschaftsmodell - Kugelbahn"
date: "2012-05-29T10:02:15"
picture: "kugelbahn6.jpg"
weight: "7"
konstrukteure: 
- "tobs9578 & Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35021
- /detailsee4a.html
imported:
- "2019"
_4images_image_id: "35021"
_4images_cat_id: "2591"
_4images_user_id: "1162"
_4images_image_date: "2012-05-29T10:02:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35021 -->
Der rechte Streckenteil mit "Werbeschild".