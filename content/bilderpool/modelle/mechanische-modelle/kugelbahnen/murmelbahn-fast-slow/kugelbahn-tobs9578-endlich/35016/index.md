---
layout: "image"
title: "Gemeinschaftsmodell - Kugelbahn"
date: "2012-05-29T10:02:15"
picture: "kugelbahn1.jpg"
weight: "2"
konstrukteure: 
- "tobs9578 & Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35016
- /details96ef.html
imported:
- "2019"
_4images_image_id: "35016"
_4images_cat_id: "2591"
_4images_user_id: "1162"
_4images_image_date: "2012-05-29T10:02:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35016 -->
Fischertechnik verbindet - Zu Besuch bei Tobias Horst (tobs9578). Zusammen haben wir eine Kugelbahn für die Convention 2012 gebaut. 
Ziel war es die komplette Murmelbahn möglichst auf eine Bauplatte zu packen, naja etwas anbauen mussten wir trotzdem. 
Seht doch aber einfach selbst!

MfG
Endlich & tobs9578

P.S. Ein Video gibt es auch noch http://www.youtube.com/watch?v=_MtKTozVDhk&feature=g-u-u