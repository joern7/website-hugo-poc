---
layout: "image"
title: "Gesamtansicht von oben"
date: "2011-08-02T19:13:39"
picture: "murmelbahnfastslow01.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/31503
- /details17b2-2.html
imported:
- "2019"
_4images_image_id: "31503"
_4images_cat_id: "2340"
_4images_user_id: "1007"
_4images_image_date: "2011-08-02T19:13:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31503 -->
Die Mumelbahn habe ich mit Hilfe des neuen Kastens Dynamics entworfen.
Es wurden alle Flex-Schienen verbaut.