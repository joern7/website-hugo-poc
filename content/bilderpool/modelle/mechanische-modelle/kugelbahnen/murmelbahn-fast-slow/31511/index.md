---
layout: "image"
title: "Die Zusammenführung"
date: "2011-08-02T19:13:39"
picture: "murmelbahnfastslow09.jpg"
weight: "9"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/31511
- /detailsc001-2.html
imported:
- "2019"
_4images_image_id: "31511"
_4images_cat_id: "2340"
_4images_user_id: "1007"
_4images_image_date: "2011-08-02T19:13:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31511 -->
Hier kommen die Murmeln beider Bahen heraus und werden zum Aufzug geleitet.