---
layout: "image"
title: "Kugelbahn mit Pendelaufzug"
date: "2014-06-19T19:13:42"
picture: "kugelbahnmitpendelaufzug02.jpg"
weight: "2"
konstrukteure: 
- "Ritterrost"
fotografen:
- "Ritterrost"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/38951
- /detailsa63f.html
imported:
- "2019"
_4images_image_id: "38951"
_4images_cat_id: "2916"
_4images_user_id: "1129"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38951 -->
Gesamtansicht (etwas näher)
Links der Turm für den Pendelaufzug 
Rechts der Turm für die Etagen und die Kugelbahn