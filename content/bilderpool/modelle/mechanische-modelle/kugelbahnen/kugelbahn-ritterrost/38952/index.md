---
layout: "image"
title: "Kugelbahn mit Pendelaufzug"
date: "2014-06-19T19:13:42"
picture: "kugelbahnmitpendelaufzug03.jpg"
weight: "3"
konstrukteure: 
- "Ritterrost"
fotografen:
- "Ritterrost"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/38952
- /details0997.html
imported:
- "2019"
_4images_image_id: "38952"
_4images_cat_id: "2916"
_4images_user_id: "1129"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38952 -->
Gesamtansicht (etwas näher)