---
layout: "image"
title: "Flexschienenbahn"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34423
- /details4344.html
imported:
- "2019"
_4images_image_id: "34423"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34423 -->
Flexschienenbahn