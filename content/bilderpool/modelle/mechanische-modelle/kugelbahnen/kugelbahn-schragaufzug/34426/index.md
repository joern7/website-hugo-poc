---
layout: "image"
title: "Die Rückseite Obererteil der Kubelbahn"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34426
- /detailsf2ac.html
imported:
- "2019"
_4images_image_id: "34426"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34426 -->
Die Rückseite Obererteil der Kubelbahn