---
layout: "image"
title: "Umlenkung in den Schrägaufzug"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34428
- /details41c6.html
imported:
- "2019"
_4images_image_id: "34428"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34428 -->
Umlenkung in den Schrägaufzug