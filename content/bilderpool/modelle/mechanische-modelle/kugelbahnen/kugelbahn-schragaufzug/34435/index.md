---
layout: "image"
title: "Auswurf aus den Schrägaufzug II"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34435
- /detailsb3e7-2.html
imported:
- "2019"
_4images_image_id: "34435"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34435 -->
Auswurf aus den Schrägaufzug II