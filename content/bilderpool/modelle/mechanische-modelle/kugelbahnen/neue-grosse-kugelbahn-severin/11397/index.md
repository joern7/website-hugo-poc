---
layout: "image"
title: "Kugelbahn"
date: "2007-08-20T16:42:05"
picture: "neuegrossekugelbahn3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/11397
- /details2f1e.html
imported:
- "2019"
_4images_image_id: "11397"
_4images_cat_id: "1021"
_4images_user_id: "558"
_4images_image_date: "2007-08-20T16:42:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11397 -->
