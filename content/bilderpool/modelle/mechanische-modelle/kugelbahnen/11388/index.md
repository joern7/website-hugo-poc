---
layout: "image"
title: "Aufzug für Cuborokugelbahn im Dauerbetrieb"
date: "2007-08-17T13:39:27"
picture: "Kugelbahn.jpg"
weight: "3"
konstrukteure: 
- "Yvo"
fotografen:
- "Yvo"
keywords: ["Kugelbahn", "Aufzug", "Cuboro"]
uploadBy: "Yvo"
license: "unknown"
legacy_id:
- /php/details/11388
- /details7049-2.html
imported:
- "2019"
_4images_image_id: "11388"
_4images_cat_id: "1030"
_4images_user_id: "641"
_4images_image_date: "2007-08-17T13:39:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11388 -->
Der Aufzug transport die Kugel zum Beginn der Kugelbahn, die dadurch endlos läuft.