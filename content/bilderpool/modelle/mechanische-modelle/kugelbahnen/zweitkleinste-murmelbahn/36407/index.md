---
layout: "image"
title: "Zweitkleinste Murmelbahn"
date: "2013-01-04T17:11:55"
picture: "zweitkleinstemurmelbahn1.jpg"
weight: "1"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36407
- /detailsd028.html
imported:
- "2019"
_4images_image_id: "36407"
_4images_cat_id: "2703"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T17:11:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36407 -->
Nach Bernhard Lehners kleinster Murmelbahn aus dem Adventskalender 2012, jetzt die zweitkleinste Murmelbahn, bei der das Rad sogar auf zwei Achsen gekippt wird