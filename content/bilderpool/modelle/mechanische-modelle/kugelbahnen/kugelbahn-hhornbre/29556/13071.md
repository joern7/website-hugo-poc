---
layout: "comment"
hidden: true
title: "13071"
date: "2010-12-31T21:00:52"
uploadBy:
- "HHornBre"
license: "unknown"
imported:
- "2019"
---
Hallo Euch allen - und vielen Dank für die vielen Kommentare.

Freut mich, dass Euch die Kugelbahn gefällt.

Die etwas "wilde" Farbgebung ist schlicht dem Umstand geschuldet, dass ich zum Zeitpunkt meines Wiedereinstiegs noch nicht genügend Teile in einheitlicher Farbe hatte.

Die Bilder sind mit indirektem Blitzen entstanden.
Die Bewegungen sind mit der Stroboskop Funktion des Nikon SB800 gemacht.

Soweit - viele Grüße aus Bremen & Guten Rutsch

Holger