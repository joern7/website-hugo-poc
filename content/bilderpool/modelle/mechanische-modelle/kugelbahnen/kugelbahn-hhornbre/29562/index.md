---
layout: "image"
title: "FT-Kugelbahn_HH_07"
date: "2010-12-31T10:59:39"
picture: "FT-Kugelbahn_HH_07.jpg"
weight: "7"
konstrukteure: 
- "Holger Horn"
fotografen:
- "Holger Horn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HHornBre"
license: "unknown"
legacy_id:
- /php/details/29562
- /detailscf47-3.html
imported:
- "2019"
_4images_image_id: "29562"
_4images_cat_id: "2151"
_4images_user_id: "1254"
_4images_image_date: "2010-12-31T10:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29562 -->
