---
layout: "image"
title: "Kugelbahn"
date: "2016-12-10T21:29:02"
picture: "P1040714.jpg"
weight: "15"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44884
- /details3eb9.html
imported:
- "2019"
_4images_image_id: "44884"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44884 -->
