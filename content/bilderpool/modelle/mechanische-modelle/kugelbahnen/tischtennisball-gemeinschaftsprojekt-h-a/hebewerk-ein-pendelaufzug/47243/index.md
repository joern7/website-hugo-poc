---
layout: "image"
title: "Hebewerk - Stirnansicht"
date: "2018-02-04T20:58:46"
picture: "hebewerk2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Gemeinschaftsprojekt", "Tischtennisballmaschine", "Hebewerk", "Pendelaufzug"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47243
- /details759d-2.html
imported:
- "2019"
_4images_image_id: "47243"
_4images_cat_id: "3496"
_4images_user_id: "1557"
_4images_image_date: "2018-02-04T20:58:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47243 -->
Von hinten (unten) rollen die Bälle rein, rollen von der mittleren Wanne (hier verdeckt) aus nach links oder nach rechts, je nachdem welcher Trog gerade unten ist und rollen oben von der jeweiligen Wanne wieder in die Mitte und dann nach vorne auf die Rampe. Die Rampe macht den Aufzug zur universellen Ecke denn sie ist um +/-95° schwenkbar.

Der komplette Innenraum wird von den Trögen, der Wanne, den Schienen und den Tischtennisbällen beansprucht.

Der Maltesersatz oben dreht die Schaltwelle schrittweise. Eine Umdrehung der Schaltwelle steuert den kompletten Zyklus.