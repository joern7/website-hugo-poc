---
layout: "image"
title: "Arduino UNO R3"
date: "2016-12-10T21:29:02"
picture: "P1040722.jpg"
weight: "20"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Arduino", "Steuercomputer"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44889
- /detailsf575.html
imported:
- "2019"
_4images_image_id: "44889"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44889 -->
Der Steuercomputer.

Das Interface Modul und die Karte für die Motorsteuerung sind für diese Aufnahme abgezogen worden.