---
layout: "image"
title: "Pneumatik-Schieber (1)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46803
- /details4c07.html
imported:
- "2019"
_4images_image_id: "46803"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46803 -->
Drei identische Mechaniken schieben den Ball jeweils ein langes Stück vor und etwas nach oben. Die Pneumatik-Zylinders sind *in* dem Gestänge untergebracht. Das ergibt eine ganz reizvolle Kinematik, die den nicht sehr großen Hub der Zylinder in eine weit ausladende Bewegung übersetzt.