---
layout: "image"
title: "Korbwerfer (5)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46813
- /details2be9-2.html
imported:
- "2019"
_4images_image_id: "46813"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46813 -->
Ein grauer 1:20-PowerMotor treibt das linke Z40 an, mit dem die Kurvenscheibe per eingesteckter Metallachse 30 kraftschlüssig verbunden ist. Die 12 Zähne der Kurvenscheibe greifen ins rechte Z40 und drehen das also, bis die 12 Zähne "durch" sind. Dann schnalzt das rechte Z40 zurück. Für einen Wurf dreht sich das linke Z40 genau ein Mal.

Mit dem Taster im Vordergrund kann man einen Wurfzyklus manuell auslösen.