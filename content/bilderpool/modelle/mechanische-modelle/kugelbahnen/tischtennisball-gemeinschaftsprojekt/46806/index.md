---
layout: "image"
title: "Pneumatik-Schieber (4)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46806
- /details1285-2.html
imported:
- "2019"
_4images_image_id: "46806"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46806 -->
Ursprünglich sollte eine Schaltung aus Festo-Ventilen in den Endlagen und als Logikelementen sowie Betätigern die Maschine immer mit voller Geschwindigkeit betreiben, und das Erkennen, ob ein Ball da ist, sollte mit einer ganz feinen pneumatischen Waage mit einer Selbstbau-Staudüse wie in ft:pedia 2014-3 Seite 21 - 31 geschehen. Zwei Dinge verhinderten das:

- Es war wiedermal zu wenig Luft da (in L/min). Meinen großen Kompressor konnte ich aber nicht verwenden, weil die Lautstärke mitten in der Convention-Halle unzumutbar gewesen wäre.

- Die Waage sprach zwar fein an, aber leider zu langsam.

Also gab es Ersatz in dieser simplen Zeitablaufs-Steuerung: Der Motor, dessen Drehzahl am PowerBlock regelbar ist, treibt die beiden Schalträder an, die auf je ein Festo-Ventil (Öffner) gehen. Eines davon steuert über einen Doppelbetätiger und zwei Ventile (Öffner + Schließer) die Zylinder 1 und (antiparallel) 3 an, das andere auf dieselbe Art Zylinder 2. Die Sekundärventile (einen Block sieht man rechts neben dem Z30, wo man durchgucken kann) sind möglichst nahe bei den Zylindern eingebaut, damit von dort möglichst kurze Schläuche zu den Zylindern gehen und die Bewegungen also möglichst schnell gehen (weil nicht unnötig viel Volumen gefüllt werden muss).