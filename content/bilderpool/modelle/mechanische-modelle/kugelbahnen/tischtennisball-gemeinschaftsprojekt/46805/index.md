---
layout: "image"
title: "Pneumatik-Schieber (3)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46805
- /details0547.html
imported:
- "2019"
_4images_image_id: "46805"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46805 -->
Der erste und der letzte Zylinder sind antiparallel geschaltet. Das Schieben geht so vor sich:

Grundstellung: Schieber 1 (links) und 2 (Mitte oben) sind eingefahren, 3 (rechts) ausgefahren.

Erster Hub: 1 ausgefahren (schiebt denn Ball zu 2), 3 eingefahren (macht Platz für den gleich von 2 kommenden Ball).

Zweiter Hub: 2 ausgefahren (schiebt den Ball zu 3).

Dritter Hub: 3 ausgefahren (schiebt den Ball zum Ausgang) und 1 eingefahren (macht Platz für den nächsten Ball).

Vierter Hub: 2 wird wieder eingefahren (Rückkehr zur Grundstellung).