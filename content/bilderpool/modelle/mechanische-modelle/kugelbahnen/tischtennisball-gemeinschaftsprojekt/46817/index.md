---
layout: "image"
title: "Korbwerfer (9)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46817
- /detailsb860.html
imported:
- "2019"
_4images_image_id: "46817"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46817 -->
Damit auch bei Staus im Korb alle Bälle zuverlässig raus dürfen, aber nicht zu viele auf einmal zum nächsten Modul gelangen (was das vielleicht gar nicht verkraftet), heben und senken sich die Hebel hier also mit der 1,5-fachen Frequenz des Wurfs. Damit kommen Bälle garantiert heraus, aber nicht in zu schneller Abfolge fürs nächste Modul.

Die Schienen zwischen den beiden Modulen sind herausnehmbare Testschienen, damit ich die beiden Module im Dauerbetrieb testlaufen lassen konnte. Unten sieht man die blauen Flügel eines Ventilators, der den Bällen etwas mehr Begeisterung verleiht, sich über die flache Strecke zum Pneumatikschieber-Modul zu bewegen.