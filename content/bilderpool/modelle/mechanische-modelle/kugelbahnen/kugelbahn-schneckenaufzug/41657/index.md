---
layout: "image"
title: "Kugel im Auswurf von oben - etwas weiter"
date: "2015-07-29T11:02:34"
picture: "kbmsa10.jpg"
weight: "10"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41657
- /detailse8c5.html
imported:
- "2019"
_4images_image_id: "41657"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41657 -->
Wieder der gleiche Stand von oben