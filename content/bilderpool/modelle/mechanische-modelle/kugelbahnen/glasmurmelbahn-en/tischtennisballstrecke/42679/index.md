---
layout: "image"
title: "Der Tischtennisballheber - Einlaßseite"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42679
- /details513e.html
imported:
- "2019"
_4images_image_id: "42679"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42679 -->
Weiter oben als Heberad für Tischtennisbälle angesprochen (oder kurz TTB-Heber) könnte man es auch als Stachelwalze bezeichnen. Allzu neu ist die Idee mit den Achsen in den Drehscheiben nicht. Irgendwo in den tiefen des Netzes gibt es Bilder oder Videos einer Konstruktion, die Clipsachsen in den Drehscheiben für genau diese Funktion verwendet.

Eine halbkreisförmige Führung für die Tischtennisbälle ermöglicht es die Hubhöhe voll auszunutzen. Die Statiklaschen sind weniger Deko, sie stabilisieren die Konstruktion gegen Verschieben. Die querliegenden Nuten der WS und die Zapfen der BS halten ohne Nachhilfe nicht so supertoll. Die Handhabung beim Transport ist ziemlich beschwerlich und hinterher ist dann doch was rausgerutscht. Dank der Laschen verschiebt sich da nichts mehr. Als Ornament ist das auch noch ungewöhnlich anzusehen.

----

The ping-pong-ball lifter - inlet side

Somewhere deep in the web there is already a construction handling ping-pong-balls that uses this approach of small axles in the slotted disks.

This special setup with its semi-circular guiding allows to lift the ping-pong-balls full height. The sides of the semi-circular guide are stabilized by clips. Otherwise the building parts might slide off their tongue and groove joints. It also gives a nice ornament.