---
layout: "image"
title: "Umlenkung per Kipper"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn9.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42682
- /details2397.html
imported:
- "2019"
_4images_image_id: "42682"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42682 -->
Vor dem Trichter ist im Anbau der Kipper montiert. Die Tischtennisbälle kommen links oben an, fallen in den Kipper, werden dadurch langsam nach unten gereicht und laufen auf der unteren Etage nach links wieder weg. Der Kipper wird nur durch die Gewichtskraft der Tischtennisbälle gesteuert.

Auch hier dürfte ein Blick auf Rob's geniales Video viele Fragen der Funktion klären:
https://youtu.be/Nyy7FhNCYv8?t=551

----

Reflecting by a tippler

In front of the funnel there is the tippler located. The ping-pong-balls enter at the upper track from the left side. The tippler gently lowers them to the lower track back to the left side. Just the weight of the ping-pong-balls triggers the tippler.

Just have a look at the video of Rob van Baal: https://youtu.be/Nyy7FhNCYv8?t=551