---
layout: "image"
title: "Steckbare Wellenverbindung"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42678
- /detailsb5c6-2.html
imported:
- "2019"
_4images_image_id: "42678"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42678 -->
Hier noch ein schärferes Bild der Wellenlager am ZickZack-Turm mit der steckbaren Wellenkupplung. Ursprünglich von ihrem Schöpfer als Längenausgleich propagiert, macht sie sich auch noch als ebensolcher hier nützlich. Wenn nur die V-Steine nicht so selten wären...

----

Pluggable shaft joint

The small black part forms a kind of mechanical receptacle. It is mounted to one end of a metall rod. The next section is a cardan joint (the ligth grey part). Both parts fit perfectly into each other and permit a from-fit transmission of the momentum. In case of a transport of the machine this joint can easily be removed and rejoined.