---
layout: "image"
title: "Die Tischtennisballstrecke"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn8.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42681
- /details1c58.html
imported:
- "2019"
_4images_image_id: "42681"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42681 -->
Am besten erkennt man sie auf der Aufnahme der neuen Hebekunst. In einem eleganten Bogen von rechts hinter der Hebekunst nach rechts vorne. Rechts ausserhalb des Bildes ist die Umlenkung montiert, von dort schräg zurück in den TTB-Heber.

----

The ping-pong-ball track

The best it is seen on a picture of the new Hebekunst (another lifter for glas marbles). It is coming from behind the Hebekunst bending around it and leading to the right at the front. There (outside of this picture) a small device reflects the ping-pong-balls back. Again a bend guides them back to their lifter.