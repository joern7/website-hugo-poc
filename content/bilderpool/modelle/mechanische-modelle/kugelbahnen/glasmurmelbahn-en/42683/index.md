---
layout: "image"
title: "Glasmurmelbahn 'A Tribute To Imperfection'"
date: "2016-01-08T13:30:54"
picture: "uebersichten1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42683
- /details4467.html
imported:
- "2019"
_4images_image_id: "42683"
_4images_cat_id: "3146"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42683 -->
Gebaut für die ft Community Convention 2014 in Erbes-Büdesheim.

Build for the ft Community Convention 2014 at Erbes-Büdesheim.