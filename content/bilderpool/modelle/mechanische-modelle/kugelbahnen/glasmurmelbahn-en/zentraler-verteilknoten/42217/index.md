---
layout: "image"
title: "Befestigungsdetail an der Unterseite"
date: "2015-11-06T21:29:57"
picture: "pic2.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42217
- /detailsa775.html
imported:
- "2019"
_4images_image_id: "42217"
_4images_cat_id: "3148"
_4images_user_id: "1557"
_4images_image_date: "2015-11-06T21:29:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42217 -->
Die Position des Verteilers war duch den Vorgänger vorgegeben. Eine Änderung hätte eine Neuanfertigung der 4 Streckenabschnitte bedeutet. Somit war die Montagepostition anzupassen. Normalerweise kein großer Aufwand, jedoch sind überall Inneneinbauten im Weg (hier die Winkel 38423). Kürzen der Winkel, um BS7,5 verwenden zu können,  wäre möglich, ist aber nicht erwünscht. Die Befestigung nach aussen zu legen ist optisch unschön.

Zwei Klemmhülsen 35980 bringen mit ihren 7,5mm den BS30 an die richtige Stelle ohne daß das Schnitzmesser die Winkel verletzen muß. Die Befestigung ist optisch unauffällig innenliegend. Die Zapfen der Federnocken kommen genau passend in die Löcher der Grundplatte.

----

Some details on how the mounting of the new device has been adapted the the footprint of the old one.