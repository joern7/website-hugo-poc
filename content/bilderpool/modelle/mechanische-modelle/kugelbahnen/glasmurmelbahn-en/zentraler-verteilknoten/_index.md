---
layout: "overview"
title: "Zentraler Verteilknoten"
date: 2020-02-22T08:16:46+01:00
legacy_id:
- /php/categories/3148
- /categoriese442.html
- /categories104f-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3148 --> 
Zwei Teilkreisläufe treffen im zentralen Verteiler zusammen und werden dort auch wieder aufgetrennt.