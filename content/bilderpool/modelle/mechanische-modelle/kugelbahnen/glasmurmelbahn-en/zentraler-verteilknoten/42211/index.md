---
layout: "image"
title: "Vergleich neu <=> alt"
date: "2015-11-03T19:35:57"
picture: "pic3.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42211
- /detailsf44f-2.html
imported:
- "2019"
_4images_image_id: "42211"
_4images_cat_id: "3148"
_4images_user_id: "1557"
_4images_image_date: "2015-11-03T19:35:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42211 -->
Links der Neue, rechts der Alte. Beide sind auf dem Foto identisch ausgerichtet, soll heißen: die ankommenden Bahnen von links und rechts, die abgehenden Bahnen nach oben und unten.

Der alte Verteiler war zum Schluß experimentell auf optimale Funktion gebracht worden. Das war wacklig, optisch unschön aber immerhin funktionsfähig.

----

 **Central distributor:**
On the left hand side there is the new version, on the right hand side the old one. Both have the same orientation on the picture. The incoming tracks from left and right, the outgoing tracks to top and bottom.

The old version had some issues and after getting it to optimum work this was somewhat rickety and ugly.