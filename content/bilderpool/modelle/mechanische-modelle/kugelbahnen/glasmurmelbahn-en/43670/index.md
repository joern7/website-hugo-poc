---
layout: "image"
title: "Glasmurmelbahn 'Zwanzig Sechzehn'"
date: "2016-06-03T15:50:43"
picture: "P1050301.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Kugelbahn", "marble", "run", "marble", "coaster"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43670
- /details8b8c.html
imported:
- "2019"
_4images_image_id: "43670"
_4images_cat_id: "3146"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43670 -->
So stand sie dann bei der Maker Faire 2016 in Hannover. Mit Hebekunst und neuer Deko sowie ein paar technischen Verbesserungen an einigen Details.