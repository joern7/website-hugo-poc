---
layout: "image"
title: "Befestigung des Fundaments"
date: "2016-06-03T15:50:44"
picture: "leuchtturm3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43673
- /detailsc26b.html
imported:
- "2019"
_4images_image_id: "43673"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43673 -->
Natürlich soll der Sockel an Ort und Stelle bleiben. Dafür sorgen die Streben mit denen er rundum am Sockel des Oktogons befestigt ist. Wird der Leuchtturm einzeln aufgestellt, ist keine Grundplatte nötig. Der Fundamentring trägt sich und den Turm problemlos.

----

To keep the foundation in place some struds lead to the inner foundation of the track supports. This foundation is fixed to the base plate only.

If the lighthouse is used stand-alone there is no base plate needed.