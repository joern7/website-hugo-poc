---
layout: "image"
title: "Optikantrieb"
date: "2016-06-03T15:50:44"
picture: "leuchtturm5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["leuchtturm", "antrieb", "schneckengetriebe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43675
- /details29e0.html
imported:
- "2019"
_4images_image_id: "43675"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43675 -->
Auf dem Weg nach oben sitzt der XS-Motor mit Getriebe. Dies ist der Antrieb für die drehende Optiktrommel.

----

Half-way to the top there is a small motor to turn the optic drum. A gear mechanisms reduces the speed of the output shaft.