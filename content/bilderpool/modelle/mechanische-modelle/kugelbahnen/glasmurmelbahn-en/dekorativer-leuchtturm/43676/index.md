---
layout: "image"
title: "Die Optik mit Wartungsplattform"
date: "2016-06-03T15:50:44"
picture: "leuchtturm6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43676
- /detailsc79b.html
imported:
- "2019"
_4images_image_id: "43676"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43676 -->
Durch die drehende Optiktrommel werden entsprechende Lichtblitze in die Umgebung gestrahlt. Dazu ist die Originalkonstruktion etwas verändert worden um die Glühlampe (6V / 0,7W) möglichst genau im Fokus der Linsen unterzubringen. Erfreulicherweise geht es ohne Schleifringe und ohne Hohlwelle. Die Lampe steht auf einem festen Sockel und nur die Optik dreht sich um sie herum - wie bei einem echten Leuchtturm auch.

Das Geländer besteht aus 24 Gelenklaschen. Um den Umfang zu treffen sitzen sie abwechselnd innen und aussen.

Der Leuchtturmwärter macht gerade mal Pause und genießt die Aussicht.

----

Here we have the platform and the optics drum. A small light bulb (6V / 0.7W) together with the lenses sends narrow light beams into the environement. By the rotation of the optic drum this gives short flashes of light to observers.

The lighthouse keeper just has a break.