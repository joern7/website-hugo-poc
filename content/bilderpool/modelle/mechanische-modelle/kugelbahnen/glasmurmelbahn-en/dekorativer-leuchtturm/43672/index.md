---
layout: "image"
title: "Das Fundament"
date: "2016-06-03T15:50:44"
picture: "leuchtturm2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43672
- /detailsa212.html
imported:
- "2019"
_4images_image_id: "43672"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43672 -->
Jedes Bauwerk steht nur so gut wie sein Fundament. Der Sockel ist ein achteckiger Ring auf den die Last des Turms verteilt wird. Die grauen Winkelträger vom alten Baukran hübschen das optisch auf und unterstreichen die robuste Konstruktion. Der Fundamentring liegt ohne eigene Befestigung auf der Grundplatte.

----

This is the foundation of the lighthouse. The weight of the tower is led into the octogonal ring. This ring just lies on the base plate.