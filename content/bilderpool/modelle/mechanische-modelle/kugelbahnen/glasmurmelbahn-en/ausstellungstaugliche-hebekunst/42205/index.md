---
layout: "image"
title: "Halbbogen - Detailansicht"
date: "2015-11-02T21:20:35"
picture: "P1040198.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42205
- /details72c4.html
imported:
- "2019"
_4images_image_id: "42205"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T21:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42205 -->
Das Arrangement erzielt genau den nötigen Radius um die Kette am BS15 glatt anliegend abzuholen. Laut einem alten Clubheft liegt die Bruchlast einer ft-Kette jenseits der 30N (grob 3kg frei hängend).

----

Details of the part arrangement to get the radius of the arc as precise as possible.