---
layout: "image"
title: "Gesamtansicht"
date: "2015-11-06T18:51:41"
picture: "P1040412_pub.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42215
- /details19d1-2.html
imported:
- "2019"
_4images_image_id: "42215"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-06T18:51:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42215 -->
Und so sieht sie derzeit aus. Aus dem bunt zusammengesteckten Protoyp ist ein stabiler Aufbau geworden, der nur noch gelegentlich mal eine Murmel danebenwirft. Viele Bauteile der alten zickigen Hebekunst sind wieder mit dabei.

Das Getriebe sitzt rechts unten, im Vordergrund quer ist der Pneumatikzylinder zu sehen. Die P-Betätiger an den Murmeleinlässen spielen auch wieder mit. Die haben jeder nun so etwa eine halbe Million (kein Witz!) Hübe hinter sich und funktionieren immer noch ohne Probleme.

----

A complete view o the "Hebekunst" marble lifting device. On the right bottom side the robust gear with the motor is located. A big massive crank forces the piston rod to move up and down and swing the lever at the top. Lever swing causes the two lifting rods to mututally swing up and down in opposite directions.

In the front the pneumatic drive to the marble inlets does its job. It is coupled to the motor and turns synchronously to the lifting rod movement.

On the top two outgoing tracks are visible. One in front, the other behind, they both leave to a simple combiner that colletcs all marbles to the same main track. The lifter device works double sided!