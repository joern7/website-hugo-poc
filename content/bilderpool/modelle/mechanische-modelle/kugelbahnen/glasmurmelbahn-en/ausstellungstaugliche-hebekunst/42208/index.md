---
layout: "image"
title: "Kettenumlenkung"
date: "2015-11-02T21:20:35"
picture: "P1040194.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42208
- /details9e06-3.html
imported:
- "2019"
_4images_image_id: "42208"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T21:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42208 -->
Vom Hebel mit dem Bogen muß die Kette zu den eng beieinanderliegenden Hubstangen umgelenkt werden. Freilauf-Z15 geben ungefähr das richtige Maß. Metallachsen erzeugen das geringste Reibmoment und sind zu bevorzugen. Auch hier ist ein Hauch Vaseline angeraten.

Die S-Aufnahmestreifen (35771) sichern die Bausteine gegen verrutschen in den Nuten, ohne selbst allzu dick aufzutragen.

Das Freilauf-Z15 (35695) wird von der Kette an der optimalen Position gehalten.

----

How to get the chains from the top lever closer to each other to move the lifting rods: a pulley (here a 15 teeth gear) gently leads the chains form the levers arc to the new distance.