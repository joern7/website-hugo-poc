---
layout: "image"
title: "Mittelstück mit Querung - linker Turm"
date: "2016-01-08T13:30:55"
picture: "zickzack5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42693
- /details95e5.html
imported:
- "2019"
_4images_image_id: "42693"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42693 -->
In einer Lücke zwischen den Etagen quert eine andere Strecke das ZickZack. Dies spart nicht nur eine zusätzliche Stützkonstruktion, der nötige Winkel für die Steilkurve ergibt sich so ohne weiteres Zutun durch geschickte Nutzung der Geometrie.

Und weil der Turm schon so schön da in der Gegend steht, muß er sich noch ein drittes mal nützlich machen: Die Antriebswelle für die Tischtennisballstrecke (http://www.ftcommunity.de/categories.php?cat_id=3175) trägt er auch gleich noch.

----

Middle section with transversal - left side tower

Just a gap between the levels gives way for a transversal track. Thus there is no necessity for another track support and the banking of the track just comes from the geometry already given.

And since this tower is just there where it is, it can take a third usage. It supports the drive shaft for the ping-pong-ball track (http://www.ftcommunity.de/categories.php?cat_id=3175).

