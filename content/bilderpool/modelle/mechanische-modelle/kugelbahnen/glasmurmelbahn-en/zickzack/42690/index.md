---
layout: "image"
title: "Übersicht der Bahnen"
date: "2016-01-08T13:30:54"
picture: "zickzack2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42690
- /detailsf3b0.html
imported:
- "2019"
_4images_image_id: "42690"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42690 -->
Wie schon in der Einleitung beschrieben: Immer hin und her mit den kleinen Kullerchen. Sehr schön zu sehen ist hier die um 45° verdrehte Anordnung auf der Grundplatte. Im ZickZack sind Holzstäbe anstelle der Aluminiumschienen verbaut. Das dämpft die Geräusche besser.

Andere Strecken überqueren oder durchqueren das ZickZack.

----

Overview of the tracks

As already written in the introduction the marbles find their way down in a zig zag. Quite easily to see here is the 45° angle of the whole construction. The ZigZag tracks are made of wood instead of aluminium. Wood gives less noise.

Other tracks run over or through the ZigZag.