---
layout: "image"
title: "Looping"
date: "2016-06-03T15:50:44"
picture: "looping1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43678
- /details2f73.html
imported:
- "2019"
_4images_image_id: "43678"
_4images_cat_id: "3233"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43678 -->
Zum Looping selbst gibt es nichts zu sagen.

Das Haltegestell war nötig weil nur eine Befestigung im Zenith die Schienen fest genug in Position hält. Ohne Befestigung im Zenith drücken die Murmeln die Schienen auseinander und die Murmeln fliegen raus.

----

Just a simple looping.

The only drawback is the supporting frame construction. It is needed to get a stable track mount into the lopping's zenith. Otherwise the marbles' centrifugal force pushes the tracks apart and the marbles then fly out.