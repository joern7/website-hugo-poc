---
layout: "image"
title: "Trichter und Umgebung"
date: "2016-01-08T13:30:54"
picture: "trichter1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42685
- /details2ae5.html
imported:
- "2019"
_4images_image_id: "42685"
_4images_cat_id: "3176"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42685 -->
So steht er im Schienengewirr. Die Zuführung kommt, teile- und türmesparend, durch den vorderen ZickZackturm. Im Vorbau ist ein Effekt der Tischtennisballstrecke untergebracht.

----

Funnel and surroundings

The funnel at its position. The entry track uses the front tower of the ZickZack reducing the count of parts or additional supports. In the front of the funnel an effect for the ping-pong ball tracks is housed.

The funnel is a special shape resembling a potential field.