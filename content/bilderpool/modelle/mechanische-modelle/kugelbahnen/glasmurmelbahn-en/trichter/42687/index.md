---
layout: "image"
title: "Im Trichter"
date: "2016-01-08T13:30:54"
picture: "trichter3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42687
- /details5810-2.html
imported:
- "2019"
_4images_image_id: "42687"
_4images_cat_id: "3176"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42687 -->
Von links oben im Bild kommen die Murmeln mit etwa 1,2 m/s in den Trichter hinein. Eine Unmlenkung am Trichterrand - hier wegen der Farbästhetik in gelb gehalten - sorgt dafür, daß keine Murmel über den Rand springt. Die Momentaufnahme läßt nur erahnen, daß im tieferen, steileren Teil des Trichters die Murmeln dem Schwerkraftsog völlig zu widerstehen scheinen und an den nahezu senkrechten(!) Wänden kreisen ohne herunter zu fallen. Wer es nicht glauben mag, kann sich einen ersten Eindruck aus Rob's Video holen: https://youtu.be/Nyy7FhNCYv8?t=503

----

In the funnel

From the upper left marbles are guided at 1.2m/s (approx. 3feet/s) into the funnel. A guide - yellow to reduce the contrast to the funnel - prevents marbles to tip over the edge. The shot gives just a very coarse impression of the effect that the marbles are swirling around the nearly vertical walls without dropping down. Have a look at the video clip: https://youtu.be/Nyy7FhNCYv8?t=503 (courtesy Rob van Baal)