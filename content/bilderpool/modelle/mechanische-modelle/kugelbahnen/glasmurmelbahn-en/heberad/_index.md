---
layout: "overview"
title: "Heberad"
date: 2020-02-22T08:16:52+01:00
legacy_id:
- /php/categories/3498
- /categoriesa1a9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3498 --> 
Die Weiterentwicklung des Heberades, welches schon 2014 zur Convention in Erbes-Büdesheim mit von der Partie war (http://www.ftcommunity.de/details.php?image_id=39558). Allzuviele technische Änderungen gab es aber nicht, der Ausstieg der Murmeln ist ein klein wenig verändert und eine Bahnstrecke führt durch die Stützkonstruktion. Dafür ist der dekorative Vorbau nicht mehr vorhanden.

---

The big lifting wheel, already shown in 2014 at the Convention at Erbes-Büdesheim, has been developed a little bit further. There have been minor technical changes, just the marble outlet is modified and a track now leads through the supporting construction. Because of this the decorative front building has been removed.