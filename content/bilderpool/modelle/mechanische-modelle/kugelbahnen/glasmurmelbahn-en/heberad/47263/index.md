---
layout: "image"
title: "Einstieg - Rückseite"
date: "2018-02-11T21:48:50"
picture: "heberad3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47263
- /details2888.html
imported:
- "2019"
_4images_image_id: "47263"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47263 -->
Auch hier verhindern Bauplatten zur Führung, daß die Murmeln durch das Rad hindurchrollen. Die Mechanik mit der Feder drückt die hintere Führungsrolle, und damit das Rad, gegen die vorderen Führungsrollen. Ankommende Murmeln rollen in die nächste freie Kammer und werden im Uhrzeigersinn (im Bild allerdings nach rechts) mitgenommen.