---
layout: "image"
title: "'Allrad'-Antrieb"
date: "2018-02-11T21:48:50"
picture: "heberad7.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47267
- /detailsa062-2.html
imported:
- "2019"
_4images_image_id: "47267"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47267 -->
Vor dem Rad sitzt dekorativ noch eine Kette um auch die zweite Antriebswelle anzukoppeln. Das Rad liegt oben auf den beiden Wellen auf und hängt ansonsten frei. Spurkränze (2 je Welle) treiben das Hohlrad kraftschlüssig an und führen es gleichzeitig.