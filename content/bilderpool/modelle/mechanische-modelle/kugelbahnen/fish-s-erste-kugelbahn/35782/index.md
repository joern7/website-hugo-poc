---
layout: "image"
title: "Gesamtansicht von oben"
date: "2012-10-04T20:21:08"
picture: "fishserstekugelbahn1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35782
- /details38bc.html
imported:
- "2019"
_4images_image_id: "35782"
_4images_cat_id: "2645"
_4images_user_id: "1113"
_4images_image_date: "2012-10-04T20:21:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35782 -->
Unten rechts ist der Aufzug, oben links eine 180° Wende der Bahn.