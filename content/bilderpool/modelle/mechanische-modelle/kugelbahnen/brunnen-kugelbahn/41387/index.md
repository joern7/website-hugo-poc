---
layout: "image"
title: "Der letzte Höhenunterschied"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme08.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41387
- /detailscc7f-2.html
imported:
- "2019"
_4images_image_id: "41387"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41387 -->
Es ergab sich, dass die Kugeln von oben rechts kommend noch etwas Fallhöhe überwinden müssen. Sie fallen in diesen kleinen Turm, ...