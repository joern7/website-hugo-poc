---
layout: "image"
title: "Eine Kugel im Magazin"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme06.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41385
- /details0102.html
imported:
- "2019"
_4images_image_id: "41385"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41385 -->
So kann eine einzelne Kugel gehalten werden.