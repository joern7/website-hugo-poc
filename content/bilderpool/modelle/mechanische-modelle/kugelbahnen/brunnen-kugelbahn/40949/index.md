---
layout: "image"
title: "Seitenansicht"
date: "2015-05-08T17:37:28"
picture: "brunnenkugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40949
- /details407b.html
imported:
- "2019"
_4images_image_id: "40949"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40949 -->
Der Aufzug transportiert Glasmurmeln hoch, die dann insgesamt drei Mal um den Brunnen laufen, bis sie wieder unten ankommen. Die Leistung reicht leider nicht für die gedachten 12 Aufzugsheber, mit 6 funktionierte es dann aber ganz ordentlich. Allerdings brachte die im Brunnen unter Wasser befindliche Pumpe wegen Verstopfung auch nicht die volle Leistung - die muss ich mal komplett zerlegen, dann geht es wohl noch etwas besser.