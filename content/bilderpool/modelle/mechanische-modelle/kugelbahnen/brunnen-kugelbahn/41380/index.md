---
layout: "image"
title: "Antrieb (1)"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme01.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41380
- /detailsac5a-2.html
imported:
- "2019"
_4images_image_id: "41380"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41380 -->
Auf einfachen Wunsch eines einzelnen ThanksForTheFish hier noch ein paar Detailbilder nachgereicht. Vom Wasserrad geht's über zwei Z10 (der Drehrichtung wegen) auf ein Z30. Durch diese Untersetzung genügt das Drehmoment.