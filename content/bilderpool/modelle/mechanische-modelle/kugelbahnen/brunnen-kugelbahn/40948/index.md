---
layout: "image"
title: "Aufzug"
date: "2015-05-08T17:37:28"
picture: "brunnenkugelbahn1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40948
- /detailsc4d2.html
imported:
- "2019"
_4images_image_id: "40948"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40948 -->
Dieser Brunnen pumpt das Wasser im Turm hoch, vorne fällt es wieder runter. Das war ja völlig klar, dass da eine Kugelbahn mit Wasserradantrieb dran muss!

Im gesamten Modell ist kein einziges Teil verbaut, in dem Metall vorkäme, damit nichts rostet. Nichtmal Grundbausteine, deren Zapfen ja mit einem Metallzapfen im Baustein verankert sind.