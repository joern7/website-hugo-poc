---
layout: "overview"
title: "Kugelbahn XXL (bummtschick)"
date: 2020-02-22T08:16:35+01:00
legacy_id:
- /php/categories/2880
- /categoriese3b2.html
- /categories21f1.html
- /categories2dc6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2880 --> 
My own Kugelbahn which has grown over the last ten months or so. This is originally model 3 from the ft "Dynamic" kit, some bits of which are left at the front right, but most has been replaced and much enlarged. Enjoy!