---
layout: "image"
title: "04 top close up"
date: "2014-04-16T15:10:50"
picture: "04.jpg"
weight: "4"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38554
- /detailsd34c.html
imported:
- "2019"
_4images_image_id: "38554"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38554 -->
Artsy close-up of the tracks at the top. The fast blue track is clearly visible in the center.