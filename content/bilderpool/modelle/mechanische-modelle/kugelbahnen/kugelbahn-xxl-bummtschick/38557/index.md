---
layout: "image"
title: "07 middle"
date: "2014-04-16T15:10:50"
picture: "07.jpg"
weight: "7"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38557
- /detailsdc9f.html
imported:
- "2019"
_4images_image_id: "38557"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38557 -->
Long shot of the Kugelbahn's middle part. The basket and plane 1 and 2 are visible at the top and center. The track departing from plane 2 is also visible. At the end of that track two switches (named A and B here) split the one track into three possible paths. The "Silberlinge" at the front left are described with picture 15. They are not necessary for the Kugelbahn's operation but just an additional special effect.