---
layout: "image"
title: "09 rear view"
date: "2014-04-16T15:10:50"
picture: "09.jpg"
weight: "9"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38559
- /details0217.html
imported:
- "2019"
_4images_image_id: "38559"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38559 -->
Full rear view. Switch A is visible in the center left, switch B on the very left. On the right the steep descent from swich A is clearly visible (continued in picture 13).