---
layout: "image"
title: "01 full front"
date: "2014-04-16T15:10:50"
picture: "01.jpg"
weight: "1"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38551
- /detailsb28c.html
imported:
- "2019"
_4images_image_id: "38551"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38551 -->
Front view. There are two elevators (run by a single motor, connected by an axle at about half hight). Elevator 1 starts at the bottom right and drops balls onto a track made of two metal bars at about half height (at the border between the black and gray construction on the right). The balls then run on two metal bars toward the center, from where the second elevator (red chain) transports them up to the top (green track at 3/4 height). The very top is only decoration. Roughtly the bottom right quarter of this construction is left over from model 3 of the original ft "dynamic" kit, including the great barrier / release construct from there which I liked enough to keep (see picture 10). The rest has been replaced and extended over the months...