---
layout: "image"
title: "02 top half"
date: "2014-04-16T15:10:50"
picture: "02.jpg"
weight: "2"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38552
- /detailsc111.html
imported:
- "2019"
_4images_image_id: "38552"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38552 -->
View of the top half from below, and from the back. Here the two metal bars are visible at the bottom, upon which the first elevator drops the balls and from which the second elevator picks them up.