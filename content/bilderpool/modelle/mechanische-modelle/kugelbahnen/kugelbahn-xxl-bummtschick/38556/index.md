---
layout: "image"
title: "06 red plane"
date: "2014-04-16T15:10:50"
picture: "06.jpg"
weight: "6"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38556
- /details4940-3.html
imported:
- "2019"
_4images_image_id: "38556"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38556 -->
Red plane no. 2. The balls from red plane no. 1 arrive via the green track visible at the top right. They then bounce back and forth between the green mounted on plane no. 2 and leave at the front right.