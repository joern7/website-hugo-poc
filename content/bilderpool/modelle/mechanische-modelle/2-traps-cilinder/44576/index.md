---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-15T19:30:48"
picture: "IMG_0725.jpg"
weight: "9"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/44576
- /details5cbc.html
imported:
- "2019"
_4images_image_id: "44576"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-15T19:30:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44576 -->
Cilinder helemaal in. Moet nog iets bijwerken om het dikke draadeind iets verder in te krijgen.