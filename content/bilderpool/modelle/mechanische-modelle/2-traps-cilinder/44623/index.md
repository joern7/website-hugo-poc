---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-17T21:02:42"
picture: "IMG_0729.jpg"
weight: "12"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/44623
- /details2c77-2.html
imported:
- "2019"
_4images_image_id: "44623"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-17T21:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44623 -->
M10 links draad moer heel ver afgedraaid zodat hij in de koker past.