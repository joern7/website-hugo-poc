---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-15T17:17:39"
picture: "IMG_0720.jpg"
weight: "4"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/44571
- /details0ae1.html
imported:
- "2019"
_4images_image_id: "44571"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-15T17:17:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44571 -->
Zicht op het extra draadeind dat ermee uitkomt