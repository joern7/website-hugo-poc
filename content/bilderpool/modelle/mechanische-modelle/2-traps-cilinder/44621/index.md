---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-17T21:02:41"
picture: "IMG_0727.jpg"
weight: "10"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/44621
- /details9610.html
imported:
- "2019"
_4images_image_id: "44621"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-17T21:02:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44621 -->
Het papier waar het idee op ontstond.
Uiteindelijk is het M10x1 linksdraad geworden. En binnenin zit M5x0,8 rechts draad.
Heb het draadeind met een lange 5mm boor bijna door geboord. In de ander kant van het M10 draadeind zit een 3mm gat (ongeveer 10mm diep) waar dan inderdaad zo'n kleine electromotor inzit.De elctromotor schuift mee heen en weer in het profiel. In de kant van het 5mm gat in het draadeind zit een M5 helicoil.
Het moet dus met een links en een rechts spoed draadeind!!