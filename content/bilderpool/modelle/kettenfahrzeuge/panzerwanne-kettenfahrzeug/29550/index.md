---
layout: "image"
title: "Unterseite/Federung"
date: "2010-12-27T16:29:50"
picture: "panzer4.jpg"
weight: "4"
konstrukteure: 
- "Nils"
fotografen:
- "Nils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/29550
- /detailsa095-2.html
imported:
- "2019"
_4images_image_id: "29550"
_4images_cat_id: "2150"
_4images_user_id: "456"
_4images_image_date: "2010-12-27T16:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29550 -->
Durch den Blick auf die Unterseite wird klar der Vorteil der hier verbauten Federung deutlich. Ich habe nach langem Versuchen eine Federung hinbekommen, die nicht breiter als die Kette selbst ist und doch gut federt ohne die Statik des Gesamtmodells zu gefährden (trotz recht hohem Gewicht verbiegt sich da garnichts!). Folglich verfügt das Modell über einen sehr großen Bodenabstand.