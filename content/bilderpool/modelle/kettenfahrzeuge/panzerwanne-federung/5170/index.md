---
layout: "image"
title: "Pz-Wanne04.JPG"
date: "2005-10-31T21:06:56"
picture: "Pz-Wanne04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Panzer", "Fahrgestell", "Wanne"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5170
- /detailscf08.html
imported:
- "2019"
_4images_image_id: "5170"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T21:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5170 -->
Als Antrieb ist das Getriebe #04 aus http://www.ftcommunity.de/categories.php?cat_id=31 angedacht. Hier ist es nur lose aufgelegt worden; die Befestigung fehlt noch.