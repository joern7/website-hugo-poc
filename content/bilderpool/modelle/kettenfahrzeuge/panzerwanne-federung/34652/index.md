---
layout: "image"
title: "bully54.JPG"
date: "2012-03-16T09:58:47"
picture: "bully54.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34652
- /details216c.html
imported:
- "2019"
_4images_image_id: "34652"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-03-16T09:58:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34652 -->
Die Bodenfreiheit ist gewaltig. Außerdem liegen die Fahrwerkteile allesamt innerhalb der Kettenbreite