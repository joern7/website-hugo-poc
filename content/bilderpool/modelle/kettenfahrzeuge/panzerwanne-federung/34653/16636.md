---
layout: "comment"
hidden: true
title: "16636"
date: "2012-03-18T22:14:08"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

ha, Du bist schon aus dem Laufhaufen zurück!

Ja, eine schnuckelige Idee. Würde mich allerdings überraschen, wenn der Servo diese Lenkung ohne Probleme antreibt (schacher Motor, schlechter Hebel, verkantende Zahnräder...). Ich habe bei meinen Gangschaltungen ziemlich viel mit solchen Antrieben herumexperimentiert. Was definitiv funktioniert, ist die Konstruktion von thomas004 mit dem Getriebehalter mit Schnecke (http://www.ftcommunity.de/details.php?image_id=10722) oder ein Z10 an Zahnstange (http://www.ftcommunity.de/details.php?image_id=28987). Nachteil ist natürlich, dass man dafür eher das IR-Fernsteuermodul "für Erwachsene" (ohne Servo-Anschluss) benötigt.

Gruß, Dirk