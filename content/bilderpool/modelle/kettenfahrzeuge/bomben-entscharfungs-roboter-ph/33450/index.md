---
layout: "image"
title: "Interfaces"
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter22.jpg"
weight: "22"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33450
- /details83bf-2.html
imported:
- "2019"
_4images_image_id: "33450"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33450 -->
..