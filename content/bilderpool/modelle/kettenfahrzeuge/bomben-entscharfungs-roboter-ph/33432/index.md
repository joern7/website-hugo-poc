---
layout: "image"
title: "Aufsteckbarer Geifarm"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter04.jpg"
weight: "4"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33432
- /details7888.html
imported:
- "2019"
_4images_image_id: "33432"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33432 -->
Der Arm kann sich strecken und wieder ein fahren.
