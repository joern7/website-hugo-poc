---
layout: "image"
title: "Steuerung Gesamt"
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter20.jpg"
weight: "20"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33448
- /details0f40.html
imported:
- "2019"
_4images_image_id: "33448"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33448 -->
ohne Worte