---
layout: "image"
title: "Antreib für den Greifer"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter03.jpg"
weight: "3"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33431
- /details06f6.html
imported:
- "2019"
_4images_image_id: "33431"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33431 -->
Drehen.