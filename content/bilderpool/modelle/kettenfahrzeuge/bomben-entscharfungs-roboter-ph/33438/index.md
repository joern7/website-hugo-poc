---
layout: "image"
title: "Kamera"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter10.jpg"
weight: "10"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33438
- /details8cc1-2.html
imported:
- "2019"
_4images_image_id: "33438"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33438 -->
Motor