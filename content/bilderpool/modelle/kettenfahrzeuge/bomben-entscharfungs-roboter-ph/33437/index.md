---
layout: "image"
title: "Kamera"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter09.jpg"
weight: "9"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33437
- /detailsf742.html
imported:
- "2019"
_4images_image_id: "33437"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33437 -->
Vertikal und Horizontal