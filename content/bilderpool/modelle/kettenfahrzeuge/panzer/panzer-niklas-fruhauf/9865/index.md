---
layout: "image"
title: "Panzer"
date: "2007-03-31T18:08:14"
picture: "panzer2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9865
- /detailsd429.html
imported:
- "2019"
_4images_image_id: "9865"
_4images_cat_id: "891"
_4images_user_id: "557"
_4images_image_date: "2007-03-31T18:08:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9865 -->
Antrieb durch zwei 50:1 Powermotoren, direkt auf Kette, oben Links-Rechts Schwenkantrieb sichtbar