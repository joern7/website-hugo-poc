---
layout: "image"
title: "Panzer11.jpg"
date: "2010-02-02T23:13:18"
picture: "Panzer11.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26197
- /details75bf.html
imported:
- "2019"
_4images_image_id: "26197"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:13:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26197 -->
Der Schleifring hat zwei Bahnen, von denen jede zur Sicherheit mit zwei Kontakten versorgt wird. Die beiden Motoren im Turm (Rohr auf/ab, Turm links/rechts) haben einen eigenen IR-Empfänger (neues Modell).