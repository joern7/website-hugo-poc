---
layout: "image"
title: "Panzer02.jpg"
date: "2010-02-02T23:06:39"
picture: "Panzer02.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26193
- /detailseb79.html
imported:
- "2019"
_4images_image_id: "26193"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:06:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26193 -->
Ja, ich weiß.


Es gibt Stimmen für und wider diese Art von Modellen. Ich hab's trotzdem gebaut und kann bei Bedarf weitere Angaben dazu machen.