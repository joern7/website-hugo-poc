---
layout: "comment"
hidden: true
title: "10730"
date: "2010-02-04T23:36:02"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Zum Geradeauslauf mit zwei Motoren aus meiner Sicht noch eine Ergänzung:
Die aktuelle erweitere Motorsteuerung mit RoboPro, Robo TX Controller und Encodermotor (wegsynchron) realisiert eine impulssychrone Motorsteuerung. Das heisst einfach erklärt, der jeweils schnellere Antrieb wartet auf den langsameren. Ist der langsamere extrem schwergängig und bleibt stehen fehlen seine nächsten Wegimpulse und die Steuerung hält an.
Bei 75 Impulsen je Umdrehung erfolgen so viele Ausgleichkorrekturen, dass eine Geradeausfahrt unter normalen Bedingungen gesichert bleibt. Ich habe diese Steuerung auf meinem 3D-XYZ probiert. Eine diagonale Bewegungslinie mit 45° wird hiermit so exakt gefahren als wenn es eine linerare Achsenlinie wäre ...