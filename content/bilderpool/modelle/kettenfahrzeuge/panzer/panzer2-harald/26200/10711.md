---
layout: "comment"
hidden: true
title: "10711"
date: "2010-02-04T07:33:53"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Wieso ist dem aktuellen Control Set ein Gleichlaufgetriebe auch nicht erforderlich? Ein Gleichlaufgetriebe dient ja dazu, bei Moten mit naturgemäß geringfügig unterschiedlichen Drehzahlen (Toleranzen) einen perfekten Geradeauslauf herzustellen. Und das kann das aktuelle Control Set auch im Kettenmodus nicht.

Oder was meinst Du?

Gruß, Thomas