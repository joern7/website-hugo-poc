---
layout: "image"
title: "Antrieb und Fahrwerk"
date: "2017-09-30T19:42:20"
picture: "pz6.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46562
- /details9b32.html
imported:
- "2019"
_4images_image_id: "46562"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46562 -->
Die Fahrwerksschwingen (Tragarme) sind auch selbst gedruckt. Ich habe drei Varianten ausprobiert, und eine vierte ist noch in Arbeit. Der Gummizug geht der Reihe nach durch alle Tragarme hindurch und ist in seiner Mitte vorn hinter der Bugplatte verknotet.

Das Getriebe hat im Bilderpool schon einen eigenen Platz gefunden: als "Gleichlauf (11) unter https://ftcommunity.de/categories.php?cat_id=3346

Als einzige Änderung ist hier eine rote Motor-Halteplatte von TST angebracht, die ein zusätzliches Loch von 4 mm Durchmesser erhalten hat, damit die Rastachse fürs Lenkgetriebe da hindurch passt und die Zahnräder nicht voneinander weg rutschen können.