---
layout: "image"
title: "TragarmCAD.jpg"
date: "2017-09-30T20:17:17"
picture: "TragarmCAD.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46565
- /details6f26.html
imported:
- "2019"
_4images_image_id: "46565"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T20:17:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46565 -->
Durch die flach liegenden Bohrungen wird der Gummizug der Federung gefädelt. Die blauen Löcher sind für Lagerung (links) und die tragende Achse.