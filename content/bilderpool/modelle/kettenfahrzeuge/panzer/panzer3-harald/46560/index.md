---
layout: "image"
title: "Turm von innen"
date: "2017-09-30T19:42:20"
picture: "pz4.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46560
- /details2e5f.html
imported:
- "2019"
_4images_image_id: "46560"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46560 -->
Im Turm sind untergebracht:
- der Akku-Hauptschalter hinten links
- der Bluetooth-Fernsteuerungsempfänger (linke Seite, hier unter dem Speichenrad versteckt
- ein ft-Soundmodul (rechte Seite)
- der Drehantrieb für den Turm (grauer Motor hinten rechts, mit Schnecke auf die Achse vom Rast-Z10)
- der Höhenrichtantrieb für die Kanone (Strebe 30 und ft-Servo, hier ganz verdeckt)

Es ist trotzdem noch reichlich Platz da drin, nicht zuletzt weil der Turm auf einem 3D-gedruckten Zahnkranz (nächstes Bild) aufliegt und die Turm-Innereien bis zum Wannenboden nach unten (da wo jetzt das Speichenrad sitzt) reichen. 

Ein Schleifring ist für später vorgesehen. Derzeit zwirbelt man das Kabel auf, wenn man den Turm lange drehen lässt.