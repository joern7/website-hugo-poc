---
layout: "image"
title: "Kampfpanzer"
date: "2008-10-22T18:56:33"
picture: "kampfpanzer1.jpg"
weight: "7"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16030
- /details5144.html
imported:
- "2019"
_4images_image_id: "16030"
_4images_cat_id: "32"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T18:56:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16030 -->
Der Panzer kann nach vorne fahren, kann Schussrohr drehen und mit einer Seilwinde kann es auch hochhehoben oder gesenkt werden.