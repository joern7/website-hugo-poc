---
layout: "image"
title: "Minipanzer - Gesamtansicht"
date: "2012-09-16T20:52:47"
picture: "minipanzer1.jpg"
weight: "1"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35525
- /detailseb6e.html
imported:
- "2019"
_4images_image_id: "35525"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35525 -->
