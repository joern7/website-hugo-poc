---
layout: "image"
title: "Minipanzer - Ansicht von unten"
date: "2012-09-16T20:52:47"
picture: "minipanzer2.jpg"
weight: "2"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35526
- /details5590.html
imported:
- "2019"
_4images_image_id: "35526"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35526 -->
