---
layout: "image"
title: "Panzer 7"
date: "2007-06-04T21:22:22"
picture: "Panzer_13.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10720
- /detailsba45.html
imported:
- "2019"
_4images_image_id: "10720"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10720 -->
Hier ist die hohe Bodenfreiheit gut zu sehen. Es gibt nichts, was neben den Ketten in die Bodenfreiheit ragt, da die Aufhängung der Kettenräder schmaler als die Kette baut.

Entschuldigt bitte dieses etwas martialische Foto, aber es musste einfach sein... ;o) Ich mag es!