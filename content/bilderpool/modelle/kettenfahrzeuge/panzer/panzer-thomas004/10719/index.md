---
layout: "image"
title: "Panzer 6"
date: "2007-06-04T21:22:22"
picture: "Panzer_09.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10719
- /details6050.html
imported:
- "2019"
_4images_image_id: "10719"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10719 -->
Hier sieht man gut den Bremsmechanismus im Detail: Rechts im Bild in der Mitte ist der Drehpunkt der Wippe, die die Bauplatte 15x30 oben im Bild verschiebt. Diese Bauplatte ist in der Nut des Rahmens verschiebbar geführt und greift als Bremse in das Zahnrad Z10 neben dem Differenzial.

Die beiden gelben Streben halten nur den Power-Motor und dienen zur Stabilisierung.