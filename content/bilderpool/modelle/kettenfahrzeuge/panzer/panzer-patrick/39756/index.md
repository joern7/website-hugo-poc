---
layout: "image"
title: "Ansicht von unten"
date: "2014-10-27T17:53:42"
picture: "tank3.jpg"
weight: "3"
konstrukteure: 
- "Patrick"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39756
- /details90f9-2.html
imported:
- "2019"
_4images_image_id: "39756"
_4images_cat_id: "2982"
_4images_user_id: "2228"
_4images_image_date: "2014-10-27T17:53:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39756 -->
Minimotor zum Drehen des Aufbaus, darüber zwei Powermotoren, die den Panzer antreiben.