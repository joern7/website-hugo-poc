---
layout: "image"
title: "Ansicht von oben ohne Aufbau"
date: "2014-10-27T17:53:42"
picture: "tank5.jpg"
weight: "5"
konstrukteure: 
- "Patrick"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39758
- /details6c79.html
imported:
- "2019"
_4images_image_id: "39758"
_4images_cat_id: "2982"
_4images_user_id: "2228"
_4images_image_date: "2014-10-27T17:53:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39758 -->
Der Aufbau ist auf einer Drehscheibe fixiert.