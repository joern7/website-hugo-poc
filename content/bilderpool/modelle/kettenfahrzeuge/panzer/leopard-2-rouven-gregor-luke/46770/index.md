---
layout: "image"
title: "Panzer Leopard 2 (06)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven6.jpg"
weight: "6"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- /php/details/46770
- /detailsc2fa-2.html
imported:
- "2019"
_4images_image_id: "46770"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46770 -->
Hier mit geöffneter Motorklappe.