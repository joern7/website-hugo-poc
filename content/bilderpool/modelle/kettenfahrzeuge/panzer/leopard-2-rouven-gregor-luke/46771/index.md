---
layout: "image"
title: "Panzer Leopard 2 (07)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven7.jpg"
weight: "7"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- /php/details/46771
- /detailsf8ad-2.html
imported:
- "2019"
_4images_image_id: "46771"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46771 -->
Der Turm könnte noch etwas flacher gebaut werden. Mal sehen ob mir das gelingt. (Unterbringung Akku, und zwei Mini-Motoren)