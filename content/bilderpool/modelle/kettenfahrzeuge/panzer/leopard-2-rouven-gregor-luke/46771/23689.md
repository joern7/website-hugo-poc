---
layout: "comment"
hidden: true
title: "23689"
date: "2017-10-15T08:57:28"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Der sieht schon gut aus (nur zum Original fehlt noch etwas)

Den Turm kriegst du flacher, wenn du die volle Breite der Wanne ausnutzt und das Turmheck nach hinten verlängerst (da passt dann der Akku prima hin). Dann wird er dem Original auch ähnlicher.

Machst du noch ein paar Fotos vom Fahrwerk und der Unterseite?

Gruß,
Harald