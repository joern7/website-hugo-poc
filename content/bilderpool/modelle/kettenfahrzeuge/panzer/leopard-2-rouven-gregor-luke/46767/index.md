---
layout: "image"
title: "Panzer Leopard 2 (03)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven3.jpg"
weight: "3"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- /php/details/46767
- /details6561.html
imported:
- "2019"
_4images_image_id: "46767"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46767 -->
Außer dem Kanonenrohr wurde alles aus FT-Teilen gebaut.