---
layout: "image"
title: "Getriebe-Unterseite"
date: "2014-10-02T21:56:48"
picture: "pistenbully09.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39506
- /detailsd634.html
imported:
- "2019"
_4images_image_id: "39506"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39506 -->
Das Gleichlaufgetriebe ist "Nummer 9" von NBGer ( http://ftcommunity.de/categories.php?cat_id=2918 )

Rechts sieht man die Lieblingsbeschäftigung der Motoren für den gleichsinnigen Eingang: es ist leichter, die Achsverschraubung 38843/38844 los zu drehen (egal was da noch mit eingeklemmt wird) als das Fahrzeug an zu treiben...