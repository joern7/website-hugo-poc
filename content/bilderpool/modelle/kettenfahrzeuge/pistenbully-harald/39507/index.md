---
layout: "image"
title: "Unterseite ohne Ketten"
date: "2014-10-02T21:56:48"
picture: "pistenbully10.jpg"
weight: "10"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39507
- /details9db4-2.html
imported:
- "2019"
_4images_image_id: "39507"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39507 -->
Die Biegeform der schwarzen Flachträger deutet es an: das Fahrzeug ist einmal als Panzer angefangen worden.