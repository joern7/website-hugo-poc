---
layout: "image"
title: "Gabelkippmechanismus von oben"
date: "2008-12-14T10:53:30"
picture: "industreadozer6.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16616
- /detailsda99-2.html
imported:
- "2019"
_4images_image_id: "16616"
_4images_cat_id: "1504"
_4images_user_id: "845"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16616 -->
