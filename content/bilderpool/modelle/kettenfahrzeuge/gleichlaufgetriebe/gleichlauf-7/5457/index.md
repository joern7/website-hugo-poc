---
layout: "image"
title: "Baustein-15 (32064) mit Bohrung + 2st Kugellager Conrad 214426  (4x4x9mm)"
date: "2005-12-09T19:51:41"
picture: "FT-Antrieb_006.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5457
- /detailsce7e.html
imported:
- "2019"
_4images_image_id: "5457"
_4images_cat_id: "622"
_4images_user_id: "22"
_4images_image_date: "2005-12-09T19:51:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5457 -->
Reicht dieser Antrieb mit dem grauen (20:1) Motor ?  .....Schau:  http://www.ftcommunity.de/details.php?image_id=5170
 
Ich uberdenke mir eine dritte Antriebmotor.
Einer zum Lenken und die 2 andere parallel zum gerade aus fahren mit eine grossere Kraft (und gleiche Geschwindigkeit).
Sonst kan man statt die Powermotor mit einer graue Deckel (20:1), auch die mit eine rote Deckel (50:1) nehmen.
 
Zum Antrieb nutze ich manchmal statt Baustein 15 mit Bohrung 32064:
2 st. Conrad Kugellager 214426 (4/9mm) die genau passen in die original Schneckenmutter m=1 35973.
Damit lauft alles leichter !........