---
layout: "overview"
title: "Gleichlaufgetriebe mit 4 Motoren"
date: 2020-02-22T08:35:59+01:00
legacy_id:
- /php/categories/1623
- /categoriesbb8f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1623 --> 
Das ganze soll noch in ein Kettenfahrwerk eingebaut werden. Durch das Differenzial zwischen 2 Motoren können diese sich nicht gegenseitig blockieren.