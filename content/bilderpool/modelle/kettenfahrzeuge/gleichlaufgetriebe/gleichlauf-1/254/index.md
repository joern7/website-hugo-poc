---
layout: "image"
title: "kette13"
date: "2003-04-21T21:14:41"
picture: "kette13.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/254
- /details4291.html
imported:
- "2019"
_4images_image_id: "254"
_4images_cat_id: "616"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T21:14:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=254 -->
