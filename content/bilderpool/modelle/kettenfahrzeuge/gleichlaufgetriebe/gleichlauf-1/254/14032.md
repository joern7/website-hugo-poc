---
layout: "comment"
hidden: true
title: "14032"
date: "2011-04-04T21:26:35"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hm, *grübelgrübel*
müsste aber klappen, wenn ich Deinen coolen Z44-Trick hier hineinfrickele:
http://www.ftcommunity.de/details.php?image_id=30426#col3
Die Kurbel mit den drei Kegelzahnrädern gehört dann auf die andere Seite... und schon "funkts", oder?
Gruß, Dirk