---
layout: "image"
title: "Synchrongetriebe"
date: "2006-08-31T21:43:17"
picture: "synkrongetriebe1.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/6763
- /detailsba68.html
imported:
- "2019"
_4images_image_id: "6763"
_4images_cat_id: "31"
_4images_user_id: "456"
_4images_image_date: "2006-08-31T21:43:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6763 -->
Hier habe ich mal ein Synchrongetriebe gebaut. Das besondere daran ist, dass es sehr lang, aber dafür auch nicht sehr hoch ist.
(Weitere Bilder folgen)