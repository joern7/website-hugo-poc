---
layout: "comment"
hidden: true
title: "1268"
date: "2006-09-01T15:13:22"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
.Ich habe das mal auf "SynCHrongetriebe" umbenannt.

Ihr meint beide das gleiche, nur mit anderen Worten: eine Seite bremsen und die andere beschleunigen heißt eben, eine Richtungsänderung überlagern :-)


Auf diese Mechanik muss man wirklich erst mal kommen (MarMac war auch schon da: siehe http://www.ftcommunity.de/details.php?image_id=5112 ).

Gruß,
Harald