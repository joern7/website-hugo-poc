---
layout: "image"
title: "Raupenfahrzeug mit Gleichlaufgetriebe 'Classic' (Vorderansicht)"
date: "2011-01-03T20:19:05"
picture: "Raupenfahrzeug_mit_Gleichlaufgetriebe_Classic_II.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: ["Raupenfahrzeug", "Kettenantrieb", "Wall-E", "Gleichlaufgetriebe"]
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29604
- /details8f2d.html
imported:
- "2019"
_4images_image_id: "29604"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-01-03T20:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29604 -->
Und so sieht das fertige Raupenfahrzeug mit Antrieb (zwei M-Motoren mit Schnecke), Fernsteuerung und Akku aus. Das Fahrzeug dreht sehr elegant. Gedacht ist es als Chassis für ein Wall-E-Modell; höher darf der Schwerpunkt allerdings nicht liegen, sonst besteht bei größeren Hindernissen Kippgefahr; für Schnee-Ausfahrten sollte man zudem die Fernsteuerung höher montieren.