---
layout: "image"
title: "Gleichlaufgetriebe 'Classic'"
date: "2010-12-21T19:25:38"
picture: "gleichlaufgetriebeclassic1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29496
- /details61b7.html
imported:
- "2019"
_4images_image_id: "29496"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2010-12-21T19:25:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29496 -->
Für ein Kettenfahrzeug benötigten wir ein besonders flaches Gleichlaufgetriebe mit stabilen Achsen - herausgekommen ist eine "Classic"-Variante der Getriebe von thomas004 und fitec, ein "Gleichlaufgetriebe für Arme" sozusagen. Das Getriebe lässt sich auch komplett "in grau" bauen (die  schwarz-rote Variante erschien uns nur fotogener ;-). Für Nachbauer: Der Motor vorne im Bild lässt die Ketten in Gegenrichtung laufen (lenken/drehen des Fahrzeugs), der andere sorgt für einen gleichmäßigen Antrieb. Achtung: die Hinterachse (im Bild links vorne) ist geteilt. Ersetzt man die Z20 des Kettenantriebs durch Z40er, dann hat das Fahrgestell auch ausreichend Bodenfreiheit.