---
layout: "image"
title: "Raupenfahrzeug mit Gleichlaufgetriebe 'Classic' (Rückansicht)"
date: "2011-01-03T20:19:06"
picture: "Raupenfahrzeug_mit_Gleichlaufgetriebe_Classic_I.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: ["Raupenfahrzeug", "Gleichlaufgetriebe", "Wall-E", "Kettenantrieb"]
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29605
- /detailscbd9.html
imported:
- "2019"
_4images_image_id: "29605"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-01-03T20:19:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29605 -->
Ansicht von hinten. Auf dem schwarzen Baustein 30 wird der Akku montiert.