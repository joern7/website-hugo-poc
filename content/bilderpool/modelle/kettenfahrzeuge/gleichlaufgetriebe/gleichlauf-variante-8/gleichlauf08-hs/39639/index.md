---
layout: "image"
title: "Gleichlauf08_3"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs3.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39639
- /details919d-2.html
imported:
- "2019"
_4images_image_id: "39639"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39639 -->
Aufbauvariante mit 2x15°-Winkeln, hier zusammengesteckt und ergänzt um die 90°-Winkelübertragung mittels Kronenrädern.
