---
layout: "image"
title: "Gleichlauf08_5"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs5.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39641
- /detailsc1fc-2.html
imported:
- "2019"
_4images_image_id: "39641"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39641 -->
Nur zum Spaß einmal ganz aufgeklappt und überstreckt. Wenn man jetzt noch die Gelenke fixiert und die Kette ratterfrei auf den rechten Motor bekommt, funktioniert es immer noch.