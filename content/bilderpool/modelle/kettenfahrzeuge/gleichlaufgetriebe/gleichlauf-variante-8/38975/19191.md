---
layout: "comment"
hidden: true
title: "19191"
date: "2014-06-24T17:44:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Sehr schön und kompakt. Da wäre ich ja lieber selber drauf gekommen :-)

Die 90°-Kehre mit den Klemm-Kegelzahnrädern war bisher noch immer eine Schwachstelle in meiner Bauerei. Das muss "verblockt" oder noch besser zusammen geschweißt werden, damit das nicht in knatterndes Zahnhüpfen entartet. Mit Kronenrädern (schwarze Kette auf einem ft-Zahnrad) sollte das zu beheben sein, und könnte sogar noch einen Tick kompakter werden. Mussmalebenganzschnellweg...