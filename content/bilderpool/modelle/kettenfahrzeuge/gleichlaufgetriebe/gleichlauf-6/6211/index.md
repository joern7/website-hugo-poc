---
layout: "image"
title: "Gleichlauf06-06.JPG"
date: "2006-05-05T19:27:45"
picture: "Gleichlauf06-06.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6211
- /details6106.html
imported:
- "2019"
_4images_image_id: "6211"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6211 -->
Das Bild gibt es nicht genau her: auf der rechten Achse sitzt ein Z10 vor dem Z15. 

Das Z10 ist "gekappt", d.h. der Stummel, um eine zweite Achse anzuklipsen, ist abgeschnitten (Idee ist von Claus).