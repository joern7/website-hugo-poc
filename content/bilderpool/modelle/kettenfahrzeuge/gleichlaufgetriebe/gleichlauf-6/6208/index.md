---
layout: "image"
title: "Gleichlauf06-03.JPG"
date: "2006-05-05T19:22:37"
picture: "Gleichlauf06-03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6208
- /details6965.html
imported:
- "2019"
_4images_image_id: "6208"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:22:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6208 -->
