---
layout: "image"
title: "Gleichlauf06-02.JPG"
date: "2006-05-05T19:21:31"
picture: "Gleichlauf06-02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6207
- /detailsc41e.html
imported:
- "2019"
_4images_image_id: "6207"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:21:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6207 -->
