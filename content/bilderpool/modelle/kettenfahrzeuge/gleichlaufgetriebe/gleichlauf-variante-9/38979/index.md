---
layout: "image"
title: "Antriebseinheit für Panzer"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante4.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38979
- /detailsbba1.html
imported:
- "2019"
_4images_image_id: "38979"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38979 -->
Die zwei Getriebe sind in der Panzer-Antriebseinheit kombiniert.
Die beiden Getriebe haben in etwa den selben Höhenbedarf, dadurch kann man nach wie vor eine sehr flache Panzerwanne bauen.

Der ganze Aufbau zeigt erst mal nur das Prinzip. Die genaue Geometrie ergibt sich erst beim Einbau bzw. der Gestaltung der Panzerwanne.
Die Stabilität ist noch mangelhaft. Den Hauptantriebs-Motor habe ich bereits provisorisch eingebaut, um die Getriebeübersetzung im Zusammenspiel mit dem Gleichlaufgetriebe zu testen.

