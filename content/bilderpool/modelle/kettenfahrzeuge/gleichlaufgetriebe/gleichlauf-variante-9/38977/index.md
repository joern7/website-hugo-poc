---
layout: "image"
title: "Gleichlaufgetriebe Antriebsseite"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante2.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38977
- /detailse877.html
imported:
- "2019"
_4images_image_id: "38977"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38977 -->
Die Achsen auf dieser Seite sind über ein mittleres Zahnrad gekoppelt. An einer dieser Achsen wird also der Antrieb für die Lenkungsüberlagerung eingespeist
Der Hauptantrieb erfolgt über die Differentialgehäuse.
Das ist der Hauptunterschied zum Getriebe in der Variante 8. Hauptantrieb  und Antrieb der Lenkungsüberlagerung sind miteinander vertauscht.