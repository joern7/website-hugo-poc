---
layout: "image"
title: "Gleichlaufgetriebe Abtriebsseite"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38976
- /details1fbb.html
imported:
- "2019"
_4images_image_id: "38976"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38976 -->
Diese Gleichlaufgetriebe-Variante sieht fast gleich aus wie die "Variante 8", funktioniert aber etwas unterschiedlich.
Der Abstand der Differentiale ist geringer, so daß die Differentiale direkt ineinandergreifen.
Dadurch ist auch der Abstand der Kegelräder geringer. 
