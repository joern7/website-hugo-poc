---
layout: "image"
title: "Gleichlauf02-03.JPG"
date: "2005-08-20T19:32:55"
picture: "Gleichlauf02-03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Gleichlauf", "Sigma-Delta", "Summe-Differenz", "Panzer", "Fly-by-Wire"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4605
- /details7f38-2.html
imported:
- "2019"
_4images_image_id: "4605"
_4images_cat_id: "617"
_4images_user_id: "4"
_4images_image_date: "2005-08-20T19:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4605 -->
Die roten Schnecken müssen noch ersetzt werden durch die klemmbaren Schneckenteile. Hier gings erstmal nur ums Prinzip.