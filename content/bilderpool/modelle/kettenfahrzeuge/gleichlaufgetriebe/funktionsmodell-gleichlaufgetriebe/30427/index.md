---
layout: "image"
title: "Funktionsmodell Gleichlaufgetriebe - Hauptantrieb"
date: "2011-04-04T00:13:27"
picture: "funktionsmodellgleichlaufgetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30427
- /details9535.html
imported:
- "2019"
_4images_image_id: "30427"
_4images_cat_id: "2262"
_4images_user_id: "1126"
_4images_image_date: "2011-04-04T00:13:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30427 -->
Diese Kurbel sorgt für die Geradeausfahrt.