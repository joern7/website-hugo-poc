---
layout: "image"
title: "Gesamtansicht Funktionsmodell Gleichlaufgetriebe"
date: "2011-04-04T00:13:27"
picture: "funktionsmodellgleichlaufgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30426
- /details2de4.html
imported:
- "2019"
_4images_image_id: "30426"
_4images_cat_id: "2262"
_4images_user_id: "1126"
_4images_image_date: "2011-04-04T00:13:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30426 -->
Die hintere Kurbel sorgt für den Vortrieb, die vordere Kurbel ist für die überlagerte Steuerung zuständig.
Die Antriebsachsen lassen sich für einen Motorantrieb mit wenigen Handgriffen versetzen (um 90° drehen, nach oben oder unten verschieben etc.).