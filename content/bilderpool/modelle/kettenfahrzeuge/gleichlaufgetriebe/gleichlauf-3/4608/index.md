---
layout: "image"
title: "Gleichlauf03-02.JPG"
date: "2005-08-20T19:32:55"
picture: "Gleichlauf03-02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Gleichlauf", "Sigma-Delta", "Summe-Differenz", "Panzer", "Fly-by-Wire"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4608
- /details39ae.html
imported:
- "2019"
_4images_image_id: "4608"
_4images_cat_id: "618"
_4images_user_id: "4"
_4images_image_date: "2005-08-20T19:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4608 -->
Als Summe und Differenz werden die beiden Kardanwellen definiert; die roten Schnecken mit Z10 sind linker / rechter Ausgang.