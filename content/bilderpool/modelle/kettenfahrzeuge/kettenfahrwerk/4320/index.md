---
layout: "image"
title: "Kettenfahrwerk (Blick auf den Antrieb 2)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_006.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4320
- /detailsda40.html
imported:
- "2019"
_4images_image_id: "4320"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4320 -->
Das rechte der beiden Z10 ist eines von zwei übereinander liegenden, die eine Spur (genauer: Eine ft-Zapfenlänge) tiefer sitzen als das vom Differential angetriebene. Dadurch erreicht das untere der beiden rechten Z10 das Z20, dessen Gegenstück auf der anderen Fahrwerkseite via Kette angetrieben wird. Das ergibt die erforderlichen Drehrichtungen für den Synchronantrieb.