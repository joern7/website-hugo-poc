---
layout: "image"
title: "Antrieb von hinten unten"
date: "2005-10-30T11:29:25"
picture: "Antrieb_003.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5144
- /detailse0d6.html
imported:
- "2019"
_4images_image_id: "5144"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5144 -->
Die Z10 ganz außen griffen in die Z20 ein, auf denen die Kette lief.