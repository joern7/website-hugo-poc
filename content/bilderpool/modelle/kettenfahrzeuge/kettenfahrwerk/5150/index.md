---
layout: "image"
title: "Antrieb Abstände links"
date: "2005-10-30T11:29:25"
picture: "Antrieb_009.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5150
- /details2f4c.html
imported:
- "2019"
_4images_image_id: "5150"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5150 -->
Hier dasselbe von der anderen Seite. Die Riegelscheiben sind die für den wegen der Kette notwendigen Abstand.