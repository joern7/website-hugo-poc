---
layout: "image"
title: "Kettenfahrwerk (Detailblick auf die Radaufhängung)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_011.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4324
- /details8bd1.html
imported:
- "2019"
_4images_image_id: "4324"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4324 -->
Die BS30, an denen die Radaufhängung befestigt ist, halten sich wiederum über einen Federnocken am Alu fest. Die Federn sind mit je einer Platte 15x15 mm abgedeckt, um sie eine Spur zu verlängern und damit "härter" zu machen. Die zwei Räder direkt beim Antrieb (der hinten sitzt) haben anstatt der Platte 15x15 mm eine via Federnocken angebrachte Platte 15x15 mm mit Nut, die noch ein kleines bisschen dicker ist, denn diese Federn werden am stärksten eingedrückt, wenn der Antrieb die Ketten bei Vorwärtsfahrt anzieht.