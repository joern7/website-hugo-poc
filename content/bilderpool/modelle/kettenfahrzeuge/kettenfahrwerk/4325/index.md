---
layout: "image"
title: "Kettenfahrwerk (Frontansicht)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_002.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4325
- /details6e75.html
imported:
- "2019"
_4images_image_id: "4325"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4325 -->
Der Schalter dient zum Abschalten. Wer hätt's gedacht.