---
layout: "image"
title: "Antrieb Getriebe rechts"
date: "2005-10-30T11:29:25"
picture: "Antrieb_005.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5146
- /detailsebc3.html
imported:
- "2019"
_4images_image_id: "5146"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5146 -->
Viel simpler geht's nimmer: Z10, Z20, Kette, fertig. Z10 und Z20 sitzen auf einer Riegelscheibe, um den nötigen Abstand von den Bausteinen (für die Kette) zu erhalten.