---
layout: "image"
title: "Kettenfahrwerk (Detail der Befestigung)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_014.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4323
- /details1228.html
imported:
- "2019"
_4images_image_id: "4323"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4323 -->
Hier ein Detail der Befestigung des Antriebsblocks mit den zwei Alus. Der Antriebsblock ist von oben wie auf den anderen Fotos ersichtlich mit eine paar roten Teilen mit den Alus verbunden. Unten gibt es noch eine Verbindung, bei der es sich als nützlich erwiesen hat, dass ich meine ca. 30 Jahre alten defekten Bausteine nicht weggeworfen hatte. In Bildmitte sieht man die Innereien (zwei Zapfen auf einer Metallstange) eines zerbrochenen Baustein 15 mit zwei Zapfen. Der passt gerade so hier hinein und sorgt so mit seinem Gegenstück auf der anderen Seite für insgesamt 4 anstatt nur 2 Befestigungspunkte zwischen Antrieb und Alus. Diese Teile sind übrigens auch sehr nützlich für die Befestigung von Powermotoren an der Seite, wo die Welle herauskommt - da passen die nämlich noch daneben.