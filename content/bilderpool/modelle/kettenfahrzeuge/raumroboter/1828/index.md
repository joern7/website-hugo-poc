---
layout: "image"
title: "Räumroboter_004"
date: "2003-10-14T11:28:35"
picture: "rBot004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Räumroboter"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1828
- /details0e3e.html
imported:
- "2019"
_4images_image_id: "1828"
_4images_cat_id: "194"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:28:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1828 -->
IR-Ferngesteuerter Räumroboter