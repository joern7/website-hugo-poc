---
layout: "image"
title: "Wartung und Erweiterung"
date: "2013-08-07T18:39:27"
picture: "IMG_9078.jpg"
weight: "51"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37237
- /details99cb.html
imported:
- "2019"
_4images_image_id: "37237"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37237 -->
Nachdem die Ketten entfernt wurden, lassen sich die Motorblöcke mit nur wenigen Handgriffen entfernen.