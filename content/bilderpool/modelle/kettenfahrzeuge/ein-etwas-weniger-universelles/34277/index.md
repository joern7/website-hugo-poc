---
layout: "image"
title: "Kleiner Umbau"
date: "2012-02-19T15:44:06"
picture: "IMG_3622.jpg"
weight: "9"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: ["Modding"]
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34277
- /details2f6b.html
imported:
- "2019"
_4images_image_id: "34277"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34277 -->
Aus einem 40-er Zahnrad machte ich ein 20-er Impulsrad. Da das Fahrzeug relativ groß ist, bewegt es sich bei einer Umdrehung ca. 26cm. Bei einem 4-er Impulsrad sind das 6,5cm pro Impuls. Nicht besonders genau.