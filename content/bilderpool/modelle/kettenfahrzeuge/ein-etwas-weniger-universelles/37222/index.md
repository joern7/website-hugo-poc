---
layout: "image"
title: "Von schräg vorn"
date: "2013-08-06T18:51:14"
picture: "IMG_7835.jpg"
weight: "37"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37222
- /detailsd562.html
imported:
- "2019"
_4images_image_id: "37222"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37222 -->
