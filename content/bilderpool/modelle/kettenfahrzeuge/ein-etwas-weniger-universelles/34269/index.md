---
layout: "image"
title: "Der absolute Prototyp"
date: "2012-02-19T15:44:06"
picture: "IMG_3550.jpg"
weight: "1"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34269
- /details4894.html
imported:
- "2019"
_4images_image_id: "34269"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34269 -->
ein sehr grobes modell, um die größen zu bestimmen. und siehe da, es ist etwa so groß wie das versagermodell (großes universelles kettenfahrzeug) der letzten generation.