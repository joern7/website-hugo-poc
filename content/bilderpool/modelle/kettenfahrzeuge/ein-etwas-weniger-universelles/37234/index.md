---
layout: "image"
title: "Antrieb 3"
date: "2013-08-07T18:39:26"
picture: "IMG_9076.jpg"
weight: "48"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37234
- /details253c.html
imported:
- "2019"
_4images_image_id: "37234"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37234 -->
Die letzte Achse treibt sowohl die vorderen als auch die unteren Antriebsräderpaare an. Die unteren Antriebsräder liegen etwa 1 bis 2 mm höher, als die unteren mittleren Laufräder. Somit müssen sie nicht die Last tragen. Für das Fahrwerk habe ich mir bei fischerfriendswoman ausschließlich sehr leicht drehbare Drehkränze bestellt.
Wenn mich mein technisches Wissen nicht im Stich läßt, besitzt das Vehikel eine Antriebskraft von 24Nm. Da das Ganze natürlich auf Kosten der Geschwindigkeit geht, plane ich einen Akku, der nur für den Antrieb dient und eine 25% höhere Nennspannung hat.