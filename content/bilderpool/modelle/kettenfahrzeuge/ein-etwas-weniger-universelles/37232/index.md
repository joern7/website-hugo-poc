---
layout: "image"
title: "Antrieb"
date: "2013-08-07T18:39:26"
picture: "IMG_9072.jpg"
weight: "46"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37232
- /details3030.html
imported:
- "2019"
_4images_image_id: "37232"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37232 -->
Das Modell wiegt in dieser Ausbaustufe 7,9Kg. Jeweils 2 Motoren werden über ein Differential gekoppelt. Das ist auch seine einzige Funktion. Dieses wiederum treibt zwei Schnecken an. Die Schnecken sind am Ende durch Achsen und Kugellager gelagert.