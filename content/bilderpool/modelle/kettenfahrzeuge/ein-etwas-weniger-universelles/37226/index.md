---
layout: "image"
title: "Komplett von der Seite"
date: "2013-08-06T18:51:14"
picture: "IMG_8712.jpg"
weight: "41"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37226
- /details232d.html
imported:
- "2019"
_4images_image_id: "37226"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37226 -->
Da sich das Fahrzeug ausschließlich auf ebenen Grund bewegt, habe ich auf eine Federung verzichtet. Die vier mittleren Räderpaare sind aber dennoch vertikal beweglich.