---
layout: "image"
title: "Draufsicht"
date: "2013-08-06T18:51:14"
picture: "IMG_8717.jpg"
weight: "42"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37227
- /details2aef.html
imported:
- "2019"
_4images_image_id: "37227"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37227 -->
Die Verkabelung ist natürlich noch provisorisch, um die Funktion zu testen.