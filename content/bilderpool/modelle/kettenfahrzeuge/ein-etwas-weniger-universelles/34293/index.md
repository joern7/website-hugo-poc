---
layout: "image"
title: "Näher"
date: "2012-02-19T15:44:28"
picture: "IMG_3875.jpg"
weight: "25"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34293
- /details9f5a.html
imported:
- "2019"
_4images_image_id: "34293"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34293 -->
Näher