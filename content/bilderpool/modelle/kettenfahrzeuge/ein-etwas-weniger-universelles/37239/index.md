---
layout: "image"
title: "Wartung und Erweiterung 3"
date: "2013-08-07T18:39:27"
picture: "IMG_9080.jpg"
weight: "53"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37239
- /detailsda21.html
imported:
- "2019"
_4images_image_id: "37239"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37239 -->
Durch die entstandene Lücke läßt sich der Weiterbau selbst am Rahmen vornehmen.