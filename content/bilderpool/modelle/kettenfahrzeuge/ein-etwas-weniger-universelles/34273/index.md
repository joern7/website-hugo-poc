---
layout: "image"
title: "Ein Blick von der Seite"
date: "2012-02-19T15:44:06"
picture: "IMG_3591.jpg"
weight: "5"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34273
- /details1a67.html
imported:
- "2019"
_4images_image_id: "34273"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34273 -->
Nochmal von der seite.