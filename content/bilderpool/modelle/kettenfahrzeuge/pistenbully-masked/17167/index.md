---
layout: "image"
title: "Ansicht 2"
date: "2009-01-24T17:46:15"
picture: "pbrtw2.jpg"
weight: "8"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/17167
- /details77e8.html
imported:
- "2019"
_4images_image_id: "17167"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-24T17:46:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17167 -->
Natürlich auch mit Treppe und Türen.