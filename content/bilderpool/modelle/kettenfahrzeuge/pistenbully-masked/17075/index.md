---
layout: "image"
title: "Frontansicht"
date: "2009-01-18T20:08:00"
picture: "pb1.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/17075
- /details6c09.html
imported:
- "2019"
_4images_image_id: "17075"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-18T20:08:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17075 -->
Jedes Jahr nach dem Skiurlaub will ich einen Pistenbully bauen - jetzt hab ichs auch getan, zumindestens in einem ersten Entwurf.
Grundlage ist ein verlängerter PowerBulldozer, von dem aber nicht sonderlich viel übrig geblieben ist.
Auf den Drehkranz hinten soll noch eine Winde, die dann exakt die selbe Geschwindigkeit haben soll wie der Fahrantrieb. Damit ziehen sich die Raupen an besonders steilen Hängen hoch, wenn der Fahrantrieb nicht mehr ausreicht.
Die Beleuchtung sind 18.000mcd-Led-Bausteine, die ich gestern zusammengelötet habe.

Das größte Problem ist aktuell die Stromabschaltung der ft-Fernsteuerung bei 250mA pro Ausgang. Denn das haben die PowerMotoren schnell weg. Schon drehen auf der Stelle (auf Laminat) kann eine echte Herausforderung werden, Lenken auf Teppich ist so gut wie unmöglich.