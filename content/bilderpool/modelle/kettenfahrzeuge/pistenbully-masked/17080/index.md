---
layout: "image"
title: "Kettenaufhängung"
date: "2009-01-18T20:08:01"
picture: "pb6.jpg"
weight: "6"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/17080
- /details9256.html
imported:
- "2019"
_4images_image_id: "17080"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-18T20:08:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17080 -->
Übernommen von den PowerBulldozern, nur etwas verlängert und verdoppelt. Das vordere Zahnrad auf der rechten Seite ist mangels eines weiteren Kettenrades Z20 einfach eine gestutzte Nabe mit Z20.