---
layout: "image"
title: "Aber wehe, ..."
date: "2010-12-18T14:02:47"
picture: "kettenfahrzeugschnee3.jpg"
weight: "3"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/29473
- /detailsf5b6.html
imported:
- "2019"
_4images_image_id: "29473"
_4images_cat_id: "2144"
_4images_user_id: "373"
_4images_image_date: "2010-12-18T14:02:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29473 -->
...es kommt das Ende der bei der letzten Fahrt schon zusammengedrückten Strecke. Hier sieht man auch schon, dass der "Schutz" hinten mehr stört als hilft, weil sich der Schnee darin festsetzt.