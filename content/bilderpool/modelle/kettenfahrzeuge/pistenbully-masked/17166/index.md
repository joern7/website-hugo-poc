---
layout: "image"
title: "Pistenbully-Rettungswagen"
date: "2009-01-24T17:46:14"
picture: "pbrtw1.jpg"
weight: "7"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/17166
- /details3a27.html
imported:
- "2019"
_4images_image_id: "17166"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17166 -->
Das Vorbild hierzu ist dieses Fahrzeug von System Strobel: http://www.system-strobel.de/sonder_koffer200.php
Genau wie beim Original lässt sich der Kofferaufbau sehr einfach abnehmen/aufsetzen und somit kann der Pistenbully auch noch als normales Fahrzeug verwendet werden.
Leider fehlt mir eine zweite blaue Leuchtkappe, deshalb musste eine orangene herhalten...