---
layout: "image"
title: "2. Steuerungsmöglichkeit 1"
date: "2006-12-03T13:49:45"
picture: "kettenfahrzeugmitgleichlaufgetriebe15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7659
- /details641f-2.html
imported:
- "2019"
_4images_image_id: "7659"
_4images_cat_id: "717"
_4images_user_id: "502"
_4images_image_date: "2006-12-03T13:49:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7659 -->
Ich hab mir noch ein Joystick gebaut mit dem das Kettenfahrzeug gesteuert werden kann. Allerdings hängt das ding dann an einem Kabel. Innendrin sind einfach 4 Taster um die Achse drum herum. Das Ding ist so hoch weil ich noch Gummis zur verstärkung eingebaut habe.