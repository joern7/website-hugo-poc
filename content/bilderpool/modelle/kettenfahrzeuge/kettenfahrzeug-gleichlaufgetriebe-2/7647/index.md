---
layout: "image"
title: "Kettenfahrzeug  von der Seite"
date: "2006-12-03T13:49:35"
picture: "kettenfahrzeugmitgleichlaufgetriebe03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7647
- /details640e.html
imported:
- "2019"
_4images_image_id: "7647"
_4images_cat_id: "717"
_4images_user_id: "502"
_4images_image_date: "2006-12-03T13:49:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7647 -->
Die beiden mittleren Zahräder sind etwas weiter unten damit man gute Kurven fahren kann.