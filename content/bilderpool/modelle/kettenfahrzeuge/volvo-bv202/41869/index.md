---
layout: "image"
title: "neue Lenkung-Seitenansicht"
date: "2015-08-28T21:20:28"
picture: "volvobv2.jpg"
weight: "46"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/41869
- /details207a.html
imported:
- "2019"
_4images_image_id: "41869"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41869 -->
