---
layout: "image"
title: "Lenkung-Seitenansicht"
date: "2015-05-01T22:04:59"
picture: "volvobv20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40913
- /detailsfcf7.html
imported:
- "2019"
_4images_image_id: "40913"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40913 -->
Hier schön zu sehen der Kabelstrang zwischen Vorder- und Hinterwagen. Er beinhaltet Masse und Pluspol des Akkus (für den Kompressor, die Magnetventile sowie die Beleuchtung) und 2 Motorausgänge der Fernsteruerung (Steuerung von Lenkung und Antriebsmotor).