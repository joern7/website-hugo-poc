---
layout: "image"
title: "Verdrehausgleich-Detail2"
date: "2015-08-28T21:20:28"
picture: "volvobv6.jpg"
weight: "50"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/41873
- /detailsee0d.html
imported:
- "2019"
_4images_image_id: "41873"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41873 -->
Hier ist das dritte Gelenk besser sichtbar.