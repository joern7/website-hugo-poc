---
layout: "image"
title: "Gesamtansicht-1"
date: "2015-05-01T22:04:59"
picture: "volvobv01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40894
- /details36be.html
imported:
- "2019"
_4images_image_id: "40894"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40894 -->
Hier nun die Weiterentwicklung des Hägglunds (http://ftcommunity.de/details.php?image_id=38167#col3). Diesmal ist er aus 100% fischertechnik gebaut!! 

Wie beim ersten Modell sind alle vier Ketten angetrieben und gelenkt wird durch eine Knicklenkung.
Der Antrieb erfolgt durch einen XM-Motor. Gelenkt wird durch vier Pneumatikzylinder, die durch 4 Magnetventile gesteuert werden und durch den blauen ft-Kompressor mit Druckluft versorgt werden. Am Hinterwagen ist eine Hebebühne montiert, die wiederum von 2 Magnetventilen gesteuert wird. Akku sowie Fernsteuerung befinden sich ebenfals im Hinterwagen.

Bei ca. 25° Steigung bleibt im Betrieb über Akku und Fernsteuerung der Antriebsmotor stehen, trotzdem würde ich das Fahrzeug für extrem geländegängig einstufen.

Länge: ca. 56,5 cm
Breite: ca. 16 cm
Höhe des Vordewagens: ca. 15 cm