---
layout: "image"
title: "Hebebühne-3"
date: "2015-05-01T22:04:59"
picture: "volvobv31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40924
- /details99b7-2.html
imported:
- "2019"
_4images_image_id: "40924"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40924 -->
