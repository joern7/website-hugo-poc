---
layout: "image"
title: "Unterseite-Hinterwagen"
date: "2015-05-01T22:04:59"
picture: "volvobv42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40935
- /detailsec1f.html
imported:
- "2019"
_4images_image_id: "40935"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40935 -->
