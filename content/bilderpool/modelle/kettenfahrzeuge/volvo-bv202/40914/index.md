---
layout: "image"
title: "Lenkung-unten"
date: "2015-05-01T22:04:59"
picture: "volvobv21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40914
- /detailsfb67.html
imported:
- "2019"
_4images_image_id: "40914"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40914 -->
