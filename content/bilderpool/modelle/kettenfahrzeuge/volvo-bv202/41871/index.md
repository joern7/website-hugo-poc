---
layout: "image"
title: "neue Lenkung-max. Lenkeinschlag"
date: "2015-08-28T21:20:28"
picture: "volvobv4.jpg"
weight: "48"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/41871
- /details9851.html
imported:
- "2019"
_4images_image_id: "41871"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41871 -->
Auch der maximale Lenkeinschlag ist ziemlich gleich geblieben.