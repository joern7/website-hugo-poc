---
layout: "image"
title: "Hinterwagen"
date: "2015-05-01T22:04:59"
picture: "volvobv27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40920
- /detailsc792.html
imported:
- "2019"
_4images_image_id: "40920"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40920 -->
Hier schön zu sehen die Hebebühne, zur Steuerung befindt sich jeweils ein Magnetventil in den Boxen am vorderen Ende des Hinterwagens.
Die Steurerung funktioniert gleich wie die der Lenkzylinder, aber dadurch dass sich die Hebebühne von selbst wieder nach unten bewegt, benötigt man nur 2 Magnetventile.