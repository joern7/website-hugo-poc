---
layout: "comment"
hidden: true
title: "20570"
date: "2015-05-02T12:26:12"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wenn ich die Beschreibung richtig verstehe, ist es Dir durch "gegeneinander Schalten" zweier Magnetventile gelungen, ein wirklich sperrendes und nicht nur mit Abluft verbindendes Magnetventil herzustellen. Das ist wirklich Klasse! Mir fiel bislang keine gescheite Möglichkeit ein, den Abluftausgang der Magnetventile zu schließen oder zu drosseln, aber mit zweien geht es wohl. Super.
Gruß,
Stefan