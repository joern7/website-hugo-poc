---
layout: "image"
title: "Unterseite"
date: "2015-05-01T22:04:59"
picture: "volvobv37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40930
- /details4cfb-3.html
imported:
- "2019"
_4images_image_id: "40930"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40930 -->
Der Antrieb geht vom XM-Motor direkt aufs Mitteldifferential, dann nach vorne/hinten zu einem Getriebe und dann auf die Antriebsdifferentiale. Die Welle links des Differentials ist innsgesammt um das Doppelte übersetzt, als die rechte Welle, weil der Hinterwagen etwas leichter ist und sich das somit ganz gut ausgleicht.