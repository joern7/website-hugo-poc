---
layout: "image"
title: "Gesamtansicht-2"
date: "2015-05-01T22:04:59"
picture: "volvobv02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40895
- /detailsbd92.html
imported:
- "2019"
_4images_image_id: "40895"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40895 -->
