---
layout: "image"
title: "Hydraulik"
date: "2007-12-23T13:36:18"
picture: "Universal-Schneeschieberaupe9.jpg"
weight: "9"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13153
- /details0cc9.html
imported:
- "2019"
_4images_image_id: "13153"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-23T13:36:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13153 -->
Hier sieht man den "Wirkzylinder", der von einem Hubgetriebe bewegt wird. Er wird später im Führerhaus Platz finden.