---
layout: "comment"
hidden: true
title: "8069"
date: "2008-12-26T11:20:28"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Dies sollte kein scale-Modell bauen, ich baue eben immer so wie ich es für richtig halte und da das Teil auch im echten Schnee im Einsatz war brauche es eben ein großes Schild, das kann man von den Proportionen her nicht mit einem normalen Schieber vergleichen:
Echte Raupe -> ft Raupe
Die Raupe wird kleiner
Echter Schnee -> gleicher Schnee in dem ich mit der ft Raupe fahre
=> Die Raupe wird kleiner, der Schnee nicht. Damit der Schnee nicht über das Schild quillt und sich auf die Raupe setzt braucht man ein großes Schild, das weiß jeder der schon mal im Schnee Modellauto gefahren ist.