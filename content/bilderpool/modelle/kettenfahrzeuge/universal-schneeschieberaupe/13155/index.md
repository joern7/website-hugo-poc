---
layout: "image"
title: "Im Einsatz"
date: "2007-12-24T12:51:58"
picture: "Universal-Schneeschieberaupe11.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13155
- /detailsae06.html
imported:
- "2019"
_4images_image_id: "13155"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-24T12:51:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13155 -->
Lampen fehlen noch, leider auch der Schnee (schnief). Deswegen habe ich schonmal den VBoden unterm Christbauem von Tannennadeln befreit, wie man sehen kann. Die Hydraulik tut gute Dienste. 2 Pneumatikzylinder häten hier Schwierigkeiten gehabt, weil der Räumschild sehr massiv und schwer ist. Die Hydraulik packt das im Nu.