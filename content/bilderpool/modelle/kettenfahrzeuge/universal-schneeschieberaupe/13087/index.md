---
layout: "image"
title: "Heckansicht"
date: "2007-12-17T18:22:08"
picture: "Universal-Schneeschieberaupe3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13087
- /details6920.html
imported:
- "2019"
_4images_image_id: "13087"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-17T18:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13087 -->
Hier von hinten, wo man auch ein wenig vom Getriebe sieht.