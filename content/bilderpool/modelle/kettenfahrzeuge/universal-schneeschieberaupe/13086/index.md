---
layout: "image"
title: "Frontansicht"
date: "2007-12-17T18:22:08"
picture: "Universal-Schneeschieberaupe2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13086
- /detailsb22f.html
imported:
- "2019"
_4images_image_id: "13086"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-17T18:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13086 -->
Hier von vorne. Der Akku und Empfänger sind noch nicht endgültig platziert. Ach ja, das Gefährt hat Doppelketten wie man sehen kann.