---
layout: "comment"
hidden: true
title: "13379"
date: "2011-01-26T17:49:01"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Also manchmal möchte man meinen, dass Leute absichtlich düstere Bilder machen.

Ich möchte mal ein Experiment vorschlagen: mach bitte das gleiche Foto nochmal, nur mit irgend etwas hellem (Pappe, Bettuch, irgendwas) 1. auf dem Boden und 2. als Hintergrund, anstelle oder auf den Bauplatten da hinten.

Fürs Gelände wäre es wohl besser, das Stützrad (oben) zu einer weiteren Laufrolle (unten) zu machen. 

Gruß,
Harald