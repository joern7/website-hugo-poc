---
layout: "image"
title: "von Rechts"
date: "2011-01-26T15:11:16"
picture: "explorer02.jpg"
weight: "2"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/29803
- /details2eb3.html
imported:
- "2019"
_4images_image_id: "29803"
_4images_cat_id: "2193"
_4images_user_id: "1275"
_4images_image_date: "2011-01-26T15:11:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29803 -->
Hier kann man die Aufhängung für die Kamera sehen.