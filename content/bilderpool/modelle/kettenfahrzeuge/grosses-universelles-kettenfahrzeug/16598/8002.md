---
layout: "comment"
hidden: true
title: "8002"
date: "2008-12-13T14:00:37"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
In der Tat, das wird was Großes.

Mit den vielen Zahnrädern ist nicht viel geholfen, wenn die Kette die Schwachstelle ist.

Die Federung muss aber schon alle Laufräder umfassen, sonst hast du keinen Nutzen davon. Der Punkt ist, dass unnötige Hubarbeit geleistet wird, wenn beim Überfahren eines Steinchens das ganze Gefährt um die Höhe des Steinchens angehoben wird. Diese Hubarbeit geht vom Vorschub ab, und das Fahrzeug wird langsamer. Bei 10 kg Gewicht macht sich das schon bemerkbar.

Gruß,
Harald