---
layout: "image"
title: "KF 2.10"
date: "2008-12-12T22:54:13"
picture: "Bild_003.jpg"
weight: "10"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16598
- /detailscf08-2.html
imported:
- "2019"
_4images_image_id: "16598"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16598 -->
Beide Seitenteile.