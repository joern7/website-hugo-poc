---
layout: "image"
title: "KF 3.9"
date: "2008-12-30T18:22:36"
picture: "IMG_2911.jpg"
weight: "20"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16812
- /detailsc553-3.html
imported:
- "2019"
_4images_image_id: "16812"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16812 -->
Viel Innenraum für Elektronik. Wenn nötig kommen da zwei Interfaces - ein neueres und ein älteres - in der maximalen Ausbaustufe zur Anwendung. Inwieweit die Interfaces miteinander korrespondieren, weiß ich noch nicht. Geplant ist auch eine stationäre Ladestation, die ähnlich wie ein Greifer aufgebaut sein soll.