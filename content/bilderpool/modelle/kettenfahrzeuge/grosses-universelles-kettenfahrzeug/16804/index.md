---
layout: "image"
title: "KF 3.1"
date: "2008-12-30T17:03:01"
picture: "IMG_2906.jpg"
weight: "12"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16804
- /details940e.html
imported:
- "2019"
_4images_image_id: "16804"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T17:03:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16804 -->
Das ist Revision 3. Da das Fahrwerk nicht 100% verwindungssteif ist, traten an den Antriebszahnrädern Verspannungen auf, die das Fahrzeug zu stark abbremsten. Deswegen bin ich wieder zur einfacheren Lösung zurückgekommen. Was aber wieder die Radnabe zum Problem werden läßt. Aus Mangel an Bauteilen sind die Antriebsräder in der Mitte noch einfach ausgeführt. Doppelte Ketten wären zwar schön, bringen aber zusätzliche Probleme, erhöhen das Gewicht enorm und ist auch eine Kostenfrage. Eine Kette besteht aus 332 Gliedern und 166 Belägen.