---
layout: "image"
title: "KF 2.3"
date: "2008-12-12T22:54:12"
picture: "IMG_2795.jpg"
weight: "3"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16588
- /detailsdb9f.html
imported:
- "2019"
_4images_image_id: "16588"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16588 -->
Als Kettenspanner dienen Federgelenksteine.