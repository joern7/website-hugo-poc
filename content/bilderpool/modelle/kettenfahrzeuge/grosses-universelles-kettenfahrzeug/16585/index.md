---
layout: "image"
title: "KF 2.1"
date: "2008-12-12T22:53:59"
picture: "IMG_2809.jpg"
weight: "1"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16585
- /details2770.html
imported:
- "2019"
_4images_image_id: "16585"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:53:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16585 -->
Da ich vorhabe, ein großes Kettenfahrzeug zu bauen, machte mir die Festigkeit der Radnaben Sorgen. Also teilte ich die Antriebskräfte auf zwei Naben auf.Dazwischen das eigentliche Antriebsrad, das durch drei 30mm Kunststoffachsen in Position gehalten wird. Das ganze ist außerordentlich fest und robust.