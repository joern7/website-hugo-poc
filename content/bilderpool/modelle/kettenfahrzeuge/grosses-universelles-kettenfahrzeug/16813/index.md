---
layout: "image"
title: "KF 3.10"
date: "2008-12-30T19:18:00"
picture: "IMG_2930.jpg"
weight: "21"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16813
- /detailsb52b.html
imported:
- "2019"
_4images_image_id: "16813"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T19:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16813 -->
Damit der Greifer bis zum Boden langt, muß er etwa drei mal so lang werden.