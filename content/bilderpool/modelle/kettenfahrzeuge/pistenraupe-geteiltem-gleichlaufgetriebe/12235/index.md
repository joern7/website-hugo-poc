---
layout: "image"
title: "Pistenraupe 1"
date: "2007-10-16T20:59:37"
picture: "Pistenraupe_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12235
- /detailsbf81.html
imported:
- "2019"
_4images_image_id: "12235"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T20:59:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12235 -->
Hier nun die erste Pistenraupe in der ftCommunity! :o)

Äußerlich kompakt, sehr flink, wendig und nach klassischem Vorbild, aber technisch mit einer Neuerung ausgestattet, dem "geteilten Gleichlaufgetriebe"! Details gibt es dazu später bei der Unterbodenansicht.

Angetrieben wird die Pistenraupe von zwei Mini-Motoren, und der Accupack ist auch an Bord. Gesteuert wird sie mit einer Kabelfernsteuerung.