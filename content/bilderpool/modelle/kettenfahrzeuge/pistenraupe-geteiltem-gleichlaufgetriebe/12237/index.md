---
layout: "image"
title: "Pistenraupe 3"
date: "2007-10-16T20:59:38"
picture: "Pistenraupe_05.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12237
- /detailsea63.html
imported:
- "2019"
_4images_image_id: "12237"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T20:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12237 -->
Hier die Standardausrüstung mit Räumschild (vorn) und Glättbrett (hinten).

Das Räumschild wird über den Winkelträger 30 in die beiden Kupplungen eingerastet und hält somit verdrehsicher.