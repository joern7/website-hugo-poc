---
layout: "image"
title: "Pistenraupe 2"
date: "2007-10-16T20:59:38"
picture: "Pistenraupe_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12236
- /detailsb7b9.html
imported:
- "2019"
_4images_image_id: "12236"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T20:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12236 -->
Vorn und hinten gibt es Kupplungen zum Einrasten von Anbauteilen und Werkzeugen.