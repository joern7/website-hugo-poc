---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:30:57"
picture: "Kettenfahrwerk1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9370
- /detailsd92e.html
imported:
- "2019"
_4images_image_id: "9370"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9370 -->
Das ist der Anfang eines Kettenfahrwerks.
Das Gleichlaufgetriebe wurde auch schon ausgestellt.