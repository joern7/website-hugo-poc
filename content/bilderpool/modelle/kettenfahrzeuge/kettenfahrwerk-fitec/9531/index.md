---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-15T19:02:23"
picture: "Kettenfahrwerk16.jpg"
weight: "16"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9531
- /details6bf3.html
imported:
- "2019"
_4images_image_id: "9531"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-15T19:02:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9531 -->
Hier sieht man gut die Federung. Vorher hatte das Kettenfahrwerk keine Federung.