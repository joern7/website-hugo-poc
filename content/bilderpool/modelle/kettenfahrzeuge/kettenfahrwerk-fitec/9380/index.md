---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:31:36"
picture: "Kettenfahrwerk11.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9380
- /details3fb3.html
imported:
- "2019"
_4images_image_id: "9380"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9380 -->
Wie man sieht hat es genug Bodenabstand für schwieriges Gelände.