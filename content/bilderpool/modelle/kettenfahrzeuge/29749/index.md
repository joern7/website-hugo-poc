---
layout: "image"
title: "'Kastomobil'"
date: "2011-01-22T17:25:25"
picture: "kastomobil1.jpg"
weight: "3"
konstrukteure: 
- "Lars B."
fotografen:
- "Lars B."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29749
- /details7297.html
imported:
- "2019"
_4images_image_id: "29749"
_4images_cat_id: "498"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T17:25:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29749 -->
zu sehen ist ein kettenfahrzeug dass sogut wie nie fahrunfähig ist ( theopraktisch gesehen )
es kann auf 4 von 6 seiten fahren  ( ca.66%)