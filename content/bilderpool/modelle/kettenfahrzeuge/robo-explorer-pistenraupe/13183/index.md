---
layout: "image"
title: "anleitung 1"
date: "2008-01-01T09:40:10"
picture: "roboexplorerpistenraupe04.jpg"
weight: "4"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- /php/details/13183
- /details47cc.html
imported:
- "2019"
_4images_image_id: "13183"
_4images_cat_id: "1191"
_4images_user_id: "642"
_4images_image_date: "2008-01-01T09:40:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13183 -->
man benötigt: ein Robo Explorer Grundmodel mit rest teilen, Zwei rote kleine Platten, zwei Gelenke und ein Schild. Dann kann man meine Pistenraupe nachbauen. Programme werden noch zum download hinzugefügt.