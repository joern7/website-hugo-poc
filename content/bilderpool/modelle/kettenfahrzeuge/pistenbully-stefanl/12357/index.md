---
layout: "image"
title: "Pistenbully 5"
date: "2007-10-28T12:45:39"
picture: "pistenbully05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12357
- /detailsafd1.html
imported:
- "2019"
_4images_image_id: "12357"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12357 -->
