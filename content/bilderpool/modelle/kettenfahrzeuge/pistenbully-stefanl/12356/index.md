---
layout: "image"
title: "Pistenbully 4"
date: "2007-10-28T12:45:39"
picture: "pistenbully04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12356
- /details3927.html
imported:
- "2019"
_4images_image_id: "12356"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12356 -->
