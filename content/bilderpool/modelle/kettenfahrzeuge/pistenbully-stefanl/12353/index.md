---
layout: "image"
title: "Pistenbully 1"
date: "2007-10-28T12:45:38"
picture: "pistenbully01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12353
- /details0acd-2.html
imported:
- "2019"
_4images_image_id: "12353"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12353 -->
