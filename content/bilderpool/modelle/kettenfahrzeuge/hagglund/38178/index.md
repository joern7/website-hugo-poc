---
layout: "image"
title: "Motorraum"
date: "2014-02-05T12:24:18"
picture: "haegglund12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38178
- /details97e6-2.html
imported:
- "2019"
_4images_image_id: "38178"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38178 -->
Der Antrieb erfoglt durch 2  1:50 Powermotoren