---
layout: "image"
title: "Klettern4"
date: "2014-02-05T12:24:18"
picture: "haegglund33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38199
- /detailsbde3.html
imported:
- "2019"
_4images_image_id: "38199"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38199 -->
