---
layout: "overview"
title: "Hägglund"
date: 2020-02-22T08:37:22+01:00
legacy_id:
- /php/categories/2841
- /categories5f18.html
- /categories7238.html
- /categories58c6.html
- /categories2d28.html
- /categories8b3a.html
- /categories8bc2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2841 --> 
Für jedes Gelände geeignet ;-)

Gewicht: ca. 3 kg
Länge: 54 cm
Breite: 17 cm