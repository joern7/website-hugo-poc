---
layout: "image"
title: "linke Seite"
date: "2014-02-05T12:24:18"
picture: "haegglund06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38172
- /detailsf28a.html
imported:
- "2019"
_4images_image_id: "38172"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38172 -->
