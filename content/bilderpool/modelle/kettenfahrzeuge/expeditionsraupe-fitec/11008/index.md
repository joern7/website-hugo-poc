---
layout: "image"
title: "Expeditionsraupe"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11008
- /detailsc7f8.html
imported:
- "2019"
_4images_image_id: "11008"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11008 -->
Besonder gut ist, dass es so klein, kompakt und trozdem so geländetüchtig ist.