---
layout: "image"
title: "Raupe im Gelände"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe6.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11005
- /detailseaaf.html
imported:
- "2019"
_4images_image_id: "11005"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11005 -->
Die Raupe rechts unten im Bild, ist die von tim-tech. Jetzt wisst ihr auch wieso ich immer mit Empfänger 2 fahre.