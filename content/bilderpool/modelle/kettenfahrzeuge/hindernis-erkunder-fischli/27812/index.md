---
layout: "image"
title: "2"
date: "2010-08-08T17:09:26"
picture: "herberthindernis2.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27812
- /details5260-2.html
imported:
- "2019"
_4images_image_id: "27812"
_4images_cat_id: "2005"
_4images_user_id: "1082"
_4images_image_date: "2010-08-08T17:09:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27812 -->
Dies sind die beiden Fühler vorne. Wird der rechte gedrückt, fährt Herbert 2 Sekunden Rückwärts und dreht sich anschließend 1 Sekunde nach links. Das ist dann ungefähr eine viertel Drehung. Wird der linke Fühler betätigt, passiert das gleiche, nur er dreht sich anders herum.