---
layout: "image"
title: "5"
date: "2010-08-08T17:09:26"
picture: "herberthindernis5.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27815
- /details4842.html
imported:
- "2019"
_4images_image_id: "27815"
_4images_cat_id: "2005"
_4images_user_id: "1082"
_4images_image_date: "2010-08-08T17:09:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27815 -->
Hier sieht man nochmal wie klein er ist, kaum größer als meine Hand.