---
layout: "image"
title: "Seitenansicht"
date: "2007-01-08T18:01:30"
picture: "Kettenfahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8353
- /detailsc34c.html
imported:
- "2019"
_4images_image_id: "8353"
_4images_cat_id: "769"
_4images_user_id: "456"
_4images_image_date: "2007-01-08T18:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8353 -->
Hier sieht man es von der Seite. Die Metallachse in den Grundbausteinen ist gut für Stabilität.