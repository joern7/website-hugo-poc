---
layout: "image"
title: "Kettenfahrzeug"
date: "2007-01-08T18:01:30"
picture: "Kettenfahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8350
- /details2c56.html
imported:
- "2019"
_4images_image_id: "8350"
_4images_cat_id: "769"
_4images_user_id: "456"
_4images_image_date: "2007-01-08T18:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8350 -->
Hier ist mein 2. Kettenfahrzeug. Es hat ein Gleichlaufgetriebe und ist für härtestes Gelände gebaut. Getestet habe ich es in/auf:-Teppichboden
              -Parkett
              -Wiese/Rasen
              -Schlam/Erde