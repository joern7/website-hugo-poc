---
layout: "image"
title: "Heckansicht"
date: "2007-01-08T18:01:30"
picture: "Kettenfahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8352
- /detailsfa26.html
imported:
- "2019"
_4images_image_id: "8352"
_4images_cat_id: "769"
_4images_user_id: "456"
_4images_image_date: "2007-01-08T18:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8352 -->
Hier sieht man es von hinten. Angebaut habe ich eine Lampe und eine Seilwinde. Einer meiner Freunde baut auch fischertechnik und damit konnte ich sein Auto apschleppen, wenn es nicht mehr weitergekommen ist.