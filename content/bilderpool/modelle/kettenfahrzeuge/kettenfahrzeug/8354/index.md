---
layout: "image"
title: "Von oben"
date: "2007-01-08T18:01:30"
picture: "Kettenfahrzeug5.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8354
- /detailsf0d6.html
imported:
- "2019"
_4images_image_id: "8354"
_4images_cat_id: "769"
_4images_user_id: "456"
_4images_image_date: "2007-01-08T18:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8354 -->
Hier sieht man es von oben. Der Schalter ist für die Lichter.