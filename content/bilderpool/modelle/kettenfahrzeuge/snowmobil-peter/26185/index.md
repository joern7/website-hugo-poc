---
layout: "image"
title: "Snowmobil 4"
date: "2010-01-29T18:58:25"
picture: "snowmobil4.jpg"
weight: "4"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26185
- /details0421-2.html
imported:
- "2019"
_4images_image_id: "26185"
_4images_cat_id: "1857"
_4images_user_id: "998"
_4images_image_date: "2010-01-29T18:58:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26185 -->
