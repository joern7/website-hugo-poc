---
layout: "image"
title: "Snowmobil 5"
date: "2010-01-29T18:58:25"
picture: "snowmobil5.jpg"
weight: "5"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26186
- /details6031.html
imported:
- "2019"
_4images_image_id: "26186"
_4images_cat_id: "1857"
_4images_user_id: "998"
_4images_image_date: "2010-01-29T18:58:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26186 -->
Spuren im Schnee