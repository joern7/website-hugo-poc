---
layout: "image"
title: "Rohrneigung mitte"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk05.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/43436
- /detailse31e.html
imported:
- "2019"
_4images_image_id: "43436"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43436 -->
Der Motor Rutscht hierbei hin und her und zieht sich, zusammen mit dem Z30, über bzw unter die Rohrverlängerung ins innere.