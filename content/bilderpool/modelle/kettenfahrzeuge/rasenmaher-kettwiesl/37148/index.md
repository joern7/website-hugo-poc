---
layout: "image"
title: "Kettwiesl 2013 - Kettenspanner und seine Aufhängung"
date: "2013-07-06T16:01:17"
picture: "P1010271.jpg"
weight: "17"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37148
- /detailsabae.html
imported:
- "2019"
_4images_image_id: "37148"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37148 -->
Wie sich das für eine anständiges Kettenfahrzeug mit Federung der Laufrollen gehört, ist auch ein Kettenspanner für den Geometrieausgleich vorhanden. Der wurde auch nochmal überarbeitet und ist jetzt viel stabiler aufgehängt als in der alten Version. Es sind die gleichen Federn wie bei den Federbeinen verwendet.