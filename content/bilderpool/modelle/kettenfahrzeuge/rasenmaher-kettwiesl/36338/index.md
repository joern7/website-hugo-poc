---
layout: "image"
title: "Ein Schrittchen weiter"
date: "2012-12-20T17:12:15"
picture: "Hindernis_25mm.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36338
- /details7ef6-2.html
imported:
- "2019"
_4images_image_id: "36338"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36338 -->
Noch mal das Hindernis, diesmal ein paar cm weitergefahren.