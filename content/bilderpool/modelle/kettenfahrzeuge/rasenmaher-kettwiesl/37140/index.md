---
layout: "image"
title: "Kettwiesl 2013 - fast alles neu konstruiert"
date: "2013-07-06T16:01:17"
picture: "P1010286.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37140
- /detailsdef6.html
imported:
- "2019"
_4images_image_id: "37140"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37140 -->
Am alten Fahrwerk gab es zwei Probleme. Der gravierendste waren die Federbeine, aber auch der hochliegende Schwerpunkt durch die Hubmechanik des Mähwerks hatte seinen Anteil.

Wegen der langen Federwege (ca. 22mm) mußten die 23mm-Räder den 60mm-Drehscheiben weichen. Dafür gibt es jetzt nur 3 Laufrollenpaare pro Kette.