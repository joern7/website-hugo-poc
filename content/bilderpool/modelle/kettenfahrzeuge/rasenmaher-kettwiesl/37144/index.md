---
layout: "image"
title: "Kettwiesl 2013 - Seilwinde zur Höhenverstellung des Mähwerks"
date: "2013-07-06T16:01:17"
picture: "P1010266.jpg"
weight: "13"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37144
- /detailsfb64.html
imported:
- "2019"
_4images_image_id: "37144"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37144 -->
Die Seilwinde arbeitet auf beide Seiten gleichzeitig. Dadurch reduzieren sich die Radialkräfte auf die Welle erheblich und die einfache Befestigung mit den Statikstreben reicht aus. Die Seilwinde selbst besteht aus 2 Scheiben mit Hülse (35981), einer Rastachse und einem kleinen O-Ring. Der O-Ring fixiert das Seil an der Achse, man sieht ihn aber hier nicht. Leidiglich der kleine Wulst etwa in der Mitte der "Seiltrommel" läßt ihn noch erahnen.