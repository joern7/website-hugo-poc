---
layout: "image"
title: "Kettwiesl 2013 - Hubmechanik rechts"
date: "2013-07-06T16:01:17"
picture: "P1010269.jpg"
weight: "15"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37146
- /details028f.html
imported:
- "2019"
_4images_image_id: "37146"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37146 -->
Und der Endschalter für "Hubwerk ist oben" ist auch schön zu sehen. Elektrisch ist er noch nicht angeschlossen.