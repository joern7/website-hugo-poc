---
layout: "image"
title: "Kettwiesl 2013 - Montagedetail der Seilwindenhalterung"
date: "2013-07-06T16:01:17"
picture: "P1010284.jpg"
weight: "22"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk", "Clipsachse"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37153
- /detailsc2b3-2.html
imported:
- "2019"
_4images_image_id: "37153"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37153 -->
Beim Experiementieren mit der Geometrie habe ich mir eine herumliegende Achse gegriffen - das war zufälligerweise eine Clipsachse. Also flugs durch die Statikstrebe in den BS30 gedrückt um zu testen ob das Maß paßt - und dann rastet das Dingens da perfekt ein! Damit hatte ich nicht gerechnet. Hut ab vor den Entwicklern bei ft. Die Clipsachse 34 (32870) paßt rein wie extra dafür gemacht. Sie rastet perfekt in die kleine Fuge zwischen den beiden BS30 ein. Und oben stimmt das Maß exakt um die Statikstrebe locker aber sicher festzuhalten.