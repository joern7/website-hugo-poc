---
layout: "image"
title: "Rollgrenzwinkel"
date: "2012-12-20T17:12:15"
picture: "Rollgrenzwinkel.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36340
- /details1d04.html
imported:
- "2019"
_4images_image_id: "36340"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36340 -->
Noch ein bißchen mehr Neigung, und dann kippt er doch um. Das sind etwa 45°.

Also bevor das Ding kippt, rutscht es weg. So muß es sein.