---
layout: "image"
title: "Kettwiesl 2013 - Aus Sicht der Wiese"
date: "2013-07-07T12:17:48"
picture: "P1010261a.jpg"
weight: "23"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37154
- /detailse6c4-2.html
imported:
- "2019"
_4images_image_id: "37154"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-07T12:17:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37154 -->
Sorry für die schlechte Bildqualität, das Grafikprogramm ist mir gerade abgeschmiert und mir fehlt die Zeit das jetzt zu debuggen.

Die BS30 oben quer (bis auf einen alle gelb), an denen das Mähwerk hängt, werden später durch ein Alu210 ersetzt. Das Foto zeigt noch die Experimentierphase.

Die Bauplatte 120x60 trägt das Mähwerk mit Antrieb und hängt seitlich "in den Seilen". Da sind nämlich die Umlenkrollen der Höheneinstellung angebracht. Die senkrechten Metallachsen sind ebenfalls mit der Platte verbunden und gleiten durch Führungen am Querträger mit hinauf und hinunter. Die Schnitthöhe soll zwischen etwa 3cm und 8cm einstellbar sein. [edit] Laut einigen Rasenexperten soll man nicht unter 5cm mähen! [/edit]

Das Mähwerk ist aber auf diesem Bild noch nicht fertiggestellt. Ich arbeite noch daran. Wer Lust hat, kommt Ende September 2013 nach Erbes-Büdesheim und schaut sich das Gerät mal in 3D an.