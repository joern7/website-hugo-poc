---
layout: "image"
title: "Kettwiesl 2013 - Blick von hinten oben"
date: "2013-07-06T16:01:17"
picture: "P1010263.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk", "Gleichlaufgetriebe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37143
- /details209c-2.html
imported:
- "2019"
_4images_image_id: "37143"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37143 -->
Der "Maschinenraum" für den Fahrantrieb und die Hubmechanik des Mähwerks ist hier im Fokus.

Die Vorwärtsfahrt erledigt der PowerMotor mit 8:1, die Lenkung übernimmt sein Kollege mit 20:1.

Die Schnitthöhenverstellung wird per Seilwinde erledigt. Der Motor (32293) ist unterhalb der PowerMotoren untergebracht.

Das Mähwerk ist erstmal nur angedeutet, der Messerantrieb ist noch nicht eingebaut (Motor mit 4mm Welle und satten 50W bei 10.000/min. Da sind erstmal Kugellager nötig und noch so einige Schnitzereien oder auch nft-Teile für das Getriebe).