---
layout: "image"
title: "Grenzwinkel Rutschen"
date: "2012-12-20T17:12:15"
picture: "Grenzwinkel_seitliches_wegrutschen.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36339
- /details4cda.html
imported:
- "2019"
_4images_image_id: "36339"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36339 -->
Auf dem Holzbrett geht es bis hier stabil, bei stärkerer Neigung rutscht die Maschine seitlich weg.