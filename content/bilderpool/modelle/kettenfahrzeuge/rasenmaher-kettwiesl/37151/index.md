---
layout: "image"
title: "Kettwiesl 2013 - Aufhängung der Kettenspanner"
date: "2013-07-06T16:01:17"
picture: "P1010279.jpg"
weight: "20"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37151
- /details85d0-2.html
imported:
- "2019"
_4images_image_id: "37151"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37151 -->
Auf dieses Detail bin ich besonders stolz. Die Metallachsen des Kettenspanners und die Metallachsen des Federbeines kreuzen sich auf engstem Raum und sind trotzdem fest verankert. Da es keinen ft-Stein für den Job gibt, mußte ich mit ein paar Kabeldurchführungstüllen (Innendurchmesser ca. 4mm) nachhelfen.

Der BS7,5 links sorgt für den nötigen Abstand der Federn zu den Drehscheiben, da sonst die Scheiben an den Federn scheuern würden. Die beiden BS7,5 rechts helfen mit die Metallachsen des Kettenspanners stabil zu fixieren.