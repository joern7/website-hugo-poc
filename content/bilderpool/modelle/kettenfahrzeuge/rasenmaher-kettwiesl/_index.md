---
layout: "overview"
title: "Rasenmäher Kettwiesl"
date: 2020-02-22T08:37:19+01:00
legacy_id:
- /php/categories/2696
- /categories2531.html
- /categoriesac1b.html
- /categoriesd5fb.html
- /categoriese4d4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2696 --> 
Der erste funktionsfähige Rasenmäher (fast) nur aus ft -Teilen. Gebaut als Hilfe für Mäharbeiten in steilem Gelände.
 - Kettenfahrwerk voll gefedert
 - automatische Kettenspanner
 - höhenverstellbares Mähwerk (Schneidteller)