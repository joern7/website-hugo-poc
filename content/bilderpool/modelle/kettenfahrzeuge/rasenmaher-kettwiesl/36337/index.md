---
layout: "image"
title: "Fahrwerk bei der Arbeit"
date: "2012-12-20T17:12:15"
picture: "Rechte_Kette_Hindernis_25mm.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36337
- /detailsc205.html
imported:
- "2019"
_4images_image_id: "36337"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36337 -->
Hier die andere Seite an exakt der gleichen Position wie vor. Das Chassis ist recht steif geworden.