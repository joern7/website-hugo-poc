---
layout: "image"
title: "Fahrwerk bei der Arbeit"
date: "2012-12-20T17:12:15"
picture: "Linke_Kette_Hindernis_25mm.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36336
- /details5d38.html
imported:
- "2019"
_4images_image_id: "36336"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36336 -->
Bedingt durch das Wetter (Winter) gibt es ein simuliertes Gelände, naja. Eignetlich ist es nur ein Holzbrett und noch zwei Wäscheklammern, die eine Kante mit etwa 25mm Höhe bilden. Deutlich ist das Einfedern zu erkennen.