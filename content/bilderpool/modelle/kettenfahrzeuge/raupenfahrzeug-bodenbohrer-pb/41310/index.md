---
layout: "image"
title: "Rups-33"
date: "2015-06-26T19:36:40"
picture: "raupen32.jpg"
weight: "32"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41310
- /details6ee7.html
imported:
- "2019"
_4images_image_id: "41310"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41310 -->
