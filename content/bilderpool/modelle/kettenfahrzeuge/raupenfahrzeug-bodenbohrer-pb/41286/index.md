---
layout: "image"
title: "Rups-09"
date: "2015-06-26T19:36:39"
picture: "raupen08.jpg"
weight: "8"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41286
- /details62a1.html
imported:
- "2019"
_4images_image_id: "41286"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41286 -->
