---
layout: "image"
title: "Rups-17"
date: "2015-06-26T19:36:40"
picture: "raupen16.jpg"
weight: "16"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41294
- /details7bf8.html
imported:
- "2019"
_4images_image_id: "41294"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41294 -->
