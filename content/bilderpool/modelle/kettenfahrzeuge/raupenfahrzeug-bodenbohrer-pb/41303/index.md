---
layout: "image"
title: "Rups-26"
date: "2015-06-26T19:36:40"
picture: "raupen25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41303
- /detailsff08-2.html
imported:
- "2019"
_4images_image_id: "41303"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41303 -->
