---
layout: "image"
title: "Rups-20"
date: "2015-06-26T19:36:40"
picture: "raupen20.jpg"
weight: "20"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41298
- /details59b6.html
imported:
- "2019"
_4images_image_id: "41298"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41298 -->
Mit Hilfe anderen Bausteinen (hier in Grau hinzugefügt zum Verdeutlichung) ist das massives Block später wieder zu demontieren.