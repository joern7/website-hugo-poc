---
layout: "image"
title: "Rups-01"
date: "2015-06-26T19:36:39"
picture: "raupen01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41279
- /details6cd3.html
imported:
- "2019"
_4images_image_id: "41279"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41279 -->
Ein Modell vond voriges Jahr. Ein Raupenfahrzeug das ZB Boden-Proben nehmen kann. Ich hatte hier erstmal was herumgeguckt, aber weil Ich nicht sehr technisch bin, hab Ich kein kompliziertes Antrieb gewählt. Ich wollte den Antrieb völlig symmetrisch haben. Auch möchte Ich das er grössere Hindernisse nemen kann, und auch darum hab Ich beide Motoren zum Antrieb eingesetzt und nicht Einen zum fahren und den Anderen zum Lenken. Kleine Hindernisse nimmt er alsob sie nicht da waren, und auch eine steile, uneben Rampe (von einige Kissen oder so etwas) kann er bezwingen, wenn gut angesteuert.