---
layout: "image"
title: "Rups-22"
date: "2015-06-26T19:36:40"
picture: "raupen21.jpg"
weight: "21"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41299
- /details72c8-3.html
imported:
- "2019"
_4images_image_id: "41299"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41299 -->
Abdecken mit Platten so das kein Staub, Haar oder anders Ungewünschtes dazwischen kommen kann.