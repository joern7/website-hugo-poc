---
layout: "image"
title: "Rups-03"
date: "2015-06-26T19:36:39"
picture: "raupen03.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41281
- /detailsba7a.html
imported:
- "2019"
_4images_image_id: "41281"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41281 -->
