---
layout: "image"
title: "Rups-36"
date: "2015-06-26T19:36:40"
picture: "raupen35.jpg"
weight: "35"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41313
- /detailsde53.html
imported:
- "2019"
_4images_image_id: "41313"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41313 -->
