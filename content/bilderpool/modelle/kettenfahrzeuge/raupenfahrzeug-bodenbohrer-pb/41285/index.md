---
layout: "image"
title: "Rups-08"
date: "2015-06-26T19:36:39"
picture: "raupen07.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41285
- /details3f7f.html
imported:
- "2019"
_4images_image_id: "41285"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41285 -->
