---
layout: "image"
title: "Rups-31"
date: "2015-06-26T19:36:40"
picture: "raupen30.jpg"
weight: "30"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41308
- /detailsf067.html
imported:
- "2019"
_4images_image_id: "41308"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41308 -->
Ist hier nog 'ne Alte IR-empfanger. Weil fur das Fahren besser den Neuen benutzt werden kann, soll Man das hier besser auch tun.