---
layout: "image"
title: "Pistenraupe 2"
date: "2010-01-31T17:46:50"
picture: "pistenraupepeter2.jpg"
weight: "2"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26189
- /detailsda7b.html
imported:
- "2019"
_4images_image_id: "26189"
_4images_cat_id: "1860"
_4images_user_id: "998"
_4images_image_date: "2010-01-31T17:46:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26189 -->
Bunte Kette
Die Gummibeläge kommen natürlich auf die schwarzen Glieder.