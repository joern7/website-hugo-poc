---
layout: "image"
title: "Pistenraupe 4"
date: "2010-01-31T17:46:50"
picture: "pistenraupepeter4.jpg"
weight: "4"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26191
- /details82b3.html
imported:
- "2019"
_4images_image_id: "26191"
_4images_cat_id: "1860"
_4images_user_id: "998"
_4images_image_date: "2010-01-31T17:46:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26191 -->
<h1>150%Steigung</h1>
Wahrscheinlich sogar noch mehr, aber dann fängt es an zu rutschen und ich finde keine Matte die viel Reibung hat.

PS: Bei diesen Bild fährt sie runter weil ich nicht gleichzeitig auf den Auslöseknopf und auf die Fernbedienung drücken kann.
Video kommt bald.