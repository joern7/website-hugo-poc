---
layout: "image"
title: "Pistenraupe 3"
date: "2010-01-31T17:46:50"
picture: "pistenraupepeter3.jpg"
weight: "3"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26190
- /details7114.html
imported:
- "2019"
_4images_image_id: "26190"
_4images_cat_id: "1860"
_4images_user_id: "998"
_4images_image_date: "2010-01-31T17:46:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26190 -->
Schaufel, Führerhaus und Fräße fehlen noch

Kraftvoller Antrieb mit 4 Power Motoren.
Muss nur noch ein zweites Relaismodul bauen, dann schafft es vielleicht mehr als 150% Steigung8siehe nächstes Bild)