---
layout: "image"
title: "IR Pneumatik Raupen-Löffelbagger von DasKasperle - SEITE"
date: "2013-05-26T09:50:17"
picture: "irpneumatikraupenloeffelbaggervondaskasperle01.jpg"
weight: "1"
konstrukteure: 
- "DasKasperle alias Sushiteck"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36959
- /details15af.html
imported:
- "2019"
_4images_image_id: "36959"
_4images_cat_id: "2748"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36959 -->
