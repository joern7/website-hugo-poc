---
layout: "comment"
hidden: true
title: "1714"
date: "2006-12-14T23:49:55"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Doch, das geht auch: einen Motor stillsetzen oder etwas langsamer drehen lassen als den anderen.

Ich bin immer noch der Ansicht, dass die Zahnräder da mittendrin ziemlich nutzlos sind. Sie leisten dasselbe, was auch die äußeren Ketten links und rechts auch schon leisten: die Z30 der linken Seite laufen gleich schnell, und die Z30 der rechten Seite laufen gleich schnell. Mehr aber auch nicht.

Gruß,
Harald