---
layout: "image"
title: "Seitenansicht"
date: "2009-12-16T17:58:51"
picture: "fahrwerk5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/25965
- /detailsf60a.html
imported:
- "2019"
_4images_image_id: "25965"
_4images_cat_id: "1825"
_4images_user_id: "453"
_4images_image_date: "2009-12-16T17:58:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25965 -->
Das Fahrwerk voll. Es biegt sich kaum durch bei 8 Kilo.