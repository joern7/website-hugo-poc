---
layout: "image"
title: "Der Greifer 2"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_010.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: ["Schnecke", "35977"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4626
- /details22d4-2.html
imported:
- "2019"
_4images_image_id: "4626"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4626 -->
Hier der Greifer von unten. Die beiden Rastschnecken sind gleichzeitig die tragenden Elemente für die beweglichen Zangenhälften.