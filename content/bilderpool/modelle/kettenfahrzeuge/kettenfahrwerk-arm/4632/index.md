---
layout: "image"
title: "Die Schaufel 1"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_016.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4632
- /details0342.html
imported:
- "2019"
_4images_image_id: "4632"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4632 -->
Mit wenigen Handgriffen ist der Greifer durch eine Schaufel ausgetauscht. Anstatt Greifer auf/zu heißt es dann Schaufel abknicken/hochknicken.