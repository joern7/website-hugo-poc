---
layout: "image"
title: "Greiferantrieb 1"
date: "2005-08-21T20:36:53"
picture: "Kettenfahrwerk_mit_Arm_005.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4621
- /detailsbb04-2.html
imported:
- "2019"
_4images_image_id: "4621"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:36:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4621 -->
Das Z20 sitzt direkt auf dem Achsstummel eines Rast-Winkelgetriebes. Von dort geht eine Achse durch das erste Armsegment zur Mitte.