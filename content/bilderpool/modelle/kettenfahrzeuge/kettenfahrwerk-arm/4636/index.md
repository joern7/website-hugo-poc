---
layout: "image"
title: "Akkutausch 3"
date: "2005-08-21T20:54:42"
picture: "Kettenfahrwerk_mit_Arm_020.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4636
- /details5d13.html
imported:
- "2019"
_4images_image_id: "4636"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4636 -->
... der zum Laden Ruck-Zuck herausgehoben werden kann.