---
layout: "image"
title: "Akkutausch 1"
date: "2005-08-21T20:54:42"
picture: "Kettenfahrwerk_mit_Arm_018.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4634
- /details53c5.html
imported:
- "2019"
_4images_image_id: "4634"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4634 -->
Das Ganze ist immer noch wartungsfreundlich: Ein kurzer Griff, und der komplette Arm ist mitsamt seinen Motoren hochgeklappt, ...