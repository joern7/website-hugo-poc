---
layout: "image"
title: "Pneumatik Greifer mit großem Öffnungswinkel"
date: "2015-05-04T14:53:01"
picture: "greif.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Öffnungswinkel", "Greifer", "Pneumatik", "Zylinder"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40947
- /detailscec4.html
imported:
- "2019"
_4images_image_id: "40947"
_4images_cat_id: "1275"
_4images_user_id: "579"
_4images_image_date: "2015-05-04T14:53:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40947 -->
Bei relativ kleinem Hub wird hier ein großer Öffnungswinkel erreicht