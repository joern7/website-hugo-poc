---
layout: "image"
title: "umgebaute Variante des Pneumatikgreifers mit Hebemechanismus"
date: "2015-05-04T14:53:01"
picture: "hebe.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Pneumatik", "Greifer", "Hebemechanismus", "Hebevorrichtung", "Zylinder"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40946
- /details4ba8.html
imported:
- "2019"
_4images_image_id: "40946"
_4images_cat_id: "1275"
_4images_user_id: "579"
_4images_image_date: "2015-05-04T14:53:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40946 -->
Habe die tolle Vorlage von equester umgebaut auf geringere Einbaubreite und größeren Öffnungswinkel des Greifers