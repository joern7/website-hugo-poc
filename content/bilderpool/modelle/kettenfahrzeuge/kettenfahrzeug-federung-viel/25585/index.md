---
layout: "image"
title: "Gesamtübersicht"
date: "2009-10-30T21:05:03"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit1.jpg"
weight: "1"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/25585
- /details656d-2.html
imported:
- "2019"
_4images_image_id: "25585"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25585 -->
Hier sieht man das ganze Fahrzeug. Federung sowie Bodenfreiheit sind auch zu sehen (bzw. zu erahnen). Mit den Motoren nach oben gerichtet erinnert es mich ein bisschen an die alten Ozeandampfer. Die Scheinwerfer vorne brauchen natürlich nicht extra geschützt zu werden. Diesen extra Scheinwerfer-Schutz an manch (echtem) Panzer fand ich aber immer irgendwie anziehend.
Robo-Interface wäre ebenfalls nicht wirklich notwendig und wurde hauptsächlich deshalb eingebaut, weil ich die Sensoren, die ich in Erbes-Büdesheim günstig bekommen habe, noch verbauen will.
Den Fotowiderstand, um die Scheinwerfer automatisch ein und auszuschalten, habe ich (wie man sieht) bereits angeschlossen.