---
layout: "image"
title: "Mini-Kunstraupe"
date: "2006-03-16T18:01:40"
picture: "Fischertechnik_001.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5898
- /detailscadf.html
imported:
- "2019"
_4images_image_id: "5898"
_4images_cat_id: "498"
_4images_user_id: "420"
_4images_image_date: "2006-03-16T18:01:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5898 -->
