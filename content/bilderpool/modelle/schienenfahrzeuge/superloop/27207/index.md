---
layout: "image"
title: "Bahn ohne Ende"
date: "2010-05-09T20:30:29"
picture: "superloop1.jpg"
weight: "1"
konstrukteure: 
- "JMN"
fotografen:
- "JMN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/27207
- /detailsc892-2.html
imported:
- "2019"
_4images_image_id: "27207"
_4images_cat_id: "1953"
_4images_user_id: "162"
_4images_image_date: "2010-05-09T20:30:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27207 -->
Der Zug fährt durch einen Kreis und danach durch die vertikale Ständigen arch. Denn der Zug ist schwer, lockern den Bogen unter dem Zug und wird den Arch 180 Grad drehen, so dass eine weitere Runde gemacht worden kann. Dann fährt der Zug in die entgegengesetzte Richtung und macht die gleiche Strecke nochmahls.