---
layout: "image"
title: "Lokomotive 003"
date: "2004-12-05T10:22:02"
picture: "Lokomotive_003.JPG"
weight: "3"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3387
- /details5ebe.html
imported:
- "2019"
_4images_image_id: "3387"
_4images_cat_id: "294"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:22:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3387 -->
