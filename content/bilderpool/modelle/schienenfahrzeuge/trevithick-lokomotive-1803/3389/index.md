---
layout: "image"
title: "Lokomotive 005"
date: "2004-12-05T10:22:02"
picture: "Lokomotive_005.JPG"
weight: "5"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3389
- /details0bde.html
imported:
- "2019"
_4images_image_id: "3389"
_4images_cat_id: "294"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:22:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3389 -->
