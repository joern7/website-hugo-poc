---
layout: "image"
title: "Antrieb"
date: "2008-11-16T16:09:15"
picture: "highspeedmonorail5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16284
- /details497f.html
imported:
- "2019"
_4images_image_id: "16284"
_4images_cat_id: "1468"
_4images_user_id: "853"
_4images_image_date: "2008-11-16T16:09:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16284 -->
Die Gummireifen sind direkt an die Powermotoren gekoppelt.