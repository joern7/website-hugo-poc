---
layout: "image"
title: "Ladestation"
date: "2008-11-16T16:09:14"
picture: "highspeedmonorail2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16281
- /detailsa9eb.html
imported:
- "2019"
_4images_image_id: "16281"
_4images_cat_id: "1468"
_4images_user_id: "853"
_4images_image_date: "2008-11-16T16:09:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16281 -->
Einer der Endpunkte der Strecke wird zum Akkuladen genutzt. Hier ein Bild des Andockmechanismus: Die Kontakte des Ladegerätes sind mit zwei alufolienbewehrten Platten verbunden. 

Die gefederte Platte unten betätigt den Endtaster am Fahrzeug.