---
layout: "image"
title: "Ladekontakte"
date: "2008-11-16T16:09:15"
picture: "highspeedmonorail4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16283
- /detailsa40f-2.html
imported:
- "2019"
_4images_image_id: "16283"
_4images_cat_id: "1468"
_4images_user_id: "853"
_4images_image_date: "2008-11-16T16:09:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16283 -->
Die Ladekontakte werden über gefederte Achsen abgegriffen. Das klappt jetzt schon seit etlichen Stunden problemlos.