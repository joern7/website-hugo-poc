---
layout: "image"
title: "Solarzug"
date: "2008-04-04T17:10:20"
picture: "solarzug1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14166
- /detailsbd1d.html
imported:
- "2019"
_4images_image_id: "14166"
_4images_cat_id: "1310"
_4images_user_id: "747"
_4images_image_date: "2008-04-04T17:10:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14166 -->
Das ist ein Gesamtbild meines Solarzugs.