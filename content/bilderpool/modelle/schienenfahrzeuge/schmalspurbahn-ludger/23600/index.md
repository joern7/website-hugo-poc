---
layout: "image"
title: "sehr große Dampflok"
date: "2009-04-05T15:40:20"
picture: "DSCN2674.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23600
- /details9e1e-3.html
imported:
- "2019"
_4images_image_id: "23600"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-04-05T15:40:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23600 -->
