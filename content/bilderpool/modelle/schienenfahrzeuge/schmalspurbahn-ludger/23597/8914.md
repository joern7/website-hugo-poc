---
layout: "comment"
hidden: true
title: "8914"
date: "2009-04-06T08:18:41"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Reiner,

ich habe den Empfänger zuerst nur einmal eingebaut.
Eine Probe habe ich noch nicht gemacht.
Vielleicht muss er auch noch weiter nach hinten geschoben werden.
Ist aber das gleiche Prinzip wie damals bei der ersten Eisenbahn.
Ja, der Kessel ist nach hinten offen.

Gruß ludger