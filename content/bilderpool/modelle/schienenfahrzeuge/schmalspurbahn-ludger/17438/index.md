---
layout: "image"
title: "kleine Dampflok (ansicht von unten)"
date: "2009-02-19T11:53:06"
picture: "DSCN2611.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17438
- /details97cd-3.html
imported:
- "2019"
_4images_image_id: "17438"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17438 -->
Die Lok fährt auf den Statik Trägern.
Sie wird lediglich von den roten und schwarzen Bauteilen auf der Bahn gehalten.