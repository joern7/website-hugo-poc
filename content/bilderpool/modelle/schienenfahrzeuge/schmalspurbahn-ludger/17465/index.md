---
layout: "image"
title: "Prototyp einer 30mm Weiche"
date: "2009-02-20T09:47:04"
picture: "DSCN2624.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17465
- /details19f6-3.html
imported:
- "2019"
_4images_image_id: "17465"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-20T09:47:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17465 -->
