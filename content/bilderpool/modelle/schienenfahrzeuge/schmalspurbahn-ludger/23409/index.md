---
layout: "image"
title: "große Dampflok"
date: "2009-03-07T14:35:51"
picture: "DSCN2629.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23409
- /detailscabb-2.html
imported:
- "2019"
_4images_image_id: "23409"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23409 -->
