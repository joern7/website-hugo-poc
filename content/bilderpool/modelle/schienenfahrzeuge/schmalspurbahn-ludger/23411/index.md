---
layout: "image"
title: "Die kleine Dampflok"
date: "2009-03-07T14:35:51"
picture: "DSCN2633.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23411
- /details5951-3.html
imported:
- "2019"
_4images_image_id: "23411"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23411 -->
