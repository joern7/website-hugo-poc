---
layout: "image"
title: "Parallel-Gleis-Kreis"
date: "2009-03-07T14:35:51"
picture: "DSCN2625.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23405
- /details5958-3.html
imported:
- "2019"
_4images_image_id: "23405"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23405 -->
Hier habe ich mal einen Kreis mit einer parallelen Gleisführung aufgebaut.
Es hat sehr lange gedauert bis alles so richtig passte.