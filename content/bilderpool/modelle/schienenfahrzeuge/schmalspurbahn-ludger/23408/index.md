---
layout: "image"
title: "Lok mit Waggon"
date: "2009-03-07T14:35:51"
picture: "DSCN2628.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23408
- /detailsf4d7-2.html
imported:
- "2019"
_4images_image_id: "23408"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23408 -->
Die große Dampflok