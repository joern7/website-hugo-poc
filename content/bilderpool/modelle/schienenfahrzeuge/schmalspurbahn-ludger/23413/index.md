---
layout: "image"
title: "Lorenwaggon"
date: "2009-03-07T14:35:51"
picture: "DSCN2631.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23413
- /details0c2e.html
imported:
- "2019"
_4images_image_id: "23413"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23413 -->
Eine etwas überarbeitete Version