---
layout: "image"
title: "große Dampflok"
date: "2009-03-07T14:35:51"
picture: "DSCN2637.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23410
- /detailsc6d5.html
imported:
- "2019"
_4images_image_id: "23410"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23410 -->
so sieht sie von unten aus