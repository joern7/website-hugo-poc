---
layout: "image"
title: "große Dampflok"
date: "2009-02-19T11:53:06"
picture: "DSCN2612.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17439
- /detailsdaa0.html
imported:
- "2019"
_4images_image_id: "17439"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17439 -->
erster Versuch mit einer größeren Dampflok.
Das Problem mit dem Entgleisen tritt nur bei der hier gezeigten Kurvenfahrt auf.
Die Lok kippt dann (manchmal) zum Betrachter hin um.