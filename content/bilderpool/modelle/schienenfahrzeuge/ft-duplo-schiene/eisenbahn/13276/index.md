---
layout: "image"
title: "Lok mit Power Motor"
date: "2008-01-05T06:40:38"
picture: "eisenbahn1_2.jpg"
weight: "6"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13276
- /details8a50.html
imported:
- "2019"
_4images_image_id: "13276"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-05T06:40:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13276 -->
Hier die erste Testlok mit Power Motor Antrieb.  Zieht problemlos drei Waggons (inkl. Tiefladewagen). Geschwindigkeit optimal.