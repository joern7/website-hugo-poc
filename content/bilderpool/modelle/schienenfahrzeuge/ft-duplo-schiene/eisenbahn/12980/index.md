---
layout: "image"
title: "Eisenbahn 4"
date: "2007-12-02T15:46:17"
picture: "eisenbahn4.jpg"
weight: "4"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12980
- /detailsf843.html
imported:
- "2019"
_4images_image_id: "12980"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12980 -->
Ladewagen und Muldenkippwagen