---
layout: "image"
title: "Güterlok vor Lichtsignal"
date: "2007-01-28T08:55:13"
picture: "PICT1232.jpg"
weight: "7"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8705
- /details35ad.html
imported:
- "2019"
_4images_image_id: "8705"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8705 -->
Hier kann ich den Zug anhalten, vor- oder rückwärts fahren lassen oder ihm freie fahrt geben.