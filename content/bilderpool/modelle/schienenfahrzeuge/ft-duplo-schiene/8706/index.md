---
layout: "image"
title: "Blick auf die Teststrecke"
date: "2007-01-28T08:55:14"
picture: "PICT1235.jpg"
weight: "8"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8706
- /detailscb5b.html
imported:
- "2019"
_4images_image_id: "8706"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8706 -->
