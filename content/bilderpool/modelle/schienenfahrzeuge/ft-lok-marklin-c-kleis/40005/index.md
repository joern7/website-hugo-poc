---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis13.jpg"
weight: "13"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/40005
- /details9fea.html
imported:
- "2019"
_4images_image_id: "40005"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40005 -->
Nun wird ein 2 poliges-Kabel an eine C-Kleis schiene angelötet. Ein Draht für die Mittelschiene und der andere die Schiene. Dafür gibt es unter der Schiene 2 Metallstifte