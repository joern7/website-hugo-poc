---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis09.jpg"
weight: "9"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/40001
- /details8f79-2.html
imported:
- "2019"
_4images_image_id: "40001"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40001 -->
Nun wird der Zug wieder zusammen gebaut.