---
layout: "image"
title: "Signal (grün)"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich05.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30950
- /details1471-2.html
imported:
- "2019"
_4images_image_id: "30950"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30950 -->
Das Signal wird über einen Taster (gedrückt: Grün, nicht gedrückt: Rot) geschaltet.