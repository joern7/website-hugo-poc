---
layout: "image"
title: "Beleuchtung"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich04.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30949
- /detailsb7a7-2.html
imported:
- "2019"
_4images_image_id: "30949"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30949 -->
Für allezeit Helligkeit (Hey, das reimt sich ja!) sorgen zwei alte Klassik-Leuchten.