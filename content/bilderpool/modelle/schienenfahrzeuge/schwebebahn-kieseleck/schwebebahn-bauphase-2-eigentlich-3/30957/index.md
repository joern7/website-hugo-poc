---
layout: "image"
title: "Wagen im 'S'"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich12.jpg"
weight: "12"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30957
- /details6f7a-3.html
imported:
- "2019"
_4images_image_id: "30957"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30957 -->
Der Triebwagen in der S-Kurve.