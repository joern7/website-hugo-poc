---
layout: "image"
title: "Der Triebwagen"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich14.jpg"
weight: "14"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30959
- /details2e31-2.html
imported:
- "2019"
_4images_image_id: "30959"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30959 -->
Der Triebwagen ist völlig neu konstruiert worden. Er hängt an zwei Drehgestellen zu je zwei Achsen (die Drehgestelle mussten wegen den scharfen  Kurven (60 Grad) her). Die wichtigste Neuerung sind die Drehgestellaufhängungen (die beim altenTriebwagen machten Probleme).