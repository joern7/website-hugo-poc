---
layout: "image"
title: "Station 2"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich08.jpg"
weight: "8"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30953
- /details1b9d.html
imported:
- "2019"
_4images_image_id: "30953"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30953 -->
Sie ist ein Neubau von der eigentlichen Bauphase 2.