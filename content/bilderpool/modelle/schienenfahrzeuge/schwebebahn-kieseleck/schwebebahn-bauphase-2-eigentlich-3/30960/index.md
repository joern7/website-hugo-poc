---
layout: "image"
title: "Der Wagen..."
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich15.jpg"
weight: "15"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30960
- /detailsa4e0-2.html
imported:
- "2019"
_4images_image_id: "30960"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30960 -->
...dient zum Transport von Gütern und Personen.