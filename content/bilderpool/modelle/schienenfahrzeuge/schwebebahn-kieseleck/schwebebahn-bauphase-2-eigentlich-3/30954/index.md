---
layout: "image"
title: "Station 1"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich09.jpg"
weight: "9"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30954
- /detailsdfcf-2.html
imported:
- "2019"
_4images_image_id: "30954"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30954 -->
Hier wurde nur der "Bahnsteig" entfernt ansonsten ist sie noch ganz Bauphase 1