---
layout: "image"
title: "Die Sitze"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck10.jpg"
weight: "10"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30816
- /detailsba52.html
imported:
- "2019"
_4images_image_id: "30816"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30816 -->
Fischermans im Wagen.