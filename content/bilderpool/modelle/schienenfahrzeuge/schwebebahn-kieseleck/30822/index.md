---
layout: "image"
title: "Wagen in Station2"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck16.jpg"
weight: "16"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30822
- /details76ad-2.html
imported:
- "2019"
_4images_image_id: "30822"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30822 -->
Hier gibt es nichts zu sagen außer, dass der zweite Bahnsteig fehlt.