---
layout: "image"
title: "Der Wagen"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck07.jpg"
weight: "7"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30813
- /details8a6a.html
imported:
- "2019"
_4images_image_id: "30813"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30813 -->
Der Wagen "im Trockenen".