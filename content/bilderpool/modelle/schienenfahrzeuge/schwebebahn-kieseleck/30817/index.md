---
layout: "image"
title: "Der Wagen auf der Schiene"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck11.jpg"
weight: "11"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30817
- /detailsb410-4.html
imported:
- "2019"
_4images_image_id: "30817"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30817 -->
Auf Jungfernfahrt...