---
layout: "image"
title: "Achslager"
date: "2010-06-14T14:39:14"
picture: "Motordraisine_13.jpg"
weight: "13"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27515
- /detailsbfe7-3.html
imported:
- "2019"
_4images_image_id: "27515"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-14T14:39:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27515 -->
Obwohl jedes Lager mit 4 Federn abgefedet ist, stehen die Federn fast an. Sie sind für dieses Modell eigentlich schon zu schwach. Bei der Fahrt kommt das Fahrzeug auch sehr schwankend daher.