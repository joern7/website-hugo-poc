---
layout: "image"
title: "Radsatz"
date: "2010-06-10T14:27:35"
picture: "Motordraisine_07.jpg"
weight: "7"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27446
- /detailscf2c-4.html
imported:
- "2019"
_4images_image_id: "27446"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27446 -->
jedes Rad besteht aus 4 Drehscheiben 60, als Spurkranz dient eine 4 mm starke Kunststoffscheibe.