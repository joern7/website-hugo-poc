---
layout: "image"
title: "Federung"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_05.jpg"
weight: "5"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27444
- /detailsc892.html
imported:
- "2019"
_4images_image_id: "27444"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27444 -->
je Achslager sind 4 Federn eingebaut