---
layout: "image"
title: "Viehwaggon - mit geöffneter Laderampe"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40303
- /details58b4.html
imported:
- "2019"
_4images_image_id: "40303"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40303 -->
Entriegelte und geöffnete Laderampe (beidseitig)