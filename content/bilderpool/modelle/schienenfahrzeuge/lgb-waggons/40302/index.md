---
layout: "image"
title: "Viehwaggon - Gesamtansicht"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40302
- /detailsc233.html
imported:
- "2019"
_4images_image_id: "40302"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40302 -->
Gesamtansicht des Viehwaggons (Achtung: nicht geeignet für Geflügel - kann entweichen...)