---
layout: "image"
title: "Langholzwagen - Unterseite"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40299
- /details4ff6.html
imported:
- "2019"
_4images_image_id: "40299"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40299 -->
Unterseite des Langholzwagens mit Fahrgestellen und Kupplung