---
layout: "image"
title: "angetriebenes Fahrgestell"
date: "2009-03-29T21:05:07"
picture: "lgb1.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/23557
- /detailsf408.html
imported:
- "2019"
_4images_image_id: "23557"
_4images_cat_id: "1609"
_4images_user_id: "373"
_4images_image_date: "2009-03-29T21:05:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23557 -->
Passend für LGB-Schienen. Mit einem 8:1er PowerMotor direkt 1:1 auf die Zahnräder oben hat das ganze eigentlich eine ordentliche Geschwindigkeit.