---
layout: "image"
title: "Antrieb"
date: "2009-11-02T21:41:41"
picture: "bumpf9.jpg"
weight: "9"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/25633
- /detailsdd20.html
imported:
- "2019"
_4images_image_id: "25633"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25633 -->
Power-Motor 1:20 und Differenzialgetriebe.
Fazit. Kraftübertragung muss dringend verbessert werden. Werde es mal mit zwei Motoren probieren.