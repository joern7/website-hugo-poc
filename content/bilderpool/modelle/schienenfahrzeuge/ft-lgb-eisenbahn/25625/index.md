---
layout: "image"
title: "Auf geht's, zum ersten Test im Freien"
date: "2009-11-02T21:41:40"
picture: "bumpf1.jpg"
weight: "1"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/25625
- /details3317.html
imported:
- "2019"
_4images_image_id: "25625"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25625 -->
