---
layout: "image"
title: "Gewichtskontrolle"
date: "2009-11-02T21:41:41"
picture: "bumpf8.jpg"
weight: "8"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/25632
- /detailseaf8.html
imported:
- "2019"
_4images_image_id: "25632"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25632 -->
1,5 Kg bringt die Lok auf die Waage.