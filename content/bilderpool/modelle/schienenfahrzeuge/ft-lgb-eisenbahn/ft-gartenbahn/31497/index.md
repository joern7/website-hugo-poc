---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf2.jpg"
weight: "2"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31497
- /details366e.html
imported:
- "2019"
_4images_image_id: "31497"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31497 -->
Steuerung (Leider sind die ft-Transformer zu schwach, da jede Lok mit 2 Power-Motoren angetrieben wird)