---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf4.jpg"
weight: "4"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31499
- /details4568.html
imported:
- "2019"
_4images_image_id: "31499"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31499 -->
Rangierlok
Antrieb: 2 Power-Motoren, alle 4 Achsen angetrieben