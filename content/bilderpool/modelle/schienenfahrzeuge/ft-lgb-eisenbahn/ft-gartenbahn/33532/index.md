---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-11-20T18:07:00"
picture: "waltermariograf2.jpg"
weight: "13"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33532
- /detailsd5a9-2.html
imported:
- "2019"
_4images_image_id: "33532"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-20T18:07:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33532 -->
Antrieb mit Power-Motor 1:50