---
layout: "image"
title: "Antrieb"
date: "2011-11-30T22:24:35"
picture: "waltermariograf2_2.jpg"
weight: "17"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33590
- /details4788.html
imported:
- "2019"
_4images_image_id: "33590"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-30T22:24:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33590 -->
Power-Motor 1:50. Kraftübertragung mit zwei Kegelzahnräder Z12