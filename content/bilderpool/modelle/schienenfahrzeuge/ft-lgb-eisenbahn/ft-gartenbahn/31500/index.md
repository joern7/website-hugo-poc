---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf5.jpg"
weight: "5"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31500
- /detailse363-3.html
imported:
- "2019"
_4images_image_id: "31500"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31500 -->
Rangierlok
Das Problem mit den ablösenden Gummiringen hat sich gelöst