---
layout: "image"
title: "Kessel"
date: "2011-11-30T22:24:35"
picture: "waltermariograf6.jpg"
weight: "21"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33594
- /details997f.html
imported:
- "2019"
_4images_image_id: "33594"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-30T22:24:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33594 -->
Für Stefan
Innenleben des Kessels