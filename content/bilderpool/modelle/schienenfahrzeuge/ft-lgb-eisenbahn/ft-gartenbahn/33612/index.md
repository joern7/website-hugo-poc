---
layout: "image"
title: "Einkuppeln 2"
date: "2011-12-04T14:45:52"
picture: "waltermariograf4_3.jpg"
weight: "25"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33612
- /details8f92-2.html
imported:
- "2019"
_4images_image_id: "33612"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-12-04T14:45:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33612 -->
Der Wagen ist eingehackt. Entkupplungsgleis umschalten und die Fahrt kann weitergehen.