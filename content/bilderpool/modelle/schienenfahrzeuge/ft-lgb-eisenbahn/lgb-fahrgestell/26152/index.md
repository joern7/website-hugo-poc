---
layout: "image"
title: "seitlich von oben"
date: "2010-01-25T22:35:42"
picture: "seitlichoben.jpg"
weight: "1"
konstrukteure: 
- "kilo70"
fotografen:
- "kilo70"
keywords: ["lgb", "schienen", "playmobil"]
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/26152
- /detailse953-2.html
imported:
- "2019"
_4images_image_id: "26152"
_4images_cat_id: "1854"
_4images_user_id: "1071"
_4images_image_date: "2010-01-25T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26152 -->
hier mal mein Versuch ein Fahrgestell für Playmobil/LGB Schienen zu bauen. Ich hatte lange nichts mehr mit ft gebaut und es ist daher etwas provisorisch.
Ziel war einfach:
-alle Räder angetrieben
-zwei Fahrgestelle
-Antrieb über mittige Welle
-Stromabnehmer, keine Batterien
-"irgendwie" den Motor unterbringen