---
layout: "image"
title: "verbunden"
date: "2010-01-25T22:35:42"
picture: "vonuntenkomplett.jpg"
weight: "4"
konstrukteure: 
- "kilo70"
fotografen:
- "kilo70"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/26155
- /details52b7-2.html
imported:
- "2019"
_4images_image_id: "26155"
_4images_cat_id: "1854"
_4images_user_id: "1071"
_4images_image_date: "2010-01-25T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26155 -->
und hier beide Fahrgestelle verbunden über 2 Kardangelenke. Ist natürlich nicht so das wahre, der Drehpunkte sollte lieber jeweils zwischen zwei Achsen sitzen.
Der Schalter ändert die Fahrtrichtung indem er gegen "Schalthuppel" neben den Schienen stösst.

Das Ding fährt trotzdem ganz gut, allerdings sind die Schienen ziemlich oxidiert was zu Aussetzern führt...