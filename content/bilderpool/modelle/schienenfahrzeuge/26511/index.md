---
layout: "image"
title: "Kleine Eisenbahn"
date: "2010-02-23T21:27:16"
picture: "IMG_1269_2.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/26511
- /details311f.html
imported:
- "2019"
_4images_image_id: "26511"
_4images_cat_id: "481"
_4images_user_id: "968"
_4images_image_date: "2010-02-23T21:27:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26511 -->
Hier mal eine klein Bastelei aus der Vorweihnachtszeit.