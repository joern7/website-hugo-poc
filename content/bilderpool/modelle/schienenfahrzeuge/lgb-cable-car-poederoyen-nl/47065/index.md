---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47065
- /details1e09-3.html
imported:
- "2019"
_4images_image_id: "47065"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47065 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=