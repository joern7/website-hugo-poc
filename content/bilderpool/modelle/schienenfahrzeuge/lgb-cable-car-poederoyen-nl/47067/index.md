---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:09"
picture: "lgbcablecar12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47067
- /detailsfd52.html
imported:
- "2019"
_4images_image_id: "47067"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47067 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=