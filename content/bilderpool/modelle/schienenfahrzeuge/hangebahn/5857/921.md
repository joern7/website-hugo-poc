---
layout: "comment"
hidden: true
title: "921"
date: "2006-03-16T17:32:08"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Na zum Spaß nicht, aber von dreien ist immer einer etwas schwächer als die anderen und einer kommt später als die anderen. Das heißt: du wirst immer Spannung im Getriebe haben. Wenn's gut läuft, erhöht das nur den Verschleiß der Zahnräder. Wenn's weniger gut läuft, platzt eins der kleinen Rädchen (entweder links außen oder ganz innen drin). Kannst ja mal die Motoren/Getriebe voneinander trennen und dünne Papierstreifen dazwischen einklemmen. Wenn davon einer reißt, weißt du, was die Zähnchen leisten müssen.

Davon abgesehen: für diese Bahn reicht doch ein Motor voll und ganz! Die fährt doch horizontal, nicht bergauf.

Gruß,
Harald