---
layout: "image"
title: "Das Fahrzeug"
date: "2010-02-28T22:06:56"
picture: "achterbahnnullgleisig3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26567
- /details3b50.html
imported:
- "2019"
_4images_image_id: "26567"
_4images_cat_id: "1894"
_4images_user_id: "104"
_4images_image_date: "2010-02-28T22:06:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26567 -->
Zwei Minimotoren - einer liegend, einer senkrecht stehend - treiben je eines der Antriebsräder. Ein drittes Rad mit Bodenkontakt wird einfach hinterher gezogen und ist drehbar aufgehängt.

Wenn das Fahrzeug an der Innenseite eines Bogens anstößt, wird der kurveninnere (also gegenüber liegende) Motor durch den dann gedrückten Taster abgeschaltet. Es läuft dann nur noch das kurvenäußere Rad und dreht das Fahrzeug um das dann stillstehende kurveninnere so lange, bis der Taster wieder freigegeben wird. Dieser Vorgang wiederholt sich bei einem Bogen mehrmals, bis das Fahrzeug wieder geradeaus fahren kann.