---
layout: "image"
title: "Gesamtansicht"
date: "2010-02-28T22:06:56"
picture: "achterbahnnullgleisig1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26565
- /detailsc1c4-2.html
imported:
- "2019"
_4images_image_id: "26565"
_4images_cat_id: "1894"
_4images_user_id: "104"
_4images_image_date: "2010-02-28T22:06:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26565 -->
Das kleine Fahrzeug fährt eine "8". Es lenkt selbständig an den Kurven entlang.