---
layout: "image"
title: "Fahrwerk und Antrieb"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36906
- /details962d.html
imported:
- "2019"
_4images_image_id: "36906"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36906 -->
Die Fahrwerke aller drei Autos sind genau gleich, lediglich die Aufbauten sind unterschiedlich. Je zwei waagerecht liegende Vorstuferäder halten das Fahrzeug in der Spur.

Nachdem ich erst mit zwei Fäden (einer vorne, einer hinten) experimentierte, stellte sich diese Lösung als die beste heraus: Ein Faden geht durch die Achse des vorderen Spur-Rades (damit ist es zuverlässig immer in der Spurmitte) und wird weiter oben zwischen Nut und Zapfen eingeklemmt (zwei bis drei Mal um den Zapfen gewickelt). Die Kette zieht einfach daran, und schon fährt das Fahrzeug.