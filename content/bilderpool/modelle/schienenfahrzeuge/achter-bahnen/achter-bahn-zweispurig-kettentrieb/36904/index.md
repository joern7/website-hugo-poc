---
layout: "image"
title: "Antrieb (2)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36904
- /details4e0a.html
imported:
- "2019"
_4images_image_id: "36904"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36904 -->
Der Antrieb von unten gesehen.