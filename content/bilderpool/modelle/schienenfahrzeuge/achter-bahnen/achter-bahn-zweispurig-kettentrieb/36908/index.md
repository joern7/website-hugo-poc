---
layout: "image"
title: "Rennwagen (1)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36908
- /detailsf099.html
imported:
- "2019"
_4images_image_id: "36908"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36908 -->
Das zweite Fahrzeug ist ein kleines Rennauto. Bis auf den Aufbau sitzt dasselbe Fahrwerk darunter wie bei den anderen Fahrzeugen.