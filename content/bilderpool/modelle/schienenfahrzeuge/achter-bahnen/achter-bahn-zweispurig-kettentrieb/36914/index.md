---
layout: "image"
title: "Kurve (1)"
date: "2013-05-12T21:59:19"
picture: "achterbahnzweispurigmitkettentrieb14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36914
- /detailsa149.html
imported:
- "2019"
_4images_image_id: "36914"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36914 -->
Alle Kurven sind gleich aufgebaut und bis auf die Motor-Mimik auch mit dem Antrieb identisch.