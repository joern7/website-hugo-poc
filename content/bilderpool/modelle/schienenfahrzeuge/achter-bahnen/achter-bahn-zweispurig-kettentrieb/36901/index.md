---
layout: "image"
title: "Gesamtansicht"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36901
- /details9dff.html
imported:
- "2019"
_4images_image_id: "36901"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36901 -->
Auf dieser Bahn fahren die drei Autos eine "8". Der Antrieb erfolgt zentral durch einen einzigen stationär angebrachten Power-Motor.