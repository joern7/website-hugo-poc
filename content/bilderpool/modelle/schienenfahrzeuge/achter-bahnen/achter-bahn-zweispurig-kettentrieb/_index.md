---
layout: "overview"
title: "Achter-Bahn zweispurig mit Kettentrieb"
date: 2020-02-22T08:35:28+01:00
legacy_id:
- /php/categories/2742
- /categories1e07.html
- /categories3a82.html
- /categories2416.html
- /categoriesd403.html
- /categories2f2e-3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2742 --> 
Eine Bahn, auf der mehrere Fahrzeuge gleichzeitig vom selben Motor zentral angetrieben eine "8" fahren.