---
layout: "image"
title: "Der Wagen (Stellgliedseite)"
date: "2010-02-23T21:27:17"
picture: "achterbahneinspurig4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26517
- /details1c0d.html
imported:
- "2019"
_4images_image_id: "26517"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26517 -->
Der graue MiniMot dient als Gegengewicht zum S-Motor auf der Antriebsseite (er ist übrigens deutlich schwerer als ein S-Motor). Vorne sieht man die bewegliche "Hand", die dafür sorgt, dass der Kreuzungsbalken immer richtig ausgerichtet wird, wenn die Bahn ankommt.