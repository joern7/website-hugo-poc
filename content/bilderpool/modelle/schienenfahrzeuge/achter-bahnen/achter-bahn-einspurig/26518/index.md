---
layout: "image"
title: "Stellen der Kreuzung"
date: "2010-02-23T21:27:17"
picture: "achterbahneinspurig5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26518
- /detailsae90-2.html
imported:
- "2019"
_4images_image_id: "26518"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26518 -->
Egal wie der Kreuzungsbalken steht, wird er doch immer korrekt und hinreichend genau ausgerichtet.