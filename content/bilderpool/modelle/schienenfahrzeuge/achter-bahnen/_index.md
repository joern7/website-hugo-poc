---
layout: "overview"
title: "Achter-Bahnen"
date: 2020-02-22T08:35:24+01:00
legacy_id:
- /php/categories/1884
- /categories32b3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1884 --> 
Bahnen, die eine "8" fahren und so ohne Akku und Stromschienen von außen versorgt werden können, ohne dass sich Kabel verdrillen.