---
layout: "image"
title: "Schikanen"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40854
- /details5ea4.html
imported:
- "2019"
_4images_image_id: "40854"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40854 -->
Das Fahrzeug kann solche Höhenunterschiede leicht bewältigen, solange nur das Lenk-Rad wieder hinreichend schnell zwischen die Schienen gelangt und das Fahrzeug also in der Spur halten kann. Das geht auch (siehe das Video) mit einem einzigen Paar Bogenstücken 60° oben anstatt zwei 30°-Paaren.

Über Bogenstücke und Zwischenbausteine (zur Einhaltung des 30-mm-Abstands zwischen den Schienen) ist auch die Kurven-Schikane rechts am Bildrand realisiert.