---
layout: "image"
title: "Stromzufuhr (1)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40855
- /detailsf6f0.html
imported:
- "2019"
_4images_image_id: "40855"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40855 -->
Der Strom kommt hier im Turm an...