---
layout: "image"
title: "Weichen (2)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40851
- /detailsea7b.html
imported:
- "2019"
_4images_image_id: "40851"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40851 -->
Nach der Schleife kommt es von oben (im Bild) an und fährt aus der Schleife und Weiche heraus. Dabei wird die Zunge aber auf die andere Fahrbahnseite gedrückt und bleibt dort. Beim nächsten Anfahren wird es also die Schleife in umgekehrter Richtung durchfahren. Damit verdrillt sich das Zuleitungskabel nicht.