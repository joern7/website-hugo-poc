---
layout: "overview"
title: "Renn-Achter-Bahn"
date: 2020-02-22T08:35:30+01:00
legacy_id:
- /php/categories/3069
- /categories60d5.html
- /categories8a53.html
- /categoriesfb0d.html
- /categories6b67.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3069 --> 
Eine Zwischenstation auf dem langen Weg zu sowas wie einer Carrera-Bahn: Eine Achter-Bahn, auf der ein Wagen tatsächlich mal "rennt", also einigermaßen flott unterwegs ist.