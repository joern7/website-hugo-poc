---
layout: "image"
title: "Fahrzeugunterseite (1)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40847
- /details74c2.html
imported:
- "2019"
_4images_image_id: "40847"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40847 -->
Die Antriebsachse stammt von den älteren ft-Mini-Motoren. Die starre Vorderachse lenkt mittels einer 130593 Rastaufnahmeachse 22,5, die auf dem BS7,5 steckt.