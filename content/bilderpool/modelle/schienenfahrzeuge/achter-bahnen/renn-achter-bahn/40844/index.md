---
layout: "image"
title: "Gesamtansicht"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40844
- /details0e73.html
imported:
- "2019"
_4images_image_id: "40844"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40844 -->
Auf dieser Bahn "rast" ein kleiner Rennwagen herum. Seine Stromversorgung bekommt per Leitung über den Turm links. Der hat einen schwenkbaren Arm und kann somit dem Rennwagen auf der gesamten Bahn genügend folgen. Das Kabel verdrillt wieder nicht, weil auch dies hier eine "Achter-Bahn" ist, in der das Fahrzeug eine "8" abfährt. Nur ist hier der "Knoten" in der Mitte in die Länge gezogen.

Ein Video incl. Zeitlupenaufnahmen gibt's auch: https://www.youtube.com/watch?v=RgeysZOrdcw