---
layout: "image"
title: "Der Wagen von unten"
date: "2010-02-21T13:26:37"
picture: "achterbahnzweispurig04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26493
- /detailsf595-2.html
imported:
- "2019"
_4images_image_id: "26493"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26493 -->
Die Spurkränze machen genau das: Sie halten den Wagen zwangsweise in der Spur. Da kommt es auf Härte an, deshalb ist das Strebenpaket in der Mitte nochmal befestigt. Alles was sich irgendwie biegt verschlingt ja durch Kraft * Weg Energie, die dem Vortrieb abgeht.