---
layout: "image"
title: "Gesamtansicht (2)"
date: "2010-02-21T13:26:37"
picture: "achterbahnzweispurig02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26491
- /detailsb071.html
imported:
- "2019"
_4images_image_id: "26491"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26491 -->
Bei Tageslicht, aber auch leider bei Gegenlicht. Dies ist eine größere Fassung einer ähnlichen Achter-Bahn, die ich vor Jahrzehnten mal baute. Die war nur einspurig im Gegensatz zur Zweispurigen hier. Die Einspurige will ich aber auch noch bauen, und noch eine ganz anders funktionierende soll auch noch kommen.