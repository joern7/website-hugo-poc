---
layout: "image"
title: "Gesamtansicht (1)"
date: "2010-02-21T13:26:37"
picture: "achterbahnzweispurig01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26490
- /details2ebf-2.html
imported:
- "2019"
_4images_image_id: "26490"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26490 -->
Ohne Tageslicht, dafür sieht man wenigstens was: Dies ist eine Achter-Bahn im Wortsinne. Der Wagen darauf fährt eine "8".

Das nette daran ist, dass man keinen Akku oder Stromschienen benötigt, sondern einfach von außen (oben) ein Kabel runter hängen lassen kann. Durch die Bahnführung verdrillt sich das Kabel nämlich nicht, weil es immer abwechselnd mal links herum, dann wieder rechts herum gedreht wird. So kann die Bahn endlos laufen.