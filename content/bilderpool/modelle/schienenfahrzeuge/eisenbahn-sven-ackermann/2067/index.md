---
layout: "image"
title: "Schwerkleinwagen - Kupplungsdetail"
date: "2004-01-15T21:31:52"
picture: "Skl1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- /php/details/2067
- /details7319.html
imported:
- "2019"
_4images_image_id: "2067"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2067 -->
Hier sieht man im Detail die Kupplung zwischen sem Skl und einem LGB-Wagen. Die Kupplung mit Puffer des ft-Fahrzeugs besteht natürlich aus ft und ist mit den LGB-Kupplungen voll kompatibel.