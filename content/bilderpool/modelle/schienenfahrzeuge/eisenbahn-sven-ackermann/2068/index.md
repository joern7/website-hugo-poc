---
layout: "image"
title: "Schwerkleinwagen - Ansicht"
date: "2004-01-15T21:31:52"
picture: "Skl2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- /php/details/2068
- /detailsa810.html
imported:
- "2019"
_4images_image_id: "2068"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2068 -->
Das komplette Fahrzeug mit angehängtem Güterwagen.