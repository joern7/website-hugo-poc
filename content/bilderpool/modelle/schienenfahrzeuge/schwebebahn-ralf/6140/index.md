---
layout: "image"
title: "Der Teil der die Verbindung nach unten bringt"
date: "2006-04-26T16:02:09"
picture: "Schwebebahn03.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Schwebebahn"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/6140
- /detailsb365.html
imported:
- "2019"
_4images_image_id: "6140"
_4images_cat_id: "529"
_4images_user_id: "381"
_4images_image_date: "2006-04-26T16:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6140 -->
Durch zwei Federgelenksteine wird die Bahn auf der Schiene gehalten. In den Kurven (rechts oder links) bewegen die Federgelenksteine sich dann nach innen oder außen.