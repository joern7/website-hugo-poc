---
layout: "image"
title: "Der Antriebswagen"
date: "2006-04-26T16:02:09"
picture: "Schwebebahn02.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Schwebebahn"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/6139
- /detailsbba3.html
imported:
- "2019"
_4images_image_id: "6139"
_4images_cat_id: "529"
_4images_user_id: "381"
_4images_image_date: "2006-04-26T16:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6139 -->
Ein Minimot treibt die Bahn über einen kleinen Zwillingsreifen an.

Die Stromabnehmer sind schon dran, das Aluklebeband oberhalb und unterhalb der Schiene fehlt allerdings noch.