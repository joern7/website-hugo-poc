---
layout: "image"
title: "Eisenbahn"
date: "2009-12-26T19:06:07"
picture: "minibahn1.jpg"
weight: "1"
konstrukteure: 
- "Tobias T."
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25985
- /details5a50-2.html
imported:
- "2019"
_4images_image_id: "25985"
_4images_cat_id: "1832"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25985 -->
Am Beispiel von Ludger hat unser Tobias sich auch mal an eine Kleineisenbahn gesetzt.