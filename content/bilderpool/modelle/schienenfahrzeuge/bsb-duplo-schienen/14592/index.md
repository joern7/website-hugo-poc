---
layout: "image"
title: "Dampflok"
date: "2008-05-30T22:39:35"
picture: "bsb1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/14592
- /detailsccf5.html
imported:
- "2019"
_4images_image_id: "14592"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14592 -->
