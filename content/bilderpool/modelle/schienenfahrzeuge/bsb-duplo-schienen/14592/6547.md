---
layout: "comment"
hidden: true
title: "6547"
date: "2008-06-01T16:02:49"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

Deine gebaute Bahn sieht wirklich ausgesprochen gut aus!

Gruß

Lurchi

P.S.: Welche Haftreifen hast Du denn hierzu verwendet?
Sind das original ft-Teile?
Wenn nicht, woher sind diese Haftreifen?