---
layout: "image"
title: "Doppel Flip-Flop"
date: "2009-01-02T21:08:45"
picture: "bumpf6.jpg"
weight: "6"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/16846
- /details17ba.html
imported:
- "2019"
_4images_image_id: "16846"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16846 -->
Mit dem einem Flip-Flop kann ich die Lok in Fahrt setzen oder anhalten, mit dem anderem Flip-Flop wird über ein Relais die Fahrtrichtung gesteuert.