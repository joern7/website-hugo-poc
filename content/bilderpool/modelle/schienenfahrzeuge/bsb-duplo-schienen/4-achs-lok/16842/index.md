---
layout: "image"
title: "Ansicht 4-Achs-Lok"
date: "2009-01-02T21:08:43"
picture: "bumpf2.jpg"
weight: "2"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/16842
- /details2921.html
imported:
- "2019"
_4images_image_id: "16842"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16842 -->
Nach unzähligen Fehlversuchen mit verschiedenen Motoren und Achslagerungen fährt die Lok super und bringt auch genügend Kraft auf die Schienen.