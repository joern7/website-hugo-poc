---
layout: "image"
title: "Ansicht"
date: "2008-05-30T22:39:35"
picture: "bsb4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/14595
- /details3a5a.html
imported:
- "2019"
_4images_image_id: "14595"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14595 -->
