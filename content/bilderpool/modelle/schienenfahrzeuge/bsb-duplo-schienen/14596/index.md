---
layout: "image"
title: "Pufferdetail"
date: "2008-05-30T22:39:35"
picture: "bsb5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/14596
- /details12d5.html
imported:
- "2019"
_4images_image_id: "14596"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14596 -->
