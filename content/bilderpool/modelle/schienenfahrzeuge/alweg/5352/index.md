---
layout: "image"
title: "beam support"
date: "2005-11-23T17:29:50"
picture: "PICT2222.jpg"
weight: "21"
konstrukteure: 
- "Daniel Nowicki"
fotografen:
- "Daniel Nowicki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/5352
- /details6e41.html
imported:
- "2019"
_4images_image_id: "5352"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-11-23T17:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5352 -->
Auch an der "Fahrbahntechnik" hat sich etwas getan:

Ich habe dem Beam an der Unterseite zwei 4mm breite Bohrungen im Abstand von 3cm verpasst, damit die ft-Metallachsen reinpassen und somit eine Verbindung des Beams mit dem (komplett in ft gehaltenem) Support ermöglichen.