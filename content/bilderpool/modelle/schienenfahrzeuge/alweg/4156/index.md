---
layout: "image"
title: "Seitenansicht"
date: "2005-05-19T21:52:53"
picture: "PICT1255.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/4156
- /details67d0.html
imported:
- "2019"
_4images_image_id: "4156"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4156 -->
Wie Ihr sehen könnt, habe ich nochmals einige Änderungen am Wagenkasten vorgenommen. Auch erprobe ich z.Zt. verschiedene Türsysteme (eine ist schon eingebaut).

Ich denke ich werde mich für die pneumatisch betätigte Schiebetüren entscheiden (die Pneumatik ist noch nicht drin!).

Weitere "große" Neuerungen:
Ich hab der "Dame" Fenderskirts verpasst und der Wagen befindet sich auf einem hölzernen Beam (das ist mein Montagebeam!).