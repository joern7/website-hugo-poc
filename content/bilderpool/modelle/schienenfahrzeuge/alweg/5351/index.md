---
layout: "image"
title: "UPDATE: mehr Wagenlänge und leicht geänderte Bogies"
date: "2005-11-23T17:29:50"
picture: "PICT2218.jpg"
weight: "20"
konstrukteure: 
- "Daniel Nowicki"
fotografen:
- "Daniel Nowicki"
keywords: ["ALWEG", "monorail", "Seattle", "Turin", "Hitachi", "Disney", "Bombardier"]
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/5351
- /details2b2d.html
imported:
- "2019"
_4images_image_id: "5351"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-11-23T17:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5351 -->
So!
Hat wieder etwas länger gedauert, aber nun gibt's neue Fotos von "meiner" ft-ALWEG.

Die Fenderskirts habe ich entfernen müssen, da ich noch einige Änderungen an den Wagenkästen vorgenommen habe (sie sind jetzt länger!). Außerdem habe ich noch ein Mittelteil samt zweiten Mitteldrehgestell (die Motoren fehlen noch!) hinzugefügt.