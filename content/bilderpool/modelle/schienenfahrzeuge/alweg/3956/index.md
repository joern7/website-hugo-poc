---
layout: "image"
title: "Stadtbahnwagen - Rohbau - 1.Teil(Seitenansicht)"
date: "2005-04-03T16:53:17"
picture: "Stadtbahnwagen - Rohbau - 1.Teil(Seitenansicht).jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3956
- /details35bc.html
imported:
- "2019"
_4images_image_id: "3956"
_4images_cat_id: "340"
_4images_user_id: "5"
_4images_image_date: "2005-04-03T16:53:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3956 -->
