---
layout: "image"
title: "Zweiachslenk-Bogie"
date: "2005-05-19T21:52:53"
picture: "PICT1270.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/4163
- /detailsf04c.html
imported:
- "2019"
_4images_image_id: "4163"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4163 -->
... an dem großen Fahrgestell habe ich nicht viel geändert (im Vergleich zum ersten Prototypen). Die lustige Kinematik habe ich natürlich unverändert gelassen.

Ach ja, den Holzbalken habe ich bei Hornbach gekauft (~ 2 EUR). Er hat genau die Profilabmessungen wie mein ursprüngliches FT-Gleis (15mm x 60mm), nur ist der Balken viel verwindungssteifer, tragfähiger und sehr viel länger (2m).