---
layout: "image"
title: "Auch der Fahrer scheint seinen Spaß an diesem Vehikel zu haben!"
date: "2005-05-19T21:52:53"
picture: "PICT1266.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/4159
- /detailsd0c5.html
imported:
- "2019"
_4images_image_id: "4159"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4159 -->
