---
layout: "overview"
title: "Straßenbahnen - Karlsruher Spiegelwagen"
date: 2020-02-22T08:35:41+01:00
legacy_id:
- /php/categories/2531
- /categoriesf691.html
- /categories757a.html
- /categories8469.html
- /categoriesb1ef.html
- /categories83b9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2531 --> 
Karlsruher Straßenbahnwagen von 1929-1972