---
layout: "image"
title: "Zielfilmkasten 6"
date: "2012-02-18T19:58:49"
picture: "Zielfilmkasten_4.jpg"
weight: "14"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34244
- /details3b4e-2.html
imported:
- "2019"
_4images_image_id: "34244"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-18T19:58:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34244 -->
Und hier das komplette Prachtstück von vorne.