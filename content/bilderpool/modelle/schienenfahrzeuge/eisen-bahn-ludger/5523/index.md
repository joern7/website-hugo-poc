---
layout: "image"
title: "Bogenstück"
date: "2005-12-27T15:08:57"
picture: "DSCN0516.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5523
- /details2076.html
imported:
- "2019"
_4images_image_id: "5523"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5523 -->
