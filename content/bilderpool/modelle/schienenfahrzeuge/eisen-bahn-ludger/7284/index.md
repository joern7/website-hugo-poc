---
layout: "image"
title: "Impressionen (1)"
date: "2006-10-30T18:55:43"
picture: "DSCN1066.jpg"
weight: "34"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7284
- /detailsb063.html
imported:
- "2019"
_4images_image_id: "7284"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-30T18:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7284 -->
mit "Emma" auf großer Fahrt ...