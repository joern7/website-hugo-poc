---
layout: "image"
title: "'Akku' Tender (2)"
date: "2005-12-29T17:20:43"
picture: "DSCN0527.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5551
- /details95e2-2.html
imported:
- "2019"
_4images_image_id: "5551"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-29T17:20:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5551 -->
Ansicht von hinten.
Gut zu erkennen die beiden Akkus. Wenn einer leer ist wird auf den anderen umgesteckt.