---
layout: "image"
title: "mit Beleuchtung"
date: "2006-11-02T12:59:01"
picture: "DSCN1100.jpg"
weight: "44"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7305
- /details50d8.html
imported:
- "2019"
_4images_image_id: "7305"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-11-02T12:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7305 -->
Hier habe ich die LED Halter montiert. Leider erkennt man nicht das sie leuchten.