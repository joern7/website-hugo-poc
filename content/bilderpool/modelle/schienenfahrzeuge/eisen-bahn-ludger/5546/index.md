---
layout: "image"
title: "Drehgestell Loren Waggon (Ansicht von oben)"
date: "2005-12-27T15:32:28"
picture: "DSCN0513.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5546
- /details4853.html
imported:
- "2019"
_4images_image_id: "5546"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:32:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5546 -->
