---
layout: "image"
title: "Gerüst"
date: "2005-11-30T22:50:57"
picture: "geruest.jpg"
weight: "3"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/5440
- /details3639.html
imported:
- "2019"
_4images_image_id: "5440"
_4images_cat_id: "470"
_4images_user_id: "373"
_4images_image_date: "2005-11-30T22:50:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5440 -->
So sieht das Gerüst im Detail aus