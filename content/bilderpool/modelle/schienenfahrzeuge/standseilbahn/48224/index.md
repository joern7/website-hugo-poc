---
layout: "image"
title: "Antrieb (3)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48224
- /detailsef17.html
imported:
- "2019"
_4images_image_id: "48224"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48224 -->
Hier ein Blick seitlich in die Umlenkrollen