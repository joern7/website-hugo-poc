---
layout: "image"
title: "Türmotoren"
date: "2018-10-15T16:10:18"
picture: "standseilbahn20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48238
- /detailsac36-2.html
imported:
- "2019"
_4images_image_id: "48238"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48238 -->
Hier sieht man genauer, wie die Schwenkmechanik gebaut ist.