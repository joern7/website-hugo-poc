---
layout: "image"
title: "Antrieb (1)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48222
- /detailsfdb7.html
imported:
- "2019"
_4images_image_id: "48222"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48222 -->
Das eine einzige umlaufende Seil, das mit seinen Enden an beiden Wagen befestigt ist, wird oben um die kräftig angetriebene Drehscheibe (siehe die nächsten Bilder) geführt. Die links herausstehende Umlenkrolle ist durch Einbauen oder Entnehmen von Bausteinen versetzbar und kann so dafür verwendet werden, die nach einiger Zeit ob der Last festzustellende Verlängerung des Seils auszugleichen. Außerdem kann man so auch etwas justieren, wie weit der obere Wagen in die Bergstation einfährt (sodass die Kontakte zuverlässig erreicht werden nämlich), denn die Wagenposition bei der Talstation wird durch die dortigen Taster bestimmt.