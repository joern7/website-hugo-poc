---
layout: "image"
title: "Fahrgestelle (2)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48232
- /details10c3.html
imported:
- "2019"
_4images_image_id: "48232"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48232 -->
Das ist die Unterseite der Wagen.