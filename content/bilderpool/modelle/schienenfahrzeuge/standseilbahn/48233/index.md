---
layout: "image"
title: "Kurvenfahrt (1)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48233
- /details6a5c.html
imported:
- "2019"
_4images_image_id: "48233"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48233 -->
Dieses und die folgenden Bilder zeigen, wie die Wagen mit ihren Fahrwerken über die Weichen kommen. Hier der rechte Wagen kurz vor Überfahren der unteren Weiche bei Talfahrt.