---
layout: "image"
title: "Türen offen in der Talstation"
date: "2018-10-15T16:10:18"
picture: "standseilbahn22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48240
- /details8f1b.html
imported:
- "2019"
_4images_image_id: "48240"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48240 -->
Während die Türen offen sind, leuchtet jeweils die grüne Lampe, in allen anderen Zuständen die rote.