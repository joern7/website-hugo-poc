---
layout: "image"
title: "Heuaufnehmer"
date: "2009-06-29T23:27:02"
picture: "niederdruckpresse03.jpg"
weight: "3"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24478
- /detailsf06b.html
imported:
- "2019"
_4images_image_id: "24478"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24478 -->
Hier habe ich alte Fischertechnik Ketten mit Noppen an Zahnräder montiert.