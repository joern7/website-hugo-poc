---
layout: "image"
title: "Licht"
date: "2009-06-29T23:27:03"
picture: "niederdruckpresse11.jpg"
weight: "11"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24486
- /details63f1.html
imported:
- "2019"
_4images_image_id: "24486"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:03"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24486 -->
Hier sind die zwei Lampen hinten an.