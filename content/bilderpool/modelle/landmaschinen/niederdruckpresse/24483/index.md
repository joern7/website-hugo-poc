---
layout: "image"
title: "Von oben"
date: "2009-06-29T23:27:03"
picture: "niederdruckpresse08.jpg"
weight: "8"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24483
- /details4a8c.html
imported:
- "2019"
_4images_image_id: "24483"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24483 -->
