---
layout: "image"
title: "Innenansichten"
date: "2012-01-07T19:43:04"
picture: "k-100_0399.jpg"
weight: "11"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/33856
- /details300b.html
imported:
- "2019"
_4images_image_id: "33856"
_4images_cat_id: "2502"
_4images_user_id: "119"
_4images_image_date: "2012-01-07T19:43:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33856 -->
