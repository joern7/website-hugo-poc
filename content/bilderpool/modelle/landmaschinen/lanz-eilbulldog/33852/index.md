---
layout: "image"
title: "Bulldog mit Verdeck"
date: "2012-01-07T19:42:39"
picture: "k-100_0384.jpg"
weight: "7"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/33852
- /details19ef.html
imported:
- "2019"
_4images_image_id: "33852"
_4images_cat_id: "2502"
_4images_user_id: "119"
_4images_image_date: "2012-01-07T19:42:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33852 -->
