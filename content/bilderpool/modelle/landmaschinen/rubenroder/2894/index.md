---
layout: "image"
title: "Ruebenroder 02"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_02.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2894
- /details96ea.html
imported:
- "2019"
_4images_image_id: "2894"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2894 -->
von Claus-W. Ludwig