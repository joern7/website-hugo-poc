---
layout: "image"
title: "Ruebenroder 11"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_11.JPG"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2903
- /details77f0-2.html
imported:
- "2019"
_4images_image_id: "2903"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2903 -->
von Claus-W. Ludwig