---
layout: "image"
title: "Dungstreuer"
date: "2003-10-03T14:04:29"
picture: "MSt07.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1764
- /details029d.html
imported:
- "2019"
_4images_image_id: "1764"
_4images_cat_id: "191"
_4images_user_id: "4"
_4images_image_date: "2003-10-03T14:04:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1764 -->
Das Schrittschaltwerk. Die Griffe der Flachnabe mußten mit einem Messer etwas angespitzt werden. Bei jeder Umdrehung wird das Z30 um 2 x 2 Zähne weitergedreht.