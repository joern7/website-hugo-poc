---
layout: "image"
title: "FT-Strohpresse2"
date: "2003-05-30T16:30:45"
picture: "FT-traktor-Strohpresse2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/1139
- /details8e25-2.html
imported:
- "2019"
_4images_image_id: "1139"
_4images_cat_id: "132"
_4images_user_id: "22"
_4images_image_date: "2003-05-30T16:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1139 -->
