---
layout: "image"
title: "Lanz Bulldog"
date: "2012-08-26T20:28:54"
picture: "lanzbulldog01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35369
- /detailsbf87.html
imported:
- "2019"
_4images_image_id: "35369"
_4images_cat_id: "2624"
_4images_user_id: "22"
_4images_image_date: "2012-08-26T20:28:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35369 -->
De Lanz Bulldog is een type tractor ontwikkeld door de Duitse fabrikant Lanz.
Ontworpen door de Duitse ingenieur Fritz Huber, had de tractor een liggende één-cilinder tweetakt gloeikopdieselmotor. Door gebruik van deze ééncilinder had die een karakteristiek geluid, een staccato "boem-boem" geluid.
