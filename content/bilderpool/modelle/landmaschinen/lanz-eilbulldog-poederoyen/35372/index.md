---
layout: "image"
title: "Lanz Bulldog"
date: "2012-08-26T20:28:54"
picture: "lanzbulldog04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35372
- /detailsbc08.html
imported:
- "2019"
_4images_image_id: "35372"
_4images_cat_id: "2624"
_4images_user_id: "22"
_4images_image_date: "2012-08-26T20:28:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35372 -->
Werking
De ééncilinder had twee grote vliegwielen met aan ene kant de regulateur en de andere kant de koppeling. Deze vliegwielen hielden de motor op toeren. Het starten van deze tractoren ging als volgt: eerst werd een gas- of benzinebrander onder de gloeikop gezet om de "peer" roodgloeiend op te warmen, ondertussen konden diverse smeerwerkzaamheden worden uitgevoerd waaronder ook het bijvullen van de oliepomp. Tijdens het voorgloeien moest de oliepomp diverse malen worden rondgedraaid om de buizen goed met olie te vullen zodat men zeker was van een goede smering direct tijdens de start. Als de "peer" roodgloeiend geworden was zette men de gashendel in de middelste stand, daarna ongeveer twee maal pompen met de handslinger aan de dieselpomp om diesel in de gloeikop te krijgen. Nu werd het vliegwiel eerst rustig een paar keer van links naar recht gedraaid om zuurstof bij de diesel te krijgen zodat het mengsel goed was voor ontbranding. Als laatste handeling gaf men met het vliegwiel een krachtige draai met de klok mee. Door de compressie en de roodgloeiend gestookte peer kwam het dieselmengsel tot ontbranding en dus de motor tot leven wat altijd gepaard ging met een paar harde klappen daarna liep deze rustig ploffend stationair. Na het starten moest nog wel gecontroleerd worden of de motor in de juiste richting draaide. Dit gebeurde middels een aangebrachte pijl op het vliegwiel of een mechanische meter in de cabine van de tractor. Bleek dat de tractor in de verkeerde richting draaide dan zette je de gashendel helemaal terug en wachtte je totdat de tractor bijna zou stoppen. Op dat moment deed je de gashendel weer helemaal uit en zag je het vliegwiel naar links en rechts gaan en uiteindelijk in de goede richting.
Bij diverse modellen moest het stuurwiel met stuurstang los genomen worden om deze in het vliegwiel te plaatsen. Dit omdat verder op het vliegwiel geen voorziening was om het vliegwiel handmatig een slinger te geven. Later produceerde Lanz ook types die voorzien waren van een zogeheten 'aanwerpschijf' die gemonteerd zat op het vliegwiel.
