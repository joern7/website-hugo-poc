---
layout: "image"
title: "Maehdr12.JPG"
date: "2003-11-11T19:15:06"
picture: "Maehdr12.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1974
- /detailsb856.html
imported:
- "2019"
_4images_image_id: "1974"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1974 -->
Rechte Seite. Das Z30 treibt den Körneraufzug (Kette im Alu-Vierkant auf der gegenüberliegenden Seite an)