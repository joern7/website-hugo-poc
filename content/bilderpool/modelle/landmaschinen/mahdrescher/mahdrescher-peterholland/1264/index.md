---
layout: "image"
title: "FT-mahdresher"
date: "2003-07-25T11:07:09"
picture: "FT-MD1.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/1264
- /details99c5.html
imported:
- "2019"
_4images_image_id: "1264"
_4images_cat_id: "192"
_4images_user_id: "22"
_4images_image_date: "2003-07-25T11:07:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1264 -->
Fischertechnik Mahdrescher 

Einen ”New Holland” , ein “Case-International”, oder eine “Massey Ferguson -Mahdrescher” zu bauen war nicht unbedingt meine Absicht.  Es ist schliesslich eine “Fischertechnik Mahdrescher“.
Für den Straßentransport habe ich ein Wagen hinzu, auf dem das Schneidwerk hinterhergezogen wird.

Der Mähdrescher ist sozusagen selbsttragend. Es gibt ein Gerüst aus U-Trägern, Winkelträgern und viele Alu-Profilen, an dem alle beweglichen Teile befestigt sind. 

Meine Fischertechnik Mahdrescher hat insgesamt 5 Motoren:

1. Fahr-Antrieb (Conrad Powermot 110 min-1 und grosse Conrad-Reifen)
2. Lenkung (S-Motor)
3. Heben Schneidwerk (S-Motor)
4. Korntankschnecke-entlerung (Minimot)
5. Dreschwerk (Conrad PowerMot 110 min-1): eine kombinierter Antrieb fur 10 Funkionen

Das Dreschwerk enthalt die folgende 10 Sachen: schneiden(1), hasplen(2), zusammen fugen mit eine Einzugsschnecke(3), eine Schrägförderer(4), eine Dreschtrommel(5), eine Abnehm/Wendetrommel(6), eine lange Strohschuttelrutsche(7), eine Siebenschuttelrutsche(8), Reinigung mit Ventilator(9), und Korn zusammeln mit eine Elevator(10). Mit eine Leuchtstein kann man drinnen Alles anschauen. 
Der Schrägförderer dient gleichzeitig als Kettenantrieb für das Schneidwerk, Haspel und Einzugsschnecke. Meine Mahdrescher hat keine Strohhäcksler hinten am Mähdrescher.