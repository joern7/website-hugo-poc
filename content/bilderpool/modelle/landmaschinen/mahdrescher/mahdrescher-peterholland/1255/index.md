---
layout: "image"
title: "FT-Mahdrescher"
date: "2003-07-25T11:07:09"
picture: "FT-MD24.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/1255
- /detailsd87a.html
imported:
- "2019"
_4images_image_id: "1255"
_4images_cat_id: "192"
_4images_user_id: "22"
_4images_image_date: "2003-07-25T11:07:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1255 -->
Fahrantrieb, Lenkung und die Korntankschnecke-entlerung werden über das IR-Control gesteuert. Damit kann man fahren und entlehren, ohne daß Schalter am Mähdrescher gebraucht werden. Das heben des Schneidwerk ist semi-IR-Control gesteuert; nur fur die Drehrichtung braucht man eine Schalter hinter dem Fahrersitz.  Auch fur das Dreschwerk und die Beleuchtung habe ich Schalter.
Ich brauche 8 st NiMH-1.2V-Akkus für die Stromzufuhr. Das reicht nur gerade genug.

Die Lenkung hat eine S-Motor und Klemmschnecke-antrieb. Die hat eine langsame Übersetzung und braucht nicht soviel Höhe zum Einbau. 
 
Mit ein M-4-Gewinde (316L) wird das Schneidwerk langsam gehoben und gesenkt. Das Schneidwerk ist ziemlich schwer, vorallem wegen die viele 316L-Metallachse fur die Haspel.
Die Haspel mit Pneumatik Zylinder kann nur mit Handbetrieb gehoben werden. Ich habe keine speziale Kompressor dazu eingebaut.

Die Mahdrescher hat eine Reinigungs-Ventilator unter dem Dreschtrommel, Abnehmtrommel und Siebboden. Die Reinigungs-Ventilator habe ich gebaut mit 2 Kreuzknotenplatten (31665) und 4 st  
I-Strebe im jedem Loch der Kreuzknotenplatte. 

Fur dem Elevator habe ich kleine Rastraupenbelag (37210) und ein Alu-profil-20x40mm vom Baumarkt genutzt. Das Alu-profil-20x40mm ist innen 16x36 mm. Das reicht genau. Das selbe billige (nicht FT-) Alu-Profil, habe ich auch fur meine Autokran genutzt. Mit eine Zahnstangen-getriebe (37272) ist Alles sehr kompakt und Stark.

Die Korntank-Entleerungsschnecke kann nur mit Handbetreib ausgeschwenkt und eingeklappt werden. Die Entleerungsschnecke darf nur dann laufen, wenn die Schnecke ausgeschwenkt ist, sonst gibt es Bruch im Getriebe.