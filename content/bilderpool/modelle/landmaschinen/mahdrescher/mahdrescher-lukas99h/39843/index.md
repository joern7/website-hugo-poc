---
layout: "image"
title: "Mähdrescher05"
date: "2014-11-23T09:32:19"
picture: "maehdrescher5.jpg"
weight: "5"
konstrukteure: 
- "lukas99h."
fotografen:
- "lukas99h."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39843
- /detailsc264.html
imported:
- "2019"
_4images_image_id: "39843"
_4images_cat_id: "2989"
_4images_user_id: "1631"
_4images_image_date: "2014-11-23T09:32:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39843 -->
Im Gelände fährt er auch ohne Probleme, ich habe mal einen Test gemacht, er schafft so ca.35grad Vorwärts und ca. 46!grad Rückwärts.... liegt daran, dass der Akku oberhalb der hinteren Achse platziert ist.:/