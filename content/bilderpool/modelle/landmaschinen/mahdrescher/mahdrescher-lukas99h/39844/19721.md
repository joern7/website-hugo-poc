---
layout: "comment"
hidden: true
title: "19721"
date: "2014-11-23T17:49:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein sehr gelungenes Modell! Jetzt würden aber noch Bilder von unten und von den Innereien interessieren - hast Du da vielleicht noch welche?
Gruß,
Stefan