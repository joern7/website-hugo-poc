---
layout: "image"
title: "Mähdrescher06"
date: "2014-11-23T09:32:19"
picture: "maehdrescher6.jpg"
weight: "6"
konstrukteure: 
- "lukas99h."
fotografen:
- "lukas99h."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39844
- /detailse464.html
imported:
- "2019"
_4images_image_id: "39844"
_4images_cat_id: "2989"
_4images_user_id: "1631"
_4images_image_date: "2014-11-23T09:32:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39844 -->
Das Mähwerk ist relativ unspektakulär, aber es kann per Stecksytem ( mit einer Achse) mit dem Mähdrescher verbunden werden.