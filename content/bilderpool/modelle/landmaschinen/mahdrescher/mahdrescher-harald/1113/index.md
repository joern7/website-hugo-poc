---
layout: "image"
title: "MMM10"
date: "2003-05-05T21:01:23"
picture: "MMM10.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1113
- /details87d4-2.html
imported:
- "2019"
_4images_image_id: "1113"
_4images_cat_id: "113"
_4images_user_id: "4"
_4images_image_date: "2003-05-05T21:01:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1113 -->
Mähdrescher Modell Mammut, noch im Werden. 
Dem Siggi sei ein Trullala  - die Reifen sind genial!