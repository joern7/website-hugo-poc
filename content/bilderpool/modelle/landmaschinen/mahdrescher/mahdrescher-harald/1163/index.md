---
layout: "image"
title: "MMM14.jpg"
date: "2003-05-31T22:03:57"
picture: "MMM14.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schalter", "mit", "Schiebe-Betätigung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1163
- /detailse537-2.html
imported:
- "2019"
_4images_image_id: "1163"
_4images_cat_id: "113"
_4images_user_id: "4"
_4images_image_date: "2003-05-31T22:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1163 -->
Schalter aus Minitastern