---
layout: "image"
title: "Forwader 03"
date: "2006-05-07T15:16:33"
picture: "Forwader_03.jpg"
weight: "3"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/6225
- /detailsc580-2.html
imported:
- "2019"
_4images_image_id: "6225"
_4images_cat_id: "537"
_4images_user_id: "10"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6225 -->
Blick auf das gefederte Nachlaufmodul mit Pendelachse