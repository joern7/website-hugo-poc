---
layout: "overview"
title: "Forwarder (Holzernter)"
date: 2020-02-22T08:24:56+01:00
legacy_id:
- /php/categories/537
- /categories6dee.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=537 --> 
Forwarder sind geländegänige, robuste Maschinen um abgelängte Baumstämme aus dem Wald zu bergen