---
layout: "image"
title: "Der zweite Anhänger"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern08.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42553
- /details30b8.html
imported:
- "2019"
_4images_image_id: "42553"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42553 -->
Etwas näher am Original und doch auch anders. Was halt noch so an Beifang in der Schublade rumlag.