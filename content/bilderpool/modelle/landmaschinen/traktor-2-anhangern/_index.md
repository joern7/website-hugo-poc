---
layout: "overview"
title: "Traktor mit 2 Anhängern"
date: 2020-02-22T08:26:28+01:00
legacy_id:
- /php/categories/3163
- /categorieseb83.html
- /categories0739.html
- /categoriesf4cf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3163 --> 
'Rollen' heißen die Zweiachsanhänger mit Deichsel. So fuhren sie in meiner Kindheit durchs Ort hoch beladen mit Heu- und Strohballen.