---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:02:59"
picture: "traktormitanhaenger05.jpg"
weight: "5"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33903
- /detailsac8a.html
imported:
- "2019"
_4images_image_id: "33903"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33903 -->
Anhängerdeichsel und Traktorkupplung getrennt.