---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:02:58"
picture: "traktormitanhaenger01.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33899
- /details4840.html
imported:
- "2019"
_4images_image_id: "33899"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33899 -->
Traktor sowie Anhänger sind fernsteuerbar. Der Traktor hat folgende Fernsteuerfunktionen: Fahrt vorwärts - rückwärts, schnell - langsam, rechts - links - geradeaus. Licht: Frontscheinwerfer und Rücklichter an - aus, Arbeitsscheinwerfer an - aus. Das Warnlicht blinkt, wenn die Arbeitsscheinwerfer eingeschaltet sind. Mit derselben Fernsteuerung kann auch der Anhänger gesteuert werden. Fernsteuerfunktionen des Anhängers: Ladefläche heben - senken (kippen), Gesamten Anhänger samt Deichsel mit Hilfe des Stützrades anheben - absenken. Dadurch ist es möglich, den Anhänger mittels Fernsteuerung an- bzw. abzukuppeln. Es fordert aber einiges an Geschick des Bedieners, den Traktor jeweils so zu positionieren, dass die Deichselöse genau auf dessen Anhängerkupplung passt.