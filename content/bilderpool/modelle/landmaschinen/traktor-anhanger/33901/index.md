---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:02:59"
picture: "traktormitanhaenger03.jpg"
weight: "3"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33901
- /details3751.html
imported:
- "2019"
_4images_image_id: "33901"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33901 -->
Gespann von schräg hinten.