---
layout: "image"
title: "Traktor"
date: "2012-01-13T19:02:59"
picture: "traktormitanhaenger09.jpg"
weight: "9"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33907
- /details6933.html
imported:
- "2019"
_4images_image_id: "33907"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33907 -->
Vorderradlenkung.