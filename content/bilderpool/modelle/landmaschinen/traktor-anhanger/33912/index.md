---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:03:10"
picture: "traktormitanhaenger14.jpg"
weight: "14"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33912
- /detailsac65.html
imported:
- "2019"
_4images_image_id: "33912"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33912 -->
Die Deichselöse besteht aus einem halben Gelenkstein (ArtNr. 35848 od. 36223)