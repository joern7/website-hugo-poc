---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:03:10"
picture: "traktormitanhaenger13.jpg"
weight: "13"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33911
- /detailsa80e.html
imported:
- "2019"
_4images_image_id: "33911"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33911 -->
Der Fernsteuerempfänger wurde mit einem kleinen Stück doppelseitigen Klebebandes auf das rechte hintere Schutzblech aufgeklebt.