---
layout: "image"
title: "MB Trac - Gesamtansicht 4"
date: "2016-02-29T21:09:00"
picture: "mbtrac04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42933
- /detailsec3c.html
imported:
- "2019"
_4images_image_id: "42933"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42933 -->
Am spannendsten ist wahrscheinlich die Vorderachse mit Federung, Lenkung, Antrieb und Versatzgetriebe. Die hatte ich schon mal separat aufgedröselt, siehe https://ftcommunity.de/categories.php?cat_id=3166&page=5 .
Aber die Räder muss ich noch verbessern. In der Bilderserie für die Vorderachse hat H.A.R.R.Y. schon richtig bemerkt, dass die Sternlaschen mit dem Statik-Langloch in der Mitte die Kräfte nicht dauerhaft vertragen. Ich habe beim Testen noch gemerkt, dass die Scheiben, die die Sternlasche auf dem Lagerstück festhalten, manchmal abspringen. Insofern ist das Ganze noch als Prototyp einzustufen.