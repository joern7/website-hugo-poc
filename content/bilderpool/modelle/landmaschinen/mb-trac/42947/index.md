---
layout: "image"
title: "MB Trac - Aufhängung Hinterradschwinge 3"
date: "2016-02-29T21:09:00"
picture: "mbtrac18.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42947
- /details8811.html
imported:
- "2019"
_4images_image_id: "42947"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42947 -->
Da sind zwei BS7,5 verschoben aufaibabdergebaut und mit einem Klemmdingsbums, einer Platte 15x15 und einer BSB-Spur-N-Grundplatte gesichert.