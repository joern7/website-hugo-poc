---
layout: "image"
title: "MB Trac - Befestigung Kotflügel 1"
date: "2016-02-29T21:09:00"
picture: "mbtrac20.jpg"
weight: "20"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42949
- /details3e35-2.html
imported:
- "2019"
_4images_image_id: "42949"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42949 -->
Ein Blick auf die Befestiung der Kotflügel.