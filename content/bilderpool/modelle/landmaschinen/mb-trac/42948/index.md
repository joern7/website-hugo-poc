---
layout: "image"
title: "MB Trac - Hinterradschwinge"
date: "2016-02-29T21:09:00"
picture: "mbtrac19.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42948
- /detailsd6dc-2.html
imported:
- "2019"
_4images_image_id: "42948"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42948 -->
Hier sieht man wie die Kardamwelle in der Hinterradschwinge gelagert ist. Kurzkupplung mit Federnocken zwischen Kardangelenk und Kegelzahnrad, auf den Federnocken ist noch eine Klemmbuchse 5 geschoben, damit das Ganze etwas dicker ist halbwegs gut in den "entkernten" Gelenkbaustein passt.