---
layout: "image"
title: "Ansicht von vorne"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug02.jpg"
weight: "2"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/9396
- /detailsfd38.html
imported:
- "2019"
_4images_image_id: "9396"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9396 -->
Der Akku im Führerhaus kommt raus sobald ich wieder geladene 9V-Blöcke habe.