---
layout: "image"
title: "seilkranmitfahrzeug10.jpg"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug10.jpg"
weight: "10"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/9404
- /details9215.html
imported:
- "2019"
_4images_image_id: "9404"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9404 -->
In die Batteriekästen können die Seile o.Ä. verstaut werden. Außerdem schützen sie den Powermotor dahinter vor Stößen etc. Die ganzen grauen 30er muss ich noch ersetzen, das sieht nicht so toll aus.
Außerdem habe ich vor noch einen Anhänger zu bauen, auf dem ein Kran montiert ist um das Holz zu verladen.