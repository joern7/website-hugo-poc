---
layout: "image"
title: "MB-Traktor + Pflug2"
date: "2004-07-02T12:55:57"
picture: "MB-Tractor__Pflug2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/2538
- /details4d14.html
imported:
- "2019"
_4images_image_id: "2538"
_4images_cat_id: "239"
_4images_user_id: "22"
_4images_image_date: "2004-07-02T12:55:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2538 -->
