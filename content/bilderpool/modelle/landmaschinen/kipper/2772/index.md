---
layout: "image"
title: "Kipper 001"
date: "2004-11-03T11:41:06"
picture: "Kipper_001.JPG"
weight: "1"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/2772
- /details7065.html
imported:
- "2019"
_4images_image_id: "2772"
_4images_cat_id: "271"
_4images_user_id: "119"
_4images_image_date: "2004-11-03T11:41:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2772 -->
