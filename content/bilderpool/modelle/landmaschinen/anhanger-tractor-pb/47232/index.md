---
layout: "image"
title: "Anh-f05"
date: "2018-02-01T15:19:19"
picture: "anhaenger05.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47232
- /details1353-4.html
imported:
- "2019"
_4images_image_id: "47232"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47232 -->
