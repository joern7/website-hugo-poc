---
layout: "image"
title: "Anh-gesamt-02"
date: "2018-02-01T15:19:18"
picture: "anhaenger02.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47229
- /detailseb4a.html
imported:
- "2019"
_4images_image_id: "47229"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47229 -->
