---
layout: "image"
title: "anhaengertractorpb3.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb3.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47166
- /detailsf88b-2.html
imported:
- "2019"
_4images_image_id: "47166"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47166 -->
Das Kippen gehit mit einen Powermotor 50:1 (den 20:1 hat's nicht geschafft) über ein Schneckengetriebe. Den Motor befindet sich an Hintenseite zwischen die Räder.