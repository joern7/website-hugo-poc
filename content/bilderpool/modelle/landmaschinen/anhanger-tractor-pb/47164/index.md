---
layout: "image"
title: "anhaengertractorpb1.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb1.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47164
- /details6b1b.html
imported:
- "2019"
_4images_image_id: "47164"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47164 -->
Ein passender Ernter findet man hier: https://ftcommunity.de/categories.php?cat_id=3486
Den Anhänger nimmt sein Strom zum Kippen und Aufmachen der Heckklappe vom IR-Empfänger am Tractor. Er wäre noch schöner gewesen wenn er den Motor vom Tractor benützte fürs Kippen, aber dazu musste ich eine Kupplung/Freilauf/Wellen-wahl (PTO, Power take off) konstruieren, so das man separat Reiten und Kippen kann. Das ist in der Kleinraum wohl nicht möglich. (Oder? Herausforderung?)