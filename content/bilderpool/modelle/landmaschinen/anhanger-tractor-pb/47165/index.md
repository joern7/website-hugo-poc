---
layout: "image"
title: "anhaengertractorpb2.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb2.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47165
- /details9770-2.html
imported:
- "2019"
_4images_image_id: "47165"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47165 -->
Den Tractor ist wieder Mal den aus den Tractor IR-set.