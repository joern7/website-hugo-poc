---
layout: "image"
title: "Heuballenpresse 006"
date: "2004-11-03T12:23:14"
picture: "Heuballenpresse_006.JPG"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2787
- /details4f9a.html
imported:
- "2019"
_4images_image_id: "2787"
_4images_cat_id: "273"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2787 -->
von Claus-W. Ludwig