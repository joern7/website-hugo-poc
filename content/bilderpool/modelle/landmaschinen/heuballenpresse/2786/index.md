---
layout: "image"
title: "Heuballenpresse 005"
date: "2004-11-03T12:23:14"
picture: "Heuballenpresse_005.JPG"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2786
- /details0130.html
imported:
- "2019"
_4images_image_id: "2786"
_4images_cat_id: "273"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2786 -->
von Claus-W. Ludwig