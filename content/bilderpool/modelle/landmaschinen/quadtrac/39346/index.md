---
layout: "image"
title: "Fahrwerk und Geländeausgleich"
date: "2014-09-12T11:45:54"
picture: "qtrac12.jpg"
weight: "12"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39346
- /details8c9e.html
imported:
- "2019"
_4images_image_id: "39346"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39346 -->
Die Kettenpaare sind immer paarweise aufgehängt. Die Last lastet aber nicht auf dem oberen Antriebszahnrad und dessen Achse sondern auf dem Gelenk, das sich 15mm unterhalb dessen befindet.