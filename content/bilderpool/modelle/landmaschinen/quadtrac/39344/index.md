---
layout: "image"
title: "Beweglichkeit des Drehgelenks"
date: "2014-09-12T11:45:42"
picture: "qtrac10.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39344
- /details234d.html
imported:
- "2019"
_4images_image_id: "39344"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39344 -->
Die Stifte am schwarzen Zahnrad zeigen die Beweglichkeit des Gelenks an.