---
layout: "image"
title: "Geländebeispiel 1"
date: "2014-09-12T11:45:42"
picture: "qtrac04.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39338
- /details7f07.html
imported:
- "2019"
_4images_image_id: "39338"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39338 -->
Dank dem Drehgelenk, das den Hinterwagen mit dem Vorderwagen verbindet, können größe Unebenheiten im Boden ausgeglichen werden (hier Extrembeispiel mit Bücherstapel)