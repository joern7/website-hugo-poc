---
layout: "image"
title: "Drehgelenk 3"
date: "2014-09-12T11:45:42"
picture: "qtrac09.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39343
- /details6eab.html
imported:
- "2019"
_4images_image_id: "39343"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39343 -->
Damit das drehbare Rote Rad sich nicht löst, ist es mit einer Nabenmutter auf eine Stange geschraubt. Die Stange ist drehbar und wird am Hinterwagen fixiert.