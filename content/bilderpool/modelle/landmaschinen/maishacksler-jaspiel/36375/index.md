---
layout: "image"
title: "Maishakselaar"
date: "2013-01-02T21:12:33"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- /php/details/36375
- /details3712.html
imported:
- "2019"
_4images_image_id: "36375"
_4images_cat_id: "2763"
_4images_user_id: "1295"
_4images_image_date: "2013-01-02T21:12:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36375 -->
Een FT-maishakselaar. Bijzonderheden: 
- alu profielen als dragende constructie voor zowel chassis trekker als hakselbek
- 4WD aangedreven door XM, autom. versnellingsbak (afgekeken van o.a. Peter Holland), op afstand bestuurbaar maar gemonteerd op twee sokkels
- de 6-rijige maisbek kan handmatig omhoog en omlaag via een wormaandrijving
- de maisbek heeft het kettinginvoersusteem plus invoerwals, aangedreven door twee aparte motoren
- de uitstroompijp is eveneens motorisch aangedreven en via de afstandsbediening bestuurbaar
- tot slot natuurlijk verlichting V en A plus schijnwerpers