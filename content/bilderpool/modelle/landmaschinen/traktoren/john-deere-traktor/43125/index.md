---
layout: "image"
title: "Traktor von der Seite"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor01.jpg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43125
- /detailse546.html
imported:
- "2019"
_4images_image_id: "43125"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43125 -->
Hier einmal ein Modell in der Größe vom Baukasten (ADVANCED Traktor Set IR Control von Ft. )