---
layout: "image"
title: "Trichter von Beifahrerseite :)"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor06.jpg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43130
- /detailsf7a6-2.html
imported:
- "2019"
_4images_image_id: "43130"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43130 -->
Als Tür habe ich einfach das Bauteil Lenkstange 47 (Triangel) 32884 verbaut. Dadurch hat der Traktor jetzt sozusagen Flügeltüren. :)