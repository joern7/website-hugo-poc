---
layout: "image"
title: "Ansicht"
date: "2007-07-19T14:52:01"
picture: "DSCN1448.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/11136
- /detailsbd79.html
imported:
- "2019"
_4images_image_id: "11136"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11136 -->
Hier ist der Vorderradantrieb zu erkennen. Über die Welle, Kette, Zahnrad zum Differential. Das liegt schön versteckt im Modell.
Zur lagerung der Vorderräder habe ich Kugellager verwendet.