---
layout: "image"
title: "Vorderache (Prototyp)"
date: "2007-09-26T15:07:38"
picture: "DSCN1517.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12015
- /detailsfae8.html
imported:
- "2019"
_4images_image_id: "12015"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12015 -->
Hier habe ich bei der Vorderachse auf den Antrieb verzichtet.