---
layout: "image"
title: "andere Hinterachs-Variante"
date: "2007-09-26T15:07:38"
picture: "DSCN1514.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12013
- /detailsd954.html
imported:
- "2019"
_4images_image_id: "12013"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12013 -->
Hier habe ich noch ein bisschen weiter entwickelt.
Die obere Schneckenmutter enthält nur ein Kugellager. So kann ich das Rastzahnrad von außen auf die K-Achse, bis in die Schneckenmutter hinein, aufstecken.