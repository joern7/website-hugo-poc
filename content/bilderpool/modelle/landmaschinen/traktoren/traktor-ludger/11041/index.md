---
layout: "image"
title: "Lenkung"
date: "2007-07-13T12:03:15"
picture: "DSCN1434.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["32455"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/11041
- /detailsc924.html
imported:
- "2019"
_4images_image_id: "11041"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-13T12:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11041 -->
Die Lenkung habe ich im Prinzip vom meinem vorherigen Modell übernommen. Nur das ich die Zahnstange etwas "verlängern" mußte. Klappt aber ganz gut.