---
layout: "image"
title: "Lenkung"
date: "2007-07-19T14:52:01"
picture: "DSCN1454.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/11137
- /details409f.html
imported:
- "2019"
_4images_image_id: "11137"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11137 -->
Das ist der Antrieb für die Lenkung. Die Verbindung der Kardangelenke habe ich mit einer Federnocke gemacht. Das ist kürzer als die Rastachse.