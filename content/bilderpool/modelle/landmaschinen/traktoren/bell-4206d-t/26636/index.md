---
layout: "image"
title: "zeer flexibel"
date: "2010-03-07T10:12:48"
picture: "P3060378.jpg"
weight: "14"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26636
- /details80b3-2.html
imported:
- "2019"
_4images_image_id: "26636"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26636 -->
Ongeveer de maximale uitslag van het knikpunt. Besturing heeft er geen last van want die zit ervoor