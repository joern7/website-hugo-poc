---
layout: "image"
title: "Bell 4206D-T"
date: "2010-03-08T17:31:10"
picture: "P3080004.jpg"
weight: "24"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26666
- /details9264-2.html
imported:
- "2019"
_4images_image_id: "26666"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-08T17:31:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26666 -->
Nu nog beter te zien. Ook heb ik een extra schalm in de kettingen gedaan om de ketting spanning te verminderen.