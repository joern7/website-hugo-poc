---
layout: "image"
title: "knikpunt"
date: "2010-03-07T10:12:48"
picture: "P3060370.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26629
- /detailsced5.html
imported:
- "2019"
_4images_image_id: "26629"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26629 -->
De beide schanieren zorgen voor het roteren van de achterzijde ten opzichte van de voorzijde.
De aandrijf as heeft dus geen dragende functie.