---
layout: "image"
title: "aandrijving achter"
date: "2010-03-07T10:12:48"
picture: "P3060375.jpg"
weight: "12"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26634
- /details2e17-4.html
imported:
- "2019"
_4images_image_id: "26634"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26634 -->
Net zo als de voorkant, Alleen zit de diff ver naar achter vanwege de schuivende tandwielen