---
layout: "image"
title: "geknikt"
date: "2010-03-07T22:41:44"
picture: "P3070398.jpg"
weight: "20"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26657
- /details453c.html
imported:
- "2019"
_4images_image_id: "26657"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T22:41:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26657 -->
Kan nu niet zover knikken want dan komen de rupsen tegen elkaar.
Optisch mooier dan met de wielen.