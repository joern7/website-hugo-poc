---
layout: "comment"
hidden: true
title: "8751"
date: "2009-03-11T08:24:34"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Gibt es die Längslenker (hier die langen Statikstreben seitlich) zur Stabilisierung der Vorderachse im Original auch? Oder hast Du die nur zur Verbesserung des Modells hinzugefügt?

Gruß, Thomas