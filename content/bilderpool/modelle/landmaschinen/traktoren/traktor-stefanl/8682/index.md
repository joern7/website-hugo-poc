---
layout: "image"
title: "Traktor 17"
date: "2007-01-25T17:58:40"
picture: "traktor17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8682
- /detailsdcac.html
imported:
- "2019"
_4images_image_id: "8682"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8682 -->
Hier der Antrieb für den Hebemechanismus.