---
layout: "image"
title: "Traktor 20"
date: "2007-01-25T17:58:40"
picture: "traktor20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8685
- /detailsc294-2.html
imported:
- "2019"
_4images_image_id: "8685"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8685 -->
