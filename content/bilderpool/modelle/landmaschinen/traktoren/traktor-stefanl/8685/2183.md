---
layout: "comment"
hidden: true
title: "2183"
date: "2007-01-26T08:44:46"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Na also!!! Nix Fehlversuch mehr! Das Ding ist sauber gebaut und sieht hervorragend aus! Super! Mir persönlich fehlen jetzt noch Details wie Sitz und Lenkrad, aber das ist Kleinkram...

Wie sieht's denn im Gelände aus? Was hat der Traktor für eine Steigfähigkeit? Fliegen da nicht gern mal die Kardangelenke auseinander? ;o)

@Frank: Da ich auch gerade einen Traktor baue: Was ist denn ein "Ballasthalter vorne"??? Wozu braucht man das?

Gruß, Thomas