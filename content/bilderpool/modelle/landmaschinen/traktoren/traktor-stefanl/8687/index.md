---
layout: "image"
title: "Traktor 22"
date: "2007-01-25T17:58:40"
picture: "traktor22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8687
- /detailsff01.html
imported:
- "2019"
_4images_image_id: "8687"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8687 -->
