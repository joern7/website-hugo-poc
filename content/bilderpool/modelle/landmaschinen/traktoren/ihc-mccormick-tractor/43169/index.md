---
layout: "image"
title: "IHC Schaltechnik 5 Microservo in Nullstellung"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor35.jpg"
weight: "35"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43169
- /detailsf4c9.html
imported:
- "2019"
_4images_image_id: "43169"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43169 -->
Hier wieder die Nullstellung der Schalttechnik.

Ich möchte mich auf diesem Wege erst mal bei Allen bedanken für das Interesse, die Kommentare und die tolle Aufnahme in die Ft-Gemeinde. Ich beschäftige mich jetzt schon wieder seit14 Jahre intensiv mit Fischertechnik und habe immer bei Ft- Community reingeschaut. Nun habe ich die ersten Schritte nach draußen gewagt. Ich werde Eure Fragen gerne beantworten, aber gebt mir bitte etwas Zeit, ich  muß mich erst an das ganze Neue gewöhnen.Meine e-mail Adresse ist freigeschaltet, falls ihr Fragen an mich direkt habt.

Gruß Detlef