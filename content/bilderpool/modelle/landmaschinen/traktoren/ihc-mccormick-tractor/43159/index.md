---
layout: "image"
title: "IHC noch mal von vorn 1"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor25.jpg"
weight: "25"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43159
- /details3a8a-3.html
imported:
- "2019"
_4images_image_id: "43159"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43159 -->
ohne Beschreibung