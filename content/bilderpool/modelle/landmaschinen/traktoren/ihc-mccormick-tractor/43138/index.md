---
layout: "image"
title: "IHC  Blick von oben auf den Rahmen"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor04.jpg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43138
- /details4625.html
imported:
- "2019"
_4images_image_id: "43138"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43138 -->
ohne Beschreibung