---
layout: "image"
title: "IHC von vorn"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor15.jpg"
weight: "15"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43149
- /details3c46.html
imported:
- "2019"
_4images_image_id: "43149"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43149 -->
ohne Beschreibung