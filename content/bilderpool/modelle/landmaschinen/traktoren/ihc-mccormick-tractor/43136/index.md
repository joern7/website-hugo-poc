---
layout: "image"
title: "IHC Blick auf die Vorderachse 1"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor02.jpg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43136
- /detailse7f4.html
imported:
- "2019"
_4images_image_id: "43136"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43136 -->
Der Powermotor ist für den Kardanwellenanschluß amHeck, und wid natürlich extra angesteuert. Der Motor "New-age black" 32618 betätigt die Ackerschiene (Geräteträger) auf und ab und wird ebenfalls extra angesteuert.