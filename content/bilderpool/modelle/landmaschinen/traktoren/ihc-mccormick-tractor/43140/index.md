---
layout: "image"
title: "IHC Hinterad Antrieb"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor06.jpg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43140
- /detailsc021.html
imported:
- "2019"
_4images_image_id: "43140"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43140 -->
Der Powermotor geht direckt aufs Differential ,von dort wird der Antrieb über eine Rastachse an die Planetengetriebe, welche sich an der äusseren Rad innenseite befinden, übertragen.