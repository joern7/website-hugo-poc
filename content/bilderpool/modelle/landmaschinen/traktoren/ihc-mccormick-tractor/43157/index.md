---
layout: "image"
title: "IHC Blick in die Fahrerkabiene von hinten 1"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor23.jpg"
weight: "23"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43157
- /detailsd6aa-2.html
imported:
- "2019"
_4images_image_id: "43157"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43157 -->
ohne Beschreibung