---
layout: "image"
title: "IHC von hinden"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor17.jpg"
weight: "17"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43151
- /details11c1.html
imported:
- "2019"
_4images_image_id: "43151"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43151 -->
Hier sieht man den verstelbaren Halter für die Anbaugeräte im oberen Bereich.