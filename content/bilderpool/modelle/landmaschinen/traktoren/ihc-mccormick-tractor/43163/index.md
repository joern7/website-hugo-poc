---
layout: "image"
title: "IHC von oben 2"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor29.jpg"
weight: "29"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43163
- /details8443.html
imported:
- "2019"
_4images_image_id: "43163"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43163 -->
ohne Beschreibung