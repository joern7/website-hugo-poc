---
layout: "image"
title: "IHC Blick in die Fahrerkabiene von hinten 2"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor24.jpg"
weight: "24"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43158
- /detailsabcb.html
imported:
- "2019"
_4images_image_id: "43158"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43158 -->
ohne Beschreibung