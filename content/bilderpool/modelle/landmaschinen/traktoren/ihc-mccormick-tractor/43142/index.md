---
layout: "image"
title: "IHC Lenkung 2"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor08.jpg"
weight: "8"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43142
- /details76a4.html
imported:
- "2019"
_4images_image_id: "43142"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43142 -->
ohne Beschreibung