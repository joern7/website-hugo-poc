---
layout: "image"
title: "IHC Schaltechnik 3 Microservo in Nullstellung"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor33.jpg"
weight: "33"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43167
- /detailsf4f3.html
imported:
- "2019"
_4images_image_id: "43167"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43167 -->
Hier sieht man, das die Bauplatte 5 am  Schaltnocken hängen bleibt. (Dauerbetrieb)