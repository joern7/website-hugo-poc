---
layout: "image"
title: "IHC Hinterachse 1"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor10.jpg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43144
- /details53a2-2.html
imported:
- "2019"
_4images_image_id: "43144"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43144 -->
Hinterachse mit Differential; die Stahlachse ist die Trägerachse der Räder.Auch hier schön zu sehen: das Planetengetriebe am Radinneren.