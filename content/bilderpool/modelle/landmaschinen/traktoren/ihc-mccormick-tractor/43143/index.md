---
layout: "image"
title: "IHC Lenkung 3"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor09.jpg"
weight: "9"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43143
- /detailsfc97.html
imported:
- "2019"
_4images_image_id: "43143"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43143 -->
Hier ein Bild wo der Rahmen mit  Vorderachse und Lenkung kopfüber auf dem Tisch liegt.