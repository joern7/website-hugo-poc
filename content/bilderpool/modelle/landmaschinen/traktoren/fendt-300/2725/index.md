---
layout: "image"
title: "Fendt300-07"
date: "2004-11-01T10:20:22"
picture: "Fendt300-07.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2725
- /detailsf739.html
imported:
- "2019"
_4images_image_id: "2725"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2725 -->
Noch ein Blick auf die Unterseite. Die Vorderachse ist nur über die roten Bauplatten 90x30 (hochkant beiderseits des Power-Mot) tragend mit dem Hinterteil verbunden. Die gelben Streben 75 sorgen für den definierten Abstand und halten die  Vorderachse im rechten Winkel zum Korpus, was die schwarze K-Achse im BS30-Loch nicht alleine schafft.