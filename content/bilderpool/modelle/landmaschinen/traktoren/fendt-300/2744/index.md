---
layout: "image"
title: "Fendt300-79"
date: "2004-11-01T10:20:22"
picture: "Fendt300-79.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Traktor", "Monsterreifen", "Großreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2744
- /details6638.html
imported:
- "2019"
_4images_image_id: "2744"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2744 -->
Das Innenleben der Monsterreifen. 

Man kann das ft-Speichenrad (z.B. aus der Classic-Serie mit den Dampfmaschinen) auf einer zusätzlichen Nabe einsetzen --- so man denn hat.

Die Füllung besteht aus hartem Schaumstoff. Die "Schlange" rechts neben dem Reifen sieht man in Schwimmbädern, damit lernen kleine Kinder die Angst vor dem Wasser zu verlieren. Mit einem scharfen Messer ist das Material leicht zu bearbeiten.

Für den Reifen habe ich einen Streifen ca. 2,5 cm breit, 2 cm hoch und etwa 15 cm lang ausgeschnitten (die Werte sind aus dem Gedächtnis, ich möchte den Reifen nicht gern zerlegen). Dann wird die eine Seite flach abgerundet und von der anderen Seite werden viele kleine Keile (Dreieckprofile)1 ausgeschnitten. Durch die Rundung paßt sich die äußere Seite besser in den Reifen ein, durch die ausgeschnittenen Keile läßt sich der Schaumstoff besser in Kreisform biegen.