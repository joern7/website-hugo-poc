---
layout: "image"
title: "Allradlenkung02.JPG"
date: "2007-09-25T19:01:57"
picture: "Allradlenkung02.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12010
- /details3bf1-2.html
imported:
- "2019"
_4images_image_id: "12010"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2007-09-25T19:01:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12010 -->
Ansicht von oben.

Die Lenkgeometrie stimmt noch nicht (bei Geradeausfahrt "schielen" die Räder), aber das wird noch. 

Die Teile mit den zwei schwarzen Zapfen sind das, was übrig bleibt, wenn ein BS15 mit Doppelzapfen in die ewigen Jagdgründe eingeht.