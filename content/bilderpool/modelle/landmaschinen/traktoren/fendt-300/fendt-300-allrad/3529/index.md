---
layout: "image"
title: "Fendt300all08.JPG"
date: "2005-01-15T12:30:18"
picture: "Fendt300all08.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Allrad", "Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3529
- /details945b.html
imported:
- "2019"
_4images_image_id: "3529"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T12:30:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3529 -->
Er wär so schön gewesen -- es hat nicht sollen sein. 

Die Lenkung ist nicht fertig (zwischen der Schnecke auf der linken Traktorseite und der Spurstange ist noch nichts), und nachdem der Antrieb nur zum Schön-Aussehen, aber nicht zum Antreiben taugt,  wird es auch dabei bleiben.