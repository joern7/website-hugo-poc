---
layout: "comment"
hidden: true
title: "404"
date: "2005-01-15T17:07:58"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Und die Kraftübertragung könnte auch noch werdenmit
- zwei Gelenksteinen mit 4mm-Durchgang (eins oben, eins unten),
- einer Metallachse 50 durch beide hindurch,
- und als mittlerem Kegelzahnrad eins von der Sorte, die sonst in einem schwarzen Differenzial mitten drin sitzt und lose mitläuft
sieht die Welt schon etwas freundlicher aus :-))))

Der Aufbau außen drum herum verzieht sich noch etwas, aber für nicht zu harte Ansprüche tut es das!