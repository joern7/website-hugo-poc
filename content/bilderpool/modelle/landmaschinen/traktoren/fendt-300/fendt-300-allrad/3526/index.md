---
layout: "image"
title: "Allrad04.JPG"
date: "2005-01-15T12:30:18"
picture: "Allrad04.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3526
- /details5dcf.html
imported:
- "2019"
_4images_image_id: "3526"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T12:30:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3526 -->
Wie im Bild Allrad01.jpg, aber etwas schmaler gebaut, so dass der Antrieb zur Spurweite des Fendt passt.