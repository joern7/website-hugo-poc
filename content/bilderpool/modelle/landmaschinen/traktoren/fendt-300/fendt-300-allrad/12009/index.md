---
layout: "image"
title: "Allradlenkung01.JPG"
date: "2007-09-25T19:00:03"
picture: "Allradlenkung01.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12009
- /details68df.html
imported:
- "2019"
_4images_image_id: "12009"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2007-09-25T19:00:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12009 -->
Ansicht von unten.

Hier ist ein neuer Anlauf, um die Lenkung für den Allrad hinzukriegen. Der Schlüssel ist das "Eigenbau"-Hubgetriebe mit der "Führungsplatte" 32455
(--> http://www.ftcommunity.de/details.php?image_id=6754 ), das hier quer vor den Antrieb montiert ist.