---
layout: "image"
title: "Fendt300-01"
date: "2004-11-01T10:20:22"
picture: "Fendt300-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2720
- /detailsa138.html
imported:
- "2019"
_4images_image_id: "2720"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2720 -->
Die Kabel und P-Schläuche fehlen noch, der Rest dürfte wohl passen.