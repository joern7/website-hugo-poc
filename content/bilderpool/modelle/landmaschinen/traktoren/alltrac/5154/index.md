---
layout: "image"
title: "AllTrac 4"
date: "2005-10-30T16:57:12"
picture: "AllTrac_04.jpg"
weight: "4"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5154
- /details837e-2.html
imported:
- "2019"
_4images_image_id: "5154"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5154 -->
