---
layout: "image"
title: "AllTrac 3"
date: "2005-10-30T16:57:11"
picture: "AllTrac_03.jpg"
weight: "3"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5153
- /details7574.html
imported:
- "2019"
_4images_image_id: "5153"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5153 -->
Frontansicht mit einem Teil der vorderen Anbaumöglichkeit (Fronthydraulik beim echten Trecker)