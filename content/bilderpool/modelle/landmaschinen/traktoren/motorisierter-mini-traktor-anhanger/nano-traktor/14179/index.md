---
layout: "image"
title: "Größenvergleich Mini-Traktor und Nano-Traktor nebeneinander"
date: "2008-04-04T21:55:04"
picture: "mini_und_minimini_02.jpg"
weight: "5"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14179
- /detailsc12f.html
imported:
- "2019"
_4images_image_id: "14179"
_4images_cat_id: "1312"
_4images_user_id: "327"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14179 -->
