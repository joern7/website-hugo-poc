---
layout: "overview"
title: "Motorisierter Mini-Traktor mit Anhänger"
date: 2020-02-22T08:25:38+01:00
legacy_id:
- /php/categories/1311
- /categories183b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1311 --> 
Hallo,

nachdem ich ja normalerweise lieber Fahrwerksprototypenkomponenten baue, habe ich mich mal dazu entschlossen, ein komplettes Modell bis zu Ende zu bringen ;-)

Wieder einmal kommt meine Vorliebe zum kompakten Bau mit FT zur Geltung.