---
layout: "image"
title: "ft reloaded"
date: "2014-12-19T17:19:28"
picture: "20141218-Fischertechnik.jpg"
weight: "8"
konstrukteure: 
- "Markus Thomas"
fotografen:
- "Markus Thomas"
keywords: ["Tracktor", "Anhänger"]
uploadBy: "Spieltrieb"
license: "unknown"
legacy_id:
- /php/details/39945
- /details971e.html
imported:
- "2019"
_4images_image_id: "39945"
_4images_cat_id: "605"
_4images_user_id: "2322"
_4images_image_date: "2014-12-19T17:19:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39945 -->
Nun ja, der Konstrukteur bin ich nicht, aber selbst gebaut! Mein erstes Modell seid ca. 30 Jahren. Und ich glaub es ist ein Guter Anfang erstmal di Ideen aus den Anleitungen aufzugreifen. Jetzt habe ich erstmal alles eingepackt und werd es für mich unter den Baum legen. Und dann geht los!