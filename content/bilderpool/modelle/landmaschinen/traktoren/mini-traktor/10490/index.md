---
layout: "image"
title: "Mini-Traktor 7"
date: "2007-05-23T13:17:38"
picture: "Traktor_7.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10490
- /detailsc50d.html
imported:
- "2019"
_4images_image_id: "10490"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-23T13:17:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10490 -->
Hier das Fahrgestell mit Antrieb und Lenkung ohne den ganzen Optik-Firlefanz. Sieht simpel aus, war aber trotzdem ganz schön viel Arbeit...