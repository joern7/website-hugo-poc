---
layout: "image"
title: "Mini-Traktor 4"
date: "2007-05-22T21:04:55"
picture: "Traktor_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10487
- /detailsc284-3.html
imported:
- "2019"
_4images_image_id: "10487"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-22T21:04:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10487 -->
Der Fahr-Motor guckt leider auch etwas hinten raus...