---
layout: "image"
title: "Mini-Traktor 6"
date: "2007-05-23T13:17:38"
picture: "Traktor_6.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10489
- /details72d7-2.html
imported:
- "2019"
_4images_image_id: "10489"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-23T13:17:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10489 -->
Hier ist nun zum ersten mal die Stromversorgung zu sehen. Der Deckel vom Batteriekasten hat aber nicht mehr reingepasst, das die Batterie schon am Getriebe anliegt. Geht aber auch ohne... ;o)