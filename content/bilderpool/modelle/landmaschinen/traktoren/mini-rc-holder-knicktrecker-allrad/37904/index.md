---
layout: "image"
title: "Mit Rad ab"
date: "2013-12-08T15:52:06"
picture: "D70_4471.jpg"
weight: "8"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37904
- /detailsa25d.html
imported:
- "2019"
_4images_image_id: "37904"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T15:52:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37904 -->
Zum Nachbauen in einer genaueren Aufnahme. Nota bene ist die Servoseite am Batteriefach aufgehängt - das Servo schiebt man so weit hoch, daß es am Batteriefach hängenbleibt. Auf der anderen Seite hängt es direkt am Motorgetriebe.