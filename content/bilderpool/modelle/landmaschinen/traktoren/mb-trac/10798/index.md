---
layout: "image"
title: "MB-Truck 4"
date: "2007-06-10T20:09:31"
picture: "mbtruck05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10798
- /details78dd-2.html
imported:
- "2019"
_4images_image_id: "10798"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10798 -->
