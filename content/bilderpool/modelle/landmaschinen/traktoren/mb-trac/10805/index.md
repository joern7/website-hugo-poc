---
layout: "image"
title: "MB-Truck 11"
date: "2007-06-10T20:09:31"
picture: "mbtruck12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10805
- /details9b77.html
imported:
- "2019"
_4images_image_id: "10805"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10805 -->
