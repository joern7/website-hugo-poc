---
layout: "image"
title: "MB-Truck 15"
date: "2007-06-20T17:16:08"
picture: "mbtruck2.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10900
- /detailseb63.html
imported:
- "2019"
_4images_image_id: "10900"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10900 -->
