---
layout: "image"
title: "MB-Truck 19"
date: "2007-06-20T17:16:08"
picture: "mbtruck6.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10904
- /detailsd34f-2.html
imported:
- "2019"
_4images_image_id: "10904"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10904 -->
