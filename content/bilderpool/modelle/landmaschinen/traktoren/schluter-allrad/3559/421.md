---
layout: "comment"
hidden: true
title: "421"
date: "2005-02-13T17:34:14"
uploadBy:
- "Claus"
license: "unknown"
imported:
- "2019"
---
StabilitätMit neuen Teilen ist es schon stabil. Aber da ich gern auf Nummer sicher gehe wurde bei einigen der roten Teile mit Sekundenkleber nachgeholfen.

Damit die Räder auch bei dem hohen Gewicht des Trecker grade stehen habe ich darüber hinaus zwischen zwei rote Teile der Achsaufhängung auf jeder Seite ein Stück Kabel eingeklemmt (auf den Bildern nicht sichtbar). Die so erreichte leichte Spreizung der Teile reicht genau aus damit die Räder gerade stehen. Die Verwendung eines 7,5 Grad Baustein wäre zu viel gewesen.