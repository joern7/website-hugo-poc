---
layout: "image"
title: "Mähwerk"
date: "2007-06-04T15:48:00"
picture: "Traktor20.jpg"
weight: "20"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10690
- /detailsa2cc-2.html
imported:
- "2019"
_4images_image_id: "10690"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10690 -->
Man sieht das Mähwerk. Es sind 2 Cuttermesser an der Drehscheibe befestigt. Es wird per pneumatischer Heckhydraulik gehoben und gesenkt und an der Zapfwelle angetrieben.