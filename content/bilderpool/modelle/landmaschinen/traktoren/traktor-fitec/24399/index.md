---
layout: "image"
title: "Antrieb"
date: "2009-06-16T17:17:05"
picture: "traktor3.jpg"
weight: "69"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24399
- /details1e4d.html
imported:
- "2019"
_4images_image_id: "24399"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24399 -->
Hier sieht man erst mal das Mitteldifferenzial. Das wird von einem 50:1 P-Mot angetrieben, zusätzlich aber noch mal 2:1 untersetzt.
Das heckdifferential ist seitlich versetzt eingebaut.