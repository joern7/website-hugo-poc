---
layout: "image"
title: "Lenkung"
date: "2007-07-15T17:49:00"
picture: "Traktor27.jpg"
weight: "27"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11080
- /details4ab7.html
imported:
- "2019"
_4images_image_id: "11080"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11080 -->
Hier sind die Zylinder an Gelenken festgemacht. Jetzt kann man richtig lenken.