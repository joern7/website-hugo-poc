---
layout: "image"
title: "Antrieb"
date: "2007-05-20T18:53:06"
picture: "Traktor8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10480
- /details69ff.html
imported:
- "2019"
_4images_image_id: "10480"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-20T18:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10480 -->
Hier sieht man etwas vom Antrieb. der 50:1 P-mot wurde nicht weiter untersetzt, sondern geht mit einem Z20 auf's Zentraldifferenzial.