---
layout: "image"
title: "Traktor"
date: "2007-05-20T18:53:05"
picture: "Traktor1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10473
- /details3994-2.html
imported:
- "2019"
_4images_image_id: "10473"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-20T18:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10473 -->
Das ist mein Traktor. Er besitzt Allrad, Achsschenkellenkung, Pendelachse, Hebemachanik mit Pneumatikzylindern, Magnetventile, Kompressor und Abschaltung für den Kompressor. Die Hebmechanik funktioniert mit den Magnetventilen, welche an nur einem Ausgang (M3) vom Empfänger angeschlossen sind. Das geht mit 2 Dioden.Die Hebemchanik funktioniert, er fährt auch gut. Ich konnte ihn bis jetzt aber nur auf teppichboden testen, weil mein Accu ist gerade kaputt und ich habe 9hn dann mit tim-techs Accu testen müssen. Auf Rasen und Erde ging es leider noch nicht, weil tim-tech dann nach Hause musste. Ihr werdet von mir hören, wenn ich das getestet habe.