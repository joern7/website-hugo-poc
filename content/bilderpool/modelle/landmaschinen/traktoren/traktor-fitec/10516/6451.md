---
layout: "comment"
hidden: true
title: "6451"
date: "2008-05-07T19:36:30"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Fitec!

Ich habe da eine kleine Frage:
Welche Achse hast Du zwischen Differential und Kardangelenk eingebaut?
(Eine 30er Rastachse ist zu kurz, eine 45er Rastachse viel zu lang; eigentlich bräuchte man eine 35er Rastachse.)

Mir ist bewußt, daß Du das Bild vor einem Jahr eingestellt hast, hoffe aber dennoch noch auf eine Antwort.

Viele Grüße

Mirose