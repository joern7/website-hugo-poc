---
layout: "image"
title: "Neuer Frontantrieb"
date: "2007-05-27T18:23:23"
picture: "Traktor18.jpg"
weight: "18"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10516
- /details0e97.html
imported:
- "2019"
_4images_image_id: "10516"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-27T18:23:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10516 -->
Dieser neue Antrieb hat etwas mehr Bodenfreiheit.