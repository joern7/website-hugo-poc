---
layout: "image"
title: "Hydraulik"
date: "2007-07-21T14:04:57"
picture: "Traktor36.jpg"
weight: "36"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11181
- /details223b.html
imported:
- "2019"
_4images_image_id: "11181"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:04:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11181 -->
Ich habe mal das Führerhaus weggemacht, damit man besser die Hydraulik sehen kann. So ist sie hochgefahren.