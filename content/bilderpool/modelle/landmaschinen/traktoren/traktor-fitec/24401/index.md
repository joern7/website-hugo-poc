---
layout: "image"
title: "Hydraulik"
date: "2009-06-16T17:17:05"
picture: "traktor5.jpg"
weight: "71"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24401
- /details465b.html
imported:
- "2019"
_4images_image_id: "24401"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24401 -->
Hydraulik, Anhängerkupplung und Zapfwelle (rechts).