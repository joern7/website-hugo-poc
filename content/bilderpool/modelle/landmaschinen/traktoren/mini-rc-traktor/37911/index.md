---
layout: "image"
title: "MicroRC-Trac 6"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac6.jpg"
weight: "6"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37911
- /detailsc615-2.html
imported:
- "2019"
_4images_image_id: "37911"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37911 -->
Unteransicht. Der 5mm Baustein sichert des Getriebe, macht aber etwas schlechtere Bodenfreiheit. Aber so gibts keinen Stress beim Spielen. ("Papa, der fährt nicht mehr...").