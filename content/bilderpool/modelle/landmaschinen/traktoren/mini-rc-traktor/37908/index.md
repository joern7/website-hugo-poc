---
layout: "image"
title: "MicroRC-Trac3"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac3.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37908
- /detailscbe2.html
imported:
- "2019"
_4images_image_id: "37908"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37908 -->
Vorne ist er recht schlank. Wenn die Batterie nicht nötig wäre, könnte man sich auch vorne noch einiges Vorstellen...