---
layout: "comment"
hidden: true
title: "18553"
date: "2014-01-09T12:26:31"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Sehr hübsches Modell! Ich mag den minimalistischen Ansatz. Und wenn dann noch die Proportionen stimmen - super!

Gruß, Thomas