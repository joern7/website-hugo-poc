---
layout: "image"
title: "Mini-RC-Traktor V2 2"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv02.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37914
- /detailsa466.html
imported:
- "2019"
_4images_image_id: "37914"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37914 -->
Hat nun eher Ähnlichkeiten zu alten Kramer-Traktoren?