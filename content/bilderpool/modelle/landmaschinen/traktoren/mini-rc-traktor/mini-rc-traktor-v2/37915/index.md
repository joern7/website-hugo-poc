---
layout: "image"
title: "Mini-RC-Traktor V2 3"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv03.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37915
- /detailsb4ce.html
imported:
- "2019"
_4images_image_id: "37915"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37915 -->
Ist recht robust und geländegängig - zieht aber leicht auf eine Seite durch den Einzelradantrieb. Ist aber nicht weiters schlimm.