---
layout: "image"
title: "Mini-RC-Traktor V2 12"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv12.jpg"
weight: "12"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37924
- /detailsb561-2.html
imported:
- "2019"
_4images_image_id: "37924"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37924 -->
Aufgebockt rechts. Antriebsseite.