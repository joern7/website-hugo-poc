---
layout: "overview"
title: "Mini-RC-Traktor V2"
date: 2020-02-22T08:25:46+01:00
legacy_id:
- /php/categories/2820
- /categories1fd5.html
- /categories0ebc.html
- /categoriesd562.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2820 --> 
Kaum online, schon gibts ein update: jetzt ist nur ein Rad angetrieben. Damit ist der Trecker langsamer (40km/h => old times 20km/h), aber er geht sehr willig um die Kurven. Auch sind jetzt die Schutzbleche besser montiert. Sonst ist vieles beim Alten. Schaumermal, was noch geht.