---
layout: "image"
title: "MicroRC-Trac 2"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37907
- /detailsebb7.html
imported:
- "2019"
_4images_image_id: "37907"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37907 -->
Sorry wieder für den Kabelwirrwar. Die beiden obersten Bausteine dienen als quick&dirty Kabelwicklungsbaum. Der Akku klemmt ganz natürlich zwischen den beiden roten Bausteinen.