---
layout: "image"
title: "MicroRC-Trac 7"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac7.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37912
- /detailsfbac.html
imported:
- "2019"
_4images_image_id: "37912"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37912 -->
Ohne Räder, damit ihr das Teil besser nachbauen könnt. Baut sich eigentlich recht stabil und vergleichsweise "auf Raster". Gut zu sehen der senkrechte Einbau von Empfänger und Motor.