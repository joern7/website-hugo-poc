---
layout: "comment"
hidden: true
title: "21661"
date: "2016-02-09T20:28:11"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Was ich vergessen hatte zu schreiben: das Rastkegelrad oben rechts im Bild sitzt auch in einem Kugellager 8x12, mit einem Messingröhrchen dazwischen.

Der Wagenheber ist nichts besonderes: ein ganz normaler BS5*15*30, der zufällig genau von der schmalen Seite geknipst wurde.

Gruß,
Harald