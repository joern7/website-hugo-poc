---
layout: "image"
title: "Im Gelände"
date: "2015-08-05T21:06:56"
picture: "gtamod03.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41717
- /detailsd945.html
imported:
- "2019"
_4images_image_id: "41717"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41717 -->
Der Allradantrieb ist geländegängig. Dafür sorgt das Zentralgelenk, das hier als durchgehende Achse ausgebildet ist und zugleich als Zapfwelle dient. Diese Achse ist hier nicht zu sehen.

Das mit der Zapfwelle ist aber so eine Sache: sie dreht schwer, weil sie ständig auf Biegung beansprucht wird, und sie liegt so tief, dass ein Kardangelenk Ärger mit der Bodenfreiheit bekommt.

Die Box auf der rechten Dachseite ist rund herum mit Gummikleber abgedichtet und hat an der Vorderseite einen P-Anschluss, damit sie als Vorratsbehälter dienen kann.