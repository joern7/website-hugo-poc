---
layout: "image"
title: "Mk3: Stretch-Traktor"
date: "2016-02-08T00:03:38"
picture: "gtamk6.jpg"
weight: "19"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42864
- /detailsc8ec.html
imported:
- "2019"
_4images_image_id: "42864"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42864 -->
Beim Urvater der Fendt-Geräteträger konnte man durch Umstecken der Vorderachse den Radstand verändern, z.B. um Platz für den Zwischenachsanbau von Arbeitsgeräten zu schaffen (etwa: Egge vor der Vorderachse, Kartoffel-Legemaschine zwischen den Achsen, Scheibensech am Heck). Die Lenkstange musste dann natürlich auch umgesteckt werden. Beim GTA380 Mk3 geht das auch: Lochstreben 120 in anderen Löchern einstecken, Länge der Antriebsachse anpassen, fertig.