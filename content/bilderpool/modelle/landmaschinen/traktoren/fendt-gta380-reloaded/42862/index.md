---
layout: "image"
title: "Mk3: Untersetzung ganz weit hinten"
date: "2016-02-08T00:03:38"
picture: "gtamk4.jpg"
weight: "17"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42862
- /details0573.html
imported:
- "2019"
_4images_image_id: "42862"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42862 -->
Die Stahlachse im Reifen kommt da hin, wo jetzt die K-Achse steckt. Sie läuft im Fahrzeug leer; das Z10 mit Messingbund sitzt nur lose drauf. Der Antrieb vom Hinterachsdifferenzial kommt vom rechten (vorderen) Z10, und führt auf das Z30-Innenzahnrad im Reifen. Das mittlere Z10 dient zum Stabilisieren, das hintere ... ist überflüssig.

Das antreibende Z10 steckt mit seinem Schaft im Kugellager. Das ist möglich, weil das Kugellager eines mit 8 mm Innendurchmesser (und 12 mm außen) ist. Der Schaft des Z10 hat 7 mm Durchmesser, und dazwischen ist noch ein Stückchen Messingrohr hinein "geflickt". 

Der hintere Kraftheber ist ziemlich umständlich aufgebaut und ist noch nicht der Weisheit letzter Schluss. Der Antriebsmotor liegt auf dem IR-Empfänger, mit dem Stufengetriebe nach vorn (rechts oben) zeigend. Von dort führt die schwarze Achse oberhalb der Schnecke m1 nach hinten zu einem Z10, das hier von den Ketten verdeckt wird. Die quer verlaufende Kette treibt die beiden Schnecken m1 an, die dann (mangels geeigneter Streben) über Kettenzüge die Oberlenker des Krafthebers nach oben ziehen.