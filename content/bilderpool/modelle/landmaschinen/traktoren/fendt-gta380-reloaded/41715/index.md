---
layout: "image"
title: "Totale"
date: "2015-08-05T21:06:56"
picture: "gtamod01.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41715
- /detailse1e5.html
imported:
- "2019"
_4images_image_id: "41715"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41715 -->
Der Geräteträger aus 2003 ist jetzt auf Allradantrieb aufgerüstet. Auch sonst ist kaum etwas unverändert geblieben.

Die beiden großen Zylinder sind Eigenbauten aus 2004. Die vorderen Felgen haben Innenverzahnung mit 26 Zähnen im Modul 1 und werden über ein Z10 (mittels scharfem Messer aus der Rastkurbel 35071 entstanden) angetrieben.

Zur Eigenbau-Felge gibt es hier mehr: http://ftcommunity.de/details.php?image_id=40964