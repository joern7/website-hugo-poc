---
layout: "image"
title: "Blick unters Dach"
date: "2015-08-05T21:06:56"
picture: "gtamod09.jpg"
weight: "10"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41723
- /details5179.html
imported:
- "2019"
_4images_image_id: "41723"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41723 -->
Die Kabine ist nach vorne abgeklappt und erlaubt jetzt einen Blick unters Dach. Dort ist die gesamte Pneumatik untergebracht, mit Kompressor, Überdruck-Abschaltung, Vorratsbehälter und zwei Handventilen für die Zylinder im Frontlader. Der Verteiler mit 5 Anschlüssen ist selbstgemacht.

Die zwei Mini-Taster in der Mitte schalten den Kompressor und das Licht. Sie werden durch Herausziehen der Achskupplungen am oberen Bildrand betätigt.

Die kleinen "BSB Spur N Grundplatte" 36093 aus der BSB (die hier die beiden Kotflügel halten) waren mir schon vor dem FanClub-Tag aufgefallen :-)