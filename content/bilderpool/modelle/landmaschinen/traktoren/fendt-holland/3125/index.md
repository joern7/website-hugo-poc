---
layout: "image"
title: "Case-International"
date: "2004-11-15T11:18:20"
picture: "Fischertechnik-modellen-Fendt_033.jpg"
weight: "31"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/3125
- /details1e90-2.html
imported:
- "2019"
_4images_image_id: "3125"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-15T11:18:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3125 -->
