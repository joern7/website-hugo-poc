---
layout: "image"
title: "Case-International"
date: "2004-11-14T21:52:45"
picture: "Fischertechnik-modellen-Fendt_027.jpg"
weight: "25"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/3119
- /details335d-2.html
imported:
- "2019"
_4images_image_id: "3119"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-14T21:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3119 -->
