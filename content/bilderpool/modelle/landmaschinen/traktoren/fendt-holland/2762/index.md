---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-31T21:19:47"
picture: "Fischertechnik-modellen-Fendt_018.jpg"
weight: "16"
konstrukteure: 
- "peterholland"
fotografen:
- "peterholland"
keywords: ["Großreifen", "Monsterreifen"]
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/2762
- /detailsd82a.html
imported:
- "2019"
_4images_image_id: "2762"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-31T21:19:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2762 -->
Fendt-Holland weiter in Anbau....