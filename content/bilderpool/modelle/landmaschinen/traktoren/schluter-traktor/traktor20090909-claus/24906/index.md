---
layout: "image"
title: "[3/5] Unter der Motorhaube"
date: "2009-09-10T21:27:15"
picture: "traktorclaus3.jpg"
weight: "3"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24906
- /detailsb637.html
imported:
- "2019"
_4images_image_id: "24906"
_4images_cat_id: "1716"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24906 -->
