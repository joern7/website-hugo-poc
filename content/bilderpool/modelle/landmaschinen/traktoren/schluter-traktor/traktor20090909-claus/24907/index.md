---
layout: "image"
title: "[4/5] Unteransicht"
date: "2009-09-10T21:27:16"
picture: "traktorclaus4.jpg"
weight: "4"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24907
- /details4f9a-2.html
imported:
- "2019"
_4images_image_id: "24907"
_4images_cat_id: "1716"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24907 -->
