---
layout: "image"
title: "Fendt380GTA07.jpg"
date: "2015-07-19T20:44:34"
picture: "P1010040mit.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41457
- /detailsac75.html
imported:
- "2019"
_4images_image_id: "41457"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:44:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41457 -->
Die Lenkung, solo. Die Lenkhebel-Schnitzereien stammen von früheren Taten; der Winkel 30° links unten muss in den Lenkwürfel hinein passen, auch wenn man zur anderen Seite lenkt. 
Die Konstruktion mit den gestapelten 7,5mm-Stöpsel-Dingsda-Teilen ist ziemlich wackelig.