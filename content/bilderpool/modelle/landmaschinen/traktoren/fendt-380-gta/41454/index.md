---
layout: "image"
title: "Fendt380GTA04.jpg"
date: "2015-07-19T20:36:27"
picture: "P1110121mit.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41454
- /detailse08f.html
imported:
- "2019"
_4images_image_id: "41454"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:36:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41454 -->
Das schwarze Differenzial geht mit Untersetzung 1:4 auf die Hinterachse. Der Abstand bis zum Z10 auf dem Motor wird mit einem freilaufenden Z15 überbrückt.