---
layout: "image"
title: "Fendt380GTA10.jpg"
date: "2015-07-19T20:51:23"
picture: "PA030055mit.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41460
- /details8885.html
imported:
- "2019"
_4images_image_id: "41460"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:51:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41460 -->
Die Fronthydraulik mit einem einzelnen kurzen Zylinder ist nur für leichtere Anbaugeräte gedacht.