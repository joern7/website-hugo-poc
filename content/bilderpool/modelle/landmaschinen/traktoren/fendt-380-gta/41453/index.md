---
layout: "image"
title: "Fendt380GTA03.jpg"
date: "2015-07-19T20:33:38"
picture: "P1050114mit.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41453
- /detailsaec2.html
imported:
- "2019"
_4images_image_id: "41453"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:33:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41453 -->
Die gelbe Box dient als Dummy für den Akku, den ich seinerzeit noch nicht besaß. Die Zylinder im Heck sind Eigenbauten.