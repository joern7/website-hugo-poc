---
layout: "image"
title: "Fendt380GTA09.jpg"
date: "2015-07-19T20:50:01"
picture: "P1110127mit.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41459
- /details712b.html
imported:
- "2019"
_4images_image_id: "41459"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:50:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41459 -->
Unter dem Dach werkelt ein Eigenbau-Kompressor (Details hier: http://ftcommunity.de/categories.php?cat_id=18) neben dem Akku-Dummy und der Überdruck-Abschaltung mit P-Betätiger.