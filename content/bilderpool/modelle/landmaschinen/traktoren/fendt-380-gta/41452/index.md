---
layout: "image"
title: "Fendt380GTA02.jpg"
date: "2015-07-19T20:31:48"
picture: "P1050112mit.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41452
- /details91dd.html
imported:
- "2019"
_4images_image_id: "41452"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:31:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41452 -->
Der quer liegende Power-Motor treibt die Hinterachse an. Ihm gegenüber und unten drunter liegt der Antrieb für die Zapfwelle. Vorn und hinten gibt es pneumatische Kraftheber; die weiteren Anschlüsse sind für den Frontlader vorgesehen.