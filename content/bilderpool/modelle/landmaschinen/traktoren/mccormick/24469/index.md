---
layout: "image"
title: "Von der Seite"
date: "2009-06-29T23:27:02"
picture: "mccormick2.jpg"
weight: "2"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24469
- /details20b8.html
imported:
- "2019"
_4images_image_id: "24469"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24469 -->
Hier sieht man den Antrieb den Empfänger und den Akku.