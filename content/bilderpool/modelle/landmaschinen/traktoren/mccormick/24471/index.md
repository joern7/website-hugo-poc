---
layout: "image"
title: "Vorne Mitte"
date: "2009-06-29T23:27:02"
picture: "mccormick4.jpg"
weight: "4"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24471
- /detailsd6bc.html
imported:
- "2019"
_4images_image_id: "24471"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24471 -->
Hier sieht man den Servo und die zwei Linsenlampen.