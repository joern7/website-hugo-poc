---
layout: "image"
title: "noch nen Trecker bei Tageslicht"
date: "2009-07-03T09:11:46"
picture: "Traktor03.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/24490
- /details57ed.html
imported:
- "2019"
_4images_image_id: "24490"
_4images_cat_id: "605"
_4images_user_id: "968"
_4images_image_date: "2009-07-03T09:11:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24490 -->
