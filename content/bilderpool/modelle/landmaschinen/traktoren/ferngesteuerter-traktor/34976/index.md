---
layout: "image"
title: "ferngesteuerter Traktor"
date: "2012-05-19T20:22:22"
picture: "ferngesteuertertraktor6.jpg"
weight: "6"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34976
- /details1ede.html
imported:
- "2019"
_4images_image_id: "34976"
_4images_cat_id: "2589"
_4images_user_id: "1476"
_4images_image_date: "2012-05-19T20:22:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34976 -->
