---
layout: "image"
title: "Tr30445-03.JPG"
date: "2004-02-14T14:54:58"
picture: "Tr30445-03.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2108
- /detailsfd05-2.html
imported:
- "2019"
_4images_image_id: "2108"
_4images_cat_id: "607"
_4images_user_id: "4"
_4images_image_date: "2004-02-14T14:54:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2108 -->
Der schräg durchbohrte Baustein 30 ist original ft. Die U-förmig gebogene Achse auch, obwohl das hier vorliegende Exemplar vermutlich eine Nachfertigung ist.