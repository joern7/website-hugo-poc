---
layout: "image"
title: "Traktor mit pneumatischem Heuballengreifer 2"
date: "2014-03-29T23:32:29"
picture: "109_5266.jpg"
weight: "5"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Nordmann"
license: "unknown"
legacy_id:
- /php/details/38518
- /detailsef3a.html
imported:
- "2019"
_4images_image_id: "38518"
_4images_cat_id: "605"
_4images_user_id: "2159"
_4images_image_date: "2014-03-29T23:32:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38518 -->
