---
layout: "image"
title: "Tractor-PM-fase04"
date: "2018-01-24T17:32:34"
picture: "tractorpm5.jpg"
weight: "15"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47187
- /details7bfb.html
imported:
- "2019"
_4images_image_id: "47187"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-24T17:32:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47187 -->
