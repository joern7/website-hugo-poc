---
layout: "image"
title: "Tractor-non-IR-fase-01-02"
date: "2018-01-23T16:33:17"
picture: "tractornonir02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47174
- /detailsa9f8.html
imported:
- "2019"
_4images_image_id: "47174"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-23T16:33:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47174 -->
In manche Bildbearbeitungsapps lassen sich die Bilder einfach kombinieren. Ich habe Photoshop benutzt und werde also nicht so viele Bilder brauchen wie vorher für meine Bauanleitungen. Und es hat den Vorteil das man beim Bauen in einen Bild sehen kann, wie man die neuen Teilen anbauen soll. Man lernt immer weiter.