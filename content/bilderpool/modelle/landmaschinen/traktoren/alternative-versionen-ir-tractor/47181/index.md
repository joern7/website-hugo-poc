---
layout: "image"
title: "Tractor-non-IR-fase-12"
date: "2018-01-23T16:33:17"
picture: "tractornonir09.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47181
- /detailse0f1.html
imported:
- "2019"
_4images_image_id: "47181"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-23T16:33:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47181 -->
Den Hebemechanismus ist genau das aus der ft-Kasten.