---
layout: "image"
title: "Combi mit Anhänger 1"
date: "2018-01-21T20:14:24"
picture: "combimitanhaenger1.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47162
- /detailse691.html
imported:
- "2019"
_4images_image_id: "47162"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47162 -->
Hier sieht man den Ernter mit ein zusätzlicher Tractor mit Anhänger. Den gelben Tractor wird von ein Powermotor 50:1 angetrieben (weil ich nur eine grauen Tractormotor besitze). Für's Übrige ist es genau dasselbe wie der Roten.