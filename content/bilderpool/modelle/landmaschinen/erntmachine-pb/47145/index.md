---
layout: "image"
title: "erntmachine5.jpg"
date: "2018-01-17T18:01:36"
picture: "erntmachine5.jpg"
weight: "5"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47145
- /details686e.html
imported:
- "2019"
_4images_image_id: "47145"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47145 -->
Den Oberseite des Auslegers. Hier keine Ritzel Z10, sondern 2 Seilrollen und am Untenseite 2 Hülsen 15mm.