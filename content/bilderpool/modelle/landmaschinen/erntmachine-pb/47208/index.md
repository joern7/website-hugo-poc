---
layout: "image"
title: "Ernter-fase05-06"
date: "2018-01-30T16:23:29"
picture: "ernter07.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47208
- /details8c2d.html
imported:
- "2019"
_4images_image_id: "47208"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47208 -->
