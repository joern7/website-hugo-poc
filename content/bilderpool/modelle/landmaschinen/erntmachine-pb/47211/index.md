---
layout: "image"
title: "Ernter-fase10-12"
date: "2018-01-30T16:23:29"
picture: "ernter10.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47211
- /details1045-2.html
imported:
- "2019"
_4images_image_id: "47211"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47211 -->
Konstruktion des Auslegers.