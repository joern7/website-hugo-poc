---
layout: "image"
title: "tim carlo maishexler"
date: "2010-11-07T21:56:25"
picture: "maishexler__01.jpg"
weight: "1"
konstrukteure: 
- "tim carlo"
fotografen:
- "tim carlo"
keywords: ["Mais", "hexler", "siku"]
uploadBy: "carlo"
license: "unknown"
legacy_id:
- /php/details/29202
- /details35f0.html
imported:
- "2019"
_4images_image_id: "29202"
_4images_cat_id: "2119"
_4images_user_id: "893"
_4images_image_date: "2010-11-07T21:56:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29202 -->
Maishexler nach Vorbild von John Deere und Siku-Modell