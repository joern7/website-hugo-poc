---
layout: "image"
title: "tim carlo maishexler"
date: "2010-11-07T21:56:26"
picture: "maishexler__47.jpg"
weight: "9"
konstrukteure: 
- "tim carlo"
fotografen:
- "tim carlo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "carlo"
license: "unknown"
legacy_id:
- /php/details/29210
- /detailse116.html
imported:
- "2019"
_4images_image_id: "29210"
_4images_cat_id: "2119"
_4images_user_id: "893"
_4images_image_date: "2010-11-07T21:56:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29210 -->
Maishexler nach Vorbild von John Deere und Siku-Modell