---
layout: "image"
title: "Waage belastet"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie5.jpg"
weight: "5"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47403
- /detailsb70c.html
imported:
- "2019"
_4images_image_id: "47403"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47403 -->
Der dunklere Probekörper befindet sich auf der linken Waagschale, sein helleres Gegenstück steht auf der rechten Waagschale. Wie man im Bild eindeutig sieht, neigt sich die Waage tatsächlich in Richtung des dunkleren Probestücks. Das bedeutet, dass dies tatsächlich eine etwas größere Masse als das hellere Probestück hat. Die Theorie scheint sich zu bestätigen.