---
layout: "image"
title: "Versuchsaufbau"
date: "2016-01-13T14:57:44"
picture: "vbse1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42721
- /detailsa9b9.html
imported:
- "2019"
_4images_image_id: "42721"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42721 -->
Der Versuchsaufbau sieht wie folgt aus:
- Taster zum Starten
- Elektromagnet zum Loslassen der Kugel
- Laserlichtschranke zum Berechnen der Verdunklungszeit
- Laserlichtschranke zum Messen der Zeitdauer für das Herabrollen der Kugel

Es soll die Beschleunigung und die Endgeschwindigkeit der Kugel bestimmt werden. Da es sich um eine schiefe Ebene handelt, ist die Beschleunigung konstant. Der Versuchsaufbau nutzt die Bewegungsgleichungen, um die Endgeschwindigkeit zu berechnen, ohne sehr kleine Zeiten messen zu müssen.