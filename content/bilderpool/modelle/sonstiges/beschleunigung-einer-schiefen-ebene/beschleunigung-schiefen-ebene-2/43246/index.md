---
layout: "image"
title: "Versuchsaufbau"
date: "2016-04-04T21:54:43"
picture: "badsema1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43246
- /details2084.html
imported:
- "2019"
_4images_image_id: "43246"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43246 -->
Versuchsaufbau, wie in ft:pedia 1/2016 beschrieben