---
layout: "image"
title: "Verdunklungszeit"
date: "2016-01-13T14:57:44"
picture: "vbse6.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42726
- /detailsf4d3.html
imported:
- "2019"
_4images_image_id: "42726"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42726 -->
Slo-Motion-Aufnahme der Kugel beim Verdunkeln der Lichtschranke