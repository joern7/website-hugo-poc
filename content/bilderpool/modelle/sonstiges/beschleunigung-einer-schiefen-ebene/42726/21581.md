---
layout: "comment"
hidden: true
title: "21581"
date: "2016-01-17T10:39:15"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ich verwende einen Laser von Laserfuchs: https://www.laserfuchs.de/de/categorie?page=categorie&cat=10&sorting=price
Der Typ, den ich verwende, gibt es mittlerweile nicht mehr zu kaufen. Man kann den Laser zwischen 3 und 12 Volt betreiben.

Die Stahlkugel streut das Licht vielmehr als dass sie es reflektiert. Da dies nur in einem sehr kurzen Zeitraum der Fall ist, wäre mir der Effekt ohne den Hinweis gar nicht aufgefallen. Aus diesem Grund halte ich eine Blende für nicht zwingend erforderlich.