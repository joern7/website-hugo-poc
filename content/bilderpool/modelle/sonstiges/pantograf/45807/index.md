---
layout: "image"
title: "Pantograf"
date: "2017-05-11T16:38:15"
picture: "Pantograf_klein.jpg"
weight: "1"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Pantograf", "Pantograph", "Pantagraph", "zeichnen", "vergrößern"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/45807
- /details05e6.html
imported:
- "2019"
_4images_image_id: "45807"
_4images_cat_id: "3406"
_4images_user_id: "2179"
_4images_image_date: "2017-05-11T16:38:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45807 -->
Dies ist ein recht altes Instrument mit dem man Zeichnungen vergrößern kann.
Meine Zeichnung wirkt ein wenig zittrig, da ich das Blatt Papier hätte am Boden fixieren müssen. So musste ich es beim Zeichnen festhalten, was zu kleinen motorischen Ungenauigkeiten führte. Und leider haben die Gelenke von ft sehr viel Spiel. Aber zur Demonstration ist dieser Pantograf exakt genug.