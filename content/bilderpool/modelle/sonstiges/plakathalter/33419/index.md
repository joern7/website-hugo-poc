---
layout: "image"
title: "Plakathalter"
date: "2011-11-05T20:17:25"
picture: "coneb1.jpg"
weight: "1"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/33419
- /details70cd.html
imported:
- "2019"
_4images_image_id: "33419"
_4images_cat_id: "2476"
_4images_user_id: "430"
_4images_image_date: "2011-11-05T20:17:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33419 -->
Diese Fläche ist der Plakathalter, den man auf der Convention 2011 sehen konnte. Die Quadrate in den Ecken, sind alte Kartonagen, die mit Hilfe des Heißklebers mit der Platte verbunden wurden. Die Quadrate, bieten Angrifffläche für Tesa Powerstrips, so dass man perfekt ein Poster befestigen kann.