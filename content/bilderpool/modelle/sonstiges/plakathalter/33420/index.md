---
layout: "image"
title: "Plakathalter 'Mechanik'"
date: "2011-11-05T20:17:25"
picture: "coneb2.jpg"
weight: "2"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/33420
- /details72ca.html
imported:
- "2019"
_4images_image_id: "33420"
_4images_cat_id: "2476"
_4images_user_id: "430"
_4images_image_date: "2011-11-05T20:17:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33420 -->
Um den Transport zu erleichtern, hab ich mehrere Gelenkwürfel verbaut, damit man das Gestell zusammenklappen kann. Zusammengeklappt, ist es kaum höher als 60 mm!