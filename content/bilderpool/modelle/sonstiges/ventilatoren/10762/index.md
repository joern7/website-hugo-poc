---
layout: "image"
title: "ventilator"
date: "2007-06-09T14:59:05"
picture: "venti1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10762
- /detailsb098.html
imported:
- "2019"
_4images_image_id: "10762"
_4images_cat_id: "975"
_4images_user_id: "557"
_4images_image_date: "2007-06-09T14:59:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10762 -->
hier eines der Rotorenblätter. das papier ist normales druckerpapier