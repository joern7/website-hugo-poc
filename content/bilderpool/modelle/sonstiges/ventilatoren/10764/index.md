---
layout: "image"
title: "ventilator"
date: "2007-06-09T14:59:05"
picture: "venti3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10764
- /detailsb62f.html
imported:
- "2019"
_4images_image_id: "10764"
_4images_cat_id: "975"
_4images_user_id: "557"
_4images_image_date: "2007-06-09T14:59:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10764 -->
hier die befestigung des rotors