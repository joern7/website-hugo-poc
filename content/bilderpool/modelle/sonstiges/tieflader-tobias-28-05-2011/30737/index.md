---
layout: "image"
title: "Tieflader"
date: "2011-05-30T17:12:07"
picture: "tieflader1.jpg"
weight: "1"
konstrukteure: 
- "Tobias"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30737
- /detailsafe5.html
imported:
- "2019"
_4images_image_id: "30737"
_4images_cat_id: "2292"
_4images_user_id: "1162"
_4images_image_date: "2011-05-30T17:12:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30737 -->
Hier der Tieflader den mein Bruder gebaut hat. Naja ich hab für die Idee gesorgt ;)

MfG
Endlich