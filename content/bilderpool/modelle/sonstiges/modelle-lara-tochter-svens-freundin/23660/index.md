---
layout: "image"
title: "Kino"
date: "2009-04-10T19:57:04"
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "Lara"
fotografen:
- "Sven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/23660
- /details6840.html
imported:
- "2019"
_4images_image_id: "23660"
_4images_cat_id: "1615"
_4images_user_id: "1"
_4images_image_date: "2009-04-10T19:57:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23660 -->
Dieses kleine Modell soll ein Kino sein.
