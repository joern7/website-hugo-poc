---
layout: "image"
title: "Stativ1"
date: "2011-10-19T14:25:45"
picture: "Stativ1.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: ["Stativ", "Handystativ", "ft", "Fischertechnik"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33232
- /detailsa9a9.html
imported:
- "2019"
_4images_image_id: "33232"
_4images_cat_id: "2459"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T14:25:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33232 -->
Hallo,
hier seht Ihr das ganze Stativ. Das Handy wird zwischen die Statikteile geklemmt (bei Kleineren Handys muss man die Position dieser verändern!). Die Halterung wird durch eine Schnecke geneigt, eine Drehfunktion habe ich (noch) nicht eingebaut.

LG
Lukas