---
layout: "image"
title: "Schon wieder ein Goldenes Schaukelpferd fur fischertechnik!"
date: "2014-08-31T15:58:24"
picture: "dasgoldeneschaukelpferd2.jpg"
weight: "2"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/39326
- /detailsf153.html
imported:
- "2019"
_4images_image_id: "39326"
_4images_cat_id: "2944"
_4images_user_id: "162"
_4images_image_date: "2014-08-31T15:58:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39326 -->
