---
layout: "image"
title: "Kettenfertiger"
date: "2016-10-26T19:22:36"
picture: "kettenf2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44686
- /details1568.html
imported:
- "2019"
_4images_image_id: "44686"
_4images_cat_id: "3327"
_4images_user_id: "558"
_4images_image_date: "2016-10-26T19:22:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44686 -->
Das ganze lässt sich bestimmt auch automatisieren. Das ist ein erster Ansatz, evtl kommt da bald mehr :)