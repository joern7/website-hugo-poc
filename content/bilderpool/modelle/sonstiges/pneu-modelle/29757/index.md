---
layout: "image"
title: "Bagger 02"
date: "2011-01-22T19:45:39"
picture: "pneumodelle4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29757
- /details7150-2.html
imported:
- "2019"
_4images_image_id: "29757"
_4images_cat_id: "2188"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29757 -->
