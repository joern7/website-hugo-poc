---
layout: "image"
title: "Katapult 01"
date: "2011-01-22T19:45:39"
picture: "pneumodelle7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29760
- /details7090.html
imported:
- "2019"
_4images_image_id: "29760"
_4images_cat_id: "2188"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29760 -->
Wieder nach der Anleitung gebaut das Katapult