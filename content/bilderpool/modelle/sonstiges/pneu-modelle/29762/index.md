---
layout: "image"
title: "Dreh- Bearbeitungsstation"
date: "2011-01-22T19:45:39"
picture: "pneumodelle9.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29762
- /details1ae4.html
imported:
- "2019"
_4images_image_id: "29762"
_4images_cat_id: "2188"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29762 -->
