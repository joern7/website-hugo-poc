---
layout: "image"
title: "insel Hinten"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne09.jpg"
weight: "9"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11790
- /details9594.html
imported:
- "2019"
_4images_image_id: "11790"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11790 -->
Hier sieht man vor allem das IF. Links ist der Schalter für den Richtscheinwerfer.