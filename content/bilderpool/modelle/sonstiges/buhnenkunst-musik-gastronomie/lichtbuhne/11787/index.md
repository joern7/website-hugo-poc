---
layout: "image"
title: "Kalte/Dunkel Beluechtung"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne06.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11787
- /details1733.html
imported:
- "2019"
_4images_image_id: "11787"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11787 -->
Man sollte hier nicht zu viel Strom liefern...