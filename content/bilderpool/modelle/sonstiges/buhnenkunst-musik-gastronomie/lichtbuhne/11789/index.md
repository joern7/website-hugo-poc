---
layout: "image"
title: "LED Stern (Grün)"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne08.jpg"
weight: "8"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11789
- /detailsc94c.html
imported:
- "2019"
_4images_image_id: "11789"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11789 -->
Selbstbau!