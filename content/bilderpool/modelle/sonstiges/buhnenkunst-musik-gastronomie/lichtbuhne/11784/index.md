---
layout: "image"
title: "Kleiner bewegbarer Scheinwerfer"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne03.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11784
- /details49f4.html
imported:
- "2019"
_4images_image_id: "11784"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11784 -->
Das ist ebenfalls eine Linsenlampe, jedoch am IF.