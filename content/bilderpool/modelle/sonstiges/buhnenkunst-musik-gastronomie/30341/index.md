---
layout: "image"
title: "Sommer_37"
date: "2011-03-31T13:20:12"
picture: "Sommer_4837.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30341
- /details1af2.html
imported:
- "2019"
_4images_image_id: "30341"
_4images_cat_id: "2121"
_4images_user_id: "4"
_4images_image_date: "2011-03-31T13:20:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30341 -->
Ihr werdet's nicht erraten, wonach mir im Moment der Sinn steht!

(Und es wird allmählich Zeit, dass auch ft-Figuren in "W" (w wie weiblich) herausgebracht werden.)