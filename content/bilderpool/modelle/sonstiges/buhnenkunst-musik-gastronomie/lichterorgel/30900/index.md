---
layout: "image"
title: "Bauanleitung Nr.1"
date: "2011-06-23T20:23:55"
picture: "lichterorgel3.jpg"
weight: "3"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30900
- /details2862.html
imported:
- "2019"
_4images_image_id: "30900"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30900 -->
Hier werden die Leuchtsteine mit der Linsenlampe auf dem Baustein 15 montiert.