---
layout: "image"
title: "Bauanleitung Nr.5 Fertig!!"
date: "2011-06-23T20:23:55"
picture: "lichterorgel9.jpg"
weight: "9"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30906
- /details9c8d.html
imported:
- "2019"
_4images_image_id: "30906"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30906 -->
Wer eine ausführliche Teileliste haben möchte oder das Programm soll sich bitte an mich wenden.
Ich werde versuchen das Programm auf die FTC hochzuladen.
Viel Spaß beim Bauen

tobs9578