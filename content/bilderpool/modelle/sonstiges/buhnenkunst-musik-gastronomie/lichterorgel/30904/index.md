---
layout: "image"
title: "Bauanleitung Nr.3 Hintere Ansicht"
date: "2011-06-23T20:23:55"
picture: "lichterorgel7.jpg"
weight: "7"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30904
- /details3fe4.html
imported:
- "2019"
_4images_image_id: "30904"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30904 -->
Hier sind schön die 2 Scharniere mit den 60 Grad Bausteinen zu sehen.