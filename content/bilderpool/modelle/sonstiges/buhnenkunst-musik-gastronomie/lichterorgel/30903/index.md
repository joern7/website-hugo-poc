---
layout: "image"
title: "Bauanleitung Nr.3"
date: "2011-06-23T20:23:55"
picture: "lichterorgel6.jpg"
weight: "6"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30903
- /details30b3-2.html
imported:
- "2019"
_4images_image_id: "30903"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30903 -->
Hier werden die Rastleuchtkappen auf den Leuchtbausteinen angebracht.
Hinweis:Man kann die Farben beliebig kombenieren.