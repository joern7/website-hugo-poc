---
layout: "image"
title: "Baunanleitung Nr.4 Die Stüzen"
date: "2011-06-23T20:23:55"
picture: "lichterorgel8.jpg"
weight: "8"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30905
- /details191d.html
imported:
- "2019"
_4images_image_id: "30905"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30905 -->
