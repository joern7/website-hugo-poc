---
layout: "image"
title: "Bauanleitung Nr.2"
date: "2011-06-23T20:23:55"
picture: "lichterorgel5.jpg"
weight: "5"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30902
- /detailsa235.html
imported:
- "2019"
_4images_image_id: "30902"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30902 -->
Hier werden die 15er Bausteine durch die Statikträger verbunden.