---
layout: "image"
title: "Fischertechnik Vogelstimme"
date: "2013-03-16T10:56:51"
picture: "fiischertechnikvogelstimme1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/36764
- /details2097.html
imported:
- "2019"
_4images_image_id: "36764"
_4images_cat_id: "2726"
_4images_user_id: "968"
_4images_image_date: "2013-03-16T10:56:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36764 -->
Hier mein kleiner Vogelstimmenapparat.
Zu sehen und zu hören:

http://www.youtube.com/watch?v=H-umOpEyxfE