---
layout: "image"
title: "Schlagzeug"
date: "2007-03-06T18:08:46"
picture: "ftband07.jpg"
weight: "7"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9311
- /details7c9d.html
imported:
- "2019"
_4images_image_id: "9311"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9311 -->
Damit die Band im Rhytmus bleibt.