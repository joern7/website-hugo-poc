---
layout: "image"
title: "Snare"
date: "2007-03-06T18:15:49"
picture: "ftband13.jpg"
weight: "13"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9317
- /detailsa2cf.html
imported:
- "2019"
_4images_image_id: "9317"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:15:49"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9317 -->
Das ist rund und erst noch am "Stengel".