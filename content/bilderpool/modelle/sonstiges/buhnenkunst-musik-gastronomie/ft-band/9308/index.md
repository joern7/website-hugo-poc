---
layout: "image"
title: "Von hinten ohne Menschchen"
date: "2007-03-06T18:08:46"
picture: "ftband04.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9308
- /details46ea.html
imported:
- "2019"
_4images_image_id: "9308"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9308 -->
Der Notenhalter wird von zwei 15° Winkelsteinen in der richtigen Position gehalten.