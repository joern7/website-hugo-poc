---
layout: "image"
title: "E-Bassist/Gitarist"
date: "2007-03-06T18:08:46"
picture: "ftband02.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9306
- /detailsc35c.html
imported:
- "2019"
_4images_image_id: "9306"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9306 -->
Eine Gitarre aus einer Platte zwei Klipachshaltern (einer doppelachsaufnahme,einer mit Befestigung).