---
layout: "image"
title: "Piano (Seitenansicht)"
date: "2011-02-15T21:14:52"
picture: "piano2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/29988
- /detailsab87.html
imported:
- "2019"
_4images_image_id: "29988"
_4images_cat_id: "2211"
_4images_user_id: "1112"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29988 -->
