---
layout: "comment"
hidden: true
title: "13576"
date: "2011-02-16T07:39:59"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Wirklich großartig, sowohl optisch, als auch technisch! Dirk, die Töne gleich durch die Taster auszulösen wäre doch langweilig ... ;o)

@uhen: Kannst Du vielleicht mal ein Video machen und hochladen? Mich interessiert sehr die Spielbarkeit des Instruments und die Zeitverzögerung zwischen Anschlag und Klang (wir Musiker sagen dazu Latenz), danke!