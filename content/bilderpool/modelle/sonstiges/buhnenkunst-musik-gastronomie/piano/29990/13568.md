---
layout: "comment"
hidden: true
title: "13568"
date: "2011-02-15T21:22:45"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Modellidee ist grandios! Und wie ein Klavier sieht es ja auch fast aus. Aber vielleicht lässt sich mit der Tonerzeugung noch was machen. Wenn Du 80er-Jahre-Elektronik hast, hast Du vielleicht noch noch die alte Spiral-Antriebsfeder von ft? So um die 15 oder 20 cm lang? Die kann man prima als veränderlichen Widerstand verwenden, indem man an einem Ende und variabel irgendwo in der Mitte abgreift. Wenn Du nun so eine Spiralfeder quer durchs Klaiver spanntest und mit den Tasten Kontakte an den jeweiligen Punkten draufdrücken würdest - könnte man dann nicht erreichen, dass der jeweils gedrückte Ton sofort kommt?

Gruß,
Stefan