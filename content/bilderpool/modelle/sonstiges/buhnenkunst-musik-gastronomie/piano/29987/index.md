---
layout: "image"
title: "Piano (von vorne)"
date: "2011-02-15T21:14:52"
picture: "piano1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/29987
- /detailsc1de.html
imported:
- "2019"
_4images_image_id: "29987"
_4images_cat_id: "2211"
_4images_user_id: "1112"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29987 -->
Es hat 7 Tasten statt 8 (zu eng gebaut für die Taster, leider erst später gemerkt)