---
layout: "image"
title: "Unterwasser-Explorer"
date: "2018-09-09T17:59:27"
picture: "1536502603509.jpeg"
weight: "1"
konstrukteure: 
- "Reus' Tochter"
fotografen:
- "Reus' Tochter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- /php/details/47873
- /details496f-2.html
imported:
- "2019"
_4images_image_id: "47873"
_4images_cat_id: "3531"
_4images_user_id: "708"
_4images_image_date: "2018-09-09T17:59:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47873 -->
Unterwasserexplorer.

Meine Tochter findet die alten Figuren farblich viel cooler als die langweiligen neuen blauen.

Außerdem sind die durchsichtigen Platten toll um ein schönes Modell zu bauen (Bemerkung des Vaters an die Firma fischertechnik: Denkt daran, wenn ihr Kinder als Kunden gewinnen wollt!)