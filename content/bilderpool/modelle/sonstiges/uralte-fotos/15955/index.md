---
layout: "image"
title: "Blockhobel 1"
date: "2008-10-11T22:48:04"
picture: "OLD13.jpg"
weight: "13"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15955
- /details3b64.html
imported:
- "2019"
_4images_image_id: "15955"
_4images_cat_id: "1448"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15955 -->
Alter ca. 30 Jahre