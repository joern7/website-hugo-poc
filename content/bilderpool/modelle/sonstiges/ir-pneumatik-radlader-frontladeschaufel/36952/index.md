---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - UNTEN"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon2.jpg"
weight: "2"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36952
- /details0af9.html
imported:
- "2019"
_4images_image_id: "36952"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36952 -->
