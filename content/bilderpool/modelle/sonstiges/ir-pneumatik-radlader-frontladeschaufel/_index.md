---
layout: "overview"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle"
date: 2020-02-22T08:30:35+01:00
legacy_id:
- /php/categories/2747
- /categories97e9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2747 --> 
Der Pneumatik Traktor/Radlader ist der aus dem Pneumatik III Reihe.
Dieser hier wurde allderings mit pneumatischen Elektroventilen augfestattet und mit dem neuen IR Set ausgestattet.

Er belädt unseren Pneumatik Kipplaster und das IR Förderband für Kastanien.

LINK: http://www.youtube.com/watch?v=SF-P_u1B4_Q&feature=youtu.be