---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - OBEN"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon3.jpg"
weight: "3"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36953
- /details2fe2.html
imported:
- "2019"
_4images_image_id: "36953"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36953 -->
