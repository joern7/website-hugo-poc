---
layout: "image"
title: "auf kleinsten Räumen gehts voran . ."
date: "2015-04-12T13:29:03"
picture: "kak5.jpg"
weight: "5"
konstrukteure: 
- "Jonas Hannes Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40762
- /detailsc504.html
imported:
- "2019"
_4images_image_id: "40762"
_4images_cat_id: "3062"
_4images_user_id: "1359"
_4images_image_date: "2015-04-12T13:29:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40762 -->
auch ein Baustein-Breiter Gang ist kein Hinderniss ;-)
Allerdings muss die Höhe des Durchgangs minimal höher als 15 sein . die normale v1 kommt durch, die v2 braucht mehr Platz.

mal sehen, was sich für unsere neuen kleinen Freunde noch so bauen lässt ...
(to be continued....)