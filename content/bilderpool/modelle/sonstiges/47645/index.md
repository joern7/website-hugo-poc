---
layout: "image"
title: "FT Entscheidungsfinder"
date: "2018-05-12T10:30:23"
picture: "Fischertechnik_Entscheidungsfinder_-_1_of_7.jpg"
weight: "30"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47645
- /detailse4ec.html
imported:
- "2019"
_4images_image_id: "47645"
_4images_cat_id: "323"
_4images_user_id: "2739"
_4images_image_date: "2018-05-12T10:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47645 -->
Der Entscheidungsfinder hilft schwierige Entscheidungen zu treffen - einfach den "Greifer" mit der Kugelbahnkugel an der "richtigen" Position loslassen und abwarten.... 

Video:
https://youtu.be/jNNhwywdfwA