---
layout: "image"
title: "Tor 2 und die Wippe"
date: "2014-05-04T15:08:33"
picture: "IMG_0081.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Minigolf", "Kricket"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38742
- /detailsee71.html
imported:
- "2019"
_4images_image_id: "38742"
_4images_cat_id: "2894"
_4images_user_id: "1359"
_4images_image_date: "2014-05-04T15:08:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38742 -->
