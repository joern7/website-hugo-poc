---
layout: "image"
title: "(Update 05.10.10) Lenkrad mit Schaumstoffgriffen"
date: "2010-10-05T16:42:50"
picture: "IMG_9388.jpg"
weight: "8"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/28929
- /details6425.html
imported:
- "2019"
_4images_image_id: "28929"
_4images_cat_id: "2033"
_4images_user_id: "986"
_4images_image_date: "2010-10-05T16:42:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28929 -->
