---
layout: "image"
title: "Weitere Magnete in ihren Fassungen"
date: "2009-09-07T21:17:19"
picture: "sarja6.jpg"
weight: "6"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24890
- /details9f52-2.html
imported:
- "2019"
_4images_image_id: "24890"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24890 -->
Hier die gegenüberliegende Seite der Magneten auf dem zylindrischen Körper, der den Hauptteil von Sarja bildet.
Im geschlossenen Zustand liegen die Magneten genau übereinander und halten das Modell fest zusammen (ich kann das Modell problemlos am [eigentlich] beweglichen Teil hochheben, ohne dass die Magneten sich voneinander lösen.)