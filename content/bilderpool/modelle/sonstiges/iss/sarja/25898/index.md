---
layout: "image"
title: "Fülgelhalterung"
date: "2009-12-06T19:38:52"
picture: "sarjaupdate02.jpg"
weight: "8"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25898
- /details297a.html
imported:
- "2019"
_4images_image_id: "25898"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25898 -->
Leider musste ich für die Flügel eine Halterung bauen, da es hier (im Gegensatz zum Weltraum) eine ziemlich hohe Gravitation gibt (9,81 m/s²).
Nur so konnte ich erreichen, dass die Flügel nicht durchbiegen.
Das Rad lässt sich drehen, da die Flügel mit Motoren ansteuerbar sind, um sie im richtigen Winkel zur Sonne stehen zu lassen (somit soll eine optimale Stromversorgung zu jeder Zeit gewährleistet sein).