---
layout: "image"
title: "Magnete in ihren Fassungen"
date: "2009-09-07T21:17:19"
picture: "sarja5.jpg"
weight: "5"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24889
- /detailsf9ae-2.html
imported:
- "2019"
_4images_image_id: "24889"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24889 -->
Hier kann man ganz gut die Magnete in ihren Fassungen sehen.

Einmal die drei losen Enden, die jeweils einen Magneten halten und einmal am rechten Rand einen BS 5 der einen Magneten hält (insgasamt gibt es auf der ganzen Länge 3 Magnete, die kann man hier aber nicht sehen). Die Magnete passen in alles was eine runde Führungsnut hat ;-) Dadurch sind die Anwendungsmöglichkeiten nahezu unbegrenzt.