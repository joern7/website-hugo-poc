---
layout: "image"
title: "Impulstaster"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate12.jpg"
weight: "18"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25908
- /detailsde0c-2.html
imported:
- "2019"
_4images_image_id: "25908"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25908 -->
Noch mal den Impulstaster genauer.