---
layout: "image"
title: "Sarja aufgeklappt"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate09.jpg"
weight: "15"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25905
- /details926c-2.html
imported:
- "2019"
_4images_image_id: "25905"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25905 -->
Natürlich lässt sich Sarja immer noch aufklappen, damit man leichter im Inneren arbeiten kann.