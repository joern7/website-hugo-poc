---
layout: "image"
title: "X-Wing und Radar"
date: "2016-10-30T20:05:42"
picture: "DSC00426_sc01.jpg"
weight: "4"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/44713
- /detailscdae.html
imported:
- "2019"
_4images_image_id: "44713"
_4images_cat_id: "3330"
_4images_user_id: "2488"
_4images_image_date: "2016-10-30T20:05:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44713 -->
...der X-Wing, mit dem alles anfing, und unten links im Bild die einzige Eigenkonstruktion, eine transportable Radarstation....