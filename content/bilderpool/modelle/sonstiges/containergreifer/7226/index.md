---
layout: "image"
title: "containergreifer11.jpg"
date: "2006-10-24T10:05:52"
picture: "containergreifer11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7226
- /details1e71.html
imported:
- "2019"
_4images_image_id: "7226"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-24T10:05:52"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7226 -->
