---
layout: "image"
title: "containergreifer08.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7223
- /details3bca.html
imported:
- "2019"
_4images_image_id: "7223"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7223 -->
