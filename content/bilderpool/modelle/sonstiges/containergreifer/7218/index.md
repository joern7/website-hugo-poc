---
layout: "image"
title: "containergreifer03.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7218
- /detailsf1a7.html
imported:
- "2019"
_4images_image_id: "7218"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7218 -->
