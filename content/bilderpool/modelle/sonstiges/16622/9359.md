---
layout: "comment"
hidden: true
title: "9359"
date: "2009-05-27T17:35:19"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Ich habe das so verstanden, das der erste Motor von der Software angesteuert wird. Dieser Motor dreht dabei den zweiten Motor, die Spannung die dieser dabei produziert wird gemessen.

Ich sehe da keinen Denkfehler.