---
layout: "image"
title: "Drehausgleicher"
date: "2008-12-15T20:21:18"
picture: "Motorausgleicher.jpg"
weight: "12"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["Motor", "Ausgleicher", "Regler", "-Michael-"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/16622
- /details4d40.html
imported:
- "2019"
_4images_image_id: "16622"
_4images_cat_id: "323"
_4images_user_id: "820"
_4images_image_date: "2008-12-15T20:21:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16622 -->
Bei diesem Drehausgleicher (ich nenne ihn mal so :) ), kann ich am Computer (über den Regler(*¹)) einstellen wie schnell der Motor 1(*²) sein soll. Man kann die Werte zwischen 1 und 1000 einstellen.(Leider geht der Motor pro 8 Werte nur eine Stufe Höher). 

So und jetzt kommt der Wichtigste Teil:
Der andere Motor(*³) wird als Sensor benutzt und spuckt auch Werte aus. Wenn der Wert des Motors den Gleichen Wert wie der Motor 2 hat ist alles Ok, aber wenn nicht dann gleicht die Software wieder alles aus, so dass die Geschwindigkeit bei dem Motor Exsagt die gleiche ist wie man beim Regler zuvor eingestellt hat.