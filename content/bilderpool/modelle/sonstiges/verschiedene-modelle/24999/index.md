---
layout: "image"
title: "Fire Truck"
date: "2009-09-20T19:18:07"
picture: "verschiedenemodelle4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/24999
- /details9f9a.html
imported:
- "2019"
_4images_image_id: "24999"
_4images_cat_id: "1768"
_4images_user_id: "374"
_4images_image_date: "2009-09-20T19:18:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24999 -->
Fire Truck mit ausgefahrener Drehlaeiter