---
layout: "image"
title: "Iphone5 Halterung 1"
date: "2013-09-03T22:01:42"
picture: "IMG_2452.jpg"
weight: "1"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: ["Stativ", "Bennik", "Iphone", "5", "Halterung"]
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/37296
- /details3c3f.html
imported:
- "2019"
_4images_image_id: "37296"
_4images_cat_id: "2776"
_4images_user_id: "1549"
_4images_image_date: "2013-09-03T22:01:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37296 -->
Halterung fürs Iphone5