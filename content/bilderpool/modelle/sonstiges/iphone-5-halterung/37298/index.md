---
layout: "image"
title: "Iphone 5 Halterung"
date: "2013-09-04T15:34:06"
picture: "IMG_0572.jpg"
weight: "3"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: ["Iphone", "5", "Halterung", "Stativ", "Bennik"]
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/37298
- /details7bd2.html
imported:
- "2019"
_4images_image_id: "37298"
_4images_cat_id: "2776"
_4images_user_id: "1549"
_4images_image_date: "2013-09-04T15:34:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37298 -->
Halterung fürs Iphone 5