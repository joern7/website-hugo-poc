---
layout: "image"
title: "Sonnen-aufgangs-wecker"
date: "2006-05-01T14:20:13"
picture: "Jakob_ft-Bilder2.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/6191
- /details1d3c.html
imported:
- "2019"
_4images_image_id: "6191"
_4images_cat_id: "534"
_4images_user_id: "420"
_4images_image_date: "2006-05-01T14:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6191 -->
