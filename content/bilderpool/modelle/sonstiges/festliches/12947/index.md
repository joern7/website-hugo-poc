---
layout: "image"
title: "ft Mayflower (Thanksgiving Theme)"
date: "2007-11-30T23:31:39"
picture: "ft-Mayflower.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Thanksgiving", "Mayflower", "ship"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12947
- /detailsbf86.html
imported:
- "2019"
_4images_image_id: "12947"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2007-11-30T23:31:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12947 -->
A model of the ship Mayflower in celebration of the first Thanksgiving. (Ein Modell des Schiffes Mayflower in der Feier des ersten Thanksgiving. -Google translation)