---
layout: "image"
title: "Advent Ship #2"
date: "2010-12-14T19:54:34"
picture: "ship_advent.jpg"
weight: "49"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["ship", "Advent", "calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29456
- /details7893.html
imported:
- "2019"
_4images_image_id: "29456"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-14T19:54:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29456 -->
This is a micro build ship for an ft advent calendar.