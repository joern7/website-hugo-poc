---
layout: "image"
title: "Stocking"
date: "2010-12-04T11:08:30"
picture: "stocking.jpg"
weight: "32"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29405
- /details1451.html
imported:
- "2019"
_4images_image_id: "29405"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-04T11:08:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29405 -->
This is a quick stocking model. For the Advent Calendar project.