---
layout: "image"
title: "Weihnachten"
date: "2011-12-25T14:29:38"
picture: "schmuck2.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33806
- /detailsbff3.html
imported:
- "2019"
_4images_image_id: "33806"
_4images_cat_id: "1507"
_4images_user_id: "453"
_4images_image_date: "2011-12-25T14:29:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33806 -->
