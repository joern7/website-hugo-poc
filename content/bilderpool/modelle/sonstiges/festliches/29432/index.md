---
layout: "image"
title: "Flying Reindeer"
date: "2010-12-07T07:25:49"
picture: "flying-raindeerA.jpg"
weight: "37"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Flying", "Reindeer", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29432
- /detailsd898-2.html
imported:
- "2019"
_4images_image_id: "29432"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-07T07:25:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29432 -->
This is a model of Flying Reindeer for the Advent Calendar