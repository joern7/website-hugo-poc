---
layout: "image"
title: "Flying Reindeer"
date: "2010-12-07T07:25:49"
picture: "flying-raindeerB.jpg"
weight: "38"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Flying", "Reindeer", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29433
- /details9039-2.html
imported:
- "2019"
_4images_image_id: "29433"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-07T07:25:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29433 -->
This is an alternative view of the Flying Reindeer model for the Advent Calendar