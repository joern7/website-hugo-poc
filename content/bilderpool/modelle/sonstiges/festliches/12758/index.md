---
layout: "image"
title: "ft Turkey (Thanksgiving Theme)"
date: "2007-11-16T21:59:48"
picture: "ft-turkey_a.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Turkey", "Thanksgiving"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12758
- /details97cd.html
imported:
- "2019"
_4images_image_id: "12758"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2007-11-16T21:59:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12758 -->
A model of a Turkey in celebration of Thanksgiving. (Ein Modell der Türkei zur Feier der Danksagung. -Google translation)