---
layout: "image"
title: "Advent Lighthouse"
date: "2010-12-17T18:09:17"
picture: "advent_lighthouse.jpg"
weight: "60"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "Lighthouse"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29467
- /detailsb719-3.html
imported:
- "2019"
_4images_image_id: "29467"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-17T18:09:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29467 -->
This is a (admitedly sad) lighthouse for an advent calendar.