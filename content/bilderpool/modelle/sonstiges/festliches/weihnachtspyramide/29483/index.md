---
layout: "image"
title: "Weihnachtspyramide 8"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_8.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29483
- /detailsf1b9.html
imported:
- "2019"
_4images_image_id: "29483"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29483 -->
Unten drunter sieht es weniger aufgeräumt aus - hier laufen alle Kabel der Kerzen und des Motors zusammen. Ein 9V-Blockakku versorgt die Pyramide mit Strom, der Mini-Motor treibt die Drehscheibe an.