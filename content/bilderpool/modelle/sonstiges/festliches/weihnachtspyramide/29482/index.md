---
layout: "image"
title: "Weihnachtspyramide 7"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_7.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29482
- /detailsbf25.html
imported:
- "2019"
_4images_image_id: "29482"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29482 -->
Blick von oben auf das enge Package von Schlitten und Rentier. Es schleift gerade so nicht an den Trägern der Pyramide.