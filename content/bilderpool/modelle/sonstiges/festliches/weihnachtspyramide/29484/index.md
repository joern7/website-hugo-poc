---
layout: "image"
title: "Weihnachtspyramide 9"
date: "2010-12-18T14:56:51"
picture: "Weihnachtspyramide_9.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29484
- /details8e25-3.html
imported:
- "2019"
_4images_image_id: "29484"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29484 -->
Links im Bild ein zweistufiger Schalter. Die Statikstrebe lässt sich nur in eine Richtung drehen und schaltet über den ersten Taster zuerst nur die Kerzen an. Dreht man sie weiter, wird über den zweiten Taster der Motor zugeschaltet. Noch weiter gedreht geht die Pyramide wieder komplett aus.

Die Zweistufigkeit habe ich eingebaut, um auf Wunsch nur das stimmungsvolle Kerzenlicht ohne das laute Motorgeräusch einschalten zu können. Außerdem lässt sich so Strom sparen.