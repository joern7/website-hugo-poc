---
layout: "image"
title: "Weihnachtspyramide 3"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_4.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29478
- /details16e8.html
imported:
- "2019"
_4images_image_id: "29478"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29478 -->
Die Draufsicht. Die Flügel haben hier natürlich keine Funktion.