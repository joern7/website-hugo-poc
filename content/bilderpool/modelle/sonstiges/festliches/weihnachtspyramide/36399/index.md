---
layout: "image"
title: "Weihnachtspyramide anderer Antrieb 1"
date: "2013-01-04T12:50:56"
picture: "weihnachtspyramideandererantrieb1.jpg"
weight: "10"
konstrukteure: 
- "thomas004, Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36399
- /details9703.html
imported:
- "2019"
_4images_image_id: "36399"
_4images_cat_id: "2145"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T12:50:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36399 -->
Dieser Antrieb für die Weihnachtspyramide von thomas004 läuft etwas langsamer als der Antrieb durch zwei Zahnräder in einer - wie ich finde - realistischeren Geschwindigkeit
Video: https://www.dropbox.com/s/3crifybxxmepox0/Weihnachtspyramide.mp4 (Entschuldigung wegen der Qualität)