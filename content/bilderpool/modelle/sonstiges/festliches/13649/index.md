---
layout: "image"
title: "Valentine Heart"
date: "2008-02-15T19:04:11"
picture: "Valentine_Heart.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Valentine", "Heart"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13649
- /detailseeb4.html
imported:
- "2019"
_4images_image_id: "13649"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2008-02-15T19:04:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13649 -->
Ein Herz für meine Valentine!