---
layout: "image"
title: "Jaialdi 2010!"
date: "2010-08-01T11:23:53"
picture: "jaialdi_stone.jpg"
weight: "23"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["jaialdi", "harrijasotzaile"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27787
- /details9998-2.html
imported:
- "2019"
_4images_image_id: "27787"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-08-01T11:23:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27787 -->
HAPPY JAIALDI 2010!
A harrijasotzaile lifting the stone.