---
layout: "image"
title: "Weihnachtsgruß 1"
date: "2006-12-12T08:54:27"
picture: "Weihnachtsschlitten_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7875
- /detailsbbba.html
imported:
- "2019"
_4images_image_id: "7875"
_4images_cat_id: "739"
_4images_user_id: "328"
_4images_image_date: "2006-12-12T08:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7875 -->
Allen Fischertechnik-Freunden und Euren Familien eine besinnliche Adventszeit und ein frohes und gesundes Weihnachtsfest!!!

Gruß, Thomas