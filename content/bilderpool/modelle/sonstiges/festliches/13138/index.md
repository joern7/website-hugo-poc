---
layout: "image"
title: "Weihnachtskugeln bearbeiten"
date: "2007-12-22T07:38:05"
picture: "Weihnachten07-1.jpg"
weight: "5"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/13138
- /detailsec36.html
imported:
- "2019"
_4images_image_id: "13138"
_4images_cat_id: "1180"
_4images_user_id: "59"
_4images_image_date: "2007-12-22T07:38:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13138 -->
Dies ist eine simple Apparatur, um neulackierte Weihnachtskugeln sicher
trocknen zu lassen. Der Lack sollte relativ dünnflüssig sein, damit es eine
gleichmäßige Farbstruktur gibt. Die Kugeln können dann beliebig (im Bei-
spiel ca. 2 Stunden) langsam gedreht werden, so daß keinerlei "Laufnasen"
im Lack entstehen. Die alten Kugeln (im Vordergrund) sollten zuvor etwas
angerauht werden (losen Lack vorsichtig abschleifen). Die Metallachsen
werden nur mit Küchenpapier umwickelt und die Kugel mit Tesafilm fixiert.