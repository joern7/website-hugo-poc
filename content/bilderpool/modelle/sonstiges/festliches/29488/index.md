---
layout: "image"
title: "Schwibbogen"
date: "2010-12-18T18:32:08"
picture: "schwibbogen1.jpg"
weight: "63"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/29488
- /detailsd52d-3.html
imported:
- "2019"
_4images_image_id: "29488"
_4images_cat_id: "1180"
_4images_user_id: "791"
_4images_image_date: "2010-12-18T18:32:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29488 -->
Das ist ein Schwibbogen aus fischertechnik. Er steht bei uns auf dem Fensterbrett im Wohnzimmer. Im Schwibbogen stehen ein Schneemann, eine Tanne und ein Rentier samt Schlitten und Geschenken. Außerdem hängt ein Stern vom Bogen. Beleuchtet wird das ganze von 6 Lampen.
Frohe Weihnachten.