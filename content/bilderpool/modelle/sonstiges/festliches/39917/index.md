---
layout: "image"
title: "Schneemann mit Hintergrund"
date: "2014-12-11T17:18:30"
picture: "Schneemann2_small.jpg"
weight: "71"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39917
- /details1216.html
imported:
- "2019"
_4images_image_id: "39917"
_4images_cat_id: "1180"
_4images_user_id: "502"
_4images_image_date: "2014-12-11T17:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39917 -->
