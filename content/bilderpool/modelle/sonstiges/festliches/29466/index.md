---
layout: "image"
title: "Advent Plane"
date: "2010-12-17T18:09:17"
picture: "advent_plane2.jpg"
weight: "59"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "airplane"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29466
- /details5d4d.html
imported:
- "2019"
_4images_image_id: "29466"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-17T18:09:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29466 -->
A small airplane model for the Advent Calendar