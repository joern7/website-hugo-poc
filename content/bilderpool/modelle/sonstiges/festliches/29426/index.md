---
layout: "image"
title: "Lichterbogen"
date: "2010-12-05T17:04:57"
picture: "lichterbogen1_2.jpg"
weight: "34"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/29426
- /details98f8.html
imported:
- "2019"
_4images_image_id: "29426"
_4images_cat_id: "1180"
_4images_user_id: "1"
_4images_image_date: "2010-12-05T17:04:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29426 -->
Habe den Lichterbogen von Masked mal mit 7 Leuchten nachgebaut.
Anstelle der 60er Winkelträger habe ich BS30 gelb genutzt!
