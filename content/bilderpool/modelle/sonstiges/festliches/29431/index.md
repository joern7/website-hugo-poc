---
layout: "image"
title: "Yule Tree"
date: "2010-12-06T12:51:44"
picture: "sm_ft_tree.jpg"
weight: "36"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Yule", "Tree", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29431
- /details143f.html
imported:
- "2019"
_4images_image_id: "29431"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-06T12:51:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29431 -->
A rendered Yule Tree for Advent Calendar Project.