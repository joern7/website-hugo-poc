---
layout: "image"
title: "Menora"
date: "2010-12-16T18:11:20"
picture: "Menora.jpg"
weight: "57"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/29464
- /details9c6c-2.html
imported:
- "2019"
_4images_image_id: "29464"
_4images_cat_id: "1180"
_4images_user_id: "59"
_4images_image_date: "2010-12-16T18:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29464 -->
Dies ist meine Interpretation einer Menora. Da ich aber kaum rote Bausteine habe und nicht auch noch dreifarbig bauen wollte, habe ich den Unterbau komplett in dezentem Schwarz aufgebaut.
Betrieben werden die Lämpchen mit 24 V ~ je 2 x in Reihe: ein Zweig mit 4 Lampen und ein Zweig mit 3 Lampen + Widerstand (47 Ohm).