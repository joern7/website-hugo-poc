---
layout: "image"
title: "Christmas Present"
date: "2010-12-02T17:03:50"
picture: "xmasbox.jpg"
weight: "28"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Christmas", "Present", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29401
- /details4fa0.html
imported:
- "2019"
_4images_image_id: "29401"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-02T17:03:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29401 -->
This is the second in the ft Advent Calendar series.