---
layout: "image"
title: "Advent Snowflake"
date: "2010-12-10T00:52:54"
picture: "snowflake1.jpg"
weight: "44"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Snowflake", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29446
- /details7231-2.html
imported:
- "2019"
_4images_image_id: "29446"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-10T00:52:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29446 -->
This is a Snowflake model for an Advent Calendar.