---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-30T21:05:05"
picture: "sm_skull_000.jpg"
weight: "11"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "skull"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25592
- /details194f.html
imported:
- "2019"
_4images_image_id: "25592"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25592 -->
This is my 2009 fischertechnik Skull