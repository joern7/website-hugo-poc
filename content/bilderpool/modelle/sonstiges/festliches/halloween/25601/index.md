---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-31T14:15:29"
picture: "sm_skull_5.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "skull", "Halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25601
- /details6bab.html
imported:
- "2019"
_4images_image_id: "25601"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-31T14:15:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25601 -->
This is my 2009 fischertechnik Skull