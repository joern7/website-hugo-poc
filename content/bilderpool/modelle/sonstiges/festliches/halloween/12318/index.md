---
layout: "image"
title: "ft Halloween Skull (Halloween Theme)"
date: "2007-10-27T07:41:23"
picture: "sp_skull_2_2.jpg"
weight: "7"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["motorized", "halloween", "skull"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12318
- /details3c91.html
imported:
- "2019"
_4images_image_id: "12318"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-27T07:41:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12318 -->
A rising and spinning skull for Halloween!  This is a inexpensive plastic skull fixed on an axle. The model is programmed to slowly rise and then the skull will rotate. The model will be hidden under a sheet, giving the appearance of a rising ghost! 
	
(Eine steigende und Spinnen Schädel für Halloween! Dies ist eine kostengünstige Kunststoff Schädel auf einer festen Achse. Das Modell ist so programmiert zu langsam steigen und dann wird der Schädel drehen. Das Modell wird versteckt unter einem Blatt, die das Aussehen einer steigenden Geist!-google translation)