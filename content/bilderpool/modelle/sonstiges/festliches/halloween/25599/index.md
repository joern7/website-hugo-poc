---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-30T21:05:05"
picture: "sm_skull_6_2.jpg"
weight: "18"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "skull", "halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25599
- /details9f2e-3.html
imported:
- "2019"
_4images_image_id: "25599"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25599 -->
This is my 2009 fischertechnik Skull