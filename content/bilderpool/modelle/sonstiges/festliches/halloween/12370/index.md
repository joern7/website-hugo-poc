---
layout: "image"
title: "ft Halloween Skull structure"
date: "2007-10-30T18:02:55"
picture: "S_Skull_sm2.jpg"
weight: "8"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Halloween", "Skull", "structure"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12370
- /detailscdef.html
imported:
- "2019"
_4images_image_id: "12370"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-30T18:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12370 -->
This is a rendering of the fischertechnik structure used for the Halloween Skull model. 

(Dies ist eine Darstellung aus dem fischertechnik Struktur für die Halloween Skull Modell -google translation)

Parts List: 
3 Hub Nut
2 Link 15
2 Motor Reducing Gearbox
2 Clip Axle with Gear Teeth T28
1 Hub Nut
11 Spring Cam
3 Building Block 15 with Bore
2 Mini Motor 6-9v
1 Building Block 15 x 15
2 Bottom Plate 30x90
2 Building Block 30
1 Building Block 15
3 Flat Hub Collet
4 Building Block 15x30x5 with Groove and Pin
1 Clip Axle 30
1 Axle Coupling
1 Clip Axle 75
1 Base Plate 120x60
2 Cog Wheel T 10 m=1.5 Narrow
1 Gear Wheel T 30
2 Angle Girder 120
2 Angle Girder 60
2 Rivet 4
1 I-Strut 45
5 Building Block 5
1 Building Block 5 with two Pins
8 Building Block 7.5
1 Clip Axle 180
1 Clip 5
1 Locking Worm m=1.5
2 Worm Nut m=1.5
4 Worm m=1.5
1 Angular Block 10x15x15
1 Building Plate 15x30x5 with 3 Grooves