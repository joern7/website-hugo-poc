---
layout: "image"
title: "ft Jack-o-Lantern (Halloween Theme)"
date: "2007-10-10T19:44:36"
picture: "ft-Jack-o_Lantern.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["jack", "o", "lantern", "pumpkin", "Halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12173
- /detailsdf8f.html
imported:
- "2019"
_4images_image_id: "12173"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-10T19:44:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12173 -->
A jack-o-lantern for Halloween! (Eine Steckfassung-Olaterne für Halloween! - google translation)