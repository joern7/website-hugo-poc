---
layout: "comment"
hidden: true
title: "6852"
date: "2008-08-13T18:17:53"
uploadBy:
- "ft_idaho"
license: "unknown"
imported:
- "2019"
---
Howdy,

Instructables just announced the winners of the robot contest. My ft egg placed in the sensors category! 

http://www.instructables.com/community/RoboGames_Robot_Contest_Winners/

I thought I would share!

Richard :-)