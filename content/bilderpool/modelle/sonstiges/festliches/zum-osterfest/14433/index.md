---
layout: "image"
title: "ft Easter Egg Robot (Easter Theme)"
date: "2008-05-02T00:57:26"
picture: "ft-egg_vb.jpg"
weight: "10"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Easter", "Egg", "Robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14433
- /detailsea9b-2.html
imported:
- "2019"
_4images_image_id: "14433"
_4images_cat_id: "1508"
_4images_user_id: "585"
_4images_image_date: "2008-05-02T00:57:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14433 -->
This is a model of a ft Easter Egg Robot! I am entering this egg in the instructables.com robot contest. 

google translation: 	
Dies ist ein Modell eines ft Ostern Egg Robot! Ich bin in diesem Ei in der instructables.com Roboter-Wettbewerb.