---
layout: "comment"
hidden: true
title: "6118"
date: "2008-03-31T23:49:28"
uploadBy:
- "ft_idaho"
license: "unknown"
imported:
- "2019"
---
I have posted the instructions to build a fischertechnik Easter Rabbit for the instructables.com "Pocket-Sized Speed Contest". 

http://www.instructables.com/id/fischertechnik-Easter-Rabbit/

Please visit it and vote for me! 

The rules: http://www.instructables.com/id/How-To-Enter-the-Pocket-Sized-Speed-Contest/

Thanks!

Richard

***google translation: 


Howdy,

Ich habe die Anleitungen zum Bau eines fischertechnik Ostern Rabbit für die instructables.com "Pocket-Sized Speed Contest".

Http://www.instructables.com/id/fischertechnik-Easter-Rabbit/

Bitte besuchen Sie sie und für mich voten!


Die Regeln: http://www.instructables.com/id/How-To-Enter-the-Pocket-Sized-Speed-Contest/

Vielen Dank!

Richard