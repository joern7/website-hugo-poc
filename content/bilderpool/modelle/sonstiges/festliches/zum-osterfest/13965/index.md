---
layout: "image"
title: "Eierausblasvorrichtung1"
date: "2008-03-20T10:00:56"
picture: "Bild1.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/13965
- /details5d97.html
imported:
- "2019"
_4images_image_id: "13965"
_4images_cat_id: "1508"
_4images_user_id: "182"
_4images_image_date: "2008-03-20T10:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13965 -->
Hier eine Vorrichtung unm das lästige Ausblasen von Eiern zu erleichtern. Seit dem Bau gibts bei uns öfters Rühreier. :))