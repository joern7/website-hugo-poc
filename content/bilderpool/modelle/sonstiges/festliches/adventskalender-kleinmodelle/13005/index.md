---
layout: "image"
title: "Erdbohrer mit Handkurbel"
date: "2007-12-06T19:07:02"
picture: "adv2.jpg"
weight: "2"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13005
- /details79de-2.html
imported:
- "2019"
_4images_image_id: "13005"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13005 -->
