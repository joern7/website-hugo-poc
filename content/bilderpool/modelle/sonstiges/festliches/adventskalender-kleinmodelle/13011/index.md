---
layout: "image"
title: "Schranke mit Handkurbel"
date: "2007-12-06T19:07:02"
picture: "adv8.jpg"
weight: "8"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13011
- /details85a3.html
imported:
- "2019"
_4images_image_id: "13011"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13011 -->
