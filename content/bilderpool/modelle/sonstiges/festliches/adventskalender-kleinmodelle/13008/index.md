---
layout: "image"
title: "Raupe / Bulldozer"
date: "2007-12-06T19:07:02"
picture: "adv5.jpg"
weight: "5"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13008
- /details3bf1.html
imported:
- "2019"
_4images_image_id: "13008"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13008 -->
Hier werden die "schmalen" Raupenbeläge verwendet, die bei der Raupenkette als jedes zweite Glied ein Spezielles brauchen, auf das man einen Raupenbelag-Baustein aufschieben kann. Wohl leider nicht mehr lieferbar.