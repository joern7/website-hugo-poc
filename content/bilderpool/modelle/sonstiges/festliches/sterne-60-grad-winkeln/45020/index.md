---
layout: "image"
title: "Kleiner Stern"
date: "2017-01-07T20:37:39"
picture: "IMG_1053.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45020
- /details8f58.html
imported:
- "2019"
_4images_image_id: "45020"
_4images_cat_id: "3350"
_4images_user_id: "1359"
_4images_image_date: "2017-01-07T20:37:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45020 -->
