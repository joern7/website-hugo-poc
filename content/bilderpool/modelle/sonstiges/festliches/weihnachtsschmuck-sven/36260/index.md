---
layout: "image"
title: "original ft Weihnachtsschmuck"
date: "2012-12-12T15:41:00"
picture: "weihnachten6.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36260
- /details71b5-2.html
imported:
- "2019"
_4images_image_id: "36260"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T15:41:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36260 -->
Hier habe ich mal einen 1000er Box voll mit original ft Weihnachtsschmuck (alles was es so in den letzten Jahren von ft an Weihnachtsschmuck gab) gebaut.
Bei den Gocken fehlt noch etwas, wird noch nachträglich angebaut.