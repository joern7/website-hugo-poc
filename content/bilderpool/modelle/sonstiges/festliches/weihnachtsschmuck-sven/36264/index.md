---
layout: "image"
title: "riesen Stern"
date: "2012-12-12T16:28:20"
picture: "riesenstern1.jpg"
weight: "10"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36264
- /details6718-2.html
imported:
- "2019"
_4images_image_id: "36264"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T16:28:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36264 -->
Und weiter gehts mit einem riesen Stern in etwas anderer Form.