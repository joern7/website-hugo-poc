---
layout: "image"
title: "Kugel blau"
date: "2012-12-13T19:31:22"
picture: "kugel_blau.jpg"
weight: "12"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36266
- /detailsdd60-2.html
imported:
- "2019"
_4images_image_id: "36266"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36266 -->
Noch mal eine Kugel in anderer Form.
Mittels zwei Drehscheiben gebaut.