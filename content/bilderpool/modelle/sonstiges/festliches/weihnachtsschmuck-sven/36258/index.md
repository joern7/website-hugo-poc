---
layout: "image"
title: "Weihnachtskugel Aufhänger"
date: "2012-12-12T15:41:00"
picture: "weihnachten4.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36258
- /details498b.html
imported:
- "2019"
_4images_image_id: "36258"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T15:41:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36258 -->
Hier sieht man den Aufhänger der Kugel