---
layout: "image"
title: "großer Stern Mitte"
date: "2012-12-12T16:16:39"
picture: "weihnachstsschmuck3.jpg"
weight: "9"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36263
- /details7fd5.html
imported:
- "2019"
_4images_image_id: "36263"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T16:16:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36263 -->
