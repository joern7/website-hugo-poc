---
layout: "image"
title: "ft Yule Soap"
date: "2008-12-22T07:14:08"
picture: "sm_ft_soap.jpg"
weight: "14"
konstrukteure: 
- "Laura Baran"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Yule", "Soap"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16680
- /detailsdf44.html
imported:
- "2019"
_4images_image_id: "16680"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2008-12-22T07:14:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16680 -->
For Yule, Laura gave everyone homemade bars of soap with a ft  BB30 inside! Happy Yule everyone!