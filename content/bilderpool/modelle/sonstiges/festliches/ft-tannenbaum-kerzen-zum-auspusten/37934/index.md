---
layout: "image"
title: "Robo Pro Programm"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten3.jpg"
weight: "3"
konstrukteure: 
- "DasKasperle"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37934
- /detailsd093.html
imported:
- "2019"
_4images_image_id: "37934"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37934 -->
Anstelle von Motor wurden LED´s angeschlossen.
An den Ausgängen M1 bis M3 befinden sich:
3 Led´s mit dem plus Pol - links, minus Pol - rechts und
3 Led´s mit dem plus Pol - rechts, minus Pol - links.
Ist die Motordrehrichtung mit links angegeben leuchten die ersten drei Led´s und Motordrehrichtung rechtsherum leuchten die anderen drei Led´s.

Am M4 befindet sich das 9V Relais, es aktiviert das Sprachausgabemodul.
Am I1 ist der "PappKerzen-PusteSensor" angeschlossen.

In der ersten Schleife leuchten alle sechs Led´s, in jeder weiteren Schleife wird eine Motordrehrichtung auf AUS gestellt. Somit erlischt nach jedem Pusten eine Led.
Sind alle Schleifen abgearbeitet, sind alle Led´s aus und das Relais aktiviert das Soundmodul.

Nach 10 Sekunden wird das Relais abgeschaltet und der Baum erstrahlt wieder mit allen sechs Led´s.
  


