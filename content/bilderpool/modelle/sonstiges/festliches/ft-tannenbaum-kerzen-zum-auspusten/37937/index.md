---
layout: "image"
title: "Kerze mit Kerzenhalter"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten6.jpg"
weight: "6"
konstrukteure: 
- "DasKasperle"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37937
- /detailsb086.html
imported:
- "2019"
_4images_image_id: "37937"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37937 -->
Die Kerzene bestehen aus zwei Zylindern (Schornsteinatrappe), 5mm gelb-flackernden LED´s 3,2V 20mA, Schrumpfschlauch (schwarz), transparenten Zwillingslitzen (0,04mm), 330Ohm Wiederständen.