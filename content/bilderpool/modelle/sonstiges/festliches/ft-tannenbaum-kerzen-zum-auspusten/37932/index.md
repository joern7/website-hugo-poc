---
layout: "image"
title: "Tannenbaum"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten1.jpg"
weight: "1"
konstrukteure: 
- "DasKasperle"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37932
- /detailsa510.html
imported:
- "2019"
_4images_image_id: "37932"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37932 -->
Aus den Statikbausteienn und ein paar Winkelsteinen wurde das Tannenbaumgerüst gesteckt.
In  der Mitte (Spiegelachse ) verläuft ein Nylonfaden (6Kg , D 0,40mm) von unten nach oben, da durch werden die Winkelsteine nicht so stark belastet.
Denn in jedem Stockwerk ist der Faden mit dem Grundgerüst verbunden.
Mit dem Faden wurde der TB an der Tür aufgehangen. Die Zweige wurden mit Blumendrat befestigt.

