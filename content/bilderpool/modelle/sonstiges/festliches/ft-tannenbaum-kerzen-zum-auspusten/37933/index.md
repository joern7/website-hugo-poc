---
layout: "image"
title: "Steuerzentrale"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten2.jpg"
weight: "2"
konstrukteure: 
- "DasKasperle"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37933
- /details3b25.html
imported:
- "2019"
_4images_image_id: "37933"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37933 -->
TXC, Sprachmodul 9V, 20sek. mit "Ohhh Tannebaum" besungen, Relais 9V, zum aktivieren des Sprachmoduls.

Den Ausgänge M1 bis M3 habe ich mehr Raum gegeben, da die Litzen der Led mit 0,04 mm so dünn sind das sie leicht einen Kabelbruch bekommen. Außerdem suchte ich nach einer Möglickeit die Wiederstände besser unter zu bringen.
