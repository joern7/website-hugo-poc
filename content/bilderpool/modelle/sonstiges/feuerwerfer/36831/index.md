---
layout: "image"
title: "Feuerwerfer 04"
date: "2013-04-18T11:36:21"
picture: "feuerwerfer04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36831
- /details46a3.html
imported:
- "2019"
_4images_image_id: "36831"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36831 -->
Hier liegt die Feuerzeuggas-Dose, die das Butan liefert. Darüber zwei Ventile, eins öffnet den Weg zur Düse, das andere ist ein Ablassventil.