---
layout: "image"
title: "Feuerwerfer 13"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36840
- /detailsdf52-2.html
imported:
- "2019"
_4images_image_id: "36840"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36840 -->
Bitte lasst mal eure Meinung zu dem Modell in den Kommentaren da, bin gespannt auf Kritik, Vorschläge und Ratschläge. Sind die Bilder von der Qualität her gut?

Ist fischertechnik für so was gemacht, soll ich das Projekt weiter verfolgen und ggf. noch einen Selbst-Zünder einbauen?

kurzes Video: http://www.youtube.com/watch?v=-nuXwW-KDRo