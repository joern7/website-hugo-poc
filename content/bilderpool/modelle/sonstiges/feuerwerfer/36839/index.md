---
layout: "image"
title: "Feuerwerfer 12"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36839
- /detailsf4e7.html
imported:
- "2019"
_4images_image_id: "36839"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36839 -->
Hier noch das Steuerpult.

Der Joystick links zum fahren, rechts kann man die Düse drehen und das Ventil öffnen.

Der linke Schalter öffnet die Gasdose, der Schalter in der Mitte das Ablassventil. Der rechte Schalter hat keine Funktion.

kurzes Video: http://www.youtube.com/watch?v=-nuXwW-KDRo