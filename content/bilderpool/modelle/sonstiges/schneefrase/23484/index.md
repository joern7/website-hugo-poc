---
layout: "image"
title: "(1) von vorne, rechts"
date: "2009-03-23T07:37:09"
picture: "1_von_vorne_rechts.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/23484
- /details8219.html
imported:
- "2019"
_4images_image_id: "23484"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23484 -->
Gesamtansicht;
Bedienungselemente: rechter Griff: Taster schaltet die Fräse ein.
Polwendeschalter für die Beleuchtung

Ein  Video (14 MB) findet Ihr auf Youtube:

http://www.youtube.com/watch?v=rOtx3rGRcRk

Wenn man genau hinsieht erkennt man, wie der Schnee wegfliegt.