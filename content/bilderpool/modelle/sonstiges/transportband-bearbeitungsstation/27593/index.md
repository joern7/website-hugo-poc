---
layout: "image"
title: "Transportband mit Bearbeitungsstation (Nutfräse) alte Version"
date: "2010-06-25T18:21:28"
picture: "transportbandmitbearbeitungsstationnutfraese5.jpg"
weight: "4"
konstrukteure: 
- "Jürgen Ihrig"
fotografen:
- "Jürgen Ihrig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "juergen669"
license: "unknown"
legacy_id:
- /php/details/27593
- /detailsff17.html
imported:
- "2019"
_4images_image_id: "27593"
_4images_cat_id: "1986"
_4images_user_id: "1158"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27593 -->
