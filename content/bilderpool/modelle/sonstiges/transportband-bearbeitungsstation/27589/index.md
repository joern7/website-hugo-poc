---
layout: "image"
title: "Transportband mit Bearbeitungsstation (Nutfräse)"
date: "2010-06-25T18:21:28"
picture: "transportbandmitbearbeitungsstationnutfraese1.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Ihrig"
fotografen:
- "Jürgen Ihrig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "juergen669"
license: "unknown"
legacy_id:
- /php/details/27589
- /details91e8.html
imported:
- "2019"
_4images_image_id: "27589"
_4images_cat_id: "1986"
_4images_user_id: "1158"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27589 -->
Draufsicht