---
layout: "image"
title: "Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar."
date: "2014-11-16T15:34:20"
picture: "alziendoog10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39835
- /details55df.html
imported:
- "2019"
_4images_image_id: "39835"
_4images_cat_id: "2988"
_4images_user_id: "22"
_4images_image_date: "2014-11-16T15:34:20"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39835 -->
Ophanging gemaakt uit combinatie van Fischertechnik + 15x15mm Open-Beams.

De OpenBeam Corner Cubes 15x15x15mm zijn zeer handig voor diverse constructie toepassingen in combinatie met Fischertechnik.....

http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Products/OB-CC-151515C-P12