---
layout: "image"
title: "Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar."
date: "2014-11-16T15:34:20"
picture: "alziendoog01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39826
- /details59b3.html
imported:
- "2019"
_4images_image_id: "39826"
_4images_cat_id: "2988"
_4images_user_id: "22"
_4images_image_date: "2014-11-16T15:34:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39826 -->
-Op afstand bestuurd draai- en kantelbaar (7,8kg)

Ophanging gemaakt uit combinatie van Fischertechnik + 15x15mm Open-Beams.

http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Categories/OPENBEAM

De OpenBeam Corner Cubes 15x15x15mm zijn zeer handig voor diverse constructie toepassingen in combinatie met Fischertechnik.....

http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Products/OB-CC-151515C-P12

OpenBeam Projects and FischerTechnik : 
http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Categories/OpenBeam_Projects/OpenBeam_and_FischerTechnik