---
layout: "image"
title: "Aufwickelbetrieb"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46789
- /details063d-2.html
imported:
- "2019"
_4images_image_id: "46789"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46789 -->
Zum schnellen Zurückwickeln der Fahrbahn wird die (dann volle) Aufwickelrolle wie hier gezeigt auf die obere Aufnahme aufgelegt, damit sie frei drehen kann. An der hinteren Rolle kann man dann schnell (über die Rastadapter auf den Rastachsen) zurück wickeln.