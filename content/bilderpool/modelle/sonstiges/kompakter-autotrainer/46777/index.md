---
layout: "image"
title: "Mit Austausch-Bahnen"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46777
- /detailsc4dd.html
imported:
- "2019"
_4images_image_id: "46777"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46777 -->
Drei verschiedene Rennstrecken hatte ich damals als Kind mit Wasserfarben auf Rechenmaschinenrollen meines Vaters gemalt.