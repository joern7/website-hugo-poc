---
layout: "image"
title: "Lenkung (2)"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46784
- /detailsca01-2.html
imported:
- "2019"
_4images_image_id: "46784"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46784 -->
Die Mimik mit den S-Kupplungen oben dient dazu, die Rolle mit den großen Reifen dran entweder unten beim Motor (zum Fahren) oder oben auf den Träger (zum Zurückspulen) aufzulegen.