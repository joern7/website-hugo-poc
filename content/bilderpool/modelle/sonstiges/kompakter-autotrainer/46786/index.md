---
layout: "image"
title: "Zählwerk"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46786
- /details8891-3.html
imported:
- "2019"
_4images_image_id: "46786"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46786 -->
Wer eines hat, kann ein em-6-Zählwerk anschließen, das die Fehler zählt. Der Taster untendrunter dient zum Zurückstellen des Zählwerks auf 0.