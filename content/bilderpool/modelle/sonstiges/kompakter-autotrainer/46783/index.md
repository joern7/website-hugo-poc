---
layout: "image"
title: "Lenkung (1)"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46783
- /details6de4-2.html
imported:
- "2019"
_4images_image_id: "46783"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46783 -->
Damit man das Lenkrad richtig herumdrehen muss, geht sein Z20 auf die Kette, in der unten der Mitnehmer für den verschiebbaren Rahmen hängt, auf dem die Lichtschranke befestigt ist.