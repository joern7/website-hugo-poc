---
layout: "comment"
hidden: true
title: "22828"
date: "2016-12-23T17:30:08"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Wäre es sinnvoll den oberen (schwarzen) Teil mit Statikteilen zu gestalten oder mit Platten zu verkleiden? Oder muss der so massiv sein?
Ich meine, je mehr träge Masse da oben bewegt werden muss, um so wuchtiger wird das später, um so stabiler muss der Unterbau sein, um so mehr arbeitet der Motor, um so mehr werden die Zahnräder belastet....