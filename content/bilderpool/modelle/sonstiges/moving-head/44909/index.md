---
layout: "image"
title: "Bau des beweglichen Kopfes"
date: "2016-12-15T17:20:56"
picture: "movinghead11.jpg"
weight: "11"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44909
- /details597b.html
imported:
- "2019"
_4images_image_id: "44909"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44909 -->
Blick in die andere Richtung auf das LED-Modul