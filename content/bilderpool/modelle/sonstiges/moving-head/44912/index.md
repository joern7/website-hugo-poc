---
layout: "image"
title: "Moving-Head von Oben"
date: "2016-12-15T17:20:57"
picture: "movinghead14.jpg"
weight: "14"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44912
- /details732f.html
imported:
- "2019"
_4images_image_id: "44912"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44912 -->
Moving-Head von Oben