---
layout: "image"
title: "Kopf von unten"
date: "2016-12-15T17:20:57"
picture: "movinghead13.jpg"
weight: "13"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44911
- /detailse8ff-2.html
imported:
- "2019"
_4images_image_id: "44911"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44911 -->
Hier ist auch das Gewicht zu sehen, das dazu dient den Kopf möglichst ohne unwucht drehen zu lassen.