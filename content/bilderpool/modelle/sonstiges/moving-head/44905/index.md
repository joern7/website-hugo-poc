---
layout: "image"
title: "Lichtmuster_1"
date: "2016-12-15T17:20:56"
picture: "movinghead07.jpg"
weight: "7"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44905
- /details894f.html
imported:
- "2019"
_4images_image_id: "44905"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44905 -->
Durch die Linse/Lupe kann man die einzelnen Lichtpunkte der LEDs sehen. Abstand zwischen Linse und Fläche ca. 2 Meter.