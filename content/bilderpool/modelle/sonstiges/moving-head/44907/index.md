---
layout: "image"
title: "Lichtmuster_3"
date: "2016-12-15T17:20:56"
picture: "movinghead09.jpg"
weight: "9"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44907
- /detailsda6a.html
imported:
- "2019"
_4images_image_id: "44907"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44907 -->
Abstand zwischen Linse und Fläche ca. 7 Meter.