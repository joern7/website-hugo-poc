---
layout: "image"
title: "Schleifkontakt"
date: "2016-12-15T17:20:57"
picture: "movinghead19.jpg"
weight: "19"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44917
- /details23dc-2.html
imported:
- "2019"
_4images_image_id: "44917"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44917 -->
Die zwei Metallachsen rechts und links werden zum Übertragen des Stroms für das LED-Modul genuzt. Dafür wurde ein Draht jeweils innen und außen auf die Achse gewickelt. Die Übertragung funktioniert gut und Unterbrechungsfrei.