---
layout: "image"
title: "die Fischertechnik Vogelspinne"
date: "2013-03-03T17:53:23"
picture: "P1200678.jpg"
weight: "21"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/36708
- /details33b8.html
imported:
- "2019"
_4images_image_id: "36708"
_4images_cat_id: "323"
_4images_user_id: "1635"
_4images_image_date: "2013-03-03T17:53:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36708 -->
Gesamtansicht der ferngesteuerten Fischertechnik Vogelspinne.