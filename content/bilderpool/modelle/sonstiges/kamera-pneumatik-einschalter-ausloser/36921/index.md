---
layout: "image"
title: "Pneumatische Kamerasteuerrung - oben"
date: "2013-05-14T21:23:14"
picture: "kameramitpneumatikeinschalterausloeser6.jpg"
weight: "6"
konstrukteure: 
- "Kai Baumgart"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36921
- /detailsdef6-2.html
imported:
- "2019"
_4images_image_id: "36921"
_4images_cat_id: "2743"
_4images_user_id: "1677"
_4images_image_date: "2013-05-14T21:23:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36921 -->
