---
layout: "image"
title: "E-Tecs"
date: "2015-03-29T20:33:31"
picture: "spurfolger5.jpg"
weight: "5"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40692
- /detailscd7e.html
imported:
- "2019"
_4images_image_id: "40692"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40692 -->
Beide E-Tecs operieren als Inverter. Am Eingang jeweils ein Anschluss vom IR-Spursensor.