---
layout: "image"
title: "FtApp - Verbindung herstellen"
date: "2016-05-22T19:12:43"
picture: "ftapp1.jpg"
weight: "1"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/43409
- /detailsb912.html
imported:
- "2019"
_4images_image_id: "43409"
_4images_cat_id: "3225"
_4images_user_id: "1549"
_4images_image_date: "2016-05-22T19:12:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43409 -->
Ich habe mich in den Ferien mal hingesetzt und eine kleine Android App geschrieben. Es ist nur die erste (beta-) Version aber ich wollte sie euch nicht vorenthalten :D. Wenn ihr daran interessiert seid, könnt ihr diese Version unter diesem Link herunterladen: https://github.com/Bennik2000/FtApp/blob/master/de.bennik2000.ftapp.apk?raw=true

Falls sich jemand den Source Code und das TCP/IP Protokoll anschauen will, hier ist auch dieser Link: https://github.com/Bennik2000/FtApp