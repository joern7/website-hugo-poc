---
layout: "image"
title: "2.Plastik"
date: "2013-07-07T12:17:49"
picture: "IMG_48442.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/37156
- /detailsd5aa.html
imported:
- "2019"
_4images_image_id: "37156"
_4images_cat_id: "2757"
_4images_user_id: "1631"
_4images_image_date: "2013-07-07T12:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37156 -->
Geht eingendlich relativ einfach, einfach von einem pneumatikschlauch ein kleines Stück ab schneiden und dieses auf die achsen schieben