---
layout: "image"
title: "Viel Bodenfreiheit"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph12.jpg"
weight: "12"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30273
- /details465f.html
imported:
- "2019"
_4images_image_id: "30273"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30273 -->
Viel Bodenfreiheit