---
layout: "image"
title: "Der Wischlappen"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph06.jpg"
weight: "6"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30267
- /details135c.html
imported:
- "2019"
_4images_image_id: "30267"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30267 -->
Der Wischlappen wird vor und zurück gezogen.