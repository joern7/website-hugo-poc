---
layout: "image"
title: "Ansschluss für die 25 Bananenstecker"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph19.jpg"
weight: "19"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30280
- /details15b0.html
imported:
- "2019"
_4images_image_id: "30280"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30280 -->
Ein dickes Kabel :)