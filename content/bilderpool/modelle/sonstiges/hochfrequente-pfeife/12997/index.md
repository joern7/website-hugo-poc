---
layout: "image"
title: "Hochfrequente Pfeife"
date: "2007-12-04T16:58:09"
picture: "pfeife4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/12997
- /details1657.html
imported:
- "2019"
_4images_image_id: "12997"
_4images_cat_id: "1177"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12997 -->
(bei diesem bild ist die Fläche vom rad zu sehen) Die fertige Pfeife, natürlich lässt sich sich in richtung des speicherades länger machen. Beim funktionstest: ganz ganz vorsichtig auf der seite des Speichenrades pusten und dann langsam stärker werden bis man etwas hört. Aber wirklich nur ganz ganz leicht pusten. Für die älteren: nehmt euch ein Kind und schaut wann es drauf reagiert  *g* (der ton ist extrem hoch! Masked weis wovon ich spreche)