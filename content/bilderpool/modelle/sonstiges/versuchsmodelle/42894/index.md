---
layout: "image"
title: "Kabelkanal 1"
date: "2016-02-15T22:41:37"
picture: "IMG_3343.jpg"
weight: "18"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42894
- /details7aba.html
imported:
- "2019"
_4images_image_id: "42894"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42894 -->
Ziel dieses Versuchsaufbaus bzw. dieser Konstruktion ist die Schaffung eines mittelgroßen Kabelkanals mittels vorhandener original ft-Teile.

Als "Kanalunterteil" eignet sich hierzu perfekt das Rollenlager 37636, als "Bogenstück" der Eckbaustein 38240 und als "Halter" der Federnocken 31982.