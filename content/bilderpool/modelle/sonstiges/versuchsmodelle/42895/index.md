---
layout: "image"
title: "Kabelkanal 2"
date: "2016-02-15T22:41:37"
picture: "IMG_3345.jpg"
weight: "19"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42895
- /details7658.html
imported:
- "2019"
_4images_image_id: "42895"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42895 -->
Als "Deckel" für den Kabelkanal dienen die Bauplatte 15x30x3,75 (32330) und die recht selten gewordene Bauplatte 15x15x3,75 (32315).