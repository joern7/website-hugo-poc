---
layout: "image"
title: "U-Träger-Tragwerk 6"
date: "2016-02-15T22:41:37"
picture: "IMG_3288.jpg"
weight: "6"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42882
- /detailsc7a8.html
imported:
- "2019"
_4images_image_id: "42882"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42882 -->
Ansicht eines äußeren Knotenpunkts.