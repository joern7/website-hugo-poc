---
layout: "comment"
hidden: true
title: "21696"
date: "2016-02-16T08:08:30"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Wer's nicht glauben mag kann ja nachzählen. Das Foto zeigt freundlicherweise alle Teile ;-)

Mit 49 x WS7,5° sind es je Winkelstein etwa 0,15° zuviel für den Vollkreis, aber das fällt nicht weiter auf. Von den Abmessungen her müßte es sehr stramm sitzen - eventuell verkippen die WS zu der Seite die im Bild auf der Tischplatte liegt.