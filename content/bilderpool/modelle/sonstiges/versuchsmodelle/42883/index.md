---
layout: "image"
title: "U-Träger-Brückentragwerk 5"
date: "2016-02-15T22:41:37"
picture: "IMG_3299.jpg"
weight: "7"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42883
- /details932e.html
imported:
- "2019"
_4images_image_id: "42883"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42883 -->
Inspektion eines äußeren Knotenpunkts.

Die Aufnahme macht sehr gut deutlich wie massiv und robust dieses Tragwerk ist.