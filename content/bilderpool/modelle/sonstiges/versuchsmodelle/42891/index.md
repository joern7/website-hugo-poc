---
layout: "image"
title: "Träger/Kabelkanal 1"
date: "2016-02-15T22:41:37"
picture: "IMG_3334.jpg"
weight: "15"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42891
- /details0154.html
imported:
- "2019"
_4images_image_id: "42891"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42891 -->
Ziel dieses Versuchsaufbaus bzw. dieser Konstruktion war es einen Hauptträger für Funktionsmodelle zu konstruieren.

Der Träger sollte dabei drei Funktionen erfüllen:
1.)	Massive Konstruktion im Maß 30 x 30.
2.)	Integration von elektrischen Abzweigkästen in Form von Batteriegehäusen zur Anbindung  elektrischer Geber wie z.B. Reedkontakte (36120).
3.)	Verbleibender Innen- bzw. Hohlraum des Trägers soll als durchgehender Kabelkanal genutzt werden.