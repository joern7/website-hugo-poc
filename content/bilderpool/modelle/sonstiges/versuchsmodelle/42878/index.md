---
layout: "image"
title: "U-Träger-Tragwerk 1"
date: "2016-02-15T22:41:37"
picture: "IMG_3262.jpg"
weight: "2"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42878
- /details4724.html
imported:
- "2019"
_4images_image_id: "42878"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42878 -->
Ziel dieses Versuchsaufbaus bzw. dieser Konstruktion war es, ein äußerst belastbares Tragwerk zu schaffen, wie es beispielsweise zum Bau von Großkranen oder langen freistehenden Brücken benötigt wird.