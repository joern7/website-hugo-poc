---
layout: "image"
title: "Schwenkrollenwagen 2"
date: "2016-02-15T22:41:37"
picture: "IMG_3321.jpg"
weight: "14"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42890
- /details60ae.html
imported:
- "2019"
_4images_image_id: "42890"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42890 -->
Detailansicht eines Schwenkrollenlagers.