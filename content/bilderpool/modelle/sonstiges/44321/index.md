---
layout: "image"
title: "Flexschiene als Energiekette"
date: "2016-08-24T20:35:06"
picture: "energiekette1.jpg"
weight: "23"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44321
- /details8fae.html
imported:
- "2019"
_4images_image_id: "44321"
_4images_cat_id: "323"
_4images_user_id: "2465"
_4images_image_date: "2016-08-24T20:35:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44321 -->
Eine weitere Anwendung der Flexschienen:
Der Verfahrweg ist zwar nicht allzu groß (ca 15cm) aber in manchen Anwendungen könnte das vielleicht eine Alternative zur (viel teureren) Energiekette    
http://ft-datenbank.de/details.php?ArticleVariantId=7b6e24dd-e302-4788-84c4-0da797827ded    
sein.