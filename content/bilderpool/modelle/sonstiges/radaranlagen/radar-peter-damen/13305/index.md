---
layout: "image"
title: "Radar +  4-polige-Jackplug-3,5mm (statt Schleifringen)"
date: "2008-01-11T21:09:58"
picture: "Radar-4-polige-Jackplug-35mm_004.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13305
- /detailsf7e7-2.html
imported:
- "2019"
_4images_image_id: "13305"
_4images_cat_id: "1230"
_4images_user_id: "22"
_4images_image_date: "2008-01-11T21:09:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13305 -->
Radar +  4-polige-Jackplug-3,5mm (statt Schleifringen)