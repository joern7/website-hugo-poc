---
layout: "image"
title: "schleifr01"
date: "2003-04-27T17:46:14"
picture: "schleifr01.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/967
- /details6d1c-3.html
imported:
- "2019"
_4images_image_id: "967"
_4images_cat_id: "28"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=967 -->
