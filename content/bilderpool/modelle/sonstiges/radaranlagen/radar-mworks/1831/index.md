---
layout: "image"
title: "Radar_002"
date: "2003-10-14T11:30:49"
picture: "Radar002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Radar"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1831
- /detailsc75a.html
imported:
- "2019"
_4images_image_id: "1831"
_4images_cat_id: "198"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:30:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1831 -->
Ein frühes Werk (ca. 1972)