---
layout: "image"
title: "Gesamtansicht"
date: "2014-04-27T16:09:00"
picture: "waschstrasse01.jpg"
weight: "1"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38683
- /details4fe1.html
imported:
- "2019"
_4images_image_id: "38683"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38683 -->
