---
layout: "image"
title: "Antrieb Förderband"
date: "2014-04-27T16:09:00"
picture: "waschstrasse14.jpg"
weight: "14"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38696
- /detailsf94d.html
imported:
- "2019"
_4images_image_id: "38696"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38696 -->
Ein 50:1 Powermotor mit einer Schnecke von TST.