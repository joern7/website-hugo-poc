---
layout: "image"
title: "Die Bürsten"
date: "2014-04-27T16:09:00"
picture: "waschstrasse07.jpg"
weight: "7"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38689
- /details18ca.html
imported:
- "2019"
_4images_image_id: "38689"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38689 -->
Die schrägen Bürsten sind klappbar, sie passen sich dem Auto an. Die Senkrechten Bürsten sind angetrieben.
 Die Waagerechte Bürste wird über 2 Lichtschranken gesteuert und fährt hoch und runter.