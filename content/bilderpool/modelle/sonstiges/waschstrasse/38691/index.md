---
layout: "image"
title: "Lichtschranke"
date: "2014-04-27T16:09:00"
picture: "waschstrasse09.jpg"
weight: "9"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38691
- /details9ae1-2.html
imported:
- "2019"
_4images_image_id: "38691"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38691 -->
HIer sieht man die Hoch/Runter Lichtschranken.