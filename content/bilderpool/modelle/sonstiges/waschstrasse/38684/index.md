---
layout: "image"
title: "Gesamtansicht 2"
date: "2014-04-27T16:09:00"
picture: "waschstrasse02.jpg"
weight: "2"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38684
- /details43b7.html
imported:
- "2019"
_4images_image_id: "38684"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38684 -->
Das Auto wird von einem Förderband gezogen.