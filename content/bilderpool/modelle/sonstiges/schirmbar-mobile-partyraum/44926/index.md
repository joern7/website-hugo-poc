---
layout: "image"
title: "Aufbau 1"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum06.jpg"
weight: "6"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44926
- /details7b44.html
imported:
- "2019"
_4images_image_id: "44926"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44926 -->
Zum Aufbauen werden die Seitenteile einfach nach außen geklappt.