---
layout: "image"
title: "Aufbau 4"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum10.jpg"
weight: "10"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44930
- /details3b1b.html
imported:
- "2019"
_4images_image_id: "44930"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44930 -->
Fertig zusammengeschoben und mit aufgebauter Inneneinrichtung.