---
layout: "image"
title: "Zusammen 2"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum05.jpg"
weight: "5"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44925
- /detailsf3ea.html
imported:
- "2019"
_4images_image_id: "44925"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44925 -->
Zum Transport lassen sich die zwei äußeren Hälften in die Mitte klappen so dass es zu einem Anhänger wird, welcher an den LKW passt.