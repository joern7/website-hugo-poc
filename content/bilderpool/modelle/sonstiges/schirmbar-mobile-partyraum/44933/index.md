---
layout: "image"
title: "Aufbau 7"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum13.jpg"
weight: "13"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44933
- /detailsb082-3.html
imported:
- "2019"
_4images_image_id: "44933"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44933 -->
Die Plane ist zur Befestigung einfach über die Zapfen der Winkel und Bausteine gesteckt.