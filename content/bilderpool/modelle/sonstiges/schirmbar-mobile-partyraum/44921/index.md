---
layout: "image"
title: "Gesamtansicht"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum01.jpg"
weight: "1"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44921
- /details1134-2.html
imported:
- "2019"
_4images_image_id: "44921"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44921 -->
Die Schirmbar ist ein mobiler Veranstaltungsraum in Form eines klappbaren Anhängers