---
layout: "image"
title: "Escher's Relativity"
date: "2007-04-09T19:22:56"
picture: "relativity4.jpg"
weight: "7"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- /php/details/10040
- /details7c9d-2.html
imported:
- "2019"
_4images_image_id: "10040"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-04-09T19:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10040 -->
another picture, view from the right