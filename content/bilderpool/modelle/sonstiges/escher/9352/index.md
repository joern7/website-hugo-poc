---
layout: "image"
title: "Penrose"
date: "2007-03-08T22:36:00"
picture: "IMG_1711s.jpg"
weight: "2"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- /php/details/9352
- /details61a7.html
imported:
- "2019"
_4images_image_id: "9352"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-03-08T22:36:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9352 -->
Penrose stairs