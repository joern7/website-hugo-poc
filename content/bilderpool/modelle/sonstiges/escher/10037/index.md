---
layout: "image"
title: "Escher's Relativity"
date: "2007-04-09T12:58:10"
picture: "relativity2_small.jpg"
weight: "5"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- /php/details/10037
- /details9c99.html
imported:
- "2019"
_4images_image_id: "10037"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-04-09T12:58:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10037 -->
Slightly different angle, less distortion