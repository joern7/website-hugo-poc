---
layout: "image"
title: "Chefsessel44"
date: "2011-08-08T21:10:25"
picture: "Chefsessel44.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31549
- /details325c.html
imported:
- "2019"
_4images_image_id: "31549"
_4images_cat_id: "323"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:10:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31549 -->
Die Armlehnen sind nur aufgeklemmt und halten ziemlich schlecht. Die Lösung mit einem Kettenglied drunter und einem BS7,5 hält besser, macht den Sessel aber fürchterlich breit. Die S-Riegel sind 6 mm lang und lassen damit "Luft" für die Neigung der Rückenlehne.
Die Kette wird mit einem "rückwärts" zwischen die schwarzen Glieder montierten Förderglied 37192 in den BS7,5 unter der Sitzfläche gehalten.