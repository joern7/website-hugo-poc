---
layout: "image"
title: "Münzsortierer"
date: "2010-06-20T12:18:05"
picture: "flipper03.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/27535
- /details5e55.html
imported:
- "2019"
_4images_image_id: "27535"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27535 -->
