---
layout: "image"
title: "Spielautomat Draufsicht 2"
date: "2010-04-07T12:40:32"
picture: "spielautomat2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26898
- /details1c90.html
imported:
- "2019"
_4images_image_id: "26898"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26898 -->
