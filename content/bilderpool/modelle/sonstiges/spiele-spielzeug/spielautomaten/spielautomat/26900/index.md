---
layout: "image"
title: "Spielautomat Schieber"
date: "2010-04-07T12:40:32"
picture: "spielautomat4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26900
- /detailsee30.html
imported:
- "2019"
_4images_image_id: "26900"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26900 -->
Dieser Schieber sorgt dafür, dass wenn im Münzbehälter mehr als 20 Münzen drin sind, dass dann alle neueingeworfenen Münzen in den Sammelbehälter fallen, damit der Münzbehälter nicht überläuft.