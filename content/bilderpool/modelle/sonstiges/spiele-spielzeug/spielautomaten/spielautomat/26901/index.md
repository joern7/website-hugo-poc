---
layout: "image"
title: "Spielautomat Münzbehälter"
date: "2010-04-07T12:40:32"
picture: "spielautomat5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26901
- /detailsf5ac.html
imported:
- "2019"
_4images_image_id: "26901"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26901 -->
Hier werden die 2 Cent Münzen &quot;gesammelt&quot; und ggfs ausbezahlt. (in der Regel verspielt man mehr, als man gewinnt)