---
layout: "image"
title: "Ein 'Spieler'"
date: "2012-04-14T18:08:47"
picture: "minitischkicker10.jpg"
weight: "10"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34801
- /details34bc.html
imported:
- "2019"
_4images_image_id: "34801"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34801 -->
Zwischen jedem Spieler-Baustein ist ein Gummi eingeklemmt, welches verhindert, dass sich der Baustein dreht.