---
layout: "image"
title: "Von vorne"
date: "2012-04-14T18:08:46"
picture: "minitischkicker02.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34793
- /detailsd9a0-2.html
imported:
- "2019"
_4images_image_id: "34793"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34793 -->
-