---
layout: "image"
title: "Blick auf das Spielfeld"
date: "2012-04-14T18:08:46"
picture: "minitischkicker03.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34794
- /details62ac.html
imported:
- "2019"
_4images_image_id: "34794"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34794 -->
Zu der Mannschaft gehören 6 Spieler ( in rot und blau unterteilt ).