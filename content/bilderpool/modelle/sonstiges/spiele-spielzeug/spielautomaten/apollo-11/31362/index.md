---
layout: "image"
title: "P3020068"
date: "2011-07-24T16:39:18"
picture: "apollo23.jpg"
weight: "23"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31362
- /detailsca8b.html
imported:
- "2019"
_4images_image_id: "31362"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31362 -->
Closeup of the floating mechanism for approaching the Eagle.