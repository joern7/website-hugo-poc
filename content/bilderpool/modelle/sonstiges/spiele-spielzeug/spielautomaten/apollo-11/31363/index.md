---
layout: "image"
title: "P3020069"
date: "2011-07-24T16:39:18"
picture: "apollo24.jpg"
weight: "24"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31363
- /details52d8.html
imported:
- "2019"
_4images_image_id: "31363"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31363 -->
Firmation of the Eagle with double sided tape.