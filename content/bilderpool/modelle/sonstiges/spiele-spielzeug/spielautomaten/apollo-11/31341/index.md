---
layout: "image"
title: "P3020039"
date: "2011-07-24T16:39:17"
picture: "apollo02.jpg"
weight: "2"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31341
- /details0314.html
imported:
- "2019"
_4images_image_id: "31341"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31341 -->
Same overview picture from another angle.