---
layout: "image"
title: "P3020042"
date: "2011-07-24T16:39:18"
picture: "apollo05.jpg"
weight: "5"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31344
- /details7fa9.html
imported:
- "2019"
_4images_image_id: "31344"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31344 -->
Left the command module and lunar module to the right, without of course launch pad
because that is left on the lunar surface.
In the red box at the top left are three lens lights to illuminate the background.