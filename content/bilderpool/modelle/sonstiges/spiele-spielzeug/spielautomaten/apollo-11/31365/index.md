---
layout: "image"
title: "P3020071"
date: "2011-07-24T16:39:18"
picture: "apollo26.jpg"
weight: "26"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31365
- /details2c95.html
imported:
- "2019"
_4images_image_id: "31365"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31365 -->
Closeup of the remote control.
You have to push on the lamps.