---
layout: "image"
title: "Flipper PD-Holland"
date: "2012-08-10T20:35:09"
picture: "flipperpdholland07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35299
- /details522f.html
imported:
- "2019"
_4images_image_id: "35299"
_4images_cat_id: "2616"
_4images_user_id: "22"
_4images_image_date: "2012-08-10T20:35:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35299 -->
- Rad-Kugellift +
- Schnecken-Kugellift  (= vijzel-lift)  +
- Auto-Flipper