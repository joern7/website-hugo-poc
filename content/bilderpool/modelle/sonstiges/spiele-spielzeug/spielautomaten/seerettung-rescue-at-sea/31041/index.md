---
layout: "image"
title: "tackle2"
date: "2011-07-12T10:37:57"
picture: "tackle2.jpg"
weight: "18"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31041
- /details65db.html
imported:
- "2019"
_4images_image_id: "31041"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T10:37:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31041 -->
