---
layout: "image"
title: "Linktrainer"
date: "2011-05-29T14:42:43"
picture: "total_view.jpg"
weight: "1"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: ["linktrainer"]
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30668
- /detailsd6f4.html
imported:
- "2019"
_4images_image_id: "30668"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30668 -->
The linktrainer was the first flightsimulator. The manufacturer was Edwin A. Link (1904–1981). It was build in 1931 and a great succes. Thousands of pilots were trained on this machine.Especially in the second world war there was a great need ofcourse for safety training.
This fischertechnik model has the same components and features as the real machine.

How the model works:

First you set your destiny by placing the photocel on the plotter above the place to fly to. Departing point always is Amsterdam Airport, Schiphol. Then you choose the weather type, nice weather with a minimum movement or stormy weather with a lot of movements. Then you push the startbutton. You have to hold the bleubox horizontal and at a stabile height with the stick, and by turning the stick, steering in the choosen direction.
When outerst limits occured there are warning sounds and lights. When the lamp under the flightmap of the plotter the photocel has activated, you have
arrived your destination. The farrest destination needs approximately 4 minutes to fly. You have to practice to fly, it is not easy to do, specially with stormy weather. When someone has enough experience he can fly to his destiny within a few minutes and when arrived, he shall have the altmeter on zero. With this model, you can play a competition with friends to reach a destination by setting the time in robopro. It is also possible the automatically movements making worse, especially the direction movement make the competition very exciting!