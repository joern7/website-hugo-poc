---
layout: "image"
title: "tailrotor"
date: "2011-07-12T17:33:43"
picture: "tailrotor.jpg"
weight: "22"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31045
- /detailsb98c-2.html
imported:
- "2019"
_4images_image_id: "31045"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T17:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31045 -->
