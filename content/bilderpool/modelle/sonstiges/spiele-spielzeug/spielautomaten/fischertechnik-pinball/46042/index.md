---
layout: "image"
title: "Der Unterbau"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian15.jpg"
weight: "15"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46042
- /detailsa564-2.html
imported:
- "2019"
_4images_image_id: "46042"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46042 -->
Ich benötigte sowohl für die Aufbauten als auch auf der Unterseite für die Füße Nuten zum Befestigen. Die Lösung war, zwei Grundplatten 500 (32985) mit einem Klemmstift 15 x 4,1 mm (107356) zu verbinden