---
layout: "image"
title: "Kugelförderung"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian48.jpg"
weight: "47"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46075
- /details4853-2.html
imported:
- "2019"
_4images_image_id: "46075"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46075 -->
Hier sieht man, wie die Kugel gefördert wird.