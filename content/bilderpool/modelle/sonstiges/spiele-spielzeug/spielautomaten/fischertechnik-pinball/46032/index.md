---
layout: "image"
title: "Seite rechts"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian05.jpg"
weight: "5"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46032
- /details1b18.html
imported:
- "2019"
_4images_image_id: "46032"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46032 -->
Die Statikplatten wurden am Ende mit Aufklebern versehen.