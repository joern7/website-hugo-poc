---
layout: "image"
title: "Jackpot-Leuchte"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian12.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46039
- /details1da3-2.html
imported:
- "2019"
_4images_image_id: "46039"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46039 -->
Sobald 5.000 Punkte erreicht sind beginnt die Leuchte zu leuchten. Es sind immer 2 Lämpchen über Kreuz miteinander verbunden.