---
layout: "image"
title: "Der Tilt-Sensor"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian45.jpg"
weight: "44"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46072
- /detailscd0e.html
imported:
- "2019"
_4images_image_id: "46072"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46072 -->
Reedkontakt mit Magnetpendel.