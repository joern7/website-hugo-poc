---
layout: "image"
title: "Spielfläche oben"
date: "2017-07-10T19:44:37"
picture: "piratesofthecaribbian26.jpg"
weight: "26"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46053
- /details7112.html
imported:
- "2019"
_4images_image_id: "46053"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:37"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46053 -->
Links und rechts sind 2 Fototransistoren verbaut. Mittig ist ein Tor mit Magnet und Reedkontakt verbaut. Wird durchgespielt, so flackert das Lämpchen und ein Klackern kommt aus dem Soundmodul.