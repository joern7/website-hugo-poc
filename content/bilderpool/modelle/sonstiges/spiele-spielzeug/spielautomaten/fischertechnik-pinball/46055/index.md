---
layout: "image"
title: "Flipper vorne"
date: "2017-07-10T19:44:37"
picture: "piratesofthecaribbian28.jpg"
weight: "28"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46055
- /details005b.html
imported:
- "2019"
_4images_image_id: "46055"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:37"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46055 -->
Hier kann man sehen, wie das Spielfeld unten aussieht. Man kann sehr gut, die am Spielrand links und rechts, verbauten RGB-Stripes sehen.