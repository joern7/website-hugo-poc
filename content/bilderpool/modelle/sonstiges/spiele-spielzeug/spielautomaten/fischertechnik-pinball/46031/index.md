---
layout: "image"
title: "Flipper rechts"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46031
- /details16da.html
imported:
- "2019"
_4images_image_id: "46031"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46031 -->
Der Flipper ist ringsherum mit 90er Statikplatten verkleidet.