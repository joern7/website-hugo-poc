---
layout: "image"
title: "Frontscheibe"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian39.jpg"
weight: "38"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46066
- /details61a6.html
imported:
- "2019"
_4images_image_id: "46066"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46066 -->
Opakes Acrylglas mit Backlightfolie beklebt. Diese wird dann von hinten mit weißen LED-Stripes beleuchtet.

Druck: 
https://www.wir-machen-druck.de/