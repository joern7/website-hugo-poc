---
layout: "image"
title: "Flipper frontal"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian03.jpg"
weight: "3"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46030
- /details7ce9.html
imported:
- "2019"
_4images_image_id: "46030"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46030 -->
Hier sieht man das Spielfeld mit dem Kopfaufbau.