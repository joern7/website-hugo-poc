---
layout: "image"
title: "10 Drehbereich"
date: "2010-05-31T21:14:39"
picture: "m10.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27327
- /details704d.html
imported:
- "2019"
_4images_image_id: "27327"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27327 -->
Das hier ist der Bereich vom Automaten, wo entschieden wird, ob man gewinnt oder verliert. Fällt die Kugel durch eines der drei Räder, fällt sie ganz runter auf die Platten, durchquert die Lichtschranke (über dem Buzzer) und kommt zurück zur Kette.
Wenn sie allerdings in eines der drei Löcher zwischen den Rädern fällt, wird sie solange an dem Kreis aus Grundbausteinen und 30° Winkelstücken entlanggeschleift, bis sie durch die Lücke durch die andere Lichtschranke fällt. Dann rollt sie weiter und fällt auch runter in den Gang, der durch die erste Lichtschranke und dann zur Kette führt. 
Unten ist gut der Buzzer zu erkennen.