---
layout: "image"
title: "26 Der Drehteller"
date: "2010-05-31T21:14:40"
picture: "m26.jpg"
weight: "26"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27343
- /details726d.html
imported:
- "2019"
_4images_image_id: "27343"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27343 -->
Hier wird die Kugel so lange an dem Kreis entlanggeschleift, bis sie durch das Loch und die Lichtschranke fällt. Wenn die Kugel also in so einem Loch landet, hat man verloren.