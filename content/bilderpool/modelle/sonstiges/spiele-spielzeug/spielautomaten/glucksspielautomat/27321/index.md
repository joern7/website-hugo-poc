---
layout: "image"
title: "04 hinten"
date: "2010-05-31T21:14:38"
picture: "m04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27321
- /details7179.html
imported:
- "2019"
_4images_image_id: "27321"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27321 -->
So sieht der ganze Automat von hinten aus. Gut zu sehen ist der Powermotor, der den Drehteller antreibt und die zwei Motoren, die das Geld aus ihren "Haus" rausschieben.