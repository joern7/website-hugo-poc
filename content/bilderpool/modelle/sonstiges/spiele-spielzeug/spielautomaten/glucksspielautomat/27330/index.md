---
layout: "image"
title: "13 gesamte Abschussrampe"
date: "2010-05-31T21:14:39"
picture: "m13.jpg"
weight: "13"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27330
- /details60eb.html
imported:
- "2019"
_4images_image_id: "27330"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27330 -->
Hier ist der gesamte Teil mit der Abschussrampe. Man sieht, wie die Kugel die Gondel (die übrigens gerade falschherum an der Kette hängt) verlässt und zur Abschussrampe rollt. 
Unten an dem Kupplungsstück mit den drei Löchern (#38260) muss man ziehen und wieder loslassen. Das Gummi zieht den ganzen Hebel zurück und schießt die Kugel nach vorne, die dann auf den Drehteller fällt. (Drehteller nicht zu sehen!)