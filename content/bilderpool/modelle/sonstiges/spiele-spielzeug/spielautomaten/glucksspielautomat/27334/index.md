---
layout: "image"
title: "17 Blick auf die Gondel"
date: "2010-05-31T21:14:39"
picture: "m17.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27334
- /details03d9.html
imported:
- "2019"
_4images_image_id: "27334"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27334 -->
Von oben ein Blick nach unten zur Gondel. In ihr ist gerade die Kugel. 
Die Kette macht eine viertel Drehung, wie man hier gut erkennt.