---
layout: "image"
title: "16 unterer Teil"
date: "2010-05-31T21:14:39"
picture: "m16.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27333
- /detailse997.html
imported:
- "2019"
_4images_image_id: "27333"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27333 -->
Der Hebel der Abschussrampe, an dem man ziehen muss, ist hier rechts zu sehen. 
Das große Z40 dient als Impulszähler, da ist keinen mehr hatte. Kann man auf den nächsten Bildern besser sehen.
Der U-Träger ist für die Befestigung der Kette oben zuständig.