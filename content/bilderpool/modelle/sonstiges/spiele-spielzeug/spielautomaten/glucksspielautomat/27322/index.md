---
layout: "image"
title: "05 Geldhaus"
date: "2010-05-31T21:14:39"
picture: "m05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27322
- /detailsd4e6-2.html
imported:
- "2019"
_4images_image_id: "27322"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27322 -->
Wenn das Geld rausgeschoben wird, fällt es auf das Förderband, das es dann zur Entnahme führt. 
Unter dem Förderband ist der Not-Aus-Schalter.