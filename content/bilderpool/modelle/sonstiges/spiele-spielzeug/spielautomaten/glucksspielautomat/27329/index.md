---
layout: "image"
title: "12 Drehteller"
date: "2010-05-31T21:14:39"
picture: "m12.jpg"
weight: "12"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27329
- /details5f07.html
imported:
- "2019"
_4images_image_id: "27329"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27329 -->
Das ist der Drehteller. Die sechs Löcher kann man gut erkennen  und man sieht auch die drei "falschen" Löcher, in denen die Kugel am Kreis entlangschleift.
Wenn die Kugel die Abschussrampe verlässt, kommt sie zuerst auf das 6-Eck aus den Grundbausteinen, bis sie schließlich auf den eigentlichen Teller mit den sechs Löchern fällt.