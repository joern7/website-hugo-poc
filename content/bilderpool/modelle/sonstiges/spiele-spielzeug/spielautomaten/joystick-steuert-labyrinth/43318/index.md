---
layout: "image"
title: "Übersicht"
date: "2016-04-26T13:19:44"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/43318
- /detailsed93.html
imported:
- "2019"
_4images_image_id: "43318"
_4images_cat_id: "3216"
_4images_user_id: "891"
_4images_image_date: "2016-04-26T13:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43318 -->
Manche mag die grauen Bausteine (und die Silberlinge erstrecht) altmodisch finden, aber ich mag sie.