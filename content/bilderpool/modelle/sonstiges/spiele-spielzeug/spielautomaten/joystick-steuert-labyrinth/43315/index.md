---
layout: "image"
title: "Joystick Steuerung"
date: "2016-04-26T13:19:44"
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: ["Joystick", "Silberlinge"]
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/43315
- /details9ba8.html
imported:
- "2019"
_4images_image_id: "43315"
_4images_cat_id: "3216"
_4images_user_id: "891"
_4images_image_date: "2016-04-26T13:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43315 -->
Hier sieht man eine klassische Joystick Steuerung mit Silberlingen. Es ist aber eigentlich nicht so kompliziert, wie man befürchten könnte. Joystick besteht hauptsächlich aus sechs Tastern. Mit diesem Joystick steuert man die beiden Motoren, die das Labyrinth horizontal und vertikal neigen. Vier der Taster aktivieren den jeweiligen Motor. Die beiden zusätzlichen Taster aktivieren jeweils einen der Relais. So dreht sich z.B. der vertikale Motor entweder nach links oder nach rechts.
Ich hatte ursprünglich (wie bei ft üblich) die Stecker ineinander gesteckt, was ich jedoch unübersichtlich fand. So habe ich die Steuerung umgebaut, damit nirgendswo Stecker ineinander gesteckt sind und damit die Übersichtlichkeit erhöht wird.
Rechts oben sind die Steckplätze, wo die Stromversorgung sowie die beiden Motoren angeschlossen werden. So kann man zum einen für Zugentlastung z.B. beim Transport gesorgt, zum anderen weiß man wo man die Stecker wieder reinstecken muss, wenn man sie mal abgemacht hat.
Mit einer Robo Interface/Controller könnte man sicherlich den Verkabelungsaufwand reduzieren, aber die Silberlinge haben schon auch ihren Reiz. (Außerdem konnte sich der Experimentiertfeld-Silberling hier mal gut in Szene setzen.)