---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "027.jpg"
weight: "27"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15892
- /detailsae0e.html
imported:
- "2019"
_4images_image_id: "15892"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15892 -->
ein Teil der Arbeitsplattform ragt unter die erste der beiden gelben Bauplatte 30x45 und verhindert somit das sie sich anheben kann.