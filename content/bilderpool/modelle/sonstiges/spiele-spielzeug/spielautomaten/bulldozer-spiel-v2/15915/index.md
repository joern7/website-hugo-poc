---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:10"
picture: "050.jpg"
weight: "50"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15915
- /detailsaa49.html
imported:
- "2019"
_4images_image_id: "15915"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15915 -->
Hier die Beleuchtung.
Aus Platzgründen musste ich auf die Stecker verzichten.