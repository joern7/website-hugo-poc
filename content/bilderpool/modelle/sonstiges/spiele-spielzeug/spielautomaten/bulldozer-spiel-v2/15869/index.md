---
layout: "image"
title: "Raupen Ansicht"
date: "2008-10-10T12:29:00"
picture: "004.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15869
- /detailse731.html
imported:
- "2019"
_4images_image_id: "15869"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T12:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15869 -->
Den Bulldozer habe ich ähnlich wie den aus dem Power Bulldozer Baukasten aufgebaut.
Und dann etwas aufgemotzt.
Die sieben weißen Steine werden automatisch, pneumatisch ausgeworfen.