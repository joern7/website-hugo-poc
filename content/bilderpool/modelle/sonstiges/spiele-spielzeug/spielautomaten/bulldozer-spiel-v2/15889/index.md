---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "024.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15889
- /details7608-2.html
imported:
- "2019"
_4images_image_id: "15889"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15889 -->
Die Pneumatik ist eingebaut und nun bereite ich das Ganze vor damit ich die Arbeitsplattform montieren kann.