---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "032.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15897
- /details278a.html
imported:
- "2019"
_4images_image_id: "15897"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15897 -->
Ansicht von unten unter das Modell.
Ich habe hier mal eines der Förderbänder getrennt damit man die Konstruktion erkennen kann.
Hier sieht es etwas aufgeräumter aus als beim ersten Modell.
Das kommt daher weil der Bulldozer einen eigenen Antrieb hat.