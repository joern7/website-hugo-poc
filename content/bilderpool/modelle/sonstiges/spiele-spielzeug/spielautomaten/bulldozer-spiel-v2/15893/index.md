---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "028.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15893
- /detailse68a.html
imported:
- "2019"
_4images_image_id: "15893"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15893 -->
Das ist sie, die Arbeitsplattform.
Die beiden Riegelsteine greifen unter die am Modell montierten Bauplatten. Mit den Verbindern 15 kann man sie zusätzlich noch vorn am Modell befestigen.