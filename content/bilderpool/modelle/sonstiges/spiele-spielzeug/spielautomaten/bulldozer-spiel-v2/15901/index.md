---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "036.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15901
- /detailsefde-2.html
imported:
- "2019"
_4images_image_id: "15901"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15901 -->
die roten Bauplatten 15x60 die man am Schieber, vor den schwarzen Bauplatten 30x30 erkennt, passen zufällig genau in die Nut der gelben Bauplatten die sich an den Spielfeldrändern befinden.