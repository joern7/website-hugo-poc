---
layout: "image"
title: "Detail Auffangkorb"
date: "2013-02-14T13:45:40"
picture: "DSCN4966.jpg"
weight: "63"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36620
- /details27b2.html
imported:
- "2019"
_4images_image_id: "36620"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36620 -->
