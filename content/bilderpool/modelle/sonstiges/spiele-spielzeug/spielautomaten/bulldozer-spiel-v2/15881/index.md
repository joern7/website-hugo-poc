---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:08"
picture: "016.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15881
- /details70d0-2.html
imported:
- "2019"
_4images_image_id: "15881"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15881 -->
Zwei der Schächte die die herunterfallenden Steine aufnehmen.
Die Steine die vorn herunterfallen rutschen über die Schräge,
die anderen fallen direkt auf das Förderband.