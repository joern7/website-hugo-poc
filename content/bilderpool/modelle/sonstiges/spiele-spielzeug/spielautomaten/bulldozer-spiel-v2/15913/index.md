---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "048.jpg"
weight: "48"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15913
- /detailsd5fb-3.html
imported:
- "2019"
_4images_image_id: "15913"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15913 -->
Bulldozer Ansicht einer früheren Version