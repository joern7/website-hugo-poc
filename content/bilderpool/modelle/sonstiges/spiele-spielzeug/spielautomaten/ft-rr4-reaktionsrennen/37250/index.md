---
layout: "image"
title: "ft-RR4-11: Programm-Bildschirm"
date: "2013-08-21T21:30:52"
picture: "l11.jpg"
weight: "11"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37250
- /detailsdf83-2.html
imported:
- "2019"
_4images_image_id: "37250"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:52"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37250 -->
Alle Bedienknöpfe und Anzeigen sind auch auf dem Monitor abgebildet.