---
layout: "comment"
hidden: true
title: "18254"
date: "2013-08-24T18:39:28"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Wirklich eine sehr originelle Modellidee und schöne Umsetzung - hoffentlich sehen wir es auf der Convention!
Gruß, Dirk