---
layout: "image"
title: "ft-RR4-01: Gesamtansicht"
date: "2013-08-21T21:30:37"
picture: "l01.jpg"
weight: "1"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37240
- /detailsd669.html
imported:
- "2019"
_4images_image_id: "37240"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37240 -->
Drei Spieler versuchen durch Konzentration und schnelle Reaktion ihr Auto ins Ziel zu bringen.

Spielprinzip:
Jeder Spieler erhält einen Handtaster.
Nach dem Aufleuchten eines roten Lichtbandes muss der Taster so schnell wie möglich gedrückt werden.
Das Auto des Spielers mit der schnellsten Reaktion fährt einige Schritte vor.
Bei einem Frühstart fährt das Auto einige Schritte zurück.
Wenn das erste Auto das Ziel erreicht, steht der Sieger fest.