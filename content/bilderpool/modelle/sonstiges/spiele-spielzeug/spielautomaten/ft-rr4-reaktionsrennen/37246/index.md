---
layout: "image"
title: "ft-RR4-07: Antrieb"
date: "2013-08-21T21:30:38"
picture: "l07.jpg"
weight: "7"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37246
- /detailsc9dd.html
imported:
- "2019"
_4images_image_id: "37246"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37246 -->
Die Fahrbahn wurde entfernt. Links befindet sich der Motor für Bahn 2, rechts der für Bahn 3.