---
layout: "image"
title: "ft-RR4-06: Zielbereich"
date: "2013-08-21T21:30:38"
picture: "l06.jpg"
weight: "6"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37245
- /details788b.html
imported:
- "2019"
_4images_image_id: "37245"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37245 -->
Hier befinden sich die Zieltaster und Anzeigelampen.