---
layout: "image"
title: "ft-RR4-04: Modell aufgeklappt"
date: "2013-08-21T21:30:38"
picture: "l04.jpg"
weight: "4"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37243
- /details7b5f.html
imported:
- "2019"
_4images_image_id: "37243"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37243 -->
Für Servicezwecke können die Rückwand und die Seitenwände aufgeklappt werden.