---
layout: "image"
title: "ft-RR4-05: Startbereich"
date: "2013-08-21T21:30:38"
picture: "l05.jpg"
weight: "5"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37244
- /details393f.html
imported:
- "2019"
_4images_image_id: "37244"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37244 -->
Die Startschalter stellen die Startposition der Autos fest.
Die Schaltscheiben betätigen Schalter, die die Impulse für die Bewegungskontrolle liefern.