---
layout: "image"
title: "Flipper4 - Verdrahtungsplan"
date: "2012-06-06T22:26:53"
picture: "flipper14.jpg"
weight: "14"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35037
- /details3cf5.html
imported:
- "2019"
_4images_image_id: "35037"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35037 -->
Robo-Interface:
für die Sensoren und Motoren

Erweiterungsmodul 1:
für die Ventile und Lichtschrankenbeleuchtungen

Erweiterungsmodul 2:
für die Lichteffekte und parallel dazu die Toneffekte via Soundmodul