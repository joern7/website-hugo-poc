---
layout: "image"
title: "Flipper4 - Interfaces"
date: "2012-06-06T22:26:53"
picture: "flipper13.jpg"
weight: "13"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35036
- /details5006.html
imported:
- "2019"
_4images_image_id: "35036"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35036 -->
Ich benutze ein Robo-Interface mit 2 Erweiterungsmodulen. So kann der Strombedarf von etwa 2A auf 3 Stecker-Netzteile verteilt werden.
Ganz links sind 3 Sound-Module für 8 Toneffekte zu sehen.