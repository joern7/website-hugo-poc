---
layout: "image"
title: "Flipper4 - Gesamtansicht"
date: "2012-06-06T22:26:53"
picture: "flipper01.jpg"
weight: "1"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35024
- /details89e6.html
imported:
- "2019"
_4images_image_id: "35024"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35024 -->
Die Spielfläche ist 2 x 4 Grundplatten 30385 groß.
