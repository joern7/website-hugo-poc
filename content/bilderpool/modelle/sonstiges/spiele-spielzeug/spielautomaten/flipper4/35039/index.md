---
layout: "image"
title: "Flipper4 - Anzeige"
date: "2012-06-06T22:26:53"
picture: "flipper16.jpg"
weight: "16"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35039
- /details6cb9-2.html
imported:
- "2019"
_4images_image_id: "35039"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35039 -->
Das Bedienfeld von RoboPro wird als Anzeige benutzt.