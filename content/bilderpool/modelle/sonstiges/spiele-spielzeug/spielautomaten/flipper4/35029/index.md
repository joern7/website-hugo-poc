---
layout: "image"
title: "Flipper4 - Schaukel"
date: "2012-06-06T22:26:53"
picture: "flipper06.jpg"
weight: "6"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35029
- /detailse9b6-2.html
imported:
- "2019"
_4images_image_id: "35029"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35029 -->
Die Schaukel wird von der Kugel angestoßen und die Lichtschranke wird ausgelöst.