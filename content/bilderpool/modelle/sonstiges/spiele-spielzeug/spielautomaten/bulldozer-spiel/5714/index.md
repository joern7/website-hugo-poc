---
layout: "image"
title: "Speicher"
date: "2006-02-01T14:21:51"
picture: "DSCN0674.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5714
- /details4a06.html
imported:
- "2019"
_4images_image_id: "5714"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5714 -->
Der Luftspeicher ist deshalb so groß weil er auf der Modellschau in Steinfurt noch ein weiteres Modell versorgen musste.