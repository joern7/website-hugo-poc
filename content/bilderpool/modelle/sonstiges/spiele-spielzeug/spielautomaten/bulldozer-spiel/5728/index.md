---
layout: "image"
title: "Details"
date: "2006-02-01T14:22:01"
picture: "Frderbnder_und_Schchte.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5728
- /detailsa044.html
imported:
- "2019"
_4images_image_id: "5728"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5728 -->
eine frühe Bauphase