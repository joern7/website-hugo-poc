---
layout: "image"
title: "Ansicht (1)"
date: "2006-02-01T14:21:40"
picture: "DSCN0644.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5700
- /details76c1.html
imported:
- "2019"
_4images_image_id: "5700"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5700 -->
Das Bulldozerspiel ist ein Glücksspiel. Man versucht möglichst viele weiße Spielsteine zu bekommen.