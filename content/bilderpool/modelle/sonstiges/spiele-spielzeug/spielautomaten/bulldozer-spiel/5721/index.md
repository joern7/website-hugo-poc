---
layout: "image"
title: "Bulldozer (2)"
date: "2006-02-01T14:22:01"
picture: "DSCN0663.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5721
- /detailsb529.html
imported:
- "2019"
_4images_image_id: "5721"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5721 -->
Die Magnetventiele hinter den roten Platten steuern das Ganze.