---
layout: "image"
title: "Schaltzentrale"
date: "2006-02-01T14:21:51"
picture: "DSCN0659.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5718
- /details2e5f-2.html
imported:
- "2019"
_4images_image_id: "5718"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5718 -->
Ich habe hier jedem Stromverbrauchen einen eigenen Schalter zugewiesen.