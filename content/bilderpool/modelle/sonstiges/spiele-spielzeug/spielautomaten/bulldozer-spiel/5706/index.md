---
layout: "image"
title: "Ansicht (6)"
date: "2006-02-01T14:21:41"
picture: "DSCN0657.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5706
- /detailsd7e7.html
imported:
- "2019"
_4images_image_id: "5706"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5706 -->
Hier erkemmt man den eigentlichen Antrieb des Bulldozers.
Die beiden Ketten laufen rund, sie "nehmen die Statikstreben mit" und treiben den Bulldozer so an.