---
layout: "overview"
title: "Flipper (flyingcat)"
date: 2020-02-22T08:31:15+01:00
legacy_id:
- /php/categories/1520
- /categories2324.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1520 --> 
Ich habe mir in den Kopf gesetzt, einen Flipper - annähernd in Originalgröße - zu bauen. Das Projekt ist noch lange nicht fertig; dennoch gibt es hier schon Bilder der aktuellen Bauphase zu sehen - vielleicht hat ja jemand Anregungen dazu.