---
layout: "image"
title: "L. VUK, Loch & linke Rampe"
date: "2015-09-02T20:01:59"
picture: "bild04_5.jpg"
weight: "115"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41880
- /details29f5.html
imported:
- "2019"
_4images_image_id: "41880"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41880 -->
Der linke VUK, hier im Bild der rote Klotz, hat übrigens ganz gut funktioniert. Er schleudert eine Kugel von unten nach obenauf die Rampe. Von wo unten? Vom geheimen Loch! Wo das ist? Sag ich nicht, es wäre sonst nicht geheim ;) dieses hat übrigens auch eine Art Weiche bekommen, sodass die Kugel kontrolliert in das Loch gelenkt werden kann.

Das Auswurfloch hat noch ein Gesicht bekommen. Darf ich vorstellen: Crazy Bob! ;-) Seine Augen können leuchten, es sind dort rote LEDs eingebaut.

An der Rampe hat sich nichts verändert.