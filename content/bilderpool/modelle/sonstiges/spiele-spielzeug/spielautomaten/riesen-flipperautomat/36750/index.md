---
layout: "image"
title: "Beleuchtung 1 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild06_3.jpg"
weight: "57"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36750
- /details9cf7-2.html
imported:
- "2019"
_4images_image_id: "36750"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36750 -->
Hier sieht man die Beleuchtung im Bereich der rechten Innen- und Außenbahn. Die beiden vorderen Linsenlampen sind für die Lichtschranken.