---
layout: "image"
title: "Vorderseite (noch im Bau)"
date: "2013-02-03T10:19:44"
picture: "bild01.jpg"
weight: "2"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36543
- /details63e6.html
imported:
- "2019"
_4images_image_id: "36543"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36543 -->
Hier sieht man die Vorderseite des Flippers.