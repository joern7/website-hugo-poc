---
layout: "image"
title: "Rampe, überarbeitet 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild12_3.jpg"
weight: "63"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36756
- /detailsd869.html
imported:
- "2019"
_4images_image_id: "36756"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36756 -->
Der Wendepunkt der Rampe. Die rote Platte 15x60 ist etwas schräg, sodass die Kugel nicht zurückrollen oder herausfallen kann.