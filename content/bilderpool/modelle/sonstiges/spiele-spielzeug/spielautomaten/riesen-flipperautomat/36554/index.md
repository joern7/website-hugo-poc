---
layout: "image"
title: "'Bel. Kickback'-Ziele & Drehziel"
date: "2013-02-03T10:19:45"
picture: "bild12.jpg"
weight: "13"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36554
- /details2f29.html
imported:
- "2019"
_4images_image_id: "36554"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36554 -->
In dieser Testversion sind diese drei Ziele noch nicht in Benutzung. Trifft die Kugel eine der beiden weißen Scheiben, wird ein Kontakt geschlossen. Hat sie beide getroffen, wird der Kickback aktiviert.
Das Drehziel wertet Punkte, wenn die Kugel unter ihm durchrollt und es in Drehung versetzt.