---
layout: "image"
title: "TX-Controller (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild17.jpg"
weight: "68"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36761
- /detailsb4c8.html
imported:
- "2019"
_4images_image_id: "36761"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36761 -->
Das Master-TX ist jetzt an der linken Seite des Flippers, nicht mehr mittig unter dem Spielfeld. Man kommt so einfach besser dran.