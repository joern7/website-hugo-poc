---
layout: "image"
title: "Beleuchtung 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild07_3.jpg"
weight: "58"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36751
- /details3319-2.html
imported:
- "2019"
_4images_image_id: "36751"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36751 -->
Natürlich gibt's auch auf der anderen Seite "Beleuchtung". Beide Linsenlampen werden für Lichtschranken benötigt.