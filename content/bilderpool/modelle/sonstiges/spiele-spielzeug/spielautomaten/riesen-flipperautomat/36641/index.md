---
layout: "image"
title: "Slingshot 2 (noch im Bau)"
date: "2013-02-17T23:43:44"
picture: "bild3_2.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36641
- /details96ee-2.html
imported:
- "2019"
_4images_image_id: "36641"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36641 -->
Ein schärferes und weniger verwackeltes Bild. Dafür sieht man die Metallachse nicht...