---
layout: "image"
title: "Ordnung (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild18.jpg"
weight: "69"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36762
- /details2a61-2.html
imported:
- "2019"
_4images_image_id: "36762"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36762 -->
...Ordnung? Ich hoffe es. Ich versuche jetzt mit einer neuen Verkabelung mehr Ordnung im Flipperinnerem zu schaffen, die Kabel an den Stützen entlang zu führen, damit man im Notfall auch mal leicht reingreifen kann.