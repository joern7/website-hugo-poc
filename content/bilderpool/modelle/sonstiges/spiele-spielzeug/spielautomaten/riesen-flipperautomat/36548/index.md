---
layout: "image"
title: "Flipperknopf (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild06.jpg"
weight: "7"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36548
- /detailsf526.html
imported:
- "2019"
_4images_image_id: "36548"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36548 -->
Der Spieler drückt auf das Zahnrad, welches über eine Achse den Taster betätigt.