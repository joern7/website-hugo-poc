---
layout: "image"
title: "Rampe, überarbeitet 1 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild11_3.jpg"
weight: "62"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36755
- /detailsa81d-3.html
imported:
- "2019"
_4images_image_id: "36755"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36755 -->
Die alte Rampe war zu steil. Diese hier ist etwas flacher, trotzdem braucht die Kugel ein bisschen Schwung. Das schwarze Drehdingsbumms wird über eine Kette von einem Powermoter 8:1 angetrieben.