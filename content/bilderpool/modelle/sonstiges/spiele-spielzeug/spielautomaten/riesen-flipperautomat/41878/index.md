---
layout: "image"
title: "Das Plakat im Detail"
date: "2015-09-02T20:01:59"
picture: "bild02_5.jpg"
weight: "113"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41878
- /details9a4b.html
imported:
- "2019"
_4images_image_id: "41878"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41878 -->
An dieser Stelle möchte ich mich nochmal bei meinem Freund Philipp bedanken, der mir dieses Bildchen gestaltet hat. Später soll das noch auf Plexiglas oder so gedruckt werden und von hinten beleuchtet werden.

....es wird mehr von dem schwarzblauem sichtbar... :-)