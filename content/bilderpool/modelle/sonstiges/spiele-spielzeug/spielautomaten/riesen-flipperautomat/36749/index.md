---
layout: "image"
title: "Flipperfinger oben (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild05_3.jpg"
weight: "56"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36749
- /details97b5.html
imported:
- "2019"
_4images_image_id: "36749"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36749 -->
Linker Flipperfinger in oberer Stellung.