---
layout: "image"
title: "Gesamtansicht 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild03_3.jpg"
weight: "54"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36747
- /detailse3f5.html
imported:
- "2019"
_4images_image_id: "36747"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36747 -->
Mal wieder eine Gesamtansicht, sogar mit eingeschalteter Beleuchtung (die noch längst nicht komplett ist). Seit der letzten Gesamtansicht hat sich viel getan...
Hier habe ich auch ein paar Daten:

Länge: (bisher): 1 Meter
Breite: 43,5 cm
7 P-Zylinder
18 Lampen, davon 2 LEDs
7 Taster
1 Motor
8 Alu-Profile

Als nächstes werde ich die Fallziele neben der Rampe einbauen und vernünftig befestigen und Fotos vom funktionierendem Reset-Mechanismus machen (2 Powermotoren!). Danach kommt der Kran, der bestimmt ein ganz nettes und lustiges Tischobjekt wird... er bewegt einen BS 30 auf dem Tisch hin und her...