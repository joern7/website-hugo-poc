---
layout: "image"
title: "Flipper hochkant (noch im Bau)"
date: "2013-03-17T22:29:29"
picture: "bild1_4.jpg"
weight: "71"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36768
- /details48e3.html
imported:
- "2019"
_4images_image_id: "36768"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-17T22:29:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36768 -->
Na ja, Gesamtansichten hat's ja genug. Ich wollte einfach mal zeigen, dass man den Flipper auch hochkant hinstellen kann. So komme ich *viel* einfacher an die Innereien und kann alles besser verkabeln.