---
layout: "image"
title: "Flipperfinger-Mechanismus 2"
date: "2013-10-03T09:29:05"
picture: "bild04_4.jpg"
weight: "93"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37498
- /details5a8b.html
imported:
- "2019"
_4images_image_id: "37498"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37498 -->
Hier sieht man etwas weiter links von der Bildmitte die untere Winkelachse des linken Flipperfingers. Gut zu erkennen ist auch das Messingteil, was ich mir auf der Convention am Samstag vom TST gekauft hab (weiß grad nicht wie es heißt). Ohne dieses tolle Teil würde der Flipperfinger nie gut funktionieren.