---
layout: "image"
title: "Rechte Rampe 3 (noch im Bau)"
date: "2013-02-04T10:32:23"
picture: "bild3.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36578
- /detailsaa52-3.html
imported:
- "2019"
_4images_image_id: "36578"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36578 -->
Eine andere Ansicht der Rampe. Die BS 30, die an den gelben Dingern (keine Ahnung wie diese biegbaren Dinger heißen) befestigt sind, sind im 30° Winkel befestigt. Ich hoffe SEHR, dass die Flipperfinger stark genug sind, um die Kugel da hoch zu befördern.