---
layout: "image"
title: "'Roll-Over' 2"
date: "2015-09-02T20:01:59"
picture: "bild13_3.jpg"
weight: "124"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41889
- /details4d03.html
imported:
- "2019"
_4images_image_id: "41889"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41889 -->
Andere Seite. Hier sieht man die Metallstreifen, die von dem Draht zusammengedrückt werden.