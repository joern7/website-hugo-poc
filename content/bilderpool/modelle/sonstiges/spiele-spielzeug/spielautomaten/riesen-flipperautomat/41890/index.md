---
layout: "image"
title: "Die Elektronik"
date: "2015-09-02T20:01:59"
picture: "bild14_3.jpg"
weight: "125"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41890
- /details2ea5.html
imported:
- "2019"
_4images_image_id: "41890"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41890 -->
So schauts im Moment aus. Wir beginnen mal oben links, die Platine mit den orangenen und blauen Kabeln: das ist die Lampentreiberplatine. Ziemlich einfach, denn es sind nur ULN-Treiber. Im großen Flipper sind auf der Platine u. a. auch Schutzschaltungen mit Shuntwiderstand vorhanden, aber da wird auch mit 18V und Glühbirnen gearbeitet.
Rechts nebendran sehen wir das Arduino Mega 2560-Board, welches für die Steuerung des ganzen Flippers verantwortlich ist. Es hängt sich nur manchmal im Programm auf. Das Board kann ganz einfach aus der Halterung entfernt werden, man muss dazu nur die Kabel aus den Buchsen ziehen, den Federgelenkstein nach rechts drücken und das Board nach rechts unten herausbewegen.
Zwischen den beiden Platinen ist eine weitere, sehr kleine Platine, von der grüne und gelbe Kabel abgehen, Das ist die Schaltersteuerplatine. Im Prinzip aber nichts anderes als ein ATmega328 und zwei Keramikkondensatoren. Er dient als Portexpander: Über ein Matrixsystem kann er 64 Schalter abfragen und schickt die Schalternummer über I2C an den Arduino Mega. Links davon sind Buchsen für Schalter, die nicht über die Schaltermatrix abgefragt werden. Das sind z. B. die Flipperknöpfe oder das Tiltpendel.
Unterhalb des Arduino Megas befindet sich der Spulentreiber. Das sind 10 MOSFETs, die vom Arduino direkt über Optokoppler geschaltet werden. Eine Masseverbindung zwischen dem "Hochstromkreis" und dem "Niederstromkreis" besteht nicht.
Weiter unten, sowohl links als auch mittig befinden sich die Kabelverbinder. Wenn man die Backbox mal abbauen muss, zieht man einfach alle Stecker raus und steckt sie nach der Montage der Backbox wieder rein. Die Kabel sind übrigens lang genug, sodass man die Backbox auch ohne die Stecker abzuziehen herunterklappen kann. Die 9-poligen SubD-Verbinder (so heißen die glaube ich) sind jeweils auf 8 Pins reduziert worden. Dieser fehlende Pin ist bei jedem Stecker an einer anderen Stelle, und in der zugehörigen Buchse ist an der Stelle wo im Stecker der Pin fehlt das Loch mit Drähten zugestopft. So kann man nicht versehentlich die Schalter an den Lampentreiber anschließen.
Schließlich ist ganz rechts noch der Teil mit der Stromversorgung. Vom vielzu kleinen Trafo, der am Alugerüst unter dem Spielfeld montiert ist wird die Wechselspannung gleichgerichtet und an die verschiedenen Verbraucher weitergeleitet. Mal ganz nebenbei: Die Kühlkörper sind alle aus Altgeräten ausgebaut, der lange, oben rausstehende ist in Wirklichkeit nur ein Metallstück welches sich aber prima als Kühler verwenden lässt ;)