---
layout: "image"
title: "Auswurfloch von vorne (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild6.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36581
- /details66db.html
imported:
- "2019"
_4images_image_id: "36581"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36581 -->
Das wird wohl einer der besten Spielfeldkomponenten auf dem Flipper sein: ein Auswurfloch! Wird die Kugel dort hineingeschossen, wird sie durch einen P-Zylinder wieder ausgeworfen. Und zwar heftig!