---
layout: "image"
title: "Fallziele 5 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild06_2.jpg"
weight: "45"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36737
- /detailsf1bf.html
imported:
- "2019"
_4images_image_id: "36737"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36737 -->
Unten ist der Reset-Balken gut zu erkennen, der sich nach oben bewegt, um die Ziele zu resetten.