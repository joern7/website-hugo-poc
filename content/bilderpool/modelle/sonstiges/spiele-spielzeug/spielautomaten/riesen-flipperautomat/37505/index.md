---
layout: "image"
title: "VUK links"
date: "2013-10-03T09:29:05"
picture: "bild11_4.jpg"
weight: "100"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37505
- /details8d60.html
imported:
- "2019"
_4images_image_id: "37505"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37505 -->
Einer der beiden Up-Kicker auf dem Flipper. VUK = Vertical-Up-Kicker. Dieser VUK schießt Kugeln von unter dem Spielfeld auf die linke Rampe. Zu diesem VUK gelangt die Kugel nur über das geheime Loch. (Vom geheimen Loch hab ich natürlich kein Foto gemacht, sonst wäre es ja nicht geheim...)