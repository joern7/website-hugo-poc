---
layout: "image"
title: "Auswurfloch eingebaut (noch im Bau)"
date: "2013-02-17T23:43:44"
picture: "bild1_2.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36639
- /detailsb939.html
imported:
- "2019"
_4images_image_id: "36639"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36639 -->
Nun ist das Auswurfloch über den "Bel. Kickback"-Zielen eingebaut.