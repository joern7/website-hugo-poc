---
layout: "image"
title: "Platte 1"
date: "2014-06-03T22:44:12"
picture: "bild1_7.jpg"
weight: "101"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/38909
- /details25a3-4.html
imported:
- "2019"
_4images_image_id: "38909"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-06-03T22:44:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38909 -->
Ich habe endlich eine ganz gute Platte für den Flipper gefunden. Es ist eine 2mm starke Plexiglasplatte. Ursprünglich hatte sie die Maße 100x50cm, auf dem Bild ist sie aber schon passend zugeschnitten. Es fehlen noch die Aussparungen für die Teile des Flippers, die aus dem Spielfeld herausragen, also Pfosten, etc. Wegen der Dicke muss ich später alle Objekte etwas höher machen. Für die die wissen wollen, wie ich die Platte bearbeitet habe: Mit einer Stichsäge und einem passenden Sägeblatt. Klappt super. Saubere Kanten.
Um die Umrisse einzeichnen zu können, hab ich schon mal angefangen, einige Spielfeldteile teilweise abgebaut, darunter die hüpfenden Männchen und die linke Rampe.
Übrigens: Der Kran ist erst mal wieder runtergekommen, dafür habe ich neue Fallziele eingebaut. Ich habe kaum eine Möglichkeit gefunden, den Kran zu befestigen. Außerdem war er neben dem Zentrum und den Männchen (die allerdings 100% bleiben) einer der Elemente, die ich mir aus einem der großen Flipper abgeschaut und wieder weggemacht habe, weil ich lieber eigene Sachen einbaue. Aber so wie er jetzt aussieht, bin ich ganz zufrieden.