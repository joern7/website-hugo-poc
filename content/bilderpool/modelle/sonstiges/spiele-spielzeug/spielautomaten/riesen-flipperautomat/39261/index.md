---
layout: "image"
title: "Gesamt - Spielbereit! (Aber immer noch nicht fertig)"
date: "2014-08-20T17:27:31"
picture: "bild7_4.jpg"
weight: "111"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39261
- /details360e-2.html
imported:
- "2019"
_4images_image_id: "39261"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39261 -->
Endlich ist er soweit - Der Flipper ist spielbar! Die Flipperfinger und die Schlagtürme funktionieren, und mit ein paar zusätzlichen Teilen (Relais, Widerstand...) funktioniert sogar der Kickback. Die Flipperfinger werden einfach durch die Taster an den Seiten links und rechts direkt betätigt, die Schlagtürme werden per TX gesteuert (den sieht man oben links zur Hälfte). Außerdem ist am Auswurfloch ebenfalls ein Taster angeschlossen, mit dem man die Kugel - falls sie bei einem Testspiel dort hineingerät - manuell wieder auswerfen lassen kann.
Von der fertigen Steuerung ist hier aber noch keine Spur - alle Tischobjekte, außer den Schlagtürmen, werden von einem Mikrocontroller gesteuert.
Die Plexiglasplatte bringt übrigens noch einen Riesenvorteil mit sich: Ich kann die irre vielen Streben, die die damalige Papierplatte halten sollten, in der Mitte entfernen, sodass es wieder ein bisschen übersichtlicher und sauberer wird. Und die Teile habe ich dadurch gespart!