---
layout: "image"
title: "Slingshot (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild11.jpg"
weight: "12"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36553
- /detailsaefb.html
imported:
- "2019"
_4images_image_id: "36553"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36553 -->
Die beiden Slingshots sind ziemlich einfach aufgebaut. Im Prinzib nur zwei Hebel - einer, der die Kugel zurückwirft (die schwarze Strebe) und einer, der einen Treffer registriert (Metallachse). Die ft-Taster schließen leider zu schwer.