---
layout: "image"
title: "Kran (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild3_6.jpg"
weight: "84"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37128
- /details9e2e.html
imported:
- "2019"
_4images_image_id: "37128"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37128 -->
Hier ist der Kran. Wenn er gerade nicht aktiv ist, befindet sich sein BS15 in einer Multe, die ich da noch hinbauen muss. Ist der Kran aktiv, bewegt er den BS15 so auf dem Spielfeld hin und her, dass er die verschiedenen Hauptschüsse blockiert. Um den BS15 aus dem Weg zu räumen muss man ihn treffen. Ein Treffer wird durch vier Taster erkannt, die von einem BS7,5 betätigt werden, der mit zwei Federfüßen mit dem BS15 und dem Kranarm verbunden ist.

Hm. Er ist mir ein bisschen zu hoch gebaut... und an der Stabilität mangelt es auch ein wenig... was sagen denn die Kranbauer?