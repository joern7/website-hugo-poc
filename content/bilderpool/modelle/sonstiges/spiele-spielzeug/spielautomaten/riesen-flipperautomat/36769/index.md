---
layout: "image"
title: "Elektronik für Standziele"
date: "2013-03-17T22:29:29"
picture: "bild2_4.jpg"
weight: "72"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36769
- /detailse086.html
imported:
- "2019"
_4images_image_id: "36769"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-17T22:29:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36769 -->
Angeregt von Triceratops' Idee, Selbstbauelektronik in den Flipper einzubauen, habe ich diese Schaltung aufgebaut. Drückt man den linken Taster, geht die grüne LED an und bleibt auch dank dem Flip Flop (4027, rechtes IC) an. Drückt man jetzt den rechten Taster, geht auch die rote LED an, das UND-Gatter (4081, linkes IC) schaltet durch und versorgt den Transistor und den Kondensator über einen Widerstand mit Strom. Sobald der Kondensator weit genug aufgeladen ist, leitet der Transistor. Dieses Signal wird vom UND-Gatter nochmal verstärkt und geht dann auf die beiden anderen Eingänge der Flip Flops (die wo die Taster nicht dran sind). Dies schaltet die LEDs nach einer Verzögerung aus.