---
layout: "image"
title: "Flipperfinger neu 1"
date: "2013-10-03T09:29:05"
picture: "bild01_4.jpg"
weight: "90"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37495
- /detailsfdef.html
imported:
- "2019"
_4images_image_id: "37495"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37495 -->
Neuer Flipperfinger, der jetzt auch an einer Metallachse (Winkelachse) befestigt ist.