---
layout: "image"
title: "Zweiter TX-Controller (noch im Bau)"
date: "2013-02-19T18:03:38"
picture: "bild7_2.jpg"
weight: "38"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36653
- /details237a.html
imported:
- "2019"
_4images_image_id: "36653"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36653 -->
So, ein zweiter TX ist nun auch angeschlossen. Noch nicht ganz, da sind noch ein paar Ein- und Ausgänge unbeschaltet, das kommt aber noch. Zwischen den beiden "Gehirnen" befindet sich die Kickback-"Schaltung", die das direkte Anschließen eines Netzteils an das Master-TX (rechts) verhindert. Schön blöd...