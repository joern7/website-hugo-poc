---
layout: "image"
title: "Figur-Mann und Ziele (noch im Bau)"
date: "2013-05-23T10:57:11"
picture: "bild3_5.jpg"
weight: "80"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36924
- /details24ae.html
imported:
- "2019"
_4images_image_id: "36924"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-05-23T10:57:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36924 -->
Ein kleiner Spezialeffekt auf dem Flipper. Die roten Platten müssen alle (auf der gegenüberliegenden Seite befinden sich noch ein Figur-Mann und drei weitere Ziele) ein oder mehrere Male, und dann das Auswurfloch getroffen werden, um einen Modus namens "Durchdrehen" zu starten. Und jetzt kommt's: beide Männchen HÜPFEN, bis eines der Ziele unter ihnen getroffen wurde.