---
layout: "image"
title: "'Roll-Under' 2"
date: "2015-09-02T20:01:59"
picture: "bild11_5.jpg"
weight: "122"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41887
- /detailsa3e5-2.html
imported:
- "2019"
_4images_image_id: "41887"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41887 -->
Hier sieht man noch so ein Gate. Dieses hier ist einseitig gelagert, die andere Seite lagere ich später vielleicht noch mit einer Kardangabel. Die hat ja so schöne kleine Löcher.