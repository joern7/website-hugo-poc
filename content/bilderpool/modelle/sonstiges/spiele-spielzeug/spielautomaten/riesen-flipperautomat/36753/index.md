---
layout: "image"
title: "Lichter in den Bahnen 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild09_3.jpg"
weight: "60"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36753
- /details4132.html
imported:
- "2019"
_4images_image_id: "36753"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36753 -->
Hier leuchtet nur das Licht für die rechte Innenbahn.