---
layout: "image"
title: "Unteres Spielfelddrittel (noch im Bau)"
date: "2013-02-03T10:19:44"
picture: "bild05.jpg"
weight: "6"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36547
- /details5871.html
imported:
- "2019"
_4images_image_id: "36547"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36547 -->
In der Mitte unten sind die beiden Flipperfinger zu sehen, zwischen denen sich das Loch für den Mittelpfosten befindet. Wenn der draußen ist, passt die Kugel nicht durch den Spalt durch.