---
layout: "image"
title: "Auswurfloch, überarbeitet 1 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild13_2.jpg"
weight: "64"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36757
- /detailsc193.html
imported:
- "2019"
_4images_image_id: "36757"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36757 -->
Das Auswurfloch ist ein Stück nach rechts gewandert und hat ein drittes Licht bekommen. Das erste (dem Loch am nähesten) zeigt blinkend an, ob eine Extrakugel beleuchtet ist, beim Treffer während das zweite Licht blinkt gibt's eine Glücksfall-Belohnung, und das dritte startet einen Modus.