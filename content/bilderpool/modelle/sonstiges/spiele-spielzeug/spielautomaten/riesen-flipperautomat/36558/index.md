---
layout: "image"
title: "Flipper-LED"
date: "2013-02-03T10:19:45"
picture: "bild16.jpg"
weight: "17"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36558
- /detailsb1eb.html
imported:
- "2019"
_4images_image_id: "36558"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36558 -->
Das ist meine Flipper-LED, die ich selbst in einen ft Leuchtstein gelötet habe. Die ist wahrscheinlich aus Versehen mal mitgeliefert worden, als ich mir 4 Flipperkugeln bestellt habe. Ich habe festgestelle, dass ich die mit 9V problemlos betreiben kann, und dazu ist die Polung egal.