---
layout: "image"
title: "Neuer Kugelrücklauf"
date: "2014-08-20T17:27:31"
picture: "bild3_8.jpg"
weight: "107"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39257
- /details0a27.html
imported:
- "2019"
_4images_image_id: "39257"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39257 -->
Der neue Kugelrücklauf. Er funktioniert jetzt auch zuverlässig. Was hier aber noch fehlt, sind die Sensoren (werde wohl Lichtschranken nehmen), die erkennen, ob der Spieler eine Kugel verloren hat, und welche, die erkennen, ob überhaupt und wie viele Kugeln sich im Magazin befinden. Evtl. verzichte ich aber auch auf diese, wen der Einbauaufwand zu groß wird.