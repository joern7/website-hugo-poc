---
layout: "image"
title: "Der Schlagturmbereich"
date: "2015-09-02T20:01:59"
picture: "bild05_5.jpg"
weight: "116"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41881
- /details5370.html
imported:
- "2019"
_4images_image_id: "41881"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41881 -->
Hier ist auch nichts wirklich neu. Unter dem milchigem "Fenster zwischen den dreich Schlagtürmen befindet sich der momentan einzige Flasher des Flippers. Dieser und die Schlagtürme wurden (und werden vielleicht auch später noch) von einem TX-Controller angesteuert.
Der Elektromagnet oben links im Bild ist für das dortige Einwegetor zuständig. Siehe nächstes Bild...