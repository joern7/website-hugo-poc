---
layout: "image"
title: "Neuer Kickback"
date: "2014-08-20T17:27:31"
picture: "bild1_8.jpg"
weight: "105"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39255
- /detailsfe25.html
imported:
- "2019"
_4images_image_id: "39255"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39255 -->
So funktioniert er - wenn die Lichtschranke unterbrochen wird, zieht ein Relais an, welches den Strom für die Spule frei gibt. Und dann schießt die Kugel wieder zurück ins Spielfeld. Ganz kreativ testweise vonm TX gesteuert, der eigentlich die Schlagtürme ansteuert. Einfach den Fototransistor und nen Widerstand an den Eingang und den Motor durch das Relais für die Spule ersetzen- fertig.
Obwohl der Mikrocontroller noch gar nicht eingebaut ist, habe ich den Programmteil des Kickbacks - samt zugehörigem Licht - schon fertig geschrieben. Er wird später nur "kicken", wenn sein Licht eingeschaltet ist. Hat er dann mal die Kugel zurückgeschossen, bleibt er noch ca. 7 Sek aktiv, während sein Licht sehr schnell blinkt. Nach 5 Sek wird dieses dann ausgeschaltet.