---
layout: "image"
title: "Unterseite von vorne (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild14.jpg"
weight: "15"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36556
- /details3273.html
imported:
- "2019"
_4images_image_id: "36556"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36556 -->
Später wird sich hier eine Verkleidung befinden. Oben links, in den BS 15 mit Loch, kommt noch der Knopf für den Spielstart rein.