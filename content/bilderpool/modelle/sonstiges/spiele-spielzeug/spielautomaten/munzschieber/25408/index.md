---
layout: "image"
title: "(6) von hinten"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_206.jpg"
weight: "33"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25408
- /details83cd.html
imported:
- "2019"
_4images_image_id: "25408"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25408 -->
