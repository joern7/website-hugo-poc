---
layout: "image"
title: "(20) Münzprüfer"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_220.jpg"
weight: "47"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25422
- /detailsa500-2.html
imported:
- "2019"
_4images_image_id: "25422"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25422 -->
