---
layout: "image"
title: "(12) Kabel"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_212.jpg"
weight: "39"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25414
- /details3bee.html
imported:
- "2019"
_4images_image_id: "25414"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25414 -->
Viele Kabel… Die senkrechte Achse hängt an einem Kardangelenk. Unten ist ein Kupferring zu sehen. Wenn die Achse den Ring berührt, dann hört man für etwa 7 Sekunden den Alarm (Summer).