---
layout: "image"
title: "(22) Antriebsseite"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_222.jpg"
weight: "49"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25424
- /details6201.html
imported:
- "2019"
_4images_image_id: "25424"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25424 -->
