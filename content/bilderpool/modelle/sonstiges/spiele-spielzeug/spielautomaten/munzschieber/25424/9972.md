---
layout: "comment"
hidden: true
title: "9972"
date: "2009-09-28T21:46:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Einfach ein tolles Modell, Kompliment! So schön gefischertechnikt. Und den Polwende-Seilschalter sieht man ja auch selten mittlerweile - es muss halt nicht immer ein Robo-Interface mit zwei Tastern sein! Superschön gebaut das alles.

Gruß,
Stefan