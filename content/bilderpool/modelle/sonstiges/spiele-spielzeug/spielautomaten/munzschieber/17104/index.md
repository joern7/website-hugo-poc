---
layout: "image"
title: "(26) Betrieb"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_026.jpg"
weight: "24"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17104
- /details0850-2.html
imported:
- "2019"
_4images_image_id: "17104"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17104 -->
Blick in den Münzschieber, während des Betriebs