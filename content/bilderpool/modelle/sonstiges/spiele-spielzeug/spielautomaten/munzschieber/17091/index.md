---
layout: "image"
title: "(12) Antrieb"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_012.jpg"
weight: "11"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17091
- /detailsf8ba.html
imported:
- "2019"
_4images_image_id: "17091"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17091 -->
Rechts unten der Antrieb für die Schiene