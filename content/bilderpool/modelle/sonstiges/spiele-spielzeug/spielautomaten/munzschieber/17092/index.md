---
layout: "image"
title: "(13) Seitenansicht des Antriebs"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_013.jpg"
weight: "12"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17092
- /details71bd.html
imported:
- "2019"
_4images_image_id: "17092"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17092 -->
