---
layout: "image"
title: "(11) Antrieb"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_011.jpg"
weight: "10"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17090
- /details8ea1-2.html
imported:
- "2019"
_4images_image_id: "17090"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17090 -->
Hinter den Kulissen…
Der Antrieb der oberen Schieber.
Links sieht man den Polwendeschalter für das Hin- und Herbewegen der Schiene