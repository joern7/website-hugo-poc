---
layout: "image"
title: "(3) von vorne, rechts"
date: "2009-01-19T17:21:54"
picture: "Mnzschieber_003.jpg"
weight: "2"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17082
- /details1403.html
imported:
- "2019"
_4images_image_id: "17082"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17082 -->
Gesamtansicht
Zum Schutz der Münzen vor unerlaubtem Hineingreifen der Spieler habe ich an drei Seiten 2 mm starke durchsichtige Polystyrolplatten angebracht.