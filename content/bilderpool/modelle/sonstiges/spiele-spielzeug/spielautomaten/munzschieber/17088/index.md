---
layout: "image"
title: "(9) Schiene"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_009.jpg"
weight: "8"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17088
- /details9b39.html
imported:
- "2019"
_4images_image_id: "17088"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17088 -->
Noch einmal die Schiene, von der die Münzen auf das Prallblech fallen.