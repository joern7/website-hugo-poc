---
layout: "image"
title: "(23) Schalter…"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_223.jpg"
weight: "50"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25425
- /detailsc256.html
imported:
- "2019"
_4images_image_id: "25425"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25425 -->
Im Inneren der Maschine befindet sich diese Platte mit dem Schlüsselschalter (Antriebsmotoren), in der Mitte Schalter für das Licht, und rechts: Schalter für das Aktivieren der Alarmanlage und Taster, um den Alarm abzu-stellen.