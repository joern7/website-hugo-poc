---
layout: "image"
title: "(2) von vorne, links"
date: "2009-01-19T17:21:54"
picture: "Mnzschieber_002.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17081
- /details879c.html
imported:
- "2019"
_4images_image_id: "17081"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17081 -->
Gesamtansicht
Ein Münzschieber (im Englischen auch Coin Pusher oder Penny Pusher) ist ein Geldspielautomat, in dem Münzen auf mehreren Ebenen geschoben werden.