---
layout: "image"
title: "(22) Münzprüfer"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_022.jpg"
weight: "21"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17101
- /detailsfab6.html
imported:
- "2019"
_4images_image_id: "17101"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17101 -->
Um etwas mehr zu sehen, ohne Motor. Der Motor hat die Aufgabe, die Münzen beim Einwurf zu bremsen und die Münzen an den Schlitz zu drücken, damit zu kleine Münzen ausgeschieden werden können. Die Bauart des Münzprüfers ist die Gleiche wie bei meinem Geldsortierer, nur etwas kürzer. Zu kleine Münzen gelangen über die schwarze Röhre direkt in den Untergrund, werden also nicht mehr an den Spieler zurückgegeben, da auch versuchtes Schwindeln bestraft werden muß…
Nicht überprüft wird das Material der Münze.