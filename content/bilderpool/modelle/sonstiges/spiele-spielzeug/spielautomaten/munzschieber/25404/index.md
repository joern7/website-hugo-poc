---
layout: "image"
title: "(2) etwas näher"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_202.jpg"
weight: "29"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25404
- /details1df4-2.html
imported:
- "2019"
_4images_image_id: "25404"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25404 -->
Gut sind die beiden Ebenen mit dem Geld zu sehen