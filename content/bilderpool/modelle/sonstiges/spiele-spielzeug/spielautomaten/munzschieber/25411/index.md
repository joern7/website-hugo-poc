---
layout: "image"
title: "(9) hinten, Türen geöffnet"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_209.jpg"
weight: "36"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25411
- /detailsd421.html
imported:
- "2019"
_4images_image_id: "25411"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25411 -->
Links und rechts neben den Türscharnieren kann man die Taster erkennen, die den Alarm auslösen, wenn man die Türen öffnet. Im Inneren der Maschine die beiden weißen Behälter, in dem das Geld für den Betreiber gesammelt wird.