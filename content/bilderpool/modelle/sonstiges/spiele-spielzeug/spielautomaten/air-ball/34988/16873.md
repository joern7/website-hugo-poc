---
layout: "comment"
hidden: true
title: "16873"
date: "2012-05-22T17:30:17"
uploadBy:
- "sven"
license: "unknown"
imported:
- "2019"
---
Hallo Ludger,

nimm eine Sperrholzplatte 10mm, im Baumarkt sägen sie Dir sowas auf von Dir angegebene Maße.
Kannst Du noch die Grundplatten trennen, also die mit der Rückseite gegeneinander liegen.
Dann würd ich 2 Sperrholzplatten nehmen. Auf die eine die Grundplatten mit der Verkabelung, die andere als Träger für die Spielplatte.
Die beiden Sperrholzplatten verbindest Du an den Ecken mit Klötzen, sagen wir mal 10cm Höhe.
Rein theroretisch könntest Du die Seiten dann auch noch mit Holz zu machen, muss aber ja nicht unbedingt sein.

Gruß
Sven