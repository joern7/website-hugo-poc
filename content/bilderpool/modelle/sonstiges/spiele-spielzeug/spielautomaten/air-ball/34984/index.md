---
layout: "image"
title: "Details"
date: "2012-05-21T17:24:21"
picture: "07-DSCN4747.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34984
- /details8461.html
imported:
- "2019"
_4images_image_id: "34984"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34984 -->
Die vier Ventilatoren die auf einer Spielseite angeordnet sind, sind in Baugruppen montiert
die für den Transport sehr leicht zu entfernen sind. Hinter der gelben 30 x 45 Bauplatte
liegen zwei S-Motoren. Einer für den roten und einer für den schwarzen Ventilator.
Die Antriebsachse des schwarzen kann man einfach nach links rausschieben. Als Lagerung
dient lediglich ein Winkelträger 15.