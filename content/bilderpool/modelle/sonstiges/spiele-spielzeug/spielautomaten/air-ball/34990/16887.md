---
layout: "comment"
hidden: true
title: "16887"
date: "2012-05-26T16:10:52"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Guckst du hier:
http://www.ftcommunity.de/data/media/2479/fischertechnikmodellschau42.jpg

Ganz rechts am Minitaster zu sehen habe ich alte FT-Stecker, an denen die Schraube ausgeleiert ist, neu konfiguriert. Die Kabel habe ich dazu kurzerhand einfach angelötet und offen gelassen. Die Einbauhöhe beträgt nur wenige mm.

Gruß, Thomas