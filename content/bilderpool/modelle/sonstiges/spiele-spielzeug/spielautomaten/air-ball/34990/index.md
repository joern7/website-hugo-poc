---
layout: "image"
title: "Fernsteuerung"
date: "2012-05-21T17:24:21"
picture: "13-DSCN4790.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34990
- /details70f6-2.html
imported:
- "2019"
_4images_image_id: "34990"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34990 -->
hier die ergonomisch konstruierte Fernsteuerung aus der Sicht des Spielers.
Zeige- und Mittelfinger der rechten und linken Hand umgreifen die Seilzugschalter und
betätigen die innenliegenden Taster. Mit Hilfe dieser Tasten und Schalter werden
die Ventilatoren Paarweise gedreht.
Die vier Taster steuern die Motoren der Ventilatoren.
Das bunte Kabel habe ich übrigens selbst angefertigt.....