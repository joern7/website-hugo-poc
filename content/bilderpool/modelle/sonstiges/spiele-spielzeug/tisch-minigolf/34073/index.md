---
layout: "image"
title: "Tisch-Minigolfbahn - Prototyp"
date: "2012-01-30T18:31:32"
picture: "tischminigolfprototyp2.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/34073
- /details0cdd.html
imported:
- "2019"
_4images_image_id: "34073"
_4images_cat_id: "2521"
_4images_user_id: "1"
_4images_image_date: "2012-01-30T18:31:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34073 -->
Noch mal aus einer anderen Perspektive.