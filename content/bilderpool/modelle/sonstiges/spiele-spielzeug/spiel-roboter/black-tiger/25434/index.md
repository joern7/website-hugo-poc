---
layout: "image"
title: "Der auslöse Arm"
date: "2009-09-28T21:32:25"
picture: "blacktiger08.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebstian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/25434
- /detailscfcd.html
imported:
- "2019"
_4images_image_id: "25434"
_4images_cat_id: "1780"
_4images_user_id: "791"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25434 -->
