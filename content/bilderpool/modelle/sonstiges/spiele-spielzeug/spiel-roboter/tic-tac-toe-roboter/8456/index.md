---
layout: "image"
title: "Gesamtansicht ohne Spielfeld"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8456
- /detailsf41f.html
imported:
- "2019"
_4images_image_id: "8456"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8456 -->
Hier sieht man wie der Roboter die Steine bewegen kann.
Um ihn leichter reparieren zu können gibt es eine Klappe an der Seite an der man das Spielfeld herausziehen kann.