---
layout: "image"
title: "Magnetantrieb"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8459
- /details3d67.html
imported:
- "2019"
_4images_image_id: "8459"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8459 -->
Der Magnet bewegt die Steine, in denen auch Magnete sind. Erst hatte ich dort einen normalen ft Magneten, aber da er zwei Pole hat, schob er andere Steine beiseite.