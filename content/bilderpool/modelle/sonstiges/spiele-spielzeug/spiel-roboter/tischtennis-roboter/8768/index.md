---
layout: "image"
title: "Gesamtansicht Roboter"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter1.jpg"
weight: "1"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8768
- /details5a5a.html
imported:
- "2019"
_4images_image_id: "8768"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8768 -->
Hier sieht man den ganzen Roboter, nur der Rücklauf fehlt noch.