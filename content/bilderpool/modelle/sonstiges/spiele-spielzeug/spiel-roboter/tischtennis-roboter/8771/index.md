---
layout: "image"
title: "Hebemechanik"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter4.jpg"
weight: "4"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8771
- /detailsaf7a.html
imported:
- "2019"
_4images_image_id: "8771"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8771 -->
An dem linken Z40 sind drei Stangen, die die Bälle nach oben drücken.