---
layout: "image"
title: "Ballrücklauf"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8770
- /details51b0.html
imported:
- "2019"
_4images_image_id: "8770"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8770 -->
Hier sieht man wie die Bälle zurück zum Roboter laufen.