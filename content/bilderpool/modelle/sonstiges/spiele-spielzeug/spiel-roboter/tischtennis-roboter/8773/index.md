---
layout: "image"
title: "Vorderansicht"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter6.jpg"
weight: "6"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8773
- /details71e4.html
imported:
- "2019"
_4images_image_id: "8773"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8773 -->
