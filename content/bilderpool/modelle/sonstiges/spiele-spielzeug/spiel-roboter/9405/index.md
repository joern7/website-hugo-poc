---
layout: "image"
title: "Flipper Detail Version 2"
date: "2007-03-11T14:19:05"
picture: "IMG_0140.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9405
- /detailsd258.html
imported:
- "2019"
_4images_image_id: "9405"
_4images_cat_id: "776"
_4images_user_id: "557"
_4images_image_date: "2007-03-11T14:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9405 -->
Software siehe Downloads(11.03.07)
Drehzähler.