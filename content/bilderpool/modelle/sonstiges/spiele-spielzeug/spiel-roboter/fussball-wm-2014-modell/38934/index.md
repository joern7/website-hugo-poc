---
layout: "image"
title: "Fußball WM 2014 Modell -  Gegnerische Mannschaft"
date: "2014-06-11T21:54:26"
picture: "WM4.jpg"
weight: "4"
konstrukteure: 
- "Jonas"
fotografen:
- "Markus"
keywords: ["Fußball", "WM", "2014", "Passsspiel"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/38934
- /details76a0.html
imported:
- "2019"
_4images_image_id: "38934"
_4images_cat_id: "2912"
_4images_user_id: "1608"
_4images_image_date: "2014-06-11T21:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38934 -->
Hier eine Nahaufnahme der gegnerischen Mannschaft.