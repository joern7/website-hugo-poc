---
layout: "image"
title: "Fußball WM 2014 Modell - Übersicht 1"
date: "2014-06-11T21:54:26"
picture: "WM2.jpg"
weight: "1"
konstrukteure: 
- "Jonas"
fotografen:
- "Markus"
keywords: ["Fußball", "WM", "2014", "Passspiel"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/38931
- /detailsb4b3.html
imported:
- "2019"
_4images_image_id: "38931"
_4images_cat_id: "2912"
_4images_user_id: "1608"
_4images_image_date: "2014-06-11T21:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38931 -->
Hier eine kleine Übersicht über das Modell. Die Spieler auf dem Spielfeld passen sich hin und her. Alle Spielfiguren können sich drehen und schießen.

Videolink: http://youtu.be/89wQyRqwreI