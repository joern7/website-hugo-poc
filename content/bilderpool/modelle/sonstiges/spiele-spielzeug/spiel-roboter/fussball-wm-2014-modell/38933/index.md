---
layout: "image"
title: "Fußball WM 2014 Modell - Mannschaft mit blauem Band"
date: "2014-06-11T21:54:26"
picture: "WM1.jpg"
weight: "3"
konstrukteure: 
- "Jonas"
fotografen:
- "Markus"
keywords: ["Fußball", "WM", "2014", "Passspiel"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/38933
- /detailsefbd.html
imported:
- "2019"
_4images_image_id: "38933"
_4images_cat_id: "2912"
_4images_user_id: "1608"
_4images_image_date: "2014-06-11T21:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38933 -->
Hier eine Nahaufnahme von den Spielern der blauen Mannschaft.