---
layout: "image"
title: "Auslegeeinheit (1)"
date: "2012-07-18T18:50:53"
picture: "bild6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/35194
- /details274a.html
imported:
- "2019"
_4images_image_id: "35194"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35194 -->
Die Auslegeeinheit kann auf "Befehl" eine Kugel ausgeben, die dann nach unten auf die geneigte Bahn fällt. Die Ausgabe erfolgt durch ein nach Vornedrücken des Schiebers in der Mitte (nächstes Bild). Funktioniert pneumatisch.