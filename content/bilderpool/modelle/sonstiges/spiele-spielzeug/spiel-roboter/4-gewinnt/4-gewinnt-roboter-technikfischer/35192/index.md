---
layout: "image"
title: "Auslage"
date: "2012-07-18T18:50:53"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/35192
- /detailsa8f2.html
imported:
- "2019"
_4images_image_id: "35192"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35192 -->
Hier ist die Schnecke zum sehen, welche die Bahn auf das richtige Feld schiebt. Auch die Motorsteuereinheit ist zu sehen.