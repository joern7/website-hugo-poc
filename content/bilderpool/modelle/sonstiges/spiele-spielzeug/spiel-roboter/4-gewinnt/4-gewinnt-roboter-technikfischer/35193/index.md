---
layout: "image"
title: "Motor"
date: "2012-07-18T18:50:53"
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/35193
- /details057b.html
imported:
- "2019"
_4images_image_id: "35193"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35193 -->
Hier sieht man die genaue Motorsteuerung der Schnecke. Gleich neben der Bahn der Endtaster. Hinter dem Impulsrad zwischen Motor und Schneckenhalter ist der Impulsschalter, um genau zielen zu können ( 25 Impulse für eine Bahn)