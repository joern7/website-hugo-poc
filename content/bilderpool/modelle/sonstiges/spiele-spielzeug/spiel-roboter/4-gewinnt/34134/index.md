---
layout: "image"
title: "'Vier gewinnt'-Maschine (2)"
date: "2012-02-12T12:01:19"
picture: "VG2.jpg"
weight: "7"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ulib"
license: "unknown"
legacy_id:
- /php/details/34134
- /detailsdb61.html
imported:
- "2019"
_4images_image_id: "34134"
_4images_cat_id: "1401"
_4images_user_id: "1330"
_4images_image_date: "2012-02-12T12:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34134 -->
