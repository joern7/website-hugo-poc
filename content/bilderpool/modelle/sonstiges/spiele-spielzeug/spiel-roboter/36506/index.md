---
layout: "image"
title: "Untere Seite des Flippers"
date: "2013-01-22T17:34:51"
picture: "Bild_5.jpg"
weight: "15"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36506
- /detailsa426-2.html
imported:
- "2019"
_4images_image_id: "36506"
_4images_cat_id: "776"
_4images_user_id: "1608"
_4images_image_date: "2013-01-22T17:34:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36506 -->
Hier ist nochmal die untere Seite des Flippers, nur noch mal mit einem größeren Ausschnitt.