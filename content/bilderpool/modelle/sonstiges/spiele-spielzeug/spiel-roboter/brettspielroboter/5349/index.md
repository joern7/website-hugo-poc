---
layout: "image"
title: "Brettspielroboter1"
date: "2005-11-20T19:48:43"
picture: "Neuer_Ordner_2_001.jpg"
weight: "1"
konstrukteure: 
- "Christopher Wecht/ffcoe"
fotografen:
- "Christopher Wecht/ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5349
- /details1553.html
imported:
- "2019"
_4images_image_id: "5349"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2005-11-20T19:48:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5349 -->
