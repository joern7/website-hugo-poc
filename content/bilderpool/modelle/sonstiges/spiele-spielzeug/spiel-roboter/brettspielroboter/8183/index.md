---
layout: "image"
title: "'Der-Neue'--ft-SCARA_01"
date: "2006-12-29T19:03:11"
picture: "ftscara1.jpg"
weight: "9"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/8183
- /detailsb02b.html
imported:
- "2019"
_4images_image_id: "8183"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2006-12-29T19:03:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8183 -->
Die alte Version war doch zu wackelig. Somit habe ich ihn hier konstruiert. Er wurde von diesem Roboter "inspiriert":
http://www.ssirobotics.com/images/yamaha-scara-robot.jpg

Noch fehlt ein Motor; der ist noch wo anders verbaut. Mal sehen, ob sich hier reproduktive Positionen ermitteln lassen...