---
layout: "image"
title: "Greifer1"
date: "2005-11-29T20:12:05"
picture: "Brettspielroboter_001.jpg"
weight: "2"
konstrukteure: 
- "Christopher Wecht\ffcoe"
fotografen:
- "Christopher Wecht\ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5424
- /detailse329.html
imported:
- "2019"
_4images_image_id: "5424"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2005-11-29T20:12:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5424 -->
Dies ist der neue Greifer für den Brettspiel- Roboter. Hiermit wird die Figur mit vier "Fingern" gegriffen. So wird die Figur automtisch zentiert.

Zu den Komentaren beim vorherigem Photo:
Ich habe diesen Roboter völlig zerlegt, da er zu instabiel war. Nun baue ich primär an einem Portalroboter. Secundär baue ich noch an einem Greifarm, der sehr nach DMBT´s Kitzelroboter kommt (Gutes wird gern kopiert ;-)