---
layout: "image"
title: "Schlange-Antrieb"
date: "2009-01-04T14:10:37"
picture: "Fischertechnik__Kalmthoutse-Heide_037.jpg"
weight: "36"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16860
- /details0ae3.html
imported:
- "2019"
_4images_image_id: "16860"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T14:10:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16860 -->
Schlange-Antrieb