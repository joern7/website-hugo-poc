---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T19:02:46"
picture: "Fischertechnik__Kalmthoutse-Heide_103.jpg"
weight: "68"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16892
- /detailsc1ec.html
imported:
- "2019"
_4images_image_id: "16892"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T19:02:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16892 -->
Elefant mit Vakuum-Rüssel