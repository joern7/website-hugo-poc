---
layout: "image"
title: "August  Pneumatik  ( =Bruder Eucalypta )"
date: "2010-12-26T10:55:44"
picture: "August-Maske-Robot_002.jpg"
weight: "80"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29538
- /details1b9d-2.html
imported:
- "2019"
_4images_image_id: "29538"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2010-12-26T10:55:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29538 -->
Maske-Robot  August