---
layout: "comment"
hidden: true
title: "8172"
date: "2009-01-06T21:13:58"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Dieser Getriebe nutze ich für den langsame Antrieb der obere Schalscheibe (37727) zum "End-Positionierung" des Drahtseil und Elefanten-Rüssel. Dieser Schalscheibe darf max. nur eine Umdrehung machen.

Der Powermotor-Asche zum direkten Antrieb der Drahtseile und Puls-Taster macht mehrere Umdrehungen. 
Der Trick ist die Antrieb-Asche nach mehrere Umdrehungen eine reproduzierbare Endposition-Taster bedienen zu lassen.

Gruss,

Peter D.