---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T14:10:38"
picture: "Fischertechnik__Kalmthoutse-Heide_011.jpg"
weight: "46"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16870
- /detailsd250.html
imported:
- "2019"
_4images_image_id: "16870"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T14:10:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16870 -->
Elefant mit Vakuum-Rüssel