---
layout: "image"
title: "Detailansicht Auswurfmechansimus (vorne)"
date: "2011-12-23T19:30:21"
picture: "cac10.jpg"
weight: "9"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33728
- /detailsca6d.html
imported:
- "2019"
_4images_image_id: "33728"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33728 -->
Detailansicht des vorderen Auswurfmechansimus´. Die Karte wird über die Räder komplett unter dem Stapel hervor gezogen.