---
layout: "image"
title: "Auswurfmechanismus Hinten"
date: "2011-12-23T19:30:21"
picture: "cac4_2.jpg"
weight: "15"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33734
- /details7a70-2.html
imported:
- "2019"
_4images_image_id: "33734"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33734 -->
Rückansicht mit dem Auswurfmechanismus.
Beim Auswurf einer Karte fährt der Auswerfer zum zufällig gewählten Schacht und schiebt die Karte hinaus. Nach dem Auswurf fährt er im Normalfall wieder in seine Warteposition in der Mitte zurück. Wenn aber die Befüllungskontrolle der Schächte ergeben hat, dass ein Schacht leer ist, so wartet der Auswerfer hinter dem noch gefüllten Schacht, bis dieser entweder auch leer ist, oder der leere Schacht wieder befüllt wurde.