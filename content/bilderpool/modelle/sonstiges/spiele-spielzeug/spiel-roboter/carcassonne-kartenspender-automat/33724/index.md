---
layout: "image"
title: "Kartenauswurf Vorderseite"
date: "2011-12-23T19:30:21"
picture: "cac5.jpg"
weight: "5"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33724
- /detailsfd19-2.html
imported:
- "2019"
_4images_image_id: "33724"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33724 -->
Da der rückwärtige Auswerfer, die Karten nicht vollständig heraus schieben kann, werden die Karten zusätzlich über zwei Räder aus dem Stapel herausgezogen. Danach fallen sie auf das Förderband und werden zum Ausgabekasten gebracht.