---
layout: "image"
title: "scrabble3.jpg"
date: "2017-03-18T21:15:01"
picture: "scrabble3.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45563
- /details462e-2.html
imported:
- "2019"
_4images_image_id: "45563"
_4images_cat_id: "3387"
_4images_user_id: "2449"
_4images_image_date: "2017-03-18T21:15:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45563 -->
