---
layout: "image"
title: "scrabble1.jpg"
date: "2017-03-18T21:15:01"
picture: "scrabble1.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45561
- /details2ef3.html
imported:
- "2019"
_4images_image_id: "45561"
_4images_cat_id: "3387"
_4images_user_id: "2449"
_4images_image_date: "2017-03-18T21:15:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45561 -->
