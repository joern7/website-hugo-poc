---
layout: "image"
title: "scrabble2.jpg"
date: "2017-03-18T21:15:01"
picture: "scrabble2.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45562
- /details93c7.html
imported:
- "2019"
_4images_image_id: "45562"
_4images_cat_id: "3387"
_4images_user_id: "2449"
_4images_image_date: "2017-03-18T21:15:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45562 -->
