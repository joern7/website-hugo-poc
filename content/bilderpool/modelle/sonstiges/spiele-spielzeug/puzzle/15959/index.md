---
layout: "image"
title: "5-Würfel-Puzzle 3"
date: "2008-10-11T22:48:04"
picture: "Puzzle3.jpg"
weight: "3"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15959
- /details6c3a.html
imported:
- "2019"
_4images_image_id: "15959"
_4images_cat_id: "1449"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15959 -->
Man kann mit den 29 Teilen diverse Quader zusammensetzen.