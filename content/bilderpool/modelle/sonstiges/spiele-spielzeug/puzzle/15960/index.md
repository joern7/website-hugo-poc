---
layout: "image"
title: "5-Würfel-Puzzle 4"
date: "2008-10-11T22:48:04"
picture: "Puzzle4.jpg"
weight: "4"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15960
- /details3510-3.html
imported:
- "2019"
_4images_image_id: "15960"
_4images_cat_id: "1449"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15960 -->
Mit 27 der 29 Teile kann man Vergrößerungen jedes einzelnen Teils im Maßstab 3:1 bauen.