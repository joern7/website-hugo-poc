---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Teil 3"
date: "2014-12-21T09:02:59"
picture: "geschicklichkeitsspiel3.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39948
- /detailsed10.html
imported:
- "2019"
_4images_image_id: "39948"
_4images_cat_id: "3001"
_4images_user_id: "182"
_4images_image_date: "2014-12-21T09:02:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39948 -->
Dann wird eine Rastachse mit Zahnrad in das Rad 23 gesteckt.
So ergibt sich das Grundgerüst für das Spiel.