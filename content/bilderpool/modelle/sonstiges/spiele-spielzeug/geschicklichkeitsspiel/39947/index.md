---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Teil 2"
date: "2014-12-21T09:02:59"
picture: "geschicklichkeitsspiel2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39947
- /detailsba48-2.html
imported:
- "2019"
_4images_image_id: "39947"
_4images_cat_id: "3001"
_4images_user_id: "182"
_4images_image_date: "2014-12-21T09:02:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39947 -->
Im ersten Schritt werden die 12 Rastachsen mit den Zahnräder wie abgebildet zusammengebaut.