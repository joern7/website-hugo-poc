---
layout: "image"
title: "Lösung zum Geschicklichkeitsspiel"
date: "2014-12-30T07:14:11"
picture: "loesung1_4.jpg"
weight: "11"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/40067
- /details7167-3.html
imported:
- "2019"
_4images_image_id: "40067"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40067 -->
Bild 6