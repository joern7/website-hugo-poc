---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Lösung"
date: "2014-12-25T13:17:41"
picture: "loesung1_5.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39983
- /detailsca95-2.html
imported:
- "2019"
_4images_image_id: "39983"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-25T13:17:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39983 -->
So, da es ja doch für die meisten zu schwierig scheint habe ich mal eine "Dalli Klick" Lösung vorbereitet :))