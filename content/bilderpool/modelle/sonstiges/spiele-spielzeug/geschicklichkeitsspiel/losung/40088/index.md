---
layout: "image"
title: "Lösung zum Geschicklichkeitsspiel"
date: "2014-12-31T07:49:08"
picture: "loesung1_3.jpg"
weight: "13"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/40088
- /details0bea-2.html
imported:
- "2019"
_4images_image_id: "40088"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40088 -->
Bild 8