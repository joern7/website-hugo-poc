---
layout: "image"
title: "Mini-Kreisel (3)"
date: "2013-02-23T18:00:41"
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36670
- /details73b9.html
imported:
- "2019"
_4images_image_id: "36670"
_4images_cat_id: "2719"
_4images_user_id: "1624"
_4images_image_date: "2013-02-23T18:00:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36670 -->
Hier sieht man gut, wie der Widerstand in der Nabe festgeschraubt ist.