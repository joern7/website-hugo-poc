---
layout: "image"
title: "Kartenausgeber 4"
date: "2007-03-28T15:09:54"
picture: "kartenausgeber04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9835
- /details0de2.html
imported:
- "2019"
_4images_image_id: "9835"
_4images_cat_id: "886"
_4images_user_id: "502"
_4images_image_date: "2007-03-28T15:09:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9835 -->
Die Karten kommen von unter und werden dann über die Rolle um 45° gedreht.