---
layout: "image"
title: "Darda-Aufziehstation Bild 3"
date: "2009-12-13T16:39:59"
picture: "darda-aufziehstation_03.jpg"
weight: "3"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25958
- /detailsd18f.html
imported:
- "2019"
_4images_image_id: "25958"
_4images_cat_id: "1824"
_4images_user_id: "1027"
_4images_image_date: "2009-12-13T16:39:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25958 -->
Hier ist die "Aufziehrolle" (Räder 23 mit Slicks) zu sehen.