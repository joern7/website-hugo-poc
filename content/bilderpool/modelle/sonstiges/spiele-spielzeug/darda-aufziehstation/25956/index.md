---
layout: "image"
title: "Darda-Aufziehstation"
date: "2009-12-13T16:39:57"
picture: "darda-aufziehstation_01.jpg"
weight: "1"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25956
- /detailsce64-2.html
imported:
- "2019"
_4images_image_id: "25956"
_4images_cat_id: "1824"
_4images_user_id: "1027"
_4images_image_date: "2009-12-13T16:39:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25956 -->
Vor kurzem habe ich für meinen Sohn meine alte Dardabahn aus dem Keller gekramt. Das sind kleine Autos mit Rückziehmotor, die auf Steckschienen fahren (manch einer wird sich bestimmt daran erinnern...). Um das erneute, manuelle Aufziehen nach gefahrener Runde zu umgehen oder auch besonders lange Streckenfahrten zu ermöglichen, habe ich mir eine "Aufziehstation" erdacht. So kann der kleine Buggy endlos seine Runden drehen - Sohnemann ist jedenfalls begeistert!

Die Ansteuerung wäre auch mit Flipflop, Monoflop und ein paar Tastern zu machen - aber ich wollte unbedingt meinen neuen TX einweihen (und das erste mal ein RoboPro-Programm erstellen).

Zur Funktion: Das Auto fährt in die Station ein und wird von einem geschlossenen "Ausfahrtstor" gestoppt. Die Lichtschranke registriert das Auto und ein "Klemmhebel" presst das Auto im Heck an die Gummirollen (Rad 23 mit Slicks). Diese drehen sich einige Sekunden und laden somit den Federmotor auf. Danach öffnet sich das Ausfahrtstor, der Klemmhebel löst sich und das Auto bekommt grünes Licht.