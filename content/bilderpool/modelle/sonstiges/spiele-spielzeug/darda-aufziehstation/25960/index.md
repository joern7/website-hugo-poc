---
layout: "image"
title: "Darda-Aufziehstation Bild 5"
date: "2009-12-13T16:39:59"
picture: "darda-aufziehstation_05.jpg"
weight: "5"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25960
- /details7af4.html
imported:
- "2019"
_4images_image_id: "25960"
_4images_cat_id: "1824"
_4images_user_id: "1027"
_4images_image_date: "2009-12-13T16:39:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25960 -->
Nach ein paar Sekunden ist der Rückzugmotor aufgeladen, Ausfahrt und Klemmhebel lösen sich - und der Buggy bekommt grünes Licht und flitzt los.