---
layout: "image"
title: "FT Entscheidungsfinder - Magnete 15mm * 15mm"
date: "2018-05-12T10:30:23"
picture: "Fischertechnik_Entscheidungsfinder_-_7_of_7.jpg"
weight: "24"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47639
- /details9b66.html
imported:
- "2019"
_4images_image_id: "47639"
_4images_cat_id: "323"
_4images_user_id: "2739"
_4images_image_date: "2018-05-12T10:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47639 -->
Unter jeder "Entscheidung" steckt ein Magnet, der die Kugelbahn-Kugel anzieht - je nach Einstellung ergeben sich unvorhersehbare Bewegungen.