---
layout: "image"
title: "Sprungschanze 5 (vorne links)"
date: "2017-12-30T13:50:28"
picture: "FT_Sprungschanze_5_600x800.jpg"
weight: "4"
konstrukteure: 
- "gerhard"
fotografen:
- "gerhard"
keywords: ["Sprungschanze", "Schispringer"]
uploadBy: "gerhard"
license: "unknown"
legacy_id:
- /php/details/47011
- /detailsc1d8.html
imported:
- "2019"
_4images_image_id: "47011"
_4images_cat_id: "3480"
_4images_user_id: "2812"
_4images_image_date: "2017-12-30T13:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47011 -->
