---
layout: "overview"
title: "Sprungschanze für Schispringer"
date: 2020-02-22T08:32:24+01:00
legacy_id:
- /php/categories/3480
- /categoriesd333.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3480 --> 
Um meine Lego und Playmobil spielenden Kinder auch für FischerTechnik zu begeistern habe ich ihnen vor ein paar Jahren eine Schisprungschanze aus FischerTechnik gebaut. Insbesondere die Playmobil-Schifahrer springen auf dieser Schanze sehr gut und wir haben richtige Wettbewerbe veranstaltet. Ich habe dazu ein paar Fotos begelegt.