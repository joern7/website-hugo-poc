---
layout: "image"
title: "Sprungschanze von oben"
date: "2017-12-30T13:50:28"
picture: "FT_Sprungschanze_3_600x800.jpg"
weight: "3"
konstrukteure: 
- "gerhard"
fotografen:
- "gerhard"
keywords: ["Sprungschanze", "für", "Schispringer"]
uploadBy: "gerhard"
license: "unknown"
legacy_id:
- /php/details/47010
- /details2d1a.html
imported:
- "2019"
_4images_image_id: "47010"
_4images_cat_id: "3480"
_4images_user_id: "2812"
_4images_image_date: "2017-12-30T13:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47010 -->
