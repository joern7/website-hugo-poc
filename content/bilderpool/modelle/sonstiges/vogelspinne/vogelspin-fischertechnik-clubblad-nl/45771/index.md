---
layout: "image"
title: "FT-Vogelspin + Powermotor 50-1"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl15.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45771
- /detailse3a7-2.html
imported:
- "2019"
_4images_image_id: "45771"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45771 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017

FT Vogelspin met Powermotor 50 : 1 
https://www.youtube.com/watch?v=GxA4DuOwgYo