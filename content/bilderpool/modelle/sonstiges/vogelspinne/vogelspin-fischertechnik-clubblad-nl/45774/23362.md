---
layout: "comment"
hidden: true
title: "23362"
date: "2017-04-15T16:36:28"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo David,

Auch der neue Encodermotor-Spinne wird über die alte ( ! ) IR-Empfänger gesteuert.
- Die "neuere" IR-Empfänger Control-Set hat zu wenig Leistung wegen dem Strombegrenzung bei 1A.
- Beim neuen Bluetooth Control Set  540585 wird es gut funktionieren.

- Die altere Encodermotor hat zu wenig Leistung. Nur der neue Encodermotor funktioniert beim Spinne gut bei hohere Spannung (=12V).