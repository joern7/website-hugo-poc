---
layout: "image"
title: "fischertechnik-Spin met Powermotor 50-1"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl08.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45764
- /detailseb49.html
imported:
- "2019"
_4images_image_id: "45764"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45764 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017