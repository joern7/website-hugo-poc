---
layout: "image"
title: "Taster"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb5.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13887
- /detailsd8a8.html
imported:
- "2019"
_4images_image_id: "13887"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13887 -->
Auf diesem Bild ist der Taster, den man betätigen muss, wenn man die Tür öffnen will.