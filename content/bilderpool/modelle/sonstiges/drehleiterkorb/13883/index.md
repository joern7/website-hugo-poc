---
layout: "image"
title: "Drehleiter"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13883
- /details776a.html
imported:
- "2019"
_4images_image_id: "13883"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13883 -->
Das ist die Drehleiter-Korb!