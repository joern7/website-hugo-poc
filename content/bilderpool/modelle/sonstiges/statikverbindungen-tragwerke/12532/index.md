---
layout: "image"
title: "Möglichkeiten von Tragwerksverbindungen"
date: "2007-11-08T07:58:39"
picture: "IMG_134_135.jpg"
weight: "1"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12532
- /details855e.html
imported:
- "2019"
_4images_image_id: "12532"
_4images_cat_id: "1129"
_4images_user_id: "611"
_4images_image_date: "2007-11-08T07:58:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12532 -->
Möglichkeiten verschiedener horizontaler Tragwerksverbindungen. Weitere Vorschläge?