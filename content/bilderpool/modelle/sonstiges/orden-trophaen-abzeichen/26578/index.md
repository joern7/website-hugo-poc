---
layout: "image"
title: "Laura Modelling the Medal"
date: "2010-03-03T20:05:24"
picture: "ft_medalb.jpg"
weight: "7"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Medal"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26578
- /details4188.html
imported:
- "2019"
_4images_image_id: "26578"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-03T20:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26578 -->
Experimenting with medal designs