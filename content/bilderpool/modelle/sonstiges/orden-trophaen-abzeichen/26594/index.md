---
layout: "image"
title: "Rendered Trophy"
date: "2010-03-04T21:23:29"
picture: "sm_ft_trophy.jpg"
weight: "8"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Trophy"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26594
- /detailse059.html
imported:
- "2019"
_4images_image_id: "26594"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-04T21:23:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26594 -->
Used POVRAY to render this image of a ft trophy! Thought to share.