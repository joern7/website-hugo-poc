---
layout: "image"
title: "Düse"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/39675
- /detailse489.html
imported:
- "2019"
_4images_image_id: "39675"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39675 -->
Die Düsen stehen in Grundstellung senkrecht. Dadurch, dass ihre Achse aber geneigt ist, führen sie beim Drehen der Zahnräder aber eine ausladende Schwenkbewegung aus.