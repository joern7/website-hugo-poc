---
layout: "image"
title: "Noch ein Blick in die Mechanik"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/39679
- /detailsa940.html
imported:
- "2019"
_4images_image_id: "39679"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39679 -->
Es ist auch noch nicht ausgetestet, ob diese Art der Ansteuerung die richtigen Winkel ergibt. Das ist müßig, solange ich nicht genügend Druckluft habe.