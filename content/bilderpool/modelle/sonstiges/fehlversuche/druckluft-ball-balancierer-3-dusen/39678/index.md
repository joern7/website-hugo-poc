---
layout: "image"
title: "Bewegung (2)"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/39678
- /details3251.html
imported:
- "2019"
_4images_image_id: "39678"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39678 -->
Hier ist die Düse fast angekommen. Den Ball muss man sich jetzt vor der rechten Düse frei schwebend vorstellen, bis er von eben dieser rechten Düse erfasst und "übernommen" wird.