---
layout: "image"
title: "Ostereier Färbe Maschine - Mechanik"
date: "2015-05-03T19:52:12"
picture: "OstereierFaerbeMaschine_Mechanik.jpg"
weight: "4"
konstrukteure: 
- "pinkepunk"
fotografen:
- "pinkepunk"
keywords: ["Ostereier", "färben", "Mischen", "Küche"]
uploadBy: "pinkepunk"
license: "unknown"
legacy_id:
- /php/details/40945
- /details0302.html
imported:
- "2019"
_4images_image_id: "40945"
_4images_cat_id: "716"
_4images_user_id: "2431"
_4images_image_date: "2015-05-03T19:52:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40945 -->
Die Herausforderung bestand vor allem darin, den Aufbau starr genug gegenüber den auftretenden mechanischen Belastungen zu konstruieren.