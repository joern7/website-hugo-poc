---
layout: "image"
title: "Seilführung (3)"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7689
- /details68d2.html
imported:
- "2019"
_4images_image_id: "7689"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7689 -->
Einer der Endpunkte.