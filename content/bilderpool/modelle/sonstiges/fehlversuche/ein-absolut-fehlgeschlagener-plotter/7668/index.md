---
layout: "image"
title: "Stiftaufnahme (4)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7668
- /details7f9d.html
imported:
- "2019"
_4images_image_id: "7668"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7668 -->
Hier sieht man die Stiftaufnahme von hinten. Oben sieht man die Umlenkrollen für das (eine!) Seil für die Bewegung des Stiftes über den Querträger.