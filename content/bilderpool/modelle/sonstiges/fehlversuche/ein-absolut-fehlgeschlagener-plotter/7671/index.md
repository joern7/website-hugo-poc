---
layout: "image"
title: "Antrieb (1)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7671
- /details8752.html
imported:
- "2019"
_4images_image_id: "7671"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7671 -->
Der Antrieb geschah durch Reibung: Die Seile für x und y wurden wie hier gezeigt zwischen angetriebenen Zahnrädchen eingeklemmt. Einen Schrittmotor oder so was hatte ich auch nicht, deshalb der Impulsgeber per ft-Schleifring und Taster.