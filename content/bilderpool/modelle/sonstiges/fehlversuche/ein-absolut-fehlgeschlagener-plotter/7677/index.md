---
layout: "image"
title: "Antrieb (7)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7677
- /detailsd61f.html
imported:
- "2019"
_4images_image_id: "7677"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7677 -->
Und noch mal.