---
layout: "image"
title: "Impulsgeber (2)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7681
- /details3966.html
imported:
- "2019"
_4images_image_id: "7681"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7681 -->
Und wieder ein anderer Blickwinkel.