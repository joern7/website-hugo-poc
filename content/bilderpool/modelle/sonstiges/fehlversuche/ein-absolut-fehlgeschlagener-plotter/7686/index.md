---
layout: "image"
title: "Stiftheber (5)"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7686
- /details5fc9-2.html
imported:
- "2019"
_4images_image_id: "7686"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7686 -->
Blick von oben.