---
layout: "image"
title: "Seilführung (2)"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7688
- /detailse98d.html
imported:
- "2019"
_4images_image_id: "7688"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7688 -->
Das Seil musste nach oben umgelegt werden, weil es sonst mit dem Stiftträger ins Gedränge käme.