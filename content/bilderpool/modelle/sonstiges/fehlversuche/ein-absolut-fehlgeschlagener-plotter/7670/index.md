---
layout: "image"
title: "Seilzüge"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7670
- /detailse1a8.html
imported:
- "2019"
_4images_image_id: "7670"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7670 -->
Alle Motoren einschließlich Stiftabsenken sollten unbeweglich außerhalb der Mechanik verbaut werden. Dazu gab es eine "raffinierte" Verlegung von Schnüren. So viel Kette hatte ich nämlich nicht.