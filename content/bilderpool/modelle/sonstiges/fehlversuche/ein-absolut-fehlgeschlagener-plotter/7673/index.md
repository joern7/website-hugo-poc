---
layout: "image"
title: "Antrieb (3)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7673
- /detailse544.html
imported:
- "2019"
_4images_image_id: "7673"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7673 -->
Hier sieht man den Endpunkt eines der Zugseile.