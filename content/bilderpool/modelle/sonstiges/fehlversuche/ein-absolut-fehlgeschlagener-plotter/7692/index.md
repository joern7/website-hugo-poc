---
layout: "image"
title: "Parallel-Interface"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7692
- /detailsbb3d-2.html
imported:
- "2019"
_4images_image_id: "7692"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7692 -->
Der Plotter wurde von Logitech/MultiScope Modula-2 unter MS-DOS angesteuert.