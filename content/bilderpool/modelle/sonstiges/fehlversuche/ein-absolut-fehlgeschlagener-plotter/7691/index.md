---
layout: "image"
title: "Antriebsseite"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7691
- /detailsc1b6.html
imported:
- "2019"
_4images_image_id: "7691"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7691 -->
Hier die Seite mit den Antrieben fast komplett. Rechts unten der Stifthebeantrieb.