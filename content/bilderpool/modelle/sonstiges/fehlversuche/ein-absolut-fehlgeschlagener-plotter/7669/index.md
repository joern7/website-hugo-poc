---
layout: "image"
title: "Stiftaufnahme (5)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7669
- /details208e.html
imported:
- "2019"
_4images_image_id: "7669"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7669 -->
Man blickt hier praktisch parallel zum Papier unter den Querträger.