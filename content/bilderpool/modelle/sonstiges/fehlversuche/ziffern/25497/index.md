---
layout: "image"
title: "Ziffern 0 - 9"
date: "2009-10-06T19:23:22"
picture: "ziffern1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25497
- /details9513-2.html
imported:
- "2019"
_4images_image_id: "25497"
_4images_cat_id: "1785"
_4images_user_id: "104"
_4images_image_date: "2009-10-06T19:23:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25497 -->
Für eine gedachte neue Anzeige sind diese Ziffern konzipiert. Allerdings braucht man tierisch viele Winkelsteine 30° und rote BS30. Die Ziffern sollten möglichst gut ausgeformt sein und nur eine Farbe haben. Alle haben unten in der Mitte eine Nut, an der man sie befestigen kann.

Die 6 und 9 sind identisch aufgebaut. Bei denen sind die älteren, etwas kleineren WS30 verwendet, weil mir die neueren, etwas größeren ausgingen. Alle anderen Ziffern verwenden die neuere Form des WS30.

Für mehrere Ziffern reicht mein Vorrat an roten Teilen allerdings leider nicht, so dass das wohl bei mir ein ungenutzter Prototyp bleibt.