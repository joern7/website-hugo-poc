---
layout: "image"
title: "Motoren von oben"
date: "2011-08-30T19:15:00"
picture: "bagger05.jpg"
weight: "5"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31706
- /details3462.html
imported:
- "2019"
_4images_image_id: "31706"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31706 -->
Die Kabel sind durch umgebaute Bananenstecker um die Ecke eingesteckt.