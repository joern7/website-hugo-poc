---
layout: "image"
title: "Schnecken Antrieb"
date: "2011-08-30T19:15:00"
picture: "bagger23.jpg"
weight: "23"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31724
- /details5805.html
imported:
- "2019"
_4images_image_id: "31724"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31724 -->
Beim roten Schnecken-Stein wurde ein Zapfen abgemacht.