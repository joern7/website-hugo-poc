---
layout: "image"
title: "2. Gelenk"
date: "2011-08-30T19:15:00"
picture: "bagger20.jpg"
weight: "20"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31721
- /detailscfd9.html
imported:
- "2019"
_4images_image_id: "31721"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31721 -->
