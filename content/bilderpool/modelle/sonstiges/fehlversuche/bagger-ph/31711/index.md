---
layout: "image"
title: "Kompressor"
date: "2011-08-30T19:15:00"
picture: "bagger10.jpg"
weight: "10"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31711
- /details8312.html
imported:
- "2019"
_4images_image_id: "31711"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31711 -->
Die Welle ist selbst gebaut