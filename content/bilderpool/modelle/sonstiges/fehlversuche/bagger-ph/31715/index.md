---
layout: "image"
title: "Bagger"
date: "2011-08-30T19:15:00"
picture: "bagger14.jpg"
weight: "14"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31715
- /detailsf4e2.html
imported:
- "2019"
_4images_image_id: "31715"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31715 -->
Gesamt