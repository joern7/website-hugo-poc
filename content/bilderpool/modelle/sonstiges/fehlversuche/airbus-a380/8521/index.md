---
layout: "image"
title: "Kabinenmodul46.JPG"
date: "2007-01-19T13:51:45"
picture: "Kabinenmodul46.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8521
- /detailsd5ff.html
imported:
- "2019"
_4images_image_id: "8521"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T13:51:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8521 -->
So schön der Gedanke mit der Modulbauweise auch ist, unterm Strich werden gerade mal drei (höchstens vier) davon existieren. Wegen der Türen und Notrutschen und der Verjüngung in Bug und Heck sind dort wieder Anpassungen nötig.

Im Mittelgang wäre noch locker Platz für einen Sitz zur linken oder zur rechten. Die Kabinenhöhe ist etwas knapp bemessen, aber so kann ich die Außenhaut mit Bauplatten 90x30 verkleiden, die vom unteren zum oberen U-Träger reichen.

Dadurch können die Module aber keine gescheit hohen Türen haben; also müssen die Türen irgendwo zwischen den Modulen verbaut werden.