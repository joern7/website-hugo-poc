---
layout: "overview"
title: "H.A.R.R.Y.'s Glasmurmelbahn - die zickige Hebekunst"
date: 2020-02-22T08:28:12+01:00
legacy_id:
- /php/categories/3106
- /categories419d.html
- /categories00ea.html
- /categoriesfad0.html
- /categories3ed2.html
- /categoriesf58d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3106 --> 
Eine kleine Dokumentation über die zickige Hebekunst. An sich funktionierte sie, aber sie war einfach nicht zuverlässig genug für eine Ausstellung. Und deswegen landet sie hier, auf dem "Schrottplatz" der ft-Geschichte.
Das Prinzip zum Kugeltransport könnt ihr Euch hier anschauen: www.kugelbahn.info/deutsch/bergwerk/dusyma.html
Es ist die kleine Animation am Anfang des ersten Absatzes, die zeigt wie das so vor sich gehen sollte. In dem Fall hier sogar doppelseitig auf Vorder- und Rückseite gleichzeitig.

Zu Problemen führte eindeutig der Verschleiß (Abrieb) und das Getriebe mit den vielen Kunststoffachsen war zu instabil. Auch an der Geometrie der Hubplattformen war das Optimum wohl noch nicht erreicht. Die Durchmessertoleranzen der Murmeln sind nicht ausreichend berücksichtigt.

Die Pneumatik - ohne Kompressor - allerdings lief absolut perfekt.