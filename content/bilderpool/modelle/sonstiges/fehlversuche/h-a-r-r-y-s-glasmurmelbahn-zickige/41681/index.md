---
layout: "image"
title: "Verschleiß - ein Grund für die Unzuverlässigkeit"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst13.jpg"
weight: "13"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41681
- /details2936.html
imported:
- "2019"
_4images_image_id: "41681"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41681 -->
Hier ist die halbierte Hebekunst zu sehen. An den Stellen mit der grauen Verfärbung hat es erheblich Abrieb gegeben - trotz Vaseline. An diesen Stellen haben sich die Hubstangen auch immer wieder verhakt und so den Mechanismus blockiert.

------------

Another cause for failing: wear.

At the gray areas the parts were rubbing aginst others causing this wear and also they got jammed.