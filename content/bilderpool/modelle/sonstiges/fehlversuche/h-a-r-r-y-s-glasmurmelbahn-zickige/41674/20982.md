---
layout: "comment"
hidden: true
title: "20982"
date: "2015-08-05T07:45:15"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Keine Scham, die Mechanik ist komplex genug um mal gedanklich falsch abzubiegen.
Von TST habe ich im Vorfeld seine wunderbaren Klemmnaben bekommen, die den Job perfekt erledigen würden, für Notfälle vor Ort. Nur versuche ich OHNE modifizierte Teile auszukommen - sonst wäre das ja viel zu einfach.

Allerdings, der Fehlversuch wurmt mich, mein Gespür sagt mir "es geht doch" und ich will es jetzt wissen! Eine Hebekunst für 14 bis 16 Murmeln gleichzeitig nach dem "Dusyma"-Prinzip. Nichtmodifiziertes ft, nicht verklebt und auch keine anderen Tricks. Pro Murmel sind das etwa 7g und so könnt ihr abschätzen was da an Drehmomenten auf die Wellen gestemmt und an Schub-/Zugkräften übertragen werden muß.

Nachträglich betrachtet war es daher doch eine gute Idee die zickige Hebekunst komplett zu zerlegen. Das macht den Kopf frei um aus den Fehlern zu lernen und schafft Raum für neue Lösungansätze.

Grüße
H.A.R.R.Y.