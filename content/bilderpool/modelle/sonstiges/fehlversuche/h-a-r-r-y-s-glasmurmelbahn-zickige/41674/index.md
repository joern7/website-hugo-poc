---
layout: "image"
title: "Antrieb der pendelnden Hubstangen"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst06.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41674
- /details9695.html
imported:
- "2019"
_4images_image_id: "41674"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41674 -->
Die pendelnde Auf- und Abbewegung der Hubstangen wird von einer Pleuelstange erzeugt. Eine Lenksäule 38133 wir von dem Strebenpaket über ein Gelenkstück 31888 am Lenkhebel angelenkt. Die hin und herdrehende Bewegung wird auf das Z40 übertragen.

--------------

The up and down movement of the mechanism is generated from a continous rotary movement by a crank and a connecting rod. Form this connecting rod the torque is tranferred to the big tooth wheel inside the top.