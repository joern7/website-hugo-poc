---
layout: "image"
title: "Werkstück"
date: "2008-02-04T13:59:56"
picture: "olli2.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13522
- /detailsa375.html
imported:
- "2019"
_4images_image_id: "13522"
_4images_cat_id: "1240"
_4images_user_id: "504"
_4images_image_date: "2008-02-04T13:59:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13522 -->
Dies hier ist ein Fehlversuch!!!
Insgesamt waren immer 2 Werkstücke unterwegs. Aber da ich damals nur 2 Lichtschranken bauen konnte (Mangel an Fototransistoren) war auch relativ viel über Zeit gesteuert, was natürlich nicht so hingehauen hat.