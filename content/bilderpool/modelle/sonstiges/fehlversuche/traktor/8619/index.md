---
layout: "image"
title: "Traktor 15"
date: "2007-01-23T16:01:39"
picture: "traktor15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8619
- /details7873.html
imported:
- "2019"
_4images_image_id: "8619"
_4images_cat_id: "793"
_4images_user_id: "502"
_4images_image_date: "2007-01-23T16:01:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8619 -->
