---
layout: "image"
title: "Detail Magnetträger"
date: "2011-02-19T23:02:57"
picture: "ffwirbelstrom2.jpg"
weight: "2"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30095
- /details1413-2.html
imported:
- "2019"
_4images_image_id: "30095"
_4images_cat_id: "2218"
_4images_user_id: "373"
_4images_image_date: "2011-02-19T23:02:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30095 -->
Die Magneten sind mit 2-Komponenten-Epoxy an die ft-Steine geklebt. Das war eine Schei'arbeit, da die Klebverbindung bei Stößen wieder abgehen. Und mir sind die Magnete verdammt oft aneinandergerasselt. Insgesamt 3 Stück mussten den Weg in die Ablage P nehmen...
Wie man hier auch sieht, ist die Gondel Kugelgelagert. Ansonsten wäre eine entsprechend niedrige Reibung niemals realisierbar gewesen.