---
layout: "image"
title: "Transportkette"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/38997
- /details223d-3.html
imported:
- "2019"
_4images_image_id: "38997"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38997 -->
Die hat eigentlich einwandfrei funktioniert, siehe nächste Bilder. Hier nur mal eine kurze Teststrecke. Von rechts sollte das Fahrzeug kommen(links im Hintergrund steht es) und mit den rechten Reifen auf den roten Bauplatten(Fortzusetzen bis zum Ende) fahren.