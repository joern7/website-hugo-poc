---
layout: "image"
title: "Rollenende"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked6.jpg"
weight: "6"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39002
- /details8ea8-2.html
imported:
- "2019"
_4images_image_id: "39002"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39002 -->
Das nicht angetriebene Ende der Rolle mit zwei Fototransistoren. Die Höhensteuerung hat auch nie richtig funktioniert.