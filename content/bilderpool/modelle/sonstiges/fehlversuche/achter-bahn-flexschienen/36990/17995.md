---
layout: "comment"
hidden: true
title: "17995"
date: "2013-05-26T21:27:49"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
wenn Du die Mitnehmer weiter auseinanderbringen kannst, könntest  Du 2 Kettenglieder dazwischensetzen , damit wird es etwas flexibler.
wenn du die Mitnehmer nicht unbedingt mittig auf der Kette benötigst, kannst du sie auch seitlich an den 7,5mm Steinen anbringen , damit kannst Du die 7,5er grundsätzlich mit 2 Kettengliedern verbinden.
Als Andruckrolle würde ich 2 Ritzel mit 2 Federgelenksteinen kurz vor und hinter dem Antriebsritzel anbringen.
hoffe, das Klappt dann so?!
Jens