---
layout: "image"
title: "Rolltreppe02.JPG"
date: "2007-08-05T17:05:57"
picture: "Rolltreppe02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11300
- /detailscf41.html
imported:
- "2019"
_4images_image_id: "11300"
_4images_cat_id: "1016"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:05:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11300 -->
Die Umlenkung oben ginge auch mit der Drehscheibe anstelle des Malteserrades. So ist es aber deutlich eleganter.

Das Gerüst ist nicht genau eingestellt, normalerweise liegen die Stufen im geraden Teil der Bahn durchaus waagerecht.