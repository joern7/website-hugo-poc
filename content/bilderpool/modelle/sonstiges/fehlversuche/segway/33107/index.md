---
layout: "image"
title: "Von links"
date: "2011-10-05T11:04:35"
picture: "segway7.jpg"
weight: "7"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: ["Segway"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/33107
- /details0ca5-2.html
imported:
- "2019"
_4images_image_id: "33107"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T11:04:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33107 -->
Ansicht von links.