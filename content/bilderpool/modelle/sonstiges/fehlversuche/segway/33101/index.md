---
layout: "image"
title: "Von vorne"
date: "2011-10-05T11:04:35"
picture: "segway1.jpg"
weight: "1"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: ["Segway"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/33101
- /details4550.html
imported:
- "2019"
_4images_image_id: "33101"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T11:04:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33101 -->
Ansicht von vorne.