---
layout: "overview"
title: "Großer Kompressor mit original FT-Kurbelwelle und Doppelkolben"
date: 2020-02-22T08:28:00+01:00
legacy_id:
- /php/categories/1287
- /categories94d3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1287 --> 
Hallo,

hier ein etwas älteres Modell von mir. Ein Druckluftkompressor mit riesiegem Schwungrad und Doppelkolben. Diese waren nötig, weil ich die original FT-Kurbelwelle verwenden wollte.

Hat einigermaßen funktioniert, allerdings nicht wirklich so gut, daß man es hätte verwenden können. War halt eher so ein langsam laufender Kompressor, man hat dem Schwungrad schon ein bißchen nachhelfen müssen und ich habe mir dabei einen alten Motor kaputt gemacht, weil der ständig viel zu lamgsam gelaufen ist... aber lustig zum zuschauen war's!