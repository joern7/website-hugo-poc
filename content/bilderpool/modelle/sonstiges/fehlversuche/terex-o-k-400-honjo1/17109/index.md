---
layout: "image"
title: "O&K 400 unvollendet"
date: "2009-01-19T17:21:56"
picture: "ok_honjo1_042.jpg"
weight: "2"
konstrukteure: 
- "honjo1"
fotografen:
- "honjo1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/17109
- /details7dbc-2.html
imported:
- "2019"
_4images_image_id: "17109"
_4images_cat_id: "1535"
_4images_user_id: "14"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17109 -->
