---
layout: "image"
title: "O&K 400 noch unvollendet"
date: "2009-01-19T17:21:56"
picture: "ok_honjo1_040.jpg"
weight: "1"
konstrukteure: 
- "honjo1"
fotografen:
- "honjo1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/17108
- /details5b42.html
imported:
- "2019"
_4images_image_id: "17108"
_4images_cat_id: "1535"
_4images_user_id: "14"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17108 -->
