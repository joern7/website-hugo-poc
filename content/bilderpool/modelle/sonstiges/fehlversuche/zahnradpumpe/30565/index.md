---
layout: "image"
title: "Zapu38.JPG"
date: "2011-05-14T22:15:50"
picture: "Zapu38.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30565
- /detailse3de-3.html
imported:
- "2019"
_4images_image_id: "30565"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-05-14T22:15:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30565 -->
Die nicht gescheit pumpende Zahnradpumpe. Zwischen Ober- und Unterschale werkeln zwei 'alte' ft-Differenziale, wobei das 'werkeln' mit Masse ein 'planschen' sein dürfte.