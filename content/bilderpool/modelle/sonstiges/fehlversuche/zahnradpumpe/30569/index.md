---
layout: "image"
title: "Zapu_5258"
date: "2011-05-16T20:30:21"
picture: "Zapu_5258.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30569
- /detailsfe09-4.html
imported:
- "2019"
_4images_image_id: "30569"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-05-16T20:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30569 -->
Auch nicht besser: flachgedrehte und später noch plangefräste Rast-Z10, zwischen Plastikplatten als Gehäuse. Die Dicke stimmt nie: entweder sind die Zahnräder zu dick und der Aufbau klemmt, oder sie sind zu dünn und die Pumpe läuft leichtgängig, aber mit 100% Materialschlupf.