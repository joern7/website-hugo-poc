---
layout: "image"
title: "Geschwindikeitsmessanlage- von hinten nach vorne"
date: "2011-01-01T17:32:48"
picture: "sdfsfs5.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29578
- /details7228.html
imported:
- "2019"
_4images_image_id: "29578"
_4images_cat_id: "2154"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:32:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29578 -->
Im Hintergrund sieht man die erste Lichtschranke durch die der Zug fährt, um die Messung zu starten. Danach fährt er durch die zweite Lichtschranke um die Messung zu beenden. Die Geschwindikteit errechnet sich so: 
Weg : Zeit= m/s (Meter pro Sekunde)                                                          m/s *3,6= km/h
(Weg muss in Metern angeben werden und Zeit in Sekunden)