---
layout: "overview"
title: "LED-Modul für vierstellige 7-Segment-Anzeige"
date: 2020-02-22T08:30:36+01:00
legacy_id:
- /php/categories/2827
- /categories7292.html
- /categories68c3-2.html
- /categories9a5e-2.html
- /categories205f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2827 --> 
ft-fremde Í²C-Sensoren und -Aktoren lassen sich meist nicht ohne Weiteres stabil und dezent in ft-Modellen verbauen.
Für die vierstellige 7-Segment-LED-Anzeige von Conrad lässt sich jedoch ein Modul in den Maßen der Grundplatte 90 x 45 konstruieren, das zu größeren Anzeigen kombiniert werden kann.