---
layout: "image"
title: "LED-Modul: Ansicht von schräg vorne"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38016
- /detailsc8fb.html
imported:
- "2019"
_4images_image_id: "38016"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38016 -->
Mehrere Module lassen sich durch Achsen und Verbinder zu einer größeren Anzeige zusammenfügen.