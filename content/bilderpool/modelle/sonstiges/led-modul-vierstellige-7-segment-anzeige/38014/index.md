---
layout: "image"
title: "LED-Modul: Rückansicht (halb offen)"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38014
- /details22e0.html
imported:
- "2019"
_4images_image_id: "38014"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38014 -->
Einbau der Rückwand.