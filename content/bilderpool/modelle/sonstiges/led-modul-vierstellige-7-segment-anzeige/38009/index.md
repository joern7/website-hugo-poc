---
layout: "image"
title: "LED-Modul: Montage auf Grundplatte (von hinten)"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38009
- /details9c43.html
imported:
- "2019"
_4images_image_id: "38009"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38009 -->
