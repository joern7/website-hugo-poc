---
layout: "image"
title: "Gelenk mit Reibung"
date: "2014-04-17T22:41:08"
picture: "gelenkmitreibung1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38569
- /details2bd3.html
imported:
- "2019"
_4images_image_id: "38569"
_4images_cat_id: "2881"
_4images_user_id: "104"
_4images_image_date: "2014-04-17T22:41:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38569 -->
Wer mal ein schwergängiges Gelenk aus aktuellen Bauteilen benötigt, kann das hier verwenden. Die Klemmwirkung rührt von einem Federnocken her, um den sich die Pneumatik-Adapter drehen und der darin klemmt.