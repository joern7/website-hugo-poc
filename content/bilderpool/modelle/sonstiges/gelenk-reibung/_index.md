---
layout: "overview"
title: "Gelenk mit Reibung"
date: 2020-02-22T08:30:38+01:00
legacy_id:
- /php/categories/2881
- /categories08d5.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2881 --> 
Ein Gelenk, das seine jeweilige Stellung beibehält.