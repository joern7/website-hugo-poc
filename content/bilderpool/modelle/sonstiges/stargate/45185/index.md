---
layout: "image"
title: "Fremdwelt Gate"
date: "2017-02-11T21:15:13"
picture: "stargate43.jpg"
weight: "43"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45185
- /details9695-2.html
imported:
- "2019"
_4images_image_id: "45185"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45185 -->
Die Gates sind alle gleich aufgebaut.