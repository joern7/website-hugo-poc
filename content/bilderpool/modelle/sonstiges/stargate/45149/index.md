---
layout: "image"
title: "Gate Kontrollpult"
date: "2017-02-11T21:15:13"
picture: "stargate07.jpg"
weight: "7"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45149
- /details431c.html
imported:
- "2019"
_4images_image_id: "45149"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45149 -->
Ohne Plexiglas, die Feuchtigkeits-Sensoren sind am kapazitiven Touch-Kontroller angeschlossen.