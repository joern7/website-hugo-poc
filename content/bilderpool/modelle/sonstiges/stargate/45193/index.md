---
layout: "image"
title: "Gate Chevron"
date: "2017-02-11T21:15:13"
picture: "stargate51.jpg"
weight: "51"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45193
- /detailsd9d8.html
imported:
- "2019"
_4images_image_id: "45193"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45193 -->
Abdeckung für die äußeren Gate LEDs.
Gebaut aus Plexiglas und einer ausgeschnittenen Lampenkappe