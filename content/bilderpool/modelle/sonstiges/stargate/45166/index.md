---
layout: "image"
title: "Gate"
date: "2017-02-11T21:15:13"
picture: "stargate24.jpg"
weight: "24"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45166
- /detailsd61f-2.html
imported:
- "2019"
_4images_image_id: "45166"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45166 -->
Die Mannschaft ist vollzählig zurück und der Schild wurde aktiviert.
Wenn der Schild aktiviert ist leuchtet das Gate Rot.