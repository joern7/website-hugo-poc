---
layout: "image"
title: "Flügeltüren"
date: "2017-02-11T21:15:13"
picture: "stargate28.jpg"
weight: "28"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45170
- /detailsf23c.html
imported:
- "2019"
_4images_image_id: "45170"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45170 -->
Sie werden über den Taster und Wechselschalter gesteuert.