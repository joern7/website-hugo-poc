---
layout: "image"
title: "Gesamtansicht"
date: "2017-02-11T21:15:12"
picture: "stargate01.jpg"
weight: "1"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45143
- /detailsff62.html
imported:
- "2019"
_4images_image_id: "45143"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45143 -->
Der gesamte Komplex bestehend aus:
Gateraum
Aufenthaltsraum
Krankenstation 
Forschungslabor 
Landeplätzen