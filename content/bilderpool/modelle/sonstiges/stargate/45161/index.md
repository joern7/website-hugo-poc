---
layout: "image"
title: "Die Elektronik"
date: "2017-02-11T21:15:13"
picture: "stargate19.jpg"
weight: "19"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45161
- /details0f79-2.html
imported:
- "2019"
_4images_image_id: "45161"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45161 -->
Unter der Treppe sind die Kontrollpulte angeschlossen.