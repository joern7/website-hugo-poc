---
layout: "image"
title: "Die Beleuchtung"
date: "2017-04-14T22:49:00"
picture: "raumschiff14.jpg"
weight: "14"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45754
- /detailsaae4-2.html
imported:
- "2019"
_4images_image_id: "45754"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45754 -->
Nach vorne sind 2 rainbow leds angebaut und unten jeweils eine rote Positionslampe