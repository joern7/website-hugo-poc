---
layout: "image"
title: "Die Unterseite"
date: "2017-04-14T22:49:00"
picture: "raumschiff13.jpg"
weight: "13"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45753
- /detailsd5c5.html
imported:
- "2019"
_4images_image_id: "45753"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45753 -->
Das Schiff steht nur auf den drei Füßen die seitlichen "Antriebe" sind nur zur Dekoration.