---
layout: "overview"
title: "Stargate"
date: 2020-02-22T08:32:15+01:00
legacy_id:
- /php/categories/3365
- /categoriese2c8.html
- /categories8956.html
- /categories538c.html
- /categories37c7.html
- /categories13a7.html
- /categories3f25.html
- /categories5662.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3365 --> 
Das Stargate ist ein Tor um in wenigen Sekunden von Planet zu Planet, also von einem Gate zum nächsten zu reisen.