---
layout: "image"
title: "Flügeltüren"
date: "2017-02-11T21:15:13"
picture: "stargate26.jpg"
weight: "26"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45168
- /details1375-2.html
imported:
- "2019"
_4images_image_id: "45168"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45168 -->
Angetrieben durch einen mini Motor und untereinander durch Ketten verbunden.