---
layout: "image"
title: "Die Elektronik"
date: "2017-02-11T21:15:13"
picture: "stargate18.jpg"
weight: "18"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45160
- /details8bdd-2.html
imported:
- "2019"
_4images_image_id: "45160"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45160 -->
Die Pneumatikventiele links sind zur Steuerung der Türen, der Kompressor befindet sich unter dem oberen Boden.
Die zwei rechten LEDs sind die Statusanzeigen der Gateverbindung, bei Verbindung pulsieren sie Blau.