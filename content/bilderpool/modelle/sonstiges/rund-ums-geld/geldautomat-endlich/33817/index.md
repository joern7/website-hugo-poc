---
layout: "image"
title: "Im Kartenschlitz"
date: "2011-12-28T22:55:31"
picture: "geldautomat4.jpg"
weight: "4"
konstrukteure: 
- "M.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33817
- /details2786.html
imported:
- "2019"
_4images_image_id: "33817"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33817 -->
Hier sieht man nun die Innereien des Kartenschlitzes, wie man sieht ist nicht viel zu sehen, nur der Taster und die eingesteckte Karte. Bei Betätigung des Tasters, werden der Kompressor und das Magnetventil mit Strom versorgt. Der Kompressor erzeugt nun Luft und das Magnetventil schaltet nun den Luftweg von Kompressor zu den Handventilen frei. Eine ausführliche Beschreibung und Skizze findest du in der ft:pedia 4/2011.