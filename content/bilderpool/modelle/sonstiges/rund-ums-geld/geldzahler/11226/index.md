---
layout: "image"
title: "Geldscheinzähler"
date: "2007-07-29T13:00:52"
picture: "geld8.jpg"
weight: "8"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11226
- /details404a.html
imported:
- "2019"
_4images_image_id: "11226"
_4images_cat_id: "1014"
_4images_user_id: "557"
_4images_image_date: "2007-07-29T13:00:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11226 -->
und hier nochmal von oben