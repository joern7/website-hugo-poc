---
layout: "image"
title: "Geldsortierer 10"
date: "2008-11-18T17:22:05"
picture: "Geldsortierer_10.jpg"
weight: "9"
konstrukteure: 
- "Michael Sengstschmid"
fotografen:
- "Michael Sengstschmid"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16352
- /details26ca-2.html
imported:
- "2019"
_4images_image_id: "16352"
_4images_cat_id: "1014"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:22:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16352 -->
Und das Ganze noch einmal von der anderen Seite.