---
layout: "overview"
title: "Maoam-Automat mit QR-Code-Kartenleser"
date: 2020-02-22T08:28:34+01:00
legacy_id:
- /php/categories/3141
- /categories9aaa-2.html
- /categories8e0e.html
- /categories9d98.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3141 --> 
Ein Maoam-Warenautomat, bei dem QR-Code-Karten als Bezahlsystem eingesetzt werden.