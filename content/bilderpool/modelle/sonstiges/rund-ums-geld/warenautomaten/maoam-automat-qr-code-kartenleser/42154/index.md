---
layout: "image"
title: "Der Auswerfer von hinten"
date: "2015-10-26T17:17:22"
picture: "DSC08338_1.jpg"
weight: "5"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42154
- /details2948.html
imported:
- "2019"
_4images_image_id: "42154"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T17:17:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42154 -->
Mit etwas Fantasie (schwarzer Adler auf schwarzem Grund) erkennt man rechts den Motor, links unter dem Zylinder das Hubgetriebe und obendrauf das untere Ende des LED-Mastes.

Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168