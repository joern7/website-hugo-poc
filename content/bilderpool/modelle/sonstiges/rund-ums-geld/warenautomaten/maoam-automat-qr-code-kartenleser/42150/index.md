---
layout: "image"
title: "Frontalansicht"
date: "2015-10-26T14:47:12"
picture: "DSC08334_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42150
- /details1b1f.html
imported:
- "2019"
_4images_image_id: "42150"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42150 -->
Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168

Links die Warenschächte, darüber die Anzeigetäfelchen mit den vorhandenen Sorten.
Rechts der Kartenleser, eine einfach USB-Webcam, deren Bild am PC ausgewertet wird. Darunter die vier Bedientasten - zwei Auswahltasten, die Bestätigungs- und die Abbruchtaste.

Hinter den Warenschächten läuft ein S-Motor mit Hubgetriebe auf einer Zahnschiene.
An ihm ist ein Pneumatik-Federzylinder befestigt, der das Maoam aus den Warenschacht schiebt.
Außerdem besitzt er einen Arm, auf dem eine LED sitzt, die das Anzeigetäfelchen des gerade ausgewählten Schachts von hinten beleuchtet.
Auswurfmechanismus und Anzeige-LED werden also zum jeweils gewünschen Warenschacht bewegt.

Ein Video gibts hier:
https://www.youtube.com/watch?v=M8RHEYUFU7U