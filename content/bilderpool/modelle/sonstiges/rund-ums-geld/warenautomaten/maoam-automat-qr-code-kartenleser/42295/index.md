---
layout: "image"
title: "Jetzt neu: mit Gehäuse"
date: "2015-11-07T18:07:35"
picture: "DSC08436_sc01.jpg"
weight: "8"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42295
- /details2e83.html
imported:
- "2019"
_4images_image_id: "42295"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-11-07T18:07:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42295 -->
...heute morgen im Baumarkt gabs die Plexiglasplatte 100x50x0,25cm für 5¤ wegen Sortimentswechsel... 

Man beachte das sanft-blaue Schimmern im Kartenschacht :-)