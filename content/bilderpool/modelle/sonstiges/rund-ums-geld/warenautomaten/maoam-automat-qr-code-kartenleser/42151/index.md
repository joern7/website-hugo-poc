---
layout: "image"
title: "Kartenschacht und Bedientasten"
date: "2015-10-26T14:47:12"
picture: "DSC08335_1.jpg"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42151
- /details158a-2.html
imported:
- "2019"
_4images_image_id: "42151"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42151 -->
Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168

Der Kartenschacht wird innen von einer LED mit blauer Kappe ausgeleuchtet. Über dem Kartenschacht kann man die Webcam erkennen, die die QR-Codes scannt.

Unter dem Kartenschacht die vier Bedientasten: 
Links- und Rechtstaste zur Auswahl der Maoam-Sorte, "Kaufen"- und "Abbruch-" Taste