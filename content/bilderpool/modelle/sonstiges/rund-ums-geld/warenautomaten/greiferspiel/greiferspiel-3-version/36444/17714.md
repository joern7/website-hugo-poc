---
layout: "comment"
hidden: true
title: "17714"
date: "2013-01-06T23:50:59"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist das erste Mal, dass ich eine Nabe (ganz ohne Narben ;-) auf einem Zylinderkolben sehe. Prima Idee, um die Auslenkung zu begrenzen.

Gruß,
Stefan