---
layout: "image"
title: "Greiferspiel 2.Version Greifer hat was"
date: "2010-06-04T10:54:31"
picture: "greiferspielversion6.jpg"
weight: "6"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27362
- /detailsd8c8.html
imported:
- "2019"
_4images_image_id: "27362"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27362 -->
Der Boden des Spiel sit mit vielen FT-Reifen bestückt. So kann man abwechselnd spielen und wer die meisten Reifen gegriffen hat, hat gewonnen ;-)