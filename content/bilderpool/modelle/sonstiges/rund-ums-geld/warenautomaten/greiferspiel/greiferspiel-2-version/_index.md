---
layout: "overview"
title: "Greiferspiel 2.Version"
date: 2020-02-22T08:28:30+01:00
legacy_id:
- /php/categories/1965
- /categories5408.html
- /categories5645-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1965 --> 
Hier ist eine Neuauflage meines Greiferspiels. Diesmal etwas kompakter und "ordentlicher".
Dafür war die Bauzeit auch sehr viel länger!