---
layout: "image"
title: "Greiferspiel Gesamtanscicht"
date: "2010-04-07T12:40:31"
picture: "greiferspiel1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26889
- /details9890-3.html
imported:
- "2019"
_4images_image_id: "26889"
_4images_cat_id: "1927"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26889 -->
