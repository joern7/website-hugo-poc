---
layout: "image"
title: "Frontansicht"
date: "2011-10-19T16:49:06"
picture: "DSCF7744.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33235
- /detailsf76e.html
imported:
- "2019"
_4images_image_id: "33235"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33235 -->
Mein Fahrkartenautomat von vorne. Man sieht (von links nach rechts)
den Geldeinzug (5€) dann folgt die Fahrkartenausgabe. Unten befindet sich die Geldrückgabe (1€ Münzen) und etwas darüber die 7 Segment Anzeige. (Preis in €)
Weiter links ist der Bildschirm, welcher nach zahlreichen Fehlversuchen nun ein hoch und runter fahrendes Element ist.
Ganz links kann man dann noch den Münzeinwurf erkennen. Er nimmmt 50ct und 2€.
(Die Mitglieder im Chat erinnern sich sicher noch an meine seltsamen Probleme...Und Dinge wie Papiereinzug (Forum) und 7 Segment Anzeige habe ich auch mit Hilfe des Forums gelöst)