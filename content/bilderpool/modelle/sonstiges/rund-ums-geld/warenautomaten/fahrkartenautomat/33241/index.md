---
layout: "image"
title: "Scheinannahme"
date: "2011-10-19T16:49:06"
picture: "DSCF7753.jpg"
weight: "7"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33241
- /details8f90-3.html
imported:
- "2019"
_4images_image_id: "33241"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33241 -->
Ein leider etwas zu dunkel geratenes Bild der Scheinannahme. Die beiden Rollen nehmen die Scheine an, ein Sensor misst, ob auch wirklich ein solcher hindurch geschoben wurde. Leider wird nicht zwischen Papier und Geldschein unterschieden.