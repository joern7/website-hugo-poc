---
layout: "image"
title: "Detailaufnahme Münzeinwurf"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat04.jpg"
weight: "4"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31758
- /details96b3.html
imported:
- "2019"
_4images_image_id: "31758"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31758 -->
Das ist ein Detail des Münzeinwurfs, gleiche Funktion, wie in Version 1. Man wirft 20 Cent-Münzen ein.