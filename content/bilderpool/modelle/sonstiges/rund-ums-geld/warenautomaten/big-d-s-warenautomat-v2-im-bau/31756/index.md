---
layout: "image"
title: "Detailaufnahme der Front(1)"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat02.jpg"
weight: "2"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31756
- /details171c.html
imported:
- "2019"
_4images_image_id: "31756"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31756 -->
Zu sehen: 
drei Taster, als Steuerelemente und drei Leuchten in den Farben rot, grün und blau.