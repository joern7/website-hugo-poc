---
layout: "image"
title: "Innenansicht(4)"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat08.jpg"
weight: "8"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31762
- /details6ae1-3.html
imported:
- "2019"
_4images_image_id: "31762"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31762 -->
Das ist mein Hi-Power Kompressor.
Er schafft die Fischertechnik-typischen 0,3 Bar in 3 Sekunden.