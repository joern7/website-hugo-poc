---
layout: "image"
title: "Detailaufnahme der Front(2)"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat03.jpg"
weight: "3"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31757
- /detailsd171.html
imported:
- "2019"
_4images_image_id: "31757"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31757 -->
Hier sieht man einen verborgenen Schlüsselschalter zum Administrieren des Automaten.