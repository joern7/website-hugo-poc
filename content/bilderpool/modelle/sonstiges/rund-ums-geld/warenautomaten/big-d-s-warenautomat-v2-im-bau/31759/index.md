---
layout: "image"
title: "Innenansicht(1)"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat05.jpg"
weight: "5"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31759
- /details40cf.html
imported:
- "2019"
_4images_image_id: "31759"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31759 -->
Man erkennt die 1,5 Literflasche als Getränkebehälter für Wasser etc.