---
layout: "image"
title: "Geldsortierer 09"
date: "2008-11-18T16:44:39"
picture: "Geldsortierer_09.jpg"
weight: "9"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16335
- /detailsd035.html
imported:
- "2019"
_4images_image_id: "16335"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16335 -->
Der obere Teil von hinten.