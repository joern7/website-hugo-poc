---
layout: "image"
title: "Geldsortierer 19"
date: "2008-11-18T17:03:24"
picture: "Geldsortierer_19.jpg"
weight: "18"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16344
- /detailscb8f.html
imported:
- "2019"
_4images_image_id: "16344"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:03:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16344 -->
Ein paar Sekunden später…
Als Sammelbehälter dienen übrigens Topfenbecher (natürlich ausgewaschen).
(Ich glaube, Topfen heißt auf Deutsch Quark)