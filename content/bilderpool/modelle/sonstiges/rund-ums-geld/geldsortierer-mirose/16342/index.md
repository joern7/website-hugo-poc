---
layout: "image"
title: "Geldsortierer 17"
date: "2008-11-18T16:59:54"
picture: "Geldsortierer_17.jpg"
weight: "16"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16342
- /details2f36-2.html
imported:
- "2019"
_4images_image_id: "16342"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16342 -->
Der Vereinzelner bei der Arbeit.