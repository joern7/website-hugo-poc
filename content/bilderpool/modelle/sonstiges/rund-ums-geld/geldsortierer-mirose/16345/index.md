---
layout: "image"
title: "Geldsortierer 20"
date: "2008-11-18T17:03:25"
picture: "Geldsortierer_20.jpg"
weight: "19"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16345
- /detailse056-2.html
imported:
- "2019"
_4images_image_id: "16345"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16345 -->
Und wieder etwas später…