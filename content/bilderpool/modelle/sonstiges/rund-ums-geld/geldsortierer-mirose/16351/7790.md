---
layout: "comment"
hidden: true
title: "7790"
date: "2008-11-19T07:50:27"
uploadBy:
- "sulu007"
license: "unknown"
imported:
- "2019"
---
Hallo,
was für eine klasse Anlage. Ganz schöne friemelarbeit bei der Einstellung der Größen, wenn man mal den kleinen Größenunterschied zwischen 5 Cent un 20 Cent oder zwischen 50 Cent und 1 Euro sich ansieht.
Zum Glück hast du keine Sortieranlage für Scheine gebaut, da hätte ich das mit den Größenverhältnissen nicht prüfen können, wer hat schon einen 500 euro oder 200 Euro Schein in der Tasche.
Zählen würde über Lichtschranke gehen, oder benutzt eine Präzisionswaage => Gemessenes Gewicht/ Gewicht einer Münze.