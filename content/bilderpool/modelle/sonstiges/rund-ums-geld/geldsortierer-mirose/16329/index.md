---
layout: "image"
title: "Geldsortierer 03"
date: "2008-11-18T16:44:38"
picture: "Geldsortierer_03.jpg"
weight: "3"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16329
- /detailsdd84.html
imported:
- "2019"
_4images_image_id: "16329"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16329 -->
Noch einmal die Gesamtansicht, anderer Winkel.