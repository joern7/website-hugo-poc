---
layout: "image"
title: "Geldsortierer 11"
date: "2008-11-18T16:59:01"
picture: "Geldsortierer_11.jpg"
weight: "10"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16336
- /details57b4-4.html
imported:
- "2019"
_4images_image_id: "16336"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16336 -->
Noch einmal der rechte Teil.