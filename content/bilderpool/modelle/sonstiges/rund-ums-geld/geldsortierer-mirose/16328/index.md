---
layout: "image"
title: "Geldsortierer 02"
date: "2008-11-18T16:44:38"
picture: "Geldsortierer_02.jpg"
weight: "2"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16328
- /detailsbd63.html
imported:
- "2019"
_4images_image_id: "16328"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16328 -->
Noch einmal die Gesamtansicht.