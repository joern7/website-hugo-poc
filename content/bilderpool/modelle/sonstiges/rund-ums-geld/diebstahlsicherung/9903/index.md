---
layout: "image"
title: "Diebstahlsicherung"
date: "2007-04-03T17:33:55"
picture: "antidieb4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9903
- /details2ac1.html
imported:
- "2019"
_4images_image_id: "9903"
_4images_cat_id: "895"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9903 -->
Taster zum Entsichern