---
layout: "image"
title: "05 Schieber"
date: "2012-04-02T19:39:17"
picture: "geldwechsler5.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34746
- /detailsf380-3.html
imported:
- "2019"
_4images_image_id: "34746"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34746 -->
Der Motor treibt über die Zahnräder eine Kurbel an, die den Schieber vor und zurück bewegt