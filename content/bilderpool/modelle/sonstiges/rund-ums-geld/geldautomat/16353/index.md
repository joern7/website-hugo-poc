---
layout: "image"
title: "Geldsortierer 27-32"
date: "2008-11-20T17:27:39"
picture: "Geldsortierer_27-32.jpg"
weight: "3"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16353
- /details1a39.html
imported:
- "2019"
_4images_image_id: "16353"
_4images_cat_id: "496"
_4images_user_id: "765"
_4images_image_date: "2008-11-20T17:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16353 -->
1. Nachtrag, Detail Sortierer

Ich habe noch einmal einen der Sortierer auf Wunsch von Laserman nachgebaut.

Links oben: Ansicht von links

Oben Mitte: von hinten

Oben rechts: von vorne

Unten links: noch einmal von links 

Unten Mitte: noch einmal von vorne

Unten rechts: einzelne Baugruppen; der obere Teil ist der Entscheidende: je nachdem, wie weit er hineingeschoben wird, entscheidet er, ob die Münze noch weiterrutschen darf, oder durch das Loch fällt.
Schön erkennt man die Längsneigung des Sortierers von 15° und die Querneigung von 37,5°.

Die einzelnen Baugruppen werden von Verbindungsstücken 30 (31061) zusammengehalten. Natürlich kann man auch Federnocken (31982) oder Verbindungsstücke 15 (31060) verwenden.
Befestigung der Bauplatte 90 x 30 (38251) erfolgt mit jeweils einer Bauplatte 15 x 30 x 5 mit durchgängigen Nuten und einer Bauplatte 15 x 30 x 5 ohne durchgängigen Nuten. (Die Alten sind noch bei Fischerfriendswoman erhältlich) sowie 2 Federnocken.
Es fehlt noch die vordere Versteifung mit Statikbauteilen zwischen den Bausteinen 15.