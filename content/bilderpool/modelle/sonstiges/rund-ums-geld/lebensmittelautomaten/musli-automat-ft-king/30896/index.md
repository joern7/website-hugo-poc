---
layout: "image"
title: "Müsliautomat von rechts"
date: "2011-06-22T11:49:08"
picture: "01_01_10-_24_01_10_015.jpg"
weight: "3"
konstrukteure: 
- "ft-King"
fotografen:
- "ft-King"
keywords: ["Müsliautomat", "Muesliautomat", "Müslimaschine", "Mueslimaschine"]
uploadBy: "ft-King"
license: "unknown"
legacy_id:
- /php/details/30896
- /detailsc7bd.html
imported:
- "2019"
_4images_image_id: "30896"
_4images_cat_id: "2306"
_4images_user_id: "1329"
_4images_image_date: "2011-06-22T11:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30896 -->
Müsliautomat von rechts