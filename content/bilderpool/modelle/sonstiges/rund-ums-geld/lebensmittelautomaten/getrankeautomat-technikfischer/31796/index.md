---
layout: "image"
title: "Straße von vorne"
date: "2011-09-14T19:23:07"
picture: "technikfischer03.jpg"
weight: "3"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31796
- /details35a7.html
imported:
- "2019"
_4images_image_id: "31796"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31796 -->
Hier ist der Wagen zu sehen