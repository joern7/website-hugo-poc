---
layout: "comment"
hidden: true
title: "19279"
date: "2014-07-24T22:49:14"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Der Wagen hat vier rote Räder, die auf den Spurschienen laufen, vom Betrachter aus ins Bild hinein. Linke und rechte Seite sind mit grauen Bausteinen ausgeführt, der Rest in schwarz.
Das Ding links im Bild dürfte ein kleiner Laser sein (nicht Original-ft), der mit Kabelbindern in einem ft-Stein fixiert ist und mit dem Sensor rechts gegenüber eine Lichtschranke darstellt.