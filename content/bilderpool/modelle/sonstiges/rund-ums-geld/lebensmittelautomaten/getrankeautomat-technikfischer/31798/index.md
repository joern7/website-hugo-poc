---
layout: "image"
title: "Antriebsmotor"
date: "2011-09-14T19:23:07"
picture: "technikfischer05.jpg"
weight: "5"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31798
- /details6710.html
imported:
- "2019"
_4images_image_id: "31798"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31798 -->
Dieser Motor treibt den Wagen an. Die kleine Feder oberhalb des Rades dient zur Spannung der Schnur