---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T16:04:39"
picture: "wasserspender3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28016
- /detailsd1ea.html
imported:
- "2019"
_4images_image_id: "28016"
_4images_cat_id: "2031"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T16:04:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28016 -->
Hier könnt ihr die Kompressoreinheit sehen, im Vordergrund den Taster, mit ihm kann man den Kompressor einschalten, der produziert wiederum Luft, und drückt das Wasser aus dem Tank.