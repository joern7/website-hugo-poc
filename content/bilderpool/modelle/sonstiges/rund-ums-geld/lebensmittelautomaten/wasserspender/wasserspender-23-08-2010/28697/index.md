---
layout: "image"
title: "Equipmentbox"
date: "2010-09-28T16:46:06"
picture: "ag12.jpg"
weight: "12"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28697
- /details6b4a.html
imported:
- "2019"
_4images_image_id: "28697"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28697 -->
Hier kann man meine Equipmentbox sehen, in ihr bewahre ich die Deckel auf, zum Verschließen der Flaschen, und noch die austauscharen Bedienschilder.