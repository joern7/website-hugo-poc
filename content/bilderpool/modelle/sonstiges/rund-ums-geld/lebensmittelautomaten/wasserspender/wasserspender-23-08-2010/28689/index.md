---
layout: "image"
title: "Münzfach"
date: "2010-09-28T16:46:04"
picture: "ag04.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28689
- /detailsdf67.html
imported:
- "2019"
_4images_image_id: "28689"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28689 -->
Hier werden die hineingeworfenen Münzen gesammelt. Mann/Frau kann auch sehen, dass hinter dem Geldschlitz die Lichtschranke sitz, die prüft, ob man eine Münze hineingeschmissen hat. Der Papierstreifen dient dazu, restliche Münzen  aus dem Münzfach herauszunehmen.