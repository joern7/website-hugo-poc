---
layout: "image"
title: "Flaschen"
date: "2010-09-28T16:46:06"
picture: "ag13.jpg"
weight: "13"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28698
- /details2d94.html
imported:
- "2019"
_4images_image_id: "28698"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28698 -->
Hier kann man die Flaschen von der Seite sehen, die Linke ist mit blauer Lebensmittelfarbe gefüllt und die andere mit (ausgebleichter) gelben Farbe. 
Der Tipp mit den Deckeln stammt von fitec, vielen Dank hierfür. Man nehem handelsübliche Wattestäbchen, schneide die Watte ab, bohre in den Deckel, 2,3,.. Löcher, und versiegele es von innen mit 2-Komponenten-Kleber , oder Heißklebe.