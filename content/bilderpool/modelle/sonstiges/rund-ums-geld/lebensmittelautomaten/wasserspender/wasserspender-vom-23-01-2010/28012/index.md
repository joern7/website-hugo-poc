---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T14:44:04"
picture: "wasserspendervom2.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28012
- /details8851.html
imported:
- "2019"
_4images_image_id: "28012"
_4images_cat_id: "2030"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:44:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28012 -->
