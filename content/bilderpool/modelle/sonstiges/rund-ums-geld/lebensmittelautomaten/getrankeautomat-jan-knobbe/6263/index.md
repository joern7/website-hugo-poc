---
layout: "image"
title: "Getränkeautomat Transport"
date: "2006-05-11T19:59:39"
picture: "getraenkeautomat_transport.jpg"
weight: "4"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6263
- /details4c14-3.html
imported:
- "2019"
_4images_image_id: "6263"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T19:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6263 -->
Auf dieser früheren Version des Gerätes kann man erkennen, wie der Bechertransport, unterstützt durch Rollen, noch besser funktionieren kann. Die Taster sind wieder Auslöser des nächsten Programmschrittes.