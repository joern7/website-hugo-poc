---
layout: "image"
title: "Getränkeautomat schräg von oben"
date: "2006-05-11T20:00:49"
picture: "getraenkeautomat_schraeg_von_oben.jpg"
weight: "6"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6265
- /details06a0.html
imported:
- "2019"
_4images_image_id: "6265"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T20:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6265 -->
Die Pumpe ist nach der Vorlage eines FT-Club-Heftes aus den 70er Jahren nachgebaut