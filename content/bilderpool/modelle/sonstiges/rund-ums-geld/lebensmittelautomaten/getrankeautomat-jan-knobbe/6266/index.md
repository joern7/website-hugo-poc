---
layout: "image"
title: "Getränkeautomat von hinten"
date: "2006-05-11T20:01:18"
picture: "getraenkeautomat_von_hinten.jpg"
weight: "7"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6266
- /details7c13.html
imported:
- "2019"
_4images_image_id: "6266"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T20:01:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6266 -->
Ich habe zwei MF, ein FF, zwei GB, zwei Relais-Bausst. und das Relais aus Hobby III verwendet. Die Grundbausteine sind als Verstärker für die beiden Fühler notwendig (Münz-Lichtschranke und Feuchtigkeitsfühler - wenn Becher voll). Fünf Motoren sind eingebaut: Bechertransport, Pumpe, je ein Förderband für die beiden Brausetablettensorten und ein Motor, der den Feuchtigkeitsfühler in den Becher senkt und wieder anhebt.