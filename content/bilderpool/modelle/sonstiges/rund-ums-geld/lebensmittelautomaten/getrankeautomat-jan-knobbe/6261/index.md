---
layout: "image"
title: "Getränkeautomat von der Seite"
date: "2006-05-11T19:57:50"
picture: "getraenkeautomat_von_der_seite.jpg"
weight: "2"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6261
- /detailsbe4e.html
imported:
- "2019"
_4images_image_id: "6261"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T19:57:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6261 -->
Auf der Grundplatte ist für 5 Becher Platz, die, jeweils nach vorn geschoben werden.