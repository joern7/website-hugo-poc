---
layout: "image"
title: "Gesamtansicht"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13904
- /details2999.html
imported:
- "2019"
_4images_image_id: "13904"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13904 -->
Der Getränkeautomat funktioniert folgerdermaßen: Der Kunde wählt das gewünschte Getränk und wirft Geld ein.Das Ventil wählt die richtige Flasche und der Kompressor wird eingeschaltet. Er pumpt Luft in die Flasche. Durch den hohen Druck steigt die Flüssigkeit durch den Wasserschlauch und fließt in den Becher. Der Kunde kann das Getränk entnehmen.