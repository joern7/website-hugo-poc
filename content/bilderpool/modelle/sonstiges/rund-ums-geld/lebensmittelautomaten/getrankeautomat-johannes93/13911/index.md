---
layout: "image"
title: "Ventilsteuerung"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat08.jpg"
weight: "8"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13911
- /details2120-2.html
imported:
- "2019"
_4images_image_id: "13911"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13911 -->
Das Ventil wählt zwischen den zwei Getränkesorten.