---
layout: "image"
title: "Anleitung"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat04.jpg"
weight: "4"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13907
- /details74ab.html
imported:
- "2019"
_4images_image_id: "13907"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13907 -->
