---
layout: "image"
title: "Schrägansicht"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13920
- /details5477-2.html
imported:
- "2019"
_4images_image_id: "13920"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13920 -->
