---
layout: "image"
title: "Wasserschlauch"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat13.jpg"
weight: "13"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13916
- /detailsb351-2.html
imported:
- "2019"
_4images_image_id: "13916"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13916 -->
Am Wasserschlauch sind zwei Kabel befestigt. Sie melden dem Interface ob noch Wasser in der der Flasche ist.