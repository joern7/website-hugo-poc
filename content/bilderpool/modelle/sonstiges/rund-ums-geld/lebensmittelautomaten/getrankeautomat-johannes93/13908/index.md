---
layout: "image"
title: "Bedienfeld von der Seite"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat05.jpg"
weight: "5"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13908
- /details8606.html
imported:
- "2019"
_4images_image_id: "13908"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13908 -->
