---
layout: "image"
title: "Kompressor"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat07.jpg"
weight: "7"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13910
- /details25e7.html
imported:
- "2019"
_4images_image_id: "13910"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13910 -->
