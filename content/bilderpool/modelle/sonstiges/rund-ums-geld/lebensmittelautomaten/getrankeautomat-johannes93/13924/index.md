---
layout: "image"
title: "Warnlichter"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat21.jpg"
weight: "21"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13924
- /details6d5e-2.html
imported:
- "2019"
_4images_image_id: "13924"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13924 -->
Die einzelnen Warnlichter blinken, wenn entweder kein Becher, oder das gewünschte Getränk, nicht mehr verfügar ist.