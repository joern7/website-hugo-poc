---
layout: "image"
title: "Innenansicht (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot16.jpg"
weight: "16"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42494
- /details2487.html
imported:
- "2019"
_4images_image_id: "42494"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42494 -->
Hier sind drei der vier Schokoladensorten gut zu erkennen und wie sie in den Kartuschen gestapelt werden. Zu Bemerken ist noch, dass als Ware RitterSport Tafeln verwendet wurden, jedoch die mit 7,5 Gramm und nicht die mit 16.75 Gramm.