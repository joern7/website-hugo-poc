---
layout: "image"
title: "Der Saugheber"
date: "2015-12-07T17:49:00"
picture: "schorobot03.jpg"
weight: "3"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42481
- /detailsc747.html
imported:
- "2019"
_4images_image_id: "42481"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42481 -->
Um bei der Schokoladenausgabe festzustellen, wie tief nach unten "getaucht" werden muss, um bis zum Schokoladengrund durchzustoßen ist auf der nach unten zeigenden Seite eine Metallachse durchgeschoben, die am anderen Ende des Aluminiumprofils mit Hilfe eines Tasters feststellen kann, ob der Sauger eine Schokoladentafel berührt