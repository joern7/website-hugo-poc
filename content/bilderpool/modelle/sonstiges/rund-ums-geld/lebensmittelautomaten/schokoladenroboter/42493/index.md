---
layout: "image"
title: "Innenansicht"
date: "2015-12-07T17:49:00"
picture: "schorobot15.jpg"
weight: "15"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42493
- /details2e93-2.html
imported:
- "2019"
_4images_image_id: "42493"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42493 -->
Auf diesem Foto kann man die Fahrspur der Laufkatze nachvollziehen. Denn diese fährt auf der Schiene nach vorne oder nach hinten, um die verschiedenen Sorten zu erreichen.