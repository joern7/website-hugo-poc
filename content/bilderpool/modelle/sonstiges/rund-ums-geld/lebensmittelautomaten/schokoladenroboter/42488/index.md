---
layout: "image"
title: "Gesamtansicht Laufkatze"
date: "2015-12-07T17:49:00"
picture: "schorobot10.jpg"
weight: "10"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42488
- /details35e6-2.html
imported:
- "2019"
_4images_image_id: "42488"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42488 -->
Gut zu erkennen sind hier drei der vier Hauptkartuschen, die fünfte (Neben-) Kartusche und die Laufkatze mit dem Saugheber (von der Stütze verdeckt)