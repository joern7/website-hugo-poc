---
layout: "image"
title: "Die fünfte Sorte"
date: "2015-12-07T17:49:00"
picture: "schorobot09.jpg"
weight: "9"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42487
- /details6ae8.html
imported:
- "2019"
_4images_image_id: "42487"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42487 -->
Hier sieht man die Ausgabestation der fünften Schokoladensorte beziehungsweise den Förderbandantrieb.