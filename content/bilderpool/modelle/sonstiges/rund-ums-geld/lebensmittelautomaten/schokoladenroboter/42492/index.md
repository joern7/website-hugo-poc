---
layout: "image"
title: "Kompressorstation (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot14.jpg"
weight: "14"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42492
- /details74f3.html
imported:
- "2019"
_4images_image_id: "42492"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42492 -->
Auf dieser Aufnahme kann man gut die Luftverteilung und die beiden orangen Lampen sehen. Diese werden jedoch nur zur saubereren Verkabelung der beiden Ventile und des Kompressors benötigt.