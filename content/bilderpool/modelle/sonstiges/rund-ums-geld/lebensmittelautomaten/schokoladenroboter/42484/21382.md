---
layout: "comment"
hidden: true
title: "21382"
date: "2015-12-08T22:47:29"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Schönes Modell. Das volle Programm: Mechanik, Pneumatik, ein wenig Computing.
Und beim Bauen hat man sogar genug Nerven-Nahrung zur Hand!
Deine Geldprüfung ist sehr großzügig - nach der Aufschrift akzeptiert der Automat angeblich nur 10 und 20 ct-Stücke.... ;-)
Gruß, Dirk