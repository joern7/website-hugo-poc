---
layout: "image"
title: "Tresor offen"
date: "2007-09-25T09:40:59"
picture: "P1040343_003.jpg"
weight: "3"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/11991
- /details8864.html
imported:
- "2019"
_4images_image_id: "11991"
_4images_cat_id: "1104"
_4images_user_id: "637"
_4images_image_date: "2007-09-25T09:40:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11991 -->
Zustand des Tresors wenn die richtige Zahlenkombination eingegeben wurde.(wenn man eine falsche Zahl eingibt fängt er wieder von vorn an) Zusätzlich wird der Tresor mit 4 Lampen beleuchtet. 
Inhalt:Taschenmesser