---
layout: "image"
title: "Tresor"
date: "2010-10-10T15:36:11"
picture: "mechanischertresor10.jpg"
weight: "10"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28980
- /details9784.html
imported:
- "2019"
_4images_image_id: "28980"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:11"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28980 -->
Übersicht