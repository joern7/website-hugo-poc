---
layout: "image"
title: "Tresor"
date: "2007-04-21T14:21:53"
picture: "tresor4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10143
- /details55a8.html
imported:
- "2019"
_4images_image_id: "10143"
_4images_cat_id: "917"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10143 -->
Summer