---
layout: "comment"
hidden: true
title: "12032"
date: "2010-08-26T22:14:09"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Mir gefallen ja solche echten Anwendungen immer. Und wenn sie dann noch so einfach (im Sinne von nicht unnötig künstlich verkompliziert) gebaut sind, erst recht.

Wenn der Regen aufhört, bleibt aber in der Wanne das Wasser stehen. Würde es auch funktionieren, wenn die Drähte nur auf einer Platte auflägen?

Gruß,
Stefan