---
layout: "image"
title: "Bewässerungsanlage V2 - Gesamtansicht"
date: "2011-02-15T21:14:52"
picture: "bwv01.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29991
- /details427f.html
imported:
- "2019"
_4images_image_id: "29991"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29991 -->
So, in der letzten Zeit hat sich bei meiner Bewässerungsanlage viel getan:

Jetzt fährt nämlich ein Arm zu den Pflanzen um sie zu gießen. Auch gibt es jetzt eine andere Ventilsteuerung, Ventilatoren, Lichter und ein Bedienfeld, aber dazu später mehr. 
Beschreibungen findet ihr jeweils bei den jeweiligen Bildern. An der Steuereinheit hat sich von der V1 zur V2 eigentlich nichts getan. 

MfG
Endlich

PS: Und hier gibt es noch ein Video: http://www.youtube.com/watch?v=QZNglEZQXwo