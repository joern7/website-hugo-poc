---
layout: "image"
title: "Bewässerungsanlage V2 - Arm"
date: "2011-02-15T21:14:52"
picture: "bwv03.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29993
- /details1131.html
imported:
- "2019"
_4images_image_id: "29993"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29993 -->
Hier kann man den Arm sehen, der dann zu den einzelnen Positionen fährt, um die Pflanzen zu bewässern.