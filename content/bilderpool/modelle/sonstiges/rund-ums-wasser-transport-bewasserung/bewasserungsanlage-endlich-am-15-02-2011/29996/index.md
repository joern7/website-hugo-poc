---
layout: "image"
title: "Bewässerungsanlage V2 - Reedkontakt"
date: "2011-02-15T21:14:52"
picture: "bwv06.jpg"
weight: "6"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29996
- /details51f5.html
imported:
- "2019"
_4images_image_id: "29996"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29996 -->
Hier sieht man den Magneten, der am Arm befestigt ist, und der den Reedkontakt auslöst.