---
layout: "image"
title: "Schlauchpumpe"
date: "2010-01-17T18:10:07"
picture: "Schlauchpumpe-Kompakt.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26112
- /details8c69-2.html
imported:
- "2019"
_4images_image_id: "26112"
_4images_cat_id: "2155"
_4images_user_id: "22"
_4images_image_date: "2010-01-17T18:10:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26112 -->
Schlauchpumpe -Kompakt

Inspiration:  

- Schlauchpumpe Clubheft 73-2
- Schlauchpumpe ins Krankenhaus meiner Schwiegermutter

Grüss,

Peter Poederoyen NL