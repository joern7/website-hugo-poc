---
layout: "comment"
hidden: true
title: "20809"
date: "2015-07-02T15:42:26"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Na, ich hoffe doch und nehme auch mal an, dass der Kompressor nur Druckluft in die Flasche pumpt und damit das Wasser von ganz alleine rausgedrückt wird. Der Kompressor hat doch auch gar keinen Ansaugstutzen, an den man einen Schlauch anschließen könnte.

Gruß,
Stefan