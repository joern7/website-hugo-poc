---
layout: "image"
title: "Gießmaschine"
date: "2015-07-01T18:05:08"
picture: "DSC_3256.jpg"
weight: "1"
konstrukteure: 
- "Bauteil"
fotografen:
- "Bauteil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bauteil"
license: "unknown"
legacy_id:
- /php/details/41368
- /details1f4c.html
imported:
- "2019"
_4images_image_id: "41368"
_4images_cat_id: "3091"
_4images_user_id: "2452"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41368 -->
Hier ist die Gießmaschine zu sehen.