---
layout: "image"
title: "Bewässerungsanlage- Interface"
date: "2011-01-01T17:41:04"
picture: "ssfsf3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29584
- /details703c.html
imported:
- "2019"
_4images_image_id: "29584"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29584 -->
Das hier ist das Robo Interface. Das durch eine Klappe gegen Spritzwasser geschützt ist. Das Interface ist bei den M- Ausgängen und I-Eingängen komplett belegt.