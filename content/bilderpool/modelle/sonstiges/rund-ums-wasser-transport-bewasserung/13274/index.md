---
layout: "image"
title: "Water wheel with turbine"
date: "2008-01-04T22:24:26"
picture: "IMG_6407.jpg"
weight: "4"
konstrukteure: 
- "Hopkins"
fotografen:
- "Hopkins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tomas Drbal"
license: "unknown"
legacy_id:
- /php/details/13274
- /details3550.html
imported:
- "2019"
_4images_image_id: "13274"
_4images_cat_id: "2155"
_4images_user_id: "523"
_4images_image_date: "2008-01-04T22:24:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13274 -->
