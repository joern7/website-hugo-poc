---
layout: "comment"
hidden: true
title: "6476"
date: "2008-05-17T10:37:39"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Nice. I'd love to see Mickey and Goofy as well. As for the yellow blocks, Ludger has uploaded some great images:

http://www.ftcommunity.de/details.php?image_id=13434
http://www.ftcommunity.de/details.php?image_id=13386
http://www.ftcommunity.de/details.php?image_id=13390