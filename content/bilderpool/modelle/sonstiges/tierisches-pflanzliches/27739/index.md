---
layout: "image"
title: "Wolf"
date: "2010-07-13T15:40:02"
picture: "sm_ft-wolf.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Wolf", "instructables", "gift", "exchange"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27739
- /details1409.html
imported:
- "2019"
_4images_image_id: "27739"
_4images_cat_id: "1236"
_4images_user_id: "585"
_4images_image_date: "2010-07-13T15:40:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27739 -->
Alternative model in my Scorpion kit for the Instructables Gift Exchange