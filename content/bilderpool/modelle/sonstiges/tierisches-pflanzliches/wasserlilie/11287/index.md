---
layout: "image"
title: "FT-Waterlelie   Poederoyen NL"
date: "2007-08-03T16:12:02"
picture: "Efteling-2007_085.jpg"
weight: "23"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11287
- /details0d46-2.html
imported:
- "2019"
_4images_image_id: "11287"
_4images_cat_id: "1028"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11287 -->
