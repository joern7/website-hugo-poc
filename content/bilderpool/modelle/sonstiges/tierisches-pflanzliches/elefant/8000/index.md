---
layout: "image"
title: "Elefant mit Rüssel unten"
date: "2006-12-20T19:22:05"
picture: "Elefant_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8000
- /details4826.html
imported:
- "2019"
_4images_image_id: "8000"
_4images_cat_id: "746"
_4images_user_id: "328"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8000 -->
Ich habe mich mal an ein für FT-Verhältnisse eher außergewöhnliches Thema gewagt und den Bau eines Elefanten versucht. Ich denke, es ist ganz gut gelungen... ;o)

Damit es doch noch ein wenig (Fischer-) Technik bleibt, habe ich einen kleinen Clou integriert: Dreht man am Schwanz, hebt und senkt sich der Rüssel! Niedlich, oder?