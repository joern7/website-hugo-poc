---
layout: "image"
title: "8"
date: "2010-07-09T08:53:32"
picture: "solarventilator08.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27731
- /detailsc8d2.html
imported:
- "2019"
_4images_image_id: "27731"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27731 -->
Der Motor hat eine 1 mm Welle.