---
layout: "image"
title: "2"
date: "2010-07-09T08:53:31"
picture: "solarventilator02.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27725
- /details1708.html
imported:
- "2019"
_4images_image_id: "27725"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27725 -->
Noch einmal die Solarzellen, diesmal von innen