---
layout: "image"
title: "5"
date: "2010-07-09T08:53:32"
picture: "solarventilator05.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27728
- /detailsa03d-2.html
imported:
- "2019"
_4images_image_id: "27728"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27728 -->
Man kann den Ventilator auch von der Halterung nehmen, deshalb der Griff zum anfassen.