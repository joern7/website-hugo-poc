---
layout: "image"
title: "Orginal bild aus dem Clubheft 1974-2"
date: "2016-07-14T18:10:27"
picture: "image_27.jpeg"
weight: "8"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "fischertechnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredspies"
license: "unknown"
legacy_id:
- /php/details/43905
- /details3f8f.html
imported:
- "2019"
_4images_image_id: "43905"
_4images_cat_id: "3253"
_4images_user_id: "2620"
_4images_image_date: "2016-07-14T18:10:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43905 -->
