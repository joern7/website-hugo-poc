---
layout: "image"
title: "Taschentuchspender"
date: "2015-12-27T10:20:56"
picture: "100_0344.jpg"
weight: "2"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/42568
- /detailsb475.html
imported:
- "2019"
_4images_image_id: "42568"
_4images_cat_id: "1617"
_4images_user_id: "2496"
_4images_image_date: "2015-12-27T10:20:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42568 -->
