---
layout: "image"
title: "Schrift"
date: "2012-02-23T21:35:27"
picture: "kopiereri04.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34382
- /detailsfe53.html
imported:
- "2019"
_4images_image_id: "34382"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2012-02-23T21:35:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34382 -->
