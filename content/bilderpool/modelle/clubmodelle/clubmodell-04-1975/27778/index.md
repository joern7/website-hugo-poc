---
layout: "image"
title: "Clubmodell"
date: "2010-07-27T22:47:53"
picture: "clubmodell1.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/27778
- /detailsf7e7.html
imported:
- "2019"
_4images_image_id: "27778"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2010-07-27T22:47:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27778 -->
Nachbau eines Clubmodells http://www.fischertechnik-museum.ch/doc/FanClub/Club_25_1975.pdf
Ab Seite 7