---
layout: "image"
title: "Stift"
date: "2012-02-23T21:35:27"
picture: "kopiereri02.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34380
- /details2296.html
imported:
- "2019"
_4images_image_id: "34380"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2012-02-23T21:35:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34380 -->
