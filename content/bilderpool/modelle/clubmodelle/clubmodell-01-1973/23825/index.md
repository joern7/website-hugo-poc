---
layout: "image"
title: "Elektronisch gesteuerte Uhr"
date: "2009-04-29T17:24:19"
picture: "clubmodell3.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23825
- /details9741.html
imported:
- "2019"
_4images_image_id: "23825"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23825 -->
Ansicht von hinten.