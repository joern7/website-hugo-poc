---
layout: "comment"
hidden: true
title: "9099"
date: "2009-04-29T20:43:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Gratulation zu dieser Kategorie! Ich bin dafür, dass wir hier mal nach und nach *alle* Modelle aus den Rubriken "Aktuelles zum Nachbauen" und "Neues vom fischertechnik-Club" der alten Clubhefte einstellen. Wäre doch eine schöne Sammlung und demonstriert, wie lange ft schon tolle Modelle ermöglicht - und es regt garantiert die Phantasie an!

Gruß,
Stefan