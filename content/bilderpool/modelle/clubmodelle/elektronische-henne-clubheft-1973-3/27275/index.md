---
layout: "image"
title: "Seitenansicht"
date: "2010-05-16T21:24:01"
picture: "elektronischehenne06.jpg"
weight: "6"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27275
- /details1d14.html
imported:
- "2019"
_4images_image_id: "27275"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27275 -->
fischergeometric kannte rechteckige, dreieckige und runde Teile, die man jeweils mit den roten Verbinderlaschen zusammenstecken konnte. Später kamen noch Kegel und Kegelstümpfe hinzu, die bei dieser Henne aber keine Verwendung finden.