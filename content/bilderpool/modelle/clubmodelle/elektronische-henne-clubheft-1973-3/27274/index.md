---
layout: "image"
title: "Fotozelle"
date: "2010-05-16T21:24:01"
picture: "elektronischehenne05.jpg"
weight: "5"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27274
- /details0c1b.html
imported:
- "2019"
_4images_image_id: "27274"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27274 -->
Diese Fotozelle registriert, wenn jemand der Henne über den Rücken streichelt. Und dann gackert sie eben freundlich zurück!