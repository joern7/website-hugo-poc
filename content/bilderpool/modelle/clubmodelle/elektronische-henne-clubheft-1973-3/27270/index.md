---
layout: "image"
title: "Clubheft 1973-3 Seiten 20 und 21"
date: "2010-05-16T21:23:59"
picture: "elektronischehenne01.jpg"
weight: "1"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27270
- /details1f08.html
imported:
- "2019"
_4images_image_id: "27270"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:23:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27270 -->
Das Clubheft kann man an verschiedenen Stellen im Web als PDF herunterladen. So sah das damals aus. Die Elektronik, deren Schaltung hier dargestellt ist, gackert etwa wie Gack-Gack-Gack-Gack-Gaaaaack, mit einem höheren, längeren letzten Ton.