---
layout: "image"
title: "Clubheft 1973-3 Seiten 22"
date: "2010-05-16T21:24:00"
picture: "elektronischehenne02.jpg"
weight: "2"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27271
- /detailsd579.html
imported:
- "2019"
_4images_image_id: "27271"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27271 -->
Diese Seite ist um 90° gedreht, sie war im Querformat bedruckt.