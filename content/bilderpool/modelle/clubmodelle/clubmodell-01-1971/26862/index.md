---
layout: "image"
title: "Clubmodell"
date: "2010-04-02T23:27:45"
picture: "clubmodell2.jpg"
weight: "2"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26862
- /detailsd7a1-2.html
imported:
- "2019"
_4images_image_id: "26862"
_4images_cat_id: "1923"
_4images_user_id: "453"
_4images_image_date: "2010-04-02T23:27:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26862 -->
