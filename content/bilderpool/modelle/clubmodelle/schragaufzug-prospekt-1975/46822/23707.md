---
layout: "comment"
hidden: true
title: "23707"
date: "2017-10-19T11:13:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nachtrag: Und auch die 10-mm-Winkelstücke und die Federnocken, mit denen ich Lampe und Fotowiderstand fixiert hatte, gab es wohl noch nicht. In den äußeren Bereichen der 90x180-Grundplatte halten aber bei mir nur ganz selten mal Bausteine fest, sodass mir das für einen Convention-Dauerlauf nötig erschien.

Gruß,
Stefan