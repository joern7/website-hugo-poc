---
layout: "image"
title: "Ballroboter (c)"
date: "2008-02-09T12:07:19"
picture: "Ballroboter_b.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13607
- /details65cf.html
imported:
- "2019"
_4images_image_id: "13607"
_4images_cat_id: "1250"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T12:07:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13607 -->
Der Roboter von vorne