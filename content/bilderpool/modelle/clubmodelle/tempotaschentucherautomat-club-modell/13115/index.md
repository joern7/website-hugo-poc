---
layout: "image"
title: "Gesamtansicht frontal"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus6.jpg"
weight: "6"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13115
- /detailsf01b.html
imported:
- "2019"
_4images_image_id: "13115"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13115 -->
Nochmals eine Gesamtansicht.