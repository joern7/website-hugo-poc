---
layout: "image"
title: "Ausgabe leer"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus5.jpg"
weight: "5"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13114
- /detailsb4cf-2.html
imported:
- "2019"
_4images_image_id: "13114"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13114 -->
Hier fällt die Packung drauf