---
layout: "image"
title: "Münzeinwurf hinten"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus7.jpg"
weight: "7"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13116
- /details9cb0.html
imported:
- "2019"
_4images_image_id: "13116"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13116 -->
Deutlich zu erkennen, die LIchtschranke. Übrigens auch eine LED.