---
layout: "image"
title: "Antrieb"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus2.jpg"
weight: "2"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13111
- /details0de6.html
imported:
- "2019"
_4images_image_id: "13111"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13111 -->
Beim Antrieb musste ich improvisieren, da meine beiden Hubgetrieb kaputt sind. Klappt aber mit dem Schneckeantrib genauso gut.