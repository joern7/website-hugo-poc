---
layout: "image"
title: "Der Nachbau"
date: 2020-05-09T17:28:16+02:00
picture: "Spielautomat von 1975 2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Oben passen 5-Cent-Münzen (Pfennige gibt's ja nicht mehr) in den Schlitz. Wenn man eine hineinwirft, unterbricht das eine Lichtschranke darunter. Die Münze fällt in einen Vorratsschacht. Die beiden Scheiben drehen sich eine bestimmte Zeit lang. Wenn beim Stopp "weiß" nach oben zeigt, hat man gewonnen und bekommt in den Behälter vorne zwei Münzen aus dem Vorrat herausgeschoben - man halt also gegenüber dem Spieleinsatz eine Münze Gewinn gemacht.