---
layout: "image"
title: "Das Original"
date: 2020-05-09T17:28:17+02:00
picture: "Spielautomat von 1975 1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Im Clubheft 1975-2 fand sich dieser Glücksspielautomat. Ich weiß nicht, wer damals zehn Silberlinge besaß, aber ich jedenfalls nicht. Deshalb war es immer ein Traum, dieses Modell endlich mal nachzubauen. Na, irgendwann war's dann halt so weit, dass das einfach "dran" war.