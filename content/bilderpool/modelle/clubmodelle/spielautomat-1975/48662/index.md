---
layout: "image"
title: "Die Elektronik"
date: 2020-05-09T17:28:14+02:00
picture: "Spielautomat von 1975 3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Schaltung funktioniert so:

1. Alles steht still. Man wirft durch den Schlitz an der Oberseite eine Münze ein. Die fällt durch eine Lichtschranke.

2. Dieses Signal triggert ein Mono-Flop, das über ein Relais die beiden Motoren der Glücksräder in Bewegung setzt.

3. Sobald das Mono-Flop wieder abfällt, entscheiden die beiden Taster hinter den Glücksrädern. Nur wenn beide gleichzeitig gedrückt sind, wird das Flip-Flop („der Spieler hat gewonnen“) gesetzt (ansonsten bleibt die Maschine still – man hatte seinen Einsatz verloren).

4. Das Gewinn-Signal wiederum setzt – über Flip-Flops als Zähler geschaltet – den Auswurfmotor zweimal (!) jeweils vor und dann wieder zurück bis zu den jeweiligen Endlagen-Tastern in Bewegung. Es werden so zwei Münzen aus dem Schacht ausgeworfen – eine  mehr als der Einsatz war. Während des Auswurfs der beiden Münzen leuchtet die grüne Gewinn-Lampe, die an der Frontseite des Automaten eingelassen ist.

5. Danach steht die Maschine wieder still und wartet auf die nächste eingeworfene Münze.