---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:15"
picture: "clubmodell2.jpg"
weight: "2"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23831
- /details7e63.html
imported:
- "2019"
_4images_image_id: "23831"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23831 -->
Antrieb und Elektronikbausteine.