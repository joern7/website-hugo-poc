---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:16"
picture: "clubmodell6.jpg"
weight: "6"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23835
- /detailsa5d6.html
imported:
- "2019"
_4images_image_id: "23835"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23835 -->
Rückansicht. Unsere kleine Maus im Hintergrund wird langsam auch ein ft-Fan :-)