---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:15"
picture: "clubmodell1.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23830
- /details6944.html
imported:
- "2019"
_4images_image_id: "23830"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23830 -->
Komplettansicht von vorne. Links auf dem Tisch sieht man schon einen Teil vom Clubmodell 02/1977. Elektronische Uhr die zweite.... Bilder kommen bald, warte nur noch auf den Flip-Flop Baustein.