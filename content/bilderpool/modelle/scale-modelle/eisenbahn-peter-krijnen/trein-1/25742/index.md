---
layout: "image"
title: "Unterseite der nierderbordwagen"
date: "2009-11-08T19:43:55"
picture: "trein14.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25742
- /detailsae0f.html
imported:
- "2019"
_4images_image_id: "25742"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25742 -->
