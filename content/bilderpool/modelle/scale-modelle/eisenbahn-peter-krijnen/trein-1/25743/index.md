---
layout: "image"
title: "Eine der beide Drehgestelle"
date: "2009-11-08T19:43:55"
picture: "trein15.jpg"
weight: "15"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25743
- /detailsf01d.html
imported:
- "2019"
_4images_image_id: "25743"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25743 -->
