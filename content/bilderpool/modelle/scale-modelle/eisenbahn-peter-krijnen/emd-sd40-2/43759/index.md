---
layout: "image"
title: "EMD SD40-2. 20"
date: "2016-06-12T19:48:23"
picture: "emdsd20.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43759
- /details157b.html
imported:
- "2019"
_4images_image_id: "43759"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43759 -->
Drehgestel von oben:
Auf der Platine 4 Kontaktflächen: Links für die 3 Motoren. Recht für die Gleiskontakten.