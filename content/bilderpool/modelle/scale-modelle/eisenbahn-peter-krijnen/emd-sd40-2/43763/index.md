---
layout: "image"
title: "EMD SD40-2. 24"
date: "2016-06-12T19:48:23"
picture: "emdsd24.jpg"
weight: "24"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43763
- /details11dc.html
imported:
- "2019"
_4images_image_id: "43763"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43763 -->
Hinterseite einer der 6 eingebauten LED-Platinen: 4x 5mm Led Weis, und 2x 5mm LED Rot. Jeder mit ein SMD Widerstand.
Platine mit ein m2 bolzen in der drehmachine eingespant und bis etwa 13,3mm abgedreht. Da die LED's auch nach hinten licht ausstrahlen komt doch recht viel licht durch das loch heraus. Da mus ich mir mal gedanken machen wie ich da einen abblendung machen kan.