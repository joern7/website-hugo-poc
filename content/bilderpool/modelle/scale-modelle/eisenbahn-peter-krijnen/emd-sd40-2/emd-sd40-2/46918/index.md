---
layout: "image"
title: "EMD SD40-2. 62"
date: "2017-11-08T17:20:12"
picture: "emdsd09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46918
- /detailsc4bb-3.html
imported:
- "2019"
_4images_image_id: "46918"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46918 -->
Gegenüber Bild 7 ist hier zu erkennen das das teil unterhalb der Kabine auch um 15mm erhöht ist. Auch zu erkennen ist das der Lufterausbau (vom Turbo) an der Linke Seite breiter geworden ist