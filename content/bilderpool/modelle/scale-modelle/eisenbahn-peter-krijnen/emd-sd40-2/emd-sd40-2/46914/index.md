---
layout: "image"
title: "EMD SD40-2. 58"
date: "2017-11-08T17:20:12"
picture: "emdsd05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46914
- /details3a13-2.html
imported:
- "2019"
_4images_image_id: "46914"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46914 -->
Die höhe der Motorhaube ist auch um 15 höher geworden.