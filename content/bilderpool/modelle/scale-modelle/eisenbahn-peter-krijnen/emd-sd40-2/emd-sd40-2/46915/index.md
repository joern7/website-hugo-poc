---
layout: "image"
title: "EMD SD40-2. 59"
date: "2017-11-08T17:20:12"
picture: "emdsd06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46915
- /details3291.html
imported:
- "2019"
_4images_image_id: "46915"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46915 -->
Auch an der hinter Front sind die Geländer geänderd.