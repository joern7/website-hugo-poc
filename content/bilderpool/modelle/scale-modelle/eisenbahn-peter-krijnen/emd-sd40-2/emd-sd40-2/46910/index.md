---
layout: "image"
title: "EMD SD40-2. 54"
date: "2017-11-08T17:20:12"
picture: "emdsd01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46910
- /details1c32.html
imported:
- "2019"
_4images_image_id: "46910"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46910 -->
Ich war doch noch nicht gans zu frieden. Nachdem ich im Internet entlich ein Bild fand das von eine Brücke genommen war, hab ich mein Modell gleich geänderd.
Die 5 Lufteraufbauten sind jetzt Rot. Das 3-Ton Horn ist nach hinter versetzt worden. Weiter ist die Länge der Lok um 60mm verlängerd und 15mm erhöht. Und die beide Klauenkupplungen sind auch etwas nach ausen versetzt. Das dach der Kurze Nase ist jetzt Schwarz.

Noch ein par Daten: total sind 7273 teilen in der Lok verbaut worden, die sich auf 187 Artikeln verteilen und bringen die kosten auf  4145 Euro.