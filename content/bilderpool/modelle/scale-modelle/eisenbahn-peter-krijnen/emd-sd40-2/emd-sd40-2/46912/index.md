---
layout: "image"
title: "EMD SD40-2. 56"
date: "2017-11-08T17:20:12"
picture: "emdsd03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46912
- /detailscbfc.html
imported:
- "2019"
_4images_image_id: "46912"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46912 -->
Der Oberbau der Lok ist auch 15mm höher geworden.
Die Drehgestelen sind beide um 2x5 mm länger geworden.