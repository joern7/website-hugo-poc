---
layout: "overview"
title: "EMD SD40-2"
date: 2020-02-22T08:21:20+01:00
legacy_id:
- /php/categories/3239
- /categoriesda06.html
- /categoriesdb99.html
- /categories453b-2.html
- /categories3c04.html
- /categories4518.html
- /categories9b76.html
- /categories3935.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3239 --> 
Werend der "Modelshow Europe 2015" habbe ich ein Amerikanische Diesellokomotieve gesehen.
Der bauer hat das Modell aus Lego aufgebaut.
Später hab ich beim Googlen heraus gefunden das das ein in Maßstab 1:16 gebauten EMD SD40-2 ist.
Die wolte ich auch bauen, aber in erster linie in Maßstab 1:22. Auf LGB geleisen. Das bedeutete aber, Normalspurlokomotieve auf Schmalspurgleisen. Das machen LGB und PIKO auch.
Am anfang habbe ich mir erst der LGB Katalog durch geblätterd. Die haben schon ein Amerikaner im program. Ich habbe aber später heraus gefunden das das ein ALCO ist und nicht ein EMD.

Googled man "EMD SD40" bekomt man eine ganse menge links und foto's.

Da gibt es auch einige personen die Zeichmungen machen.
Auf Trainiax.net findet man eine menge Zeichnungen in den Maßstäbe 1:55, 1:36 und 1:18.
Am ende habbe ich die 1:18 Zeichnungen von eine BN version herunter geladen.
Leider gib es keine abmessungen. Auf die site ist aber zu lesen: 1 pixel = 1/4 Soll.

Da hab ich die Zeichnungen in "Paint" geladen, pixel gezählt, multiplisiert mit 4 und mit 25,4. Ergebnis? 21031mm länge, was in maßstab 1:16 1314,4mm ist.
Leider fand ich erst beim aufmessn heraus das mein Modell um etwa 100mm kurzer ist. Das bedeutet das mein Modell eher ein Maßstab von 1:17 hat.

Länge:          1237mm
Breite:             183mm
Höhe:              285mm
Spurweite:       88mm

Als räder verwende ich #31019 Drehscheibe, mit zur antrieb #36264 Zahnrad-Z30, an der innenseite angebaut.
Da der SD40-2 ein 6 Achser ist habbe ich 6  #151178 Tracktormotor, eingebaut die alle parallel geschaltet sind. Mit #35945 Rastritzel-Z10, übertragen die Motoren ihre Kraft auf die Z30.

Ich bezitse schon seit 2 jahr ein digital steuercentrale von ESU: der Ecos 5000. Ihr verstäht schon: da mus einfach ein decoder rein. Natürlich auch ein von ESU: der Loksound V4.0 XL. Das tolle ist nämlich das man bei ESU auch ein Soundfile von der SD40 zur download anbieted, Das hab ich aber mein Händler machen lassen.
Aus 2 Actiflautsprecher habbe ich die beide Mitteltöner ausgebaut, und in mein Modell eingebaut.

Für alle Scheinwerfer wolte ich wegen der digital steuerung nur LED's verwenden. Nur zur Kabinebeleuchtung benutse ich noch 2 12Volt Birnen.

Das Modell ist in teilen afgebaut. Das bedeutet das ich auf einige plätsen im Modell Federkontakten benutsen mus.
Bis auf die LED's für die Nummerkasten ist das Model fertig. Bei einige foto's ist zu lesen das ich noch einiges zu tun hab, aber fahren kan er schon auf 7m gleis.
Nachdem ich ein Software update auf der Ecos geladen hatte, kan ich jetst ale functionen programieren und schalten die eingebaut sind. TOLL, der macht richtig lärm und viel Spass.
Nur noch bei Sonneschein viele scharfe foto's machen.