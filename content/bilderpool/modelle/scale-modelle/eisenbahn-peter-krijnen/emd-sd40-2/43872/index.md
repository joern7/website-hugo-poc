---
layout: "image"
title: "EMD SD40-2. 33  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd3.jpg"
weight: "33"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43872
- /details9fbd.html
imported:
- "2019"
_4images_image_id: "43872"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43872 -->
Seht man sich Bild 7 mal an, seht man ein nach rechts immer weiteren Spalt unterhalb der Kabine.
Unterhalb der Schwarzen Grundplatte #35129, hab ich die Rote Bauplatten entfernt und durch Schwartzen Bauplatten ersetzt.
Die sind dan aber eine etage tiefer eingebaut, damit der Spalt über die ganse breite gleich ist.
Vorteil ist damit das das Geländer auch gleich in die Horizontale gekommen ist.