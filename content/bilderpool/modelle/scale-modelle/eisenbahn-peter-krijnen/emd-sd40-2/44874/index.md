---
layout: "image"
title: "EMD SD40-2. 50  CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd11_2.jpg"
weight: "50"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44874
- /details8ce7.html
imported:
- "2019"
_4images_image_id: "44874"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44874 -->
Am Computer in Paint gezeichnet, als Tekstform in Excel geladen und auf 14,5x29,5mm scaliert.