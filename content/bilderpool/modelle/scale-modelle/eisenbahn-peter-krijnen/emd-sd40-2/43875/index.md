---
layout: "image"
title: "EMD SD40-2. 36  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd6.jpg"
weight: "36"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43875
- /detailse03f.html
imported:
- "2019"
_4images_image_id: "43875"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43875 -->
An beide enden der Drehgestellen sind Sanfalrohren, zodas bei glätte immer beim vorwärts fahren gesandet werden kan.