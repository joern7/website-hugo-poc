---
layout: "image"
title: "EMD SD40-2. 39  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd9.jpg"
weight: "39"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43878
- /details7f5c.html
imported:
- "2019"
_4images_image_id: "43878"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43878 -->
Rückseite:
Da in Amerika und Canada nur mit der Kabine vorraus gefahren werd, brauchen die am ende keine Ditchlights.
Da immer mit bis zur 6 Loks hinter einander gefahren werd, brauchen die auch keine Bahnraumer und Nummerkasten an der Rückseite.
Der letste Lok in die reihe ist umgedreht worden, zodas an jede ende ein Kabine ist.