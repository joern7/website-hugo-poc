---
layout: "image"
title: "EMD SD40-2. 52 CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd13_2.jpg"
weight: "52"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44876
- /detailse4d3-2.html
imported:
- "2019"
_4images_image_id: "44876"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44876 -->
Bei einbruch der Nacht könte man die leuchten der Treppen und der Drehgestellen einschalten.
