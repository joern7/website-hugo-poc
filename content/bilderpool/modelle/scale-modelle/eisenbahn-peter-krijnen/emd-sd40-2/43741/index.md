---
layout: "image"
title: "EMD SD40-2. 2"
date: "2016-06-12T19:48:23"
picture: "emdsd02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43741
- /details3366.html
imported:
- "2019"
_4images_image_id: "43741"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43741 -->
Laut "Community Publisher", fehlte hier noch ein Beschreibung!
Logo und/oder name von der Betreiber?
Logo: ein Fisch mit 2x "R" drin
Name: Fischer Rail Road (company)