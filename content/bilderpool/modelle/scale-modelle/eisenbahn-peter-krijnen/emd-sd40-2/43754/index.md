---
layout: "image"
title: "EMD SD40-2. 15"
date: "2016-06-12T19:48:23"
picture: "emdsd15.jpg"
weight: "15"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43754
- /details2c79.html
imported:
- "2019"
_4images_image_id: "43754"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43754 -->
Das ist unter der lange Motorhaube zu finden.