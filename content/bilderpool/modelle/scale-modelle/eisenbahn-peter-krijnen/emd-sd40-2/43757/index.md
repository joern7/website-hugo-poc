---
layout: "image"
title: "EMD SD40-2. 18"
date: "2016-06-12T19:48:23"
picture: "emdsd18.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43757
- /details11a6.html
imported:
- "2019"
_4images_image_id: "43757"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43757 -->
Unterseit er Kabine.
4x 31306 Federkontakt:
1x Plus
1x Frontscheinwerfer
1x Nummerkasten
1x Innenbeleuchtung