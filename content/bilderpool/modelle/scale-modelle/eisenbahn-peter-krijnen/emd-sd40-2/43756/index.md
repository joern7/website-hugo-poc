---
layout: "image"
title: "EMD SD40-2. 17"
date: "2016-06-12T19:48:23"
picture: "emdsd17.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43756
- /detailsa6c2.html
imported:
- "2019"
_4images_image_id: "43756"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43756 -->
Das ist unter der Kabine und der kurzen Vorbau zu finden