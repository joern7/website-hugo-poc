---
layout: "image"
title: "EMD SD40-2. 19"
date: "2016-06-12T19:48:23"
picture: "emdsd19.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43758
- /details3a95.html
imported:
- "2019"
_4images_image_id: "43758"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43758 -->
Auch im inneren ist der Kabine ausgestattet. hier leider nicht zu erkennen.
Drin sind 3 sitzen, Bedienungsconsole und Armaturenbret.
http://www.rrpicturearchives.net/showPicture.aspx?id=370706