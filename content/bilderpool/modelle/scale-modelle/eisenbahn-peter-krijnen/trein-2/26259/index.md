---
layout: "image"
title: "Trein 2: ....und wieder der kleine 'MATRA'."
date: "2010-02-10T15:59:13"
picture: "trein09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26259
- /detailsf5bb.html
imported:
- "2019"
_4images_image_id: "26259"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26259 -->
Zur vorbild stand das LGB Modell #40420