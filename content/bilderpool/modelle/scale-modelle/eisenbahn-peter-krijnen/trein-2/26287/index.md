---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 9"
date: "2010-02-10T15:59:15"
picture: "trein37.jpg"
weight: "37"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26287
- /detailse22a.html
imported:
- "2019"
_4images_image_id: "26287"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26287 -->
...den die achse ist in ein Drehgestell LGB #67302 gelagert.