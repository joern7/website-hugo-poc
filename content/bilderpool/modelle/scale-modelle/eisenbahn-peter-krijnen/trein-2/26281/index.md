---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 3"
date: "2010-02-10T15:59:14"
picture: "trein31.jpg"
weight: "31"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26281
- /detailseb18-3.html
imported:
- "2019"
_4images_image_id: "26281"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26281 -->
