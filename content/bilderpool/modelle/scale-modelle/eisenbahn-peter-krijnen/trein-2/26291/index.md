---
layout: "image"
title: "Trein 2: der gegengewichtswagen 3"
date: "2010-02-10T15:59:15"
picture: "trein41.jpg"
weight: "41"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26291
- /details15a6-2.html
imported:
- "2019"
_4images_image_id: "26291"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26291 -->
