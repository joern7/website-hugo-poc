---
layout: "image"
title: "Trein 2: Lok, lagerung der Achsen"
date: "2010-02-10T15:59:14"
picture: "trein19.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26269
- /details74e5.html
imported:
- "2019"
_4images_image_id: "26269"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26269 -->
...und werd von ein Abstandsring #31597 auf etwas erhöht, damit der nicht in der Riegelstein untertaucht.
Wurde man der Riegelstein auf 5mm höhe reducieren, braucht man den ring nicht.