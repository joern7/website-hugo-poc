---
layout: "image"
title: "Trein 2: ...Zwischenwagen..."
date: "2010-02-10T15:59:13"
picture: "trein06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26256
- /details0e48.html
imported:
- "2019"
_4images_image_id: "26256"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26256 -->
Zur vorbild stand das LGB Modell #41100