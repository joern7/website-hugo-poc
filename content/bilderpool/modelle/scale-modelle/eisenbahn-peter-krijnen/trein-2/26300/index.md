---
layout: "image"
title: "Trein 2: Großen Kran 1"
date: "2010-02-10T15:59:15"
picture: "trein50.jpg"
weight: "50"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26300
- /details4fff.html
imported:
- "2019"
_4images_image_id: "26300"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26300 -->
Vor Jahren hab ich in ein LGB Depesche, mal ein modell gesehen von ein Kran der Rhatischen Bahn.
Mein nachbau ist aber ein sehr freie nachamung.
Leider hab ich damals zu wenig foto's gemacht.