---
layout: "image"
title: "Trein 2: Ausleger-auflagewagen 3"
date: "2010-02-10T15:59:15"
picture: "trein46.jpg"
weight: "46"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26296
- /details38d3-2.html
imported:
- "2019"
_4images_image_id: "26296"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26296 -->
Der dreh und neichbare Drehschemmel zur auflage der Ausleger.