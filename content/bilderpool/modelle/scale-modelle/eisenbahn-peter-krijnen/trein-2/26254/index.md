---
layout: "image"
title: "Trein 2: Diesellok der Baureihe 251..."
date: "2010-02-10T15:59:12"
picture: "trein04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26254
- /details9124.html
imported:
- "2019"
_4images_image_id: "26254"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26254 -->
Zur vorbild stand das LGB Modell #21510