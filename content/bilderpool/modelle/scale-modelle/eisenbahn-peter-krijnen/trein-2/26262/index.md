---
layout: "image"
title: "Trein 2: Lok, Drehgestell"
date: "2010-02-10T15:59:13"
picture: "trein12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26262
- /details2aa1.html
imported:
- "2019"
_4images_image_id: "26262"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26262 -->
Gelenkkurbel #35088 ist als Bremscylinder benutst.
Als Hebel werd ein gekurzten Kurbel 60/40 genutst.