---
layout: "image"
title: "Trein 2: Ausleger-auflagewagen 4"
date: "2010-02-10T15:59:15"
picture: "trein47.jpg"
weight: "47"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26297
- /details1167.html
imported:
- "2019"
_4images_image_id: "26297"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26297 -->
