---
layout: "image"
title: "Trein 2: doppelachsdrehgestelle"
date: "2010-02-10T15:59:15"
picture: "trein43.jpg"
weight: "43"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26293
- /detailse17e.html
imported:
- "2019"
_4images_image_id: "26293"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26293 -->
Ein sats LGB #67402.
Auch hier werd der Clipsachse #32870 zur motage benutst.