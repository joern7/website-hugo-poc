---
layout: "image"
title: "Trein 2: Großen Kran 2"
date: "2010-02-10T15:59:15"
picture: "trein51.jpg"
weight: "51"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26301
- /details1b5e.html
imported:
- "2019"
_4images_image_id: "26301"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26301 -->
Hier das modell mit angebauten Gegengewicht.
Unterhalb der beide Seilwinden sind zwei S-motoren...