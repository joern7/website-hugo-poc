---
layout: "image"
title: "Trein 2: Großen Kran 4"
date: "2010-02-10T15:59:15"
picture: "trein53.jpg"
weight: "53"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26303
- /details4837.html
imported:
- "2019"
_4images_image_id: "26303"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26303 -->
Zur heben der Ausleger: 6 Seilrollen 12 in vertikaler lage.