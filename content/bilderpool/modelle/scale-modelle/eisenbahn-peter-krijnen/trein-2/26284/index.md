---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 6"
date: "2010-02-10T15:59:14"
picture: "trein34.jpg"
weight: "34"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26284
- /detailsae6c-2.html
imported:
- "2019"
_4images_image_id: "26284"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26284 -->
Der rechter hand der Kranführer ist gleich der Bremse der Seilwinde.