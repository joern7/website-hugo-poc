---
layout: "image"
title: "Brücke absenken"
date: "2007-04-06T19:08:57"
picture: "10_Ausleger_absenken_-_Brcke_ablegen__10.jpg"
weight: "21"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10015
- /details68c2-2.html
imported:
- "2019"
_4images_image_id: "10015"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10015 -->
Ausleger absenken – Brücke ablegen:
Der Ausleger senkt sich nun langsam ab. In diesem Augenblick herrschen wohl die Größten Kräfte – auch bei mir am Modell.
Der Heckausleger fährt in seine Transportstellung zurück.