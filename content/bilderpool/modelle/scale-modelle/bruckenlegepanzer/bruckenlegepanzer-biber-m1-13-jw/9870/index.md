---
layout: "image"
title: "Fertigstellung Package autom.Steurung003"
date: "2007-03-31T18:08:14"
picture: "Fertigstellung_Package_autom.Steurung01.jpg"
weight: "9"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/9870
- /details7fd0.html
imported:
- "2019"
_4images_image_id: "9870"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-03-31T18:08:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9870 -->
Leider zwangen mich die hohen Kräft, bedingt durch den Hebelarm,zu einem Flaschenzug. Die Kräfte sind enorm. Verwendet habe ich Zahnseide. Sie ist dünn geschmeidig und kann große Zugkräfte aushalten. Um die auftretenden Kräfte überhaupt bewältigen zu können wird die Zahnseide direkt auf der 4 mm Welle aufgewickwelt. Zusehen sind auch die "Hörner" welche das obere Brückenmodul anhebt und absenkt.