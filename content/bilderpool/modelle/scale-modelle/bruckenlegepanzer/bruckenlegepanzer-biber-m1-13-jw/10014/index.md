---
layout: "image"
title: "Brücke ausfahren"
date: "2007-04-06T19:08:57"
picture: "09_Brcke_ausfahren_9.jpg"
weight: "20"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10014
- /detailsa2cd.html
imported:
- "2019"
_4images_image_id: "10014"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10014 -->
Brücke ausfahren:
Die Brücke wird nun ausgefahren.