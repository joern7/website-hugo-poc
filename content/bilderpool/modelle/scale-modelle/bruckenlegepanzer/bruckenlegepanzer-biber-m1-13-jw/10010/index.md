---
layout: "image"
title: "Hilfsarme anheben"
date: "2007-04-06T19:01:59"
picture: "05_Hilfsarme_anheben_5.jpg"
weight: "16"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10010
- /detailsb288.html
imported:
- "2019"
_4images_image_id: "10010"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10010 -->
Hilfsarme anheben:
Ist das vordere Brückenmodul an der späteren Verbolzposition angekommen, hebt der Hilfsarm das obere Brückenmodul etwas an. Anschließend wird der Heckausleger vollständig abgelassen. Das Brückenmodul beschreibt dabei eine Bahnkurve und befindet, fixiert durch die Heckausleger, in der Verbolzposition.