---
layout: "image"
title: "Heckausleger in 90 Grad"
date: "2007-04-06T19:01:53"
picture: "02_Heckausleger_in_90_Grad_2.jpg"
weight: "12"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10006
- /details4d31.html
imported:
- "2019"
_4images_image_id: "10006"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10006 -->
Heckausleger in 90 Grad:
Der Heckausleger wird in eine 90 Grad Stellung verdreht. Der Heckausleger, mit dem oberen Brückenmodul formschlüssig verbunden, hebt durch seine Kröpfung das obere Brückenmodul etwas an. Nun kann das untere Brückenmodul nach vorn gefahren werden.