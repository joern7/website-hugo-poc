---
layout: "image"
title: "Stützschild absenken"
date: "2007-04-06T19:01:53"
picture: "03_Sttzschild_absenken_3_1.jpg"
weight: "13"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10007
- /details6fa0.html
imported:
- "2019"
_4images_image_id: "10007"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10007 -->
Stützschild absenken:
Nun wird das Stützschild abgesenkt. Der Panzer kann sich nun sehr exakt ausrichten, da die Vorderkante des Stützschildes in etwa den Beginn der Brücke symbolisiert.