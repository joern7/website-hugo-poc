---
layout: "comment"
hidden: true
title: "2363"
date: "2007-02-14T17:39:48"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Juergen,

Schieben alle Brück-teilen wie beim PANZERSCHNELLBRÜCKE BIBER (BW):
http://www.panzerbaer.de/types/bw_pzschnbr_1_biber-a.htm

.....oder bewegen die Brücketeilen auch in eine verticale Position wie bei meinem 
Bridge Tank TM-5-5420-203-14 ?...
http://www.ftcommunity.de/details.php?image_id=5781


Gruss,

Peter D