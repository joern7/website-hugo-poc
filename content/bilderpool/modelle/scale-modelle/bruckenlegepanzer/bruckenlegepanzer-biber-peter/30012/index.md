---
layout: "image"
title: "Brückenlegepanzer-Biber van J. de Grave (Nederlandse Vereniging van Modelbouwers) De Meern"
date: "2011-02-18T14:12:42"
picture: "ftbrueckenlegepanzerbiber04.jpg"
weight: "6"
konstrukteure: 
- "J.de Grave"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30012
- /details6ba8.html
imported:
- "2019"
_4images_image_id: "30012"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30012 -->
Brückenlegepanzer-Biber