---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber27.jpg"
weight: "29"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30035
- /details9fba.html
imported:
- "2019"
_4images_image_id: "30035"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30035 -->
FT-Brückenlegepanzer-Biber unten mit Federung-details

Wie im Original:  7 Laufrollen je Seite, und per Drehstab gefedert.  Die Laufrollen haben schwarze Freilaufnaben. Die Radaufhängung und zugleich Federung besteht aus A2-RVS-Achsen die ich pro Achse 2x 90 Grad biegen muss.   
Die Drehstäbe reichen bis zur Gegenseite durch und deshalb sind wie beim Leo auch die Aufstandsflächen der Ketten (links/rechts) unterschiedlich angeordnet - die eine Seite liegt etwas weiter hinten auf als die andere. 

Die FT-Kette hat Kufen die zwischen den Rädern 60 hindurch läuft und damit die Kette in der Spur hält. 
