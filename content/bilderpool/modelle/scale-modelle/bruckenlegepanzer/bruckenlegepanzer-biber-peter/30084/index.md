---
layout: "image"
title: "Zahnstang-Alternativen geklebt an 0,5mm oder 1,0mm Aluminiumplatten"
date: "2011-02-19T12:56:17"
picture: "Zahnstange-Alternative-FTC.jpg"
weight: "49"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30084
- /details2a97.html
imported:
- "2019"
_4images_image_id: "30084"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-19T12:56:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30084 -->
Was denkt ihr von dieser Zahnstang-Alternativen, geklebt an 0,5mm oder 1,0mm Aluminiumplatten ?
 
Die 0,5mm oder 1,0mm Aluminiumplatten kann man biegen in die gewunschte U-Form für eine grosse Wiederstands- und Tragheidsmoment  (W und I) ohne viele Klebstoffe.
 
Gruss,
 
Peter
Poederoyen NL