---
layout: "image"
title: "Brückenpanzer Biber - 2/2"
date: "2011-01-04T21:46:43"
picture: "a2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/29607
- /details74eb.html
imported:
- "2019"
_4images_image_id: "29607"
_4images_cat_id: "2214"
_4images_user_id: "389"
_4images_image_date: "2011-01-04T21:46:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29607 -->
Hier ist das Detailbild vom Antrieb der Kette.
Gut zu erkennen sind die Kufen die perfekt in das Raster der 30er Achsen passen.