---
layout: "image"
title: "Brückenlegepanzer-Biber in Entwicklung"
date: "2011-03-13T19:56:35"
picture: "Brckenlegepanzer-Biber_002.jpg"
weight: "51"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30247
- /detailsf36d-3.html
imported:
- "2019"
_4images_image_id: "30247"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-03-13T19:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30247 -->
Gestern habe ich aus 1,0mm Aluminiumplatten die Brückenteilen gemacht.
Es gibt aber noch viel Arbeit zu tun.
2 Hubspindel-getriebe (Lemo-Solar) mit Alu-Rohr als Hydraulik-Zylinder habe ich vergangene Woche eingebaut.
 
Gruss,
 
Peter
Poederoyen NL