---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber30.jpg"
weight: "32"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30038
- /detailsfdc6.html
imported:
- "2019"
_4images_image_id: "30038"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30038 -->
FT-Brückenlegepanzer-Biber unten