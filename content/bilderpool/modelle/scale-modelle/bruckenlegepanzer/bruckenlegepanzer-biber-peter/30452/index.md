---
layout: "image"
title: "Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder"
date: "2011-04-14T21:26:00"
picture: "Brckenlegepanzer-details_015.jpg"
weight: "58"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30452
- /details7167-4.html
imported:
- "2019"
_4images_image_id: "30452"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-04-14T21:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30452 -->
Meine Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder -unten mit Kugellager.
Ich hoffe das Andreas T. es schaft eine schöne Löschung für die Brücken-Verbinder  aus Stahl oder Messing zu machen. Jeder Verbinder kleben und mit einer M4-Schraube + Kugellager befestigen an einer Brücketeil wäre gut sein.
Ohne Andreas wichtige Brücken-Verbinder  funktioniert die Brückenlegepanzer nicht !
Das is auch der Grund warum ich die Bruckenteilen noch nicht verklebt habe.

Einige Wochen her hatte ich versucht eine Verbinder zu machen mit original FT-Aluminium-Profil und einer 90-Grad-Stahl-Achse. Dieser Löschung sieht gut aus aber deformiert zu leicht wegen die (zu) hohe Kräfte.
  
Peter Poederoyen NL