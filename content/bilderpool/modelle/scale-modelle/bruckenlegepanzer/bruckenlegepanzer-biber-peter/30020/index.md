---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber12.jpg"
weight: "14"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30020
- /detailsfe94.html
imported:
- "2019"
_4images_image_id: "30020"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30020 -->
FT-Brückenlegepanzer-Biber hinten mit FT-1:50-Powermotor-antrieb für jede Kette.