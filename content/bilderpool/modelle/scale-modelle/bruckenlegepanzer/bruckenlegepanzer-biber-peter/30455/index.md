---
layout: "image"
title: "Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder"
date: "2011-04-14T21:26:00"
picture: "Brckenlegepanzer-details_003.jpg"
weight: "61"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30455
- /details34e1.html
imported:
- "2019"
_4images_image_id: "30455"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-04-14T21:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30455 -->
Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder