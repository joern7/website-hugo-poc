---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber39.jpg"
weight: "41"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30047
- /details824e.html
imported:
- "2019"
_4images_image_id: "30047"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30047 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught