---
layout: "image"
title: "Brückenlegepanzer-Biber in Entwicklung"
date: "2011-03-13T19:56:34"
picture: "Brckenlegepanzer-Biber_001.jpg"
weight: "50"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30246
- /detailse7d6-2.html
imported:
- "2019"
_4images_image_id: "30246"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-03-13T19:56:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30246 -->
Brückenlegepanzer-Biber in Entwicklung