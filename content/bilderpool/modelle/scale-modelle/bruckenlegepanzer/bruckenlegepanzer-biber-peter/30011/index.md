---
layout: "image"
title: "Brückenlegepanzer-Biber van J. de Grave (Nederlandse Vereniging van Modelbouwers) De Meern"
date: "2011-02-18T14:12:42"
picture: "ftbrueckenlegepanzerbiber03.jpg"
weight: "5"
konstrukteure: 
- "J. de Grave"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30011
- /detailsf283.html
imported:
- "2019"
_4images_image_id: "30011"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30011 -->
Brückenlegepanzer-Biber