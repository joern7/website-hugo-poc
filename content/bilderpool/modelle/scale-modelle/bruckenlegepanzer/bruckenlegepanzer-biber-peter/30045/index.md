---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber37.jpg"
weight: "39"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30045
- /details7079-2.html
imported:
- "2019"
_4images_image_id: "30045"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30045 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught