---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber42.jpg"
weight: "44"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30050
- /details1b8e.html
imported:
- "2019"
_4images_image_id: "30050"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30050 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught