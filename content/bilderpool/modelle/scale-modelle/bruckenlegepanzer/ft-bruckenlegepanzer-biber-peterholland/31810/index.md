---
layout: "image"
title: "Zylinder-Antrieb"
date: "2011-09-18T14:48:35"
picture: "Antrieb-Zylinder_004.jpg"
weight: "42"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31810
- /detailsc4d8.html
imported:
- "2019"
_4images_image_id: "31810"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-09-18T14:48:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31810 -->
Zylinder-Antrieb wie bei Lego 
Vorteil: Zylinder + Antrieb können sich drehen damit leichtere Einbau möglich ist.

(Andreas T hat dieser Löschung auch schon einige Jahren gezeigt) 

Grüss,

Peter
Poederoyen NL