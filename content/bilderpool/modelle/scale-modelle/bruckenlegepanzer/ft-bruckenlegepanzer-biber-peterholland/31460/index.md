---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber27.jpg"
weight: "27"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31460
- /details2435.html
imported:
- "2019"
_4images_image_id: "31460"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31460 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Ist die Brücke abgelegt - Panzerfahrer und Modellbauer sind erleichtert, wird das Stützschild angehoben.