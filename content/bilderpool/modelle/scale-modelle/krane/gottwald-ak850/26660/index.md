---
layout: "image"
title: "gottwald 28"
date: "2010-03-07T22:41:44"
picture: "gottwald_28.jpg"
weight: "28"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26660
- /details797c.html
imported:
- "2019"
_4images_image_id: "26660"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-07T22:41:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26660 -->
