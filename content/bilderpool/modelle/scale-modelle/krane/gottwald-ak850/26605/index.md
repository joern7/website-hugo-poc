---
layout: "image"
title: "gottwald 18"
date: "2010-03-06T19:20:54"
picture: "gottwald_18.jpg"
weight: "6"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26605
- /details1c61-2.html
imported:
- "2019"
_4images_image_id: "26605"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26605 -->
