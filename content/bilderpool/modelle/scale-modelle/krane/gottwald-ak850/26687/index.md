---
layout: "image"
title: "gottwald 37"
date: "2010-03-13T17:40:46"
picture: "gottwald_37.jpg"
weight: "37"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26687
- /details00e8-2.html
imported:
- "2019"
_4images_image_id: "26687"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26687 -->
