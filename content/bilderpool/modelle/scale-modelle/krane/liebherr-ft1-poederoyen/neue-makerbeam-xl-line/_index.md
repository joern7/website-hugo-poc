---
layout: "overview"
title: "Neue Makerbeam-XL line"
date: 2020-02-22T08:23:37+01:00
legacy_id:
- /php/categories/3277
- /categories296a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3277 --> 
In October 2016 bij Makerbeam verkrijgbaar in de lengtes van 50mm/100mm/150mm/200mm/300mm/ 360mm(kossel)/500mm/750mm/750mm(kossel)/1000mm en 2000mm
In eerste instantie alleen in Black, Clear waarschijnlijk in later stadium.
