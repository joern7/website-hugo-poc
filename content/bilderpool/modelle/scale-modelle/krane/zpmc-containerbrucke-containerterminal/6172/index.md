---
layout: "image"
title: "AGV 1"
date: "2006-04-29T18:58:43"
picture: "CTA_-_AGV_1.jpg"
weight: "7"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6172
- /details9c0e.html
imported:
- "2019"
_4images_image_id: "6172"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6172 -->
