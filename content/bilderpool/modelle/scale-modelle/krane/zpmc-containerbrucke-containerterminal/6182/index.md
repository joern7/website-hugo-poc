---
layout: "image"
title: "Portalkatze 1"
date: "2006-04-29T18:58:51"
picture: "CTA_-_Portalkatze_3.jpg"
weight: "16"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6182
- /detailse456.html
imported:
- "2019"
_4images_image_id: "6182"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6182 -->
Der Spreader ist mir gang gut gelungen. Auch hir habe ich sehr lange im Internet suchen müssen bis ich brauchbare Bilder gefunden habe. Das Problem der meisten Bilder im Internet ist, das beim Zoomen die Bilder unscharf werden und im Prinzip unbrauchbar sind. Die Bilder von Profifotografen haben mir sehr geholfen...