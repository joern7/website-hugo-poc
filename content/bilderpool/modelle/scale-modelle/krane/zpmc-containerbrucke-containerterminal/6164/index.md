---
layout: "image"
title: "CTA - Portalkatze 2"
date: "2006-04-29T11:14:33"
picture: "CTA_-_Portalkatze_2.jpg"
weight: "4"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6164
- /detailsb00d.html
imported:
- "2019"
_4images_image_id: "6164"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6164 -->
Die "Portalkatze" wird mit LLWin gesteuert was eigentlich nur zur Überprüfung der Positions- und Funktionsgenauigkeit taugt. Die Positionsgenauigkeit der Portalkatze beträgt etwa +/- 0,5mm. Doch ist es damit nicht getan. Die gesamte Brücke muss von Baubeginn an sehr exakt und genau gebaut werden. Die Abweichungen insgesamt liegen im Millimeterbereich. Beachtet man diese Festforderung nicht, summieren sich alle Toleranzen zu einem einzigen Desaster. Dieses ist mir jedoch erspart geblieben.