---
layout: "image"
title: "Laschplattform 1"
date: "2006-04-29T18:58:43"
picture: "CTA_-_Laschplattform_1.jpg"
weight: "12"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6178
- /details345f-2.html
imported:
- "2019"
_4images_image_id: "6178"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6178 -->
Die Laschplattform - ein Patent der HHLA. 1 1/2 Jahre habe ich im Internet nach Detailfotos gesucht. Die Ergebnisse waren bescheiden. Erst der Filmbeitrag von VOX zeigte Details, über die ich mir so lange im unklaren war. 
Ich würde eine ganze Menge dafür geben einmal auf dieser Plattform stehen zu dürfen - aber das kann man vergessen...