---
layout: "comment"
hidden: true
title: "358"
date: "2004-11-24T21:55:42"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
BleigewichteDer Ballast besteht aus sechs Bleigewichten mit je 1kg Masse. Diese Bleiplatten sind Teil eines Bleigurtes zum Tauchen. Die Bleigewicht auf dem Oberwagen stammen von meinem eigenem Bleigurt der wie ein Patronengürtel aussieht (je 250 Gramm).  Ich empfehle euch Kontakt zu einem Tauchclub aufzunehmen und dort für relativ wenig Geld Bleigewichte zu kaufen. Diese ummantelt Ihr mit Paketklebeband damit eure Finger sauber bleiben und ggf. eure Kinder sie nicht in den Mund nehmen. Die Schlitze zum durchführen des Gurtes eigenen sich gut zum arretieren mit den roten Bauplatten 30x30. 
Viel Spaß Jürgen