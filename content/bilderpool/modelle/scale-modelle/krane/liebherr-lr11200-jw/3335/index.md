---
layout: "image"
title: "Ballastwagen ISO 2"
date: "2004-11-23T21:59:56"
picture: "Ballastwagen_ISO_2.jpg"
weight: "33"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3335
- /detailsc65d.html
imported:
- "2019"
_4images_image_id: "3335"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T21:59:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3335 -->
