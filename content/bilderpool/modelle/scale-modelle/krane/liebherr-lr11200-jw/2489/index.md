---
layout: "image"
title: "Gesamtansicht 8"
date: "2004-06-06T10:40:03"
picture: "LR11200_Gesamtansicht_8_Ostern_03.jpg"
weight: "9"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/2489
- /detailsc3f5.html
imported:
- "2019"
_4images_image_id: "2489"
_4images_cat_id: "230"
_4images_user_id: "1"
_4images_image_date: "2004-06-06T10:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2489 -->
