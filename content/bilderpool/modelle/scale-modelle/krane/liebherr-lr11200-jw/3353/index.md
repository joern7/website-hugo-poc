---
layout: "image"
title: "LR11200 - ISO 025"
date: "2004-11-23T22:00:16"
picture: "Raupe_von_oben_1.jpg"
weight: "51"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3353
- /detailse9df.html
imported:
- "2019"
_4images_image_id: "3353"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T22:00:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3353 -->
