---
layout: "image"
title: "Draufsicht Drehgestell 1"
date: "2004-11-23T21:59:56"
picture: "Draufsicht_Drehgestell_1_2.jpg"
weight: "37"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3339
- /details6d99-2.html
imported:
- "2019"
_4images_image_id: "3339"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T21:59:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3339 -->
