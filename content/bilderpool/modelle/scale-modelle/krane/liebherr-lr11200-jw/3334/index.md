---
layout: "image"
title: "Ballastwagen ISO 1"
date: "2004-11-23T21:59:56"
picture: "Ballastwagen_ISO_1.jpg"
weight: "32"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3334
- /detailscdc9.html
imported:
- "2019"
_4images_image_id: "3334"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T21:59:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3334 -->
