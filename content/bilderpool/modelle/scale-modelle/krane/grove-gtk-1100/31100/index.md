---
layout: "image"
title: "grovegtk002.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk002.jpg"
weight: "2"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31100
- /details48c5.html
imported:
- "2019"
_4images_image_id: "31100"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31100 -->
