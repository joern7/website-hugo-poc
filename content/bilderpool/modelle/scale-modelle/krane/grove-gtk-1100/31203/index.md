---
layout: "image"
title: "grovegtk105.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk105.jpg"
weight: "104"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31203
- /details6b49.html
imported:
- "2019"
_4images_image_id: "31203"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31203 -->
