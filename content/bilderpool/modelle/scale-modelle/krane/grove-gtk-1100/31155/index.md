---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk057.jpg"
weight: "56"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31155
- /details30f8-2.html
imported:
- "2019"
_4images_image_id: "31155"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31155 -->
