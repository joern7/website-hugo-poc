---
layout: "image"
title: "grovegtk041.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk041.jpg"
weight: "40"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31139
- /details5732.html
imported:
- "2019"
_4images_image_id: "31139"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31139 -->
