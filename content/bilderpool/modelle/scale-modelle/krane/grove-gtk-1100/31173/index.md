---
layout: "image"
title: "Oberwagen"
date: "2011-07-14T10:50:29"
picture: "grovegtk075.jpg"
weight: "74"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31173
- /detailsab5f-2.html
imported:
- "2019"
_4images_image_id: "31173"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31173 -->
die IR- Empfänger