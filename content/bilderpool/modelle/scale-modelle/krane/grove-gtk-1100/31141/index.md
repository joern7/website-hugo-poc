---
layout: "image"
title: "Führerhäuschen"
date: "2011-07-14T10:50:29"
picture: "grovegtk043.jpg"
weight: "42"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31141
- /details45cc.html
imported:
- "2019"
_4images_image_id: "31141"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31141 -->
Mit Hilfe des Ladekrans wird das Führerhäuschen abgeladen.