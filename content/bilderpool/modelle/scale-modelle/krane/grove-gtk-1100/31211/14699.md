---
layout: "comment"
hidden: true
title: "14699"
date: "2011-07-23T17:59:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was ein großartiges Modell, Kompliment!

Nochmal zu den Fotos: Tun wir uns doch gegenseitig den Gefallen, *unterbelichtete* und *unscharfe* Fotos gar nicht erst hochzuladen! Haben wir lieber Geduld, bis die Sonne wieder scheint, lassen viel Licht rein, machen von wenigen guten Motiven Fotos, davon von diesen jeweils mehrere, und sortieren gnadenlos aus, was nicht wirklich gute Qualität hat. Das freut alle - schließlich bleiben die Fotos hier ja für die Ewigkeit!

Gruß,
Stefan