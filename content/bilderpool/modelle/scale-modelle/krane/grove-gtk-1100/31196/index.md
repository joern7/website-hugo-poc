---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk098.jpg"
weight: "97"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31196
- /details6065-2.html
imported:
- "2019"
_4images_image_id: "31196"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31196 -->
Zum Schluss wird der Hauptmast ausgefahren, die Streben sind nun straff und bringen deutlich mehr Stabilität. Mein Mast wurde von Hand ausgefahren, aber man kann das bestimmt noch mit einer Gewindestange automatisieren. Da meine stützen nicht fest genug am Unterwagen halten und die streben so straff sein müssen, habe ich au f jede Stütze einen Wegsten gelegt.