---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk028.jpg"
weight: "27"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31126
- /detailsc3ae.html
imported:
- "2019"
_4images_image_id: "31126"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31126 -->
Die 2 Powermotoren, die den Hauptmast aufrichten.