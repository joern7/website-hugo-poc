---
layout: "image"
title: "grovegtk008.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk008.jpg"
weight: "8"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31106
- /detailsac32.html
imported:
- "2019"
_4images_image_id: "31106"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31106 -->
