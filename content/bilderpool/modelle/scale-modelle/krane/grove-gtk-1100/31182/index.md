---
layout: "image"
title: "Führerhäusschen"
date: "2011-07-14T10:50:29"
picture: "grovegtk084.jpg"
weight: "83"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31182
- /details973e.html
imported:
- "2019"
_4images_image_id: "31182"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31182 -->
Das Führerhäusschen lässt sich nach oben und unten neigen.