---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk027.jpg"
weight: "26"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31125
- /detailsb9e6-2.html
imported:
- "2019"
_4images_image_id: "31125"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31125 -->
