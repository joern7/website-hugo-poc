---
layout: "comment"
hidden: true
title: "14426"
date: "2011-06-08T16:24:04"
uploadBy:
- "pk"
license: "unknown"
imported:
- "2019"
---
Er is helemaal geen Amerikaans aan, want Made in China.

Ik had er 2 besteld bij Conrad en het past ook best wel. Maar de radiale centrering is niet goed.

Daarom heb ik een SKF 51111 gekocht. Deze kost dan wel 34euro maar is D=78 x d=55 x h=16.
Hierdoor past hij bijna precies in het zwarte deel van de draaikrans. Door nu ook aan de onderkant van de bovenbouw dit zwarte gedeelte te plaatsen, verkrijg je een betere radiale centrering.
Aan de onderkant van de onderwagen zit dan de UBC 51110 klem tussen een paar bouwstenen. De 3 pinnen van de z40 passen precies in de 51110.  In plaats van en naaf+naafmoer ziter  een M10 inslagmoer in de z40. Het geheel wordt dan met een M10 x 80 bout, en een paar carosserieringen + sluitplaat, samen getrokken.
Probleem hierbij is dat er geen mogelijkheid meer is om de kabeltjes voor de motoren door te voeren. Daarom ga ik de M10 bout vervangen voor een buis met 1/2" schroefdraad.

Als ik het allemaal klaar heb zal ik er foto's van maken en die een keer uploaden.

Gruß,
peter