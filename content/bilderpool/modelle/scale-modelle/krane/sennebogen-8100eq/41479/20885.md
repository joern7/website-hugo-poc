---
layout: "comment"
hidden: true
title: "20885"
date: "2015-07-26T13:07:07"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Supermodel Ruurd!! Grote klasse, zoals gebruikelijk :)

Hoogstwaarschijnlijk ligt het probleem van het "opwinden" in het gebruik van de kunststof assen. Die zijn te flexibel. Volgens mij heeft FT ooit een wormwiel gehad van gelijke afmeting (diameter) maar met stalen as. Wellicht is dat een oplossing, al speelt dan het probleem van doorslippen van het tandwiel Z20 op de as weer. :S

Höchstwahrscheinlich liegt das Problem vom "aufwinden" (tordieren) im Gebrauch von den kunststoff Rastasschen. Die sind zu flexibel. Ich glaube FT hat einmal ein Schneckengetriebe mit Stahlasche gehabt, vielleicht wäre das eine Lösung. Ich furchte aber dass die Reibung zwischen Z20 Zahnrad und Stahlasche nicht ausreichen wird. 

MfrGr Marten