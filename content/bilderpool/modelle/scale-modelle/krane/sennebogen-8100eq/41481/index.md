---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250020.jpg"
weight: "17"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/41481
- /detailsac58.html
imported:
- "2019"
_4images_image_id: "41481"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41481 -->
De afgewerkte rups unit. Ze zijn beide demonteerbaar door het verwijderen van 6 stalen assen peer kant.