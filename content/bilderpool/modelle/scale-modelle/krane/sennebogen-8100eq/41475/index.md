---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250012.jpg"
weight: "11"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/41475
- /details6d32.html
imported:
- "2019"
_4images_image_id: "41475"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41475 -->
Dubbele xm motor gekoppeld met een ketting zorgt voor de beweging van de arm.