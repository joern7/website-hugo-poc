---
layout: "image"
title: "manitowoc4"
date: "2011-03-06T18:12:43"
picture: "man4.jpg"
weight: "11"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30213
- /detailsa5d4.html
imported:
- "2019"
_4images_image_id: "30213"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-03-06T18:12:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30213 -->
