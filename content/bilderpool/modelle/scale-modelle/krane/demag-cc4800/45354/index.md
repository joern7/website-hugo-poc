---
layout: "image"
title: "DEMAG CC4800_16"
date: "2017-03-01T15:57:19"
picture: "demagcc16.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45354
- /details583f-2.html
imported:
- "2019"
_4images_image_id: "45354"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45354 -->
Das untere ende der A-bock: das ist nicht gans richtig, aber man braucht diesen 2x 8 Seilscheiben um der A-Bock zu heben.
Hier Tanzt aber einer aus der Reihe. Der ist aber zeer wichtig da er benutzt werd zur messung der Kräften.
Die 2 Horizontal eingebaute Seilscheiben dienen zur längenausgleich und umlenkung von das Seil.