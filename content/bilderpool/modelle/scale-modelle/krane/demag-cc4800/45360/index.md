---
layout: "image"
title: "DEMAG CC4800_22"
date: "2017-03-01T15:57:19"
picture: "demagcc22.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45360
- /details1b9b.html
imported:
- "2019"
_4images_image_id: "45360"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45360 -->
Drei 312:1 Getriebemotoren zur drehen de Kran. Elektrisch sind die drei in Serie geschalted.
Der kleine Seilwinde werd benutzt um der Draht durch die Hakenflasche zu siehen.