---
layout: "image"
title: "DEMAG CC4800_10"
date: "2017-03-01T15:57:19"
picture: "demagcc10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45348
- /details341e.html
imported:
- "2019"
_4images_image_id: "45348"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45348 -->
Auslegerspitze. 21 Seilscheiben 45,5mm.