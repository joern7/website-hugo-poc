---
layout: "image"
title: "DEMAG CC4800_23"
date: "2017-03-01T15:57:19"
picture: "demagcc23.jpg"
weight: "23"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45361
- /details706c-2.html
imported:
- "2019"
_4images_image_id: "45361"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45361 -->
Aufsicht auf der ganse unterbau der Kran.