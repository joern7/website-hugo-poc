---
layout: "image"
title: "DEMAG CC4800_47"
date: "2017-03-01T15:57:19"
picture: "demagcc47.jpg"
weight: "47"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45385
- /details5b81.html
imported:
- "2019"
_4images_image_id: "45385"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45385 -->
Links: Anschlussen für Spannungsversorgung und Kabelfernsteuerung.
Mitte: 2 der 16 Fahrtregler; Conrad 190040-62.
Rechts: Schalter zur auswahl der Spannungsversorgungen und "Power" LED.
Hinter der Accu-Stecker ist gerade noch eine der 4 Ferderkontakten 31306 zu sehen. (sehe auch Bild 26)