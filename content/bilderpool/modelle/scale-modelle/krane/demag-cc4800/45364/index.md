---
layout: "image"
title: "DEMAG CC4800_26"
date: "2017-03-01T15:57:19"
picture: "demagcc26.jpg"
weight: "26"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45364
- /detailsfe06.html
imported:
- "2019"
_4images_image_id: "45364"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45364 -->
Beide Abstützarmen neben und in einander.
Um die Motoren mit Strom zu versorgen habe ich 2 2mm Messing Achsen in 4x2mm ABS Röhren gesteckt. Diesen Messing Achsen kommen in Kontakt mit Federkontakten 31306.
Auf Bild 47 ist gerade noch einer der 4 Federkontakten zu sehen.