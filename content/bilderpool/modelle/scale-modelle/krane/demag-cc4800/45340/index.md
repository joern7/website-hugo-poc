---
layout: "image"
title: "DEMAG CC4800_2"
date: "2017-03-01T15:57:19"
picture: "demagcc02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45340
- /details6a05.html
imported:
- "2019"
_4images_image_id: "45340"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45340 -->
Da drausen das Wetter schlecht is und ich trotzdem Bilder brauchte für ein Beitrag für das magazin "De Modelbauwer", habe ich der Kran mal im Wohnzimmer aufgebaut.
Da aber der Spitze der Dereckausleger auf eine höhe von 2,50m kam und der Decke nur 2,43m ist, wohlte das natürlich nicht passen.