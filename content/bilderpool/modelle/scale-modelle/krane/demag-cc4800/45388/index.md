---
layout: "image"
title: "DEMAG CC4800_50"
date: "2017-03-01T15:57:19"
picture: "demagcc50.jpg"
weight: "50"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45388
- /details16fd-3.html
imported:
- "2019"
_4images_image_id: "45388"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45388 -->
Noch ein Bild von das damals noch unvollendetes Modell mit Schwebender Ballastplatte.
Speziell für das Tranport von dieses Modell, habe ich 4 Größe Kisten gebaut.