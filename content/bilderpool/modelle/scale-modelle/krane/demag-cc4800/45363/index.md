---
layout: "image"
title: "DEMAG CC4800_25"
date: "2017-03-01T15:57:19"
picture: "demagcc25.jpg"
weight: "25"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45363
- /detailse22c.html
imported:
- "2019"
_4images_image_id: "45363"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45363 -->
Beide Abstützarmen neben einander.