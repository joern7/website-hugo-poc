---
layout: "image"
title: "DEMAG CC4800_5"
date: "2017-03-01T15:57:19"
picture: "demagcc05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45343
- /detailsd561.html
imported:
- "2019"
_4images_image_id: "45343"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45343 -->
Übersicht auf der Kran.