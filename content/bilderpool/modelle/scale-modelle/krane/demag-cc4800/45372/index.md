---
layout: "image"
title: "DEMAG CC4800_34"
date: "2017-03-01T15:57:19"
picture: "demagcc34.jpg"
weight: "34"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45372
- /details7487-2.html
imported:
- "2019"
_4images_image_id: "45372"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45372 -->
Raupe nicht gespannen.