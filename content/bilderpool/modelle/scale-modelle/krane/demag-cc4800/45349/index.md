---
layout: "image"
title: "DEMAG CC4800_11"
date: "2017-03-01T15:57:19"
picture: "demagcc11.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45349
- /details497f-2.html
imported:
- "2019"
_4images_image_id: "45349"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45349 -->
Auslegerspitze mit drei Umlenkseilscheiben.