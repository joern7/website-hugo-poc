---
layout: "image"
title: "DEMAG CC4800_4"
date: "2017-03-01T15:57:19"
picture: "demagcc04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45342
- /detailse5c6.html
imported:
- "2019"
_4images_image_id: "45342"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45342 -->
Front ansicht.
Hier ist auch zu sehen das der Auslegerfuß in die breite als in die höhe Konisch ist.: Breite über die Lager 195mm und die Breite der ausleger ist 240mm. Die höhe ist 186mm.