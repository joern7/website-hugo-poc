---
layout: "image"
title: "DEMAG CC4800_7"
date: "2017-03-01T15:57:19"
picture: "demagcc07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45345
- /detailsa1e5.html
imported:
- "2019"
_4images_image_id: "45345"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45345 -->
Kabine aus der Vogelperspektive.
Um zu verhindern das das Personal abstürzt gibt es Laufblechen und Geländer.