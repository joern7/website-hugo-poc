---
layout: "image"
title: "DEMAG CC4800_46"
date: "2017-03-01T15:57:19"
picture: "demagcc46.jpg"
weight: "46"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45384
- /detailsa37e.html
imported:
- "2019"
_4images_image_id: "45384"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45384 -->
Radioplatte 2: 8 Fahrtregler und BEC von Conrad und 2 Decoder 8042 von Robbe.

Regler   9: Multipropkanal 1; Aufrichtebock; 2x 20:1 PM
Regler 10: Multipropkanal 2; Ausleger; 2x 20:1 PM
Regler 11: Multipropkanal 3; Winde 3; 20:1 PM
Regler 12: Multipropkanal 4; Zusatswinde 4; mini-motor
Regler 13: Multiswitchkanal 3; Abstützung 1; 100:1 Micro Metal Gear Motor 
Regler 14: Multiswitchkanal 2; Abstützung 2; 100:1 Micro Metal Gear Motor 
Regler 15: Multiswitchkanal 6; Abstützung 3; 100:1 Micro Metal Gear Motor 
Regler 16: Multiswitchkanal 7; Abstützung 4; 100:1 Micro Metal Gear Motor