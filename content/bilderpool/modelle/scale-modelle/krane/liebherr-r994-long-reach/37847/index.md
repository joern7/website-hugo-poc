---
layout: "image"
title: "Laagste stand"
date: "2013-11-27T14:54:23"
picture: "PB260357.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/37847
- /detailsb34d-2.html
imported:
- "2019"
_4images_image_id: "37847"
_4images_cat_id: "2812"
_4images_user_id: "838"
_4images_image_date: "2013-11-27T14:54:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37847 -->
Ik sta er elke keer versteld van wat die cilinders drukken kunnen aan kilo's. Alleen het bakje weegt al 250 gram en dat dan op 83 cm.
Meer foto's volgen