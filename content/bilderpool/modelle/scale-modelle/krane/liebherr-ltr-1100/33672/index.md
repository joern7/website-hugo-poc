---
layout: "image"
title: "Hubzylinder"
date: "2011-12-13T23:22:52"
picture: "liebherrltr29.jpg"
weight: "29"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33672
- /details5351.html
imported:
- "2019"
_4images_image_id: "33672"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33672 -->
Der Hubzylinder arbeitet mit einer Gewindestange, doe von einem 1:20 Powermot. angetrieben wird. Der Zylinder besteht aus zwei Allurohroren die ineinander fahren. Das innere Rohr habe ich so modifiziert, dass eine Mutter am Anfang sitzt. So kann die Gewindestange das Rohr bewegen. Das äussere Rohr gibt nur Stabilität und schützt die Gewindestange.