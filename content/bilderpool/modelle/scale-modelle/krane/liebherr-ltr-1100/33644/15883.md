---
layout: "comment"
hidden: true
title: "15883"
date: "2011-12-15T22:42:37"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Thilo,
da ist dir ein respektables Modell gelungen, zu dem man dich gern beglückwünscht.
Vielleicht ist es für das Modell ein Glück, daß es im ft-System keine geeigneten Teile für derartige Teleskopausleger gibt. Da würde der mobile Raupenkran durch das Gewicht des Auslegers nach vorn sicher umfallen. Aber Reserve im Raupenfahrwerk hättest du dazu schon noch nach vorn, wenn man sich die Proportionen am Vorbild anschaut und dann auch schwerere Gewichte nehmen würde.
Gruß, Udo2