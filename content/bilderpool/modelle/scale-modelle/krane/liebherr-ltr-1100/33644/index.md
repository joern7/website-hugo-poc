---
layout: "image"
title: "Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr01.jpg"
weight: "1"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33644
- /details2bec.html
imported:
- "2019"
_4images_image_id: "33644"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33644 -->
So sieht der Kran in Transportstellung aus.
Er wird mit der ft-Fernsteuerung bedient (Fahren, Lenken, Teleskope ausfahren, Drehen, Ausleger haben/senken, Haken bewegen), die anderen Funktionen (Spurweitenänderung, Ballastaufnahme, Kabine anheben) werden am Modell bedient.