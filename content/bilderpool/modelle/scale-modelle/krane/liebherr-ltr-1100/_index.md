---
layout: "overview"
title: "Liebherr LTR 1100"
date: 2020-02-22T08:23:19+01:00
legacy_id:
- /php/categories/2490
- /categories85e6.html
- /categories1e3d.html
- /categories6ba3.html
- /categories0ced.html
- /categoriesdbb1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2490 --> 
Mit diesem Modell ahbe ich den Liebherr LTR 1100 im Maßstab 1:20 Nachgebaut. Ich habe versucht alle Funktionen des Vorbilds umzusetzen. Wie zum Beispiel Teleskopausleger, Spurweitenverstellung und anhebbare Fahrerkabine. Die Jack Up Zylinder des Vorbilds konnte ich nicht nachbauen. Den Teleskopausleger und den Hebezylinder habe ich nicht aus ft, sondern aus Sperholz und allu-profilen gebastelt.