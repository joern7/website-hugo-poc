---
layout: "image"
title: "Seilwinde"
date: "2011-12-13T23:22:52"
picture: "liebherrltr28.jpg"
weight: "28"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33671
- /detailse507-2.html
imported:
- "2019"
_4images_image_id: "33671"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33671 -->
Minimot. an der Seilwinde. unten im Bild erkennt man das Akkupack, dass den Strom liefert.