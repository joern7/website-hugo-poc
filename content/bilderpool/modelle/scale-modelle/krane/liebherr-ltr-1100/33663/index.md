---
layout: "image"
title: "Spurweitenverstllung"
date: "2011-12-13T23:22:51"
picture: "liebherrltr20.jpg"
weight: "20"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33663
- /details7587.html
imported:
- "2019"
_4images_image_id: "33663"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33663 -->
Mit dem Polwendeschalter stellt man die Richtung ein (Einschieben/Ausschieben) und mit dem Taster rechts daneben werden die zwei Abschalttaster überbrückt.