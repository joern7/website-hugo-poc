---
layout: "image"
title: "Blick in den Ausleger"
date: "2011-12-13T23:22:51"
picture: "liebherrltr06.jpg"
weight: "6"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33649
- /details54c8.html
imported:
- "2019"
_4images_image_id: "33649"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33649 -->
Der Ausleger besteht aus drei Teleskopen und ist komplett aus 4mm Sperrholz gebaut. Am äussersten Element ist ein Powermot. befestigt, der durch eine Gewindestange die Teleskope ausschiebt.