---
layout: "image"
title: "Kabine"
date: "2011-12-13T23:22:51"
picture: "liebherrltr17.jpg"
weight: "17"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33660
- /details6156.html
imported:
- "2019"
_4images_image_id: "33660"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33660 -->
Hier kann man die angehobene Kabine sehen. Ein Pneumatikzylinder, der hinter den Kabeln versteckt ist, bewegt sie. Der blaue Kolben schimmert ein bisschen hindurch.