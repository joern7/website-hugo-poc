---
layout: "image"
title: "Hubzylinder"
date: "2011-12-13T23:22:52"
picture: "liebherrltr31.jpg"
weight: "31"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33674
- /detailsabbe.html
imported:
- "2019"
_4images_image_id: "33674"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33674 -->
Auf den Übergang habe ich eine Kappe gesetzt, die Früher mal den Deckel eines Reagenzglases gebildet hat.