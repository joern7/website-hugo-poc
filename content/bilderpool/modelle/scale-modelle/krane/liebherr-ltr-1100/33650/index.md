---
layout: "image"
title: "Spurweitenverstellung"
date: "2011-12-13T23:22:51"
picture: "liebherrltr07.jpg"
weight: "7"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33650
- /details8594.html
imported:
- "2019"
_4images_image_id: "33650"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33650 -->
Da die Spurweite in der Transportstellung nicht ausreichend Standfläche bietet, kann der Kran die Raupen ausschieben und so die Standfläche erhöhen. In echt geschieht dies durch Hydraulik, bei mir mit Gewindestangen. Jedoch funktioniert es nur auf glattem Untergrund wie Fliesen.