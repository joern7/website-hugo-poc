---
layout: "image"
title: "Demag CC1400_32"
date: "2016-12-29T19:33:43"
picture: "demagcc32.jpg"
weight: "32"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44978
- /detailsfa2d.html
imported:
- "2019"
_4images_image_id: "44978"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44978 -->
Das untere Kugellager. Die 3 Stiften der z40 passen genau im innere des Lagers. Mit hilfe von Leitungsteilen aus der Sanitärbereich werden die Teilen der Drehkranz zusammen gedreht.