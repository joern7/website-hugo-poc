---
layout: "image"
title: "Demag CC1400_3"
date: "2016-12-29T19:33:43"
picture: "demagcc03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44949
- /details847a.html
imported:
- "2019"
_4images_image_id: "44949"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44949 -->
In der Vogelperspektive. Na ja, das heist, vom Tisch aus gesehen.