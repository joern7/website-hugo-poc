---
layout: "image"
title: "Demag CC1400_36"
date: "2016-12-29T19:33:43"
picture: "demagcc36.jpg"
weight: "36"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44982
- /details010a.html
imported:
- "2019"
_4images_image_id: "44982"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44982 -->
Der Drehkranz besteht aus 2 Axial-Kugellager und ein Zahnkranz.
Zur zentrirung past ein z40. Das 3/4 Soll gewinde von das Wasserleitungsrohr past in der z40.