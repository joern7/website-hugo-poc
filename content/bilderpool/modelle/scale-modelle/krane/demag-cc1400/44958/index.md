---
layout: "image"
title: "Demag CC1400_12"
date: "2016-12-29T19:33:43"
picture: "demagcc12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44958
- /details5e53-2.html
imported:
- "2019"
_4images_image_id: "44958"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44958 -->
Schwebende Umlenkrollen.