---
layout: "image"
title: "Demag CC1400_31"
date: "2016-12-29T19:33:43"
picture: "demagcc31.jpg"
weight: "31"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44977
- /details638e-2.html
imported:
- "2019"
_4images_image_id: "44977"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44977 -->
Der Unterwagen von unten gesehen.
Der ganse Kran steht eigenlich auf 40x 14er Räder.
Angetrieben werden die Raupen von 2x 50er PW's