---
layout: "image"
title: "Demag CC1400_18"
date: "2016-12-29T19:33:43"
picture: "demagcc18.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44964
- /details7f04.html
imported:
- "2019"
_4images_image_id: "44964"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44964 -->
Anbau der Wipplenker auf der Wippspitze.