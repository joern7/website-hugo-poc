---
layout: "image"
title: "Demag CC1400_8"
date: "2016-12-29T19:33:43"
picture: "demagcc08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44954
- /details3d4e.html
imported:
- "2019"
_4images_image_id: "44954"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44954 -->
Von oben durch der Aufrichtebock hindurch sehen wir:
- die feste Umlenkrollen und Doppelte Seilwinde des Auslegerverstellwerk
- 2 XM Motoren, der drite werd vom Aufrichtebock verdeckt
- Winde 1, Winde 2 werd vom Derrickauslegerfuß verdeckt
- der Ballast hat bei mir ein anderes aussehen dan beim Orginal