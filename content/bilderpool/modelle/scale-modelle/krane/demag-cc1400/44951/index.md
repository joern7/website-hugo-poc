---
layout: "image"
title: "Demag CC1400_5"
date: "2016-12-29T19:33:43"
picture: "demagcc05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44951
- /details0012-3.html
imported:
- "2019"
_4images_image_id: "44951"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44951 -->
Von der rechte seite.