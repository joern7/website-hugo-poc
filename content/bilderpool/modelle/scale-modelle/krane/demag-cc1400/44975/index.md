---
layout: "image"
title: "Demag CC1400_29"
date: "2016-12-29T19:33:43"
picture: "demagcc29.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44975
- /details56f4-2.html
imported:
- "2019"
_4images_image_id: "44975"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44975 -->
Der kleine Getriebemotor ist in ein aufgebohrten m1,5 Schneckenmutter gelagert. 2 Bauplatten 15x15 tragen dazu bei das der Motor sich nicht verdrehen kan.