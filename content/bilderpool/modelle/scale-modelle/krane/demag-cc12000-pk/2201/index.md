---
layout: "image"
title: "CC12000_11"
date: "2004-02-29T21:26:11"
picture: "cc12000_11.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2201
- /details2d8d.html
imported:
- "2019"
_4images_image_id: "2201"
_4images_cat_id: "211"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2201 -->
