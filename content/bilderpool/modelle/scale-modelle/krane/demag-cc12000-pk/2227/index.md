---
layout: "image"
title: "CC12000_35"
date: "2004-02-29T21:26:12"
picture: "cc12000_35.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2227
- /details69b6-2.html
imported:
- "2019"
_4images_image_id: "2227"
_4images_cat_id: "211"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2227 -->
Der junger man mit seine Sonnenbrille neben der Kran bin ich: Somer 1995.