---
layout: "image"
title: "Mani16.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani16.jpg"
weight: "14"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2195
- /details63ae.html
imported:
- "2019"
_4images_image_id: "2195"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2195 -->
Hier sind die Räder fast in Fahrposition angekommen. Wenn sie alle parallel stehen, kann der Kran als Ganzes vor- und zurückfahren.