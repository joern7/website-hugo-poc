---
layout: "comment"
hidden: true
title: "166"
date: "2004-02-27T21:24:30"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
BallastwagenDanke pk :-)
In der Tat, ab Seite 27 des Product Guide wird der Ballastwagen dargestellt, und er hat genau dieses Fahrwerk der tanzenden Räder. Also keine dichterische Freiheit, sondern wirklich ein SCALE-Modell!

--
Die Firma ist recht neugierig und hat vor dem Download eine Menge Daten verlangt. Die Email-Adresse wird aber nicht geprüft, und für solchen Fällen hab ich sowieso einen Dummy-Satz von persönlichen Daten parat. Der Product Guide wurde als EXE-Datei heruntergeladen, gab beim Starten aber eine Fehlermeldung. Einfach umbenennen auf .PDF, und die Sache läuft.