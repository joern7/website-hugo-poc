---
layout: "image"
title: "Mani04a.jpg"
date: "2004-02-23T00:24:27"
picture: "Mani04a.jpg"
weight: "4"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2185
- /detailsec7d.html
imported:
- "2019"
_4images_image_id: "2185"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2185 -->
Der Saal ist gerade mal hoch genug, um den Kran voll aufzurichten.