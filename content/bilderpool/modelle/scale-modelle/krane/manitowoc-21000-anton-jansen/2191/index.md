---
layout: "image"
title: "Mani11.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani11.jpg"
weight: "10"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2191
- /details143c-2.html
imported:
- "2019"
_4images_image_id: "2191"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2191 -->
Der Drehkranz sieht etwas filigran aus.
Das Führerhaus wird pneumatisch gekippt.