---
layout: "image"
title: "Seitenansicht Stütze komplett ausgefahren"
date: "2017-06-11T21:52:18"
picture: "20170531_104826.jpg"
weight: "4"
konstrukteure: 
- "The Rob"
fotografen:
- "The Rob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "The Rob"
license: "unknown"
legacy_id:
- /php/details/45934
- /detailsb0dc.html
imported:
- "2019"
_4images_image_id: "45934"
_4images_cat_id: "3411"
_4images_user_id: "2745"
_4images_image_date: "2017-06-11T21:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45934 -->
Hier sieht man, dass die Stützen den Kran komplett von den Rädern heben können, sodass der nur noch auf den vier Stützen steht.
Mit dem fertigen Chassis (ca. 2kg) funktioniert das bereits motorisiert.