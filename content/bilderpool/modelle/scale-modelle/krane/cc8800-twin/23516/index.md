---
layout: "image"
title: "Draufsicht Unterwagen 4/4"
date: "2009-03-24T06:44:02"
picture: "unteroberwagen10.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23516
- /details4dee-2.html
imported:
- "2019"
_4images_image_id: "23516"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:44:02"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23516 -->
Noch im Rohzustand