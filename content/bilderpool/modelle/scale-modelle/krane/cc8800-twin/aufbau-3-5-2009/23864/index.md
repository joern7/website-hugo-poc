---
layout: "image"
title: "CC8800 Twin 28/28"
date: "2009-05-04T21:14:31"
picture: "cctwin28.jpg"
weight: "28"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23864
- /details070b-3.html
imported:
- "2019"
_4images_image_id: "23864"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:31"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23864 -->
Das ist die obere Quertraverse des Derrickwagens.
Man kann ganz deutlich erkennen wie sich die Achse in die Strebe reindrückt.
Das war der Grund für den Abbruch.