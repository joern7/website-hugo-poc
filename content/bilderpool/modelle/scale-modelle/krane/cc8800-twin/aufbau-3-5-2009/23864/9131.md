---
layout: "comment"
hidden: true
title: "9131"
date: "2009-05-05T02:42:45"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Jetzt weiß ich auch, warum ich bei meiner Seilbahn letztes Jahr alle relevanten
Tragelemente doppelt bzw. dreifach ausgelegt habe - Masse aller Spannge-
wichte: 12,3 kg! Bei derart martialischen Verwindungen ist das eigentlich nur
konsequent.

Gruß, Thomas