---
layout: "image"
title: "CC8800 Twin 24/28"
date: "2009-05-04T21:14:30"
picture: "cctwin24.jpg"
weight: "24"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23860
- /details6266.html
imported:
- "2019"
_4images_image_id: "23860"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23860 -->
"Der kleine Junge, da in der Mitte, wollte wohl von seinen Eltern aus der Spieleecke abgeholt werden"
Mal im ernst bei diesem Anblick habe ich selber ein mulmiges Gefühl wenn nicht zusagen macht mir das ein bißchen Angst.