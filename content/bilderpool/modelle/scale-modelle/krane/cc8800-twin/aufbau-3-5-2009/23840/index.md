---
layout: "image"
title: "CC8800 Twin 4/28"
date: "2009-05-04T21:14:29"
picture: "cctwin04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "FrankLinde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23840
- /details2b09.html
imported:
- "2019"
_4images_image_id: "23840"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23840 -->
