---
layout: "image"
title: "CC8800 Twin 7/28"
date: "2009-05-04T21:14:30"
picture: "cctwin07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23843
- /detailsd4d0.html
imported:
- "2019"
_4images_image_id: "23843"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23843 -->
