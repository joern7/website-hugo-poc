---
layout: "image"
title: "CC8800 Twin 13/28"
date: "2009-05-04T21:14:30"
picture: "cctwin13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23849
- /details0ba4-2.html
imported:
- "2019"
_4images_image_id: "23849"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23849 -->
Arbeit war angesagt beim zusammenbau der einzelnen Komponenten  aber ich hatte fleißige Helfer