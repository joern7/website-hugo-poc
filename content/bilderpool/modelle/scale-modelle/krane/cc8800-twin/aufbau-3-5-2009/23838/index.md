---
layout: "image"
title: "CC8800 Twin 2/28"
date: "2009-05-04T21:14:29"
picture: "cctwin02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23838
- /details5873.html
imported:
- "2019"
_4images_image_id: "23838"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23838 -->
