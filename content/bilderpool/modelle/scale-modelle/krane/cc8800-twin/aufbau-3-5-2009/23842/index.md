---
layout: "image"
title: "CC8800 Twin 6/28"
date: "2009-05-04T21:14:29"
picture: "cctwin06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23842
- /details8164.html
imported:
- "2019"
_4images_image_id: "23842"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23842 -->
Die Teile links und rechts von mir müssen alle noch verbaut werden.