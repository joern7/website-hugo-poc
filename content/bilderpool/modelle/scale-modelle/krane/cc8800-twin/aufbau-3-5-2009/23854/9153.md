---
layout: "comment"
hidden: true
title: "9153"
date: "2009-05-05T19:35:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch,

ich überlege die ganze Zeit, ob man nicht zwischen die oben liegenden Statikträger ab und zu einen BS5 dazwischen bauen könnte. Das hätte den Effekt, dass der Arm eigentlich einen Buckel beschreibt, unter seinem Eigengewicht aber wieder einigermaßen gerade dasteht. Ginge sowas wohl?

Sehr respektvoller Gruß,
Stefan