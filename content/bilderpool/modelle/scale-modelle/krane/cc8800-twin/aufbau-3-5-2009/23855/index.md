---
layout: "image"
title: "CC8800 Twin 19/28"
date: "2009-05-04T21:14:30"
picture: "cctwin19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23855
- /details2281.html
imported:
- "2019"
_4images_image_id: "23855"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23855 -->
Um auf dem Boden der Tatsachen wieder zurück zu kommen musste ich feststellen das dieses Monster ohne fremde Hilfe(wenn auch passiv) leider nicht zu bändigen war.