---
layout: "image"
title: "Derrickwagen CC8800 Twin 5/12"
date: "2009-03-08T18:34:56"
picture: "cctwin05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23426
- /details7df2.html
imported:
- "2019"
_4images_image_id: "23426"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23426 -->
Hier einer von 4 Türmen des Schwebeballastes.