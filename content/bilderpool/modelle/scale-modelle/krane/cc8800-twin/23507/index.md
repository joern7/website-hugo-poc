---
layout: "image"
title: "Draufsicht Oberwagen 1/6"
date: "2009-03-24T06:43:31"
picture: "unteroberwagen01.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23507
- /details4dbd-2.html
imported:
- "2019"
_4images_image_id: "23507"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23507 -->
Noch nicht ganz fertig aber man kann schon erkennen welche Ausmaße das ganze an nimmt