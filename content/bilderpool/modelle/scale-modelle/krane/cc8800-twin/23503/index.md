---
layout: "image"
title: "Kranhaken 1/4"
date: "2009-03-24T06:43:30"
picture: "kranhaken1.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23503
- /details4e44-3.html
imported:
- "2019"
_4images_image_id: "23503"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23503 -->
Das ist der passende Haken für mein Modell.
Hier wieder ein Größenvergleich Orginal Kranhaken, ft Männchen und Kranhaken des CC8800 Twin.