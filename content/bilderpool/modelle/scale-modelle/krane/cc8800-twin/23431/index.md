---
layout: "image"
title: "Derrickwagen CC8800 Twin 10/12"
date: "2009-03-08T18:34:57"
picture: "cctwin10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23431
- /details6f75.html
imported:
- "2019"
_4images_image_id: "23431"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23431 -->
Bei diesem Bild kann man gut erkennen wie die Gestelle um 90° gedreht sin.