---
layout: "image"
title: "Derrickwagen CC8800 Twin 8/12"
date: "2009-03-08T18:34:56"
picture: "cctwin08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23429
- /details459d.html
imported:
- "2019"
_4images_image_id: "23429"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23429 -->
Das Bild zeigt den neuen Zahnstangenantrieb von TST.
Für mein Modell mußten gleich 2 herhalten.