---
layout: "image"
title: "CC8800 Twin 3/6"
date: "2009-04-26T19:08:26"
picture: "twin3.jpg"
weight: "30"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23816
- /details3a14.html
imported:
- "2019"
_4images_image_id: "23816"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23816 -->
Hier ist das Maschinenhaus mit Laufstegen, Gegengewichten und Rückfallsicherung zu sehen.