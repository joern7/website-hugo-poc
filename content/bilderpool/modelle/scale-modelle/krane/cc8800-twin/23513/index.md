---
layout: "image"
title: "Draufsicht Unterwagen 1/4"
date: "2009-03-24T06:44:02"
picture: "unteroberwagen07.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23513
- /details9821.html
imported:
- "2019"
_4images_image_id: "23513"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:44:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23513 -->
Noch im Rohzustand