---
layout: "image"
title: "Derrickwagen CC8800 Twin 4/12"
date: "2009-03-08T18:34:56"
picture: "cctwin04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23425
- /details545f.html
imported:
- "2019"
_4images_image_id: "23425"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23425 -->
