---
layout: "image"
title: "Draufsicht Oberwagen 5/6"
date: "2009-03-24T06:43:31"
picture: "unteroberwagen05.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23511
- /details954d-2.html
imported:
- "2019"
_4images_image_id: "23511"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23511 -->
