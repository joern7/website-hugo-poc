---
layout: "image"
title: "Draufsicht Unterwagen 2/4"
date: "2009-03-24T06:44:02"
picture: "unteroberwagen08.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23514
- /details7a2b.html
imported:
- "2019"
_4images_image_id: "23514"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:44:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23514 -->
Noch im Rohzustand