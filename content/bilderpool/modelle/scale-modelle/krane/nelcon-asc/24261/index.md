---
layout: "image"
title: "asc_spreader V2_5"
date: "2009-06-07T14:43:36"
picture: "nelconasc39.jpg"
weight: "38"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24261
- /details7c68.html
imported:
- "2019"
_4images_image_id: "24261"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24261 -->
Zur bedienung der Endschalter hab ich auf die achsen der Twistlocks betätiger aufgesetst.
Einige stückchen Kunststoff rohr auf ein ander geklebt und von einen Madenschraube versehen und an einen ende abgewinkeld.

Da bei diesen kompakten aufbau kein plats ist für die ft Flachstecker, habbe ich, aus 10mm langen Messung rohr 2,5x1,6 , kontakstiften hergesteld.
An einen ende habbe ich ein schlits eingefreest um da die drahten der Dioden und kabelenden ein zu Löten.