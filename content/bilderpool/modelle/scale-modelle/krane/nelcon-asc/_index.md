---
layout: "overview"
title: "Nelcon ASC"
date: 2020-02-22T08:22:51+01:00
legacy_id:
- /php/categories/1661
- /categories4551.html
- /categoriesf594.html
- /categoriesa1ef.html
- /categoriesdda5.html
- /categories48de.html
- /categories1631.html
- /categories44be.html
- /categories0efb.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1661 --> 
Nachbau eines von der niederländische Firma Nelcon gebauten "Automatic Stacking Crane" im Maßstab 1:27.