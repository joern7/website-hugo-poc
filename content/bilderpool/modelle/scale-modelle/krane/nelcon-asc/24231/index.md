---
layout: "image"
title: "asc_6"
date: "2009-06-07T14:43:34"
picture: "nelconasc09.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24231
- /details42fc.html
imported:
- "2019"
_4images_image_id: "24231"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24231 -->
Auf die gleiche Achsen sind auch die Spurkränzen 36331, die zur zweit neben einander in die Rille des ALU-profils laufen.
Auf jeder ecke des Krans 4 stück. Total also 16 stück.