---
layout: "image"
title: "asc_spreader V2_21"
date: "2009-06-07T14:43:36"
picture: "nelconasc55.jpg"
weight: "54"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24277
- /details58bd-3.html
imported:
- "2019"
_4images_image_id: "24277"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24277 -->
Spreader und 40 Fus Container.