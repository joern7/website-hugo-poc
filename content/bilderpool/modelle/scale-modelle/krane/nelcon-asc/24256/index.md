---
layout: "image"
title: "asc_spreader V1_7"
date: "2009-06-07T14:43:35"
picture: "nelconasc34.jpg"
weight: "33"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24256
- /detailsabb7.html
imported:
- "2019"
_4images_image_id: "24256"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:35"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24256 -->
Gekurtsten Schnecke.
Hier sind auch der modificierten Federnocken zu sehen die das Z10 und der Rastkuplung verbindet.