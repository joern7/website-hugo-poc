---
layout: "image"
title: "asc_spreader V2_19"
date: "2009-06-07T14:43:36"
picture: "nelconasc53.jpg"
weight: "52"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24275
- /detailse0ad.html
imported:
- "2019"
_4images_image_id: "24275"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24275 -->
Twistlocks in die verriegelten stelle.
Es fehlt noch einen centrierung von der spreader auf die Container.
Deswegen funktinoniert diesen Twislock aufbau nicht immer: die Rastachsen kommen nicht mitig in das loch der statikwinkelträger und können deswegen nicht drehen.