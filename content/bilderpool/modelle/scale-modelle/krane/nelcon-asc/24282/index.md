---
layout: "image"
title: "asc_containers_4"
date: "2009-06-07T14:43:36"
picture: "nelconasc60.jpg"
weight: "59"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24282
- /details5937.html
imported:
- "2019"
_4images_image_id: "24282"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24282 -->
Wichtig ist das die Löcher auf die Ecken alle in die gleiche lage eingebaut werden.