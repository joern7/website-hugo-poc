---
layout: "image"
title: "Gesamtansicht"
date: "2012-07-03T06:50:30"
picture: "LR_11350_Gesamtansicht_2.jpg"
weight: "11"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/35088
- /details9e34-2.html
imported:
- "2019"
_4images_image_id: "35088"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-07-03T06:50:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35088 -->
Da der Hintergrund unwichtig ist hab ich das Foto entsprechend bearbeitet. Zwei Tage hatte ich die Möglichkeit mein Modell auszustellen und die Funktionen zu zeigen. Noch nie hab ich so lange mit einem Kran aus ft gespielt !
Leider machten die Motore der Hubwerke die Dauerbelastung nicht so mit, wie zu Hause erprobt. Die Leistungskurve ging irgendwie nach unten und ich konnte, trotz vielfachem Motorwechsel, am zweiten Tag nur eingegrenzt arbeiten.