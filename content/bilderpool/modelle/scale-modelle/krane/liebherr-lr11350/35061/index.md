---
layout: "image"
title: "Zusatzballast"
date: "2012-06-15T21:09:19"
picture: "LR_11350_Zusatzballast.jpg"
weight: "9"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/35061
- /details6934-2.html
imported:
- "2019"
_4images_image_id: "35061"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-06-15T21:09:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35061 -->
Dieser Ballast befindet sich auf einem Niederflur Schwerlasttransporter. Diese Art des mobilen Ballasttransportes ist Stand der Technik. Im Modell wurde der Zusatzballast zum Aufrichten des Mastes nicht benötigt, da der Mast nur etwa 2m hoch war und mit dem freischwebenden Ballast, 2kg Masse, hochgehoben werden konnte