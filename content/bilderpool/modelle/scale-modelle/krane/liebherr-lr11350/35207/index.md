---
layout: "image"
title: "Getriebe Antrieb Drekranz"
date: "2012-07-23T19:16:45"
picture: "Getriebe_Oberwagen.jpg"
weight: "23"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/35207
- /details6cd4.html
imported:
- "2019"
_4images_image_id: "35207"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-07-23T19:16:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35207 -->
Der Antrieb ist nichts neues. Wurde schon oft im Kettenfahrzeugantrieb verwendet.