---
layout: "image"
title: "Details 2"
date: "2006-04-14T22:37:04"
picture: "IMG_4452.jpg"
weight: "35"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6121
- /details3692-2.html
imported:
- "2019"
_4images_image_id: "6121"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6121 -->
Ohne die Abfangung in der Mitte des Wippauslegers würde sich dieser gefährlich dürchbiegen obwohl ich an jedem Stoß eines 120er Winkelträgers 30er Laschen für die Zugentlastung habe.