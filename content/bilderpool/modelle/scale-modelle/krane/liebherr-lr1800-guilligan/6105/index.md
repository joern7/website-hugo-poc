---
layout: "image"
title: "Gesamtansicht"
date: "2006-04-14T22:36:19"
picture: "2.jpg"
weight: "20"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6105
- /details07d1-2.html
imported:
- "2019"
_4images_image_id: "6105"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6105 -->
