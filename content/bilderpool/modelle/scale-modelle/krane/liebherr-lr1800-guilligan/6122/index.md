---
layout: "image"
title: "Kran mit Konstrukteur"
date: "2006-04-15T15:33:16"
picture: "IMG_4427.jpg"
weight: "36"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: ["Guilligan", "Kran", "Raupenkran", "Gittermastkran", "Liebherr", "LR1800"]
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6122
- /details8d95.html
imported:
- "2019"
_4images_image_id: "6122"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-15T15:33:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6122 -->
