---
layout: "image"
title: "Drehkranz LR1800 004"
date: "2006-03-05T12:34:12"
picture: "LR1800_004.JPG"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: ["Drehkranz", "Kran", "Raupenkran"]
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/5815
- /detailsd67d.html
imported:
- "2019"
_4images_image_id: "5815"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-03-05T12:34:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5815 -->
