---
layout: "image"
title: "MHC700_9"
date: "2005-03-05T15:29:11"
picture: "MHC700_9.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3717
- /details1e3a.html
imported:
- "2019"
_4images_image_id: "3717"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T15:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3717 -->
Generator oben, Fahrcabine unter.
Der Fan auf der Generator (ein 20er Zahnrad) wird von ein S-Motor angetrieben.