---
layout: "image"
title: "MHC700_2"
date: "2005-03-05T15:17:44"
picture: "MHC700_2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3710
- /detailsae76.html
imported:
- "2019"
_4images_image_id: "3710"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T15:17:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3710 -->
Dieses model ist um die 1,50 hoch.