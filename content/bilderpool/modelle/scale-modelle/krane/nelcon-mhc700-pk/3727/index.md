---
layout: "image"
title: "MHC700_20"
date: "2005-03-05T16:13:52"
picture: "MHC700_20.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3727
- /details79bd-2.html
imported:
- "2019"
_4images_image_id: "3727"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T16:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3727 -->
Der Containergreifer etwas neher.