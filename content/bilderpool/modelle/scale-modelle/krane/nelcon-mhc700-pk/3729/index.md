---
layout: "image"
title: "MHC700_21"
date: "2005-03-05T17:32:34"
picture: "MHC700_21.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3729
- /detailsa995-3.html
imported:
- "2019"
_4images_image_id: "3729"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T17:32:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3729 -->
Der Kübel. Elektrifisiert mit S-Motor und endschalter.