---
layout: "image"
title: "LTR 1220"
date: "2014-08-09T22:30:57"
picture: "P8080070.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39200
- /detailsac6c.html
imported:
- "2019"
_4images_image_id: "39200"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39200 -->
Mijn nieuwste model een Liebherr LTR 1220
Totaal gewicht 8 kg
Voorzien van 10 motoren
2x rij motor
1x spoorbreedte aanpassing motor
1x hef motor
1x zwenk motor
2x motor voor de telescoopmast heffen
2x motor voor het uitschuiven van de mast
1x lier motor