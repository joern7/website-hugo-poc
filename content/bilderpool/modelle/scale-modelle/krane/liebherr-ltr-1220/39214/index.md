---
layout: "image"
title: "Contra gewicht"
date: "2014-08-09T22:30:57"
picture: "P8080065.jpg"
weight: "15"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39214
- /details76f3.html
imported:
- "2019"
_4images_image_id: "39214"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39214 -->
Onder de mast zit een beweegbaar contra gewicht.
Hoe lager de mast hoe verder het gewicht naar achteren gaat