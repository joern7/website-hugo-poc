---
layout: "comment"
hidden: true
title: "19359"
date: "2014-08-10T00:24:04"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Wirklich super, dieses Modell!! 
Gerne weitere Bilder vom Drehkranz, bitte! Und bitte auch von diesen Axiallager, da bin ich gespannt was du da gemacht hast. 
Es ist schwer ein zu schätzen wie gross das Modell ist, es sieht alles so kompakt und klein aus, obwohl ich mich gut mit FT aus kenne :)

Werkelijk super, dit model! Graag nog extra foto's van de draaikrans en die axiaallagers ajb, ik ben erg benieuwd hoe je dat gemaakt hebt! 

Het is lastig in te schatten hoe groot dat model is. Het ziet er zo compact en klein uit, hoewel ik goed bekend ben met FT.
(ow ja, Ruurd, kan jij of een moderator een paar foto's recht zetten die je gepost hebt?)

Mfrgr Marten