---
layout: "image"
title: "Eindelijk klaar"
date: "2014-08-09T22:30:57"
picture: "P8080069.jpg"
weight: "18"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39217
- /detailsf382.html
imported:
- "2019"
_4images_image_id: "39217"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39217 -->
Alleen het elektrische gedeelte zou nog aangesloten moeten worden.
Ben ermee aan het bouwen geweest vanaf Modelbouw show 2014 in Ede
Zaten wat moeilijke  punten in Vooral de telescoopmast hielt me goed bezig.
Ook het hefsysteem draait ook om 4 axiaal lagers (dank nog voor de tip Anton). Deze drukt nu soepel de 8 kg omhoog.
Bij vragen maak ik wel meer foto's