---
layout: "image"
title: "CC4800_1"
date: "2004-02-29T21:26:12"
picture: "TwinringLD_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2237
- /details2e82.html
imported:
- "2019"
_4images_image_id: "2237"
_4images_cat_id: "212"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2237 -->
