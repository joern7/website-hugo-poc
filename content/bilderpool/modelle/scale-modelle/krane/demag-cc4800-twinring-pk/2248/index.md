---
layout: "image"
title: "CC4800_12"
date: "2004-02-29T21:51:53"
picture: "TwinringLD_12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2248
- /detailse0d9-2.html
imported:
- "2019"
_4images_image_id: "2248"
_4images_cat_id: "212"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:51:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2248 -->
