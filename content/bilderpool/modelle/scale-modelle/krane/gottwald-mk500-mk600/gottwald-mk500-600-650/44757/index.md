---
layout: "image"
title: "Gottwald MK500/600/650_2"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44757
- /detailse54f.html
imported:
- "2019"
_4images_image_id: "44757"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44757 -->
In diese etwas größeren Gegegengewicht hab ich ein steuerung eingebaut.