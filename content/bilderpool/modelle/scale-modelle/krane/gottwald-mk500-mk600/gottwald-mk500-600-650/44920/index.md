---
layout: "image"
title: "Gottwald MK500/600/650_7"
date: "2016-12-26T17:11:47"
picture: "PICT0139.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44920
- /details1dde.html
imported:
- "2019"
_4images_image_id: "44920"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-12-26T17:11:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44920 -->
Sol Expert ER612.  Conrad art: 249468
Links der Anschluss vom Emfänger, rechts die beide Motor Anschlussen, oben die Anschlussen für Brems- und Rückfahrleuchten.