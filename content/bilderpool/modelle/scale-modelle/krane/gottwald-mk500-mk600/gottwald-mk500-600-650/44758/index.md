---
layout: "image"
title: "Gottwald MK500/600/650_3"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44758
- /detailse6c7-2.html
imported:
- "2019"
_4images_image_id: "44758"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44758 -->
Der IR Emfänger.