---
layout: "image"
title: "MK650 Sarens_7"
date: "2016-11-13T15:14:19"
picture: "mksarens7.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44755
- /details051f.html
imported:
- "2019"
_4images_image_id: "44755"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44755 -->
In Transportstellung.