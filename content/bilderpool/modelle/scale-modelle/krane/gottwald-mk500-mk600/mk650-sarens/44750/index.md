---
layout: "image"
title: "MK650 Sarens_2"
date: "2016-11-13T15:14:19"
picture: "mksarens2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44750
- /details8ad9.html
imported:
- "2019"
_4images_image_id: "44750"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44750 -->
Als Sattelugmaschine wurde ein angepasstes 6 Achsiges MOL Kranträgerfahrgestell eingesetzt.