---
layout: "overview"
title: "MK650 Sarens"
date: 2020-02-22T08:23:50+01:00
legacy_id:
- /php/categories/3334
- /categoriesadd0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3334 --> 
Modell der in 1972 an die belgische Firma Saren gelieferte MK650 in Maßstab 1:27.