---
layout: "image"
title: "MK650 Sarens_4"
date: "2016-11-13T15:14:19"
picture: "mksarens4.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44752
- /detailsca82.html
imported:
- "2019"
_4images_image_id: "44752"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44752 -->
Bei die 3 andere 650er könte ich als träger noch 2x 390mm Alu's einbauen.
Bei der Sarens Kran muste ich mir etwas anders einfallen lassen.