---
layout: "overview"
title: "MK650 Stoof"
date: 2020-02-22T08:23:49+01:00
legacy_id:
- /php/categories/3333
- /categories280d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3333 --> 
Modell der in 1972 an die niederländische Firma Stoof gelieferte MK650 in Maßstab 1:27.