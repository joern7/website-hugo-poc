---
layout: "image"
title: "MK650 Stoof_3"
date: "2016-11-13T15:14:18"
picture: "mkstoof3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44745
- /details0f6f-3.html
imported:
- "2019"
_4images_image_id: "44745"
_4images_cat_id: "3333"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44745 -->
Hintere Tiefladerfahrgestel mit jetzt 4 Achslienen mit je 4 Räder.
Das Gegengewicht wurde auf 135t gesteigert.