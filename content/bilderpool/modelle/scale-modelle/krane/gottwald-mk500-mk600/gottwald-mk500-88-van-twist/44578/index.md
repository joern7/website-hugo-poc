---
layout: "image"
title: "MK500-88 van Twist_2"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44578
- /details36db.html
imported:
- "2019"
_4images_image_id: "44578"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44578 -->
Charakteristisch an diesen Kran war der unter/zwischen der Auslegerfus angebaute Kabine.