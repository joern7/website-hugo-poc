---
layout: "image"
title: "MK500-88 van Twist_7"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44583
- /details79f3.html
imported:
- "2019"
_4images_image_id: "44583"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44583 -->
Die drei Seilwinden und untere Umlenkrollen.
Für das Auslegerverstelwerk wurde ein doppelte Seilwinde benutst.
Da der Kran ein Transportgewicht von über 90t auf die Waage brachte, wurde der oberwagen geteilt.
Das Heckteil mit die 3 Seilwinden und der Aufrichtebock wurden getrent verfahren.
Die beide Scharnieren sollen die obere verbolzung darstellen.