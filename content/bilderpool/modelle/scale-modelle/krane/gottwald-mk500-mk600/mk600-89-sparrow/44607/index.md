---
layout: "image"
title: "MK600-89 Sparrow_8"
date: "2016-10-17T17:40:21"
picture: "mksparrow08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44607
- /details2b76.html
imported:
- "2019"
_4images_image_id: "44607"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44607 -->
Rollenkopf.