---
layout: "image"
title: "MK600-89 Sparrow_4"
date: "2016-10-17T17:40:21"
picture: "mksparrow04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44603
- /details5841.html
imported:
- "2019"
_4images_image_id: "44603"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44603 -->
Der Sparrows Kran von links hinten.