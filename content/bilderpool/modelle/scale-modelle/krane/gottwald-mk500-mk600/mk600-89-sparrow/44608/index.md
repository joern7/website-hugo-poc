---
layout: "image"
title: "MK600-89 Sparrow_9"
date: "2016-10-17T17:40:21"
picture: "mksparrow09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44608
- /details2ce5.html
imported:
- "2019"
_4images_image_id: "44608"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44608 -->
Drei Seilwinden.