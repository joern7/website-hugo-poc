---
layout: "image"
title: "MK600-89 Sparrow_2"
date: "2016-10-17T17:40:21"
picture: "mksparrow02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44601
- /details61bf.html
imported:
- "2019"
_4images_image_id: "44601"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44601 -->
Auf grund der eingeschränkten Sichtverhältnisse wurde bei die MK600 ein zusätzliche Kabine montiert.
