---
layout: "overview"
title: "MK600-89 Sparrow"
date: 2020-02-22T08:23:45+01:00
legacy_id:
- /php/categories/3320
- /categoriesbd08.html
- /categoriesa3cf.html
- /categories5ee8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3320 --> 
Modell der in 1971 an die Britische Firma Sparrow gelieferten 500t MK600-89 in Maßstab 1:27.