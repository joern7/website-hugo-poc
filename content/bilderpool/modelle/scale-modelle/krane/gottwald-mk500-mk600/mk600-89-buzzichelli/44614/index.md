---
layout: "image"
title: "MK600-89 Buzzichelli_5"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44614
- /details6a60.html
imported:
- "2019"
_4images_image_id: "44614"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44614 -->
Gegengewicht.