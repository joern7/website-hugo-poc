---
layout: "overview"
title: "MK600-89 Buzzichelli"
date: 2020-02-22T08:23:46+01:00
legacy_id:
- /php/categories/3321
- /categories72cc.html
- /categories2a4f.html
- /categoriesf9f1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3321 --> 
Modell der in 1972 an der Französische Firma Buzzichelli gelieferte 500t MK600-89.