---
layout: "image"
title: "MK600-89 Buzzichelli_8"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44617
- /details878c.html
imported:
- "2019"
_4images_image_id: "44617"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44617 -->
Aus der Hubschrauber perspektive.