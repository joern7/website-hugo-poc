---
layout: "image"
title: "MK600-89 Buzzichelli_4"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44613
- /details58ec.html
imported:
- "2019"
_4images_image_id: "44613"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44613 -->
Der Buzzichelli Kran von links hinten.
