---
layout: "image"
title: "MK600-89 Buzzichelli_3"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44612
- /details6571-2.html
imported:
- "2019"
_4images_image_id: "44612"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44612 -->
Der MK600-89 bekam einem 3,5m breiten Farhgestel die auch 500 länger war als der MK500-88.