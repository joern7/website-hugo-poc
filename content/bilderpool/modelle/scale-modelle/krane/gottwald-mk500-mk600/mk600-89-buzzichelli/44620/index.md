---
layout: "image"
title: "MK600-89 Buzzichelli_11"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli11.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44620
- /detailsfba5.html
imported:
- "2019"
_4images_image_id: "44620"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44620 -->
2 eingepreste, bearbeiteten 14er Räder, redusieren das 12mm loch auf 4mm.