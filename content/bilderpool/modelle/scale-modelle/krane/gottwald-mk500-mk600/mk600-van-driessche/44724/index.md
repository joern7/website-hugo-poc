---
layout: "image"
title: "MK600 van Driessche_9"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44724
- /details65f2.html
imported:
- "2019"
_4images_image_id: "44724"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44724 -->
Krantopf mit die 4 Abstützarme, beide Fahrgestellen und der Mack.
