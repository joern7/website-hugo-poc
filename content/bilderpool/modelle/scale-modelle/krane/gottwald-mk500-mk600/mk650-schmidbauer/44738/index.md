---
layout: "image"
title: "MK650 Schmidbauer_10"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44738
- /details4be2.html
imported:
- "2019"
_4images_image_id: "44738"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44738 -->
Von oben.
Auf die Straße durfte diesen Kran so nicht verfahren worden: das Heckteil mit Seilwinden und der A-bock wurden getrennt transportiert.