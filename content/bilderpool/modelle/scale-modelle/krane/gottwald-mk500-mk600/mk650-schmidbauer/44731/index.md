---
layout: "image"
title: "MK650 Schmidbauer_3"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44731
- /details2b09-2.html
imported:
- "2019"
_4images_image_id: "44731"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44731 -->
Um biegen der Kranoberwagen entgegen zu treten, hab ich 2 435mm Achsen in die beide Längsträger ein gesteckt.