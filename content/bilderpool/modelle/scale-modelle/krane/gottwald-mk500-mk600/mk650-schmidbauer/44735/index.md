---
layout: "image"
title: "MK650 Schmidbauer_7"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44735
- /details2c1b.html
imported:
- "2019"
_4images_image_id: "44735"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44735 -->
Alle MK500/600/650 Kranen könten mit 2 einselnen Haken ausgestatet worden.
Die beide könten aber auch mechanisch zur eine doppelhaken gekuppeld worden.