---
layout: "image"
title: "MK650 Schmidbauer_5"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44733
- /detailsb823.html
imported:
- "2019"
_4images_image_id: "44733"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44733 -->
Auch bei diesen Kran: 125t Ballast.