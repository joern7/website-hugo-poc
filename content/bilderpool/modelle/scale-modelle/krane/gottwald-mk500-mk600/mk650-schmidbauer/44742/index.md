---
layout: "image"
title: "MK650 Schmidbauer_14"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer14.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44742
- /details2b39.html
imported:
- "2019"
_4images_image_id: "44742"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44742 -->
Der Kran wurde zwischen 2 Tiefladerfahrgestellen angebolzt .