---
layout: "overview"
title: "MK650 Schmidbauer"
date: 2020-02-22T08:23:48+01:00
legacy_id:
- /php/categories/3332
- /categories8de3.html
- /categories7041.html
- /categories90f9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3332 --> 
Modell der in 1972 an die Deutsche Firma Schmidbauer gelieferte MK650.