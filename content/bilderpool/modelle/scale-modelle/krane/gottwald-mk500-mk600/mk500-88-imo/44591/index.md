---
layout: "image"
title: "MK500-88 IMO_4"
date: "2016-10-16T16:02:29"
picture: "mkimo4.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44591
- /detailseadf-2.html
imported:
- "2019"
_4images_image_id: "44591"
_4images_cat_id: "3319"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44591 -->
Ohne Heckteil.