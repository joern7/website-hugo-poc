---
layout: "image"
title: "basic truck"
date: "2014-01-24T01:01:20"
picture: "LG2.jpg"
weight: "3"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/38119
- /details36e4.html
imported:
- "2019"
_4images_image_id: "38119"
_4images_cat_id: "2836"
_4images_user_id: "541"
_4images_image_date: "2014-01-24T01:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38119 -->
The outriggers are so strong that they carry the compleet weight of the crane