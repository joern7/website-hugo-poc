---
layout: "image"
title: "Jib"
date: "2012-02-26T12:53:07"
picture: "terex_015.jpg"
weight: "13"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34409
- /details4faa.html
imported:
- "2019"
_4images_image_id: "34409"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34409 -->
Mast voorzien van jib. Mast is 1,5 m lang. En kan gewoon op en neer bewogen met de hefmotor op de langste stand zelfs met dit gewicht eraan. (gaat niet van harte maar het gaat wel)