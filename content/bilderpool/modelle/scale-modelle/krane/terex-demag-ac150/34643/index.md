---
layout: "image"
title: "Terex Demag AC150"
date: "2012-03-12T17:26:59"
picture: "terex_026.jpg"
weight: "25"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34643
- /details9b07.html
imported:
- "2019"
_4images_image_id: "34643"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-03-12T17:26:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34643 -->
Kleine houder gemaakt om een bijna volle fles te tillen