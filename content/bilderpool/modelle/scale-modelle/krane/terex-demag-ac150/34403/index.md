---
layout: "image"
title: "hefsysteem"
date: "2012-02-26T12:53:07"
picture: "terex_009.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34403
- /detailsbcd7.html
imported:
- "2019"
_4images_image_id: "34403"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34403 -->
Aandrijving van de spindels alles voorzien van kogellager. Dit heeft me veel tijd gekost voordat me het goed genoeg en snel genoeg omhoog ging.