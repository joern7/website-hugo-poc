---
layout: "image"
title: "Terex Demag AC150"
date: "2012-03-12T17:26:59"
picture: "terex_030.jpg"
weight: "28"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34646
- /details4ef7.html
imported:
- "2019"
_4images_image_id: "34646"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-03-12T17:26:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34646 -->
Klein probleem!

Dacht dat de draadeinden krom waren maar het bleek dat omdat de mast rechtop stond dat ze alleen nog maar in de moeren zaten. En dus geen ondersteuning hadden in de Alu's. 

Dus uiteindelijk geen schade.