---
layout: "image"
title: "Terex Demag AC150"
date: "2012-03-12T17:26:58"
picture: "terex.jpg"
weight: "24"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34642
- /detailsbea0-3.html
imported:
- "2019"
_4images_image_id: "34642"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-03-12T17:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34642 -->
Gewicht om mijn kraan arm eens te testen