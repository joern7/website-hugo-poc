---
layout: "image"
title: "Antrieb Hinterachse"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx25.jpg"
weight: "26"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45071
- /details839a-2.html
imported:
- "2019"
_4images_image_id: "45071"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45071 -->
Die Achse besteht aus 2 Differentiale mit ein Mittlere Hauptachse (ähnlich wie den Terex vom Peter Krijnen: http://www.ftcommunity.de/details.php?image_id=3927). Auf die 2 Differential steckt ein Z15 Zahnrad der durch ein Schnecke betreiben werd (Spezial Teile Andres Tacke).

Die ganze Hinterachse ist auf 6 stuck Schwarze fischertechnik Federn gelagert.