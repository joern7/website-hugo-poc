---
layout: "image"
title: "Antrieb Lenkung rechts"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx29.jpg"
weight: "30"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45075
- /details4005-3.html
imported:
- "2019"
_4images_image_id: "45075"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45075 -->
Jeder Achse ist mit 8 schwarzen fischertechnik Federn befestigt.