---
layout: "image"
title: "Antrieb Lenkung links"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx28.jpg"
weight: "29"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45074
- /details07b2-2.html
imported:
- "2019"
_4images_image_id: "45074"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45074 -->
Ja das war schon eine Tufflerei!

Hauptsachlich besteht die Hauptachse aus einem Rechteck aus Aluminium. Ins Mitte eine Achse mit Differential. An beiden Seiten von der Achse habe ich Bausteine mit Loch eingebaut für die Drehpunkte (4 stuck 90 Grad Winkelachsen). Am Ende habe ich dann eine Bauplatte 45x45 verwendet. In diese Grundplatte habe ich dann ein Zimmer gedreht für so ein kleines Kugellager damit die Achse gelagert ist.

Die Bruder Reifen habe ich ein wenig ausgedreht damit dass Drehpunkt etwas naher in die Mitte die Reife kommt. Mehr als 10mm hat das aber nicht gebracht (leider). Dann habe ich noch ein 4mm Loch durch die Reifen gebohrt und eine Antriebsachse durchgesteckt. Am Ende ein klemmbares Z10 Ritzel drauf gedreht und ein Kardangelenk gebraucht für die Lenkung.

Am Wagenrechte Aluminium Baustein vom Hauptachse habe ich dann ein verschiebbares Aluminium gemacht (4 stuck Kettenglied einzeln in die Nutten von die Aluminium Bausteine geschoben, damit das kleine Aluminium Baustein nach links und rechts bewegen kann). Dies ist dann die Lenkstange geworden. Auf dieses Aluminium Baustein habe ich dann Zahnstange montiert, so dass die nicht mehr verschieben können. Die erste Lenkung wird durch ein Z15 Zahnrad betreiben und den zweite Lenkung durch ein Z10 Zahnrad. Damit haben die 2 Lenkachsen unterschiedlichen Lenkausschlag. Beide Zahnräder sind mit einander verbunden mittels Kardangelenke. Der Lenkstange wird mittels einer Pollin Motor (ich glaube etwas 1:700) angetrieben.