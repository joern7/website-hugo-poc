---
layout: "image"
title: "Abrollkipper Kabine"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx03.jpg"
weight: "4"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45049
- /detailsc170.html
imported:
- "2019"
_4images_image_id: "45049"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45049 -->
Die Kabine hat als Grundplatte den Schwarzen platte von den 500 Kasten. Die Sitze sind aufgebaut aus Graue 30 und 15 mm Bausteine. Die sind bestückt mit weiße und schwarze Bauplatten. Weil ich Bauplane von einem Volvo FH Kabine gebraucht habe, ist auch ein Bett eingebaut. Dies soll normalerweise nicht in eine Kabine von einem Abrollkipper eingebaut sein.