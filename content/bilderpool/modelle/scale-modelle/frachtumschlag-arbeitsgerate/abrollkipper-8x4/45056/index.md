---
layout: "image"
title: "Abrollkipper Mulde hub"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx10.jpg"
weight: "11"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45056
- /detailsc27e.html
imported:
- "2019"
_4images_image_id: "45056"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45056 -->
Wann die Mulde nach hinten geschoben ist, werden die 2 Power Motoren gestartet der die Hubzylinder antreiben. Der Mulde fangt an zu kippen.