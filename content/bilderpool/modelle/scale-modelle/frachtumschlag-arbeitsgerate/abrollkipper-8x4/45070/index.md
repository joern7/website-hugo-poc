---
layout: "image"
title: "Antrieb"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx24.jpg"
weight: "25"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45070
- /details5e95.html
imported:
- "2019"
_4images_image_id: "45070"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45070 -->
Der LKW wird durch 2 8:1 power Motoren betrieben mittels Schnecken auf Differentiale. Weil durch viel Kraft die Differentiale durchrutschen, habe ich 3 stuck Differentiale neben einander verwendet. Die 3 Zahnräder von den Differentialen sind miteinander zusammen gesteckt wie ein Drei-eck. Der 2 äußerste werden durch die Motoren angetrieben. Beide greifen an und die Mittlere werd dann angetrieben. Die Achsen habe ich dann an beide Seiten mit so ein Sonderteil von Andreas Tacke 3x Z15 Zahnrad festgeschraubt. Damit werden 3 Differentiale eigentlich "eine".

Auf eine die ausgehende Achse habe ich dann Kardangelenke geschraubt für die weitere Antrieb.