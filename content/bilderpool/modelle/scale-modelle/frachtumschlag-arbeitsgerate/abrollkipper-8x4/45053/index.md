---
layout: "image"
title: "Abrollkipper Heck"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx07.jpg"
weight: "8"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45053
- /detailsf673.html
imported:
- "2019"
_4images_image_id: "45053"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45053 -->
