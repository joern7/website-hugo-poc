---
layout: "image"
title: "Mulde 3"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx20.jpg"
weight: "21"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45066
- /details14a4.html
imported:
- "2019"
_4images_image_id: "45066"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45066 -->
