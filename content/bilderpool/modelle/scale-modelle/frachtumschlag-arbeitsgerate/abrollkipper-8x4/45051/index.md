---
layout: "image"
title: "Abrollkipper Dashboard"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx05.jpg"
weight: "6"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45051
- /details9369.html
imported:
- "2019"
_4images_image_id: "45051"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45051 -->
Hier ein blick auf das Dashboard. Ich werde noch Sticker machen für die Geschwindigkeits und Drehzahlmesser und die Tasten.