---
layout: "image"
title: "Abrollkipper rechter Seite Vorne"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx02.jpg"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45048
- /details4386.html
imported:
- "2019"
_4images_image_id: "45048"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45048 -->
Das Dach ist fertig mit LED Lichte. Vielleicht werde ich die Kotflügel noch überarbeiten.