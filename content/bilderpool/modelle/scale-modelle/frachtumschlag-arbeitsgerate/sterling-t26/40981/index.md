---
layout: "image"
title: "Sterling T26"
date: "2015-05-16T19:01:51"
picture: "P5160126.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/40981
- /detailsa9fa-2.html
imported:
- "2019"
_4images_image_id: "40981"
_4images_cat_id: "3078"
_4images_user_id: "838"
_4images_image_date: "2015-05-16T19:01:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40981 -->
Mijn nieuwste model een oud legervoertuig uit de jaren 60-70