---
layout: "image"
title: "Reachstacker at Modelshow europa 2008"
date: "2008-05-12T21:02:06"
picture: "bemmel7.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/14507
- /details8bbb-2.html
imported:
- "2019"
_4images_image_id: "14507"
_4images_cat_id: "1279"
_4images_user_id: "162"
_4images_image_date: "2008-05-12T21:02:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14507 -->
