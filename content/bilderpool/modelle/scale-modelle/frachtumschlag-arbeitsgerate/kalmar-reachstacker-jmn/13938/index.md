---
layout: "image"
title: "40ft container"
date: "2008-03-17T07:24:29"
picture: "kalmarreackstacker8.jpg"
weight: "8"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/13938
- /details68a3.html
imported:
- "2019"
_4images_image_id: "13938"
_4images_cat_id: "1279"
_4images_user_id: "162"
_4images_image_date: "2008-03-17T07:24:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13938 -->
