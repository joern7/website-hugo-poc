---
layout: "image"
title: "Reachstacker"
date: "2008-03-17T07:24:28"
picture: "kalmarreackstacker1.jpg"
weight: "1"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/13931
- /detailsf67b.html
imported:
- "2019"
_4images_image_id: "13931"
_4images_cat_id: "1279"
_4images_user_id: "162"
_4images_image_date: "2008-03-17T07:24:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13931 -->
Hier habe ich versucht eine Reachstacker nach zu bauen.
insgesammt eingebaut:

2x 50:1 Motore fur die Antreib
1x 780(?):1 Motor fur Lenkung (Polin motor)
1x M-motor fur die Antrieb der Kabine
2x 125:1 Motore fur der Ausleger nach hoch und unten zu lasse
1x 50:1 Motor furs ausschieben der Ausleger
2x S-motor fur die verdrehung von container handler
2x S-motor fur positionirung (link-rechts) von container handler
2x S-motor fur ausschieben von 20ft nach 40ft container

Dann soll noch dazu kommen:

2x S-motor fur greifen die Containers
1x S-motor oder Powermotor fur das Schaukeln (vorne nach hintern) von container handler
1 oder 2 S-motor furs Schaukeln (links-rechts) von container handler

Ob ich die letzte 2 moglichkeiten motorisiert bekommen ist noch im frage. Es gibt kaum noch platz