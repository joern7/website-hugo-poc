---
layout: "image"
title: "Ausleger_1"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger131.jpg"
weight: "131"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27190
- /details32d7-2.html
imported:
- "2019"
_4images_image_id: "27190"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27190 -->
Draufsicht auf der Baggerasleger.
Die fürung für das Band ist hier zu sehen.