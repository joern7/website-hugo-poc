---
layout: "image"
title: "Frame des Abgabegarät"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger097.jpg"
weight: "97"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27156
- /detailsd75a-3.html
imported:
- "2019"
_4images_image_id: "27156"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27156 -->
Zur steuering der beide Raupen dient ein greifstangen-getriebe #32342