---
layout: "image"
title: "Abgabegerät_4"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger085.jpg"
weight: "85"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27144
- /details9e28-2.html
imported:
- "2019"
_4images_image_id: "27144"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27144 -->
Hier ist der Vertikale vorschub von Band 5 zu sehen.