---
layout: "image"
title: "Raupenschiffen 10"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger113.jpg"
weight: "113"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27172
- /details388f.html
imported:
- "2019"
_4images_image_id: "27172"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "113"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27172 -->
