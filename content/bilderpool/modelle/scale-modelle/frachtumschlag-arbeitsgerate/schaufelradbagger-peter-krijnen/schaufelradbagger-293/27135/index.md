---
layout: "image"
title: "Drehkranze 2_1"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger076.jpg"
weight: "76"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27135
- /details474d-2.html
imported:
- "2019"
_4images_image_id: "27135"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27135 -->
Drehkranz zur auflage der Baggerbrücke