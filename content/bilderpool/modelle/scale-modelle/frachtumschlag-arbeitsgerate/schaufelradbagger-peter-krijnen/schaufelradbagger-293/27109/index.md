---
layout: "image"
title: "Baggerbrücke_2"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger050.jpg"
weight: "50"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27109
- /detailsac44.html
imported:
- "2019"
_4images_image_id: "27109"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27109 -->
