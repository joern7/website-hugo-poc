---
layout: "image"
title: "Abgabegerät_3"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger084.jpg"
weight: "84"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27143
- /detailsf017.html
imported:
- "2019"
_4images_image_id: "27143"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27143 -->
