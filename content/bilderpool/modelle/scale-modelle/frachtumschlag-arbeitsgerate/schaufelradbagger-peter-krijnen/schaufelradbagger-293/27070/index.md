---
layout: "image"
title: "Hauptgerät_7"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger011.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27070
- /detailsa5c1.html
imported:
- "2019"
_4images_image_id: "27070"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27070 -->
