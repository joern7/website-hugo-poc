---
layout: "image"
title: "Antrieben der Bänder 2 und 3"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger066.jpg"
weight: "66"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27125
- /detailsac81.html
imported:
- "2019"
_4images_image_id: "27125"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27125 -->
