---
layout: "image"
title: "Baggerbrücke_5"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger053.jpg"
weight: "53"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27112
- /details1abd-3.html
imported:
- "2019"
_4images_image_id: "27112"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27112 -->
