---
layout: "image"
title: "Frame 1"
date: "2010-05-08T20:05:16"
picture: "schaufelradbagger003.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27062
- /detailsa18d-2.html
imported:
- "2019"
_4images_image_id: "27062"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27062 -->
Das Frame: hier auf war der Drekranz aufgebaut.