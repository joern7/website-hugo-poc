---
layout: "image"
title: "Hauptgerät_4"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger008.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27067
- /detailse6fc.html
imported:
- "2019"
_4images_image_id: "27067"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27067 -->
Antrieb der Kran 3.
Antrieb mittels Conrad Powermotor 312:1
Anschlußplatinne der steuercentrale