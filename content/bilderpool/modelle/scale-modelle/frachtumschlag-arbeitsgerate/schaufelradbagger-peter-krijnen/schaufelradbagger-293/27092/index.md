---
layout: "image"
title: "Steuercentrale_4"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger033.jpg"
weight: "33"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27092
- /details39f4.html
imported:
- "2019"
_4images_image_id: "27092"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27092 -->
