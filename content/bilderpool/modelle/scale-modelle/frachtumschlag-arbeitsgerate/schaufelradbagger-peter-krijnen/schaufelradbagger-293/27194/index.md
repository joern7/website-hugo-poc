---
layout: "image"
title: "Ausleger_5"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger135.jpg"
weight: "135"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27194
- /detailsbf68-3.html
imported:
- "2019"
_4images_image_id: "27194"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "135"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27194 -->
