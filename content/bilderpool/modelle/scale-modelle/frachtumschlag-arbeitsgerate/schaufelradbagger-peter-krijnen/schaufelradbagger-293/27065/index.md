---
layout: "image"
title: "Hauptgerät_2"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger006.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27065
- /details9de3.html
imported:
- "2019"
_4images_image_id: "27065"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27065 -->
Aufliegerkupplung Oberteil #31264, werd hier als unterteil benutst, damit der abgabebrücke hierauf aufgelegen werden kan.