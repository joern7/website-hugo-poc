---
layout: "image"
title: "Kran 3-5"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger024.jpg"
weight: "24"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27083
- /details7180-2.html
imported:
- "2019"
_4images_image_id: "27083"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27083 -->
