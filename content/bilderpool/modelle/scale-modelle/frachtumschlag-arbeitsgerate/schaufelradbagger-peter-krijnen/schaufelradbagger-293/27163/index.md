---
layout: "image"
title: "Raupenschiffen 2"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger104.jpg"
weight: "104"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27163
- /details213e-2.html
imported:
- "2019"
_4images_image_id: "27163"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "104"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27163 -->
8 Raupenschiffen neben einander.
Oberhalb der vierte Raupen von links ist der M-Motor zu sehen.