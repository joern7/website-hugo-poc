---
layout: "image"
title: "Rote Bausteinen 3"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger127.jpg"
weight: "127"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27186
- /details6c7b.html
imported:
- "2019"
_4images_image_id: "27186"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "127"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27186 -->
38240 - 42x
38423 - 122x