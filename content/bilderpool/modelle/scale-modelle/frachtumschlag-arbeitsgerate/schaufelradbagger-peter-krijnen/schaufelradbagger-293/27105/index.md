---
layout: "image"
title: "Kran 1-2"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger046.jpg"
weight: "46"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27105
- /details9c68-3.html
imported:
- "2019"
_4images_image_id: "27105"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27105 -->
Lagerung und antreib der Kran 1