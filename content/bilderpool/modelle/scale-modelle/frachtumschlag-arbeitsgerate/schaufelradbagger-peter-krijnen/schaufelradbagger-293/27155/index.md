---
layout: "image"
title: "Band vorschub"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger096.jpg"
weight: "96"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27155
- /details6ef3.html
imported:
- "2019"
_4images_image_id: "27155"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27155 -->
Horizontaler vorschub der Bandschleive 5. Damit der abgabe auf das Grubenband stat findet und nicht daneben.