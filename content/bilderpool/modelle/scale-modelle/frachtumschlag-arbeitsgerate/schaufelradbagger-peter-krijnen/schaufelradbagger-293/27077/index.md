---
layout: "image"
title: "Kran 2_6"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger018.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27077
- /details168b-2.html
imported:
- "2019"
_4images_image_id: "27077"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27077 -->
