---
layout: "image"
title: "Raupenschiffen 6"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger109.jpg"
weight: "109"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27168
- /detailsab08.html
imported:
- "2019"
_4images_image_id: "27168"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "109"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27168 -->
In Clubheft 1/77 steht auf Seite 11 als Tip der aufbau von breitere Raupen