---
layout: "image"
title: "Seilen_4"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger029.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27088
- /details27d0.html
imported:
- "2019"
_4images_image_id: "27088"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27088 -->
2 x 8 Seilrollen 23