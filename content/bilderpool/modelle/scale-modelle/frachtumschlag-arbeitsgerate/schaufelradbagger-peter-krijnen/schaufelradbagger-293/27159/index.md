---
layout: "image"
title: "Frame des Abgabegarät_4"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger100.jpg"
weight: "100"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27159
- /detailsb778-3.html
imported:
- "2019"
_4images_image_id: "27159"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27159 -->
Die enden der Raupensteuerframen gleiten zwischen zwei Alu's