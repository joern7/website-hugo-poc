---
layout: "image"
title: "Vooraanzicht"
date: "2011-03-07T19:27:09"
picture: "pivot_008.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30225
- /details833a.html
imported:
- "2019"
_4images_image_id: "30225"
_4images_cat_id: "2245"
_4images_user_id: "838"
_4images_image_date: "2011-03-07T19:27:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30225 -->
