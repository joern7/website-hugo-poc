---
layout: "image"
title: "Smart Fortwo 5"
date: "2007-01-07T17:53:02"
picture: "Smart_Fortwo_07.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8326
- /details2b43-2.html
imported:
- "2019"
_4images_image_id: "8326"
_4images_cat_id: "766"
_4images_user_id: "328"
_4images_image_date: "2007-01-07T17:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8326 -->
In der Draufsicht durch das Glasdach erkennt man, dass der Beifahrersitz im Vergleich zum Fahrersitz wie beim echten Fortwo weiter hinten positioniert ist, um durch mehr Schulterfreiheit der Insassen deren Komfort zu erhöhen.