---
layout: "image"
title: "Smart Fortwo 8"
date: "2007-01-07T17:53:02"
picture: "Smart_Fortwo_13.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8329
- /detailsb0ef.html
imported:
- "2019"
_4images_image_id: "8329"
_4images_cat_id: "766"
_4images_user_id: "328"
_4images_image_date: "2007-01-07T17:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8329 -->
Der doppelte Unterboden des Smart Fortwo.