---
layout: "image"
title: "Der Radlader mit gefüllter Schaufel"
date: "2011-08-29T10:16:37"
picture: "catg01.jpg"
weight: "1"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31671
- /detailsfaeb.html
imported:
- "2019"
_4images_image_id: "31671"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31671 -->
Mein erster richtiger Radlader!
- 4,7 kg
- 5 Pneumatik Zylinder
- 2 Powermotoren zum Antrieb