---
layout: "image"
title: "Radlader hinten rechts"
date: "2011-08-29T10:16:37"
picture: "catg08.jpg"
weight: "8"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31678
- /details939d.html
imported:
- "2019"
_4images_image_id: "31678"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31678 -->
Leider konnte ich den Radlader nicht mehr von hinten verkleiden, da er so schon sehr sehr schwer war, dass hätte nicht gehalten.