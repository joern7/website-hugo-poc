---
layout: "image"
title: "Baggerfahrer"
date: "2011-08-29T10:16:37"
picture: "catg14.jpg"
weight: "14"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31684
- /details6e46.html
imported:
- "2019"
_4images_image_id: "31684"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31684 -->
Wie herrlich muss es sein, so eine Maschine zu steuern :)