---
layout: "image"
title: "Deteils"
date: "2011-08-29T10:16:38"
picture: "catg26.jpg"
weight: "26"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31696
- /details9459-2.html
imported:
- "2019"
_4images_image_id: "31696"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31696 -->
"Licht mach das Modell lebendig", die vorderen Scheinwerfer.