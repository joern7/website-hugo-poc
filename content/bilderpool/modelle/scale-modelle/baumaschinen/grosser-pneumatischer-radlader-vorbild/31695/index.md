---
layout: "image"
title: "Pneumatik"
date: "2011-08-29T10:16:38"
picture: "catg25.jpg"
weight: "25"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31695
- /detailsef94.html
imported:
- "2019"
_4images_image_id: "31695"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31695 -->
Der Zylinder bewegt den Hubarm, auf der anderen Seite sitzt sein Partner.