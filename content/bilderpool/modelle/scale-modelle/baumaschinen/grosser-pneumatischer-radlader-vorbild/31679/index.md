---
layout: "image"
title: "Der Radlader von oben"
date: "2011-08-29T10:16:37"
picture: "catg09.jpg"
weight: "9"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31679
- /details15b5.html
imported:
- "2019"
_4images_image_id: "31679"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31679 -->
Natürlich wird die Baumaschine durch eine Knicklenkung gelenkt.