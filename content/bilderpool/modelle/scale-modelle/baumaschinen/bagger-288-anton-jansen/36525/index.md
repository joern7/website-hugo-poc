---
layout: "image"
title: "bagger288.5"
date: "2013-01-26T22:29:42"
picture: "bagger-288.5_2.jpg"
weight: "18"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/36525
- /details0230.html
imported:
- "2019"
_4images_image_id: "36525"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2013-01-26T22:29:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36525 -->
