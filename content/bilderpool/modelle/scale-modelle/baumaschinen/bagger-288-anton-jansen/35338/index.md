---
layout: "image"
title: "bagger9"
date: "2012-08-15T17:21:05"
picture: "bagger288-9.jpg"
weight: "8"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/35338
- /detailsc8c0-2.html
imported:
- "2019"
_4images_image_id: "35338"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-15T17:21:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35338 -->
