---
layout: "image"
title: "bagger 288-1"
date: "2012-08-14T07:20:03"
picture: "bagger_288-2.jpg"
weight: "1"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/35331
- /detailsc1f6.html
imported:
- "2019"
_4images_image_id: "35331"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-14T07:20:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35331 -->
Die erste Bilder