---
layout: "image"
title: "bagger7"
date: "2012-08-15T17:21:05"
picture: "bagger288-7.jpg"
weight: "6"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/35336
- /details59ef-2.html
imported:
- "2019"
_4images_image_id: "35336"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-15T17:21:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35336 -->
