---
layout: "image"
title: "Caterpillar Motorgrade 24H cylinder connection"
date: "2011-04-02T23:50:37"
picture: "caterpillarmotorgradeh13.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30387
- /detailse3f6.html
imported:
- "2019"
_4images_image_id: "30387"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30387 -->
Damit die Cylinder alle bewegungen folgen kann habe ich ein kreuzkupplung gemacht. Die wird fest gehalten mit eine Gerandelte Achse 50.