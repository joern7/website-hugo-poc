---
layout: "image"
title: "Caterpillar Motorgrade 24H turntable"
date: "2011-04-02T13:57:28"
picture: "caterpillarmotorgradeh09.jpg"
weight: "11"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30383
- /details8197.html
imported:
- "2019"
_4images_image_id: "30383"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T13:57:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30383 -->
Mit Statikteile und achsen habe ich dieses Teil stark gemacht. Mit die Achsen dadurch gibt es gute moglichkeiten Teile an zu bauen.