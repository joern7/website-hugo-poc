---
layout: "image"
title: "Motorraum"
date: "2011-08-03T07:52:54"
picture: "cath1_2.jpg"
weight: "20"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/31518
- /detailsa815.html
imported:
- "2019"
_4images_image_id: "31518"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-08-03T07:52:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31518 -->
Jetzt volgebaut mit:

- 6 Luftspeicher und kompressor damit genug Luft ist fur bedienung van Treppen (Bilder folgen)
- Alle Kabel
- Hydraulik Anlage fur Neigung von das Blatt (probe version, ich weiß noch niht ob es funktioniert). 2 Zylinder an einen Nockenscheibe mit ein vierkantmitnehmer und Achse 78 (d=4mm) mit Vierkant (32081)
- Hydraulik Anlage fur Neigung von die Reifen vorne (probe version, ich weiß noch nicht ob es funktioniert). 2 Zylinder an einen Drehscheibe 180 Grad gegenuber einander verbunden, damit ein aus schiebt und denn andere raus