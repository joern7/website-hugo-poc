---
layout: "image"
title: "Caterpillar motorgrader 24H, Hydraulic anlage"
date: "2011-11-27T00:13:35"
picture: "cath14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/33577
- /detailsb7ea-2.html
imported:
- "2019"
_4images_image_id: "33577"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33577 -->
Dieses Hydraulik Anlage ist fur die Neigung die Reifen. Weil ein Zylinder aus schiebt muss der andere nach innen schieben. Ich habe dass wie abgebildet gelost. Der schiebe wird durch ein M-motor angetrieben und ist auf die Achse mit ein Vierkantmitnehmer fest gemacht.
Durch die Stange die Zylinders ist es nicht moglich die Zylinder durch das Mittelpunkt zu drehen.