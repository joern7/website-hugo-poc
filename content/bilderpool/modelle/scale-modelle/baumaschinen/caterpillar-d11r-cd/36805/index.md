---
layout: "image"
title: "Innenleben; Ripper unten"
date: "2013-03-24T00:01:31"
picture: "catdrcd2.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/36805
- /details29a9.html
imported:
- "2019"
_4images_image_id: "36805"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-24T00:01:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36805 -->
Hier ist der Ripper nach unten. Die 2 Powermotore von der bedienung Ripper kommen dann nach "oben". Die Schnecke geht dan durch das Heckschild.