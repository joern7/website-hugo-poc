---
layout: "image"
title: "CAT D11R CD (Carrydozer) Raupen + Antrieb"
date: "2013-03-19T22:15:53"
picture: "drcd10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/36781
- /details77f2-2.html
imported:
- "2019"
_4images_image_id: "36781"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36781 -->
Mehrere Details vom Raupen: http://www.fischertechnikclub.nl/index.php?option=com_phocagallery&view=detail&catid=86%3Aclubdag-poeldijk&id=5077%3Aarjen-neijsen-01-poeldijk-2013&Itemid=113&lang=nl

Die Raupen sind hauptsächlich aus den Folgenden Teile gebaut: 

I-strebe 30
U-trager adapter
Lagerhulse
Clipsachse 34 
Bauplatte 90x30
Riegelscheiben
Abstandsring (um etwas mehr als 15mm zwischen den Ketten zu haben)