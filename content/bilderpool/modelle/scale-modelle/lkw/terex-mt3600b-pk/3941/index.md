---
layout: "image"
title: "einselrad ohne antrieb_1"
date: "2005-03-30T21:12:11"
picture: "rad_ohne_antrieb_1.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3941
- /details85d8.html
imported:
- "2019"
_4images_image_id: "3941"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-30T21:12:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3941 -->
Das Resept:

Man nehme:
2 x 31019 - Drescheibe 60
1 x 31023 - Klembusche 10 mit Federring
1 x 31033 - Achse 50 Metall
2 x 31058 - Nabenmutter mit scheibe
6 x 32882 - Baustein 15 mit 2 Zapfen
2 x 32928 - Freilausnabe, Rot
1 x 36334 - Riegelscheibe
82 x 37192 - Föderglied
12 x 37237 - Baustein 5
unterlegscheiben M4