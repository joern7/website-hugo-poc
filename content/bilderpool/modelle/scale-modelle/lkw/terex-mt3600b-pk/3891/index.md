---
layout: "image"
title: "terex_41"
date: "2005-03-26T19:25:47"
picture: "terex_41.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3891
- /details97ea.html
imported:
- "2019"
_4images_image_id: "3891"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-26T19:25:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3891 -->
Obwohl diesen Monster von einen Fahrer bedient werden, gibt es trotsdem zwei Zitsen.
BTW. Die Roten Räder 23mm sind hier die Luftfilter.