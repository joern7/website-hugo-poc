---
layout: "image"
title: "MORRIS C.8.Mkll _2"
date: "2008-12-14T10:53:10"
picture: "MORIS_GUNTRACER_1_2.jpg"
weight: "2"
konstrukteure: 
- "peter krijnen"
fotografen:
- "peter krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/16600
- /details7c6b.html
imported:
- "2019"
_4images_image_id: "16600"
_4images_cat_id: "1503"
_4images_user_id: "144"
_4images_image_date: "2008-12-14T10:53:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16600 -->
