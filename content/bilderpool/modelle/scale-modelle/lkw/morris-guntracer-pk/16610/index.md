---
layout: "image"
title: "MORRIS C.8.Mkll _12"
date: "2008-12-14T10:53:30"
picture: "MORIS_GUNTRACER_3_3.jpg"
weight: "12"
konstrukteure: 
- "peter krijnen"
fotografen:
- "peter krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/16610
- /details7181.html
imported:
- "2019"
_4images_image_id: "16610"
_4images_cat_id: "1503"
_4images_user_id: "144"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16610 -->
