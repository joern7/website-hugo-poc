---
layout: "image"
title: "Frontansicht"
date: "2014-06-19T19:13:42"
picture: "Bild_016.jpg"
weight: "3"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38963
- /detailsf903.html
imported:
- "2019"
_4images_image_id: "38963"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38963 -->
