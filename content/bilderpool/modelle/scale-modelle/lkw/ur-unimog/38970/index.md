---
layout: "image"
title: "Draufsicht"
date: "2014-06-19T19:13:43"
picture: "Bild_035.jpg"
weight: "10"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38970
- /details64fc.html
imported:
- "2019"
_4images_image_id: "38970"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38970 -->
Unterhalb des Führerhauses ist der Antriebsmotor der Vorderachse. Der Motor für die Hinterachse ist unter der Ladefläche.