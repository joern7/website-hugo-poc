---
layout: "image"
title: "Detail"
date: "2014-06-19T19:13:42"
picture: "Bild_023.jpg"
weight: "7"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38967
- /details7cd6.html
imported:
- "2019"
_4images_image_id: "38967"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38967 -->
Die Pendelachsen ermöglichen eine hohe Geländegängigkeit.