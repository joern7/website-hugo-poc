---
layout: "overview"
title: "Ur-Unimog"
date: 2020-02-22T08:24:46+01:00
legacy_id:
- /php/categories/2915
- /categoriesa5c0.html
- /categories42cb.html
- /categoriese7de.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2915 --> 
Hallo,

bei diesem Model habe ich versucht möglichst genau den original Ur-Unimog nach zubauen.

Daten:
•	Vorbild: 		Mercedes Benz Ur Unimog (Unimog 401)
•	Antrieb: 		zwei 1:50 Powermotoren
•	Lenkung: 		Mini- Motor
•	Beleuchtung: 	zwei ft Lampen