---
layout: "image"
title: "Die Hinterachse"
date: "2014-06-19T19:13:42"
picture: "Bild_034.jpg"
weight: "9"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38969
- /details0bf7.html
imported:
- "2019"
_4images_image_id: "38969"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38969 -->
Die Hinterachse wird durch Statikstreben geführt. Diese erhöhen die Belastungsfähigkeit, gleiches gilt für die Vorderachse.