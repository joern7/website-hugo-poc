---
layout: "image"
title: "Unimog U1300L 15"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_15.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33406
- /details826d.html
imported:
- "2019"
_4images_image_id: "33406"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33406 -->
Die Hinterachse von der Seite. Blattfederung, Pendelanschlag mittels Klemmhülse (35980) und die hintere Abstützung an der Kardanwelle sind gut zu erkennen.