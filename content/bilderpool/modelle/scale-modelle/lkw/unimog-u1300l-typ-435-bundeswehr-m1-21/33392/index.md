---
layout: "image"
title: "Unimog U1300L 01"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_03.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33392
- /details0a9a.html
imported:
- "2019"
_4images_image_id: "33392"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33392 -->
Das ist mein maßstäblicher Nachbau eines klassischen Bundeswehr-Unimogs U1300L vom Typ 435. Der Maßstab ist 1:21, und alle wichtigen Abmaße und Proportionen habe ich exakt eingehalten.

Im Lastenheft stand die Verwendung eines Power-Motors 1:50 als Antrieb, Allradantrieb (klar), 3 Differenziale, Lenkung per Servo, größtmögliche Bodenfreiheit und Rampenwinkel vorn/hinten von mindestens 45°. Alles wurde erfüllt! ;o)

Bestimmend für den Maßstab waren die kleinen Traktorreifen, die ich unbedingt verwenden wollte. Der U1300L hat im Original in der Basisausstattung Reifen vom Format 12,5x20 mit Durchmesser 1044 mm. In Verbindung mit dem Durchmesser der Traktorreifen von 50 mm ergibt sich ein Maßstab von ca. 1:21.

Ich habe mit Maßzeichnungen des U1300L besorgt, in denen die für den Nachbau relevanten Maße eingezeichnet oder abzumessen waren. Die Spurweite beträgt somit 8,9 cm, was fast der Original-Spurweite bei einem Standard-Rahmen von 60 mm Breite entspricht, prima! Ich musste die Spurweite nur noch pro Seite mit je einer Scheibe 4 (105195) verbreitern.

Wie beim Bundeswehr-Original hat mein U1300L hinten eine Plane verbaut. Ich habe dazu dicken schwarzen Bastelkarton verwendet, der sehr stabil ist und bislang schon mehrere Überschläge heil überstanden hat.

Der gesamte Antriebsstrang ist sehr stabil ausgelegt, und in Verbindung mit der Kraft des Power-Motors 1:50 ergeben sich fantastische Geländeeigenschaften!