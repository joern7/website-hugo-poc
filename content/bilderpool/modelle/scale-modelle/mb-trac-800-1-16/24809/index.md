---
layout: "image"
title: "MB trac 800 11"
date: "2009-08-18T18:45:14"
picture: "MB_trac_16.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24809
- /details3a1c.html
imported:
- "2019"
_4images_image_id: "24809"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:45:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24809 -->
Detailansicht vom Vorbau.