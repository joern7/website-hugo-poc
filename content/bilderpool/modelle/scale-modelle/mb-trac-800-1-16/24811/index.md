---
layout: "image"
title: "MB trac 800 13"
date: "2009-08-18T18:59:11"
picture: "MB_trac_20.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24811
- /detailsa96a.html
imported:
- "2019"
_4images_image_id: "24811"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24811 -->
Detailansicht der Vorderachse von unten. In der Mitte das Z20 als Ersatz fürs zu breite Differenzial, angetrieben durch eine (nicht sichtbare) Rastschnecke. Das Lenkservo sitzt vor der Hinterachse und überträgt seine Bewegung über eine lange Statikstrebe und eine L-Lasche auf die Spurstange.