---
layout: "image"
title: "Stern05.JPG"
date: "2005-11-07T19:44:17"
picture: "Stern05.JPG"
weight: "4"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5264
- /detailsdddc.html
imported:
- "2019"
_4images_image_id: "5264"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:44:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5264 -->
Hinter den Scheiben zur Aufnahme der Pleuel sitzt noch ein Gebläse auf der Hauptwelle.