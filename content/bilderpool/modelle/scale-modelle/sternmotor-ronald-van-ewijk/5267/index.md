---
layout: "image"
title: "Stern30.JPG"
date: "2005-11-07T19:51:28"
picture: "Stern30.JPG"
weight: "7"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5267
- /details8705.html
imported:
- "2019"
_4images_image_id: "5267"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:51:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5267 -->
Die Transportsicherung in Position.