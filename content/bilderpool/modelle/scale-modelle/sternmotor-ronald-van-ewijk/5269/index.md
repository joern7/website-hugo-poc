---
layout: "image"
title: "Freilauf02.JPG"
date: "2005-11-07T19:54:55"
picture: "Freilauf02.JPG"
weight: "9"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5269
- /details0313.html
imported:
- "2019"
_4images_image_id: "5269"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5269 -->
... Dazwischen kommmt eine Vorrichtung, die um 4 Kontaktfedern aus dem ft-Drehschalter herumgestrickt ist...