---
layout: "image"
title: "Stern04.JPG"
date: "2005-11-07T19:42:26"
picture: "Stern04.JPG"
weight: "3"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5263
- /detailsf299.html
imported:
- "2019"
_4images_image_id: "5263"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:42:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5263 -->
Über den sicheren Transport von ft-Modellen gibt es einen eigenen Thread im 'alten' ft-Forum ("Transport von Modellen"). Hier wird das mit einer soliden Holzplatte, Abstandsrohren und Metallwinkeln erledigt.

In den Schwanenhälse sind die Kabel zu den Zündkerzen (Glühlampen) verlegt, die von den drehenden Kontaktfingern auf der Rückseite des Modells (hier oben) gesteuert werden.