---
layout: "image"
title: "Freilauf04.JPG"
date: "2005-11-07T20:00:33"
picture: "Freilauf04.JPG"
weight: "11"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5271
- /detailsb14f.html
imported:
- "2019"
_4images_image_id: "5271"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:00:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5271 -->
... Drumherum und davor kommen noch eine ft-Freilaufnabe (mit schwarzem Innenteil, das beim Anziehen der Nabenmutter die Achse nicht festklemmt) zur radialen Führung der Propellernabe, und ein Außenkäfig, der in Bild Stern02 zu sehen ist.