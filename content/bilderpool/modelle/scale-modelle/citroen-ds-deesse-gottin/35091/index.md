---
layout: "image"
title: "Deesse13m1.JPG"
date: "2012-07-03T21:38:11"
picture: "Deesse13m1.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35091
- /details83cf.html
imported:
- "2019"
_4images_image_id: "35091"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35091 -->
Die echte DS wird im Grundriss von vorn nach hinten immer schmäler, und die Hinterräder laufen auf deutlich engerer Spur. Beides hat die ft-Deesse auch: vorne wird ein kleines bisschen gebogen, hinten gibt ein Winkel 7,5° im Längsträger auf Höhe der Hintertüren die Richtung vor.