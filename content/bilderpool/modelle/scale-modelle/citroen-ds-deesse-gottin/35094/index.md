---
layout: "image"
title: "Deesse22m1.JPG"
date: "2012-07-03T21:49:29"
picture: "Deesse22m1.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35094
- /detailsf1f2.html
imported:
- "2019"
_4images_image_id: "35094"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:49:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35094 -->
Der Antrieb (von hier: http://www.ftcommunity.de/categories.php?cat_id=1613 ) in der Ansicht von unten. Der Motor sitzt mittig über dem oberen Bildrand. Mit einem ft-Kardan geht es auf ein Klemm-Z15 und von da auf das rote Rast-Z20, von dort über ein Differenzial-Antriebsrad 31414 auf das Differenzial.