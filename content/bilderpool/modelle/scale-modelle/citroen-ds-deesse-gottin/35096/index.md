---
layout: "image"
title: "Deesse27m1.JPG"
date: "2012-07-03T21:55:21"
picture: "Deesse27m1.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35096
- /detailsd17a.html
imported:
- "2019"
_4images_image_id: "35096"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:55:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35096 -->
Die Hinterachsschwingen können soweit einfedern, bis die Stoßstange aufsetzt.