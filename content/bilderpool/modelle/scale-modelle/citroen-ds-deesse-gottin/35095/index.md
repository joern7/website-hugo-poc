---
layout: "image"
title: "Deesse25m1.JPG"
date: "2012-07-03T21:51:17"
picture: "Deesse25m1.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35095
- /details4c52.html
imported:
- "2019"
_4images_image_id: "35095"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:51:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35095 -->
Der Unterboden ist so brett-eben wie es sich für eine Deesse gehört. Hier sieht man auch schön, wie sich die Karosserie nach hinten verjüngt.