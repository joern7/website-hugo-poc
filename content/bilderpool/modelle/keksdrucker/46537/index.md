---
layout: "image"
title: "Greifer"
date: "2017-09-30T11:52:18"
picture: "keksdrucker07.jpg"
weight: "7"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46537
- /detailsd2b9.html
imported:
- "2019"
_4images_image_id: "46537"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46537 -->
Der Greifer ist möglichst leicht gebaut, um den Horizontalantrieb mit einem Hubgetriebe auf einer Zahnstange laufen zu lassen. Der Antrieb hat nur in eine Richtung einen Referenztaster, die andere Richtung wird über die Fahrzeit geregelt.
