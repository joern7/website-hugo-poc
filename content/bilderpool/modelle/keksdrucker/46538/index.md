---
layout: "image"
title: "Vertikalantrieb"
date: "2017-09-30T11:52:18"
picture: "keksdrucker08.jpg"
weight: "8"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46538
- /detailseeff.html
imported:
- "2019"
_4images_image_id: "46538"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46538 -->
Mit zwei verbundenen Hubgetrieben ist der Vertikalantrieb auf Zahnstangen präzise genug. Das zweite Getriebe ist nicht angetrieben - es kann auch ein defektes Getriebe verwendet werden.