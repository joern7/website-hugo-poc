---
layout: "image"
title: "Bandantrieb"
date: "2017-09-30T11:52:18"
picture: "keksdrucker06.jpg"
weight: "6"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46536
- /details7b45.html
imported:
- "2019"
_4images_image_id: "46536"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46536 -->
Ein M-Motor zieht das Förderband. Es wird in einer Nut zwischen zwei Winkelträger geführt. Achtung: Die Kette nicht zu straff wählen!