---
layout: "comment"
hidden: true
title: "6102"
date: "2008-03-28T10:12:36"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Danke, Harald, für die Erklärung mit der Feder!!! Das ist wieder mal eine supersimple Lösung für ein Problem, dass ich schon oft hatte: Die Zahnstange ist zu Ende, und die ganze Konstruktion fällt auseinander. Da ne Feder reinzubauen, wäre mir nie eingefallen!

SUPER GENIAL, wie immer!!!

Gruß, Thomas