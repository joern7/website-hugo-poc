---
layout: "comment"
hidden: true
title: "5809"
date: "2008-03-21T14:33:33"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Rob, wenn alles klappt, dann ist der Flieger Ende Mai in Holland zu sehen. Aber ganz sicher im Herbst in Mörshausen.

Die Flügel werden hauptsächlich durch die 65 cm langen ft-Alus getragen, die an der Hinterkante entlang verlaufen. Hinter der Vorderkante verlaufen U-Träger über die gesamte Breite. Die Streben bringen noch ein wenig Stabilität, die aber durch den elastischen Aufbau wieder "aufgefressen" wird. Stabiler wäre es, wenn die Streben nicht im Zickzack, sondern in einer Linie durchgehend, und zwar über die gesamt Breite einschließlich Mittelteil, verlaufen würden. Aber eigentlich dienen sie dazu, die Flügelkontur darzustellen und zu fixieren.

Gruß,
Harald