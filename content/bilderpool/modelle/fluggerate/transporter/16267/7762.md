---
layout: "comment"
hidden: true
title: "7762"
date: "2008-11-13T09:42:18"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Raffiniert. Ich hab mich schon gewundert, wie das ohne Verklemmen gehen soll. Was genau ist das für ein schwarzes Teil, was da gleitet? Ein Teil dieser etwas seltsam aussehenden Lenkung herausgeschnippelt?

Gruß,
Stefan