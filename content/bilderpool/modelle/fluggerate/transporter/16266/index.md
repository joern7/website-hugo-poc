---
layout: "image"
title: "BugFw07.JPG"
date: "2008-11-12T23:19:44"
picture: "BugFw07.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16266
- /detailsf19a.html
imported:
- "2019"
_4images_image_id: "16266"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-11-12T23:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16266 -->
Die Bugfahrwerke im eingezogenen Zustand. Die Bauplatte 30x45 links außen gibt grob an, wo die Unterkante des Flugzeugrumpfes verläuft.