---
layout: "image"
title: "Erlkönig15"
date: "2008-03-19T11:31:26"
picture: "EK015.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13953
- /details451b.html
imported:
- "2019"
_4images_image_id: "13953"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:31:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13953 -->
Die Heckflosse (Höhenruder) ist gegenüber der ersten Version nahezu komplett geändert, und ist jetzt deutlich leichter geworden.

Wenn mir noch die richtige Sorte Plastikplane unterkommt, werde ich mal sämtliche Flügel damit bespannen.