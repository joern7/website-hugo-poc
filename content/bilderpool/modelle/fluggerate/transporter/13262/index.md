---
layout: "image"
title: "Erlkönig02.jpg"
date: "2008-01-04T18:27:01"
picture: "EK002.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13262
- /details7d83.html
imported:
- "2019"
_4images_image_id: "13262"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:27:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13262 -->
Das soll einmal ein Flieger werden. Zur Zeit verbringt er die meiste Zeit auf dem Rücken liegend, so wie hier auch (nur das Bild ist um 180° gedreht). Unter dem Boden der Ladefläche ist jetzt ein Robo-Interface drinnen, und rechts unten ist ein einzelnes Fahrwerksbein dran, aber eingezogen.