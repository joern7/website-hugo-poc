---
layout: "image"
title: "Erlkönig14"
date: "2008-03-19T11:29:22"
picture: "EK014.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13952
- /detailse141.html
imported:
- "2019"
_4images_image_id: "13952"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13952 -->
Die Tragflächen mit Triebwerksgondeln.

Die Federgelenksteine sorgen für den Andruck der Reibrollen und ermöglichen zweitens einen leichten Ein- und Ausbau der Propeller.

Von den vielen verbauten Flachträgern 120 stammt der Großteil von einer Materialspende von Peter Derks. Danke!