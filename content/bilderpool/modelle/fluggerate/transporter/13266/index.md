---
layout: "image"
title: "Erlkönig06.jpg"
date: "2008-01-04T18:39:57"
picture: "PC170011.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13266
- /detailsbdaf.html
imported:
- "2019"
_4images_image_id: "13266"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:39:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13266 -->
Blick über die Heckrampe in den Laderaum. Längs unter dem Dach fährt dereinst einmal ein Portalkran entlang. Der E-Tec links ist nur drinnen, weil er so schön da hin (unter den Laderaumboden und seitlich neben die Lufttanks, die hier noch fehlen) gepasst hat.