---
layout: "image"
title: "Airbus A300"
date: "2011-04-02T12:47:37"
picture: "airbusa1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30367
- /detailse53f.html
imported:
- "2019"
_4images_image_id: "30367"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-02T12:47:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30367 -->
Gesamtansicht des Airbus A300. Er besitzt ein voll funktionsfähiges Bug-, und Heckfahrwerk mit Endabschaltung. Außerdem können das Höhen-, und das Seitenruder über das Control Set angesteuert werden. Das geblinke der Positionslichter, an der linken und rechten Flügelspitze, wird über das E-Tech gesteuert. Die Triebwerke sind nicht funktionsfähig, sprich nur Attrappen. Ich habe viele Bilder von AIRLINERS.NET verwendet um dem Original möglichst nahe zu kommen.