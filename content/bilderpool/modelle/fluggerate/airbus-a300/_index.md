---
layout: "overview"
title: "Airbus A300"
date: 2020-02-22T08:34:26+01:00
legacy_id:
- /php/categories/2259
- /categories33c9.html
- /categoriesee5f.html
- /categories9ec2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2259 --> 
Modell eines Airbus A300 mit funktionsfähigem Fahrwerk mit Endabschaltung, beweglichem Höhen-, und Seitenruder.