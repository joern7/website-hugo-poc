---
layout: "image"
title: "Airbus A300"
date: "2011-04-02T12:47:37"
picture: "airbusa3.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30369
- /details5f5b.html
imported:
- "2019"
_4images_image_id: "30369"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-02T12:47:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30369 -->
Das Bugfahrwerk ist ähnlich aufgebaut wie das Heckfahrwerk. Da die blauen fischertechnik Federn zu schwach sind, musste ein Eigenbau her, der sehr gut federt.