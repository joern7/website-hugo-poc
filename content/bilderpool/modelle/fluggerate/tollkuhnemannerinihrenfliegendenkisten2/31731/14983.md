---
layout: "comment"
hidden: true
title: "14983"
date: "2011-08-31T10:59:14"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Ralf,
das wird sich leider nicht sehr viel ändern, da hier zu viel Elastizität in den Teilen selbst und in den Verbindungen enthalten ist. Wenn du den Ring mit seinen Bogenteilen unten auf einer systemfremden Versteifungsplatte links und rechts jeweils neben den Schrägstützen vertikal durch die Löcher verschraubst hast du dann eine bessere Basis die Versteifungen noch oben hin zu stabilisieren.
Gruß, Udo2