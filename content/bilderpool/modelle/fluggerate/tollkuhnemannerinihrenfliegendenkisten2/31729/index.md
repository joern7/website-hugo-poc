---
layout: "image"
title: "Die Rollen plus Oberteil"
date: "2011-08-30T23:21:42"
picture: "rollenlager4.jpg"
weight: "4"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Speichenrad"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31729
- /detailsc2e6.html
imported:
- "2019"
_4images_image_id: "31729"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31729 -->
