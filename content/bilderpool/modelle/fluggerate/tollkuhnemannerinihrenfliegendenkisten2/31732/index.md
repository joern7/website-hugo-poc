---
layout: "image"
title: "Dieses Rollenlager soll einmal ein großes Flugzeugkarussel stabil halten"
date: "2011-08-30T23:21:42"
picture: "rollenlager7.jpg"
weight: "7"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Speichenrad"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31732
- /detailsc1fc.html
imported:
- "2019"
_4images_image_id: "31732"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31732 -->
