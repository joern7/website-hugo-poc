---
layout: "image"
title: "Die Seitenansicht"
date: "2011-08-30T23:21:42"
picture: "rollenlager5.jpg"
weight: "5"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Speichenrad"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31730
- /details1008.html
imported:
- "2019"
_4images_image_id: "31730"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31730 -->
