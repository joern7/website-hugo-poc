---
layout: "image"
title: "Das Speichenrad als Grundlage 2"
date: "2011-08-30T23:21:42"
picture: "rollenlager2.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31727
- /detailscf2c.html
imported:
- "2019"
_4images_image_id: "31727"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31727 -->
Hier passt dann die 45er Bauplatte drauf.
Es gibt aber auch noch einige andere Möglichkeiten der Befestigung.