---
layout: "image"
title: "Demoiselle (frz. Fräulein oder Libelle) von Alberto Santos Dumont 1909, Bild 1"
date: "2018-08-13T16:19:28"
picture: "P1140008_kl.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47763
- /details7fff.html
imported:
- "2019"
_4images_image_id: "47763"
_4images_cat_id: "3527"
_4images_user_id: "2635"
_4images_image_date: "2018-08-13T16:19:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47763 -->
Das motorisierte Leichtflugzeug habe ich nur hochgeladen, weil im Bilderpool keine neuen Bilder mehr gezeigt wurden. 
Mein Ziel war das Modell der "Demoiselle", ausgerüstet mit dem "etwas anderen Motor".
Hier ist sie, meine Demoiselle.