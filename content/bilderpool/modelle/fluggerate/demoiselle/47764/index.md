---
layout: "image"
title: "Demoiselle (frz. Fräulein oder Libelle) von Alberto Santos Dumont 1909. Bild 2"
date: "2018-08-13T16:19:28"
picture: "P1140010_kl.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47764
- /details7733.html
imported:
- "2019"
_4images_image_id: "47764"
_4images_cat_id: "3527"
_4images_user_id: "2635"
_4images_image_date: "2018-08-13T16:19:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47764 -->
Das ist mein Modell der "Demoiselle", ausgerüstet mit dem "etwas anderen Motor". 
Unten rechts eine Aufnahme des Originals.