---
layout: "image"
title: "Ultraleichtflugzeug1"
date: "2014-07-08T20:05:33"
picture: "ultraleichtflugzeug1.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39004
- /detailse7df.html
imported:
- "2019"
_4images_image_id: "39004"
_4images_cat_id: "584"
_4images_user_id: "1631"
_4images_image_date: "2014-07-08T20:05:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39004 -->
Hier sieht man das komplette Flugzeug. es wiegt 0,14 kg und hat damit glaub ich den Namen Ultraleichtflugzeug verdient. :)
Es handelt sich hierbei um die erste Version:/ da ich noch nicht so ganz zufrieden mit dem Modell bin werde ich es noch weiter bauen denn ich möchte es schaffen das Modell so zu bauen das es steuerbar ist wie im Original.

mfg

Lukas