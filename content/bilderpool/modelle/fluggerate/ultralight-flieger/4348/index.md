---
layout: "image"
title: "Ultralight-Mobile - Tragschrauber"
date: "2004-09-29T19:20:20"
picture: "Ultra03.jpg"
weight: "2"
konstrukteure: 
- "fishfriend"
fotografen:
- "Harald Steinhaus"
keywords: ["Mobile", "Ultralight"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/4348
- /detailsa464.html
imported:
- "2019"
_4images_image_id: "4348"
_4images_cat_id: "584"
_4images_user_id: "34"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4348 -->
Ein Mobile aus ft-Ultralight-Fliegern. Hier der zweite von vier Bestandteilen, der Tragschrauber.

Einfach bezaubernd, wie wenig Teile man braucht, um ein vollständiges Modell hinzukriegen.

Gehört einfach in jedes Kinderzimmer!