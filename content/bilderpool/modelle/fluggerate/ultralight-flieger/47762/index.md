---
layout: "image"
title: "Motorisiertes Leichtflugzeug"
date: "2018-08-11T11:28:53"
picture: "Leichtflugzeug_mit_Hui_kl.jpg"
weight: "11"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47762
- /details44dd.html
imported:
- "2019"
_4images_image_id: "47762"
_4images_cat_id: "584"
_4images_user_id: "2635"
_4images_image_date: "2018-08-11T11:28:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47762 -->
Der "etwas andere Motor" (ft:pedia 2016-3) hat endlich seine Bestimmung gefunden: Ratternd, schwächlich und unzuverlässig mal in die eine und dann in die andere Richtung drehend schmückt er das spartanische Fluggefährt. Achtung: Pilot noch ohne Helm! Na,  na, na.