---
layout: "overview"
title: "Linktrainer2"
date: 2020-02-22T08:34:32+01:00
legacy_id:
- /php/categories/2301
- /categories7e71.html
- /categories28cc-2.html
- /categories139c.html
- /categoriesb7bd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2301 --> 
The most important different with the first version of the linktrainer is the replacement of the cockpit instruments.
The alti-meter, artificial horizon and the compass are replaced by a "hightech digital instrument".
On a screen you see a projection of the plane and his movements. Now you only have to hold the plane horizontal
on the two lines in the centre of the screen. Seemed to be easy!
The all-in-one stick is replaced by two sticks.
By the first version the time you used to reach your destiny was unlimited. Now there is a fuelmeter which slowly decreased.
When the tank is empty your plane crashed and the game is over.