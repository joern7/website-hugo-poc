---
layout: "image"
title: "fuelmeter2"
date: "2011-06-10T09:06:27"
picture: "linktrainer02.jpg"
weight: "2"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30832
- /details10a2.html
imported:
- "2019"
_4images_image_id: "30832"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30832 -->
Here you see the front of the fuel gauge.