---
layout: "image"
title: "bluebox6"
date: "2011-06-10T09:06:27"
picture: "linktrainer14.jpg"
weight: "14"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30844
- /details590d.html
imported:
- "2019"
_4images_image_id: "30844"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30844 -->
Close up of a stick.