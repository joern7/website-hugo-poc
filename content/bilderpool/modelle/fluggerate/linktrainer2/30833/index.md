---
layout: "image"
title: "fuelmeter3"
date: "2011-06-10T09:06:27"
picture: "linktrainer03.jpg"
weight: "3"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30833
- /details079a.html
imported:
- "2019"
_4images_image_id: "30833"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30833 -->
A close up to the rubber band.