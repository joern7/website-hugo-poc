---
layout: "image"
title: "indicator5"
date: "2011-06-10T09:06:27"
picture: "linktrainer08.jpg"
weight: "8"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30838
- /details2093.html
imported:
- "2019"
_4images_image_id: "30838"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30838 -->
Front of the indicator without the screen.