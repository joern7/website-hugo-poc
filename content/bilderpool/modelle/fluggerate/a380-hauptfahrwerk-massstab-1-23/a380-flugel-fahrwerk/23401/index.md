---
layout: "image"
title: "Wing Landing Gear wird ausgefahren_3"
date: "2009-03-06T21:42:10"
picture: "Wing_Landing_Gear_wird_ausgefahren_3.jpg"
weight: "3"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/23401
- /details9555.html
imported:
- "2019"
_4images_image_id: "23401"
_4images_cat_id: "1587"
_4images_user_id: "107"
_4images_image_date: "2009-03-06T21:42:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23401 -->
Der Realisierung der verschiedenen Gelenkpunkte des Kniehebelgetriebes war eine fast unendliche Geschichte. Über die Gestaltung und Funktion dieses Getriebes habe ich mir nie Gedanken gemacht. Ein Beispiel wie man mit ft die Technik verstehen und erlernen kann...