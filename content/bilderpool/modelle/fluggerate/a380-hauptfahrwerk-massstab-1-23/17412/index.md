---
layout: "image"
title: "Body Landing Gear A380 ausgefahren"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk5.jpg"
weight: "5"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/17412
- /details88f7.html
imported:
- "2019"
_4images_image_id: "17412"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17412 -->
Diese Fahrwerkskinematik und -aufhängung ist im Internet so nur sehr schwer zu finden. Kaum zu glauben, dass die enormen Kräfte so aufgenommen und in die Rumpfstruktur weitergeleitet werden können. Tolle Ingenieurleistung