---
layout: "image"
title: "Landing Gear eingefahren Unteransicht"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk8.jpg"
weight: "8"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/17415
- /detailsaada.html
imported:
- "2019"
_4images_image_id: "17415"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17415 -->
Aus dieser Ansicht sieht man nochmals den sehr steifen Kastenaufbau, der in Verbindung mit der Flügelwurzel eine extreme aber notwendige Grundsteifigkeit und Festigkeit ermöglicht.