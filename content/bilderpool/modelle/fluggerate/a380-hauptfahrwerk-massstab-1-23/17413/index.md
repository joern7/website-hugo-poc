---
layout: "image"
title: "Wing Landing Gear A380 von der Turbine aus gesehen"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk6.jpg"
weight: "6"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/17413
- /detailsea7c.html
imported:
- "2019"
_4images_image_id: "17413"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17413 -->
Mit der Kinematik und Funktion der Flügel - Fahrwerke habe ich mich bisher nie beschäftigt. Erst mit dem Bau habe ich die grundsätzliche Funktion und Kinematik verstanden, die sich in einem sehr engen Bauraum abspielt.