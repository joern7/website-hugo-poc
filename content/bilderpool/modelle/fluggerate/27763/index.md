---
layout: "image"
title: "Tragfläche2"
date: "2010-07-16T11:18:11"
picture: "IMG_3124_Beplankung.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/27763
- /detailsb38c.html
imported:
- "2019"
_4images_image_id: "27763"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:18:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27763 -->
... eine Beplankung aus Alufolie. Ganz normale Küchenfolie, allerdings ist sie beidseitig laminiert worden (so wie man Visitenkarten laminieren kann).
Um die Positionen der Löcher zu finden, habe ich zuerst dünnes Pauspapier aufgelegt und die Punkte markiert, dann das Papier aufs Alu-Laminat gelegt und die Löcher mit einer Nadel vorgepiekst. Den Rest hat eine Lochzange erledigt. Der Lochdurchmesser ist 4,5 mm. Da kann man die S-Riegel gerade so einfädeln.