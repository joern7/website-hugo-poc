---
layout: "image"
title: "hubi10"
date: "2003-04-21T19:56:05"
picture: "hubi10.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/230
- /details999a.html
imported:
- "2019"
_4images_image_id: "230"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=230 -->
