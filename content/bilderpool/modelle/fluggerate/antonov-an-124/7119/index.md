---
layout: "image"
title: "AN124_120.JPG"
date: "2006-10-03T13:36:29"
picture: "AN124_120.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7119
- /details993f.html
imported:
- "2019"
_4images_image_id: "7119"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:36:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7119 -->
In Rumpfmitte wird es ziemlich eng, weil die Statik für Flügel etc. kaum noch Raum übrig lässt. Vorn hat der Joystick seinen Platz (die Klappe dreht sich mitsamt den Riegelsteinen), im Schacht dahinter sitzt der Umschalter 2x4 (der gerade herausgezogen ist).

Der Leuchtstein links unten ist einer von dreien, die den Laderaum ausleuchten.

Die schwarze Schnecke rechts unten gehört zum Antrieb der Heckrampe.