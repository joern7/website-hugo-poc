---
layout: "image"
title: "AN124_111.JPG"
date: "2006-10-03T13:21:20"
picture: "AN124_111.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7114
- /detailse96d.html
imported:
- "2019"
_4images_image_id: "7114"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:21:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7114 -->
Die Schalterbaugruppen (ohne diejenigen im Cockpit).
Links: 4x Schalter: 
- Positionsleuchten weiß + Cockpit gelb; 
- Positionsleuchten rot + grün; 
- Laderaumbeleuchtung grün;
- Kompressor (der Motor ist unter der linken Tragflächenwurzel eingebaut, aber nicht genutzt)

Mitte: Joystick

Rechts: Umschalter 2x4 für die Motorengruppen:
1: Seitenruder+Querruder / Höhenruder
2: Nase auf+zu / Bugrampe auf+ab
3: Heckluke auf+zu / Heckrampe auf+ab
4: Laufkatze vor+zurück / Kranseil auf+ab

Im Cockpit gibt es noch zwei weitere Schaltergruppen: links fürs Fahrwerk (Bug+Hauptfahrwerk), rechts für die Landeklappen.