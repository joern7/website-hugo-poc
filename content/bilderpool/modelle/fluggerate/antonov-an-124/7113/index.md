---
layout: "image"
title: "AN124_110.JPG"
date: "2006-10-03T13:14:33"
picture: "AN124_110.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7113
- /details4b79.html
imported:
- "2019"
_4images_image_id: "7113"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7113 -->
Der Joystick lässt sich nach Öffnen einer Klappe (mit Riegelsteinen 15x15 als Drehgelenk) herausnehmen. Hinter der Klappe sitzt der neue Umschalter 2x4, der durch Schieben anstelle Drehen betätigt wird.