---
layout: "image"
title: "AN124_118.JPG"
date: "2006-10-03T13:32:16"
picture: "AN124_118.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7118
- /details5281-2.html
imported:
- "2019"
_4images_image_id: "7118"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:32:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7118 -->
Das Getriebe für die Landeklappe. Drinnen steckt die "alte" ft-Lenkung mit Zahnspurstange 38472.