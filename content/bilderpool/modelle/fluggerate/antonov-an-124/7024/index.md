---
layout: "image"
title: "AN124_18.JPG"
date: "2006-10-01T11:52:18"
picture: "AN124_18.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Speichenrad", "Triebwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7024
- /details82d4.html
imported:
- "2019"
_4images_image_id: "7024"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-01T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7024 -->
Ein paar Impressionen vom "Flugtag" :-)