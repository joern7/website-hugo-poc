---
layout: "image"
title: "AN124_158.JPG"
date: "2006-10-03T14:23:49"
picture: "AN124_158.JPG"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7127
- /details223d.html
imported:
- "2019"
_4images_image_id: "7127"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:23:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7127 -->
Die Mechanik zum Betätigen der Heckrampe. Der Motor sitzt oben über dem Laderaum, die Antriebsachse verläuft links in der Wand zum Alternativ-Hubgetriebe.