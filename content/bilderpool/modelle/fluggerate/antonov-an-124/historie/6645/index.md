---
layout: "image"
title: "Antonov237.JPG"
date: "2006-07-14T18:34:23"
picture: "Antonov237.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6645
- /detailse32a-2.html
imported:
- "2019"
_4images_image_id: "6645"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:34:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6645 -->
Das Seitenruder hat durch den Kegelsatz noch mal eine Untersetzung 1:2 eingebaut. Das obere Kegelrad ist am Rumpf fest, das mittlere bewegt sich mit dem Ruder mit und rollt auf ersterem ab.