---
layout: "image"
title: "Antonov001.JPG"
date: "2006-07-13T21:08:20"
picture: "Antonov001.jpeg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6633
- /details1400-2.html
imported:
- "2019"
_4images_image_id: "6633"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-13T21:08:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6633 -->
Mit diesem "Gerippe" hat das ganze Teil angefangen. In den Grundzügen ist es dabei geblieben, aber überall gibt es kleine oder große Änderungen zum aktuellen Stand.