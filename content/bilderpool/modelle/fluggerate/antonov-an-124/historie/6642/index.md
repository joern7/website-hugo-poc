---
layout: "image"
title: "Antonov209.JPG"
date: "2006-07-14T18:22:21"
picture: "Antonov209.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6642
- /details5ab4.html
imported:
- "2019"
_4images_image_id: "6642"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:22:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6642 -->
Das neue Bugfahrwerk, jetzt ausgefahren.

Rechts oben im Bild der Kranschlitten auf seiner Zahnstange (erkennbar am blauen Haken).

Links unten im Bild die Lagerung der Bugrampe.