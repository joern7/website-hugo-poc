---
layout: "image"
title: "Antonov025.JPG"
date: "2006-07-14T18:15:26"
picture: "Antonov025.jpeg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6640
- /details9375.html
imported:
- "2019"
_4images_image_id: "6640"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:15:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6640 -->
Das "alte" Bugfahrwerk in eingezogenem Zustand. Es muss sehr flach bleiben, denn darüber verläuft die Rampe zum Ein- und Ausladen.