---
layout: "image"
title: "AN124_150.JPG"
date: "2006-10-03T14:16:53"
picture: "AN124_150.JPG"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7125
- /details297c-2.html
imported:
- "2019"
_4images_image_id: "7125"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7125 -->
Ein Blick unter die Decke des Laderaums. Das dreieckige Gebilde, das ganz vorn herunterhängt, ist der vordere Endanschlag für den Kran (einen hinteren Anschlag gibts nicht).

Links entlang sieht man die drei grünen Lampen zur Beleuchtung.

Im rechten oberen Drittel, über der Energiekette, ist der schräg eingebaute Motor (mit Stufengetriebe und schwarzer Schnecke) zu erkennen, der die Landeklappen antreibt.

Die rote Bauplatte, die rechts oben etwas schräg herumhängt, dient als Stütze für die Energiekette, wenn der Kran hinten ist.

Die blaue Statikplatte, die am oberen Rand ins Bild ragt, war als dritter Teil der Heckklappe gedacht. Sie hat auch einen P-Zylinder zum Öffnen und Schließen, aber nachdem dieser Zylinder das einzig verbliebene Pneumatik-Element ist, habe ich drauf verzichtet, da noch Schläuche hin zu legen.