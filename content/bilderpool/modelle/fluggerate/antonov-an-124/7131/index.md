---
layout: "image"
title: "AN124_166.JPG"
date: "2006-10-03T14:44:16"
picture: "AN124_166.JPG"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7131
- /detailsfcd5-2.html
imported:
- "2019"
_4images_image_id: "7131"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:44:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7131 -->
Blick von vorn in die hintere Hälfte des Laderaums. 

Links am Rand: der Anfang der Energiekette, die nur lose in einen Rollenbock eingeklemmt ist (was auch keine lange Autobahnfahrt übersteht...)

Dahinter: die schwarze Schnecke treibt die Landeklappen an.

Die mittlere Heckluke ist gerade heruntergeklappt. Zum Schließen dient ein P-Zylinder, der aber nur manuell bedient wird.

Die schwarze Schnecke rechts oben gehört zum Antrieb für die Heckrampe.

Ein quer durch den Rumpf gehendes Alu-Teil wäre zwar für die Stabilität besser gewesen, aber dann hätte der Kran 15 mm tiefer gemusst, um noch hinreichend freien Laderaumquerschnitt zu behalten (oder die Zelle 15 mm höher -- oops.) 

Die Zahnstangenbahn für den Kran sieht etwas krumm aus, das ist aber die Kamera gewesen.