---
layout: "image"
title: "AN124_112.JPG"
date: "2006-10-03T13:24:04"
picture: "AN124_112.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7115
- /details420c-2.html
imported:
- "2019"
_4images_image_id: "7115"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7115 -->
Die Schalterbaugruppen aus anderer Perspektive. Der Umschalter 2x4 (rechts) ist ein Eigenbau aus einem BS15x5x30 mit 8 Löchern und darin eingesetzten, abgesägten Steckerhülsen 2,6 mm. Die Federkontakte im BS15 Loch sind Original-ft.