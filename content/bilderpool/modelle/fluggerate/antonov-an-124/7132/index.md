---
layout: "image"
title: "AN124_167.JPG"
date: "2006-10-03T14:47:59"
picture: "AN124_167.JPG"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7132
- /detailsd9ba.html
imported:
- "2019"
_4images_image_id: "7132"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:47:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7132 -->
Die Bugrampe ist zweiteilig und ziemlich "lüttig" aufgebaut. Aber sie funktioniert. Das Ein- und Ausklappen wird durch Zusammenspiel von Schwerkraft und einer ft-Antriebsfeder erledigt: beim Einklappen stößt der vordere Teil irgendwann an den Cockpitboden und knickt dann ein. Beim Ausklappen hat die Antriebsfeder bei etwa 45° Schräglage des hinteren Teils genug Hebelwirkung, um den vorderen Teil aufzuklappen.

Die Antriebsfeder ist rechts am oberen Rand zu erkennen.