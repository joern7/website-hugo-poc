---
layout: "image"
title: "AN124_159.JPG"
date: "2006-10-03T14:27:01"
picture: "AN124_159.JPG"
weight: "29"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7128
- /details1410.html
imported:
- "2019"
_4images_image_id: "7128"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:27:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7128 -->
Die Bugrampe wird nach dem gleichen Prinzip wie die Heckrampe betätigt: Motor oben drüber, Antriebsachse vertikal und seitlich in der Wand geführt, danach ein Alternativ-Hubgetriebe (mit Rollenlager 37636 und "Führungsplatte für E-Magnet" 32455), sowie diversen Hebelgestängen.