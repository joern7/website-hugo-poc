---
layout: "image"
title: "AN124_20.JPG"
date: "2006-10-03T14:56:16"
picture: "AN124_20.JPG"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7134
- /details31cf.html
imported:
- "2019"
_4images_image_id: "7134"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:56:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7134 -->
Das Bugfahrwerk. Beim Einfahren betätigt die rechte Strebe 30 den Minitaster und setzt den Motor damit still. Beim Ausfahren läuft der Motor mit, bis das Hauptfahrwerk den unteren Anschlag erreicht hat. Damit dabei nichts zu Bruch geht, gibt es hier eine Rutschkupplung, bestehend aus Silikonschlauch und drübergezogenem Kabelbinder.

Das Bugfahrwerk darf sehr schwach ausgelegt sein. Bei echten Fliegern hat es niemals mehr als 10% des Gewichts zu tragen. Beim vorliegenden Modell trägt es ... ähem ... garnichts!

Um am Bugfahrwerk Bodenkontakt zu bekommen, müssten die Räder größer werden (passt aber nicht) oder das Hauptfahrwerk müsste etwas höher gesetzt werden (passt auch nicht) oder früher seinen Anschlag erreichen (geht nicht). Also bleibts wie's ist.