---
layout: "image"
title: "Joystick01.JPG"
date: "2006-07-14T18:08:35"
picture: "Joystick01.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6636
- /detailse2f0.html
imported:
- "2019"
_4images_image_id: "6636"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:08:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6636 -->
Der Joystick findet später Platz in einem Ablagefach oben in Flügelmitte.