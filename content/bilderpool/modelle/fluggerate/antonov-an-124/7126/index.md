---
layout: "image"
title: "AN124_155.JPG"
date: "2006-10-03T14:20:50"
picture: "AN124_155.JPG"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7126
- /details6f79-3.html
imported:
- "2019"
_4images_image_id: "7126"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:20:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7126 -->
Das leere Hubgetriebe vom Erlkönig ist jetzt ersetzt worden durch eine Zahnstangenführung mittels 32455. Dadurch kann auf gleicher Baulänge noch ein Endschalter untergebracht werden. Der Schalter wird betätigt, wenn der Kran vorne unter dem Cockpit angekommen ist. Der zugehörige Anschlag ragt schemenhaft von oben in das Weiße in Bildmitte hinein.