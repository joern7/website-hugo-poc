---
layout: "image"
title: "Von oben2"
date: "2010-09-14T20:16:06"
picture: "rundumblick17.jpg"
weight: "17"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28157
- /detailsa67b-2.html
imported:
- "2019"
_4images_image_id: "28157"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28157 -->
Ansicht 17