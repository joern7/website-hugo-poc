---
layout: "image"
title: "Kofferraum"
date: "2010-09-18T13:36:54"
picture: "DSCF0394.jpg"
weight: "23"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28180
- /details35f4-2.html
imported:
- "2019"
_4images_image_id: "28180"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28180 -->
Blick in den Gepäckraum des Helikopters