---
layout: "image"
title: "Rundum11"
date: "2010-09-14T20:16:06"
picture: "rundumblick11.jpg"
weight: "11"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28151
- /details8f29.html
imported:
- "2019"
_4images_image_id: "28151"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28151 -->
Ansicht 11