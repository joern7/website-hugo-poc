---
layout: "image"
title: "2 Frontscheinwerfer zur Beleuchtung"
date: "2010-09-18T13:36:54"
picture: "DSCF0402.jpg"
weight: "27"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28184
- /details347f-2.html
imported:
- "2019"
_4images_image_id: "28184"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28184 -->
Frontscheinwerfer zum Beleuchten (Beide Lampen sind mit einem Kupferdraht verbunden)