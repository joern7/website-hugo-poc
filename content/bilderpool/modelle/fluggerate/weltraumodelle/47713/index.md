---
layout: "image"
title: "Snowspeeder"
date: "2018-06-24T16:46:34"
picture: "Snowspeeder.jpg"
weight: "7"
konstrukteure: 
- "ForscherTeam"
fotografen:
- "rito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ForscherTeam"
license: "unknown"
legacy_id:
- /php/details/47713
- /details5436.html
imported:
- "2019"
_4images_image_id: "47713"
_4images_cat_id: "3503"
_4images_user_id: "2864"
_4images_image_date: "2018-06-24T16:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47713 -->
Snowspeeder Miniatur
(F 10J)