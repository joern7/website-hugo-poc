---
layout: "overview"
title: "Weltraumodelle"
date: 2020-02-22T08:34:49+01:00
legacy_id:
- /php/categories/3503
- /categoriesc5d9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3503 --> 
Hier ein paar Kleinmodelle aus einer Welt die mir unbekannt, meinem Sohn aber umso bekannter ist :) Jedenfalls habe ich da nix mit zu tun und Konstantin (9) hat das selbst konstruiert :)