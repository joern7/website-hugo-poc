---
layout: "image"
title: "DC9-19_3091.JPG"
date: "2011-02-12T13:06:10"
picture: "DC9-19_3091.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29915
- /detailse8a8-2.html
imported:
- "2019"
_4images_image_id: "29915"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T13:06:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29915 -->
Das Hauptfahrwerk, fast komplett. Flugrichtung ist nach links oben. Schräg vorn/unter dem Z10 fehlt noch ein weiteres Z10, das mit ersterem kämmt und vom Motor in der linken Tragfläche angetrieben wird. Es gab keine andere Möglichkeit, die Achse mit den beiden 130593 anzutreiben.