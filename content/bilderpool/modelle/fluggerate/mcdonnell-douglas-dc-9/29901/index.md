---
layout: "image"
title: "DC9-05_4106.JPG"
date: "2011-02-12T12:15:44"
picture: "DC9-05_4106.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29901
- /details56db.html
imported:
- "2019"
_4images_image_id: "29901"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:15:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29901 -->
Eine Erfahrung von früheren Projekten der Fliegerei ist, dass man rechte Winkel und das ft-Raster beibehalten soll, wo immer es auch geht. Das spiegelt sich hier im Gerippe der Tragflächen wieder: einzig die Vorderkante und das Querschnittsprofil sind krumm.