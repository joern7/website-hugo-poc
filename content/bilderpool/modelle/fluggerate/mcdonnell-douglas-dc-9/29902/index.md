---
layout: "image"
title: "DC9-06_4619.JPG"
date: "2011-02-12T12:19:17"
picture: "DC9-06_4619.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29902
- /details556d.html
imported:
- "2019"
_4images_image_id: "29902"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:19:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29902 -->
Die Flügelwölbung macht das Beplanken zu einer eher meditativen Angelegenheit. Es hat dann mit einer flachen Bahn geklappt, die nur auf halber Strecke einen 'Zwickel' verpasst bekommen musste.