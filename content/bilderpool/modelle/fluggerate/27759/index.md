---
layout: "image"
title: "Schiefe Verstrebungen 1"
date: "2010-07-16T11:12:26"
picture: "IMG_3097_Winkelverbindung.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/27759
- /detailsf3de.html
imported:
- "2019"
_4images_image_id: "27759"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:12:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27759 -->
Ich hab mal ein bisschen herumprobiert, und siehe da, die S-Riegel bieten bei Verbindungen mit Winkelsteinen erstaunlich viele Möglichkeiten zur Versteifung.