---
layout: "image"
title: "Luftkissenboot"
date: "2007-02-18T20:57:36"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Luftkissenboot", "fliegen"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9053
- /detailsa220.html
imported:
- "2019"
_4images_image_id: "9053"
_4images_cat_id: "825"
_4images_user_id: "34"
_4images_image_date: "2007-02-18T20:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9053 -->
Hier ein Luftkissenboot das mit normaler Spannung (9V Trafo)abhebt. Man kann den Luftsack auch etwas einfacher aufbauen, dann schwebt er auf dem Tisch (z.B. für Spiele)