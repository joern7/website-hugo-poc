---
layout: "comment"
hidden: true
title: "2415"
date: "2007-02-18T21:20:24"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Hier sieht man, das der Luftsack aus zwei Teilen besteht. Die obere Abdeckung (die hier unten liegt) und den seitlichen Teil der einfach unten zusammen geklebt ist. Und genau hier ist ein Problem. Ich denke das man den unteren Teil aus einem Extrateil Folie machen sollte um es flach zu bekommen. Manchmal hebt sich dieser Teil an die obere Abdeckung.