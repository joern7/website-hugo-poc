---
layout: "image"
title: "Rotorkopf03.JPG"
date: "2007-01-25T19:08:49"
picture: "Rotorkopf03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8699
- /details16f0.html
imported:
- "2019"
_4images_image_id: "8699"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:08:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8699 -->
Die Taumelscheibe (obere Drehscheibe) wird durch drei am Umfang verteilte Führungsplatten 32455 in der Position gehalten.