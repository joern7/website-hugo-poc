---
layout: "image"
title: "Rotorkopf04.JPG"
date: "2007-01-25T19:09:09"
picture: "Rotorkopf04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8700
- /details9f6d.html
imported:
- "2019"
_4images_image_id: "8700"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:09:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8700 -->
