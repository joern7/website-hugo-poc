---
layout: "image"
title: "Rotorkopf06.JPG"
date: "2007-01-25T19:09:48"
picture: "Rotorkopf06.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8702
- /details95cc.html
imported:
- "2019"
_4images_image_id: "8702"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:09:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8702 -->
