---
layout: "image"
title: "Düsenjet14"
date: "2011-05-29T12:09:36"
picture: "duesenjet14.jpg"
weight: "14"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30657
- /detailsbc30-2.html
imported:
- "2019"
_4images_image_id: "30657"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30657 -->
Fahrwerk hinten ausgefahren.
Das hintere Fahrwerk wird von einem Power-Motor aus-und eingefahren.