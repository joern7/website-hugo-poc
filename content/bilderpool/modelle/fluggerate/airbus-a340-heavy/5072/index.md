---
layout: "image"
title: "A340H_216.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_216.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5072
- /details9472.html
imported:
- "2019"
_4images_image_id: "5072"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5072 -->
The front door was opened by ground staff operating the gray switch on the bottom. The P-valve right next to it controls the pneumatic cargo hatch on the left hand side of the fuselage.