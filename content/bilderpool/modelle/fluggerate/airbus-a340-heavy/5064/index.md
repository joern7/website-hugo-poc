---
layout: "image"
title: "A340H_116.JPG"
date: "2005-10-06T17:25:26"
picture: "A340H_116.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5064
- /detailseb0d.html
imported:
- "2019"
_4images_image_id: "5064"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5064 -->
Yet another outdated photograph, but it's almost there. Three "wheel 45" (only two shown, the third would go at right angles between them, with its axle going from mid-left to lower-right, with a 30-tooth-wheel to connect with the worm shown at the bottom) were meant to drive the chains in opposite directions, but this turned out to cost too much power.