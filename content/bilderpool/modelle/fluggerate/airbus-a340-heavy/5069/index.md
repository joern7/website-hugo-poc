---
layout: "image"
title: "A340H_206.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_206.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5069
- /details62ce.html
imported:
- "2019"
_4images_image_id: "5069"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5069 -->
The rear part of the fuselage with air tanks in their new position. The tail does exist but was removed for transportation. 
Center of photograph shows the device that shuts off the compressor motor once the specified air pressure has been reached.

Floor plates of the cargo compartment are removed for better view onto the secrets of the underside.