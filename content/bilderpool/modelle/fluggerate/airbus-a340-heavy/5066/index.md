---
layout: "image"
title: "A340H_122.JPG"
date: "2005-10-06T17:26:22"
picture: "A340H_122.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5066
- /detailsed62.html
imported:
- "2019"
_4images_image_id: "5066"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5066 -->
Another 'history' picture, this is how the air tanks were once mounted in the rear bottom compartment of the fuselage. Then they had to move to the upside, to make room for the gearbox that drives the rudders and flaps.