---
layout: "image"
title: "A340H_202.JPG"
date: "2005-10-06T17:25:26"
picture: "A340H_202.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5061
- /details7668.html
imported:
- "2019"
_4images_image_id: "5061"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5061 -->
Der Zusatz 'Heavy' deutet im Funkverkehr immer auf eine Frachtmaschine hin. Das hier musste eine Frachtmaschine werden, weil mir für eine Passagiermaschine ein paar (ahemm....) Sitze gefehlt hätten. Oder hätte ich Bänke reintun sollen?


In air traffic control, the 'Heavy' appendix indicates a cargo plane. Oh, and heavy it sure is. This one had to become a cargo plane because I'm lacking a few (he-he...) seats to fully equip a personnel carrier. Or should I revert to seating banks?