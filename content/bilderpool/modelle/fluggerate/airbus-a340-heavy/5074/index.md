---
layout: "image"
title: "A340H_224.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_224.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5074
- /details3312.html
imported:
- "2019"
_4images_image_id: "5074"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5074 -->
The cargo hatch on the left side is closed, with its lock (black strut-15) engaged. Toward the bottom you can see the axle-180 that drives the nose landing gear.