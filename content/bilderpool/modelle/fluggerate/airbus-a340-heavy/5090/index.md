---
layout: "image"
title: "A340H_256.JPG"
date: "2005-10-14T18:19:45"
picture: "A340H_256.jpg"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5090
- /detailsbcf6-3.html
imported:
- "2019"
_4images_image_id: "5090"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-14T18:19:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5090 -->
This is the gearbox that drives the main control surfaces. The aircraft also has flaps and spoilers but these are driven by pneumatics.

For construction details of this device see http://www.ftcommunity.de/details.php?image_id=4607 (and subsequent imagery).

Its task is to control 4 control surfaces, using only 2 motors with 1 joy-stick. All the magic is hidden in the arrangement of two differentials and a couple of Z10 and Z15 gearwheels. 

The motor in the foreground drives both differential cages, in opposite directions. The motor in the background drives the rearward facing axles of the differentials, in the same direction. The function of the differentials is to add these inputs and supply them to the worm gears visible to the right. These worm gears are connected with the ailerons (Querruder) located on the outer ends of the main wings. Furthermore, the motor inputs are also output to the control surfaces in the tail. This is accomplished by two Z10 and the black axles that are visible above and below the gray compressor engine in the back.

Moving the joy-stick left or right switches on the rear motor which drives the ailerons in different directions and causes the aircraft to bank. This motor also operates the rudder (Seitenruder).

Moving the joy-stick back or forward switches on the front motor which drives the elevator (Höhenruder) and also drives the ailerons in the same directions. 

Doing both, and moving the joy-stick in all possible directions does not hurt because the drive paths do not interfere with each other. Rather, the differentials simply add all movements.

However, driving the ailerons in the same direction to support the elevators is not what real aircraft ailerons do. Well, I (a) didn't know that before and (b) thought it would be simpler.

Looking back, I could have been done with one motor to directly drive the elevator and another to drive rudder and ailerons, using nothing more than three bevel gears (Kegelzahnräder) and some reduction gear. Well, that's one lesson learned :-)