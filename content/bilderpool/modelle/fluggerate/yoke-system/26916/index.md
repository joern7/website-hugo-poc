---
layout: "image"
title: "'Lenkrad' rechts links Bewegung"
date: "2010-04-08T17:38:29"
picture: "yokesystem2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26916
- /details5228.html
imported:
- "2019"
_4images_image_id: "26916"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26916 -->
Die Übertragung auf das Poti wird mit einer FT Kette eins zu eins übertragen. So wird mit viel Drehbewegung des Lenkrades das Potentiometer nur wenig gedreht. Somit ist die Steuerung sehr fein.