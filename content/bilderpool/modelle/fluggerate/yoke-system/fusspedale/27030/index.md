---
layout: "image"
title: "Fußpedale Vordergesamtansicht (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27030
- /detailse97b.html
imported:
- "2019"
_4images_image_id: "27030"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27030 -->
Die oberen Platten sind natürlich auch noch durch innere Metallstangen verbunden.