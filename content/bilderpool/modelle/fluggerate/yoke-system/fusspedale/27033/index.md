---
layout: "image"
title: "Fußpedale Führungsschiene (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27033
- /details0e04-2.html
imported:
- "2019"
_4images_image_id: "27033"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27033 -->
Hier sieht man das Führungssytem, damit das Pedal im Aluprofil bleibt.