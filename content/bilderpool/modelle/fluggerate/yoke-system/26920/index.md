---
layout: "image"
title: "Gashebel vor und zurück"
date: "2010-04-08T17:38:29"
picture: "yokesystem6.jpg"
weight: "6"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26920
- /detailsc335.html
imported:
- "2019"
_4images_image_id: "26920"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26920 -->
Die Übertragung auf das Potentiometer ist identisch wie beim "Lenkrad"