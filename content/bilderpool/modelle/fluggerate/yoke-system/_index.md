---
layout: "overview"
title: "Yoke System"
date: 2020-02-22T08:34:18+01:00
legacy_id:
- /php/categories/1930
- /categoriesf725.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1930 --> 
Yoke System zum Steuern eines Flugsimulators. Man sieht das "Lenkrad" und den Gashebel. Die Pedale habe ich noch nicht konstruiert.