---
layout: "comment"
hidden: true
title: "11347"
date: "2010-04-11T22:23:46"
uploadBy:
- "uhen"
license: "unknown"
imported:
- "2019"
---
Was meinst Du damit "an ein Interface anschließen?"

Die Potis des Yoke Systems kannst Du natürlich an ein FT Interface anschließen, nur wird es dann nicht als Joystick erkannt und Du kannst es z.B. im Flight Simulator nicht nutzen.

Ansonsten ist diese Box ja wireless mit dem Empfänger am PC verbunden. Somit werden dann alle Potis in der Joystick Konfiguration als eine Achse erkannt.