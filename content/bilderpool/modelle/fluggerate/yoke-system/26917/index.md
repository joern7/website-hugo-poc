---
layout: "image"
title: "'Lenkrad' hoch runter Bewegung"
date: "2010-04-08T17:38:29"
picture: "yokesystem3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26917
- /detailsfe1f.html
imported:
- "2019"
_4images_image_id: "26917"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26917 -->
Für das Runter/Hochziehen erfolgt die Übertragung auf das Potentiometer mit dem mittelgroßen Zahnrad