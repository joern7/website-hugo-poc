---
layout: "image"
title: "Rotor des FlieWaTüüt"
date: "2016-01-10T14:29:38"
picture: "dasfliwatueuetkonradfox4.jpg"
weight: "4"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/42698
- /detailsf612.html
imported:
- "2019"
_4images_image_id: "42698"
_4images_cat_id: "3178"
_4images_user_id: "1126"
_4images_image_date: "2016-01-10T14:29:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42698 -->
Der Rotor ist starr (kann also nicht für die Straßenfahrt zusammengeklappt werden), besitzt dafür aber vier statt drei Rotoren. Und dreht sich natürlich (ferngesteuert).