---
layout: "image"
title: "Frontansicht des FliWaTüüt"
date: "2016-01-10T14:29:38"
picture: "dasfliwatueuetkonradfox5.jpg"
weight: "5"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/42699
- /details9eaa.html
imported:
- "2019"
_4images_image_id: "42699"
_4images_cat_id: "3178"
_4images_user_id: "1126"
_4images_image_date: "2016-01-10T14:29:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42699 -->
Man erkennt gut den Frontantrieb für die Straßenfahrt (natürlich mit Differential) und die drei Schalthebel für Pilot und Kopilot, sowie die komfortablen (Recaro-)Sitze. Hupe und IR-Empfänger verbergen sich hinter den Sitzlehnen.