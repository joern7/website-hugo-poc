---
layout: "image"
title: "tarnkapp8228.jpg"
date: "2012-10-07T15:04:18"
picture: "IMG_8228.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35804
- /detailsf8de.html
imported:
- "2019"
_4images_image_id: "35804"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:04:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35804 -->
Erstmals ungetarnt: der Tarnkappenbomber 2012 in Erbes-Büdesheim. Vorbilder hat er reichlich; am deutlichsten vertreten sind die Go229 (Horten H IX, Ho229) und die Northrop B-2.