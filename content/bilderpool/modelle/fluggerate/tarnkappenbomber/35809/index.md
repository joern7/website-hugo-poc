---
layout: "image"
title: "tarnkapp8380.jpg"
date: "2012-10-07T15:43:26"
picture: "IMG_8380.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35809
- /detailsccef.html
imported:
- "2019"
_4images_image_id: "35809"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:43:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35809 -->
Das Bugfahrwerk (ohne die Räder und den schwarzen BS15-Loch) mit Endschaltern, aber ansonsten so, wie aus der ft:pedia 02/2012 bekannt. Das graue Kardangelenk ist ganz unglücklich in die Drehbank gefallen ;-) 
Das Klemm-Z15 links gehört zur Luftbremse.