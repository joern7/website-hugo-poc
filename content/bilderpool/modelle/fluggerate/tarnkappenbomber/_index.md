---
layout: "overview"
title: "Tarnkappenbomber"
date: 2020-02-22T08:34:34+01:00
legacy_id:
- /php/categories/2673
- /categories4d31.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2673 --> 
Seit 2007 war der Tarnkappenbomber bei allen ftconventions dabei. 
Wie gut der Tarnmodus funktioniert, zeigen diese Fotos aus 2007 und aus 2011:
http://www.ftcommunity.de/details.php?image_id=11571
http://www.ftcommunity.de/details.php?image_id=28410

In 2012 wurde nun das Geheimnis gelüftet und der Bomber erstmals ungetarnt gezeigt.