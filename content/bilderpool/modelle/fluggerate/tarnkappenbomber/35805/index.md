---
layout: "image"
title: "tarnkapp8362.JPG"
date: "2012-10-07T15:17:17"
picture: "IMG_8362.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35805
- /details0dd3.html
imported:
- "2019"
_4images_image_id: "35805"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:17:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35805 -->
Der Bauzustand zur ftconvention 2012: die Fahrwerke sind bis auf die Landescheinwerfer komplett in Funktion, von den Steuerflächen sind nur die Luftbremsen (links/rechts außen) komplett.

Die drei S-Motoren bewegen:
- unten rechts, mit Stufengetriebe: das Bugfahrwerk
- Bildmitte, knapp oberhalb vom Winkel 60°, mit Schnecke (verdeckt) auf Klemm-Z15: die Luftbremsen über eine Kardanwelle, die direkt hinter den U-Trägern entlang verläuft. Besonderheit: diese Kardanwelle hat einfach so, ohne Gestückel und Gefrickel, über die ganze Breite und einschließlich aller Gelenke, hinein gepasst.
- mitte oben: mit Schnecke: das Hauptfahrwerk. Auf der gleichen Achse sind zwei Kranhaken angeordnet, an denen der Flieger aufgehängt werden sollte. Das war eine Schnapsidee, denn unter Last verbiegt sich die Achse und garnix passiert.

Die beiden grauen Leuchtsteine enthalten weiße LEDs und sind an den Fahrwerksbeinen montiert. Im eingefahrenen Zustand zeigen sie nach oben.

Am oberen Bildrand sieht man links/rechts außen die Querruder, mit Kardanwelle und drei Kegelzahnrädern, die für gegenläufige Bewegung sorgen. Da fehlt noch der Antriebsmotor.