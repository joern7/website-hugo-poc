---
layout: "image"
title: "tarnkapp8383.jpg"
date: "2012-10-07T15:53:09"
picture: "IMG_8383.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35812
- /details7e4c.html
imported:
- "2019"
_4images_image_id: "35812"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35812 -->
Die Schnecke in Bildmitte oben treibt die Luftbremsen an. Der graue Winkel daneben ist ein original-ft-Teil (!) und dient als Anschlag, wenn die Bombenluken geschlossen sind. Deren Antrieb fehlt noch. In Erbes-Büdesheim hatte ich die Luken zugeklebt, weil sie beim Herunterbaumeln dem Hauptfahrwerk ins Gehege kommen.