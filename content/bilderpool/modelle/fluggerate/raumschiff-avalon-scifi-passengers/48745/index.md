---
layout: "image"
title: "Seitenansicht"
date: 2020-05-11T16:09:45+02:00
picture: "2020-01-13 Raumschiff _Avalon_ aus dem SciFi _Passengers_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das blöde ist nur, dass ein ganz gewaltiger Brocken halt doch nicht so ganz vom Schutzschirm abgefangen werden konnte - 30 Jahre nach dem Start. Deshalb geht im Schiff nach und nach immer mehr kaputt, und den Anfang macht eine Hyperschlafkammer, die den Helden der Geschichte also 90 Jahre zu früh aufweckt. Nach einem Jahr hält er es nicht mehr aus und weckt seine Auserwählte Partner-Heldin auf. Was bei der natürlich zu *richtig* mieser Laune führt, als es herauskommt.