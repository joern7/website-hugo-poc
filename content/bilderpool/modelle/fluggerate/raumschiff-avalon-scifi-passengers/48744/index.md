---
layout: "image"
title: "Rückansicht"
date: 2020-05-11T16:09:44+02:00
picture: "2020-01-13 Raumschiff _Avalon_ aus dem SciFi _Passengers_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Letztlich müssen die beiden halt herausfinden, warum das Schiff immer mehr kaputt geht - und das Problem lösen, wenn die anderen 4.998 Leute noch ihr Ziel erreichen sollen. Kleinigkeit ;-)