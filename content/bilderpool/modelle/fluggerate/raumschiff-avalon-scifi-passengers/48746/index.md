---
layout: "image"
title: "Das Raumschiff"
date: 2020-05-11T16:09:46+02:00
picture: "2020-01-13 Raumschiff _Avalon_ aus dem SciFi _Passengers_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das echte Raumschiff ist natürlich riesig: Einer der drei geschwungenen Teile beherbergt 5.000 im Hyperschlaf befindliche Siedler, ein andere ist das Lager mit Traktoren, Maschinen und Ersatzteilen, die man für die Besiedlung eines neuen Planeten halt so braucht, und der dritte ist fürs Soziale: Er enthält Sportstätten, Swimming Pools, Restaurants und eine Bar mit einem Roboter als Barkeeper. Da sollten die Passagiere 4 Monate vor Ende ihrer 120 Jahre dauernden Reise Zeit verbringen und in Schulungen lernen, was sie zum Siedeln wissen müssen.

Die Spitze vorne generiert einen Schutzschild, der das Raumschiff vor Meteoriten und dergleichem im Weltraum Herumlungerndem beschützt - immerhin fliegt die Avalon mit halber Lichtgeschwindigkeit.

Den Film kann ich nur empfehlen - mir zumindest gefällt er super.

Ein Video des Modells findet sich unter https://youtu.be/BIG3MfqmUbY