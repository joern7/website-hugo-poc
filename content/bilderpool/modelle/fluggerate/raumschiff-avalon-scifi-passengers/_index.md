---
layout: "overview"
title: "Raumschiff 'Avalon' aus dem SciFi 'Passengers'"
date: 2020-05-11T16:09:42+02:00
---

Eines der, wie ich finde, schönsten Raumschiffe, das in einem SciFi gezeigt wurde - und es rief mit seiner reizvollen Geometrie nach einem kleinen Nachbau.