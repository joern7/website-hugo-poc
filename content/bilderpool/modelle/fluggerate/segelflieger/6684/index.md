---
layout: "image"
title: "Segler05.JPG"
date: "2006-08-15T15:37:36"
picture: "Segler05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6684
- /detailsed2a.html
imported:
- "2019"
_4images_image_id: "6684"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:37:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6684 -->
