---
layout: "image"
title: "Segler13.JPG"
date: "2006-08-15T15:38:42"
picture: "Segler13.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6686
- /details3c2b.html
imported:
- "2019"
_4images_image_id: "6686"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:38:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6686 -->
