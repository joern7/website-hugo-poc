---
layout: "image"
title: "Segler03.JPG"
date: "2006-08-15T15:36:51"
picture: "Segler03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6682
- /details7a0f.html
imported:
- "2019"
_4images_image_id: "6682"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:36:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6682 -->
