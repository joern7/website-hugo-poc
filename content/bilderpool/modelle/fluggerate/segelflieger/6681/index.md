---
layout: "image"
title: "Segler02.JPG"
date: "2006-08-15T15:35:53"
picture: "Segler02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6681
- /detailsd8b4.html
imported:
- "2019"
_4images_image_id: "6681"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:35:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6681 -->
