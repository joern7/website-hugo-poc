---
layout: "image"
title: "Chipmunk16.JPG"
date: "2006-09-17T20:58:18"
picture: "Chipmunk16.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6876
- /detailsa494.html
imported:
- "2019"
_4images_image_id: "6876"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6876 -->
