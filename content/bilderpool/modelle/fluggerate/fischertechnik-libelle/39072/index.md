---
layout: "image"
title: "Libelle   - achter onderaanzicht  + op achtergrond Smartbird als Pijlstaartrog"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle32.jpg"
weight: "24"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39072
- /details1d59-2.html
imported:
- "2019"
_4images_image_id: "39072"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39072 -->
Op de achtergrond zowel de Smartbird als de Pijlstaartrog.

https://www.youtube.com/watch?v=RjhEi15VK-4&index=7&list=UUvBlHQzqD-ISw8MaTccrfOQ       = Smartbird Earthflight Fischertechnik

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2        = Pijlstaartrog Fischertechnik