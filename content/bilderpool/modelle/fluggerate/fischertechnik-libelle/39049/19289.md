---
layout: "comment"
hidden: true
title: "19289"
date: "2014-07-28T22:18:08"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
Du entdeckst und baust aber auch immer wieder abgefahrene Teile! Eine tolle Modellidee - und eine wunderbare Umsetzung. Schade, dass man die ft-Libelle nicht wie die von Festo zum Fliegen bekommt...
Gruß, Dirk