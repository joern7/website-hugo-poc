---
layout: "image"
title: "Zelfbouw Accu-pack t.b.v.  meer vermogen en koppel    -onderaanzicht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle17.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39057
- /details6b36.html
imported:
- "2019"
_4images_image_id: "39057"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39057 -->
Het zelf samenstellen van een Accu-pack met NiMh-accus van de Aldi of Lidl met een spanning van 10,8 of 12V biedt voor wat grotere modellen meer vermogen en koppel.  Relevant is wel een multifuse-zekering in te bouwen die de stroom begrenst bij overbelasting en na onderbreking weer inschakelt. 
Deze zijn o.a. verkrijgbaar bij Conrad onder nummer 551287 :     http://www.conrad.nl/ce/nl/product/551309/Bourns-Multifuse-zekering-serie-MF-R-Afmetingen-102-mm-Afmetingen-102-mm-IH-16-A