---
layout: "overview"
title: "Fischertechnik Libelle"
date: 2020-02-22T08:34:41+01:00
legacy_id:
- /php/categories/2921
- /categories859d.html
- /categories3aa4.html
- /categories9c61.html
- /categoriesf1fd.html
- /categories6559.html
- /categoriesfa9b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2921 --> 
In de zomervakantie-2014 heb ik van Fischertechnik een Libelle met 4 onafhankelijk bewegende vleugels gemaakt. Een en ander geïnspireerd op de Festo-Bionic-Opter. 
Evenals m'n FT-smart-bird kan ik de Libelle wederom met de IR-afstand-bediening alle kanten op laten "vliegen". 


