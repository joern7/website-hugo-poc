---
layout: "image"
title: "Vleugel-uitslag- en -hoek-verstelling afh. v. centrale vleugelaandrijving en de gewenste vlucht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle43.jpg"
weight: "35"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39083
- /details279d-2.html
imported:
- "2019"
_4images_image_id: "39083"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39083 -->
De Festo-BionicOpter, die ook daadwerkelijk zelf kan opstijgen, voor- en achteruit kan vliegen, regelt zeer  nauwkeurig en snel zowel de Amplitude-vleugel-uitslag-verstelling (1) als de Vleugelhoek-verstelling (2). Een en ander afhankelijk het toerental van de centrale vleugelaandrijving en de gewenste vlucht. 

Middels een relatief trage centrale vleugelaandrijving (50:1) kan je in Fischertechnik zowel de verstelmogelijkheid van zowel de Amplitude-vleugeluitslag (1) als de Vleugelhoek (2) zien zoals eerder op de BBC-serie Earth flight en andere natuurdocumentaires. Het blijft fascinerend........
