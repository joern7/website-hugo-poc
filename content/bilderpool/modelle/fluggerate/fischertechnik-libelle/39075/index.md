---
layout: "image"
title: "Uitwijking naar rechts waarbij linker vleugels een grotere uitslag maken voor meer stuwkracht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle35.jpg"
weight: "27"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39075
- /details6f2d.html
imported:
- "2019"
_4images_image_id: "39075"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39075 -->
Libelle hangt daarbij over naar rechts waarbij de linker vleugels een grotere Amplitude-uitslag maken voor meer stuwkracht.