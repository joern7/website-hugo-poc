---
layout: "image"
title: "Libelle met wederom uitwijk-manouvre naar rechts"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle37.jpg"
weight: "29"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39077
- /detailsf25c.html
imported:
- "2019"
_4images_image_id: "39077"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39077 -->
Libelle kijkt en hangt daarbij over naar rechts waarbij de linker vleugels een grotere Amplitude-uitslag maken voor meer stuwkracht links.