---
layout: "image"
title: "Dalen van Libelle met vleugelhoek-verstelling via RoboPro + TX in download-modus met vragen ?!.."
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle44.jpg"
weight: "36"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39084
- /details57ac.html
imported:
- "2019"
_4images_image_id: "39084"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39084 -->
Het draaien van de vleugelhoek bepaalt de stuwrichting. De vleugelhoek-verstelling per vleugel vindt plaats door  XS-motoren (137096) via een RoboPro-programma door de TX-computer-interface.  
Voor herkenning van de 0-Positie gebruik ik reed-sensoren  met een buitendiameter van 3,8mm die direct ín een fischertechnik-bouwsteen geschoven kan worden. 
Als Pulsteller gebruik ik normale minitasters  37783.  Een veranderende Potmeter-weerstandswaarde zorgt voor een vleugelhoek-verstelling om de vereiste stuwrichting te blijven houden.  

Voor een betrouwbare automatische programma-start bij het inschakelen van de TX (in download-modus) blijkt een getrapte tijdvertraging noodzakelijk tbv de 0-Positie-stelling van elk van de 4 vleugel-processen.  
Dit blijft merkwaardig en roept vragen op !?......    Hoe kan dit worden verklaard ?    

