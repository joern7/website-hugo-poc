---
layout: "image"
title: "Fischertechnik Libelle in aanbouw 2    -binnenaanzicht zonder libelle-kop"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle12.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39052
- /details18b1.html
imported:
- "2019"
_4images_image_id: "39052"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39052 -->
Ik heb bewust de oude IR-afstandsbediening gebruikt ivm het hogere schakelbare vermogen én de automatische midden-positie terug-stel-mogelijkheid.  De nieuwe afstandbesturingen zijn software-matig beveiligd en schakelen bij iets hogere piek vermogens (te) snel uit.