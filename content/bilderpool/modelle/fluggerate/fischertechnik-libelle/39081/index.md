---
layout: "image"
title: "Dalen van Libelle met vleugelhoek-verstelling door XS-motoren via RoboPro in download-modus"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle41.jpg"
weight: "33"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39081
- /detailsa912.html
imported:
- "2019"
_4images_image_id: "39081"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39081 -->
De M3 van de afstandbediening drijft aan de staartzijde een 125:1  transmissie -Powermotor aan met een kabeltrommel waarmee de Libelle kan "stijgen" en "dalen".  Deze kabeltrommel is verbonden met een 5K-Potmeter (10 omwentelingen) -I6. Deze zorgt voor de vleugelhoek-verstelling door  XS-motoren (137096) via een RoboPro-programma door de TX-computer-interface.