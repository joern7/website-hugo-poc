---
layout: "image"
title: "Libelle met uitwijk-manouvre naar rechts"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle34.jpg"
weight: "26"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39074
- /details328a.html
imported:
- "2019"
_4images_image_id: "39074"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39074 -->
Libelle hangt daarbij over naar rechts waarbij de linker vleugels een grotere Amplitude-uitslag maken voor meer stuwkracht.