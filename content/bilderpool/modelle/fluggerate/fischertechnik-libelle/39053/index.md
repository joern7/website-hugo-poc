---
layout: "image"
title: "Fischertechnik Libelle in aanbouw 3         -onderaanzicht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle13.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39053
- /details0842.html
imported:
- "2019"
_4images_image_id: "39053"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39053 -->
M'n Fischertechnik -Libelle heeft (net als bij Festo) een centrale aandrijving M1 (50:1) waarmee de 4 vleugels middels de afstandbediening op 2 instelbare frequenties tegelijk op- en -neer kunnen bewegen.
De Amplitude-vleugel-uitslag-verstelling heb ik per 2 vleugel gekoppeld aan M2 waarmee ik de Libelle naar links en naar rechts kan laten kijken én tevens middels een kabeltrommel kan laten overhellen.  De aandrijving van de draaischijf bevindt zich bij de kop, bestaande uit een  125:1  transmissie -Powermotor. Deze motor drijft tevens de 2-delige rode kabeltrommel (32973) aan waarmee de gehele vogel via 2 draden naar links of naar rechts gaat hangen.  Eén motor zorgt hiermee voor het naar links, rechts of rechtuit vliegen, met bijbehorende kop- en romp-positie.  

De amplitude-vleugel-uitslag-verstelling gebeurd per vleugel middels een S-Motor + U-Getriebe. Deze heeft aan beide zijden een haakse overbrenging dmv een Rastkegel-tandwielen 35062 naar een zwarte worm M1 ( 35977) om een draaischijf-60  (31019)  te kunnen verstellen.  Op de Schneckenmutter 35973 heb ik een Gelenkklaue 15  (38446) geschoven die in de draaischijf-60 grijpt. Hiertoe heb ik één zijde van de Gelenkklaue afgeknipt.  Voor het ompolen van de S-motoren gebruik ik 2 oude EM-5-Relais  (35793) die door M2 van de IR-afstand-bediening worden getriggerd. Voor de eindposities gebruik ik diodes.

De M3 van de afstandbediening drijft aan de staartzijde een 125:1  transmissie -Powermotor aan met een kabeltrommel waarmee de Libelle kan "stijgen" en "dalen".  Deze kabeltrommel is verbonden met een 5K-Potmeter (10 omwentelingen) -I6. Deze zorgt voor de vleugelhoek-verstelling door  XS-motoren (137096) via een RoboPro-programma door de TX-computer-interface.