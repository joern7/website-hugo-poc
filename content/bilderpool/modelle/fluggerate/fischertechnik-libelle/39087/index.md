---
layout: "image"
title: "BionicOpter - Inspiration Libellenflug  -Deutsch  + NL"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle47.jpg"
weight: "39"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39087
- /details487d.html
imported:
- "2019"
_4images_image_id: "39087"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39087 -->
DE
Mit dem BionicOpter hat Festo die hochkomplexen Flugeigenschaften der Libelle technisch umgesetzt. Wie sein natürliches Vorbild kann das ultraleichte Flugobjekt in alle Raumrichtungen manövrieren, auf der Stelle fliegen und ganz ohne Flügelschlag segeln. 

Dreizehn Freiheitsgrade für einmalige Flugmanöver
Zusätzlich zur Steuerung der gemeinsamen Schlagfrequenz und der Verdrehung der einzelnen Flügel kommt in jedem der vier Flügel eine Amplitudensteuerung zum Einsatz. Das Schwenken der Flügel bestimmt die Schubrichtung. Mit der Amplitudensteuerung lässt sich die Stärke des Schubs regulieren. In Kombination kann die ferngesteuerte Libelle so nahezu jede Lageorientierung im Raum einnehmen.

Hochintegrierte Leichtbaukonstruktion
Ermöglicht wird das einzigartige Flugverhalten durch den Leichtbau und die Funktionsintegration: Sowohl Bauteile wie Sensorik, Aktorik und Mechanik als auch Steuerungs- und Regelungstechnik sind auf engstem Raum verbaut und aufeinander abgestimmt. 
Mit der ferngesteuerten Libelle demonstriert Festo die kabellose Echtzeit-Kommunikation, den permanenten Austausch von Informationen, das Zusammenführen verschiedener Sensorauswertungen sowie das Erkennen komplexer Ereignisse und kritischer Zustände.


NL
BionicOpter met 13 vrijheidsgraden
Zijn unieke vliegmanoeuvres bieden de BionicOpter dertien graden van vrijheid. Een motor in het onderste gedeelte van de behuizing neemt de aandrijving voor de gemeenschappelijke slagfrequentie van de vier vleugels (1e vrijheidsgraad) voor zijn rekening. Net als bij de echte libel, kunnen de vleugels van de BionicOpter vrij draaien
van horizontaal naar verticaal. Daarbij wordt elk van de vier vleugels individueel gestuurd door een servomotor en geroteerd tot 90 graden (2e, 3e, 4e, 5e vrijheidsgraad). 
Vier motoren op de vleugelgewrichten sturen de amplitude van de vleugels. Door een lineaire verschuiving in de vleugelwortel wordt het geïntegreerde krukmechanisme traploos zodanig ingesteld dat de piek tussen ongeveer 80 en 130 graden varieert (6e, 7e, 8e, 9e vrijheidsgraad). 
Het draaien van de vleugel bepaalt de stuwrichting. Met de vleugeluitslag-amplitude kan de stuwkracht worden geregeld. 
Er zijn ook vier extra vrijheidsgraden in de kop en de staart. Daartoe worden vier elektrische spieren van nitinol geïnstalleerd in het lichaam van de libel. De zogenaamde Shape Memory Alloys (SMA) trekken zich samen onder hitte en zetten opnieuw uit bij afkoeling. Via stroomtoevoer verkrijgt men een ultralichte actuatortechniek die
de kop horizontaal en de staart verticaal beweegt (10e, 11e, 12e, 13e vrijheidsgraad).


Samenvattend overall-overzicht +  inzicht in de werking + ontwikkeling Robot-Libelle : 

http://www.festo.com/net/SupportPortal/Files/248133/Festo_BionicOpter_de.pdf

http://www.festo.com/rep/de_corp/assets/pdf/GEO_Infografik_Roboterlibelle.pdf

Youtube-link Fischertechnik Libelle :
https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1