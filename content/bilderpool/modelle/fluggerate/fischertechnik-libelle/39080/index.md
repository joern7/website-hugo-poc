---
layout: "image"
title: "Vergrote vleugel-uitslag voor meer stuwkracht aan rechterzijde t.b.v. uitwijken naar links"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle40.jpg"
weight: "32"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39080
- /details9de3-2.html
imported:
- "2019"
_4images_image_id: "39080"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39080 -->
Vergrote Amplitude-vleugel-uitslag voor meer stuwkracht aan rechterzijde ten behoeve van uitwijken naar links
De Libelle kijkt en hangt daarbij naar links.