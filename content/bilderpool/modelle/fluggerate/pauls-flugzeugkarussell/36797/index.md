---
layout: "image"
title: "Das Flugzeug"
date: "2013-03-22T10:51:13"
picture: "paulsflugzeugkarussell2.jpg"
weight: "2"
konstrukteure: 
- "Paul Müller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36797
- /details6217.html
imported:
- "2019"
_4images_image_id: "36797"
_4images_cat_id: "2729"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36797 -->
... ist ein Leichtbauflugzeug mit einem Propeller.