---
layout: "image"
title: "Gesamtansicht"
date: "2013-03-22T10:51:13"
picture: "paulsflugzeugkarussell1.jpg"
weight: "1"
konstrukteure: 
- "Paul Müller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36796
- /detailsd0ac.html
imported:
- "2019"
_4images_image_id: "36796"
_4images_cat_id: "2729"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36796 -->
Nur durch Propellerkraft fliegt das Flugzeug mit beachtlicher Geschwindigkeit um den Turm.

Ein Video gibt's unter http://www.youtube.com/watch?v=ke0iuCVHTP0