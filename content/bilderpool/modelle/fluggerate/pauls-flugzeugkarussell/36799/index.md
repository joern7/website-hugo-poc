---
layout: "image"
title: "Schleifring (1)"
date: "2013-03-22T10:51:13"
picture: "paulsflugzeugkarussell4.jpg"
weight: "4"
konstrukteure: 
- "Paul Müller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36799
- /detailse0f9.html
imported:
- "2019"
_4images_image_id: "36799"
_4images_cat_id: "2729"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36799 -->
Der Schleifring und die federnd daran gedrückten Kontakte dienen dazu, Strom für den Propellermotor auf den drehbaren Arm zu bringen.