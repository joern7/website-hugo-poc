---
layout: "image"
title: "Schleifring (2)"
date: "2013-03-22T10:51:13"
picture: "paulsflugzeugkarussell5.jpg"
weight: "5"
konstrukteure: 
- "Paul Müller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36800
- /details0d03.html
imported:
- "2019"
_4images_image_id: "36800"
_4images_cat_id: "2729"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36800 -->
Die Buchsen des Schleifrings passen genau in die Löcher der Drehscheibe.