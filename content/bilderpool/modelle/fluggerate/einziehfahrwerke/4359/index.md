---
layout: "image"
title: "FWL02_03.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL02_03.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4359
- /details60c3.html
imported:
- "2019"
_4images_image_id: "4359"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4359 -->
Fahrwerk Nr. 2 ausgefahren und verriegelt.

Der Knackpunkt bei Einziehfarwerken ist der **Knickpunkt** im Gestänge, d.h. die Verriegelung erfolgt dadurch, dass man das menschliche Knie nachbildet, das in gestrecktem Zustand ja auch "verriegelt" ist. Aus dieser Position lässt sich das Fahrwerksbein nur durch Krafteinwirkung von der Rumpfseite her einknicken.