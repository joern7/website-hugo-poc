---
layout: "image"
title: "FWL07_01.JPG"
date: "2005-08-21T11:14:35"
picture: "FWL07_01.jpg"
weight: "39"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4610
- /details4bcb-2.html
imported:
- "2019"
_4images_image_id: "4610"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-08-21T11:14:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4610 -->
Für die einen ist das ein Heuschober auf Rädern. Für die anderen ist es der Anfang eines Großraumtransporters Antonov An-225 "Mriya" (siehe z.B. http://homepage.ntlworld.com/nick.challoner/aviation/pix/mryia/ ).

Einen Haken hat die Sache aber: das Fahrwerk verbiegt sich unter Gewichtsbelastung, deswegen wird es einen halbwegs maßstabsgerechten Flieger nicht tragen können.