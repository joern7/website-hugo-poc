---
layout: "image"
title: "FWL02_04.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL02_04.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4360
- /details27b4.html
imported:
- "2019"
_4images_image_id: "4360"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4360 -->
Nummer 2 eingezogen, von der Rückseite gesehen.