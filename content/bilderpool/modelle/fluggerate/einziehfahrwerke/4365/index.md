---
layout: "image"
title: "FWL03_05.JPG"
date: "2005-06-08T11:23:03"
picture: "FWL03_05.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4365
- /details1b49.html
imported:
- "2019"
_4images_image_id: "4365"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4365 -->
Nummer 3 von der Rückseite.