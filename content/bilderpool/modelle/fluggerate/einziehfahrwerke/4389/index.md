---
layout: "image"
title: "FWH11_03.JPG"
date: "2005-06-08T22:02:08"
picture: "FWH11_03.jpg"
weight: "38"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4389
- /detailsdc86-2.html
imported:
- "2019"
_4images_image_id: "4389"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4389 -->
Nr. 11 ganz eingezogen. Nach unten steht nichts mehr hinaus, da könnte jetzt noch eine Klappe hin, die das Loch verschließt.