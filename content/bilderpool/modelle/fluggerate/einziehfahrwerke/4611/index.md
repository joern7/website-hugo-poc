---
layout: "image"
title: "FWL07_02.JPG"
date: "2005-08-21T11:14:36"
picture: "FWL07_02.jpg"
weight: "40"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: ["Einziehfahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4611
- /detailsf96b.html
imported:
- "2019"
_4images_image_id: "4611"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-08-21T11:14:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4611 -->
Die Stummel der K-Achsen habe ich etwas gekürzt.