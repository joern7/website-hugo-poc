---
layout: "image"
title: "FWH13_02.JPG"
date: "2006-03-12T13:20:17"
picture: "FWH13_02.JPG"
weight: "50"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5863
- /detailsbf28-3.html
imported:
- "2019"
_4images_image_id: "5863"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:20:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5863 -->
