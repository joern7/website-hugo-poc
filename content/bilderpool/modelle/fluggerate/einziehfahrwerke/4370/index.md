---
layout: "image"
title: "FWL04_05.JPG"
date: "2005-06-08T11:23:04"
picture: "FWL04_05.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4370
- /detailsb24a.html
imported:
- "2019"
_4images_image_id: "4370"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4370 -->
Nummer 4 ist eingezogen. 
Das Auge lässt sich von der Mechanik leicht täuschen: dieses Fahrwerk hat tatsächlich nur 15 mm Unterschied zwischen 'drinnen' und 'draußen'.