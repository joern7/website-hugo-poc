---
layout: "image"
title: "FWL04_01.JPG"
date: "2005-06-08T11:23:03"
picture: "FWL04_01.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Schnecke", "35977"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4366
- /details3b75.html
imported:
- "2019"
_4images_image_id: "4366"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4366 -->
Fahrwerk (leicht) Nr. 4 ist wirklich nur für leichte Belastung geeignet. Unter Übergewicht drückt es die Zahnräder von den Schnecken herunter.