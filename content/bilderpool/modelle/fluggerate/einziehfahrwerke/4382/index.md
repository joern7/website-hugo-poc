---
layout: "image"
title: "FWH12_01.JPG"
date: "2005-06-08T22:01:54"
picture: "FWH12_01.jpg"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4382
- /details3949.html
imported:
- "2019"
_4images_image_id: "4382"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4382 -->
Fahrwerk (Heavy) Nummer 12 ist der große Bruder von Nummer 6: Die Mechanik ist die gleiche, nur dass eben Nr.12 deutlich größer ist.