---
layout: "image"
title: "FWH13_05.JPG"
date: "2006-03-12T13:21:33"
picture: "FWH13_05.JPG"
weight: "52"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5865
- /detailscc16.html
imported:
- "2019"
_4images_image_id: "5865"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:21:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5865 -->
