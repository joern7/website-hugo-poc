---
layout: "image"
title: "FWL07_06.JPG"
date: "2005-08-21T11:14:36"
picture: "FWL07_06.jpg"
weight: "44"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Einziehfahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4615
- /details29c3-2.html
imported:
- "2019"
_4images_image_id: "4615"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-08-21T11:14:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4615 -->
Die Schnecken sind per Bohrung und Drähtchen mit der Achse verstiftet.