---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight09.jpg"
weight: "9"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36418
- /details6224.html
imported:
- "2019"
_4images_image_id: "36418"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36418 -->
Met deze tragere centrale vleugelaandrijving kan je nu de vleugelverstellingen ook beter zien en volgen zoals op de BBC-serie Earth flight.