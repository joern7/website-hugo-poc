---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight12.jpg"
weight: "12"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36421
- /details5b04.html
imported:
- "2019"
_4images_image_id: "36421"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36421 -->
Ik heb de aan het plafond hangende "Fischertechnik-Smartbird-Earth-Flight"  uitgerust met de oude IR-afstandsbediening (ivm vermogen) voor inschakeling van de centrale vleugelaandrijving (1), positionering van de staarthoogte (2) en naar links- en naar rechts vliegen (3).
Elke vleugel heeft een "ondervleugel" voor voldoende lift, én een FT-Servo-motor met potmeter voor verdraaiing van de "eindvleugel" ten behoeve van de voorwaartse stuwkracht.
