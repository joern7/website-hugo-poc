---
layout: "comment"
hidden: true
title: "19182"
date: "2014-06-22T10:56:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Der Flug einer Libelle ist einzigartig: Sie kann in alle Raumrichtungen manövrieren, in der Luft stehen bleiben und ganz ohne Flügelschlag segeln. Durch die Fähigkeit, ihre beiden Flügelpaare unabhängig voneinander zu bewegen, kann sie abrupt abbremsen und wenden, rasant beschleunigen und sogar rückwärts fliegen. 

Mit dem BionicOpter hat Festo diese hochkomplexen Eigenschaften in einem ultraleichten Flugobjekt technisch umgesetzt. Schau mal : 
http://www.festo.com/cms/de_corp/13165.htm