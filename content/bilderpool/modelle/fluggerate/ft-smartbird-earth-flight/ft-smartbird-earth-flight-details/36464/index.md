---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails04.jpg"
weight: "4"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36464
- /details0d38.html
imported:
- "2019"
_4images_image_id: "36464"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36464 -->
Achse zwischender Kopf zum Schwanz-Drehscheibe.