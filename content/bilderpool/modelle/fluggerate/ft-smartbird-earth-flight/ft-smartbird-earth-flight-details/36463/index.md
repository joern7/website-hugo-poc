---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails03.jpg"
weight: "3"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36463
- /detailsa35f.html
imported:
- "2019"
_4images_image_id: "36463"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36463 -->
Schalter zum End- und Mitten-Positionierung der kombinierter Antrieb (Conrad 125:1 nr 224031)  für Lenkung der Kopf + Schwanz + Links/Rechts fliegen.

Ik heb de aan het plafond hangende "Fischertechnik-Smartbird-Earth-Flight"  uitgerust met de oude IR-afstandsbediening (ivm vermogen) voor inschakeling van de centrale vleugelaandrijving (1), positionering van de staarthoogte (2) en naar links- en naar rechts vliegen (3).