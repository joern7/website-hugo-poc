---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight  -Gesammt-Ansicht unten"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails14.jpg"
weight: "14"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36474
- /details37da.html
imported:
- "2019"
_4images_image_id: "36474"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36474 -->
Gesammt-Ansicht