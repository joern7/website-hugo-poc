---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight -Rechter-Ansicht"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails02.jpg"
weight: "2"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36462
- /details181e.html
imported:
- "2019"
_4images_image_id: "36462"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36462 -->
Kombinierter Antrieb (Conrad 125:1 nr 224031)  für Lenkung der Kopf + Schwanz + Links/Rechts fliegen.