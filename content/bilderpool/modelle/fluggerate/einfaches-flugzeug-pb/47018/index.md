---
layout: "image"
title: "flugz06.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47018
- /details7310.html
imported:
- "2019"
_4images_image_id: "47018"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47018 -->
