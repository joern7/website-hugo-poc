---
layout: "image"
title: "flugz02.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47014
- /details5e3b.html
imported:
- "2019"
_4images_image_id: "47014"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47014 -->
