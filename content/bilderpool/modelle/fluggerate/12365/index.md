---
layout: "image"
title: "Flieger 01.JPG"
date: "2007-10-28T13:06:38"
picture: "Flieger_01.JPG"
weight: "3"
konstrukteure: 
- "Andreas Diehlmann"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12365
- /detailsacfd.html
imported:
- "2019"
_4images_image_id: "12365"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2007-10-28T13:06:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12365 -->
Den Flieger hat mein Neffe Andreas (7) ganz alleine gebaut. Irgend etwas hat da abgefärbt, vermute ich :-)