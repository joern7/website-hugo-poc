---
layout: "image"
title: "Alti meter"
date: "2011-05-29T14:42:43"
picture: "altimeter.jpg"
weight: "1"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30669
- /detailse770.html
imported:
- "2019"
_4images_image_id: "30669"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30669 -->
The instrumentpanel is made off 2 layers of styreen. Also the clock hands are made of styreen. The dial is printed from the internet and the yellow-black movement indicator is a tube of cardboard with a yellow adhesive plastic strip. The "glass" is cut out a package and the "glassholder" is a wire of soft solder.

http://www.youtube.com/user/thedutchbuilder#p/a/u/0/alECymDbvFs