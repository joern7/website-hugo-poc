---
layout: "image"
title: "Fighter-43.JPG"
date: "2006-11-19T19:47:15"
picture: "Fighter-43.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7502
- /details8a2f.html
imported:
- "2019"
_4images_image_id: "7502"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:47:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7502 -->
Ein Fahrwerksbein im Detail. Die Spurstange aus den ft-Flitzern dient nur zur Führung; im ausgefahrenem Zustand ist das Gelenk auf Anschlag und trägt somit das Gewicht.