---
layout: "image"
title: "Fighter-55.JPG"
date: "2006-11-19T19:51:16"
picture: "Fighter-55.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7504
- /details1bed-2.html
imported:
- "2019"
_4images_image_id: "7504"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:51:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7504 -->
Das Cockpit muss noch ohne Instrumente und Hebel auskommen. Immerhin geht die Kanzel auf :-)