---
layout: "image"
title: "Fighter-54.JPG"
date: "2006-11-19T19:49:27"
picture: "Fighter-54.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7503
- /detailsa2d6.html
imported:
- "2019"
_4images_image_id: "7503"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:49:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7503 -->
Ein Blick von oben. Die Seitenruder drehen nicht parallel (ein Tribut an die Lenkgeometrie, die nicht so ganz wollte wie ich es gern gehabt hätte).