---
layout: "image"
title: "Detailansicht: Taumelscheibe"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor06.jpg"
weight: "6"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29883
- /details1a12.html
imported:
- "2019"
_4images_image_id: "29883"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29883 -->
Hier sieht man die Umsetzung im Modell. Die Justierung der Neigungswinkel der Rotorblätter erfolgt über die Antriebsachse: Der Motor wurde so befestigt, dass die Rotorblätter bei "Nullstellung" der Taumelscheibe exakt horizontal stehen.