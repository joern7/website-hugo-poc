---
layout: "image"
title: "Detailansicht: Hauptrotor"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor03.jpg"
weight: "3"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29880
- /detailsc51e.html
imported:
- "2019"
_4images_image_id: "29880"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29880 -->
Hier die Umsetzung der Konstruktion im Modell. Als Antrieb kommt ein 1:20-Powermotor zum Einsatz; die Stabilität der Gesamtkonstruktion wurde zuvor mit einem 1:8-Powermotor getestet.