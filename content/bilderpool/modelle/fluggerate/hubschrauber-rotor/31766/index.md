---
layout: "image"
title: "Hauptrotor mit drei Rotorblättern"
date: "2011-09-09T07:42:02"
picture: "Hauptrotor_mit_drei_Rotorbltten.jpg"
weight: "15"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/31766
- /details75cb.html
imported:
- "2019"
_4images_image_id: "31766"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-09-09T07:42:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31766 -->
Und hier die Umsetzung im Modell.