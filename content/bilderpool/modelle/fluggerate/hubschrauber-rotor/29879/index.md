---
layout: "image"
title: "Hauptrotor (Konstruktionszeichnung)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor02.jpg"
weight: "2"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29879
- /details3240.html
imported:
- "2019"
_4images_image_id: "29879"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29879 -->
Die Konstruktion des Hauptrotors lehnt sich an den genialen Entwurf von Harald (http://www.ftcommunity.de/categories.php?cat_id=797) an. Sie verwendet allerdings gängigere Bauteile; dafür hat sie eine etwas größere Bauhöhe.
Die beiden unteren Drehscheiben 60 sind in der Mitte mit einer schwarzen Freilaufnabe mit der Antriebsachse und miteinander durch drei Bausteine 15 verbunden; sie stabilisieren die Achse.