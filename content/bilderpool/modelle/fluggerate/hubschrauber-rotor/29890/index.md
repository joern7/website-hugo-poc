---
layout: "image"
title: "Detailansicht Pedale (Pitch-Verstellung Heckrotor)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor13.jpg"
weight: "13"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29890
- /details4327.html
imported:
- "2019"
_4images_image_id: "29890"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29890 -->
Mit den Pedalen wird - wie in einem echten Hubschrauber - der Pitch des Heckrotors verstellt. Über die beiden Seilrollen sind die Pedale gegenläufig miteinander verbunden.