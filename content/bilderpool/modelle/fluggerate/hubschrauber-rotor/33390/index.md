---
layout: "image"
title: "Hubschrauber-Rotor-Alternativ"
date: "2011-11-04T20:30:12"
picture: "Rotorkopf-alternativ_004.jpg"
weight: "18"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33390
- /details8b35.html
imported:
- "2019"
_4images_image_id: "33390"
_4images_cat_id: "2205"
_4images_user_id: "22"
_4images_image_date: "2011-11-04T20:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33390 -->
Hubschrauber-Rotor-Alternativ