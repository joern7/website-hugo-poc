---
layout: "comment"
hidden: true
title: "13515"
date: "2011-02-09T19:16:27"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Danke für die vielen "Blumen"! 
(Mein Sohn passt vor Stolz schon kaum noch durch die Türe...)
@Peterholland: Die Bilder von Max Buitings Modellen hatte ich im November "im Vorbeiflug" gesehen, aber dann nicht wiedergefunden - danke für den Link!
Gruß, Dirk