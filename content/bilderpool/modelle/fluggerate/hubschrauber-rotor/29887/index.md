---
layout: "image"
title: "Detailansicht: Heckrotor mit Pitch"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor10.jpg"
weight: "10"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29887
- /detailscc47.html
imported:
- "2019"
_4images_image_id: "29887"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29887 -->
Hier die Umsetzung im Modell. Vor allem wegen der um 90 Grad "gekippten" Montage benötigt man eine starke Rückstellfeder, die den oberen Lochstein auf den unteren drückt (und damit den Anstellwinkel der Rotorblätter zurückstellt).