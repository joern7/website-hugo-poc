---
layout: "image"
title: "Gesamtansicht Heckrotor"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor08.jpg"
weight: "8"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29885
- /details019a.html
imported:
- "2019"
_4images_image_id: "29885"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29885 -->
Die Konstruktion des Heckrotors ist vergleichsweise einfach. Die Lösung lehnt sich an die Konstruktion im Baukasten "Technical Revolutions" an, enthält aber einige Vereinfachungen und Verbesserungen.