---
layout: "image"
title: "FT_Star Wars_Millenium Falke"
date: "2016-10-19T16:58:21"
picture: "fischertechnikstarwarsmilleniumfalke20.jpg"
weight: "20"
konstrukteure: 
- "allsystemgmbh"
fotografen:
- "allsystemgmbh"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44645
- /details59eb.html
imported:
- "2019"
_4images_image_id: "44645"
_4images_cat_id: "3322"
_4images_user_id: "1688"
_4images_image_date: "2016-10-19T16:58:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44645 -->
