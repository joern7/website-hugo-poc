---
layout: "image"
title: "Antrieb"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk6.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/40585
- /details9528-2.html
imported:
- "2019"
_4images_image_id: "40585"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40585 -->
Die Rotorblätter werden über einen 50:1 Powermotor angetrieben.
Die Mechanik ist einfach gehalten, d.h. keine Taumelscheibe etc.