---
layout: "comment"
hidden: true
title: "20223"
date: "2015-02-22T18:03:46"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Supergut finde ich auch die Rundungen. Ohne vorgefertigte Bauteile ist sowas ja sehr schwierig nachzubilden, auch wenn hier die Streben dafür ziemlich leiden müssen. LG Martin