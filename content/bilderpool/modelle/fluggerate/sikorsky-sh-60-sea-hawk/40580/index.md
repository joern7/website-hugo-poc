---
layout: "image"
title: "Übersicht"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/40580
- /detailsfdda.html
imported:
- "2019"
_4images_image_id: "40580"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40580 -->
Ich habe versucht den Hubschrauber möglichst nahe am Original zu bauen.
Die genauen Abmessungen habe ich leider nicht mehr.
Wichtig war mir, dass die Proportionen halbwegs stimmen.