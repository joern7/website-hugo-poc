---
layout: "image"
title: "Cesspipsault-44.JPG"
date: "2005-06-09T23:01:53"
picture: "Cesspipsault-44.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4399
- /detailsbe0d.html
imported:
- "2019"
_4images_image_id: "4399"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4399 -->
Die Beinfreiheit in der 1. Klasse (Liegesitze in der hinteren Reihe) ist beeindruckend.

An der Flügelwurzel ist die Spannrolle für das Nylonseil erkennbar.

Das graue Statik-L-Profil 30 (mitte unten) ist zwar aus einem kaputten Winkelträger 120 selbstgeschnitzt, aber so etwas gab es auch mal ganz offiziell mit ft-Teilenummer.