---
layout: "image"
title: "Cesspipsault-48.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-48.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["35072"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4406
- /details20d1-2.html
imported:
- "2019"
_4images_image_id: "4406"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4406 -->
Nochmal die drei Schnecken für Seitenleitwerke und Hauptfahrwerk.

Die Antriebsachse fürs Hauptfahrwerk geht durch bis in die Flugzeugnase und treibt dort auch das Bugfahrwerk an.

Die Übersetzungsverhältnisse passen erstaunlich gut zusammen (hinten: dicke Schnecke + Z20 + 1/4 Achsdrehung, vorne kleine Schnecke + Z15 + 1/2 Drehung der Kurbel), d.h. die Fahrwerke sind fast gleichzeitig drinnen oder draußen. Den Rest besorgt der Schlupf zwischen den Z15 und den Kunststoff-Achsen. Wenn man also immer etwas weiter dreht als eigentlich nötig, hat man alle Fahrwerksbeine sicher drinnen bzw. draußen.