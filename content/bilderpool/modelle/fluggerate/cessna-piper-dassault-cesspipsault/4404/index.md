---
layout: "image"
title: "Cesspipsault-41.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-41.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4404
- /details5ec3.html
imported:
- "2019"
_4images_image_id: "4404"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4404 -->
Der E-Tec sort für das Geblinke der Positionslichter in den Flügeln.

Der gelbe Kasten links dient nur dazu, die Überlängen der Kabel zu verstauen.

An den Wellenstummel unten (fürs Fahrwerk) kommt vielleicht noch ein Motor, aber ohne Endschalter in den Fahrwerksbeinen ist das nichts Genaues.