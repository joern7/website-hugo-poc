---
layout: "comment"
hidden: true
title: "14840"
date: "2011-08-13T20:09:56"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Rob,
ich suche mir natürlich für das selbständige Erstellen von 3D-Dateien nur solche Teile aus, die mir übergreifend Fähigkeiten für den Querschnitt der vorkommenden Schwierigkeitsgrade vermitteln, um mir insbesondere bei noch nicht im ftD vorrätigen Teilen helfen zu können. Ansonsten ist für meinen aktuellen Bestand  diese besagte Library schon ein alter zu klein gewordener Hut ...
Viele Grüße Ingo