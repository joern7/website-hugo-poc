---
layout: "comment"
hidden: true
title: "14839"
date: "2011-08-13T19:58:12"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Manfred,
danke! In der Anfängerfase dauert natürlich alles länger. Entscheidend ist aber das Ergebnis, das natürlich mit der Realität der Vorbilder schon übereinstimmen sollte. Die Stirn- und Quadratzapfen z.B. sind natürlich nicht so nebenbei zu erstellen.
Interessant wäre es hier schon mal die CAD-Anwendungen zusammen zu bekommen, mit denen ft-Bauteile bisher von unseren Fans gearbeitet wurden ...
Du solltest aber bei deinen Veröffentlichungen auch mal an die Homepage von Michael Samek denken. Dort kann man dann bei Interesse die zugehörige ftm-Datei runterladen ... 
Viele Grüße Ingo