---
layout: "image"
title: "Baustein 30 grau mit Bohrung [1/5]"
date: "2011-08-12T22:54:27"
picture: "ftundgooglesketchup1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik, 3D-Nachkonstruktion Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/31569
- /detailsae96-2.html
imported:
- "2019"
_4images_image_id: "31569"
_4images_cat_id: "2351"
_4images_user_id: "723"
_4images_image_date: "2011-08-12T22:54:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31569 -->
Fast 68 Jahre alt werden mußte ich um mich auch mal am PC mit der Konstruktion von 3D-Teilen zu beschäftigen. Während meiner Ausbildung bis 1966 gab es solche Computer-Grafik noch nicht und in meiner beruflichen Praxis wurde das noch nicht gebraucht. Die CAD-Anwendung Google SketchUp 7.1 - mittlererweile gibt es die Version 8 - war kostenlos runterladbar und so lag es nahe mich damit an ft-Bauteilen zu versuchen. Schließlich fehlen ja noch "sehr" viele 3D-Teile im fischertechnik Designer :o) ...

Hier in der Flächendarstellung schattiert mit Kantenlinien z.B. mal 31004 Baustein 30 grau mit Bohrung und der noch nicht montierte Stirnzapfen. Leider ergab es sich noch nicht, daß mir ein einzelner beschädigter Stirnzapfen zum Zerlegen unterkam um der Konturen und Maße des Stiftes (M2,5x8,5?) habhaft zu werden. Diese Light-Version des  Google SketchUp Pro 7.1 verarbeitet leider noch keine Radien kleiner 1mm, deshalb der rechteckige Grund im Querschlitz des Zapfens.