---
layout: "image"
title: "Montagebeispiel [5/5]"
date: "2011-08-12T22:54:27"
picture: "ftundgooglesketchup5.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik, 3D-Nachkonstruktion Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/31573
- /detailseb8a.html
imported:
- "2019"
_4images_image_id: "31573"
_4images_cat_id: "2351"
_4images_user_id: "723"
_4images_image_date: "2011-08-12T22:54:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31573 -->
Kleines Montagebeispiel mit den drei verschiedenen ft-Flachsteinen 30 an zwei Bausteinen 30 grau.