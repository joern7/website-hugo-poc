---
layout: "image"
title: "BF1 Foto 3 Kopfneigung"
date: "2008-01-19T08:34:57"
picture: "BF1_Kopfschwenk_Achse_B.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13345
- /detailsd0f9.html
imported:
- "2019"
_4images_image_id: "13345"
_4images_cat_id: "1213"
_4images_user_id: "723"
_4images_image_date: "2008-01-19T08:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13345 -->
Spindelkopf in der Drehachse B in beiden Richtungen bis 90° neigbar