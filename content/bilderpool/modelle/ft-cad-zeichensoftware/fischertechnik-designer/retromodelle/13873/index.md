---
layout: "image"
title: "Pneumatik-Roboter 3"
date: "2008-03-07T07:03:15"
picture: "Pneumatik_Roboter_3_web.jpg"
weight: "15"
konstrukteure: 
- "Laserman, Original von fischertechnik Experimenta Schulprogramm"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13873
- /details3ff4.html
imported:
- "2019"
_4images_image_id: "13873"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13873 -->
In die Mulden paßt ein Tischtennisball.