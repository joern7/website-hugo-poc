---
layout: "image"
title: "Wagenheber"
date: "2008-03-07T07:03:15"
picture: "Wagenheber_web.jpg"
weight: "22"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 1_1 Maschinenkunde 1"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13880
- /details8b77-2.html
imported:
- "2019"
_4images_image_id: "13880"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13880 -->
Wenn man die Sperrklinke an der Seilrolle komplett nach oben hebt, kann man die Plattform wieder absenken.
Anheben hingegen geschieht durch hoch- und runterbewegen des linken Hebels.