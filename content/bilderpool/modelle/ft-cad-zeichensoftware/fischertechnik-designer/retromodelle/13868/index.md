---
layout: "image"
title: "Heu-Aufzug"
date: "2008-03-07T07:03:14"
picture: "Heu-Aufzug_web.jpg"
weight: "10"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 1_2 Steuerungen 1"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13868
- /detailsf90c.html
imported:
- "2019"
_4images_image_id: "13868"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13868 -->
Man hängt eine Last am Haken an und kurbelt diese nach oben. Oben angekommen, fährt der Schlitten automatisch nach links. Dann hält man das Gegengewicht fest und setzt die Last ab. Man läßt das Gewicht wieder los und der Schlitten fährt durch das Gegengewicht wieder nach rechts zur Ausgangsposition und der Haken fällt wieder herunter.