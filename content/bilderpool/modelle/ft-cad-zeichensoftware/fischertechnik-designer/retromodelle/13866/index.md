---
layout: "image"
title: "Geldwechsel-Automat"
date: "2008-03-07T07:03:14"
picture: "Geldwechsel-Automat_web.jpg"
weight: "8"
konstrukteure: 
- "Laserman, Original von fischertechnik Profi-Computing"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13866
- /details4f88.html
imported:
- "2019"
_4images_image_id: "13866"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13866 -->
Auch dieses Modell hat ein großes Experimentier-Potential.
Der Schieber kann nach links und rechts verschoben werden (durch einen Motor). Vor der entsprechenden Röhre angekommen (positioniert über Impulszähler) wird der Schieber nach vorne geschoben bis auf den Endschalter und gibt eine Münze aus. Dann fährt der Schieber wieder zurück auf den zweiten Endschalter.