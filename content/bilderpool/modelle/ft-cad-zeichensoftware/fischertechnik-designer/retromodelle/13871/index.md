---
layout: "image"
title: "Plotter 1991"
date: "2008-03-07T07:03:15"
picture: "Plotter_1991_web.jpg"
weight: "13"
konstrukteure: 
- "Laserman, Original von fischertechnik Profi-Computing"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13871
- /details9023-2.html
imported:
- "2019"
_4images_image_id: "13871"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13871 -->
Bei diesem Modell sind Schneckenantrieb, Zahnstangenantrieb, Positionierung über Impulstaster, und Positionierung über Lichtschranke (Kette) kombiniert. Der Stift wird über die Drehung der Nockenscheibe gehoben und gesenkt. 

Man kann damit Grafiken zeichnen.