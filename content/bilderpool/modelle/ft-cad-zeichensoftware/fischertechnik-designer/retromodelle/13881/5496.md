---
layout: "comment"
hidden: true
title: "5496"
date: "2008-03-08T13:13:02"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,
hallo Jürgen,

ich gebe Euch recht, wenn Ihr sagt, echte Fotos sind noch mal was anderes (auch wenn sie bei den von mir gebauten Modellen wahrscheinlich nicht viel anders wären als die 2D-Bilder aus dem Designer).

Aber 1. kann man den Designer kostenlos runterladen und sich die Modelle anschauen. Erst wenn man selber mehr als 10 Teile konstruieren will, muß / kann man sich die Vollversion kaufen. 

Und 2. Kann man schon mal ein Modell planen und bauen, ohne massenweise Teile kaufen zu müssen. Desweiteren kann man das Modell drehen und von allen Seiten betrachten. Dadurch wird der Nachbau um einiges leichter.

Denn gut geplant ist halb gearbeitet. Ich für meinen Teil kann nämlich eher weniger ins Blaue reinbauen. Ich plane mir meine Arbeiten lieber vorher.

Ich habe auch lange überlegt, ob ich mir den Designer für 100,- € kaufen soll. Aber es macht richtig Spaß damit zu arbeiten! Und schließlich ist es ein vollwertiges CAD / Konstruktionsprogramm, die normal einiges mehr kosten.

Aber ich werde gerne nochmal echte Fotos von den Modellen machen.   ;c)

Viele Grüße, Andreas.