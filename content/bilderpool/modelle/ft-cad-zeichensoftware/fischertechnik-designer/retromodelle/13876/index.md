---
layout: "image"
title: "Pneumatische Drehflügeltür"
date: "2008-03-07T07:03:15"
picture: "Peneumatische_Drehflgeltr_web.jpg"
weight: "18"
konstrukteure: 
- "Laserman, Original von fischertechnik Pneumatik"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13876
- /details7cc5.html
imported:
- "2019"
_4images_image_id: "13876"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13876 -->
