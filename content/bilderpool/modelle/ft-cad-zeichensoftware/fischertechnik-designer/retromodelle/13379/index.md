---
layout: "image"
title: "Retro2008 50-Hz-Uhr (4/4)"
date: "2008-01-24T16:53:33"
picture: "retrohzuhr4.jpg"
weight: "4"
konstrukteure: 
- "Original Stefan Falk, 3D-Retro Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13379
- /detailsb1dd.html
imported:
- "2019"
_4images_image_id: "13379"
_4images_cat_id: "1217"
_4images_user_id: "723"
_4images_image_date: "2008-01-24T16:53:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13379 -->
Vorderansicht des Uhrwerkes von unten bei Teilausblendung des Zifferblattringes. Zu sehen ist das Untersetzungsgetriebe, das vom Minutenzeiger abgegriffen mit einer Übersetzung 12:1 den Stundenzeiger antreibt.
 (2D-Ansicht aus 3D-Schattierung mit Kantenstil Profile)