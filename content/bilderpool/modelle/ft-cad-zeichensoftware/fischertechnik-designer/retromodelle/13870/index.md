---
layout: "image"
title: "Nietmaschine"
date: "2008-03-07T07:03:15"
picture: "Nietmaschine_web.jpg"
weight: "12"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 2_2 Maschinenkunde 3"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13870
- /detailse653.html
imported:
- "2019"
_4images_image_id: "13870"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13870 -->
Die Maschine arbeitet mit einem sogenannten Kniegelenk. Dieses erzeugt eine erstauliche Druckkraft. Man kann ja mal einen Finger auf den roten, runden Nietteller legen. Keine Angst - es passiert nichts! Ich habe mich auch schon von der erstaunlichen Kraft dieses Modells überzeugt.

Wenn man das Modell motorisiert, muß man ein wenig mit der Schalterstellung (E1) experimentieren.

Um die Maschine zu starten, muß man beide Taster auf der Grundplatte drücken. Dies hat die Bewandnis, damit man sich nicht die Finger klemmt, wenn man nur einen Taster drücken würde.