---
layout: "image"
title: "ft-designer Teile suchen Strg + F"
date: "2016-01-30T17:19:51"
picture: "ftdesigner8.jpg"
weight: "8"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42850
- /detailsad49.html
imported:
- "2019"
_4images_image_id: "42850"
_4images_cat_id: "3185"
_4images_user_id: "2303"
_4images_image_date: "2016-01-30T17:19:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42850 -->
Neu erstellte Bauteile im ftdesigner suchen