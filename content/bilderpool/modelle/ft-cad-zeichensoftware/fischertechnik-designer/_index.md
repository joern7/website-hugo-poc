---
layout: "overview"
title: "fischertechnik-Designer"
date: 2020-02-22T08:39:53+01:00
legacy_id:
- /php/categories/1213
- /categoriesd18e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1213 --> 
Nachbauten, Retros und Neuentwicklungen von ft-Modellen mit dem Konstruktionsprogramm ft-Designer