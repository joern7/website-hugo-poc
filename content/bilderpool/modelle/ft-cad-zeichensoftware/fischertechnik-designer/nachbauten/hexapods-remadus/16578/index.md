---
layout: "image"
title: "[1/2] pneumomechanisch"
date: "2008-12-11T00:56:24"
picture: "hexapod1.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "3D-Nachbau, Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16578
- /details40a4.html
imported:
- "2019"
_4images_image_id: "16578"
_4images_cat_id: "1501"
_4images_user_id: "723"
_4images_image_date: "2008-12-11T00:56:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16578 -->
Zwischendurch aus meinem 3D-Archiv von mir 08/2007 nachgebaute Baugruppen ...