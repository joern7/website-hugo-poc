---
layout: "image"
title: "[2/9] Rädergetriebe 1/2_"
date: "2009-09-11T21:40:48"
picture: "dremavonclausind2.jpg"
weight: "2"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24916
- /detailsd76c.html
imported:
- "2019"
_4images_image_id: "24916"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24916 -->
Baugruppe Spindelgetriebe mit insgesamt 18 mechanischen Vor- und Rückwärtsgängen (3D-Schattierung ohne Bauteilkanten, Bauteile der Baugruppe in Originalfarbe)
Eingesetzt sind hier zwei 3-stufige Schieberäder (je 2 Stangenhebel rechts vorn und Synchronräder links) und ein 2-stufiges (Bauteilhebel oben).
Das Rädergetriebe wird von zwei Power-Motoren 50:1 (rot) angetrieben