---
layout: "image"
title: "[6/9] Support 3/3"
date: "2009-09-11T21:41:20"
picture: "dremavonclausind6.jpg"
weight: "6"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24920
- /detailsc6ba.html
imported:
- "2019"
_4images_image_id: "24920"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:41:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24920 -->
Baugruppe 3 des Supports: Oberschlitten mit Vorschub sowie Drehbarkeit von Hand und Werkzeugspannklaue (Profilkantendarstellung)