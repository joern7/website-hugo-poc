---
layout: "image"
title: "[7/9] Reitstock 1/2"
date: "2009-09-11T21:41:20"
picture: "dremavonclausind7.jpg"
weight: "7"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24921
- /detailsc93c.html
imported:
- "2019"
_4images_image_id: "24921"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:41:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24921 -->
Die Mechanik der Pinole, aus Sichtgründen Teileausblendungen am Reitstockgehäuse (Profilkantendarstellung)