---
layout: "image"
title: "[5/9] Support 2/3"
date: "2009-09-11T21:41:19"
picture: "dremavonclausind5.jpg"
weight: "5"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24919
- /details66e2-2.html
imported:
- "2019"
_4images_image_id: "24919"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:41:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24919 -->
Baugruppe 2 des Supports: Querschlitten mit manuellem und automatischem Planvorschub.
Bildeinstellung manuelle Betätigung  (Profilkantendarstellung).
Das Drehkranz-Unterteil für den Oberschlitten auf den vier BS5 ist hier aus Sichtgründen ausgeblendet.