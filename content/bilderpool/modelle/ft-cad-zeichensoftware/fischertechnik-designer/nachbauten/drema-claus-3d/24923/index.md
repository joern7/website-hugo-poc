---
layout: "image"
title: "[9/9] Dreibackenfutter"
date: "2009-09-11T21:41:20"
picture: "dremavonclausind9.jpg"
weight: "9"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24923
- /details9782.html
imported:
- "2019"
_4images_image_id: "24923"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:41:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24923 -->
Manuelle selbstzentrierende Spannfuttermechanik, vorderer Spannbacken aus Sichtgründen ausgeblendet (Bauteilkantendarstellung)