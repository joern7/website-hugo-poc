---
layout: "image"
title: "BF1 Foto 2 Maschine"
date: "2008-01-19T08:34:57"
picture: "BF1_Maschine.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13344
- /details88fe.html
imported:
- "2019"
_4images_image_id: "13344"
_4images_cat_id: "1213"
_4images_user_id: "723"
_4images_image_date: "2008-01-19T08:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13344 -->
Zum Modell der Maschine selbst gehören u.a. folgende Achsen: Kreuztisch mit den Linearachsen X und Y, Spindelkopfhöhe Linearachse Z. Die 2 Drehzahlbereiche und ihr Leerlauf werden mit dem Schalthebel rechts hinten ein- und ausgerückt. Der Hebel rechts vorn dient zum Ein- und Ausrücken des Bohrvorschubes von Hand und der Fräszustellung. Bei Stellung links der vorn quer liegenden Reversierwelle ist die Fräszustellung und rechts der Bohrvorschub von Hand eingerückt. Der Handvorschub zum Bohren erfolgt über den gleichen Hebel und die Fräszustellung links vorn über den "Stellring mit Scale". Ist die Verkleidung geschlossen, kann die axiale Stellung der Arbeitsspindel durch einen Schlitz an der Vorderseite des Spindelkopfes bei den Funktionen Bohren mit Handvorschub und Fräszustellung verfolgt werden.