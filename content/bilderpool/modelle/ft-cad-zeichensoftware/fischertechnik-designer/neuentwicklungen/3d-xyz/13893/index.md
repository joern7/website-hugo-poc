---
layout: "image"
title: "[7/7] 3D-XYZ-G"
date: "2008-03-11T07:42:12"
picture: "dxyznachtrag1.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13893
- /details3c02.html
imported:
- "2019"
_4images_image_id: "13893"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-11T07:42:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13893 -->
Hier auf mehrfachen Wunsch die Ergänzung zu Bild [1/7] bis [6/7] 3D-XYZ-G mit Einzelheiten zur Gleitführung des Modells 3D-XYZ

Ideenquelle:
Der Zufall brachte es, als ein Verbinder 30 in der Nut eines Alu-Profiles nur durch sein Gewicht hinunterrutschte.
     Funktionsprinzip:
Trockene Gleitführung ohne bewegliche Bauteile; Werkstoffpaarung Alu anox mit Spritzgußhaut Nylon; Gleitbahn Alu-Profil; Schlittenkufe Baustein 15; Führungelement Verbinder 15 fest mit Rundzapfen 1 und gleitend mit Flachzapfen 2; spielausgleichende Eigenschaft durch die Federung des Verbinders 15; je Schlitten 2x2 Verbinder 15
     Toleranzklassen:
Dem Bauteil Alu-Profil wurden bei den Nuten 3 geeignete Tolenzklassen zugeordnet eng TKe (Bild links oben), mittel TKm (Bild mitte oben) und weit TKw (Bild rechts oben)
     Gleitsicherung-stufig (Ist-Zustand):
Bei TKe und TKm erhält der Verbinder 15 an Zapfen 2 Nuten gemäß Bild (Simulation Schwarze Scheibe)
Vorteil: Schnelle Präparation der Verbinder 15
Nachteil: Maschine mit Sägeblattbreiten 0,5, 1,0 und 1,5mm erforderlich
     Gleitsicherung-stufenlos (Ziel-Zustand):
Für die Tolenzklassen TKe, TKm und TKw erfolgt ein Einschleifen mit Schleifpaste unter Nutzung der Vorrichtung nach Bild. Das Einschleifen der Verbinder 15 am  Zapfen 2 (gelb) erfolgt im Paarungsteil Alu-Profil  in seiner vollen Länge
Vorteil: Optimale Anpassung an TK-Istwert von Hand ohne große Schwächung  des Verbinders 15 mit vergleichbaren Laufeigenschaften zwischen den Achsen
Nachteil: Zeitaufwändige Präparation
     Experimentalaufbau (aktuell):
X-Achse TKe Verbinder 15 mit breiter Nut
Y-Achse TKw Verbinder 15 original
Z-Achse TKm Verbinder 15 mit schmaler Nut
     Funktionsreserven:
1. Einschleifen Verbinderzapfen 2 in Alu-Profil
2. Schmierung mit Vaseline
     Beprobungen u.a.:
Lichtspalt-Prüfung der Alu-Profile Gleitbahn
Einmessung Bodenviereck mit Haarwinkel 
Getriebmechanischer  Achsenlauftest XYZ per Handkurbel und mit Schrittmotor (geschmiert nur per Hand)
Geplotteter Achsenlauftest XY(Z) per Hand
     Fazit 1:
Die bisherigen Beprobungserkenntnisse haben die Fortführung der Entwicklung bestärkt, siehe 3D-YXZ (2D2) und 3D-XYZ (2D3). Die Nutzung der Verbinder mit stufiger Anpassung an die Nuttoleranzen ist noch unbefriedigend und führt besonders bei TKe zu einer Schwächung der Querstabilität. In der nächsten Anpassungsstufe Einschleifen wird ein befriedigender Abschluß der Entwicklungsabteilung Gleitführung gesehen. Es bleibt einkalkuliert, daß hier durch das Anschleifen der Spritzgußhaut der Verbinder 15 dann eine Schmierung erforderlich sein könnte. Eine evtl. erforderliche Verdrehsicherung von Zapfen 1 wird durch Einlagen (Draht axial) in den Spalt (vor dem Einschleifen!) erreicht.
(C) Udo2, Stand 09.03.2008
