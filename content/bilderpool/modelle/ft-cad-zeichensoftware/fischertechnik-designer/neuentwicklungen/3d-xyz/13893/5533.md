---
layout: "comment"
hidden: true
title: "5533"
date: "2008-03-12T09:01:13"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
noch eine Ergänzung am Beispiel der X-Achse. Der Verbinderhals dient hier hauptsächlich der waagerechten Stabilität quer und der Zapfen 2 der vertikalen senkrecht zur X-Achse. Es wirkt zwar hier das Gewicht des übrigen Gebildes von Y und Z mit Arbeitskopf, aber die angedachten Anwendungen entwickeln mehr und weitere Kräfte als "nur" das vertikal auf das Papier übertragene Gewicht eines Plotterstiftes mit Halterung. Die Achsen Y und Z hängen noch zusätzlich sprichwörtlich gesehen "an der Wand" ihrer Bettführung.
Gruß Udo2