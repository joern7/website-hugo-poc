---
layout: "image"
title: "[1/7] 3D-XYZ-G"
date: "2008-03-05T18:08:58"
picture: "dxyzlinearrobotervorabvorstellungd1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13838
- /details2daa.html
imported:
- "2019"
_4images_image_id: "13838"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-05T18:08:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13838 -->
Zweite eigene Neuentwicklung (ft-Neuling seit 12/2006) zum Kennenlernen der Eigenschaften des Bauteils Alu-Profil, vorgestellt vorab als 2D-Kopien des 3D-Fensters der Konstruktionsarbeit. Die Entwicklung hat zum Ziel eine Modellbasis 3D-linear für die Erschließung von Anwendungen inklusive einer Erweiterung mit Drehachsen. Hier das aktuelle Gestell Version 3 mit der Getriebevariante Schrittmotor Übersetzung 1:1 und Schneckentrieb. Der abgebildete Arbeitsstand ist seit 10/2007 aufgebaut und befindet sich mit größeren Unterbrechungen durch Parallelarbeiten an weiteren Modellprojekten in der Beprobung zunächst von Hand. Die Linearwege betragen max. X=335mm, Y=219mm und Z=94mm, die Schrittgröße(rechnerisch) 0,104mm/Schritt und die Lineargeschwindigkeit 5mm/Motorumdrehung.
Durch eine Unachtsamkeit hat das Modell auch schon einen Sturz aus 1,50m Höhe von einem Turm Box 1000 hinter sich. Schadenprotokoll: Aufschlag mit der hinteren Außenkante des X-Motors, Zerlegung in den Führungsachsen in 3 Teile, Riß eines BS15 an der Halterung des X-Motors und Abriß eines Schwenkzapfens der X-Schneckenmutter an der  Längsseite Ymax, Dejustierung der Parallelität der  X-Schlitten 0,5mm.  Instandsetzung knapp 30min mit Wechsel des beschädigten BS15, Montage und Justierung.

Eingefügt 08.09.2010:
Vorstellung praktischer Modelltest http://www.ftcommunity.de/details.php?image_id=28068#col3