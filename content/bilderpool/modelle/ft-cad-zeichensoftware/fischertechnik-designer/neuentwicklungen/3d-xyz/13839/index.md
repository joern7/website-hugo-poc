---
layout: "image"
title: "[2/7] 3D-XYZ-G"
date: "2008-03-05T18:08:58"
picture: "dxyzlinearrobotervorabvorstellungd2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13839
- /details463d-2.html
imported:
- "2019"
_4images_image_id: "13839"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-05T18:08:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13839 -->
Da Elastizität und Teiletoleranzen des ft-Systems Modellentwickler ständig beschäftigen nun im Namen aller Betroffenen durch Umkehr des Sachverhaltes eine der dazu schon lange fälligen Antworten. Hier die vier Linearschlitten Schlittenpaar X und Schlittenkreuz YZ. Die Schlittenkonstruktion hat keine beweglichen Teile und besteht ausschließlich aus Teilen des ft-Systems. Es könnte sein, daß die Einfachheit dieser Lösung nicht mehr zu unterbieten ist. Bei der manuellen Beprobung der drei Achsen wurden Systemeigenschaften gefunden, die die Fortführung der Entwicklung als sinnvoll erscheinen lassen. Die automatische Aufzeichnung der Linearbewegung von X und Y durch einen Schreibstift kam einer Stiftauflösung von 5 Linien/mm nahe. Die Fortführung der Beprobung des Modells verbunden mit seiner Weiterentwicklung durch eine geeignete Steuerung wird zeigen, inwieweit die gefundenen Eigenschaften reproduzierbar sind (Fotos und Video folgen dann).