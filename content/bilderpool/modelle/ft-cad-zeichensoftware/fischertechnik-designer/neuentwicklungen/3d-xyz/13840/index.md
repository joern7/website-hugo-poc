---
layout: "image"
title: "[3/7] 3D-XYZ-G"
date: "2008-03-05T18:08:59"
picture: "dxyzlinearrobotervorabvorstellungd3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13840
- /details7bac-3.html
imported:
- "2019"
_4images_image_id: "13840"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-05T18:08:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13840 -->
Das Modell legt seine Lineareigenschaften am besten offen, wenn es sie während seiner Bewegungen aufzeichnet. Dazu wurde ein Stifthalter entwickelt. Das ist die aktuelle Version 5 in praktischer Erprobung. Die Baugruppe ist modular ausgelegt und kann beidhändig mit einer Bewegung vom Z-Schlitten abgenommen bzw. wieder aufgesetzt werden.
Da man als erwachsener Mensch gemäß der allseits bekannten respektlosen Bewertung "Das Kind im Manne" bei der Beprobung des Modells auch Spielphasen durchmacht, wurde dabei die Idee geboren per Software einmal eine Steuerung zu entwickeln, die es ermöglicht per Z-Schlitten einen Schreibstift nach dem Prinzip Einstrichlinie so zu steuern, daß drei deutlich voneinander unterscheidbare Linienstärken in Folge entstehen. Im manuellen Versuch war das schon ansatzweise erkennbar. Das wäre dann eine Möglichkeit, auch die Z-Eigenschaften des Systems ausreichend zu beproben und offenzulegen.