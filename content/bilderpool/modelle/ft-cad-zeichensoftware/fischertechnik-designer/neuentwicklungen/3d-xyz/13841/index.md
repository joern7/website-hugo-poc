---
layout: "image"
title: "[4/7] 3D-XYZ-G"
date: "2008-03-05T18:08:59"
picture: "dxyzlinearrobotervorabvorstellungd4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13841
- /details323c.html
imported:
- "2019"
_4images_image_id: "13841"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-05T18:08:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13841 -->
Aus den Versuchen eine Möglichkeit zum Aufspannen des Papiers, als Auszug dargestellt an der Anschlagecke X0Y0. Die Lösung ermöglicht das allseitige Zeichnen bis 3mm vom Rand. Die Bauplatte 15x15 rot sichert die Parallität der Anschlagkante Y0 zur X-Achse. Die Rechtwinklichkeit der Anschlagkante X0 zu Y0 sichert die erste Nut der Arbeitsplatte 270x390. Die Bausteine 5 gelb dienen als Anschlagkanten. Baustein 5 gelb für X0 hat eine Doppelfunktion für Anschlag und Spannung. Notwendig sind nur 3 Anschläge, davon 2 an Y0. Gespannt wird das Papier durch Aufschieben der Federnocken in die Nut der Bausteine 5 vom Papier zum Rand hin. Der Überstand der Federnocken beträgt dann 2mm. Eine harte Unterlage unter dem Papier wie kupferkaschiertes Hartpapier gewebefrei ist sinnvoll, damit sich das Papier am Spannelement nicht wölbt. Unter der Unterlage kann zum Höhenausgleich noch Papier oder Pappe untergelegt werden. Das Papier muß aufgespannt leicht klemmen und natürlich auch am Anschlag anliegen, was hier in der Simulation wegen seines Durchscheinens nicht möglich war. Eine Stifthalterung, deren Auflagekraft durch ihr Gewicht begrenzt oder durch Federn bestimmt wird, ist Voraussetzung.