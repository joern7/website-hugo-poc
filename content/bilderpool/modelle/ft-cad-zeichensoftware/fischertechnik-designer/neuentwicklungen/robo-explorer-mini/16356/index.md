---
layout: "image"
title: "(3/7) Fahrwerk ohne Ketten"
date: "2008-11-21T10:10:15"
picture: "roboexplorerminiind3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16356
- /detailsdcad.html
imported:
- "2019"
_4images_image_id: "16356"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16356 -->
Für die Lagerung der Zahnräder müssen die Schneckenmuttern m1,5 ausgedreht werden. Da ihre Bohrungen aber schon größer sind als die Klemmhülsen der Zahnräder, kann nur der ausgedrehte Schneckengang zur Lagerung genutzt werden. Die drei Klemmbuchsen 10 und die äußeren Lager fixieren axial die Lage der Kernachse und der beiden freilaufenden Zahnradblöcke.