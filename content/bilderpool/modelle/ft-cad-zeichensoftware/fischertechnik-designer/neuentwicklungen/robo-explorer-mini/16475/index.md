---
layout: "image"
title: "[4/5] Optimierungen im Chassis"
date: "2008-11-23T12:49:04"
picture: "roboexplorermini4.jpg"
weight: "11"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16475
- /details6596-2.html
imported:
- "2019"
_4images_image_id: "16475"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-23T12:49:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16475 -->
Das Ergebnis der Sichtung meiner Materialien zur Anfertigung der beiden Spezialadapter erlaubt mir vorn eine Kürzung um 5mm und damit hinten eine Verlängerung um 5mm. Das war notwendig, weil die Steckeranschlüsse am 3D-Teil Power Motor nicht an der richtigen Stelle sitzen ...