---
layout: "image"
title: "(5/7) Fahrwerk mit Akku"
date: "2008-11-21T10:10:16"
picture: "roboexplorerminiind5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16358
- /detailsb2da.html
imported:
- "2019"
_4images_image_id: "16358"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16358 -->
Der Akkupack für den vorläufigen Einsatz mit dem ROBO Interface besteht aus sieben Mignon-Akkus 1,2V für eine Nennspannung 8,4V. Da diese Akku-Zellen schon bis zu 2900mAh erhältlich sind, bekommt das Akkupaket ordentlich Kraft. Mit weniger Geld kann man aber auch schächere Akku-Zellen einsetzen. Die Mignon-Zellen werden - hier leider nicht darstellbar - in Einzelboxen aufgenommen. Das ist zwar teurer, ergibt aber eine niedrige Einbauhöhe und einen schnelleren Zugriff auf die Zellen. Die Winkelträger 120 sind bis hierher zunächst auf je zwei Strebenadapter aufgesetzt.