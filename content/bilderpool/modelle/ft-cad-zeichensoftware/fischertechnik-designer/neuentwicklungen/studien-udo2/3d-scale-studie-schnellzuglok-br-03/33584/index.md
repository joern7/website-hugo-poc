---
layout: "image"
title: "[3/7] BR 003 131-1 Führerhaus und Feuerbüchse"
date: "2011-11-29T11:56:35"
picture: "dscalestudieschnellzuglokbrwitte3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/33584
- /details3cc6.html
imported:
- "2019"
_4images_image_id: "33584"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33584 -->
Die geometrische und maßstäbliche Treue von Führerhaus und Feuerbüchse zum Vorbild war hier eine der größeren Herausforderungen. Natürlich fehlen z.B. noch die Türen.