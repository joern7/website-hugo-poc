---
layout: "image"
title: "[6/7] BR 003 131-1 Innenansicht längs"
date: "2011-11-29T11:56:36"
picture: "dscalestudieschnellzuglokbrwitte6.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/33587
- /detailscb2a.html
imported:
- "2019"
_4images_image_id: "33587"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33587 -->
Die Modellstudie - es sind immerhin schon 832 Bauteile - habe ich als Halbseitenmodell ausgeführt. Ein Blick auf die "Rückseite" zeigt den Aufbau des statischen Modellgerippes. Leider ist es so, daß auch der fischertechnik-Designer Elemente maßstäblich kleiner 1:1 mit absinkender Auflösung und Transparenz abbildet. Das liegt mit daran, daß sich die Strichstärke der Profilkanten maßstäblich nicht verändert und bei Verkleinerungen dicker wirkt.