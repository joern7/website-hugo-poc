---
layout: "comment"
hidden: true
title: "15792"
date: "2011-11-30T15:55:59"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
mit dem erforderlichen Praxiswissen über die Eigenschaften der Einzelteile und bei ihrem Zusammenbau kann man schon sehr zutreffend das praktische Aufbauergebnis vorausdenken. Die Studie ist ja noch in meinem ft-Elevenjahr entstanden (Einstieg 12/2006). Da sie nur eine! Baufase hat, werde ich jetzt erst mal strukturiert das Ganze in 10er Baufasen-/Baugruppen schon wegen meiner noch offenen Funktionswünsche neu auffädeln. Sollte ich zur Convention 2012 kommen können, habe ich aber noch andere zuvor hier in der ft:c vorgestellte Modelle mitzubringen.
Gruß, Ingo