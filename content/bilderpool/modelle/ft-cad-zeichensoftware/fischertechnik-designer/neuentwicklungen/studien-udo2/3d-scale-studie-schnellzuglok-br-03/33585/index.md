---
layout: "image"
title: "[4/7] BR 003 131-1 Heusinger-Steuerung"
date: "2011-11-29T11:56:35"
picture: "dscalestudieschnellzuglokbrwitte4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/33585
- /details5683.html
imported:
- "2019"
_4images_image_id: "33585"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33585 -->
Eine funktionstüchtige Heusinger-Steuerung zumindest was ihre Gestängebewegungen anbetrifft ist eines meiner Modellziele. Leider haben die maßstäblich annäherbaren Statikstreben Löcher. So sieht das Gestänge bis auf die Kurbelstange damit leider weniger gut aus.