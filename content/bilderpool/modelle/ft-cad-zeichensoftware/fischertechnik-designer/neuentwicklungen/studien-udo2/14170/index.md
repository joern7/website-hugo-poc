---
layout: "image"
title: "[2] LKW-Achse mit Blattfeder"
date: "2008-04-04T21:55:04"
picture: "studienvonudo1_2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/14170
- /details0ac0.html
imported:
- "2019"
_4images_image_id: "14170"
_4images_cat_id: "1308"
_4images_user_id: "723"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14170 -->
Verstärkte Ausführung der Blattfedern (I-Streben mit Loch) mit 4x90 und 4x60 gegenüber Bild [1] mit 3x90, 3x60 und 2x30. Die Teile Verbindungsstopfen und Klemmbuches 5 der Blattfedersicherung bei Kranhub entfallen hier.