---
layout: "image"
title: "Wälzlager, axial (2/4)"
date: "2008-09-30T09:58:22"
picture: "axiallagereinstellbar2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15663
- /detailsf626-2.html
imported:
- "2019"
_4images_image_id: "15663"
_4images_cat_id: "1439"
_4images_user_id: "723"
_4images_image_date: "2008-09-30T09:58:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15663 -->
Mein bewusst provokativ als Illustration gedachter Lösungsansatz zur Einstellbarkeit des Umkreises der Wälzkörper mit axialer Achse auf den Innendurchmesser der Laufringe zeigt wie man es nicht machen sollte. Er hat zwar einen Vorteil, dafür aber  zwei Konstruktionsfehler. Als Vorteil ermöglicht er die Reduzierung der axialen Bauhöhe um 9 mm auf 44 mm. Der erste Fehler ist die Übertragung der Axialkräfte über axial instabile Bereiche der beiden Lauffringe. Der zweite Fehler verursacht mit den kleinen Laufrollen und der damit grösseren Krümmung ihrer tonnenförmigen Zylindermantel einen schlechteren Übergang über die gestossenen Verbindungen der Laufringe.