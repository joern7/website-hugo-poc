---
layout: "image"
title: "Wälzlager, axial (1/4)"
date: "2008-09-30T09:58:21"
picture: "axiallagereinstellbar1.jpg"
weight: "1"
konstrukteure: 
- "Nachbau Udo2, Original Remadus"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15662
- /details93d1.html
imported:
- "2019"
_4images_image_id: "15662"
_4images_cat_id: "1439"
_4images_user_id: "723"
_4images_image_date: "2008-09-30T09:58:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15662 -->
Der 3D-Nachbau des Originals mit Vertikalachse von Martin Roman [Remadus] vorgestellt auf der Convention 2008.
Funktion:
Der obere gelbe Laufring dreht sich über dem auf der Arbeitsplatte fixierten unteren. Die Räder 23 sind die Wälzkörper und am rot/schwarzen Bauteilring dem Wälzkäfig drehbar gelagert. Die roten Räder 23 übertragen axial die Belastung vom oberen Laufring-Zylindermantel über den des unteren Laufringes auf die Arbeitsplatte. Die weissen und schwarzen Räder 23 halten beide Laufringe radial zentriert übereinander. Der Wälzkäfig folgt der Drehrichtung des oberen Laufringes.
Einsatzmöglichkeiten:
Die Originallösung ist nutzbar für Drehtische. Wenn es gelingt durch konstruktive Veränderungen die beiden Laufringe trotz ihrer gegenläufigen Bewegung axial stabilisiert zusammenzuhalten könnte ein Einsatz mit horizontaler Achse oder als Drehkränze möglich werden. Letztere sind uns ja konventionell in mannigfaltigen Dimensionen mit oft gewaltigem Materialeinsatz hinreichend bekannt.
Aufgabenstellung der Modifikation:
Durch eine konstruktive Änderung des Originals soll erreicht werden, dass die Umkreisdurchmesser der weissen und schwarzen Wälzkörper durch eine radiale Verstellbarkeit auf die Innendurchmesser der Laufringe anpassbar werden.
Lösungsweg:
Durch Herausnahme der 24 BS5 aus dem Wälzkäfig wird sein Durchmesser reduziert und damit an seinem Umfang Platz geschaffen für die Anordnung radialer Verstellelemente BS15. Die Radachsen der weissen und schwarzen Wälzkörper sind durch Aufnahmeachsen auszutauschen. Die Aufnahmen der roten Wälzkörper sind zu verlängern.
 

Nachtrag 21.10.2008
Nachbau des Axiallagers von Remadus
http://www.ftcommunity.de/details.php?image_id=15801
Tauchantrieb mit Führungsring zum Axiallager von Remadus
http://www.ftcommunity.de/details.php?image_id=15961
Entwicklung zum Drehkranz
http://www.ftcommunity.de/details.php?image_id=16020