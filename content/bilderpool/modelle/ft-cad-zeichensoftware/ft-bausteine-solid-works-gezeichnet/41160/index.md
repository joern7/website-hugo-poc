---
layout: "image"
title: "Grundbaustein 15 Schwarz"
date: "2015-06-08T21:30:57"
picture: "teilekalti09.jpg"
weight: "13"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41160
- /detailsc19a-2.html
imported:
- "2019"
_4images_image_id: "41160"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41160 -->
