---
layout: "image"
title: "Verbinder 30 Rot"
date: "2015-06-08T21:30:57"
picture: "teilekalti02.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41153
- /details38ff.html
imported:
- "2019"
_4images_image_id: "41153"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41153 -->
