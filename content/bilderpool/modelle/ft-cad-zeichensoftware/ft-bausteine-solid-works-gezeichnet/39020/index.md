---
layout: "image"
title: "Diverse Bausteine"
date: "2014-07-11T19:10:48"
picture: "Bausteine2.png"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39020
- /detailscad4-2.html
imported:
- "2019"
_4images_image_id: "39020"
_4images_cat_id: "2906"
_4images_user_id: "502"
_4images_image_date: "2014-07-11T19:10:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39020 -->
Habe mich gerade etwas in SolidWorks eingearbeitet.