---
layout: "image"
title: "Mini Traktor 2"
date: "2014-11-22T16:26:37"
picture: "Traktor_2.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39838
- /details73d1.html
imported:
- "2019"
_4images_image_id: "39838"
_4images_cat_id: "2906"
_4images_user_id: "502"
_4images_image_date: "2014-11-22T16:26:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39838 -->
