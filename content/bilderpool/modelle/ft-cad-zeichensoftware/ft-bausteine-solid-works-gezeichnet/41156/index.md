---
layout: "image"
title: "Stecker Rot"
date: "2015-06-08T21:30:57"
picture: "teilekalti05.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41156
- /details6fee.html
imported:
- "2019"
_4images_image_id: "41156"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41156 -->
