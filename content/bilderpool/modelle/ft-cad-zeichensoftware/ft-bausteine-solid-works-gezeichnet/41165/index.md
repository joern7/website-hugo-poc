---
layout: "image"
title: "Hülse Schwarz"
date: "2015-06-08T21:30:57"
picture: "teilekalti14.jpg"
weight: "18"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41165
- /details04a1-2.html
imported:
- "2019"
_4images_image_id: "41165"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41165 -->
