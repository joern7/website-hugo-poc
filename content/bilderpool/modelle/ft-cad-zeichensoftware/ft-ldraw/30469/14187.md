---
layout: "comment"
hidden: true
title: "14187"
date: "2011-04-27T23:17:11"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Copying/Drawing machine. Put a pen into one Baustein 15 mit Loch and start drawing. The other Baustein 15 mit Loch will have to follow. There's one downside to the copying part: When one joint is fully extended, it can 'choose' in which direction to go next. Exploiting this, you can create a 'waving' motion of the machine. This will produce some beautiful drawings that I'm too lazy to imagine right now.

Ich hoffe, dein Englisch reicht ... wenn nicht, übersetz ichs dir schnell im IRC :o)