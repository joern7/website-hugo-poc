---
layout: "image"
title: "'Lichtwagen 1'"
date: "2012-12-17T12:19:05"
picture: "Lichtwagen_1.jpg"
weight: "172"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36323
- /details88a0-4.html
imported:
- "2019"
_4images_image_id: "36323"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-17T12:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36323 -->
aus: Licht - Elektronik Bd. 2, S. 176f