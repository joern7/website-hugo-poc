---
layout: "image"
title: "Kartengeber 05"
date: "2012-11-15T23:20:59"
picture: "Kartgeb_05.jpg"
weight: "165"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36085
- /details64a1.html
imported:
- "2019"
_4images_image_id: "36085"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-15T23:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36085 -->
Fan-Club Modell 1976/3 Erweiterung Kartengeber

Baustufe. Modifikation bei den Kartenhaltern, da die Originalteile mir nicht zur Verfügung stehen.