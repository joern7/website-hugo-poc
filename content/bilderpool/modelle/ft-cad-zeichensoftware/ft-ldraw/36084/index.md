---
layout: "image"
title: "Kartengeber 04"
date: "2012-11-15T23:20:59"
picture: "Kartgeb_04.jpg"
weight: "164"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36084
- /details5aa5-2.html
imported:
- "2019"
_4images_image_id: "36084"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-15T23:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36084 -->
Fan-Club Modell 1976/3 Erweiterung Kartengeber

Baustufe.