---
layout: "image"
title: "Traktor 2"
date: "2013-01-22T17:34:38"
picture: "Traktor_Inet_02.jpg"
weight: "198"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36500
- /details3e0a.html
imported:
- "2019"
_4images_image_id: "36500"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-22T17:34:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36500 -->
Modell und einige Bauschritte, nach einem dieser Tage gesehenen Bild.