---
layout: "image"
title: "Kipper 5"
date: "2012-08-05T18:40:13"
picture: "Kipper_05.jpg"
weight: "146"
konstrukteure: 
- "ft, con.barriga"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35262
- /details38fb-2.html
imported:
- "2019"
_4images_image_id: "35262"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-08-05T18:40:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35262 -->
Der modifizierte Hydraulik-Kipper aus den '80ern .