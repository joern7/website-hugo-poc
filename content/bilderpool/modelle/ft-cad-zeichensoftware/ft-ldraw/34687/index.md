---
layout: "image"
title: "Strobel-Brücke_05"
date: "2012-03-25T16:44:26"
picture: "Strob_Brueck_Bew_hal2.jpg"
weight: "126"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34687
- /detailsf2a2.html
imported:
- "2019"
_4images_image_id: "34687"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34687 -->
Die "Strobel-Brücke" aus Hobby 1/3 S.77ff in drei verschiedenen Phasen.
(Mit POVRAY gespielt)