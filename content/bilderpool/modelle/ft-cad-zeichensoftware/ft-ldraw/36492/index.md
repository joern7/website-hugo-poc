---
layout: "image"
title: "modifizierter Warenautomat 4"
date: "2013-01-14T10:57:06"
picture: "Warenautomat_Inet_4.jpg"
weight: "190"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36492
- /details1eff.html
imported:
- "2019"
_4images_image_id: "36492"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-14T10:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36492 -->
Geändert wurden:
    der Warenturm (es passen jetzt süße kleine Schokotäfelchen hinein), 
    der Schieber, 
    der Münzprüfer (lässt nur 5 Centmünzen durch) und
    die Steuerung (durch ein Interface).