---
layout: "image"
title: "KnickArm_05"
date: "2011-12-11T13:57:08"
picture: "KnickArm_05.jpg"
weight: "81"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/33624
- /detailsf073.html
imported:
- "2019"
_4images_image_id: "33624"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-12-11T13:57:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33624 -->
Kleine Änderung am Greifer:
Greifer auf: bis Taster offen
Greifer zu: bis Taster zu und noch eine feste Zeitspann