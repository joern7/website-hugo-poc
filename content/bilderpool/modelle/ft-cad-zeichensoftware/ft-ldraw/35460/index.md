---
layout: "image"
title: "Super Loop 4"
date: "2012-09-04T19:52:53"
picture: "Superloop_04.jpg"
weight: "150"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35460
- /details1a30-2.html
imported:
- "2019"
_4images_image_id: "35460"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-04T19:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35460 -->
Das Fan-Club Modell Nr. 4 mit kleinen Modifikationen bei Antrieb und Kabine.