---
layout: "image"
title: "Papierhalter für alten Scanner"
date: "2011-06-11T12:42:23"
picture: "Antriebsferne_Schmalseite_abgesenkt.jpg"
weight: "18"
konstrukteure: 
- "con.barriga"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30851
- /detailsa930-2.html
imported:
- "2019"
_4images_image_id: "30851"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-06-11T12:42:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30851 -->
Kleine Änderungen am alten FT-Scanner:

- Zusätzlicher Endetaster; gibt mehr Sicherheit, kann aber auch (wie beim 3D-Drucker von A. Rozek) zur Kalibrierung genutzt werden.
- Papierhalter; mit einem ähnlichen Teil auf der anderen Seite erspart man sich das Festkleben des Papiers.