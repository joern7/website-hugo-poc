---
layout: "image"
title: "Kniehebelpresse 4 (aus Hobby 1 Band 3 S. 42)"
date: "2011-08-14T12:14:07"
picture: "Kniehebelpresse_25.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31577
- /details4254.html
imported:
- "2019"
_4images_image_id: "31577"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-14T12:14:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31577 -->
