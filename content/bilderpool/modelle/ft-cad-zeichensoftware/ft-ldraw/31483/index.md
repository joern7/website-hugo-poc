---
layout: "image"
title: "Variante des Stifthalters für ft-Scanner 1"
date: "2011-07-30T00:52:45"
picture: "Schreibkopf_LPub_page_1.jpg"
weight: "32"
konstrukteure: 
- "con.barriga"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31483
- /detailsece3.html
imported:
- "2019"
_4images_image_id: "31483"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-30T00:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31483 -->
Halter für zwei verschiedenfarbige Stifte.
Benutzte Stifte: Stabilo point 88, fine 0,4
Geht nur mit den neueren E-Magneten von Fa. Knobloch
Interface + Erweiterung erforderlich