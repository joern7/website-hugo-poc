---
layout: "image"
title: "Manipulator 2"
date: "2011-08-28T18:26:04"
picture: "Manipulator_LPub_page_2.jpg"
weight: "56"
konstrukteure: 
- "ltam"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31653
- /details96bb-2.html
imported:
- "2019"
_4images_image_id: "31653"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-28T18:26:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31653 -->
