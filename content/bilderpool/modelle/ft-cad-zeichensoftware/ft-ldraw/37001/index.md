---
layout: "image"
title: "Knetmaschine 1"
date: "2013-05-28T15:55:06"
picture: "Knetmaschine_1.jpg"
weight: "210"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/37001
- /details4d80.html
imported:
- "2019"
_4images_image_id: "37001"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-05-28T15:55:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37001 -->
Nach einer Bauanleitung aus 1977