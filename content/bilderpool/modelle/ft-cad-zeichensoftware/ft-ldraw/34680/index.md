---
layout: "image"
title: "Strobel-Brücke_03"
date: "2012-03-25T10:32:06"
picture: "Strob_Brueck_Bew_ob.jpg"
weight: "124"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34680
- /detailsc9d9.html
imported:
- "2019"
_4images_image_id: "34680"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-25T10:32:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34680 -->
Die "Strobel-Brücke" aus Hobby 1/3 S.77ff in drei verschiedenen Phasen