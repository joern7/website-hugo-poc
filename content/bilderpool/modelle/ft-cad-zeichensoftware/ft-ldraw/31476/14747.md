---
layout: "comment"
hidden: true
title: "14747"
date: "2011-07-30T16:24:42"
uploadBy:
- "con.barriga"
license: "unknown"
imported:
- "2019"
---
Dank für die Komplimente.

@Ingo
Du hast recht, so ist der Abstand zu groß. In der Wirklichkeit sind zum einen die Federn kürzer (aber die Konstruktion von Federn ist ganz fürchterlich) und zum anderen können die Rückschlussplatten mit den waagrechten 30-Steinen zur Justierung benutzt werden.