---
layout: "image"
title: "3Achs Rob_3.jpg"
date: "2011-03-04T09:23:07"
picture: "3Achs_Rob_3_2.jpg"
weight: "3"
konstrukteure: 
- "??"
fotografen:
- "con.barriga"
keywords: ["LDraw", "LPub"]
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30196
- /details0e70.html
imported:
- "2019"
_4images_image_id: "30196"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-03-04T09:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30196 -->
Teile einer Bauanleitung für einen 3-Achs Rob