---
layout: "image"
title: "Federhammer 5 (aus Hobby 2 Band 1 S. 65)"
date: "2011-08-01T20:18:57"
picture: "Federhammer_LPub_page_7.jpg"
weight: "39"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31490
- /detailsf7e8.html
imported:
- "2019"
_4images_image_id: "31490"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-01T20:18:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31490 -->
