---
layout: "image"
title: "Kompressor 1"
date: "2011-08-22T23:54:53"
picture: "Pneu_Kompressor_page_2.jpg"
weight: "52"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31641
- /detailsc6a1.html
imported:
- "2019"
_4images_image_id: "31641"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-22T23:54:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31641 -->
Drei aufeinanderfolgende Arbeitsschritte bei denen der/die vorhergende(n) "geweißt" wurden.