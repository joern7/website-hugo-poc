---
layout: "image"
title: "HR-Rob Säule_6"
date: "2011-05-08T19:29:19"
picture: "Hochregalrobot_06.jpg"
weight: "13"
konstrukteure: 
- "???"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30544
- /details6395.html
imported:
- "2019"
_4images_image_id: "30544"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-05-08T19:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30544 -->
