---
layout: "image"
title: "Pneum. Bagger 01"
date: "2011-08-16T21:34:57"
picture: "Pneu_Bagger_01.jpg"
weight: "49"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31585
- /details8212.html
imported:
- "2019"
_4images_image_id: "31585"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-16T21:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31585 -->
Versuch mit pneumatischen Bauteilen.
Die Schlauchführung ist noch mit "zittriger Hand" und nicht so ganz genau...

Die Bodenplatte ist aber absichtlich gelb, sonst sieht man nur noch eine schwarze Masse.

Gruß con.barriga