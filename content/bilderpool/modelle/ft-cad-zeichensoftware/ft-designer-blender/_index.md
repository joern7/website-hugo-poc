---
layout: "overview"
title: "FT Designer und Blender"
date: 2020-02-22T08:40:36+01:00
legacy_id:
- /php/categories/3270
- /categories33d9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3270 --> 
Mit der Version 1.2 des Fischertechnik Designers können die im Designer erstellten Modelle in das Extensible 3D Format exportiert werden. Dies ermöglicht u.a. die Nachbearbeitung (und vor allem das Rendern) der Modelle im OpenSource Programm Blender.