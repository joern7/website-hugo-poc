---
layout: "image"
title: "Knickarmroboter 2"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter2.jpg"
weight: "2"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- /php/details/39763
- /details5a68.html
imported:
- "2019"
_4images_image_id: "39763"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39763 -->
Knickarmroboter von der Seite [ groß ]
 erstellt mit Ft-Designer