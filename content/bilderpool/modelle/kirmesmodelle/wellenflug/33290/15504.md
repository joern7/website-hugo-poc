---
layout: "comment"
hidden: true
title: "15504"
date: "2011-10-21T21:20:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

stimmt, das ist eine Gleitlagerung auf dem äußeren Umfang des Speichenrads. Das Speichenrad selber und alles darin rotiert selber nicht (bzw. hier sitzt es ja drehfest auf dem langsam rotierenden Mast). Der 1:50 PowerMot zieht das aber sogar "trocken" durch. Am Jumping (stand in E-B. gleich daneben) hatte ich mit etwas Teflon-Fett von Robbe nachgeholfen.

Gruß,
Harald