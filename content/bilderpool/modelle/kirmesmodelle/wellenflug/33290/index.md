---
layout: "image"
title: "WF6875.JPG"
date: "2011-10-21T17:29:41"
picture: "WF6875.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33290
- /detailsa3ad.html
imported:
- "2019"
_4images_image_id: "33290"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T17:29:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33290 -->
Antrieb und  Simpel-Draht-Schleifring. Der Gondelträger ist mit den "Führungsplatten für E-Magnet" 32455 gelagert. Der Motor musste oben drauf, weil er unten überall im Weg gewesen wäre, und er mitten hinein nicht gepasst hat.