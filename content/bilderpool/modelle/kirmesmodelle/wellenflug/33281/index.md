---
layout: "image"
title: "WF6860.JPG"
date: "2011-10-21T16:08:49"
picture: "WF6860.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33281
- /details5cdf.html
imported:
- "2019"
_4images_image_id: "33281"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:08:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33281 -->
Die Stirnzapfen der Flachträger 120 sind zu schwach, um den Mast zu halten. Deshalb sind die Flachträger miteinander verstiftet (zwei Bohrungen mit abgeknipsten Bildernägeln; den hinteren sieht man nicht) und gegen Zug mit einem roten Kabelbinder gesichert.