---
layout: "image"
title: "Schleif6016.JPG"
date: "2011-10-21T16:48:08"
picture: "Schleif6016.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33286
- /detailsd5de.html
imported:
- "2019"
_4images_image_id: "33286"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:48:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33286 -->
Das ist der Versuch, mit den ft-Stahlfedern einen Schleifring aufzubauen. Es ist beim Versuch geblieben. Die Federn leiten schlecht, und gut funktionierende Schleifkontakte, die auch dort bleiben wo sie sollen, müssen noch gefunden werden. 

Rechts: Stahlfeder 270 mm, die Überlänge kann man innen drin verstauen. 

Mitte: die Stahlfeder mit ca. 70 mm passt genau. Die Kontaktierung von innen erfolgt mit einem Teil aus dem ft-Drehschalter. 

Links: zwei Varianten zur Kontaktierung: einmal mit ft-Stecker, einmal mit einem Kontakt aus dem Unterteil des ft-Drehschalters 31311.