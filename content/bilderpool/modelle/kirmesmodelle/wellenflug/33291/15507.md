---
layout: "comment"
hidden: true
title: "15507"
date: "2011-10-21T23:02:21"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
vorteilhaft für euere Modelliden, die ihr als Vorbilder in der Nähe von Ballungszentren eher mal zu sehen bekommt. Dieses Modell war natürlich wieder nach deinem Geschmack. Um ehrlich zu sein, ist es schwer was "Gescheites" zu den hier von dir gefundenen Lösungsdetails zu kommentieren, weil man nicht sofort weiß wie man es nach eigenen Überlegungen beim Eigenbau selbst gemacht hätte.
Gruß, Ingo