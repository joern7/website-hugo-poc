---
layout: "image"
title: "WF6107.JPG"
date: "2011-10-21T17:01:41"
picture: "WF6107.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33287
- /details5f90-3.html
imported:
- "2019"
_4images_image_id: "33287"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T17:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33287 -->
Die BSB-Federkontakte allein hätten *vielleicht* ausgereicht, wenn sie einzeln gefedert gelagert wären: wenn die Kontaktierung der Innenseite vorbei fährt, drückt sie den äußeren Schleifkontakt weg. Wenn der zweite dann wegen mechanischer Kopplung auch abhebt, ist der Stromfluss unterbrochen. 

Der K(r)ampf mit den Stahlfedern und den Messingblechen, die einfach nicht da bleiben wollen, wo sie hin gehören. Gegen Kurzschluss ist sogar noch eine Plastikfolie mit 5 mm Übermaß zwischen den Drehscheiben eingesetzt (durchsichtig, über dem BSB-Kontakt zu erahnen). Nutzt aber alles nicht.