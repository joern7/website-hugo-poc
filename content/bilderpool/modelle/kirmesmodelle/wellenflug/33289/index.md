---
layout: "image"
title: "WF5830.JPG"
date: "2011-10-21T17:16:12"
picture: "WF5830.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33289
- /detailsc8fa.html
imported:
- "2019"
_4images_image_id: "33289"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T17:16:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33289 -->
Fast fertig. Die Drehscheiben als Dekoration waren dann doch zu schwer und flogen wieder heraus. Ebenso der ft-Schleifring oben mitte, der am krummen Bahnteil Probleme macht.

Was dort wie Spinnenfäden aussieht, ist ein Nylonfaden (Anglerschnur, original-ft!!), mit dem die aufrecht eingeklemmten Streben I-30 in ihren Positionen fixiert sind.

Der S-Motor reicht auch nicht aus. Dafür kommt später ein PowerMot 1:50 hin.