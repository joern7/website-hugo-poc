---
layout: "image"
title: "Wellenflug110.JPG"
date: "2011-10-21T15:50:51"
picture: "Wellenflug110.JPG"
weight: "5"
konstrukteure: 
- "Fa. Zierer"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33278
- /details60a5-2.html
imported:
- "2019"
_4images_image_id: "33278"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T15:50:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33278 -->
Ordentlich heraus geputzt macht so ein Wellenflug ganz schön was her.


München, Oktoberfest 2004