---
layout: "image"
title: "Schleif6014.JPG"
date: "2011-10-21T16:46:23"
picture: "Schleif6014.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33285
- /details067d-2.html
imported:
- "2019"
_4images_image_id: "33285"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:46:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33285 -->
Ein Schleifring, dessen Schleifkontakte mit der ft-Plastikfeder gehalten werden. Die ft-Kette schließt den Kreis, und fertig. Die Kabel verhindern ein Mitdrehen.

Das Gelenk mit den ft-Steckern und den Röhren aus einem Wattestäbchen war nötig, damit der ganze Aufbau die Kurve im oberen Bahnteil schafft. 

Insgesamt hat aber auch diese Variante nicht überzeugt.