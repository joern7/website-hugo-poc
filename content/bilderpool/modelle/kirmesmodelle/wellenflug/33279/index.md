---
layout: "image"
title: "WF6870.jpg"
date: "2011-10-21T16:01:21"
picture: "WF6870.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33279
- /details1074.html
imported:
- "2019"
_4images_image_id: "33279"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:01:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33279 -->
Wie beim Original dreht sich der ganze Mast. Er ist nur lose von oben eingesteckt, hängt aber noch über Seil und Kabel am Sockel fest.

Nach einem Tag Betrieb in Erbes-Büdesheim sammelt sich etwas Abrieb an.