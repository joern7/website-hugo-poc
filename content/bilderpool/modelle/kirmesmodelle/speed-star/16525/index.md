---
layout: "image"
title: "Blitzer"
date: "2008-11-28T23:04:42"
picture: "BILD0426.jpg"
weight: "7"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
keywords: ["Speed", "-", "Star"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16525
- /details35e5.html
imported:
- "2019"
_4images_image_id: "16525"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:04:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16525 -->
