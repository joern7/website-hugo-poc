---
layout: "image"
title: "Gesmatansicht ohne Abdeckung"
date: "2007-12-12T07:32:26"
picture: "flugzeugkarussel27.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13062
- /details5258-2.html
imported:
- "2019"
_4images_image_id: "13062"
_4images_cat_id: "1183"
_4images_user_id: "636"
_4images_image_date: "2007-12-12T07:32:26"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13062 -->
