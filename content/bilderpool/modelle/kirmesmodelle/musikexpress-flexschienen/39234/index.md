---
layout: "image"
title: "Giebeldetail"
date: "2014-08-10T21:11:11"
picture: "IMG_0025.jpg"
weight: "48"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39234
- /details0b23.html
imported:
- "2019"
_4images_image_id: "39234"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-08-10T21:11:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39234 -->
