---
layout: "image"
title: "15 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell15.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: ["Schleifring"]
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36573
- /details5cf4.html
imported:
- "2019"
_4images_image_id: "36573"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36573 -->
Bei Pollin heißt es Twist-Stop, Nr. 540 041
Das schwarze Teil ist ein Adapter, mit dem man es gut verbauen kann. Nr. 540 167 müsste das sein.