---
layout: "image"
title: "02 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36560
- /details622f.html
imported:
- "2019"
_4images_image_id: "36560"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36560 -->
1. Achse um 180° gedreht