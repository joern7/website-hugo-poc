---
layout: "image"
title: "14 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell14.jpg"
weight: "14"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: ["Schleifring"]
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36572
- /details92a7.html
imported:
- "2019"
_4images_image_id: "36572"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36572 -->
Dieses Teil soll eigentlich das Verdrehen von Telefonkabeln verhindern, scheinbar kann man es aber auch zweckentfremden ;-)
http://www.ftcommunity.de/details.php?image_id=13464