---
layout: "image"
title: "05 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36563
- /details1ba3.html
imported:
- "2019"
_4images_image_id: "36563"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36563 -->
Antrieb der ersten Achse + Impulstaster

Glücklicherweise drehen die geschraubten Z10 Ritzel nicht durch, hätte ich nicht gedacht.