---
layout: "image"
title: "13 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell13.jpg"
weight: "13"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36571
- /details1caf.html
imported:
- "2019"
_4images_image_id: "36571"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36571 -->
Motor der dritten Achse + LEDs zu Beleuchtung.