---
layout: "image"
title: "04 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36562
- /details8974.html
imported:
- "2019"
_4images_image_id: "36562"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36562 -->
oben ist das Gegengewicht, bestehend aus drei Akkus