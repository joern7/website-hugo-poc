---
layout: "image"
title: "Lichtschranke"
date: "2008-07-15T22:17:21"
picture: "hullygully22.jpg"
weight: "22"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14860
- /details63bd.html
imported:
- "2019"
_4images_image_id: "14860"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14860 -->
Die Lichtschranke ist für das Anhalten der einzelnen Kabinen zuständig.