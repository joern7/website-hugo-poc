---
layout: "comment"
hidden: true
title: "6758"
date: "2008-07-21T16:49:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das geht doch ganz einfach dadurch, dass man die Radnabe nicht ganz fest zieht. Ggf. muss man sie oben mit einem Klemmring gegen Abrutschen sichern.

Gruß,
Stefan