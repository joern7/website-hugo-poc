---
layout: "image"
title: "Karussell in der vordersten Position"
date: "2008-07-15T22:17:06"
picture: "hullygully13.jpg"
weight: "13"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14851
- /details9089.html
imported:
- "2019"
_4images_image_id: "14851"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14851 -->
In der Grundposition drehen sich das Ober- und das Unterteil in entgegengesetzter Richtung. Dies erzeugt eine gewollte Unwucht (eliptische Bahn), die zu tollen Fahreffekten führt.