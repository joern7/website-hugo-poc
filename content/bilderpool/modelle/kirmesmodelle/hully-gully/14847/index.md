---
layout: "image"
title: "Kabine von vorne"
date: "2008-07-15T22:17:05"
picture: "hullygully09.jpg"
weight: "9"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14847
- /details1ca5.html
imported:
- "2019"
_4images_image_id: "14847"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14847 -->
Hier sieht man das Geländer, dass während der Fahrt umgeklappt wird.