---
layout: "image"
title: "Kippantrieb"
date: "2008-07-15T22:17:21"
picture: "hullygully20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14858
- /details9c8e-2.html
imported:
- "2019"
_4images_image_id: "14858"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14858 -->
Der obere Teil kann von einem Powermotor mit roter Kappe über eine Schnecke um 45° gekippt werden.