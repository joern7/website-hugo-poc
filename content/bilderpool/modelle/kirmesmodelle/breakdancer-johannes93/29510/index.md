---
layout: "image"
title: "Tafel mit Beleuchtung"
date: "2010-12-23T15:37:40"
picture: "breakdancer04.jpg"
weight: "4"
konstrukteure: 
- "Johannes W."
fotografen:
- "Johannes W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/29510
- /detailsac69.html
imported:
- "2019"
_4images_image_id: "29510"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29510 -->
