---
layout: "image"
title: "Draufsicht 1"
date: "2010-12-23T15:37:40"
picture: "breakdancer05.jpg"
weight: "5"
konstrukteure: 
- "Johannes W."
fotografen:
- "Johannes W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/29511
- /details8708.html
imported:
- "2019"
_4images_image_id: "29511"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29511 -->
