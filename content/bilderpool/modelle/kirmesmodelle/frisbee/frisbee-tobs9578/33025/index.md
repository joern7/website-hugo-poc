---
layout: "image"
title: "Kompressor"
date: "2011-10-01T13:47:25"
picture: "frissbevontobs1.jpg"
weight: "1"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33025
- /details2495.html
imported:
- "2019"
_4images_image_id: "33025"
_4images_cat_id: "2434"
_4images_user_id: "1007"
_4images_image_date: "2011-10-01T13:47:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33025 -->
Das ist der Kompressor mit einer Druckabschaltung.