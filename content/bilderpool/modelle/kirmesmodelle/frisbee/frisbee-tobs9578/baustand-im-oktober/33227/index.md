---
layout: "image"
title: "Treppe mit Leds"
date: "2011-10-19T14:25:45"
picture: "frisbee1_2.jpg"
weight: "3"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33227
- /details32df.html
imported:
- "2019"
_4images_image_id: "33227"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T14:25:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33227 -->
In dieser kleinen Treppe habe ich 16 Leds eingebaut.