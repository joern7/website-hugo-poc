---
layout: "image"
title: "Extensions"
date: "2011-10-29T19:13:33"
picture: "fri3.jpg"
weight: "16"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33364
- /details3fd6.html
imported:
- "2019"
_4images_image_id: "33364"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-29T19:13:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33364 -->
Aufgaben:
- Lichtsteuerung
- Musiksteuerung