---
layout: "image"
title: "Stütze mit Leds"
date: "2011-10-19T19:22:33"
picture: "frisbee5_2.jpg"
weight: "12"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33255
- /details5fb8.html
imported:
- "2019"
_4images_image_id: "33255"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33255 -->
In eine Stütze kommen 30 Leds.
Angesteuert werden sie über ein Flachbandkabel,wo noch eine Platine drannkommt( in Arbeit).