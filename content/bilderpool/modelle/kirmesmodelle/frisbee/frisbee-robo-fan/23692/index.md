---
layout: "image"
title: "Gesamtansicht"
date: "2009-04-13T14:50:49"
picture: "frisbee01.jpg"
weight: "1"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23692
- /detailscd10.html
imported:
- "2019"
_4images_image_id: "23692"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23692 -->
So sieht er aus