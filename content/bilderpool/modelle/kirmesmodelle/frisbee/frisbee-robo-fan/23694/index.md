---
layout: "image"
title: "Die Aufhängung"
date: "2009-04-13T14:50:49"
picture: "frisbee03.jpg"
weight: "3"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23694
- /details80b3.html
imported:
- "2019"
_4images_image_id: "23694"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23694 -->
Die obere rote Scheibe hängt an dem Arm der hin und her schwenkt und ist mit der Gondel nicht verbunden