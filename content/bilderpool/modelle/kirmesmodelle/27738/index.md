---
layout: "image"
title: "Small Swing Ride"
date: "2010-07-10T22:02:36"
picture: "ft-swing_sm.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27738
- /detailsf9eb.html
imported:
- "2019"
_4images_image_id: "27738"
_4images_cat_id: "124"
_4images_user_id: "585"
_4images_image_date: "2010-07-10T22:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27738 -->
Fun model I am going to use as a basis for a new nook ereader set of plans.