---
layout: "image"
title: "Jumping8673"
date: "2013-06-04T21:30:04"
picture: "IMG_8673.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37023
- /details586c-2.html
imported:
- "2019"
_4images_image_id: "37023"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:30:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37023 -->
Oberhalb vom E-Tec ist die Seiltrommel fürs Einstellen der Länge des Zugseils im Teleskopmast angebracht. Die Kurvenscheibe wird vom S-Riegel gesperrt. Die Achse ist gleichzeitig Stütz-Punkt für den vorderen Hilfsmast; deshalb ist der Nylonfaden da herum gefädelt und zieht den Hilfsmast in Position.