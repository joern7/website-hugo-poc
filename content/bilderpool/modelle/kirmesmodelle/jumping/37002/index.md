---
layout: "image"
title: "Jumping602.jpg"
date: "2013-06-01T22:57:51"
picture: "JP602_mit.JPG"
weight: "1"
konstrukteure: 
- "Fa. Huss"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37002
- /detailsfd08-2.html
imported:
- "2019"
_4images_image_id: "37002"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T22:57:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37002 -->
Oktoberfest München 2004: Fa. Distel fährt mit dem "Jumping" von Huss auf. Der Mittelbau ist eine mächtige Fuhre mit 6 Achsen und gut 40 Tonnen Gewicht. Die Fahrgastgondeln und das Podium sind auf einem zweiten Wagen verladen.

Aufbau: Die "Dolly"-Vorderachse wird unterm Fahrzeug herausgezogen, seitlich werden die Stützen ausgeklappt, und dann treten die dicken grünen Hubzylinder in Aktion, um den Mast mit seinen fünf Gondel-Armen aufzurichten, der nach hinten abgelegt wurde. Die "Krone" und weiteres Schmuckwerk liegt vorn über der Dolly-Achse. Der grüne Kasten in der Mitte wird mit Ballastwasser gefüllt.