---
layout: "image"
title: "Jumping5112.jpg"
date: "2013-06-02T09:45:08"
picture: "IMG_6112.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37011
- /details78e8.html
imported:
- "2019"
_4images_image_id: "37011"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T09:45:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37011 -->
Der Gondelträger beruht auf einem Fünfeck, das von oben mittels Führungsplatten 32455 auf dem Rand des Speichenrads geführt wird.