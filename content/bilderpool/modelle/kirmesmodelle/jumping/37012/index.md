---
layout: "image"
title: "Jumping6126"
date: "2013-06-02T09:51:01"
picture: "IMG_6126_mit.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37012
- /details63cd.html
imported:
- "2019"
_4images_image_id: "37012"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T09:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37012 -->
Von der anderen Seite (hier unten, im Modell dann oben) wird das Fünfeck mittels Schneckenmuttern m1 (35973) gehalten, die dank der versetzten Nuten etwas heraus stehen. Vom  Drehkranz rotiert die rote Seite, die schwarze mit dem Zahnkranz ist am Mast fest. Deshalb rotiert der Antriebsmotor später mitsamt der Schnecke und dem Gondelträger um den Mast herum.