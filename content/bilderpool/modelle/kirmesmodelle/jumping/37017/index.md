---
layout: "image"
title: "Jumping8619"
date: "2013-06-02T10:25:45"
picture: "IMG_8619.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37017
- /details2c2c.html
imported:
- "2019"
_4images_image_id: "37017"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T10:25:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37017 -->
Dieser Winkel 60° koppelt den Mastschlitten an den Schneckenantrieb. Man kann auch den Flachstein 15x30 abnehmen, dann den Winkelstein seitlich von der Achse abziehen, und schon ist der Mast frei in den Schienen verschieblich.

Unten, an der Dolly-Achse befindet sich eine IR-Fernbedienung (in ein rotes Gehäuse umgezogen; es gab sonst nirgendwo ein Plätzchen dafür) für die Motoren, die beim Aufrichten und Einklappen beteiligt sind:
- Schneckenantrieb (auf der Zugmaschine; das Kabel dazu liegt vorn auf der Deichsel)
- Seilwinde (Nylonfaden) zum Aufrichten der Hilfsmasten
- Seilzug für den Teleskopschub im vorderen Hilfsmast; der Motor dazu wird im unteren rechten Eck vom BS7,5 verdeckt.