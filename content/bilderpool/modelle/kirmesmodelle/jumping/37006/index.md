---
layout: "image"
title: "Jumping6039"
date: "2013-06-01T23:32:05"
picture: "IMG_6039_mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37006
- /detailsc43b.html
imported:
- "2019"
_4images_image_id: "37006"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:32:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37006 -->
Mein erster Entwurf hatte zwei Speichenräder und ein Gegengewicht, das am Seil auf und ab gezogen wurde. Das war zu sperrig. Der Antrieb mit fest geklemmter Kette (als Zahnrad-Ersatz) und darin eingreifendem Z10 vom mit-rotierenden Power-Mot war auch nicht der richtige.