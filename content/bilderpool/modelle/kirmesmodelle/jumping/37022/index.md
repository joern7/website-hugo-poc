---
layout: "image"
title: "Jumping8671"
date: "2013-06-04T21:24:44"
picture: "IMG_8671.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37022
- /details7ed0.html
imported:
- "2019"
_4images_image_id: "37022"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:24:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37022 -->
An den seitlichen Stützen sieht man ein Viergelenkgetriebe bei der Arbeit. Hier, im abgestützten Zustand bildet es ein überstrecktes Kniegelenk und die obere Stahlfeder verhindert, dass es von alleine über den Totpunkt hinweg wieder einknickt. Im eingeklappten Zustand hält sie die Stütze auch fest. Die untere Stahlfeder fixiert die Fußplatte in der oberen Lage, wenn sich die Stütze an den Mast anschmiegt. 

Der E-Tec ist Motortreiber für den Motor unten im Mastschlitten, der den Teleskopschub im Mast betätigt.