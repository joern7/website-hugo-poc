---
layout: "image"
title: "Jumping8611"
date: "2013-06-01T23:59:09"
picture: "IMG_8611.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37010
- /details01c6.html
imported:
- "2019"
_4images_image_id: "37010"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:59:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37010 -->
Direkt von vorn sieht man den Querschnit des Mastes (schwarze BS15 in Bildmitte)  mit den seitlichen Stützen (Platten 90x45 mit Stahlfedern). Mittig über dem Vorderachs-Dolly sieht man die Achse des Power-Mot, der den Teleskopschub für die Mastkrone per Seilzug betätigt.

Die schwarzen Stirnzapfen links und rechts davon (außen sind gelbe Statikträger 15 dran) gehören zu den Alu-Trägern, die das Rückgrat des Wagens bilden. In den inneren Nuten gleitet der Schlitten, der den Mast trägt, auf Bauplatten 30x30. Oben auf den Alu-Trägern befinden sich lange Messing-Achsen mit Schneckenteilen, die über die grauen Kardanteile gedreht werden und den Schlitten auf dem Wagen entlang verfahren. 

Der Antrieb für die Kardans befindet sich auf dem Heck des Zugfahrzeugs.