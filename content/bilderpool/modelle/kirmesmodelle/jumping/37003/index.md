---
layout: "image"
title: "Jumping302.jpg"
date: "2013-06-01T23:09:41"
picture: "JP302_mit.JPG"
weight: "2"
konstrukteure: 
- "Fa. Huss"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37003
- /details8ead.html
imported:
- "2019"
_4images_image_id: "37003"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:09:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37003 -->
Der Jumping bei Tag: der zentrale Teleskopschub hebt die Krone und damit die fünf Gondelträger nach oben. Das Ganze rotiert um den Mast in der Mitte. Die Gondeln sind halb-kardanisch aufgehängt und drehen sich am Ort. In der frühen Serie konnten die Gondeln pendeln, was aber den Fahrgästen unangenehm war. Deswegen wurden die dünnen Streben mit den Gleitführungen hinzugefügt. Der große "Knackpunkt" beim Jumping ist laut Patentschrift das sehr schnelle Absenken der Gondeln, das einen dem Freifallturm ähnlichen Eindruck bewirken soll. Sooo schnell kriege ich das Absenken leider nicht hin.


München, Oktoberfest 2004