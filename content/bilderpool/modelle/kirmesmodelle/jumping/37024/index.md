---
layout: "image"
title: "Jumping8676"
date: "2013-06-04T21:35:08"
picture: "IMG_8676.JPG"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37024
- /details7798.html
imported:
- "2019"
_4images_image_id: "37024"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:35:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37024 -->
Die Zugmaschine ist vorgefahren. Die beiden V-Steine werden über die Kardanhälften geschoben, das Kabel angeschlossen und dann kann der Schlitten mit dem Mast verfahren werden.