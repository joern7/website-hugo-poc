---
layout: "image"
title: "Jumping35"
date: "2013-06-04T21:42:08"
picture: "Jumping35_mit.JPG"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37025
- /detailsa4c3.html
imported:
- "2019"
_4images_image_id: "37025"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:42:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37025 -->
Die Gondeln in der Ausführung von 2011. Die Führungsplatten 32455 verhindern, dass die Winkel 60° sich nach unten aus den Nuten herauswackeln. Die Arme sind seither länger geworden und es musste Gewicht gespart werden, weshalb einige Winkelträger (nach diversen Messungen auf der Küchenwaage) durch Statik-Streben ersetzt wurden.