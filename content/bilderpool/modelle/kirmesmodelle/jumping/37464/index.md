---
layout: "image"
title: "Jumping9555.JPG"
date: "2013-09-30T18:32:44"
picture: "Jumping9555.JPG"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37464
- /details23a9.html
imported:
- "2019"
_4images_image_id: "37464"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:32:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37464 -->
Wagen Nummer 3. Die Gondeln sind in je drei Teile zerlegt und auf lange K-Achsen aufgesteckt.