---
layout: "image"
title: "Riesenrad 1m Details"
date: "2003-11-24T21:01:24"
picture: "Riesenrad_Details.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- /php/details/2008
- /details1138.html
imported:
- "2019"
_4images_image_id: "2008"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-24T21:01:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2008 -->
Hier sieht man die Magneten und den Reedkontakt.
Auch die Kabelverlegung im schwarzen Schlauch/Rohr aus dem Baumarkt.