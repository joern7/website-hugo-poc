---
layout: "image"
title: "Riesenrad 1m Seiltrommel"
date: "2003-11-26T15:00:56"
picture: "Riesenrad_Seiltrommel.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- /php/details/2011
- /details76cb.html
imported:
- "2019"
_4images_image_id: "2011"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-26T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2011 -->
Die Kette wird sonst durch die Seiltrommeln am Rad geführt.
Bei der Kettenlänge ist die Führung besser als nur mit Ritzeln.