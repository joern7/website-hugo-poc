---
layout: "image"
title: "Riesenrad 1m Antrieb"
date: "2003-11-24T21:01:24"
picture: "Riesenrad_Antrieb.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- /php/details/2007
- /detailsb0a6-3.html
imported:
- "2019"
_4images_image_id: "2007"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-24T21:01:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2007 -->
Antrieb mit Kettenspanner.
Motor/Getriebe können sich zwischen den Alus auf und ab bewegen,dadurch wird die Kette gespannt.