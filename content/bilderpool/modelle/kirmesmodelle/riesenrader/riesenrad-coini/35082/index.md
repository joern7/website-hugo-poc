---
layout: "image"
title: "Riesenrad Blick von unten"
date: "2012-06-17T14:59:56"
picture: "riesenrad07.jpg"
weight: "7"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/35082
- /detailsdf31.html
imported:
- "2019"
_4images_image_id: "35082"
_4images_cat_id: "2597"
_4images_user_id: "1476"
_4images_image_date: "2012-06-17T14:59:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35082 -->
Inspiriert von dem Riesenrad aus dem Baukasten Master Plus Adventure Park und aus dem Baukasten Advanced Fun Park habe ich dieses Riesenrad gebaut. Das Riesenrad wird mit einem Mini-Motor über einen Gummi-Riemen angetrieben und ist beleuchtet.