---
layout: "image"
title: "Riesenrad-Nabe"
date: "2012-03-10T23:15:59"
picture: "IMG_6599.jpg"
weight: "3"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34616
- /details1fd2-2.html
imported:
- "2019"
_4images_image_id: "34616"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34616 -->
Riesenrad-Nabe im uneingebauten Zustand.