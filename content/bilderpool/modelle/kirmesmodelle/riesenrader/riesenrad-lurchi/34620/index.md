---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2483.jpg"
weight: "7"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34620
- /detailsd71f.html
imported:
- "2019"
_4images_image_id: "34620"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34620 -->
Einblick auf die Innennabe.
Die fünf Halter zur Befestigung der Mittelnabe wurden im Winkel von ca. 72° montiert.