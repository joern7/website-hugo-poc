---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2460.jpg"
weight: "8"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34621
- /detailsed8c.html
imported:
- "2019"
_4images_image_id: "34621"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34621 -->
Mit symmetrischer Anordnung von 20 Grundbausteinen und Gelenkwürfel auf dem äußeren Umkreis der Mittelnabe (aus 25 WT 7,5°) wurden die Befestigungspunkte für die "U-Träger-Strahlen" geschaffen.