---
layout: "overview"
title: "Riesenrad von Lurchi"
date: 2020-02-22T07:58:08+01:00
legacy_id:
- /php/categories/2555
- /categoriesde10.html
- /categoriesd407.html
- /categoriesa0ae.html
- /categories0ec4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2555 --> 
Im Mittelpunkt zum Bau des Riesenradmodells stand die Konstruktion einer geeigneten Nabe.
Ein Wunsch meinerseits war es zudem diese hochbelastbare Nabe ausschließlich aus unveränderten Serienbauteilen zu erstellen, was bei ähnlich großen Modellen dieser Art bisher nicht der Fall war.