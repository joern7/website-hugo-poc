---
layout: "image"
title: "Wiener Riesenrad 3.0"
date: "2018-01-03T19:28:44"
picture: "dritteswiener1.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus "
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/47012
- /detailsf87a-2.html
imported:
- "2019"
_4images_image_id: "47012"
_4images_cat_id: "2638"
_4images_user_id: "968"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47012 -->
Hier die dritte und letzte Version meines Wiener Riesenrades.Höhe 283 cm,Durchmesser 260 cm ,Achshöhe 153 cm.
Der Unterschied zum Vorgängermodell liegt in der besseren Zerlegbarkeit. Somit kann ich es auch besser transportieren.
Ausstellen werde ich es zu ersten Mal  bei den Lipper Modellbauten am 3ten Januarwochenende in Bad Salzuflen.
Außerdem habe ich mir dieses Jahr die Nordconvention und die Ausstellung in Münster vorgenommen :)
