---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad8.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34695
- /detailsdbb9.html
imported:
- "2019"
_4images_image_id: "34695"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34695 -->
Der Antrieb,noch ohne Antriebsseil.
Das Original hat heute noch die über hundert Jahren alte Antriebstechnik.Lediglich die Steuerungstechnik ist auf dem Stand der Zeit.
Das 240 t schwere Rad wird von zwei 15PS Motoren angetrieben,wobei einer nur zur Sichertheit mitläuft.
Auch heute ist das Rad noch von Hand kurbelbar!!!!