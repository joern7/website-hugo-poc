---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34689
- /details0390.html
imported:
- "2019"
_4images_image_id: "34689"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34689 -->
Noch schwebt die Achse,das Lager kommt noch.