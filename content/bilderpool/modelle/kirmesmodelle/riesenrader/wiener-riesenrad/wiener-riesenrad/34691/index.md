---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34691
- /details4aad.html
imported:
- "2019"
_4images_image_id: "34691"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34691 -->
Hier 11 von 12 Kabinen.Die mit den gelben Streben sind Kabinen mit einen
45mm Rollstuhltür.
Die Fenster sind aus Pappe ausgeschnitten und aufgeklebt.
Bei meinen sehr zahlreichen Aufenthalten in Wien und im Prater sind mir die weißen Fenster immer so ins 
Auge gefallen,das ich beim Modell nicht drauf verzichten wollte.