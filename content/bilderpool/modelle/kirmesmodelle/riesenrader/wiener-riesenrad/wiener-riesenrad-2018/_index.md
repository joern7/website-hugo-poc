---
layout: "overview"
title: "Wiener Riesenrad 2018"
date: 2020-02-22T07:58:06+01:00
legacy_id:
- /php/categories/3534
- /categories8a08.html
- /categories4560.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3534 --> 
Nachdem ich bei meinem Vorgänger-Riesenrad bei der Intermodellbau 2016 schon mal einseitig 30 "Neonröhren" angebaut hatte,
wolte ich beim aktuellen Modell, wie beim Original pro Seite 60 "Neonröhren" anbringen. 
Das Ergebniss ist auf den folgenden Fotos zu sehen