---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48186
- /details65fb.html
imported:
- "2019"
_4images_image_id: "48186"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48186 -->
Das Alu mit den Abstandshülsen. Die relativ aufwändige Konstruktion ist nötig, weil die "Neonröhren" aussen am
Rad montiert sind und sonst schnell Schaden (Transport) nehmen würden.