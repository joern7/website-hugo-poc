---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48187
- /details3f8e.html
imported:
- "2019"
_4images_image_id: "48187"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48187 -->
120 Röhrenhalter lackiert.