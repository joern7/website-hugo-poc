---
layout: "image"
title: "Wiener Riesenrad"
date: "2011-12-27T11:34:57"
picture: "wienerriesenrad2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33809
- /details7c34.html
imported:
- "2019"
_4images_image_id: "33809"
_4images_cat_id: "2498"
_4images_user_id: "968"
_4images_image_date: "2011-12-27T11:34:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33809 -->
Das aufgespeichte Rad.
1,55m Durchmesser. Die Speichen sind aus 1,8 mm Aludraht.
Mit diesem Draht wurden in den 60ziger und 70 Jahren Mechaniken für Pfeifenorgeln gebaut.
Heute wird das bei Renonvierungen durch dünnes Holz ersetzt,deshalb fallen die Aludrähte bei uns
 im Orgelbau als Abfall  an.