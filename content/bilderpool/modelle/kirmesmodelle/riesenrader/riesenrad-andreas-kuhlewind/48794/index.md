---
layout: "image"
title: "Segment"
date: 2020-08-09T19:40:29+02:00
picture: "rrad0103.jpg"
weight: "3"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
keywords: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Sechs dieser Segmente bilden dass äußere Rad