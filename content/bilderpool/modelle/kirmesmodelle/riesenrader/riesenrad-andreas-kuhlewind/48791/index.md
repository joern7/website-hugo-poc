---
layout: "image"
title: "Achse und Innenrad"
date: 2020-08-09T19:40:26+02:00
picture: "rrad0106.jpg"
weight: "6"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
keywords: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse besteht aus zwei Drehkränze, die durch Aluprofile verbunden werden.