---
layout: "image"
title: "Gesamtansicht mit Beleuchtung"
date: "2015-02-16T17:29:12"
picture: "riesenrad02.jpg"
weight: "2"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Riesenrad", "Kirmes", "txt", "Beleuchtung"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40551
- /details5531.html
imported:
- "2019"
_4images_image_id: "40551"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40551 -->
Die sechs Lampen werden paarweise vom TXT gesteuert.