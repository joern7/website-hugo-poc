---
layout: "image"
title: "Ein und Ausstieg"
date: "2015-02-16T19:29:09"
picture: "EinUndAusstieg02.jpg"
weight: "8"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Kirmes", "Riesenrad", "TXT"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40557
- /details2516-2.html
imported:
- "2019"
_4images_image_id: "40557"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T19:29:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40557 -->
Die Gondel hält, dank Lichtschranke am Rad Immer so an, dass man ein und aussteigen kann. Weitere Drehmodi werden mittels Lichtschranke und TXT geregelt.