---
layout: "image"
title: "Antriebsräder"
date: "2015-02-16T17:29:12"
picture: "Antrieb.jpg"
weight: "5"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40554
- /details4387.html
imported:
- "2019"
_4images_image_id: "40554"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40554 -->
Zuerst drehte ich - wie ich es häufig gesehen habe, das Rad per Motor und Kette an. Doch mein Sohn, der überall genau hinschaut und alles analysiert, hat mich darauf hingewiesen, dass ja in Wirklichkeit das Ganze von Rädern angetrieben würde.
Also suchte ich nach Lösungen dieses Riesenrad mit Gummirädern anzutreiben.