---
layout: "image"
title: "Riesenradrohbau"
date: "2007-11-24T07:59:33"
picture: "riesenrad1.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/12796
- /details3a52.html
imported:
- "2019"
_4images_image_id: "12796"
_4images_cat_id: "623"
_4images_user_id: "130"
_4images_image_date: "2007-11-24T07:59:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12796 -->
Noch eine andere Ansicht. Die gelben Statikteile waren nur als Platzhalter gedacht und sind inzwischen wieder entfernt.