---
layout: "image"
title: "Details vom Riesenrad4"
date: "2003-06-03T22:56:59"
picture: "CNXT0037_1.jpg"
weight: "5"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1174
- /details2100.html
imported:
- "2019"
_4images_image_id: "1174"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T22:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1174 -->
Die erste Ansicht von vorne.