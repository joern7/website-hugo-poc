---
layout: "image"
title: "Details vom Riesenrad 6"
date: "2003-06-03T22:56:59"
picture: "CNXT0041_1.jpg"
weight: "7"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1176
- /details0da7.html
imported:
- "2019"
_4images_image_id: "1176"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T22:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1176 -->
Seitenansicht