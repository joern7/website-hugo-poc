---
layout: "image"
title: "Launch-Kupplung"
date: "2009-10-31T14:15:31"
picture: "fischertechnikmegacoasterbauphase13.jpg"
weight: "13"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25614
- /details333c.html
imported:
- "2019"
_4images_image_id: "25614"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:31"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25614 -->
wird verwendet um den Wagen nach dem Abschuss zurückzuziehen

[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)