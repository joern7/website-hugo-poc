---
layout: "image"
title: "Looping"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase03.jpg"
weight: "3"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25604
- /detailsc7ab-3.html
imported:
- "2019"
_4images_image_id: "25604"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25604 -->
[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)