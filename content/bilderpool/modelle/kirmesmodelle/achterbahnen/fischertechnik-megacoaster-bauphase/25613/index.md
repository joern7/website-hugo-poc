---
layout: "image"
title: "Bügel zu (Wagen)"
date: "2009-10-31T14:15:31"
picture: "fischertechnikmegacoasterbauphase12.jpg"
weight: "12"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25613
- /details508f.html
imported:
- "2019"
_4images_image_id: "25613"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:31"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25613 -->
Die Bügel werden mit einer 15er I-Strebe am Sitz verriegelt. Trotz der Inversion haben die fischertechnik-Männchen viel Bewegungsfreiheit und können sich an der fahrt erfreuen :)

[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)