---
layout: "comment"
hidden: true
title: "10160"
date: "2009-10-31T22:49:52"
uploadBy:
- "C-Knobloch"
license: "unknown"
imported:
- "2019"
---
the "caddy" accelerates the coastercar. A XM Motor gets overvoltage (27 V) so that it can pull the caddy and the car. At the end of the Launch track, the caddy stops and the car follows the rest of the track by its self.

Der "Caddy" (Mitnehmer) beschleunigt den Wagen. Ein Xm motor bekommt 27V, sodass er den caddy und den Wagen nach vorn ziehen kann. Am Ende der Launch-Strecke stoppt der Caddy und der Waggen folgt der restlichen Strecke von selbst.

Christian