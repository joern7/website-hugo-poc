---
layout: "image"
title: "Launch-Überblick"
date: "2009-10-31T14:15:31"
picture: "fischertechnikmegacoasterbauphase09.jpg"
weight: "9"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25610
- /details2778.html
imported:
- "2019"
_4images_image_id: "25610"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:31"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25610 -->
Unten links die Kupplung, um den Caddy zurück ziehen zu können. oben ist die Seiltrommel, auf der das Seil aufgewickelt und somit der Wagen abgeschossen wird.

[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)