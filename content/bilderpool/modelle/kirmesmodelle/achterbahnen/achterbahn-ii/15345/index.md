---
layout: "image"
title: "Interface"
date: "2008-09-20T19:20:48"
picture: "achterbahn51.jpg"
weight: "51"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15345
- /details7deb.html
imported:
- "2019"
_4images_image_id: "15345"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15345 -->
Das Interface befindet sich unter dem Eingang der Station.