---
layout: "image"
title: "Sicherheitsbügel"
date: "2008-09-20T19:20:48"
picture: "achterbahn43.jpg"
weight: "43"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15337
- /details170b-2.html
imported:
- "2019"
_4images_image_id: "15337"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15337 -->
Die Bügel werden mit einem Gummi zurückgehalten.