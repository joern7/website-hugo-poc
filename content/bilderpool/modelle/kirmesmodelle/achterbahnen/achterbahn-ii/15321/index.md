---
layout: "image"
title: "Wagen in Kurve II"
date: "2008-09-20T19:20:47"
picture: "achterbahn27.jpg"
weight: "27"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15321
- /details24a3-2.html
imported:
- "2019"
_4images_image_id: "15321"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15321 -->
