---
layout: "image"
title: "Wagen auf dem Lifthill"
date: "2008-09-20T19:20:47"
picture: "achterbahn19.jpg"
weight: "19"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15313
- /details4b71.html
imported:
- "2019"
_4images_image_id: "15313"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15313 -->
