---
layout: "image"
title: "Wagen mit Fahrgästen"
date: "2008-09-20T19:20:48"
picture: "achterbahn49.jpg"
weight: "49"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15343
- /details2fb8.html
imported:
- "2019"
_4images_image_id: "15343"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15343 -->
