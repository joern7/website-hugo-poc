---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-20T19:20:46"
picture: "achterbahn06.jpg"
weight: "6"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15300
- /details10bf.html
imported:
- "2019"
_4images_image_id: "15300"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15300 -->
