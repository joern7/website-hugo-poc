---
layout: "image"
title: "Wagen in 3/4-Ansicht"
date: 2020-04-06T16:49:55+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Je ein Männchen sitzt - nun also doch mit einem Fremdteil - mit einem Gummi angeschnallt auf einem solchen Wagen. Links und rechts laufen Platten 15x60, und eine dritte liegt andersherum quer unter der Wagenmitte. Die beiden mittleren Zapfen aller drei Platten tragen je einen BS7,5, dessen Nuten alle in Längsrichtung zeigen.