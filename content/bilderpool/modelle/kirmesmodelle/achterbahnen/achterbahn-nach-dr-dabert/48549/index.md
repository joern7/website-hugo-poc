---
layout: "image"
title: "Wagen kurz vor der Freigabe (2)"
date: 2020-04-06T16:49:40+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

... und die Kralle vorne das Vorderrad freigibt. So kann der Wagen ruckfrei losrollen, sobald die Kralle nach unten aus dem Wagen geführt wird.