---
layout: "image"
title: "Streckenführung (3)"
date: 2020-04-06T16:49:36+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Diese (die obere) erste Steilkurve ist nicht symmetrisch und führt die Wagen zur mitten im Modell liegenden Kehrtwende.