---
layout: "image"
title: "Wagen von vorne"
date: 2020-04-06T16:49:54+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Drei der vier senkrecht stehenden Rollen bekommt man noch "normal" eingebaut, wenn man die Längsträger verschiebt. Beim vierten wird's eklig: Man muss es hineinquetschen, und der Zapfen der Bauplatte jammert dabei schon etwas.