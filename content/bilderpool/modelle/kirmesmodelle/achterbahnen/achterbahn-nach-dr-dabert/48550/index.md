---
layout: "image"
title: "Wagen kurz vor der Freigabe (1)"
date: 2020-04-06T16:49:41+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Wagen fangen oben schon an, herunter zu fahren und läuft schon etwas schneller als die Kette - bis das Hinterrad am Mitnehmer anstößt...