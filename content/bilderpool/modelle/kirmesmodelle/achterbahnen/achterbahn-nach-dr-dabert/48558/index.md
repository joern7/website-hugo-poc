---
layout: "image"
title: "Wagen mit abgenommenen Teilen"
date: 2020-04-06T16:49:50+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind BS7,5 mit waagerechtem Rad sowie eine Gewichtshalterung abgenommen, damit man den inneren Aufbau besser erkennt. Auf den mittigen BS7,5 sitzt über zwei Federnocken befestigt ein Baustein 5x15x30 3N. Der wiederum trägt einen BS5, und der den Sitz.