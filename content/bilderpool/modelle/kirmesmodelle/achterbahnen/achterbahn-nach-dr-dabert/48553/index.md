---
layout: "image"
title: "Wagen wird aufgenommen"
date: 2020-04-06T16:49:44+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Mitnehmer greift genau in den Raum zwischen Vorder- und Hinterrad und krallt sich am Vorderrad fest.