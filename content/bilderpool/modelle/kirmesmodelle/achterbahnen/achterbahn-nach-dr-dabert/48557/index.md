---
layout: "image"
title: "Befestigung des Sitzes"
date: 2020-04-06T16:49:49+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Sitzbefestigung bis auf den vom Sitz verdeckten BS5.