---
layout: "image"
title: "Streckenführung (2)"
date: 2020-04-06T16:49:38+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Den Fahrgästen wird aber keine Entspannung gegönnt, sondern es geht sofort ins zweite Tal und über einen Berg in die Kurve am anderen Ende der Bahn.