---
layout: "image"
title: "Wagen auf dem Lift Hill"
date: 2020-04-06T16:49:43+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Durch diesen Krall-Effekt wird nicht nur der Wagen zuverlässig hochgezogen, ohne fallen zu können. Er macht auch eine Unterstützung der Kette durch längs laufende Träger darunter unnötig, da die Kette vom Wagen selbst oben gehalten wird.