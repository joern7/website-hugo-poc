---
layout: "image"
title: "Streckenführung (8)"
date: 2020-04-06T16:49:31+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Nach der letzten Kurve sind die Figuren erlöst. Bis sie kurz darauf wieder hochgezogen werden. :-)