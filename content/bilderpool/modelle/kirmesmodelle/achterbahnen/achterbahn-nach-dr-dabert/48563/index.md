---
layout: "image"
title: "Gesamtansicht (3)"
date: 2020-04-06T16:49:56+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ich wollte keine Grundplatten, sondern ein selbsttragendes Gerüst für die Bahn. Durch die bei jeder sich bietenden Gelegenheit angebrachten Verstrebungen ist das ganze sehr stabil und kann problemlos herumgetragen werden.