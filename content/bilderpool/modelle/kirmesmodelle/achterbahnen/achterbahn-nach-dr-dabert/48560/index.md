---
layout: "image"
title: "Wagen von schräg unten"
date: 2020-04-06T16:49:52+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die vier waagerecht liegenden Räder sitzen hingegen einfach wieder an BS7,5. Links und rechts sitzen je zwei BS7,5 und ein BS5, der je einen der älteren Dauermagnete zum Austarieren des Gewichts halten.

Alle Räder und Radachsen sind auf leichtgängige Kombinationen handverlesen.