---
layout: "image"
title: "Antriebsmotor"
date: 2020-04-06T16:49:48+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein 9V-M-Motor mit einem Aufsteckgetriebe genügt für den Betrieb. Man beachte, dass das erste Zwischenrad des Aufsteckgetriebes nicht verwendet wird (das ist bei der neueren der Aufsteckgetriebe möglich). Dadurch läuft die Anlage hinreichend schnell.