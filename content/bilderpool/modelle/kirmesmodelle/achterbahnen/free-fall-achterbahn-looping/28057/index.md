---
layout: "image"
title: "26 Schiene"
date: "2010-09-07T18:06:07"
picture: "achterbahn26.jpg"
weight: "26"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28057
- /details07e2-2.html
imported:
- "2019"
_4images_image_id: "28057"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28057 -->
Auf diese Weise wird am Ende die Schiene vor und zurück geschoben. Ein Kurve konnte ich logischerweise nicht nehmen, da der Wagen sonst falschrum in der Station wäre. (sonst könnte er nicht rückwärts hochgezogen werden)