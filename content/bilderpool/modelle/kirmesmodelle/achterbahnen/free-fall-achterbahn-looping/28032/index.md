---
layout: "image"
title: "01 Free Fall Achterbahn mit Looping - Gesamtansicht"
date: "2010-09-07T18:06:04"
picture: "achterbahn01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28032
- /details8ea4-3.html
imported:
- "2019"
_4images_image_id: "28032"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28032 -->
Das ist nun die Bahn, die eigentlich zu diesem Wagen gehört: http://www.ftcommunity.de/details.php?image_id=27751
Da die Achterbahn jetzt aber schon ca. 3 Monaten bei mir steht, ohne dass ich an ihr weitergebaut habe, werde ich jetzt auch ganz aufhören und sie in ihre Einzelteile zerlegen. Das liegt schlichtweg daran, dass sie nicht funktioniert. In der Kurve vor dem Looping reißt der Wagen auseinander und entgleist, sodass er es nicht durch den Looping schafft und in der Mitte zum Stehen kommt. Woran das genau liegt, kann ich aber nicht sagen. Warscheinlich sind einfach die aus dem (fast) freien Fall resultierenden Kräfte so hoch, dass der Wagen der abrupten 90° Kurve nicht standhält und entgleist. Davon habe ich auch ein Video gemacht: http://www.youtube.com/watch?v=8W9eN5mJOCU