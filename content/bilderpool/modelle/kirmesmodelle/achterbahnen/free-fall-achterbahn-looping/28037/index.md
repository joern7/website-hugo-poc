---
layout: "image"
title: "06 Interface"
date: "2010-09-07T18:06:06"
picture: "achterbahn06.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28037
- /details000f.html
imported:
- "2019"
_4images_image_id: "28037"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28037 -->
Blick von der Schiene nach unten. Das sehen auch die ft-Menschen, wenn sie im Wagen nach unten fahren. 
Das 1/0 Extension steuert die Motoren und Sensoren oben im Turm, das Interface die am Boden. Die zwei dicken Kabel sind auch erkennbar, eins führt nach hinten zur Bremse, das andere in den Turm.