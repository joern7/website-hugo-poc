---
layout: "image"
title: "12 Sperre"
date: "2010-09-07T18:06:07"
picture: "achterbahn12.jpg"
weight: "12"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28043
- /details8e3d.html
imported:
- "2019"
_4images_image_id: "28043"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28043 -->
In der Nahaufnahme. Ein Teil der Schiene habe ich entfernt, damit man sie besser erkennen kann.