---
layout: "comment"
hidden: true
title: "12109"
date: "2010-09-08T02:28:50"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Der Looping ist nicht rund genug - viel zuviele Ecken und (eingefügte) Geraden.
Und da die Wagen sicher relativ leicht sind, ist die Schwungmasse deutlich zu
gering, als daß sie gleichmäßig durchlaufen können. Ich würde die Bausteine
15 im Looping weglassen und den auch leicht konisch (oval) gestalten - also
etwa 1,1 bis 1,2 mal so hoch wie breit; auch das läßt die Wagen gleichmäßiger
durchlaufen (liegt an den Fliehkräften innerhalb des Loopings).

Gruß, Thomas