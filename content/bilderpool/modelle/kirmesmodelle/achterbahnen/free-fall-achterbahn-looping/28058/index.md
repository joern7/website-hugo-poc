---
layout: "image"
title: "27 Bremse"
date: "2010-09-07T18:06:07"
picture: "achterbahn27.jpg"
weight: "27"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28058
- /detailse5c2-2.html
imported:
- "2019"
_4images_image_id: "28058"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28058 -->
Von beiden Seiten und von unten drücken diese Platten gegen den Wagen, damit er abbremst.