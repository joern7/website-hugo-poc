---
layout: "image"
title: "10 Schienenbefestigung"
date: "2010-09-07T18:06:07"
picture: "achterbahn10.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28041
- /details8909-2.html
imported:
- "2019"
_4images_image_id: "28041"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28041 -->
Die zwei Varianten, mit denen ich die Schienen am Turm befestigt habe. 
Auf der hinteren Schiene wird der Wagen hochgezogen, auf der vorderen fährt er runter.