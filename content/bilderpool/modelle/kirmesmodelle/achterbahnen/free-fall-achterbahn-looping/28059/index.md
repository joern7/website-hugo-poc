---
layout: "image"
title: "28 Schiene"
date: "2010-09-07T18:06:07"
picture: "achterbahn28.jpg"
weight: "28"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28059
- /details54ee-2.html
imported:
- "2019"
_4images_image_id: "28059"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28059 -->
Die Schiene vorne.