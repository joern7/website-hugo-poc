---
layout: "image"
title: "04 Gleisverschiebeung"
date: "2010-09-07T18:06:06"
picture: "achterbahn04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28035
- /details7ce3-4.html
imported:
- "2019"
_4images_image_id: "28035"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28035 -->
Am hinteren Teil der Bahn wird der Wagen gebremst, kommt aber noch auf die Schiene, die ihn dann an den Anfang zurück bringt. (theoretisch)
Vorne ist die Pneumatikstation zu sehen, die für das Bremsen des Wagens verantwortlich ist. (Kompressor, Druckabschaltung, Ventil)
Die Bremse ist auch nur provisorisch da und nicht genau aus die Geschwindigkeit des Wagens abgestimmt, denn so weit kommt er ja eh nur sehr selten.