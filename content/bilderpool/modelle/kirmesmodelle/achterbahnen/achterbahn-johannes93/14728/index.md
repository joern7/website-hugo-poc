---
layout: "image"
title: "Kurve 4"
date: "2008-06-21T18:28:23"
picture: "achterbahn10.jpg"
weight: "10"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14728
- /detailsc2c9.html
imported:
- "2019"
_4images_image_id: "14728"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14728 -->
Die Kurve 4 ist die schärfste Kurve der Bahn.