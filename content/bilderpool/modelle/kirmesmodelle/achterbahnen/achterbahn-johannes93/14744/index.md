---
layout: "image"
title: "Lift"
date: "2008-06-21T18:28:24"
picture: "achterbahn26.jpg"
weight: "26"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14744
- /detailsc759-3.html
imported:
- "2019"
_4images_image_id: "14744"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14744 -->
Der Lift hakt sich vorne in den Wagen ein.