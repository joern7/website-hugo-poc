---
layout: "image"
title: "Antrieb Lift"
date: "2008-06-21T18:28:23"
picture: "achterbahn14.jpg"
weight: "14"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14732
- /details2aa6-2.html
imported:
- "2019"
_4images_image_id: "14732"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14732 -->
Der Antrieb erfolgt über einen Powermotor mit roter Kappe.