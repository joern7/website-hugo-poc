---
layout: "image"
title: "Wagen"
date: "2008-06-21T18:28:24"
picture: "achterbahn17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14735
- /details9be4-3.html
imported:
- "2019"
_4images_image_id: "14735"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14735 -->
Der Wagen liegt mit den weißen Rädern auf der Strecke. Die seitlichen Räder verhindern das entgleisen des Wagens.