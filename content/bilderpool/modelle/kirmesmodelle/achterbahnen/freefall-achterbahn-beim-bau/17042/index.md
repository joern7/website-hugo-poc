---
layout: "image"
title: "Fahrwagen Version 7"
date: "2009-01-17T13:23:39"
picture: "freefallachterbahnbeimbau02.jpg"
weight: "2"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17042
- /details3392.html
imported:
- "2019"
_4images_image_id: "17042"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17042 -->
Bild wurde zum besseren Verständnis um 180° gedreht. Diese Version war optisch mein Favorit, leider hat sie sich trotz anfänglichem Optimismus als zu "bremsend" erwiesen .Die Sitzbank wäre noch unten hingekommen.