---
layout: "image"
title: "Fahrwagen Version 10"
date: "2009-01-17T13:23:39"
picture: "freefallachterbahnbeimbau03.jpg"
weight: "3"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17043
- /detailse01c.html
imported:
- "2019"
_4images_image_id: "17043"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17043 -->
Bild wurde zum besseren Verständnis um 180° gedreht. Das Rad 23 ist vom Rollwiederstand die beste Lösung. Aber diese Version entgleiste auch bei extremer Belastung von 1,5 Meter  "freiem Fall" in die 90° Kurve. Die normalen Bausteine waren hier doch zu flexibel und gaben nach.