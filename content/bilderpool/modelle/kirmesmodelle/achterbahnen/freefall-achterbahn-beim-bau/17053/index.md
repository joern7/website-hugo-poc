---
layout: "image"
title: "Linker und rechter Turm"
date: "2009-01-17T14:45:22"
picture: "freefallachterbahnbeimbau13.jpg"
weight: "13"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17053
- /detailsb547.html
imported:
- "2019"
_4images_image_id: "17053"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T14:45:22"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17053 -->
Hier sieht man auch die Motorisierung des ersten Aufzuges, 2 x Power-Motor  50:1 (rot)