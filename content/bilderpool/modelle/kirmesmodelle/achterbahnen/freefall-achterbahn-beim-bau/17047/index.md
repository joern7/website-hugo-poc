---
layout: "image"
title: "Fahrwagen Version 14"
date: "2009-01-17T13:23:39"
picture: "freefallachterbahnbeimbau07.jpg"
weight: "7"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17047
- /details569f-2.html
imported:
- "2019"
_4images_image_id: "17047"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17047 -->
Das ist bis jetzt die aktuelle Version. Durch die Alus ist es sehr stabil geworden, aber auch schwerer. Ein Fahrwagen wiegt unbemannt 250 Gramm. Im Bild sind 2 Fahrwagen zu sehen.