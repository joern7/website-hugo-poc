---
layout: "image"
title: "Drehung der Schiene"
date: "2007-02-04T12:35:36"
picture: "achterbahn6.jpg"
weight: "6"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8876
- /detailsd292.html
imported:
- "2019"
_4images_image_id: "8876"
_4images_cat_id: "805"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8876 -->
Damit Kurven möglich sind, muss die Schiene gedreht werden. Das muss so lang sein, weil sonst der Wagen aus der Führung springt.