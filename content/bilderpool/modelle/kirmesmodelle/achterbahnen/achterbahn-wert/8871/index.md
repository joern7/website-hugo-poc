---
layout: "image"
title: "Gerade Strecke"
date: "2007-02-04T12:35:30"
picture: "achterbahn1.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8871
- /detailsbe7a.html
imported:
- "2019"
_4images_image_id: "8871"
_4images_cat_id: "805"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8871 -->
Dass soll einmal eine Achterbahn werden. Da ich aber noch nicht genug Führungsschienen habe, kann das noch dauern... Hier sieht man ein gerades Stück Strecke.