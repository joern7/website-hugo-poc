---
layout: "comment"
hidden: true
title: "11854"
date: "2010-07-16T11:06:05"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Es sind nicht nur die elastischen Winkelsteine und besonders die BS7,5. Die schiere Anzahl an Verbindungs- nein, eher Trennstellen macht es. Mit je einem Winkelträger oben und unten, nebst Querversteifungen wird das brauchbar, wenn auch nicht so hübsch.

Gruß,
Harald