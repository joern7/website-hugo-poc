---
layout: "image"
title: "04 ohne Menschen"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen4.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27754
- /detailsffae.html
imported:
- "2019"
_4images_image_id: "27754"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27754 -->
Ohne Menschen kann man die Sitze sehen. Theoretisch würden wohl auch mehr Sitze auch einen Wagen passen, aber das sähe dann wohl nicht mehr so schön aus.