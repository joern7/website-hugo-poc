---
layout: "image"
title: "High Speed Coaster Car"
date: "2006-07-08T23:04:53"
picture: "FT_002.jpg"
weight: "5"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/6607
- /detailsae1a.html
imported:
- "2019"
_4images_image_id: "6607"
_4images_cat_id: "569"
_4images_user_id: "430"
_4images_image_date: "2006-07-08T23:04:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6607 -->
Gelaunchter Wagen