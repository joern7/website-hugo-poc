---
layout: "image"
title: "Aufzug für die Achterbahn(8)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn08.jpg"
weight: "8"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15568
- /details7d19-2.html
imported:
- "2019"
_4images_image_id: "15568"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15568 -->
Von Oben kann man alles sehen.