---
layout: "image"
title: "Aufzug für die Achterbahn(1)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn01.jpg"
weight: "1"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15561
- /detailsabf0.html
imported:
- "2019"
_4images_image_id: "15561"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15561 -->
Da ist die Seite zu sehen und der Wagen in der Gondel.