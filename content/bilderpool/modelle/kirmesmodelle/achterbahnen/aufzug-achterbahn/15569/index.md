---
layout: "image"
title: "Aufzug für die Achterbahn(9)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn09.jpg"
weight: "9"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15569
- /details5e2c.html
imported:
- "2019"
_4images_image_id: "15569"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15569 -->
Da an der linken Seite sieht man die Endschalter.