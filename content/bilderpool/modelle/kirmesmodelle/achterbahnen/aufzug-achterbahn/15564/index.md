---
layout: "image"
title: "Aufzug für die Achterbahn(4)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn04.jpg"
weight: "4"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15564
- /detailsf5c2.html
imported:
- "2019"
_4images_image_id: "15564"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15564 -->
Hier ist der Antrieb zum drehen.