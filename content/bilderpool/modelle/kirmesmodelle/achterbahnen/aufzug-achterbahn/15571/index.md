---
layout: "image"
title: "Aufzug für die Achterbahn(11)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn11.jpg"
weight: "11"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15571
- /detailsae32.html
imported:
- "2019"
_4images_image_id: "15571"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15571 -->
Um die Gondel zu neigen, damit der Wagen nicht runter fährt wird über das Hubgetriebe die Gondel geneigt.