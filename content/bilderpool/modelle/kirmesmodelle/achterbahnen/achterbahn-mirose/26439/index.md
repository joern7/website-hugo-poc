---
layout: "image"
title: "Detail Wagen untere Rollen"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_43.jpg"
weight: "35"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26439
- /details126b.html
imported:
- "2019"
_4images_image_id: "26439"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26439 -->
unten seht Ihr den Möbelmagnet, der mit doppelseitigem Klebeband auf einer Bauplatte 15 x 30 aufgeklebt ist.