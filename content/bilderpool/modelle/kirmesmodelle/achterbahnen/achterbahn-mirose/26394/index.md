---
layout: "image"
title: "Kurz vorm Kamelbuckel"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_26.jpg"
weight: "26"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26394
- /details504a.html
imported:
- "2019"
_4images_image_id: "26394"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26394 -->
