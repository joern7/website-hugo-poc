---
layout: "image"
title: "Im Looping mit Schwung nach oben"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_21.jpg"
weight: "21"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26389
- /details1527.html
imported:
- "2019"
_4images_image_id: "26389"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26389 -->
Das Video:

http://www.youtube.com/watch?v=sXhuZxIf68g
_