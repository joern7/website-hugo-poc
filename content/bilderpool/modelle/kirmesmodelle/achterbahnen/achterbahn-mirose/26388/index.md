---
layout: "image"
title: "Beginn des Loopings"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_20.jpg"
weight: "20"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26388
- /details7672-2.html
imported:
- "2019"
_4images_image_id: "26388"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26388 -->
Mit mehreren Winkelsteinen 15 kann man eine Kurve bauen (klingt brutal, ist aber so). Das war notwendig, da sich die Winkelträger beim Befahren mit dem Wagen so verbogen haben, daß sich die Wagenräder an den Schienen verhakt haben, und der Wagen ruckartig zu stehen kam.

Das Video:

http://www.youtube.com/watch?v=sXhuZxIf68g

_