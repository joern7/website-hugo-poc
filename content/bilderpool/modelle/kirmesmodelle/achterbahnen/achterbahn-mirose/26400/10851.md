---
layout: "comment"
hidden: true
title: "10851"
date: "2010-02-13T22:40:20"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

die Rollen sind aus Kunststoff, 20 mm Durchmesser (Conrad), selbst auf der Drehbank gedreht.
Das Video folgt, wird aber noch etwas dauern.
Es fehlt noch die Steuerung, derzeit sind noch alle Motoren parallel geschaltet.

Ich habe vor, dieses Modell auf der Convention in Erbes-Büdesheim (Sept. 2010) herzuzeigen.

Viele Grüße

Mirose