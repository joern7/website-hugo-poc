---
layout: "image"
title: "Während der Hinauffahrt"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_14.jpg"
weight: "14"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26382
- /detailseb1f.html
imported:
- "2019"
_4images_image_id: "26382"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26382 -->
Den Aufzug habe ich mit 2 Stück Aluprofile 15 x 15 mm; 2 m lang (aus dem Baumarkt) versteift