---
layout: "image"
title: "Wagenmitnehmer"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_48.jpg"
weight: "40"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["modding"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26444
- /detailsd96f.html
imported:
- "2019"
_4images_image_id: "26444"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26444 -->
der bearbeitete S-Riegel klinkt sich in die Kette ein.
Hier ist die alte Kette abgebildet, die neue rote eignet sich für den Aufzug im Bereich der Niederhalter (bearbeitet Bausteine 7,5) jedoch besser, da sie breiter ist und sich nicht so leicht verhakt.