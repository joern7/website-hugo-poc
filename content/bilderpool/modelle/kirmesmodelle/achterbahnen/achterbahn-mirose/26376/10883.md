---
layout: "comment"
hidden: true
title: "10883"
date: "2010-02-14T17:09:51"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Michael,

Wieder ein sehr schönes Top-Klasse-Modell ! 

Woher kommen die "Diabolo-" Seilrolle ?
.....oder hast du die Selber gemacht ?

Ich habe schön mit Thomas Falkenberg (Speedy) verabredet in der Alte Backstube.
Thomas hat schon reerviert für das FT-Treffen in Erbes-Budesheim.

Grüss,

Peter, Poederoyen NL