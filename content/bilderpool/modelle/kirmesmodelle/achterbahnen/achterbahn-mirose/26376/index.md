---
layout: "image"
title: "Der Wagen hat soeben die Einstiegsstelle verlassen"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_08.jpg"
weight: "8"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26376
- /details7a55.html
imported:
- "2019"
_4images_image_id: "26376"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26376 -->
Alle Fotos sind mit stehendem Fahrzeug aufgenommen worden! (= technisches Stilleben)

Das Video:

http://www.youtube.com/watch?v=sXhuZxIf68g
_