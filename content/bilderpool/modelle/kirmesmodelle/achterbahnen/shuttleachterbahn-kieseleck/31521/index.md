---
layout: "image"
title: "Gesamt"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck01.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31521
- /details67aa.html
imported:
- "2019"
_4images_image_id: "31521"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31521 -->
Hier sieht man die gesamte Achterbahn. Wenn man den Start-Taster gedrückt hat, wird der Passagierwagen vom Magnetwagen (er hängt am Seil) und schließlich losgelassen; unten wird er vom Federbrett abgefangen.