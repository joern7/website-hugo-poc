---
layout: "image"
title: "Im Notfall"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck22.jpg"
weight: "22"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31542
- /details3b27-2.html
imported:
- "2019"
_4images_image_id: "31542"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31542 -->
So sieht es aus, wenn man Notaus gedrückt hat ;-) (dabei bleibt der Magnet an)