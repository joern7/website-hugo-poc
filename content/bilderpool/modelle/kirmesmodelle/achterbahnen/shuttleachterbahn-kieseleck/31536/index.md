---
layout: "image"
title: "Detail Passagierwagen"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck16.jpg"
weight: "16"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31536
- /details757b.html
imported:
- "2019"
_4images_image_id: "31536"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31536 -->
Hier sieht man das Stück Metal, das der Magnet anzieht