---
layout: "image"
title: "Seilzug"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck17.jpg"
weight: "17"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31537
- /details1344-2.html
imported:
- "2019"
_4images_image_id: "31537"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31537 -->
Hier sieht man den PM mit Seilwinde, der den Magnetwagen hochzieht.