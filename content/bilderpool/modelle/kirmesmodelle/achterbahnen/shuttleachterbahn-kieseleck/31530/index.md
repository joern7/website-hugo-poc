---
layout: "image"
title: "The Hill"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck10.jpg"
weight: "10"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31530
- /details7ba2-2.html
imported:
- "2019"
_4images_image_id: "31530"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31530 -->
Hier ist der "Hill", den der Wagen hochgezogen wird.