---
layout: "image"
title: "Gateampel"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck08.jpg"
weight: "8"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31528
- /details61d8.html
imported:
- "2019"
_4images_image_id: "31528"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31528 -->
Sie zeigt auch an wann die Anlage in Betrieb ist.