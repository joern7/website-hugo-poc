---
layout: "image"
title: "Der Wagen 4.0"
date: "2007-01-14T17:39:35"
picture: "wagen1_2.jpg"
weight: "20"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/8461
- /detailsd4f8-2.html
imported:
- "2019"
_4images_image_id: "8461"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2007-01-14T17:39:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8461 -->
Da nichts so klappt wie ich will, hab ich mich entschlossen, das ich den Lifthill durch einen Aufzug ersetzte und das ich dann gleich den Wagen 4.0 rausbrinnge. Das Besondere am Wagen 4.0 ist das er sich kontroliert drehen kann. Wie es in der realität klappt seht ihr demnächst.