---
layout: "image"
title: "Bremse"
date: "2006-11-25T19:00:05"
picture: "bremseundstation5.jpg"
weight: "16"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7613
- /details0b7c.html
imported:
- "2019"
_4images_image_id: "7613"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7613 -->
Hier ist die Bremse ie den Wagen stoppen soll. Die Bremse ist im geschlossenen Zustand