---
layout: "image"
title: "Die Bremse von Vorne"
date: "2006-11-25T19:00:05"
picture: "bremseundstation6.jpg"
weight: "17"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7614
- /detailsfbaf-3.html
imported:
- "2019"
_4images_image_id: "7614"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7614 -->
Hier sieht man das gekippte Bremsbrett.