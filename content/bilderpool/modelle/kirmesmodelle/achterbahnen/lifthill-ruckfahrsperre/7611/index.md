---
layout: "image"
title: "Die Station ohne Wagen"
date: "2006-11-25T19:00:05"
picture: "bremseundstation3.jpg"
weight: "14"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7611
- /details7e40-2.html
imported:
- "2019"
_4images_image_id: "7611"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7611 -->
Die Schiene bei der Station hat eine Rückfahrsperre drinnen damit der Wagen nicht zurückrollen kann. Desweiteren ist neben der Rückfahrsperre ein Winkelträger(rechts. Auf dem Winkelträger soll später die Kette laufen, sie den Wagen dann Hochzieht.
Link sieht man noch den Motor der die Türen (gates) antreibt.