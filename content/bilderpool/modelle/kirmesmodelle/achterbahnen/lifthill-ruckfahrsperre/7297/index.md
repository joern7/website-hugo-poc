---
layout: "image"
title: "Der Wagen 3.0 Draufsicht"
date: "2006-11-01T21:51:43"
picture: "wagen3.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7297
- /detailsa599.html
imported:
- "2019"
_4images_image_id: "7297"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-01T21:51:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7297 -->
In der Vogelperspektive sieht man sehr schön die aufteilung des neuen Wagens