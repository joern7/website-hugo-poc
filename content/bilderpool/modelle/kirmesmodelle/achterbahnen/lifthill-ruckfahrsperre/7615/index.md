---
layout: "image"
title: "Die Technik der Bremse"
date: "2006-11-25T19:00:05"
picture: "bremseundstation7.jpg"
weight: "18"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7615
- /details07b8.html
imported:
- "2019"
_4images_image_id: "7615"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7615 -->
Hier sieht man die Technik der Bremse.
Ein Mini Motor treibt eine Schnecke an die das Brett klippen lässt.