---
layout: "image"
title: "Die neuen Räder"
date: "2006-11-01T21:51:43"
picture: "wagen4.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7298
- /details1e6f-2.html
imported:
- "2019"
_4images_image_id: "7298"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-01T21:51:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7298 -->
Die neue Anordnung der Räder verspricht eine reibungsarme Fahrt.