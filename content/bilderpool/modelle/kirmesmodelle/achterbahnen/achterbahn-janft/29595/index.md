---
layout: "image"
title: "Kasse"
date: "2011-01-02T17:21:30"
picture: "Bild_028.jpg"
weight: "2"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29595
- /detailsc7cc.html
imported:
- "2019"
_4images_image_id: "29595"
_4images_cat_id: "2158"
_4images_user_id: "1164"
_4images_image_date: "2011-01-02T17:21:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29595 -->
auf den weißen Schild links sollte eigentlich stehen: 

Erw.
2,00€

Kinder
1,50€

ist leider etwas unscharf