---
layout: "image"
title: "Achterbahn Simulator - Erste Versuche"
date: "2013-04-14T21:16:32"
picture: "Achterbahnwagen-V1.jpg"
weight: "1"
konstrukteure: 
- "Jonas"
fotografen:
- "Markus"
keywords: ["Achterbahn", "Simulator", "Jonas", "Wagen"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36827
- /detailsb021.html
imported:
- "2019"
_4images_image_id: "36827"
_4images_cat_id: "2734"
_4images_user_id: "1608"
_4images_image_date: "2013-04-14T21:16:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36827 -->
Hier ein erstes Bild von dem Achterbahnwagen, der später vor dem Bildschirm steht. Auf dem Bildschirm kommt ein Video meiner RCT3 Achterbahn. Der Wagen wird dazu die passenden Bewegungen machen.