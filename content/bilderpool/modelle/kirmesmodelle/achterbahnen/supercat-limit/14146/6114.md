---
layout: "comment"
hidden: true
title: "6114"
date: "2008-03-30T21:24:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Großen Respekt, ich freue mich auf das erste Video vom Stapellauf! Ein Tipp: Die Schneckenmutter wird arg beansprucht. Du könntest vielleicht zwei davon in ein paar cm Abstand verwenden. Damit die Schneckenachse nicht so belastet wird, könntest Du noch eine Führung z. B. gegen die Statikträger oder noch mehr lange Achsen ergänzen.

Gruß,
Stefan