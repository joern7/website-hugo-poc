---
layout: "image"
title: "Sicht aus dem Wagen"
date: "2008-03-14T07:06:10"
picture: "supercat1_2.jpg"
weight: "22"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13900
- /detailsc9ac-2.html
imported:
- "2019"
_4images_image_id: "13900"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-03-14T07:06:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13900 -->
Die Insassen in der ersten Reihe werden ungefähr das sehen :)