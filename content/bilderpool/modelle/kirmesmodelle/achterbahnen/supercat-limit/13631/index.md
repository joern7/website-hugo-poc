---
layout: "image"
title: "Supercat Fotomaschine"
date: "2008-02-09T22:49:06"
picture: "sc09.jpg"
weight: "20"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13631
- /details2be7-2.html
imported:
- "2019"
_4images_image_id: "13631"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13631 -->
Diesen Lichtblitz soll die Gabellichtschranke später auslösen.