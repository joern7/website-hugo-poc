---
layout: "image"
title: "Supercat untere Strecke Teil 2"
date: "2008-02-09T22:49:06"
picture: "sc08.jpg"
weight: "19"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13630
- /details235e-2.html
imported:
- "2019"
_4images_image_id: "13630"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13630 -->
Das gleiche in umgekehrter Reihenfolge:
Gabellichtschranke, Bremsschwerter, Reibräder (die ich mal austauschen sollte) 
Die Gabellichtschranke soll durch das Bremsschwert, das sich unter dem Wagen befindet, unterbrochen werden. Da ich aber schon mal Test mit so was gemacht habe befürchte ich, dass das Bremsschwert einfach zuviel Licht durchlässt. Wahrscheinlich muss da wieder schwarzes Isolierband aushelfen.