---
layout: "image"
title: "Supercat - Mal wieder ein Problem - fortsetzung"
date: "2008-08-31T14:01:49"
picture: "sc2_4.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/15160
- /detailsb294.html
imported:
- "2019"
_4images_image_id: "15160"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-08-31T14:01:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15160 -->
...das der Baustein, der auf den Taster aufschlagen sollte den nicht immer trifft.
Meine Frage ist, ob man das durch einen simplen Trick ändern kann oder ob man den ganzen Taster versetzen muss.