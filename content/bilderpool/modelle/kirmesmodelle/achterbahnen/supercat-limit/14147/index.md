---
layout: "image"
title: "Supercat Station"
date: "2008-03-30T20:29:26"
picture: "sc3_2.jpg"
weight: "26"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/14147
- /details0652-2.html
imported:
- "2019"
_4images_image_id: "14147"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-03-30T20:29:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14147 -->
Hier sieht man die Station.
Rechts ist der Ausgang mit dem "Operator Pult" :)