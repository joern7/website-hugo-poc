---
layout: "image"
title: "Supercat Aufzug Seiltrommel"
date: "2008-01-19T17:53:54"
picture: "sc6.jpg"
weight: "6"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13351
- /detailsf64c.html
imported:
- "2019"
_4images_image_id: "13351"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13351 -->
Das ist auch ein Grund warum ich die Wippe nochmal überholen muss, da die runden Scheiben erst fürs Halten der Wippe zuständig waren.
Nachdem ich aber hier relativ viel Seil aufwickeln muss, hat sich die Art der Rolle perfekt angeboten.
Die angeschlossene Motoren haben eine Umsetztung 8:1