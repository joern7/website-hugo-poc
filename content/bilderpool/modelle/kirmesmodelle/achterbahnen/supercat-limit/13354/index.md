---
layout: "image"
title: "Supercat Bremse und Station"
date: "2008-01-19T17:53:54"
picture: "sc9.jpg"
weight: "9"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13354
- /details8c5c-2.html
imported:
- "2019"
_4images_image_id: "13354"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13354 -->
Von rechts nach links:
Bremse
Das soll die Bremse darstellen, die den Wagen auch richtig Abbremsen soll (ob es funktioniert weiß ich nicht).
Der Statikträger 60 (der der ein bisschen schief ist) ist der bewegliche Teil. Er ist mit Gummis umwickelt damit das ganze einen richtigen Grip hat :)
Da nach folgt ein Reibrad, dass den Wagen aus der Bremse in die Station schieben soll.
In der Station (noch nicht fertig) soll dann das Ein- und Aussteigen gewährleistet werden. Des weiteren soll auch überprüft, ob alle Bügel geschlossen sind.
Das Prinzip des Reibrades ist sehr einfach. Unten am Wagen befindet sich ein Bremsschwert, das zwischen den beiden Reifen gerät und somit angetrieben werden kann.