---
layout: "image"
title: "Supercat Abbau - Station von oben"
date: "2008-12-24T12:16:40"
picture: "supercatabbau10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16720
- /detailsebeb.html
imported:
- "2019"
_4images_image_id: "16720"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16720 -->
Da nun das Dach weg ist, kann man wunderbar von oben auf die Technik der Schiene schauen.