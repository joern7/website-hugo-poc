---
layout: "image"
title: "Supercat Abbau - Technik der Station"
date: "2008-12-24T12:16:40"
picture: "supercatabbau14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16724
- /detailsde9d.html
imported:
- "2019"
_4images_image_id: "16724"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16724 -->
Wunderbar zu sehen sind die im vorher angesprochenen Reed-Kontakte