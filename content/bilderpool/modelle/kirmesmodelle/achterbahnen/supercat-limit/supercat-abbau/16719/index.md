---
layout: "image"
title: "Supercat Abbau - Station ohne Dach"
date: "2008-12-24T12:16:40"
picture: "supercatabbau09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16719
- /details4042.html
imported:
- "2019"
_4images_image_id: "16719"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16719 -->
Hier wurde als erstes das Dach abgenommen.