---
layout: "image"
title: "Supercat Abbau - Gates Getriebe"
date: "2008-12-24T12:16:41"
picture: "supercatabbau17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16727
- /details7bfe.html
imported:
- "2019"
_4images_image_id: "16727"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16727 -->
Noch mal das Getriebe der Tore. Hier ist der Taster besser zusehen.