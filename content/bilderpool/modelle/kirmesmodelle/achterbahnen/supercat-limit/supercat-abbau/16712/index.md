---
layout: "image"
title: "Supercat Abbau - Ohne Welle"
date: "2008-12-24T12:16:39"
picture: "supercatabbau02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16712
- /details22e6.html
imported:
- "2019"
_4images_image_id: "16712"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16712 -->
Hier sieht man die Stützkonstruktion, welche die Welle getragen hat.