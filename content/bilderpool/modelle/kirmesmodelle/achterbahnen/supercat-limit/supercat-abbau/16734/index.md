---
layout: "image"
title: "Supercat Abbau - Bogen"
date: "2008-12-24T12:16:41"
picture: "supercatabbau24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16734
- /details745b-3.html
imported:
- "2019"
_4images_image_id: "16734"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16734 -->
Von der Wippe (rechts) zur Station (links).
Die beiden platten, die an Winkelsteine angebracht sind sollten den Wagen über das Bremsschwert zentrieren. Damit er sich nicht mehr an der Bremse sich verfängt.