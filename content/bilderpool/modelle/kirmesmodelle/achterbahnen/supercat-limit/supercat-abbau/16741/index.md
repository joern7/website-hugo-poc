---
layout: "image"
title: "Supercat Abbau - Reste des Aufzugs"
date: "2008-12-24T12:16:41"
picture: "supercatabbau31.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16741
- /details7d93-2.html
imported:
- "2019"
_4images_image_id: "16741"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16741 -->
Unten sieht man die Reibräder sowie die Bremsanlage.