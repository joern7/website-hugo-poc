---
layout: "image"
title: "Supercat neue Wippe Übergang"
date: "2008-02-09T22:49:05"
picture: "sc02.jpg"
weight: "13"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13624
- /details8909-3.html
imported:
- "2019"
_4images_image_id: "13624"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13624 -->
Hier sieht man den Übergang vom Beweglichen Teil zur festen Strecke.
Leider ist hier der Stoß noch nicht wirklich das Wahre.
Vielleicht kann ich das mit einer Vorrichtung vermeiden, die den beweglichen Teil zentriert.