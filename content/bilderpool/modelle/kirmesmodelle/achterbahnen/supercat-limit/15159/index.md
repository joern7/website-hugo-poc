---
layout: "image"
title: "Supercat - Mal wieder ein Problem"
date: "2008-08-31T14:01:48"
picture: "sc1_6.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/15159
- /details3ba2.html
imported:
- "2019"
_4images_image_id: "15159"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-08-31T14:01:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15159 -->
Bei der momentanen Problembekämpfung bei meiner Achterbahn, bin ich auf folgendes gestoßen.
DIESER Taster sollte einen Stop der Motoren von der Wippe auslösen. 
Das Problem, besteht darin, das dieser Taster nicht immer auslöst. 
Das kann daran liegen, das der Taster nicht absolut starr befestigt ist oder...
(nächstes Bild)