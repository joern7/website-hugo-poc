---
layout: "image"
title: "Achterbahn03.JPG"
date: "2007-10-23T19:54:47"
picture: "Achterbahn03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12294
- /detailsf7f1.html
imported:
- "2019"
_4images_image_id: "12294"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:54:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12294 -->
Variante der Befestigung mit dem Doppelzapfen aus einem kaputten BS15 mit zwei Zapfen. Hier sieht man auch, wie die Flachnaben auf der Oberseite des Gleises aufliegen. Die Naben sind soweit schräg gestellt, dass sie seitlich nicht unter die Gleise hineingreifen. Dadurch können sie aber wiederum leicht abheben, was zum Ende der zwangsgeführten Fortbewegung führt (zu deutsch: sie entgleisen). Um das zu verhindern, wird auf die Oberseite des Gleises ein "Schwert" aufgesetzt, hier z.B. mit den schwarzen Lochstreben im Doppelpack.