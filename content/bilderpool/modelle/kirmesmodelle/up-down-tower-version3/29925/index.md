---
layout: "image"
title: "Turmspitze"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion08.jpg"
weight: "8"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29925
- /detailsbaa9.html
imported:
- "2019"
_4images_image_id: "29925"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29925 -->
Seitliche Sicht auf die gesamte Turmspitze mit Hochzieher, Abkoppelmechanik und Seilwinde.