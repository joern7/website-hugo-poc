---
layout: "image"
title: "Mittelturm-Hauptturm"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion03.jpg"
weight: "3"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29920
- /details7f33.html
imported:
- "2019"
_4images_image_id: "29920"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29920 -->
Der Mittelturm wird mit den schwarzen Adaptern (ganz oben / unten) mit Station und Turmspitze verbunden. In der Mitte: die Mittelstation; der Zug kann vom Hochzieher abgekoppelt werden; Fahrzeit: 4min.