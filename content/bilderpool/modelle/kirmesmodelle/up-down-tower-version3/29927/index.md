---
layout: "image"
title: "Detail-Hochzieher"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion10.jpg"
weight: "10"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29927
- /details3007.html
imported:
- "2019"
_4images_image_id: "29927"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29927 -->
Der Hochzieher: Der vordere Teil ist beweglich gelagert, sodass er in den Zug vorne einhaken kann. Diese Verankerung wird wieder gelöst, indem 2 Stangen diese Klappe wegdrücken.