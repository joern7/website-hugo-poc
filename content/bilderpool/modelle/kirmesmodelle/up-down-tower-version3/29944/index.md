---
layout: "image"
title: "Zug4"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion27.jpg"
weight: "27"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29944
- /details39aa.html
imported:
- "2019"
_4images_image_id: "29944"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29944 -->
Hier seht ihr noch mal die Gelenke, mit denen die Wagen verbunden sind.