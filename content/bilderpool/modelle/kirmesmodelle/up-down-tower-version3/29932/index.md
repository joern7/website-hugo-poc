---
layout: "image"
title: "Adapter zwischen den Türmen"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion15.jpg"
weight: "15"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29932
- /details855a-2.html
imported:
- "2019"
_4images_image_id: "29932"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29932 -->
Zunächst wird der Turm mit den schwarzen Klötzen aufgeschoben und dann mit denn I-Streben 15 und einer Verbindung auf der Rückseite gesichert.