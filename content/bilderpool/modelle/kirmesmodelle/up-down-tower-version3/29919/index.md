---
layout: "image"
title: "Turmspitze-Hauptturm"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion02.jpg"
weight: "2"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29919
- /detailseb0f.html
imported:
- "2019"
_4images_image_id: "29919"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29919 -->
Zusehen ist oben die Hochziehmechanik (Seilwinde) und in der Mitte der Hochzieher.