---
layout: "image"
title: "Station"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion07.jpg"
weight: "7"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29924
- /detailsefde.html
imported:
- "2019"
_4images_image_id: "29924"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29924 -->
Draufsicht auf die Station ohne Ein- und Ausgang.