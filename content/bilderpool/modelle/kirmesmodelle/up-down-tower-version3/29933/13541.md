---
layout: "comment"
hidden: true
title: "13541"
date: "2011-02-12T21:58:52"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Jonas,

Ein sehr schönes Modell !   Kompliment.........

Auch deiner Abkoppel-Löschung is sehr interessant !     Der Motor (50:1) dreht die Zahnräder, diese drehen die Schnecken und diese fahren die Stangen zum abkoppeln herraus.

Einige Monaten her hatte ich auch so etwas überdacht, doch ich habe eine andere Lössung für meine Freefall-Turm gemacht.
http://www.ftcommunity.de/details.php?image_id=29003#col3
Die Ober-Gondel hat eines Verriegelungsystem (ohne Motorantrieb) zum Verbindung an die Unter-Gondel mit Sessel; genau wie beim Adrenalin-Freifallturm.

Grüss,

Peter 
Poederoyen NL