---
layout: "image"
title: "Station ohne Aufbauten"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion31.jpg"
weight: "31"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29948
- /details89cb.html
imported:
- "2019"
_4images_image_id: "29948"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29948 -->
Die Station mit Bodenplatten, Schranken und Elektronik, aber ohne Geländer. Unten ist der Eingang mit den Schranken und oben der Ausgang. Links das Extension und rechts das Interface.