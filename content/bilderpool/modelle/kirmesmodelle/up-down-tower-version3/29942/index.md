---
layout: "image"
title: "Zug2"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion25.jpg"
weight: "25"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29942
- /details99e0.html
imported:
- "2019"
_4images_image_id: "29942"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29942 -->
Die Fischermans sind mit Bügeln gesichert ;-)