---
layout: "image"
title: "Detail-Seilwinde2"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion20.jpg"
weight: "20"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29937
- /detailsa96e.html
imported:
- "2019"
_4images_image_id: "29937"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29937 -->
Hier ohne Verkleidung. Rechts sind 2 Summer für den Signalton vor dem Fall eingebaut. Links-Mitte ist die Befestigung für die Seilwinde zusehen.