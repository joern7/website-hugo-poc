---
layout: "image"
title: "Mechanisches Relais"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion36.jpg"
weight: "36"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29953
- /detailse254.html
imported:
- "2019"
_4images_image_id: "29953"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29953 -->
Hier ist nochmal eine Großaufnahme des selbst gebauten Relais. Ein Taster ist jeweils zur Positionserkennung fürs Interface und einer jeweils zum Steuern.

Wenn ihr Fragen habt oder etwas bestimmtes sehen wollt, könnt ihr das gerne posten. Ich habe noch sehr, sehr viel mehr Bilder und es ist sehr warscheinlich das bestimmte, dass ihr wollt dabei;-)
Außerdem haben meine Bilder eine sehr hohe Auflösung, bei Bedarf kann ich einige Bilder in bester Qualität per E-mail verschicken (pro Bild ca. 1,5 - 2 MB Größe)
