---
layout: "image"
title: "Schiffschaukel, der Antrieb(Bild 2)"
date: "2003-08-17T20:16:34"
picture: "RIMG0042.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- /php/details/1335
- /details0585.html
imported:
- "2019"
_4images_image_id: "1335"
_4images_cat_id: "143"
_4images_user_id: "26"
_4images_image_date: "2003-08-17T20:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1335 -->
Hier könnt ihr das Herzstück der Schiffschaukel sehen, den Antrieb.
Der Antrieb selber ist gefedert und mit einem P. Motor bestückt.
Die Federn sind sehr wichtig, da durch die Fliehkräfte das  Schiff immer mehr nach unten gedrückt wird(umso schneller es ist).