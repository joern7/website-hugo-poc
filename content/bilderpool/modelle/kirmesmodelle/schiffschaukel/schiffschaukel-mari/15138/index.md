---
layout: "image"
title: "Gesamtansicht"
date: "2008-08-30T13:50:53"
picture: "Schiffschaukel_11.jpg"
weight: "1"
konstrukteure: 
- "Marius Moosmann"
fotografen:
- "Marius Moosmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15138
- /detailsd28c.html
imported:
- "2019"
_4images_image_id: "15138"
_4images_cat_id: "1386"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15138 -->
