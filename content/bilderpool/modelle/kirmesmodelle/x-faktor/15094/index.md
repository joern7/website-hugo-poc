---
layout: "image"
title: "Kleiner Ring drehend"
date: "2008-08-24T16:00:37"
picture: "xfaktor16.jpg"
weight: "16"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15094
- /details2880.html
imported:
- "2019"
_4images_image_id: "15094"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15094 -->
Der kleine Ring dreht sich etwas schneller als der Große ist ja verständlich...