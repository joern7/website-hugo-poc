---
layout: "image"
title: "Eingabepulthälfte links"
date: "2008-08-24T16:00:36"
picture: "xfaktor02.jpg"
weight: "2"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15080
- /details12ee.html
imported:
- "2019"
_4images_image_id: "15080"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15080 -->
1=Motor 1+2 AN
2=Richtungswechsel
3=Schneller
4=Langsamer
5=Motor 3 An
6=Richtungswechsel
7=Schneller
8=Langsamer                                    

Wenn kein Taster gedrückt wird stehen alle Motoren zur Sicherheit