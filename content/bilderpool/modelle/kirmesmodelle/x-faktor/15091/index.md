---
layout: "image"
title: "Ausgangsposition"
date: "2008-08-24T16:00:37"
picture: "xfaktor13.jpg"
weight: "13"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15091
- /details3f97.html
imported:
- "2019"
_4images_image_id: "15091"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15091 -->
Hier dreht sich der Ring nicht ganz so schnell...