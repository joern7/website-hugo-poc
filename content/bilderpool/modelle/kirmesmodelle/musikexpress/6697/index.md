---
layout: "image"
title: "Der Rohbau"
date: "2006-08-18T13:27:58"
picture: "ME_1.jpg"
weight: "9"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6697
- /details8350.html
imported:
- "2019"
_4images_image_id: "6697"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-08-18T13:27:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6697 -->
Hier seht ihr mein fast fertiges Modell. Es fehlen jetzt noch der Unterbau mit der Schiene wo die Chaisen (Wagen) später drauf laufen. Das Dach kommt auch noch drauf. Alles im allen ist es noch ne Menge Arbeit. Den Mittelbau hab ich auch noch umgebaut weil mir das andere alles zu wackelig war. Ich hoffe ich schaffe das noch das Modell bis zur Convention fertig zu bekommen.