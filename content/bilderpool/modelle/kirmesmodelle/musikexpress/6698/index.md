---
layout: "image"
title: "Frontpaneel"
date: "2006-08-18T13:27:58"
picture: "ME_2.jpg"
weight: "10"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6698
- /detailsc8b9.html
imported:
- "2019"
_4images_image_id: "6698"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-08-18T13:27:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6698 -->
Das ist das Frontpaneel was noch vorne angebracht werden muss. Die ganzen Teile werden noch schön verkleidet.