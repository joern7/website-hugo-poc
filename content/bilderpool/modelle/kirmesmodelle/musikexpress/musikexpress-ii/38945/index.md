---
layout: "image"
title: "Blick von unten auf die Gondel"
date: "2014-06-12T13:24:35"
picture: "musikexpressii10.jpg"
weight: "10"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38945
- /detailsd6e8.html
imported:
- "2019"
_4images_image_id: "38945"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38945 -->
