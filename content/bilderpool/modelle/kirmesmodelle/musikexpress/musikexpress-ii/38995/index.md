---
layout: "image"
title: "Noch eine Seitenansicht"
date: "2014-07-03T22:30:20"
picture: "meii4.jpg"
weight: "15"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38995
- /detailsf970.html
imported:
- "2019"
_4images_image_id: "38995"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-07-03T22:30:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38995 -->
Schwierig war es auch mit der oberen Kette nicht das erste Antriebsritzel, ein Z10, zu streifen. Die Verstrebung musste ich auch umbauen weil das Z 40 sonst nicht gepasst hätte. Ist inzwischen aber etwas umgeändert damit die obere Reihe auch vernünftigen Halt hat.