---
layout: "comment"
hidden: true
title: "19146"
date: "2014-06-12T23:20:49"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Wenn du die Drehung und Durchbiegung verrinngeren willst, solltest du diese blauwe / rote Platten als Konstruktionselement benutzen. Je höher den Rahmen, je steifer. Also die Statikteilen an Oben- und Untenseite der Platten fixieren. 

Viel Erfolg mit diesem grossen Projekt!

Marten