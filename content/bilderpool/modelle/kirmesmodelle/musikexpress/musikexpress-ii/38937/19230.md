---
layout: "comment"
hidden: true
title: "19230"
date: "2014-07-13T13:24:50"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Irgendwo musste ich ja anfangen, Stefan. Hinten sind jetzt noch 3 Böcke zu bauen und dann die Schiene daran noch befestigen. Danach gehts mit den vorderen weiter die zum Glück aber wesentlich niedriger sind. (Nein ich habe keinen Materialmangel.) ;-) Dauert nur die großen Böcke zu bauen und zu montieren zumal es auf dem Dachboden nicht gerade sonderlich kühl ist. Ein großer Ventilator hilft ungemein. ^^