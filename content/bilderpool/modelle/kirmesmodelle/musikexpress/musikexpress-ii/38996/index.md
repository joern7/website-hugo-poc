---
layout: "image"
title: "Eine versuchte Abänderung der Dachkonstruktion"
date: "2014-07-03T22:30:20"
picture: "meii5.jpg"
weight: "16"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38996
- /detailsf09a.html
imported:
- "2019"
_4images_image_id: "38996"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-07-03T22:30:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38996 -->
Damit wollte ich das Verdrehen eigentlich etwas mehr unterbinden aber genau das Gegenteil ist eingetreten. Es verdreht sich mehr als mit der alten Konstruktion.Nun baue ich sie wieder um aber eine andere Idee zu dem vorderen Bereich und den ganzen Winkelsteinen ist mir schon eingefallen. Mal sehen ob es klappt. Erstmal kommen jetzt die Böcke auf die Sohle und da dann die Fußböden und Schiene dran.