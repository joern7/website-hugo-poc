---
layout: "image"
title: "Gondel Aussenansicht"
date: "2014-06-12T13:24:35"
picture: "musikexpressii06.jpg"
weight: "6"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38941
- /details4454.html
imported:
- "2019"
_4images_image_id: "38941"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38941 -->
