---
layout: "image"
title: "Der Rohbau 2"
date: "2006-08-18T13:27:58"
picture: "ME_3.jpg"
weight: "11"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6699
- /detailsc8da.html
imported:
- "2019"
_4images_image_id: "6699"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-08-18T13:27:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6699 -->
Hier noch ein Bild vom Rohbau.