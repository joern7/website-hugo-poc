---
layout: "image"
title: "Gesamtansicht des Fahrzeugs"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes13.jpg"
weight: "13"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/30356
- /detailsb775.html
imported:
- "2019"
_4images_image_id: "30356"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30356 -->
Das Fahrzeug ist mit zwei Bodenschleifern sowie einem Stromabnehmer für das Gitter ausgestattet. Der obere Stromabnehmer ist aus Federdraht. Der Motor ist drehbar gelagert, sodass das Fahrzeug um die Kurve fahren kann.