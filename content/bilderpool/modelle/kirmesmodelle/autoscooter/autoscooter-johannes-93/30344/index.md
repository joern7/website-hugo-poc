---
layout: "image"
title: "Gesamtansicht"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes01.jpg"
weight: "1"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/30344
- /detailse368.html
imported:
- "2019"
_4images_image_id: "30344"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30344 -->
Hier ist das Video zum Autoscooter: http://www.youtube.com/watch?v=65zVweYyjqU