---
layout: "image"
title: "Fahrbahn"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes08.jpg"
weight: "8"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/30351
- /details882c.html
imported:
- "2019"
_4images_image_id: "30351"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30351 -->
Die Fahrbahn ist eine Holzplatte, die mit Alufolie bespannt ist.