---
layout: "image"
title: "Kassenhäuschen"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes12.jpg"
weight: "12"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/30355
- /details0ba2-2.html
imported:
- "2019"
_4images_image_id: "30355"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30355 -->
