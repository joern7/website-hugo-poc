---
layout: "image"
title: "Fahrzeug auf dem Fahrboden"
date: "2007-12-26T18:26:58"
picture: "autoscooter3.jpg"
weight: "3"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13169
- /detailsfe70.html
imported:
- "2019"
_4images_image_id: "13169"
_4images_cat_id: "1189"
_4images_user_id: "672"
_4images_image_date: "2007-12-26T18:26:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13169 -->
Ein Auto-Scooter ohne Auto ist ja nicht das Wahre - also gibt es auch welche ;-)