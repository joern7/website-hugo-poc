---
layout: "image"
title: "Komplettansicht"
date: "2007-12-26T18:26:57"
picture: "autoscooter1.jpg"
weight: "1"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13167
- /detailsc8cb.html
imported:
- "2019"
_4images_image_id: "13167"
_4images_cat_id: "1189"
_4images_user_id: "672"
_4images_image_date: "2007-12-26T18:26:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13167 -->
Das simple Fahrgeschäft in der Übersicht.

Als Fahrboden dient eine mit Alu-Folie beklebte Pappe, als Gitter oben ein Volierendraht (hier: verzinkter Stahldraht mit 12x12 Abstand). Die Basis bilden 4 Grundplatten 1000, auf denen der Fahrboden lose aufliegt - der ist nur an den Ecken unter die Statik-Bögen der Fahrbodenbegrenzung geklemmt, damit er nicht verrutschen kann. Der Volierendraht oben ist einfach an der Umrandung zwischen Statik-Winkelträger geklemmt und mit S-Riegeln gesichert.

Die Stabilität des Volieren-Drahtgitters (Drahtsträrke 1 mm) ist so gut, dass man auf die Verstrebungen im Dach auch hätte verzichten können. Oder das Ganze problemlos von der Grundfläche her 2 bis 4 mal so groß machen können.

Auf die Optik habe ich, wie man sieht, keinen großen Wert gelegt - Hauptsache, es funktioniert. Verzierungen, Beleuchtung usw. können bei Bedarf hinzugefügt werden.
