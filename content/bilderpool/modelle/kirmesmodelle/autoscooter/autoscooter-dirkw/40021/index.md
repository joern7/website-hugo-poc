---
layout: "image"
title: "Aufbauten des Autoscooter"
date: "2014-12-28T22:12:40"
picture: "autoscooter10.jpg"
weight: "10"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40021
- /details8e4a.html
imported:
- "2019"
_4images_image_id: "40021"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40021 -->
Hier seht ihr die Aufbauten des Autoscooters. Ich habe überwiegend mit Statikträgern gebaut. 
Die äußeren Träger habe ich mit  Bauplatten verkleidet.