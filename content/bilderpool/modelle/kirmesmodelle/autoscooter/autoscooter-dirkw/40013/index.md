---
layout: "image"
title: "4 Scooter"
date: "2014-12-28T22:12:40"
picture: "autoscooter02.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40013
- /details5477.html
imported:
- "2019"
_4images_image_id: "40013"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40013 -->
Hier die 4 Scooter beleuchtet nebeneinander.
