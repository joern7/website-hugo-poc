---
layout: "image"
title: "Scooter ohne Sitz"
date: "2014-12-28T22:12:40"
picture: "autoscooter32.jpg"
weight: "32"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40043
- /details7dd1.html
imported:
- "2019"
_4images_image_id: "40043"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40043 -->
Hier kann man den Antrieb gut erkennen.