---
layout: "image"
title: "Taster für Stromversorgung"
date: "2014-12-28T22:12:40"
picture: "autoscooter25.jpg"
weight: "25"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40036
- /detailse251.html
imported:
- "2019"
_4images_image_id: "40036"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40036 -->
Zum Ein- und Ausschalten habe ich noch einen Taster im Scooter verbaut.