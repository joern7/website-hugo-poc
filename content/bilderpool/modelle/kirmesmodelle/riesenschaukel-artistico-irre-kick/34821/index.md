---
layout: "image"
title: "Die ersten Masten"
date: "2012-04-25T12:02:58"
picture: "modellartistico2.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34821
- /detailsb334.html
imported:
- "2019"
_4images_image_id: "34821"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-04-25T12:02:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34821 -->
Die waren viel zu instabil und mein Teilevorrat an U-Trägern 150 war auch zu gering.