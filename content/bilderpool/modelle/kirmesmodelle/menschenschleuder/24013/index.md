---
layout: "image"
title: "Gondelkette"
date: "2009-05-13T17:02:00"
picture: "menschenschleuder13.jpg"
weight: "13"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24013
- /details8d9b.html
imported:
- "2019"
_4images_image_id: "24013"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:02:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24013 -->
Durch die Kette werden die Gondeln angetrieben.