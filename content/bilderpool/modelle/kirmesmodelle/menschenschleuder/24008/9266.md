---
layout: "comment"
hidden: true
title: "9266"
date: "2009-05-13T18:29:34"
uploadBy:
- "Limit"
license: "unknown"
imported:
- "2019"
---
Hallo,
Sven das ist mir schon klar. Ich wollte nur wissen, was er verwendet hat.
Hat jemand mit den Klemmen schon mal gearbeitet? Ich bin letztens über Löt-/Crimp Klemmen gestolpert und würde gerne wissen, was einfacher zu verarbeiten ist.
Gruß, 
Marius