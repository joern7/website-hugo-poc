---
layout: "image"
title: "Schleifkontakt"
date: "2009-05-13T17:01:38"
picture: "menschenschleuder08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24008
- /details7df9.html
imported:
- "2019"
_4images_image_id: "24008"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:01:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24008 -->
