---
layout: "image"
title: "Einsteigesteg"
date: "2009-05-13T17:01:38"
picture: "menschenschleuder05.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24005
- /details82df.html
imported:
- "2019"
_4images_image_id: "24005"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:01:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24005 -->
Hier steigen die Passagiere in die Gondeln ein.