---
layout: "image"
title: "Drehkranz ganz nah."
date: "2015-02-08T12:54:27"
picture: "IMG_0063.jpg"
weight: "38"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40505
- /details875f.html
imported:
- "2019"
_4images_image_id: "40505"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40505 -->
die gelben und blauen Kunsttoff-Doppelachsen sorgen dafür, dass die Flex-Schienen nicht nach aussen wandern. die blauen achsen markieren die 90-Grad Achsen zur Orientierung