---
layout: "image"
title: "'Sleeveblock' auf der Spindel, von Aussen betrachtet"
date: "2014-11-09T17:21:24"
picture: "IMG_0020.jpg"
weight: "9"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39773
- /details5e81.html
imported:
- "2019"
_4images_image_id: "39773"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39773 -->
