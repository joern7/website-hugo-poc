---
layout: "image"
title: "Antrieb an der Brücke NEU"
date: "2015-02-08T12:54:27"
picture: "IMG_0061.jpg"
weight: "30"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40497
- /details0d9b-3.html
imported:
- "2019"
_4images_image_id: "40497"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40497 -->
Ich habe die Brücke nun aus Gewichts - und Stabilitätsgründen nur noch mit 2 Alu-Profilen gebaut, diese habe ich auf Länge passend bestellt - DANKE, FFM ;-) .