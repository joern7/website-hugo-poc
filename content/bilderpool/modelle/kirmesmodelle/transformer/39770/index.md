---
layout: "image"
title: "Schleifring Mitte"
date: "2014-11-09T17:21:24"
picture: "IMG_0010.jpg"
weight: "6"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39770
- /details7bf3.html
imported:
- "2019"
_4images_image_id: "39770"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39770 -->
