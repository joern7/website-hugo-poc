---
layout: "image"
title: "Gegenlager Schleifring"
date: "2014-11-09T17:21:24"
picture: "IMG_0013.jpg"
weight: "22"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39786
- /details5832.html
imported:
- "2019"
_4images_image_id: "39786"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39786 -->
Der Schleifring ist die Mittelbohrung des Zahnkranzes gesteckt und am Sleeve-Block fest montiert. es rotieren also die Schleifer um den feststehenden Schleifring