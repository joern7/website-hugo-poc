---
layout: "image"
title: "Brücke komplett, Position Überkopf"
date: "2014-11-09T17:21:24"
picture: "IMG_0016.jpg"
weight: "10"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39774
- /details4cf8.html
imported:
- "2019"
_4images_image_id: "39774"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39774 -->
hier sieht man, wie die Brücke auf "Überkopfposition" rotiert wurde