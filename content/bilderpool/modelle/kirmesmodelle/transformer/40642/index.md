---
layout: "image"
title: "versenkter Einbau"
date: "2015-03-14T12:36:48"
picture: "IMG_0014.jpg"
weight: "44"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40642
- /detailsd8e5.html
imported:
- "2019"
_4images_image_id: "40642"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40642 -->
ich habe 2 alter Steckerinnenteile bündig abgesägt, um die Einbautiefe zu minimieren . . so kann man grundsätzlich seitliche Steckerführungen realisieren, um Platz zu sparen ..