---
layout: "comment"
hidden: true
title: "20358"
date: "2015-03-14T17:43:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist auch eine schicke Idee für einen einpoligen Schleifring ;-)

Und wie schön - da kennt jemand den Begriff "Stromdichte"! Du hast aber nicht zufällig in Karlsruhe Physik studiert?

Gruß,
Stefan