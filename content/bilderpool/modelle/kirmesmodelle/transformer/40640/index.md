---
layout: "image"
title: "Schleiferabgriff Mittelstange Drehscheibe"
date: "2015-03-14T12:36:48"
picture: "IMG_0001_2.jpg"
weight: "42"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40640
- /details1c44-2.html
imported:
- "2019"
_4images_image_id: "40640"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40640 -->
eingebaut in Baustein 30 mit Loch (eckige Ausführung des Loches)