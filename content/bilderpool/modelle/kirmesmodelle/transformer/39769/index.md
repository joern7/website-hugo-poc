---
layout: "image"
title: "zentraler Schleifring"
date: "2014-11-09T17:21:24"
picture: "IMG_0009.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39769
- /details3f59-2.html
imported:
- "2019"
_4images_image_id: "39769"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39769 -->
Stromversorgung für die Lampen in der Mitte . als Schleifkontakte verwende ich die Federpins vom Stufenschalter