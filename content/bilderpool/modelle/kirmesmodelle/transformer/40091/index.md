---
layout: "image"
title: "Innenkreis"
date: "2014-12-31T17:27:48"
picture: "IMG_0069.jpg"
weight: "26"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40091
- /detailsed6c-2.html
imported:
- "2019"
_4images_image_id: "40091"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-12-31T17:27:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40091 -->
auf diesem bereits getetsteten Drehgestell wird der Transformer später komplett rotieren. ein Lasttest wurde erfolgreich bestanden. Der Kettenaufbau für den Antrieb am Statik-Kreis wurde in Anlehnung an den FTpedia-Beitrag 4/2014 / schreiender Wecker S.25ff von Andreas Gail weiter entwickelt.  Im Innenkreis muss später die Schleifring-Konstruktion zur Stromversorgung noch untergebracht werden.