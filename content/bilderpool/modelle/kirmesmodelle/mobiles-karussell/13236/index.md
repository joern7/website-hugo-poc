---
layout: "image"
title: "Zerlegtes Karussell"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel14.jpg"
weight: "14"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13236
- /details6d68.html
imported:
- "2019"
_4images_image_id: "13236"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13236 -->
