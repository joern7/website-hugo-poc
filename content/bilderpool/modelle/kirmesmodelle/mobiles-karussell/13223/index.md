---
layout: "image"
title: "Gesamtansicht"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel01.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13223
- /details9e9c.html
imported:
- "2019"
_4images_image_id: "13223"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13223 -->
