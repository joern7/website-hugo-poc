---
layout: "image"
title: "Drehkranz am Turm"
date: "2005-04-16T20:48:27"
picture: "Modell_Ikarus23.jpg"
weight: "7"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/3980
- /details1d5b-2.html
imported:
- "2019"
_4images_image_id: "3980"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T20:48:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3980 -->
Hier halte ich den Drehkranz mal spasseshalber an den Turm an um zu sehen wieviel Platz noch zwischen Turm und Drehkranz ist. Es passt soweit ganz gut.