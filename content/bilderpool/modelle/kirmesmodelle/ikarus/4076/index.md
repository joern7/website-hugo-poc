---
layout: "image"
title: "Modell Ikarus"
date: "2005-04-25T10:58:12"
picture: "Modell_Ikarus_33.jpg"
weight: "15"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4076
- /details98ae-2.html
imported:
- "2019"
_4images_image_id: "4076"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-25T10:58:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4076 -->
Hier sieht man deutlich die oberen Stützräder.