---
layout: "image"
title: "Modell Ikarus"
date: "2005-04-25T10:58:11"
picture: "Modell_Ikarus_30.jpg"
weight: "12"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4073
- /details5953-3.html
imported:
- "2019"
_4images_image_id: "4073"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-25T10:58:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4073 -->
Nochmal ein Bild von oben auf den Drehkranz und den Masten. Es ist ganz schön eng.