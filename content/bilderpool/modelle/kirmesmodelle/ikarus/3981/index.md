---
layout: "image"
title: "Drehkranz von innen"
date: "2005-04-16T20:48:28"
picture: "Modell_Ikarus25.jpg"
weight: "8"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/3981
- /detailse819.html
imported:
- "2019"
_4images_image_id: "3981"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T20:48:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3981 -->
Noch eine Innenansicht vom Drehkranz. Die Aufnahmen sind inzwischen noch etwas verstärkt worden damit sich das Ganze nicht verzieht.