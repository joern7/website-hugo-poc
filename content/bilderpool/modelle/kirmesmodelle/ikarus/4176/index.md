---
layout: "image"
title: "Auch hier alles im Lot"
date: "2005-05-21T18:42:07"
picture: "Modell_Ikarus_47.jpg"
weight: "27"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4176
- /details4421.html
imported:
- "2019"
_4images_image_id: "4176"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-21T18:42:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4176 -->
