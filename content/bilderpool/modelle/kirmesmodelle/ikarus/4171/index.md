---
layout: "image"
title: "Drehkranz am Turm"
date: "2005-05-20T20:27:57"
picture: "Modell_Ikarus_43.jpg"
weight: "22"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4171
- /detailse1e1.html
imported:
- "2019"
_4images_image_id: "4171"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:27:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4171 -->
Der Drehkranz am Turm.