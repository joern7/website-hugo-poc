---
layout: "image"
title: "Der Drehkranz von oben"
date: "2005-04-24T00:01:47"
picture: "Modell_Ikarus_27.jpg"
weight: "10"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4071
- /details7827.html
imported:
- "2019"
_4images_image_id: "4071"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-24T00:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4071 -->
Hier nun noch eine Draufsicht. In die roten Bausteine 15 mit Bohrung kommen die Radachsen mit den Rädern für die obere Abstützung rein.