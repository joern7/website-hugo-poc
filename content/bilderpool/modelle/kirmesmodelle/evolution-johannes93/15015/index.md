---
layout: "image"
title: "Drehscheibe am Kranhaken"
date: "2008-08-05T13:42:50"
picture: "evolution21.jpg"
weight: "21"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15015
- /details1c6a.html
imported:
- "2019"
_4images_image_id: "15015"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15015 -->
