---
layout: "image"
title: "Transportwagen mit Zugmaschine"
date: "2008-08-05T13:42:50"
picture: "evolution04.jpg"
weight: "4"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14998
- /details0965.html
imported:
- "2019"
_4images_image_id: "14998"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14998 -->
