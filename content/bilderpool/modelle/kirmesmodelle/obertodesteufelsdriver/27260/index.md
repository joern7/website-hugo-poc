---
layout: "image"
title: "Der Wagen - Gesamtansicht"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27260
- /details4a7f.html
imported:
- "2019"
_4images_image_id: "27260"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27260 -->
Hier sitzt, oder vielmehr liegt, der Obertodesteufelsdriver. Es besteht natürlich Helmpflicht. ;-) Die Vorstuferäder führen zu einem leichtgängigen Lauf und sichern den Wagen auch seitlich.