---
layout: "image"
title: "Auffangpuffer zerlegt"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27259
- /details535f.html
imported:
- "2019"
_4images_image_id: "27259"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27259 -->
Der Kraftmesser ist mit insgesamt drei Federnocken befestigt. Von dem am Ende des U-Trägers in der Mitte des Kraftmessers sieht man gerade noch den Zapfen. Der bewegliche Puffer selbst wird über die Metallachse 30 in den Kraftmesser eingehängt.