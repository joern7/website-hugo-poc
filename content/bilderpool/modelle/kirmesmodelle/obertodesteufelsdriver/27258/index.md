---
layout: "image"
title: "Auffangpuffer"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27258
- /detailsa625.html
imported:
- "2019"
_4images_image_id: "27258"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27258 -->
Ein ehrwürdiger alter fischertechnik-Kraftmesser (bestückt mit der schwächeren, weicheren Feder) fängt den Wagen hinreichend sanft auf. Die Statikbausteine 15 rechts fangen den Wagen, und die leichte Konstruktion zieht die Feder des Kraftmessers.