---
layout: "image"
title: "Tischkantenbefestigung"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27253
- /details6b27.html
imported:
- "2019"
_4images_image_id: "27253"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27253 -->
Wenn man die Bahn einfach auf einem Tisch oder wie hier auf dem Fußboden aufstellt, rutscht sie bei Benutzung über die Zeit etwas in Fahrtrichtung. Die dafür benötigte Energie kommt natürlich aus dem Wagen und fehlt dem also. Wenn man wie hier gezeigt die Bahn sich an einer Tischkante festhalten lässt, läuft die Sache noch besser. Die nach unten ragenden Bausteine hängen dann über der Tischkante herunter und verhindern, dass die Bahn verrutscht. Über die gesamte Länge sind Zugstreben angebracht, damit auch die anderen Teile der Bahn nicht rutschen können.