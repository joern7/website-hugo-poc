---
layout: "image"
title: "Der Wagen - Unterboden"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27262
- /details33d0.html
imported:
- "2019"
_4images_image_id: "27262"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27262 -->
Eine Ansicht von unten. Der Mittelteil besteht aus vier Bausteinen 7,5. Wo es nur geht, stecken Verbinder oder, wie hier sichtbar, eine Platte 30 * 30 zur Verstärkung.