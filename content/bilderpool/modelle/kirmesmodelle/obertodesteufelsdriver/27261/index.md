---
layout: "image"
title: "Der Wagen - Seitenansicht"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27261
- /details8971-2.html
imported:
- "2019"
_4images_image_id: "27261"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27261 -->
Der Gute liegt, wie man sieht, ziemlich nahe über den Rädern.