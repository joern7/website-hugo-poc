---
layout: "image"
title: "Alternativer, aber zu breiter Wagen - Front"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27266
- /detailsc1ee.html
imported:
- "2019"
_4images_image_id: "27266"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27266 -->
Wir sehen die Füße des Fahrers und Teile seiner Sitzbefestigung.