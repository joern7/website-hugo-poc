---
layout: "image"
title: "Sprungschanze"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27257
- /details5ca6-2.html
imported:
- "2019"
_4images_image_id: "27257"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27257 -->
Der Wagen kommt von rechts und springt über den freien Teil. Die Statikträger in der Mitte halten nur den Abstand und die Richtung ein.