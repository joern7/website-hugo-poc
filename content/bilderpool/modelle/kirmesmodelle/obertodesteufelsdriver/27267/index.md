---
layout: "image"
title: "Alternativer, aber zu breiter Wagen - Heckansicht"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27267
- /detailsaa0a-2.html
imported:
- "2019"
_4images_image_id: "27267"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27267 -->
Dasselbe von hinten.