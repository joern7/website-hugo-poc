---
layout: "image"
title: "Alternativer, aber zu breiter Wagen - Gesamtansicht"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27265
- /details9a7e.html
imported:
- "2019"
_4images_image_id: "27265"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27265 -->
Diese Wagenkonstruktion vermeidet die Nachteile des tatsächlich eingesetzten Wagens. Leider ist sie zu breit, um durch den gezeigten Looping zu kommen. Man müsste wohl auch noch etwas verstärken, um die Laufräder am Verdrehen der Zapfen zu hindern.