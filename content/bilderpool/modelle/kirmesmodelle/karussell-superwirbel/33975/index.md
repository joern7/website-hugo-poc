---
layout: "image"
title: "Karussell SuperWirbel"
date: "2012-01-17T19:05:36"
picture: "karussellsuperwirbel5.jpg"
weight: "5"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33975
- /details4098.html
imported:
- "2019"
_4images_image_id: "33975"
_4images_cat_id: "2514"
_4images_user_id: "1361"
_4images_image_date: "2012-01-17T19:05:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33975 -->
Seitenansicht