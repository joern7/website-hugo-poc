---
layout: "overview"
title: "Karussell, SuperWirbel"
date: 2020-02-22T08:00:21+01:00
legacy_id:
- /php/categories/2514
- /categories25c4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2514 --> 
Das Karussell ist computergesteuert und läuft vollautomatisch. Während sich das Karussell dreht, drehen sich auch die vier sechssitzigen "Wirbel" um ihre eigene Achse. Durch einen Exzenter werden die vier "Wirbel" bei jeder Umdrehung des Karussells zusätzlich in die Höhe gehoben. Nach einigen Runden verlangsamt sich die Karusselldrehung bis ein "Wirbel" an der Ein- und Ausstiegsplattform zum Stehen kommt. Jetzt kann an diesem Wirbel der Personenwechsel stattfinden. Damit es den übrigen Mitfahrern in dieser Phase nicht langweilig wird, drehen sich die übrigen drei "Wirbel" weiter. Nach einem Signalton setzt sich das Karussell langsam in Bewegung, bis der nächste Wirbel die Ein- und Ausstiegsplattform erreicht. Auch er bleibt nun für kurze Zeit zum Personenwechsel stehen. Das Spiel setzt sich so lange fort, bis alle vier "Wirbel" einmal zum Stehen gekommen sind. Ein Doppelsignalton zeigt an, dass nun eine Runde beginnt.
Ein Video, das das Modell in Betrieb zeigt, kann in Youtube unter "fischertechnik Karussell.avi" angesehen werden.