---
layout: "image"
title: "Aufhängung"
date: "2014-08-07T10:27:25"
picture: "DSC00059.jpg"
weight: "10"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
keywords: ["bauFischertechnik", "Looping", "Karussell", "Fischertechnik", "Schleifkontakt", "Robo", "TX", "Controller", "RoboPro"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39158
- /details6a27.html
imported:
- "2019"
_4images_image_id: "39158"
_4images_cat_id: "2927"
_4images_user_id: "2086"
_4images_image_date: "2014-08-07T10:27:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39158 -->
Hier siht man die Aufhängung des Dreharms.