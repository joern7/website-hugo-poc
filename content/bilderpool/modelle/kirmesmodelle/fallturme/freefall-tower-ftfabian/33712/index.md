---
layout: "image"
title: "Die Sitzplattform / the carriage"
date: "2011-12-18T21:03:16"
picture: "ftfabian11.jpg"
weight: "11"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33712
- /details02cf.html
imported:
- "2019"
_4images_image_id: "33712"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33712 -->
Der Wagen auf halber Höhe. Eigentlich sind die roten Rollen dafür besser geeignet, da ich aber nicht genügend hatte, musste ich auch schwarze benutzen. Dahinter ist der Fototransistor, der zum auslösen der Bremse gebraucht wird. In der Mitte des Turms ist die Zusammenführung der Seile und das Gegengewicht(ist mehr Deko).

The carriage on half high of the tower. Normally the red curlings are better but I do not have enough curlings. So I had to use black ones, too. Behind the carriage there is the phototransistor which is used to activate the brake. In the middle of the Tower the fan-in of the ropes and the balance weight (it is more decor) can be seen.

