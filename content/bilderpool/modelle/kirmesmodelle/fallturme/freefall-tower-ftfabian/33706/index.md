---
layout: "image"
title: "Kupplung ausgekuppelt / the coupler"
date: "2011-12-18T21:02:57"
picture: "ftfabian05.jpg"
weight: "5"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33706
- /details9205.html
imported:
- "2019"
_4images_image_id: "33706"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33706 -->
Die Kupplung im ausgekuppeltem Zustand, wobei die roten 5er Bausteine(1/3 Bausteine) zu sehen sind, die dann ineinander greifen.

You can see the red 5er bricks (1/3 bricks), which gear into each other.