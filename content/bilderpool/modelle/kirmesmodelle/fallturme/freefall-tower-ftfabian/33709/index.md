---
layout: "image"
title: "Bremse / the brake"
date: "2011-12-18T21:02:57"
picture: "ftfabian08.jpg"
weight: "8"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33709
- /detailsf93c.html
imported:
- "2019"
_4images_image_id: "33709"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33709 -->
Die Bremse im "Bremszustand". Der Powermotor (hier nicht zu sehen, wäre am rechten Bildrand) wickelt das Seil auf, dadurch werden die beiden Hebel oben zusammengedrückt und drücken gegen das Rad. Auf den Hebeln habe ich Gummipuffer aufgeklebt, wodurch die Bremswirkung enorm verbessert wurde. 

The brake in braking position. The power motor (which would be on the right side) winds the rope up. So the arms are tightened together and block the wheel. On the two arms I have glued on bump rubbers, This makes the brake action much better.
