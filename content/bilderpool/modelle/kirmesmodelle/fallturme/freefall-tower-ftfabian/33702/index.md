---
layout: "image"
title: "Gesamtansicht / general view"
date: "2011-12-18T21:02:57"
picture: "ftfabian01.jpg"
weight: "1"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33702
- /detailsc64c.html
imported:
- "2019"
_4images_image_id: "33702"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33702 -->
Die Gesamtansicht meines etwas über 2 Meter hohen Freefall-Towers.
Videos: Eine Fahrt: http://youtu.be/UhS1wm28_EE, Die Kupplung: http://youtu.be/A6crGXXpvSY 

The general view of my freefall-tower. It is a little bit more than 2 meters tall.
Videos can be found here: The whole tower: http://youtu.be/UhS1wm28_EE, The coupler: http://youtu.be/A6crGXXpvSY