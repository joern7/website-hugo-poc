---
layout: "image"
title: "Die Sitzplattform / the carriage"
date: "2011-12-18T21:03:16"
picture: "ftfabian12.jpg"
weight: "12"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33713
- /detailsd7fa-2.html
imported:
- "2019"
_4images_image_id: "33713"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33713 -->
Die Plattform unten angekommen und mit geöffneten Bügeln.

The carriage at the bottom of the tower with opened bails.