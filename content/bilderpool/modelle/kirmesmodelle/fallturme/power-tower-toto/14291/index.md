---
layout: "image"
title: "Seilwinde u. Gondel"
date: "2008-04-19T08:42:56"
picture: "powertower02.jpg"
weight: "2"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/14291
- /details142e.html
imported:
- "2019"
_4images_image_id: "14291"
_4images_cat_id: "1323"
_4images_user_id: "762"
_4images_image_date: "2008-04-19T08:42:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14291 -->
Von Oben