---
layout: "image"
title: "Freifallturm mit wirbelstrombremsen"
date: "2010-10-09T13:47:32"
picture: "Freifallturmwirbelstrombremsen_005.jpg"
weight: "20"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28948
- /detailsad27-2.html
imported:
- "2019"
_4images_image_id: "28948"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-09T13:47:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28948 -->
Freifallturm mit wirbelstrombremsen