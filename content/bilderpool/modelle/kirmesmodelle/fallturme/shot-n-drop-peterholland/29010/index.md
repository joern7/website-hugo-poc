---
layout: "image"
title: "Freifallturm mit Wirbelstrombremsen:"
date: "2010-10-16T13:39:03"
picture: "Freifallturmwirbelstrombremsen_028.jpg"
weight: "48"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29010
- /details27eb-2.html
imported:
- "2019"
_4images_image_id: "29010"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-16T13:39:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29010 -->
Die Ober-Gondel hat eines Verriegelungsystem zum Verbindung an die Unter-Gondel mit Sessel;  genau wie beim Adrenalin-Freifallturm.  

Ein Video gibt es:

https://www.youtube.com/watch?v=_j52wnH5f6A

An 2 Seiten 10 st Magneten :
http://www.supermagnete.de/Q-15-15-08-N 
Q-15-15-08-N 
15 x 15 x 8 mm 
Gewicht 14 g 
vernickelt (Ni-Cu-Ni) 
Magnetisierung: N42 
Haftkraft: ca. 7,6 kg/st 

und ein Alu-Profil U20x20x2mm  Länge: 0,6m
Die Gondel Bremmst sehr gut.