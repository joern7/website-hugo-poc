---
layout: "image"
title: "Freefall Mechanik 1"
date: "2010-04-07T12:40:31"
picture: "freefall5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26879
- /details1d7f.html
imported:
- "2019"
_4images_image_id: "26879"
_4images_cat_id: "1925"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26879 -->
Nach dem Hochziehen wird das Zahnrad &quot;losgelassen&quot; und der Schlitten fährt nach unten. Gebremst wird durch den Pneumatikzylinder.