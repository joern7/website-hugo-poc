---
layout: "image"
title: "Freefall Plattform"
date: "2010-04-07T12:40:31"
picture: "freefall1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26875
- /detailsb72e.html
imported:
- "2019"
_4images_image_id: "26875"
_4images_cat_id: "1925"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26875 -->
..mit Kassenhäuschen, wo der Start Schalter sich befindet