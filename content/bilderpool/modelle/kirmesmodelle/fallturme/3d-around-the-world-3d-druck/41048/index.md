---
layout: "image"
title: "3D-Around-the-World mit einer 3D-Druck Innenzahnkranz   -oben"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld05.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41048
- /detailsdd89.html
imported:
- "2019"
_4images_image_id: "41048"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41048 -->
Link zum Video 3D-Around-the-World : 

https://www.youtube.com/watch?v=5YlsH9CfI5E