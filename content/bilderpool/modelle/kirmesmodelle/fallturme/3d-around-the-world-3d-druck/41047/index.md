---
layout: "image"
title: "3D-Around-the-World mit einer 3D-Druck Innenzahnkranz   -oben"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41047
- /detailsaeac.html
imported:
- "2019"
_4images_image_id: "41047"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41047 -->
