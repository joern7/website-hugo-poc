---
layout: "image"
title: "3D-Around-the-World mit einer 3D-Druck Innenzahnkranz"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41044
- /detailse865.html
imported:
- "2019"
_4images_image_id: "41044"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41044 -->
Johannes Visser hat im ft:pedia-2015-1 ein 3D-Druck  Innenzahnkranz  Entwurfen.
Ich habe bei  Trinckle 3D GmbH  12st in grau Polyamid bestellt.   
6 Stuck für dieser  "3D-Around-the-World" + 6 Stuck für andere Modellen.

Link zum Video 3D-Around-the-World : 

https://www.youtube.com/watch?v=5YlsH9CfI5E

OpenBeam is compatible with FischerTechnik. Look here for examples:  
http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Categories/OpenBeam_Projects/OpenBeam_and_FischerTechnik