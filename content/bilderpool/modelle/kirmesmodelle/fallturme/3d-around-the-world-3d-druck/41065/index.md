---
layout: "image"
title: "3D-Around-the-World  mit Geschwindigkeit"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld22.jpg"
weight: "22"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41065
- /detailsde01.html
imported:
- "2019"
_4images_image_id: "41065"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41065 -->
Link zum Video 3D-Around-the-World : 

https://www.youtube.com/watch?v=5YlsH9CfI5E