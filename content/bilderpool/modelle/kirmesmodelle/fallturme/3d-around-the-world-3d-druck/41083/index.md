---
layout: "image"
title: "Einbau der 6st Polyamid 3D-Druck  Innenzahnkranz + Federschleifer oderSchleifkontakten"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld40.jpg"
weight: "40"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41083
- /details694b.html
imported:
- "2019"
_4images_image_id: "41083"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41083 -->
Sehr wichtig beim Aluminium-Profilen ist die Entfernung der Anodisierung an die Ecken zum Electrischen Kontakten !
Anodiseerlagen zijn elektrisch isolerend. Middels schuurpapier is deze anodiseer-laag  effectief te verwijderen met blijvende goede geleiding voor de sleepcontacten.

OpenBeam is compatible with FischerTechnik. Look here for examples: 
http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Categories/OpenBeam_Projects/OpenBeam_and_FischerTechnik