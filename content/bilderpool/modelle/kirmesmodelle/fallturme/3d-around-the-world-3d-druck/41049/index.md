---
layout: "image"
title: "3D-Around-the-World   -Detail Kabels"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld06.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41049
- /details03b7.html
imported:
- "2019"
_4images_image_id: "41049"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41049 -->
