---
layout: "image"
title: "Rechts  3D-Around-the-World + links meiner Freifallturm mit Wirbelstrombremsen"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld41.jpg"
weight: "41"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41084
- /detailsd522.html
imported:
- "2019"
_4images_image_id: "41084"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41084 -->
Links meiner Freifallturm mit Wirbelstrombremsen : 
http://www.ftcommunity.de/details.php?image_id=28964#col3
Die Ober-Gondel hat eines Verriegelungsystem zum Verbindung an die Unter-Gondel mit Sessel; genau wie beim Adrenalin-Freifallturm.
Link zum Video's: 
http://www.youtube.com/watch?v=Fz71mrg-mSo    und  
http://www.youtube.com/watch?v=_j52wnH5f6A
