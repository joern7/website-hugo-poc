---
layout: "image"
title: "6st   3D-Druck  Innenzahnkranz  in grau Polyamid zum '3D-Around-the-World'"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld02.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41045
- /detailsc046.html
imported:
- "2019"
_4images_image_id: "41045"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41045 -->
Details weiter....