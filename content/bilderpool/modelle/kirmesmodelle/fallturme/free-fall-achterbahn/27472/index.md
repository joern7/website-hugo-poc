---
layout: "image"
title: "69 Wagen"
date: "2010-06-13T11:39:50"
picture: "freefallachterbahn02_2.jpg"
weight: "60"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27472
- /details4e52.html
imported:
- "2019"
_4images_image_id: "27472"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27472 -->
Der Wagen alleine.
Er bietet Platz für 4 Ft-Menschen, allerdings können nicht gleichzeitig 4 Menschen mitfahren. 
Umso mehr Menschen mitfahren, umso höher ist das Gewicht, was zur Folge hat, dass der ganze Wagen sehr schnell wird und in der Kurve hohe Kräfte auftreten. Bei vier Menschen wird das Material stark belastet und es fliegt mindestens einer trotz Sicherheitsbügel aus dem Wagen.