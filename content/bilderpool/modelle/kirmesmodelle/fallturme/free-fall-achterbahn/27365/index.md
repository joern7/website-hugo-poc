---
layout: "image"
title: "02 Gesamtansicht"
date: "2010-06-04T10:54:32"
picture: "freefallachterbahn2.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27365
- /detailsf380-2.html
imported:
- "2019"
_4images_image_id: "27365"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-04T10:54:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27365 -->
Nochmal das gesamte Modell aus einer anderen Perspektive. 
Der Wagen steht übrigens unten in der Station, kann man leider schlecht erkennen.

Ein paar Daten:

- Höhe: 1,65m
- Interface + Erweiterung, E-Tec Modul
- 5 Motoren
. 2 Lichtschranken / 1 Reed-Kontakt
- 10 Taster
- 12 Lampen
- gebaut auf 5 Grundplatten
- 2,4 Meter Schiene
- knapp 2300 Bauteile