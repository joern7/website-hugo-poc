---
layout: "image"
title: "27 Oben"
date: "2010-06-05T13:59:46"
picture: "freefallachterbahn24.jpg"
weight: "28"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27391
- /detailsecb7.html
imported:
- "2019"
_4images_image_id: "27391"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:46"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27391 -->
Das dicke Kabel macht hier einen Bogen.
Hinten ist die Schiene und der Endtaster zu sehen.