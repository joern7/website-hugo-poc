---
layout: "image"
title: "47 Interface"
date: "2010-06-07T21:41:45"
picture: "freefalltower21.jpg"
weight: "49"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27426
- /details4660.html
imported:
- "2019"
_4images_image_id: "27426"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27426 -->
aus einer anderen Perspektive.
Hier vorne sieht man das E-Tec Modul. Das lässt die Lampen oben blinken. 
Es ist zwar noch ein Lampenausgang frei, den wollte ich aber für weitere Scheinwerfer verwenden, was ich dann aber doch nicht gemacht habe.