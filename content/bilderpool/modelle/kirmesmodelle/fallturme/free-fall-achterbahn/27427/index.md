---
layout: "image"
title: "48 Kabel"
date: "2010-06-07T21:41:45"
picture: "freefalltower22.jpg"
weight: "50"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27427
- /details00d8.html
imported:
- "2019"
_4images_image_id: "27427"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27427 -->
Ich habe versucht, die Kabel nicht alle einach so hinzulegen, denn Unordnung in Ft-Modellen mag ich gar nicht ;-)
Der Fototransistor ist für die Lichtschranke. Normalerweise ist er am Rahmen des Wartebereiches befestigt, den musste ich ja aber abnehmen :-)