---
layout: "image"
title: "76 Wagen"
date: "2010-06-13T11:39:51"
picture: "freefallachterbahn07_2.jpg"
weight: "65"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27477
- /details233b-2.html
imported:
- "2019"
_4images_image_id: "27477"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27477 -->
nochmal der hintere Teil, jetzt aber von oben