---
layout: "image"
title: "68 Haken"
date: "2010-06-13T11:39:50"
picture: "freefallachterbahn01_2.jpg"
weight: "59"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: ["modding"]
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27471
- /details147d-2.html
imported:
- "2019"
_4images_image_id: "27471"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27471 -->
Links ist der bearbeitete Haken, wie ich ihn im Modell verwendet habe. Rechts ist zum Vergleich ein unbearbeiterer.