---
layout: "image"
title: "10 Schalter"
date: "2010-06-05T13:59:44"
picture: "freefallachterbahn06.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27373
- /details399e.html
imported:
- "2019"
_4images_image_id: "27373"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27373 -->
Die beiden Taster sind zum Steuern da. Zum Anfang kann man damit den Wagen in die richtige Position fahren, danach ist er zum Starten da (der rechte). Mit dem linken Taster kann man das Licht einschalten.