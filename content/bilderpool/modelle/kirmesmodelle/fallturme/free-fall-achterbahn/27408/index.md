---
layout: "image"
title: "30 kleiner Turm"
date: "2010-06-07T21:41:43"
picture: "freefalltower03.jpg"
weight: "31"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27408
- /details4c42.html
imported:
- "2019"
_4images_image_id: "27408"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27408 -->
Gesamtansicht des kleinen Turm. Hier fährt der Wagen wieder nach oben, um ihm den Schwung zu nehmen.