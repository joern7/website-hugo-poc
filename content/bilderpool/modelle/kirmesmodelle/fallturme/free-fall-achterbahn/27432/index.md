---
layout: "image"
title: "53 Verdeck"
date: "2010-06-07T21:41:46"
picture: "freefalltower27.jpg"
weight: "55"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27432
- /details2dd4.html
imported:
- "2019"
_4images_image_id: "27432"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:46"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27432 -->
Das Verdeck ist logsicherweise zum Verdecken  der Kabel da ;-)
Er ist normalerweise über dem Verteilerkasten.