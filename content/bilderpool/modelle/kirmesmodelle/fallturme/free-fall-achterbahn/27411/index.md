---
layout: "image"
title: "32 kleiner Turm"
date: "2010-06-07T21:41:44"
picture: "freefalltower06.jpg"
weight: "34"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27411
- /details28a3.html
imported:
- "2019"
_4images_image_id: "27411"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27411 -->
gut zu sehen ist der Befestigung des langen Stabes, der nach oben zum großen Turm führt und zur Stabilität da ist.