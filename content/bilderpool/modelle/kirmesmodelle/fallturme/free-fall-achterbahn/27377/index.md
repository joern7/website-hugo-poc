---
layout: "image"
title: "13 Förderband"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn10.jpg"
weight: "14"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27377
- /detailsf727-2.html
imported:
- "2019"
_4images_image_id: "27377"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27377 -->
Der Moter des Förderbandes und sein Impulszähler ist zu sehen. Wenn man genau hinsieht, kann man in rechts von Zahnrad auch den Haken erkennen, der den Wagen fasst und bewegt. Auf dem Förderband sind zwei davon befestigt. 
Die große Lichtschranke kann man auch gut sehen. Da der Wagen von oben hier so schnell vorbeirast, erkennt die Lichschranke ihn meistens nicht. Das hat das Programmieren natürlich sehr erschwert.