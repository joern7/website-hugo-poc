---
layout: "image"
title: "34 Bühne"
date: "2010-06-07T21:41:44"
picture: "freefalltower08.jpg"
weight: "36"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27413
- /detailsaf31.html
imported:
- "2019"
_4images_image_id: "27413"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27413 -->
Hier sieht man jetzt den Wagen auf der Schiene und die Bühne hintendran. 
Und ja, in Wirklichkeit hängt sie auch so schief ;-)