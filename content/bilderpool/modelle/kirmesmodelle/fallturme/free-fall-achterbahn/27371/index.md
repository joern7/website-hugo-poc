---
layout: "image"
title: "08 Interface"
date: "2010-06-05T13:59:44"
picture: "freefallachterbahn04.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27371
- /details2315.html
imported:
- "2019"
_4images_image_id: "27371"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27371 -->
Nochmal das Interface. Vorne dran ist der Anschluss für den Strom.