---
layout: "image"
title: "15 Lichtschranke"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn12.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27379
- /detailsea73.html
imported:
- "2019"
_4images_image_id: "27379"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27379 -->
Die zweite Lichtschranke. Da meine zweite Linsenlampe kaputt gegangen ist, konnte ich den Abstand der anderen Lichtschranke nicht mehr überbrücken und musste es so bauen.