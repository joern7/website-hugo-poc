---
layout: "image"
title: "07 Interface"
date: "2010-06-05T13:59:44"
picture: "freefallachterbahn03.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27370
- /detailsd9e2.html
imported:
- "2019"
_4images_image_id: "27370"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27370 -->
Unter dem Wartebereich befindet sich die ganze Elektronik, zu sehen ist das Interface und ein E-Tec Modul, das die Lampen zum Blinken brinkt.