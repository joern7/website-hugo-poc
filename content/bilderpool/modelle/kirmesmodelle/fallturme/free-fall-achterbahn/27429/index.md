---
layout: "image"
title: "50 Verteilerkasten"
date: "2010-06-07T21:41:45"
picture: "freefalltower24.jpg"
weight: "52"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27429
- /detailsc3e9.html
imported:
- "2019"
_4images_image_id: "27429"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27429 -->
Wie schon mal erwähnt sieht man die den Verteilerkasten. Auf der vorderen Seite sind die zwei Lichtschranken angeschlossen, hinten dann alle Lampen und Motoren, die nur an einen Lampenausgang angeschlossen sind. 
Der Powermoter in der Mitte triebt den Kompressor an, der wie gesagt keine Funktion hat und deshalb auch nicht benutzt wird.