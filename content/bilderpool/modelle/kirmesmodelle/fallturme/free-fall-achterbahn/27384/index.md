---
layout: "image"
title: "20 Wagen oben"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn17.jpg"
weight: "21"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27384
- /detailsb5b7-2.html
imported:
- "2019"
_4images_image_id: "27384"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27384 -->
... bis der Wagen schlielich oben ankommt.