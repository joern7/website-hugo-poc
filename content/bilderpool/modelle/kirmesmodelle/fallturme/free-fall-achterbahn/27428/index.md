---
layout: "image"
title: "49 von oben"
date: "2010-06-07T21:41:45"
picture: "freefalltower23.jpg"
weight: "51"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27428
- /detailsb133.html
imported:
- "2019"
_4images_image_id: "27428"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27428 -->
Die Schiene, der Reed-Kontakt und nochmal die Kabel