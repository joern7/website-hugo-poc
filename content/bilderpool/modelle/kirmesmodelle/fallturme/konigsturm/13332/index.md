---
layout: "image"
title: "Königsturm Gondelansicht (2)"
date: "2008-01-14T22:48:29"
picture: "free_9.jpg"
weight: "8"
konstrukteure: 
- "Sebatian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/13332
- /details216f.html
imported:
- "2019"
_4images_image_id: "13332"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-14T22:48:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13332 -->
Andere Perspektive der mit 16 bequemen  Sitzplätzen bestückten Gondel und... der Bügel ist geschlossen.