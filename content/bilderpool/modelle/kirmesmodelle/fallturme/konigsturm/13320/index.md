---
layout: "image"
title: "Königsturm Gesamtansicht (1)"
date: "2008-01-13T22:29:27"
picture: "free_2.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/13320
- /details5c1d.html
imported:
- "2019"
_4images_image_id: "13320"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13320 -->
Das ist Königsturm in seiner gesamten Größe und vollen Funktionstüchtigkeit.
Er misst eine stolze Höhe von 1,76m, wobei der Turm kein Vorbild nachahmt, sondern aus reiner Phantasie entstanden ist.