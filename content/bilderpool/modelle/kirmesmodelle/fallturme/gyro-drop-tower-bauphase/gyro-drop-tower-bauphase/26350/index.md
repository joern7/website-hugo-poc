---
layout: "image"
title: "Gyro Drop Tower-Bauphase(1)"
date: "2010-02-13T15:24:19"
picture: "gyrodroptowerbauphase1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26350
- /details083f-2.html
imported:
- "2019"
_4images_image_id: "26350"
_4images_cat_id: "1874"
_4images_user_id: "999"
_4images_image_date: "2010-02-13T15:24:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26350 -->
Freifallturm, der die Fahrgastgondel während des Hochfahren dreht.
Oben angekommen wird die Gondel vom "Catchcar" ausgeklinkt und unten von Wirbelstrombremsen (die momentan noch fehlen) gebremst.