---
layout: "image"
title: "Gondel ohne Fahrgastträger"
date: "2010-02-14T14:01:02"
picture: "s11.jpg"
weight: "11"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26411
- /details13f9.html
imported:
- "2019"
_4images_image_id: "26411"
_4images_cat_id: "1862"
_4images_user_id: "999"
_4images_image_date: "2010-02-14T14:01:02"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26411 -->
unten sieht man gut die rollen für den Fahrgastträger