---
layout: "image"
title: "Drehkranz Antrieb"
date: "2010-02-14T14:00:25"
picture: "s04.jpg"
weight: "4"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26404
- /details0c76.html
imported:
- "2019"
_4images_image_id: "26404"
_4images_cat_id: "1862"
_4images_user_id: "999"
_4images_image_date: "2010-02-14T14:00:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26404 -->
der PM Bezieht seinen Strom über Gewindestangen die am Turm befestigt sind.