---
layout: "image"
title: "Magneten am Turm"
date: "2010-03-29T19:03:26"
picture: "gyrodroptowerbauphasestandderdinge6.jpg"
weight: "6"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26845
- /details6145.html
imported:
- "2019"
_4images_image_id: "26845"
_4images_cat_id: "1920"
_4images_user_id: "999"
_4images_image_date: "2010-03-29T19:03:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26845 -->
In der mitte: 50K Magnet
Links u. Rechts: 25K Magneten