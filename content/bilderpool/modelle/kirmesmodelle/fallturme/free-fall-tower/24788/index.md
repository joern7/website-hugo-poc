---
layout: "image"
title: "Getriebe"
date: "2009-08-14T21:37:26"
picture: "freefalltower3.jpg"
weight: "3"
konstrukteure: 
- "Armin Jacob"
fotografen:
- "Armin Jacob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "armin.jacob"
license: "unknown"
legacy_id:
- /php/details/24788
- /details77ac.html
imported:
- "2019"
_4images_image_id: "24788"
_4images_cat_id: "1705"
_4images_user_id: "988"
_4images_image_date: "2009-08-14T21:37:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24788 -->
Hier sieht man den Powermotor der die Gondel hochzieht und dem Zylinder für den freien Fall.