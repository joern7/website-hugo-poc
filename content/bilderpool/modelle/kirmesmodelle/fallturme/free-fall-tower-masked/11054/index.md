---
layout: "image"
title: "Beleuchtung 1"
date: "2007-07-13T17:46:55"
picture: "freefalltower7.jpg"
weight: "7"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11054
- /details2f69.html
imported:
- "2019"
_4images_image_id: "11054"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11054 -->
4 Turmstrahler (LEDs) beleuchten den FreeFall im Dunklen. Alles Licht das man hier auf dem Foto sieht stammt von den 4 LEDs