---
layout: "image"
title: "Beleuchtung"
date: "2007-08-05T15:30:45"
picture: "freefallumbauten4.jpg"
weight: "12"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11295
- /details7668-3.html
imported:
- "2019"
_4images_image_id: "11295"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11295 -->
Selbstverständlich ist das Kassenhäuschen beleuchtet, allerdings nur mit 2 ft-Leuchtbausteinen. Die Beiden sind in Reihe geschaltet, sonst wären sie zu hell.
Die 5 mm-Leds sind von Conrad, die Fassungen ebenfalls. Hatte ich noch im Keller und die spontane Idee sie zu verwenden... jeweils 3 sind in Reihe geschaltet, dann geht es an einen Verteiler (siehe letztes Foto). Von dort geht es an den zentralen Lichtverteiler im Turm, an dem sind auch die Leds für die Turmbeleuchtung angeschlossen. Das ganze hat einen eigenen Akku, mit an das Interface-Netzteil wollte ich es nicht hängen.