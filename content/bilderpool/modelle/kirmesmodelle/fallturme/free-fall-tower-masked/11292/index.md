---
layout: "image"
title: "Neue Anbauten"
date: "2007-08-05T15:30:45"
picture: "freefallumbauten1.jpg"
weight: "9"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11292
- /detailsaef5.html
imported:
- "2019"
_4images_image_id: "11292"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11292 -->
Neu dazugekommen sind die Verkleidung des Podests, die Treppe mit Beleuchtung der Stufen und das Kassenhäuschen. Außerdem ist der Turm nochmal 40cm höher geworden. Jetzt kann man ihn auch wirklich Turm nennen, vorher wars ja eher ein Türmchen.