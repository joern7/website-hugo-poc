---
layout: "image"
title: "FreeFallTurm_Andockklappen"
date: "2010-07-06T20:03:36"
picture: "freefallturm09.jpg"
weight: "9"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27717
- /details7fb3.html
imported:
- "2019"
_4images_image_id: "27717"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27717 -->
Hier ist nochmal eine der zwei Andockklappen. Wenn die Abstoßwippe oben drauf drückt, lassen sie den Passagierwagen fallen.