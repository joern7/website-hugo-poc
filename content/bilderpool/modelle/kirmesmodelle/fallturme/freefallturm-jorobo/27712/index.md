---
layout: "image"
title: "FreeFallTurm_Bremse, Antrieb, etc."
date: "2010-07-06T20:03:35"
picture: "freefallturm04.jpg"
weight: "4"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27712
- /details174d.html
imported:
- "2019"
_4images_image_id: "27712"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27712 -->
Hier ist der untere Teil des Turms zu sehen: (von Oben nach Unten)
- pneumatische Bremse (die abstehenden Klappen)
-(ganz klein) der Antrieb
- Fläche zum ein- und aussteigen