---
layout: "comment"
hidden: true
title: "11822"
date: "2010-07-06T20:29:02"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
So eine Bremse und auch so eine Loslass-Mechanik hatten wir hier noch nicht. Es gibt doch immer wieder *noch* eine andere Möglichkeit. :-) Schönes Modell. Hätte für die Fotos etwas mehr Licht verdient. ;-)

Gruß,
Stefan