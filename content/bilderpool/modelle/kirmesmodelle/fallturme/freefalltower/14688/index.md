---
layout: "image"
title: "Rückansicht"
date: "2008-06-14T13:25:33"
picture: "freefalltower11.jpg"
weight: "11"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14688
- /detailsb6f3-3.html
imported:
- "2019"
_4images_image_id: "14688"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14688 -->
Im roten Kasten befindet sich der Antrieb, die Bremse und der Akku für den Notstrom.