---
layout: "image"
title: "Bremse"
date: "2008-06-14T13:25:33"
picture: "freefalltower13.jpg"
weight: "13"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14690
- /details6353-3.html
imported:
- "2019"
_4images_image_id: "14690"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14690 -->
Die Bremse besteht aus einem Elektromagneten, einem Minimotor mit Seilwinde und drei Zahnrädern. Beim Lösen der Bremse wird zuerst ein Zahnrad ber Minimotor angehoben und der Elektomagnet eingeschaltet. Wenn das Zahnrad vom Elektromagnet angezogen wird, wird von der Seilwinde wieder ein wenig Seil freigegeben. Beim Auslösen der Bremse wird einfach der Elektromagnet ausgeschaltet, und so das Zahnrad wieder freigegeben. Die Bremse funktioniert somit auch bei einem Stromausfall.