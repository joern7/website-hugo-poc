---
layout: "image"
title: "Kran"
date: "2008-06-14T13:25:33"
picture: "freefalltower22.jpg"
weight: "22"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14699
- /detailsd9ee.html
imported:
- "2019"
_4images_image_id: "14699"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14699 -->
Der Kran ist für Wartungsarbeiten gedacht.