---
layout: "image"
title: "Schalter vorne"
date: "2008-06-14T13:25:32"
picture: "freefalltower07.jpg"
weight: "7"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14684
- /details7298.html
imported:
- "2019"
_4images_image_id: "14684"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14684 -->
Mit den beiden Schaltern kann die Fahrt gestartet und gestoppt werden. Beim stoppen bremst die Gondel sofort ab, und fährt langsam wieder zur Station zurück.