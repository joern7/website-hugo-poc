---
layout: "image"
title: "Blick ins Oberteil"
date: "2008-06-14T13:25:33"
picture: "freefalltower21.jpg"
weight: "21"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14698
- /details347d.html
imported:
- "2019"
_4images_image_id: "14698"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14698 -->
Im Oberteil befindet sich der Antrieb des Krans, sowie die Verkabelung der einzelnen Lampen.