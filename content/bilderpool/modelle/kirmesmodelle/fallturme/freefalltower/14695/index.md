---
layout: "image"
title: "Reedkontakt"
date: "2008-06-14T13:25:33"
picture: "freefalltower18.jpg"
weight: "18"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14695
- /detailse20a.html
imported:
- "2019"
_4images_image_id: "14695"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14695 -->
Der Reedkontakt dient dem Auslösen der Bremse.