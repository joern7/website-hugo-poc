---
layout: "image"
title: "Die Warteschlange"
date: "2011-06-03T09:38:22"
picture: "topspin1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30762
- /detailsbf7b.html
imported:
- "2019"
_4images_image_id: "30762"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T09:38:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30762 -->
Habe sie aus etwa 3m Aluprofil gebaut.