---
layout: "image"
title: "Die neuen Bügel"
date: "2011-06-03T09:38:22"
picture: "topspin2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30763
- /details1767.html
imported:
- "2019"
_4images_image_id: "30763"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T09:38:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30763 -->
Bügel für den Fahrgastträger.Sie gehen parallel zueinander auf.