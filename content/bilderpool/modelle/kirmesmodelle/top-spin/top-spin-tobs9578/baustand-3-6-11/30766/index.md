---
layout: "image"
title: "Die Steuerung des Stroboskops"
date: "2011-06-03T09:38:22"
picture: "topspin5.jpg"
weight: "5"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30766
- /details68c2.html
imported:
- "2019"
_4images_image_id: "30766"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T09:38:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30766 -->
Die 2 Potentiometer dienen zur Verstellung des Kondensatormikrofons( Empfindlichkeit).
Die Lüsterklemmen dienen zur Verbindung der Kabel von den Leds und der Steuerung.