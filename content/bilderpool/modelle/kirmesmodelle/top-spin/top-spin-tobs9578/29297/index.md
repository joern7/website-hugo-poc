---
layout: "image"
title: "Das Schild des Fahrgeschäfts"
date: "2010-11-20T11:19:03"
picture: "topspin1_3.jpg"
weight: "12"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29297
- /details17f0-3.html
imported:
- "2019"
_4images_image_id: "29297"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-20T11:19:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29297 -->
Das Schild besteht aus 54 Led`s und einer Spanholzplatte auf der ich die Löcher ausgesägt habe.
Einfach zu bauen und sehr effektiv.Die Led`s und die dazugehörigen Led- Halter stammen von Conrad.