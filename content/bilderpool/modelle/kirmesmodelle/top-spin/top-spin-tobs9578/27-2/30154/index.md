---
layout: "image"
title: "Das Wasserbecken"
date: "2011-02-28T17:31:46"
picture: "top1_2.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30154
- /details40cd-2.html
imported:
- "2019"
_4images_image_id: "30154"
_4images_cat_id: "2230"
_4images_user_id: "1007"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30154 -->
Der kleine schwarze Kasten ist die Pumpe für die Wassereffekte.
Sie wird so gesteuert, dass der Fahrgastträger nicht nass wird.