---
layout: "image"
title: "Statik Dekowand"
date: "2011-02-27T20:53:29"
picture: "top7.jpg"
weight: "7"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30150
- /detailsd3bd.html
imported:
- "2019"
_4images_image_id: "30150"
_4images_cat_id: "2230"
_4images_user_id: "1007"
_4images_image_date: "2011-02-27T20:53:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30150 -->
Ich werde die Wand naturlich noch gestalten und versuche es etwas wie beim Original aussehen zu lassen.