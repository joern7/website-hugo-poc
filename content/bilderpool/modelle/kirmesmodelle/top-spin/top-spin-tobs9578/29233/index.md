---
layout: "image"
title: "Die Bremse"
date: "2010-11-13T21:55:48"
picture: "topspin3.jpg"
weight: "3"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29233
- /details7c04.html
imported:
- "2019"
_4images_image_id: "29233"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-13T21:55:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29233 -->
Mit diesem kleinen Zylinder kann man die Gondel anhalten und so bei geeignetem loslassen der Bremse überschläge erzeugt werden.