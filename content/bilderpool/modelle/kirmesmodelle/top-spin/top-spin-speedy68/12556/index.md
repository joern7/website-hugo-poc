---
layout: "image"
title: "Drehkranz"
date: "2007-11-08T23:07:49"
picture: "Bild_2.jpg"
weight: "2"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Drehkranz", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12556
- /details8eeb.html
imported:
- "2019"
_4images_image_id: "12556"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:07:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12556 -->
Drehkranz - Seitenteil