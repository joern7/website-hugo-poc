---
layout: "image"
title: "Grundversion"
date: "2007-11-08T23:08:51"
picture: "Bild_10.jpg"
weight: "10"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Drehkranz", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12564
- /details98e4-2.html
imported:
- "2019"
_4images_image_id: "12564"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:08:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12564 -->
