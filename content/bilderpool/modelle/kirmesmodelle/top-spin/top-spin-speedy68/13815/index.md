---
layout: "image"
title: "Antrieb Version 12"
date: "2008-03-02T01:49:47"
picture: "Bild_31.jpg"
weight: "31"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Antrieb", "Power", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/13815
- /details2158.html
imported:
- "2019"
_4images_image_id: "13815"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2008-03-02T01:49:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13815 -->
