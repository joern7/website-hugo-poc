---
layout: "image"
title: "Gesamtansicht"
date: "2007-11-11T08:33:13"
picture: "Bild_13.jpg"
weight: "13"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["speedy68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12630
- /details8f04.html
imported:
- "2019"
_4images_image_id: "12630"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12630 -->
Bei der ersten Fahrprobe