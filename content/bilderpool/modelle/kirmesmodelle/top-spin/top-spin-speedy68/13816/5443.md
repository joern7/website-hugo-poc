---
layout: "comment"
hidden: true
title: "5443"
date: "2008-03-02T13:15:48"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
@Stefan:
Klar, umso mehr Zahnräder, desto mehr Reibungsverluste. Deshalb habe ich auch die unteren Zahnräder separat angetrieben. 
Gruß
Thomas