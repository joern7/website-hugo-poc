---
layout: "comment"
hidden: true
title: "5446"
date: "2008-03-02T16:53:41"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Mein liiieber Scholli, was ein Teil!


Aber was ich nicht verstehe: die Zahnräder greifen doch gar nicht richtig in die Kette ein. Über die oberen zwei (links/rechts am Ende der Zahnrad-Reihe) läuft die Kette so flach, dass selbst mit Zahn-Eingriff kein Drehmoment zu übertragen wäre (es sei denn, mit Andruckrollen von außen).

Und die Z20 rundherum hoppelt die Kette bestimmt auch drüber, wenn mal etwas hängt oder beim Bremsen/Beschleunigen. Wenn das nicht gerade als Überlastschutz gebraucht wird, würden es kleine spitze Teile (z.B. S-Riegel) als Mitnehmer doch auch tun, oder?


Gruß,
Harald