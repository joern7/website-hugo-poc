---
layout: "image"
title: "Untere Gondel"
date: "2007-11-11T08:33:13"
picture: "Bild_21.jpg"
weight: "21"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["speedy", "68", "Kirmesmodell", "Gondel"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12638
- /details9a5d.html
imported:
- "2019"
_4images_image_id: "12638"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12638 -->
Minimalster Abstand zum Boden: Knapp 3cm