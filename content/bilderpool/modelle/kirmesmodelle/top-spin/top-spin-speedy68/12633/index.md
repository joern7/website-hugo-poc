---
layout: "image"
title: "Antrieb"
date: "2007-11-11T08:33:13"
picture: "Bild_16.jpg"
weight: "16"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["speedy", "68", "Kirmesmodell", "Antrieb"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12633
- /details29f9.html
imported:
- "2019"
_4images_image_id: "12633"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12633 -->
Antrieb Version 1 auf einer Seite von hinten