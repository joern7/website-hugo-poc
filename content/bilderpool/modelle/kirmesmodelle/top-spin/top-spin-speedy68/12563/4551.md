---
layout: "comment"
hidden: true
title: "4551"
date: "2007-11-12T10:00:47"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Ich habe mir auch schon den Kopf zerbrochen. Die Zahnräder müssen ja arretiert sein, sonst würden sie sich ja in sich selbst drehen und obendrein öfter nicht genau in die Kette greifen. Ich bin gespannt auf die Antwort.

Gruß, Ralf