---
layout: "image"
title: "Brennstoffzellen Karussell"
date: "2010-01-18T23:33:39"
picture: "Strom_ohne_Dose1ftc.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann"
fotografen:
- "Frederik Vormann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26113
- /detailsbd59.html
imported:
- "2019"
_4images_image_id: "26113"
_4images_cat_id: "124"
_4images_user_id: "453"
_4images_image_date: "2010-01-18T23:33:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26113 -->
das füllen des Wasserstofftanks dauert mit einer Schreibtischlampe, über die 3 Solarzellen, ca. 50 Minuten. Das reicht dann für ca. 40 Minuten Betrieb.