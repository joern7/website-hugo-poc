---
layout: "image"
title: "08 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36094
- /details7737.html
imported:
- "2019"
_4images_image_id: "36094"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36094 -->
Wenn die Gondel ganz unten ist, ist das Kabel ganz abgewickelt.