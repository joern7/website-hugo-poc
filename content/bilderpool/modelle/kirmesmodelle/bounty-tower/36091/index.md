---
layout: "image"
title: "05 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36091
- /details46bc.html
imported:
- "2019"
_4images_image_id: "36091"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36091 -->
Auf drei Grundplatten habe ich das Ganze gebaut, auf eine richtige Station für die Fahrgäste habe ich verzichtet.