---
layout: "image"
title: "18 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower18.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36104
- /details3baa.html
imported:
- "2019"
_4images_image_id: "36104"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36104 -->
Kabel müssen auch irgendwo verlegt werden, am Besten da, wo sie nicht in den Drehkranz gelangen können.