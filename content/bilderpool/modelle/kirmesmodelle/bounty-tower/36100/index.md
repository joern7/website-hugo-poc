---
layout: "image"
title: "14 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower14.jpg"
weight: "14"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36100
- /detailse45a-2.html
imported:
- "2019"
_4images_image_id: "36100"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36100 -->
Ein Blick von oben.