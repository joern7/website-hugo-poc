---
layout: "image"
title: "02 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36088
- /details49c3.html
imported:
- "2019"
_4images_image_id: "36088"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36088 -->
Ich denke man kann ganz gut erkennen, dass sich das Gewicht der Gondeln doch ein wenig bemerkbar macht.

Video: http://www.youtube.com/watch?v=vbnUDZFNeU4