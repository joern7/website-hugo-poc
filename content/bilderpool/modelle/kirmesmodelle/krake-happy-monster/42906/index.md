---
layout: "image"
title: "Die KRAKE - Aufzugsmotor an der Banane"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster10.jpg"
weight: "10"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42906
- /details08f6-2.html
imported:
- "2019"
_4images_image_id: "42906"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42906 -->
