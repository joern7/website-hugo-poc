---
layout: "image"
title: "Die KRAKE Gondelkreuz von Oben inkl antrieb"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster03.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42899
- /detailsfd05.html
imported:
- "2019"
_4images_image_id: "42899"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42899 -->
