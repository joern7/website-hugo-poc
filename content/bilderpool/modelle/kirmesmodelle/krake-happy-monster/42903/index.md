---
layout: "image"
title: "die KRAKE - Rollbahn / Kugellager + Antrieb für Rotation der Banane"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster07.jpg"
weight: "7"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42903
- /details27bc.html
imported:
- "2019"
_4images_image_id: "42903"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42903 -->
