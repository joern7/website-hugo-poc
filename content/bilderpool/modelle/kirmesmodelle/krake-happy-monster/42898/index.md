---
layout: "image"
title: "Die KRAKE Krak(g)armkonstruktion und Antrieb der Gondelkreuze"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster02.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42898
- /detailsb658.html
imported:
- "2019"
_4images_image_id: "42898"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42898 -->
Krak(g)armkonstruktion und Antrieb der Gondelkreuze