---
layout: "image"
title: "Die KRAKE - Aufhängung der Arme"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster23.jpg"
weight: "23"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42919
- /detailsc244.html
imported:
- "2019"
_4images_image_id: "42919"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42919 -->
hierzu sei erwähnt, inzwischen wurde die LAgerung der Arme und die Aufhängung am Drehkranz der Banane bereits modifiziert