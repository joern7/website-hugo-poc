---
layout: "image"
title: "Die KRAKE - von unten Detail"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster17.jpg"
weight: "17"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42913
- /details673e.html
imported:
- "2019"
_4images_image_id: "42913"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42913 -->
