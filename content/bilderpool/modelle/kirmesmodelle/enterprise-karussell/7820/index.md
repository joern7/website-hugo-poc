---
layout: "image"
title: "Sprekende en spelende kermisdraaimolem"
date: "2006-12-10T17:51:48"
picture: "Voice-mudule_001.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7820
- /detailsbfe2.html
imported:
- "2019"
_4images_image_id: "7820"
_4images_cat_id: "1578"
_4images_user_id: "22"
_4images_image_date: "2006-12-10T17:51:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7820 -->
