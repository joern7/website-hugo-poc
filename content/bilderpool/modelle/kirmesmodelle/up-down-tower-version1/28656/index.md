---
layout: "image"
title: "Wagen1"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion16.jpg"
weight: "13"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28656
- /detailsd8c7.html
imported:
- "2019"
_4images_image_id: "28656"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28656 -->
Hier ist einer der Wagen abgebildet. Er hat 8 Räder, die in verschiedenen Winkeln angeordned sind.