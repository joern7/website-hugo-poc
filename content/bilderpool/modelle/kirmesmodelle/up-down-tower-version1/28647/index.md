---
layout: "image"
title: "Haupteil2"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion04.jpg"
weight: "4"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28647
- /detailsa1d4.html
imported:
- "2019"
_4images_image_id: "28647"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28647 -->
Nahaufnahme des Hauptteils.