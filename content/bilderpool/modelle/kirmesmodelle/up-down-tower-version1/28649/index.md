---
layout: "image"
title: "Hauptteil4"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion06.jpg"
weight: "6"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28649
- /detailsfc38.html
imported:
- "2019"
_4images_image_id: "28649"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28649 -->
Hier ist der Hauptteil meines Turms. Dieser Teil ist noch relativ stabiel gebaut mit vielen Verstrebungen. Diese seht ihr auch noch in den nächsten Bildern.