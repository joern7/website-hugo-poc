---
layout: "image"
title: "Inferno_606.JPG"
date: "2005-04-19T11:25:10"
picture: "Inferno_606.jpg"
weight: "2"
konstrukteure: 
- "Vorbild: Fa. Mondial"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4001
- /details3517.html
imported:
- "2019"
_4images_image_id: "4001"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4001 -->
Das ist der "Mittelbau", fertig zum Straßentransport. Der rotierende Arm ist zweigeteilt, seine untere Hälfte bleibt am Träger und ist hier auf dessen Unterseite zu sehen. Die obere Hälfte ist das Gegengewicht und wird auf einem gesonderten Anhänger transportiert. Mittendrin ist noch ein Block, der beide Teile verbindet und elektrische Schleifringe sowie hydraulische Dreh-Übertrager enthält. Das ist der blaue Klotz hier in Bildmitte. Oben kommt das Gegengewicht dran, unten ist der Gondel-Arm gerade eingeschwenkt. 

Zum Aufbau wird das vordere Radgestell mitsamt der rot lackierten Dreiecksträger gelöst, dann werden die Stützen unten ausgeschwenkt und der Arm per Hydraulik angehoben. Ein Kran hievt das Gegengewicht auf die eine Seite und die Gondel (von einem eigenen Transportwagen) auf die andere.