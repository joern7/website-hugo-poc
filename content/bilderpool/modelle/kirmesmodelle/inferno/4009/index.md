---
layout: "image"
title: "Inferno-Podium02.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno-Podium02.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4009
- /detailsb196-3.html
imported:
- "2019"
_4images_image_id: "4009"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4009 -->
Auf diesem Wagen werden das Podium, das Kassenhäuschen, die Treppe und die Gondel transportiert.