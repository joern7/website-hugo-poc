---
layout: "image"
title: "Inferno-Podium40.JPG"
date: "2005-04-19T20:23:04"
picture: "Inferno-Podium40.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4017
- /details0c69.html
imported:
- "2019"
_4images_image_id: "4017"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4017 -->
... bis es dann ganz aufgeklappt ist (jetzt von oben gesehen). Die Aussparung auf der hinteren Seite ist natürlich für den "Inferno" da.