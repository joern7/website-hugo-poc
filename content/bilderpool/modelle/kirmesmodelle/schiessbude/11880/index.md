---
layout: "image"
title: "Volautomatische Schießbude mit 'Trefferzähler'"
date: "2007-09-20T18:48:55"
picture: "Schiessbude.jpeg"
weight: "1"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/11880
- /detailsc909.html
imported:
- "2019"
_4images_image_id: "11880"
_4images_cat_id: "1063"
_4images_user_id: "653"
_4images_image_date: "2007-09-20T18:48:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11880 -->
Die  "Abschussfiguren" werden an einer Kette gezogen und auf der Rückseite der Schießbude automatisch aufgestellt.Die Treffer
werden mit einem Zähler aus  einem Kasettenrekorder gezählt.
Man kann eine Gummibandpistole benutzen.