---
layout: "image"
title: "Karussell 011"
date: "2004-12-05T10:21:16"
picture: "Karussell_011.JPG"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3378
- /details21c4-2.html
imported:
- "2019"
_4images_image_id: "3378"
_4images_cat_id: "293"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:21:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3378 -->
