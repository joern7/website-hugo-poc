---
layout: "image"
title: "Laser-Gabellichtschranke als Impulszähler für die Motoren"
date: "2017-02-12T20:43:14"
picture: "selbststaendigfahrendeseinparkendesautocarolocup5.jpg"
weight: "5"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "johafo"
license: "unknown"
legacy_id:
- /php/details/45199
- /detailsfb5f.html
imported:
- "2019"
_4images_image_id: "45199"
_4images_cat_id: "3366"
_4images_user_id: "1292"
_4images_image_date: "2017-02-12T20:43:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45199 -->
Da uns auch die neuen Encodermotoren zu wenig Leistung lieferten, mussten wir auf XM-Motoren umsteigen. Diese haben allerdings keine eingebauten Impulszähler, die wir zum Einparken brauchien. Die Impulszählung mithilfe eines Tasters erzeugte einen zu hohen Widerstand, daher suchten wir nach einer kontaktlosen Impulsmessung. 

Schließlich haben wir eine Laser-Gabellichtschranke entwickelt: Der Laser (goldfarben) leuchtet auf die Fotodiode. Das rote Z20 unterbricht den Laserstrahl 20 Mal pro Umdrehung. Der Impulszähler ist zwar nicht so fein aufgelöst wie der des Encodermotors, umgerechnet erhalten wir aber pro zurückgelegtem Zentimeter auf der Straße 2,12 Impulse. Für's Einparken ist das ausreichend.