---
layout: "image"
title: "Unterseite des Autos"
date: "2017-02-12T20:43:14"
picture: "selbststaendigfahrendeseinparkendesautocarolocup2.jpg"
weight: "2"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "johafo"
license: "unknown"
legacy_id:
- /php/details/45196
- /details77e3.html
imported:
- "2019"
_4images_image_id: "45196"
_4images_cat_id: "3366"
_4images_user_id: "1292"
_4images_image_date: "2017-02-12T20:43:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45196 -->
Der Aufbau des Roboters ist sehr einfach: der TXT Controller bildet die Mitte des Autos.
Rechts im Bild ist die Lenkung zu sehen: Der ft-Servo, darüber das Servo-Shield und die gelbe Spurstange der Achsschenkellenkung.
In den roten Batteriegehäusen sind zwei Spannungswandler untergebracht (9 V zu 5 V (für den Servo) und 9 V zu 3,3 V (für die I²C Schnittstelle)).

Der Servo erhält über das Servo-Shield ein PWM-Signal, das der TXT über die I²C Schnittstelle steuert. Theoretisch ist es also ganz einfach, einen Servo mit dem TXT zu steuern ....

Der Antrieb (links im Bild) ist über zwei XM-Motoren realisiert, deren Drehmoment mittels einer Kette auf die Antriebsachse übertragen wird. Die Kette hat weniger Reibungsverlust als eine Übertragung mit Zahnrädern; das haben unsere Tests ergeben.