---
layout: "image"
title: "Kurbel"
date: "2015-10-19T18:53:44"
picture: "magnetruehrer2.jpg"
weight: "2"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/42103
- /details7d2d.html
imported:
- "2019"
_4images_image_id: "42103"
_4images_cat_id: "3135"
_4images_user_id: "2465"
_4images_image_date: "2015-10-19T18:53:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42103 -->
Hier kann man die Kurbel erkennen die, angetrieben über ein etwas verstecktes Winkelgetriebe, den Rührmagneten bewegt. In der roten Hülse auf der Kurbel stecken vier von diesen Magneten: http://www.reichelt.de/MAGNET-4-2/3/index.html?&ACTION=3&LA=446&ARTICLE=151644&artnr=MAGNET+4.2&SEARCH=magnete
Der Rührmagnet ist derselbe, wie die Magnete auf der Kurbel
Bei früheren Konstruktionen waren Rührmagnet und Kurbel zu weit voneinander entfernt, so dass der Rührmagnet oft an den Rand des Glases geschleudert wurde und dann dort liegenblieb. Bei dieser Konstruktion kann man die rote Hülse geringfügig verschieben, um den Ringmagnet auf seiner Bahn zu halten, was inzwischen sehr zuverlässig funktioniert.