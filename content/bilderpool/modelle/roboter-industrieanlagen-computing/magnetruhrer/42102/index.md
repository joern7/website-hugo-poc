---
layout: "image"
title: "Magnetrührer Gesamtansicht"
date: "2015-10-19T18:53:44"
picture: "magnetruehrer1.jpg"
weight: "1"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/42102
- /detailse5b9.html
imported:
- "2019"
_4images_image_id: "42102"
_4images_cat_id: "3135"
_4images_user_id: "2465"
_4images_image_date: "2015-10-19T18:53:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42102 -->
Hier eine Gesamtansicht des Magnetrührers. Angetrieben wird das Ganze von einem Power-Motor 1:8. Im Marmeladenglas kann man den Rührmagneten erkennen, der dort Flüssigkeiten verrühren kann.