---
layout: "image"
title: "Spindelkopf schwenkbar"
date: "2017-02-14T19:26:39"
picture: "fraese12.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45211
- /details3296-3.html
imported:
- "2019"
_4images_image_id: "45211"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45211 -->
Der Spindelkopf ist beweglich über einen Drehkranz mit Kugellager von TST.