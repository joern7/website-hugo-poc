---
layout: "comment"
hidden: true
title: "23034"
date: "2017-02-15T18:02:34"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hallo David,

Die Enc.-Motoren werden über die beiden Controller gesteuert. Über den I2C-Anschluss wird nur das Display und die Segmentanzeige angesteuert. Eine Porterweiterung wäre auch noch zusätzlich möglich.

Gruß
Dirk