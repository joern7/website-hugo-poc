---
layout: "image"
title: "ROBO TX Controller"
date: "2017-02-14T19:27:09"
picture: "fraese36.jpg"
weight: "36"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45235
- /details2ebf-3.html
imported:
- "2019"
_4images_image_id: "45235"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45235 -->
Die Ansteuerung wird über zwei ROBO TX Controller
mit dem I2C-Anschluss durchgeführt.