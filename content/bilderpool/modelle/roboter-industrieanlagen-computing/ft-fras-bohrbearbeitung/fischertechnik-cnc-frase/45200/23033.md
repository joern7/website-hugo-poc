---
layout: "comment"
hidden: true
title: "23033"
date: "2017-02-15T17:59:10"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Danke. Ja, ich habe beim Bau der CNC-Fräse darauf geachtet, das diese möglichst
genau fährt und auch stabil ist. Bei der Genauigkeit ist sich noch Luft nach oben.
Mir ging es bei der Fräse eher um die Simulation. Zum richtigen Fräsen ist sie nicht
geeignet.

Zu deiner Frage, ob man eine Diagonale damit fräsen kann? Ja es ist möglich. Die Geschwindigkeit im Vor- und Rücklauf pro Achse werden gespeichert. Bei einem Plotter
kann man das sogar zeichnen, bzw. sichtbar machen.

Mit den neuen Schrittmotoren wäre das sich möglich, allerdings bräuchte man noch einen
Treiber zum komfortablen ansteuern über ROBO Pro.

Grüße
Dirk