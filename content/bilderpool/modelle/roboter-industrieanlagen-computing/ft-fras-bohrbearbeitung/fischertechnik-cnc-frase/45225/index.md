---
layout: "image"
title: "Ansicht vorne schräg"
date: "2017-02-14T19:27:01"
picture: "fraese26.jpg"
weight: "26"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45225
- /details918f-2.html
imported:
- "2019"
_4images_image_id: "45225"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45225 -->
