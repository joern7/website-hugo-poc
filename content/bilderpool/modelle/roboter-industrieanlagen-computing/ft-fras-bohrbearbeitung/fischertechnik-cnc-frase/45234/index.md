---
layout: "image"
title: "Steckeranschlüsse hinten"
date: "2017-02-14T19:27:09"
picture: "fraese35.jpg"
weight: "35"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45234
- /details3c4b.html
imported:
- "2019"
_4images_image_id: "45234"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45234 -->
Links und rechts die Anschlüsse für die beiden ROBO TX Controller.
Mittig der I2C-Anschluss für die Segmentanzeige und das Display.