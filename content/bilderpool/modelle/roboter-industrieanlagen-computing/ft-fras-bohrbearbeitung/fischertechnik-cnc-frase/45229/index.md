---
layout: "image"
title: "Ansicht seitlich"
date: "2017-02-14T19:27:01"
picture: "fraese30.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45229
- /detailsa962.html
imported:
- "2019"
_4images_image_id: "45229"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45229 -->
Links der Faltenbalg, den ich aus Pappe gefaltet habe.

http://www.buntbahn.de/modellbau/printview.php?t=5795