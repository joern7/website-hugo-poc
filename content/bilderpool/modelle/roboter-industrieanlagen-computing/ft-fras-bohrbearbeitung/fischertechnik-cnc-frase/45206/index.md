---
layout: "image"
title: "Bedienpanel vorne"
date: "2017-02-14T19:16:21"
picture: "fraese07.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45206
- /details9169.html
imported:
- "2019"
_4images_image_id: "45206"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45206 -->
Das 16x2 Display wird über I2C angesteuert. Es besitzt eine Menüführung für die CNC-Fräsmaschine um zu Teachen
und Programme abzurufen.

Die Miniatatur Drucktaster sind von Pollin.