---
layout: "image"
title: "fischertechnik CNC-Fräse seitlich"
date: "2017-02-14T19:16:21"
picture: "fraese02.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45201
- /detailsd6ac.html
imported:
- "2019"
_4images_image_id: "45201"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45201 -->
