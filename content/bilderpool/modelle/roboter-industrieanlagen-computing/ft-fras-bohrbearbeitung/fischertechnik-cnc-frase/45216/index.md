---
layout: "image"
title: "Antrieb Spindelkopf"
date: "2017-02-14T19:26:39"
picture: "fraese17.jpg"
weight: "17"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45216
- /detailsb6eb.html
imported:
- "2019"
_4images_image_id: "45216"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45216 -->
Antrieb Spindelkopf. Er kann + - 45 Grad geschwenkt werden.