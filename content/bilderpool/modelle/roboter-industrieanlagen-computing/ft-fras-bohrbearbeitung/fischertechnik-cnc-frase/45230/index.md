---
layout: "image"
title: "Ansicht hinten links schräg"
date: "2017-02-14T19:27:09"
picture: "fraese31.jpg"
weight: "31"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45230
- /details1234-2.html
imported:
- "2019"
_4images_image_id: "45230"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45230 -->
