---
layout: "image"
title: "Beweglicher Arm"
date: "2017-02-14T19:16:21"
picture: "fraese06.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45205
- /detailsbabf.html
imported:
- "2019"
_4images_image_id: "45205"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45205 -->
Der bewegliche Arm ist aus Gelenksteinen gebaut.