---
layout: "image"
title: "Druckmaschine - Druckkopf"
date: "2011-01-01T11:41:32"
picture: "P1000195.jpg"
weight: "4"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/29567
- /details6c27.html
imported:
- "2019"
_4images_image_id: "29567"
_4images_cat_id: "3398"
_4images_user_id: "1258"
_4images_image_date: "2011-01-01T11:41:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29567 -->
