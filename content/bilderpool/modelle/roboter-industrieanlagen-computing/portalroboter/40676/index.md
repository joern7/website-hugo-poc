---
layout: "image"
title: "Portalroboter-Antrieb X-Achse"
date: "2015-03-21T18:16:47"
picture: "portalroboter09.jpg"
weight: "9"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/40676
- /detailscee8.html
imported:
- "2019"
_4images_image_id: "40676"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40676 -->
Der Antrieb der X-Achse von hinten.