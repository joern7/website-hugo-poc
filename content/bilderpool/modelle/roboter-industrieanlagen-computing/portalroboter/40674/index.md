---
layout: "image"
title: "Portalroboter-X-Achse"
date: "2015-03-21T18:16:47"
picture: "portalroboter07.jpg"
weight: "7"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/40674
- /detailsb2de.html
imported:
- "2019"
_4images_image_id: "40674"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40674 -->
In dem Batteriefach ist ein Gegengewicht, da der Kopf sost nach vorne Kippt sobald er ein Gewicht aufnimmt. Links ist einer der 4 Reedkontakte zur Endlagenerfassung der Y-Achse zu sehen