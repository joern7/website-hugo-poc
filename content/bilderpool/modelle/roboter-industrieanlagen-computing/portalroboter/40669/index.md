---
layout: "image"
title: "Portalroboter-Bausteinmagazin"
date: "2015-03-21T18:16:47"
picture: "portalroboter02.jpg"
weight: "2"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/40669
- /details387b.html
imported:
- "2019"
_4images_image_id: "40669"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40669 -->
Das Magazin für die Holzklötze. Wenn der Roboter einen Klotz herausgeholt hat, wird automatisch der nächste nachgeschoben. Wenn das Magazin dann leer ist, fährt der Schieber wieder zurück.