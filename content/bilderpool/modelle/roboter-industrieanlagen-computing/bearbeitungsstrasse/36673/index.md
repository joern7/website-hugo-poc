---
layout: "image"
title: "Bearbeitungsstraße 2"
date: "2013-02-24T14:53:34"
picture: "IMG_4620.jpg"
weight: "10"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/36673
- /details90fd-2.html
imported:
- "2019"
_4images_image_id: "36673"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-24T14:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36673 -->
Hier kann man gut die Stanzmaschiene erkennen , ursprünglich sollte da eigendlich die Bohrstation sein aber ich habe leider nur 1 Tx-Controller und da hätten die Eingänge leider nicht gereicht :-( :-(