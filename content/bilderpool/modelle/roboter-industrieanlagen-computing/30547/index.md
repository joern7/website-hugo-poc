---
layout: "image"
title: "Training Roboter II"
date: "2011-05-14T20:56:35"
picture: "DSC00652.jpg"
weight: "17"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
keywords: ["Here", "is", "my", "latest", "FT", "project", "largely", "inspired", "by", "the", "original", "Training", "roboter", "30572.", "I", "replaced", "the", "original", "optical", "readers", "(", "32357)and", "the", "Bar", "code", "wheel", "(", "32367", ")", "which", "cannot", "be", "use", "with", "the", "TX-C", "by", "the", "new", "encoder", "motors", "135484.", "That", "was", "made", "possible", "by", "the", "use", "of", "the", "TST", "special", "colers", "that", "allow", "the", "use", "of", "metal", "axles", "with", "the", "encoder", "motors", "Translation", "Hier", "ist", "meine", "neueste", "FT", "Projekt", "wird", "weitgehend", "von", "der", "ursprünglichen", "Ausbildung", "inspiriert", "Roboter", "30572.", "Ich", "ersetzte", "die", "ursprüngliche"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30547
- /detailse038.html
imported:
- "2019"
_4images_image_id: "30547"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T20:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30547 -->
Here is my latest FT project,largely inspired by the original Training
roboter 30572.
 I replaced the original optical readers ( 32357)and the Bar code wheel
( 32367 ),which cannot be use with the TX-C,by the new encoder motors 135484.
 That was made possible by the use of the TST special colers that allow the
 use of metal axles  with the encoder motors

 Translation

Hier ist meine neueste FT Projekt wird weitgehend von der ursprünglichen Ausbildung inspiriert
 Roboter 30572.
  Ich ersetzte die ursprüngliche optische Lesegeräte (32.357) und der Bar-Code-Rad
 (32367), die nicht den Einsatz mit dem TX-C, durch die neuen Encoder-Motoren 135484 werden können.
  Das war möglich durch die Verwendung des TST besondere colers, die es dem
  Einsatz von Metall-Achsen mit den Encoder-Motoren