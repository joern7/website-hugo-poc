---
layout: "image"
title: "Zange"
date: "2011-06-14T22:35:23"
picture: "rob3.jpg"
weight: "3"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: ["Industrieroboter", "Zange", "Baustein", "Joystick", "Gamepad", "Java"]
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30857
- /details2e6c.html
imported:
- "2019"
_4images_image_id: "30857"
_4images_cat_id: "2303"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30857 -->
Die Zange hält gerade einen Baustein.

Das Video zum Roboter gibt's hier: http://www.youtube.com/watch?v=43wwCYx08m4