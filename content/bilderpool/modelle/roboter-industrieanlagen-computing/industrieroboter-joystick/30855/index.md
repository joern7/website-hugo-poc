---
layout: "image"
title: "Gesamtansicht"
date: "2011-06-14T22:35:23"
picture: "rob1.jpg"
weight: "1"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: ["Roboter", "Industrieroboter", "Zange", "Baustein", "Java", "Joystick", "Gamepad"]
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30855
- /detailsc01d.html
imported:
- "2019"
_4images_image_id: "30855"
_4images_cat_id: "2303"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30855 -->
Der Roboter von vorne.

Das Video zum Roboter gibt's hier: http://www.youtube.com/watch?v=43wwCYx08m4