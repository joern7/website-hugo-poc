---
layout: "image"
title: "Kamera und Sensorfelder"
date: "2018-04-17T22:07:48"
picture: "othelloroboter16.jpg"
weight: "16"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47463
- /details58e8.html
imported:
- "2019"
_4images_image_id: "47463"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47463 -->
Die 16 RGB Sensorfelder c0 bis cF und das Referenz-Gelb (das lange schmale Sensorfeld) werden parallel 20mal eingelesen, anhand der Referenz-Weiß-RGB-Werte aus Sensorfeld cW korrigiert und ins HSV Farbsystem konvertiert. Die Farbzuordnung erfolgt dann durch Vergleich der Hue-Werte relativ zum Hue des Referenz-Gelb.