---
layout: "image"
title: "Spielsteinwender, Detail links"
date: "2018-04-17T22:07:48"
picture: "othelloroboter06.jpg"
weight: "6"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47453
- /details4049.html
imported:
- "2019"
_4images_image_id: "47453"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47453 -->
Im Prinzip ist das ein Planetengetriebe, allerdings ohne das äußere Hohlrad. Das ganze Getriebe wird von einer nicht drehbar  fixierten (die Seiltrommel links im Bild versteckt) Metallachse gehalten. Der Planetenträger, die 60er Drehscheibe mit Freilaufnabe, dreht sich frei auf dieser Achse und wird über das hintere Z30-Zahnrad, Schnecke und Kardanwelle von einem Encoder-Motor angetrieben. Das Sonnenrad (Z20) steht fest. Auf ihm rollen die Planetenräder (Rast-Z10) ab und drehen dabei die Magneten. 
Für einen Wendevorgang vollführt der Planetenträger eine halbe Umdrehung. Die Magneten drehen sich dabei einmal komplett um die eigene Achse. Auf halben Weg stehen sie waagerecht einander zugewandt. 
Also:
1) Der untere Magnet nimmt einen Spielstein auf.
2) Vierteldrehung des Trägers bzw. halbe Drehung der Magneten
3) In waagrechter einander zugewandter Position der Magneten erfolgt die Übergabe des Spielsteins
4) Vierteldrehung in die gleiche Richtung
5) Der vormals obere Magnet ist nun unten angekommen und legt den gedrehten Stein ab.
Aufeinanderfolgende Wendeprozesse erfolgen in entgegengesetzter Drehrichtung.