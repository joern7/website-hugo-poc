---
layout: "image"
title: "Die Rückseite"
date: "2018-04-17T22:07:48"
picture: "othelloroboter03.jpg"
weight: "3"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47450
- /detailsa907.html
imported:
- "2019"
_4images_image_id: "47450"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47450 -->
Unten im Vordergrund ist der Antriebsmotor des linearen Schneckenvortriebs zu sehen, dahinter querstehend der Antriebsmotor des Drehtellers. In diesem Bild ist das Spielbrett weit, aber noch nicht vollständig, nach hinten eingefahren. Rechts zu sehen, der etwas ausladend montierte xs-Motor-Kurbelantrieb des Spielsteinspenders. In der Mitte sieht man den hinteren Befestigungspunkt des Wende-Mechanismus: Die drei roten 5mm-Steine unten am Aluprofil. Rechts davon hinter der gelochten 60er Strebe kann man den Antriebsmotor des Wendemechanismus erahnen. Die drei verbauten Aluprofile stellen übrigens meinen gesamten Bestand dar. Glück, dass sie so schön passen.