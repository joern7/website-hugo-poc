---
layout: "image"
title: "umgebauter Kettenantrieb"
date: "2015-08-26T21:05:18"
picture: "Kettenantrieb.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Untersetzung", "Kettenantrieb"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41863
- /details314e.html
imported:
- "2019"
_4images_image_id: "41863"
_4images_cat_id: "3109"
_4images_user_id: "579"
_4images_image_date: "2015-08-26T21:05:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41863 -->
Nachdem beim Untersetzungsgetriebe ab und zu ein Zahnrad rausgesprungen ist, ich daher mit höherer Drehzahl am Zahnrad auf die Kette gehen wollte und auch eine höhere Auflösung der Segmentscheiben und mehr Bodenfreiheit gebrauchen konnte, habe ich den Kettenantrieb umgebaut. Der Antrieb erfolgt über das eine Z10. Die Segmentscheibe wird über das andere Z10 angetrieben. So laufen die Motoren besser, es drehen keine Antriebsachsen mehr durch und die Segmentscheiben laufen mit der dreifachen Drehzahl.