---
layout: "image"
title: "Ansicht von oben"
date: "2015-08-03T13:01:55"
picture: "1029.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41705
- /detailsa59b-2.html
imported:
- "2019"
_4images_image_id: "41705"
_4images_cat_id: "3109"
_4images_user_id: "579"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41705 -->
