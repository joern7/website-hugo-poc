---
layout: "image"
title: "R2D2 - Gesamtansicht"
date: "2012-12-22T11:46:56"
picture: "rd1.jpg"
weight: "1"
konstrukteure: 
- "Nils und Marie"
fotografen:
- "Thomas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Nils555"
license: "unknown"
legacy_id:
- /php/details/36342
- /detailsd9ad.html
imported:
- "2019"
_4images_image_id: "36342"
_4images_cat_id: "2697"
_4images_user_id: "1602"
_4images_image_date: "2012-12-22T11:46:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36342 -->
R2D2-Nachbau aus Fischertechnik