---
layout: "image"
title: "R2D2-Füße auf Rädern"
date: "2012-12-22T11:46:57"
picture: "rd2.jpg"
weight: "2"
konstrukteure: 
- "Nils und Marie"
fotografen:
- "Thomas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Nils555"
license: "unknown"
legacy_id:
- /php/details/36343
- /details1f61.html
imported:
- "2019"
_4images_image_id: "36343"
_4images_cat_id: "2697"
_4images_user_id: "1602"
_4images_image_date: "2012-12-22T11:46:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36343 -->
Die R2D2-Füße rotieren frei 360° um eine Achse.
Damit kann der R2D2 fahren.