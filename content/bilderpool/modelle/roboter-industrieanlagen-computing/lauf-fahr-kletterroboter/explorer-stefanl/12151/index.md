---
layout: "image"
title: "Explorer 8"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12151
- /details2741-2.html
imported:
- "2019"
_4images_image_id: "12151"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12151 -->
