---
layout: "image"
title: "Explorer 8"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14550
- /details3c1d.html
imported:
- "2019"
_4images_image_id: "14550"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14550 -->
Akku, Spurensensor und Summer. Es ist aber immer noch Platz für weitere Funktionen.