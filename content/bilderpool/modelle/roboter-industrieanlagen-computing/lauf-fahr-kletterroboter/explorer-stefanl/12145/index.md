---
layout: "image"
title: "Explorer 2"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12145
- /detailsa616.html
imported:
- "2019"
_4images_image_id: "12145"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12145 -->
