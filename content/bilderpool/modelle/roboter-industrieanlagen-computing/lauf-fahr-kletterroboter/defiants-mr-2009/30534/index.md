---
layout: "image"
title: "scan360.png"
date: "2011-05-02T20:14:40"
picture: "scan360.png"
weight: "8"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/30534
- /detailsea32.html
imported:
- "2019"
_4images_image_id: "30534"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2011-05-02T20:14:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30534 -->
Time for an update: An 360° look around with the ultra sonic range finder.