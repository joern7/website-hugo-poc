---
layout: "image"
title: "path_detect.jpg"
date: "2012-02-25T14:04:01"
picture: "path_detect.jpg"
weight: "9"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/34395
- /details6678.html
imported:
- "2019"
_4images_image_id: "34395"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2012-02-25T14:04:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34395 -->
Spurverfolgung ohne Spursensor:
Bild 1: Die Spur wird mit einer Kameraaufgenommen (Aus Datenschutzgrund hier etwas bearbeitet)
Bild 2: Das Bild wird anschließend Binarisiert. Es wird nur der relevante Teil weiter betrachtet.
Bild 3: Es wird ein Kantenfilter angewendet um die Spur zu identifizieren.
Bild 4: Aus dem letzten Bild lässt sich dann eine Kurve interpolieren (Hier 3. Grad) auf dem der Roboter fahren kann.