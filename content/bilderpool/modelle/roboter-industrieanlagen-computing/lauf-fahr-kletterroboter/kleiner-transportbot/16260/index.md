---
layout: "image"
title: "Luftbild"
date: "2008-11-12T21:53:44"
picture: "autonomerkleinroboter3.jpg"
weight: "3"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16260
- /details8df7-2.html
imported:
- "2019"
_4images_image_id: "16260"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16260 -->
Hier sieht man, wie die beiden Motoren für den Antrieb zusammengebaut sind.