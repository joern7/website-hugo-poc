---
layout: "image"
title: "Radar"
date: "2008-03-22T12:31:00"
picture: "mr7.jpg"
weight: "7"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/14002
- /detailsf331-2.html
imported:
- "2019"
_4images_image_id: "14002"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-03-22T12:31:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14002 -->
Antrieb und Positionstaster.