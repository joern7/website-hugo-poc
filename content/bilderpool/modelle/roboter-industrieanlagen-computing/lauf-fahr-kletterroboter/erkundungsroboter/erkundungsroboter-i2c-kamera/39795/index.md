---
layout: "image"
title: "Pixy I2C Kamera hinten"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung06.jpg"
weight: "6"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39795
- /detailse088.html
imported:
- "2019"
_4images_image_id: "39795"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39795 -->
Hinten habe ich das I2C-Kabel durch die Kassette geführt.