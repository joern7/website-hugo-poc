---
layout: "image"
title: "Pfostenbuchse Robo TX Controller"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung15.jpg"
weight: "15"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39804
- /detailse377-2.html
imported:
- "2019"
_4images_image_id: "39804"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39804 -->
6 pol. I2C-Kabel (Eigenbau) für den Robo TX Controller.