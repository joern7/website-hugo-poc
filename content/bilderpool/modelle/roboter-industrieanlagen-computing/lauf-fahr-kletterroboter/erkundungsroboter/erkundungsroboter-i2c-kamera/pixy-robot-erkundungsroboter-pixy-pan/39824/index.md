---
layout: "image"
title: "Pixy Objekt anlernen"
date: "2014-11-15T19:29:09"
picture: "pixysoftware7.jpg"
weight: "7"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39824
- /details033e-2.html
imported:
- "2019"
_4images_image_id: "39824"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39824 -->
Dann auf: 

1. Action - Set Signature...1
2. Objekt 1 mit der Maus markieren.
3. Das Objekt ist erlernt
4. Es wird ein Rahmen mit S=1 angezeigt

Pixy behält die RGB Werte in seinem Speicher, auch wenn ihr
Pixy ausschaltet. :-)