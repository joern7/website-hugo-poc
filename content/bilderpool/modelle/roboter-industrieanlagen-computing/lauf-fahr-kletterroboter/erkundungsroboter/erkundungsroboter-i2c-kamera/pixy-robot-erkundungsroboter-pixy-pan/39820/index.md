---
layout: "image"
title: "PIXY Robot Erkundungsroboter Objekterkennung"
date: "2014-11-15T19:29:09"
picture: "pixysoftware3.jpg"
weight: "3"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39820
- /details1f71.html
imported:
- "2019"
_4images_image_id: "39820"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39820 -->
Hier ist mein PIXY Robot Erkundungsroboter

M1 ist rechts in Fahrrichtung
M2 ist links in Fahrrichtung angeschlossen

Kamera an Robo TX Ext 2 I2C
Pixy I2C = Hex 0x14 

Das Modell dazu findet ihr auch auf
https://www.youtube.com/watch?v=ptRdp4T47gI
oder 
https://www.ftcommunity.de/categories.php?cat_id=2985