---
layout: "image"
title: "Pixy Software"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung22.jpg"
weight: "22"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39811
- /details1051-2.html
imported:
- "2019"
_4images_image_id: "39811"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39811 -->
Software Installation

Als erstes müsst ihr euch die Software von der Pixy Seite laden.
Dann die Software und Firmware installieren.

Dazu braucht ihr ein möglichst kurzes Mini-Usb Kabel, welches hinten in die Kamera gesteckt wird.
(ist im Lieferumfang leider nicht enthalten) 

Quelle Software:
http://cmucam.org/projects/cmucam5/wiki