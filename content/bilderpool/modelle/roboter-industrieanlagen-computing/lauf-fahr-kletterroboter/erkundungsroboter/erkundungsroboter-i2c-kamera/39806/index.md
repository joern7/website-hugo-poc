---
layout: "image"
title: "PIXY I2C-Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung17.jpg"
weight: "17"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39806
- /details244b.html
imported:
- "2019"
_4images_image_id: "39806"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39806 -->
Die Kamera wird von Charmed Labs hergestellt..
Sie ist sehr vielseitig. Sie wurde für speziell für den Bereich visuelle Robotik entwickelt als Kickstarter Projekt. 

Die Kamera und Software ist ein Open-Source Projekt. :-)

Sie ist sehr einfach zu handhaben. 

Sie kann über SPI, Uart, I2C, analog/digital X-Wert, analog/digital Y-Wert
ausgelesen werde.

Hier ein paar Grunddaten:

- verstellbare I2C Adresse (wichtig)
- 7 verschiedene Farben erlernen/erkennen
- mehrere hundert Obkekte gleichzeitig erkennen 
- X- und Y- Wert des Objekts
- Länge und Breite des Objekts

Das Erkennen von Objekten ist über den Modusknopf der Kamera möglich,
oder über eine Software möglich.

weitere Infos findet ihr hier:
http://cmucam.org/projects/cmucam5

Bezugquelle Pixy Cam (ca 65,45 Euro + Versand)

http://www.watterott.com/de/Pixy-CMUcam




