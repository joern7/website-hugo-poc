---
layout: "image"
title: "PIXY I2C-Kamera + Linse"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung21.jpg"
weight: "21"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39810
- /details810d.html
imported:
- "2019"
_4images_image_id: "39810"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39810 -->
Die Linse kann durch drehen verstellt werden. (Man kann auch andere Linsen montieren.)