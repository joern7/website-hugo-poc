---
layout: "image"
title: "Kassette von schräg vorn"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung14.jpg"
weight: "14"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39803
- /details5e0e-2.html
imported:
- "2019"
_4images_image_id: "39803"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39803 -->
Oben ist ein kleines Loch ca. 4,0 mm für den Modusknopf der 
I2C-Kamera.