---
layout: "image"
title: "Erkundungsroboter + I2C Kamera seitlich"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung02.jpg"
weight: "2"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39791
- /details5d56-2.html
imported:
- "2019"
_4images_image_id: "39791"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39791 -->
