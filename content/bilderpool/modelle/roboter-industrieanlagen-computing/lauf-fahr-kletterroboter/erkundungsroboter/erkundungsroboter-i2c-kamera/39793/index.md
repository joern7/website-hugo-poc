---
layout: "image"
title: "Pixy I2C Kamera vorne"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung04.jpg"
weight: "4"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39793
- /details573f.html
imported:
- "2019"
_4images_image_id: "39793"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39793 -->
Die Kamera habe ich in einer Kassette 32076 mit Klarsichtdeckel 35360
untergebracht. Die LED leuchtet, wenn ein Objekt erkannt wurde. 
7 verschiedene Objekte (Farben) erlernbar.