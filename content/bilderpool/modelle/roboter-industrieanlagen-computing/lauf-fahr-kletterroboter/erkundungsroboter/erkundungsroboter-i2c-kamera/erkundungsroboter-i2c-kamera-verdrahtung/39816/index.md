---
layout: "image"
title: "Robo Pro I2C Objekterkennung Verdrahtung"
date: "2014-11-12T18:50:54"
picture: "roboproicobjekterkennungverdrahtung1.jpg"
weight: "1"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39816
- /detailse417-2.html
imported:
- "2019"
_4images_image_id: "39816"
_4images_cat_id: "2986"
_4images_user_id: "2303"
_4images_image_date: "2014-11-12T18:50:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39816 -->
Hier seht ihr die genaue Belegung vom Robo TX I2C und der Pixy Kamera.

Achtung:
Das mitgelieferte Pixy Kabel funktioniert nicht. Es ist für den Arduino SPI Anschluß.

Bitte nur die 5 Volt vom RoboTX I2C in den 10 pol. Anschluß senden.


