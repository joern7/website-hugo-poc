---
layout: "image"
title: "Pixy I2C-Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung10.jpg"
weight: "10"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39799
- /details8cec-2.html
imported:
- "2019"
_4images_image_id: "39799"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39799 -->
Hier die zerlegte I2C-Kamera.

1x Pixy I2C Kamera
1x Kassette
1x Deckel
4x Distanzschrauben M3 -16 mm lang v. Mainboard
3x Muttern M3
3x Schrauben M3 - 5 mm lang
