---
layout: "image"
title: "Gesamtansicht"
date: "2008-06-14T13:25:32"
picture: "invertiertespendel1.jpg"
weight: "1"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/14675
- /details30f3-2.html
imported:
- "2019"
_4images_image_id: "14675"
_4images_cat_id: "1347"
_4images_user_id: "521"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14675 -->
Dieser Roboter balanciert den Stab, indem er die Plattform so bewegt, dass der Stab nicht umfällt.