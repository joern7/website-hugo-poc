---
layout: "image"
title: "Laufroboter - Elektronik"
date: "2013-02-07T14:24:48"
picture: "ft_blackfoot4..jpg"
weight: "9"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/36587
- /detailsd6fa-2.html
imported:
- "2019"
_4images_image_id: "36587"
_4images_cat_id: "1184"
_4images_user_id: "427"
_4images_image_date: "2013-02-07T14:24:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36587 -->
Beeper - Kompass - IR - Entfernung