---
layout: "image"
title: "Bild 3"
date: "2007-12-12T20:32:33"
picture: "3.jpg"
weight: "3"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13066
- /detailsff4d.html
imported:
- "2019"
_4images_image_id: "13066"
_4images_cat_id: "1184"
_4images_user_id: "34"
_4images_image_date: "2007-12-12T20:32:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13066 -->
Interessant wir es ihn mal hochzuhalten und die Bewegung sich anzuschauen.
Man würde meinen das er eigendlich anders herum laufen müsste.
Das Modell funktioniert deshalb weil sich die Teile etwas nach innen biegen und er somit stabil steht. Wenn man eine Führung macht geht es nicht mehr und er kippt um.
Zwischen Platte und Bein sind 7,5 Grad Bausteine!
Hinter den Platten oben sind nur an den Seiten 15er Bausteine.
Nächste Herrausforderung ist es einen mit Powermot zu bauen...