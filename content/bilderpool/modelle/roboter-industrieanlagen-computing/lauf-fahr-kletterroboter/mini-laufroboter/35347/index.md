---
layout: "image"
title: "walker_1.jpg"
date: "2012-08-21T17:42:04"
picture: "walker_2.jpg"
weight: "4"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/35347
- /details5260-3.html
imported:
- "2019"
_4images_image_id: "35347"
_4images_cat_id: "1184"
_4images_user_id: "427"
_4images_image_date: "2012-08-21T17:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35347 -->
ft-LPE-Laufroboter ( nach David Buckley "Bambino" )