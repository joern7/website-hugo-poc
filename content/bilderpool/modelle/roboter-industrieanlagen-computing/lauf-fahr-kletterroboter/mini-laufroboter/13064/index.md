---
layout: "image"
title: "Mini Lauf Roboter"
date: "2007-12-12T20:32:33"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Mini", "Laufroboter", "Hunamoid", "Roboter", "Zweibeiniger"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13064
- /details8769.html
imported:
- "2019"
_4images_image_id: "13064"
_4images_cat_id: "1184"
_4images_user_id: "34"
_4images_image_date: "2007-12-12T20:32:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13064 -->
Mini Laufroboter.
Die Streben sind unten mit Verbindern festgemacht. Durch das einseitige Gewicht vom Motor läuft er etwas im Kreis.
Wenn man den Trafo voll aufdreht ist er sehr schnell!