---
layout: "image"
title: "Heavy-duty Explorer mit Kugellager und Alu's"
date: "2007-02-12T17:46:56"
picture: "Explorer-Eucalypta_008.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/8989
- /details11a4.html
imported:
- "2019"
_4images_image_id: "8989"
_4images_cat_id: "818"
_4images_user_id: "22"
_4images_image_date: "2007-02-12T17:46:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8989 -->
Heavy-duty Explorer mit Kugellager und Alu's