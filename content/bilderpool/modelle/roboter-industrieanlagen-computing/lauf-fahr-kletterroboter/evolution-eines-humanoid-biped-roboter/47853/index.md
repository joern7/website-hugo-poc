---
layout: "image"
title: "Unterschenkel komplett Stellung A"
date: "2018-09-03T16:06:26"
picture: "u_schenkel_A.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47853
- /detailsb7e9-3.html
imported:
- "2019"
_4images_image_id: "47853"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-03T16:06:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47853 -->
der komplette Unterschenkel mit Antrieb und Potentiometer für die Winkemessung. Hier in der vordersten Stellung.
Daneben meine Bedieneinheit, Joystick