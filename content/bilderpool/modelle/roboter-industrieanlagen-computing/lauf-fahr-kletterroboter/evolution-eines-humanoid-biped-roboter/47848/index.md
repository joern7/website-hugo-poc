---
layout: "image"
title: "Vollansicht zur Veranschaulichung der Idee"
date: "2018-08-27T17:50:39"
picture: "GesamtPlan_2.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47848
- /detailsa501.html
imported:
- "2019"
_4images_image_id: "47848"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-08-27T17:50:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47848 -->
Wie man sieht ist nur das 1. Bein fertig. Für das weitere hatte ich noch keine Zeit und auch kein Material.

Konzept:
Sprunggelenk, Knie und Hüfte (eine Richtung) werden angetrieben mit einem Schneckenantrieb.
Vorteil: selbsthemmend; kein Energiebedarf, wenn keine Bewegung
Detail: Die Antriebe werden unterstützt durch Federn, die die potentielle Energie aufnehmen.
Dadurch ist es für die Motoren zwar etwas schwerer, den Roboter abzusenken, aber viel leichter, ihn anzuheben. Im Idealfall sollten die Federn alleine den Roboter halten können, dazu fehlt mir aber eine ausreichende Auswahl an Zug- und Druckfedern.

Ein Bein wiegt im Moment 800g.
Pro Bein habe ich nur 3 Motoren (Freiheitsgrade) vorgesehen. Damit ist die Funktion natürlich beschränkt! Erstens, er wird nicht um die Kurve laufen können. Zweitens, um auf einem Bein zu stehen, muß er den Oberkörper als Gegengewicht verschieben; dazu muß ich am Ende noch Gewicht drauf packen. Eventuell komme ich mit 1 weiteren Motor in der Hüfte aus.

Eigentlich braucht man noch ein paar Freiheitsgrade mehr. Zusätzlich einmal in der Ferse und 2 x in der Hüfte (je Seite). Das bringt aber noch mehr Gewicht und Komplexität. Ich versuche es erstmal so, ob ich ihn überhaupt zum Laufen bringe.

Die ersten Versuche, die Motoren zu drehen, sind vielversprechend. Das Bein ist relativ stabil und die Motoren schaffen es locker, hoch- und runter zu fahren.

Im Moment mache ich Versuche mit einem Arduino, die Motoren anzusteuern. Erstes Ergebnis: Beschleunigen und Bremsen der Motoren mit Rampenfunktion bringt Vorteile.
Abruptes Anfahren/Anhalten der Motoren von 0V auf 9V und umgekehrt bringt das Bein ins Nachschwingen und macht es instabil.

Jetzt habe ich erstmal weiteres Material bestellt; die Motoren für das 2te Bein und ein paar Alus, um die Beine noch stabiler zu machen