---
layout: "comment"
hidden: true
title: "24163"
date: "2018-09-16T19:19:26"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Hüftmechanik und die Idee mit dem Oberkörper-Gewichtsausgleich sind doch prima. Ich bin gespannt auf das Endergebnis und wünsche viel Erfolg!

Gruß,
Stefan