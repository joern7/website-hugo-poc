---
layout: "image"
title: "Absolutwertgeber / Winkelmessung"
date: "2018-09-01T20:48:11"
picture: "Absolutwertgeber_1.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Winkelmessung", "Absolutwertgeber", "Potentiometer"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47849
- /details3423.html
imported:
- "2019"
_4images_image_id: "47849"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-01T20:48:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47849 -->
Um den Biped richtig steuern zu können, muß ich immer den genauen Winkel der Gelenke kennen. Absolutwertgeber/ elektronische Winkelmesser sind ja meist recht teuer und groß.

Ich habe eine Idee von früher: https://www.ftcommunity.de/categories.php?cat_id=2914
wieder aufgegriffen und verwende Trimm-Potis. Da passt eigentlich eine Rastachse ganz gut rein, für diese Anwendung hat mir das ganze aber zuviel Spiel. Deswegen nehme ich eine Winkelachse und feile die Enden flach...aber nur soviel, dass fast eine Presspassung mit dem Poti entsteht. Ansonsten ist alles Standard-Fischertechnik. Die Lampenfassung hält das Poti gut am Platz.