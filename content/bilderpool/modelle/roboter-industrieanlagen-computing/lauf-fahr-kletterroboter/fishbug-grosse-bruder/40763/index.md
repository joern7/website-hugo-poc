---
layout: "image"
title: "Fishbug - Perspektive"
date: "2015-04-16T17:40:45"
picture: "fishbugdergrossebruder1.jpg"
weight: "1"
konstrukteure: 
- "K"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40763
- /details2813-2.html
imported:
- "2019"
_4images_image_id: "40763"
_4images_cat_id: "3063"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T17:40:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40763 -->
Inspieriert durch lemkajen´s Bauten  habe ich mir gedacht:
Jetzt muss auch ein echter Fischer-Bug her :)

Die jetztige Konstruktion "geht erst richtig ab", wenn sie mit 16V "befeuert" wird.
Damit der Motor aber nicht "abraucht" wird also die Konstruktion noch verändert.
Leichter, andere Beine usw. mal sehen wie weit wir kommen.....


