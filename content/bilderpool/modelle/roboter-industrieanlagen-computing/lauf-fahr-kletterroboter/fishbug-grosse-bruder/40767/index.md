---
layout: "image"
title: "Fishbug von Oben"
date: "2015-04-16T17:40:45"
picture: "fishbugdergrossebruder5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40767
- /detailscba0.html
imported:
- "2019"
_4images_image_id: "40767"
_4images_cat_id: "3063"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T17:40:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40767 -->
