---
layout: "image"
title: "Hugo der kleine Laufroboter"
date: "2012-12-10T22:41:43"
picture: "hugo1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/36249
- /details670b.html
imported:
- "2019"
_4images_image_id: "36249"
_4images_cat_id: "2690"
_4images_user_id: "381"
_4images_image_date: "2012-12-10T22:41:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36249 -->
Auf Wanderschaft