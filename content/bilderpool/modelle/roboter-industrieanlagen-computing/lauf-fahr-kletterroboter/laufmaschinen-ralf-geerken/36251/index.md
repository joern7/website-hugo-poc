---
layout: "image"
title: "Lauter Platten."
date: "2012-12-10T22:41:44"
picture: "hugo3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/36251
- /details836a-3.html
imported:
- "2019"
_4images_image_id: "36251"
_4images_cat_id: "2690"
_4images_user_id: "381"
_4images_image_date: "2012-12-10T22:41:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36251 -->
Da bin ich aber geplättet.