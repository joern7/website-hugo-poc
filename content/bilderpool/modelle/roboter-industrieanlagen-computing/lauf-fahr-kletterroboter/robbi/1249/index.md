---
layout: "image"
title: "Mein neuer Robbi"
date: "2003-07-20T19:33:03"
picture: "Robbi.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1249
- /detailsb4ec.html
imported:
- "2019"
_4images_image_id: "1249"
_4images_cat_id: "209"
_4images_user_id: "130"
_4images_image_date: "2003-07-20T19:33:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1249 -->
Das ist mein neuer Roboter der die Arme hoch und runter bewegen kann und den Kopf dreht. Die Augen und der Mund sind mit den Highlights nachgebildet und blinken natürlich. Bin am überlegen ob ich ihn noch an den Händen motorisiere zum abwinkeln und schliessen der Hände bzw. Greifzangen. Evtl noch einen Motor zum fahren. ZZt. steht er fest auf einer schwarzen Bauplatte. Es war ein altes Ausstellungsstück der fischerwerke und ich hab ihn bei ebay ersteigert.