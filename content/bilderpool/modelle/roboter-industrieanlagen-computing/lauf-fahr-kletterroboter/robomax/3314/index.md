---
layout: "image"
title: "robomax04.jpg"
date: "2004-11-23T19:48:21"
picture: "img_3646_resize.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3314
- /details4e25-2.html
imported:
- "2019"
_4images_image_id: "3314"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3314 -->
Bick von oben. Gut erkennbar die beiden Akkublocks ganz unten. Diese werden zur Schwerpunktverlagerung beim Einfahren des vorderen oder hinteren Beines benötigt :)