---
layout: "image"
title: "robomax06.jpg"
date: "2004-11-23T19:48:21"
picture: "img_3665_resize.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3316
- /details2245.html
imported:
- "2019"
_4images_image_id: "3316"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3316 -->
Innenraum im Detail, oben kommt auch eine Bauplatte drauf. Für ROBO Interface und I/O ist genügend Platz.