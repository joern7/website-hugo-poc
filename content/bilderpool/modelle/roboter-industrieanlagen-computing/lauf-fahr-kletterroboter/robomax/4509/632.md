---
layout: "comment"
hidden: true
title: "632"
date: "2005-07-16T01:15:02"
uploadBy:
- "Marten"
license: "unknown"
imported:
- "2019"
---
kardangelenk-effektenIch furchte diese Weise von Antrieb einer gelenkten Asche hat ein grosses Problem :(

Die Rader werden im eingelenkten Zustand nicht mit konstantem Drehzahl drehen. 
Fur Antrieb ohne Drehzahlschwankungen braucht man zwei Kardangelenken. Die Linie woruber die Lenkung sich dreht muss dabei zwischen beide Kardangelenken platziert sein. 
Problem wird dann dass die varierende Abstand zwischen den Kardangelenken beim Einlenken. Dazu braucht mann z.B. eine "Splined Asche": 1 Rohr mit Innenverzahnung, 1 Asche mit Aussenverzahnung.

Eine Loesung waere drei Kegelzahnrader pro Rad zu benutzen. Die Asche des Kegelzahnrads faellt zusammen mit der Asche woruber das Rad beim Lenken gedreht wird (zB ein gelenkwurfel wie im Bild). 
Das Kegelrad aus dem Differential greift ein im Kegelzahnrad der Drehasche. Das dritte Kegelrad greift ebenfals im Kegelzahnrad der Drehasche ein und treibt das Rad an.

Nachteil hier ist das verschieben der FT teilen wenn (zuviel) Kraft drauf kommt. Hier werde ich mal versuchen Teilen (metal Kegelrader) aus der Conrad Modellbau Katalog zu benutzen.

mfrgr Marten, der gerne ein FT splined Asche haben wurde :)