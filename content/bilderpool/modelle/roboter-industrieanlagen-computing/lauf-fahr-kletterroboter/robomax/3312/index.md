---
layout: "image"
title: "robomax02.jpg"
date: "2004-11-23T19:37:52"
picture: "dsc01808_rotate_resize.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3312
- /details4612.html
imported:
- "2019"
_4images_image_id: "3312"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-23T19:37:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3312 -->
Die Achsen von zwei der drei Stützen werden angetrieben, ein- und ausfahren lässt sich jede einzelne.