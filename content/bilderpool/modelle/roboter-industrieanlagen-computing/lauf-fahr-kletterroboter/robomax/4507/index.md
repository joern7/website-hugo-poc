---
layout: "image"
title: "robomax13.jpg"
date: "2005-07-11T09:52:25"
picture: "img_4475_resize.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4507
- /details6959.html
imported:
- "2019"
_4images_image_id: "4507"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4507 -->
Mal schnell für's Foto zusammengesteckt. Am Fahrwerk wird noch gearbeitet :)