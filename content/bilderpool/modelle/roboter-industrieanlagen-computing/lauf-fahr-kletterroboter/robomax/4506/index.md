---
layout: "image"
title: "robomax12.jpg"
date: "2005-07-11T09:52:25"
picture: "img_4482_resize.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4506
- /details0744-3.html
imported:
- "2019"
_4images_image_id: "4506"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4506 -->
Interface Deckel mit dem 3 Akku, unter der Hubmechanik ist leider nur Platz für 2 Stück nebeneinander plus Verschiebemotoren auf Hubzahnstangen.