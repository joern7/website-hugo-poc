---
layout: "image"
title: "robomax17.jpg"
date: "2005-07-11T09:52:25"
picture: "img_3900_resize.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4511
- /detailsa434.html
imported:
- "2019"
_4images_image_id: "4511"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4511 -->
Spur wurde verbreitert um Platz für den MiniMot zum Antrieb der Lenkung zu bekommen. Endlich erste Fahrversuche :)