---
layout: "image"
title: "Antrieb"
date: "2012-03-30T13:45:04"
picture: "roboter5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34710
- /details38bd-2.html
imported:
- "2019"
_4images_image_id: "34710"
_4images_cat_id: "2563"
_4images_user_id: "453"
_4images_image_date: "2012-03-30T13:45:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34710 -->
Antrieb Version 2