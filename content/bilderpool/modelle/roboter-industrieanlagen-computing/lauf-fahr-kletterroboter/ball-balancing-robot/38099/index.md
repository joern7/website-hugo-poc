---
layout: "image"
title: "The ball (2)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38099
- /detailse1ae.html
imported:
- "2019"
_4images_image_id: "38099"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38099 -->
The styropor is light, but very soft. I hardened it using many layers of parquet lacquer and japan paper. This is what is used to make smooth light surfaces for the wings of model planes. After a lot of trail and error (wrong paper, using lacker that solved the styropor), I did about 6 layers of lacquer, small pieces of japan paper, with lots of sanding in between. It gave a very smooth, reasonably hard surface. I mounted the ball on a ft motor driven contruction to have it rotate when I work on it. The hairdryer was used to speed up the drying and hardening process of a layer.