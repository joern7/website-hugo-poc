---
layout: "image"
title: "Drive system prototype"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38092
- /details112f-4.html
imported:
- "2019"
_4images_image_id: "38092"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38092 -->
First I experimented driving a ball up side down to get the translations from driving the motors to rotating the ball in a certain direction. By controlling the 3 motors, you control movement of the ball in 3 dimensions: rotations along the 3 axes of the ball, two horizontal ones and one vertical one. For example, driving all three motors in the same direction and at the same speed makes the ball spin around its vertical axis. Driving 2 motors in opposing direction and at the same speed, and not moving the 3rd one, moves the ball in the direction of the 3rd motor (or away from it). With a bit of math it is possible to derive the translations that allow you to drive in any direction, and reversely, from the 3 wheel encoders, derive the ball position and speed.