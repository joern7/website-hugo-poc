---
layout: "image"
title: "Fine-tuning the controller"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38089
- /details4bfd.html
imported:
- "2019"
_4images_image_id: "38089"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38089 -->
The screenshot shows the movement of the robot ball on the ground while staying in position (or trying to). The top two graphs (px and py) are the real world coordinates of the ball position. The bottom graph shows them combined in 2D. The graph top-right is the body rotation of the robot. Also visible are the controller parameters, that can be changed in real-time using the PC app, and sent to the robot using the bluetooth connection.