---
layout: "image"
title: "The frame (2)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38095
- /details4fc5-2.html
imported:
- "2019"
_4images_image_id: "38095"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38095 -->
At first sight the construction seems heavy for a simple robot (more suitable for a crane), but apparenty the dynamic forces are fairly significant.