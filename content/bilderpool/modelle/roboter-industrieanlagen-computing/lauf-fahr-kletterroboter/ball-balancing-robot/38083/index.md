---
layout: "image"
title: "The drive system in action"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38083
- /details3f5b.html
imported:
- "2019"
_4images_image_id: "38083"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38083 -->
The robot rides on three axles with omni wheels, directly driven by 3 red (1:50) power motors. I tried to make it work with grey 1:20 motors which would have allowed the robot to drive faster, but they don't have enough power for the job, at least not when driven by effectively 7V, which is what you get out of the motor outputs of the TX controller, using the ft battery as power source. I was tempted to try using a 9.6V, 8 cell NiMH battery, but was't completely ready to sacrifice two TX controllers...