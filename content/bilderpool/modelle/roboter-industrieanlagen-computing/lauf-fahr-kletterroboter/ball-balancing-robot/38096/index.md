---
layout: "image"
title: "The controllers (1)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38096
- /detailsaf6e.html
imported:
- "2019"
_4images_image_id: "38096"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38096 -->
The brains are on top of the frame. Two TX controllers (needed for the 6 counters), and between it (from bottom to top) the gyro/accelerometer/magnometer board, the decoder printer, the battery, and the remote control unit.