---
layout: "image"
title: "The ball (1)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38098
- /detailsbf6a.html
imported:
- "2019"
_4images_image_id: "38098"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38098 -->
Somebody who had built a (non ft) ball balancing robot gave me a recepe for making a smooth, light stiff ball, based on a 25cm styropor one (http://www.amazon.de/gp/product/B00ARFRO8E/ref=ox_sc_act_title_1?ie=UTF8&psc=1&smid=A30YUXLJNYIIR6).