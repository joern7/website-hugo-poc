---
layout: "image"
title: "The frame (1)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38094
- /detailsae9f.html
imported:
- "2019"
_4images_image_id: "38094"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38094 -->
There are fairly significant forces on the legs of the frame, and having as little flexibility as possible is important, as I found out. The initial version was a lot less stiff, and although I could have the robot balance, driving it was very difficult, as it seemed to require controller parameter settings that were out of the range where proper balancing was possible. Making the frame more stiff helped a lot.