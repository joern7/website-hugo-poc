---
layout: "image"
title: "The back side"
date: "2013-05-12T16:50:50"
picture: "balancingrobot03.jpg"
weight: "3"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36893
- /details7696.html
imported:
- "2019"
_4images_image_id: "36893"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36893 -->
Showing the small board on the back that holds the ICs that transform the quad encoder signal from wheel encoders into up and down counters that feed into the TX counter inputs. Each wheel uses two counters, one that counts when moving clockwise, and one that counts when moving counter clockwise. The board also generates a 5V source for the gyro/accelerometer.

The wheel encoders are not needed to balance. Balancing works fine without position information. They are just use to give the robot a sense of location and to enable the remote control movements and steering.