---
layout: "image"
title: "Robot Balancing"
date: "2013-05-12T16:50:50"
picture: "balancingrobot02.jpg"
weight: "2"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36892
- /details0ea2.html
imported:
- "2019"
_4images_image_id: "36892"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36892 -->
The robot can stand upright, while slowly swinging back and forth. It has several modes. In one mode it stays on position. If you push it away it will come back to position. In another mode the robot doesn't care where it is and will move at the slightest touch. If you put a finger on top, you can take it with you. When it sees a signal from the remote control, it will go into drive mode and move forward and backward, and make turns like a track vehicle.
While tuning the controller the robot would fall over a lot, so to prevent damage I had a couple of studs connected so that it could never fall flat on its face. You can see it in some of the video's (http://www.youtube.com/watch?v=DWVOfmidFlE). I removed them when done as it makes the robot look better ;)