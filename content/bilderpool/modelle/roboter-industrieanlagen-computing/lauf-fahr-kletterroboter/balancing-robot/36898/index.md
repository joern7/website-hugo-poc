---
layout: "image"
title: "Balancing"
date: "2013-05-12T16:50:50"
picture: "balancingrobot08.jpg"
weight: "8"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36898
- /details04aa.html
imported:
- "2019"
_4images_image_id: "36898"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36898 -->
What happens if you tell the robot to stay on location, but you bring it out of balance by sticking some alus in? It will find a new balance, for which it needs to heel over, which you can see it do in the picture here.

What happens when you bring the robot out of balance, but you don't care where it is? It will start moving towards the weight in order not to fall. That is one way of making a balancing robot move: throw it off balance, either physically (by putting some weight on) or electronically (by making it think 'above' is somewhere else). 

This is all demonstrated in the video here: http://www.youtube.com/watch?v=DWVOfmidFlE