---
layout: "image"
title: "The program"
date: "2013-05-12T16:50:50"
picture: "balancingrobot09.jpg"
weight: "9"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36899
- /detailsb212.html
imported:
- "2019"
_4images_image_id: "36899"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36899 -->
The robot is controlled by a C program running in offline mode on the TX. It uses a 10ms cycle to:
- read the I2C gyro and accelerometer sensors (it's reading 6 bytes, and takes a couple miliseconds),
- calculate the Kalman filter for the angle and gyro bias,
- read the counter inputs, subtract them, and deal with counter overruns, 
- apply a Kalman filter to the counter values in order to get decent position and speed values, 
- read the remote control inputs and calculate how the robot is to be driven/steered,
- calculate the PWM drive for the motors using the PID function
- and do some housekeeping, such a putting some data on the TX display and send more data over bluetooth to a PC that monitors the robot.
Each of the above steps use 200-300 microsecond (some more, some less) of a 'ProgTic' call, so it all easily fits into a 10ms cycle.

The robot functions stand-alone, but can connect to an application running on a PC and send real-time data to the PC. The PC can also tell the robot to change the PID variables which was very useful for the manual tuning of the PID constants.

I've demonstrated the robot at a meeting of the FischerTechnik Club Nederland in Schoonhoven in November 2012: http://www.fischertechnikclub.nl/index.php?option=com_phocagallery&view=detail&catid=83%3Aclubdag-schoonhoven&id=4884%3Awillem-evert-nijenhuis-01-schoonhoven-2012&Itemid=113&lang=en