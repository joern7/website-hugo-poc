---
layout: "image"
title: "15-IR-Aufnahme"
date: "2008-11-24T19:38:43"
picture: "15-IR-Aufnahme.jpg"
weight: "15"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["WebCam", "IR-LED", "Infrarotfilter", "Infrarotlicht", "Reflexlichtschranke"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16504
- /detailse66d-2.html
imported:
- "2019"
_4images_image_id: "16504"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-24T19:38:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16504 -->
Wenn man einer WebCam das IR-Sperrfilter klaut, kann man das helle Leuchten von IR-Leuchtdioden schön sichtbar machen. Die Helligkeit der IR-Dioden ist sehr erheblich.

Auf diese Weise findet man prima heraus, ob die Reflexlichtschranke ihr Licht an die richtige Stelle bringt.