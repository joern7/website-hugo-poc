---
layout: "image"
title: "13-Einbau Sensor"
date: "2008-11-23T16:17:33"
picture: "13-Einbau_Sensor.jpg"
weight: "13"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Reflexlichtschranke", "Einbau", "Spange"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16502
- /details5a4d-3.html
imported:
- "2019"
_4images_image_id: "16502"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-23T16:17:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16502 -->
So sieht das dann nach dem Einbau aus. Aus einem zusätzlichen Kupferdraht und aus den Drähten angelöteter Widerstände habe ich eine Art Spange gemacht, die die Reflexlichtschranke am Platz hält.