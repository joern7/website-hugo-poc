---
layout: "image"
title: "01-Lenkgetriebe"
date: "2008-11-09T14:32:55"
picture: "01-Lenkgetriebe.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Lenkgetriebe", "Fahrwerk", "kompakt", "Vorderachse"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16232
- /details2d32-2.html
imported:
- "2019"
_4images_image_id: "16232"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16232 -->
Um einen kleineren Labyrinthroboter zu bauen, habe ich mich von meiner bisher favorisierten Lenkantriebsachse verabschiedet. Dieses Lenkgetriebe für eine ganz normale Fahrwerksvorderachse baut da schon mal nett kompakt. Die Betätigung des Lenkhebels ist auf späteren Bildern zu sehen.