---
layout: "image"
title: "14-Einbau Elektrik"
date: "2008-11-23T16:17:33"
picture: "14-Einbau_Elektrik.jpg"
weight: "14"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Fototransistor", "Widerstand", "Schmitt-Trigger", "Spannungsteiler"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16503
- /details7e58-2.html
imported:
- "2019"
_4images_image_id: "16503"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-23T16:17:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16503 -->
Auf der anderen Seite des Getriebes ist in einer kleinen Nische Platz für die Widerstände. Als Vorwiderstand für die IR-Diode dienen die beiden blauen 820-Ohm Widerstände, weil ich keinen 390er oder 470er mehr hatte. Darunter ist ein 18kOhm Widerstand, der mit dem Fototransistor einen Spannungsteiler bildet.

Leitet der Fototransistor, dann liegt auf der Ergebnisleitung fast die volle Spannung, sonst fast Masse. Mal sehen, ob das Interface das schon so auswerten kann, zusammen mit einem eingeschalteten Schmitt-Trigger (CMos 4093) geht es auf jeden Fall.