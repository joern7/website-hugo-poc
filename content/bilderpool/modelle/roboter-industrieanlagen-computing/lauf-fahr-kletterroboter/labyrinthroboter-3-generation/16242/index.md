---
layout: "image"
title: "11-Größenvergleich"
date: "2008-11-09T14:34:47"
picture: "11-Grenvergleich.jpg"
weight: "11"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Größenvergleich", "Taster", "Kollisionstaster", "Entfernungssensoren"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16242
- /details5998.html
imported:
- "2019"
_4images_image_id: "16242"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:34:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16242 -->
Der Größenvergleich beider Roboter macht deutlich, was für ein gewaltiger Brocken die bisherige dreirädrige Version ist.

Bleibt noch eine Hürde zu nehmen: Der kleine Roboter braucht noch Kolissionstaster nach vorne und nach hinten, sowie zwei Entfernungssensoren nach der Seite. Hoffentlich findet sich dafür noch Platz.