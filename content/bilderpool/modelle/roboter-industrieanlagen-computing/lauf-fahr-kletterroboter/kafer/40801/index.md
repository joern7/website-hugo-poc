---
layout: "image"
title: "Käfer"
date: "2015-04-17T23:15:50"
picture: "kaefer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40801
- /details442f.html
imported:
- "2019"
_4images_image_id: "40801"
_4images_cat_id: "3065"
_4images_user_id: "104"
_4images_image_date: "2015-04-17T23:15:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40801 -->
Der Körper trägt die Federbeide sowie einen kleinen Motor. Mit ein bisschen Papier ist die Schnecke in eine Hälfte eines Gelenksteins eingeklemmt. Das ergibt eine recht flotte Unwucht. Die Bewegungsgeschwindigkeit des Käfers lässt allerdings noch zu wünschen übrig (vielleicht ginge es mit etwas steiler angebrachten Federn besser), und ganz bestimmt tut die Unwucht dem Motor auch nicht gut. Das müsste man noch entkoppeln.

Ein Video gibt's unter https://www.youtube.com/watch?v=zYF5-LxLGZU