---
layout: "image"
title: "Akku"
date: "2008-07-15T22:17:21"
picture: "nichtandiewandfahrendesauto7.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14874
- /detailscdfa.html
imported:
- "2019"
_4images_image_id: "14874"
_4images_cat_id: "1357"
_4images_user_id: "747"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14874 -->
Auf diesem Bild sieht man den Akku.