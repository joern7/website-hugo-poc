---
layout: "image"
title: "Lab2-08"
date: "2007-06-09T20:47:34"
picture: "Lab2-08.jpg"
weight: "8"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/10777
- /details0416.html
imported:
- "2019"
_4images_image_id: "10777"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10777 -->
Hier das ganze von unten: Die Radsensoren lösen nur 8 Impulse je Umdrehung auf oder in Strecke gesehen, 18 mm. Eine richtige Odometrie muß ja nicht unbedingt sein, der Rundumsensor ist wichtiger. Der Rest ist so mehr zur Kontrolle.

Jetzt muß nur noch etwas Software drauf.