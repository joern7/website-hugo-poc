---
layout: "image"
title: "Lab2-04"
date: "2007-06-09T20:47:34"
picture: "Lab2-04.jpg"
weight: "4"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/10773
- /detailse72d.html
imported:
- "2019"
_4images_image_id: "10773"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10773 -->
Von oben nach unten: Antrieb des Rundumsensors mit Positionstaster, darunter das RoboPro-Interface, davor der Lenkmotor. Man beachte die überaus elegante Halterung des Potentiometers für die Lenkwinkelmessung. Das hat zufällig alles exakt gepaßt. Das Poti steckt in einem Reedkontakthalter. Die Teile sind einfach für alles zu gebrauchen.