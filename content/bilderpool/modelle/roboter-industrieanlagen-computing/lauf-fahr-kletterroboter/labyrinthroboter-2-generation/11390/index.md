---
layout: "image"
title: "Lab2-11"
date: "2007-08-17T19:24:15"
picture: "Sackgasse.jpg"
weight: "11"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11390
- /details6644.html
imported:
- "2019"
_4images_image_id: "11390"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-08-17T19:24:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11390 -->
Der Roboter steht jetzt in einer knapp 30 cm breiten und knapp 60 cm langen Sackgasse. Jetzt sind die Begrenzungswände klar auszumachen, der Entfernungssensor arbeitet jetzt mit mm-Genauigkeit.

Trick: nicht gebrauchte Analogeingänge auf Masse gelegt. Voilà.

Auswerteformel: Entfernung in mm = 16000*Meßwert^(-1,0467).