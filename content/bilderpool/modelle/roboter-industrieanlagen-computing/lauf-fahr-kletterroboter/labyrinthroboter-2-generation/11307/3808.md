---
layout: "comment"
hidden: true
title: "3808"
date: "2007-08-09T20:14:35"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Das kommt von der geringen Auflösung des AD-Wandlers. Für die Zwischenräume hat er keine Werte.
Solange der Wandler den immer gleichen Wert abgibt entsteht im Diagramm ein Kreisbogen. Aufgezeichnet werden ja der Radius (Entfernung) und der Winkel. Das sind Polarkoordinaten, die ich im obigen Diagramm in rechtwinklige Koordinaten umgerechnet habe.

Bis dann
Remadus