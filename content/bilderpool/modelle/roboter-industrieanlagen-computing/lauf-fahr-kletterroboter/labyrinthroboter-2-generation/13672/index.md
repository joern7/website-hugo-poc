---
layout: "image"
title: "Lab2-12"
date: "2008-02-17T15:39:33"
picture: "Neue_Sensoren.jpg"
weight: "12"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13672
- /detailsb493-2.html
imported:
- "2019"
_4images_image_id: "13672"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13672 -->
Der Labyrinthroboter ist jetzt umgestellt auf Kugellagerung und Gummireifen. Dann hat er noch neue Teilscheiben erhalten, die eine vierfach höhere Auflösung erlauben, als die bisher benutzten Taster mit Impulsrädern.

Das Interface kommt damit an seine Grenze. Bei maximaler Geradeausgeschwindigkeit vergißt das Interface bereits erste Impulse zu zählen.

Mit etwas kleinerer Geschwindigkeit zählt das Interface sauber und erlaubt jetzt auch ein recht exaktes Wendemanöver.