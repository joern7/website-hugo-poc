---
layout: "image"
title: "Unterboden"
date: "2016-06-06T17:29:03"
picture: "fahrroboter4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/43696
- /detailsfb9b.html
imported:
- "2019"
_4images_image_id: "43696"
_4images_cat_id: "3236"
_4images_user_id: "2042"
_4images_image_date: "2016-06-06T17:29:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43696 -->
Auf diesem Foto der Roboterunterseite sind die beiden Motoren und der Akku gut zu sehen. Das Kabel vom Akkublock bis zum TXT-Controller wird dabei im Spalt zwischen der schwarzen Statikstrebe und dem Akku selbst nach oben durchgeführt.