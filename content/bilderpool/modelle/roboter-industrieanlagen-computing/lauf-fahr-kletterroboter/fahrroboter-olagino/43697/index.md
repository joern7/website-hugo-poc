---
layout: "image"
title: "Front und Seite"
date: "2016-06-06T17:29:03"
picture: "fahrroboter5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/43697
- /details013e-2.html
imported:
- "2019"
_4images_image_id: "43697"
_4images_cat_id: "3236"
_4images_user_id: "2042"
_4images_image_date: "2016-06-06T17:29:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43697 -->
Hier ist nochmal der Roboter in voller Ansicht zu sehen, wie auch die beiden LED's für die ausreichende Beleuchtung des Kamerabildes.