---
layout: "image"
title: "Seitenansicht"
date: "2016-06-06T17:29:03"
picture: "fahrroboter3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/43695
- /detailsf563.html
imported:
- "2019"
_4images_image_id: "43695"
_4images_cat_id: "3236"
_4images_user_id: "2042"
_4images_image_date: "2016-06-06T17:29:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43695 -->
Hier ist das flache Profil des Roboters zu sehen, allerdings steht die Kamera über, was für einen ausreichend großen Bildausschnitt nötig war.