---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-04T17:01:04"
picture: "MobilerRoboter7.jpg"
weight: "7"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7704
- /detailsfc7e.html
imported:
- "2019"
_4images_image_id: "7704"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T17:01:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7704 -->
Hier ist der ganze Roboter mit Akku und Interface zu sehen.