---
layout: "overview"
title: "Mobiler Roboter mit Gleichlaufgetriebe"
date: 2020-02-22T08:01:03+01:00
legacy_id:
- /php/categories/722
- /categories0ae8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=722 --> 
Ein mobiler Roboter mit Gleichlaufgetriebe.
Eine Verbesserung des Basismodells vom Robo Mobile Set.