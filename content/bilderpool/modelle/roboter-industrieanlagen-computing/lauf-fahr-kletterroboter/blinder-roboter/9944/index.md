---
layout: "image"
title: "'Abstandssensor'"
date: "2007-04-04T10:29:45"
picture: "blinderroboter12.jpg"
weight: "19"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9944
- /detailsb560.html
imported:
- "2019"
_4images_image_id: "9944"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9944 -->
Hier wird einmal ein Abstandssensor sein, dort wo jetzt der Solarschalter ist.