---
layout: "image"
title: "Befestigung hinten"
date: "2007-04-04T10:29:45"
picture: "blinderroboter06.jpg"
weight: "13"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9938
- /details0515-2.html
imported:
- "2019"
_4images_image_id: "9938"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9938 -->
Befestigung der Streben.