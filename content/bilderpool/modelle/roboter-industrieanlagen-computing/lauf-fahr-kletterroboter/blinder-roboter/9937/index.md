---
layout: "image"
title: "Abstandssensorhalterung"
date: "2007-04-04T10:29:45"
picture: "blinderroboter05.jpg"
weight: "12"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9937
- /details447d-3.html
imported:
- "2019"
_4images_image_id: "9937"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9937 -->
Um die Wände zu peilen, ein Sinn den die Blinden auch haben.