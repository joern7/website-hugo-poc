---
layout: "image"
title: "Taster vorne"
date: "2007-03-01T16:56:00"
picture: "blinderroboter3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9189
- /details0bf5.html
imported:
- "2019"
_4images_image_id: "9189"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9189 -->
Ein guter alter Taster tuts für das...