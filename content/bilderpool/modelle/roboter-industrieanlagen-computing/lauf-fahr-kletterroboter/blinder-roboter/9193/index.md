---
layout: "image"
title: "Kette von oben"
date: "2007-03-01T16:56:00"
picture: "blinderroboter7.jpg"
weight: "7"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9193
- /details5759.html
imported:
- "2019"
_4images_image_id: "9193"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9193 -->
