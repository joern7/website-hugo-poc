---
layout: "image"
title: "Roboter neu"
date: "2007-04-04T10:29:45"
picture: "blinderroboter03.jpg"
weight: "10"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9935
- /detailsb0dc-2.html
imported:
- "2019"
_4images_image_id: "9935"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9935 -->
Gesamtansicht