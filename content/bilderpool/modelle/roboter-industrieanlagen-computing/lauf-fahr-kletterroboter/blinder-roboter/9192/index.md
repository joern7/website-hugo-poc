---
layout: "image"
title: "Roboter mit Stockantrieb"
date: "2007-03-01T16:56:00"
picture: "blinderroboter6.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9192
- /detailse27e.html
imported:
- "2019"
_4images_image_id: "9192"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9192 -->
Die Kraft reicht nur mit zusätzlicher Übersetzung.