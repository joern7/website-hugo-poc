---
layout: "image"
title: "Schalter"
date: "2007-04-04T10:29:45"
picture: "blinderroboter10.jpg"
weight: "17"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9942
- /details6ed4.html
imported:
- "2019"
_4images_image_id: "9942"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9942 -->
Diesen Schalter habe ich eingebaut um nicht immer den Stecker ziehen zu müssen.