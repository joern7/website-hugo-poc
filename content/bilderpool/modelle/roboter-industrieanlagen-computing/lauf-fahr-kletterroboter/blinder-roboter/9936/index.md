---
layout: "image"
title: "Roboterdesign neu"
date: "2007-04-04T10:29:45"
picture: "blinderroboter04.jpg"
weight: "11"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9936
- /details2d98.html
imported:
- "2019"
_4images_image_id: "9936"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9936 -->
Sieht dem von Robo Explorer ähnlich.