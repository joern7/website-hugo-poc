---
layout: "image"
title: "Rollen unten am Arm"
date: "2007-04-04T10:29:44"
picture: "blinderroboter01.jpg"
weight: "8"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9933
- /details028a.html
imported:
- "2019"
_4images_image_id: "9933"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9933 -->
Diese Rollen dienen zur Stützung des Armes und zur Abgrunderkennung.