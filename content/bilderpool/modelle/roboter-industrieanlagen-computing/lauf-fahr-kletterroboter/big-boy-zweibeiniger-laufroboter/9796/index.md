---
layout: "image"
title: "Schritt mit Gewichtsverlagerung"
date: "2007-03-27T12:30:45"
picture: "131_3188.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9796
- /details40dc.html
imported:
- "2019"
_4images_image_id: "9796"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-27T12:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9796 -->
