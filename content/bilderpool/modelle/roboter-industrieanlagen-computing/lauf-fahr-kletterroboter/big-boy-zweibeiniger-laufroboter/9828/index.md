---
layout: "image"
title: "Mitte"
date: "2007-03-28T09:08:54"
picture: "131_3171.jpg"
weight: "12"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9828
- /details5b27-2.html
imported:
- "2019"
_4images_image_id: "9828"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-28T09:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9828 -->
