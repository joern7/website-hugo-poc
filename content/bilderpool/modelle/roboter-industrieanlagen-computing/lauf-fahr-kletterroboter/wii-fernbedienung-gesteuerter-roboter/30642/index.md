---
layout: "image"
title: "Wii Fernbedienung Roboter"
date: "2011-05-27T22:19:18"
picture: "wiifernbedienunggesteuerterroboter1.jpg"
weight: "1"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/30642
- /detailsa936.html
imported:
- "2019"
_4images_image_id: "30642"
_4images_cat_id: "2284"
_4images_user_id: "1264"
_4images_image_date: "2011-05-27T22:19:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30642 -->
Ein Roboter der über eine Wii Fernbedienung eine IR Quelle findet.
Wenn mein Programm fertig ist werde ich ein Video Hochladen.