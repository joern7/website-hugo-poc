---
layout: "image"
title: "IR Quelle"
date: "2011-05-27T22:19:18"
picture: "wiifernbedienunggesteuerterroboter2.jpg"
weight: "2"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/30643
- /details497c.html
imported:
- "2019"
_4images_image_id: "30643"
_4images_cat_id: "2284"
_4images_user_id: "1264"
_4images_image_date: "2011-05-27T22:19:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30643 -->
Die Spurensensoren funktionieren mit IR Licht das sich auch prima für die Wii Fernbedienung nutzen lässt.