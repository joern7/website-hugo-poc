---
layout: "image"
title: "Nochmal von unten"
date: "2007-02-11T15:17:16"
picture: "classicstileroboter6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8937
- /details18c9-2.html
imported:
- "2019"
_4images_image_id: "8937"
_4images_cat_id: "811"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:17:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8937 -->
