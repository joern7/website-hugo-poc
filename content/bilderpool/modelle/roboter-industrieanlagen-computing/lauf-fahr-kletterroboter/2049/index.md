---
layout: "image"
title: "Samu und Joe"
date: "2004-01-05T22:07:54"
picture: "SamuraivonSiggi.jpg"
weight: "1"
konstrukteure: 
- "NN"
fotografen:
- "NN"
keywords: ["Samurai"]
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2049
- /details25c8.html
imported:
- "2019"
_4images_image_id: "2049"
_4images_cat_id: "579"
_4images_user_id: "5"
_4images_image_date: "2004-01-05T22:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2049 -->
von Siegfried Kloster