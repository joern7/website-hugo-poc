---
layout: "image"
title: "T-rex the Roborama robot 2008 - right side"
date: "2008-12-12T22:54:13"
picture: "IMG_1144.jpg"
weight: "10"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["Roborama", "competition", "robot"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/16595
- /details7aa2.html
imported:
- "2019"
_4images_image_id: "16595"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16595 -->
Using a pneumatic gripper, 2x Ultrasound, 3 x Infra-red, and 2 x line sensors. The software is written in C and can operate both in host and download mode.
On november 8 it takes the 1st place in the Overall category of the Roborama competition.
Clearly visible is the electronics on top. It is an I/O unit with display and rotary encoder.