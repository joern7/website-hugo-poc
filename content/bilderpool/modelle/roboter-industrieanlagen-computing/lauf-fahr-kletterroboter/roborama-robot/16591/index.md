---
layout: "image"
title: "T-rex the Roborama robot 2008"
date: "2008-12-12T22:54:12"
picture: "IMG_1141.jpg"
weight: "8"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["Roborama", "robot", "competition"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/16591
- /details0012-4.html
imported:
- "2019"
_4images_image_id: "16591"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16591 -->
Using a pneumatic gripper, 2x Ultrasound, 3 x Infra-red, and 2 x line sensors. The software is written in C and can operate both in host and download mode.
On november 8 it takes the 1st place in the Overall category of the Roborama competition.
Clearly visible are the compressor and the bag to hold the collected cans.
The electronics on top is an I/O unit with display and rotary encoder.