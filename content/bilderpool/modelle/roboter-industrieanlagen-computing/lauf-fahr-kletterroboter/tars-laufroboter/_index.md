---
layout: "overview"
title: "TARS - Laufroboter"
date: 2020-02-22T08:01:56+01:00
legacy_id:
- /php/categories/3445
- /categoriesc8c3.html
- /categories41ec.html
- /categoriese1d3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3445 --> 
Inspiriert durch die Roboter im Film Interstellar von Christopher Nolan versuchte ich einen Laufroboter zu bauen. Im Gegensatz zum Original hat mein Modell lediglich drei Beine und läuft nicht direkt, sondern bewegt sich durch ständiges "sich aus dem Gleichgewicht bringen" fort. 
Video des Roboters: https://youtu.be/NL_046zrgOs