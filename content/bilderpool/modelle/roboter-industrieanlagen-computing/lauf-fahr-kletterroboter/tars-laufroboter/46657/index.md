---
layout: "image"
title: "Endanschlag eines Beines"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter09.jpg"
weight: "9"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46657
- /detailsa296-2.html
imported:
- "2019"
_4images_image_id: "46657"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46657 -->
In der Mitte des Drehkanzes werden sowohl die Kabel und der Pneumatikschlauch durchgeführt, als auch der Stab an dem der Endschalter befestigt ist und um den sich das Bein dreht.