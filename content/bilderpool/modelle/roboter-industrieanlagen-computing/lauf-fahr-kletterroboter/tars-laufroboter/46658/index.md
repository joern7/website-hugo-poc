---
layout: "image"
title: "Kolbenbefestigung am Bein"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter10.jpg"
weight: "10"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46658
- /details1b4d.html
imported:
- "2019"
_4images_image_id: "46658"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46658 -->
Damit das Ende des Kolbens nicht unten absteht verschiebt die Winkelkombination das Ganze etwas nach oben. Nicht symmetrisch aber funktional. (: