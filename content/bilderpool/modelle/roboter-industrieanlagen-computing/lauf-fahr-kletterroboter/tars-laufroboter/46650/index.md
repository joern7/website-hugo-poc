---
layout: "image"
title: "Gesamtansicht Rückseite"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter02.jpg"
weight: "2"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46650
- /details4056-3.html
imported:
- "2019"
_4images_image_id: "46650"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46650 -->
Auf der Rückseite verläuft der Kabelstrang zu den beiden Beinen bzw. zur Kompressorstation im mittleren Fuß. Der Akku unten ist leider zu schwach um den Laufroboter zu betreiben allerdings wird er benötigt um den Schwerpunkt des Roboters witer nach unten zu verlagern.