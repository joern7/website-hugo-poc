---
layout: "image"
title: "Verkabelung"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter07.jpg"
weight: "7"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46655
- /details2cc3.html
imported:
- "2019"
_4images_image_id: "46655"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46655 -->
In der Mitte sieht man gut wie sich der vom Controller kommende Kabelstrang auf die beiden Beine aufteilt. Außerdem gut zu erkennen sind die beiden Schnecken links und rechts, die durch zwei Zahnräder und einer Kette synchron zueinander gehalten werden. So wird verhindert, dass sich die beiden Beine auf Dauer verziehen (Durch leichte Transportschäden ist der U-Träger in der Mitte dennoch etwas schief)