---
layout: "image"
title: "Motor und Übersetzung der Hebemechanik"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter04.jpg"
weight: "4"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46652
- /details2a2f-2.html
imported:
- "2019"
_4images_image_id: "46652"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46652 -->
Zur rechten: Kabelchaos
Zur linken: Ein neuer Encodermotor mit einer Untersetzung (im Nachhinein wäre wohl doch eine Übersetzung besser gewesen) für das Hebewerk. Unten sieht man auch noch die Kette die die beiden Schnecken auf der gleichen Höhe hält. (Die Motoren laufen durch die Mechanik synchron bzw. werden dazu gezwungen)