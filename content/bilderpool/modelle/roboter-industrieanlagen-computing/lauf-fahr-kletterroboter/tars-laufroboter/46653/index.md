---
layout: "image"
title: "Kompressor und Ventilstation"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter05.jpg"
weight: "5"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46653
- /details9cb8.html
imported:
- "2019"
_4images_image_id: "46653"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46653 -->
Im mittleren Fußende ist die Kompressorstation verbaut. Da der Kompressor für das Umkippen des Roboters eigentlich zu stark ist und sich nur schlecht regeln lässt, habe ich ein Drosselrückschlagventil montiert, welches die Kolben langsamer ausfahren lässt.