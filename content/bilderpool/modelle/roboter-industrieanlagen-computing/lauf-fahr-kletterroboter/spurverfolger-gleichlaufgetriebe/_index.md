---
layout: "overview"
title: "Spurverfolger mit Gleichlaufgetriebe"
date: 2020-02-22T08:01:46+01:00
legacy_id:
- /php/categories/2722
- /categories7999.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2722 --> 
Ein Fahrroboter mit Gleichlaufgetriebe benötigt keine Schritt- oder Encodermotoren für eine saubere Geradeausfahrt und kann punktgenau auf der Stelle drehen.