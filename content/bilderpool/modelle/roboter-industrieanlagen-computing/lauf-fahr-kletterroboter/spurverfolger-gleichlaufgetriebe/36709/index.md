---
layout: "image"
title: "Kompaktes Gleichlaufgetriebe"
date: "2013-03-04T14:09:17"
picture: "spurverfolgermitgleichlaufgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36709
- /detailsff91-2.html
imported:
- "2019"
_4images_image_id: "36709"
_4images_cat_id: "2722"
_4images_user_id: "1126"
_4images_image_date: "2013-03-04T14:09:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36709 -->
Mit zwei klassischen Differentialen lässt sich ein sehr kompaktes und (im Vergleich mit dem Rast-Differential) äußerst reibungsarmes und damit leichtläufiges Gleichlaufgetriebe für einen Spurverfolger konstruieren.