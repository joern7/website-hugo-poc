---
layout: "image"
title: "Von oben"
date: "2014-03-16T12:02:06"
picture: "S1050598.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38460
- /details592b.html
imported:
- "2019"
_4images_image_id: "38460"
_4images_cat_id: "2722"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T12:02:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38460 -->
