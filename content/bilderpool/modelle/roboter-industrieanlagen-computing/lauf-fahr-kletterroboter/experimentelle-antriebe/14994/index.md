---
layout: "image"
title: "defiant_prototyp_3_rad_v2_noservo.jpg"
date: "2008-08-03T18:29:56"
picture: "defiant_prototyp_3_rad_v2_noservo.jpg"
weight: "5"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14994
- /details78c4-2.html
imported:
- "2019"
_4images_image_id: "14994"
_4images_cat_id: "1346"
_4images_user_id: "3"
_4images_image_date: "2008-08-03T18:29:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14994 -->
Die Servos waren leider nur ein kurzer Ausflug: Zurück zu den stabilen Drehkränzen.

Die Positionserfassung wird wahrscheinlich über Neigungsmesser erfolgen.