---
layout: "image"
title: "defiant_prototyp_3_rad_v2_iic.jpg"
date: "2008-07-27T20:25:22"
picture: "defiant_prototyp_3_rad_v2_iic.jpg"
weight: "4"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14965
- /detailsc037.html
imported:
- "2019"
_4images_image_id: "14965"
_4images_cat_id: "1346"
_4images_user_id: "3"
_4images_image_date: "2008-07-27T20:25:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14965 -->
Die ft Gummireifen haben jetzt schon unter dem Gewicht nachgegeben.
Also habe ich sie ausgetauscht gegen die älteren Reifen aus dem Start 100.

Die Steuerung, ein Spartan3e FPGA mit Microblaze softcore, gibt jetzt die Position für die Servos über I2C vor.

Die Servos sind leider schon jetzt ziemlich beschäftigt.