---
layout: "image"
title: "Omniwheel"
date: "2012-09-08T15:23:52"
picture: "omnidirektionalesfahrwerk1.jpg"
weight: "1"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35462
- /detailsa1e8.html
imported:
- "2019"
_4images_image_id: "35462"
_4images_cat_id: "2629"
_4images_user_id: "1196"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35462 -->
Die kleinen Rollen ermöglichen es dem Rad sich zu dem Seiten zu bewegen. Damit lassen sich omnidirektionale Antriebe bauen.