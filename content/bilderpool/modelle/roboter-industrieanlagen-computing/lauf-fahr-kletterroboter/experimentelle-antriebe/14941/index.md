---
layout: "image"
title: "defiant_prototyp_3_rad_v2_alu.jpg"
date: "2008-07-21T18:13:29"
picture: "defiant_prototyp_3_rad_v2_alu.jpg"
weight: "3"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14941
- /detailsab51.html
imported:
- "2019"
_4images_image_id: "14941"
_4images_cat_id: "1346"
_4images_user_id: "3"
_4images_image_date: "2008-07-21T18:13:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14941 -->
Alu Alu Alu! - Es geht nichts über Stabilität.