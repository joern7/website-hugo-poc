---
layout: "image"
title: "Unten"
date: "2010-05-01T14:59:01"
picture: "superflacherroboter5.jpg"
weight: "5"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- /php/details/27027
- /details1fe4.html
imported:
- "2019"
_4images_image_id: "27027"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27027 -->
Hier erkennt man die motoren und nochmal das drehrad.