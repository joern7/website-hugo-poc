---
layout: "image"
title: "Das hintere drehrad"
date: "2010-05-01T14:59:00"
picture: "superflacherroboter1.jpg"
weight: "1"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- /php/details/27023
- /detailsc751.html
imported:
- "2019"
_4images_image_id: "27023"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27023 -->
Hier seht ihr das drehrad hinten. Das ist aufgebaut wie das aus den robo mobileset, nur die platte ist anders. Die ist um 90 grad gedreht.