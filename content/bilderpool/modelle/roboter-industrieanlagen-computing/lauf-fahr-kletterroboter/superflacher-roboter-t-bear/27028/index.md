---
layout: "image"
title: "Vorne"
date: "2010-05-01T14:59:01"
picture: "superflacherroboter6.jpg"
weight: "6"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- /php/details/27028
- /details52f2-2.html
imported:
- "2019"
_4images_image_id: "27028"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27028 -->
Der roboter von vorne. Man sieht gut die beiden fototransistoren.