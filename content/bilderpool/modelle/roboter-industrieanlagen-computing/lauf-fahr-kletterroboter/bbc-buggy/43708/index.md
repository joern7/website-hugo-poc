---
layout: "image"
title: "Obere Hälfte vom Display mit Messwerten der Sensoren"
date: "2016-06-08T18:14:50"
picture: "Display_oben.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "Labyrinth", "Grauwerte", "Messwerte", "Display"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43708
- /details6aad-2.html
imported:
- "2019"
_4images_image_id: "43708"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43708 -->
Die analogen Messwerte der Liniensensorelemente werden hier als rechteckige Flächen mit Grauwerten dargestellt.