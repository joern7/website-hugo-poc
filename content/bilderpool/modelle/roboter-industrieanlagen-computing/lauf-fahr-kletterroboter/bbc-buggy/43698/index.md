---
layout: "image"
title: "Frontansicht"
date: "2016-06-08T15:17:13"
picture: "Buggy1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "1983", "Labyrinth", "Maze", "Linien"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43698
- /detailsd013.html
imported:
- "2019"
_4images_image_id: "43698"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T15:17:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43698 -->
Roland Enzenhofer hat den mechanischen Aufbau erstellt.