---
layout: "image"
title: "Kugel-Lager aus Rolands 3D Drucker"
date: "2016-06-08T18:14:50"
picture: "Buggy6.jpg"
weight: "6"
konstrukteure: 
- "Roland Enzenhofer"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Linien"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43703
- /detailsab76.html
imported:
- "2019"
_4images_image_id: "43703"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43703 -->
Hinten rollt der Buggy auf einer Kugel. Dies Spezialteil hat Roland auf seinem 3D Drucker angefretigt.