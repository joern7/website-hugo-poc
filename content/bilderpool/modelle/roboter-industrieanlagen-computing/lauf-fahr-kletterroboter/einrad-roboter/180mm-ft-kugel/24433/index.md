---
layout: "image"
title: "180mm-FT-Kugel"
date: "2009-06-23T07:42:22"
picture: "180mm-Kugel_004.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24433
- /details3dbd.html
imported:
- "2019"
_4images_image_id: "24433"
_4images_cat_id: "1675"
_4images_user_id: "22"
_4images_image_date: "2009-06-23T07:42:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24433 -->
