---
layout: "image"
title: "Sensormessungen"
date: "2009-04-14T18:57:53"
picture: "messungen1.jpg"
weight: "7"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/23715
- /detailsdea9.html
imported:
- "2019"
_4images_image_id: "23715"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-04-14T18:57:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23715 -->
Hier habe ich einen Stab einfach umfallen lassen und dabei die Sensoren ausgelesen.
Blau ist das Signal von einem Potientiometer, ähnlich befestigt wie beim Invertierten Pendel, und entspricht dem Winkel des Stabes.
Gelb ist der Beschleunigungssensor, rot der Gyro.
 Den Stab habe ich erst möglichst gerade hingestellt und dann kippen lassen. Am blauen Graphen sieht man sehr gut in welchem Winkel der Stab gerade steht. Auch das Signal des Gyros ist in Ordnung: Der Stab fängt langsam an zu kippen und wird dann schneller. 
Nur der Beschleunigungssensor liefert merkwürdige Daten: sein Signal ändert sich überhaupt nicht, obwohl der Sensor funktioniert. Ich vermute das es daran liegt, dass sich die Erdbeschleunigung und die Fallbeschleunigung des Stabes genau aufheben. Der Sensor liefert gute Werte, wenn man den Stab nicht umkippen lässt, sondern langsam neigt -> weniger bis gar keine Fallbeschleunigung.