---
layout: "image"
title: "Sensormessungen"
date: "2009-04-14T18:57:54"
picture: "messungen2.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/23716
- /details050b.html
imported:
- "2019"
_4images_image_id: "23716"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-04-14T18:57:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23716 -->
Derselbe Versuch, allerdings befindet sich der Beschleunigungssensor jetzt sehr weit unten am Stab, nahe am Drehpunkt. Dadurch wrikt fast nur noch die Erdbeschleunigung auf ihn und er liefert wieder sinnvolle Werte. Vermutlich funktioniert deshalb auch mein Kugelroboter nicht so gut. Funktionisbedingt sind die Sensoren sehr weit oben, besser wäre es wenn sie IN dem Ball wären, das habe ich aber noch nicht geschafft ;-)