---
layout: "image"
title: "Kugelroboter - vorne links"
date: "2009-07-23T18:00:52"
picture: "kugelroboter1_2.jpg"
weight: "13"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/24667
- /details0317-2.html
imported:
- "2019"
_4images_image_id: "24667"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-23T18:00:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24667 -->
Die neue Version des Kugelroboters. Eigentlich nicht ganz richtig hier, da er nicht aus fischertechnik gebaut ist. Funktionieren tut er grundsätzlich wie der alte, nur eben besser. Das liegt daran, dass er stabiler ist und deutlich bessere Gyros hat.