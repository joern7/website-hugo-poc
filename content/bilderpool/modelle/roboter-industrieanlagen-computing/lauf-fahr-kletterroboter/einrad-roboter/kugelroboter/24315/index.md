---
layout: "image"
title: "Zini"
date: "2009-06-12T19:41:10"
picture: "kugel_800x800.jpg"
weight: "9"
konstrukteure: 
- "Fitzcarraldo"
fotografen:
- "Fitzcarraldo"
keywords: ["Kugelroboter"]
uploadBy: "Fitzcarraldo"
license: "unknown"
legacy_id:
- /php/details/24315
- /detailsc3bf-2.html
imported:
- "2019"
_4images_image_id: "24315"
_4images_cat_id: "1500"
_4images_user_id: "971"
_4images_image_date: "2009-06-12T19:41:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24315 -->
Dies ist Zini - meine ferngesteuerte Kugel!

1 x kleiner Motor
1 x großer Motor
1 x Controlset

16 cm
962 g