---
layout: "image"
title: "von unten"
date: "2008-12-10T16:45:53"
picture: "kugelroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/16576
- /detailsb20d.html
imported:
- "2019"
_4images_image_id: "16576"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2008-12-10T16:45:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16576 -->
In der Mitte der neue Motortreiber. Das ist jetzt schon der dritte. Die L293/L298 hatten mir alle viel zu viel Spannungsabfall, mit den Fets haben die Motoren jetzt genug Kraft.
Es ist übrigens gar nicht so einfach mit fischertechnik eine stabile, dreieckige Konstruktion zu bauen.