---
layout: "image"
title: "Kugelroboter - Fernsteuerung"
date: "2009-07-24T17:57:04"
picture: "fernsteuerung1.jpg"
weight: "17"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Jan Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/24676
- /details1f3c-2.html
imported:
- "2019"
_4images_image_id: "24676"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-24T17:57:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24676 -->
Im wesentlichen besteht die Fernsteuerung aus einem Grafik-Display. (Pollin 120 346). Auch wenn es da nicht steht: das Display hat eine Touch-Folie. Damit kann man sehr gut den Roboter bedienen. Vom Hauptmenü aus kann man verschiedene Funktionen aufrufen:
Sensoren:
Eine Art Mini-Oszilloskop. Die Werte der Sensoren des Roboters werden grafisch dargestellt und gleichzeitig über die RS232-Schnittstelle an den PC gesendet.

Fahren:
Direkte Ansteuerungsmöglichkeit für die Motoren. Z.B um diese zu testen oder einfach nur zu zeigen, dass der Roboter in jede Richtung fahren kann.

Regler:
Die Regelparameter des PID-Reglers können verändert werden. Sie werden auf dem Roboter gespeichert, nicht in der Fernsteuerung.

Start:
Balancieren starten.