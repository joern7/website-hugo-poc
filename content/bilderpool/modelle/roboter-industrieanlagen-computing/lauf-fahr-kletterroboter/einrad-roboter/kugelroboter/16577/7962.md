---
layout: "comment"
hidden: true
title: "7962"
date: "2008-12-10T18:51:37"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wenn Du das wirklich zum Laufen bringst: SA-GEN-HAFT! Einfach nur irre, was Du in Deinem Alter für Aufgaben bezwingst.

Tief beeindruckt,
Stefan