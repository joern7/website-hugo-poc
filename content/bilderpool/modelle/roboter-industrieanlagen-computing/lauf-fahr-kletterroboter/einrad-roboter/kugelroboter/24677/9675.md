---
layout: "comment"
hidden: true
title: "9675"
date: "2009-07-28T17:25:10"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Richtig, ist geätzt.
Das Funkmodul ist von CSD-Electronics. Kostet dort nur 4 Euro, bei Pollin 6. Kurzzeitig wars dort ausverkauft, gibt es jetzt aber wieder, nur der Quarz ist noch kleiner geworden. 
Best-Nr.3040915 (http://www.csd-electronics.de/de/index.htm)