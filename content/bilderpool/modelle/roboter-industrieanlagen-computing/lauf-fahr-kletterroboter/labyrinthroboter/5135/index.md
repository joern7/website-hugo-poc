---
layout: "image"
title: "Akku darunter"
date: "2005-10-29T17:25:36"
picture: "13-mit_Akku_darunter.jpg"
weight: "13"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5135
- /details8924.html
imported:
- "2019"
_4images_image_id: "5135"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5135 -->
Der Akku hat bei 12 Volt volle 4 Ah. Das sollte für ca. 5 Stunden Betrieb reichen.

Der Rechner nimmt sich schon einen ganz ordentlichen Schluck aus der Pulle und jetzt kommen noch die Sensoren dazu. Der Stromverbrauch dürfte erheblich werden, so daß die Antriebsmotoren gar nicht mehr sonderlich ins Gewicht fallen.