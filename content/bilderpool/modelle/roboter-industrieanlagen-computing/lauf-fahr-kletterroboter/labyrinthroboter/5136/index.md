---
layout: "image"
title: "Multi-IO-Board"
date: "2005-10-29T17:25:37"
picture: "14-mit_Multi-IO-Board.jpg"
weight: "14"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5136
- /detailsd986-5.html
imported:
- "2019"
_4images_image_id: "5136"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5136 -->
Die Basis des Wahrnehmungsapparats für den kleinen Roboter. Damit hat er 4 Digitalausgänge, 3 Digitaleingänge, 8 Analogeingänge (12bit), ein dreifach 16-bit programmierbarer Zählerbaustein und die Interruptleitung zur Verfügung.