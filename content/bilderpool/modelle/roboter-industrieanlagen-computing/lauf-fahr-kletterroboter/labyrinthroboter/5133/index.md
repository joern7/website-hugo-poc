---
layout: "image"
title: "Dritter Versuch"
date: "2005-10-29T17:25:36"
picture: "11-Dritter_Versuch.jpg"
weight: "11"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5133
- /details8d95-2.html
imported:
- "2019"
_4images_image_id: "5133"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5133 -->
Das vorige Chassis war einfach zu lang, zeigte aber schon den benötigten Stauraum.

Hier jetzt ein wiederum dreirädriger Versuch, weil er ohne Federung und ohne Differentialgetriebe auskommt. Außerdem hat die Lenkachse jetzt auch einen Schleifring für die Stromübergabe auf die Antriebsachse.