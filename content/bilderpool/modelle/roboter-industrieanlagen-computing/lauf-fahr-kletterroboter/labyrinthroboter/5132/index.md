---
layout: "image"
title: "unverkabelte Ausrüstung"
date: "2005-10-29T17:25:15"
picture: "10-unverkabelte_Ausrstung.jpg"
weight: "10"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5132
- /detailsab16.html
imported:
- "2019"
_4images_image_id: "5132"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5132 -->
Mit Elektronik an Bord wirkt die Sache schon fast echt. Aber es fehlen noch alle Kabel, Bedienelemente und Sensoren.

Diese Version bringt jetzt schon 1,3 kg auf die Waage.