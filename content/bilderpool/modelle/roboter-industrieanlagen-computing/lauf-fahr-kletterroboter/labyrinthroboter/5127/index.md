---
layout: "image"
title: "Zweiter Versuch"
date: "2005-10-29T17:25:15"
picture: "05-Zweiter_Versuch.jpg"
weight: "5"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5127
- /detailsec3f.html
imported:
- "2019"
_4images_image_id: "5127"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5127 -->
Dieses Chassis ist schon erheblich größer. Es hat jetzt auch vier Räder.