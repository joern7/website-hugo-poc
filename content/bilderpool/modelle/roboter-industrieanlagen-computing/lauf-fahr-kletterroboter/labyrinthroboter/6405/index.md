---
layout: "image"
title: "Labyrinthroboter - Seitenansicht"
date: "2006-06-02T11:33:39"
picture: "Labyrinthroboter3.jpg"
weight: "23"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/6405
- /detailsa16d-3.html
imported:
- "2019"
_4images_image_id: "6405"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-06-02T11:33:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6405 -->
Hier die Sicht glatt von der Seite zeigt die genauen Abmessungen und die Geometrie.