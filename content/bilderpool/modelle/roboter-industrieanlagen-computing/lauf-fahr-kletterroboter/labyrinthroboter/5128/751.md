---
layout: "comment"
hidden: true
title: "751"
date: "2005-10-31T05:41:03"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Problem?"Bleibt als Problem nur die Stromzufuhr zum Antriebsmotor." - Der Antriebsmotor ist doch der schwarze da oben drauf, oder nicht? Für mich sieht das so aus, als ob die Antriebswelle nach Art der BMW-"Königswelle" senkrecht nach unten, durch den Drehkranz hindurch, auf das Differenzial führen würde.