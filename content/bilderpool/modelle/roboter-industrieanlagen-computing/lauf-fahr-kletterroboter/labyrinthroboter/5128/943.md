---
layout: "comment"
hidden: true
title: "943"
date: "2006-03-25T10:44:12"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Aber wo soll denn dieses Drehmoment herkommen? Wenn man die Vorderachse im Stand dreht, dann dreht, dank Drehschemel, ein Rad rückwärts und das andere genausoweit vorwärts. Im Differenzial addieren sich die Drehwinkel zu Null, und damit ist's doch gut.