---
layout: "image"
title: "Wendemanöver"
date: "2005-10-29T17:25:15"
picture: "03-Wendemanver.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5125
- /details2919-2.html
imported:
- "2019"
_4images_image_id: "5125"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5125 -->
Mit um 90 Grad eingelenkter Vorderachse kann der kleine Roboter fast auf der Stelle drehen.