---
layout: "image"
title: "Teilscheibe am Hinterrad"
date: "2006-02-11T16:06:28"
picture: "Teilscheibe_am_Hinterrad.jpg"
weight: "20"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5757
- /details6701-2.html
imported:
- "2019"
_4images_image_id: "5757"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T16:06:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5757 -->
Kleiner Fortschritt für die Odometrie. Der Reifen ist mit einem sehr präzisen O-Ring ausgerüstet worden und rollt jetzt perfekt ab. Dazu kommt die Teilscheibe, hier mit 160 Strich Auflösung. Zusammen mit dem Quadratur-Dekoder ergibt das 640 Impulse je Umdrehung. Der Raddurchmesser ist exakt 2 Zoll oder 50,8 mm, so daß die Auflösung des Fahrwegs 0,25 mm erreicht.

Ein guter Plan. Klappen muß er nur noch.