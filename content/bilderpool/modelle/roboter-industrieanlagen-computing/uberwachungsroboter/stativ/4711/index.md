---
layout: "image"
title: "Ständer_3"
date: "2005-08-31T19:45:00"
picture: "E-Modelle_003.jpg"
weight: "10"
konstrukteure: 
- "ffcoe"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4711
- /details6f7a-2.html
imported:
- "2019"
_4images_image_id: "4711"
_4images_cat_id: "380"
_4images_user_id: "332"
_4images_image_date: "2005-08-31T19:45:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4711 -->
Und man kann den Testständer sehr schön als kleinen Tisch verwenden.