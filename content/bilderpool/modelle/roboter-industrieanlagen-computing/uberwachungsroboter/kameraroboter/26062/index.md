---
layout: "image"
title: "Detail4"
date: "2010-01-11T18:19:57"
picture: "kameraroboter08.jpg"
weight: "8"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26062
- /detailsacde.html
imported:
- "2019"
_4images_image_id: "26062"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26062 -->
