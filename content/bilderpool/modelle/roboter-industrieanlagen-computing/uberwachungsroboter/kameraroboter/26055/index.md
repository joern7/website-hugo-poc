---
layout: "image"
title: "von vorne"
date: "2010-01-11T18:19:56"
picture: "kameraroboter01.jpg"
weight: "1"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26055
- /detailsceb4.html
imported:
- "2019"
_4images_image_id: "26055"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26055 -->
