---
layout: "image"
title: "Steuerung1"
date: "2010-01-11T18:20:07"
picture: "kameraroboter11.jpg"
weight: "11"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26065
- /details5f62.html
imported:
- "2019"
_4images_image_id: "26065"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:20:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26065 -->
