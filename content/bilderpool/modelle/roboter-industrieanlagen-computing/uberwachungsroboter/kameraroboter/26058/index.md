---
layout: "image"
title: "Detail2"
date: "2010-01-11T18:19:56"
picture: "kameraroboter04.jpg"
weight: "4"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26058
- /detailsf524.html
imported:
- "2019"
_4images_image_id: "26058"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26058 -->
