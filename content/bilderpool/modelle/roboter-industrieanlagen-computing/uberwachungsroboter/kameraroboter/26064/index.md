---
layout: "image"
title: "Detail6"
date: "2010-01-11T18:19:57"
picture: "kameraroboter10.jpg"
weight: "10"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26064
- /details097e.html
imported:
- "2019"
_4images_image_id: "26064"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26064 -->
