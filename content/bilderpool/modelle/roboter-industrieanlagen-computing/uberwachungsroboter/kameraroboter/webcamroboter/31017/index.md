---
layout: "image"
title: "Gelenk"
date: "2011-07-09T15:57:38"
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31017
- /details079a-2.html
imported:
- "2019"
_4images_image_id: "31017"
_4images_cat_id: "2317"
_4images_user_id: "1218"
_4images_image_date: "2011-07-09T15:57:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31017 -->
Hier das gespiegelte Gelenk. Mit einem Strohalm wird das Einzwicken der Kabel verhindert