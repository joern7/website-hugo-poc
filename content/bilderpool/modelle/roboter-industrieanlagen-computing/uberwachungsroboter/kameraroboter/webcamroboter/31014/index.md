---
layout: "image"
title: "Roboter von seitlich unten"
date: "2011-07-09T15:57:38"
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31014
- /details823c.html
imported:
- "2019"
_4images_image_id: "31014"
_4images_cat_id: "2317"
_4images_user_id: "1218"
_4images_image_date: "2011-07-09T15:57:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31014 -->
Dieser Webcamroboter lässt sich von überall (weltweit) über php steuern.
Man sieht, dass er an der Decke aufgehängt ist.