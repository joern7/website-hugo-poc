---
layout: "image"
title: "Standfläche"
date: "2011-12-23T19:30:21"
picture: "spycam13.jpg"
weight: "13"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33753
- /details12a8.html
imported:
- "2019"
_4images_image_id: "33753"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33753 -->
Das selbstgebaute Gestell auf dem SpyCam ruht.