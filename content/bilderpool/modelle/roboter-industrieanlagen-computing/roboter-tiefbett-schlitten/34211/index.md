---
layout: "image"
title: "Roboter-Tiefbett-Schlitten-04"
date: "2012-02-18T13:28:17"
picture: "Schlitten-04.jpg"
weight: "4"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34211
- /detailsfc37.html
imported:
- "2019"
_4images_image_id: "34211"
_4images_cat_id: "2534"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34211 -->
hier ist das Ganze einfach von unten zu sehen. Die grauen Bausteine führen den Schlitten innerhalb der Alu-Profile. So ist ein verkanten nicht möglich.