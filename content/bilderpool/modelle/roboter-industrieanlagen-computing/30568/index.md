---
layout: "image"
title: "Original Model Program"
date: "2011-05-14T23:07:09"
picture: "IMG_0003.jpg"
weight: "26"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30568
- /details2173.html
imported:
- "2019"
_4images_image_id: "30568"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T23:07:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30568 -->
An exemple of the language to drive the original model.
I beleive it is "Assembly Language"

Ein Beispiel für die Sprache zu fahren das ursprüngliche Modell.
 Ich glaube, es ist "Assembly Language"