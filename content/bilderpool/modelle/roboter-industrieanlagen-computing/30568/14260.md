---
layout: "comment"
hidden: true
title: "14260"
date: "2011-05-15T13:50:52"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
This is C64-Basic. 
It is mixed with a machine-code / Assembler  "driver". The functions of the driver are called with the "SYS" statements.

Dies ist C64-Basic. 
Im Programmcode sieht man die Aufrufe eines "Treibers", der offenbar in Maschinencode (Assembler) vorliegt, das ist zu sehen an den "SYS" - Befehlen.