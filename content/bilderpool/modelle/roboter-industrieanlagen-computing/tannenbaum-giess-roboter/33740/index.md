---
layout: "image"
title: "Das Programm"
date: "2011-12-23T19:30:21"
picture: "Programm.jpg"
weight: "6"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/33740
- /details9fb6.html
imported:
- "2019"
_4images_image_id: "33740"
_4images_cat_id: "2495"
_4images_user_id: "986"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33740 -->
Das Programm ist extrem simpel. Das Interface frägt den digitalen Eingang 1 ab. Wenn dieser auf low ist, wird so lange gepumpt, bis Eingang 1 wieder auf high ist. Dann stoppt die Pumpe und geht dann einen Tag in den "Ruhemodus", um "Milliliterpumpen" (gibts da 'n Fachbegriff für?) zu vermeiden