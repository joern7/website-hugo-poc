---
layout: "image"
title: "Buggy in grau"
date: 2020-08-17T14:40:56+02:00
picture: "P1000910.JPG"
weight: "1"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man den Classic-Buggy in seiner vollen Größe. Zwei Punkte sind zu beachten:
* Die Steckerleiste auf dem Motorshield wurde erst nachträglich aufgelötet. Daher sitzt sie in der zweiten Reihe.
* Bei meinem Motorshield funktioniert der Ausgang M2 nicht. Daher ist der 2. Motor an M3 angeschlossen. Das Skript habe ich entsprechend angepasst.
