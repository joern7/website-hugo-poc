---
layout: "image"
title: "Ansicht von unten"
date: 2020-08-17T14:40:53+02:00
picture: "P1000913.JPG"
weight: "4"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die ganze Konstruktion ist in der Ansicht von unten nachvollziehbar.