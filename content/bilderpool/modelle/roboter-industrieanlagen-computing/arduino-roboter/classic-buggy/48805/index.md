---
layout: "image"
title: "Nachlaufrad"
date: 2020-08-17T14:40:54+02:00
picture: "P1000912.JPG"
weight: "3"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Für das Nachlaufrad aus grauen Teilen fehlte mir die zündende Idee, um die Radachse senkrecht und drehbar mit dem Chassis zu verbinden. Daher habe ich zu einem Teil aus dem 3D-Drucker gegriffen.