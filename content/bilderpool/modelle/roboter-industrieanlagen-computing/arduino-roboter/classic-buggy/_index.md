---
layout: "overview"
title: "Classic-Buggy 
"
date: 2020-08-17T14:40:52+02:00
---

 Gelgegentlich kommt die Anfrage, ob man denn mit den alten grauen Bauteilen überhaupt noch etwas anfangen kann.
 Andere Fans wollen mit dem Bau von Modellen zum Buch nicht auf den Kasten dazu warten müssen.
 Daher gibt es hier ein Beispiel, wie ein Arduino-gesteuerter Buggy aus Classic-Bausteinen aussehen kann.
 
 Verwendet habe ich einen [Grundkasten 300](https://ft-datenbank.de/ft-article/1968), 
 2 [Minimots](https://ft-datenbank.de/ft-article/2906), 
 3 alte [Taster](https://ft-datenbank.de/ft-article/4299), 
 eine [Community-Akkubox](https://ftcommunity.de/knowhow/bauanleitungen/akkubox/) 
 und ein [Nachlaufrad](https://www.thingiverse.com/thing:3849443) aus dem 3D-Drucker.
