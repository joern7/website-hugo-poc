---
layout: "image"
title: "Infrarot Programmbeispiel"
date: "2009-01-07T21:22:46"
picture: "Infrarot.jpg"
weight: "2"
konstrukteure: 
- "Lysander Blote  Software Fischertechnik"
fotografen:
- "Lysander Blote"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lysander Blote"
license: "unknown"
legacy_id:
- /php/details/16942
- /details806d.html
imported:
- "2019"
_4images_image_id: "16942"
_4images_cat_id: "1553"
_4images_user_id: "805"
_4images_image_date: "2009-01-07T21:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16942 -->
Das ist ein kleines Programm das wie der
Empfänger von altem Controlset arbeitert.
Hier wird nur 1 Motor angesteuert aber
zur demonstration reicht es.