---
layout: "image"
title: "Automat4"
date: "2009-02-03T15:20:35"
picture: "prog4.jpg"
weight: "6"
konstrukteure: 
- "Niklas"
fotografen:
- "Niklas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "conradelectric2"
license: "unknown"
legacy_id:
- /php/details/17299
- /detailse47e.html
imported:
- "2019"
_4images_image_id: "17299"
_4images_cat_id: "1553"
_4images_user_id: "911"
_4images_image_date: "2009-02-03T15:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17299 -->
