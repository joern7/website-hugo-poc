---
layout: "image"
title: "Bedienfeld"
date: "2009-05-24T19:31:40"
picture: "rpm3.jpg"
weight: "3"
konstrukteure: 
- "Kacker303"
fotografen:
- "Kacker303"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kacker303"
license: "unknown"
legacy_id:
- /php/details/24102
- /details25aa.html
imported:
- "2019"
_4images_image_id: "24102"
_4images_cat_id: "1652"
_4images_user_id: "924"
_4images_image_date: "2009-05-24T19:31:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24102 -->
Das Bedienfeld zur Einstellung von Daten wie Impulsanzahl und Messzeit und der Anzeige von der Umdrehungszahl.