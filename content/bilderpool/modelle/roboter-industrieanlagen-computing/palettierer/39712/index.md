---
layout: "image"
title: "Palettierer mit geschlossenem Fallboden"
date: "2014-10-20T21:59:38"
picture: "2_-_Palettierer_mit_geschlossenem_Fallboden.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39712
- /details595e.html
imported:
- "2019"
_4images_image_id: "39712"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39712 -->
