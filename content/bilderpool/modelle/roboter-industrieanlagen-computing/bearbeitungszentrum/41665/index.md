---
layout: "image"
title: "Scanner"
date: "2015-07-31T11:29:50"
picture: "bzmfr07.jpg"
weight: "7"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41665
- /details9cbc.html
imported:
- "2019"
_4images_image_id: "41665"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41665 -->
Scanner bestimmt die Fabe des Werkstücks:
- je nach Farbe wird dieses unterschiedlich bearbeitet
- je nach Farbe wird es nach der Bearbeitung in das passende Fach gelegt