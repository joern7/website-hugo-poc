---
layout: "image"
title: "Gesamtansicht"
date: "2015-07-31T11:29:50"
picture: "bzmfr01.jpg"
weight: "1"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41659
- /detailsdf40-2.html
imported:
- "2019"
_4images_image_id: "41659"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41659 -->
Ansicht auf die Anlage, alle Komponenten befinden sich bei ihrem Referenzpunkt