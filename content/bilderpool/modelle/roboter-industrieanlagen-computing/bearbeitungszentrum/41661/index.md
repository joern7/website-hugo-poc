---
layout: "image"
title: "Ansicht von oben"
date: "2015-07-31T11:29:50"
picture: "bzmfr03.jpg"
weight: "3"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41661
- /details6c08.html
imported:
- "2019"
_4images_image_id: "41661"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41661 -->
zeigt den Aufbau der Anlage von oben