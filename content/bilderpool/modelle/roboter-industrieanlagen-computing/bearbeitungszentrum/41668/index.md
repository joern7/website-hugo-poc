---
layout: "image"
title: "Statusdisplay"
date: "2015-07-31T11:29:50"
picture: "bzmfr10.jpg"
weight: "10"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: ["RoboPro", "Statusdisplay", "Bedienfeld"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41668
- /details711a.html
imported:
- "2019"
_4images_image_id: "41668"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41668 -->
Umfangreiches Statusdisplay:
- zeigt den aktuellen Bearbeitungsprozess an
- zeigt Aktivität von Komponenten wie z.B Kompressor an
- Fehlerdisplay
- Einstellungsmöglichkeiten bei der Bearbeitung (je nach Farbe des Werkstücks):
    - Dauer des Schweißens
    - anpassbarer Stanzdruck, dieser wird simuliert, indem das Magnetventil unterschiedlich lang geöffnet wird
    - Anzahl Stanzdurchläufe sind einstellbar