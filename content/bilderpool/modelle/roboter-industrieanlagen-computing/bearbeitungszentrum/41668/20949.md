---
layout: "comment"
hidden: true
title: "20949"
date: "2015-07-31T11:34:08"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Super sauber gebaut, gratuliere. Kannst Du noch was zu den Roboterarm-Berechnungen sagen? Machst Du da inverse Kinematik in RoboPro?
Gruß,
Stefan