---
layout: "image"
title: "Stanzpresse"
date: "2015-07-31T11:29:50"
picture: "bzmfr09.jpg"
weight: "9"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41667
- /detailsd603.html
imported:
- "2019"
_4images_image_id: "41667"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41667 -->
über Hubgetriebe vor- / zurückfahrbar