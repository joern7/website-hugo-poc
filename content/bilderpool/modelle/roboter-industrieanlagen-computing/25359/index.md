---
layout: "image"
title: "Walking Robot"
date: "2009-09-26T00:18:20"
picture: "sm_walker_1b.jpg"
weight: "11"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["walker", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25359
- /details21e4.html
imported:
- "2019"
_4images_image_id: "25359"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-26T00:18:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25359 -->
We are working on a series of walking robots integrating the PCS BRAIN.