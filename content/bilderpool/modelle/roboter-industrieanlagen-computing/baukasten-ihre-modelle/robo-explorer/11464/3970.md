---
layout: "comment"
hidden: true
title: "3970"
date: "2007-09-16T07:51:56"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Hammer! Vor allem die schwarzen Kettenglieder. Die Sensoren natürlich sowieso. Aber eine Frage: Ist da nur ein Distanzsensor dabei? Frederik hat mir erklärt das wäre nur einer. Ein Kreis zum Sensden und ein Kreis zum Empfangen. Dann könnte man doch gar keine Hindernisse rechts/links erkennen, nur geradeaus.

Gruß fitec