---
layout: "image"
title: "..."
date: "2009-06-14T09:40:01"
picture: "DSC_2310.jpg"
weight: "9"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24353
- /details1c03.html
imported:
- "2019"
_4images_image_id: "24353"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24353 -->
