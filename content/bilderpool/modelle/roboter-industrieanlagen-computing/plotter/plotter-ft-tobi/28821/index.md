---
layout: "image"
title: "Der Antrieb der Y-Achse (1)"
date: "2010-10-02T23:55:11"
picture: "plotter2.jpg"
weight: "2"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- /php/details/28821
- /details6a6d-2.html
imported:
- "2019"
_4images_image_id: "28821"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28821 -->
