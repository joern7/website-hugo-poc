---
layout: "image"
title: "Gesamtansicht des Plotters"
date: "2010-10-02T23:55:11"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- /php/details/28820
- /detailsc27f.html
imported:
- "2019"
_4images_image_id: "28820"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28820 -->
Der Stift wird nur parallel zur y-Achse bewegt. Um auch in x-Richtung zeichnen zu können, wird zusätzlich das Papier bewegt, das beim Zeichnen auf der grauen Grundplatte liegt.