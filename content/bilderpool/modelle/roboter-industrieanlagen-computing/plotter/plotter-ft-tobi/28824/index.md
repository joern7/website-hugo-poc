---
layout: "image"
title: "Der Schreibkopf"
date: "2010-10-02T23:55:12"
picture: "plotter5.jpg"
weight: "5"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- /php/details/28824
- /details766b.html
imported:
- "2019"
_4images_image_id: "28824"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28824 -->
