---
layout: "image"
title: "Die Stifthalterung"
date: "2010-10-02T23:55:12"
picture: "plotter6.jpg"
weight: "6"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- /php/details/28825
- /details0a8b.html
imported:
- "2019"
_4images_image_id: "28825"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28825 -->
