---
layout: "image"
title: "Portalroboter"
date: "2006-11-04T21:25:07"
picture: "IMG_0167.jpg"
weight: "1"
konstrukteure: 
- "Frank Walter"
fotografen:
- "Frank Walter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "The Specialist"
license: "unknown"
legacy_id:
- /php/details/7313
- /detailsc97b.html
imported:
- "2019"
_4images_image_id: "7313"
_4images_cat_id: "1600"
_4images_user_id: "496"
_4images_image_date: "2006-11-04T21:25:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7313 -->
