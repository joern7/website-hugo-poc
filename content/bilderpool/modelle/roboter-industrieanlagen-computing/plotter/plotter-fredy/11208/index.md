---
layout: "image"
title: "Antrieb"
date: "2007-07-23T12:03:13"
picture: "plotter05.jpg"
weight: "10"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11208
- /detailsf6f6.html
imported:
- "2019"
_4images_image_id: "11208"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11208 -->
Dieser Schrittmotor treib die Achse mit den Papiereinzugsrollen an.