---
layout: "image"
title: "Geamtansicht einer Achse"
date: "2007-04-30T13:23:03"
picture: "plotter2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10234
- /detailsc973-3.html
imported:
- "2019"
_4images_image_id: "10234"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-04-30T13:23:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10234 -->
Wenn der Plotter irgendwann mal fertig ist möchte ich damit DIN A4 Blätter beplotten...