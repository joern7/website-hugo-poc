---
layout: "image"
title: "Einstellung"
date: "2007-07-23T12:07:00"
picture: "plotter11.jpg"
weight: "16"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11214
- /details60a1-3.html
imported:
- "2019"
_4images_image_id: "11214"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:07:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11214 -->
An dem roten 32064 kann man einstellen wie dick das Papier ist damit es von den Papiereinzugsrollen bewegt werden kann. Den Baustein kann hoch und runter geschoben werden dadurch ändert sich der abstand zwischen den Rollen und das Papier passt Perfekt dazwischen. Das ganze ist natürlich auf beiden Seiten eingebaut;-)

Im Hintergrund könnt ihr sehen wie das Papier zwischen die beiden Rollen liegt.

