---
layout: "image"
title: "Papiereinzugsrollen"
date: "2007-07-23T12:03:13"
picture: "plotter08.jpg"
weight: "13"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11211
- /details1b2b.html
imported:
- "2019"
_4images_image_id: "11211"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11211 -->
Die obere Rolle ist angetrieben, das Papier wird zwischen der oberen und der unteren Rolle eingefädelt. Wenn sich dann die obere Rolle dreht bewegt sich das Papier vor und zurück.

Die Rollen haben am Rand zwei Streifen die hervorstehen deshalb bekommt mein Papier immer noch eine markierung, das ist nicht so schön. Die werde ich wohl durch Glatte Rollen und vieleicht noch etwas breitere erstezen.