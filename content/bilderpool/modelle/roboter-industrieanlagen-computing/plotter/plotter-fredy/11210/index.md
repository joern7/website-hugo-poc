---
layout: "image"
title: "Stifthalter"
date: "2007-07-23T12:03:13"
picture: "plotter07.jpg"
weight: "12"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11210
- /details2209-3.html
imported:
- "2019"
_4images_image_id: "11210"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11210 -->
Der Stift müsste hier in der Halterung noch weiter nach unten geschoben werden damit er, wenn der Magnet Strom bekommt, er  aufs Papier kommt.