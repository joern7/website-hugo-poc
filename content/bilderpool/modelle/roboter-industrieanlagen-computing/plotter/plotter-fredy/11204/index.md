---
layout: "image"
title: "Plotter"
date: "2007-07-23T12:03:13"
picture: "plotter01.jpg"
weight: "6"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11204
- /details3da0.html
imported:
- "2019"
_4images_image_id: "11204"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11204 -->
In dieser Verfassung des Plotters funktioniert die Mechanik, wenn man sie manuell ausprobiert. Das Papier fährt vor und zurück der Stift fährt nach Links und Rechts und der Stift bewegt sich hoch und runter.