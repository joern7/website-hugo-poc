---
layout: "image"
title: "Antrieb"
date: "2007-07-23T12:03:13"
picture: "plotter03.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11206
- /detailse6ca-3.html
imported:
- "2019"
_4images_image_id: "11206"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11206 -->
Der untere Schrittmotor auf dem Bild treibt die Papier einzugs Rollen an. Der obere fährt die kette hin und her. Diese Verbinder mit den Schrauben, aus Messing, habe ich mir gedreht weil die normalen aus Plastik durchgerutscht sind.