---
layout: "image"
title: "Schlitten"
date: "2007-07-23T12:03:13"
picture: "plotter09.jpg"
weight: "14"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11212
- /details6ee7-2.html
imported:
- "2019"
_4images_image_id: "11212"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11212 -->
In der Mitte erkent man den Schlitten und wie er durch die beiden Metallstangen geführt wird, die Kette zieht den Schlitten hin und her.