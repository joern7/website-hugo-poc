---
layout: "image"
title: "Plotter Software"
date: "2010-04-23T19:51:01"
picture: "plotter5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26981
- /detailsa338.html
imported:
- "2019"
_4images_image_id: "26981"
_4images_cat_id: "1939"
_4images_user_id: "1112"
_4images_image_date: "2010-04-23T19:51:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26981 -->
Ich habe hier ein einfaches Delphi Programm geschrieben, mit dem man sogar vom Bildschirm "abpausen" kann. Wie man sieht, ist es ein wenig kantig, aber es ist ja schließlich auf den Plotter abgestimmt.