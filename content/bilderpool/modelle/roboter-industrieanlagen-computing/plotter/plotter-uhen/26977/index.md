---
layout: "image"
title: "Plotter Gesamtansicht"
date: "2010-04-23T19:50:59"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26977
- /detailsccfc.html
imported:
- "2019"
_4images_image_id: "26977"
_4images_cat_id: "1939"
_4images_user_id: "1112"
_4images_image_date: "2010-04-23T19:50:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26977 -->
Plotter mit relativ wenigen Bauteilen, aber trotzdem hoher Genauigkeit. Die Ansteuerung erfolgt mit der Schrittmotorenelektronik (http://www.elv.de/output/controller.aspx?cid=74&detail=10&detail2=9270&flv=1&bereich=&marke=) von ELV, die ich in einem Kunststoffgehäuse untergebracht habe. Die Schrittmotoren sind ebenfalls von ELV (http://www.elv.de/output/controller.aspx?cid=74&detail=10&detail2=258&flv=1&bereich=&marke=) und benötigen 96 Schritte für eine Drehung. Mit der Elektronik wird eine Libary für Visual C++ mitgeliefert. Diese habe ich dann für Delphi umgeschrieben, so dass nun die Ansteuerung aus Delphi erfolgen kann.