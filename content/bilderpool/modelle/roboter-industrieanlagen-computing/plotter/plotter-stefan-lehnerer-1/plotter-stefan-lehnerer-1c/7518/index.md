---
layout: "image"
title: "Plotter"
date: "2006-11-20T19:01:26"
picture: "plotterueberarbeitet3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7518
- /details0a10-3.html
imported:
- "2019"
_4images_image_id: "7518"
_4images_cat_id: "709"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7518 -->
