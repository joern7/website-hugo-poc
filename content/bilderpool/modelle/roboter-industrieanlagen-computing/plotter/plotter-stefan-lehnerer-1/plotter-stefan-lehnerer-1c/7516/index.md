---
layout: "image"
title: "Plotter"
date: "2006-11-20T19:01:26"
picture: "plotterueberarbeitet1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7516
- /detailsae21-2.html
imported:
- "2019"
_4images_image_id: "7516"
_4images_cat_id: "709"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7516 -->
Das Ergebnis.