---
layout: "image"
title: "Plotter2"
date: "2006-11-19T16:27:23"
picture: "plotter2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7496
- /details33f6.html
imported:
- "2019"
_4images_image_id: "7496"
_4images_cat_id: "706"
_4images_user_id: "502"
_4images_image_date: "2006-11-19T16:27:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7496 -->
Antrieb