---
layout: "image"
title: "Schreibkopf mit Kabelhaltern oder Gelenkwürfelklauen (z.B. für Kugelschreiberminen)"
date: "2012-03-24T18:41:26"
picture: "Schreibkopf_mit_Kabelhalter_schrg.jpg"
weight: "13"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34676
- /details1335.html
imported:
- "2019"
_4images_image_id: "34676"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2012-03-24T18:41:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34676 -->
Dünne Stifte (z.B. Kugelschreiberminen) lassen sich mit Kabelhaltern und Gelenkwürfel-Klauen näher am Schlitten und (ggf. mit etwas Heftpflaster umwickelt) besonders stabil befestigen.