---
layout: "image"
title: "Erstes Plot-Ergebnis: die Columbia"
date: "2011-11-29T10:04:04"
picture: "Columbia-Plot.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33580
- /details1ebe.html
imported:
- "2019"
_4images_image_id: "33580"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-11-29T10:04:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33580 -->
Die "Columbia" war in den 80er Jahren ein verbreitetes Testbild der HP-Plotter und -PCs.
Der Plot entstand aus einer HP-GL-Datei mit 2.260 Plot-Befehlen und insgesamt ca. 145.000 Impulsen in X- und 115.000 Impulsen in Y-Richtung (Dauer: ca. 5h).
Man erkennt noch kleine Ungenauigkeiten, die ich auf das Spiel der Kette zurückführe (vielleicht gehen aber im Online-Modus auch gelegentlich Impulse verloren). 
Verwendet habe ich eine Ball-Pen-Mine; das ist der feinste stabil montierbare Stift, den ich bisher gefunden habe. Leider zeichnet sie nicht ganz Kleks-frei.