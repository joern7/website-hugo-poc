---
layout: "comment"
hidden: true
title: "15809"
date: "2011-12-02T01:30:48"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Ralf,

freut mich sehr, dass es Dir gefällt! Der Plotter ist ausschließlich aus "Standard"-ft-Teilen gebaut, und überwiegend aus solchen, die sich in jeder Sammlung finden. Statt der Schrittmotoren kann man auch wie Stefan Falk den S-Motor mit Impulsrad 4 verwenden (http://ftcommunity.de/details.php?image_id=3913#col3); allerdings sinkt damit die Genauigkeit um den Faktor 10.

Übrigens habe ich eben das Ketten-Spiel der Y-Koordinate nochmal deutlich reduzieren können (Bild folgt demnächst); das Spiel der X-Koordinate versuche ich softwaremäßig auszugleichen. Ich hoffe dass es vor Weihnachten noch eine fehlerfreie Columbia gibt...

Um Dir den Mund noch ein wenig wässriger zu machen: In Ausgabe 4 der ft:pedia erscheint eine ausführliche Dokumentation des Projekts. Und die Software gibt es in Kürze auch zum Download (ein paar letzte Feinarbeiten sind noch zu erledigen).

Herzlicher Gruß,
Dirk