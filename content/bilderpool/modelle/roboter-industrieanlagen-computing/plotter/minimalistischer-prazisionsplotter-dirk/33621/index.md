---
layout: "image"
title: "24 Kreise (Radius 800)"
date: "2011-12-06T10:14:12"
picture: "24_Kreise_mit_Lineal.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33621
- /detailse53a.html
imported:
- "2019"
_4images_image_id: "33621"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-12-06T10:14:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33621 -->
Das Ergebnis einer meiner Testroutinen: 24 Kreise (Bresenham) mit einem Radius von 800 Impulsen, kreisförmig um einen gemeinsamen Mittelpunkt angeordnet.
Insgesamt also 76.800 Punke (in 192 Kreissegmenten) und 240 Schreibkopfbewegungen mit angehobenem Stift. Der Durchmesser des "Gesamtkreises" müsste bei 6,67 cm liegen - tatsächlich sind es in der Höhe ca. 3 mm weniger, in der Breite ca. 5 mm.

Damit liegt die Ungenauigkeit der Konstruktion bei weniger als 20 Impulsen pro Kreis (5 mm/24 Kreise/0,02083*2), bei 6.400 Impulsen mit gesenktem und 7.200 Impulsen mit angehobenem Stift (also unter 0,15%).