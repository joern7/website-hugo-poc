---
layout: "image"
title: "Zum Vergleich: Die Columbia als 'Bildschirm-Grafik'"
date: "2011-11-29T11:56:35"
picture: "Columbia-Grafik.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33581
- /detailsb678.html
imported:
- "2019"
_4images_image_id: "33581"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-11-29T11:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33581 -->
Hier zum Vergleich der Export eines "Bildschirm-Plots" - keine Klekse, keine Ungenauigkeiten, aber dafür eine erheblich schlechtere Auflösung.