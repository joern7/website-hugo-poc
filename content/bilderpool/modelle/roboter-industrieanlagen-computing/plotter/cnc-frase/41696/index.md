---
layout: "image"
title: "Schneckenmutter"
date: "2015-08-03T13:01:55"
picture: "cncfraese9.jpg"
weight: "9"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41696
- /details24a5.html
imported:
- "2019"
_4images_image_id: "41696"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41696 -->
Das ganze wird von vier Schneckenmuttern, zwei pro Schnecke, angetrieben. Dahinter sieht man nochmal zwei der Rollen