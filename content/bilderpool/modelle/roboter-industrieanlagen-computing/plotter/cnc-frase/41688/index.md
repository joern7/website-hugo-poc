---
layout: "image"
title: "Gesamtansicht x-Achse"
date: "2015-08-03T13:01:55"
picture: "cncfraese1.jpg"
weight: "1"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41688
- /detailsf14c.html
imported:
- "2019"
_4images_image_id: "41688"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41688 -->
Hier eine Gesamtansicht der x-Achse. Auf die umgedrehte Grundplatte 500 soll nachher ein Werkstück(Styropor, ...) zum Fräsen eingespannt werden, Das ganze kann sich nach entlang der zwei Schnecken bewegen. Die z-Achse und die y-Achse sollen später noch als Portal drübergebaut werden