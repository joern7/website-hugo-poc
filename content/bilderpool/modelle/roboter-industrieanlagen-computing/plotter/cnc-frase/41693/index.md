---
layout: "image"
title: "Endschalter 1"
date: "2015-08-03T13:01:55"
picture: "cncfraese6.jpg"
weight: "6"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41693
- /detailsfa9c.html
imported:
- "2019"
_4images_image_id: "41693"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41693 -->
Hier sieht man den ersten Endschalter und die Übertragung von der Kette auf die Schnecke