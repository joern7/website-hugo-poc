---
layout: "image"
title: "Motor x-Achse"
date: "2015-08-03T13:01:55"
picture: "cncfraese3.jpg"
weight: "3"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41690
- /detailsa634.html
imported:
- "2019"
_4images_image_id: "41690"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41690 -->
Hier die Befestigung des Motors, im Rollenlager 15 steckt auf der Achse des Powermotors eine Antriebshülse 15 in der dann eine Rastachse steckt