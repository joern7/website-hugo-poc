---
layout: "image"
title: "Befestigung"
date: "2015-08-03T13:01:55"
picture: "cncfraese5.jpg"
weight: "5"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41692
- /detailsc870.html
imported:
- "2019"
_4images_image_id: "41692"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41692 -->
Hier sieht man die Befestigung auf der (Fichten)Holzplatte die gelbe Strebe sorgt für zusätzliche Stabilität