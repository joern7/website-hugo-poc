---
layout: "image"
title: "Scanner/Plotter 28"
date: "2007-04-13T16:09:16"
picture: "scannerplotter7.jpg"
weight: "28"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10085
- /details9b15-2.html
imported:
- "2019"
_4images_image_id: "10085"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-13T16:09:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10085 -->
Das Ergebnis kann sich sehen lassen.