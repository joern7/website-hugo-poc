---
layout: "image"
title: "Scanner/Plotter 4"
date: "2007-04-07T11:10:28"
picture: "scannerplotter04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10020
- /details8cc4.html
imported:
- "2019"
_4images_image_id: "10020"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10020 -->
