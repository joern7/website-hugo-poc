---
layout: "image"
title: "Scanner/Plotter 3"
date: "2007-04-07T11:10:28"
picture: "scannerplotter03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10019
- /detailse3ff-2.html
imported:
- "2019"
_4images_image_id: "10019"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10019 -->
