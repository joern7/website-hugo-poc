---
layout: "image"
title: "Scanner/Plotter 12"
date: "2007-04-07T11:10:28"
picture: "scannerplotter12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10028
- /detailsd5ee.html
imported:
- "2019"
_4images_image_id: "10028"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10028 -->
