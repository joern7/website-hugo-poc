---
layout: "image"
title: "Scanner/Plotter 16"
date: "2007-04-07T11:10:28"
picture: "scannerplotter16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10032
- /details1e6c.html
imported:
- "2019"
_4images_image_id: "10032"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10032 -->
