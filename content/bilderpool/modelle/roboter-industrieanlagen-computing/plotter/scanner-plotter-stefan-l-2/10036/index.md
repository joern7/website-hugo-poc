---
layout: "image"
title: "Scanner/Plotter 20"
date: "2007-04-07T11:39:50"
picture: "scannerplotter1_5.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10036
- /details5f2a.html
imported:
- "2019"
_4images_image_id: "10036"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:39:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10036 -->
Das Ergebnis.