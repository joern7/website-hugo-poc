---
layout: "image"
title: "Scanner/Plotter 32"
date: "2007-04-23T21:15:31"
picture: "scannerplotter2_3.jpg"
weight: "32"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10149
- /detailsa80c.html
imported:
- "2019"
_4images_image_id: "10149"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10149 -->
Ein wenig stabilisiert.