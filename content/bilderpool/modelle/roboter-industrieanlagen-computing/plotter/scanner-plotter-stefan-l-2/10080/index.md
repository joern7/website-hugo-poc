---
layout: "image"
title: "Scanner/Plotter 23"
date: "2007-04-13T16:09:16"
picture: "scannerplotter2.jpg"
weight: "23"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10080
- /details4109.html
imported:
- "2019"
_4images_image_id: "10080"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-13T16:09:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10080 -->
