---
layout: "image"
title: "Scanner/Plotter 27"
date: "2007-04-13T16:09:16"
picture: "scannerplotter6.jpg"
weight: "27"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10084
- /details488b-2.html
imported:
- "2019"
_4images_image_id: "10084"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-13T16:09:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10084 -->
