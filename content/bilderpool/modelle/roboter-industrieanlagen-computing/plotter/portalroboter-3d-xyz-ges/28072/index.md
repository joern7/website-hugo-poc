---
layout: "image"
title: "[5/13] Encodermotor Y-Achse"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges05.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28072
- /details3943.html
imported:
- "2019"
_4images_image_id: "28072"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28072 -->
Alle vier Motoren sind in ihrer Zapfenachse und quer dazu verstellbar aus zwei Gründen:
1. Kettenspannung ohne Trum, sonst Umkehrspiel mit Verlauf!
2. Axialer Sitz der Zapfenritzel, Motor mit Ritzel axial ca. 0,5mm weiter vorn  
Da die Gehäusewandungen der Encodermotoren (auch die 2009er Ausgabe der XM- und XS-Motoren) dünn und Toleranzen bei den Nuten nicht vermeidbar sind, ist eine grossflächige Befestigung der Motoren sinnvoll (mehr als 2 Befestigungspunkte und weit auseinander).
Die Encoderkabel habe ich nicht gekürzt, deshalb diese Wickel an den Motoren.