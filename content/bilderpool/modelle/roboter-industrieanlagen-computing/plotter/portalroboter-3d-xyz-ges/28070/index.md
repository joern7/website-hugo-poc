---
layout: "image"
title: "[3/13] Schnellwechsel-Adapter am Modell"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges03.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28070
- /details0f75.html
imported:
- "2019"
_4images_image_id: "28070"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28070 -->
Die 28 pol. Buchsenplatte, hier in meiner Version 1 - ausschliesslich grün für Minus und rot für Plus - modifiziert.