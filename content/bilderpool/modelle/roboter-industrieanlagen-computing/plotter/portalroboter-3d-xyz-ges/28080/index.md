---
layout: "image"
title: "[13/13] XY-Testplott 2,5D beendet"
date: "2010-09-08T14:39:29"
picture: "portalroboterdxyzges13.jpg"
weight: "13"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28080
- /details8665-2.html
imported:
- "2019"
_4images_image_id: "28080"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:29"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28080 -->
Das Ende des Plotts ist erreicht. Die 35 Quadrate mit ihren jeweils beiden Diagonalen sind gezeichnet. Der Stift hat abgehoben.

Ein vorläufiges Fazit:

Mit der vorliegenden Statik und Mechanik des Modellaufbaus ruht vorerst meine "Alu-Studie".  Über die Gleitfführung und meine Methodik auf dem Weg zur Lösung gab es ja zur 2D-Vorstellung 2008 hier in der ftC vielschichtige Kommentare. Die zunächst gewählte Antriebsmechanik Schneckentrieb in reiner ft-Lösung ist toleranzbedingt fehlerbehaftet, langsam und wohl auch durch die einpoligen? Encoder unzureichend genau. Das Einhalten einer Toleranzgrenze von +/- 0,5 mm im Achsenlauf mit diesem noch reinen ft-Modell erscheint nach umfangreichen Testläufen aber dennoch als erreichbar.

Von Auskünften aus dem spezifischen Laborwissen von Remadus weiss ich von den Toleranzen der Ketten und Schnecken reiner ft-Modelle. Ich wollte das aber studienhalber auch mal praktisch durchleben. Das prägt und orientiert dann nachhaltiger ! Mit Anwendungen und Experimenten wird es wohl dann halt kein reines ft-Modell mehr bleiben können. Die Alu-Profile werden für Stabilität und Linearführung aber ihren Platz behalten.

Wenn meine Teilnahme zur Convention 2010 klappt, bringe ich das Modell mit. Da ich dort sicher wenig am Stand sein werde, lege ich zum Modell für interessierte Besucher ein Infoblatt aus :o)