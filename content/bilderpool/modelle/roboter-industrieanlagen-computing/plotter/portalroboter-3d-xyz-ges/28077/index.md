---
layout: "image"
title: "[10/13] XY-Testplott, Quadrat 1 von 35"
date: "2010-09-08T14:39:29"
picture: "portalroboterdxyzges10.jpg"
weight: "10"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28077
- /details940c-2.html
imported:
- "2019"
_4images_image_id: "28077"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28077 -->
Die zweite Diagonale des ersten Quadrats wird gezeichnet. Dass das Plott hier wirklich echt läuft :o) erkennt man an der Bewegungsunschärfe von Stift und Halterung. Das in Sachaufnahmen versierte weitwinklige Objektiv (25mm KB) meiner Digicam würde sonst diese Objekttiefe von oben bis unten durchgehend scharf zeichen.

Gezeichnet werden im Gegenuhrzeigersinn in einem Linienzug die vier Seiten und eine Diagonale. Zur zweiten Diagonale hebt der Stift über die Seitenlänge 2.

Die ungewollte offene Ecke am Quadrat zwischen Linie 1 und 4+5 dürfte auch RoboProEntwickler interessieren. Ob das bei Level 3 mit RoboPro dann auch so ist, konnte ich noch nicht testen.