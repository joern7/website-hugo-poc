---
layout: "image"
title: "[7/13] Referenzstellung"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges07.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28074
- /details0232.html
imported:
- "2019"
_4images_image_id: "28074"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28074 -->
Alle 4 Achsenschlitten stehen hier am standardisierten Referenzpunkt:  X=0 links, Y=0 vorn und Z=0 oben.
Dazu ist mir als UP-Baustein eine kleine aber feine automatische Endlagenjustierung (Referenzautomatik)eingefallen. Sie ermöglicht, dass man bei Referenzfahrten zur Justierung auf die fest definierten Achsenpositionen  0 mm von grösseren Entfernungen her auch mit Maximalgeschwindigkeit heranfahren kann. Die beiden X-Schlitten justieren sich natürlich auch noch unabhängig voneinander.
Zu erkennen ist hier auch am Z-Schlitten die Adapteraufnahme 2xBS52Z und 2xBS5.