---
layout: "image"
title: "HPGL Umwandlung"
date: "2012-02-26T19:36:22"
picture: "hpgl1.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34451
- /detailsf64f-2.html
imported:
- "2019"
_4images_image_id: "34451"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-26T19:36:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34451 -->
Links: Unbearbeitete HPGL-Datei; Mitte: Bearbeitete HPGL-Datei; Rechts: Fertige Excel-CSV-Datei
Das wurde geändert: Strichpunkt fliegt raus, hinter das Komma kommt noch ein Leerzeichen, der Befehl PA fliegt raus, PU (Pen up) wird ersetzt durch "32000, 0" damit das ProboPro verarbeiten kann, aus PD (Pen down) wird "32111, 0"