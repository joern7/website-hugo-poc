---
layout: "image"
title: "Plot 1"
date: "2012-02-21T22:16:44"
picture: "praezisionsplotter1_2.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34352
- /details1cdf.html
imported:
- "2019"
_4images_image_id: "34352"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T22:16:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34352 -->
Hier lässt sich schon mal erahnen mit welcher Genauigkeit der Plotter arbeitet