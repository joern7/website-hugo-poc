---
layout: "image"
title: "Plot 6"
date: "2012-02-26T14:42:24"
picture: "plot2_2.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34443
- /details0b75.html
imported:
- "2019"
_4images_image_id: "34443"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-26T14:42:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34443 -->
Hier hab ich den Elch im dxf-Format heruntergeladen und dann mit dem Programm BOcnc eine HPGL Datei erstellt.