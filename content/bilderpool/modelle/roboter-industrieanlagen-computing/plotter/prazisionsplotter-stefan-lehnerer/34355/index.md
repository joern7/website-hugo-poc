---
layout: "image"
title: "Plot 4"
date: "2012-02-22T16:06:50"
picture: "plot3.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34355
- /detailsa1dd-2.html
imported:
- "2019"
_4images_image_id: "34355"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-22T16:06:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34355 -->
Breite ca. 12cm