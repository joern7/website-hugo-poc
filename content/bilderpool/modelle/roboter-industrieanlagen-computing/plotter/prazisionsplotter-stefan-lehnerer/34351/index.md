---
layout: "image"
title: "Präzisionsplotter 6"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34351
- /details2b0c.html
imported:
- "2019"
_4images_image_id: "34351"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34351 -->
Antriebe der Achsen