---
layout: "image"
title: "Plot 7"
date: "2012-02-26T14:42:24"
picture: "plot3_2.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34444
- /details2b1a-2.html
imported:
- "2019"
_4images_image_id: "34444"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-26T14:42:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34444 -->
