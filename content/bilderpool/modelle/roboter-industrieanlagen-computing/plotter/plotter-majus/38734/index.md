---
layout: "image"
title: "Neuer Plotter (Frontansicht)"
date: "2014-05-02T16:40:08"
picture: "Plotter1.jpg"
weight: "4"
konstrukteure: 
- "majus"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/38734
- /details9829.html
imported:
- "2019"
_4images_image_id: "38734"
_4images_cat_id: "2336"
_4images_user_id: "1239"
_4images_image_date: "2014-05-02T16:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38734 -->
Mein neuer Plotter als digitale Variante.

Wie druckt der Plotter?
Ich habe ein kleines Programm geschrieben (c#), welches Bilder der Größe von 70x70 Pixeln in eine CSV Tabelle umwandelt. Dazu werden weißen Pixeln der Wert 0 und schwarzen Pixeln der Wert 1 zugeordnet. RoboPro liest diese Tabelle aus und verwandelt sie in ein fertiges Bild. 

Als Stift habe ich einen dicken Stabilo verwendet, die X/Y Koordinatenachsen sind über Taster sehr präzise.