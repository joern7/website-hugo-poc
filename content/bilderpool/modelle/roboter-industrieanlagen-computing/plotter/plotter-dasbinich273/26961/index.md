---
layout: "image"
title: "Gesamtansicht"
date: "2010-04-18T19:35:08"
picture: "plotter02.jpg"
weight: "2"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26961
- /details7c0c.html
imported:
- "2019"
_4images_image_id: "26961"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26961 -->
Mein erster Plotter.
Bei dem Plotter wird das Papier vor- und rückwärts bewegt und der Stift nur nach links und rechts