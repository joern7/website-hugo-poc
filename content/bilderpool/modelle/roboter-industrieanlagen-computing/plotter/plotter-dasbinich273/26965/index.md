---
layout: "image"
title: "Detail der Längsachse"
date: "2010-04-18T19:35:08"
picture: "plotter06.jpg"
weight: "6"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26965
- /details1da7.html
imported:
- "2019"
_4images_image_id: "26965"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26965 -->
Die Halterung war mehr als knifflig.