---
layout: "image"
title: "Draufsicht Technikmodul"
date: "2012-10-06T21:10:39"
picture: "schrankplotter10.jpg"
weight: "10"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35799
- /details1f43.html
imported:
- "2019"
_4images_image_id: "35799"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35799 -->
Die 'Zähne', die die Module aufeinander halten sind zu sehen