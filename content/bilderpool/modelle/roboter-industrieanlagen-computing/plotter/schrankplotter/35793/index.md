---
layout: "image"
title: "Ansicht der Rückseite"
date: "2012-10-06T21:10:39"
picture: "schrankplotter04.jpg"
weight: "4"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35793
- /details8fe3.html
imported:
- "2019"
_4images_image_id: "35793"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35793 -->
Man sieht die Kabelbäume (links) den Antrieb der X-Achse (links mittig) und den Antrieb der Y-Achse (rechts)