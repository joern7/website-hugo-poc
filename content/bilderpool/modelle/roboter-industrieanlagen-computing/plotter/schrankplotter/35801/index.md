---
layout: "image"
title: "Innenansicht Technikmodul"
date: "2012-10-06T21:10:39"
picture: "schrankplotter12.jpg"
weight: "12"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35801
- /detailsbba3-2.html
imported:
- "2019"
_4images_image_id: "35801"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35801 -->
Man sieht den TX (oben links) und die Verkabelung