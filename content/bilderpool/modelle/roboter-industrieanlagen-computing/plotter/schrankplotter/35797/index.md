---
layout: "image"
title: "Detail Y-Achse Taster"
date: "2012-10-06T21:10:39"
picture: "schrankplotter08.jpg"
weight: "8"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35797
- /detailsc303.html
imported:
- "2019"
_4images_image_id: "35797"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35797 -->
Man sieht den Endtaster und die Bafestigungen gut