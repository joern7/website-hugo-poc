---
layout: "image"
title: "Plotter 1"
date: "2007-12-09T13:33:17"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13020
- /details6ed2.html
imported:
- "2019"
_4images_image_id: "13020"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13020 -->
