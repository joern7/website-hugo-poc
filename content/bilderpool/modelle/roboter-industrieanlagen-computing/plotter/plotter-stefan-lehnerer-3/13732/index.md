---
layout: "image"
title: "Neue Stifthalterung 3"
date: "2008-02-24T14:33:01"
picture: "plotter4_3.jpg"
weight: "18"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13732
- /detailsf051-2.html
imported:
- "2019"
_4images_image_id: "13732"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2008-02-24T14:33:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13732 -->
