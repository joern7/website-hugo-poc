---
layout: "comment"
hidden: true
title: "5142"
date: "2008-01-27T19:36:04"
uploadBy:
- "StefanL"
license: "unknown"
imported:
- "2019"
---
Hallo,
das kommt von der Stiftmechanik, die noch ziemlich wackelig ist. Wenn der Stift angehoben oder abgesenkt wird, verschiebt sich der Stift auf der y-Achse. Wenn beider Achsen gleichzeitig angefahren werden, braucht die x-Achse erst einen kurzen Moment bis der Stift sich nicht mehr bewegen kann, dann erst bewegt sie sich richtig. Die Stiftmechanik ist momentan der größte Schwachpunk der noch beseitigt werden muss.
Grus Stefan