---
layout: "comment"
hidden: true
title: "11298"
date: "2010-04-01T01:00:33"
uploadBy:
- "Severin"
license: "unknown"
imported:
- "2019"
---
Die Treiber-Plattine wird von einem Atmega32 angesteuert. Die Treiber sind übrigends L298. Geschwindigkeit ist im Moment bei 50mm/s das Übersetztungsverhältnis musste ich wegen einem Programmierfehler auf 2:1 setzen.
Videos werde ich demnächst machen....

Gruß Severin