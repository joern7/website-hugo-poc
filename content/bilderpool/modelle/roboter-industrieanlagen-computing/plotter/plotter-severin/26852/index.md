---
layout: "image"
title: "Gesamtansicht"
date: "2010-03-31T20:00:30"
picture: "plotterseb1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26852
- /details6e36.html
imported:
- "2019"
_4images_image_id: "26852"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26852 -->
Links im Plotter befindet sich eine plexiglasplatte die das Papier nach reiben mit einem Pullover statisch anzieht und das Papier perfekt glatt festhält.