---
layout: "image"
title: "Der Y-Schlitten"
date: "2010-03-31T20:00:31"
picture: "plotterseb5.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26856
- /details86e5.html
imported:
- "2019"
_4images_image_id: "26856"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26856 -->
Sechs Angewinkelte Räder, auf der anderen Seite durch einen Gummizug angedrückt