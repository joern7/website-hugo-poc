---
layout: "image"
title: "Y-Achse mit Seilspanner"
date: "2010-03-31T20:00:30"
picture: "plotterseb3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26854
- /details7331.html
imported:
- "2019"
_4images_image_id: "26854"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26854 -->
