---
layout: "image"
title: "Antrieb lange Achse"
date: "2005-03-29T00:25:17"
picture: "Plotter_004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3914
- /detailsa919.html
imported:
- "2019"
_4images_image_id: "3914"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3914 -->
Das Prinzip ist dasselbe wie bei der kurzen Achse. Die ganz lange Antriebsachse ist eine 50 mm-Achse von Knobloch (also doch nicht wirklich ganz Original ft). Man sieht aber sehr schön die zwecks Spielausgleich an der kurzen Achse angebrachte Federspannung (die Federn *sind* Original ft).