---
layout: "comment"
hidden: true
title: "9695"
date: "2009-08-02T01:55:32"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo,

naja, wenn es kumulative Fehler gäbe, wäre das als Plotter ja nicht zu gebrauchen und verdiente auch nicht diesen Namen. Von einem "Plotter" muss man imho annehmen können, dass er wie erwartet funktioniert. Sonst geht er bestenfalls als Malmaschine oder als Fehlversuch (http://www.ftcommunity.de/categories.php?cat_id=724) durch.

Gruß,
Stefan