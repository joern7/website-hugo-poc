---
layout: "image"
title: "Screenshot 1"
date: "2005-03-29T09:54:46"
picture: "Screenshots_001.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3921
- /detailsb7f3.html
imported:
- "2019"
_4images_image_id: "3921"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3921 -->
Die Software ist in VB.NET geschrieben und kann auch völlig ohne angeschlossenen Plotter arbeiten. Man sieht, was der Plotter zeichnet. Hier sieht man, wie eine Funktion eingegeben werden kann. Diese wird dann .NET-mäßig kompiliert und (letztlich also in Maschinencode) ausgeführt.