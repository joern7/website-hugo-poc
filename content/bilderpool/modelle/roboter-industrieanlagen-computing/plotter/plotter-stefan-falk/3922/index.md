---
layout: "image"
title: "Screenshot 2"
date: "2005-03-29T09:54:46"
picture: "Screenshots_002.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3922
- /details086f.html
imported:
- "2019"
_4images_image_id: "3922"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3922 -->
Außerdem führt die Software auch Protokoll über die Plotteraktionen. Da hier nur simuliert wurde, sind die Zeiten alle bei 0 s. Die Software enthält auch einen globalen Optimierer: Alle Linien werden zunächst im RAM gehalten und anschließend so optimiert ausgegeben, dass nur minimal Bewegungen mit abgehobenem Stift auftreten. Bei dem regelmäßigen Vieleck ist das im Wesentlichen ganz ohne "Leerlauf" möglich; die Software findet einen Weg ohne Stiftabsetzen, wenn es einen gibt.