---
layout: "image"
title: "14 Portalroboter Anwendung"
date: "2005-05-11T16:15:30"
picture: "14-Pyramide.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4138
- /details64dc.html
imported:
- "2019"
_4images_image_id: "4138"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4138 -->
Taugt die Maschine denn nun? Um das festzustellen, habe ich dem Portalroboter das gleiche Kunststückchen beigebracht, wie es auch schon währen der Convention 2004 vom Hexapod gezeigt worden war. Die Ortsreproduzierbarkeit des Roboters ist im gesamten Fahrraum besser als 0,5 mm. Für diese Vorführung muß aber die maximale Fahrgeschwindigkeit etwas reduziert werden.