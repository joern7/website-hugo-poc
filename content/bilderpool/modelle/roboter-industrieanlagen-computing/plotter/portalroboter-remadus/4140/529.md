---
layout: "comment"
hidden: true
title: "529"
date: "2005-05-13T00:18:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Danke, aber...Tach auch!

Die Definition des tanh hätte ich sicher noch im Bronstein gefunden. Meine Frage bezog sich mehr darauf, wie man herleiten kann, dass für die Pausenlänge die Formel mit dem tanh gilt.

Außerdem: Wenn Ruck = d Beschleunigung / dt, dann müsste der Ruck in Deinem Diagramm das falsche Vorzeichen haben, oder?

Angenehmes Rechnen,
Stefan