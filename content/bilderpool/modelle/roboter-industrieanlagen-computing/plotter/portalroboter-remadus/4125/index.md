---
layout: "image"
title: "01 Portalroboter von links"
date: "2005-05-10T23:27:35"
picture: "01-Gesamtmodell.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4125
- /details43ba-2.html
imported:
- "2019"
_4images_image_id: "4125"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4125 -->
Unter dem zurückgefahrenen Portal ist der blau-rote Kasten zu sehen, in dem die Steuerelektronik untergebracht ist. Das sind drei Schrittmotortreiber und die Auswerteelektronik der Endschalter. Der Roboter selbst kann direkt an den Parallelport angeschlossen werden.