---
layout: "image"
title: "03 Linke Antriebsseite"
date: "2005-05-10T23:27:35"
picture: "03-Antriebsseite_links.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4127
- /details1ab4-3.html
imported:
- "2019"
_4images_image_id: "4127"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4127 -->
Unschwer zu erkennen ist nur die allernotwendigste Länge mit Ft-Ketten gemacht. Der Rest ist eine Kevlar-Zugschnur, die mit Federn vorgespannt ist. Die Schnur ist exzellent steif und sehr leicht. Das Portal ist ein Stück vorgefahren, wodurch die Wagenkonstruktion sichtbar wird, auf der das Querhaupt ruht.