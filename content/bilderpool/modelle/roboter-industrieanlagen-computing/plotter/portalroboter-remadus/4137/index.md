---
layout: "image"
title: "13 Portalroboter Rückansicht"
date: "2005-05-11T16:15:30"
picture: "13-Kabelbaum.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4137
- /details05d6-2.html
imported:
- "2019"
_4images_image_id: "4137"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4137 -->
In dieser Ansicht ist der Antriebsmotor der Z-Achse zu erkennen. Darüber, im oberen schwarz-roten Würfel, ist der Motor der X-Achse eingehaust, der da saugend, schmatzend reinpaßt. Das Kabel kommt unterhalb der roten Platten heraus.