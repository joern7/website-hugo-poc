---
layout: "image"
title: "Stifthalter"
date: "2011-06-29T07:16:20"
picture: "plotter2_4.jpg"
weight: "13"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30979
- /details7dec.html
imported:
- "2019"
_4images_image_id: "30979"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2011-06-29T07:16:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30979 -->
