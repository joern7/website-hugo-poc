---
layout: "image"
title: "Der Stifhalter"
date: "2010-12-19T10:14:51"
picture: "plotter6.jpg"
weight: "6"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29494
- /details66ee.html
imported:
- "2019"
_4images_image_id: "29494"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2010-12-19T10:14:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29494 -->
