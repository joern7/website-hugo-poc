---
layout: "image"
title: "Fischertechnik"
date: "2011-06-26T19:48:17"
picture: "plotter2_3.jpg"
weight: "11"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30945
- /details8adc.html
imported:
- "2019"
_4images_image_id: "30945"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30945 -->
