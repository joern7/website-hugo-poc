---
layout: "image"
title: "Minimalistischer Präzisionsplotter von Dirk Fox"
date: "2014-01-20T20:51:53"
picture: "plotter04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supermaxi19840"
license: "unknown"
legacy_id:
- /php/details/38105
- /detailsb2f0.html
imported:
- "2019"
_4images_image_id: "38105"
_4images_cat_id: "2835"
_4images_user_id: "2109"
_4images_image_date: "2014-01-20T20:51:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38105 -->
Die Endpunkttaster von der x- und- y- Achse. Beide taster sind an 1 und 3 angeschlossen.