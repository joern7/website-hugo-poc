---
layout: "image"
title: "Minimalistischer Präzisionsplotter von Dirk Fox"
date: "2014-01-20T20:51:53"
picture: "plotter07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supermaxi19840"
license: "unknown"
legacy_id:
- /php/details/38108
- /detailseb75.html
imported:
- "2019"
_4images_image_id: "38108"
_4images_cat_id: "2835"
_4images_user_id: "2109"
_4images_image_date: "2014-01-20T20:51:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38108 -->
Modell von "hinten"