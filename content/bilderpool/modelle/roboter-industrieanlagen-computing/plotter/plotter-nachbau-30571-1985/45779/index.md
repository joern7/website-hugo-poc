---
layout: "image"
title: "Motorshield"
date: "2017-04-16T22:07:55"
picture: "IMG_2332.jpg"
weight: "16"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45779
- /details80e7.html
imported:
- "2019"
_4images_image_id: "45779"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2017-04-16T22:07:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45779 -->
Die Grundidee des Aufbaus mit Arduino und Adafruit-Motorshield V1.0 habe ich aus der "c't Hacks Ausgabe 1/2013"Seite66ff.

Hier wird wie in vielen Blogs und Makerartikeln beschrieben, wie aus 2 DVD-Laufwerken ein LaserEngraver bzw. Plotter gebaut werden kann.
Ich habe statt des Lasers unseren ftElektromaneten angeschlossen und somit ist es ein Plotter. Der Aufbau entspricht hardwaremäßig weitestgehend dem ft ComputingPlotter 1985.
Andere Aufbauten funktionieren ganz sicher auch.
Adafruit v1.0 - an Pin 2 des Arduino (unten links) habe ich ein Kabel angelötet, um den Elektromagneten für das Anheben des Stiftes anzusteuern ( via LST Baustein von ft). Die gemeinsame Masse (GND) hole ich mir aus dem mittleren Klemmanschluss zwischen "M3" und "M4".
Ansteuersoftware:
Leicht modifizierte Sketche von dem c't Downloadlink.
Im Prinzip ein GCodeInterpreter auf dem Arduino, der seine GCode Daten via USB/serieller Schnittstelle vom PC erhält, dort läuft eine GCode-Sender -hier teste ich noch etwas, im Internet findet man zu Hauf genug einfache bis kompliziertere Tools.