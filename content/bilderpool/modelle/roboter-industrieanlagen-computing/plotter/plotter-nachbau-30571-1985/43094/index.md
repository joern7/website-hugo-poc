---
layout: "image"
title: "plotternachbauaus04.jpg"
date: "2016-03-13T12:19:14"
picture: "plotternachbauaus04.jpg"
weight: "4"
konstrukteure: 
- "Lemkajen (JENS)"
fotografen:
- "Lemkajen (JENS)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43094
- /detailsc21a.html
imported:
- "2019"
_4images_image_id: "43094"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2016-03-13T12:19:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43094 -->
