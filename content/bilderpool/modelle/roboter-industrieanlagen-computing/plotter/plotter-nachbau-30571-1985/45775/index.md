---
layout: "image"
title: "Arduino-gesteuert mit Adafruit V1 Steppershield"
date: "2017-04-16T22:07:55"
picture: "IMG_2328.jpg"
weight: "12"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Plotter", "Scanner", "Arduino", "Adafruit", "Motorshield"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45775
- /details165f.html
imported:
- "2019"
_4images_image_id: "45775"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2017-04-16T22:07:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45775 -->
den Plotter habe ich jetzt mal mit dem Arduino mittels Adafruit V1 Shield verbunden.
die software basiert auf einer laser graver software. den link reiche ich noch nach..