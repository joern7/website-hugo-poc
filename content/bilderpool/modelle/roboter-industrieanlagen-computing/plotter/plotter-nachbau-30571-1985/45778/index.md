---
layout: "image"
title: "Steuerung"
date: "2017-04-16T22:07:55"
picture: "IMG_2326.jpg"
weight: "15"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45778
- /detailsff13-2.html
imported:
- "2019"
_4images_image_id: "45778"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2017-04-16T22:07:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45778 -->
unter dem Motorshield sitzt ein Arduino Uno. 
Am Motorshield habe ich pin2 des Arduino abgegriffen und steuere damit die Leistungsstufe für den E Maneten zum Stift absenken