---
layout: "image"
title: "3D Drucker"
date: "2014-12-19T11:56:14"
picture: "ddrucker01.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: ["3D", "Drucker"]
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39929
- /details7867.html
imported:
- "2019"
_4images_image_id: "39929"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2014-12-19T11:56:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39929 -->
Hier die nicht-Fischertechnik-Teile und die verwendete Software:

Elektronik: http://fabber-parts.de/shop/index.php?a=186
Hotend: http://fabber-parts.de/shop/index.php?a=210
X und Y Schrittmotoren: Nema 17 aus der Bucht im 5er Pack
Filament "greifer": http://fabber-parts.de/shop/index.php?a=227

Als Firmware läuft Marlin
Als .stl zu .gcode converter: Cura, manchmal Slic3r

Die weißen Zahnräder hab ich mir von einem Bekannten ausdrucken lassen :P

Hier mit altem Druckkopf: http://www.ftcommunity.de/details.php?image_id=39398