---
layout: "image"
title: "Filament Rollen"
date: "2015-02-17T21:23:03"
picture: "drucker8.jpg"
weight: "23"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/40565
- /details96cb.html
imported:
- "2019"
_4images_image_id: "40565"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40565 -->
Meine Filamentrollen (je 1kg mit 1,75mm)

PLA in schwarz, grau, durchsichtig rot und dunkelbraun (Nicht im Bild, wird zu Zeit des Fotos für den Druck des Kerzenständers gebraucht)
TPE in Blau
HIPS in Weiß