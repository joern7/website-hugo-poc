---
layout: "image"
title: "Tütentragehilfe"
date: "2015-02-17T21:23:03"
picture: "drucker3.jpg"
weight: "18"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/40560
- /details284a-2.html
imported:
- "2019"
_4images_image_id: "40560"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40560 -->
In die Finger schneidende Tragetüten? Niewieder!