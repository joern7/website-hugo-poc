---
layout: "image"
title: "“Fischertechnik-3D-Drucker-Poederoyen-NL”"
date: "2009-03-01T20:05:03"
picture: "3D-Drucker-Poederoyen-NL_012.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23326
- /detailsa2c7.html
imported:
- "2019"
_4images_image_id: "23326"
_4images_cat_id: "1585"
_4images_user_id: "22"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23326 -->
Meine “Fischertechnik-3D-Drucker-Poederoyen-NL”

Original von Andreas Rozek. 
Weitere Einzelheiten gibt es auf der Wiki-Seite: 

http://objects.reprap.org/wiki/Builders/FTIStrap 

------------------------------------------------------------------------------------------------

Ich verwende wie Andreas auch eine kleine Heißklebepistole von Proxxon, die mit 7mm Sticks arbeitet - auf diese Weise ist der Aufbau schön klein.

Man beachte auch die:

-Aluprofilen und schwarze Anbauwinkel 32.615 für mehr Stabilität.
-Die Y-Asche mit 3 statt 2 Asche-4mm wie im Modell Andreas Rozek und Andreas Gürten
-Die X-Asche mit Gleitführung ……….schau auch mal unter: 

http://www.ftcommunity.de/details.php?image_id=13882

und :

http://www.ftcommunity.de/details.php?image_id=13893

Gruss,

Peter Damen,  
Poederoyen NL