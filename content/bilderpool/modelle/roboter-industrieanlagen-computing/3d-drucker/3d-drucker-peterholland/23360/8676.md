---
layout: "comment"
hidden: true
title: "8676"
date: "2009-03-05T17:34:13"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Sehe nur G01 bzw. G1 Geraden-Interpolation mit dadurch sehr vielen Programmzeilen # im Kreis. Verarbeitet der Interpreter z.B. auch Kreis-Interpolationen G02 (Uhrzeigersinn) und G03 (Gegenuhrzeigersinn)?