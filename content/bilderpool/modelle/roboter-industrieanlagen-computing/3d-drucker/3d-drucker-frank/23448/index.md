---
layout: "image"
title: "3D-Drucker10"
date: "2009-03-14T20:33:25"
picture: "PICT4672.jpg"
weight: "10"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23448
- /detailse10d-2.html
imported:
- "2019"
_4images_image_id: "23448"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23448 -->
Ansicht von links