---
layout: "image"
title: "3D-Drucker08"
date: "2009-03-09T08:11:10"
picture: "ddruckerfrank8.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23441
- /details2770-2.html
imported:
- "2019"
_4images_image_id: "23441"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-09T08:11:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23441 -->
Detail der X und Y-Achsen