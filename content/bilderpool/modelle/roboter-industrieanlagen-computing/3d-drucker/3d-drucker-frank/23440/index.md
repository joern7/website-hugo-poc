---
layout: "image"
title: "3D-Drucker07"
date: "2009-03-09T08:11:10"
picture: "ddruckerfrank7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23440
- /details528d.html
imported:
- "2019"
_4images_image_id: "23440"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-09T08:11:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23440 -->
Detail der X und Y-Achsen