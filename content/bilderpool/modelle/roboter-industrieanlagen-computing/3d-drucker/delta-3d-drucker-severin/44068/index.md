---
layout: "image"
title: "Druckvorgang"
date: "2016-07-31T23:36:59"
picture: "druckvorang1.jpg"
weight: "23"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44068
- /details8191-2.html
imported:
- "2019"
_4images_image_id: "44068"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T23:36:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44068 -->
Hier während eines Drucks, der Turm links wird später abgebrochen und dient nur als Supportstruktur für ein überhängenden Teil der eigentlichen Figur.