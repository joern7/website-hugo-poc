---
layout: "image"
title: "Heizbett und Touchscreen"
date: "2016-07-31T17:44:03"
picture: "deltaddrucker02.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44047
- /details5810.html
imported:
- "2019"
_4images_image_id: "44047"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44047 -->
Durch die beiden Extruder können verschiedene Farben gedruckt werden.