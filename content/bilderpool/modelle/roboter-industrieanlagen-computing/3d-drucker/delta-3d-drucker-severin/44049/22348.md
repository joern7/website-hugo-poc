---
layout: "comment"
hidden: true
title: "22348"
date: "2016-08-01T11:36:42"
uploadBy:
- "Severin"
license: "unknown"
imported:
- "2019"
---
Dazu muss noch gesagt werden, die Entlastung der Motorwelle ist nicht dafür da die Lager der Motoren zu schonen (die halten das ohne weiteres aus), sondern die Motorhalterung zu stabilisieren