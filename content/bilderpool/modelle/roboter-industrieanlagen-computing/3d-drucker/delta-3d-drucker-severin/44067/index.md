---
layout: "image"
title: "Filamentrollenhalter"
date: "2016-07-31T17:44:19"
picture: "deltaddrucker22.jpg"
weight: "22"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44067
- /detailsa3ff.html
imported:
- "2019"
_4images_image_id: "44067"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:19"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44067 -->
Die Metallachsen werden von gut eingebauten Kugellagern gelagert und ermöglichen sehr leichtes abrollen, auch von vollen Rollen.