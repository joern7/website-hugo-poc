---
layout: "image"
title: "Delta 3D Drucker"
date: "2016-07-31T17:44:03"
picture: "deltaddrucker01.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44046
- /details2932.html
imported:
- "2019"
_4images_image_id: "44046"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44046 -->
Technische Daten:
24cm Durchmesser beheiztes Druckbett
360W Stromverbrauch beim Aufheizvorgang von Bett und Hotend
E3D Cyclops Mixing Extruder
RAMPS 1.4 
OctoPi auf dem Raspberry Pi 2 mit DSI Touchscreen
500mm Druckhöhe (360 getestet)
80mm/s Druckgeschwindigkeit kein Problem
