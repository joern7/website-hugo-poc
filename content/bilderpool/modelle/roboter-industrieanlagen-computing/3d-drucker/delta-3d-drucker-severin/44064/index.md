---
layout: "image"
title: "Umlenkung"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker19.jpg"
weight: "19"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44064
- /detailsdb87.html
imported:
- "2019"
_4images_image_id: "44064"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44064 -->
Hier die Befestigung der Umlenkrolle. Die ist ein wenig massiver ausgefallen, da ich verhindern will, dass es mir die Zapfen aus den Bausteinen zieht.