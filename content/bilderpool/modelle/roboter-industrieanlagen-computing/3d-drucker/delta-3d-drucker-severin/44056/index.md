---
layout: "image"
title: "Effektor mit Hotend"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker11.jpg"
weight: "11"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44056
- /details5471.html
imported:
- "2019"
_4images_image_id: "44056"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44056 -->
Der Effektor ist, so wie alle anderen gedruckten Teile, vom alten Drucker erstellt worden. Dieser wurde in den aktuellen fast vollständig Recycled. 
Das zu sehende Hotend ist das E3D Cyclops.