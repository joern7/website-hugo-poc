---
layout: "image"
title: "Drucker von oben"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker14.jpg"
weight: "14"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44059
- /detailsf95c-2.html
imported:
- "2019"
_4images_image_id: "44059"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44059 -->
Die doppelten Verstrebungen durch die Alus helfen der Stabilität sehr und sehen gut aus ;)