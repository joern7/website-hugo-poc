---
layout: "comment"
hidden: true
title: "16810"
date: "2012-05-05T18:55:12"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Schnee = sneeuw....

Het object wat hier dmv lijmpatronen, stappenmotoren,  een lijmpistool en het 3D-programma "Schnee" wordt gemaakt is hier een sneeuw-kristal.

Andreas (Laserman) heeft een programma hiervoor geschreven.
Tevens ook voor enkele andere objecten.

Via de Links is deze 3D-Printer goed zelf na te bouwen.
O.a. Andreas, Paul en ikzelf hebben dit enkele jaren terug ook gedaan. 


Link zum Forum : 

http://forum.ftcommunity.de/viewtopic.php?f=8&t=736&hilit=3D+drucker 

Laserman hat es geschafft, selbst neue Teile zu entwickeln und auf die Wiki-Seite hochzuladen: 
http://objects.reprap.org/wiki/Builders/FTIStrap#Example_Objects  
  


Grüss,

Peter
Poederoyen NL