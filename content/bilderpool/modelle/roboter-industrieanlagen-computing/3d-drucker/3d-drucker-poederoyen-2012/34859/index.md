---
layout: "image"
title: "3D-Drucker Poederoyen-2012"
date: "2012-05-05T13:12:25"
picture: "ddruckerpoederoyen03.jpg"
weight: "3"
konstrukteure: 
- "a"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/34859
- /detailsdceb.html
imported:
- "2019"
_4images_image_id: "34859"
_4images_cat_id: "2582"
_4images_user_id: "22"
_4images_image_date: "2012-05-05T13:12:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34859 -->
3D-Drucken "Schnee"