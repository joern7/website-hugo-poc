---
layout: "image"
title: "3D-Drucker Poederoyen-2012"
date: "2012-05-05T13:12:25"
picture: "ddruckerpoederoyen16.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/34872
- /details5509.html
imported:
- "2019"
_4images_image_id: "34872"
_4images_cat_id: "2582"
_4images_user_id: "22"
_4images_image_date: "2012-05-05T13:12:25"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34872 -->
3D-Drucken  2x  "Schnee"  + "Opel"