---
layout: "image"
title: "3D-Drucker Poederoyen-2012"
date: "2012-05-05T13:12:25"
picture: "ddruckerpoederoyen07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/34863
- /details0117-2.html
imported:
- "2019"
_4images_image_id: "34863"
_4images_cat_id: "2582"
_4images_user_id: "22"
_4images_image_date: "2012-05-05T13:12:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34863 -->
3D-Drucken "Schnee"