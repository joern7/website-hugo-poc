---
layout: "image"
title: "3D-Drucker 2.0 Beleuchtung"
date: "2018-04-16T19:24:22"
picture: "ddrucker31.jpg"
weight: "31"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47442
- /details474f.html
imported:
- "2019"
_4images_image_id: "47442"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:22"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47442 -->
Über den Schalter  vorne lässt sich die 12 Volt LED-Beleuchtung einschalten.