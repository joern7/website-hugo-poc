---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:18"
picture: "ddrucker26.jpg"
weight: "26"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47437
- /detailsf191-3.html
imported:
- "2019"
_4images_image_id: "47437"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47437 -->
Das höhenverstellbare Heizbett ist von Anycubic.