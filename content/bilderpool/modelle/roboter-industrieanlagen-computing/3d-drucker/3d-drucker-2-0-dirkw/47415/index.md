---
layout: "image"
title: "3D-Drucker 2.0 TXT eingeklappt"
date: "2018-04-16T19:24:07"
picture: "ddrucker04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47415
- /details6d1e.html
imported:
- "2019"
_4images_image_id: "47415"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47415 -->
Der TXT lässt sich für den Transport einklappen.