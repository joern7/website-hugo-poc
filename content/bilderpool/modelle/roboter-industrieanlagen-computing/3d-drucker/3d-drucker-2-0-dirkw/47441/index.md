---
layout: "image"
title: "3D-Drucker 2.0 unten"
date: "2018-04-16T19:24:18"
picture: "ddrucker30.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47441
- /details8d02-4.html
imported:
- "2019"
_4images_image_id: "47441"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47441 -->
Das Netzteil wurde auf kleine Platten gebaut. Das Lochraster passt genau dazu.