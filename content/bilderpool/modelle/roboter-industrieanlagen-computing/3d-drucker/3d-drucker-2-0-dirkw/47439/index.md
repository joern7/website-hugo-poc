---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:18"
picture: "ddrucker28.jpg"
weight: "28"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47439
- /detailsc4df.html
imported:
- "2019"
_4images_image_id: "47439"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47439 -->
Die Kettenumlenkung ist vom fischertechnik 3D-Printer abgeschaut.