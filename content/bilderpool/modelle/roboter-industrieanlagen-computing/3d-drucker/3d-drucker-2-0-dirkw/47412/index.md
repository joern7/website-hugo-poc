---
layout: "image"
title: "3D-Drucker 2.0 Vorderansicht"
date: "2018-04-16T19:24:06"
picture: "ddrucker01.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47412
- /detailscfe7.html
imported:
- "2019"
_4images_image_id: "47412"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47412 -->
Die Abmaße betragen: L= 30 cm x B = 32 cm x H = 34 cm.
Sein stolzes Gewicht beträgt = 8,4 kg.