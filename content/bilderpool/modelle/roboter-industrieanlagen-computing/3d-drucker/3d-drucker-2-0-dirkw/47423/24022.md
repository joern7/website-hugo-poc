---
layout: "comment"
hidden: true
title: "24022"
date: "2018-04-16T19:48:46"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Entsprechen die Schrittmotorhalterungen den Nema 14 Spezifikationen, sodass man auch andere Nema 14 Motoren damit verwenden könnte?