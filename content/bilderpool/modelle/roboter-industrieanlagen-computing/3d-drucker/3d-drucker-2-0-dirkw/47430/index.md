---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:12"
picture: "ddrucker19.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47430
- /detailsdf9c.html
imported:
- "2019"
_4images_image_id: "47430"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47430 -->
Zusätzlich zur Führung durch Schnecke und Achse, sind 2 kleine Seilrollen verbaut worden.