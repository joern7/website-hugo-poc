---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:18"
picture: "ddrucker22.jpg"
weight: "22"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47433
- /details0eef-2.html
imported:
- "2019"
_4images_image_id: "47433"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47433 -->
Antrieb der Y-Achse.