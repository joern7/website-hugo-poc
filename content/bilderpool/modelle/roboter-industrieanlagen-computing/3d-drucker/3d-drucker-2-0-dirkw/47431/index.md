---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:13"
picture: "ddrucker20.jpg"
weight: "20"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47431
- /detailse406.html
imported:
- "2019"
_4images_image_id: "47431"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:13"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47431 -->
Auf der nächsten Abbildung ist die Höhenverstellung der Z-Achse zu erkennen. 
Diese lässt sich sehr leicht und genau über ein Zahnrad Z15 verstellen.