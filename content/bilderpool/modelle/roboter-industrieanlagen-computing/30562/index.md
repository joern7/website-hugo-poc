---
layout: "image"
title: "Training Roboter II"
date: "2011-05-14T22:07:37"
picture: "DSC00655.jpg"
weight: "24"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30562
- /details0ab8.html
imported:
- "2019"
_4images_image_id: "30562"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30562 -->
Details of the Z axis limit switch.

Informationen über die Z-Achse Endschalter