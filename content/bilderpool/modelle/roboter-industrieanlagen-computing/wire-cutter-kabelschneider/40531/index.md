---
layout: "image"
title: "Detail Litzen-Transport"
date: "2015-02-12T22:59:16"
picture: "3_Detail_Kabeltransport.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40531
- /details4c0d.html
imported:
- "2019"
_4images_image_id: "40531"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40531 -->
Vorne links ist die Führung
Zwischen den 2 Rädern wird die Litze weitertransportiert
Das Rad mit den Federn kann in der Höhe verstellt werden