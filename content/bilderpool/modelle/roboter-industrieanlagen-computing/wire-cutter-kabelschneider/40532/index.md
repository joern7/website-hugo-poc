---
layout: "image"
title: "Lichtschranke TCST 2103 Vishay"
date: "2015-02-12T22:59:16"
picture: "4_Lichtschranke.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40532
- /detailsa6f4.html
imported:
- "2019"
_4images_image_id: "40532"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40532 -->
IR-Leuchtdiode (Infrarot):
+ von Betriebsspannung (9V) über 500 Ohm-Widerstand an + von Leuchtdiode
- von Betriebsspannung (9V) an E von Leuchtdiode

Empfänger (für TX-Controller):
- von Betriebsspannung (9V) an + von Empfänger
Eingang TX-Controller (z.B. I1) an D von Empfänger

Codierscheibe:
19 Segmente
Raddurchmesser 30mm
>> Auflösung 5mm pro Impuls