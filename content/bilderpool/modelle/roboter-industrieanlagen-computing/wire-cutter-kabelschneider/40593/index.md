---
layout: "image"
title: "einen Augenblick später"
date: "2015-02-22T19:09:59"
picture: "DS_07_abgeschnitten.jpg"
weight: "13"
konstrukteure: 
- "Mi_ch_ae_l   Se_ng_st_sc_hm_id"
fotografen:
- "Mi_ch_ae_l   Se_ng_st_sc_hm_id"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/40593
- /details3b36.html
imported:
- "2019"
_4images_image_id: "40593"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T19:09:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40593 -->
So, fertig, der Draht ist abgeschnitten.

Wichtig 1: nicht den Federzylinder verwenden, da auch für das Zurückfahren Kraft benötigt wird.

Wichtig 2: der Draht muß auf der Seite der Drahtrolle fixiert werden, sonst wird der Draht hineingezogen und nicht abgeschnitten.

Natürlich könne noch Verbesserungen vorgenommen werden.
Eine Klinge mit gebogener Schneide könnte evt ein noch besseres Ergebnis liefern.

Und nochmals die Warnung:

Messer ist scharf, da kann man sich wehtun - AUA --> BLUT --> ROT --> ft versaut...