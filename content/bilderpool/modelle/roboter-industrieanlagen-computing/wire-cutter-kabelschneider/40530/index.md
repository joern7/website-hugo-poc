---
layout: "image"
title: "Wire Cutter / Kabelschneider von vorne"
date: "2015-02-12T22:59:16"
picture: "2_Wire_Cutter_von_vorne.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40530
- /details4170.html
imported:
- "2019"
_4images_image_id: "40530"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40530 -->
Vorne kommt die Litze raus und kann schön abgeschnitten werden
Die roten 30er Platten verhindern, daß das obere Rad nach vorne gezogen wird