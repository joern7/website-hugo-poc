---
layout: "image"
title: "jede Menge Taster im Detail"
date: "2014-04-04T22:26:58"
picture: "IMG_0019.jpg"
weight: "3"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Tastatur", "Matrix", "Minitaster", "Arduino"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38529
- /details39c0.html
imported:
- "2019"
_4images_image_id: "38529"
_4images_cat_id: "2876"
_4images_user_id: "1359"
_4images_image_date: "2014-04-04T22:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38529 -->
