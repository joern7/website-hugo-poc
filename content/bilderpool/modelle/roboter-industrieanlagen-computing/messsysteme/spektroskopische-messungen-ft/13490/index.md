---
layout: "image"
title: "ft-Messanordnung"
date: "2008-02-01T17:44:12"
picture: "ft-Anordnung_vorne.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13490
- /details6695.html
imported:
- "2019"
_4images_image_id: "13490"
_4images_cat_id: "1233"
_4images_user_id: "731"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13490 -->
Dies ist die ft-Messanordung, mit der eine Fotodiode per Schrittmotor und Inteface an der Projektionsfläche entlang geführt wird Helligkeitswerte liefert.