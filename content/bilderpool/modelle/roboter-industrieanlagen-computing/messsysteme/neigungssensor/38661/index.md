---
layout: "image"
title: "Neigungssensor_001"
date: "2014-04-25T22:31:16"
picture: "001.jpg"
weight: "1"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
keywords: ["Neigungssensor", "Gleichgewicht", "Sensor", "Gyro", "Messen", "Messtechnik"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38661
- /detailsae2e.html
imported:
- "2019"
_4images_image_id: "38661"
_4images_cat_id: "2887"
_4images_user_id: "2167"
_4images_image_date: "2014-04-25T22:31:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38661 -->
Dies ist ein einfacher Neigungssensor, der mit Hilfe der Schwerkraft arbeitet.

Den zentralen Bestandteil bildet dabei ein Foto-Widerstand, vor dem ein Pendel hängt,
jedoch leicht versetzt.
Wenn man den Sensor nun in die eine Richtung neigt, wird der Foto-Widerstand vom Pendel
mehr verdeckt, als vorher.
Neigt man den Sensor in die andere Richtung, wird der Foto-Widerstand weniger verdeckt.
Es lässt sich also nicht nur bestimmen, wie stark er geneigt wird,
sondern auch in welche Richtung.
Das Pendel wird durch eine Aufnahmeachse gehalten, da diese viel leichtgängiger ist,
als ein normales Gelenk.
Der Strebenadapter bestimmt den Maximalausschlag des Pendels, wodurch verhindert wird,
dass das Pendel, nachdem es den kompletten Foto-Widerstand abgedeckt hat,
ihn wieder freigibt (sollte man den Sensor noch weiter neigen).