---
layout: "image"
title: "BNO055 Gyroscope"
date: "2016-02-13T21:30:12"
picture: "BNO055_Gyroscope.jpg"
weight: "7"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: ["Sensor", "Bosch", "BNO055"]
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/42865
- /details9e9a.html
imported:
- "2019"
_4images_image_id: "42865"
_4images_cat_id: "2887"
_4images_user_id: "2374"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42865 -->
Objective: I2C MPU driver optimization wrt user needs.
Setup: Encodermotor with Turning table and lever arm. The Sensor BNO055 is located at the outer position with a lever arm of approx. 17cm.
Table is rotated firstly by +360° afterwards by -360° with two different speed.
No closed loop control.
BNO055 Gyroscope Sensor data has been recorded.
Issues:
&#8220;Closed loop Speed control &#8220; is missing in RoboPro
Accurate sampling rate