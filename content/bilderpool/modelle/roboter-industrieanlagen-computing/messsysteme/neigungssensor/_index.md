---
layout: "overview"
title: "Neigungssensor"
date: 2020-02-22T08:08:12+01:00
legacy_id:
- /php/categories/2887
- /categories1079.html
- /categories3cef.html
- /categoriesf6f6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2887 --> 
Ein kompakter Sensor zum Messen von Neigungen