---
layout: "image"
title: "Messarm während einer Messung"
date: "2012-01-02T19:55:04"
picture: "Bildschirmfoto_2012-01-02_um_17.17.07.jpg"
weight: "3"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33831
- /detailsd88c.html
imported:
- "2019"
_4images_image_id: "33831"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33831 -->
Durch den Algorithmus wird als Grundlage der Weg in Cm und die Zeit in ms genommen und mit der zeitlichen Differenz des abgemessenen Objektes so verwurstelt, dass ein Wert in Centimetern ausgegeben werden kann.