---
layout: "image"
title: "Unterprogramm Messen"
date: "2012-01-02T19:55:05"
picture: "Messen.jpg"
weight: "7"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33835
- /details55c0.html
imported:
- "2019"
_4images_image_id: "33835"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33835 -->
Unterprogramm Messen - Dient einerseits zum Erfassen der Zeit bis zum berühren des Objektes, als auch zur Umrechnung der zeitlichen Differenz und der Ausgabe des Wertes in Centimetern auf dem Displays des TXs.