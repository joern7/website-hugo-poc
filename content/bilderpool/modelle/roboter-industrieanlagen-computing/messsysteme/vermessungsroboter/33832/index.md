---
layout: "image"
title: "Ausgabe des gemessenen und berechneten Wertes auf dem TX Display"
date: "2012-01-02T19:55:04"
picture: "Bildschirmfoto_2012-01-02_um_17.17.15.jpg"
weight: "4"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33832
- /detailsec82.html
imported:
- "2019"
_4images_image_id: "33832"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33832 -->
