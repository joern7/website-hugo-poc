---
layout: "image"
title: "Elektronik"
date: "2016-09-08T14:31:03"
picture: "netduinodemo2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44341
- /detailse1b8.html
imported:
- "2019"
_4images_image_id: "44341"
_4images_cat_id: "3273"
_4images_user_id: "104"
_4images_image_date: "2016-09-08T14:31:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44341 -->
Das interessante an der Sache ist der große Komfort beim Entwickeln: fast vollständiges .net (incl. Objektorientierung, Multithreading, Garbage Collection, Netzwerk, Exception Handling und und und), Visual Studio mit vollem Live-Debugging etc.

Damit kann man der Software eine richtig schöne Architektur geben und softwaretechnisch halt aus dem Vollen schöpfen. Das erkauft man sich mit mehr CPU-Zyklen als z.B. bei in C programmierten Arduinos oder PIs, da im .net Micro Framework der IL-Code zur Laufzeit interpretiert wird. Aber schnell genug für I/O mit ein paar kHz ist es allemal und mir den Komfort auf jeden Fall wert.

Außerdem benutze ich hier Leistungsstufen der 1980er-Jahre-fischertechnik-Elektronik als weitere Treiber (außer denen des Adafruit-Boards), die einfach an normalen I/O-Leitungen des Netduino hängen.