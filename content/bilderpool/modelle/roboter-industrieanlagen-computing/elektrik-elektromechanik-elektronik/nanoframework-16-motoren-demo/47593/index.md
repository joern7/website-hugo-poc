---
layout: "image"
title: "Motorenseite - Motor side"
date: "2018-05-06T15:34:10"
picture: "nanoframeworkmotorendemo1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47593
- /detailsf9c5.html
imported:
- "2019"
_4images_image_id: "47593"
_4images_cat_id: "3510"
_4images_user_id: "104"
_4images_image_date: "2018-05-06T15:34:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47593 -->
Unten befinden sich 8 S-Motoren, obendrauf 4 S-Motoren, 3 XS-Motoren und ein älterer 6V-Mini-Motor. Die Verbinder 15 zeigen den Lauf der Motoren gut sichtbar an.

---------

On the bottom there are 8 S motors, ontop 4 S motors, 3 XS motors and one older 6V Mini Motor. The red 15 mm connectors give a nice view of the motors spinning.