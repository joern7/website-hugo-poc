---
layout: "image"
title: "Rüdkseite - Back side"
date: "2018-05-06T15:34:11"
picture: "nanoframeworkmotorendemo2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47594
- /detailsa49b.html
imported:
- "2019"
_4images_image_id: "47594"
_4images_cat_id: "3510"
_4images_user_id: "104"
_4images_image_date: "2018-05-06T15:34:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47594 -->
Ein 9V-Netzteil versorgt die vier Motor Shields und damit die Motoren mit Strom, ein weiteres den Netduino (das unterste board) selbst. Jedes Motor Shield kann vier Gleichstrom-Motoren (oder auch zwei Schrittmotoren, dazu wird es noch eine Demo geben) ansteuern.

----------

One 9V power supply delivers power to the four Motor Shields and thereby the motors themselve, another power supply is used for the Netduino board (at the bottom). Each Motor Shield can drive up to four DC motors (or two stepper motors, for which a demo will follow also).