---
layout: "image"
title: "Status-LEDs - Status LEDs"
date: "2018-05-06T15:34:11"
picture: "nanoframeworkmotorendemo3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47595
- /details8475-2.html
imported:
- "2019"
_4images_image_id: "47595"
_4images_cat_id: "3510"
_4images_user_id: "104"
_4images_image_date: "2018-05-06T15:34:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47595 -->
Die normale onboard-LED des Netduino wird auf einem separaten Thread sanft aufleuchtend und dimmend betrieben, um zu sehen, ob das Board selbst noch korrekt arbeitet und um zu zeigen, wie sanft man andere Dinge tun kann, obwohl die CPU mit dem sanften Beschleunigen von 16 Motoren und dem dazu notwendigen I²C-Datenverkehr zu den Motor Shields beschäftigt ist.

Die drei GoPort-LEDs des Boards werden zur Anzeige der Anzahl von Motoren verwendet, die die jeweils aktuelle Zielgeschwindigkeit während des sanften Beschleunigens oder Abbremsend noch nicht erreicht haben: 1, 2 oder 3 LEDs leuchten, wenn 1, 2 oder mehr als 2 Motoren noch nicht die Zielgeschwindigkeit erreicht haben. Immer wenn alle Motoren auf die Zielgeschwindigkeit gebracht wurden, sind die drei GoPort-LEDs aus und es wird drei Sekunden gewartet, bevor die Geschwindigkeiten wieder geändert werden.

----------

The normal onboard LED of the Netduino is smoothly lit up and dimmed again using its own thread in order to see if the board is still operating, and to demonstrate how smooth other things can be done while the CPU is busy with smoothly accelerating or decelerating the 16 motors and the I²C traffic necessary for that.

The three GoPort LEDs of the board are used to show the number of motors which have not yet reach the current target velocity during the smooth acceleration/deceleration: 1, 2, or 3 LEDs light up when 1, 2 or more than 2 motors have not yet reached their target speed. Every time when all motors have realized the target velocity, the three GoPort LEDs are off and a pause of thee seconds is made before the target speeds get changed again.