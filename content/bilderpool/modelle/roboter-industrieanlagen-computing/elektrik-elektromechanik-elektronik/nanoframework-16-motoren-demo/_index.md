---
layout: "overview"
title: "nanoFramework - 16-Motoren-Demo"
date: 2020-02-22T08:08:32+01:00
legacy_id:
- /php/categories/3510
- /categoriescd44.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3510 --> 
Dies ist eine Demo, wie mit einem Netduino-3-Board, vier Adafruit Motor Shields, dem nanoFramework (https://nanoframework.net/) und Visual Studio 2017/C# sechzehn fischertechnik-Motoren simultan sanft beschleunigend und bremsend angesteuert werden können.

----------

This is a demonstration how a Netduino 3 board, four Adafruit Motor Shields, nanoFramework (https://nanoframework.net/) and Visual Studio 2017/C# can drive sixteen fischertechnik motors simultaneously smoothly accelerated and decelerated.