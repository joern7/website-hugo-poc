---
layout: "image"
title: "Kurzhub Gesenkhammer"
date: "2016-05-08T21:51:22"
picture: "DSCI1413.jpg"
weight: "27"
konstrukteure: 
- "Thomas Bullmann"
fotografen:
- "Thomas Bullmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtAnfaenger"
license: "unknown"
legacy_id:
- /php/details/43344
- /detailsd8d3.html
imported:
- "2019"
_4images_image_id: "43344"
_4images_cat_id: "125"
_4images_user_id: "2595"
_4images_image_date: "2016-05-08T21:51:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43344 -->
In Bau