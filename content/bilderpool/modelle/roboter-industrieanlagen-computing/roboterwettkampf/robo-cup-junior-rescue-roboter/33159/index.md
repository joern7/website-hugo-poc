---
layout: "image"
title: "Version 3 Bild 05"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot08.jpg"
weight: "8"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33159
- /details058e.html
imported:
- "2019"
_4images_image_id: "33159"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33159 -->
