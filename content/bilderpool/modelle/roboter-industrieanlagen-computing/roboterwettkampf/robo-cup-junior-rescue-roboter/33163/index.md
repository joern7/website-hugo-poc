---
layout: "image"
title: "Version 3 Bild 09"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot12.jpg"
weight: "12"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33163
- /detailsbde3-2.html
imported:
- "2019"
_4images_image_id: "33163"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33163 -->
