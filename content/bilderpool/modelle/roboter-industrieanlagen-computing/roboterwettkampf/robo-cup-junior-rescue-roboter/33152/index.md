---
layout: "image"
title: "Version 1"
date: "2011-10-15T17:38:04"
picture: "rcjrescuebot01.jpg"
weight: "1"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33152
- /detailsb88a.html
imported:
- "2019"
_4images_image_id: "33152"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33152 -->
Das ist die erste Version des Roboters, sie basiert weitestgehend auf dem Robo Explorer, besitzt allerdings eine eigene Steuerung. Hier hatte ich noch auf mehrere Platinen gesetzt, die über den CAN-Bus verbunden waren.
Diese Version hat noch keinen Greifer, da ich damals noch von eine älteren Aufgabe ausging (da mussten "nur" farbige Männchen auf dem Boden erkannt werden).