---
layout: "image"
title: "Version 3 Bild 24"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot27.jpg"
weight: "27"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33178
- /detailsaa39-2.html
imported:
- "2019"
_4images_image_id: "33178"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33178 -->
Hier hab ich schon mal die Kabel weg gemacht, jetzt sieht man auch die beiden Motortreiber (L293D). Die Platine biebtet schon die Anschlüsse, für die Sensoren, die noch dazu kommen werden (unter anderem Sharp IR-Entfernungssensoren und SRF10 Ultraschallsensoren)