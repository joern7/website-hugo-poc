---
layout: "image"
title: "Version 3 Bild 10"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot13.jpg"
weight: "13"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33164
- /detailsc1f1-2.html
imported:
- "2019"
_4images_image_id: "33164"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33164 -->
