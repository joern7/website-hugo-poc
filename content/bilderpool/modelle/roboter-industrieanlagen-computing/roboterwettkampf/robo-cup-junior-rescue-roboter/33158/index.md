---
layout: "image"
title: "Version 3 Bild 04"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot07.jpg"
weight: "7"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33158
- /details9203.html
imported:
- "2019"
_4images_image_id: "33158"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33158 -->
