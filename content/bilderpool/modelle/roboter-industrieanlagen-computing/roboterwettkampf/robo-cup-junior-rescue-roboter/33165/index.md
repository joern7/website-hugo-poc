---
layout: "image"
title: "Version 3 Bild 11"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot14.jpg"
weight: "14"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33165
- /details65b2.html
imported:
- "2019"
_4images_image_id: "33165"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33165 -->
Hier sieht man auch ein bischen, das die Ketten sich "durchbiegen", in der neuen Version habe ich vor Metallachsen zu verwenden und zweifach zu führen.