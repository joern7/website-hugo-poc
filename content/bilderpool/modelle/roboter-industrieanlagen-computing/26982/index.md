---
layout: "image"
title: "Mobile Roboter for Eurobot Contest"
date: "2010-04-25T16:21:08"
picture: "cheburek-2-500.jpg"
weight: "15"
konstrukteure: 
- "ft-ninja and students"
fotografen:
- "ft-ninja"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-ninja"
license: "unknown"
legacy_id:
- /php/details/26982
- /details272b.html
imported:
- "2019"
_4images_image_id: "26982"
_4images_cat_id: "125"
_4images_user_id: "1080"
_4images_image_date: "2010-04-25T16:21:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26982 -->
