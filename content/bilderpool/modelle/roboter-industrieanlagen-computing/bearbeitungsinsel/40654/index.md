---
layout: "image"
title: "Robo-Interface"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel2.jpg"
weight: "2"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40654
- /details788c.html
imported:
- "2019"
_4images_image_id: "40654"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40654 -->
Unter der Extention befindet sich ein Robo-Interface. In der Mitte rechts ist eine Schweißstation mit Pneumatikzylinder und Lampe.