---
layout: "image"
title: "Greifer"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel4.jpg"
weight: "4"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40656
- /detailsabb9.html
imported:
- "2019"
_4images_image_id: "40656"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40656 -->
Links sieht man den Greifer in Mittelposition. Er kann um eine Achse gedreht werden und setzt ein Werkstück vom schwarzen Förderband zürück ins Magazin (rotes Fb).