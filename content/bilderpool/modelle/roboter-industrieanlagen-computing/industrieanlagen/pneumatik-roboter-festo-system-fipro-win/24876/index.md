---
layout: "image"
title: "Pneumatik-Roboter 2"
date: "2009-09-06T19:17:12"
picture: "Pneumatik-Roboter_3_800x600.jpg"
weight: "2"
konstrukteure: 
- "Lorenz Parting"
fotografen:
- "Lorenz Parting"
keywords: ["Pneumatik", "Computing", "Graue", "Steine", "Festo"]
uploadBy: "Macgyver"
license: "unknown"
legacy_id:
- /php/details/24876
- /details8612.html
imported:
- "2019"
_4images_image_id: "24876"
_4images_cat_id: "1711"
_4images_user_id: "726"
_4images_image_date: "2009-09-06T19:17:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24876 -->
Das Ganze nochmal mit CVK-Interface