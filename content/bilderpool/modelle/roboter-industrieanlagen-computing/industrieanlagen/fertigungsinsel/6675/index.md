---
layout: "image"
title: "Fertigungsinsel"
date: "2006-08-10T18:11:12"
picture: "Fuji-Bilder_001.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- /php/details/6675
- /detailsc0d3.html
imported:
- "2019"
_4images_image_id: "6675"
_4images_cat_id: "647"
_4images_user_id: "341"
_4images_image_date: "2006-08-10T18:11:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6675 -->
Fertigungsinsel von der Seite gesehen.