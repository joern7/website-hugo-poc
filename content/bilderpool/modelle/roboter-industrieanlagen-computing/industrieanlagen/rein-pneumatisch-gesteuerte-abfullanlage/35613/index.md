---
layout: "image"
title: "Drosseln der Zeitglieder"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35613
- /details4166.html
imported:
- "2019"
_4images_image_id: "35613"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35613 -->
Diese beiden Drosseln regeln den Luftdurchlass in die beiden Zeitglieder. Umgekehrt parallel ist je ein Rückschlagventil eingebaut, damit die Druckluft nach Ablauf der Zeit schnell wieder zurück entweichen kann.