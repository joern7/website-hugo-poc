---
layout: "image"
title: "Zurücksetzen des Zählwerks (2)"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35617
- /details174f.html
imported:
- "2019"
_4images_image_id: "35617"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35617 -->
Die Zylinder drücken die lange BS30-Reihe kurz nach rechts (in Bildrichtung) und ziehen sie dann wieder nach links zurück. Die gelben Statikstreben schieben dann die acht zur Zählung verwendeten Schalthebel wieder in die Ausgangsstellung zurück.