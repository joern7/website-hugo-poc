---
layout: "image"
title: "Zurücksetzen des Zählwerks (1)"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35616
- /details3249.html
imported:
- "2019"
_4images_image_id: "35616"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35616 -->
Sobald das Zeitglied des Füllvorgangs schaltet, wird das Flaschenzählwerk auf Null zurück gestellt. Das übernehmen diese beiden parallel geschalteten Pneumatikzylinder.