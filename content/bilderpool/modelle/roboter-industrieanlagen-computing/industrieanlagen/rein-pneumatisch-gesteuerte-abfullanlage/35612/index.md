---
layout: "image"
title: "Realisierung des Füllvolumens"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35612
- /details6e8e.html
imported:
- "2019"
_4images_image_id: "35612"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35612 -->
Wie in der echten Anlage wird nicht etwa bis zum Erreichen eines konkreten Füllstandes gepumpt, sondern bis ein pneumatisches Zeitglied auslöst. Dessen Justage bestimmte die Füllhöhe, und seine Genauigkeit war in der echten Anlage ausreichend.

Hier sieht man zwei fast identisch übereinander aufgebaute Zeitglieder. Das untere stoppt die Pumpzeit, und das obere... dat kriejen wa später ;-)

Wie in einem der vorigen Bilder beschrieben, geht die Druckluft vom unteren Endlagenventil des Füllkopf-Hebe-Zylinders noch an eine zweite Stelle, nämlich über eine Drossel ganz langsam in einen Pneumatiktank (gerade noch links im Bild zu sehen) und in den unteren der beiden einfachwirkenden Zylinder. Der fährt also ganz langsam aus und drückt dadurch die gelenkig angebrachte Platte 15 * 60 nach rechts. Damit (durch furchtbar feines Einstellen der Drossel) konnte eine Füllzeit in der Größenordnung von 20 s realisiert werden. Das genügt zwar nur, um die Flaschen im Modell ca. ein Viertel voll zu machen, aber bei kompletter Füllung wäre das Zuschauen vielleicht zu langweilig.