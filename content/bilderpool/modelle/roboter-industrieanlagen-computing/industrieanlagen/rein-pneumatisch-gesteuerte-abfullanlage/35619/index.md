---
layout: "image"
title: "Stopperzylinder in Ausgangsstellung (1)"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35619
- /details54e5.html
imported:
- "2019"
_4images_image_id: "35619"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35619 -->
Die beiden Stopperzylinder werden also von der Ausgangslage umgesteuert: Der hier linke wird eingezogen, damit die gefüllten Flaschen ablaufen können, und der rechte fährt aus, um neue Flaschen am Einlaufen in die Füllstation zu hindern.