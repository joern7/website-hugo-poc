---
layout: "image"
title: "Staudüse zur Flaschenerkennung"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35597
- /detailsdc11.html
imported:
- "2019"
_4images_image_id: "35597"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35597 -->
Die echte Anlage lief mit einem ca. 2,50 m lange und über 1 m hohen Kompressor mit einem entsprechenden Drucktank. Der Kompressor stand in einem anderen Raum und war der zweite im Einsatz: Der erste, vielleicht 1,50 m lange, hatte sich nämlich als dem Dauerbetrieb nicht gewachsen gezeigt - er war ständig am Pumpen und kam dem Luftverbrauch der Anlage kaum nach.

Eine Staudüse (sämtliche Pneumatikteile stammten übrigens von FESTO) stellte fest, wenn eine Flasche vorbeilief. Die pustet einfach Luft aus einem Loch hinaus, und wenn gerade eine Flasche vorbeilief, wurde dieses Loch leicht abgedeckt. Dadurch änderte sich der Innendruck der Staudüse. Der wurde verstärkt und einem Zählwerk zugeführt. Alles nur mit Druckluft, wohlgemerkt!.