---
layout: "image"
title: "Füllstation"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35596
- /details1412.html
imported:
- "2019"
_4images_image_id: "35596"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35596 -->
Man sieht quer zum Förderband zwei Stopperzylinder, die immer gegenläufig angesteuert werden. Hier ist der hintere ausgefahren (das Band läuft von rechts nach links) und der vordere eingefahren. Es können also Flaschen in die Füllstation einfahren. Rechts werden sie detektiert, und nach vier Flaschen beginnt der Füllvorgang: Der Füllkopfträger mit den vier Füllköpfen senkt sich, dann wird Flüssigkeit in die Flaschen gepumpt und die Füllköpfe werden wieder angehoben. Schließlich werden die beiden Stopperzylinder eine Zeitlang umgesteuert, sodass die vier gefüllten Flaschen ablaufen können, ohne dass gleich leere nachlaufen.