---
layout: "comment"
hidden: true
title: "17352"
date: "2012-10-03T11:21:53"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Naja, eine echte "Schleuse" - vier Flaschen rein, hintere Sperre zu, vordere Sperre auf, vier Flaschen raus. Dafür müsstest Du (wenn die Anlage nicht rein pneumatisch arbeiten würde) nicht einmal zählen - wenn die letzte Flasche einen Hebel gedrückt hielte, wüsstest Du, dass alle Flaschen "drin" sind (von Verkantungen einmal abgesehen).
Gruß, Dirk