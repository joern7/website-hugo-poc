---
layout: "image"
title: "Schaltbild"
date: "2015-04-05T22:03:33"
picture: "diagrammederabfuellanlage2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40724
- /detailse526.html
imported:
- "2019"
_4images_image_id: "40724"
_4images_cat_id: "3060"
_4images_user_id: "104"
_4images_image_date: "2015-04-05T22:03:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40724 -->
Hier das komplette Pneumatik-Schaltbild der Anlage. Alles ist mit unveränderte (1980er-Jahre) fischertechnik-Pneumatik realisiert: Das Schaltbild zeigt den Zustand im Ruhezustand, also wenn die Anlage auf das Ankommen der Flaschen wartet.

1. Links unten Kompressor, Druckbehälter und Manometer zur Druckkontrolle. Im gezeigten Anfangszustand sind Stopper1 eingefahren und Stopper 2 ausgefahren.

2. Am linken Rand ist die Drossel/Staudüse-Kombination zu sehen, die die einfahrenden Flaschen registriert. Für jede Flasche wird das Ventil daneben ein Mal durchgeschaltet (Flasche ist vor Staudüse) und wieder freigegeben (Flasche ist an der Staudüse vorbei).

3. Diese Impulse werden mit beiden Schritten durch die Baugruppe aus 8 Ventilen gezählt. Der erste Impuls steuert das erste Ventil an. Wenn der Impuls vorbei ist, kann durch das so geschaltete Ventil Druckluft strömen - und das zweite Ventil durchsteuern.

4. Der nächste Impuls (der zweiten Flasche nämlich) beaufschlagt den Eingang des zweiten Ventils mit Druckluft und steuert somit das dritte Ventil an. Beim Loslass-Signal der zweiten Flasche wird bekommt eben dieses Ventil an seinem Versorgungseingang Druckluft und steuert also das vierte Ventil um.

Dieses Spiel wiederholt sich für die vier Flaschen. Die ach Halbsignale für die vier Flaschen werden auf diese Weise Halbschritt für Halbschritt "durchgereicht". (Im Original übernahm diese Aufgabe ein fertiges, bis 999.999 einstellbares Zählwerk.)

Sobald die vierte Flasche "durch" ist, wird das achte Ventil umgesteuert. Dessen Versorgungseingang hängt permanent an Druckluft...

5. ... und steuert das 4/2-Wegeventil rechts unten an. Das bewirkt, dass der Zylinder für die Füllköpfe sich langsam (mit gedrosselter Abluft) absenkt: Die Füllköpfe senken sich majestätisch auf die Flaschen.

6. Am unteren Anschlag wird das Ventil etwas links von der Schaltbildmitte mechanisch betätigt. Solange das der Fall ist, strömt langsam (gedrosselt) Luft in den Zylinder des ersten Zeitglieds, welches die Fülldauer bestimmt. Die Fülldauer ist durch Einstellung der Drossel justierbar.

Zudem wird das Ventil der oberen Endlage freigegeben. Das in Ruhestellung nämlich durchgeschaltete Zeitglied oben wird damit entlüftet. Das wird später noch wichtig.

7. Sobald die Füllzeit erreicht ist, wird das Ventil rechts daneben betätigt. Druckluft - vom achten Ventil der Zählgruppe nämlich (ein wichtiges Detail) gelangt also zum 4/2-Wegeventil ganz unten. Das betätigt den (bzw. aus Kraftgründen die beiden) Zylinder zum Rückstellen des gesamten Zählwerks.

8. In diesem Moment entfällt automatisch die Druckluft für dieses Ventil, denn die stammte ja vom achten Zählventil - was gerade zurückgesetzt wurde. Die Rückstellzylinder fahren also wieder in ihre Ausgangsposition zurück.

Außerdem fällt dadurch das Ventil ganz rechts unten zurück. Der Füllkopf-Zylinder hebt (durch das Rückschlagventil an der Drossel vorbei) so schnell wie möglich wieder an - die Füllköpfe sind schwer genug, dass das langsam geht.

9. Oben angekommen, wird das obere Endlagenventil betätigt. Langsam gedrosselt strömt also Druckluft ins obere Zeitglied. Wichtig ist außerdem, dass nur jetzt (nicht also bei losgelassenem oberen Endlagenventil) Druckluft am Ausgangsventil dieses Zeitglieds anliegt. Das steuert schließlich das Ventil ganz rechts oben für die Stopperzylinder um. Die Stopper verfahren also so lange, bis das Zeitglied abgelaufen ist.

10. Danach hat die Anlage wieder ihre Ausgangsposition eingenommen. Die nächsten vier Flaschen können kommen.