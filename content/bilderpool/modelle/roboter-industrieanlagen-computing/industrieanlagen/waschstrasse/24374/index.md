---
layout: "image"
title: "Gesamtansicht"
date: "2009-06-15T22:51:30"
picture: "waschstrasse01.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24374
- /detailsb443-2.html
imported:
- "2019"
_4images_image_id: "24374"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24374 -->
