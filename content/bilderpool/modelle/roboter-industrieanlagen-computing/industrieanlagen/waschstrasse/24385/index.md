---
layout: "image"
title: "Antrieb, der die Bürste hoch und runter bewegt."
date: "2009-06-15T22:51:45"
picture: "waschstrasse12.jpg"
weight: "12"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24385
- /detailsdfd4.html
imported:
- "2019"
_4images_image_id: "24385"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24385 -->
