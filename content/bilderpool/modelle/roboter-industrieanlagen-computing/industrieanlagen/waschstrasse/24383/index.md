---
layout: "image"
title: "Ansicht auf Ventilator und Ampel"
date: "2009-06-15T22:51:31"
picture: "waschstrasse10.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24383
- /detailscf1e.html
imported:
- "2019"
_4images_image_id: "24383"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24383 -->
