---
layout: "image"
title: "Pneumatik-Roboter 5 Bild 2"
date: "2008-03-08T22:39:12"
picture: "Pneumatik-Roboter_5_Bild_2.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten, Original von FT Experimenta Schulprogramm"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13890
- /details3732-2.html
imported:
- "2019"
_4images_image_id: "13890"
_4images_cat_id: "1274"
_4images_user_id: "724"
_4images_image_date: "2008-03-08T22:39:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13890 -->
Der Roboter kann z.B. den Tischtennisball von einer Mulde zu einer der beiden anderen Mulden befördern.