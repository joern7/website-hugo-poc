---
layout: "image"
title: "Bearbeitungszentrum 005"
date: "2011-10-20T17:10:01"
picture: "FT_Derk_005.jpg"
weight: "6"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/33261
- /details21c4.html
imported:
- "2019"
_4images_image_id: "33261"
_4images_cat_id: "635"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T17:10:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33261 -->
