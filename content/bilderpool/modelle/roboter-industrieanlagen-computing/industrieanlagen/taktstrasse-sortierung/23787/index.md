---
layout: "image"
title: "Taktstraße mit Sortierung 32"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung32.jpg"
weight: "32"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23787
- /detailsba7b.html
imported:
- "2019"
_4images_image_id: "23787"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23787 -->
Der Straße entlang, von links nach rechts.