---
layout: "image"
title: "Taktstraße mit Sortierung 48"
date: "2009-04-24T08:32:35"
picture: "taktstrassemitsortierung48.jpg"
weight: "48"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23803
- /details6127.html
imported:
- "2019"
_4images_image_id: "23803"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:35"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23803 -->
