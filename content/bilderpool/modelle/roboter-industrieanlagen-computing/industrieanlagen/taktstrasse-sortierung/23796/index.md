---
layout: "image"
title: "Taktstraße mit Sortierung 41"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung41.jpg"
weight: "41"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23796
- /details7ac7.html
imported:
- "2019"
_4images_image_id: "23796"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23796 -->
Nahaufnahme der Schweißstation.

Diese wird mit einem Zylinder mit Rückholfeder über ein Magnetventil angesteuert und fährt den Schweißkopf zum Werkstück.

Ein Taster auf der Unterseite aktiviert den Schweißkopf sobald der Zylinder maximal ausgefahren und der Schweißkopf nah am Werkstück ist.