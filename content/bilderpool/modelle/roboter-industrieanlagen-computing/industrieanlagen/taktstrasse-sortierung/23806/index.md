---
layout: "image"
title: "Taktstraße mit Sortierung 51"
date: "2009-04-24T08:32:35"
picture: "taktstrassemitsortierung51.jpg"
weight: "51"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23806
- /details1d66.html
imported:
- "2019"
_4images_image_id: "23806"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:35"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23806 -->
