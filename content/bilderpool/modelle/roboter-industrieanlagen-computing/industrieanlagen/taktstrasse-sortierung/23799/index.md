---
layout: "image"
title: "Taktstraße mit Sortierung 44"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung44.jpg"
weight: "44"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23799
- /details4af4-2.html
imported:
- "2019"
_4images_image_id: "23799"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23799 -->
