---
layout: "image"
title: "Taktstraße mit Sortierung 27"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung27.jpg"
weight: "27"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23782
- /detailsb5bf.html
imported:
- "2019"
_4images_image_id: "23782"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23782 -->
Der Straße entlang, von rechts nach links.