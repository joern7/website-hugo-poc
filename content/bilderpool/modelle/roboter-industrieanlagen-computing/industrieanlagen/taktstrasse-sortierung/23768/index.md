---
layout: "image"
title: "Taktstraße mit Sortierung 13"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung13.jpg"
weight: "13"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23768
- /details6b1d-3.html
imported:
- "2019"
_4images_image_id: "23768"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23768 -->
Nahaufnahme der Putzstation von schräg oben. 
Links im Bild kann man den Taster der den Motor der Bohrstation aktiviert erkennen.