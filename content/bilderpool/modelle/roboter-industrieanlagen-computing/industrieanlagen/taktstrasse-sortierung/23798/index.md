---
layout: "image"
title: "Taktstraße mit Sortierung 43"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung43.jpg"
weight: "43"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23798
- /detailsc323-3.html
imported:
- "2019"
_4images_image_id: "23798"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23798 -->
