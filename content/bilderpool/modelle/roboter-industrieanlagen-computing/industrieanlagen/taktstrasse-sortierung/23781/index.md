---
layout: "image"
title: "Taktstraße mit Sortierung 26"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung26.jpg"
weight: "26"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23781
- /detailscc8f.html
imported:
- "2019"
_4images_image_id: "23781"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23781 -->
Nahaufnahme des Pneumatikgreifers mit Antrieb und Positionstaster (Band)