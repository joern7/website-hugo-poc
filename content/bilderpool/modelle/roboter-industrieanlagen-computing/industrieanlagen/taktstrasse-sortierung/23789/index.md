---
layout: "image"
title: "Taktstraße mit Sortierung 34"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung34.jpg"
weight: "34"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23789
- /detailsb570.html
imported:
- "2019"
_4images_image_id: "23789"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23789 -->
Der Straße entlang, von rechts nach links.