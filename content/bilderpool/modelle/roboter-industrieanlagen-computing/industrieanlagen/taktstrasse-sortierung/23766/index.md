---
layout: "image"
title: "Taktstraße mit Sortierung 11"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung11.jpg"
weight: "11"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23766
- /details24fa.html
imported:
- "2019"
_4images_image_id: "23766"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23766 -->
In der mitte ist Bandmotor 5 zu sehen, sowie der Kompressor dahinter.