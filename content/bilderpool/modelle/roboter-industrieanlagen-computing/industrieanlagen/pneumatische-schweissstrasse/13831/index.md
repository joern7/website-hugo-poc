---
layout: "image"
title: "Loslegen"
date: "2008-03-04T16:29:19"
picture: "pneumatischeschweissstrasse2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13831
- /details13a7.html
imported:
- "2019"
_4images_image_id: "13831"
_4images_cat_id: "1269"
_4images_user_id: "747"
_4images_image_date: "2008-03-04T16:29:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13831 -->
Zuerst legt man das Werkstück das ein Flugzeugteil sein soll zwischen die Lichtschranke.