---
layout: "image"
title: "stoppen"
date: "2008-03-04T16:29:19"
picture: "pneumatischeschweissstrasse3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13832
- /detailsa0bc.html
imported:
- "2019"
_4images_image_id: "13832"
_4images_cat_id: "1269"
_4images_user_id: "747"
_4images_image_date: "2008-03-04T16:29:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13832 -->
Jetzt wird der Förderband -Motor angeschaltet und stoppt, wenn die zweite Lichtschranke durchbrochen ist.