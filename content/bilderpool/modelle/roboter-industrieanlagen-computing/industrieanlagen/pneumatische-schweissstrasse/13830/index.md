---
layout: "image"
title: "Schweißstraße"
date: "2008-03-04T16:29:18"
picture: "pneumatischeschweissstrasse1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13830
- /detailsc905.html
imported:
- "2019"
_4images_image_id: "13830"
_4images_cat_id: "1269"
_4images_user_id: "747"
_4images_image_date: "2008-03-04T16:29:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13830 -->
Das ist die pneumatische Schweißstraße!