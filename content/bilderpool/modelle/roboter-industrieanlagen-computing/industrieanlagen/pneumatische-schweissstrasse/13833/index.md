---
layout: "image"
title: "Schweißen"
date: "2008-03-04T16:29:19"
picture: "pneumatischeschweissstrasse4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13833
- /detailsa9ab.html
imported:
- "2019"
_4images_image_id: "13833"
_4images_cat_id: "1269"
_4images_user_id: "747"
_4images_image_date: "2008-03-04T16:29:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13833 -->
Nun kommt von außen ein pneumatisch gesteuerter Schweißer und verschweißt eine Stelle des Flugzeugteils.