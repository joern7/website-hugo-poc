---
layout: "image"
title: "Roboter-Aufzug-08"
date: "2012-02-18T13:28:18"
picture: "HUB-08.jpg"
weight: "8"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34221
- /detailsf1a1-2.html
imported:
- "2019"
_4images_image_id: "34221"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34221 -->
das ist die Verankerung der Laufschien (hinten) Ist ganz einfach gemacht, aber wahnsinnig stabil und steif.