---
layout: "image"
title: "Austrag01"
date: "2004-03-11T20:12:31"
picture: "austrag-01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2305
- /details4179.html
imported:
- "2019"
_4images_image_id: "2305"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2305 -->
So arbeitet mein Sohn am liebsten mit. Das Bild zeigt den Aufbau des Stoßofens.