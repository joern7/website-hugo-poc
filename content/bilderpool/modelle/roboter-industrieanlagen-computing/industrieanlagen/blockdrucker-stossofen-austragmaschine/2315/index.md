---
layout: "image"
title: "Austrag11"
date: "2004-03-13T21:06:34"
picture: "Austrag-11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Interface", "iba", "econ", "Austrag", "Austragmaschine", "Blockentnahmemaschine", "Stoßofen", "Stossofen", "Industrieanlage"]
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2315
- /details6ccb-2.html
imported:
- "2019"
_4images_image_id: "2315"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-13T21:06:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2315 -->
So soll die Steuerung mit neuem Interface und Bedienpult ungefähr aussehen. Bis September muß es fertig sein ;-)