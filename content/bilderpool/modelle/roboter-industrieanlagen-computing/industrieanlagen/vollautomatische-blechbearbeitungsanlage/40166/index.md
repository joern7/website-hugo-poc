---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage07.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage07.jpg"
weight: "6"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40166
- /detailsf46e.html
imported:
- "2019"
_4images_image_id: "40166"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40166 -->
