---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage08.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage08.jpg"
weight: "7"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40167
- /details9925.html
imported:
- "2019"
_4images_image_id: "40167"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40167 -->
