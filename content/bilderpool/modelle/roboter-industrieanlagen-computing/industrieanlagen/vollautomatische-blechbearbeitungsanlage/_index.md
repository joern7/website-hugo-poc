---
layout: "overview"
title: "Vollautomatische Blechbearbeitungsanlage"
date: 2020-02-22T08:04:33+01:00
legacy_id:
- /php/categories/3017
- /categorieseea8.html
- /categories5525.html
- /categories6c0f.html
- /categoriesc84b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3017 --> 
Die vollautomatische Blechbearbeitungsanlage besteht aus einem Roboterarm mit Vakuumgreifer, einer Blechvorlage mit Lichtschranke, einer Reinigungsstation, einer Schweissstation und einem Auswurfförderband. Die Steuerungssoftware wurde bewusst nicht auf maximalen Durchsatz optimiert. Ein Video findet Ihr hier: http://youtu.be/ZVD5b5BQInY