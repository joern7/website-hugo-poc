---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage20.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage20.jpg"
weight: "19"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40179
- /detailse91b.html
imported:
- "2019"
_4images_image_id: "40179"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40179 -->
