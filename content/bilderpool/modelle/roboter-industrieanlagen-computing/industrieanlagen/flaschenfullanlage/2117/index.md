---
layout: "image"
title: "Flafue02.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue02.jpg"
weight: "16"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2117
- /detailscfbd-3.html
imported:
- "2019"
_4images_image_id: "2117"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2117 -->
Diese Station ist in Veghel 2004 dazugekommen. 
Die Flaschen kommen von links und werden vom linken Greifer abgenommen, umgestülpt und in einem der Ringe zum Austropfen abgestellt. Danach dreht das Karussel eine Position weiter. Eine halbe Umdrehung später schlägt der Greifer auf der rechten Seite zu, richtet die Flaschen wieder auf und stellt sie auf das Förderband.