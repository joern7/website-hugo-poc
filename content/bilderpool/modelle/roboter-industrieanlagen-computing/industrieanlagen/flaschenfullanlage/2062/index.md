---
layout: "image"
title: "Flaschenfuell13.jpg"
date: "2004-01-07T20:37:28"
picture: "Flaschenfll13.jpg"
weight: "13"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2062
- /details7c06-2.html
imported:
- "2019"
_4images_image_id: "2062"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2062 -->
Jetzt geht es allmählich in die Zielgerade, aber vorher gibt es noch eine Schikane zu meistern.

Die Flaschen kommen von rechts hinten an. Wenn sich vor jedem der drei Taster eine Flasche befindet, schwenkt der Greifer herum, ergreift sie (derweil eine leere Kiste von links her zugeführt wird), schwenkt zurück und steckt sie in die Kiste.