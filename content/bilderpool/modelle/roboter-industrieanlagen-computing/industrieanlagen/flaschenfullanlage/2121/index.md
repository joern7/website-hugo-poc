---
layout: "image"
title: "Flafue15.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue15.jpg"
weight: "20"
konstrukteure: 
- "Frans leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2121
- /details531b-3.html
imported:
- "2019"
_4images_image_id: "2121"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2121 -->
Station "Deckel aufschrauben", Veghel 2004.
Detailaufnahme zu Flafue07.JPG.

Die Metallachsen sind Vertikalführungen für die Schraubstationen. Innen drin das Riesenzahnrad und ein Z10, das gerade eingekuppelt ist.