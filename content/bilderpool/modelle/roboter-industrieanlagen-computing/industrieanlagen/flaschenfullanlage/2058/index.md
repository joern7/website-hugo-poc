---
layout: "image"
title: "Flaschenfuell09.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll09.jpg"
weight: "9"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2058
- /detailsab03.html
imported:
- "2019"
_4images_image_id: "2058"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2058 -->
Rechts der Schüttbehälter für die Deckel. Dahinter, im hinteren querliegenden Förderband eine Vorrichtung, die die Deckel so dreht, dass die Öffnung nach unten zeigt. Die lagerichtig gedrehten Deckel warten im Magazin (links) auf ihren Einsatz.