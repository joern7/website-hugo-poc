---
layout: "image"
title: "Bedienung"
date: "2014-08-20T14:19:12"
picture: "trl09.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39253
- /details662d-2.html
imported:
- "2019"
_4images_image_id: "39253"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39253 -->
Dies ist das Bedienpult: Es besteht aus zwei Lampen, einem Conrad I²C LED Display, einem Kippschalter (eigentlich Polwendeschalter), eine normalen ft-Taster und einem Potentiometer. Die Bedienung ist kinderleicht: Sie basiert auf dem Koordinatensystem, d.h. Regal = x; Fach =y.
Zuerst kippt man den Schalter auf "Regal". Durch das Drehen am Poti lässt sich dann ein Wert zwischen 2 und 12 auswählen (das wird durch das Programm gesteuert). Anschließend kippt man den Schalter auf "Fach". Nun lässt sich ein Wert zwischen 1 und 4 auswählen. Der aktuelle Wert wird am LED Display angezeigt. Ist die Eingabe gültig und abgeschlossen, drückt man den roten Taster und die Maschine führt den Auftrag aus.

Während der Eingabe durch den Benutzer durchsucht das Programm im Hintergrund seine Datenbank und prüft, ob die aktuelle Eingabe gültig ist. Dies wird dann durch die grüne Lampe signalisiert.