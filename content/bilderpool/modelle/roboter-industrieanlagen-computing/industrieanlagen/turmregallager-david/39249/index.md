---
layout: "image"
title: "Annahmestelle"
date: "2014-08-20T14:19:12"
picture: "trl05.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39249
- /details6d89.html
imported:
- "2019"
_4images_image_id: "39249"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39249 -->
Dies ist die Annahmestelle. Die Zahlen dienen dem Benutzer zur Eingabe seines Auftrages.