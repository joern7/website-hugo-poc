---
layout: "image"
title: "Bedienfeld"
date: "2014-08-20T14:19:12"
picture: "trl10.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39254
- /details267a.html
imported:
- "2019"
_4images_image_id: "39254"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39254 -->
Mit diesem Bedienfeld lassen sich Aufträge (ein / auslagern) auswählen und die aktuelle Position der Maschine überwachen. Durch Bedienanweisungen wird die Bedienung vereinfacht.