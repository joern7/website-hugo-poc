---
layout: "comment"
hidden: true
title: "19425"
date: "2014-08-20T18:11:35"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Mir fällt gerade ein: Zu dem Zeitpunkt des Screenshots war das Programm ausgeschaltet, ansonsten stünde in der Box Auftrag "Tonne umlagern", da der Schieberegler gerade auf dieser Option eingestellt ist.