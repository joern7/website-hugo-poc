---
layout: "overview"
title: "Turmregallager (david)"
date: 2020-02-22T08:04:32+01:00
legacy_id:
- /php/categories/2937
- /categories25d4.html
- /categoriesdec9.html
- /categoriesd731-2.html
- /categoriesdf3d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2937 --> 
Dies ist mein Turmregallager, das ich auch schon am Fischertechnik FanClub Tag 2014 ausgestellt habe. Ein besonderes Merkmal ist die runde Form, die zum einen kurze Fahrwege der Regalbedienung, als auch platzsparende Lagerung ermöglicht...