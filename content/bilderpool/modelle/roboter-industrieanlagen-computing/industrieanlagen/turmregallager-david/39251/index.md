---
layout: "image"
title: "Regalbedieung"
date: "2014-08-20T14:19:12"
picture: "trl07.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39251
- /detailsa893.html
imported:
- "2019"
_4images_image_id: "39251"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39251 -->
Die Regalbedienung hat drei Achsen:

1. C-Achse zum Drehen
2. Vertikale Achse zum Anheben und Absenken der Gabel
3. Achse in horizontaler Richtung zum Aufnehmen oder Ablegen der gelben ft-Tonnen