---
layout: "image"
title: "Portalwaschanlage"
date: "2017-05-21T13:53:33"
picture: "pwa1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45913
- /detailsade1-2.html
imported:
- "2019"
_4images_image_id: "45913"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45913 -->
Übersicht der Portalwaschanlage:

Die Portalwaschanalage reinigt beliebige Fahrzeuge. Zunächst wird die Fahrzeugkontur über zwei Laserlichtschranken erfasst. Gleichzeitig wird auch die Breit und der Radstand gemessen. Im zweiten Durchlauf wird das Fahrzeug gereinigt. Die horizontale Walze passt sich der Fahrzeugkontur an, die vertikale passt sich der Fahrzeugbreite an. Zwei zusätzliche Bürsten reinigen die Felgen des PKW. Abschließend wird das Heck des Fahrzeugs gereinigt, indem die vertikalen Walzen aufeinander zulaufen.

In folgendem Video sind die Funktionen dargestellt: https://youtu.be/Lc_4gvUTjd8

