---
layout: "image"
title: "regal1"
date: "2005-01-02T15:48:44"
picture: "regal1.JPG"
weight: "7"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3444
- /details4c50.html
imported:
- "2019"
_4images_image_id: "3444"
_4images_cat_id: "319"
_4images_user_id: "5"
_4images_image_date: "2005-01-02T15:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3444 -->
