---
layout: "image"
title: "Druckstation (in Arbeit)"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine3.jpg"
weight: "3"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- /php/details/14125
- /details7386.html
imported:
- "2019"
_4images_image_id: "14125"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14125 -->
