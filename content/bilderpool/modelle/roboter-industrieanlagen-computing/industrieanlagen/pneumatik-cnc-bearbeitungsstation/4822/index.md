---
layout: "image"
title: "Optoelektronische Abtastung Ecke"
date: "2005-09-25T14:03:00"
picture: "Opto.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4822
- /details302f.html
imported:
- "2019"
_4images_image_id: "4822"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-09-25T14:03:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4822 -->
