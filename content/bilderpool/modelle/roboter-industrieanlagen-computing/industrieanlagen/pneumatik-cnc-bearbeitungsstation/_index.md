---
layout: "overview"
title: "Pneumatik CNC-Bearbeitungsstation"
date: 2020-02-22T08:03:35+01:00
legacy_id:
- /php/categories/362
- /categoriesdfc3.html
- /categories99c6.html
- /categories7521.html
- /categories514b.html
- /categoriesfc48.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=362 --> 
CNC-Bearbeitungsstation mit Presse, Fräse, Härteofen und Kühlung. Aussortierung und Rückführung in den Kreislauf.