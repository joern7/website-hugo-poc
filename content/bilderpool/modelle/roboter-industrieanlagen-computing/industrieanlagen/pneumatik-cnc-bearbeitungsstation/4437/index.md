---
layout: "image"
title: "Härteofen"
date: "2005-06-12T17:43:32"
picture: "Ofen_2.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4437
- /details0265-3.html
imported:
- "2019"
_4images_image_id: "4437"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T17:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4437 -->
