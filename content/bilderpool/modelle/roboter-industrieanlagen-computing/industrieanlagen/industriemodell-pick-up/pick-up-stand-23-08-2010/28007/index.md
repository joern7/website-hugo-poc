---
layout: "image"
title: "E-Magnet"
date: "2010-08-28T14:01:14"
picture: "pickup12.jpg"
weight: "12"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28007
- /detailsf3d8.html
imported:
- "2019"
_4images_image_id: "28007"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28007 -->
Hier seht ihr noch mal den E-Magnet, und das Ritzel für den Drehkranz.