---
layout: "image"
title: "Pick-up mit Förderband"
date: "2010-08-28T14:01:13"
picture: "pickup01.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27996
- /detailsed71.html
imported:
- "2019"
_4images_image_id: "27996"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27996 -->
Hier könnt ihr meine Pick up sehen (von 23.08.2010). Er kann die gelben FT-Tonnen hochheben und auf dan Tisch stellen.  Er wird über den Hauptschalter (3Bilder weiter) eingeschalten und wenn eine Tonne auf Band gestellt wird, dann kommt der Pick-up, hebt die Tonne hoch und stellt sie auf dem Tisch ab.