---
layout: "image"
title: "Endschalter"
date: "2010-08-28T14:01:14"
picture: "pickup09.jpg"
weight: "9"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28004
- /details102b.html
imported:
- "2019"
_4images_image_id: "28004"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28004 -->
Hier ist der Endschalter für die Drehung.