---
layout: "image"
title: "Industriemodell Pick-up"
date: "2010-08-26T17:40:16"
picture: "pickup7.jpg"
weight: "7"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27962
- /details1bd2-3.html
imported:
- "2019"
_4images_image_id: "27962"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27962 -->
Hier kann man ihn sehen, wie er gerade eine Tonne vom Förderband aus nimmt

Ich hoffe euch hats gefallen.

PS: Das ist die erste Version, er ist noch nicht ganz fertig, weitere Bilder folgen.