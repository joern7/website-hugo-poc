---
layout: "image"
title: "P/F-Arm oben"
date: "2011-07-27T13:51:08"
picture: "bild19.jpg"
weight: "19"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31385
- /detailsa1d8-4.html
imported:
- "2019"
_4images_image_id: "31385"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31385 -->
Hier ist das Getriebe und der Impulstaster für die Hoch/Runter-Bewegung zu sehen