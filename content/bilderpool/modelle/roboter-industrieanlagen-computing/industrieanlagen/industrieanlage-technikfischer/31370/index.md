---
layout: "image"
title: "Vorrat von oben"
date: "2011-07-27T13:51:08"
picture: "bild04.jpg"
weight: "4"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31370
- /details2ac3-2.html
imported:
- "2019"
_4images_image_id: "31370"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31370 -->
Der Vorrat, man sieht die Lichtschranke, die schaut, ob noch was zu bearbeiten da ist