---
layout: "image"
title: "P/F-Arm"
date: "2011-07-27T13:51:08"
picture: "bild14.jpg"
weight: "14"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31380
- /details323d-2.html
imported:
- "2019"
_4images_image_id: "31380"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31380 -->
Dieser Arm ist das Prunkstück des Modells. Er kann sich drehen und hoch/runter bewegen. Die Echtheit kommt nicht zu kurz
Der seitliche Arm links dient als Endtaster und zum weiterschieben der Steine