---
layout: "image"
title: "Schweißen"
date: "2011-07-27T13:51:08"
picture: "bild10.jpg"
weight: "10"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31376
- /details4a71.html
imported:
- "2019"
_4images_image_id: "31376"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31376 -->
Wenn die Lichtschranke unterbrochen wird, geht der ganze Schweißvorgang los