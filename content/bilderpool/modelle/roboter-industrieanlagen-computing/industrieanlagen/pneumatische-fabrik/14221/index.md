---
layout: "image"
title: "Schweißstation"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik06.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14221
- /detailsb340-2.html
imported:
- "2019"
_4images_image_id: "14221"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14221 -->
Auf diesem Bild sieht man die Schweißstation mit dem Lüfter.