---
layout: "image"
title: "Lüfter"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik08.jpg"
weight: "8"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14223
- /details5991.html
imported:
- "2019"
_4images_image_id: "14223"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14223 -->
Wenn geschweißt wird, geht der Lüfter an und kühlt das Werkstück, damit es nicht zu heiß wird.