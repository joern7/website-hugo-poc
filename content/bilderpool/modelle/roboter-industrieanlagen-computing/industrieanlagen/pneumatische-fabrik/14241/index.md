---
layout: "image"
title: "Kabel"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik26.jpg"
weight: "26"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14241
- /details5423.html
imported:
- "2019"
_4images_image_id: "14241"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14241 -->
Auf diesem Bild sieht man Kabel und ein Magnetventil.