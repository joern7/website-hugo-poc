---
layout: "image"
title: "Magnetventile"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik30.jpg"
weight: "30"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14245
- /details856a-3.html
imported:
- "2019"
_4images_image_id: "14245"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14245 -->
Hier sieht man noch zwei Magnetventile und Links ist der Summer.