---
layout: "image"
title: "ausgefahrener Schweißer"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik07.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14222
- /details10ae.html
imported:
- "2019"
_4images_image_id: "14222"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14222 -->
Hier sieht man den pneumatisch angetriebenen Schweissapparat.