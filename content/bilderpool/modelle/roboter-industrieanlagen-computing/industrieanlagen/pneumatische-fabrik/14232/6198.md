---
layout: "comment"
hidden: true
title: "6198"
date: "2008-04-12T13:55:14"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Es ist genau anders herum: einen 1:1 gibt es nicht, nur einen 8:1.
Das ganze ist aber ein sehr schönes Modell!
Weiter so!