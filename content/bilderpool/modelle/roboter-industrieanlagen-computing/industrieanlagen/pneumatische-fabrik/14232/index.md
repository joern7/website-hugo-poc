---
layout: "image"
title: "Kompressor"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik17.jpg"
weight: "17"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14232
- /details17fb.html
imported:
- "2019"
_4images_image_id: "14232"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14232 -->
Da alle Station der Fabrik pneumatisch angetrieben werden benötigt sie viel Druck. Deswegen habe ich einen Kompressor mit dem Power Motor 8:1 gebaut