---
layout: "image"
title: "Kästchen-Wechsler"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik18.jpg"
weight: "18"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14233
- /details3da6.html
imported:
- "2019"
_4images_image_id: "14233"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14233 -->
Und hier in der dritten Position.