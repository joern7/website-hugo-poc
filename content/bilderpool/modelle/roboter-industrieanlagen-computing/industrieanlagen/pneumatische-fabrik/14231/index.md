---
layout: "image"
title: "Kompressor"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik16.jpg"
weight: "16"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14231
- /details5aba.html
imported:
- "2019"
_4images_image_id: "14231"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14231 -->
Auf diesem Bild sieht man den anderen Kompressor der noch zusätzlich Druck macht.