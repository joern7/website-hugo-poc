---
layout: "image"
title: "pneumatischer Schieber"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik04.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14219
- /detailsedbf.html
imported:
- "2019"
_4images_image_id: "14219"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14219 -->
Hier sieht man den pneumatischen Schieber, der die Werkstücke aus dem Magazin in den Rundtakttisch schiebt.