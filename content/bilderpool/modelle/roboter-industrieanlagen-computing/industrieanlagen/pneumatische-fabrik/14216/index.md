---
layout: "image"
title: "pneumatische Fabrik"
date: "2008-04-12T09:19:09"
picture: "pneumatischefabrik01.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14216
- /detailsa954.html
imported:
- "2019"
_4images_image_id: "14216"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14216 -->
Das ist ein Gesamtbild meiner pneumatischen Fabrik.