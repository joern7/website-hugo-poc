---
layout: "image"
title: "Gesamtbild von oben."
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik33.jpg"
weight: "33"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14248
- /details83c6-2.html
imported:
- "2019"
_4images_image_id: "14248"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14248 -->
Das ist ein Gesamtbild von oben fotografiert.