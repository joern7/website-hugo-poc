---
layout: "image"
title: "Abfrage-Taster"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik13.jpg"
weight: "13"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14228
- /detailsf4a8-2.html
imported:
- "2019"
_4images_image_id: "14228"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14228 -->
Auf diesem Bild sieht man den Abfrage-Taster. Diese raus stehenden Dinger betätigen den Taster, das dass Interface weist, wann der Motor anhalten soll.