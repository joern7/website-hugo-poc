---
layout: "image"
title: "Schieber"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik12.jpg"
weight: "12"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14227
- /details42ba-2.html
imported:
- "2019"
_4images_image_id: "14227"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14227 -->
Hier den Schieber  in eingefahrener Position.