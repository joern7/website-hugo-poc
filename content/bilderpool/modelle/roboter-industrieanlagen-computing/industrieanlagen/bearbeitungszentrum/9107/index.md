---
layout: "image"
title: "Bearbeitungszentrum 8"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9107
- /details6bd9.html
imported:
- "2019"
_4images_image_id: "9107"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9107 -->
