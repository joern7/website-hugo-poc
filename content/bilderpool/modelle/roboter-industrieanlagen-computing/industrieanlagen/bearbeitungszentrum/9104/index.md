---
layout: "image"
title: "Bearbeitungszentrum 5"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9104
- /detailsf103.html
imported:
- "2019"
_4images_image_id: "9104"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9104 -->
