---
layout: "image"
title: "Bearbeitungszentrum 6"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9105
- /detailse4da.html
imported:
- "2019"
_4images_image_id: "9105"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9105 -->
