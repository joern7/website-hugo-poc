---
layout: "image"
title: "Gesamtansicht"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat01.jpg"
weight: "1"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33454
- /details670e.html
imported:
- "2019"
_4images_image_id: "33454"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33454 -->
Horizontal verläuft die Führungsschiene, auf der die Becherhalterung für einen 2cl Einweg-Becher (Probierbecher) wie bei einer Laufkatze eines Kranes mit 2 Schnüren gezogen wird.

Links: Geldeinwurf für 50 Cent-Münzen; Mixer auf Hubschiene, das an einem Aluprofil befestigt ist

Mitte: Förderkette an der 2 gegenüberliegende Schaufeln angebracht sind, die das Eisteegranulat aus dem hinteren Behälter nach vorne befördern und in einen Trichter kippen (Trichter nicht im Bild, da Edelstahl das Licht zu start zurückwirft)

Rechts: Wasserflasche, in die Druckluft geleitet wird, welche das Wasser herausdrückt. Der Abstandssensor misst die Füllhöhe, der Not-Aus-Buzzer stoppt den Vorgang, falls etwas daneben gehen sollte