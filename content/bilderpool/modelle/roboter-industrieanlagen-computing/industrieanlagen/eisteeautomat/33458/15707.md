---
layout: "comment"
hidden: true
title: "15707"
date: "2011-11-16T14:11:05"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Hallo Lukas,

hast du denn keine Probleme wenn du mit einer Glasflasche arbeitest? Die kann sich ja nicht ausdehen und könnte bei Druck ja platzen.