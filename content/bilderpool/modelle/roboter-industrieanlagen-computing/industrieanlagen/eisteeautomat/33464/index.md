---
layout: "image"
title: "von vorne"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat11.jpg"
weight: "11"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33464
- /details8c81.html
imported:
- "2019"
_4images_image_id: "33464"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33464 -->
Die Technik (Interface+EM+Kompressor+Trafo) steckt unter der Maschine, ich habe leider vergessen ein Foto davon zu machen, werde es aber demnächst noch nachreichen. Es soll jetzt auch noch ein Arduino Uno dazukommen, der die benötigten Motor-Ausgänge erweitert, denn es ist ein Portalkran geplant, der einen Becher über einen Vakuumsauger in die Halterung stellt. Der jetztige Kompressor wird dann durch eine Luftpumpte von Pollin ersetzt und der ftKompressor mit den Spezialteilen von Andreas Tacke verwandelt sich in eine Vakuumpumpe.