---
layout: "image"
title: "Eisteebehälter"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat09.jpg"
weight: "9"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33462
- /details2c22.html
imported:
- "2019"
_4images_image_id: "33462"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33462 -->
Power-Motor (50:1) treibt die Kette mit den Schaufeln an.

(Nachtrag: Die Schleichwerbung ist unbeabsichtigt! :D)