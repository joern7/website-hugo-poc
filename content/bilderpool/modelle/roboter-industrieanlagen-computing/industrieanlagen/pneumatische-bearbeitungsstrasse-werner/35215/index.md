---
layout: "image"
title: "Stromversorgung"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse02.jpg"
weight: "2"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35215
- /details4a81.html
imported:
- "2019"
_4images_image_id: "35215"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35215 -->
Ich hab die Erfahrung gemacht, dass die 12V Ventile von ft bei 9V nicht immer schalten, darum 10V.