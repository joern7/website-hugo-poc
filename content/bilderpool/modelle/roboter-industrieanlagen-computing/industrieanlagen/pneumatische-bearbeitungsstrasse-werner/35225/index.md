---
layout: "image"
title: "Drehtisch"
date: "2012-07-30T18:30:30"
picture: "pneumatischebearbeitungsstrasse12.jpg"
weight: "12"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35225
- /detailscb71.html
imported:
- "2019"
_4images_image_id: "35225"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35225 -->
