---
layout: "image"
title: "Das Magazin"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse08.jpg"
weight: "8"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35221
- /detailsf0c6.html
imported:
- "2019"
_4images_image_id: "35221"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35221 -->
