---
layout: "image"
title: "Verteilerbox aufgezoomt"
date: "2012-08-04T12:48:47"
picture: "pneumatischebearbeitungsstrasse1_2.jpg"
weight: "23"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35236
- /details3766.html
imported:
- "2019"
_4images_image_id: "35236"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-08-04T12:48:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35236 -->
Man sieht hier wie das Messingteil das Holz "verdrängt"
Größenvergleich: das Loch für die Stecker hat einen Durchmesser von 2,5mm