---
layout: "image"
title: "Verkabelung Schweißroboter"
date: "2012-08-01T13:07:11"
picture: "pneumatischebearbeitungsstrasse2.jpg"
weight: "21"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35234
- /details63e6-3.html
imported:
- "2019"
_4images_image_id: "35234"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-08-01T13:07:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35234 -->
Lichtschranke, Transportbandantrieb und Schweißlämpchen