---
layout: "image"
title: "Die 'hässliche' Seite"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse8.jpg"
weight: "8"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/43811
- /detailsb62b.html
imported:
- "2019"
_4images_image_id: "43811"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43811 -->
Auf der Rückseite sitzen Der TX-C, magnetventile für Zylinder und Düse, und der Kompressor. Und natürlich Kabelsalat, wobei der hinter dem TX und dem grünen Magnetventil versteckt ist :)