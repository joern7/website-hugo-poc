---
layout: "image"
title: "Die Düse I"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse3.jpg"
weight: "3"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/43806
- /details83de.html
imported:
- "2019"
_4images_image_id: "43806"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43806 -->
Hier ist die bewegliche Düse zu sehen. Sie fährt mithilfe der Zahnstange zwischen den beiden Endtastern hin und her und "besprüht" die Werkstücke mit Druckluft.