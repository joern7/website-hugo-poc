---
layout: "image"
title: "Bearbeitungszentrum 010"
date: "2011-10-20T17:10:01"
picture: "FT_Derk_010.jpg"
weight: "11"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/33266
- /details1a43.html
imported:
- "2019"
_4images_image_id: "33266"
_4images_cat_id: "635"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T17:10:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33266 -->
