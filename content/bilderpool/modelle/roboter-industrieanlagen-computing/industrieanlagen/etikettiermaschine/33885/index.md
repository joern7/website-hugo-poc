---
layout: "image"
title: "Etikettiermaschine"
date: "2012-01-12T16:52:50"
picture: "etikettirmaschine01.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33885
- /details6123.html
imported:
- "2019"
_4images_image_id: "33885"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33885 -->
Die Maschine presst jeweils eine Bauplatte 15 x15 (ArtNr.31506) auf einen Grundbaustein 30 (ArtNr.31003 od. 32879) . Links befindet sich das Magazin mit den Grundbausteinen. Computergesteuert fällt jeweils ein Grundbaustei auf ein Förderband. Vor dem zweiten Magazin mit den Bauplatten stoppt er und wird durch die vor dem Magazin liegende Justiereinrichtung so in Stellung gebracht, dass die nun ausgeworfene Bauplatte mit ihrem Klipp genau über der Nut des Grundbausteins zu liegen kommt. Beim Weitertranspoert passiert diese Einheit aus Grundbaustein und justierter Bauplatte eine Presse (rechts neben den beiden Magazinen), die die Bauplatte auf den Grundbaustein presst. Am Ende des Förderbandes rutschen die "etikettierten Grundbausteine" auf die Ablageschiene (ganz rechts), wo sie durch einen Schieber geordnet abgelegt werden. Ein Video, das die Maschine in Betrieb zeigt, kann in Youtube unter "fischertechnik etikettiermaschine.avi" angeschaut werden.