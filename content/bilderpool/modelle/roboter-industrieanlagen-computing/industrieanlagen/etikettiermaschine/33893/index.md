---
layout: "image"
title: "Etikettiermaschien"
date: "2012-01-12T16:52:51"
picture: "etikettirmaschine09.jpg"
weight: "9"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33893
- /details4fb7-2.html
imported:
- "2019"
_4images_image_id: "33893"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33893 -->
Presse von oben