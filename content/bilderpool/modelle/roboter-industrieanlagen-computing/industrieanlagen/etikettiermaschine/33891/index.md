---
layout: "image"
title: "Ettikettiermaschine"
date: "2012-01-12T16:52:51"
picture: "etikettirmaschine07.jpg"
weight: "7"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33891
- /details50ef.html
imported:
- "2019"
_4images_image_id: "33891"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33891 -->
Rutsche und Schieber für Ablage von hinten