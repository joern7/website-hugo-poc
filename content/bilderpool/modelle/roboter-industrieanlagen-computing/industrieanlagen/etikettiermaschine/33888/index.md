---
layout: "image"
title: "Etikettiermaschine"
date: "2012-01-12T16:52:51"
picture: "etikettirmaschine04.jpg"
weight: "4"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33888
- /details003a.html
imported:
- "2019"
_4images_image_id: "33888"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33888 -->
Magazin für Grunbausteine mit Auswerfereinheit