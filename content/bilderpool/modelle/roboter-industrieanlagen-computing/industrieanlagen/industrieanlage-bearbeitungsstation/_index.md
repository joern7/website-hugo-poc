---
layout: "overview"
title: "Industrieanlage mit Bearbeitungsstation und Brennofen"
date: 2020-02-22T08:04:30+01:00
legacy_id:
- /php/categories/2802
- /categories85ba.html
- /categoriesc4c6.html
- /categories7c54.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2802 --> 
kleine Industrienanlage bestehend aus einem Fertigungsteil und einem Brennofen.
Die Steuerung der Fertigung erfolgt automatisch. Der Ofen und seine dazugehörigen Fließbänder werden manuell vom Bediener gesteuert.
Der Elektronische Teil befindet sich unter der Anlage.
Video: http://www.youtube.com/watch?v=5Lqh-RQF7dw