---
layout: "image"
title: "Detail fahrbahres Fließband"
date: "2013-10-12T14:55:42"
picture: "industrieanlage09.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37709
- /detailsf608-2.html
imported:
- "2019"
_4images_image_id: "37709"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37709 -->
Ansicht des Fließbandes von der anderen Seite. zu erkennen ist der XS-Motor