---
layout: "image"
title: "Starttaster offen"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse03.jpg"
weight: "3"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13685
- /detailsc41d-2.html
imported:
- "2019"
_4images_image_id: "13685"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13685 -->
