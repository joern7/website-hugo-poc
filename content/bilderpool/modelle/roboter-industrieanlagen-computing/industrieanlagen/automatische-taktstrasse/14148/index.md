---
layout: "image"
title: "Ein im Kreis Angeordnetes Förderband"
date: "2008-04-02T14:33:02"
picture: "modelle1.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/14148
- /details4c54.html
imported:
- "2019"
_4images_image_id: "14148"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-04-02T14:33:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14148 -->
Kenn jemand den Anschluss im Vordergrund?