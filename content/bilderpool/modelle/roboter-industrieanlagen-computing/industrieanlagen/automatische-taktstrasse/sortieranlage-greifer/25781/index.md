---
layout: "image"
title: "Stein halter"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer2.jpg"
weight: "2"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- /php/details/25781
- /details0c9a.html
imported:
- "2019"
_4images_image_id: "25781"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25781 -->
Hält den Stein  bis er von dem Greifer genommen wird