---
layout: "image"
title: "Gesamtansicht"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer6.jpg"
weight: "6"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- /php/details/25785
- /details6a5e.html
imported:
- "2019"
_4images_image_id: "25785"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25785 -->
