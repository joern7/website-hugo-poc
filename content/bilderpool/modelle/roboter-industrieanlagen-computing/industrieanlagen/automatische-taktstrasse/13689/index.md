---
layout: "image"
title: "Mehrspindeleinheit"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse07.jpg"
weight: "7"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13689
- /details1d42-2.html
imported:
- "2019"
_4images_image_id: "13689"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13689 -->
