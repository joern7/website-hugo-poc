---
layout: "image"
title: "Das Greifwerkzeug 3"
date: "2008-09-03T18:23:31"
picture: "produktionmaschine05.jpg"
weight: "5"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15165
- /detailsb46e.html
imported:
- "2019"
_4images_image_id: "15165"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15165 -->
Die Mitte der Greifer. Hier kommt ein Motor hin, der die Beiden Greifer gleichzeitig steuert