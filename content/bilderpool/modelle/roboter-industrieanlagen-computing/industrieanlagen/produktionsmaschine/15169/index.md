---
layout: "image"
title: "Die Stanze 2"
date: "2008-09-03T18:23:32"
picture: "produktionmaschine09.jpg"
weight: "9"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15169
- /details2663.html
imported:
- "2019"
_4images_image_id: "15169"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:32"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15169 -->
Hier kommt das Prinzip einer Nähmaschine zum Einsatz