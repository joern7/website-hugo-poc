---
layout: "image"
title: "Das Greifwerkzeug 4"
date: "2008-09-03T18:23:31"
picture: "produktionmaschine06.jpg"
weight: "6"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15166
- /detailsd4df.html
imported:
- "2019"
_4images_image_id: "15166"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15166 -->
Nochmals aus einen anderen Blickwinkel