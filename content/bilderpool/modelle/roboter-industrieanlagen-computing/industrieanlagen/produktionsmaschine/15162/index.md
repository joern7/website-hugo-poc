---
layout: "image"
title: "Die Arbeit Paletten Station"
date: "2008-09-03T18:23:31"
picture: "produktionmaschine02.jpg"
weight: "2"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15162
- /detailsf093-2.html
imported:
- "2019"
_4images_image_id: "15162"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15162 -->
Der Antrieb, welches die Kette gleichmäßig bewegt.