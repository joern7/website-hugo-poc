---
layout: "image"
title: "Interface + EM + Soundmodul + PowerController"
date: "2011-07-08T18:00:37"
picture: "chekmaker20.jpg"
weight: "24"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31011
- /details4ee5.html
imported:
- "2019"
_4images_image_id: "31011"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31011 -->
Ich habe für das Modell 2 Netzteile verwendet. Die Hauptsteuerung übernimmt das Interface. Da die Motorausgänge nicht genug waren, habe ich das Interface mit einem ErweiterungsModul "erweitert". (Man kann vielleicht erkennen, das jetzt 8 Motorausgänge [IF+EM] belegt sind)

Das Soundmodul sorgt dafür, dass nach der Beendung der Produktion ein Ton ausgegeben wird. Der PowerController versorgt das EM und das Soundmodul mit Strom, ein 2. Netzteil liefert die benötigte Energie für das Interface.