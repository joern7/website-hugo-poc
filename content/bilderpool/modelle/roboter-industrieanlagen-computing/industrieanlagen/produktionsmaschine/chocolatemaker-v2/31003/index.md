---
layout: "image"
title: "Pulverbehälter"
date: "2011-07-08T18:00:37"
picture: "chekmaker12.jpg"
weight: "16"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31003
- /details2e31-3.html
imported:
- "2019"
_4images_image_id: "31003"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31003 -->
Das ist der Pulverspeicher - Links Eiskaffee und rechts Kaba. Das Rad ist zur Auflockerung des Pulvers gedacht, da Kaba immer sehr schnell verklumpt.