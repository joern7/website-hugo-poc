---
layout: "image"
title: "Förderbandantrieb"
date: "2011-07-08T18:00:37"
picture: "chekmaker19.jpg"
weight: "23"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31010
- /details4b89.html
imported:
- "2019"
_4images_image_id: "31010"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31010 -->
Mit diesem Motor wird der Becher in Bewegung gesetzt.