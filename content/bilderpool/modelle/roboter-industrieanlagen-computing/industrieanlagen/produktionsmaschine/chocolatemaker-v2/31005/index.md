---
layout: "image"
title: "PM für Pulverausgabe"
date: "2011-07-08T18:00:37"
picture: "chekmaker14.jpg"
weight: "18"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31005
- /details6976.html
imported:
- "2019"
_4images_image_id: "31005"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31005 -->
Der Powermotor, der sich in die eine Richtung dreht, um mit der Schnecke Kaba oder in die andere Richtung um Eiskaffee in die Schaufeln auf der Kette zu fördern.