---
layout: "image"
title: "Komplettansicht"
date: "2011-07-08T18:00:37"
picture: "chekmaker01.jpg"
weight: "5"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30992
- /detailsd570.html
imported:
- "2019"
_4images_image_id: "30992"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30992 -->
Die komplette Maschine von vorne