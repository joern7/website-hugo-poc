---
layout: "image"
title: "Becherantrieb"
date: "2011-06-14T22:35:23"
picture: "chocolatemakerv2.jpg"
weight: "2"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30859
- /details3268.html
imported:
- "2019"
_4images_image_id: "30859"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30859 -->
Der Becher auf dem Antrieb (wie Laufkatze)