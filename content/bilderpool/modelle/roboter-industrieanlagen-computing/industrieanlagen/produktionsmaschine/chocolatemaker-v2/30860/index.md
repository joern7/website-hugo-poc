---
layout: "image"
title: "Becherantrieb 2"
date: "2011-06-14T22:35:23"
picture: "chocolatemakerv3.jpg"
weight: "3"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30860
- /details82b3-2.html
imported:
- "2019"
_4images_image_id: "30860"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30860 -->
Hier ist erneut der Becherantrieb zu sehen. Im Hintergrund kann man das Unterteil des Mixers erkennen.