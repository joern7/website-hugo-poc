---
layout: "image"
title: "Das Greifwerkzeug (2)"
date: "2008-09-03T18:23:31"
picture: "produktionmaschine04.jpg"
weight: "4"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15164
- /detailsa3ff-2.html
imported:
- "2019"
_4images_image_id: "15164"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15164 -->
Hier wird der Rohling Festgehalten und bearbeitet. (aus einen näheren Blickwinkel)