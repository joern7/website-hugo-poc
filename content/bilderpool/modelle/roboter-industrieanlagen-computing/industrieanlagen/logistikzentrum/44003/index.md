---
layout: "image"
title: "Kabelführung"
date: "2016-07-25T16:45:59"
picture: "logzen04.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44003
- /detailsbdb3-2.html
imported:
- "2019"
_4images_image_id: "44003"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44003 -->
Flexschiene als Kabeltunnel