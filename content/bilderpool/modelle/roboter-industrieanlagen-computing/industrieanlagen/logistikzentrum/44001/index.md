---
layout: "image"
title: "Überblick"
date: "2016-07-25T16:45:59"
picture: "logzen02.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44001
- /detailsdbf0-2.html
imported:
- "2019"
_4images_image_id: "44001"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44001 -->
oben: Hochregallager mit Regalbediengerät
mitte: Drehtisch als Schnittstelle zwischen Warenannahme / Warenausgabe und dem Lager
links: Warenannahme mit Roboter zur Zulieferung zum Drehtisch
rechts: Warenausgabe mit Robter zum Abtransport vom Drehtisch