---
layout: "comment"
hidden: true
title: "22892"
date: "2017-01-09T20:51:20"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

ich nutze einen Trick, um den TX mit dem Robo IF kommunizieren zu lassen. Da es m.W. nach kein gutes Protokoll zur Kommunikation zwischen den Controllern gibt, werden die Daten sozusagen gemorst. Die Lampe in der Benutzersteuerung gehört zum TX, der Fototransistor zum Robo IF. Gleichzeitig nimmt die Lampe eine Doppelfunktion ein und zeigt dem Nutzer über einen Blinkcode den aktuellen Programmstatus an.
Das alles funktioniert, ist aber nicht schön. Im Moment arbeite ich daran, den Arduino als Schnittstelle zu verwenden, um dann eine normale I2C Kommunikation aufzubauen.

Grüße
David