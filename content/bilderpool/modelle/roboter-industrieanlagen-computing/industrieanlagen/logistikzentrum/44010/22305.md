---
layout: "comment"
hidden: true
title: "22305"
date: "2016-07-26T18:30:44"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist ja mal fein gemacht! Nicht nur dass mal die feinen Zahnstangen anders als für Hubgetriebe zum Einsatz kommen, sondern Du hast da auch noch Anfahr- und Bremsrampen programmiert? Starke Sache.

Gruß,
Stefan