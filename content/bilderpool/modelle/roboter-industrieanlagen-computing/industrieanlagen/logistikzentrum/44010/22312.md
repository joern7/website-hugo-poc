---
layout: "comment"
hidden: true
title: "22312"
date: "2016-07-26T21:17:36"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ein sehr eleganter Übergang vom Modul 1,5 auf's Modul 0,5. Du hast Dir echt Gedanken gemacht.

Grüße
H.A.R.R.Y.