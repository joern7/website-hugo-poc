---
layout: "image"
title: "Insgesamt"
date: "2008-03-07T07:03:13"
picture: "olli01.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13844
- /detailsa29a.html
imported:
- "2019"
_4images_image_id: "13844"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13844 -->
Das ist das Modell, welches ich auf der Ausstellung in Greven dabeihatte. Es transportiert Kisten bearbeitet sie und sortiert sie nach Farbe (gelb, grau). So ein ähnliches Modell hatte ich schon mal gebaut (=> Fehlversuche).  Das Modell hat 8 Motoren. Aber da ich nur 4 Ausgänge zur Verfügung habe musste ich mit Dioden arbeiten. Somit ist der Motor für die Laufkatze alleine gesteuert. Der Motor für das Band darauf ist mit dem Bandmotor des 1. Drehtisches gesteuert, wobei der mit 4 Dioden so gesteuert ist, dass er immer in nur in die gleiche Richtung läuft. Dann die 2. & 3. Bänder werden von einem Ausgang über mit Dioden gesteuert. Die andere Laufrichtung wird für das Bearbeiten und die Farbmessung benutzt. Die Drehmotoren der Tische sind auch zusammengesteuert.