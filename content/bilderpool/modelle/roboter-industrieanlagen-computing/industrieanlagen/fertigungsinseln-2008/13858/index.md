---
layout: "image"
title: "Taster"
date: "2008-03-07T07:03:14"
picture: "olli15.jpg"
weight: "15"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13858
- /details543f.html
imported:
- "2019"
_4images_image_id: "13858"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13858 -->
Nochmal.