---
layout: "image"
title: "Bild"
date: "2008-03-07T07:03:14"
picture: "olli18.jpg"
weight: "18"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13861
- /detailse9a3-2.html
imported:
- "2019"
_4images_image_id: "13861"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13861 -->
Mit Lampen...