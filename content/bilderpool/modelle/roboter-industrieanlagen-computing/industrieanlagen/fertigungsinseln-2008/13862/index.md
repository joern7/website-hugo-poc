---
layout: "image"
title: "Bild"
date: "2008-03-07T07:03:14"
picture: "olli19.jpg"
weight: "19"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13862
- /details9554-2.html
imported:
- "2019"
_4images_image_id: "13862"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13862 -->
Mit Lampen.