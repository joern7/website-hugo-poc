---
layout: "image"
title: "Bohrmaschine"
date: "2008-03-07T07:03:14"
picture: "olli07.jpg"
weight: "7"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13850
- /detailse845.html
imported:
- "2019"
_4images_image_id: "13850"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13850 -->
Oder was auch immer.