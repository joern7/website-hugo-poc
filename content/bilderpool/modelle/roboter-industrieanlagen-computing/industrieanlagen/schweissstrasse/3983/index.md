---
layout: "image"
title: "Schweißstraße02"
date: "2005-04-17T11:48:45"
picture: "schweistrae02.jpg"
weight: "2"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3983
- /details5971.html
imported:
- "2019"
_4images_image_id: "3983"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3983 -->
Schweißstraße mit vier 3-Achs-Robotern