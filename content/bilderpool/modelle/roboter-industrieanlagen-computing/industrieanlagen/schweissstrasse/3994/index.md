---
layout: "image"
title: "Schweißstraße13"
date: "2005-04-17T11:49:02"
picture: "schweistrae13.jpg"
weight: "13"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3994
- /detailsb4b2-2.html
imported:
- "2019"
_4images_image_id: "3994"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3994 -->
Schweißstraße mit vier 3-Achs-Robotern