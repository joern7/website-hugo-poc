---
layout: "image"
title: "Schweißstraße01"
date: "2005-04-17T11:48:45"
picture: "schweistrae01.jpg"
weight: "1"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3982
- /details998f-2.html
imported:
- "2019"
_4images_image_id: "3982"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3982 -->
Schweißstraße mit vier 3-Achs-Robotern