---
layout: "image"
title: "Schweißstraße16"
date: "2005-04-17T11:49:02"
picture: "schweistrae16.jpg"
weight: "16"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3997
- /detailsc8ac.html
imported:
- "2019"
_4images_image_id: "3997"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3997 -->
Schweißstraße mit vier 3-Achs-Robotern