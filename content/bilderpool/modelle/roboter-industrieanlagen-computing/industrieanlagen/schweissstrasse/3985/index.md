---
layout: "image"
title: "Schweißstraße04"
date: "2005-04-17T11:48:45"
picture: "schweistrae04.jpg"
weight: "4"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3985
- /details21d5-3.html
imported:
- "2019"
_4images_image_id: "3985"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3985 -->
Schweißstraße mit vier 3-Achs-Robotern