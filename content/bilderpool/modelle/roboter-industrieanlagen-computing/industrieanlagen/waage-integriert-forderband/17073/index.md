---
layout: "image"
title: "2. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_2_08.jpg"
weight: "20"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17073
- /details0f8f-2.html
imported:
- "2019"
_4images_image_id: "17073"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17073 -->
Förderband ohne Ketten; Unterseite