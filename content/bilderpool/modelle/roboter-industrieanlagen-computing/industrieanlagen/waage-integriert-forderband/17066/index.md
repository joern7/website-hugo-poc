---
layout: "image"
title: "2. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_2_01.jpg"
weight: "13"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17066
- /details032d.html
imported:
- "2019"
_4images_image_id: "17066"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17066 -->
Zweite Konstruktion von vorne

Funktion:

Kassette gelang auf das Förderband. Vor dem Drehpunkt wird eine Lichtschranke unterbrochen. Ab jetzt beginnt der Zählvorgang des Förderbandes, bis es kippt.

Ungenauigkeiten können sich ergeben, da die Kassette mit der Vorderkante die Lichtschranke unterbricht, maßgebend für das ermittelte Gewicht ist allerdings der Schwerpunkt des Meßguts.

Vorteil: sehr schneller Meßvorgang. Nur ein Motor notwendig, ein Taster weniger wie bei der vorhergehenden Konstruktion.
Nachteil: relativ ungenau. Man kann aber unterscheiden, ob die Kassette leer, mit einem oder mit mehreren Bausteinen 30 gefüllt ist;

Benötigt werden:
1 Motor (Förderband),
1 Taster Impulszähler Förderband,
1 Kontakt (aus 2 Kontaktstücke 31305 gebildet) und
1 Lichtschranke.