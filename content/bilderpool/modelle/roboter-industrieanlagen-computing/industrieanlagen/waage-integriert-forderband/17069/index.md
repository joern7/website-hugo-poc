---
layout: "image"
title: "2. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_2_04.jpg"
weight: "16"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17069
- /detailsec2a.html
imported:
- "2019"
_4images_image_id: "17069"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17069 -->
Detailaufnahme Meßkontakt:
Wenn der Kontakt geschlossen wird, ist die Messung beendet.