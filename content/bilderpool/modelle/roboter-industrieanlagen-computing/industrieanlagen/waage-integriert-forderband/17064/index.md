---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_1_11.jpg"
weight: "11"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17064
- /details5e33-2.html
imported:
- "2019"
_4images_image_id: "17064"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17064 -->
Ende der Messung: Kassette mit 2 Bausteinen 30 und Taschenrechner