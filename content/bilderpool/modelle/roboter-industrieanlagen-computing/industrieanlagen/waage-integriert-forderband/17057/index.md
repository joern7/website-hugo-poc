---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:35"
picture: "Frderband__Waage_1_04.jpg"
weight: "4"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17057
- /details4cf4-2.html
imported:
- "2019"
_4images_image_id: "17057"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17057 -->
Bereit für die nächste Messung