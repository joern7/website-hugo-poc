---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:35"
picture: "Frderband__Waage_1_05.jpg"
weight: "5"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17058
- /details04d0.html
imported:
- "2019"
_4images_image_id: "17058"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17058 -->
Das Förderband von hinten