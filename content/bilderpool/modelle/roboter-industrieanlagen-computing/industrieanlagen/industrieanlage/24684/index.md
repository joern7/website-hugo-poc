---
layout: "image"
title: "Blick auf das Fließband"
date: "2009-07-29T23:29:11"
picture: "industrieanlage07.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24684
- /detailsdd19.html
imported:
- "2019"
_4images_image_id: "24684"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24684 -->
