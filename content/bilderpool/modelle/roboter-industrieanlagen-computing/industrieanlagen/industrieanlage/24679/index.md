---
layout: "image"
title: "Gesamtansicht 1"
date: "2009-07-29T23:29:11"
picture: "industrieanlage02.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24679
- /details3795.html
imported:
- "2019"
_4images_image_id: "24679"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24679 -->
