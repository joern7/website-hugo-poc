---
layout: "image"
title: "Schweißstation"
date: "2009-07-29T23:29:11"
picture: "industrieanlage06.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24683
- /details92bc.html
imported:
- "2019"
_4images_image_id: "24683"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24683 -->
