---
layout: "image"
title: "Gesamtansicht"
date: "2010-05-02T13:55:54"
picture: "fischertechnikindustriemodell1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/27036
- /detailsc466-2.html
imported:
- "2019"
_4images_image_id: "27036"
_4images_cat_id: "1946"
_4images_user_id: "453"
_4images_image_date: "2010-05-02T13:55:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27036 -->
