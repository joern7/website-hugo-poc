---
layout: "image"
title: "Übergänge"
date: "2010-05-02T13:55:54"
picture: "fischertechnikindustriemodell4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/27039
- /detailsade5.html
imported:
- "2019"
_4images_image_id: "27039"
_4images_cat_id: "1946"
_4images_user_id: "453"
_4images_image_date: "2010-05-02T13:55:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27039 -->
Die Räder fallen von dem rechten Laufband in den Drehkranz, dann fahren sie einmal rechts herum und werden mit dem gelben Hebel auf das untere Laufband geschoben