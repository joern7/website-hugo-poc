---
layout: "image"
title: "Schlitten"
date: "2010-05-02T13:55:54"
picture: "fischertechnikindustriemodell2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/27037
- /detailsae40-3.html
imported:
- "2019"
_4images_image_id: "27037"
_4images_cat_id: "1946"
_4images_user_id: "453"
_4images_image_date: "2010-05-02T13:55:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27037 -->
Der Schlitten wird durch die Kette gezogen, das Laufband darauf wird über die Linken Rastachsen angetrieben.