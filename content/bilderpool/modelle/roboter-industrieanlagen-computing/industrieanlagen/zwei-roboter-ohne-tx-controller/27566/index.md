---
layout: "image"
title: "6"
date: "2010-06-25T18:20:41"
picture: "zweiroboter6.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27566
- /detailsb5ec.html
imported:
- "2019"
_4images_image_id: "27566"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27566 -->
Der kleine Roboter noch mal von dichter dran.