---
layout: "image"
title: "Vakuumerzeuger"
date: "2018-02-15T18:59:09"
picture: "simulationsfabrikjanft11.jpg"
weight: "11"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/47297
- /details89d2.html
imported:
- "2019"
_4images_image_id: "47297"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47297 -->
Die Schnecke zieht einen alten Hydraulik-Zylinder aus und erzeugt so das Vakuum. Den Hydraulik-Zylinder habe ich genommen, da Dieser leichtgängiger ist als seine Pneumatik-Kollegen. Gesteuert wird das Ganze wie eine normale Achse mit Impulszähler und Endschalter.