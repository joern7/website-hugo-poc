---
layout: "image"
title: "Simulationsfabrik Gesamtansicht"
date: "2018-02-15T18:59:05"
picture: "simulationsfabrikjanft01.jpg"
weight: "1"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/47287
- /detailsc67d.html
imported:
- "2019"
_4images_image_id: "47287"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47287 -->
Die einzelnen Modelle der Fabrik sind angelehnt an das FT-Industriemodell.
Der Greifer stammt aus dem Industry Robots II-Set.
Der Brennofen ist komplett frei gebaut und die Sortieranlage nnach den Fotos des FT-Originalmodell