---
layout: "comment"
hidden: true
title: "1101"
date: "2006-05-17T05:01:03"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Am Ende des Transportbandes wird die Palette durch seitliche Führungen und den Endanschlag ausgerichtet. Die korrekte Ausrichtung wird durch zwei Lichtschranken geprüft. Falls die Palette nicht richtig liegt, fährt das Transportband die Palette wieder aus der Annahmestation heraus und die rote Fehlerleuchte blinkt. Stimmt, die Ausrichtung, senkt sich der pneumatische Greifer, nimmt die Palette auf und fährt zur nächsten Station.