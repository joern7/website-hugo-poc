---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre10.jpg"
weight: "10"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30115
- /detailsf0c9-2.html
imported:
- "2019"
_4images_image_id: "30115"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30115 -->
Ein Transportband prüft, befördert z.B. ein Metallteil  welches dann in verschiedene Magazine verbracht werden kann.
Der Dreiachskran kann mittels Elektromagnet das Werkstück an jeden beliebigen Platz des Modells bringen.