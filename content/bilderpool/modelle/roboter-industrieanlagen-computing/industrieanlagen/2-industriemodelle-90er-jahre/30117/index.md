---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:51:09"
picture: "industriemodellederzigerjahre12.jpg"
weight: "12"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30117
- /detailsef3c-2.html
imported:
- "2019"
_4images_image_id: "30117"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:51:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30117 -->
Der Dreiachskran von unten betrachtet