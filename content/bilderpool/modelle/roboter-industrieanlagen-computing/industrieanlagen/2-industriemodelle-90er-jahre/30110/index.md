---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre05.jpg"
weight: "5"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30110
- /details5c23.html
imported:
- "2019"
_4images_image_id: "30110"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30110 -->
Hier die aufrechten Maschineständer mit je einem Fräs. bzw. Seitenfräskopf.
Das ganze besteht aus Modulen die so damals von der Firma angeboten und zu solchen Anlagen ausgebaut werden konnten.
Das SPS Teil und die Verkabelung sind  noch vorhanden .