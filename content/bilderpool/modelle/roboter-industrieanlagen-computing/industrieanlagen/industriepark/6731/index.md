---
layout: "image"
title: "MIP 019"
date: "2006-08-28T23:28:21"
picture: "B_060826_Industriemodule_12V_006.jpg"
weight: "17"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6731
- /details2fcf.html
imported:
- "2019"
_4images_image_id: "6731"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6731 -->
