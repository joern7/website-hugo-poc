---
layout: "image"
title: "MIP 021"
date: "2006-08-28T23:28:42"
picture: "B_060826_Industriemodule_12V_011.jpg"
weight: "19"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6733
- /details9934.html
imported:
- "2019"
_4images_image_id: "6733"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6733 -->
