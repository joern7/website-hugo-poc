---
layout: "image"
title: "MIP 029"
date: "2006-08-28T23:28:42"
picture: "B_060826_Industriemodule_12V_015.jpg"
weight: "22"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6736
- /details7215-3.html
imported:
- "2019"
_4images_image_id: "6736"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6736 -->
