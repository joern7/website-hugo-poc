---
layout: "image"
title: "industriemodellvonthomasoft01.jpg"
date: "2010-08-25T00:42:59"
picture: "industriemodellvonthomasoft01.jpg"
weight: "1"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27909
- /detailsb0f7.html
imported:
- "2019"
_4images_image_id: "27909"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:42:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27909 -->
