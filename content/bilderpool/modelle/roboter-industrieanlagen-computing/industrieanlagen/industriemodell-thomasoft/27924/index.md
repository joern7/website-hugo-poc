---
layout: "image"
title: "industriemodellvonthomasoft16.jpg"
date: "2010-08-25T00:43:04"
picture: "industriemodellvonthomasoft16.jpg"
weight: "16"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27924
- /details4110-2.html
imported:
- "2019"
_4images_image_id: "27924"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:04"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27924 -->
