---
layout: "image"
title: "industriemodellvonthomasoft15.jpg"
date: "2010-08-25T00:43:04"
picture: "industriemodellvonthomasoft15.jpg"
weight: "15"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27923
- /detailse0e7.html
imported:
- "2019"
_4images_image_id: "27923"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:04"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27923 -->
