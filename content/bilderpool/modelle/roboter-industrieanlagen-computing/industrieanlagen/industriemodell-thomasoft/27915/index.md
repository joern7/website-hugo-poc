---
layout: "image"
title: "industriemodellvonthomasoft07.jpg"
date: "2010-08-25T00:42:59"
picture: "industriemodellvonthomasoft07.jpg"
weight: "7"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27915
- /details0d31-2.html
imported:
- "2019"
_4images_image_id: "27915"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:42:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27915 -->
