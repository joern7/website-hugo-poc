---
layout: "image"
title: "Cube Dreher"
date: "2005-04-16T17:53:00"
picture: "CubeDreher.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBOProEntwickler"
license: "unknown"
legacy_id:
- /php/details/3971
- /details4a3a.html
imported:
- "2019"
_4images_image_id: "3971"
_4images_cat_id: "342"
_4images_user_id: "200"
_4images_image_date: "2005-04-16T17:53:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3971 -->
Das Teil auf dem der Würfel steht dreht die unterste Scheibe des Würfels. Den Dreher kann man absenken, so dass er beim Wenden des Würfels nicht im Weh ist. Das Oberteil wird von den 4 pneumatischen Armen festgehalten