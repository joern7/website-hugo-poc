---
layout: "image"
title: "Cube Scanner"
date: "2005-04-16T17:57:48"
picture: "CubeScanner.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBOProEntwickler"
license: "unknown"
legacy_id:
- /php/details/3973
- /details6b8d.html
imported:
- "2019"
_4images_image_id: "3973"
_4images_cat_id: "342"
_4images_user_id: "200"
_4images_image_date: "2005-04-16T17:57:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3973 -->
Das ist der Scanner, der den Würfel abtastet. Der Scanner hat zwei LDRs mit je einer grünen und roten Farbkappe (alte ft Leuchtwürfelkappen). Die Flchen werden jeweils durch eine Linsenlampe beleuchtet. Der Scanner kann etwas hin und her gefahren werden, so dass ohne BEwegung des Würfels 2 Teilflächen abgetatstet werden können. Durch drehen und wenden des Würfels kann man alle Flächen abtasten. Die Farberkennung arbeitet zuverlässig.