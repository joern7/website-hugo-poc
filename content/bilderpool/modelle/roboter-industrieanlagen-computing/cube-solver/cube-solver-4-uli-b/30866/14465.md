---
layout: "comment"
hidden: true
title: "14465"
date: "2011-06-15T09:44:16"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Ulrich,
cool - da lacht das "Klassiker-Herz"... (sogar der Motor ist hinten grau ;-)
Im Ernst: Ein sehr gelungenes Modell. Bringst Du es auf die Convention mit?
(Und dürfen wir einen Beitrag darüber in der ft:pedia haben?)
Gruß, Dirk