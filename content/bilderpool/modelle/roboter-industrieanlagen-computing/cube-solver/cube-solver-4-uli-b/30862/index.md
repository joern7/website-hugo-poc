---
layout: "image"
title: "Cube Solver - 1"
date: "2011-06-14T22:35:23"
picture: "CS_1.jpg"
weight: "1"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
keywords: ["Cube", "Solver"]
uploadBy: "ulib"
license: "unknown"
legacy_id:
- /php/details/30862
- /details3e6b.html
imported:
- "2019"
_4images_image_id: "30862"
_4images_cat_id: "2305"
_4images_user_id: "1330"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30862 -->
Diesen Cube Solver in Aktion finden Sie bei YouTube unter
http://www.youtube.com/watch?v=W4estyD5r5U