---
layout: "image"
title: "Befestigung an der Holzplatte"
date: "2009-12-11T23:27:06"
picture: "cubesolver02.jpg"
weight: "8"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25917
- /detailsd797.html
imported:
- "2019"
_4images_image_id: "25917"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25917 -->
Eine von 4 60mm Stangen an denen der Roboter befestigt ist

Jetzt sind sie alle verbogen, da ich ein kleineres Loch gebohrt habe und sie mit dem Hammer reingeklopft habe