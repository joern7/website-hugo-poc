---
layout: "image"
title: "Taster geöffnet"
date: "2009-12-11T23:27:17"
picture: "cubesolver36.jpg"
weight: "36"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25951
- /detailsd2b7.html
imported:
- "2019"
_4images_image_id: "25951"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25951 -->
Der Drehkranz dreht sich...