---
layout: "image"
title: "Kippen II"
date: "2009-12-11T23:27:17"
picture: "cubesolver27.jpg"
weight: "27"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25942
- /details9da6-2.html
imported:
- "2019"
_4images_image_id: "25942"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25942 -->
40mS vergangen