---
layout: "image"
title: "version 2.-1"
date: "2009-10-10T00:09:58"
picture: "PICT0041_2.jpg"
weight: "1"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: ["Cube", "Solver", "zauberwürfel", "rubic's", "rubik's", "weltrekord"]
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25533
- /details02b5.html
imported:
- "2019"
_4images_image_id: "25533"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-10-10T00:09:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25533 -->
Prototyp ohne Kamera