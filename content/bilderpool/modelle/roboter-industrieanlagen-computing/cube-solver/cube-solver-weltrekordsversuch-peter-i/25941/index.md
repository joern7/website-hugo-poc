---
layout: "image"
title: "ohne Webcamhalterung || Kippen I"
date: "2009-12-11T23:27:16"
picture: "cubesolver26.jpg"
weight: "26"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25941
- /details0c36.html
imported:
- "2019"
_4images_image_id: "25941"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25941 -->
Das Kippen braucht:
90mS für den rechten Schieber *2
70mS für den Linken Schieber *2
ergibt 320mS(gemessen: 30*in 10sek, also 333mS)