---
layout: "image"
title: "Mit Würfel"
date: "2009-12-11T23:27:16"
picture: "cubesolver17.jpg"
weight: "23"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25932
- /details6921.html
imported:
- "2019"
_4images_image_id: "25932"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25932 -->
Es handelt sich um den Originalen Rubiks-würfel (das Logo habe ich entfernen müssen, da es sonst als blauer Stein erkannt wird)