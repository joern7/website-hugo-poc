---
layout: "image"
title: "Kippen VI"
date: "2009-12-11T23:27:17"
picture: "cubesolver31.jpg"
weight: "31"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25946
- /details3d92.html
imported:
- "2019"
_4images_image_id: "25946"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25946 -->
Fertig