---
layout: "image"
title: "Vorat an reserveaufkleber geöffnet"
date: "2009-12-11T23:27:16"
picture: "cubesolver15.jpg"
weight: "21"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25930
- /details387a-4.html
imported:
- "2019"
_4images_image_id: "25930"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25930 -->
Der Roboter ist ein bisschen brutal (besonders beim kippen)  und deswegen muss ich dauernd die Aufkleber wechseln