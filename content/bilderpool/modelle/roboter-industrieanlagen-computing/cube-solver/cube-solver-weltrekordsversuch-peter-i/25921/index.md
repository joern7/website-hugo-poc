---
layout: "image"
title: "Linker Schieber"
date: "2009-12-11T23:27:06"
picture: "cubesolver06.jpg"
weight: "12"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25921
- /detailsc494-2.html
imported:
- "2019"
_4images_image_id: "25921"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25921 -->
Die Räder sind dazu da, dass der Schieber nicht zu sehr auf und abwackelt