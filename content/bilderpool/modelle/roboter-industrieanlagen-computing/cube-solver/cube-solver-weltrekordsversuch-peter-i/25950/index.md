---
layout: "image"
title: "Taster des Drehkranzes"
date: "2009-12-11T23:27:17"
picture: "cubesolver35.jpg"
weight: "35"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hman13"
license: "unknown"
legacy_id:
- /php/details/25950
- /detailscf91-2.html
imported:
- "2019"
_4images_image_id: "25950"
_4images_cat_id: "1790"
_4images_user_id: "1100"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25950 -->
Ausgangsposition/taster ist gedrückt