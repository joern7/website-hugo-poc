---
layout: "image"
title: "Drehen I"
date: "2009-12-11T23:27:17"
picture: "cubesolver32.jpg"
weight: "32"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25947
- /details2ca9.html
imported:
- "2019"
_4images_image_id: "25947"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25947 -->
Um die unterste Ebene zu drehen, fährt zuerst der linke Schieber vor