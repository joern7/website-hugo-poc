---
layout: "overview"
title: "Cube Solver"
date: 2020-02-22T08:01:58+01:00
legacy_id:
- /php/categories/585
- /categories0727.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=585 --> 
Maschinen, die Rubic's Cube "auflösen" können, d.h. den Zauberwürfel wieder in Ausgangsposition zurückbringen, so dass alle Seiten einfarbig sind.