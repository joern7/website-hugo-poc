---
layout: "image"
title: "Drehteller"
date: "2016-10-30T12:19:35"
picture: "IMG_4875.jpg"
weight: "12"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- /php/details/44703
- /details5da2.html
imported:
- "2019"
_4images_image_id: "44703"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44703 -->
Drehteller ohne Cube