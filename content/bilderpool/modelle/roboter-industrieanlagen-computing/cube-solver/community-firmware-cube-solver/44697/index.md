---
layout: "image"
title: "Grabber und Drehteller"
date: "2016-10-30T12:19:35"
picture: "IMG_4881.jpg"
weight: "6"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- /php/details/44697
- /detailse3fe.html
imported:
- "2019"
_4images_image_id: "44697"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44697 -->
Sicht auf den Grabber und den Drehteller