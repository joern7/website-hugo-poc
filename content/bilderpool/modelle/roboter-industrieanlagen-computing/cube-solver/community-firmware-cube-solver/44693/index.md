---
layout: "image"
title: "Drehtellerantrieb"
date: "2016-10-30T12:19:35"
picture: "IMG_4889.jpg"
weight: "2"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- /php/details/44693
- /details1856.html
imported:
- "2019"
_4images_image_id: "44693"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44693 -->
Seitenansicht auf den Drehteller und seinen Stopp-Taster