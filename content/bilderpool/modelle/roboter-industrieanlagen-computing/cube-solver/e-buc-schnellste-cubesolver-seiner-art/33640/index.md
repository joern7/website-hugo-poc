---
layout: "image"
title: "e-buc Bild 9"
date: "2011-12-12T17:15:29"
picture: "ebuc09.jpg"
weight: "14"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33640
- /detailse136.html
imported:
- "2019"
_4images_image_id: "33640"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33640 -->
Der Drehkranzantriebsmotor sitzt ganz hinten in der Ecke und ist mit dem Drehkranz direkt über eine Kette verbunden.