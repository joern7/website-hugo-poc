---
layout: "image"
title: "e-buc Bild 11"
date: "2011-12-12T17:15:29"
picture: "ebuc11.jpg"
weight: "16"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33642
- /detailsa676-2.html
imported:
- "2019"
_4images_image_id: "33642"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33642 -->
