---
layout: "image"
title: "e-buc Bild 1"
date: "2011-10-24T21:37:28"
picture: "ebuc1.jpg"
weight: "1"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33316
- /detailsfc48.html
imported:
- "2019"
_4images_image_id: "33316"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33316 -->
Das ist mein Cubesolver e-buc. Er kann den Rubik's Cube in unter 20 Sekunden lösen (reine Drehzeit).
Video: http://www.youtube.com/watch?v=VB4ki5SuqMU
Post im Forum: http://forum.ftcommunity.de/viewtopic.php?f=8&t=953

Nachfolgend lade ich ein paar Detailbilder hoch, auf Anfrage kann ich auch gerne noch weitere machen.