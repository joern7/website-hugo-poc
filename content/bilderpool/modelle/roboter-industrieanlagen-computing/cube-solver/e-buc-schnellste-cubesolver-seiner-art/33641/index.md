---
layout: "image"
title: "e-buc Bild 10"
date: "2011-12-12T17:15:29"
picture: "ebuc10.jpg"
weight: "15"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33641
- /details92fa.html
imported:
- "2019"
_4images_image_id: "33641"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33641 -->
Der Kameraturm ist leicht abnehmbar z.B. zu Transportzwecken.