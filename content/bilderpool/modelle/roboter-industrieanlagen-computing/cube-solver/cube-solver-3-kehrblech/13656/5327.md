---
layout: "comment"
hidden: true
title: "5327"
date: "2008-02-17T13:43:48"
uploadBy:
- "MarMac"
license: "unknown"
imported:
- "2019"
---
Cool! Da bin ich ja echt mal gespannt...

Und allerspannendste Frage: Was verwendest du zur Programmierung? Ist der Roboter mit meinem RoboPro-Programm kompatibel?
Wenn du da Unterstützung brauchen solltest, ich helfe gerne.