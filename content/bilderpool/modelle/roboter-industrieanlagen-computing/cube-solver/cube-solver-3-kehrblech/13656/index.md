---
layout: "image"
title: "Gesamtansicht"
date: "2008-02-17T07:55:26"
picture: "cubesolver1.jpg"
weight: "1"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/13656
- /details88a0.html
imported:
- "2019"
_4images_image_id: "13656"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T07:55:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13656 -->
Das ist jetzt meine Version eines Cube Solvers.
Die Fotos sind von einer älteren Version des Roboters, bald werde ich neue hinzufügen.
Der Roboter funktionierte in der alten Version nicht immer gut, in der neuen aber schon.
