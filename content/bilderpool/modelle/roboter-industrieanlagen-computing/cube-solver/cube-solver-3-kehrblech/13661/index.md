---
layout: "image"
title: "Kippvorgang"
date: "2008-02-17T07:55:26"
picture: "cubesolver6.jpg"
weight: "6"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/13661
- /details8a53.html
imported:
- "2019"
_4images_image_id: "13661"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T07:55:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13661 -->
