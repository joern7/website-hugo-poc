---
layout: "image"
title: "Von Oben"
date: "2008-02-17T07:55:26"
picture: "cubesolver4.jpg"
weight: "4"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/13659
- /detailsbc53-2.html
imported:
- "2019"
_4images_image_id: "13659"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T07:55:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13659 -->
