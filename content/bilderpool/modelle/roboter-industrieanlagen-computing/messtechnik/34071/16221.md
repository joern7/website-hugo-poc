---
layout: "comment"
hidden: true
title: "16221"
date: "2012-01-28T20:47:30"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Kompliment, Hut ab !

Ich habe bei meinem Autowaschstrasse  herausgefunden dass : 
o Fotozellen mit dem richtingen kleine Störlichtkappen mit Laserlicht ebenso gut funktionieren als 
o Fototransistoren mit Laserlicht.

Interessant und billig sind die 5 Euro Laserpointer nr. 776265 
http://www.conrad.de/ce/de/product/776265/LASERPOINTER-SCHLUeSSELANHAeNGER-ROT 

Für die spanung der Laserpointer nutze ich eine Conrad Spannungsregler-Platine für LM 317-T Ausgangsspannung 1.2 - 32 V/DC: 
http://www.conrad.de/ce/de/product/130312/UNIVERSAL-SPANNUNGSREGLER-BAUSTEIN 

Schau auch mal unter:  
http://www.ftcommunity.de/details.php?image_id=33337

Grüss, 

Peter 
Poederoyen NL