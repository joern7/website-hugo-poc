---
layout: "image"
title: "CT - Funktion"
date: "2012-01-28T15:38:06"
picture: "Bild_2.jpg"
weight: "2"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/34068
- /detailse42c.html
imported:
- "2019"
_4images_image_id: "34068"
_4images_cat_id: "2520"
_4images_user_id: "1239"
_4images_image_date: "2012-01-28T15:38:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34068 -->
Das blaue Gitter soll ein sich im CT befindliches Objekt darstellen. 
Die Lichtschranken messen dann von allen Seiten 4x ob etwas im Weg ist.
Würde ein flaches Objekt auf dem Boden im 2ten Feld links unten liegen,
dann würde die Maschine folgendes messen:
1) 0        3) 0
2) 0        4) 1
Die Werte bei 4) zählt sie nun bei allen Variablen (jedes Feld=1 Variable)
in der untersten Reihe hinzu. Dies geschieht nun von allen Seiten und am Ende zeigt das Gerät die Felder mit den höchsten Werten an. (Bild 4 ->)