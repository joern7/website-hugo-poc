---
layout: "image"
title: "Fischertechnik Koordinaten Schalter mit (alte) FT-Feder"
date: "2008-01-20T19:57:07"
picture: "Koordinatenschalter-1.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13363
- /details5c23-2.html
imported:
- "2019"
_4images_image_id: "13363"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2008-01-20T19:57:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13363 -->
Fischertechnik Koordinaten Schalter mit (alte) FT-Feder