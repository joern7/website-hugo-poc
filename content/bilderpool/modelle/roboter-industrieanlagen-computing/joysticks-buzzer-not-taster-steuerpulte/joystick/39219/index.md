---
layout: "image"
title: "Anordnung der vier Taster"
date: "2014-08-09T22:30:57"
picture: "joystick2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39219
- /detailsda62.html
imported:
- "2019"
_4images_image_id: "39219"
_4images_cat_id: "2933"
_4images_user_id: "1126"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39219 -->
Die Taster werden auf V-Bausteinen 15 an und um einen Baustein 15 mit Magnet montiert (Taster oben).