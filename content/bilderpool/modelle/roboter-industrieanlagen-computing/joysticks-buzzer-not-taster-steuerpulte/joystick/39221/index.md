---
layout: "image"
title: "Betätiger"
date: "2014-08-09T22:30:57"
picture: "joystick4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39221
- /details5510.html
imported:
- "2019"
_4images_image_id: "39221"
_4images_cat_id: "2933"
_4images_user_id: "1126"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39221 -->
Eine Metallstange mit Rad 14, von zwei Klemmbuchsen auf der Höhe der Taster gehalten, wird auf den Magneten "aufgesetzt".