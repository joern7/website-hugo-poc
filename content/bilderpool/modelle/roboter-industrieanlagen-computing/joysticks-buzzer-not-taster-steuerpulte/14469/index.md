---
layout: "image"
title: "joystick deluxe_14"
date: "2008-05-04T14:56:37"
picture: "joystick_deluxe_14.jpg"
weight: "26"
konstrukteure: 
- "pk"
fotografen:
- "pk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/14469
- /detailsdce1-2.html
imported:
- "2019"
_4images_image_id: "14469"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-04T14:56:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14469 -->
Joystick mit vier schalter für die beide X und Y richtungen.
Zwei "Fire butons" und vier rädern, da mit das ding auf der stelle stehen bleibt, runden das ganse ab.