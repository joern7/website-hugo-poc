---
layout: "image"
title: "Joystick aus Sony PSP2000 mit Schaltbild und Anschluss-Schema"
date: "2016-08-16T18:42:14"
picture: "Joystick_Schematic.jpg"
weight: "53"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Sony", "PSP2000", "Joystick", "analog"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/44303
- /details39a9-2.html
imported:
- "2019"
_4images_image_id: "44303"
_4images_cat_id: "909"
_4images_user_id: "579"
_4images_image_date: "2016-08-16T18:42:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44303 -->
siehe Text im Bild