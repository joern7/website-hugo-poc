---
layout: "image"
title: "Joystick 04"
date: "2010-05-15T23:49:44"
picture: "joystick04.jpg"
weight: "42"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27231
- /detailsec56.html
imported:
- "2019"
_4images_image_id: "27231"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27231 -->
nochmal von oben