---
layout: "image"
title: "joystick_18"
date: "2008-05-09T10:29:41"
picture: "joystick_18.jpg"
weight: "32"
konstrukteure: 
- "pk"
fotografen:
- "pk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/14496
- /details2709.html
imported:
- "2019"
_4images_image_id: "14496"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-09T10:29:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14496 -->
Platine mit stick.