---
layout: "image"
title: "Reise Buzzer"
date: "2010-12-09T21:46:33"
picture: "rb1.jpg"
weight: "51"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/29445
- /details6650-2.html
imported:
- "2019"
_4images_image_id: "29445"
_4images_cat_id: "909"
_4images_user_id: "430"
_4images_image_date: "2010-12-09T21:46:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29445 -->
Für die kleine Quizzrunde zwischendurch. 
Wenn die Halbkugel gedrückt wird gibt der Minitaster den Strom zum Summer weiter. (praktisch ne kleine Totmannsteuerung).
Die Lampen hab ich abgeschlossen, weil das der Akku leider nicht lange mitmacht.