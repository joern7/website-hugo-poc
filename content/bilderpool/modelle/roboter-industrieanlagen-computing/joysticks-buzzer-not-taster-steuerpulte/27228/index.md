---
layout: "image"
title: "Joystick 01"
date: "2010-05-15T23:49:44"
picture: "joystick01.jpg"
weight: "39"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27228
- /detailsf76a.html
imported:
- "2019"
_4images_image_id: "27228"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27228 -->
Hier ein kleiner Joystick