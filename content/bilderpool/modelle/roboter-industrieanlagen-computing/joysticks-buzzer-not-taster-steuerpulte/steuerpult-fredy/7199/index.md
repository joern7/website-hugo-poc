---
layout: "image"
title: "Steuerpult von Rechts"
date: "2006-10-16T19:01:32"
picture: "Steuerpult_001.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7199
- /detailsc9ba.html
imported:
- "2019"
_4images_image_id: "7199"
_4images_cat_id: "691"
_4images_user_id: "453"
_4images_image_date: "2006-10-16T19:01:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7199 -->
