---
layout: "image"
title: "Joystick 03"
date: "2010-05-15T23:49:44"
picture: "joystick03.jpg"
weight: "41"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27230
- /details5fab.html
imported:
- "2019"
_4images_image_id: "27230"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27230 -->
von oben ohne den "Hebel"