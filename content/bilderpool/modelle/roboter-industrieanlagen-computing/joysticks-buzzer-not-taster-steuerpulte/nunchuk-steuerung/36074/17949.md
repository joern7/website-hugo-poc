---
layout: "comment"
hidden: true
title: "17949"
date: "2013-04-20T11:05:30"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

der Original-Nunchuk funktioniert jetzt dank des aktuellen TX-Firmware-Updates mit RoboPro-Version 3.2.3 - dafür streikt nun der Lioncast-Nachbau. Das Problem konnte ich bisher auch nicht lösten...

Es ist mir allerdings gelungen, mit der neuen Firmware-Version den (kabellosen!) Logic3-Nunchuk anzusteuern (mit einer verminderten Busgeschwindigkeit von 100 kHz, siehe auch http://forum.ftcommunity.de/viewtopic.php?f=15&t=1566#p11990). Damit gibt es jetzt eine echte TX-Fernsteuerung mit Neigungssensor, Joystick und zwei Tasten für 9,90 € (plus 20 ct. für zwei Dioden).

Das modifizierte Programm lade ich noch hoch; das Modell dazu gibt's auf der Convention 2013 - und demnächst folgt auch ein Artikelchen darüber in der ft:pedia.

Gruß, Dirk