---
layout: "comment"
hidden: true
title: "17505"
date: "2012-11-01T23:07:08"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo H.A.R.R.Y.,
es gibt viele Quellen im Netz - eine der jüngsten ist ein Artikel in elektor 9/2012 (http://www.elektor.de/Uploads/2012/8/Nunchuk-USB-Adapter.pdf).
Demnächst wirst Du auch in der ft:pedia (http://www.ftcommunity.de/ftpedia) etwas über den Anschluss an den TX finden - zum I²C-Protokoll findest Du einen Aufsatz in Ausgabe 3/2012 :-).
Gruß, Dirk