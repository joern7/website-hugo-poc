---
layout: "comment"
hidden: true
title: "17498"
date: "2012-10-31T21:07:50"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
mit meinen Dioden verliere ich nur je 0,2 V in Durchlassrichtung... sagt jedenfalls mein Messgerät. Damit komme ich zwar näher an die 3,3 V heran, aber acht Dioden in Reihe sehen schon ein wenig merkwürdig aus... Mache ich etwas falsch?
Gruß, Dirk