---
layout: "image"
title: "joystick_2"
date: "2008-05-04T14:56:37"
picture: "joystick_2.jpg"
weight: "14"
konstrukteure: 
- "pk"
fotografen:
- "pk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/14457
- /details66fd.html
imported:
- "2019"
_4images_image_id: "14457"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-04T14:56:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14457 -->
