---
layout: "image"
title: "Joystick"
date: "2010-09-02T18:58:17"
picture: "magjoy2.jpg"
weight: "50"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28028
- /detailsd05f.html
imported:
- "2019"
_4images_image_id: "28028"
_4images_cat_id: "909"
_4images_user_id: "558"
_4images_image_date: "2010-09-02T18:58:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28028 -->
Der Stab selbst wird vom Magnet festgehalten und das sogar sehr kräftig. Mittelstellung wird durch die Taster und vorallem durch den Magnet erreicht