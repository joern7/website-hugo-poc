---
layout: "image"
title: "3D-Joysticks 10kOhm"
date: "2009-11-13T21:18:50"
picture: "Pneumatik-3D-Slurf_004.jpg"
weight: "36"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25776
- /details0950-2.html
imported:
- "2019"
_4images_image_id: "25776"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2009-11-13T21:18:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25776 -->
Interessant sind die 3D-Joysticks 10kOhm ( Conrad-artnr. 425609-LN ,  ca. 1,40 Euro/st ).

Mit ein 10kOhm Wiederstand parallel gibt sich: 0 - 5 kOhm    
Passend beim TX  I-1-8-Analog :  0 - 5000