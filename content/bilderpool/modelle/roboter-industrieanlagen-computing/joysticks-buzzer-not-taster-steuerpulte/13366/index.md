---
layout: "image"
title: "Koordinatenschalter (Joy-Stick)"
date: "2008-01-22T06:32:23"
picture: "koordinatenschalter3.jpg"
weight: "5"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13366
- /detailsee20.html
imported:
- "2019"
_4images_image_id: "13366"
_4images_cat_id: "909"
_4images_user_id: "424"
_4images_image_date: "2008-01-22T06:32:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13366 -->
