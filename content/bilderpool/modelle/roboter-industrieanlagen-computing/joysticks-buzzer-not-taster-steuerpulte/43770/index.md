---
layout: "image"
title: "3D Joystick (von der Sony PSP2000)"
date: "2016-06-15T20:00:41"
picture: "3D-Taste-Analog-Joystick-Sony-PSP-2000-Schwarz.jpg"
weight: "52"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["3D", "Joystick", "Sony", "PSP2000"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43770
- /details8c7c.html
imported:
- "2019"
_4images_image_id: "43770"
_4images_cat_id: "909"
_4images_user_id: "579"
_4images_image_date: "2016-06-15T20:00:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43770 -->
