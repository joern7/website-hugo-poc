---
layout: "image"
title: "Steuerung 2"
date: "2008-08-03T09:13:55"
picture: "steuerung2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14986
- /detailsa826.html
imported:
- "2019"
_4images_image_id: "14986"
_4images_cat_id: "1366"
_4images_user_id: "389"
_4images_image_date: "2008-08-03T09:13:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14986 -->
