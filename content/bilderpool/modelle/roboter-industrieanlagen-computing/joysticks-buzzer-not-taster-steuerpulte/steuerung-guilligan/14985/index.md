---
layout: "image"
title: "Steuerung 1"
date: "2008-08-03T09:13:54"
picture: "steuerung1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14985
- /detailsc531.html
imported:
- "2019"
_4images_image_id: "14985"
_4images_cat_id: "1366"
_4images_user_id: "389"
_4images_image_date: "2008-08-03T09:13:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14985 -->
Hallo
hier ein paar Bilder meiner neuen Steuerung für den Faltkran.
