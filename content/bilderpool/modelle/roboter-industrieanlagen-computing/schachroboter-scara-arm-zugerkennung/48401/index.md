---
layout: "image"
title: "Scara Arm von schräg oben"
date: "2018-11-12T20:50:21"
picture: "C2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Schrittmotor", "DVR8825", "Mikrostepping"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/48401
- /details4f2e.html
imported:
- "2019"
_4images_image_id: "48401"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48401 -->
Der Arm wird von zwei Schrittmotoren bewegt (5 Euro pro Stück von Pollin). Die Schrittmotoren werden von zwei  DRV8825 Treibern angesteuert mit 1/32 Mikroschritten.