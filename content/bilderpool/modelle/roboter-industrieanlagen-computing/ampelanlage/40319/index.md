---
layout: "image"
title: "Ampelanlage Robopro"
date: "2015-01-14T19:13:30"
picture: "ampelanlage04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40319
- /details0730.html
imported:
- "2019"
_4images_image_id: "40319"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40319 -->
Programm 2:

Die Zeiten der Ampelanlage werden dynamisch gesteuert durch die Anzahl der Autos in den Spuren. 
Es wird die Zeit für einen Ampelphasendurchlauf und die Anzahl der Fahrzeuge gemessen. 

Die Ampelphasen werden parallel in Robopro und im Model angezeigt.
Die Taster für die Fußgänger (gelb) können in Robopro und am Model betätigt werden.