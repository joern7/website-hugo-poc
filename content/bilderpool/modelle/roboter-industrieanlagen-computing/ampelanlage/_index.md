---
layout: "overview"
title: "Ampelanlage"
date: 2020-02-22T08:08:46+01:00
legacy_id:
- /php/categories/3024
- /categoriesf433.html
- /categories2024.html
- /categoriesd49f.html
- /categoriesac06.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3024 --> 
Hallo zusammen,

ich möchte euch meine Ampelanlage vorstellen. Bei dieser Ampelanlage handelt es sich um ein Model für eine Masterarbeit.
Das Thema  der Masterarbeit sollte etwas mit aktuellten Umweltthemen zu tun haben. Ein Model woran man zeigen kann, wie
man im Straßenverkehr Energie und Zeit spart,

Das Model bestand aus einer normalen Ampelphasensteuerung und  einer optimierten Ampelphasensteuerung. Durch die 
optimierte Variante konnten erheblich Zeit, Energie und Umweltbelastungen durch Feinstaub eingespart werden.
Dies wurde in Robopro durch festhalten der Wartezeiten der Autos an der Kreuzung belegt.

