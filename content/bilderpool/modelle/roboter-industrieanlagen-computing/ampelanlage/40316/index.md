---
layout: "image"
title: "Ampelanlage Model"
date: "2015-01-14T19:13:30"
picture: "ampelanlage01.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40316
- /details65ba.html
imported:
- "2019"
_4images_image_id: "40316"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40316 -->
Hier seht ihr das komplette Model auf 4x 1000er Grundplatten aufgebaut.
Anschließend wurden diese auf eine Spanplatte geschraubt.

Die Anlage umfaßt insgesamt 10 Fahrspuren. Die Hauptstraße verläuft von links nach rechts.
Oben und unten seht ihr die Nebenstraße, dazu die 4 Fußgängerüberwege.
