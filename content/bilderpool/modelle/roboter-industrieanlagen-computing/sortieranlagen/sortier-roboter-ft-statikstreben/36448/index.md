---
layout: "image"
title: "Gesammtansicht von oben(1)"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben02.jpg"
weight: "2"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36448
- /details8afa.html
imported:
- "2019"
_4images_image_id: "36448"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36448 -->
Das Modell wird mit dem Computer und dem Robo-Interface gesteuert. Ich habe alle acht Digital-Eingänge und alle Motoren-Ausgänge belegt. Das hat gerade so gereicht. 
Es sind acht Motoren, sechs Taster und zwei Fototransistoren verbaut.