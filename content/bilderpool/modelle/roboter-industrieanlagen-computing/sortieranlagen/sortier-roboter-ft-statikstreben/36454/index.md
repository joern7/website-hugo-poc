---
layout: "image"
title: "Streben weg wischen"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben08.jpg"
weight: "8"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36454
- /details23ec.html
imported:
- "2019"
_4images_image_id: "36454"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36454 -->
