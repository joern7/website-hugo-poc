---
layout: "image"
title: "Strebenabnehmer"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben04.jpg"
weight: "4"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36450
- /detailsc818.html
imported:
- "2019"
_4images_image_id: "36450"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36450 -->
Der Power-Mot. betreibt den Strebenabnehmer. Durch die konstruktion muss der Mot. immer nur in eine Richtung drehen obwohl sich der Schlitten, der die Streben abnimmt vor und zurück bewegt. Das habe ich gemacht um nur einen Kontakt an den Mot.-Ausgängen des RI (Robo Interface) zu verwenden. Viele Funktionen in dem Modell sind so gestalltet dass der Motorn nur in eine Richtung drehen muss. Genau aus dem genannten Grund.