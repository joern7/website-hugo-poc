---
layout: "image"
title: "Gesammtansicht von (2)"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben03.jpg"
weight: "3"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36449
- /details8942.html
imported:
- "2019"
_4images_image_id: "36449"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36449 -->
