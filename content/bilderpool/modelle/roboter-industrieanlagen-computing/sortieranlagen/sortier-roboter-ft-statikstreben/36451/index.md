---
layout: "image"
title: "Strebenabnehmer"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben05.jpg"
weight: "5"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36451
- /detailsa0bb.html
imported:
- "2019"
_4images_image_id: "36451"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36451 -->
