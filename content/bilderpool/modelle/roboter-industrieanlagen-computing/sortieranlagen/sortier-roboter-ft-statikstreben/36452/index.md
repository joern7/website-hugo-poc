---
layout: "image"
title: "Messtisch"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben06.jpg"
weight: "6"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36452
- /detailsdf56.html
imported:
- "2019"
_4images_image_id: "36452"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36452 -->
von rechts wird die Strebe von dem Strebenabnehmer auf den Tisch geschoben, wo sie dann der länge nach liegt. Der Tisch fährt dann in eine recht langsamen Geschwindigkeit nach links los. Die im Bild Sichtbare Lichstschranke wird dabei von der Strebe unterbrochen