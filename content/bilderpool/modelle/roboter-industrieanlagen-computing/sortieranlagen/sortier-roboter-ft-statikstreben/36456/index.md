---
layout: "image"
title: "erstes Förderband"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben10.jpg"
weight: "10"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36456
- /details3bab.html
imported:
- "2019"
_4images_image_id: "36456"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36456 -->
auf der schrägen Fläche links vom Förderband rutschen die gemessenen Streben auf des Förderband.