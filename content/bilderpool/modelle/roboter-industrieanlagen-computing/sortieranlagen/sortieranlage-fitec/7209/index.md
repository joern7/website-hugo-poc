---
layout: "image"
title: "Neuer Antrieb..."
date: "2006-10-22T18:45:58"
picture: "Sortiermaschine15.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7209
- /details90f3.html
imported:
- "2019"
_4images_image_id: "7209"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-22T18:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7209 -->
...für das Förderband.