---
layout: "image"
title: "Einschalter"
date: "2006-11-19T12:42:13"
picture: "Sortiermaschine18.jpg"
weight: "23"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7489
- /details94f0.html
imported:
- "2019"
_4images_image_id: "7489"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-19T12:42:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7489 -->
Das ist der neue Einschalter. Wird er gedrückt, startet die Maschine.