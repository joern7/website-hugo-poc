---
layout: "image"
title: "Sortieranlage 6"
date: "2006-12-10T18:30:25"
picture: "sortieranlage6.jpg"
weight: "33"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7833
- /details8431.html
imported:
- "2019"
_4images_image_id: "7833"
_4images_cat_id: "685"
_4images_user_id: "502"
_4images_image_date: "2006-12-10T18:30:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7833 -->
