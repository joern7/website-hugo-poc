---
layout: "image"
title: "Steuerung"
date: "2006-10-16T19:01:02"
picture: "Sortiermaschine14.jpg"
weight: "14"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7191
- /detailsd997-2.html
imported:
- "2019"
_4images_image_id: "7191"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7191 -->
Hier die neue Steuereinheit. Unter dem Robo Interface befindet sich das Energy Set.