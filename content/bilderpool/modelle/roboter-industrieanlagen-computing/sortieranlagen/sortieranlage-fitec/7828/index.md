---
layout: "image"
title: "Sortieranlage 1"
date: "2006-12-10T18:30:25"
picture: "sortieranlage1.jpg"
weight: "28"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7828
- /details9081.html
imported:
- "2019"
_4images_image_id: "7828"
_4images_cat_id: "685"
_4images_user_id: "502"
_4images_image_date: "2006-12-10T18:30:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7828 -->
Links werden die Räder auf das Förderband geschoben und dann unnter den Lichtsensor gefahren. Dort wird die Lichtstärke gemessen und je nach dem was für eine Farbe es ist in die verschiedenen Fächer einsortiert.