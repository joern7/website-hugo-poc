---
layout: "image"
title: "Lichtschranke"
date: "2007-04-11T09:59:11"
picture: "sortieranlage3.jpg"
weight: "3"
konstrukteure: 
- "Bumpf"
fotografen:
- "Bumpf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/10045
- /details4655-3.html
imported:
- "2019"
_4images_image_id: "10045"
_4images_cat_id: "907"
_4images_user_id: "424"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10045 -->
Unterscheidet 15er und 30er Steine