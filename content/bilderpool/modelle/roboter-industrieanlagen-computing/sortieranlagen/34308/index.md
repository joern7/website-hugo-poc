---
layout: "image"
title: "Sortieranlage 6"
date: "2012-02-19T20:36:45"
picture: "FT_Derk_06-2012.jpg"
weight: "17"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34308
- /detailsc5a9.html
imported:
- "2019"
_4images_image_id: "34308"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:36:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34308 -->
