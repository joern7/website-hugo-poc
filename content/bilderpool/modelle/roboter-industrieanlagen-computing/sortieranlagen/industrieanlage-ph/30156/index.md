---
layout: "image"
title: "Das Steinelager"
date: "2011-02-28T17:31:46"
picture: "industrieanlage02.jpg"
weight: "2"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30156
- /details150d.html
imported:
- "2019"
_4images_image_id: "30156"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30156 -->
Auf diesen Bild kann man das Fließband und das Steinelager sehen. Die  "Steine" sind die Holzstückchen aus dem Baukasten Robo Pneuvac.