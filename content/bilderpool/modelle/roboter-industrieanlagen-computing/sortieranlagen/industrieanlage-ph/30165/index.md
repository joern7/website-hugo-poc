---
layout: "image"
title: "Weiß in die Kiste"
date: "2011-02-28T17:32:01"
picture: "industrieanlage11.jpg"
weight: "11"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30165
- /details4260.html
imported:
- "2019"
_4images_image_id: "30165"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:01"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30165 -->
Der Greifer legt den Stein in die Kiste für Weiß.