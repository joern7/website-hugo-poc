---
layout: "image"
title: "2. Farberkennung (Genauer)"
date: "2011-02-28T17:31:46"
picture: "industrieanlage09.jpg"
weight: "9"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30163
- /detailsb632.html
imported:
- "2019"
_4images_image_id: "30163"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30163 -->
Hier wird entschieden in welche Kiste der Stein kommt (Weiß oder Rot).