---
layout: "image"
title: "Türme von Hanoi"
date: "2009-11-29T22:08:58"
picture: "dietuermevonhanoi01.jpg"
weight: "1"
konstrukteure: 
- "Ironsmurf"
fotografen:
- "Ironsmurf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironsmurf"
license: "unknown"
legacy_id:
- /php/details/25866
- /details0acc-3.html
imported:
- "2019"
_4images_image_id: "25866"
_4images_cat_id: "1818"
_4images_user_id: "461"
_4images_image_date: "2009-11-29T22:08:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25866 -->
Die 5 Becher im Vordergrund symbolisieren den Turm, gleichzeitig stellen die 5 Becher das Maximun dar. Die Bilderfolge zeigt jedoch die Lösung mit 4 Bechern.