---
layout: "image"
title: "Sortieranlage (neu) 5"
date: "2006-12-25T14:31:01"
picture: "sortieranlage5.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8114
- /detailseaca.html
imported:
- "2019"
_4images_image_id: "8114"
_4images_cat_id: "750"
_4images_user_id: "502"
_4images_image_date: "2006-12-25T14:31:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8114 -->
