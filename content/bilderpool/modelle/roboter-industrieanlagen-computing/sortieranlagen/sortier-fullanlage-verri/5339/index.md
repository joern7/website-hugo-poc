---
layout: "image"
title: "Füllroboter"
date: "2005-11-13T21:17:25"
picture: "FT0006.jpg"
weight: "5"
konstrukteure: 
- "verri"
fotografen:
- "verri"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5339
- /detailscca4.html
imported:
- "2019"
_4images_image_id: "5339"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5339 -->
Der Füllroboter saugt das Werkstück an und setzt es je nach Farbe in einem Fass auf einem der Bänder rechts und links ab.Ist die maximale Füllmenge einer Farbe erreicht wird das volle Fass abgerückt und und ein leeres nachgefördert.Das andere Fass kann weiterhin gefüllt werden.