---
layout: "image"
title: "Hochregal Zwischenlager"
date: "2006-02-07T17:46:01"
picture: "HochR0002.jpg"
weight: "7"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5744
- /detailsd3a2.html
imported:
- "2019"
_4images_image_id: "5744"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2006-02-07T17:46:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5744 -->
Hier werden bis zu 9 gefüllte Fässer zwischen gelagert um immer Gruppen zu drei Stück für die Palettierung zu bekommen.