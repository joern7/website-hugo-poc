---
layout: "image"
title: "HR und Band"
date: "2006-02-07T17:46:01"
picture: "HochR0007.jpg"
weight: "10"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5747
- /details2427.html
imported:
- "2019"
_4images_image_id: "5747"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2006-02-07T17:46:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5747 -->
Einmal Gesamtansicht. 
Palettierstraße mit RFID Erkennung ist leider noch im Bau.