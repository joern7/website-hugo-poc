---
layout: "image"
title: "Display"
date: "2007-03-17T12:00:08"
picture: "sortiermaschine2.jpg"
weight: "49"
konstrukteure: 
- "Lcd: thkias, Rest: Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9537
- /detailse583.html
imported:
- "2019"
_4images_image_id: "9537"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-03-17T12:00:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9537 -->
Hier sieht man das Lcd mit Text.
Es werden Informationen wie Status, Störung, welche Farbe an Kästchen kommt, angezeigt und noch viel mehr.