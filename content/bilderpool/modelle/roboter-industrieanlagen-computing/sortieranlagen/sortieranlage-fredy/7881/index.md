---
layout: "image"
title: "Drehkranz"
date: "2006-12-12T18:06:00"
picture: "Fr_ftcommunity1_003.jpg"
weight: "12"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7881
- /detailsb1f4-2.html
imported:
- "2019"
_4images_image_id: "7881"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-12T18:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7881 -->
Hier sieht man den neuen Drehkranz.