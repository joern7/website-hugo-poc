---
layout: "image"
title: "Drehkranz Antrieb"
date: "2007-08-11T17:21:32"
picture: "industriemodell6.jpg"
weight: "58"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11358
- /detailse44c.html
imported:
- "2019"
_4images_image_id: "11358"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-08-11T17:21:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11358 -->
Neuer Antrieb für den Drehkranz in der Zufürung