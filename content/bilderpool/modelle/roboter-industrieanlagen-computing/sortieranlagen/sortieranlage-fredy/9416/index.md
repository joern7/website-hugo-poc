---
layout: "image"
title: "Schlüsselschalter"
date: "2007-03-13T19:02:36"
picture: "bilder_001.jpg"
weight: "47"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9416
- /details142e-2.html
imported:
- "2019"
_4images_image_id: "9416"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-03-13T19:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9416 -->
Ich habe hier ein Schlüssenschalter verbaut. Mit dem kann man die Sortiermaschine ein und aus aus schalten.
Er wird wie ein ft-Taster am das Interface angeschlossen und gibt die gleichen werte, sprich 1 und 0.