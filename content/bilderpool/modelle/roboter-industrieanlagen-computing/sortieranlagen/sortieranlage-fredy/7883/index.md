---
layout: "image"
title: "Sortierung"
date: "2006-12-12T18:06:00"
picture: "Fr_ftcommunity1_005.jpg"
weight: "14"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7883
- /details1ce9.html
imported:
- "2019"
_4images_image_id: "7883"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-12T18:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7883 -->
In diesem stück der Anlage werden die Kästchen sortiert.