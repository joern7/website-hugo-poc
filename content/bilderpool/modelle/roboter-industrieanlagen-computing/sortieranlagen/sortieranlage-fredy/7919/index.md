---
layout: "image"
title: "Steuerpult von unten"
date: "2006-12-17T17:47:45"
picture: "Neuer_Ordner_2_001.jpg"
weight: "27"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7919
- /detailsba15-2.html
imported:
- "2019"
_4images_image_id: "7919"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-17T17:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7919 -->
