---
layout: "image"
title: "Ersetzung der Lichtschranke"
date: "2007-01-07T13:19:20"
picture: "Neuer_Ordner_2_007.jpg"
weight: "41"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8315
- /details057c-2.html
imported:
- "2019"
_4images_image_id: "8315"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-07T13:19:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8315 -->
Hier ersetzt der cny 70 die Lichtschranke.