---
layout: "image"
title: "Überwachung des Magazines"
date: "2007-01-07T13:19:20"
picture: "Neuer_Ordner_2_005.jpg"
weight: "39"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8313
- /details5856.html
imported:
- "2019"
_4images_image_id: "8313"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-07T13:19:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8313 -->
Der cny 70 überwacht ob nach Kästchen in dem Magazin sind oder nicht.