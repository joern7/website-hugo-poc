---
layout: "image"
title: "Ketten oben"
date: "2007-08-11T17:21:31"
picture: "industriemodell2.jpg"
weight: "54"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11354
- /details9c0a.html
imported:
- "2019"
_4images_image_id: "11354"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11354 -->
