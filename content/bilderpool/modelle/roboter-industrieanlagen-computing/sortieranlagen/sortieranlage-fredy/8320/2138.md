---
layout: "comment"
hidden: true
title: "2138"
date: "2007-01-22T17:51:17"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Was mich dann auch noch interessiert. Im Forum hab ich gefragt ob man für ein Extension noch ein Netzteil braucht. Man hat mir gesagt, dass man es nicht zwingend braucht, aber wenn man mehrere Motoren/Lichter gleichzeitig anmacht ist es schon gut. Du hast ja 2 Extensions eingebaut und hast auch viele Motoren/Lichter eingebaut. Wie viele Stromversorgungen hast du benutzt?

Gruß fitec