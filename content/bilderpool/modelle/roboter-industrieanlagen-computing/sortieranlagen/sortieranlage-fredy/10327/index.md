---
layout: "image"
title: "Flächen LEDs"
date: "2007-05-06T18:36:17"
picture: "Anzeigen_001.jpg"
weight: "52"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10327
- /detailsc4bc-3.html
imported:
- "2019"
_4images_image_id: "10327"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-05-06T18:36:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10327 -->
Im Hintergrund sieht man den Kabel Kanal zum Interface, damit sich die Kabel nicht in den Drehkränzen verfangen.
Die beiden Flächen LEDs zeigen an, wenn eine Kassete auf dem Weg zu den jeweiligen Ausgängen ist.