---
layout: "image"
title: "Drei Teile"
date: "2006-12-15T15:28:56"
picture: "Neuer_Ordner_3_004.jpg"
weight: "22"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7905
- /detailsa74d.html
imported:
- "2019"
_4images_image_id: "7905"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-15T15:28:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7905 -->
Man kann das Modell in drei Teile zerlegen.
Hier mal noch ein Paar Daten zu dem Modell:
1 Interface
2 Extensions
6 Taster
7 Motoren
2 Magnetventile
5 Lampen
4 Lichtsensoren