---
layout: "image"
title: "Verteiler"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46972
- /details3dba.html
imported:
- "2019"
_4images_image_id: "46972"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46972 -->
Unter der Drehscheibe sitzt ein Z20, das von einem zweiten XS-Motor mit Schnecke angetrieben wird. Die Ruheposition wird über den silbrigen Neodym-Magneten im linken BS15 und dem darunter sichtbaren Reed-Kontakt erkannt.

Der Motor läuft, wenn überhaupt (wenn also nicht gerade ein mittelllanger Baustein durch ging), erst nach einer gewissen Zeit (einstellbar über ein zweites Monoflop) nachdem die zweite Lichtschranke freigegeben wurde, das Bauteil die Erkennung also komplett durchlaufen hat.