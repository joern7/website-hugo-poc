---
layout: "overview"
title: "Elektronische Sortieranlage für 3 Bausteinlängen"
date: 2020-02-22T08:05:56+01:00
legacy_id:
- /php/categories/3475
- /categories9ba7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3475 --> 
Diese elektronisch gesteuerte Sortieranlage trennt über zwei Lichtschranken Bausteine von drei verschiedenen Längen.