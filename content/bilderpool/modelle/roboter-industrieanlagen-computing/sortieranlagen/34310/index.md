---
layout: "image"
title: "Sortieranlage 8"
date: "2012-02-19T20:37:37"
picture: "FT_Derk_08-2012.jpg"
weight: "19"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34310
- /details921c.html
imported:
- "2019"
_4images_image_id: "34310"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:37:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34310 -->
