---
layout: "image"
title: "Anlage gesamt von vorne"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten01.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16681
- /details043f.html
imported:
- "2019"
_4images_image_id: "16681"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16681 -->
Am 1. Förderband werden die Kasetten mit Hilfe von einer zweiten Lichtschranke, einem Farbsensor und einem Reedkontakt aussortiert. Wenn die Kasette zu groß ist wird sie mit Hilfe des 2., drehbaren Förderbands nach oben ausgeworfen. Wenn sie gelb ist, dann nach unten. Wenn sie grau und magnetisch ist, durchläuft sie das 2. Förderband und wird am 3. Förderband mit Hilfe von 2 Pneumatikzylindern nach unten ausgeworfen. Ist die Kasette "nur" grau, läuft sie bis zum Ende des 3. Förderbandes, hält an der Bearbeitungsstation, die ein Loch bohrt und wird dann ausgeworfen.