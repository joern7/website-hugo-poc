---
layout: "image"
title: "magnetische, graue Kasette bei Auswurf"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten19.jpg"
weight: "19"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16699
- /details7456-3.html
imported:
- "2019"
_4images_image_id: "16699"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16699 -->
Diese Kasette wurde vom Farbsensor als grau und vom Reedkontakt als magnetisch "erkannt" und wird nun am Anfang des 3. Förderbands von den Pneumatikzylindern ausgeworfen.