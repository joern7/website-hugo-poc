---
layout: "overview"
title: "2. Überarbeitete Version"
date: 2020-02-22T08:05:31+01:00
legacy_id:
- /php/categories/1611
- /categoriesc252.html
- /categoriesee59.html
- /categories0273.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1611 --> 
Dies ist die vorerst wegen Bausteinmangels letzte Version meiner Sortieranlage.