---
layout: "image"
title: "Bearbeitungsstation (c)"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion07.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/23584
- /detailsa71c.html
imported:
- "2019"
_4images_image_id: "23584"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23584 -->
