---
layout: "image"
title: "Anlage gesamt mit Beschriftungen"
date: "2009-04-04T17:05:29"
picture: "ueberarbeiteteversion01.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/23578
- /detailsa044-2.html
imported:
- "2019"
_4images_image_id: "23578"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23578 -->
Hinzugekommen sind:
- der Auswerfer am Ende der Schienenstrecke
- eine neue Bearbeitungsstation bestehend aus einem Bohrer, einer Fräse und Luftkühlung
- ein weiteres Förderband, das magnetische Kisten nun nach hinten abtransportiert