---
layout: "image"
title: "Bearbeitungsstation von oben"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion06.jpg"
weight: "6"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/24718
- /detailse061.html
imported:
- "2019"
_4images_image_id: "24718"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24718 -->
