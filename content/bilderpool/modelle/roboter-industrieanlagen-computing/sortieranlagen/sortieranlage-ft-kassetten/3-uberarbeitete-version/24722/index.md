---
layout: "image"
title: "Startsensor"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion10.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/24722
- /details4724-3.html
imported:
- "2019"
_4images_image_id: "24722"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24722 -->
Als Startsensor nutzte ich nun einen Fotowiderstand. Sobald eine Kasette draufgelegt wird und der Sensor vom Umgebungslicht abgedunkelt wird, wird das Band gestartet.