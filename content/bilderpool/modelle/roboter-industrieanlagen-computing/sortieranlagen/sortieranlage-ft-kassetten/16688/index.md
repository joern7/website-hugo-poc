---
layout: "image"
title: "Förderband 3 mit pneumatischem Auswurf und Bearbeitungsstation"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten08.jpg"
weight: "8"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16688
- /detailse5d1-2.html
imported:
- "2019"
_4images_image_id: "16688"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16688 -->
Links die pneumatische Auswurfanlage für magnetische, graue Kasetten, rechts der (solarbetriebene) Bohrer.