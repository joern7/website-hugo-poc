---
layout: "image"
title: "Bearbeitungsstation"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion05.jpg"
weight: "5"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16966
- /details1736-2.html
imported:
- "2019"
_4images_image_id: "16966"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16966 -->
