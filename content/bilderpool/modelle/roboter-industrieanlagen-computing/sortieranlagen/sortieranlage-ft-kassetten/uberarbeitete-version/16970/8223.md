---
layout: "comment"
hidden: true
title: "8223"
date: "2009-01-09T23:52:09"
uploadBy:
- "equester"
license: "unknown"
imported:
- "2019"
---
Die Arme der Stromabnehmer musste ich verlängern, um so mehr Druck auf die Stromschienen aufzubauen. Der Kontakt riss vorher immer mal wieder ab.