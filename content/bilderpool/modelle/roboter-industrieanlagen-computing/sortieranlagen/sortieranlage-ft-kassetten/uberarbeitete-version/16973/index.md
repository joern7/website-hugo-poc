---
layout: "image"
title: "Bedienfeld RoboPro"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion12.jpg"
weight: "12"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16973
- /detailsca1a.html
imported:
- "2019"
_4images_image_id: "16973"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16973 -->
Das überarbeitete Bedienfeld, in dem natürlich der erweiterte Verlauf, durch den Schienenwagen, mitaufgenommen ist.