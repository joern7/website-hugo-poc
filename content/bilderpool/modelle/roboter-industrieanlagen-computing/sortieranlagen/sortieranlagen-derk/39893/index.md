---
layout: "image"
title: "Neue Gripper 5"
date: "2014-12-06T12:36:44"
picture: "DSC_6062.jpg"
weight: "30"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/39893
- /details3126.html
imported:
- "2019"
_4images_image_id: "39893"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2014-12-06T12:36:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39893 -->
