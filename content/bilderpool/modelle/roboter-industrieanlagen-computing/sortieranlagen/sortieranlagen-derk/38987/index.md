---
layout: "image"
title: "Neue Portal Robot 5"
date: "2014-07-03T22:25:50"
picture: "077.jpg"
weight: "21"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/38987
- /details319e-2.html
imported:
- "2019"
_4images_image_id: "38987"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2014-07-03T22:25:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38987 -->
