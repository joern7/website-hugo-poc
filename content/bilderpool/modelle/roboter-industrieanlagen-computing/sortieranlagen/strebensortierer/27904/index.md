---
layout: "image"
title: "7"
date: "2010-08-23T23:25:26"
picture: "strebensortierer07.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27904
- /detailsbd8b-2.html
imported:
- "2019"
_4images_image_id: "27904"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27904 -->
Hier sieht man den Haupttteil der Maschiene, da wird die Strebe hineingeschoben.