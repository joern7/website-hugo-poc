---
layout: "image"
title: "1"
date: "2010-08-23T23:25:25"
picture: "strebensortierer01.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27898
- /details1230-2.html
imported:
- "2019"
_4images_image_id: "27898"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27898 -->
Hier sieht man die Maschiene samt Computer. Da der Computer kein Bluetooth hat, und ich auch noch keinen Bluetooth-Adapter habe, muss der TX immer mit dem Kabel angeschlossen werden.