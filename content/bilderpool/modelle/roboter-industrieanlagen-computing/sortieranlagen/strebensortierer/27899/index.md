---
layout: "image"
title: "2"
date: "2010-08-23T23:25:26"
picture: "strebensortierer02.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27899
- /details0aec.html
imported:
- "2019"
_4images_image_id: "27899"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27899 -->
Hier sieht man die ganze Maschiene. Vorne sind die ganzen sind die Sortierfächer zu sehen.