---
layout: "image"
title: "Messstation (2)"
date: "2013-03-22T10:51:12"
picture: "farbsortierer04.jpg"
weight: "4"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36789
- /details0657.html
imported:
- "2019"
_4images_image_id: "36789"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36789 -->
Unten sieht man quer die Lichtschranke, im abgenommenen oberen Teil die Messvorrichtung.