---
layout: "image"
title: "Drehbarer Roboterarm"
date: "2013-03-22T10:51:12"
picture: "farbsortierer07.jpg"
weight: "7"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36792
- /details94dc.html
imported:
- "2019"
_4images_image_id: "36792"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36792 -->
Ein Power-Motor dreht den Roboter in eine von drei Positionen: Eine zum Teile vom Band Abheben und je eine über den beiden Ablagefächern. Positions- und Endlagetaster für die Drehbewegung sowie fürs Anheben und Absenken des Arms dürfen natürlich nicht fehlen.