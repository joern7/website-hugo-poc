---
layout: "image"
title: "Greifarm"
date: "2013-03-22T10:51:12"
picture: "farbsortierer06.jpg"
weight: "6"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36791
- /details86ae.html
imported:
- "2019"
_4images_image_id: "36791"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36791 -->
Der Greifarm ist recht standardmäßig gebaut. Eine Endlagenfeststellung gibt es nur für "Greifer ist auf". Das Zupacken wird durch eine Zeitverzögerung gesteuert.