---
layout: "image"
title: "Antrieb Förderband 2"
date: "2010-08-28T09:49:15"
picture: "Antrieb_Frderband_II.jpg"
weight: "4"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27986
- /detailsec36-2.html
imported:
- "2019"
_4images_image_id: "27986"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27986 -->
Das 2. Förderband wird auch mit einem Minimotor angetrieben