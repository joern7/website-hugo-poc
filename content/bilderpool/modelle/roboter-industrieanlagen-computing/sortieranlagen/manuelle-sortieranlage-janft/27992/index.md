---
layout: "image"
title: "Ausgabe"
date: "2010-08-28T14:01:10"
picture: "ausgabe_Werkstcke.jpg"
weight: "10"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27992
- /details1706.html
imported:
- "2019"
_4images_image_id: "27992"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T14:01:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27992 -->
Nach dem Schweißen wird das Werkstück abgekühlt und dann in die Ausgabe befördert