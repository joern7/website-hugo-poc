---
layout: "image"
title: "1. Station. Bohrer"
date: "2010-08-28T09:49:16"
picture: "Bohrer.jpg"
weight: "8"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27990
- /details327b.html
imported:
- "2019"
_4images_image_id: "27990"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27990 -->
Der Bohrer ist pneumatisch absenkbar