---
layout: "image"
title: "Schweißstation"
date: "2010-08-28T09:49:16"
picture: "Schweistation.jpg"
weight: "9"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27991
- /detailsf6bf.html
imported:
- "2019"
_4images_image_id: "27991"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27991 -->
Hier wird das Werkstück geschweißt