---
layout: "image"
title: "Stapelmagazin für die Werkstücke"
date: "2010-08-28T09:49:15"
picture: "Stapelmagazin_II.jpg"
weight: "2"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27984
- /details5679.html
imported:
- "2019"
_4images_image_id: "27984"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27984 -->
ein Werkstück wird auf das Förderband geschoben