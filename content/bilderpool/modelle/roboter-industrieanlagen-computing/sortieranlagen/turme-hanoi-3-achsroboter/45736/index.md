---
layout: "image"
title: "3-Achsroboter"
date: "2017-04-13T17:42:39"
picture: "tvha1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45736
- /detailsb083.html
imported:
- "2019"
_4images_image_id: "45736"
_4images_cat_id: "3399"
_4images_user_id: "2228"
_4images_image_date: "2017-04-13T17:42:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45736 -->
Video zum Lösen des Türme von Hanoi Knobelspiel: https://youtu.be/SPtHo2L2yRE

Obwohl der Arduino kein Multithreading erlaubt, kann man die Achsen synchron bewegen (siehe Video), die Encoder werden durch Interrupts ausgelesen (die Impulszähler über Taster zusätzlich entprellt)