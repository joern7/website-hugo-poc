---
layout: "image"
title: "Sortieranlage 12"
date: "2012-02-19T20:40:07"
picture: "FT_Derk_12-2012.jpg"
weight: "23"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34314
- /details2fca.html
imported:
- "2019"
_4images_image_id: "34314"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34314 -->
