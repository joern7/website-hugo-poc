---
layout: "image"
title: "Robo interface mit langen Kabel"
date: "2009-01-31T12:59:11"
picture: "DSC_2075_-_Version_2.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/17221
- /details3ffb.html
imported:
- "2019"
_4images_image_id: "17221"
_4images_cat_id: "1544"
_4images_user_id: "371"
_4images_image_date: "2009-01-31T12:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17221 -->
