---
layout: "image"
title: "Aus Bausteinsicht"
date: "2013-09-17T19:09:15"
picture: "sortieranlage04.jpg"
weight: "4"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/37403
- /detailsc65c.html
imported:
- "2019"
_4images_image_id: "37403"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37403 -->
Als erstes kommen die Lichtschranken (Lange Teile), dann der Reedkontakt.