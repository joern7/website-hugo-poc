---
layout: "image"
title: "TX-Display"
date: "2013-09-17T19:09:15"
picture: "sortieranlage12.jpg"
weight: "12"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/37411
- /details6b94-3.html
imported:
- "2019"
_4images_image_id: "37411"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37411 -->
Man kann die Anlage auch im Download-Modus benutzen.