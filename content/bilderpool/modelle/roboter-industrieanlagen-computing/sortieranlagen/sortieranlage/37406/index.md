---
layout: "image"
title: "Auswurf Magnet"
date: "2013-09-17T19:09:15"
picture: "sortieranlage07.jpg"
weight: "7"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/37406
- /details3941.html
imported:
- "2019"
_4images_image_id: "37406"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37406 -->
Ein Magnet wird gerade Ausgeworfen.