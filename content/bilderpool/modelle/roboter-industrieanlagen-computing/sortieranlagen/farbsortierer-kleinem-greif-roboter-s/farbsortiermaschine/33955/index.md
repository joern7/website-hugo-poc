---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader03.jpg"
weight: "3"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33955
- /details61a9.html
imported:
- "2019"
_4images_image_id: "33955"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33955 -->
Gesamtansicht aus einer leicht veränderten Perspektive.