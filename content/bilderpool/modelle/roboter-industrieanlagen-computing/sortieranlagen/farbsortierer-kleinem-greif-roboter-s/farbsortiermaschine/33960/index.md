---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader08.jpg"
weight: "8"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33960
- /detailsf2b3-2.html
imported:
- "2019"
_4images_image_id: "33960"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33960 -->
Lichtschranke für die Palettenladung