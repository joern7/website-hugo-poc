---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader14.jpg"
weight: "14"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33966
- /detailsab25.html
imported:
- "2019"
_4images_image_id: "33966"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33966 -->
Die beladenen Paletten