---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader12.jpg"
weight: "12"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33964
- /details2a34-2.html
imported:
- "2019"
_4images_image_id: "33964"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33964 -->
Die Senk- bzw.Hebemechanik für die Paletten