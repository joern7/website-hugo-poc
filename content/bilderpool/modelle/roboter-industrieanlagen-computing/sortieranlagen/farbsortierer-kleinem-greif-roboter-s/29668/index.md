---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-11T19:13:19"
picture: "farbsortierermitkleinemachsengreifroboter03.jpg"
weight: "3"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29668
- /details3ba7.html
imported:
- "2019"
_4images_image_id: "29668"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29668 -->
