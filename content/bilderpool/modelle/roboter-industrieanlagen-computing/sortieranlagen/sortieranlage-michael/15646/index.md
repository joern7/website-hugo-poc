---
layout: "image"
title: "Sotieranlage"
date: "2008-09-27T21:15:12"
picture: "SNV80005.jpg"
weight: "1"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15646
- /details974c.html
imported:
- "2019"
_4images_image_id: "15646"
_4images_cat_id: "1438"
_4images_user_id: "820"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15646 -->
