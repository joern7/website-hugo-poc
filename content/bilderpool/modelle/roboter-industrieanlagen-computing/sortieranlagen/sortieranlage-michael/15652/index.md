---
layout: "image"
title: "Sotieranlage (7)"
date: "2008-09-27T21:15:12"
picture: "SNV80012.jpg"
weight: "7"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15652
- /detailsee90.html
imported:
- "2019"
_4images_image_id: "15652"
_4images_cat_id: "1438"
_4images_user_id: "820"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15652 -->
