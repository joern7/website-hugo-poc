---
layout: "image"
title: "Selbsthaltung per Fotozelle"
date: "2016-01-24T16:06:07"
picture: "ursortieranlagemitlestaeben5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42733
- /details0dbd.html
imported:
- "2019"
_4images_image_id: "42733"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42733 -->
Wenn der rechte l-e-Stab Strom erhält, zieht er also an, wenn seine (die obere nämlich) Fotozelle Licht erhält - bei einem BS15 also. Bei einem BS30 bleibt er aus, und er steuert den Motor so herum an, dass BS30 nach hinten, ein BS15 aber vorne abgelegt wird. Der auf der Wippe aufgebaute Kreis von Winkelsteinen 60° und BS15 hat nur einen kleinen Durchlass in Ruhestellung. Ansonsten werden beide Lichtschranken - während der Drehung nämlich - unterbrochen. Bei der Rückwärtsdrehung (BS30) darf der zweite l-e-Stab aber nicht gleich wieder abfallen. Deshalb steuert der auch die Lampe links unten an, die den dritten Fotowiderstand darüber beleuchtet. Der ist parallel zum oberen Fotowiderstand der Lichtschranken geschaltet und bewirkt so eine Selbsthaltung des zweiten l-e-Stabes.