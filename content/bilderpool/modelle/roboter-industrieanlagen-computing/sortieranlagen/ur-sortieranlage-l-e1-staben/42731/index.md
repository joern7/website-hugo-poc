---
layout: "image"
title: "Funktionsweise"
date: "2016-01-24T16:06:07"
picture: "ursortieranlagemitlestaeben3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42731
- /details79f8-2.html
imported:
- "2019"
_4images_image_id: "42731"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42731 -->
Oben werden Bausteine 15 oder Bausteine 30 auf den feststehenden Teil der Rutsche eingelegt. Sie rutschen bis zum untern Anschlag der drehbaren Rutsche. Ein kurzer BS15 unterbricht nur die untere Lichtschranke, ein langer BS30 unterbricht beide Lichtschranken. Anders als im Originalmodell bekam ich das nur zum Funktionieren, wenn ich die Störlichtkappe der oberen Fotozelle ganz abnahm und auf die untere eine Störlichtkappe 8 mm aufsetze (in den Ur-Kästen waren nur Störlichtkappen 4 mm enthalten). Dadurch wird entschieden, ob sich der Drehtisch nach vorne oder nach hinten dreht.