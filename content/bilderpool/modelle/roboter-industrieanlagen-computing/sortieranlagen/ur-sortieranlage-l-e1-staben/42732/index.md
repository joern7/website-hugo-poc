---
layout: "image"
title: "Lichtelektronik-Stäbe"
date: "2016-01-24T16:06:07"
picture: "ursortieranlagemitlestaeben4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42732
- /detailsec01-2.html
imported:
- "2019"
_4images_image_id: "42732"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42732 -->
Die Schaltung zieht echt alle Register: Zunächst wäre da mal, dass der linke l-e-Stab, der von der unteren Fotozelle gesteuert wird... die Stromversorgung des rechten darstellt. Der rechte hängt also nur an der Stromversorgung, wenn der linke durch Unterbrechung der unteren Lichtschranke abgefallen ist.