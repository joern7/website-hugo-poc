---
layout: "image"
title: "Antrieb"
date: "2016-01-24T16:06:07"
picture: "ursortieranlagemitlestaeben6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42734
- /details75ba.html
imported:
- "2019"
_4images_image_id: "42734"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42734 -->
Dieser Ur-mini-mot-1 in echter BS30-Größe ist der einzige Motor im Modell. Damit sein Z10 brauchbar ins Z40 eingreift, muss die Aufhängung des Z40 etwas nach vorne geschoben werden (auf der linken Modellseite natürlich entsprechend).