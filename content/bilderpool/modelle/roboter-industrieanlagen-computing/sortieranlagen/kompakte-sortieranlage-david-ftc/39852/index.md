---
layout: "image"
title: "Magazin"
date: "2014-11-23T19:12:24"
picture: "ksa4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39852
- /detailse144.html
imported:
- "2019"
_4images_image_id: "39852"
_4images_cat_id: "2991"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39852 -->
Magazin für Steine, dahinter Scann-Tunnel