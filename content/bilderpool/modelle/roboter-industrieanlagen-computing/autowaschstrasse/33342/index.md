---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse21.jpg"
weight: "21"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33342
- /detailsa47d-2.html
imported:
- "2019"
_4images_image_id: "33342"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33342 -->
Begin- und End-Position des Portal mit 4mm Magnete + Reedkontakte.