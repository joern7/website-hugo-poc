---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse12.jpg"
weight: "12"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33333
- /detailsda81-2.html
imported:
- "2019"
_4images_image_id: "33333"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33333 -->
Das Portal bewegt sich weiter während sich die Querbürste zu drehen beginnt und die Vertikalbürsten ihre Drehung fortsetzen. Die Querbürste fährt, gesteuert von zwei Laser-Lichtschranken signalen die Höhenkontur des Fahrzeugs ab.