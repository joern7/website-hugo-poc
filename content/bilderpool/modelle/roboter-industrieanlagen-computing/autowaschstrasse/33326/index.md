---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse05.jpg"
weight: "5"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33326
- /details9509.html
imported:
- "2019"
_4images_image_id: "33326"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33326 -->
Übersicht Autowaschstrasse