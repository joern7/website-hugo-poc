---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse03.jpg"
weight: "3"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33324
- /detailsd2d2.html
imported:
- "2019"
_4images_image_id: "33324"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33324 -->
Vorderseite  Autowaschstrasse