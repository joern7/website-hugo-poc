---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse13.jpg"
weight: "13"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33334
- /detailsdbc1.html
imported:
- "2019"
_4images_image_id: "33334"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33334 -->
Das Portal bewegt sich weiter während sich die Querbürste zu drehen beginnt und die Vertikalbürsten ihre Drehung fortsetzen. Die Querbürste fährt, gesteuert von zwei Laser-Lichtschranken signalen die Höhenkontur des Fahrzeugs ab. Ich habe verschiedene billige Laserpointer dafür genutzt.
http://www.conrad.de/ce/de/product/776265/LASERPOINTER-SCHLUeSSELANHAeNGER-ROT

