---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse14.jpg"
weight: "14"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33335
- /detailsba89-2.html
imported:
- "2019"
_4images_image_id: "33335"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33335 -->
Die Querbürste fährt, gesteuert von zwei Laser-Lichtschranken signalen die Höhenkontur des Fahrzeugs ab. 
