---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse20.jpg"
weight: "20"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33341
- /details0ba2.html
imported:
- "2019"
_4images_image_id: "33341"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33341 -->
Der Gebläsetrockner, der wie zuvor die Querbürste von zwei Laser-Lichtschrankensignalen gesteuert wird, fährt die Fahrzeugkontur ab, um einen Trocknungsvorgang zu simulieren. Die obere und untere Endlage von Querbürste und Gebläsetrockner werden durch Schalter erfasst.   

Ich habe herausgefunden dass :
o	Fotozellen mit dem richtingen kleine Störlichtkappen  mit Laserlicht  ebenso gut funktionieren als 
o	Fototransistoren  mit Laserlicht.
