---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse09.jpg"
weight: "9"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33330
- /details591c.html
imported:
- "2019"
_4images_image_id: "33330"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33330 -->
Nach dem Aufeinandertreffen der beiden Vertikalbürsten, das durch einen Reed-Schalter erfasst wird, führen die beiden Vertikalbürsten zusammen eine Hin- und Herbewegung aus, die durch mechanische Endtaster begrenzt wird.