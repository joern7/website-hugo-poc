---
layout: "image"
title: "IMG_20160604_081402"
date: "2018-01-21T09:22:20"
picture: "roboter14.jpg"
weight: "14"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47159
- /details113d-2.html
imported:
- "2019"
_4images_image_id: "47159"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47159 -->
wie oben allerdings mit fertigem drehgelenk