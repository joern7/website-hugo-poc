---
layout: "image"
title: "IMG_20160604_081030"
date: "2018-01-21T09:22:20"
picture: "roboter04.jpg"
weight: "4"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47149
- /detailse8c1.html
imported:
- "2019"
_4images_image_id: "47149"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47149 -->
schwierig mit den beinen aber machbar. 2 servos je oben und unten und schon hat man ober und unterschenkel.
Das hüftgelenk wird von einem servo