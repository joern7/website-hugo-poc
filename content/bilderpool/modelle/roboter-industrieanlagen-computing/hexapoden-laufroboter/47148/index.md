---
layout: "image"
title: "IMG_20160604_081015"
date: "2018-01-21T09:22:20"
picture: "roboter03.jpg"
weight: "3"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47148
- /detailsad9a.html
imported:
- "2019"
_4images_image_id: "47148"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47148 -->
die beinhalterungen sind recht einfach gebaut. 2 schwarze bauplatten oben und unten, 3cm platz dazwischen und dann nur noch zwei querträger um die beine aufzuhängen/befestigen.