---
layout: "image"
title: "Neuer Encoder - basiert auf Entwurf von Stefan Falk"
date: "2014-02-23T18:04:05"
picture: "IMG_0001.jpg"
weight: "22"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38367
- /detailsb35a.html
imported:
- "2019"
_4images_image_id: "38367"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T18:04:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38367 -->
Ich habe jetzt den Encoder von Stefan Falk implementiert - trägt etwas dicker auf als Stefans Original, bedingt durch das Z10 an der Spindel aber leider durch Platzprobleme mutiert (die Spindel hat mehr Durchmesser als der Rast-Flansch der neuen Z10. aber gibt schön 10 Impulse pro Umdrehung, wenn man die "Einsen" nimmt - wenn man auf Flanke (also o-1 und 1-0 - Sprünge geht, verdoppelt sich die Auflösung nochmals..