---
layout: "comment"
hidden: true
title: "18746"
date: "2014-02-16T21:35:06"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein Patent auf die Idee kann ich allerdings nicht anmelden. Nur für den Fall, dass das tatsächlich nicht allseits bekannt sein sollte: Die I-Streben sind im 15-mm-Raster, passen also immer längs entlang von Statikträgern. Die X-Streben sind im sqrt(15*15+15*15) mm-Raster, sprich: Sie passen immer im 45°-Winkel ins ft-Raster. Besonderheiten: a) Eine I-Strebe 105 gibt es leider nicht, das war ft wohl zu nah an der X-Strebe 106. b) Eine I-Strebe 75 passt winkelig auch ins Raster, weil 75 mm = 5 * 15 mm, und 5" = 25 = 9 + 16 = 3" + 4". Sprich: I-Streben 45 und 60 ergeben im Dreieck mit einer I-Strebe 75 zufällig auch exakt einen rechten Winkel.

Gruß,
Stefan