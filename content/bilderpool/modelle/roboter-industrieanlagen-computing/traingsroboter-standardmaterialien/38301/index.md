---
layout: "image"
title: "Armmechanik"
date: "2014-02-15T20:10:40"
picture: "IMG_0023.jpg"
weight: "9"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38301
- /details0188.html
imported:
- "2019"
_4images_image_id: "38301"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-15T20:10:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38301 -->
