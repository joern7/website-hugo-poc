---
layout: "image"
title: "FT-DCF77-Kuckucksuhr"
date: "2013-07-28T19:13:47"
picture: "kuckucksuhr5.jpg"
weight: "5"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37205
- /details7cbf.html
imported:
- "2019"
_4images_image_id: "37205"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-28T19:13:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37205 -->
Antrieb dieser Kuckucks-Blasebälge mit eine kleine Motorantrieb.
Für 1 mal "Kuckuck" reicht eine Achse-Umdrehung mit Nocke + Schalter.