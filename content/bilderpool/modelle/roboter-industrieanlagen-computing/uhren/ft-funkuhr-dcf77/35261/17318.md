---
layout: "comment"
hidden: true
title: "17318"
date: "2012-10-01T13:44:05"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
die Software-Version meiner auf der Convention gezeigten ft-Funkuhr mit drei LED-Displays (v2.1) habe ich hochgeladen - Du solltest sie in Kürze unter http://www.ftcommunity.de/downloads.php?kategorie=RoboPro finden können.
Gruß, Dirk