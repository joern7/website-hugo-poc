---
layout: "overview"
title: "Tastereigendiagnose"
date: 2020-02-22T08:08:43+01:00
legacy_id:
- /php/categories/2909
- /categories3794.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2909 --> 
Programmierung eines Tasters, welcher auf Funktionstüchtigkeit überprüft wird.