---
layout: "image"
title: "Oberfläche Taster defekt"
date: "2014-06-03T18:59:30"
picture: "Oberflche_Taster_defekt.jpg"
weight: "5"
konstrukteure: 
- "bauFischertechnik"
fotografen:
- "bauFischertechnik"
keywords: ["ROBOPro", "Taster", "Eigendiagnose", "TX", "Controller", "Wiederstand"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/38908
- /details9d64.html
imported:
- "2019"
_4images_image_id: "38908"
_4images_cat_id: "2909"
_4images_user_id: "2086"
_4images_image_date: "2014-06-03T18:59:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38908 -->
rotes Lämpchen warnt vor defekt