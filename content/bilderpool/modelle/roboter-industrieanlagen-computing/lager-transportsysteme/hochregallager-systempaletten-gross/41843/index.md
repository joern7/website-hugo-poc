---
layout: "image"
title: "Detailansicht Drehkranz"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten11.jpg"
weight: "11"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41843
- /detailsaffb.html
imported:
- "2019"
_4images_image_id: "41843"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41843 -->
Getriebeansicht der 90°-Übersetzung vom Encodermotor zur Schnecke am Drehkranz. Diese macht eine sogar noch genauere Positionierung möglich