---
layout: "image"
title: "Drehkranz"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten29.jpg"
weight: "29"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41861
- /detailsea7e-2.html
imported:
- "2019"
_4images_image_id: "41861"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41861 -->
Hier sieht man sehr gut wie der Drehkranz das Aluprofil nach links oder rechts drückt. Die Bauweise erlaubt es dass er genau in der Mitte der Zahnstange ist. Ich musste es zwar anpassen aber, dass es geht ist gut!