---
layout: "image"
title: "Verkabelung"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten03.jpg"
weight: "3"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41835
- /details500a.html
imported:
- "2019"
_4images_image_id: "41835"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41835 -->
Ich habe mal provisorisch verkabelt um das Ganze zu testen und zu steuern. Dabei war der RBG-Turm zu wackelig bei unsanften vor und zurückfahren.