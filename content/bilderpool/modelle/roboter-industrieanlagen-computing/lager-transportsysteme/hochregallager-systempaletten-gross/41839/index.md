---
layout: "image"
title: "Draufsicht ohne Palette"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten07.jpg"
weight: "7"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41839
- /details2fb1.html
imported:
- "2019"
_4images_image_id: "41839"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41839 -->
Man sieht hier die Aufnahme für die Positionierung der Palette