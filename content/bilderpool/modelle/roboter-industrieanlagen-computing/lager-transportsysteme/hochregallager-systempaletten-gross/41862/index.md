---
layout: "image"
title: "Mechanismus"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten30.jpg"
weight: "30"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41862
- /details47cc-2.html
imported:
- "2019"
_4images_image_id: "41862"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41862 -->
Hier das letzte Bild und eine weitere Detailaufnahme der Führung. Ich habe bei dem Palettengerät kein Wert auf die Größe gelegt, dass haben schließlich die Konstrukteure bei Staudinger bewiesen. Ich wollte mit Standard-fischertechnikteilen eine Bauweise bauen, die jeder nachbauen kann wenn er möchte. Wenn genauere Detailfotos von irgendetwas gewünscht sind, bitte melden! Ich werde das hier nicht zerlegen, das restliche HRL jedoch schon. Es war viel Aufwand alles aufeinander abzustimmen und so zu bauen. Ich habe etwa 5 Versuche gebraucht bis ich hier war. Jedes Mal war die Stabilität oder die Ausfahrweite das Problem. Es ist auch nicht perfekt da zum Beispiel die Positionsabfrage gelöst werden muss und Gewicht und Größe sehr hoch sind.