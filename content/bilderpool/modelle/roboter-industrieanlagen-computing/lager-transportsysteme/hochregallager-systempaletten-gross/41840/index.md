---
layout: "image"
title: "Die Palette(n)"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten08.jpg"
weight: "8"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41840
- /detailsf1e2-2.html
imported:
- "2019"
_4images_image_id: "41840"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41840 -->
Die Systempaletten sind alle so aufgebaut. beim Aufnehmen Positioniert sicht die Palette in der Aufnahme durch das Gewicht