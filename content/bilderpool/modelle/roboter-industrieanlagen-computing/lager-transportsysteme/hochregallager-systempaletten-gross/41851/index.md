---
layout: "image"
title: "Vorne ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten19.jpg"
weight: "19"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41851
- /details2efc.html
imported:
- "2019"
_4images_image_id: "41851"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41851 -->
Vorderansicht bei ausgefahrenem Teleskop