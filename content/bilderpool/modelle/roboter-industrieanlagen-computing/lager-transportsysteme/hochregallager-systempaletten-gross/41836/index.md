---
layout: "image"
title: "Gegengewicht"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten04.jpg"
weight: "4"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41836
- /detailsf36f.html
imported:
- "2019"
_4images_image_id: "41836"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41836 -->
Das Gewicht reicht aus, damit der RBG-Turm nicht von allein umkippt. Mit einer geeigneten Ansteuerung hätte man das Anfahren und Bremsen sanft machen können. Jedes Fach hatte zwei Taster an den Seiten für die Anfahrposition ohne und die Wegfahrposition mit Palette. Daher die 2 TX-Controller. Es war geplant, dass sie mit weiteren Controllern kommunizieren und ein Förderband mit verschiedenen Strecken bzw. auch mit Drehtischen zu bauen. Das gute an der Befestigung am RBG: Ich musste keine so langen Kabel verlegen und für die beiden seitlichen Energieketten wäre das durch die Anzahl bedingt gar nicht gegangen.