---
layout: "image"
title: "Luftspeicher"
date: "2008-12-16T18:07:11"
picture: "johannes11.jpg"
weight: "11"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16635
- /details7f66.html
imported:
- "2019"
_4images_image_id: "16635"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16635 -->
