---
layout: "image"
title: "Kompressor"
date: "2008-12-16T18:07:11"
picture: "johannes12.jpg"
weight: "12"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16636
- /details67a1.html
imported:
- "2019"
_4images_image_id: "16636"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16636 -->
Unter dieser Abdeckung befindet sich der Kompressor, der aus 2 Zylindern besteht und für ausreichenden Druck sorgt.