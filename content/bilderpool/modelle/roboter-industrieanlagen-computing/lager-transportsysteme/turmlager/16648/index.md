---
layout: "image"
title: "Ausgefahrerner Greifer"
date: "2008-12-16T18:07:12"
picture: "johannes24.jpg"
weight: "24"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16648
- /detailse823.html
imported:
- "2019"
_4images_image_id: "16648"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16648 -->
