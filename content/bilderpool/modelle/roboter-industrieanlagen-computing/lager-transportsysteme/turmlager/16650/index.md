---
layout: "image"
title: "Greifer mit Werkstück"
date: "2008-12-16T18:07:12"
picture: "johannes26.jpg"
weight: "26"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16650
- /details0d22.html
imported:
- "2019"
_4images_image_id: "16650"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16650 -->
