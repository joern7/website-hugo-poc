---
layout: "image"
title: "Offener Greifer"
date: "2008-12-16T18:07:12"
picture: "johannes25.jpg"
weight: "25"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16649
- /details6918-3.html
imported:
- "2019"
_4images_image_id: "16649"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16649 -->
