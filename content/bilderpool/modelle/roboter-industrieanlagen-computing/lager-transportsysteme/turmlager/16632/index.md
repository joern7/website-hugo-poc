---
layout: "image"
title: "Förderband"
date: "2008-12-16T18:07:11"
picture: "johannes08.jpg"
weight: "8"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16632
- /details6bf1-2.html
imported:
- "2019"
_4images_image_id: "16632"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16632 -->
Wenn ein Werkstück auf das Förderband gestellt wird startet der Einlagerungsvorgang.