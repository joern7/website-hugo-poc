---
layout: "image"
title: "Zylinder zum Vorschieben des Greifers"
date: "2008-12-16T18:07:12"
picture: "johannes22.jpg"
weight: "22"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16646
- /details2c6d.html
imported:
- "2019"
_4images_image_id: "16646"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16646 -->
Zum Vorschieben des Greifers werden 2 einfach wirkende Zylinder mit Feder und roter Kolbenstange verwendet.