---
layout: "image"
title: "Drehantrieb"
date: "2008-12-16T18:07:12"
picture: "johannes20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16644
- /details5a34.html
imported:
- "2019"
_4images_image_id: "16644"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16644 -->
Am Drehantrieb befindet sich ein Impulstaster, der die Zurückgelegte Strecke misst, und somit für das Erreichen der einzelnen Fächer zuständig ist.