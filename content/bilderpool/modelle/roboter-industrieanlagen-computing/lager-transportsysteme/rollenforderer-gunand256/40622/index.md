---
layout: "image"
title: "Rollenförderer von vorne"
date: "2015-03-10T18:15:07"
picture: "rofoe1.jpg"
weight: "1"
konstrukteure: 
- "gunand256"
fotografen:
- "gunand256"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40622
- /detailse70c.html
imported:
- "2019"
_4images_image_id: "40622"
_4images_cat_id: "3048"
_4images_user_id: "2357"
_4images_image_date: "2015-03-10T18:15:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40622 -->
Wird der Taster ganz im Vordergrund betätigt, kommt die unterste Schachtel auf den Förderer, der dann auch anfängt zu laufen. Ist die Schachtel dann an der Füllstation angekommen, stoppt der Förderer und ein Zylinder wird in die Kassette gefüllt. Dann laufen die Rollen wieder los und transportieren die Kassette an das Ausgabeende. Dann kann man die Kassette dort wegnehmen und nochmal auf den Taster tippen.