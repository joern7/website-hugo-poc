---
layout: "image"
title: "HRL"
date: "2006-03-05T12:27:38"
picture: "Fischertechnik_010.jpg"
weight: "5"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5810
- /details088f-2.html
imported:
- "2019"
_4images_image_id: "5810"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-05T12:27:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5810 -->
