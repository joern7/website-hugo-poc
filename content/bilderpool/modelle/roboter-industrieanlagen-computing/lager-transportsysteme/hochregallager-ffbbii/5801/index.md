---
layout: "image"
title: "HRL Roboter (noch im Bau)"
date: "2006-03-03T18:23:07"
picture: "Fischertechnik_007.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5801
- /details1905.html
imported:
- "2019"
_4images_image_id: "5801"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-03T18:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5801 -->
