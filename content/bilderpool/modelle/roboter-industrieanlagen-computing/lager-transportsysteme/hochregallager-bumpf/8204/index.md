---
layout: "image"
title: "Stapler"
date: "2006-12-30T09:56:05"
picture: "HRL_Fischertechnik_030.jpg"
weight: "8"
konstrukteure: 
- "Walter-Mario Graf (Bumpf)"
fotografen:
- "Walter-Mario Graf (Bumpf)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8204
- /details59af-3.html
imported:
- "2019"
_4images_image_id: "8204"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8204 -->
