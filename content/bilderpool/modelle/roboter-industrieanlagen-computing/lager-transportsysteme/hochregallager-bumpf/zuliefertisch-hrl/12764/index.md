---
layout: "image"
title: "Zuliefertisch HRL 2"
date: "2007-11-18T00:51:46"
picture: "bumpf2.jpg"
weight: "2"
konstrukteure: 
- "bumpf"
fotografen:
- "walter mario graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12764
- /details23f4-2.html
imported:
- "2019"
_4images_image_id: "12764"
_4images_cat_id: "1150"
_4images_user_id: "424"
_4images_image_date: "2007-11-18T00:51:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12764 -->
Tisch ausgefahren