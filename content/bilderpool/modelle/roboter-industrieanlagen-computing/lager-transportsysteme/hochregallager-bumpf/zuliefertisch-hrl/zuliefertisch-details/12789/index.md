---
layout: "image"
title: "Zuliefertisch Details"
date: "2007-11-21T17:41:16"
picture: "zuliefertischdetails1.jpg"
weight: "1"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12789
- /detailsecd0-3.html
imported:
- "2019"
_4images_image_id: "12789"
_4images_cat_id: "1152"
_4images_user_id: "424"
_4images_image_date: "2007-11-21T17:41:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12789 -->
Links im Bild sieht man die zugeschliffenen Verbindungstücke die als Führung für den Tisch dienen.