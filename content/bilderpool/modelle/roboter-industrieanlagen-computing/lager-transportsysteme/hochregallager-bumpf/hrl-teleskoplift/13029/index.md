---
layout: "image"
title: "Ansicht HRL"
date: "2007-12-12T07:32:24"
picture: "hrlmitteleskoplift1.jpg"
weight: "1"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13029
- /detailsc55b.html
imported:
- "2019"
_4images_image_id: "13029"
_4images_cat_id: "1182"
_4images_user_id: "424"
_4images_image_date: "2007-12-12T07:32:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13029 -->
Neu entwickelter 360° drehender Teleskoplift.
Hier ein Beispiel mit einem dreiseitigen HRL.