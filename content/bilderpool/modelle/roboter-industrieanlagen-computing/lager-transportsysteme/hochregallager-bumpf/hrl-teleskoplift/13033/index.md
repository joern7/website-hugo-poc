---
layout: "image"
title: "Ansicht von unten"
date: "2007-12-12T07:32:25"
picture: "hrlmitteleskoplift5.jpg"
weight: "5"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13033
- /detailsfac1-2.html
imported:
- "2019"
_4images_image_id: "13033"
_4images_cat_id: "1182"
_4images_user_id: "424"
_4images_image_date: "2007-12-12T07:32:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13033 -->
Lift ausgefahren