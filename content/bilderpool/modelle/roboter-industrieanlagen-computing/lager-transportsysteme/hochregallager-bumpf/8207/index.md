---
layout: "image"
title: "Bedienfeld"
date: "2006-12-30T09:56:29"
picture: "HRL_Fischertechnik_002.jpg"
weight: "11"
konstrukteure: 
- "Walter-Mario Graf (Bumpf)"
fotografen:
- "Walter-Mario Graf (Bumpf)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8207
- /details5d89.html
imported:
- "2019"
_4images_image_id: "8207"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8207 -->
Hier kann ich per Mausclick die Kassetten Ein- und Auslagern.