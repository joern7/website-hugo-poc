---
layout: "image"
title: "Tunnel abgedeckt"
date: "2006-12-31T12:49:52"
picture: "HRL_Fischertechnik_Detail_002.jpg"
weight: "17"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8233
- /details3c8a.html
imported:
- "2019"
_4images_image_id: "8233"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-31T12:49:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8233 -->
Mit dem Fotowiderstand könnte ich auch verschiedene Farben einlesen. Im  jetzigen Zeitpunkt ist er nur auf Weiss eingestellt, was auf falsche Lage hindeutet.
Versuche mit verschiedenen Farben waren erfolgreich. 
Warum die Farben?
Ich könnte so mein HRL weiter ausbauen. Es würde dann nicht nur zB. ein Schwarz 5 sondern auch ein Blau 5 oder ein Grün 5 usw. geben.