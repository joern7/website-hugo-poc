---
layout: "image"
title: "Tunnel"
date: "2006-12-30T09:56:05"
picture: "HRL_Fischertechnik_022.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (Bumpf)"
fotografen:
- "Walter-Mario Graf (Bumpf)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8200
- /detailsf768.html
imported:
- "2019"
_4images_image_id: "8200"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8200 -->
Hier wird die Kassette auf die richtige Lage geprüft. Befindet sich der Code auf der falschen Seite geht Sie zurück auf den Wendeplatz und wird gewendet.