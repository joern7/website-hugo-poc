---
layout: "comment"
hidden: true
title: "7410"
date: "2008-10-01T12:13:47"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Die doppelte Auslegung vom Motor auf die beiden Abtriebswellen dürfte mit dem Drehmoment zu tun haben. Könnte sein die Ketten sind kaputt gegangen oder das Zahnrad rutscht auf der Welle.

Offenbart natürlich auch das scheinbar keine Anfahr- und Bremsrampen für den Aufzug existieren.

Warum finden wir hier eigentlich nur das Negative an dem Modell? Insgesamt finde ich es nämlich auch toll :)