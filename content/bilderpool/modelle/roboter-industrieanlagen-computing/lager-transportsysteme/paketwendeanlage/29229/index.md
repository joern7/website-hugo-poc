---
layout: "image"
title: "Paketwendeanlage mit IR detector IS471F"
date: "2010-11-13T12:29:45"
picture: "ISF471F-Paketwendeanlage_019.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29229
- /details281c.html
imported:
- "2019"
_4images_image_id: "29229"
_4images_cat_id: "2114"
_4images_user_id: "22"
_4images_image_date: "2010-11-13T12:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29229 -->
Ohne Problemen mit Fremdlichteinstrahlung,  Paketten mit und ohne weisses Etikett  sortieren mit IR detector IS471F :
http://www.conrad.de/ce/de/product/185094/MODULIERTER-IR-DETEKTOR-IS471F-SH