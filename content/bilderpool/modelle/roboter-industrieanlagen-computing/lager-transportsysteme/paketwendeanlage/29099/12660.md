---
layout: "comment"
hidden: true
title: "12660"
date: "2010-10-31T09:49:30"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Damit gibt es keine Problemen mit Fremdlichteinstrahlung wie bei:
- LDR-Fotowiderstand
- Fototransistor  (wie oben beim Paket-anlage)
- IR-Spursensor
- Optischer Farbsensor
überwiege ich ein MODULIERTER IR DETEKTOR IS471F-SH
Conrad Best.-Nr.: 185094 - 62 

Dieser Infrarot-Detektor verfügt über ein integriertes Modulationssystem. Über eine Zweidraht-Leitung wird der IR-Sender moduliert. Fremdlichteinstrahlung mit hoher Intensität wird durch einen Komparator eliminiert und als TTL-Signal ausgegeben. Durch dieses Verfahren wird verhindert, dass Messwertverfälschungen auftreten.

http://www.conrad.de/ce/de/product/185094/MODULIERTER-IR-DETEKTOR-IS471F-SH

http://www.conrad.de/ce/ProductDetailImage.html?image=/medias/global/ce/1000_1999/1800/1850/1850/185094_SZ_00_FB.EPS_400.jpg

Han hat mit schon  dieser Link gegeben für  IR detector IS471F on youtube here.

Wird es ohne Problemen mit Fremdlichteinstrahlung möglich sein mit dem  IR detector IS471F Paketten mit und ohne weisses  Etikett zu sortieren ?

Grüss,

Peter
Poederoyen NL