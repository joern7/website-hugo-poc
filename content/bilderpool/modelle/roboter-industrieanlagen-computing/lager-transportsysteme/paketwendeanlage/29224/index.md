---
layout: "image"
title: "Paketwendeanlage mit IR detector IS471F"
date: "2010-11-13T12:29:44"
picture: "ISF471F-Paketwendeanlage_003.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29224
- /details8c59.html
imported:
- "2019"
_4images_image_id: "29224"
_4images_cat_id: "2114"
_4images_user_id: "22"
_4images_image_date: "2010-11-13T12:29:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29224 -->
Ohne Problemen mit Fremdlichteinstrahlung,  Paketten mit und ohne weisses Etikett  sortieren mit IR detector IS471F :
http://www.conrad.de/ce/de/product/185094/MODULIERTER-IR-DETEKTOR-IS471F-SH