---
layout: "image"
title: "Regalanlage"
date: "2007-06-26T22:17:29"
picture: "Regalanlage_22.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- /php/details/10918
- /detailsc234.html
imported:
- "2019"
_4images_image_id: "10918"
_4images_cat_id: "990"
_4images_user_id: "341"
_4images_image_date: "2007-06-26T22:17:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10918 -->
