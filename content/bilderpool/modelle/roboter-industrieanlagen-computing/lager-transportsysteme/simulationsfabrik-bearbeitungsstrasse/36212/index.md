---
layout: "image"
title: "Werkstück-Wendeanlage"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager38.jpg"
weight: "38"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36212
- /details1377-3.html
imported:
- "2019"
_4images_image_id: "36212"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36212 -->
