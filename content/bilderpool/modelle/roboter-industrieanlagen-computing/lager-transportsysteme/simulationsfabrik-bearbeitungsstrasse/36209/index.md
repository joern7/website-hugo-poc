---
layout: "image"
title: "Linke Ecke oben"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager35.jpg"
weight: "35"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36209
- /details4566-2.html
imported:
- "2019"
_4images_image_id: "36209"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36209 -->
von links nach rechts, Liftabnehmer, Scannertunnel, Eckschieber 3, Pneumatischer Magnet-Umsetzer, 
im Hintergrund, Kompressor und Eckschieber 4
am linken Bildrand, die Kompressor Endabschaltung