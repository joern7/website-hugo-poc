---
layout: "image"
title: "Unten mitte"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager08.jpg"
weight: "8"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36182
- /details7bc4.html
imported:
- "2019"
_4images_image_id: "36182"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36182 -->
von rechts nach links, Frässtation CNC 1, Frässtation CNC 2, Bohrstation