---
layout: "image"
title: "Tragrahmen"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager60.jpg"
weight: "60"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36234
- /details5bdc-3.html
imported:
- "2019"
_4images_image_id: "36234"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36234 -->
Das Traggerüst für die obere Ebene besteht aus sechs Doppelstützen, gebaut aus  jeweils mehreren BS30 und dahinter aus einem Aluprofil.  
Die Stützen sind seitlich mehrfach mit Anbauwinkel verankert und komplett mit Bauplatten verkleidet.
Oben auf den Stützen sitzt ein Leiterrahmen aus Aluprofilen, was das Traggerüst sehr massiv und stabil macht.