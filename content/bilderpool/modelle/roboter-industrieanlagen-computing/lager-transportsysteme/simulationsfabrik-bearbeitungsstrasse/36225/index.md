---
layout: "image"
title: "Von rechts"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager51.jpg"
weight: "51"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36225
- /details4753.html
imported:
- "2019"
_4images_image_id: "36225"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36225 -->
