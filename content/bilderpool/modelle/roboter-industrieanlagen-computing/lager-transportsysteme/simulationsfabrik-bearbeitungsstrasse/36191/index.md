---
layout: "image"
title: "Mitte von oben"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager17.jpg"
weight: "17"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36191
- /details49df.html
imported:
- "2019"
_4images_image_id: "36191"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36191 -->
