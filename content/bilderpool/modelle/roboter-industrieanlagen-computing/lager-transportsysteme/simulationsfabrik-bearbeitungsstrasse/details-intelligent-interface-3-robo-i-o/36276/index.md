---
layout: "image"
title: "ROBO Interface + 3 ROBO I/O Extension Verkabelung im Detail"
date: "2012-12-15T13:32:46"
picture: "dftextak4.jpg"
weight: "4"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36276
- /details925b.html
imported:
- "2019"
_4images_image_id: "36276"
_4images_cat_id: "2693"
_4images_user_id: "941"
_4images_image_date: "2012-12-15T13:32:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36276 -->
Die Datenkabel sind aus alten IDE Festplattenkabeln selbstgemacht.

Die passenden Steckern gibts z.B. bei Conrad / Pollin / usw.