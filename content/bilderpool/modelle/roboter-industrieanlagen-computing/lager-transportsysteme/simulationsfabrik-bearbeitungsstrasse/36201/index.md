---
layout: "image"
title: "Linke Ecke hinten unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager27.jpg"
weight: "27"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36201
- /details1a82-2.html
imported:
- "2019"
_4images_image_id: "36201"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36201 -->
von rechts nach links, Stanze und Aufzug