---
layout: "image"
title: "Von rechts unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager20.jpg"
weight: "20"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36194
- /detailsbe1e.html
imported:
- "2019"
_4images_image_id: "36194"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36194 -->
von rechts nach links, Werkstückrutsche, Anlauf-Registerlage, Putzstation, Schweißstation, CNC1, CNC2