---
layout: "image"
title: "Von rechts"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager53.jpg"
weight: "53"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36227
- /details5b7c.html
imported:
- "2019"
_4images_image_id: "36227"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36227 -->
