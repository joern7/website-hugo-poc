---
layout: "image"
title: "Rechte Ecke unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager05.jpg"
weight: "5"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36179
- /detailsbf3b.html
imported:
- "2019"
_4images_image_id: "36179"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36179 -->
von rechts nach links, Putzstation, Eckschieber, Schweißstation, Frässtation CNC 1