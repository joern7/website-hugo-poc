---
layout: "image"
title: "Abnehmbarer Teil des Tragrahmens"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager63.jpg"
weight: "63"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36237
- /details1a2e.html
imported:
- "2019"
_4images_image_id: "36237"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36237 -->
Der vordere Teil des Leiterrahmens beherbergt die LED Beleuchtung für die untere Ebene und lässt sich bei Bedarf abnehmen.

LED Beleuchtung für die untere Ebene im Detail.