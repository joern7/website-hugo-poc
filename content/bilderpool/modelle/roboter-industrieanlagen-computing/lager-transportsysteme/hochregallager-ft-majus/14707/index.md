---
layout: "image"
title: "HRL2"
date: "2008-06-18T06:55:42"
picture: "115_1571.jpg"
weight: "2"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14707
- /details3155.html
imported:
- "2019"
_4images_image_id: "14707"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T06:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14707 -->
Hier erkennt man den Einlagerer. Zum hochhalten und transportieren der Kisten besitzt er einen Vakuumgreifer.