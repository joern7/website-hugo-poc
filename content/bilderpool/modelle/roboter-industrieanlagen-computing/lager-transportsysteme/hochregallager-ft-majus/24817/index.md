---
layout: "image"
title: "Förderband 1"
date: "2009-08-22T13:19:15"
picture: "IMG_3765.jpg"
weight: "65"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24817
- /details77c1-2.html
imported:
- "2019"
_4images_image_id: "24817"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24817 -->
Der Lichttaster des 1. Förderbandes.