---
layout: "image"
title: "Vakuumgreifer"
date: "2008-07-16T20:44:28"
picture: "117_1730.jpg"
weight: "35"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14907
- /detailse3a4.html
imported:
- "2019"
_4images_image_id: "14907"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-16T20:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14907 -->
Der Vakuumgreifer zum transpotieren der Kästchen. Daneben befindet sich ein Taster, mitdem erkannt wird, wenn sich der Greifer auf dem Kästchen befindet. Darüber befindet sich eine CNY70, die im Regal erkennt, ob sich im Fach ein Kästchen befindet oder nicht.