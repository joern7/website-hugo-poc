---
layout: "comment"
hidden: true
title: "12758"
date: "2010-11-13T16:59:59"
uploadBy:
- "ft-majus"
license: "unknown"
imported:
- "2019"
---
hi,
das ist ein sogenannter CNY70. Er ersetzt die ft-Lichtschranke
Ein Infrarotsensor, bei dem sich Sender und Empfänger in einem Baustein befinden.

Für optoelektronische Abtast- und Schalteinrichtung (kontaktloser Schalter), z. B. Farbmarkenerkennung, Codierscheibenabtastung, Linienerkennung.

Um ihn an einen Eingang am ft-Interface, muss man eine Transistor - Verstärkerschaltung zwischenschalten.

Ich habe sie aufgrund einer geringen Reichweite durch Infrarotlichtschranke der Marke sich ersetzt: 
http://ftcommunity.de/details.php?image_id=25892