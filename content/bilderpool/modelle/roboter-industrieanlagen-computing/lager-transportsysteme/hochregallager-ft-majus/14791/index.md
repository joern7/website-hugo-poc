---
layout: "image"
title: "Förderband 4"
date: "2008-06-29T18:30:37"
picture: "116_1649.jpg"
weight: "16"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14791
- /detailsa796.html
imported:
- "2019"
_4images_image_id: "14791"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-29T18:30:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14791 -->
Nochmal das neue Förderband.