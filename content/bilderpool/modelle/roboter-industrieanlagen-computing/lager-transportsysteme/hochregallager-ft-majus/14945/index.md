---
layout: "image"
title: "Z-Achse"
date: "2008-07-22T20:51:15"
picture: "117_1756.jpg"
weight: "36"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14945
- /details7503-2.html
imported:
- "2019"
_4images_image_id: "14945"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-22T20:51:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14945 -->
Weitere Ansicht der Z-Achse.