---
layout: "image"
title: "Neu Verkabelung"
date: "2009-03-15T15:30:04"
picture: "125_2536.jpg"
weight: "51"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23468
- /details40c6.html
imported:
- "2019"
_4images_image_id: "23468"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-03-15T15:30:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23468 -->
Gesamtansicht mit neuer Verkabelung.