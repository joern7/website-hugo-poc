---
layout: "image"
title: "HRL"
date: "2008-06-18T18:08:48"
picture: "115_1589.jpg"
weight: "5"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14710
- /detailsb0e4.html
imported:
- "2019"
_4images_image_id: "14710"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T18:08:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14710 -->
Der Antrieb der Z-Achse erfolgt über ein Hubgetriebe. An der Z-Achse befindet sich der Vakuumsauger und das dazugehörende Magnetventil.