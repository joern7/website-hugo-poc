---
layout: "image"
title: "Förderbänder"
date: "2008-10-14T21:37:50"
picture: "120_2031.jpg"
weight: "39"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/15984
- /details360f.html
imported:
- "2019"
_4images_image_id: "15984"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15984 -->
An dem ersten Förderband und dem Drehtisch gab es bis auf die Verkabelung kaum Änderungen.