---
layout: "image"
title: "Drehtisch"
date: "2009-04-13T11:49:10"
picture: "126_2635.jpg"
weight: "59"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23683
- /details356e.html
imported:
- "2019"
_4images_image_id: "23683"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-13T11:49:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23683 -->
Die Positionierung des Drehtisches Erfolg auf der einen Seite über einen Reed-Kontakt, auf der anderen über einen Taster.