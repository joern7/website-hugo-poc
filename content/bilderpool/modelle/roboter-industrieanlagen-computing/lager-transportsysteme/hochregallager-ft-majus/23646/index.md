---
layout: "image"
title: "Neu"
date: "2009-04-09T14:13:23"
picture: "125_2572.jpg"
weight: "56"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23646
- /detailsd70d.html
imported:
- "2019"
_4images_image_id: "23646"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-09T14:13:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23646 -->
Der Kompressor befindet sich nun hinter dem HRL. Daneben befindet sich ein Podest, auf dem die ausgelagerten Kästchen landen.