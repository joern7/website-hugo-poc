---
layout: "image"
title: "Neu"
date: "2009-04-09T14:13:23"
picture: "125_2569.jpg"
weight: "54"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23644
- /details2f45.html
imported:
- "2019"
_4images_image_id: "23644"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-09T14:13:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23644 -->
Die ersten beiden Förderbänder der Förderbandanlage.