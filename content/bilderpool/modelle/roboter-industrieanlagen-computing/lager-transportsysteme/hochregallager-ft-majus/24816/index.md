---
layout: "image"
title: "Förderbänder"
date: "2009-08-22T13:19:14"
picture: "IMG_3763.jpg"
weight: "64"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24816
- /detailsd711.html
imported:
- "2019"
_4images_image_id: "24816"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24816 -->
Die Förderbänder bekamen Lichttaster zur Erkennung der Kassetten. Auch die Farbsortierung geschieht per Lichttaster.