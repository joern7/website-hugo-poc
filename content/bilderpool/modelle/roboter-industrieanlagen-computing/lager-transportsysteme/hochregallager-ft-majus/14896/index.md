---
layout: "image"
title: "Einlagerer"
date: "2008-07-16T20:44:27"
picture: "117_1704.jpg"
weight: "26"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14896
- /detailse45a-3.html
imported:
- "2019"
_4images_image_id: "14896"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-16T20:44:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14896 -->
Der Einlagerer nach einigen Umbauarbeiten, die vor allem an der Befestigung des "Turmes" und an der Z-Achse stattfanden