---
layout: "image"
title: "Gesamtansicht Förderbänder"
date: "2008-06-29T18:30:36"
picture: "116_1631.jpg"
weight: "9"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14784
- /detailsa885.html
imported:
- "2019"
_4images_image_id: "14784"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-29T18:30:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14784 -->
Anstatt des Fördertisches gibt es nun nur ein einfaches Förderband, da ich mir auch einen EEprom-Speicher bauen möchte und dazu die Ein -und Ausgänge des Fördertisches benötige. Zudem wurde der Drehtisch verändert