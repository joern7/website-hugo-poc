---
layout: "image"
title: "Drehtisch"
date: "2008-06-29T18:30:37"
picture: "116_1634.jpg"
weight: "13"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14788
- /details971c-2.html
imported:
- "2019"
_4images_image_id: "14788"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-29T18:30:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14788 -->
Wegen des neuen Antrieb des Drehtisches musste auch der Drehtisch neu kontruiert werden. Auch hier werden die Kästchen über eine CNY 70 erkennt.