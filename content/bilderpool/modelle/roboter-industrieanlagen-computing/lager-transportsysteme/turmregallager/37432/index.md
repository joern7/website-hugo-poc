---
layout: "image"
title: "Förderband mit sensor tunnel (19)"
date: "2013-09-23T21:35:22"
picture: "turmregallager19.jpg"
weight: "19"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37432
- /detailsde97.html
imported:
- "2019"
_4images_image_id: "37432"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37432 -->
Dieses förderband hab ich mal als prototüb gebaut.
Im tunnel ist ein farbsensor eingebaut mit dem ich später mal Barcodes lesen lasen will.
Das förderbandsoll släter die Packete zum abholplatz transportieren und über den Barcode passend einsortieren
