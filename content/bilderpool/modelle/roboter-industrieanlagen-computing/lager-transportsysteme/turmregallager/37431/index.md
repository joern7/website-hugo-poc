---
layout: "image"
title: "Regal ausschnitt (18)"
date: "2013-09-23T21:35:22"
picture: "turmregallager18.jpg"
weight: "18"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37431
- /detailsa03d-4.html
imported:
- "2019"
_4images_image_id: "37431"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37431 -->
Regal ausschnit von oben.
Das Regal besteht aus ganz vielen einzel regalen mit je 2 Fächern.
Diese sind alle im kreis angeordnet und durch 30° winkelsteine zusammen gehalten