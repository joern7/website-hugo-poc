---
layout: "overview"
title: "Turmregallager"
date: 2020-02-22T08:07:13+01:00
legacy_id:
- /php/categories/1949
- /categories3dd3.html
- /categoriesab19.html
- /categories2710.html
- /categoriesd0b7-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1949 --> 
Mein Turmregallager sortiert automatisch die Kisten in das nächste freie Fach ein, sodass dieses dann wieder schnell aussortiert werden kann.