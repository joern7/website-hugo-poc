---
layout: "image"
title: "Gesamtübersicht"
date: "2010-06-11T18:37:53"
picture: "Gesamtansicht.jpg"
weight: "1"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/27450
- /detailsc4c9.html
imported:
- "2019"
_4images_image_id: "27450"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27450 -->
Hier die Gesamtübersicht über mein TRL (Turmregallager). Der Lagerroboter ist übrigens aus dem Baukasten "Industrierobots I".
P.S.: Es tut mir leid für die teils unscharfen Bilder. Ich habe vergessen den Nahaufnahmenknopf zu drücken. Werde es nächstes Mal besser machen.
Das Programm dazu findet ihr [hier](http://ftcommunity.de/data/downloads/robopro/v1.0.rpp)