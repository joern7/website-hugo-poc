---
layout: "image"
title: "Kiste"
date: "2010-06-11T18:37:53"
picture: "Kiste.jpg"
weight: "6"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/27455
- /details727e.html
imported:
- "2019"
_4images_image_id: "27455"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27455 -->
Solche Kisten kann der Roboter einlagern.