---
layout: "image"
title: "Z- Achse"
date: "2010-06-11T18:37:54"
picture: "Z-Achse.jpg"
weight: "10"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/27459
- /detailse914.html
imported:
- "2019"
_4images_image_id: "27459"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27459 -->
Das ist das Antriebsmodul der Z- Achse. Hier reicht ein Minimot, da diese Achse nicht so genau positioniert werden muss.