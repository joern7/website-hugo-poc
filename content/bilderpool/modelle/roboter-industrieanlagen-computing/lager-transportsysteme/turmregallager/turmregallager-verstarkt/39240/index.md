---
layout: "image"
title: "Untere Verstärkung"
date: "2014-08-11T22:13:28"
picture: "turmregallagerverstaerkt4.jpg"
weight: "4"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/39240
- /details27cf-2.html
imported:
- "2019"
_4images_image_id: "39240"
_4images_cat_id: "2935"
_4images_user_id: "1463"
_4images_image_date: "2014-08-11T22:13:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39240 -->
Diese Bilder wurden nach einer weiteren Verändereung von mir aufgenommen wo rauf man sehen kan wie ich das Regalsystem verstärkt habe um es in sich selbst stabiler zu machen. 
Was die Programirung des Lagers um ein vielfaches erleichterte, da von nun an die Regale sich nun nicht mehr so leicht verbigen konten.