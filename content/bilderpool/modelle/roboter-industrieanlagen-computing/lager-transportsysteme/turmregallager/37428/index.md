---
layout: "image"
title: "Regal (15)"
date: "2013-09-23T21:35:22"
picture: "turmregallager15.jpg"
weight: "15"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37428
- /details9b6b-2.html
imported:
- "2019"
_4images_image_id: "37428"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37428 -->
Das gesamte Regal mit 23 Fächen und einem Abholplatz.