---
layout: "image"
title: "Motor zum drehen (11)"
date: "2013-09-23T21:35:22"
picture: "turmregallager11.jpg"
weight: "11"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37424
- /details9fc3-2.html
imported:
- "2019"
_4images_image_id: "37424"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37424 -->
Der Motor zum betätigen der C-Achse die ich über zwei Kardanwellen mit der Schneck verbunden habe.
Zudem sieht man hir rechtgut wie ich die Kabel mit Kabelbindern gebündelt habe.