---
layout: "image"
title: "Strom versorgung (13)"
date: "2013-09-23T21:35:22"
picture: "turmregallager13.jpg"
weight: "13"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37426
- /detailsf23d.html
imported:
- "2019"
_4images_image_id: "37426"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37426 -->
Das Turmregallager wird durch ein power set mit Srom versorgt.
