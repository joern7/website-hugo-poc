---
layout: "comment"
hidden: true
title: "17474"
date: "2012-10-27T17:50:25"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hach, ich liebe so kompakte Modelle... Ingo spricht mir da aus dem Herzen! Und thomas004 wird das Modell sicher auch gefallen.
Die große Kunst ist die des Weglassens. Stefan Zweig sagte einmal, ein guter Tag sei ein Tag, an dem er sein Manuskript um einen Satz kürzen konnte. Wer die "Schachnovelle" kennt, weiß, welch brilliante Ergebnisse eine solche Methode liefert. 
Ein guter ft-Tag ist also ein Tag, an dem man in seinem Modell wieder ein Bauteil einsparen konnte...
Gruß, Dirk