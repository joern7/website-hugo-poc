---
layout: "image"
title: "Gabel"
date: "2012-10-22T21:09:23"
picture: "hochregallagerwerner23.jpg"
weight: "23"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36065
- /detailsffbd-2.html
imported:
- "2019"
_4images_image_id: "36065"
_4images_cat_id: "2683"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:23"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36065 -->
