---
layout: "image"
title: "Industrietransportsystem"
date: "2013-02-27T20:52:48"
picture: "industrie1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36701
- /details08a6-3.html
imported:
- "2019"
_4images_image_id: "36701"
_4images_cat_id: "2721"
_4images_user_id: "453"
_4images_image_date: "2013-02-27T20:52:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36701 -->
http://youtu.be/DrLdcQ54Od8