---
layout: "image"
title: "andere Richtung"
date: "2010-10-01T15:14:18"
picture: "drehkranz2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/28797
- /details24fc-2.html
imported:
- "2019"
_4images_image_id: "28797"
_4images_cat_id: "2097"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T15:14:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28797 -->
Insgesamt nur knapp 45 mm bis zur Oberkante der Kette und somit auch für sehr flache Förderbänder geeginet.