---
layout: "image"
title: "Greifer 2"
date: "2010-08-08T20:53:37"
picture: "umsetzermodul4.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27819
- /details390d.html
imported:
- "2019"
_4images_image_id: "27819"
_4images_cat_id: "2006"
_4images_user_id: "373"
_4images_image_date: "2010-08-08T20:53:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27819 -->
