---
layout: "image"
title: "normales Förderband"
date: "2010-01-24T13:44:37"
picture: "module1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26115
- /detailsc56b.html
imported:
- "2019"
_4images_image_id: "26115"
_4images_cat_id: "1851"
_4images_user_id: "373"
_4images_image_date: "2010-01-24T13:44:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26115 -->
Hier ein einfacher Entwurf als Beispiel für die Modulanlage auf der Convention.
Ein schlichtes, 60mm breites Förderband, an das natürlich Alles angebaut werden kann. Die Verbindung zum nächsten Modul findet über Bausteine 15 mit 2 Zapfen statt, der Abstand passt dann sogar für Werkstücke mit nur 30mm Länge oder Breite.