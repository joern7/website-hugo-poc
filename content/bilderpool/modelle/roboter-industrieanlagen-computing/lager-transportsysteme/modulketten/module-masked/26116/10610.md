---
layout: "comment"
hidden: true
title: "10610"
date: "2010-01-25T16:38:17"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Öhm, tja, peinlich ;-) Ich meine das Zahnrad des Mini-U-Getriebes, welches direkt an der MiniMot-Schnecke anliegt. Ob's da nicht mit den Betriebsstunden viel Abrieb gibt?

Gruß,
Stefan