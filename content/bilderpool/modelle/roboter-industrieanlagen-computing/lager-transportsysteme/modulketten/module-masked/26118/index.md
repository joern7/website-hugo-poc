---
layout: "image"
title: "kurzes Förderband und Drehkranz"
date: "2010-01-24T13:44:39"
picture: "module4.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26118
- /details77de.html
imported:
- "2019"
_4images_image_id: "26118"
_4images_cat_id: "1851"
_4images_user_id: "373"
_4images_image_date: "2010-01-24T13:44:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26118 -->
Der Drehkranz ist gerade mal 45mm hoch (mit Allem). Das ist nur möglich, wenn man den Antrieb nach außen verlegt (Motor über Kette) und die alten/neuen kleinen Motoren verwendet. Mit den S-Motoren hat man da keine Chance. Außerdem sollte man die alten Kettenglieder mit den alten Förderbandteilen verwenden, da die Neuen natürlich zu breit sind.
Der Drehkranz kann sich nicht ganz um 360° drehen, da die jetzt untere rechte Ecke ansonsten am Förderband anstößt und das Kabel leider außen geführt werden muss.
Der Abstand zum Förderband passt genau, um Kassetten 60 zu befördern, für 30er Werkstücke geht das natürlich nicht.