---
layout: "image"
title: "Antrieb"
date: "2008-08-22T23:17:16"
picture: "modul4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/15072
- /detailse213.html
imported:
- "2019"
_4images_image_id: "15072"
_4images_cat_id: "1372"
_4images_user_id: "558"
_4images_image_date: "2008-08-22T23:17:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15072 -->
Die Welle die die andere Seite antreibt.