---
layout: "image"
title: "Antrieb"
date: "2008-08-22T23:17:16"
picture: "modul3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/15071
- /details11ec.html
imported:
- "2019"
_4images_image_id: "15071"
_4images_cat_id: "1372"
_4images_user_id: "558"
_4images_image_date: "2008-08-22T23:17:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15071 -->
Hier der genaue und erstaunlich schnelle Antrieb der 1. Achse. 
Die 2. Achse kan oben beliebig gestaltet werden, ich warte aber noch bis das werkstück festgelegt ist