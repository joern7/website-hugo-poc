---
layout: "image"
title: "HRL (8)"
date: "2007-10-03T08:48:26"
picture: "hrl08.jpg"
weight: "8"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12077
- /details5dad.html
imported:
- "2019"
_4images_image_id: "12077"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12077 -->
Das Regal hat leider nur 6 Fächer. Ich werde es vielleicht noch vergrößern, dazu müsste ich aber auch den Einlagerer vergrößern :-|
Mal sehen...