---
layout: "image"
title: "HRL (1) Abbau"
date: "2009-09-27T23:59:13"
picture: "hochregallagerversionabbau01.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25377
- /details6c4e.html
imported:
- "2019"
_4images_image_id: "25377"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25377 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.