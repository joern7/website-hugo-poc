---
layout: "image"
title: "HRL (10) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau10.jpg"
weight: "10"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25386
- /details5842.html
imported:
- "2019"
_4images_image_id: "25386"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25386 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.