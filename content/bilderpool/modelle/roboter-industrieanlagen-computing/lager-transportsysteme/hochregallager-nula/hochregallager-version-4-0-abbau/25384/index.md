---
layout: "image"
title: "HRL (8) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau08.jpg"
weight: "8"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25384
- /details8cad-2.html
imported:
- "2019"
_4images_image_id: "25384"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25384 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.