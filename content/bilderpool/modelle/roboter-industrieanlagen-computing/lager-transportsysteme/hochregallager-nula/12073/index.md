---
layout: "image"
title: "HRL (4)"
date: "2007-10-03T08:48:26"
picture: "hrl04.jpg"
weight: "4"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12073
- /details5462.html
imported:
- "2019"
_4images_image_id: "12073"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12073 -->
An der Draufsicht kann man gut erkennen, dass das gesammte Lagersystem auf nur einer Grundplatte 1000 aufgebaut ist.