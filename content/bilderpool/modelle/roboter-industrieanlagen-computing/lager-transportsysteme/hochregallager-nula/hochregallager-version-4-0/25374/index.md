---
layout: "image"
title: "HRL (1) Übersicht"
date: "2009-09-27T23:59:12"
picture: "hochregallagerversion1.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25374
- /detailsbf89-2.html
imported:
- "2019"
_4images_image_id: "25374"
_4images_cat_id: "1777"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25374 -->
Etwas verspätet lade ich mal die letzten Bilder meines HRLs hoch. So war es auch auf der Convention 2008 zu sehen.
Es hat sich noch einiges getan. Die Auswahl des zu leerenden Fachs erfolgt über die Fernbedienung und nicht wie vorher mühsam über die zwei Taster.
Das EEprom in der gelben Kiste ist einem neuen EEprom in einer kompakteren roten 9V-Akkukiste gewichen.