---
layout: "image"
title: "HRL (3) von hinten"
date: "2009-09-27T23:59:13"
picture: "hochregallagerversion3.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25376
- /detailsd41a.html
imported:
- "2019"
_4images_image_id: "25376"
_4images_cat_id: "1777"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25376 -->
Die komische Strohhalm-Konstruktion um die Kabel vom Einlagerer zum Boden habe ich durch eine viel ordendlichere Energiekette ersetzt. Da verhakt sich auch endlich nichts mehr.