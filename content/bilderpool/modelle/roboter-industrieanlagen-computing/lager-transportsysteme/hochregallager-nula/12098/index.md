---
layout: "image"
title: "HRL (29)"
date: "2007-10-03T08:48:27"
picture: "hrl29.jpg"
weight: "29"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12098
- /detailsb272-2.html
imported:
- "2019"
_4images_image_id: "12098"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:27"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12098 -->
Da die Positionierung nicht 100% genau ist, habe ich eine Zuführung gebaut, um die Kiste zu zentrieren.
Man sieht auch, dass ich am Ende des Förderbandes eine Grenze und keine zweite Lichtschranke gebaut habe, da die Eingänge am Interface nicht reichen. Ich schalte das Förderband einfach 2,5 Sekunden ein und gehe dann davon aus, dass die Kiste am Ende des Förderbandes stehen geblieben ist.