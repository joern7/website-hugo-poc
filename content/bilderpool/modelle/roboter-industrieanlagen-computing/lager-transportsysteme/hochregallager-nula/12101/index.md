---
layout: "image"
title: "HRL (32)"
date: "2007-10-03T08:48:27"
picture: "hrl32.jpg"
weight: "32"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12101
- /details651c-2.html
imported:
- "2019"
_4images_image_id: "12101"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:27"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12101 -->
Sooo, jetzt wird's ernst...
Wenn eine Kiste ausgelagert werden soll, dann drückt man auf dem Bedienfeld auf den entsprechenden Knopf. Dann fährt der Greifer erst einmal vor das Fach mit der Kiste.
Die Anzeige im Bedienfeld zeigt:
Kiste X kommt...