---
layout: "image"
title: "HRL (2)"
date: "2007-10-03T08:48:26"
picture: "hrl02.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12071
- /detailsf4bc-2.html
imported:
- "2019"
_4images_image_id: "12071"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12071 -->
Schrägsicht. Man sieht den Einlagerer.