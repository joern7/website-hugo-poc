---
layout: "image"
title: "HRL (3)"
date: "2007-10-03T08:48:26"
picture: "hrl03.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12072
- /details0cd6.html
imported:
- "2019"
_4images_image_id: "12072"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12072 -->
Hochregal von hinten.