---
layout: "image"
title: "HRL (37)"
date: "2007-10-03T16:36:25"
picture: "HRL_20071.jpg"
weight: "37"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12125
- /details8e71.html
imported:
- "2019"
_4images_image_id: "12125"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12125 -->
Und wenn eine Kiste eingelagert werden soll, stellt man sie sie auf das Förderband, wo sie von der Lichtschranke erkannt wird.
Dann schnappt der Einlagerer sich die Kiste und stellt sie in ein leeres Fach.

Ist das Regal voll und man stellt trotzdem eine Kiste auf das Förderband, so schubst das Förderband die Kiste einfach weg und das Bedienfeld verkündet:
Regal voll!