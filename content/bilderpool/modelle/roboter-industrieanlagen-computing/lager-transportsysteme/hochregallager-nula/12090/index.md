---
layout: "image"
title: "HRL (21)"
date: "2007-10-03T08:48:26"
picture: "hrl21.jpg"
weight: "21"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12090
- /details8a87-5.html
imported:
- "2019"
_4images_image_id: "12090"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12090 -->
Detailaufnahme Greifer. Ich habe ein Gummi um ihn gewickelt, damit die zu transportierende Kiste nicht herunterfällt.
Außerdem habe ich eine Zentrierung drangemacht, damit die Kiste schön gerade auf dem Greifer liegt.