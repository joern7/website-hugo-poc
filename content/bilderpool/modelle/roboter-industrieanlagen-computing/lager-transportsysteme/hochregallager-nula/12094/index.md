---
layout: "image"
title: "HRL (25)"
date: "2007-10-03T08:48:26"
picture: "hrl25.jpg"
weight: "25"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12094
- /details614f.html
imported:
- "2019"
_4images_image_id: "12094"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12094 -->
Der Arm des Greifers ragt etwas zu weit raus. So lang brauche ich ihn nicht, aber die Konstruktion erlaubt keine Verküzung :-S