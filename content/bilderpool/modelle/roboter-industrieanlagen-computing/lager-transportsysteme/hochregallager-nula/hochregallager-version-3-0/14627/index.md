---
layout: "image"
title: "HRL (11) Interface + EEprom"
date: "2008-06-04T18:45:04"
picture: "hochregallager11.jpg"
weight: "11"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14627
- /details68e3.html
imported:
- "2019"
_4images_image_id: "14627"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:45:04"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14627 -->
Die Hauptkontrolleinheit bekommt eine eigene Platte.
rechts in dem gelben Kästchen ist das EEprom. Es funktioniert wunderbar mit ROBOPro.