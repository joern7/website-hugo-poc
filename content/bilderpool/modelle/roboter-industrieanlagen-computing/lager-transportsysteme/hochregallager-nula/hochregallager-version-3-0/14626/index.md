---
layout: "image"
title: "HRL (10) Extensionmodule"
date: "2008-06-04T18:44:51"
picture: "hochregallager10.jpg"
weight: "10"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14626
- /details2c00-3.html
imported:
- "2019"
_4images_image_id: "14626"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14626 -->
...und das alles ist nur dank des neuen Extensionmodules möglich! :-)

Die Kabel sind doch ganz ordendlich, oder?