---
layout: "image"
title: "HRL (8) Y-Achse"
date: "2008-06-04T18:44:51"
picture: "hochregallager08.jpg"
weight: "8"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14624
- /details95c8.html
imported:
- "2019"
_4images_image_id: "14624"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14624 -->
Hier sieht man noch mal den überarbeiteten Einlagerer.
Die Linse ist nicht staubig, das Bild war nur einfach zu dunkel und ich habe es mit dem Publisher heller gemacht.