---
layout: "image"
title: "HRL (12) Kistenhalter"
date: "2008-06-04T18:45:04"
picture: "hochregallager12.jpg"
weight: "12"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14628
- /details1a23-2.html
imported:
- "2019"
_4images_image_id: "14628"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:45:04"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14628 -->
Und wenn die Kisten nicht gerade im Hochregal stehen, landen sie in diesem Kistenhalter mit Federung. :-)