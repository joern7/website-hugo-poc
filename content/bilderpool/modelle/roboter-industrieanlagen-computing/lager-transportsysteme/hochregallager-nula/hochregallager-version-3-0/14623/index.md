---
layout: "image"
title: "HRL (7) Schlitten"
date: "2008-06-04T18:44:51"
picture: "hochregallager07.jpg"
weight: "7"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14623
- /details1775-2.html
imported:
- "2019"
_4images_image_id: "14623"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14623 -->
Die Schiene und den Schlitten habe ich so gelassen wie sie waren.