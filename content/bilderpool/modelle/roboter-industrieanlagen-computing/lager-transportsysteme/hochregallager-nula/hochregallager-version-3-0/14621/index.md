---
layout: "image"
title: "HRL (5) Seite"
date: "2008-06-04T18:44:51"
picture: "hochregallager05.jpg"
weight: "5"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14621
- /details9fa8.html
imported:
- "2019"
_4images_image_id: "14621"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14621 -->
Am Förderband ist nun ein Minimot.
Die Y-Achse war vorher mit Alus, ich habe jetzt alles aus Plastik gemacht.
Is fast genauso stabil und genau ist es auch. Außerdem ist er viel kleiner.