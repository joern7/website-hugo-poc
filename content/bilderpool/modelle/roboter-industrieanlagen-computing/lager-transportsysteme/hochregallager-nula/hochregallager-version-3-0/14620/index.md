---
layout: "image"
title: "HRL (4) Förderband"
date: "2008-06-04T18:44:51"
picture: "hochregallager04.jpg"
weight: "4"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14620
- /detailsff56.html
imported:
- "2019"
_4images_image_id: "14620"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14620 -->
Dank der zwei Lichtschranken wird die reale Position der Kiste bestimmt.
Außerdem erkennen sie, ob jemand die Kiste wieder wegzieht (also draufstellt und wieder runternimmt).