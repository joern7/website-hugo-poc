---
layout: "image"
title: "HRL (9) Schienen + Positionserkennung X-Achse"
date: "2008-06-04T18:44:51"
picture: "hochregallager09.jpg"
weight: "9"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14625
- /details065d.html
imported:
- "2019"
_4images_image_id: "14625"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14625 -->
Die Impulstasterpositionszählung war mir zu ungenau.
Mit den Tastern wird jetzt die reale Position bestimmt.