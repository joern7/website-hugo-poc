---
layout: "image"
title: "Abschaltung"
date: "2007-06-04T15:48:16"
picture: "HRL34.jpg"
weight: "28"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10700
- /detailseb96-2.html
imported:
- "2019"
_4images_image_id: "10700"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10700 -->
Die Abschaltung. Der Kompressor geht aus, wenn der Taster losgelassen wird.