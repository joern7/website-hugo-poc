---
layout: "image"
title: "Förderband"
date: "2007-06-04T15:48:00"
picture: "HRL26.jpg"
weight: "20"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10692
- /details3396-2.html
imported:
- "2019"
_4images_image_id: "10692"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10692 -->
Das Förderband wurde etwas verlängert.