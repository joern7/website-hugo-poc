---
layout: "image"
title: "Tunnelabdeckung"
date: "2007-06-04T15:48:22"
picture: "HRL40.jpg"
weight: "34"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10706
- /details392f.html
imported:
- "2019"
_4images_image_id: "10706"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10706 -->
Tunnelabdeckung mit Farberkennung.