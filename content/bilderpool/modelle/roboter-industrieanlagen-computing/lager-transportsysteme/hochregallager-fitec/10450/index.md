---
layout: "image"
title: "Einlagerer-Arm"
date: "2007-05-17T21:54:51"
picture: "HRL7.jpg"
weight: "7"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10450
- /detailsd4af.html
imported:
- "2019"
_4images_image_id: "10450"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-17T21:54:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10450 -->
Hier sieht man den ausfahrbaren "Arm".