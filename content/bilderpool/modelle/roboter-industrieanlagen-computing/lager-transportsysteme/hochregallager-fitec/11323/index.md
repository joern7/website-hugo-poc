---
layout: "image"
title: "Luft/Vakuum Tank"
date: "2007-08-09T22:02:25"
picture: "HRL79.jpg"
weight: "67"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11323
- /details3594-3.html
imported:
- "2019"
_4images_image_id: "11323"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11323 -->
Hier ist der selbst gebaute Vakuumtank, in dem das Rückschlagventil ist.