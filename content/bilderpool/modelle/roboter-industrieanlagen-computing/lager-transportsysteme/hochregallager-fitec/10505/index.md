---
layout: "image"
title: "Antrieb"
date: "2007-05-25T21:23:34"
picture: "HRL23.jpg"
weight: "17"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10505
- /detailsbe77.html
imported:
- "2019"
_4images_image_id: "10505"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10505 -->
Man sieht, dass der Antrieb vom Laufband unter dem Laufband ist. Das spart Platz und ist ordentlicher.