---
layout: "image"
title: "Verkabelung"
date: "2007-06-16T20:59:49"
picture: "HRL56.jpg"
weight: "50"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10870
- /detailsbfe7-2.html
imported:
- "2019"
_4images_image_id: "10870"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-16T20:59:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10870 -->
Hier sieht man das erste Em wie es Verkabelt ist. Bis auf die CNY70, die ich wohl innerhalb der nächsten 2 Wochen bestellen werde ist alles verkabelt.