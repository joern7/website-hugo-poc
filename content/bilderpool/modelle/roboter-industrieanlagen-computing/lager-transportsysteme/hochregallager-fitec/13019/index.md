---
layout: "image"
title: "Neuer Antrieb"
date: "2007-12-09T09:55:47"
picture: "HRL88.jpg"
weight: "76"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13019
- /details6a47.html
imported:
- "2019"
_4images_image_id: "13019"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-09T09:55:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13019 -->
Der 20:1 Power-Motor ist 2:1 untersetzt, gibt 40:1. Das Z20 gerift in die Zahnstange. Damit diser Antrieb ratterfrei funktioniert musste ich alles so bauen, dass sich nichts verschieben kann. Immerhin, es klappt.