---
layout: "image"
title: "Greifer"
date: "2007-05-25T21:23:34"
picture: "HRL18.jpg"
weight: "12"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10500
- /details8f9f.html
imported:
- "2019"
_4images_image_id: "10500"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10500 -->
Hier ist der neue Vakuumgreifer. Der Taster ist um aufwendige Positionierung zu vermeiden.