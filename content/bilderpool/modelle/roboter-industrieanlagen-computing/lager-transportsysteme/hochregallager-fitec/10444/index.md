---
layout: "image"
title: "HRL"
date: "2007-05-17T21:54:50"
picture: "HRL1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10444
- /details7dc5.html
imported:
- "2019"
_4images_image_id: "10444"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-17T21:54:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10444 -->
Das ist mein HRL. Der Einlagerer ist noch in Bearbeitung. Es soll sich innerhalb der nächsten Monate sehr weiterentwickeln durch z.B. Förderbänder und ein Magazin.