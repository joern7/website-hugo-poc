---
layout: "image"
title: "Einlagerer"
date: "2007-05-17T21:54:50"
picture: "HRL3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10446
- /details2ebe.html
imported:
- "2019"
_4images_image_id: "10446"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-17T21:54:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10446 -->
Hier sieht man den Einlagerer von unten. Er besitzt ein eigenes Em mit selbst gemachtem Verlängerungskabel.