---
layout: "image"
title: "Vakuumpumpe"
date: "2007-06-04T15:48:16"
picture: "HRL33.jpg"
weight: "27"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10699
- /details37f3-2.html
imported:
- "2019"
_4images_image_id: "10699"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10699 -->
Das ist die Vakuumpumpe für den Saugnapf.