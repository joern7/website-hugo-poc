---
layout: "image"
title: "Getriebe"
date: "2007-07-17T16:52:56"
picture: "HRL70.jpg"
weight: "58"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11122
- /detailsc96f.html
imported:
- "2019"
_4images_image_id: "11122"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11122 -->
Hier sieht man den Antrieb. Man sieht links ein bischen den Minimotor. Der treibt das Z10 in der Mitte an. Das rote Z20 darüber ist, das dass auf der Schiene it. Das Z10 unter dem angetriebenen Z10 ist am Impulszähler.