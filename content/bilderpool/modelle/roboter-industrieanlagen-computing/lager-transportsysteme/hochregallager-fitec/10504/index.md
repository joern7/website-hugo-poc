---
layout: "image"
title: "Laufband"
date: "2007-05-25T21:23:34"
picture: "HRL22.jpg"
weight: "16"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10504
- /details12ac-2.html
imported:
- "2019"
_4images_image_id: "10504"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10504 -->
Hier sieht man ein kleines Laufband und einen Drehkranz ( weitere Bilder vom Drehkranz folgen irgendwann noch). Vom Laufband nimmt der Einlagerer die Kästchen.