---
layout: "image"
title: "Schieber"
date: "2010-06-05T18:08:49"
picture: "streichholzschachteltransportstrasse2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/27393
- /detailsc318.html
imported:
- "2019"
_4images_image_id: "27393"
_4images_cat_id: "1967"
_4images_user_id: "453"
_4images_image_date: "2010-06-05T18:08:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27393 -->
Mit Hilfe von diesem Schieber werden die Schachteln um die erste Ecke geschoben.
Am unteren Bildrand sind 2 Schleifringe zu sehen, der hintere stoppt den Schieber nach einer Runde.
Der vordere Taster überbrückt kurze Zeit den stopp Taster der nächsten Ecke und startet sie damit.