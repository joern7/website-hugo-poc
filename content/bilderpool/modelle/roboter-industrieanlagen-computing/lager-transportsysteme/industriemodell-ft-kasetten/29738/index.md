---
layout: "image"
title: "Industriemodell - TX"
date: "2011-01-21T15:16:09"
picture: "modell17.jpg"
weight: "17"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29738
- /detailsf5d5-2.html
imported:
- "2019"
_4images_image_id: "29738"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:09"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29738 -->
Hier kann man den TX Controller sehen.