---
layout: "image"
title: "Industriemodell - Taster"
date: "2011-01-21T15:16:07"
picture: "modell03.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29724
- /details4d20-2.html
imported:
- "2019"
_4images_image_id: "29724"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29724 -->
Mit diesem Großhandtaster, kann man eine Kasette aus dem Magazin auslagern.