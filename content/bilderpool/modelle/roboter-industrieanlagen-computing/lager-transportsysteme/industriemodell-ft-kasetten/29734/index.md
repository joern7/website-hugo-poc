---
layout: "image"
title: "Industriemodell - Schieber"
date: "2011-01-21T15:16:08"
picture: "modell13.jpg"
weight: "13"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29734
- /details0154-2.html
imported:
- "2019"
_4images_image_id: "29734"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29734 -->
Hier kann man den Schieber sehen, der die Kasetten herausschiebt.