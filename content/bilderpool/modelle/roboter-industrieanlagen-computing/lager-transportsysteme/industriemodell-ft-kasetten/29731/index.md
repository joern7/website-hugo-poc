---
layout: "image"
title: "Industriemodell - Drehkranz"
date: "2011-01-21T15:16:08"
picture: "modell10.jpg"
weight: "10"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29731
- /detailsac22-2.html
imported:
- "2019"
_4images_image_id: "29731"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29731 -->
Hier kann man den Drehkranz sehen, der vom Motor angetrieben wird.