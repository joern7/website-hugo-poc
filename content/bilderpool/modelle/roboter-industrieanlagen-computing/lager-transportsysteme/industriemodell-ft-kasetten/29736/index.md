---
layout: "image"
title: "Industriemodell - Impulszähler"
date: "2011-01-21T15:16:08"
picture: "modell15.jpg"
weight: "15"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29736
- /details235b.html
imported:
- "2019"
_4images_image_id: "29736"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29736 -->
Hier sieht man den Impulszähler.