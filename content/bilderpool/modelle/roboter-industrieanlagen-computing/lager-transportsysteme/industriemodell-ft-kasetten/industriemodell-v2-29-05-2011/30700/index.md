---
layout: "image"
title: "Serienaufnahme"
date: "2011-05-29T15:10:01"
picture: "modell14.jpg"
weight: "14"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30700
- /details2582-2.html
imported:
- "2019"
_4images_image_id: "30700"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30700 -->
