---
layout: "image"
title: "Rollenband"
date: "2011-05-29T15:10:01"
picture: "modell05.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30691
- /details8e8c.html
imported:
- "2019"
_4images_image_id: "30691"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30691 -->
"Ein Bild sagt mehr als 1000 Worte"