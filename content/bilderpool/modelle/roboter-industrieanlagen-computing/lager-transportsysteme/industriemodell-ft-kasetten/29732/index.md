---
layout: "image"
title: "Industriemodell - Steuereinheit"
date: "2011-01-21T15:16:08"
picture: "modell11.jpg"
weight: "11"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29732
- /details5210-2.html
imported:
- "2019"
_4images_image_id: "29732"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29732 -->
Hier kann man das Modell von hinten sehen, und man sieht die Steuereinheit. Der Schalter dient zum Anschalten der Lichter für die Lichtschranke und die Druckabschaltung sorgt dafür, das immer genug Luft im Tank ist.