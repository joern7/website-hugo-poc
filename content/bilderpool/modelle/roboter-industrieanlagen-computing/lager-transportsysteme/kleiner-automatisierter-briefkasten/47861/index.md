---
layout: "image"
title: "Gesamt-Hinten"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten07.jpg"
weight: "7"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47861
- /details1d1b.html
imported:
- "2019"
_4images_image_id: "47861"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47861 -->
Man sieht gut die zwei Controllerund den Kabelsalat, den ich versucht habe etwas zu ordnen...dazwischen kann man auch knapp die Übertragungsschnittstelle erkennen.