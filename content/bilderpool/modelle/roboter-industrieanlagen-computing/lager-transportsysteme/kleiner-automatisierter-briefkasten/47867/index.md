---
layout: "image"
title: "Lichtübermittlung-Detail"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten13.jpg"
weight: "13"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47867
- /details4376-4.html
imported:
- "2019"
_4images_image_id: "47867"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47867 -->
Dieses Modul besteht aus drei Lampen und drei Fototransistoren (bzw. einem Fototransistor und zwei Fotowiderständen, weil ich keinen Fototransistor mehr hatte). Wenn der TxT eine Karte für ein Fach erkennt, leuchtet die jeweilige Lampe und der Tx empfängt dieses Signal. Wenn der Einwurf geöffnet werden soll, leuchten alle drei Lampen gleichzeitig. Die Lampen leuchten solange, bis der Schalter wieder umgelegt wurde und man die Karte zurückbekommt.