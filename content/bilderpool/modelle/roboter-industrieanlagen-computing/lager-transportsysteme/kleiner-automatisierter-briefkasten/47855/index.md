---
layout: "image"
title: "Gesamt-Vorne"
date: "2018-09-08T19:16:52"
picture: "kleinerautomatisierterbriefkasten01.jpg"
weight: "1"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47855
- /details744d.html
imported:
- "2019"
_4images_image_id: "47855"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47855 -->
Das ist ein dreifacher kleiner Briefkasten, der mit Codekarten gesteuert wird. Der Ultraschallsensor startet, den Einzug, wenn sich eine Person davor befindet. Die Karten werden rechts eingezogen und ausgelesen. Sofern die Karte gelesen werden konnte, leuchtet die blaue Lampe, wenn nicht, wird sie wieder ausgeworfen. Wenn die blaue Lampe leuchtet und man den Schalter nach unten drückt, leuchtet zum einen die grüne Lampe und zum anderen wird vom TxT auf den Tx per Licht ein Signal geschickt, welcher dann das jeweilige Fach, oder den EInwurf öffnet. Um seine Karte wieder zu bekommen, muss man lediglich das Fach schließen und den Schalter wieder nach oben drücken.