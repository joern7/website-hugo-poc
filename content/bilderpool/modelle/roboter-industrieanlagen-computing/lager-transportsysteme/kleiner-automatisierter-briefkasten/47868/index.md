---
layout: "image"
title: "Oben-Rechter Teil"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten14.jpg"
weight: "14"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47868
- /details4470-3.html
imported:
- "2019"
_4images_image_id: "47868"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47868 -->
