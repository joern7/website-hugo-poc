---
layout: "image"
title: "Gesamt-Oben"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten04.jpg"
weight: "4"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47858
- /details1bc0-4.html
imported:
- "2019"
_4images_image_id: "47858"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47858 -->
In der Draufsicht sieht man links das oberste Fach + Motor, in der Mitte das Modul für die Signalübertragung per Licht und rechts den Einzug der Karten.