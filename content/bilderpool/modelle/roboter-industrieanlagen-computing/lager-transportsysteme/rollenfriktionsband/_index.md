---
layout: "overview"
title: "Rollenfriktionsband"
date: 2020-02-22T08:07:13+01:00
legacy_id:
- /php/categories/1791
- /categories6c1e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1791 --> 
Mal eine andere Version eines Transportbandes. Hier erfolgt die Kraftübertragung per Reibung über Rollen auf eine Palette oder ähnliches. 
Vorteil: keine Ketten, niedrige Bauform möglich, Vielseitig.