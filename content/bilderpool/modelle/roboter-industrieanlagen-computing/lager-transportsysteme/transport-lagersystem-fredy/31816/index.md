---
layout: "image"
title: "Übersicht"
date: "2011-09-18T15:16:13"
picture: "industriemodell04.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31816
- /detailsc56a.html
imported:
- "2019"
_4images_image_id: "31816"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31816 -->
