---
layout: "image"
title: "Gesamtansicht"
date: "2011-09-18T15:16:13"
picture: "industriemodell01.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31813
- /details3c9f-2.html
imported:
- "2019"
_4images_image_id: "31813"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31813 -->
Meine Transportstraße mit Hochregallager
http://youtu.be/bjaI25BrVls