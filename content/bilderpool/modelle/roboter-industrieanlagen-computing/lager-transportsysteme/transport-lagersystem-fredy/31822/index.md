---
layout: "image"
title: "Dreh-Ecke"
date: "2011-09-18T15:16:13"
picture: "industriemodell10.jpg"
weight: "10"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31822
- /details1fbd-2.html
imported:
- "2019"
_4images_image_id: "31822"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31822 -->
Die 15x30 Platte verhindert das die Räder bei dem übergang Kippen und in dem Zwischenraum hängen bleiben. Sie hält die Räder hinten unten.