---
layout: "image"
title: "Transportwagen"
date: "2011-09-18T15:16:13"
picture: "industriemodell14.jpg"
weight: "14"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31826
- /details269f-3.html
imported:
- "2019"
_4images_image_id: "31826"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31826 -->
Hier eine leere Station