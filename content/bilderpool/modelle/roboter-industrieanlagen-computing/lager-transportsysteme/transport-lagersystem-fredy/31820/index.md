---
layout: "image"
title: "Dreh-Eckenauswurf"
date: "2011-09-18T15:16:13"
picture: "industriemodell08.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31820
- /details1acb-3.html
imported:
- "2019"
_4images_image_id: "31820"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31820 -->
Der Taster stoppt den Motor nach einer Runde.