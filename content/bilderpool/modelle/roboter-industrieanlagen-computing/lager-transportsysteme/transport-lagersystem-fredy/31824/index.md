---
layout: "image"
title: "Dreh-Ecke Unterseite"
date: "2011-09-18T15:16:13"
picture: "industriemodell12.jpg"
weight: "12"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31824
- /detailsec30.html
imported:
- "2019"
_4images_image_id: "31824"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31824 -->
Hier nochmal von einer anderen Seite