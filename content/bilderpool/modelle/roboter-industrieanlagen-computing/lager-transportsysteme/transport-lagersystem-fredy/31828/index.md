---
layout: "image"
title: "Transportwagen"
date: "2011-09-18T15:16:13"
picture: "industriemodell16.jpg"
weight: "16"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31828
- /details069f.html
imported:
- "2019"
_4images_image_id: "31828"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31828 -->
Taster, er gibt die Position für den Auswerfer vor.