---
layout: "image"
title: "Das Bedienfeld"
date: "2017-09-17T18:02:23"
picture: "hochregallager11.jpg"
weight: "11"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46259
- /details5c49.html
imported:
- "2019"
_4images_image_id: "46259"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46259 -->
Über das Display wird der richtige Platz angewählt.