---
layout: "image"
title: "Das Regal"
date: "2017-09-17T18:02:23"
picture: "hochregallager10.jpg"
weight: "10"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46258
- /detailsbc9d.html
imported:
- "2019"
_4images_image_id: "46258"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46258 -->
Hier sieht man das Regal von vorne