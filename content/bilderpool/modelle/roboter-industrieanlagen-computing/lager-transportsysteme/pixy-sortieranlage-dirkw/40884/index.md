---
layout: "image"
title: "Pixy Sortieranlage"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage08.jpg"
weight: "8"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40884
- /details6e24.html
imported:
- "2019"
_4images_image_id: "40884"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40884 -->
Mit Werkstück.