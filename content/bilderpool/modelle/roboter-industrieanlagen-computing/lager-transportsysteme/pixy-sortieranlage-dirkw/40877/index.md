---
layout: "image"
title: "Pixy Sortieranlage"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage01.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40877
- /detailsc609.html
imported:
- "2019"
_4images_image_id: "40877"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40877 -->
Model Bearbeitungsstation mit Vakuumgreifer aus dem Robo PneuVac Baukasten mit Pixy Farberkennung und Display.
Die Kamera unterscheidet 3 Farben (Bausteine) und sortiert diese in die richtigen Boxen.

Ein Video findet ihr unter:
https://www.youtube.com/watch?v=Iev69tXD9Vo
