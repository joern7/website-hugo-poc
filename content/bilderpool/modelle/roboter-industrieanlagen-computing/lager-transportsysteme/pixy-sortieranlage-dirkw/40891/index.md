---
layout: "image"
title: "RoboPro"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage15.jpg"
weight: "15"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40891
- /details3981.html
imported:
- "2019"
_4images_image_id: "40891"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40891 -->
Das Hauptprogramm.