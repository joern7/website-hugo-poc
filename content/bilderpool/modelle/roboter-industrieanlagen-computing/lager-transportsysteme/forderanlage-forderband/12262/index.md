---
layout: "image"
title: "Gesamtanlage"
date: "2007-10-17T14:21:09"
picture: "DSCN1768.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12262
- /details1c86-2.html
imported:
- "2019"
_4images_image_id: "12262"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T14:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12262 -->
Bestehend aus Transportband, Zuführeinheit und Belader (Caterpillar).