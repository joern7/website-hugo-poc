---
layout: "image"
title: "Funktioniert jetzt. .."
date: "2007-10-17T14:21:09"
picture: "DSCN1755.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12258
- /details4e2c-2.html
imported:
- "2019"
_4images_image_id: "12258"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T14:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12258 -->
... und Schwups, ist die Schaufel leer ....