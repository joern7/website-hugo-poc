---
layout: "image"
title: "Gesamtansich"
date: "2007-10-13T11:40:15"
picture: "DSCN1694.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12196
- /details083e.html
imported:
- "2019"
_4images_image_id: "12196"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12196 -->
Diese Anlage habe ich als Teil einer Industrieanlage geplant (wenn´s denn klappt).
Die Anlage ist 110 cm lang. Durch einen Kanal wird eine Kette gezogen die die Teile mitnimmt. Mit kleinen Teilen (S-Riegeln) funktioniert das schon. Durch änderungen am Design kann man auch größere Teile befördern.