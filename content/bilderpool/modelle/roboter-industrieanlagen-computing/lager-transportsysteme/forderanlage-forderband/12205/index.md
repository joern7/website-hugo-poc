---
layout: "image"
title: "Detail"
date: "2007-10-13T11:40:16"
picture: "DSCN1713.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12205
- /details36c6.html
imported:
- "2019"
_4images_image_id: "12205"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12205 -->
An den Mitnehmern habe ich zusätzliche Federnocken angebracht. So ist gewährleistet das auch kleiner Teile befördert werden.