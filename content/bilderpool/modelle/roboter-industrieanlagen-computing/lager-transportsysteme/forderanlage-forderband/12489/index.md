---
layout: "image"
title: "Zuführeinheit"
date: "2007-11-05T15:54:02"
picture: "DSCN1989.jpg"
weight: "38"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12489
- /detailsf523.html
imported:
- "2019"
_4images_image_id: "12489"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12489 -->
Der Trichter ist geblieben. Die Kette besteht nunmehr nur noch aus Mitnehmern. So ist es gewährleistet das sich die Erbsen nicht verfangen.