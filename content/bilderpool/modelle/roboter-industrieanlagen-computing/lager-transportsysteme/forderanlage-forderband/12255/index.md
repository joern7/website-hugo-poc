---
layout: "image"
title: "Funktioniert fast. .."
date: "2007-10-17T14:21:08"
picture: "DSCN1735.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12255
- /detailse08e.html
imported:
- "2019"
_4images_image_id: "12255"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T14:21:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12255 -->
Der "Trichter" geht zu weit hoch. Deswegen hat der Caterpillar ein paar Schwierigkeiten die Schaufel zu entleeren.