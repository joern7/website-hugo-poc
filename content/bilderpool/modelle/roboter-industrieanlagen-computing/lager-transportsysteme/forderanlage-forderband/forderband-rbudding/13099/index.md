---
layout: "image"
title: "Förderband ohne Robo interface I"
date: "2007-12-17T22:22:39"
picture: "DSC_0139.jpg"
weight: "8"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13099
- /details9de6.html
imported:
- "2019"
_4images_image_id: "13099"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13099 -->
mit dem Farbsensor könntest du mit Hilfe eines Triggersignales eine markante Farbfläche abfragen.