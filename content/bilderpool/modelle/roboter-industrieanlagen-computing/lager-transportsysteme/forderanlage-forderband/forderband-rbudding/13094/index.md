---
layout: "image"
title: "Transistor close up unter Förderband"
date: "2007-12-17T22:22:39"
picture: "close_up_transistor.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13094
- /details8058.html
imported:
- "2019"
_4images_image_id: "13094"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13094 -->
Used this to determine the next block is arriving.
in the robo program this is the trigger to measure the next color brick.

Mit dem Farbsensor könntest du mit Hilfe eines Triggersignales eine markante Farbfläche abfragen