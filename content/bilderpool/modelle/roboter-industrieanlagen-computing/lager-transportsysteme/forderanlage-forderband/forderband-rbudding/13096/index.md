---
layout: "image"
title: "Farbsensor!"
date: "2007-12-17T22:22:39"
picture: "close_up.jpg"
weight: "5"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13096
- /detailsf9e5.html
imported:
- "2019"
_4images_image_id: "13096"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13096 -->
Took this close up from the color sensor without photo flash light.
look how the infrared is dittering, enough to determine the difference between the RED BLACK and YELLOW FT pieces.
I took the '30' FT blocks, both standard and static as input.