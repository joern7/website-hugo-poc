---
layout: "image"
title: "The other side..."
date: "2007-12-17T22:22:39"
picture: "back_side.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13093
- /details6cf9-2.html
imported:
- "2019"
_4images_image_id: "13093"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13093 -->
Back side of the demo. The FT light is not enough for the Transistor.... redesign is needed.