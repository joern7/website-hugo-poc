---
layout: "overview"
title: "Sylvias Auto-Lackieranlage"
date: 2020-02-22T08:08:58+01:00
legacy_id:
- /php/categories/3138
- /categoriesc9c6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3138 --> 
Eine rein elektromechanisch und elektronisch (mit Silberlingen) gesteuerte Maschine, die Fahrzeuge mit einer simulierten Düse von allen Seiten lackiert und sich selbstständig der unterschiedlichen Karosseriehöhe anpasst.