---
layout: "image"
title: "Düsenumkehr"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage4.jpg"
weight: "4"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42131
- /detailscda7-2.html
imported:
- "2019"
_4images_image_id: "42131"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42131 -->
Zwei Endlagentaster (hier sieht man einen, der andere befindet sich auf der gegenüberliegenden Seite der Mechanik) steuern über ein Flipflop ein polwendendes Relais an, sodass die Düse immer hin und her läuft.