---
layout: "image"
title: "Höhenabtastung und Düse"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage5.jpg"
weight: "5"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42132
- /details8141.html
imported:
- "2019"
_4images_image_id: "42132"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42132 -->
Die simulierte Lackierdüse ist eine Pneumatikdüse, die mit einem kurzen Schlauchstückchen auf einem T-Stück befestigt ist. Tatsächlich wird da aber kein Lack versprüht im Modell ;-)

Die Linsenlampe/Fotozelle-Kombination gibt es auf der Seite gegenüber nochmal, nur sitzt da die Lampe oben und der Fotowiderstand mit dem Störlichttubus unten. Das ergibt zwei Messpunkte, die wie folgt ausgewertet werden:

- Werden beide Lichtschranken unterbrochen, sitzt die Mechanik zu tief. Der Motor für den Fahrzeugtransport wird angehalten und die Mechanik wird angehoben, bis die korrekte Höhe erreicht ist. Dann läuft der Transport weiter.

- Ist die untere Lichtschranke vom Fahrzeugrumpf unterbrochen, die obere aber nicht, ist die Sollhöhe erreicht. Die Mechanik bleibt in dieser Höhe und die Fahrzeuge werden langsam weitergeschoben.

- Ist keine der Lichtschranken unterbrochen, sitzt die Mechanik zu hoch. Der Motor für den Fahrzeugtransport wird angehalten und die Mechanik wird abgesenkt, bis die korrekte Höhe erreicht ist. Dann läuft der Transport weiter.

Die Düse läuft die ganze Zeit über permanent den Bogen entlang von einer Seite übers Fahrzeug hinweg zur anderen und zurück.