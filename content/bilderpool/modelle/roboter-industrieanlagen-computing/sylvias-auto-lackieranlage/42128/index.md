---
layout: "image"
title: "Überblick"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage1.jpg"
weight: "1"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42128
- /detailsd7ea-2.html
imported:
- "2019"
_4images_image_id: "42128"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42128 -->
Fahrzeuge mit ganz unterschiedlichen Karosserieformen werden auf den Schienen immer hin- und hergezogen, durch die Mechanik hindurch. Die tastet die Karosseriehöhe mit zwei Lichtschranken ab und fährt mit einer Düse immer an den Fahrzeugseiten hoch, quer über das Fahrzeug, auf der anderen Seite wieder herunter, genauso wieder zurück, und das endlos. Die Düse passt sich also der Karosseriehöhe direkt unterhalb der Mechanik automatisch an.