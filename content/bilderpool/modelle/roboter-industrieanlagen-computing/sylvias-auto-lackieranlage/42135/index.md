---
layout: "image"
title: "Elektronische Steuerung"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage8.jpg"
weight: "8"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42135
- /detailsb4dd.html
imported:
- "2019"
_4images_image_id: "42135"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42135 -->
Diese Silberlinge steuern das komplette Modell: Je ein Grundbaustein (Operationsverstärker) bereitet die Signale der beiden Fotozellen für die Höhenregulierung auf, das NAND-Glied wird für die Logik der Umschaltung zwischen Fahrzeugtransport und Mechanik-Höhenverstellung benötigt, das Flipflop für die Richtungsumkehr der Düse, und die drei Relais eben für das Umschalten zwischen Fahrzeugtransport und Höhenverstellung, für die richtige Drehrichtung beim Höhenverstellung (rauf oder runter) sowie für die Polung des Motors für die Düse (für die hin- und her-Bewegung). Der Schalter links oben ist für An/Aus der gesamten Anlage.