---
layout: "comment"
hidden: true
title: "18011"
date: "2013-05-28T21:51:47"
uploadBy:
- "majus"
license: "unknown"
imported:
- "2019"
---
Hallo,

vielen Dank für eure positiven Rückmeldungen! 
Meine Maschine steht noch immer bei mir, leider habe ich wegen des bald kommenden Abis immer weniger Zeit, was mir leider immer weniger Zeit für FT gibt. 
(Und mit zunehmendem Alter versuche ich auch, die Modelle perfekter zu bauen, das kostet noch mehr Zeit...)
@ Sushitechnik: Ich wäre aber mal gespannt, ob du das mit der Wasserversorgung besser hinbekommst als ich, das mit dem Becher ist doof, da man den immer wechseln muss und der blaue kleine Tank begrenzt ist. 

An alle: 
Kennt jemand eine bessere Idee, wie man eine Vakuumpumpe bauen kann? Die oben verwendete hat mich über einen Nachmittag reine Entwicklungszeit gekostet!!

Gruß
Majus