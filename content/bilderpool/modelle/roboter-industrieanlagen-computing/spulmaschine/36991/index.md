---
layout: "image"
title: "Gesamtansicht"
date: "2013-05-27T15:45:21"
picture: "Splmaschine1.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/36991
- /detailsb831.html
imported:
- "2019"
_4images_image_id: "36991"
_4images_cat_id: "2751"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36991 -->
Hier seht ihr meine selbst gebaute Spülmaschine. Sie funktioniert in 4 Schritten: 
1. Wasser mittels Vakuum in den blauen Tank hinten pumpen.
2. Wasser mittels Druck aus dem Tank in der Maschine verspränkeln
3. Lüfter trocknet das Geschirr + Abpumpen des Wassers aus der tiefsten Stelle
4. Wasser wieder in den Becher zurückpumpen

Der Becher rechts dient als Wasservorrat und wird erst leergepumpt und später wieder vollgepumpt. Dabei erkennt der Ultraschallsensor, wieviel Wasser zur Verfügung steht. 
Unter diesem Becher ist ein Lüfter, da die Vakuum Pumpe mit Elektromagnetventilen arbeitet und diese dauernd ein + ausschaltet -> der Lüfter verhindert das sie warm werden.
Und der Lüfter auf dem Spülraum links bläst beim Trocknungsvorgang Frischluft in die Maschine...