---
layout: "image"
title: "Der Innenraum"
date: "2013-05-27T15:45:22"
picture: "Splmaschine2.jpg"
weight: "2"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/36992
- /details1c1a.html
imported:
- "2019"
_4images_image_id: "36992"
_4images_cat_id: "2751"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36992 -->
Oben seht ihr den (sich drehenden!) Wasserspränkler (<- Wie heißt das richtig?) und hinten die Geschirrauflage. Links und rechts neben dem Spränkler sieht man die beiden Luftöffnungen für den Lüfter darüber. Verbrauchtes Wasser sammelt sich hinten unten in einem Becken, wo auch ein Schlauch zum Abpumpen hineinragt (von oben, denn dann ist die Maschine auch dicht)