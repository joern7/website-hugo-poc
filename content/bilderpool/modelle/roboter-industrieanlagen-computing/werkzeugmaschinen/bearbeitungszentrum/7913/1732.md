---
layout: "comment"
hidden: true
title: "1732"
date: "2006-12-17T12:00:40"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo,

kleiner Tip: nimm in der Mitte statt der drei Kegelräder ein Rast-Kegelrad auf der Motorseite und führ die Welle gerade durch zum "Bohrer", dann dreht er sich richtig herum. Dafür mußt allerdings in der Mitte etwas schieben, damit das Rast-Kegelrad reinpaßt.

Gruß,
Franz