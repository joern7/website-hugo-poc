---
layout: "image"
title: "Ansicht von hinten"
date: "2006-12-08T22:51:26"
picture: "DSCI0022.jpg"
weight: "14"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrumvon", "hinten"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7745
- /detailsf665-4.html
imported:
- "2019"
_4images_image_id: "7745"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7745 -->
Das ist das Bearbeitungszentrum von hinten.