---
layout: "image"
title: "Steckerleiste"
date: "2006-12-08T22:51:26"
picture: "DSCI0011.jpg"
weight: "11"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Steckerleiste"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7742
- /details229f.html
imported:
- "2019"
_4images_image_id: "7742"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7742 -->
Hier sieht man noch mal wie voll belegt die Steckerleiste meines uralten Interfaces ist. Da ich nur 8 Binäreingänge zur Verfügung hatte musste ich mit dem Fototransitor der Lichtschranke auf einen analogen Eingang ausweichen.