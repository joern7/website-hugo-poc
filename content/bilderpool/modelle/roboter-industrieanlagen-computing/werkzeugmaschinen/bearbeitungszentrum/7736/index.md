---
layout: "image"
title: "Stanze"
date: "2006-12-08T22:51:19"
picture: "DSCI0013.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Stanze"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7736
- /details5481.html
imported:
- "2019"
_4images_image_id: "7736"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7736 -->
Die erste Bearbeitungstation: Eine Stanze.