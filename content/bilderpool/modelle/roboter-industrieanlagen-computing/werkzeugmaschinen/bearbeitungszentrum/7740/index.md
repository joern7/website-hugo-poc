---
layout: "image"
title: "Ansicht 2"
date: "2006-12-08T22:51:19"
picture: "DSCI0029.jpg"
weight: "9"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Förderband"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7740
- /details682f.html
imported:
- "2019"
_4images_image_id: "7740"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7740 -->
Hier noch einmal das Förderband mit dem "Personal".