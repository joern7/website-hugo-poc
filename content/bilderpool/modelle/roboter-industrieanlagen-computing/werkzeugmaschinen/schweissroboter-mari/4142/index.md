---
layout: "image"
title: "Schweißroboter"
date: "2005-05-14T21:12:08"
picture: "motorisierte_Roboter_003.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4142
- /details12db-2.html
imported:
- "2019"
_4images_image_id: "4142"
_4images_cat_id: "353"
_4images_user_id: "189"
_4images_image_date: "2005-05-14T21:12:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4142 -->
