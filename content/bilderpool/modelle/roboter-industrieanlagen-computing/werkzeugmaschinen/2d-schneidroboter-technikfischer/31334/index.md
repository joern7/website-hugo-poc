---
layout: "image"
title: "X-Motor, Achsen"
date: "2011-07-22T16:23:27"
picture: "g06.jpg"
weight: "6"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31334
- /details000b.html
imported:
- "2019"
_4images_image_id: "31334"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31334 -->
Hier sieht man den X-Motor und die beiden Achse