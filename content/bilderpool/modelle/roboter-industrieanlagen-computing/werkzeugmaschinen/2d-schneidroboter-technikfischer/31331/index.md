---
layout: "image"
title: "Schneidgerüst von oben"
date: "2011-07-22T16:23:27"
picture: "g03.jpg"
weight: "3"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31331
- /detailsa25c.html
imported:
- "2019"
_4images_image_id: "31331"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31331 -->
Hier sieht man den Y-Motor und die X-Achsen