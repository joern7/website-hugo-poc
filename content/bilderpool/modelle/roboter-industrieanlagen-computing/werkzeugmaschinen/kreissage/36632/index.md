---
layout: "image"
title: "Ansicht von Hinten"
date: "2013-02-15T00:33:19"
picture: "IMG_4608.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/36632
- /details5b1e.html
imported:
- "2019"
_4images_image_id: "36632"
_4images_cat_id: "2715"
_4images_user_id: "1631"
_4images_image_date: "2013-02-15T00:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36632 -->
Hier kann man 2 blaue Schläuche erkennen,  diese sollten die Absauganlage darstellen, die den Dreck, der bei dem Sägen entsteht, aufsaugt.