---
layout: "image"
title: "(1) Seitenansicht"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter1.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17158
- /details5809.html
imported:
- "2019"
_4images_image_id: "17158"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17158 -->
