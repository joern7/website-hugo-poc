---
layout: "image"
title: "(5) Motor X-Achse"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter5.jpg"
weight: "5"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17162
- /detailsf281.html
imported:
- "2019"
_4images_image_id: "17162"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17162 -->
