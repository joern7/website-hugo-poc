---
layout: "image"
title: "(7) Y-Achse Motor"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter7.jpg"
weight: "7"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17164
- /details995d.html
imported:
- "2019"
_4images_image_id: "17164"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17164 -->
