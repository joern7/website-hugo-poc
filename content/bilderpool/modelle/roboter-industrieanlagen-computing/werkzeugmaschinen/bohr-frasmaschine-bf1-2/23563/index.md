---
layout: "image"
title: "[4/5] BF1-2-RI, Mini-Motoren 2"
date: "2009-03-30T20:09:37"
picture: "bfetappe4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23563
- /detailsaef1.html
imported:
- "2019"
_4images_image_id: "23563"
_4images_cat_id: "1610"
_4images_user_id: "723"
_4images_image_date: "2009-03-30T20:09:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23563 -->
Rechts unten der Motor des Aufsatzdrehtisches (Drehachse C). Links noch mal von oben nach unten der Motor des Teilkopfs (Drehachse A) und des Kreuzschlittens (Linearachse Y). Durch Veränderungen der Geometrie des Kreutztischzubehörs können die Antriebe von Teilkopf und Aufsatzdrehtisch jetzt gleichzeitig auf dem Arbeitstisch aufgesetzt bleiben. Zur Arbeit mit dem Teilkopf muss nur der mit Kreuznocken "verstiftete" Drehtisch abgehoben werden.