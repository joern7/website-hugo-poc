---
layout: "image"
title: "[5/5] BF1-2-RI, Screenshot Bedienfeld"
date: "2009-03-30T20:09:37"
picture: "bfetappe5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23564
- /details436e.html
imported:
- "2019"
_4images_image_id: "23564"
_4images_cat_id: "1610"
_4images_user_id: "723"
_4images_image_date: "2009-03-30T20:09:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23564 -->
Das Steuerprogramm ist vorläufig eine auf 6 Bewegungsachsen plus Arbeitsspindel erweiterte Modifizierung (jetzt 3.503KB) des "TeachIn.rpp" vom 3-Achs-Roboter des Baukastens Computing "Industry Robots II".

Die Programmierung der Abläufe kann über dieses Bedienfeld modellseitig visuell gestützt für vielstufige Bearbeitungen an Werkstücken so leichter ausgeführt werden. Neben den dazu in der Mitte manuell anfahrbaren Bewegungskoordinaten (Impulszahl) können rechts über die Regler für die Vorläufe (vom Endtaster weg) und die Rückläufe (zum Endtaster zurück) für jeden Ablaufschritt individuelle Geschwindigkeiten (Motorstufen) gewählt werden. Ebenso sind für den Spindelmotor Drehrichtung und Motorstufe einstellbar. Das ergibt für jeden Schritt des Ablaufprogramms (Liste *.csv) bis zu 20 "Prozessdaten". Während der Abläufe manuell "Teach-In" (voreingestellt) und automatisch [Play] können links in der Anzeige alle programmierten "Prozessdaten" verfolgt werden.

Die Handbedienung des Modells ohne ein Ablaufprogramm ist im Bedienfeld mit der Cursormaus über die Tasten und Regler arbeitsstufenweise natürlich damit ebenfalls komfortabel möglich.

Das Steuerprogramm benutzt vorläufig noch die unter ft momentan aktuelle Impulssteuerung Impulsrad 4 mit Mini-Taster. Bei Rechtsdrehungen der Drehachsen über den Endtaster hinweg ist hier noch zur Programmierung der Ablaufprogramme ihr Editieren unter MSEXCEL notwendig (Impulse mit Minusvorzeichen). Steuerprogramm und Modell werden mit der Verfügbarkeit hierfür geeigneter ft-Neuheiten aus 2009 fortentwickelt (Etappe 4). 

Geladen ist hier im Steuerprogramm ein Ablaufprogramm mit 28 Schritten, dessen Ablauf ich zur vorläufigen Demonstration der 6 Achsenbewegungen am Modell als Video aufgenommen habe. Bewegt werden die Achsen in der Reihenfolge XYZABC, die Linearachsen in voller Länge, die Drehachsen wegen der Videolänge nach links und rechts vom Endtaster weg je nur 45°.