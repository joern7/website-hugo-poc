---
layout: "image"
title: "[2/5] BF1-2-RI, Power Motoren"
date: "2009-03-30T20:09:36"
picture: "bfetappe2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23561
- /details036d.html
imported:
- "2019"
_4images_image_id: "23561"
_4images_cat_id: "1610"
_4images_user_id: "723"
_4images_image_date: "2009-03-30T20:09:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23561 -->
Der Spindelmotor 8:1 hat jetzt Gesellschaft von 2 Power Motoren 50:1 für die Linearachse Z (Spindelkopf vertikal) und Drehachse B (Schwenken des Spindelkopfes).