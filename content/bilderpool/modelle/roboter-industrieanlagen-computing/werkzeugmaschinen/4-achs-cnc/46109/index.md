---
layout: "image"
title: "Bedienung der CNC"
date: "2017-08-01T12:51:17"
picture: "achscnc1_2.jpg"
weight: "6"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/46109
- /details5a6f.html
imported:
- "2019"
_4images_image_id: "46109"
_4images_cat_id: "3424"
_4images_user_id: "1164"
_4images_image_date: "2017-08-01T12:51:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46109 -->
Ein Raspberry Pi 3 greift via VNC auf meinen Laptop zu, auf welchem RoboPro läuft. So habe ich keinen riesen Kabelsalat in der Fräse und kann den Laptop schön zur Seite stellen und laufen lassen.