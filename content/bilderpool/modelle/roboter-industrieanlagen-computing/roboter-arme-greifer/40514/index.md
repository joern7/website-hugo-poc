---
layout: "image"
title: "Pneumatikgreifer aus dem CVK-Schulprogramm von 1987"
date: "2015-02-09T17:13:11"
picture: "S1160005.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40514
- /details298d.html
imported:
- "2019"
_4images_image_id: "40514"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40514 -->
Hier die Details zum Pneumatik-Greifer. Vorteil: Wenig Spezialteile, robuste Funktion, einfacher Aufbau, kein Klemmen, öffnet sich alleine durch Abschalten (Entlüften) des elektropneumatischen Ventils. Daher nur eine Druckschlauch-Zuführung zum Greifer.