---
layout: "image"
title: "Grundplatte"
date: "2009-03-20T16:55:18"
picture: "achsroboter1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23473
- /detailsa6ce-2.html
imported:
- "2019"
_4images_image_id: "23473"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23473 -->
Die Bewegung läuft über den Schrittmotor ab. Klappt Problemlos.