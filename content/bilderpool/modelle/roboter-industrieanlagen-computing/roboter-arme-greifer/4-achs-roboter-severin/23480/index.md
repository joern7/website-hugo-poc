---
layout: "image"
title: "Achse 3 im Detail"
date: "2009-03-20T16:55:25"
picture: "achsroboter8.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23480
- /details3a41.html
imported:
- "2019"
_4images_image_id: "23480"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23480 -->
In der finalen Version ist der PM ein 20:1er gewesen. Rein von der Kraft her könnte man überall 8:1er nehmen, Da spielt das Roboint dann aber nicth mehr mit.