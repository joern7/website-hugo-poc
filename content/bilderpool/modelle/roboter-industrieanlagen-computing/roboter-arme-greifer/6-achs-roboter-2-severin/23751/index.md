---
layout: "image"
title: "Achse 1 und 2 (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23751
- /detailsc23a-2.html
imported:
- "2019"
_4images_image_id: "23751"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23751 -->
Achse 1 ist mit der vom 4-Achser identisch.