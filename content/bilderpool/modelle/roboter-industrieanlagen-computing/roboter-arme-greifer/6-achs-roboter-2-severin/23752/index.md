---
layout: "image"
title: "Achse 1-3 (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin5.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23752
- /detailsc7f5.html
imported:
- "2019"
_4images_image_id: "23752"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23752 -->
Mir war es wichtig, dass Achse 2 eine Bewegungsfreiheit von 180° bekommt.