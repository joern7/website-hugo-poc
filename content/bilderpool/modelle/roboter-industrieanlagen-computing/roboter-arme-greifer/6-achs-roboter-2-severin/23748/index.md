---
layout: "image"
title: "Von Rechts (0.9)"
date: "2009-04-22T17:56:28"
picture: "achsroboterseverin1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23748
- /details4d30.html
imported:
- "2019"
_4images_image_id: "23748"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23748 -->
Im Roboter sind alle Belasteten Lager mit Kugellagern ausgestattet