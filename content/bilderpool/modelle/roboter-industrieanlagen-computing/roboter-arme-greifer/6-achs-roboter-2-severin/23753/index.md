---
layout: "image"
title: "Achse 3 (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23753
- /detailsd066.html
imported:
- "2019"
_4images_image_id: "23753"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23753 -->
Die Verbindung der beiden Motoren zur 3. Achse wird über die Kardangelenke von TST erfolgen.