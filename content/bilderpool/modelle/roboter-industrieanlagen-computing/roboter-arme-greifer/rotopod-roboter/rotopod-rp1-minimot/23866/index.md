---
layout: "image"
title: "[2/7] Modellansicht"
date: "2009-05-04T21:14:31"
picture: "rotopodrp2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23866
- /details3258.html
imported:
- "2019"
_4images_image_id: "23866"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23866 -->
Bei den pneumo- und elektromechanischen Hexapodmodellen von Remadus reichten je Stützbein 4 Gelenke. Hier müssen es 5 sein, weil sich die Stützbeinantriebe noch zu den Achsen der Stützbeine im Kreisbogen drehen. Das ist mit Teilen in ft durchaus realisierbar. Bewegungsspiel und Optik konnte mich jedoch in keinem Fall der möglichen Lösungen funktionell überzeugen. Es sollten auch Stützbeine mit M-Achsen sein. Also griff ich zu den Kardangelenken und einem Trick, der hier die Kugelgelenkfunktion nachempfindet.
Leider sind die eingesetzten M-Achsen 80 etwas zu lang und alternativ dazu die M-Achsen 60 etwas zu kurz. Bei zu langen Stützbeinen fallen die Bewegungen der Tischplatine sichtbar kürzer aus.
