---
layout: "image"
title: "gezamtanzicht"
date: "2012-01-22T17:36:24"
picture: "ballgreiferk.jpg"
weight: "1"
konstrukteure: 
- "Ton van Beekum"
fotografen:
- "Ton van Beekum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "beeton"
license: "unknown"
legacy_id:
- /php/details/33989
- /detailsf616.html
imported:
- "2019"
_4images_image_id: "33989"
_4images_cat_id: "2517"
_4images_user_id: "1253"
_4images_image_date: "2012-01-22T17:36:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33989 -->
FT Balgijper
Mijn kennis van de Duitse taal is onvoldoende, dus schrijf ik in het Nederlands.
Dit model is een voortzetting van eerdere versies. Morshausen 2008, Erbes Bundesheim 2010. Wat mij aantrok in dit model was dat het absoluut nergens toe dient.
In eerste instantie heb ik geprobeerd het model na te bouwen, maar dan moet je kostbare Alu modificeren. Ook de besturing kon ik niet maken. Dus na een keer of acht alles in en uit elkaar gehaald te hebben; alle voorbeelden weggegooid en uit het hoofd gaan bouwen. Alles zo simpel mogelijk gehouden en geen modificaties toegepast. Na een tijdje prutsen een werkend model gemaakt dat 1 uur achterelkaar gedraaid heeft met twee balletjes zonder kapot te gaan.