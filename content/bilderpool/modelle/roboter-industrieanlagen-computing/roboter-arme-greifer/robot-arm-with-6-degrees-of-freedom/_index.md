---
layout: "overview"
title: "Robot arm with 6 degrees of freedom (bummtschick)"
date: 2020-02-22T08:03:16+01:00
legacy_id:
- /php/categories/2844
- /categories9b2d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2844 --> 
Photos of my model of a robot arm with six degrees of freedom, controlled by an Arduino Mega 2560. Much more to come soon, these are outdated and a few months old.