---
layout: "image"
title: "Gelenk 1 (3879)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_1_3879.jpg"
weight: "6"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38597
- /detailsc4d9.html
imported:
- "2019"
_4images_image_id: "38597"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38597 -->
Hier sieht man deutlich die zwei Drehkränze von Gelenk 1, die mit einem Aluprofil innen verbunden sind. Die vier Aluprofile außen stehen fest, das Profil innen dreht sich. Der Motor treibt den unteren Drehkranz an.  Diese Konstruktion muss extrem viel aushalten. Die Idee habe ich von Heiko Engelke ( http://www.ftcommunity.de/categories.php?cat_id=186 ). Das hält so weit auch gut, aber die Konstruktion außen ist an ihren Grenzen angekommen und müsste nochmal verstärkt werden.