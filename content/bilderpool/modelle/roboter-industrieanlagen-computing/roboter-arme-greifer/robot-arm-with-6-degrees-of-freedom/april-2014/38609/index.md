---
layout: "image"
title: "Gelenk 5 (3902)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_5_3902.jpg"
weight: "18"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38609
- /details71ac-2.html
imported:
- "2019"
_4images_image_id: "38609"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38609 -->
Gelenk Nr. 5 ist ca. 50% ausgefahren.