---
layout: "image"
title: "Gelenk 4 (3932)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_4_3932.jpg"
weight: "16"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38607
- /details75ee.html
imported:
- "2019"
_4images_image_id: "38607"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38607 -->
Hier ist Gelenk 4 von oben fotografiert. Die Bilckrichtung ist umgekehrt zu den vorherigen Bildern: das Handgelenk ist rechts, der Drehkranz links ist von Gelenk Nr. 3. Unten sichtbar ist die lange Rastachse, die vom Encodermotor ganz links (nicht mehr im Bild) kommt und über ein Kegelzahnrad die große Schnecke vom Drehkranz für Gelenk 4 antreibt. Man sieht die beiden Encodermotoren, die durch den Drehkranz von Gelenk 4 die Gelenke 5 und 6 antreiben.