---
layout: "image"
title: "A - Gesamtansicht (3921)"
date: "2014-04-22T16:15:53"
picture: "A_-_Gesamtansicht_3921.jpg"
weight: "2"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38593
- /details491f.html
imported:
- "2019"
_4images_image_id: "38593"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38593 -->
Bertha teilweise ausgefaltet, in typischer Greifstellung.