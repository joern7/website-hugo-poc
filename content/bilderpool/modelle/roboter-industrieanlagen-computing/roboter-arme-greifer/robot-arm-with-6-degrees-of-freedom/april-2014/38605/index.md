---
layout: "image"
title: "Gelenk 4 (3899)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_4_3899.jpg"
weight: "14"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38605
- /detailsc9e2-2.html
imported:
- "2019"
_4images_image_id: "38605"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38605 -->
Gelenk 4 ca. 50% gedreht.