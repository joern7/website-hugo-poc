---
layout: "image"
title: "Greifer (3916)"
date: "2014-04-22T16:15:54"
picture: "Greifer_3916.jpg"
weight: "25"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38616
- /detailsb512-2.html
imported:
- "2019"
_4images_image_id: "38616"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38616 -->
Hier sieht man Gelenk 6 und den Pneumatik-Zylinder des Greifers, der durch den letzten Drehkranz hindurch reicht.