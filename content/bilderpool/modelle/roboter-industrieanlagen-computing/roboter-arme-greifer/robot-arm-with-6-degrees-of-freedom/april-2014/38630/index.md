---
layout: "image"
title: "Y - Arduino (3859)"
date: "2014-04-22T16:15:54"
picture: "Y_-_Arduino_3859.jpg"
weight: "39"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38630
- /details0697.html
imported:
- "2019"
_4images_image_id: "38630"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38630 -->
Noch mehr Kabelsalat. Hier ist der Arduino (unten, wo der Netzstecker reingeht) mit den zwei Motor Shields schön zu sehen. Die Verdrahtung ist immer noch großenteils auf dem Breadboard, das würde ein professioneller Elektroniker sicherlich besser machen ... Die Widerstände und Kondensatoren dienen zum Entprellen der Taster, die Interrupts für die Encodersignale auslösen. Ich habe eine Weile gebraucht, um rauszufinden, warum die Software komplett falsch zählte.