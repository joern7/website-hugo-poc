---
layout: "image"
title: "Greifer geschlossen (3918)"
date: "2014-04-22T16:15:54"
picture: "Greifer_geschlossen_3918.jpg"
weight: "29"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38620
- /details2b45-3.html
imported:
- "2019"
_4images_image_id: "38620"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38620 -->
