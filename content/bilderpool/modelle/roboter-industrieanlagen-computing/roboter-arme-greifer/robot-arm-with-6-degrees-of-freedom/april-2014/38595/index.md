---
layout: "image"
title: "A - Gesamtansicht (3924)"
date: "2014-04-22T16:15:53"
picture: "A_-_Gesamtansicht_3924.jpg"
weight: "4"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38595
- /details5d25.html
imported:
- "2019"
_4images_image_id: "38595"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38595 -->
Gelenke 2, 3 und 5 auf maximale Streckung gestellt.