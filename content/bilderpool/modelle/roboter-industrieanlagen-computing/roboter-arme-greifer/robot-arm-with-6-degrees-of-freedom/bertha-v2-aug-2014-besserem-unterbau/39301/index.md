---
layout: "image"
title: "03 Bertha V2 Baustufe (5440)"
date: "2014-08-26T18:59:41"
picture: "03_Bertha_V2_Baustufe_5440.jpg"
weight: "3"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39301
- /detailse180.html
imported:
- "2019"
_4images_image_id: "39301"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39301 -->
Wie 02, aber aus anderer Position.