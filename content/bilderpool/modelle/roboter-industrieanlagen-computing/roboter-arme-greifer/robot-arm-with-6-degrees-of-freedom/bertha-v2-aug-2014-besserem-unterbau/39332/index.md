---
layout: "image"
title: "16 Stand Sept: gesamt (5486)"
date: "2014-09-04T22:58:48"
picture: "16_Stand_Sept_gesamt_5486.jpg"
weight: "16"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39332
- /details9dec-3.html
imported:
- "2019"
_4images_image_id: "39332"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39332 -->
Links neben der Drehplatte der Arduino Uno mit Adafruit Motor Shield V2. Der Shield hat sich schon bei V1 bewährt, und ich würde ihn jederzeit wieder empfehlen., gerade auch, wenn man noch nie etwas mit Arduino gemacht hat. Bertha V2 hat jetzt einen "normalen" Arduino Uno, während V1 noch einen Arduino Mega 2560 hatte (ich hatte dort ja fast 50 Pins gebraucht). Das scheint jetzt auszureichen, allerdings bin ich beim Programmieren ein paarmal an die Grenzen von nur 2 KB SRAM geraten. Die Probleme sind aber lösbar. 

Der Kabelsalat wird sich noch ordnen lassen, im Moment wird noch viel gestöpselt. Die beiden Verteilerplatten hinter dem LCD dienen der Stromversorgung. Das schwarze Kabel, das von vorne aus einem Standard-9V-Netzteil kommt, geht in den Arduino, und dort nehme ich +9V und Masse ab (geht in die Verteilerplatten), und +5V steht auch zur Verfügung (das Display will das zum Beispiel)..