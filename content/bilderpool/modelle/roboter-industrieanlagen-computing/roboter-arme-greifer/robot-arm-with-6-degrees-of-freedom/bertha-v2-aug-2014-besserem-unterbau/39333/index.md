---
layout: "image"
title: "17 Stand Sept: gesamt (5487)"
date: "2014-09-04T22:58:48"
picture: "17_Stand_Sept_gesamt_5487.jpg"
weight: "17"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39333
- /details600b.html
imported:
- "2019"
_4images_image_id: "39333"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39333 -->
Die Fernbedienung. Die Anordnung und Verkabelung der Taster ist wie bei Bertha V1. Allerdings gehen die Kabel jetzt alle in die Batteriebox mit einem Arduino Micro drin (siehe meine Darstellung in <a href="http://www.ftcommunity.de/categories.php?cat_id=2943" rel="nofollow">www.ftcommunity.de/categories.php?cat_id=2943</a> ). Wie man sieht, habe ich eine ganze Menge Buchsen belegt. Der Deckel ist gerade offen, damit ich zum Programmieren.an den USB-Micro-Stöpsel rankomme

Die Fernbedienung ist nur noch über vier Kabel mit dem Haupt-Controller verbunden, wie aus der Beschriftung auf den schwarzen Lampensockeln ersichtlich: "GND +9 SCL SDA", also Stromversorgung (Masse und +9V vom Arduino-Controller) und I2C (SCL und SDA). Wenn man eine Taste drückt, erhält der Controller Meldung und kann entsprechend Motoren anfeuern.