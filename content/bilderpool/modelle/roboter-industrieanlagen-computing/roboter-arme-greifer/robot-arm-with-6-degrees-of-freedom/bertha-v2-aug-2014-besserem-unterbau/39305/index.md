---
layout: "image"
title: "07 Bertha V2 Baustufe (5447)"
date: "2014-08-26T18:59:41"
picture: "07_Bertha_V2_Baustufe_5447.jpg"
weight: "7"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39305
- /details059c-3.html
imported:
- "2019"
_4images_image_id: "39305"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39305 -->
Wie 05, aus anderer Perspektive.