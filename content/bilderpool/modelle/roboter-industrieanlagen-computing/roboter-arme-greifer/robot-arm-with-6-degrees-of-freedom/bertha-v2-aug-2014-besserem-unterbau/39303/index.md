---
layout: "image"
title: "05 Bertha V2 Baustufe (5445)"
date: "2014-08-26T18:59:41"
picture: "05_Bertha_V2_Baustufe_5445.jpg"
weight: "5"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39303
- /details050d.html
imported:
- "2019"
_4images_image_id: "39303"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39303 -->
Der größte Teil des Roboterarms (Gelenke 2 bis 6, mit fast allen Motoren auch schon dran. Den Encodermotor in der MItte habe ich später nochmal anders montiert.)