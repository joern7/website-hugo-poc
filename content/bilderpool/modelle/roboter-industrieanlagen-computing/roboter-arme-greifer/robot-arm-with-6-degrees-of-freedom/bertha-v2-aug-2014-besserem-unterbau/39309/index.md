---
layout: "image"
title: "11 Bertha V2 gesamt (5462)"
date: "2014-08-26T18:59:41"
picture: "11_Bertha_V2_gesamt_5462.jpg"
weight: "11"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39309
- /detailsc08f.html
imported:
- "2019"
_4images_image_id: "39309"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39309 -->
Wie 10, aber aus anderer Perspektive.