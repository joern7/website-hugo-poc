---
layout: "image"
title: "Nov 2013"
date: "2014-02-08T22:06:50"
picture: "IMG_1942.jpg"
weight: "1"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38214
- /details4a5e.html
imported:
- "2019"
_4images_image_id: "38214"
_4images_cat_id: "2844"
_4images_user_id: "2106"
_4images_image_date: "2014-02-08T22:06:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38214 -->
Older version of my robot arm. This gives you an idea what it looks like, but much has happened since... more photos to come soon.