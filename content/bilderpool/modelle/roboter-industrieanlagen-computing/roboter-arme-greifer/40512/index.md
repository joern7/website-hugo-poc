---
layout: "image"
title: "Modifizierter Trainingsroboter aus dem Baukasten 36069 Profi Computing von 1991"
date: "2015-02-09T17:13:11"
picture: "S1160003.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Industrieroboter", "Trainingsroboter", "Knickarmroboter", "Pneumatikgreifer", "M-Motor", "Kompressor"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40512
- /details2f88.html
imported:
- "2019"
_4images_image_id: "40512"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40512 -->
Umrüstung auf Pneumatik Greifer von den Pneumatic Robots CVK-Modellen aus dem Schulprogramm von 1987. Diese Pneumatik-Greifer finde ich viel besser als die viel häufiger eingesetzten elektromechanischen Greifer mit Schneckengetriebe...

Der Kompressor ist auf den Arm montiert als Gegengewicht zum Ausbalancieren.

Außerdem Umbau der 2 Drehachsen auf kräftigere graue M-Motoren.