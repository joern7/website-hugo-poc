---
layout: "image"
title: "Gesamtansicht_2"
date: "2007-01-21T15:27:21"
picture: "roboterarm02.jpg"
weight: "2"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8594
- /details1b08-2.html
imported:
- "2019"
_4images_image_id: "8594"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8594 -->
Gesamtansicht