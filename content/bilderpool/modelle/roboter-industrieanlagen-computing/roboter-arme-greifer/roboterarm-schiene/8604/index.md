---
layout: "image"
title: "Gesamtansicht 5"
date: "2007-01-21T21:01:29"
picture: "roboterarm12.jpg"
weight: "12"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8604
- /detailsf63f-2.html
imported:
- "2019"
_4images_image_id: "8604"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8604 -->
Gesamtansicht