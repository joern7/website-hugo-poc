---
layout: "overview"
title: "Rob 4 aus dem alten Industry Robots Set"
date: 2020-02-22T08:02:53+01:00
legacy_id:
- /php/categories/2244
- /categories8a85.html
- /categories5ffc.html
- /categories9ada.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2244 --> 
Ich habe mal den Rob 4 aus dem alten Industry Robots Set nachgebaut. Ich habe micht soweit es ginge an die original Anleitung gehalten. An den Stellen, wo ich mit meinen Teilen nicht weiter kam habe ich ein wenig Improvisiert (wenn ich das so sagen darf?).