---
layout: "image"
title: "Greifer"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset06.jpg"
weight: "6"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/30205
- /details68a3-2.html
imported:
- "2019"
_4images_image_id: "30205"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30205 -->
Greifer