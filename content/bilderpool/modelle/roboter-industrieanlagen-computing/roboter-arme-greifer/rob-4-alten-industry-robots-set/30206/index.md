---
layout: "image"
title: "Robotterarm"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset07.jpg"
weight: "7"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/30206
- /detailsea70-2.html
imported:
- "2019"
_4images_image_id: "30206"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30206 -->
Robotterarm