---
layout: "image"
title: "kopf"
date: "2009-05-08T23:33:19"
picture: "achsroboter01.jpg"
weight: "1"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23926
- /details341e-2.html
imported:
- "2019"
_4images_image_id: "23926"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23926 -->
neues, schärferes Bild vom Kopf des Roboters