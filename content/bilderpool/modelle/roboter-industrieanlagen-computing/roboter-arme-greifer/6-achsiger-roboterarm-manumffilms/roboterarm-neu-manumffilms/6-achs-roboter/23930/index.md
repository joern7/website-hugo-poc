---
layout: "image"
title: "gesamt"
date: "2009-05-08T23:33:19"
picture: "achsroboter05.jpg"
weight: "5"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23930
- /detailsbb03-2.html
imported:
- "2019"
_4images_image_id: "23930"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23930 -->
die verkabelte Gesamtansicht