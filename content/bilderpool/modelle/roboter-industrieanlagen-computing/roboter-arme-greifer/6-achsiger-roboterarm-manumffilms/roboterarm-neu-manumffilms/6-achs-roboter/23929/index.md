---
layout: "image"
title: "kabel am Drehkranz"
date: "2009-05-08T23:33:19"
picture: "achsroboter04.jpg"
weight: "4"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23929
- /detailsc1c8.html
imported:
- "2019"
_4images_image_id: "23929"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23929 -->
die gesamten Kabel für die Achsen 2 - 7