---
layout: "image"
title: "Grundplatte"
date: "2009-05-06T21:41:35"
picture: "DSCF4122.jpg"
weight: "2"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: ["roboterarm", "manuMFfilms", "cool", "roboter"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23907
- /details5909-2.html
imported:
- "2019"
_4images_image_id: "23907"
_4images_cat_id: "1638"
_4images_user_id: "934"
_4images_image_date: "2009-05-06T21:41:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23907 -->
