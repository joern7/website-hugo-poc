---
layout: "comment"
hidden: true
title: "9173"
date: "2009-05-07T11:19:06"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Wenn das Bild schon so unscharf ist ... könntest Du es nicht nochmal scharf aufnehmen? Meistens gibts an den Kameras so eine Makro-Taste, mit der man näher rankommt. Die trägt normalerweise so ein Blumensymbol.