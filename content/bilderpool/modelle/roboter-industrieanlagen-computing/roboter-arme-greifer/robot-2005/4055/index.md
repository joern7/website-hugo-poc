---
layout: "image"
title: "Robot-2005"
date: "2005-04-20T16:01:47"
picture: "Fischertechnik-Robot-2005_011.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4055
- /detailsd439-2.html
imported:
- "2019"
_4images_image_id: "4055"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-04-20T16:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4055 -->
