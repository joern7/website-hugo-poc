---
layout: "image"
title: "Robot-2005"
date: "2005-05-01T20:27:14"
picture: "1Mei-2005-boottochtje_024.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4100
- /details4447-2.html
imported:
- "2019"
_4images_image_id: "4100"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-05-01T20:27:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4100 -->
