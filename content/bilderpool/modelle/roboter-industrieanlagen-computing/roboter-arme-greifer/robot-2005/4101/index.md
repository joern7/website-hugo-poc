---
layout: "image"
title: "Robot-2005"
date: "2005-05-02T10:15:54"
picture: "Robot-2005_001.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4101
- /details353c.html
imported:
- "2019"
_4images_image_id: "4101"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-05-02T10:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4101 -->
