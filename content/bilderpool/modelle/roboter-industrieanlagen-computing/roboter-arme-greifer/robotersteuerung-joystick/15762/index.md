---
layout: "image"
title: "Joy2Rob Java"
date: "2008-09-30T22:22:56"
picture: "Softweare.jpg"
weight: "6"
konstrukteure: 
- "Friedrich Mütschele nach FT Anleitung Modifiziert"
fotografen:
- "Friedrich Mütschele"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fridl"
license: "unknown"
legacy_id:
- /php/details/15762
- /details801b-2.html
imported:
- "2019"
_4images_image_id: "15762"
_4images_cat_id: "1441"
_4images_user_id: "753"
_4images_image_date: "2008-09-30T22:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15762 -->
Nun kann man noch die einfache Java Konsole sehen, über die gesteuert wird. Das Hilfemenü offenbart nochmal wie man den Roboter steuern muss.