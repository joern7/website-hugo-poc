---
layout: "image"
title: "Joy2Rob 5"
date: "2008-09-30T22:22:56"
picture: "IMGP8027.jpg"
weight: "5"
konstrukteure: 
- "Friedrich Mütschele nach FT Anleitung Modifiziert"
fotografen:
- "Friedrich Mütschele"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fridl"
license: "unknown"
legacy_id:
- /php/details/15761
- /details732a.html
imported:
- "2019"
_4images_image_id: "15761"
_4images_cat_id: "1441"
_4images_user_id: "753"
_4images_image_date: "2008-09-30T22:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15761 -->
