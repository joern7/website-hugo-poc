---
layout: "comment"
hidden: true
title: "8265"
date: "2009-01-14T21:19:18"
uploadBy:
- "fridl"
license: "unknown"
imported:
- "2019"
---
Hallo Lysander,

Wie erwähnt habe ich den Spaß NICHT über ROBOPro gesteuert, sondern über ein JAVA Programm. Mit einer speziellen JAVA Klasse habe ich den Joystick ausgelesen und in Bewegungsbefehle umgesetzt. An das IF wird das ganze über UMuellers JAVA Klasse übergeben.

Fridl