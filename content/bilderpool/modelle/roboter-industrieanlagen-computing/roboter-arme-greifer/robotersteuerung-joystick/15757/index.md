---
layout: "image"
title: "Joy2Rob 1"
date: "2008-09-30T22:22:56"
picture: "IMGP8023.jpg"
weight: "1"
konstrukteure: 
- "Friedrich Mütschele nach FT Anleitung Modifiziert"
fotografen:
- "Friedrich Mütschele"
keywords: ["Joystick", "Java", "Roboter", "fischertechnik", "steuerung"]
uploadBy: "fridl"
license: "unknown"
legacy_id:
- /php/details/15757
- /details9ff3.html
imported:
- "2019"
_4images_image_id: "15757"
_4images_cat_id: "1441"
_4images_user_id: "753"
_4images_image_date: "2008-09-30T22:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15757 -->
Der Joystick dient als Eingabegerät. Dieser wird von JAVA ausgelesen und mit der umFish.dll und JavaFish.dll in Befehle an das Interface umgewandelt. Somit kann man den Roboter mit dem Joystick steuern.