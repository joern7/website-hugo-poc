---
layout: "image"
title: "Antrieb ausfahrbarer Arm"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband14.jpg"
weight: "14"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29629
- /details7664.html
imported:
- "2019"
_4images_image_id: "29629"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29629 -->
Anschluss an Interface: M2