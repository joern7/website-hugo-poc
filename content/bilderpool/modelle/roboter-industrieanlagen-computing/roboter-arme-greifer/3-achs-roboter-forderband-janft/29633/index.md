---
layout: "image"
title: "Greifer geöffnet"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband18.jpg"
weight: "18"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29633
- /details0df4-2.html
imported:
- "2019"
_4images_image_id: "29633"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29633 -->
Anschluss an Interface:
Impulszähler I8
Endtaster I7