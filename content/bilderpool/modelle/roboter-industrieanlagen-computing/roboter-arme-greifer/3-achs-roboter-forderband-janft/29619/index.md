---
layout: "image"
title: "Förderband mit Werkstück"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband04.jpg"
weight: "4"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29619
- /detailsfdad.html
imported:
- "2019"
_4images_image_id: "29619"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29619 -->
Das Vorbild ist aber das Set : 50463 Transportband 9V kompl.