---
layout: "image"
title: "Hebeturm Ansicht von oben"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband11.jpg"
weight: "11"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29626
- /detailsc482.html
imported:
- "2019"
_4images_image_id: "29626"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29626 -->
Stimmt der Ausdruck "Hebeturm" ?