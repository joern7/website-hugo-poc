---
layout: "image"
title: "1. Lichtschranke Förderband"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband20.jpg"
weight: "20"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29635
- /detailsd5fd.html
imported:
- "2019"
_4images_image_id: "29635"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29635 -->
Anschluss an Extension:
Lampe M2
Transistor I2