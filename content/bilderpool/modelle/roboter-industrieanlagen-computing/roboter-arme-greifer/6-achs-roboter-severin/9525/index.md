---
layout: "image"
title: "Neues Handgelenk"
date: "2007-03-15T13:57:02"
picture: "IMG_1250.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: ["Handgelenk"]
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9525
- /details3a7f.html
imported:
- "2019"
_4images_image_id: "9525"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-15T13:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9525 -->
Das ist mein bisher gut funktionierendes Handgelenk. Ob es sich in der Praxis bewähren wird weis ich noch nicht.