---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:34:49"
picture: "achsroboter04.jpg"
weight: "22"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9806
- /details54ef.html
imported:
- "2019"
_4images_image_id: "9806"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:34:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9806 -->
Handgelenk