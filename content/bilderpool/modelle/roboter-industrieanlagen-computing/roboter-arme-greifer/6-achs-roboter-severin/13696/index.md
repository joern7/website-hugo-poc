---
layout: "image"
title: "Verkürtzter Roboter"
date: "2008-02-19T17:20:50"
picture: "robi1.jpg"
weight: "41"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13696
- /detailsdcb8.html
imported:
- "2019"
_4images_image_id: "13696"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13696 -->
