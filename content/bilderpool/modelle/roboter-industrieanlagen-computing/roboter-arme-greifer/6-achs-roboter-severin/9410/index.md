---
layout: "image"
title: "6-Achsroboter"
date: "2007-03-12T18:06:47"
picture: "IMG_1242.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: ["Roboterarm"]
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9410
- /details9e54.html
imported:
- "2019"
_4images_image_id: "9410"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-12T18:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9410 -->
Der Roboterarm wird dann auf die Platte gesteckt.