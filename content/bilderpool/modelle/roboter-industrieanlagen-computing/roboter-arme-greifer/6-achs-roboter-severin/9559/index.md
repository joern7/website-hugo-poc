---
layout: "image"
title: "Neues Handgelenk von der Seite"
date: "2007-03-18T11:28:25"
picture: "achsroboter3.jpg"
weight: "12"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9559
- /detailseb6c.html
imported:
- "2019"
_4images_image_id: "9559"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9559 -->
