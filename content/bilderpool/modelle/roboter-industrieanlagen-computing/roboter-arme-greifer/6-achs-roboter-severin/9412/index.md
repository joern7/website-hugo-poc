---
layout: "image"
title: "Roboterarm ohne Handgelenk und Gewicht 2"
date: "2007-03-12T18:06:47"
picture: "IMG_1244.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9412
- /detailsa3c4.html
imported:
- "2019"
_4images_image_id: "9412"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-12T18:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9412 -->
