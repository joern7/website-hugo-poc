---
layout: "image"
title: "Roboterarm 5"
date: "2007-01-18T22:04:46"
picture: "roboterarm05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8501
- /detailsf1cf.html
imported:
- "2019"
_4images_image_id: "8501"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8501 -->
