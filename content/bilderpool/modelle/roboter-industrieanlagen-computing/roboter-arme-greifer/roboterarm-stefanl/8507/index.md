---
layout: "image"
title: "Roboterarm 11"
date: "2007-01-18T22:46:10"
picture: "roboterarm11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8507
- /detailsd8a6-2.html
imported:
- "2019"
_4images_image_id: "8507"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:46:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8507 -->
