---
layout: "image"
title: "Roboterarm 9"
date: "2008-08-17T18:33:42"
picture: "roboterarmneu09.jpg"
weight: "9"
konstrukteure: 
- "---"
fotografen:
- "---"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15060
- /details0e0f.html
imported:
- "2019"
_4images_image_id: "15060"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15060 -->
