---
layout: "image"
title: "Roboterarm 12"
date: "2008-09-28T13:58:10"
picture: "roboterarm2.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15654
- /detailsd697.html
imported:
- "2019"
_4images_image_id: "15654"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15654 -->
