---
layout: "image"
title: "Roboterarm 19"
date: "2008-09-28T13:58:10"
picture: "roboterarm9.jpg"
weight: "19"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15661
- /detailsf005.html
imported:
- "2019"
_4images_image_id: "15661"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15661 -->
...dreht es dann herum und setzt es auf der rechten Seite ab. Ein Video werde ich noch machen.