---
layout: "image"
title: "Roboterarm 10"
date: "2008-08-17T18:33:42"
picture: "roboterarmneu10.jpg"
weight: "10"
konstrukteure: 
- "---"
fotografen:
- "---"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15061
- /detailsc7bc-2.html
imported:
- "2019"
_4images_image_id: "15061"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15061 -->
