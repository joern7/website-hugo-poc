---
layout: "image"
title: "Roboterarm 11"
date: "2008-09-28T13:58:10"
picture: "roboterarm1.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15653
- /details4bce-3.html
imported:
- "2019"
_4images_image_id: "15653"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15653 -->
Habe jetzt einen Schneckenantrieb statt dem Direktantrieb drangebaut da der Direktantrieb zu schwach und zu ungenau war. Einen neuen Greifer hat er auch bekommen.