---
layout: "image"
title: "Roboterarm 7"
date: "2008-08-17T18:33:42"
picture: "roboterarmneu07.jpg"
weight: "7"
konstrukteure: 
- "---"
fotografen:
- "---"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15058
- /details1f65.html
imported:
- "2019"
_4images_image_id: "15058"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15058 -->
