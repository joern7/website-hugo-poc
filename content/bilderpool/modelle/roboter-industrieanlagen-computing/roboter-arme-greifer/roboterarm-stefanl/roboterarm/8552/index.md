---
layout: "image"
title: "Roboterarm 24"
date: "2007-01-20T16:46:18"
picture: "roboterarm12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8552
- /details7718-2.html
imported:
- "2019"
_4images_image_id: "8552"
_4images_cat_id: "788"
_4images_user_id: "502"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8552 -->
