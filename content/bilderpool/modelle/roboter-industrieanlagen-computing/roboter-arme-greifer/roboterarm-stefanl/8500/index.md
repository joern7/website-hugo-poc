---
layout: "image"
title: "Roboterarm 4"
date: "2007-01-18T22:04:46"
picture: "roboterarm04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8500
- /details3054.html
imported:
- "2019"
_4images_image_id: "8500"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8500 -->
