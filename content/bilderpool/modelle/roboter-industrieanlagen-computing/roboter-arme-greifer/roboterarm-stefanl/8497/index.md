---
layout: "image"
title: "Roboterarm 1"
date: "2007-01-18T22:04:46"
picture: "roboterarm01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8497
- /detailsd8d1.html
imported:
- "2019"
_4images_image_id: "8497"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8497 -->
