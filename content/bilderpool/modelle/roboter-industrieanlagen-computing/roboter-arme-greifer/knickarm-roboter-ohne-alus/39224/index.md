---
layout: "image"
title: "Knickarm-Roboter komplett ausgefahren"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39224
- /details7927-2.html
imported:
- "2019"
_4images_image_id: "39224"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39224 -->
Ausgefahren kann der Knickarm-Roborer Punkte in einen Kreis von etwa einem Meter Durchmesser erreichen (Mittelpunkt des unteren Drehkranzes bis Mitte des Greifers: ca. 50 cm).