---
layout: "image"
title: "Greifer"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39228
- /details18be-2.html
imported:
- "2019"
_4images_image_id: "39228"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39228 -->
Der Greifer wird von einem XS-Motor im "Kniegelenk" betätigt; die Achse verläuft durch eine Freilaufnabe.