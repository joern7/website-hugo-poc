---
layout: "comment"
hidden: true
title: "19384"
date: "2014-08-11T22:55:32"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo, 

die Konstruktion sieht, auch ohne die bei Mehrarm-Robotern oft verwendeten Aluprofile, sehr stabil aus.

Da sich Aluprofile oft als Zapfen-Killer herausstellen, sollte man dieser Konstruktion eine besondere Beachtung schenken.

Kompliment!

Gruß+Dank

Lurchi