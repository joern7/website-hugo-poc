---
layout: "comment"
hidden: true
title: "19388"
date: "2014-08-12T00:23:35"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Super Modell: Leichtbau und doch sehr stabil. Die Minimot zur drehen der Greifer ist sehr klug plaziert: so wenig wie möglich Masse am Ende eines Armes. Spitze!

Mfrgr Marten