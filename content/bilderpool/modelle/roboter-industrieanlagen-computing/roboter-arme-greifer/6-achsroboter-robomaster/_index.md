---
layout: "overview"
title: "6-Achsroboter (RoboMaster)"
date: 2020-02-22T08:03:09+01:00
legacy_id:
- /php/categories/1852
- /categories9d1c.html
- /categories563a.html
- /categories8da4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1852 --> 
Roboterarm mit sechs Freiheitsgeraden, der mit einer Poteniometereinheit ferngesteuert werden kann.