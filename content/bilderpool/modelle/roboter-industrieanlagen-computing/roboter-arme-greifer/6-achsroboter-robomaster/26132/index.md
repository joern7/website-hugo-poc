---
layout: "image"
title: "Potentiometereinheit.2"
date: "2010-01-24T19:16:54"
picture: "DSCN55382.jpg"
weight: "12"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Potentiometereinheit"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26132
- /detailsb341-2.html
imported:
- "2019"
_4images_image_id: "26132"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T19:16:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26132 -->
Hier kann man die Potentiometereinheit noch einmal genauerbetrachten, obwohl es nur ein Nachbild vom eigentlichem Roboterarm ist, ist sie über 20cm groß