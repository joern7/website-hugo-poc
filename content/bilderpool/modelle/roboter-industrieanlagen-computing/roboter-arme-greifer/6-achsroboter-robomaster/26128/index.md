---
layout: "image"
title: "3.Achse"
date: "2010-01-24T18:15:18"
picture: "DSCN55342.jpg"
weight: "8"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Motorenkopplung"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26128
- /details11c5.html
imported:
- "2019"
_4images_image_id: "26128"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T18:15:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26128 -->
hier kann man erkennen, dass ich die 3.Achse mit zwei Encodermotoren steuere, dazu hatte ich zwei Gründe, zu einen weil sie die richtige Übersetzung haben, zum anderen aus Platzgründen, weil ich sie dann nicht mechanisch koppeln muss, was wiederum viel Wärme erzeugen würde.