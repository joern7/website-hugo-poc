---
layout: "comment"
hidden: true
title: "10597"
date: "2010-01-24T20:29:17"
uploadBy:
- "manuMFfilms"
license: "unknown"
imported:
- "2019"
---
Hi,

sehr schönes Modell, vorallem die Potentiometereinheit ist gut ausgedacht.
Die Webcam gefällt mir auch sehr gut :)

...20 Meter Kabel, das geht ja noch, ich habe bei meinem Roboter schon fast 100 Meter verbaut :)

Viele Grüße
manu