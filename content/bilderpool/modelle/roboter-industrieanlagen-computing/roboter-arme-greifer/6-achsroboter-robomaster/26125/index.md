---
layout: "image"
title: "Rückschlaggummi"
date: "2010-01-24T17:42:19"
picture: "DSCN55312.jpg"
weight: "5"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["rückschlagfeder", "Gummiband"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26125
- /details5267.html
imported:
- "2019"
_4images_image_id: "26125"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T17:42:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26125 -->
Um nicht nicht so schwere Gegengewichte besorgen zu mussen befestigte ich Fliesenlegergummis an einem Stab zwischen der 2.Achse. Insgesamt kostete mich das Verknoten der Gummis sicher 5 Stunden, wenn nicht sogar noch mehr, da ich zu anfang andere gummis verwendet habe, die jedoch nicht so elastisch waren. Üprigens, die Gummis sind zusammen 20 Meter lang!