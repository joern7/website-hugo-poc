---
layout: "image"
title: "Frontansicht"
date: "2010-01-24T18:15:18"
picture: "DSCN55352.jpg"
weight: "9"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Gegengewicht"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26129
- /details8c50.html
imported:
- "2019"
_4images_image_id: "26129"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T18:15:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26129 -->
Hier werden noch einmal die Ausmaße klar, wenn man sich ansieht, wie hoch die 2.Achse über dem Boden ist, dies ist notwendig damit die Gegengewichte hindurch schwenken können. Die Gegengewichte sind üprigens 1,5 Kilogramm schwer und bestehen aus Plastikgeheusen in dennen kleine Zinkplatten sind. Ich bin jedoch auf der Suche nach schwereren Gegenständen, bin jedoch noch nicht fündig geworden. Sowieso plane ich den Roboter durch ein paar Aluminiumproviele zusätzlich zu stabilisieren.