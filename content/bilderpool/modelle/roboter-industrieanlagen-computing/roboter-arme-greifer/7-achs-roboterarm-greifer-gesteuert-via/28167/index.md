---
layout: "image"
title: "Buzzer"
date: "2010-09-18T13:36:53"
picture: "achsroboterarmgreifergesteuertviawebcam05.jpg"
weight: "5"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28167
- /details8507.html
imported:
- "2019"
_4images_image_id: "28167"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28167 -->
Der selbstgebaute "Buzzer" dient zum Starten bzw. Abbrechen des Programms