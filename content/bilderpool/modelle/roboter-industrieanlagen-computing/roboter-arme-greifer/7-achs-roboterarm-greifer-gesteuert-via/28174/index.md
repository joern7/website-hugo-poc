---
layout: "image"
title: "Gegengewicht"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam12.jpg"
weight: "12"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28174
- /detailsd162-2.html
imported:
- "2019"
_4images_image_id: "28174"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28174 -->
Damit der Roboter im Gleichgewicht bleibt, hat er ein ein-  und ausfahrbares Gegengewicht. Vielleicht schaffe ich es noch bis zur Convention, dieses "schöner" zu verpacken...