---
layout: "image"
title: "Webcam"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam17.jpg"
weight: "17"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28179
- /details4ac3.html
imported:
- "2019"
_4images_image_id: "28179"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28179 -->
Hier ist die Webcam und deren Befestigung zu erkennen.