---
layout: "image"
title: "Arbeitsfläche"
date: "2010-09-18T13:36:52"
picture: "achsroboterarmgreifergesteuertviawebcam02.jpg"
weight: "2"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28164
- /details4a4b.html
imported:
- "2019"
_4images_image_id: "28164"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28164 -->
Der "Tracking-Bereich" oder einfach die Arbeitsfläche des Roboters ist hier zu sehen. Die grünen Eckpunkte markieren die Arbeitsfläche, sodass mein selbstgeschriebenes Java-Programm alles richtig erkennt...
Auch die Tonne ist oben grün gekennzeichnet, das dient auch zur besseren Erkennbarkeit