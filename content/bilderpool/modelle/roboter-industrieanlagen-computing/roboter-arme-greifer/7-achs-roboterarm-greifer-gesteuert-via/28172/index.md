---
layout: "image"
title: "Schienen"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam10.jpg"
weight: "10"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28172
- /detailsa9e1-2.html
imported:
- "2019"
_4images_image_id: "28172"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28172 -->
Hier sind die Fahrschienen zu sehen. Außerdem sind die Leitungen für Motoren, Sensoren und Luft erkennbar.