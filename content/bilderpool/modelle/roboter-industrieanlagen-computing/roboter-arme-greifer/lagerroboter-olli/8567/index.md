---
layout: "image"
title: "Lagerroboter"
date: "2007-01-20T16:46:18"
picture: "DSCI0057.jpg"
weight: "13"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/8567
- /details7a06.html
imported:
- "2019"
_4images_image_id: "8567"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8567 -->
Von hinten.