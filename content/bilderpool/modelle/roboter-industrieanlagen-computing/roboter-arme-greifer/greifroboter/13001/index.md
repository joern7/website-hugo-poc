---
layout: "image"
title: "Greifroboter"
date: "2007-12-04T16:58:10"
picture: "greifer4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13001
- /details156a.html
imported:
- "2019"
_4images_image_id: "13001"
_4images_cat_id: "1178"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13001 -->
