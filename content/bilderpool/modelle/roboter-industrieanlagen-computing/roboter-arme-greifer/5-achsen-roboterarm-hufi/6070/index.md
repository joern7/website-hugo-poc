---
layout: "image"
title: "5 Achser Interface"
date: "2006-04-11T22:26:23"
picture: "CTRL.jpg"
weight: "8"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6070
- /details6a16-2.html
imported:
- "2019"
_4images_image_id: "6070"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-04-11T22:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6070 -->
CPU  68HC11F1 128K Flash 32K RAM mit 5 Achsen am Limit bei der Encodererfassung.
Erweiterungprint (oben) ist zwar kaskadierbar hilft aber nix wenn die Rechenleistung fehlt, muss das Ganze demnächst auf 2 CPU´s aufteilen.