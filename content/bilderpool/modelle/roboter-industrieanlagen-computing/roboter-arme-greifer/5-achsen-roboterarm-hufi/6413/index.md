---
layout: "image"
title: "6AX Achsenlagerung und Antrieb horizontal"
date: "2006-06-02T22:22:52"
picture: "0116.jpg"
weight: "24"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6413
- /detailsef05.html
imported:
- "2019"
_4images_image_id: "6413"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-06-02T22:22:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6413 -->
