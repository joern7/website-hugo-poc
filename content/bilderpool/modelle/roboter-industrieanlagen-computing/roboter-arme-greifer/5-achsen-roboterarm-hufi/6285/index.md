---
layout: "image"
title: "6AX-Alu Drehkranz"
date: "2006-05-29T00:33:38"
picture: "0000.jpg"
weight: "10"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6285
- /details61b6.html
imported:
- "2019"
_4images_image_id: "6285"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-05-29T00:33:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6285 -->
