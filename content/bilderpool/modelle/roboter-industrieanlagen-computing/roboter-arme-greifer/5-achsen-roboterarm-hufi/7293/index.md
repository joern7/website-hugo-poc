---
layout: "image"
title: "Elektronische Geschwüre"
date: "2006-10-31T19:17:48"
picture: "Geschwuer.jpg"
weight: "34"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7293
- /detailsca5e-2.html
imported:
- "2019"
_4images_image_id: "7293"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-10-31T19:17:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7293 -->
Bislang konnte ich nur den Strom der Achsenmotoren steuern, dies führt leider zu sehr rechenintensiven Steuertasks um halbwegs konstante Achsengeschwindigkeiten zu fahren.
Anhand dieses Geschwür´s auf den PBL3717 IC´s am Motorencontroller wird es möglich die Motoren mit steuerbarer Spannung zu versorgen, zusätzlich kann der Strom mit 2 Bit in verschieden Stufen begrenzt werden.