---
layout: "image"
title: "6AX V2 Erster Zusammenbau"
date: "2006-08-13T00:09:36"
picture: "DSC03511.jpg"
weight: "28"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6677
- /detailse23d.html
imported:
- "2019"
_4images_image_id: "6677"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-08-13T00:09:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6677 -->
Die Montage der Positionsencoder und Endsensoren (Schalter) im Bereich des Greifers gestaltet sich aufgrund der knappen Platzverhältnisse eher schwierig.