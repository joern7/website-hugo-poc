---
layout: "image"
title: "6AX Sein Hirn"
date: "2006-11-09T23:40:41"
picture: "DSC03546.jpg"
weight: "38"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7443
- /details2749-2.html
imported:
- "2019"
_4images_image_id: "7443"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-11-09T23:40:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7443 -->
Das ist der Rechner der letztendlich die Steuerung des Robos übernimmt, der PC setzt lediglich per Serielle eine oder mehrere Zielpositionen auf verschiedenen Achsen ab und bekommt pro Achse erst wieder eine Rückmeldung wenn das Ziel erreicht ist (oder ein Fehler auftritt)
Während eine oder mehrere Achsen noch laufen können laufend weiter Ziele gesetzt werden.