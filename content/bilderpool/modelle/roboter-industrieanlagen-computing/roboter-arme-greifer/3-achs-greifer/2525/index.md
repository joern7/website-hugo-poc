---
layout: "image"
title: "3-Achs-Greifzange"
date: "2004-06-13T17:20:44"
picture: "Greifzange_3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2525
- /details7a81.html
imported:
- "2019"
_4images_image_id: "2525"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2525 -->
