---
layout: "image"
title: "Schwenkmodul"
date: "2004-06-13T18:31:26"
picture: "Schwenkmodul__1.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2532
- /details7982.html
imported:
- "2019"
_4images_image_id: "2532"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T18:31:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2532 -->
Dieses tolle, kleine Schwenkmodul hab ich
von ebay. Auf der 4mm Achse hab ich ein
Vierkant-Teil mit 4mm Klemmpassung an-
gebracht, der Vierkant passt in die Aus-
Fräsung des Baustein 15 mit ansenkung
und stellt so eine Formschlüssige Verbindung her. Leider entwickelt es erst
ab 2,5 bar akzeptable Kräfte.