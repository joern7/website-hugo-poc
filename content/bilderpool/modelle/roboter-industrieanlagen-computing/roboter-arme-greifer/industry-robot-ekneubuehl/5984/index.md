---
layout: "image"
title: "Bild8"
date: "2006-03-28T14:54:55"
picture: "S2400004.jpg"
weight: "8"
konstrukteure: 
- "erkn"
fotografen:
- "erkn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ekneubuehl"
license: "unknown"
legacy_id:
- /php/details/5984
- /detailse32a-3.html
imported:
- "2019"
_4images_image_id: "5984"
_4images_cat_id: "540"
_4images_user_id: "428"
_4images_image_date: "2006-03-28T14:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5984 -->
