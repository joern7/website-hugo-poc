---
layout: "image"
title: "Achse 4 Antrieb"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24307
- /details27b7.html
imported:
- "2019"
_4images_image_id: "24307"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24307 -->
zwischen motor und Achse befindet sich eine Schnecke, damit der Motor durch das Gewicht eines PM der getragen wird, nicht zusätzlich belastet wird.