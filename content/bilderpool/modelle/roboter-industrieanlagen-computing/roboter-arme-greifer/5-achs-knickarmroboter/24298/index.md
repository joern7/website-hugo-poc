---
layout: "image"
title: "Antrieb Achse 2"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24298
- /detailsce74.html
imported:
- "2019"
_4images_image_id: "24298"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24298 -->
Ein PM reicht für den Antrieb. Es könnte auch ein schnellerer sein, da der Motor nicht durch den Arm bellastet wird.