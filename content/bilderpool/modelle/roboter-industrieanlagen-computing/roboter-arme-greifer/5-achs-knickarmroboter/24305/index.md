---
layout: "image"
title: "Achse 4/5 Motoren"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24305
- /details52b8.html
imported:
- "2019"
_4images_image_id: "24305"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24305 -->
Die Motoren für Achse 4 und 5