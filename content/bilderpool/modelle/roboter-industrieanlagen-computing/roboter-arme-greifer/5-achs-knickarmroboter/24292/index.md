---
layout: "image"
title: "Achse 1/2"
date: "2009-06-10T14:57:06"
picture: "knickarmroboter02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24292
- /details1adb.html
imported:
- "2019"
_4images_image_id: "24292"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24292 -->
Der Drehkranz besteht aus 2 Statik-ringen und einem Rollenwagen dazwischen. Die Rollen fahren auf der Außenkante des Statikdrehkranzes, weil dieser sich sonst einbiegen würde. 
Der Drehkranz wird von einem 50:1 PM angetrieben, ein 8:1 PM wäre auch möglich, aber viel zu schnell
Die 2. Achse wird durch U-Träger in ihrer Position gehalten. Die Gewichte wiegen etwa 4-5 kg. Auch hier wäre ein schnellerer Motor möglich, die Seite mit dem Greifer und die Seite mit dem Gewicht stehen im Gleichgewicht zu einander,