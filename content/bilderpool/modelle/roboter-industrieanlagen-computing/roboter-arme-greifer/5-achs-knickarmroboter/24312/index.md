---
layout: "image"
title: "Greifer"
date: "2009-06-10T14:57:08"
picture: "knickarmroboter22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24312
- /details5d68-3.html
imported:
- "2019"
_4images_image_id: "24312"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:08"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24312 -->
Hier sieht man den Greifer und seine Dreh- und Kippachse. Ein Powermotor lässt sich problemlos halten und bewegen.