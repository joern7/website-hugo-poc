---
layout: "image"
title: "Kardangelenke"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24306
- /details9088-2.html
imported:
- "2019"
_4images_image_id: "24306"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24306 -->
