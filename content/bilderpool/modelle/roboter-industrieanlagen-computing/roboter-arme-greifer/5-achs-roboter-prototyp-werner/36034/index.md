---
layout: "image"
title: "Antrieb Hand"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner4.jpg"
weight: "4"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36034
- /detailsde78.html
imported:
- "2019"
_4images_image_id: "36034"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36034 -->
Die schwarzen Statikstreben hinten müsst ihr euch wegdenken, die entlasten die Kette im Ruhezustand