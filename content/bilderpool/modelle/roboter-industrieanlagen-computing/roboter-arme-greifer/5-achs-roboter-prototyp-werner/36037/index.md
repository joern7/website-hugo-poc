---
layout: "image"
title: "Unterarm"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner7.jpg"
weight: "7"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36037
- /detailsd248.html
imported:
- "2019"
_4images_image_id: "36037"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36037 -->
