---
layout: "image"
title: "Delta-Roboter Version 1 Detail"
date: "2008-01-03T02:43:49"
picture: "delta-prototyp-3.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/13207
- /details9315.html
imported:
- "2019"
_4images_image_id: "13207"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:43:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13207 -->
Achsschenkel geben prima Kugelgelenke ab. Lagerung in den Oberarmen und an der Arbeitsplattform durch Gelenkwürfelklaue; Baustein 15 mit Bohrung würde anstoßen.