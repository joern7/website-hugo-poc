---
layout: "image"
title: "Delta Version 2 Ellbogen"
date: "2009-04-12T23:34:50"
picture: "Delta_Oberarm-003.jpg"
weight: "10"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23665
- /details34cd.html
imported:
- "2019"
_4images_image_id: "23665"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2009-04-12T23:34:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23665 -->
Hier ist ein Kugelgelenk verlangt, das aber völlig torsionssteif ist. Die beiden Baustein 15 mit Loch, verbunden durch eine Metallachse, sind die beste Lösung, die ich bisher gefunden habe. 

Die alte Seilwinde ist eine prima Möglichkeit, einen Baustein an eine Achse anzusetzen. Die hält auch perfekt fest und rutscht nicht. Die Achse dreht sich frei im Drehkranz. So entsteht das Kugelgelenk.

Anfangs hatte ich statt des Alu-Profils zwei Streben mit Strebenadaptern drangesetzt. Dann hätte ich wirklich schön das Kugelgelenk ersetzt und der Aufbau wäre leichter. Leider verwinden die Streben viel zu stark. Auf diese Weise geht es so gerade eben.