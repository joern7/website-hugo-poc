---
layout: "image"
title: "Delta-Roboter Version 1 Draufsicht"
date: "2008-01-03T02:42:23"
picture: "delta-prototyp-2.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/13206
- /details917f.html
imported:
- "2019"
_4images_image_id: "13206"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:42:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13206 -->
Drei Knickarme, Schulter angetrieben, Ellbogen antriebslos, Arme identisch, Winkel 60°.