---
layout: "image"
title: "01-Rollenkranz als Basis"
date: "2012-02-12T14:48:21"
picture: "01-Roboter.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/34138
- /detailsb6d0.html
imported:
- "2019"
_4images_image_id: "34138"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34138 -->
Irgendwann musste es ja passieren, dass ich mich des Themas Industrieroboter annehme. Ich versuche mal, die Erwartungen nicht zu enttäuschen.

Es geht los mit einem großen, zentral geführten Rollenkranz. Diese Bauart habe ich schon in anderen Projekten umgesetzt, so dass ich die Genauigkeit und Belastbarkeit dieser Konstruktion kenne. Sie hat sich bewährt und rollt dann einwandfrei, wenn man die Spalten der Grundbauplatte mit einer dünnen Pappe abdeckt.

Bei den Grundplatten neueren Typs ist zu beachten, dass diese nicht mehr vollflächig aufliegen. Entweder greift man für eine solide Basis zu Bauplatten älterer Bauart oder schneidet kurzerhand den umlaufenden und überstehenden Rand der Platte ab.