---
layout: "image"
title: "12-Präzisionsroboter II"
date: "2016-10-03T19:55:15"
picture: "12-Przisionsroboter_II.jpg"
weight: "13"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/44543
- /detailsa41e-2.html
imported:
- "2019"
_4images_image_id: "44543"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44543 -->
Auf der Convention 2016 war er dann endlich zu sehen. Hier im Bild der gesamte Aufbau mit Rechner und Stromversorgung.