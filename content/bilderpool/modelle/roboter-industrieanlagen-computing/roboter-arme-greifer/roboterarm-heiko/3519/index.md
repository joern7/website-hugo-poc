---
layout: "image"
title: "bewegungsraum 001"
date: "2005-01-04T18:55:18"
picture: "bewegungsraum_001.JPG"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3519
- /details82db-2.html
imported:
- "2019"
_4images_image_id: "3519"
_4images_cat_id: "186"
_4images_user_id: "5"
_4images_image_date: "2005-01-04T18:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3519 -->
