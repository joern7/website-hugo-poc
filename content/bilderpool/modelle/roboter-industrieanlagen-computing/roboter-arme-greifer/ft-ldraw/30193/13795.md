---
layout: "comment"
hidden: true
title: "13795"
date: "2011-03-04T19:39:51"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo,
saubere allseitige Körperkanten hell bei dunklen und dunkel bei hellen Teilen. Das ist eine Kommentierung wert ! Habe mir auch mal den Quelltext der *.zip unter *.pov angesehen. Wie wurden die Daten von den originalen? 3D-Dateien konvertiert? Manuell dürfte dieser Datenumfang ja kaum bewältigt worden sein. Vielleicht kannst du uns das (M.Peppler?) mal etwas etwas erhellen.
Gruß, Udo2