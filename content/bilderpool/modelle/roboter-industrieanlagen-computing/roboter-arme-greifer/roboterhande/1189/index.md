---
layout: "image"
title: "Detail der Hand/Finger"
date: "2003-06-20T06:11:02"
picture: "fthandfingdet2.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1189
- /detailse84e.html
imported:
- "2019"
_4images_image_id: "1189"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1189 -->
Detailaufnahme eines Fingers. Zum öffnen der Finger dienten damals Ringgummis. Die sind aber leider alle wg des Alters (10Jahre) gerissen :-)