---
layout: "image"
title: "Alles in Allem"
date: "2003-06-20T06:11:03"
picture: "fthandkompl.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1191
- /details7fc8-2.html
imported:
- "2019"
_4images_image_id: "1191"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1191 -->
Hier der Überblich über das von mir vor 10 Jahren ausgestellte JugendForscht Projekt "Der Handgreifliche Roboter". Damals erzielte ich damit den 2.Platz auf Regional-Ebene. Die Hand u. der Arm ließen sich entweder via Steuerpult bedienen oder mit - naja - SteuerHandschuhen. Zwar waren die Motoren damit nicht proportional, also winkelgleich der Handbewegung steuerbar, aber damals war ich auch erst 16 :-)