---
layout: "image"
title: "Obere Motorfunktion"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_072.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4648
- /details415b-2.html
imported:
- "2019"
_4images_image_id: "4648"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4648 -->
Hier ist der obere Motor mit der Schnecke die ein Zahnrad betreibt.