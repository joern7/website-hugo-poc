---
layout: "image"
title: "Details der ersten beiden Gabellichtschranken"
date: "2015-02-18T15:55:34"
picture: "IMG_0323.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Gabellichtschranke", "Impulszähler"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40570
- /details5663.html
imported:
- "2019"
_4images_image_id: "40570"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-02-18T15:55:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40570 -->
Die kleinen Boards mit Gabellichtschranke gibt es mittlerweile für 1 Euro inkl. Versand aus China...