---
layout: "comment"
hidden: true
title: "20456"
date: "2015-04-11T15:56:10"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Die Encoderscheibe hat 32 schwarze und 32 weiße Segmente je Umdrehung. Bei jedem wechsel, wird der Counter incrementiert. Dadurch erhält man 64 Zähler je Umdrehung.