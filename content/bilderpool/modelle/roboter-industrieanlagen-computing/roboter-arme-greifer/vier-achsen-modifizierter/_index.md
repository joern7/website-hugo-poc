---
layout: "overview"
title: "Auf vier Achsen modifizierter Trainingsroboter"
date: 2020-02-22T08:03:23+01:00
legacy_id:
- /php/categories/3041
- /categories5fdb.html
- /categories44d5.html
- /categories4cf4.html
- /categoriesc82f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3041 --> 
... 30572 aus dem Baukasten 36069 Profi Computing von 1991

Als ersten Industrieroboter habe ich den 30572 aus dem Baukasten 36069 Profi Computing von 1991 nachgebaut. Da ich so mit dem Modell nicht zufrieden war, habe ich ein paar Modifikationen vorgenommen:
- Pneumatik-Greifer statt Schneckenmutter
- stärkere M-Motoren statt S-Motoren
- stabilere Statik-Träger für den Arm
- Gabellichtschranken statt Impulszahnräder
- Erweiterung um eine vierte Achse, damit der Arbeitsbereich nicht auf eine Kreislinie beschränkt ist