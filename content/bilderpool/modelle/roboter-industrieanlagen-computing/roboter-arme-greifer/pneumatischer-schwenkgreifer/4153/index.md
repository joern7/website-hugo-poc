---
layout: "image"
title: "Greifzylinder von vorne"
date: "2005-05-17T15:25:57"
picture: "Pneumatischer-schwenkgreifer013.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4153
- /details4b5d-2.html
imported:
- "2019"
_4images_image_id: "4153"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-17T15:25:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4153 -->
