---
layout: "image"
title: "Geht auch mit Greifzange"
date: "2005-05-26T12:15:42"
picture: "Schwenkbare-pneumaticgreifzange005.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4179
- /details4eb8.html
imported:
- "2019"
_4images_image_id: "4179"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-26T12:15:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4179 -->
Nun ist der Roboter mit einer schwenkenden Greifzange ausgerüstet.