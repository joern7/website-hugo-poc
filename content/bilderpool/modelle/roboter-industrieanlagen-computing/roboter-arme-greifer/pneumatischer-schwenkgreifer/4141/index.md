---
layout: "image"
title: "Pneumatischer-Schwenkgreifer"
date: "2005-05-14T13:08:54"
picture: "Pneumatischer-Schwenkgreifer010.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4141
- /detailsae2a-2.html
imported:
- "2019"
_4images_image_id: "4141"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-14T13:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4141 -->
Zwei Zylinder die Gegenstände hin- und herschwenken können (links Ansicht).