---
layout: "image"
title: "Pneumatischer-Schwenkgreifer"
date: "2005-05-08T17:05:06"
picture: "Pneumaischer-schwenkgreifer009.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4103
- /details3372.html
imported:
- "2019"
_4images_image_id: "4103"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-08T17:05:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4103 -->
Zwei Zylinder mit denen man Gegenstände hin- und herschwenken kann.