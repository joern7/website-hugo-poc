---
layout: "image"
title: "Samurai Roboter Arm"
date: "2015-11-15T19:54:42"
picture: "dirkw6.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42386
- /detailsd99a-2.html
imported:
- "2019"
_4images_image_id: "42386"
_4images_cat_id: "3154"
_4images_user_id: "2303"
_4images_image_date: "2015-11-15T19:54:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42386 -->
Hier seht ihr den Mini-Motor für den Armantrieb.