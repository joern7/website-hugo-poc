---
layout: "image"
title: "Koinzidenzentfernungsmessgerät KEM1"
date: "2018-06-06T07:19:40"
picture: "P1080321_fr_Bilderpool.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Telemeter", "Triangulation", "Koinzidenz", "Entfernungsmessgerät", "Koinzidenzentfernungsmesser", "coincidence", "rangefinder", "Optik", "optics"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47687
- /details36f1.html
imported:
- "2019"
_4images_image_id: "47687"
_4images_cat_id: "3517"
_4images_user_id: "1557"
_4images_image_date: "2018-06-06T07:19:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47687 -->
Ein optisches Entfernungsmessgerät, basierend auf dem Messprinzip der Triangulation. Messbereich: 2,5 m bis 100 m, Basislänge (optisch) 390 mm.

Links im Bild ist das Skalenrad zur Entfernungseinstellung per Drehspiegel zu sehen. Rechts ist der feste Umlenkspiegel montiert und in der Mitte steht eine Säule mit zwei weiteren festen  Umlenkspiegeln in denen die beiden Bildteile des Zielobjektes zur Deckung gebracht werden müssen. Sobald die Bildteile deckungsgleich sind, kann auf dem Skalenrad die Entfernung zum Zielobjekt abgelesen werden.

Das Stativ ist dem aus dem Profi-Optics nachempfunden und gestattet die Ausrichtung der Gerätschaft auf das Ziel - ein bißchen wacklig, aber stabil genug für die erste öffentliche Vorstellung in Karlsruhe.