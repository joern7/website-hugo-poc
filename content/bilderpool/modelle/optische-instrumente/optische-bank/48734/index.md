---
layout: "image"
title: "Die Elemente"
date: 2020-05-11T16:09:15+02:00
picture: "2020-01-06 Optische Bank04.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Elemente haben unten denselben Aufbau, der sie auf der Schiene hält, aber verschiebbar lässt.