---
layout: "image"
title: "Das Lampenmodul (2)"
date: 2020-05-11T16:09:12+02:00
picture: "2020-01-06 Optische Bank07.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die andere Seite der Lampe. Sie ist in zwei Richtungen quer zur Achse justierbar, um ihren Glühfaden in die Mitte der Linsen zu bringen.