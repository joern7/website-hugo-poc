---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-11T16:09:23+02:00
picture: "2020-01-06 Optische Bank01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz links sitzt die Abbild-Scheibe aus einem Ur-UT4-Kasten (oder auch aus einem l-e1). Auf 50cm-Metallachsen von Conrad ist die Lampe frei verschiebbar. Die Achsen selben bilden ihre Stromversorgung, sodass keine Kabel nachgezogen werden müssen. Die Lampe ist von einer Sichtblende verdeckt, damit ihr Licht nicht störend in die Augen fällt.