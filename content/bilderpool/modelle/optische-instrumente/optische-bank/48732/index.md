---
layout: "image"
title: "Das Lampenmodul (1)"
date: 2020-05-11T16:09:13+02:00
picture: "2020-01-06 Optische Bank06.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Strom geht von den Achsen über 31306 Federkontakte auf die Lampenbaugruppe. Hier ist der Blendschutz abgenommen.