---
layout: "image"
title: "Give-away 2"
date: "2015-04-09T21:06:06"
picture: "20150407_175836.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/40739
- /detailsfe83.html
imported:
- "2019"
_4images_image_id: "40739"
_4images_cat_id: "3061"
_4images_user_id: "328"
_4images_image_date: "2015-04-09T21:06:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40739 -->
Radlader mit fester Schaufel, 15 Teile