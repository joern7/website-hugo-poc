---
layout: "image"
title: "FTMann - Billard Spieler"
date: "2017-03-15T15:41:08"
picture: "billard.jpg"
weight: "44"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- /php/details/45523
- /detailsd515.html
imported:
- "2019"
_4images_image_id: "45523"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-03-15T15:41:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45523 -->
