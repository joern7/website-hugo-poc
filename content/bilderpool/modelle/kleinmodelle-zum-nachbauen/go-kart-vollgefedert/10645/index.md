---
layout: "image"
title: "Go-Kart 4"
date: "2007-05-31T21:33:02"
picture: "Go-Kart_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10645
- /detailsc797.html
imported:
- "2019"
_4images_image_id: "10645"
_4images_cat_id: "963"
_4images_user_id: "328"
_4images_image_date: "2007-05-31T21:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10645 -->
Der Unterboden mit den vier Federn.