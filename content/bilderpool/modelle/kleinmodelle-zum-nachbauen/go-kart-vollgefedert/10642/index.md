---
layout: "image"
title: "Go-Kart 1"
date: "2007-05-31T21:33:02"
picture: "Go-Kart_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10642
- /details44eb.html
imported:
- "2019"
_4images_image_id: "10642"
_4images_cat_id: "963"
_4images_user_id: "328"
_4images_image_date: "2007-05-31T21:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10642 -->
Inspiriert durch den Kasten "Go Cart" entstand mal nebenbei ein Go-Kart für Fortgeschrittene mit Einzelradaufhängung und vier einzeln gefederten Rädern.