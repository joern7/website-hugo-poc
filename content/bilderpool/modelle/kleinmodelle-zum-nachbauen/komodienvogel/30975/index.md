---
layout: "image"
title: "Flügelschlagmechanismus"
date: "2011-06-26T19:48:17"
picture: "komoedienvogel4.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30975
- /details694f.html
imported:
- "2019"
_4images_image_id: "30975"
_4images_cat_id: "2314"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30975 -->
Das Selbe, wenn die Flügel unten sind