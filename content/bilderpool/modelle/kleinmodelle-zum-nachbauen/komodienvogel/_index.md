---
layout: "overview"
title: "'Komödienvogel'"
date: 2020-02-22T08:33:09+01:00
legacy_id:
- /php/categories/2314
- /categoriesadbc.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2314 --> 
Da Vinci baute den orginalen "Komödienvogel". Dieser war für das Theater gedacht und konnte über ein Seil gezogen werden, wobei er den Flügelschlagt des Vogels nachahmte. Mein Nachbau wird allerdings über den Boden geschoben. Er ist sehr einfach gehalten und die Klassikteile lassen sich leicht ersetzen. Dann wünsche ich noch viel Spaß beim Nachbauen!
PS: Ich würde mich über Bilder von motorisierten oder gar mit Solar betriebenen "Komödienvögeln" freuen.