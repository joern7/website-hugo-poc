---
layout: "image"
title: "Ausleger"
date: 2020-05-12T15:56:36+02:00
picture: "2020-01-13 Miniatur-Turmdrehkran2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Ausleger kann gedreht werden, die Laufkatze kann man mit der Hand verschieben, es gibt einen Flaschenzug, und die hintere Metallachse lässt sich drehen, um das Tragseil hochzuziehen.