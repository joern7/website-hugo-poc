---
layout: "image"
title: "Drehmechanik"
date: 2020-05-12T15:56:34+02:00
picture: "2020-01-13 Miniatur-Turmdrehkran4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Zwei Riegelscheiben geben genau den richtigen Abstand, damit der BS15 mit Bohrung oben am Rastende anstößt.