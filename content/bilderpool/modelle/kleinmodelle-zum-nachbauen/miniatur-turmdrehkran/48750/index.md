---
layout: "image"
title: "Fahrgestell"
date: 2020-05-12T15:56:35+02:00
picture: "2020-01-13 Miniatur-Turmdrehkran3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Auf diesen Rollen kann der Kran fahren.