---
layout: "image"
title: "Zwei Geschwindigkeitsstufen"
date: "2015-07-04T16:11:48"
picture: "handventilatormitzweigeschwindigkeitsstufen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41377
- /details3e4c.html
imported:
- "2019"
_4images_image_id: "41377"
_4images_cat_id: "3093"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T16:11:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41377 -->
Ein klein wenig Elektronik realisiert zwei Geschwindigkeitsstufen: Schiebt man den Schalter nach rechts, läuft der Ventilator schnell, schiebt man ihn nach links, läuft er langsamer und leiser.