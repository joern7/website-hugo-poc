---
layout: "image"
title: "Konstruktion"
date: "2015-07-04T16:11:48"
picture: "handventilatormitzweigeschwindigkeitsstufen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41376
- /details69b6.html
imported:
- "2019"
_4images_image_id: "41376"
_4images_cat_id: "3093"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T16:11:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41376 -->
Der mechanische Aufbau ist einfach, aber stabil. Der BS30 hält den Motor mit einem Verbinder 15, steckt mit seinem Zapfen im Winkel  unten, und der obere Winkel hält den Motor über einen Federnocken. Drei BS7,5 sind mit zwei Federnocken am Motor befestigt und mit einem Verbinder 30 gesichert.