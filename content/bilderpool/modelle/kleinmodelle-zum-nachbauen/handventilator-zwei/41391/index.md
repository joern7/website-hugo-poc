---
layout: "image"
title: "Schaltbild"
date: "2015-07-04T22:12:17"
picture: "schaltplan1.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41391
- /detailsff19.html
imported:
- "2019"
_4images_image_id: "41391"
_4images_cat_id: "3093"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T22:12:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41391 -->
9-V-Akku und Schalter sitzen im gemeinsamen Batteriegehäuse. Die vier Dioden bilden einen normalen Graetz-Gleichrichter, der Motor dreht also unabhängig von der Stellung des Polwendeschalters immer gleichherum. Aber in einem Strang sitzt ein Widerstand (realisiert durch das ft-Lämpchen). In der "langsamen Polung" sind also Motor und Lämpchen in Serie geschaltet, am Motor fällt weniger Spannung ab als bei Volllast, und die Propellerschraube dreht sich langsamer.