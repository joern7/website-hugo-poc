---
layout: "image"
title: "Rechte Seite"
date: "2006-06-13T22:47:53"
picture: "oldtimer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6424
- /details693f.html
imported:
- "2019"
_4images_image_id: "6424"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6424 -->
Ich wollte nur ein einfaches Modell zum Ausprobieren der Fernbedienung.