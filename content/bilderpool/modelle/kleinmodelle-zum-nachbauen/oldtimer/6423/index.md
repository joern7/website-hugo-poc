---
layout: "image"
title: "Gesamtansicht"
date: "2006-06-13T22:47:52"
picture: "oldtimer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6423
- /details2aff.html
imported:
- "2019"
_4images_image_id: "6423"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6423 -->
Dieses Auto hatte ich mal gebaut, als ich mein IR Control Set neu bekam und natürlich mal ausprobieren wollte. Das Design ist einem Panzer nicht unähnlich, aber darauf kam es mir dabei nicht an.