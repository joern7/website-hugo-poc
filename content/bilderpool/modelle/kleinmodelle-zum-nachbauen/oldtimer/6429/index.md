---
layout: "image"
title: "Lenkung in Geradeausstellung"
date: "2006-06-13T22:47:53"
picture: "oldtimer7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6429
- /detailsd3cd.html
imported:
- "2019"
_4images_image_id: "6429"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6429 -->
Der Taster ist in dieser Stellung gedrückt.