---
layout: "image"
title: "Skateboarder01"
date: "2008-02-04T13:59:55"
picture: "skateboarder1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13517
- /details5c74.html
imported:
- "2019"
_4images_image_id: "13517"
_4images_cat_id: "1239"
_4images_user_id: "729"
_4images_image_date: "2008-02-04T13:59:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13517 -->
von vorne