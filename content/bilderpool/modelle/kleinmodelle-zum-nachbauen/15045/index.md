---
layout: "image"
title: "Fun Model"
date: "2008-08-13T18:52:43"
picture: "fun_erc_model.jpg"
weight: "8"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["spur", "gear", "model"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15045
- /details84ff.html
imported:
- "2019"
_4images_image_id: "15045"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-08-13T18:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15045 -->
This is a fun gear model I built out of our ERC kit.