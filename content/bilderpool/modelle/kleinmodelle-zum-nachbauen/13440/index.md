---
layout: "image"
title: "Enterprise"
date: "2008-01-27T17:30:26"
picture: "enterprise1.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13440
- /details0dfe.html
imported:
- "2019"
_4images_image_id: "13440"
_4images_cat_id: "335"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13440 -->
Raumschiff Enterprise. Und sie fliegt doch :-)