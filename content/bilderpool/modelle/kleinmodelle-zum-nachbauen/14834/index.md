---
layout: "image"
title: "einfaches_u_boot"
date: "2008-07-13T21:17:26"
picture: "p3240064.jpg"
weight: "5"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: ["U-Boot"]
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14834
- /detailsb593.html
imported:
- "2019"
_4images_image_id: "14834"
_4images_cat_id: "335"
_4images_user_id: "3"
_4images_image_date: "2008-07-13T21:17:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14834 -->
Kleines U-Boot.
Wollte ich schon immer mal hochladen.