---
layout: "image"
title: "Propeller Test"
date: "2009-04-18T07:37:20"
picture: "sm_prop_b.jpg"
weight: "24"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["propeller"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23741
- /details6e1d.html
imported:
- "2019"
_4images_image_id: "23741"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-18T07:37:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23741 -->
This is a test of the PCS BRAIN with a propeller.