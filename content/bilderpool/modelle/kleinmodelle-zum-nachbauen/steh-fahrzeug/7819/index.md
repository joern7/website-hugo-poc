---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-10T13:25:19"
picture: "magi14.jpg"
weight: "14"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7819
- /details651b.html
imported:
- "2019"
_4images_image_id: "7819"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7819 -->
