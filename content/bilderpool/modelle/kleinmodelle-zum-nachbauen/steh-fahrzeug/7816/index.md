---
layout: "image"
title: "Nochmal Stabilisierung"
date: "2006-12-10T13:25:19"
picture: "magi11.jpg"
weight: "11"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7816
- /details0f35.html
imported:
- "2019"
_4images_image_id: "7816"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7816 -->
Hier sollte man sehen das die Bauplette in der mitte ist