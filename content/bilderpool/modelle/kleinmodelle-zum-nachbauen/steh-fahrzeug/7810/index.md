---
layout: "image"
title: "Schrägste schräglage"
date: "2006-12-10T13:25:13"
picture: "magi05.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7810
- /details1039.html
imported:
- "2019"
_4images_image_id: "7810"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7810 -->
hier auf dem Foto ist das Modell abgestützt(sonst müsste ich sehr schnell reagieren mit Abdrücken...).