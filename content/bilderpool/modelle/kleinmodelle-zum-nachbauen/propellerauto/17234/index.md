---
layout: "image"
title: "im Betrieb"
date: "2009-02-01T19:35:38"
picture: "propellerauto6.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17234
- /detailsa064.html
imported:
- "2019"
_4images_image_id: "17234"
_4images_cat_id: "1545"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17234 -->
Hier sieht man das Auto in Betireb. Hinter dem Auto liegt der ft-Accu damit es nicht wegfährt.