---
layout: "image"
title: "Mini Hubschrauber"
date: "2018-07-26T07:16:40"
picture: "A_Gallery_9-800x465.jpg"
weight: "65"
konstrukteure: 
- "Coleen"
fotografen:
- "Robert"
keywords: ["Hubschrauber", "Mini"]
uploadBy: "klopfer"
license: "unknown"
legacy_id:
- /php/details/47761
- /detailsce1f.html
imported:
- "2019"
_4images_image_id: "47761"
_4images_cat_id: "335"
_4images_user_id: "2152"
_4images_image_date: "2018-07-26T07:16:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47761 -->
Erstes Mini Modell meiner Tochter (11 Jahre alt).
Sie ist stolz wie Oskar und ich wurde angehalten es hochzuladen ; -)
Mal sehen, was alles nächste kommt...