---
layout: "image"
title: "Der Chef"
date: "2008-02-01T17:16:23"
picture: "DSCN2086.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13489
- /details45d7.html
imported:
- "2019"
_4images_image_id: "13489"
_4images_cat_id: "1232"
_4images_user_id: "184"
_4images_image_date: "2008-02-01T17:16:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13489 -->
Er baut gerade einen Rahmen für ein neues Auto...