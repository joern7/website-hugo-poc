---
layout: "image"
title: "Hanoi 01"
date: "2011-12-15T10:39:27"
picture: "dietuermevonhanoi01.jpg"
weight: "1"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33676
- /details1c7b-2.html
imported:
- "2019"
_4images_image_id: "33676"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33676 -->
Das Handbuch ft# 38496 zu dem Baukasten "fischertechnik Computing" ft# 30554 aus dem Jahr 1984 enthielt auf den Seiten 17 bis 24 die Bauanleitung "Turm von Hanoi". Das obenstehende Bild zeigt die Seite 23 mit dem fertig aufgebauten "Turm von Hanoi". 

Der Aufbau und die Steuerung des Modells war ausgelegt auf das Parallel-Interface und einer Programmierung in Basic auf einem DOS-Computer. Das alles sind Technologien aus der "fernen Vergangenheit". Ein Nachbau mit neueren Technologien, wie ROBO-Interface und ROBOPro-Software, sowie einer moderneren, digitalen Positionierung mit Impulszählern statt mit analogen (etwas ungenauen) Dreh-Potentiometern, ist angesagt.

Das damalige Modell war so "klassisch", daß es im Wesentlichen erhalten bleiben soll. Um sich trotzdem von diesem "Turm von Hanoi" zu unterscheiden, wird die modernere Ausführung "Die Türme von Hanoi" genannt. Eine Umstellung auf modernere Technologie beinhaltet auch, daß die 6,8 Volt für Antriebe und Elektro-Magnet auf heutige 9 Volt heraufgesetzt werden. Das bedeutet, daß S-Motore und XS-Motore und neuere Elektro-Magnete eingesetzt werden müssen.

Zur Anwendung kommen auch ft-Teile aus dem neueren Programm. Lieferanten sind sowohl für neue, ungebrauchte ft-Teile und für ältere, gebrauchte ft-Teile allgemein bekannt. Zur Anwendung kommt ein einziges nicht-ft-Bauteil: Das "Hubgetriebe mit Impulsrad zur Positionsabfrage" aus dem Sonderzubehör von Andreas (TST) (im weiteren Verlauf kurz "Impuls-Hub" genannt). Zwei ft-Bauteile, Bauplatte (Abdeckplatte), ft# 32330, werden durch Absägen von 30x15 auf 15x15 verändert (verkürzt), wobei die entstehenden Teile (Hälften) mit der durchgehenden Nut von Interesse sind. In die Zuleitung der Verkabelung zum "Hubtisch" können Steckverbindungen zur leichteren Montage und Demontage des "Hubtisches" eingebaut werden. 

Die dazugehörige ROBOPro-Software "Die Türme von Hanoi", nebst verschiedenen Hilfsprogrammen, ist so modular wie möglich aufgebaut. Das erleichtert das Verständnis der Funktionsweise einer fremdentwickelten Software. Gleichzeitig kann die rekursive Lösung der gestellten Aufgabe leichter erkannt werden. Jedes Hinzufügen einer weiteren Scheibe, greift auf die schon vorhandenen Lösungen zurück.

Die "Die Türme von Hanoi" sind sehr gut als Projekt einer Projektwoche bei entsprechender Altersstufe einsetzbar.