---
layout: "image"
title: "Hanoi 07"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi07.jpg"
weight: "7"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33682
- /detailsf688.html
imported:
- "2019"
_4images_image_id: "33682"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33682 -->
Mit der Baustufe 6 beginnt auf dem Drehkranz der Aufbau des Trägers für den Hubtisch, des unteren End-Tasters I4 für den Hubtisch und des Schaltnockens für die End-Taster I2 und I3 des Drehkranzes.

Gleichzeitig wird ein Werkzeug aus einem Baustein 30 und einer Bauplatte 15x15 erstellt, mit dessen Hilfe eine Höheneinstellung des Schaltnockens vorgenommen werden kann. Der Schaltnocken soll über die beiden End-Taster I2 und I3 gleiten können und diese bestimmt betätigen können. Das Bild zeigt das Werkzeug unter dem Schaltnocken.