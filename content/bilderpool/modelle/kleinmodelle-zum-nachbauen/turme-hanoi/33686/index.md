---
layout: "image"
title: "Hanoi 11"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi11.jpg"
weight: "11"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33686
- /detailsce9f.html
imported:
- "2019"
_4images_image_id: "33686"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33686 -->
Mit der Baustufe 10 wird mit dem Aufbau des Hubtisches begonnen.

Der Hauptträger des Hubtisches (ft# 31229) wird an einem seiner Enden mit der Halteeinrichtung des Elktromagneten, der zugehörigen Kontrollleuchte und der Hateeinrichtung des Tasters I7 versehen (Baustufe 10a).

Danach wird der Hauptträger mit der Trageeinrichtung für die Hubgetriebe, deren Antriebsmotore und dem Taster I6 bestückt (Baustufe 10b).

Abschließend wird mit dem Aufbau einer Stütze für die Trageeinrichtung aus Baustufe 10b angefangen, die über die Bauteile ft# 37237, ft# 31060 und ft# 37468 mit dem Bauteil ft# 31982 aus der Baustufe 10b verbunden wird. Wichtig ist hier, daß die Stütze nach der Verbindung zur Baustufe 10b genau 90 mm vom noch freien Ende des 
Hauptträgers des Hubtisches entfernt ist (siehe Skizzierung in Baustufe 10c).