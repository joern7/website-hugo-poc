---
layout: "image"
title: "Hanoi 05"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi05.jpg"
weight: "5"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33680
- /detailsbb95.html
imported:
- "2019"
_4images_image_id: "33680"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33680 -->
Bevor der Drehkranz auf die 4 Bausteine 15 aufgeschoben wird, ist es sinnvoll die Verkabelung zu beginnen.

Die Taster I1 bis I3 und der S-Motor M1 werden schon mit der Verteilerplatte und der 28-poligen Buchsenleiste verdrahtet. Dabei kommt der schmale rote Stecker in die mittlere Buchse des Tasters I1. Die Verkabelung für die Taster I4, I5 sowie der Kabelbaum für die Taster I6 und I7 und die XS-Motore M2 und der Elektromagnet M3 des Hubtisches werden vorbereitet. Der Kabelbaum für den Hubtisch wird mit kurzen Stücken Schrumpfschlauch fixiert und ebenfalls schon an die Verteilerplatte und die 28-polige Buchsenplatte angeschlossen.

Die beiden Kabelstränge (I2, I3, I4 und I5 sowie I6, I7, M2 und M3) werden unter der Rastschnecke zwischen zwei Kabelschellen ebenfalls durch zwei Schrumpfschlauchstücke geführt. Das verhindert das Ergreifen von Kabeln durch die Rastschnecke.