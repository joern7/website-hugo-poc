---
layout: "image"
title: "Gesamtansicht mit Rampe"
date: "2007-10-08T14:12:08"
picture: "kleinerunimogfuerrccarset1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/12163
- /details33ad.html
imported:
- "2019"
_4images_image_id: "12163"
_4images_cat_id: "1088"
_4images_user_id: "445"
_4images_image_date: "2007-10-08T14:12:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12163 -->
All das kann man aus den Teilen bauen, nicht schlecht, was?