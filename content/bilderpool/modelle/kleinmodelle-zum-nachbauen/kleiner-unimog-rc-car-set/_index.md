---
layout: "overview"
title: "Kleiner Unimog für RC CAR SET"
date: 2020-02-22T08:32:49+01:00
legacy_id:
- /php/categories/1088
- /categoriescda2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1088 --> 
Dieser Unimog ist in der Verbindung mit dem RC CAR SET entstanden. Ich fand den dortigen Transporter nicht so toll, und habe selber einen gebaut. Das ist nun das Ergebnis. Ausserdem habe ich noch eine Beladerampe gebaut. Ausserdem kann man alles aus den Teilen vom RC CAR SET bauen.