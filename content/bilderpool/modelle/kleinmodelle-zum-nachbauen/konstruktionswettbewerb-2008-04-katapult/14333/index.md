---
layout: "image"
title: "Mirose Vorschlag C (1)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14333
- /details22d5.html
imported:
- "2019"
_4images_image_id: "14333"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14333 -->
Miroses Beschreibung zu diesem Vorschlag:

Wurfweite eines Bausteins 30 mit 180 cm. (15 Bauteile)