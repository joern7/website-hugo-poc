---
layout: "image"
title: "Speedy68 (6)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14341
- /details8451.html
imported:
- "2019"
_4images_image_id: "14341"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14341 -->
