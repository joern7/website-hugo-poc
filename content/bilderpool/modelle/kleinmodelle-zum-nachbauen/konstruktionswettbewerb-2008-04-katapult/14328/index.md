---
layout: "image"
title: "Mirose Vorschlag B (7)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14328
- /details7ba3-2.html
imported:
- "2019"
_4images_image_id: "14328"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14328 -->
