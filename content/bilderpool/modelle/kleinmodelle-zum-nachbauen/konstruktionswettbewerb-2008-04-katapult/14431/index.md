---
layout: "image"
title: "Details Fischertechnik Katapult"
date: "2008-04-30T21:15:35"
picture: "Poederoyen-koninginnedag_021.jpg"
weight: "49"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/14431
- /details2749.html
imported:
- "2019"
_4images_image_id: "14431"
_4images_cat_id: "1327"
_4images_user_id: "22"
_4images_image_date: "2008-04-30T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14431 -->
Details Fischertechnik Katapult