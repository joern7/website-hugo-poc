---
layout: "image"
title: "Mirose Vorschlag B (3)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14324
- /details93b4.html
imported:
- "2019"
_4images_image_id: "14324"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14324 -->
