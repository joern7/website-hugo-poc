---
layout: "image"
title: "Trebuchet - Ready to Launch"
date: "2008-04-27T01:12:36"
picture: "large_trebuchet_verb.jpg"
weight: "38"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["catapult", "trebuchet"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14385
- /detailsd939.html
imported:
- "2019"
_4images_image_id: "14385"
_4images_cat_id: "1327"
_4images_user_id: "585"
_4images_image_date: "2008-04-27T01:12:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14385 -->
This is another rendered image of my trebuchet. This type of trebuchet is a form of a free swinging weight trebuchet. 

***google translation: Dies ist ein weiterer gerendertes Bild meiner trebuchet. Diese Art von trebuchet ist eine Form eines freien schwingenden Gewicht trebuchet.