---
layout: "image"
title: "Überblick"
date: 2020-05-22T10:22:54+02:00
picture: "2020-04-19 Magnet-Schweberäder1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein XS-Motor treibt langsam ein Exzenter an, auf dem die beiden Stahlachsen also auf der linken Seite immer etwas angehoben und wieder abgesenkt werden.

Der Baustein 7,5 auf den Achsen ist optional und dämpft die ankommenden Räder, wenn sie nach links unten sausen. Wenn der Motor nämlich zu schnell läuft, kommen die Räder so schnell an, dass sie abfallen können.

Ein Video gibt's unter https://youtu.be/dpBmLwuS9z4