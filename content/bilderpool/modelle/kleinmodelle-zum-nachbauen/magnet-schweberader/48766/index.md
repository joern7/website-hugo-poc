---
layout: "image"
title: "Der Aufbau"
date: 2020-05-22T10:22:53+02:00
picture: "2020-04-19 Magnet-Schweberäder2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Trick ist, dass ganz rechts auf jeder Achse je ein Neodym-Magnet sitzt. Die beiden sind jeweils andersherum gepolt angebracht. Das Magnetfeld geht dann eben gerne durch die langen Achsen und durch die darunter hängende Querachse.