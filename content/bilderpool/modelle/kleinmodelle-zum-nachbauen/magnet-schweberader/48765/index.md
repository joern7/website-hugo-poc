---
layout: "image"
title: "Getriebe"
date: 2020-05-22T10:22:52+02:00
picture: "2020-04-19 Magnet-Schweberäder3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ein nicht ganz voll geladener 9V-Akku genügt vollauf, sodass gerade eine angenehme Drehgeschwindigkeit herauskommt.