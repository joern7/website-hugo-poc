---
layout: "overview"
title: "Magnet-Schweberäder"
date: 2020-05-22T10:22:52+02:00
---

Wenn man Neodym-Magnete mit Stahlachsen verbindet, kann man unterhalb (!) der Stahlachsen eine Querachse mit Rädern daran laufen lassen.

**Die Neodym-Magnete sind nicht für Kinder zu empfehlen.**
