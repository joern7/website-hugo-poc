---
layout: "image"
title: "Miniraupe Variante 2"
date: "2006-11-27T19:12:54"
picture: "Miniraupe04b.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7638
- /detailsef5f.html
imported:
- "2019"
_4images_image_id: "7638"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7638 -->
Oder eine kleine Arktisraupe.
Wollte eigentlich noch eine Winde anbauen, aber die sah ETWAS überdimensioniert aus...