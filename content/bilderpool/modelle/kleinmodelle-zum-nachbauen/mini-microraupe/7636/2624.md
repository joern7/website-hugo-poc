---
layout: "comment"
hidden: true
title: "2624"
date: "2007-03-08T18:41:20"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo,

da das Modell schon seit längerem wieder zerlegt ist, kann ich leider keine Fotos mehr davon machen. Die Z15 werden aber einfach nur aufgeschoben (wie eine Klemmbuchse) und halten sich selbst.

Gruß,
Franz