---
layout: "image"
title: "Das Auto"
date: "2006-12-03T19:30:05"
picture: "kleineslenkbaresauto1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7661
- /details8f6c.html
imported:
- "2019"
_4images_image_id: "7661"
_4images_cat_id: "723"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T19:30:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7661 -->
Es benötigt wenige Teile, allerdings eine Grundplatte, die wohl nicht mehr in neueren Kästen enthalten ist. Das Spielen damit hat mir aber immer viel Spaß gemacht, muss ich sagen.