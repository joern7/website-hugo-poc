---
layout: "image"
title: "Detail der Lenkung"
date: "2006-12-03T19:30:05"
picture: "kleineslenkbaresauto2.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7662
- /details0f24.html
imported:
- "2019"
_4images_image_id: "7662"
_4images_cat_id: "723"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T19:30:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7662 -->
Das ist eine stabile Ausführung. Im Originalkatalog kommt man auch mit ein paar Teilen weniger wunderbar aus.