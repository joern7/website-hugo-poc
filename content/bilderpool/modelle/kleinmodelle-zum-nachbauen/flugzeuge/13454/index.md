---
layout: "image"
title: "Flugzeug02"
date: "2008-01-27T20:10:16"
picture: "flugzeuge2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13454
- /details181a-2.html
imported:
- "2019"
_4images_image_id: "13454"
_4images_cat_id: "1226"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T20:10:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13454 -->
Gebaut von meinem Sohn Sebastian (5)