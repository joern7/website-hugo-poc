---
layout: "image"
title: "Raumschiff Spiral, Bauanleitung Teil 2"
date: "2017-12-05T19:06:52"
picture: "Raumschiff_Spiral_Bauanleitung_Teil_2.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46961
- /detailsfe0b.html
imported:
- "2019"
_4images_image_id: "46961"
_4images_cat_id: "1226"
_4images_user_id: "2635"
_4images_image_date: "2017-12-05T19:06:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46961 -->
Einen fröhlichen Nikolaustag wünsche ich euch allen!