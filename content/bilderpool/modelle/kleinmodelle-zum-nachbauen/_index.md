---
layout: "overview"
title: "Kleinmodelle zum Nachbauen"
date: 2020-02-22T08:32:31+01:00
legacy_id:
- /php/categories/335
- /categories45ea.html
- /categories3bac.html
- /categories66a1.html
- /categories7155.html
- /categories2ebe.html
- /categoriesa360.html
- /categories37c3.html
- /categoriesbc6b.html
- /categories07e0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=335 --> 
Die Kategorie für Modelle, die sich schnell nachbauen lassen und möglichst Material aus nur einem Baukasten benötigen.
Hier gilt: Je einfacher das Modell, desto besser!