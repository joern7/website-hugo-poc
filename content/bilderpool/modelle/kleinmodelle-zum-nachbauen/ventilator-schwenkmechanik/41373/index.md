---
layout: "image"
title: "Schwenkmechanik"
date: "2015-07-04T11:50:08"
picture: "ventilatormitschwenkmechanik3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41373
- /details11c8.html
imported:
- "2019"
_4images_image_id: "41373"
_4images_cat_id: "3092"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T11:50:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41373 -->
Die Drehscheibe wird vom Ventilator selbst über das für die ft-Selbstbaukompressoren verwendete Gummi angetrieben. Das ergibt schon mal eine angenehme Untersetzung. Von dort geht es per Schnecke aufs Z40 und auf das Exzenter.

Den Schwenkbereich kann man auf zwei Arten justieren: Grob durch Umstecken der senkrechten Metall-Drehachse nach vorne, in die Seite oder auf die Rückseite der BS30, und fein, wenn man denn will, durch verschieben des BS15 mit der feststehenden Achse des Exzenters.

Die Stromzuführung sollte halt geeignet davor bewahrt werden, in die Schnecke zu gelangen. Aber selbst wenn das mal passiert, ist das kein Problem, denn das Kabel landet dann einfach auf der Innenseite der Mechanik, weiter verheddert sich aber nichts.