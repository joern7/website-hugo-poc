---
layout: "image"
title: "Gummiantrieb"
date: "2015-07-04T11:50:08"
picture: "ventilatormitschwenkmechanik4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41374
- /detailsc066.html
imported:
- "2019"
_4images_image_id: "41374"
_4images_cat_id: "3092"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T11:50:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41374 -->
Hier sieht man vordringlich die stärkere Fixierung der Propellerschraube auf dem Motorgewinde mit einem Stück Papier, welches sich so aufs Angenehmste der Erstellung wenig weitsichtiger Schriftsätze entzieht, sowie den Abtrieb des ft-Gummis (rechts, auf dem Niveau etwas unterhalb der Mitte).