---
layout: "overview"
title: "Minimalistisches Servo-Fahrzeug"
date: 2020-02-22T08:33:10+01:00
legacy_id:
- /php/categories/2475
- /categories7ba7-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2475 --> 
Zum Ausprobieren der neuen IR-Servo-Fernsteuerung hatte ich ein kleines voll gefedertes Fahrwerk mit minimal wenigen Teilen gebaut.