---
layout: "image"
title: "Raumschiff"
date: "2009-04-15T06:35:05"
picture: "raumschiff1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23727
- /detailsd0b1.html
imported:
- "2019"
_4images_image_id: "23727"
_4images_cat_id: "1621"
_4images_user_id: "104"
_4images_image_date: "2009-04-15T06:35:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23727 -->
Das ist (sieht man ja auf den ersten Blick) ein Raumschiff.

Dieses Raumschiff war das erste fischertechnik-Modell, welches ich jemals sah. Und zwar bei einem Schulfreund meines Bruders, so um 1968 oder 1969 rum. Dieses Modell war es, welches mich mit dem gutartigen fischertechnik-Virus infizierte.