---
layout: "image"
title: "Mutter- und Tochterschiff"
date: "2009-04-15T06:35:06"
picture: "raumschiff3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23729
- /details4a53.html
imported:
- "2019"
_4images_image_id: "23729"
_4images_cat_id: "1621"
_4images_user_id: "104"
_4images_image_date: "2009-04-15T06:35:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23729 -->
Später ergänzte ich es noch durch dieses Beiboot, Lancet, oder wie man es gerade nennen mag.