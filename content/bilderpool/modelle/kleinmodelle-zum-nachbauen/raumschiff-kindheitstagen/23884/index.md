---
layout: "image"
title: "Small Spaceship"
date: "2009-05-05T20:32:46"
picture: "sm_spaceship3.jpg"
weight: "7"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23884
- /detailsed15.html
imported:
- "2019"
_4images_image_id: "23884"
_4images_cat_id: "1621"
_4images_user_id: "585"
_4images_image_date: "2009-05-05T20:32:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23884 -->
I based this on Stefan's description of the small spaceships that fit between the double legs of the mothership.