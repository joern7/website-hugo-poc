---
layout: "image"
title: "Unterseite"
date: "2009-04-15T06:35:06"
picture: "raumschiff2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23728
- /details2eba.html
imported:
- "2019"
_4images_image_id: "23728"
_4images_cat_id: "1621"
_4images_user_id: "104"
_4images_image_date: "2009-04-15T06:35:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23728 -->
Es besteht ausschließlich aus 32 Grundbausteinen 30.