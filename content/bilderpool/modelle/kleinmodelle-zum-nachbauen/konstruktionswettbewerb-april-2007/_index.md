---
layout: "overview"
title: "Konstruktionswettbewerb April 2007"
date: 2020-02-22T08:32:46+01:00
legacy_id:
- /php/categories/931
- /categories4ad4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=931 --> 
Der Sieger des 2. Wettbewerbs, kehrblech, definierte die Aufgabe so:



Es geht darum ein Gerät zu bauen, das an einem senkrechten Seil hochkriechen kann und dabei einen Powermotor mitschleppt. Das Seil muss oben befestigt sein und unten lose herumhängen. Das Modell darf sich nur an dem Seil festhalten (z. B. keine Hubgetriebe und lange Zahnstange) und der Powermotor darf nicht als Antrieb benutzt werden. Er muss auch direkt mit dem Modell verbunden sein, also nicht erst ein zweites Seil vom Modell zum P-Motor. Natürlich gewinnt wieder derjenige mit den wenigsten Teilen. Alu-Profile dürfen nicht benutzt werden, große Platten zu verbieten(Grundplatte 1000 etc.) ist glaube ich nicht erforderlich, da man sie eigentlich gar nicht benutzen kann.