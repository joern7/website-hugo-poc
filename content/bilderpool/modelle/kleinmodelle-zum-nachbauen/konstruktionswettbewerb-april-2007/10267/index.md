---
layout: "image"
title: "Ma-gi-er (1)"
date: "2007-05-01T19:08:42"
picture: "wettbewerb1.jpg"
weight: "1"
konstrukteure: 
- "Ma-gi-er"
fotografen:
- "Ma-gi-er"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10267
- /detailsd89f.html
imported:
- "2019"
_4images_image_id: "10267"
_4images_cat_id: "931"
_4images_user_id: "104"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10267 -->
Ma-gi-er hat den einzigen Beitrag eingesandt und beschrieb ihn wie folgt:

Teile:

Gewicht:
1x Power Motor 7:1
1x Sternchenfaden
1x Scheibe
------------------
3 Teile

Modell:
1x S-Motor Grau
1x Getriebehalter Grau
1x Zahnrad 1 Schwarz
1x Zahnrad 2 Schwarz
1x M Achse mit Zahnrad
1x Spurkranz/Drehscheibe rot
1x Nabe Rot
1x Klemme für Nabe Rot
1x Rollenlager rot
1x Reed Kontakt Halter
1x Baustein 15 mit Loch dunkelrot
1x Winkel rot
1x Baustein 30 Schwarz
1x Statikträger 60 grau
2x Statikträger 30 gelb
2x Baustein 30 grau
-----------------------------------
18 Teile


Seil:
1x Nylonfaden
1x Schrank
--------------
2 Teile