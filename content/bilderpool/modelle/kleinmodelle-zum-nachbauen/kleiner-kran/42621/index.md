---
layout: "image"
title: "Selbstbau-Kurbel"
date: "2015-12-28T19:08:43"
picture: "kleinerkran3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42621
- /detailsf36e.html
imported:
- "2019"
_4images_image_id: "42621"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42621 -->
Die Kurbel besteht aus einer "Rastaufnahmeachse 22,5", einem Baustein 15x30x5 und einem Rastadapter. Oben sieht man die Sperrklinke für die Kranarm-Neigung.