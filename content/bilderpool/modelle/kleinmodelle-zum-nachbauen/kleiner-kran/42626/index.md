---
layout: "image"
title: "Kranarmspitze"
date: "2015-12-28T19:08:43"
picture: "kleinerkran8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42626
- /details06ea.html
imported:
- "2019"
_4images_image_id: "42626"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42626 -->
Die Winkelsteine sind welche 7,5° Winkel. Das Seit für die Kranarm-Neigung ist einfach zwischen die Teile eingeklemmt.