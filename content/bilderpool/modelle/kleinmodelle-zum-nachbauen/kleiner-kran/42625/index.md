---
layout: "image"
title: "Unterseite"
date: "2015-12-28T19:08:43"
picture: "kleinerkran7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42625
- /detailsffd7-2.html
imported:
- "2019"
_4images_image_id: "42625"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42625 -->
Die untere Nabe wird festgezogen.