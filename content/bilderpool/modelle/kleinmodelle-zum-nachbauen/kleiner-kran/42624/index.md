---
layout: "image"
title: "Aufhängung der Seiltrommel"
date: "2015-12-28T19:08:43"
picture: "kleinerkran6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42624
- /details92f9.html
imported:
- "2019"
_4images_image_id: "42624"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42624 -->
Die 30 mm breite Seiltrommel sitzt in ihrem Standardgehäuse, welches mit zwei Winkeln an den BS15 befestigt ist. Die Nabe hier ist nur lose zugedreht. Durch sie geht eine "V-Achse 34 Clipachse" (die mit dem Knubbel auf einer Seite).