---
layout: "image"
title: "Kranarm-Neigung"
date: "2015-12-28T19:08:43"
picture: "kleinerkran5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42623
- /detailsc015.html
imported:
- "2019"
_4images_image_id: "42623"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42623 -->
Das Seil für die Kranarm-Neigung ist einfach in einem Klemmring fixiert und wird von der linken Kurbel angetrieben. Die Sperrklinke verhindert, dass der Arm einfach wieder herunterfällt.