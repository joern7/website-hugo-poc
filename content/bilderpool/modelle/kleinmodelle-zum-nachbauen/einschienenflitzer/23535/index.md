---
layout: "image"
title: "Einschienenflitzer"
date: "2009-03-29T17:13:53"
picture: "einschienenflitzer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23535
- /detailsf3ca.html
imported:
- "2019"
_4images_image_id: "23535"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23535 -->
Weichen gibt's zwar noch nicht, aber das Teil ist einfach eine nette Spielerei. Der Aufwand für die Bahn selbst könnte geringer kaum sein: Einfach Statikträger hintereinander. Man kann Geraden und Kurven mit 30 und 60° verwenden. Ein Video folgt noch.