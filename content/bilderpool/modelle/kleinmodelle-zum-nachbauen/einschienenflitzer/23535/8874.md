---
layout: "comment"
hidden: true
title: "8874"
date: "2009-03-29T23:08:12"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Danke Thomas.

Ein Video hab ich an marmac gemailt, das muss noch freigeschaltet werden. Da sind noch zwei Verbesserungend drin: a) S-Motor anstatt MiniMot bringt deutlich höhere Geschwindigkeit, b) je 1 BS5 vor die ganz vorderen WS60 für die Schienenführung steigern die Kurvengeschwindigkeit.

Gruß,
Stefan