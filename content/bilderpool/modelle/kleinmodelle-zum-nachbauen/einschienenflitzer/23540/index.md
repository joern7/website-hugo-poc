---
layout: "image"
title: "Ansicht von hinten"
date: "2009-03-29T17:13:54"
picture: "einschienenflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23540
- /details591e.html
imported:
- "2019"
_4images_image_id: "23540"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23540 -->
Nur die zwei Statikstreben und der darauf montierte Akku verbinden Vorder- und Hinterteil.