---
layout: "image"
title: "Der Flitzer"
date: "2009-03-29T17:13:53"
picture: "einschienenflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23536
- /details4ffc.html
imported:
- "2019"
_4images_image_id: "23536"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23536 -->
Der Antrieb ist wiedermal etwas abenteuerlich, aber er tut.