---
layout: "image"
title: "Kreuz zum Umhängen"
date: "2010-12-08T17:15:10"
picture: "kreuz1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29438
- /details7241.html
imported:
- "2019"
_4images_image_id: "29438"
_4images_cat_id: "2140"
_4images_user_id: "1162"
_4images_image_date: "2010-12-08T17:15:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29438 -->
Hier ein Kreuz zum Umhängen, es besteht aus einem S-Riegel aus Statik Strebe 30 und 60.