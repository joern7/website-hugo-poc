---
layout: "comment"
hidden: true
title: "12916"
date: "2010-12-10T20:45:57"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Ein Verschlußriegel paßt hier nicht, da die Öffnungen der Streben (jeder Strebe übrigens) zu klein sind. Eine Strebe kann nur mit dem hinteren (schmalen) Teil des Verschlußriegels fixiert werden.

Gruß, Thomas