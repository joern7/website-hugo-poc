---
layout: "image"
title: "Automobile"
date: "2008-08-31T08:58:52"
picture: "ft_car.jpg"
weight: "10"
konstrukteure: 
- "Amelia Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["automobile"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15153
- /details2fc3.html
imported:
- "2019"
_4images_image_id: "15153"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-08-31T08:58:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15153 -->
This was a car that my daughter built out of the "Basic Cranes" kit.