---
layout: "image"
title: "MisterWho"
date: "2007-02-12T06:12:49"
picture: "konstruktionswettbewerb6.jpg"
weight: "6"
konstrukteure: 
- "MisterWho"
fotografen:
- "MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8978
- /details2288.html
imported:
- "2019"
_4images_image_id: "8978"
_4images_cat_id: "817"
_4images_user_id: "104"
_4images_image_date: "2007-02-12T06:12:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8978 -->
10 Teile mit Seil (nach steffalk's Zählung; eingereicht waren "9 oder 10 Bauteile, je nachdem was vom Motor und Getriebe zählt")