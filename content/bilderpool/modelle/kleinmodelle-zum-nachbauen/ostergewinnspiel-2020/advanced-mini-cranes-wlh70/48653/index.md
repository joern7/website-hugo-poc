---
layout: "image"
title: "Hafenkran an der Kaimauer 3"
date: 2020-05-04T20:59:01+02:00
picture: "20200430_162508.jpg"
weight: "3"
konstrukteure: 
- "Box 125/1 Advanced Mini Cranes"
fotografen:
- "Wilhelm"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hafenkran mit einem historischen Hafenkran im Hintergrund.