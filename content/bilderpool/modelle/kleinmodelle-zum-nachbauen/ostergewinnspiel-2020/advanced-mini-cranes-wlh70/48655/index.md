---
layout: "image"
title: "Hafenkran an der Kaimauer 1"
date: 2020-05-04T20:59:05+02:00
picture: "20200430_155826.jpg"
weight: "1"
konstrukteure: 
- "Box 125/1 Advanced Mini Cranes"
fotografen:
- "Wilhelm"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hafenkran mit Schrottmagnet