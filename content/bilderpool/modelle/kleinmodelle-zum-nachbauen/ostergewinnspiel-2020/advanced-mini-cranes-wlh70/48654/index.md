---
layout: "image"
title: "Hafenkran an der Kaimauer 2"
date: 2020-05-04T20:59:03+02:00
picture: "20200430_160950.jpg"
weight: "2"
konstrukteure: 
- "Box 125/1 Advanced Mini Cranes"
fotografen:
- "Wilhelm"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hafenkran mit einem Schiff im Hintergrund