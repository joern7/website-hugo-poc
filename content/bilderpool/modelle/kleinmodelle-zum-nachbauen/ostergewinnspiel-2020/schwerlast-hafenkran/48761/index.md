---
layout: "image"
title: "Rastmimik"
date: 2020-05-14T17:57:29+02:00
picture: "Rastmimik.jpg"
weight: "4"
konstrukteure: 
- "Matthias Dettmer (fitenerd)"
fotografen:
- "Marie Cecile Dettmer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Rastmimik - einfach hin-und herschieben. Nach innen: gelöst, nach außen: fest