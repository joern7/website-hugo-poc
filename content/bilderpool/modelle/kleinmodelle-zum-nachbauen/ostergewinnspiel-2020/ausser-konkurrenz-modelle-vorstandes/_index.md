---
layout: "overview"
title: "Außer Konkurrenz: Modelle des Vorstandes
"
date: 2020-05-14T17:57:03+02:00
---

Auch von Mitgliedern des Vorstandes des ftc Modellbau e.V. bzw. von ihren Angehörigen wurden Modelle aus dem  
Community-Kasten vorgestellt. Diese haben selbstverständlich nicht am Gewinnspiel teilgenommen, sind aber jetzt hier zu sehen.
