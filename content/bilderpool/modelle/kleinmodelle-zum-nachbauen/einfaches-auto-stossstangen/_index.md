---
layout: "overview"
title: "Einfaches Auto mit Stoßstangen"
date: 2020-02-22T08:33:28+01:00
legacy_id:
- /php/categories/3538
- /categories2d92.html
- /categories5829.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3538 --> 
Kinder, die uns besuchten, legten fest: Es muss ein Auto mit Dach sein! Die Stoßstangen kamen dazu.