---
layout: "image"
title: "Dachkonstruktion"
date: "2018-10-14T22:59:17"
picture: "einfachesautomitstossstangen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48217
- /details3238.html
imported:
- "2019"
_4images_image_id: "48217"
_4images_cat_id: "3538"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:59:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48217 -->
Die Winkelsteine 30° vorne kommen in die zweite Reihe des U-Trägers, die am Ende in die hinterste.