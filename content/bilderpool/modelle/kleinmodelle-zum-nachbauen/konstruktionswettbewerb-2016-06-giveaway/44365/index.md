---
layout: "image"
title: "Modell F - Roland Enzenhofer (4): Mini-Schraubzwinge"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44365
- /details29d3.html
imported:
- "2019"
_4images_image_id: "44365"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44365 -->
Ene Schraubzwinge gibt's auch noch.