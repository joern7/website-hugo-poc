---
layout: "image"
title: "Modell D - Roland Enzenhofer (2): Motorsegler"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44362
- /details5a5b.html
imported:
- "2019"
_4images_image_id: "44362"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44362 -->
Eine Variante davon ist dieser Motorsegler.