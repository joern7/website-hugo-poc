---
layout: "image"
title: "Modell D - Roland Enzenhofer (2): Motorsegler"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44363
- /details1118.html
imported:
- "2019"
_4images_image_id: "44363"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44363 -->
Hier in Großaufnahme