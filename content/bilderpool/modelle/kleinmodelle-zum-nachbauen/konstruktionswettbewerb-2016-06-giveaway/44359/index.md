---
layout: "image"
title: "Modell B - René Trapp: Kleinlaster"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44359
- /details7b9e-2.html
imported:
- "2019"
_4images_image_id: "44359"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44359 -->
René rechnet für den Laster mit Teilen von fischerfriendsman einen Preis von 3,31? aus:

- 4 Stück 36581 Rad 23mm, rot
- 4 Stück 36586 Radachse mit Platte (Achse seitlich = parallel)
- 1 Stück 36576 Grundplatte 90x45x5.5, rot
- 2 Stück 38474 Bauplatte 15x60 mit 4 Zapfen, gelb
- 1 Stück 38243 Bauplatte 15x45 mit 2 Zapfen, gelb
- 4 Stück 38423 Winkelstein 10x15x15
- 1 Stück 38420 Führerhaus 45, gelb, kein Loch
- 2 Stück 38240 Baustein V15 Eck