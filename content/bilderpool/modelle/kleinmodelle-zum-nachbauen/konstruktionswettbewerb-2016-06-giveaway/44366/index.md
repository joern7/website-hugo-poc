---
layout: "image"
title: "Modell F - Roland Enzenhofer (4): Mini-Schraubzwinge"
date: "2016-09-12T10:44:53"
picture: "giveawayfuermakerfaire09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44366
- /details2d86.html
imported:
- "2019"
_4images_image_id: "44366"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44366 -->
Sogar mit Anleitung.