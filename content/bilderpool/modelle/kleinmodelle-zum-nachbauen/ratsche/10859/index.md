---
layout: "image"
title: "Ratsche"
date: "2007-06-12T20:11:50"
picture: "ratsche3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10859
- /details8703.html
imported:
- "2019"
_4images_image_id: "10859"
_4images_cat_id: "981"
_4images_user_id: "557"
_4images_image_date: "2007-06-12T20:11:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10859 -->
2te, 1. kleiner bruder