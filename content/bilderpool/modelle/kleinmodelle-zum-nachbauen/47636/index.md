---
layout: "image"
title: "FT-Jungs fahren Draisine"
date: "2018-05-09T19:31:12"
picture: "draisine_ft2.jpg"
weight: "64"
konstrukteure: 
- "Max und Christian"
fotografen:
- "Fabian"
keywords: ["FT-Mann", "Tim", "Tom", "Draisine", "Frühling", "FT-Jungs"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/47636
- /detailsf33d.html
imported:
- "2019"
_4images_image_id: "47636"
_4images_cat_id: "335"
_4images_user_id: "2611"
_4images_image_date: "2018-05-09T19:31:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47636 -->
Endlich Frühling, nix wie raus aus der muffigen Spielzeugkiste und ab an die frische Luft!
Doch wohin wollen Tim und Tom?