---
layout: "image"
title: "Art-Work-Mann"
date: "2015-03-14T12:36:48"
picture: "IMG_0041_copy.jpg"
weight: "34"
konstrukteure: 
- "Hannes"
fotografen:
- "Jens"
keywords: ["Kunstwerk"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40665
- /details6ffd.html
imported:
- "2019"
_4images_image_id: "40665"
_4images_cat_id: "335"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40665 -->
Hannes - wird im April 4(!) hat mal "eben" diese Kunstfigur erschaffen .. und selbst auf die Radachsen aufgestellt - etwas wackelig und höchst bemerkenswert ;-)