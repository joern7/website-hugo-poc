---
layout: "image"
title: "Gesamtansicht"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41392
- /detailsb247.html
imported:
- "2019"
_4images_image_id: "41392"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41392 -->
Dieser Ventilator ist recht kompakt, bietet aber dennoch drei Geschwindigkeitsstufen. Durch die Platten unterhalb des Batteriegehäuses kann man ihn zum stationären Betrieb auch aufstellen. Möge er allen Bedürftigen helfen, bei unerträglichen Hitzekapriolen den klaren Kopf wieder zu erlangen.