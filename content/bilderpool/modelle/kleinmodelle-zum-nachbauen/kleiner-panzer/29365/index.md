---
layout: "image"
title: "Kleiner Panzer Seitenansicht"
date: "2010-11-25T17:33:13"
picture: "kleinerpanzer2.jpg"
weight: "2"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29365
- /details7dce.html
imported:
- "2019"
_4images_image_id: "29365"
_4images_cat_id: "2130"
_4images_user_id: "381"
_4images_image_date: "2010-11-25T17:33:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29365 -->
