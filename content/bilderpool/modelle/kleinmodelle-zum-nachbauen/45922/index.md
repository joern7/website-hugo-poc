---
layout: "image"
title: "FTMann  -   zweier ohne steuermann"
date: "2017-05-22T13:32:39"
picture: "ze-dvojec.jpg"
weight: "53"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- /php/details/45922
- /detailsa2a7.html
imported:
- "2019"
_4images_image_id: "45922"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-05-22T13:32:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45922 -->
- add two rowers to get  "Vierer" 
- add six rowers to get  "Achter"