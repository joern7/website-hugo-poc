---
layout: "image"
title: "Portalkran von oben"
date: "2007-06-02T21:36:02"
picture: "portalkran6.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10661
- /detailsa6e3.html
imported:
- "2019"
_4images_image_id: "10661"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10661 -->
Man siet das die Laufkatze eigentlich ziemlich breit ist, das macht aber nichts. Denn die Katze ist genau so, das der Hacken neben der Befestigung der Räder durchgeht.