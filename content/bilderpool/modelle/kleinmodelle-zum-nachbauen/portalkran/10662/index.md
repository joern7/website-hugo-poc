---
layout: "image"
title: "Laukatze von unten im Gerüst"
date: "2007-06-02T21:36:02"
picture: "portalkran7.jpg"
weight: "7"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10662
- /details78b1.html
imported:
- "2019"
_4images_image_id: "10662"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10662 -->
Das Seil wird auf der seite der Bauplatte 60x15.