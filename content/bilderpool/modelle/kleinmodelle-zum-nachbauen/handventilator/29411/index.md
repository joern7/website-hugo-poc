---
layout: "image"
title: "Handventilator 6"
date: "2010-12-04T13:42:08"
picture: "handventilator6.jpg"
weight: "6"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29411
- /detailsaa0a.html
imported:
- "2019"
_4images_image_id: "29411"
_4images_cat_id: "2137"
_4images_user_id: "1162"
_4images_image_date: "2010-12-04T13:42:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29411 -->
Man kommt auch gut mit den Fingern an den Schalter.