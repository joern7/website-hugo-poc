---
layout: "image"
title: "Handventilator 5"
date: "2010-12-04T13:42:08"
picture: "handventilator5.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29410
- /details1f89.html
imported:
- "2019"
_4images_image_id: "29410"
_4images_cat_id: "2137"
_4images_user_id: "1162"
_4images_image_date: "2010-12-04T13:42:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29410 -->
Ventilator in meiner Hand. Wie man sieht liegt er sehr gut in der Hand.