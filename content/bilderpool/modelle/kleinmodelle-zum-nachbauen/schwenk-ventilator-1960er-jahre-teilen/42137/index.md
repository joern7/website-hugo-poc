---
layout: "image"
title: "Schwenkantrieb"
date: "2015-10-25T17:52:24"
picture: "schwenkventilatorauserjahreteilen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42137
- /details7f81.html
imported:
- "2019"
_4images_image_id: "42137"
_4images_cat_id: "3139"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T17:52:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42137 -->
Die auf Achsen aufschraubbaren Schnecken gab es damals noch nicht. Also wurde ein Getriebehalter mit Schnecke vom Ur-mot-1 eingebaut.