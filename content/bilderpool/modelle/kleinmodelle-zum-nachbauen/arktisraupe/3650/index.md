---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 1"
date: "2005-02-25T22:51:25"
picture: "Front.jpg"
weight: "1"
konstrukteure: 
- "Chevyfahrer"
fotografen:
- "Chevyfahrer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3650
- /detailsa731.html
imported:
- "2019"
_4images_image_id: "3650"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T22:51:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3650 -->
kleine Arktisraupe mit Wohnmodul u.Bergewinde