---
layout: "image"
title: "Portalschienen"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38573
- /details07ab.html
imported:
- "2019"
_4images_image_id: "38573"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38573 -->
Die schrägstehenden Streben sind X42,4. Sie sitzen auf einer Achse 30. Innen wird der Abstand mit zwei der kleinen Abstandshalter realisiert. Die Klemmringe dienen gleichzeitig als Endanschlag fürs Fahrwerk. Die senkrechten Streben sind I30. Damit die Räder des Kranfahrwerks sauber durchfahren können, sind sie mit 8-mm-Riegeln so befestigt, dass zwischen Strebe und Schiene Platz bleibt.