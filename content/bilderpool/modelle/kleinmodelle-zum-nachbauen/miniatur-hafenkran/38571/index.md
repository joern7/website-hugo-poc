---
layout: "image"
title: "Gesamtansicht 2"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38571
- /details8ce5.html
imported:
- "2019"
_4images_image_id: "38571"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38571 -->
Das Modell ist handbetrieben, aber voll funktionsfähig. Das Fahrgestell kann auf den Portalschienen fahren. Der Kran kann sich darauf drehen. Die Kranarm-Mechanik wirkt über einen doppelten Flaschenzug, damit sie langsam genug geht. Hier ist ungefähr die am weitesten eingezogene Stellung abgebildet.