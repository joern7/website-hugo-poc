---
layout: "image"
title: "Propeller Test"
date: "2009-04-18T07:37:20"
picture: "sm_prop_a.jpg"
weight: "23"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["propeller"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23740
- /detailsacc9.html
imported:
- "2019"
_4images_image_id: "23740"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-18T07:37:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23740 -->
Testing the PCS BRAIN with a propeller.