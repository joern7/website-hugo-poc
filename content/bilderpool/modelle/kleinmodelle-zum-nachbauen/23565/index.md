---
layout: "image"
title: "Kleine Eisenbahn"
date: "2009-03-31T16:23:55"
picture: "minieisenbahn1.jpg"
weight: "20"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/23565
- /detailsbfc6.html
imported:
- "2019"
_4images_image_id: "23565"
_4images_cat_id: "335"
_4images_user_id: "373"
_4images_image_date: "2009-03-31T16:23:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23565 -->
Das Lieblingsmodell meiner Kindheit - kaum war das ft draußen, konnte man sicher sein, dass diese Eisenbahn über den Teppich rollte.
Zum Aufbau brauche ich - glaube ich - Nichts schreiben, das Ganze ist so einfach gebaut, dass man es gut erkennt. Achja, das Ganze können übrigens auch 6-jährige wieder zusammensetzen wenn mal was kaputtgeht.