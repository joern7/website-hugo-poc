---
layout: "overview"
title: "Konstruktionswettbewerb Februar 2008"
date: 2020-02-22T08:32:53+01:00
legacy_id:
- /php/categories/1268
- /categories7f8a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1268 --> 
Anlässlich der Vorstellung des neuen Feuerwehr-Kastens von fischertechnik galt es, die darin verwendete Fertig-Leiter mit Standard-ft-Teilen nachzubauen. Die genauen Anforderungen waren:



1. Die Leiter muss mindestens 1 x ausziehbar sein.



2. Es dürfen nur Standard-fischertechnik-Teile verwendet werden.



3. Das Ausziehen kann auf beliebige Art und Weise geschehen (einschließlich manuell), sollte aber gut funktionieren, das heißt die Leiter soll mindestens von Hand ausziehbar, einrastbar (oben haltend) und wieder einfahrbar sein. Per Kurbel oder gar Motor ist natürlich noch besser.



4. Die Leiter sollte möglichst kompakt aus möglichst wenigen Teilen aufgebaut sein und möglichst gut aussehen. Eine Anforderung war also, innerhalb der Anforderung \"gut funktionierend\" möglichst kompakt zu sein. Dass man mit ft eine riesige Leiter bauen kann dürfte klar sein. Die Leiter sollte idealerweise auf das neue Feuerwehrfahrzeug passen, denn um eine Alternative zu dessen Leiter aus reinem ft geht es ja.