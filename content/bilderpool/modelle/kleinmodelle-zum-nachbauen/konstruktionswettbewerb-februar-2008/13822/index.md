---
layout: "image"
title: "Harald (2)"
date: "2008-03-03T12:39:06"
picture: "wettbewerbfebruar2.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/13822
- /details7eed.html
imported:
- "2019"
_4images_image_id: "13822"
_4images_cat_id: "1268"
_4images_user_id: "104"
_4images_image_date: "2008-03-03T12:39:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13822 -->
