---
layout: "image"
title: "Aufbau der Maus (2)"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45988
- /details19b9.html
imported:
- "2019"
_4images_image_id: "45988"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45988 -->
Der Motor ist ganz simpel von oben in den WS30° eingesteckt.