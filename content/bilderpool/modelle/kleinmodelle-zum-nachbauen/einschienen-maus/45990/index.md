---
layout: "image"
title: "Polwendeeinheit - Überblick"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45990
- /details1aeb.html
imported:
- "2019"
_4images_image_id: "45990"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45990 -->
Am Ende es Bahn"kreises" wird der Motor einfach umgepolt. Nach einer Baugruppe mit zwei Tastern und einem E-Tec ergab sich aber diese einfachere Variante: Der Polwendeschalter ist für an/aus. Links bzw. rechts stößt die Maus an und schiebt den Schieber herum.