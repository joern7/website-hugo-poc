---
layout: "overview"
title: "Einschienen-Maus"
date: 2020-02-22T08:33:26+01:00
legacy_id:
- /php/categories/3418
- /categoriesfab2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3418 --> 
Ein minimalistischer Antrieb auf einer minimalistischen Schiene.