---
layout: "image"
title: "Schraubenzieher Halter"
date: "2013-08-05T12:44:43"
picture: "halter.jpeg"
weight: "1"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: ["Schraubenzieher", "Bennik", "Schraubendreher", "Halter"]
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/37219
- /details2a48.html
imported:
- "2019"
_4images_image_id: "37219"
_4images_cat_id: "2770"
_4images_user_id: "1549"
_4images_image_date: "2013-08-05T12:44:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37219 -->
Halter des Schraubenziehers