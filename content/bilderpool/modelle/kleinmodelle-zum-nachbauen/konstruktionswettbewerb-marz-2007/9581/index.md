---
layout: "image"
title: "Jiriki 2"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb11.jpg"
weight: "11"
konstrukteure: 
- "Jiriki"
fotografen:
- "Jiriki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9581
- /details6af1.html
imported:
- "2019"
_4images_image_id: "9581"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9581 -->
