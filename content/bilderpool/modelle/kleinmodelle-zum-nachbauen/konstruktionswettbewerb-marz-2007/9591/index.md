---
layout: "image"
title: "Ma-gi-er 2"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb21.jpg"
weight: "21"
konstrukteure: 
- "Ma-gi-er"
fotografen:
- "Ma-gi-er"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9591
- /details6169.html
imported:
- "2019"
_4images_image_id: "9591"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9591 -->
