---
layout: "image"
title: "Joker 4"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb18.jpg"
weight: "18"
konstrukteure: 
- "Joker"
fotografen:
- "Joker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9588
- /detailsed18.html
imported:
- "2019"
_4images_image_id: "9588"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9588 -->
