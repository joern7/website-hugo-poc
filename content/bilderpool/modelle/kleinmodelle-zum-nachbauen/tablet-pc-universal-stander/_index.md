---
layout: "overview"
title: "Tablet-PC Universal-Ständer"
date: 2020-02-22T08:33:16+01:00
legacy_id:
- /php/categories/3243
- /categories5d19.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3243 --> 
Diese zusammenklappbare Vorrichtung dient dazu, dem geplagten Tablet-PC oder Groß-handy-Nutzer die Hände frei zu machen, um z.B. als digtaler Bilderrahmen, (e)Kochbuch oder Bauanleitungsanzeigegerät zu fungieren ;-)