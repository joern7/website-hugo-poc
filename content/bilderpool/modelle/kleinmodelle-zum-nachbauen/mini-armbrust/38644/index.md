---
layout: "image"
title: "Armbrust_010"
date: "2014-04-23T15:49:08"
picture: "010.jpg"
weight: "10"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
keywords: ["Armbrust", "Mini", "Kleinmodell", "Bogen", "Katapult", "Schießen"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38644
- /details2dd0-3.html
imported:
- "2019"
_4images_image_id: "38644"
_4images_cat_id: "2885"
_4images_user_id: "2167"
_4images_image_date: "2014-04-23T15:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38644 -->
Die Bolzen bestehen nur aus zwei Teilen: einer Rastkupplung und einer 30-Rastachse