---
layout: "image"
title: "Worm Gear Model"
date: "2008-08-07T06:47:11"
picture: "sm_worm_gear_model.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["worm", "gear", "model"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15038
- /details24c1.html
imported:
- "2019"
_4images_image_id: "15038"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-08-07T06:47:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15038 -->
This is a project taken from our curriculum we are taking to Mexico. It is estimated that 600 students will build these models.

***google translation: 
Dies ist ein Projekt aus unserem Lehrplan nehmen wir nach Mexiko. Es wird geschätzt, dass 600 Studenten werden diese Modelle bauen.