---
layout: "image"
title: "Draisine Mirose B3"
date: "2008-06-23T10:56:27"
picture: "draisine22.jpg"
weight: "26"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14781
- /detailsb095-2.html
imported:
- "2019"
_4images_image_id: "14781"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:27"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14781 -->
