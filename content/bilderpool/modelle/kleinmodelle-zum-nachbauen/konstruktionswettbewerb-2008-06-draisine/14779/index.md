---
layout: "image"
title: "Draisine Mirose B1"
date: "2008-06-23T10:56:27"
picture: "draisine20.jpg"
weight: "24"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14779
- /details3c23-2.html
imported:
- "2019"
_4images_image_id: "14779"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:27"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14779 -->
Entwurf B:
"Verschönerung" der Befestigung des Handhebels.