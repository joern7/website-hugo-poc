---
layout: "image"
title: "Kettenübertragung"
date: "2008-06-23T10:56:26"
picture: "draisine03.jpg"
weight: "7"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14762
- /detailsbced.html
imported:
- "2019"
_4images_image_id: "14762"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14762 -->
Hier sieht man die Kettenübertragung von der Vorderachse zur Hinterachse.