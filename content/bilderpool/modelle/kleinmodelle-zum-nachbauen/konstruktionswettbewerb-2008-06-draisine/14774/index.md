---
layout: "image"
title: "Draisine Mirose A4"
date: "2008-06-23T10:56:26"
picture: "draisine15.jpg"
weight: "19"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14774
- /details3749.html
imported:
- "2019"
_4images_image_id: "14774"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14774 -->
