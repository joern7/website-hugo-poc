---
layout: "image"
title: "Draisine Mirose A2"
date: "2008-06-23T10:56:26"
picture: "draisine13.jpg"
weight: "17"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14772
- /details8d69.html
imported:
- "2019"
_4images_image_id: "14772"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14772 -->
