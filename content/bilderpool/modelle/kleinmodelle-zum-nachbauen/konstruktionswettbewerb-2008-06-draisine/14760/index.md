---
layout: "image"
title: "Gesamtbild"
date: "2008-06-23T10:56:26"
picture: "draisine01.jpg"
weight: "5"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14760
- /detailsab1e.html
imported:
- "2019"
_4images_image_id: "14760"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14760 -->
Das ist meine Draisine!