---
layout: "comment"
hidden: true
title: "7476"
date: "2008-10-05T12:14:31"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Das Teil scheint noch in meiner Sammlung zu fehlen, was sofort geändert werden muß.
Kann sich der Klemmstift denn in dem Baustein 7,5 oder der Drehscheibe drehen?
Wenn er in beiden Bauteilen klemmt, dreht sich das Ganze ja nicht mehr, weil die 7,5er Bausteine ihre Ausrichtung meist beibehalten.