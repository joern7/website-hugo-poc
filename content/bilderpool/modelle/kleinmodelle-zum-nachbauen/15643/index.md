---
layout: "image"
title: "Sidekick"
date: "2008-09-27T21:15:11"
picture: "Sidekick.jpg"
weight: "11"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: ["Getriebe"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15643
- /details7418.html
imported:
- "2019"
_4images_image_id: "15643"
_4images_cat_id: "335"
_4images_user_id: "832"
_4images_image_date: "2008-09-27T21:15:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15643 -->
Zwei ungewöhnliche Getriebe für seitlich versetzte Antriebe.