---
layout: "image"
title: "Starwars im Schneegestöber."
date: "2017-12-10T19:28:36"
picture: "ft_AT-AT_Walker_mit_Schnee_800x600px.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46966
- /detailsa137.html
imported:
- "2019"
_4images_image_id: "46966"
_4images_cat_id: "3474"
_4images_user_id: "2635"
_4images_image_date: "2017-12-10T19:28:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46966 -->
AT-AT Walker (All Terrain Armored Transport).