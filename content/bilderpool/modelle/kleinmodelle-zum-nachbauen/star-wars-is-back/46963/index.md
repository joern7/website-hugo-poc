---
layout: "image"
title: "In the Woods"
date: "2017-12-08T20:57:47"
picture: "Zweibeiner_im_Wald_02_800x600px.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46963
- /details53bb.html
imported:
- "2019"
_4images_image_id: "46963"
_4images_cat_id: "3474"
_4images_user_id: "2635"
_4images_image_date: "2017-12-08T20:57:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46963 -->
Tarnen war gestern, heute erschrecken wir den Imperator mit schreiendem ROT.