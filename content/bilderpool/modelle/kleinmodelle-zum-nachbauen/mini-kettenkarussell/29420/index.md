---
layout: "image"
title: "Mini-Kettenkarussell"
date: "2010-12-05T16:35:22"
picture: "minikettenkarussell3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29420
- /details0460.html
imported:
- "2019"
_4images_image_id: "29420"
_4images_cat_id: "2139"
_4images_user_id: "1162"
_4images_image_date: "2010-12-05T16:35:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29420 -->
Die Lasche. Die sich bei der Drehung durch die Fliegkraft nach oben hebt.