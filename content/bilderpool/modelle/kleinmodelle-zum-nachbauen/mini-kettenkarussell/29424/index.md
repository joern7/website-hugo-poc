---
layout: "image"
title: "Mini-Kettenkarussell, Variante 2"
date: "2010-12-05T16:35:22"
picture: "minikettenkarussell7.jpg"
weight: "7"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29424
- /details4e9b.html
imported:
- "2019"
_4images_image_id: "29424"
_4images_cat_id: "2139"
_4images_user_id: "1162"
_4images_image_date: "2010-12-05T16:35:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29424 -->
Hier die zweite Variante, diesmal mit Kabelbindern anstatt der Laschen, man könnte die Kabelbinder auch durch eine Schnur austauschen.