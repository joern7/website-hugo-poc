---
layout: "image"
title: "Seitenansicht mit Verkleidung"
date: "2006-11-26T12:47:56"
picture: "Walze08b.jpg"
weight: "8"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7623
- /details84c6.html
imported:
- "2019"
_4images_image_id: "7623"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7623 -->
Zu guter letzt das ganze mit ein paar Bauplatten verkleidet.