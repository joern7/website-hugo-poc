---
layout: "image"
title: "Seitenansicht"
date: "2006-11-26T12:47:55"
picture: "Walze01b.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7616
- /details9f8a.html
imported:
- "2019"
_4images_image_id: "7616"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7616 -->
Hab heute mal so nebenbei eine kleine Straßenwalze gebaut. Mit etwas Geschick kann man die auch noch motorisieren und fernsteuern. Ich habe drauf geachtet, daß nur Teile drankamen, die eigentlich jeder daheim hat. Und falls nicht, kann man diese Teile auch leicht durch andere ersetzen.