---
layout: "image"
title: "Walze mit Kotflügeln (alternativ)"
date: "2006-11-26T23:08:57"
picture: "Walze11b.jpg"
weight: "11"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7629
- /details4108.html
imported:
- "2019"
_4images_image_id: "7629"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7629 -->
Für alle, die keine alten Baggerschaufeln besitzen, hier noch eine Alternative. Man kann natürlich auch andere Kotflügel nehmen, aber die hatte ich nicht.