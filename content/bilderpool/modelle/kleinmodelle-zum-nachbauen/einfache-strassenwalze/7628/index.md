---
layout: "image"
title: "Walze mit Kotflügeln Heckansicht"
date: "2006-11-26T23:08:57"
picture: "Walze10b.jpg"
weight: "10"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7628
- /detailsfe35-2.html
imported:
- "2019"
_4images_image_id: "7628"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7628 -->
Ansichti von hinten. Man beachte die "Rückleuchten" außen auf den Kotflügeln... ;o)