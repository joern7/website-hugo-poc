---
layout: "comment"
hidden: true
title: "16773"
date: "2012-04-22T13:00:21"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Markus,

Knobloch (http://www.knobloch-gmbh.de) hat den Verbinder unter der Art.-Nr. 75063

Andreas "TST" Tacke hat Verbinder in anderen Durchmessern. Die sind dann allerdings starr.

Viele Grüße, Andreas.