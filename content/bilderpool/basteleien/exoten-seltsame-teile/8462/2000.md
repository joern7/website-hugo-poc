---
layout: "comment"
hidden: true
title: "2000"
date: "2007-01-14T19:02:16"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tja, und Schaltgetriebe mit m0,5 laufen bei mir immer auf das Problem, dass man nie zwei Zahnräder gescheit (d.h. mit Drehmoment-Übertragung) auf einer Achse montieren kann. Alles was man so von den Rändelachsen oder aus Hub- und Stufengetriebe herausnehmen kann, hat entweder exotische Achsdurchmesser oder handelt sich um ein Einzelzahnrad, das wieder auf eine gerändelte Achse aufgesetzt werden muss. Oder man fängt mit Kleben oder Verstiften an.

Gruß,
Harald