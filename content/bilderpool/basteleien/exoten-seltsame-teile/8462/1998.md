---
layout: "comment"
hidden: true
title: "1998"
date: "2007-01-14T18:57:48"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich habs im ft-Museum gefunden (Suche nach "Klemmring"):

31020 Klemmring Z36 m0,5 schwarz Bi-Fi

Das "Bi-Fi" ist das entscheidende Stichwort.
---

Ab wann ist ein Getriebe ein Getriebe? Hier:
http://www.ftcommunity.de/details.php?image_id=1154
habe ich just diesen Klemmring verwendet, um das Schneckengetriebe weit genug von der restlichen Anordnung weg zu bekommen: Motor treibt Klemmring, Klemmring treibt Schnecke mit Lenkgestänge.

Gruß,
Harald