---
layout: "image"
title: "Servicestation"
date: "2006-09-13T22:02:56"
picture: "101MSDCF_008.jpg"
weight: "2"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6796
- /detailsd752.html
imported:
- "2019"
_4images_image_id: "6796"
_4images_cat_id: "654"
_4images_user_id: "473"
_4images_image_date: "2006-09-13T22:02:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6796 -->
Viele haben nachgefragt, wie so etwas früher aussah. Von den Kästen gab es 3 Stück. 1/Grundsteine 2/Motor und Getriebe 3/Elektronik und EM. Große Läden hatten einen Magazinschrank und ab 1980 etwa gab es nur noch 2 Servicekästen grau