---
layout: "image"
title: "Baustein 15 rot mit 2 Zapfen"
date: "2009-12-26T19:06:07"
picture: "neuebauteile1.jpg"
weight: "47"
konstrukteure: 
- "ft Werke"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25990
- /details9ac0.html
imported:
- "2019"
_4images_image_id: "25990"
_4images_cat_id: "782"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25990 -->
Gibt es den schon im aktuellen Programm??