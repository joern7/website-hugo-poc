---
layout: "image"
title: "graue Wanne"
date: "2008-01-03T17:18:15"
picture: "wassolldenndasseinselteneteile1.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "Pesche65"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13222
- /details76d2.html
imported:
- "2019"
_4images_image_id: "13222"
_4images_cat_id: "782"
_4images_user_id: "660"
_4images_image_date: "2008-01-03T17:18:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13222 -->
Hallo an alle, in meiner umfangreichen Sammlung entdeckte ich dieses Stück. Der ft-Konstrukteur hatte scheint einen Anfall an Kreativität verspürt zu haben. Hat jemand eine Ahnung, woher das Teil stammt? Danke für eine Antwort.