---
layout: "image"
title: "Statik-weiss-01.jpg"
date: "2010-05-13T22:02:20"
picture: "Statik-weiss-01.jpg"
weight: "49"
konstrukteure: 
- "hakra"
fotografen:
- "hakra"
keywords: ["Statikplatte", "Verkleidungsplatte"]
uploadBy: "hakra"
license: "unknown"
legacy_id:
- /php/details/27214
- /details33c2-2.html
imported:
- "2019"
_4images_image_id: "27214"
_4images_cat_id: "782"
_4images_user_id: "154"
_4images_image_date: "2010-05-13T22:02:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27214 -->
Statikplatte aus Kunststoff, weiß. In den Standardgrößen 180x90 mm und 90x90 mm. Die Platten sind auf einer CNC-Fräse erstellt.