---
layout: "image"
title: "Mein schlüsselschalter"
date: "2010-09-07T18:06:04"
picture: "schlsselschalter.jpg"
weight: "54"
konstrukteure: 
- "lars b."
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/28031
- /details950b.html
imported:
- "2019"
_4images_image_id: "28031"
_4images_cat_id: "782"
_4images_user_id: "1177"
_4images_image_date: "2010-09-07T18:06:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28031 -->
ein schlüsselschalter in einer ft platte
sehr praktisch für auto