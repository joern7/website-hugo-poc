---
layout: "image"
title: "unbekannte Verkleidungs-  bzw. Verbinderplatte"
date: "2008-12-14T18:49:24"
picture: "verbindungsplatten1.jpg"
weight: "42"
konstrukteure: 
- "ft"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16618
- /details4cab.html
imported:
- "2019"
_4images_image_id: "16618"
_4images_cat_id: "782"
_4images_user_id: "130"
_4images_image_date: "2008-12-14T18:49:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16618 -->
Hab heute in einer Kiste ft die ich auf einem Weihnachtsmarkt erstanden habe zwei seltsame Verkleidungs- bzw Verbindungsplatten gefunden. Die habe ich noch nie irgendwo gesehen. Sie haben auf beiden Seiten längliche Verbindungselemente. Auf einer Seite kurz und auf der anderen Lang und vier Stück.Hat die einer, bzw. kennt die einer von euch evtl.? Wo waren die drin und wofür waren die gedacht?