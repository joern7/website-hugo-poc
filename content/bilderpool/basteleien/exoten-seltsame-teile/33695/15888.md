---
layout: "comment"
hidden: true
title: "15888"
date: "2011-12-17T15:06:33"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo,
ich denke schon. Das aus dem Grund weil ich mehrere von diesen Federn habe.
Wenn sie gestreckt wären, wären sie nicht alle gleich lang. Das Strecken einer 20er Feder wird wohl nicht klappen. Sie geht ja in ihren Ausganszustand zurück (sie ist ja eine Feder), oder bricht bei dem Versuch. Habe ich früher schon einmal probiert.

Gruß ludger