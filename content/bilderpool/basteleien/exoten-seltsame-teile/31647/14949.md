---
layout: "comment"
hidden: true
title: "14949"
date: "2011-08-27T11:29:38"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@Udo2: Ja, zum Abdecken schon. Aber wozu sollte man das abdecken sollen? Wann stören mich die beiden Löcher, oder wann will ich, dass diese Seite rot ist? Da fiel mir gar kein Szenario dafür ein.

@Triceratops: Interessant! Ich finde die Stelle gar nicht. Ich habe in meinen PDFs für em 1 bis em 3 und ec 1 bis ec 3 sowie hobby-3 geguckt, aber nichts gefunden. Wo genau ist das bitte?

Gruß,
Stefan