---
layout: "image"
title: "Achse 30 gerändelt"
date: "2015-04-18T21:33:57"
picture: "achse1.jpg"
weight: "72"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40806
- /details1886.html
imported:
- "2019"
_4images_image_id: "40806"
_4images_cat_id: "782"
_4images_user_id: "936"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40806 -->
Von dieser Achse habe 9 Stück bei einem Konvolut gebrauchtem ft erhalten. Sie sieht 39007 ähnlich ist aber anstatt 45 mm nur 30 mm lang. Wurde hier ein Teil geschlachtet, bisher noch nicht gelistet oder gar nicht von ft. Siehe im Forum: http://forum.ftcommunity.de/viewtopic.php?f=4&t=2890