---
layout: "image"
title: "Kardanwürfel"
date: "2007-02-01T18:34:40"
picture: "DSCN1190.jpg"
weight: "34"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/8779
- /detailsdcc7.html
imported:
- "2019"
_4images_image_id: "8779"
_4images_cat_id: "782"
_4images_user_id: "184"
_4images_image_date: "2007-02-01T18:34:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8779 -->
zwei verschiedene Versionen