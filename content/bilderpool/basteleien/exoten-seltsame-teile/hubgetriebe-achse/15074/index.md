---
layout: "image"
title: "Hubgetriebe mit Achse"
date: "2008-08-23T15:07:23"
picture: "achshubgetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/15074
- /detailsfda6.html
imported:
- "2019"
_4images_image_id: "15074"
_4images_cat_id: "1373"
_4images_user_id: "558"
_4images_image_date: "2008-08-23T15:07:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15074 -->
Das Ding habe ich aus meiner Taktstraße ausgebaut.
http://www.ftcommunity.de/details.php?image_id=13688