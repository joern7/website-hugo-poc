---
layout: "comment"
hidden: true
title: "7317"
date: "2008-09-26T19:09:39"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Ergänzung:
Die Formteile der Werkzeuge sind in der Regel nicht gehärtet, da reicht auch bereits ein Handwerkzeug aus Metall aus, wenn es auf eine liegende Formhälfte des zu Reparatur, Änderung oder Revision geöffneten Werkzeuges fällt.