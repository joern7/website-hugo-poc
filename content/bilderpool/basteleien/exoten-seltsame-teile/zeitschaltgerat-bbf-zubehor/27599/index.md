---
layout: "image"
title: "BBF Steckplatte Vorderansicht"
date: "2010-06-27T19:34:32"
picture: "DSCN3519.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/27599
- /details3c1b.html
imported:
- "2019"
_4images_image_id: "27599"
_4images_cat_id: "1987"
_4images_user_id: "184"
_4images_image_date: "2010-06-27T19:34:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27599 -->
