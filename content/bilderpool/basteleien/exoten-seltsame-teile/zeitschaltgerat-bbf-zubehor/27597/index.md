---
layout: "image"
title: "BBF Stromversorgung Vorderansicht"
date: "2010-06-27T19:34:32"
picture: "DSCN3516.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/27597
- /details1076.html
imported:
- "2019"
_4images_image_id: "27597"
_4images_cat_id: "1987"
_4images_user_id: "184"
_4images_image_date: "2010-06-27T19:34:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27597 -->
