---
layout: "image"
title: "Antrieb Bosch Werbemodell"
date: "2017-02-16T18:06:43"
picture: "FT_seltenTeil.jpg"
weight: "77"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/45239
- /details1758.html
imported:
- "2019"
_4images_image_id: "45239"
_4images_cat_id: "782"
_4images_user_id: "10"
_4images_image_date: "2017-02-16T18:06:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45239 -->
mir fiel vor kurzem dieses seltsame Getriebe in die Hände. Es handelt sich wohl um das Antriebsgetriebe des Bosch Werbemodells.
Es sieht ähnlich aus, wie das Greifzangengetriebe