---
layout: "image"
title: "Schwungscheibe"
date: "2007-02-01T17:26:58"
picture: "DSCN1201.jpg"
weight: "31"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/8776
- /details77c4.html
imported:
- "2019"
_4images_image_id: "8776"
_4images_cat_id: "782"
_4images_user_id: "184"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8776 -->
mit zwei Rillen, habe ich so im Museum nicht gefunden