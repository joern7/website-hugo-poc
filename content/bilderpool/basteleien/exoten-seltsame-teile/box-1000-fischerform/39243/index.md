---
layout: "image"
title: "Box 1000"
date: "2014-08-17T21:31:25"
picture: "fischerform1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39243
- /details5e85.html
imported:
- "2019"
_4images_image_id: "39243"
_4images_cat_id: "2936"
_4images_user_id: "373"
_4images_image_date: "2014-08-17T21:31:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39243 -->
gehört zu diesem Foren-Thread: http://forum.ftcommunity.de/viewtopic.php?f=17&t=2526
Scheint eine Sonderproduktion gewesen zu sein. Ich besitze zwei Stück, habe ich mal direkt von ft bekommen, wo in irgendeiner anderen fischer-Abteilung irgendwelches Zeug drin gelagert wurde.
(Sorry für das schlechte Foto, nur schnell mitm Handy geknipst, für Doku-Zwecke sollte es reichen...)