---
layout: "image"
title: "Klemmhülsen ohne alles"
date: "2007-01-16T21:46:07"
picture: "Klemmhlsen.jpg"
weight: "27"
konstrukteure: 
- "ft"
fotografen:
- "Holger Bernhardt"
keywords: ["Klemmbuchsen", "Anfangszeit"]
uploadBy: "Svefisch"
license: "unknown"
legacy_id:
- /php/details/8489
- /details5f5a.html
imported:
- "2019"
_4images_image_id: "8489"
_4images_cat_id: "782"
_4images_user_id: "534"
_4images_image_date: "2007-01-16T21:46:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8489 -->
Bei diesen Klemmhülsen handelt es sich um Exemplare von 1967/1968. Sie weisen nur den Klemmschlitz in Längsrichtung auf und keinen Splat für irgendeinen Federring. Sie sind daher natürlich auch nicht sehr Widerstandsfähig auf den Achsen.