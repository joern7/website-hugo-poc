---
layout: "comment"
hidden: true
title: "3456"
date: "2007-06-12T16:44:26"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, so ähnlich dachte ich auch. Aber nach etwas rumfrickeln denke, dass man einen Satz aufstellen kann (nennen wir ihn mal die Haraldsche Vermutung):

"An einem Kegelzahnrad findet man ALLE Module - man muss nur nach der richtigen Stelle probieren." Da sind sicher noch ein paar Wenns und Abers dran, aber soweit hat mich die Theorie dann nicht interessiert.

Die Klemm-Kegelräder passen z.B. prima mit den Rast-Kegel zusammen. 


Gruß,
Harald