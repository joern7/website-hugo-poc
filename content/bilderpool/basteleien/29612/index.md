---
layout: "image"
title: "3D-Studie Hochseeschlepper, [1/3] Gesamtansicht"
date: "2011-01-05T19:20:45"
picture: "dstudiehochseeschlepper1.jpg"
weight: "22"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/29612
- /details686c.html
imported:
- "2019"
_4images_image_id: "29612"
_4images_cat_id: "463"
_4images_user_id: "723"
_4images_image_date: "2011-01-05T19:20:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29612 -->
3D-Modellstudie eines Hochseeschleppers der Bergungs- und Bugsierreedereien (noch ohne Deckplanken und Reling) :
Sicher ist es schwer möglich in der vom ft-Schiffsrumpf bestimmten Modellgröße ein Skalemodell eines bestimmten Vorbildschiffes zu modellieren. So wollte ich also zunächst mal geometrisch herausfinden, ob wenigstens ein Modell allgemein zu dieser Schiffsgattung möglich ist.
Dazu habe ich mich bei den Deckaufbauten von den Standardbauteilen mit Raster 15mm gelöst. Dadurch wurden immerhin folgende Deckaufbauten möglich: Mannschaftsräume mit je einer beweglichen Schotttür auf Backbord- und Steuerbordseite, Kommandobrücke, Ruderhaus, Signalmast mit Rundumradar, Maschinenhaus mit je einer beweglichen Schotttür auf Backbord- und Steuerbordseite und zwei belüftbaren Oberlichtern sowie zwei unabhängige Schlepptauantriebe mit Rollbock.
Das Modell ist für fünf Motoren gedacht, davon ins Maschinenhaus passend drei XM- bzw. Encoder-Motoren und auf Deck zwei XS-Motoren mit U-Getriebe.
Da im ft-Designer der ft-Schiffsrumpf noch nicht als 3D-Teil verfügbar ist, habe ich dazu aus meinem 3D-Archiv eine 1:1-Bauteilsimulation (329 Teile :o) ) von 2009 eingesetzt. Natürlich ist ein solches "Bauteil" für die Darstellung im ftD mit Bauteilkanten im Vergleich zur Darstellung ohne wenig geeignet. Anders wäre aber die 3D-Studie mit dem ftD zunächst nicht möglich gewesen.