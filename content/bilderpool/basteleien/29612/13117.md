---
layout: "comment"
hidden: true
title: "13117"
date: "2011-01-06T14:16:46"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo,
sensationelle Leistung, dieser Schiffsrumpf... und ein wunderbares Modell dazu! Deine Studie weckt Lust, den Fischertechnik-Bootsrumpf unten zu durchbohren. Das Original erlaubt ja quasi nur eine "Außenborder"-Motorisierung. Einzige Herausforderung: wie bekommt man die Löcher dicht?
Gruß, Dirk