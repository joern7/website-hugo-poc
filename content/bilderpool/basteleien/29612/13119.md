---
layout: "comment"
hidden: true
title: "13119"
date: "2011-01-06T15:15:40"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
nun der Schuld einer sensationellen Leistung :o) zum ft-Schiffsrumpf bin ich mir eigentlich nicht bewußt. Leider hängt der ftD mit den 3D-Teilen der Realität schon über 3 Jahre hinterher. Und wenn man ihn aktuell nutzen will, muß man sich halt solche Arbeit machen. Leider hat wie du ja weißt das manuelle Bewegen der 3D-Teile im ftD über den Koordinatenmanager nur eine beschränkte Genauigkeit mit linear 0,5mm und im Winkel mit 1°. Somit bleiben besonders am Bug viele Teile "hängen", die dann unnnötige Körperkanten zeigen ... Ich könnte ja dem Programmautor eine 3D-Zeichnung für das 3D-Teil machen. Bei dieser Geometrie aber tue ich mir das mit der mühsamen Vermessung nicht an, weil das 3D-File bei ft schon vorhanden ist.
Die Löcher im Rumpfboden erhalten eine beidseitig gekapselte Fettkammer ...
Gruß, Ingo