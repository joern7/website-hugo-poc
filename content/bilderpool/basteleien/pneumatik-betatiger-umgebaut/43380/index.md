---
layout: "image"
title: "defekter Pnaumatik-Betätiger 36075"
date: "2016-05-16T16:58:44"
picture: "pneumatikbetaetiger2.jpg"
weight: "2"
konstrukteure: 
- "JENS Lemkajen"
fotografen:
- "JENS Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43380
- /details7372.html
imported:
- "2019"
_4images_image_id: "43380"
_4images_cat_id: "3222"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43380 -->
hier einer der höchst seltenen begehrten Betätiger (leider defekt - der Balg fehlt) daneben liegt schon die ausgebaute Dichtung aus dem defekten Zylinder ..