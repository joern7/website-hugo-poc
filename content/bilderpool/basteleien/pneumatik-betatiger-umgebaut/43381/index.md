---
layout: "image"
title: "schwarze Dichtung des Innenteils"
date: "2016-05-16T16:58:44"
picture: "pneumatikbetaetiger3.jpg"
weight: "3"
konstrukteure: 
- "JENS Lemkajen"
fotografen:
- "JENS Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43381
- /detailsef9e.html
imported:
- "2019"
_4images_image_id: "43381"
_4images_cat_id: "3222"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43381 -->
hier die abgezogene schwarze Dichtung (hat ein FESTO-Prägung) - ist auf dem Foto nicht gut zu erkennen ..