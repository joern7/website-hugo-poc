---
layout: "image"
title: "Stangenverbinder II"
date: "2010-09-10T21:56:44"
picture: "stangenverbinder_montiert.jpg"
weight: "18"
konstrukteure: 
- "lars b."
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/28082
- /details93fc.html
imported:
- "2019"
_4images_image_id: "28082"
_4images_cat_id: "463"
_4images_user_id: "1177"
_4images_image_date: "2010-09-10T21:56:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28082 -->
und hier nochmal zusammenn gebaut
(man kann auch noch zahnräder zwischenlegen und fest machen)