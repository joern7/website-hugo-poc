---
layout: "image"
title: "Sim Fabrik"
date: "2011-03-19T10:31:50"
picture: "inderendphase01.jpg"
weight: "1"
konstrukteure: 
- "Michael Etz, zum großen Teil Staudinger"
fotografen:
- "Michael Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/30284
- /details57ef.html
imported:
- "2019"
_4images_image_id: "30284"
_4images_cat_id: "2251"
_4images_user_id: "473"
_4images_image_date: "2011-03-19T10:31:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30284 -->
Ich hab jetzt genug Material, um eine SIM-Fabrik (Simulations-Fabrik) nachzubauen. Man muß auch aufpassen, da man schnell an die Grenzen der Programmierung kommt. Zufällig wohnt ein Professor für Informatik im Nachbarhaus, der schon in den Startlöchern schabt. Ich versuche jetzt mit der Aufstellung eine gewisse Montage-, Sortier- und Ablagesystematik auszuprobieren. 

2 von den großen, doppelseitigen Hochregallagern (siehe Anfang der Bildreihe) werden je am Anfang und Ende stehen. Das erste ist das Rohmateriallager, das zweite wird das Endmateriallager zum auschecken. Je nach freier Steuerungskapzität kommt noch ein flächenübergreifender (ca 4qm) Portalkran über die Anlage.

Momentan wird die Anlage über 10qm groß werden. Mehr darf ich in der Wohnung nicht :(