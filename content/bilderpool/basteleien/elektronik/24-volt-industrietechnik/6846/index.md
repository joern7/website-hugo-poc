---
layout: "image"
title: "24V3"
date: "2006-09-16T23:12:56"
picture: "24V_Industrieanlage_003.jpg"
weight: "49"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6846
- /details51c2.html
imported:
- "2019"
_4images_image_id: "6846"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T23:12:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6846 -->
