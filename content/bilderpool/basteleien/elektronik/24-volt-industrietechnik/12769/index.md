---
layout: "image"
title: "HRL 2 Mein Wohnzimmer :-))"
date: "2007-11-18T19:14:58"
picture: "101MSDCF_002_4.jpg"
weight: "58"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12769
- /details2c0f.html
imported:
- "2019"
_4images_image_id: "12769"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2007-11-18T19:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12769 -->
