---
layout: "image"
title: "19"
date: "2006-09-16T16:53:06"
picture: "101MSDCF_019.jpg"
weight: "23"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6820
- /details87c3.html
imported:
- "2019"
_4images_image_id: "6820"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6820 -->
