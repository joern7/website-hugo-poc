---
layout: "image"
title: "8"
date: "2006-09-16T16:53:05"
picture: "101MSDCF_008.jpg"
weight: "12"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6809
- /details5072.html
imported:
- "2019"
_4images_image_id: "6809"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6809 -->
