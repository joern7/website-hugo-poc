---
layout: "image"
title: "Werk 2"
date: "2006-10-22T18:45:58"
picture: "061022_ft_alu_004.jpg"
weight: "56"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7211
- /detailsfa97.html
imported:
- "2019"
_4images_image_id: "7211"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-10-22T18:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7211 -->
