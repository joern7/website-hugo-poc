---
layout: "image"
title: "14"
date: "2006-09-16T16:53:06"
picture: "101MSDCF_014.jpg"
weight: "18"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6815
- /detailscdf6-2.html
imported:
- "2019"
_4images_image_id: "6815"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6815 -->
