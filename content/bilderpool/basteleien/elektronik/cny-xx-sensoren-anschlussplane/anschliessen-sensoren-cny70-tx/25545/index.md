---
layout: "image"
title: "CNY70 details"
date: "2009-10-12T00:50:41"
picture: "IMG_2132.jpg"
weight: "3"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["CNY70"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/25545
- /detailsa93c.html
imported:
- "2019"
_4images_image_id: "25545"
_4images_cat_id: "1793"
_4images_user_id: "385"
_4images_image_date: "2009-10-12T00:50:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25545 -->
Links: ft phototransistor
Mitte: Stromquelle 18 mA (LM317 als current regulator)
Rechts: TCST1103 sensor und CNY70 sensor mit wiederstand 10k.

Der Detector des TCST1103 kann angeschlossen werden wie der Phototransistor. Beim CNY70 geht das nicht. 

+9V --> collector des CNY70
emitter des CNY70 --> TX controller eingang UND mit R=10k nach GND.

Die Eingang soll Analog 10V sein.