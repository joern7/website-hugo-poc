---
layout: "image"
title: "Schaltplan"
date: "2011-03-26T21:21:04"
picture: "cnysensoren7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30320
- /details4aba.html
imported:
- "2019"
_4images_image_id: "30320"
_4images_cat_id: "2255"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:21:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30320 -->
Der CNY37 ist eine Miniatur-Lichtschranke, mit deren Hilfe man millimetergenau Impulse zählen und damit Positionen bestimmen kann.
Dazu verwendet man Transparentstreifen, die zuvor mit einem Laserdrucker mit schwarzen vertikalen Streifen bedruckt wurden.
Der CNY37 liefert bei den durchsichtigen Stellen eine 1 und bei den durch Streifen verdeckten Stellen eine 0.
Schaltplan zum Anschluss des CNY37 an ein Interface
Schaltung für das parallele Interface (von Cees Nobel):

(Dieser Schaltplan wurde von Joachim Jacobi (Jojo's Homepage) erstellt. Vielen Dank!)
Die abgeschrägte Seite befindet sich auf der Abbildung rechts.
Der Transistor ist ein "BC 547 C".
Unten ist ein Transparentstreifen zu sehen, der zusammen mit dem CNY37 verwendet wird.

Anschluss: (Wie beim CNY70):
? Roter Stecker 5V: Paralleles Interface: Durch verbundene Stiftleiste am 28-poligen Verbindungsstück Serielles Interface: Buchse an der Außenseite des Eingangs.
? Grüner Stecker 0V: - (Minus) Pol des Netzteils
? Stecker E dig.: Digitaleingang am Interface Beim seriellen Interface ist es die Buchse auf der Innenseite des gewünschten Eingangs.
Die im Schaltplan beschriebenen Widerstandswerte 15 kOhm und 470 Ohm liefern beim parallelen Interface sehr gute Ergebnisse.
Für das serielle Interface sind wegen der anderen Spannung (9V anstatt 5V) auch andere Widerstandswerte erforderlich:
Der Vorwiderstand der LED sollte von 470 Ohm auf 1 KOhm erhöht und der andere Widerstand herabgesetzt werden.
Leider habe ich noch keinen optimalen Widerstandswert dafür gefunden - je nach verwendetem Transparentstreifen ergaben sich Werte zwischen 3 und 15 KOhm.
Achtung:
Wie auch beim CNY70 gilt:
Die IR-Led des CNY37 nie ohne Vorwiderstand anschließen, Halbleiter können explodieren!