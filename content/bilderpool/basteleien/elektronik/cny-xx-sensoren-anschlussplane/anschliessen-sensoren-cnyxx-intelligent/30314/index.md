---
layout: "image"
title: "Schaltplan"
date: "2011-03-26T21:21:04"
picture: "cnysensoren1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30314
- /detailsc3a0-2.html
imported:
- "2019"
_4images_image_id: "30314"
_4images_cat_id: "2255"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:21:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30314 -->
Mit dem "Optoreflexkoppler CNY70", einer Miniatur-Reflexionslichtschranke kann man prüfen, ob ein Testobjekt hell oder dunkel ist.
Durch seine kleine Größe (6x6x5mm) passt er fast überall rein und lässt sich z.B. zur reibungs- und geräuschlosen Impulszählung verwenden, aber auch zum Einlesen von "Daten" auf Endlospapier o.ä.

Mit "Flach hinten" ist das abgeflachte, meist angewinkelte Beinchen gemeint (die Basis).
Der Transistor ist ein "BC 547 C".

Anschluss:
? Roter Stecker 5V Paralleles Interface: Durch verbundene Stiftleiste am 28-poligen Verbindungsstück
? Roter Stecker 9V Serielles Interface: Buchse an der Außenseite des Eingangs.
? Grüner Stecker 0V - (Minus) Pol des Netzteils
? Stecker E digital Digitaleingang am Interface Beim seriellen Interface ist es die Buchse auf der Innenseite des gewünschten Eingangs.
Wenn man den CNY70 so hinhält wie die Schaltung es zeigt (also Text nach links, Beinchen nach oben), dann sind die 2 Beinchen vorne der Anschluss der IR-LED des CNY70 und die beiden Beinchen hinten der Fototransistor.
Zum Testen des CNY70 kann man auch den FlipFlop-Baustein aus dem Profi Sensoric verwenden, dann reicht der 1kOhm-Vorwiderstand der IR-Led, der Fototransistor kann direkt an den Eingang des FlipFlops angeschlossen werden.
Für den Betrieb an einem Interface braucht man allerdings den zusätzlichen Transistor.
Die im Schaltplan beschriebenen Widerstandswerte 560 kOhm und 1 kOhm liefern beim parallelen Interface sehr gute Ergebnisse.
Den 1 kOhm-Widerstand der IR-Led kann man zur Vergrößerung der Reichweite herabsetzen.
Achtung: Nicht kleiner als 150 Ohm!
Für das serielle Interface sind wegen der anderen Spannung (9V anstatt 5V) auch andere Widerstandswerte erforderlich:
Anstatt dem 560 kOhm-Widerstand einen 100 kOhm-Widerstand verwenden!
Ansonsten kann dieselbe Schaltung verwendet werden.
Achtung:
Die IR-Led des CNY70 nie ohne Vorwiderstand anschließen, Halbleiter können explodieren!
(Und tun das auch meistens bei zu viel Strom...)