---
layout: "image"
title: "h4-MF im Design 'E-Tec'"
date: "2010-04-03T02:15:59"
picture: "h4-MF-Mini.jpg"
weight: "10"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/26868
- /details5b12.html
imported:
- "2019"
_4images_image_id: "26868"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-04-03T02:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26868 -->
Das Monoflop ist hier in einer Version mit
Mikroregler konfiguriert und bereits mit
Heißkleber im Minigehäuse eingefaßt.