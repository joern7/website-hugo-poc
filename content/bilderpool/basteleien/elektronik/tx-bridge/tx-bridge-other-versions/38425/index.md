---
layout: "image"
title: "5V Version"
date: "2014-03-03T11:03:35"
picture: "txbridgeotherversions1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38425
- /details7fa0.html
imported:
- "2019"
_4images_image_id: "38425"
_4images_cat_id: "2861"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38425 -->
This version on a single sided PCB runs on %V rather than 3.3V and is intended for steppermotor control via I2C