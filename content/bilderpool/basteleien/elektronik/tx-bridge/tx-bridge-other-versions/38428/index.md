---
layout: "image"
title: "Stepper motor controller"
date: "2014-03-03T11:03:35"
picture: "txbridgeotherversions4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38428
- /detailsea79.html
imported:
- "2019"
_4images_image_id: "38428"
_4images_cat_id: "2861"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38428 -->
I used the 5V bridge to control the AMIS30624 (TMC222) stepper motor controllers via I2C. Four connections go to the motor, two to the power (max. 29V) and one for an end-position switch