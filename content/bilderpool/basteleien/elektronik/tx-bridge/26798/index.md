---
layout: "image"
title: "test setup"
date: "2010-03-23T19:36:37"
picture: "tx02.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26798
- /details5a35.html
imported:
- "2019"
_4images_image_id: "26798"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26798 -->
Visible ate the master TX, the bridge and an old-style I/O Extension. Furthermore the ControlSet can send signals to RoboPro and RoboPro can send signals to the servo.