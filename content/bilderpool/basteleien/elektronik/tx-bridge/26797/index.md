---
layout: "image"
title: "RoboTxTest"
date: "2010-03-23T19:36:37"
picture: "tx01.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26797
- /details2903.html
imported:
- "2019"
_4images_image_id: "26797"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26797 -->
A real TX master with two emulated slaves (notice the name