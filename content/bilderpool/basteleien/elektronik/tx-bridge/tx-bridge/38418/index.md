---
layout: "image"
title: "The PCBs as they arrived from the Fab"
date: "2014-03-03T11:03:34"
picture: "txbridge1.jpg"
weight: "1"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38418
- /details7293.html
imported:
- "2019"
_4images_image_id: "38418"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38418 -->
The order costed $11.70 for the three PCBs incl. shipping and took about 3 weeks to arrive.