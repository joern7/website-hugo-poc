---
layout: "image"
title: "In the box"
date: "2014-03-03T11:03:35"
picture: "txbridge5.jpg"
weight: "5"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38422
- /details5c02.html
imported:
- "2019"
_4images_image_id: "38422"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38422 -->
With some persuasion it does fit but the lid is even tighter than the box