---
layout: "overview"
title: "TX-Bridge"
date: 2020-02-22T07:43:58+01:00
legacy_id:
- /php/categories/2860
- /categories46b9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2860 --> 
An ATMega based hardware interface between the ft TX controller and the old Robo Extension modules and other hardware