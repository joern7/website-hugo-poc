---
layout: "image"
title: "A PCB after filing off the stubs"
date: "2014-03-03T11:03:35"
picture: "txbridge2.jpg"
weight: "2"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38419
- /details6e05.html
imported:
- "2019"
_4images_image_id: "38419"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38419 -->
