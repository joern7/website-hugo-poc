---
layout: "comment"
hidden: true
title: "13675"
date: "2011-02-24T20:54:39"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Meine Variante hat lediglich zweimal je einen Umschaltkontakt. Allerdings sind die Relais mit Vorverstärker ausgestattet, so daß sie auch von Silberlingen und ICs direkt angesteuert werden können.

Natürlich steht mir kein aufwendiger "Maschinen- & Werkgeugpark" zur Verfügung und bin daher auf reine Handarbeit angewiesen. Funktionieren tun meine Module trotzdem.

Gruß, Thomas