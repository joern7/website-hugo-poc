---
layout: "image"
title: "Kabel 2"
date: "2012-10-09T21:02:19"
picture: "soundmodul06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35857
- /detailse593-2.html
imported:
- "2019"
_4images_image_id: "35857"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35857 -->
Kabel abschneiden

Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.