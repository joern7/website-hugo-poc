---
layout: "image"
title: "Ab isolieren"
date: "2012-10-09T21:02:19"
picture: "soundmodul08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35859
- /details9890.html
imported:
- "2019"
_4images_image_id: "35859"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35859 -->
Das Kabel mit einer automatischen Abisolierzange 2 cm ab isolieren oder mit einem Messer vorsichtig die äußere Isolierung (hier Schwarz) entfernen.
Und von der inneren Isolierung ca. 3mm entfernen


Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.