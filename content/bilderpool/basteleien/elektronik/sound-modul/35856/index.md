---
layout: "image"
title: "Kabel"
date: "2012-10-09T21:02:19"
picture: "soundmodul05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35856
- /detailsdd3c.html
imported:
- "2019"
_4images_image_id: "35856"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35856 -->
Beim Kabel eine geeignete Länge wählen Tipp:  min. 10 cm       max. 30 cm

Länge von der Buchse bis zur Abtrennstelle.

Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.