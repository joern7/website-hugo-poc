---
layout: "image"
title: "Soundmodul mit externem Verstärker"
date: "2012-10-09T21:02:19"
picture: "soundmodul01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35852
- /details66d3.html
imported:
- "2019"
_4images_image_id: "35852"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35852 -->
Ich habe den Lautsprecher des Soundmodules  entfernt und die Anschlüsse über ein Kabel nach außen geleitet!

Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!


Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.