---
layout: "image"
title: "The board in its final shape and size"
date: "2008-06-22T14:52:08"
picture: "sensorpack016-crop800.jpg"
weight: "3"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/14752
- /details8269.html
imported:
- "2019"
_4images_image_id: "14752"
_4images_cat_id: "1350"
_4images_user_id: "716"
_4images_image_date: "2008-06-22T14:52:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14752 -->
Almost all components, sockets for ATmega and MAX but no wiring yet