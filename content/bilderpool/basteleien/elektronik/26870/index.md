---
layout: "image"
title: "LED-Schaltprinzip für 'Silberlinge'"
date: "2010-04-03T02:16:00"
picture: "LED-Prinzip.jpg"
weight: "12"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/26870
- /detailsbf8e.html
imported:
- "2019"
_4images_image_id: "26870"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-04-03T02:16:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26870 -->
Diverse Versuche zeigen, daß sich diese Schaltung als optimal
für den Einsatz in "Silberling"-Nachbauten und anderen eignet.
Wichtig: Nur mit SUPERHELLEN LEDs kann der Vorwiderstand
ausreichend groß konfiguriert werden, so daß dieser Zweig der
Schaltung mit etwa 1,5 mA bei 9 V arbeitet und die betroffene
Transistorstufe so gut wie überhaupt nicht beeinflußt wird.