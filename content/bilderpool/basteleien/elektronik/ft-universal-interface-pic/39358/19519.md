---
layout: "comment"
hidden: true
title: "19519"
date: "2014-09-16T10:23:19"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Eine wunderschöne Lösung! Echte Feinarbeit! 

Sehe ich das richtig, dass Du den originalen Stecker für das Flachbandkabel zum Anschluss an den PC ausgelötet hast und neue Pins in das FT-Interface eingelötet hast um darauf das neue Board aufzulöten?