---
layout: "image"
title: "Smartphone GUI"
date: "2017-05-08T18:14:36"
picture: "wfpp5.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45803
- /details1752.html
imported:
- "2019"
_4images_image_id: "45803"
_4images_cat_id: "3404"
_4images_user_id: "2228"
_4images_image_date: "2017-05-08T18:14:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45803 -->
In Blynk habe ich ein Standard GUI erstellt, dass dem gewohnten Design des Handsenders ähnelt. Das Graphical User Interface lässt sich beliebig erweitern.

Im Video https://youtu.be/jBZQri6zQZs wird Blynk vorgestellt.