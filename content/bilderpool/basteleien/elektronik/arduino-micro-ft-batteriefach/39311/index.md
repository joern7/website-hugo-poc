---
layout: "image"
title: "01 Außenansicht (5465)"
date: "2014-08-27T20:53:34"
picture: "01_Auenansicht_5465.jpg"
weight: "1"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39311
- /details531b.html
imported:
- "2019"
_4images_image_id: "39311"
_4images_cat_id: "2943"
_4images_user_id: "2106"
_4images_image_date: "2014-08-27T20:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39311 -->
"Quick and dirty"-Prototyp eines Arduino Micro in einer ft-Batteriebox (32263/32958).

Der Arduino Micro (http://arduino.cc/en/Main/arduinoBoardMicro) ist ein vollwertiger 8-Bit-Arduino ähnlich dem Uno. Er hat 20 I/O-Ports, von denen einige Sonderfunktionen haben (auch analog verwendbar oder PWM). Ich habe die meisten Anschlüsse in die ft-Buchsen durchgeleitet. So hat man in einer kleinen Kiste 20 frei programmierbare Ports!

Motortreiber sind hier noch nicht vorhanden, jeder der 20 Ports kann also laut Arduino mit nur max. 40 mA belastet werden. Der Plan ist, eine zweite Box mit Motortreibern zu bauen, die man dann an die Ports anschließen kann.