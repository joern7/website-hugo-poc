---
layout: "image"
title: "04 geöffnet (5473)"
date: "2014-08-27T20:53:34"
picture: "04_geffnet_5473.jpg"
weight: "4"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39314
- /details58e4.html
imported:
- "2019"
_4images_image_id: "39314"
_4images_cat_id: "2943"
_4images_user_id: "2106"
_4images_image_date: "2014-08-27T20:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39314 -->
Seitenansicht. Hier sieht man, dass ich die Beinchen vom Micro einfach in die Hülsen gebogen und festgelötet hab.