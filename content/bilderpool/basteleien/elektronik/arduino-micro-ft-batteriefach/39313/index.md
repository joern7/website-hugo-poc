---
layout: "image"
title: "03 geöffnet (5471)"
date: "2014-08-27T20:53:34"
picture: "03_geffnet_5471.jpg"
weight: "3"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39313
- /detailsd8ee.html
imported:
- "2019"
_4images_image_id: "39313"
_4images_cat_id: "2943"
_4images_user_id: "2106"
_4images_image_date: "2014-08-27T20:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39313 -->
Halbseitenansicht.