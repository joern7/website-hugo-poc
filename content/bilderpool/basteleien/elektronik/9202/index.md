---
layout: "image"
title: "Serielle Kommunikation"
date: "2007-03-01T21:41:20"
picture: "Com-Verbindung.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["seriell", "Schnittstelle", "Com-Port", "RoboPro", "Interface", "Kommunikation"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/9202
- /details553b.html
imported:
- "2019"
_4images_image_id: "9202"
_4images_cat_id: "466"
_4images_user_id: "46"
_4images_image_date: "2007-03-01T21:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9202 -->
Das Robo-Interface kommuniziert mit dem kleinen PC im Hintergrund. Die Verbindung geht über das bunte Flachbandkabel.

Huckepack oben auf dem Rechner sitzt ein Interface mit 8 seriellen Schnittstellen, der Rechner selbst hat nochmals 2. Theoretisch wäre der MiniPC in der Lage 10 Robo-Interfaces mit insgesamt 30 Erweiterungen anzusprechen.