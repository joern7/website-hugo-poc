---
layout: "image"
title: "28pol-Buchsenplatte mit Flachbandkabel- Nr75082 + Nr75083"
date: "2010-02-06T15:39:42"
picture: "28pol-Buchsenplatte_004.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26211
- /detailsce92.html
imported:
- "2019"
_4images_image_id: "26211"
_4images_cat_id: "1863"
_4images_user_id: "22"
_4images_image_date: "2010-02-06T15:39:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26211 -->
28pol-Buchsenplatte mit Flachbandkabel- Nr75082 + Nr75083 

Es wäre gut sein als Knobloch etwas als Alternative für den Robo TX Controller ableiten könnte.