---
layout: "image"
title: "28pol-Buchsenplatte mit Flachbandkabel- Nr75082 + Nr75083"
date: "2010-02-06T15:39:42"
picture: "28pol-Buchsenplatte_005.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26212
- /details16cc.html
imported:
- "2019"
_4images_image_id: "26212"
_4images_cat_id: "1863"
_4images_user_id: "22"
_4images_image_date: "2010-02-06T15:39:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26212 -->
Es wäre gut sein als Knobloch etwas als Alternative für den Robo TX Controller ableiten könnte.