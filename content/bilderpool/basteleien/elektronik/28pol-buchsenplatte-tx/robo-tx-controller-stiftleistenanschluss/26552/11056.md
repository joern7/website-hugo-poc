---
layout: "comment"
hidden: true
title: "11056"
date: "2010-02-27T14:13:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo, 

Die Flachbandkabel-Leitung sind ja sehr dunn. Ich denke dass ich beim hohere Belastungen eher Spannungverluste gibt. Die beeinflussen die Analoge Eingänge usw. usw. eher unerwunschte Störungen.

Grüss, 

Peter, Poederoyen NL