---
layout: "image"
title: "01 Schnellwechsel mit Kabel"
date: "2010-02-26T21:03:44"
picture: "robotxcontrollerstiftleistenanschluss1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26552
- /details94fa.html
imported:
- "2019"
_4images_image_id: "26552"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26552 -->
Meine vorläufige Variante für den Zeitraum der Garantie: ROBO TX Controller und 28pol. Buchsenplatte mit 26pol. Stiftleisten.
Der Anschluss erfolgt über das 26pol. Flachkabel mit zwei 26pol. Steckern und ermöglicht einen schnellen Wechsel des ROBO TX Controllers bei mehren Modellen mit 28pol. Buchsenplatte ohne die sonst herumhängenden Flachkabelenden. Als Stiftleiste wurde hier verwendet 2x CONRAD 743542 "Stiftleiste-Wanne 26pol. Flachband".
Eine weitere Variante zum direkten Aufstecken des ROBO TX Controllers an eine 28pol. Buchsenplatte am Modell habe ich angedacht.
Eine Kurzanleitung zum Bau mit den Etikettenvorlagen für evtl. Interessenten zum Nachbau 1:1 kann aus Bild 07 heruntergeladen werden.
Im Fanclubforum ist das Thema "ROBO TX Controller - Schnellwechsel" zur Diskussion eröffnet unter
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&m=44022