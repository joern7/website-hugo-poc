---
layout: "image"
title: "02 ROBO TX Controller, Stiftleiste"
date: "2010-02-26T21:03:44"
picture: "robotxcontrollerstiftleistenanschluss2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26553
- /detailsbbe4.html
imported:
- "2019"
_4images_image_id: "26553"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26553 -->
Die Aufnahme der 26pol. Stiftleiste erfolgt über 2x BS5 und 2x BS V15 Eck. Auf dieser Seite bleiben somit am ROBO TX Controller zu seiner Befestigung noch 4 Nuten frei. Die Stiftleiste wird passend einstellbar aufgenommen. Da ich nach der Garantiezeit die Kabel ins Innere des TX verlege sind sie etwas länger gehalten. Für den Fall, dass der TX vorläufig auch mal auf kleinen Fahrmodellen mit direkten Flachsteckerverbindungen zum Einsatz kommen muss, habe ich zur schnellen Remontage die Flachstecker mit Aufkleber versehen. Die Stiftleiste ist auf ihrer Unterseite an zwei Punkten verklebt. Die Aufnahme kann am TX über die beiden BS5 abgezogen und wieder aufgesteckt werden.
Für den Anbau der Stiftleiste gibt es noch die Möglichkeit mit ihrer Rückseite an 38241 Bauplatte 15x30 anliegend zwischen 2x38253 Kupplungsstück. Hier passt sie genau dazwischen. Sie ist ebenso vom TX abzieh- und aufsteckbar. Dieser Anbau zwar etwas kürzer lässt dann allerdings an dieser Seite nur noch zwei Montagenuten am TX frei.