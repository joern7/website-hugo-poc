---
layout: "image"
title: "05 ROBO TX Controller, Flachstecker links"
date: "2010-02-26T21:03:45"
picture: "robotxcontrollerstiftleistenanschluss5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26556
- /detailse41a.html
imported:
- "2019"
_4images_image_id: "26556"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26556 -->
Die linke Hälfte der Flachsteckerbuchsen sowie auch die folgende rechte lässt erkennen, dass mit der möglichen Anordnung der Flachstecker eine Doppelnutzung der Buchsen über die Flachstecker nicht möglich ist. Wäre es komplett möglich, könnte man mit kurzen Kabeln die Siftleiste etwa so angeschlossen belassen und eine direkte Modellverkabelung mit max. 34 Stecker auf die vorhandenen Stecker sowie auf die freien Buchsen ohne die Notwendigkeit des temporären Abbaus bedarfsweise aufsetzen.