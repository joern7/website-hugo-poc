---
layout: "image"
title: "Scope image of Spursucher at approx. 225Hz"
date: "2009-06-13T15:03:03"
picture: "spursucherasquadratureencoderwithfastcounterze2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/24344
- /detailsbc6a.html
imported:
- "2019"
_4images_image_id: "24344"
_4images_cat_id: "1666"
_4images_user_id: "716"
_4images_image_date: "2009-06-13T15:03:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24344 -->
Here we see the two channels of the Spursucher when looking at a Drehscheibe with 18 black segments and rotating approx. 12 rev/sec. 
The duty cycle and phase relation is influenced by adjustment of the Spursucher relative to the disk (both coaxial and radial distance).