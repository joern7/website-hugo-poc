---
layout: "image"
title: "Spursucher test"
date: "2009-06-13T15:03:02"
picture: "spursucherasquadratureencoderwithfastcounterze1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/24343
- /details4376-2.html
imported:
- "2019"
_4images_image_id: "24343"
_4images_cat_id: "1666"
_4images_user_id: "716"
_4images_image_date: "2009-06-13T15:03:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24343 -->
Here the test setup for testing the Spursucher at higher rotation rates. It uses the ZE input on the flatcable and a small C-program that sends the counter increments to RoboPro 10 times per second.
In my test program RoboPro then immediately sends back this speed value and the accumulated value for display on the LCD in the Robo Interface (unfortunately it does not work in online mode).
With this speed and segment disk a RoboPro postion counter overflows in about 30 seconds. The resolution is 360/18/4 = 5 degrees and of course it automatically senses direction and counts up in one direction and down in the other.