---
layout: "image"
title: "spursucher as incremental encoder"
date: "2009-05-21T12:10:10"
picture: "IMG_0575.jpg"
weight: "1"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: ["spursucher", "segmentscheibe", "quadrature", "incremental", "encoder"]
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/24061
- /details0111.html
imported:
- "2019"
_4images_image_id: "24061"
_4images_cat_id: "1650"
_4images_user_id: "716"
_4images_image_date: "2009-05-21T12:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24061 -->
test configuration with two different resolutions