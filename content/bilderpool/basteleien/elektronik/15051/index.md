---
layout: "image"
title: "Selbst gebauter Ultraschall-Entfernungsmesser mit I2C Interface für Microcontrollerboard ATMega16"
date: "2008-08-15T17:00:29"
picture: "us_cmp_s.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Ultraschall-Entfernungsmesser", "I2C-Interface", "Microcontrollerboard", "ATMega16"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15051
- /detailsa127.html
imported:
- "2019"
_4images_image_id: "15051"
_4images_cat_id: "466"
_4images_user_id: "579"
_4images_image_date: "2008-08-15T17:00:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15051 -->
Selbst gebauter Ultraschall-Entfernungsmesser mit I2C-Interface für Microcontrollerboard ATMega16

Alles weitere inkl. Eagle Dateien zum Download hier:

http://home.arcor.de/uffmann/Ultrasonic.html