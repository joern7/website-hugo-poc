---
layout: "comment"
hidden: true
title: "6859"
date: "2008-08-15T20:17:00"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

die Verstärkungsschaltung funktioniert so:
ein Mikrocontroller schaltet über seine IO-Pins effektiv Widerstände hinzu oder weg, die im Feedback-Pfad eines Operationsverstärkers wirksam sind. Genauer ist das im Kapitel Hardware beschrieben unter:

[freenet-homepage.de/uffmann/Ultrasonic1.html](http://)

Gruß, uffi.