---
layout: "comment"
hidden: true
title: "6862"
date: "2008-08-15T22:36:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ah ja, Danke, soweit war ich noch nicht vorgedrungen.
Es sind R4, R5, R6, R8, R9, R10, die da mitspielen. Kann es sein, dass der µC an den zugehörigen Pins Tristate-Ausgänge hat und zwischen "Masse" und "Hochohmig" schaltet? Jetzt schau ich aber erst mal ins Datenblatt.

Gruß,
Harald