---
layout: "image"
title: "Schaltplan zum Spursensor mit ITR8307"
date: "2016-05-22T18:55:31"
picture: "QTR-4RC.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["QTR-4RC", "ITR8307", "Liniensensor", "Spursensor", "Line-Follower"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43408
- /detailsba7f-2.html
imported:
- "2019"
_4images_image_id: "43408"
_4images_cat_id: "2677"
_4images_user_id: "579"
_4images_image_date: "2016-05-22T18:55:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43408 -->
Der Widerstand R8 ist optional und kann weggelassen werden. Man braucht ihn nur, wenn die Infrarot-LEDs ständig leuchten sollen (z.B. weil man einen digitalen Pin zum Einschalten der LEDs einsparen möchte).

Der bipolare Transistor ist vom Typ BCX54-16 (Stromverstärkung 250 typisch). Die LEDs werden mit 50 mA betrieben. Ich schalte sie alle 5 ms für 1 ms ein (Tastverhältnis 20%). In diesem Pulsbetrieb könnte man die LEDs auch mit bis zu 250 mA betreiben (dazu müsste man den Widerstand R1 von 35 Ohm auf 7 Ohm reduzieren und den Widerstand R7 von 22 kOhm auf 4,7 kOhm reduzieren (den optionalen Widerstand R8 darf man dann nicht einbauen).

Die Messung funktioniert so:

- An einem digitalen Ausgangs-Pin am Mikrocontroller wird der 10nF-Kondensator am Fototransistor zunächst auf ca. 5V aufgeladen, Ladezeit > 50µs. 

- Dann wird der Pin auf Eingang umgeschaltet (ohne Pull-up Widerstand)) und die Zeitmessung für die Entladung gestartet. Dazu wird in einer Schleife für max 1 ms abgefragt, ob der Pin von high auf low gewechselt hat und wenn ja der zugehörige Zeitpunkt (Timerwert) abgespeichert. 

Bei mir läuft der Timer in Schritten von 4µs. Dann erhält man mit einem 8 bit Timer Werte zwischen ca. 30 auf weißem Papier und 250 (Limit bei 1ms) auf schwarzem Papier.