---
layout: "image"
title: "Eigenbau Spursensor mit 4 Detektoren ITR8307 am BBC Buggy"
date: "2016-05-20T14:22:53"
picture: "Spursensor_Buggy.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Spursensor", "Line", "Follower", "Labyrinth", "Maze", "Linienfolger", "Infrarot", "IR", "Reflexlichtschranke"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43393
- /details1804.html
imported:
- "2019"
_4images_image_id: "43393"
_4images_cat_id: "2677"
_4images_user_id: "579"
_4images_image_date: "2016-05-20T14:22:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43393 -->
Eigenbau Spursensor mit 4 Detektoren ITR8307 am BBC Buggy

Hab mich auch mit dem Linienfolgen und Linien-Labyrinthen beschäftigt und einen Sensor mit 4 Detektoren vom Typ ITR8307 S17 TR8 konstruiert. 

Diese IR-Reflex-Detektoren gibt es bei Aliexpress für 17 Cent/Stück. Ich verwende hier das gleiche Messprinzip wie Polulu beim QTR-8RC. Dadurch erhält man quasi einen Analogwert an einem digitalen Pin durch Messen der Entladezeit (nach einem Ladevorgang). 

Das funktioniert sehr gut.