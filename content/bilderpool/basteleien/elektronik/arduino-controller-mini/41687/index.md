---
layout: "image"
title: "line sensor NEU"
date: "2015-08-03T10:49:33"
picture: "minibots_arduino2.jpg"
weight: "9"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/41687
- /details0023.html
imported:
- "2019"
_4images_image_id: "41687"
_4images_cat_id: "2677"
_4images_user_id: "427"
_4images_image_date: "2015-08-03T10:49:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41687 -->
Der mitgelieferte Sensor ist für den Arduino nur sehr eingeschränkt nutzbar! 
( benötigt U >. 8V, liefert dig. Ausgangssignal) Besser, einfacher und billiger
ist diese Lösung. Hier kann der Kontrast per SW verändert werden.