---
layout: "image"
title: "Eigenbau Spursensor mit 4 Detektoren ITR8307"
date: "2016-05-20T14:22:53"
picture: "Spursensor.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["ITR8307", "QTR-8RC"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43394
- /detailsfbc8.html
imported:
- "2019"
_4images_image_id: "43394"
_4images_cat_id: "2677"
_4images_user_id: "579"
_4images_image_date: "2016-05-20T14:22:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43394 -->
Eigenbau Spursensor mit 4 Detektoren ITR8307

Detaillierteres Bild