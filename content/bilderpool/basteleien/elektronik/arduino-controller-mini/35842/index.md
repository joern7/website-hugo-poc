---
layout: "image"
title: "Lötseite der Platine"
date: "2012-10-07T23:20:05"
picture: "ftArduinoMiniBottom.jpg"
weight: "6"
konstrukteure: 
- "Dirk Grebe"
fotografen:
- "Dirk Grebe"
keywords: ["Controller", "Elektronik", "Arduino", "Motor"]
uploadBy: "gravy"
license: "unknown"
legacy_id:
- /php/details/35842
- /details0254.html
imported:
- "2019"
_4images_image_id: "35842"
_4images_cat_id: "2677"
_4images_user_id: "854"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35842 -->
Lötseite der Platine

Viel Bastelei alles unter zu kriegen ;-)