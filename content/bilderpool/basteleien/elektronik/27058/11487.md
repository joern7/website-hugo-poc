---
layout: "comment"
hidden: true
title: "11487"
date: "2010-05-08T19:46:32"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Lochrasterplatine und ansonsten alles Handarbeit (abgesehen vom Ausdruck fürs Deckelschild). Ich denke, daß der Aufwand mit Ätzen usw. im privaten Bereich zu groß ist. Wenn man natürlich beruflich die Möglichkeit hätte, professionelle Platinentechnik zu nutzen, ist das was anderes. Mit Lochraster läßt sich immerhin gut arbeiten.

Die rohen Platinen dieser Module sind übrigens im selben Themenbereich wie diese bereits vorgestellt. Für die (ausschweifende) Doku fehlt mir jedoch noch ein Teil für den h4-GB im DIL-Format (wäre sonst unvollständig).

Gruß, Thomas