---
layout: "image"
title: "Distance Sensor PCB top. Final version 20 aug 2006"
date: "2006-08-21T17:41:14"
picture: "printje_003.jpg"
weight: "15"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6705
- /details7609-4.html
imported:
- "2019"
_4images_image_id: "6705"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-08-21T17:41:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6705 -->
The print layout is lacking one connection. Due to the amount of space. the connector on top of this print is connecting the actual sensor.