---
layout: "comment"
hidden: true
title: "1108"
date: "2006-05-25T16:49:07"
uploadBy:
- "rbudding"
license: "unknown"
imported:
- "2019"
---
Hi MisterWho,

I am utilizing the analog input, 0..10V. 
but not sure that this is going to work for the D1 and D2 eingang.
Anyway, the resolution is now roughly 1V per Meter. RoboPro has a 10 bits resolution, timing have to be tested.

By the way did not receive the sensors YET (!!) 

cheers,

Richard

PS> photo's and result will be posted, of course.