---
layout: "image"
title: "Work in progress"
date: "2006-06-13T22:47:53"
picture: "work_in_progress_2.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6432
- /detailsc8b8.html
imported:
- "2019"
_4images_image_id: "6432"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6432 -->
development environment of the distance sensor. It has finally arrived!