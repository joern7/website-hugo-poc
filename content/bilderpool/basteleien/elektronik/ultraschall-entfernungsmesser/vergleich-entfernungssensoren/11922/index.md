---
layout: "image"
title: "Close up from the Train parts"
date: "2007-09-23T18:24:07"
picture: "comparedistancesensorfischertechnik13.jpg"
weight: "13"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11922
- /details3220.html
imported:
- "2019"
_4images_image_id: "11922"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T18:24:07"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11922 -->
