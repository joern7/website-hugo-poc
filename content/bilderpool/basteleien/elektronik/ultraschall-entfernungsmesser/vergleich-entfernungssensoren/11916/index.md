---
layout: "image"
title: "Output of both sensors."
date: "2007-09-23T17:39:05"
picture: "comparedistancesensorfischertechnik07.jpg"
weight: "7"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11916
- /details8e9c.html
imported:
- "2019"
_4images_image_id: "11916"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11916 -->
Upperpart the FT sensor, below the Parralax Ultra sonic one.
Look at the 'spikes' although the AVG function (6 samples) are utilized.