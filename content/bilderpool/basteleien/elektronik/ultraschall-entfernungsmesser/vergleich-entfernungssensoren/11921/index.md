---
layout: "image"
title: "full motion"
date: "2007-09-23T18:24:07"
picture: "comparedistancesensorfischertechnik12.jpg"
weight: "12"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11921
- /detailsf011.html
imported:
- "2019"
_4images_image_id: "11921"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T18:24:07"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11921 -->
