---
layout: "image"
title: "Test Bahn. Little train is carring Robo interface"
date: "2007-09-23T17:39:04"
picture: "comparedistancesensorfischertechnik01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11910
- /detailse2d2.html
imported:
- "2019"
_4images_image_id: "11910"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11910 -->
I took 1 meter of rails, straight. Placed on both sites Reed contacts / magnets.