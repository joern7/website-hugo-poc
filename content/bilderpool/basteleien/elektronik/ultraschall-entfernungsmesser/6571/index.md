---
layout: "image"
title: "Close up of the work in progress ;-)"
date: "2006-06-24T19:38:11"
picture: "Desktop_007.jpg"
weight: "8"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6571
- /details745b.html
imported:
- "2019"
_4images_image_id: "6571"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6571 -->
This mess is suposed to be fitted in the little red FT box ;-)