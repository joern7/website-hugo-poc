---
layout: "image"
title: "Distance Sensor PCB SilkScreen. Final version 20 aug 2006"
date: "2006-08-21T17:41:14"
picture: "printje_002.jpg"
weight: "14"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6704
- /details665e.html
imported:
- "2019"
_4images_image_id: "6704"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-08-21T17:41:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6704 -->
This is the component layout, checkout the number of components versus the amount of space within the 9v box....