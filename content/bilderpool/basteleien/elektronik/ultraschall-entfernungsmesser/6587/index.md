---
layout: "image"
title: "proto type boxed"
date: "2006-06-27T16:42:53"
picture: "Desktop_003.jpg"
weight: "12"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6587
- /details116e.html
imported:
- "2019"
_4images_image_id: "6587"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-27T16:42:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6587 -->
as you can see it fits!

two cables are needed:
1 power supply, 5..9V
2 output of the sensor, 0..6V, input this to ADC, A eingang.