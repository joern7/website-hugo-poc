---
layout: "image"
title: ";-) TX controller comptable"
date: "2009-09-21T22:36:14"
picture: "DSC_0008.jpg"
weight: "30"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/25050
- /details8977.html
imported:
- "2019"
_4images_image_id: "25050"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2009-09-21T22:36:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25050 -->
Just connecting 2 Ultraschall sensors to the new TX, 
even connected 8 (!!!)