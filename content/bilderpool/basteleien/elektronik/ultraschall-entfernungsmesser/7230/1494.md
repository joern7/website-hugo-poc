---
layout: "comment"
hidden: true
title: "1494"
date: "2006-10-28T12:07:27"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Das ist der Devantech Sensor SRF05. 

Vorteil:
- Lötaugen zum direkten Verbinden per Draht
(beim Parallax muß erst der abgewinkelte Stecker ausgelötet werden)
- leichter beschaffbar in Deutschland
(Distributor hat mehr "auf" Lager...)
- etwas preiswerter :)

Nachteil (wenn man so will)
- Abstand der Sensoren etwas geringer
(passt damit nicht in das gleiche Gehäuse wenn man das vorgebohrte von Richard hat)

Gruß,
Thomas