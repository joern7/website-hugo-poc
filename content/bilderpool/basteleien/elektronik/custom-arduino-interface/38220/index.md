---
layout: "image"
title: "Main board from below"
date: "2014-02-10T22:32:19"
picture: "20140201_192056.jpg"
weight: "3"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: ["Arduino", "spaghetti", "wires"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38220
- /details4e8e.html
imported:
- "2019"
_4images_image_id: "38220"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38220 -->
This is the main board from below - lots and lots of spaghetti!