---
layout: "image"
title: "Swivel robot"
date: "2014-02-10T22:32:19"
picture: "20140206_135725.jpg"
weight: "7"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: ["Robots", "Industry", "model", "arm"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38224
- /detailsaba9.html
imported:
- "2019"
_4images_image_id: "38224"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38224 -->
This model is basically the Swivel Robot from the Industry Robots kit (the original one, not Industry Robots II). Since I'm using a 6V power adapter (I have lots of old-style motors), I've substituted a small gear for the original worm gear so it rotates faster. Iave also added a photocell to sense when a part is put on the front platform.