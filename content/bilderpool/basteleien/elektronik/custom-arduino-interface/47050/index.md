---
layout: "image"
title: "View from below with lid removed"
date: "2018-01-07T14:27:05"
picture: "20160507_104648.jpg"
weight: "12"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/47050
- /detailsefae-2.html
imported:
- "2019"
_4images_image_id: "47050"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47050 -->
View from below with bottom lid removed, showing the Arduino clone board