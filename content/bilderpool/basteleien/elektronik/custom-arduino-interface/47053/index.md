---
layout: "image"
title: "Buzzer board"
date: "2018-01-07T14:27:05"
picture: "20160506_222318.jpg"
weight: "15"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/47053
- /details1899.html
imported:
- "2019"
_4images_image_id: "47053"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47053 -->
The buzzer board. It's a simple 555 oscillator.