---
layout: "comment"
hidden: true
title: "18904"
date: "2014-04-09T12:26:20"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

danke für den Code, werde ich auch mal reinschauen. Ich denke mal den könnte man zur Ansteuerung eines Extension Moduls (Steuerung über den 2. IR Kanal) sicher erweitern? Steht zwar nicht dabei aber ich vermute das arbeitet mit dem alten IR Handsender?

Gruß,
Thomas