---
layout: "image"
title: "fischertechnik Interface IBM"
date: "2017-04-01T12:40:41"
picture: "ch2.jpg"
weight: "2"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/45706
- /details5768-2.html
imported:
- "2019"
_4images_image_id: "45706"
_4images_cat_id: "3392"
_4images_user_id: "2374"
_4images_image_date: "2017-04-01T12:40:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45706 -->
