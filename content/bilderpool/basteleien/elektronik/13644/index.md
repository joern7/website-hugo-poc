---
layout: "image"
title: "2-achsiger Beschleunigungs- und Kippwinkel-Sensor"
date: "2008-02-13T18:49:20"
picture: "ACC_s.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Beschleunigung", "Sensor", "Kippwinkel", "Verkippung", "Eigenbau", "SMD", "Platine", "2", "6mm", "Buchse"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/13644
- /detailsda07.html
imported:
- "2019"
_4images_image_id: "13644"
_4images_cat_id: "466"
_4images_user_id: "579"
_4images_image_date: "2008-02-13T18:49:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13644 -->
Versorgung: +5V, GND
Output: 2 analoge Spannungen zwischen 0 und 5 V
Anschluss am Fischertechnik Computer-Interface an die Analogspannungs-Eingänge

x-Achse in Richtung der elektrischen Anschlüsse
z-Achse in Richtung nach oben

Damit kann man positive und negative Beschleunigungen in Richtung von 2 Achsen messen, und somit im statischen Fall auch den Kippwinkel (zur Erdbeschleunigung).

siehe auch:

<http://home.arcor.de/uffmann/Electronics.html>