---
layout: "image"
title: "CCD"
date: "2005-11-10T22:33:22"
picture: "Kamera_-_04.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5291
- /details1f25.html
imported:
- "2019"
_4images_image_id: "5291"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:33:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5291 -->
