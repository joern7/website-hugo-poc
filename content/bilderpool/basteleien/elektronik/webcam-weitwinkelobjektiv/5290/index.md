---
layout: "image"
title: "Objektiv"
date: "2005-11-10T22:32:31"
picture: "Kamera_-_03.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5290
- /details5230.html
imported:
- "2019"
_4images_image_id: "5290"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:32:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5290 -->
