---
layout: "image"
title: "Gesamtansicht"
date: "2005-11-10T22:31:20"
picture: "Kamera_-_02.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5289
- /details397f-2.html
imported:
- "2019"
_4images_image_id: "5289"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:31:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5289 -->
