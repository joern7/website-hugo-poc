---
layout: "image"
title: "Gehäuse mit Platine"
date: "2011-01-27T22:49:35"
picture: "impulsewandler4.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29815
- /details4fe6.html
imported:
- "2019"
_4images_image_id: "29815"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29815 -->
Platine im Gehäuse eingesetzt.