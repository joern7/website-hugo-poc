---
layout: "image"
title: "Platine mit Bauteilen"
date: "2011-01-27T22:49:35"
picture: "impulsewandler2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29813
- /details9068.html
imported:
- "2019"
_4images_image_id: "29813"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29813 -->
Hier nun fertig aufgelötet, auf dieser Platine ist die Elektronik um 2 Encoder Motoren anzuschließen.
Zur Teilung der Impulse nutze ich einen Binärzähler HC4040.
Rechts ist der Festspannungsregler und die Schutzdiode zu sehen.