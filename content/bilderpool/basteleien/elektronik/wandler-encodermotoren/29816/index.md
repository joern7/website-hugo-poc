---
layout: "image"
title: "Deckel für Gehäuse"
date: "2011-01-27T22:49:35"
picture: "impulsewandler5.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29816
- /details49ed.html
imported:
- "2019"
_4images_image_id: "29816"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29816 -->
Hier ist der Deckel zu sehen.
Er besitzt an den längsseiten 2 Nasen die in die Nuten des Gehäuses einrasten, der sitz Bomben fest.