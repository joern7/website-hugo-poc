---
layout: "comment"
hidden: true
title: "13460"
date: "2011-02-03T18:40:48"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Andreas (TST),
da ich mit meinem Alus blank (alle verbaut) bin, kann ich leider deinen Meßaufbau praktisch nicht 1:1 nachvollziehen. Wenn  ich dein Programm richtig verstanden habe, macht es bis zum zweiten Zeitelement Referenzfahrten an/um Taster I1 um dann 796 Impulse = 168,9 mm vor, die Hälfte zurück und diese wiederum vorzufahren zur Meßuhr. Damit werden zwei Richtungsänderungen gefahren. Wenn du dir die Kommentare von Stefan Falk und Remadus durchliest erkennst du sicher, daß zwei Richtungsänderungen zu wenig sind. Vielleicht kannst du die 398 Impulse mittels einer Schleife je Meßfahrt mal mindestens 10x oder auch öfter fahren. Dann wird die Aussage zur Genauigkeit des einpoligen Encodermotors aussagefähiger. Es geht hierbei denke ich mal vordergründig um die Frage zukünftiger ein- oder zweipoliger Encodermotoren und nicht um das "Anzweifeln" deines Impulsteiler-Bausteins.
Gruß, Ingo