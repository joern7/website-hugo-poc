---
layout: "image"
title: "Rückseite des Prototypen"
date: "2006-11-25T08:51:01"
picture: "robo-lcd7.jpg"
weight: "3"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7598
- /details0ba8.html
imported:
- "2019"
_4images_image_id: "7598"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-25T08:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7598 -->
Das sieht hier jetzt sehr wüst aus, ist aber halb so schlimm... Es waren ein paar technische Tricks notwendig, denn das LCD wird direkt am Prozessorbus betrieben. Es sind keine Lötarbeiten am RoboInt notwendig, die freien Steckerleisten führen alle notwendigen Signale und die 3,3V Betriebsspannung (Das LCD wird auch mit 3,3V betrieben). Die Ansteuerung erfolgt aus C heraus, die Daten werden geschrieben, indem einfach eine Adresse angesprochen wird.