---
layout: "image"
title: "LCD für Robo-Interface"
date: "2006-11-25T08:51:01"
picture: "robo-lcd5.jpg"
weight: "1"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7596
- /detailsd3e6.html
imported:
- "2019"
_4images_image_id: "7596"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-25T08:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7596 -->
Als ich letztens den Reichelt-Katalog durchblätterte, fielen mir LCDs auf, die sehr interessante Abmessungen haben. 
Und es passt auffallend gut in die Lücke zwischen RF-Modul und USB-Buchse...