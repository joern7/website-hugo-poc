---
layout: "comment"
hidden: true
title: "8339"
date: "2009-01-25T19:35:10"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
Hi Thomas,

Speed is critical, and no, it's not fast enough. Critical are the pulse width (WR, RD) and the hold times. Read is more critical than write but we could operate the display with only write operations. Sufficient write pulse width can be achieved with an extra waitstate (can be set in software). The other constraints can only be met by slowing down the clock. According to the datasheet of the LH155 (the display does not have that controller but a similar one), a clock of 10MHz and 2 waitstates should be ok for both reads and writes. Of course we only need to use these when we access the controller. I tried however with 20Mhz clock and 3 waitstates and I could both read and write (@3.3V), so I use this (2 waitstates did not work for me).

I was thinking about a solution with a timer in order to keep some simple process running. Start my own main program in flash2 in order to hook my own processes and then start the robopro program in flash1. There is no guarantee however that this works because the roboint may make a distinction between C and robopro programs, so I may not be able to invoke the robopro program, or nevr get control back because robopro 'stole' it.

Ad