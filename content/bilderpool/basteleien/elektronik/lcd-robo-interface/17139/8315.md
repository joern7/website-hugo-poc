---
layout: "comment"
hidden: true
title: "8315"
date: "2009-01-23T16:00:06"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
It's programmed with Renesas C. I haven't found a way to link it to RoboPro and I'm not a big fan of Basic. Besides the HEW environment with FtLoader works very convenient. If there is a demand I could help with setting up a Basic library. I would rather have an interface to RoboPro however but that will not be easy without the help of RoboProEntwickler.