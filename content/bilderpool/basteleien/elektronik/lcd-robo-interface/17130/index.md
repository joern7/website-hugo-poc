---
layout: "image"
title: "LCD showing FT logo"
date: "2009-01-23T08:29:17"
picture: "IMG_3021.jpg"
weight: "11"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/17130
- /detailse45c.html
imported:
- "2019"
_4images_image_id: "17130"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17130 -->
