---
layout: "image"
title: "Back side of the LCD"
date: "2009-01-23T08:29:18"
picture: "IMG_2981.jpg"
weight: "17"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/17136
- /details06c8-2.html
imported:
- "2019"
_4images_image_id: "17136"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17136 -->
The LCD is glued into the transparent cover of the RI with a hot glue gun. The LCD is reflective so there is no back light.