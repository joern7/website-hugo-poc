---
layout: "image"
title: "Externes LCD - Symbole"
date: "2006-11-26T12:47:56"
picture: "robo-lcd13.jpg"
weight: "7"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7625
- /detailse189.html
imported:
- "2019"
_4images_image_id: "7625"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7625 -->
Es handelt sich hierbei um einen Restposten von Polli, scheinbar aus einem Handy. Aber die Akku-Anzeige bzw. die Anzeige der Funksignalstärke ist sicherlich auch bei einem RoboInterface interessant.