---
layout: "image"
title: "Component side of the PCB"
date: "2009-01-23T08:29:17"
picture: "IMG_2977.jpg"
weight: "15"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/17134
- /details1736-3.html
imported:
- "2019"
_4images_image_id: "17134"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17134 -->
The electronic circuit is only needed to generate the 15V for the LCD from the 3.3V of the RI.