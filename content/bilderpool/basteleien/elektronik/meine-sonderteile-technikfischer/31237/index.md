---
layout: "image"
title: "Fernsteuerung Sender"
date: "2011-07-14T11:35:20"
picture: "bild1.jpg"
weight: "7"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31237
- /details0fd7.html
imported:
- "2019"
_4images_image_id: "31237"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31237 -->
Hier der dazugehörende 2-Kanal Sender