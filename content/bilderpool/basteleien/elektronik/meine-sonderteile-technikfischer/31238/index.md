---
layout: "image"
title: "Fernsteuerung Empfänger"
date: "2011-07-14T11:35:20"
picture: "bild2.jpg"
weight: "8"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31238
- /detailsbaa1-2.html
imported:
- "2019"
_4images_image_id: "31238"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31238 -->
Hier meine Fernsteuerung. Sie ist aus einem Modellauto ausgebaut und zum Schutz in ein Gehäuse verpackt.