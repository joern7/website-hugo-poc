---
layout: "image"
title: "0-9 Ziffernanzeige"
date: "2011-07-14T11:35:20"
picture: "bild3.jpg"
weight: "9"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31239
- /details1d96.html
imported:
- "2019"
_4images_image_id: "31239"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31239 -->
Hier meine Ziffernanzeige. Sie kann Contdown + Aufzählen im Sekundentakt; bei 0 wird ein Ausgang leitend