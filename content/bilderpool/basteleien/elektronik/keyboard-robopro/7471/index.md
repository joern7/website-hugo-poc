---
layout: "image"
title: "Keybord mit Robopro"
date: "2006-11-17T00:53:25"
picture: "Keybord_003.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7471
- /details0391.html
imported:
- "2019"
_4images_image_id: "7471"
_4images_cat_id: "1579"
_4images_user_id: "22"
_4images_image_date: "2006-11-17T00:53:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7471 -->
