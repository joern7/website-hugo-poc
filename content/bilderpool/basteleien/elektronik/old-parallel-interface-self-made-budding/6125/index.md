---
layout: "image"
title: "Close up of the FT interface"
date: "2006-04-15T23:28:53"
picture: "oude_interface_003.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6125
- /detailsfb7a.html
imported:
- "2019"
_4images_image_id: "6125"
_4images_cat_id: "528"
_4images_user_id: "371"
_4images_image_date: "2006-04-15T23:28:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6125 -->
Look at connectors. I even could use the FT plugs...