---
layout: "image"
title: "Close up of the Speech chip / interface"
date: "2006-04-15T23:28:53"
picture: "oude_interface_004.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6126
- /detailsec73-2.html
imported:
- "2019"
_4images_image_id: "6126"
_4images_cat_id: "528"
_4images_user_id: "371"
_4images_image_date: "2006-04-15T23:28:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6126 -->
This chip could be programmed with HEX codes, so it could 'speak'. The little button was to reset the device.