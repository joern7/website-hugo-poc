---
layout: "image"
title: "Interface with FT"
date: "2006-04-15T23:14:27"
picture: "oude_interface_002.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6124
- /detailsae2a.html
imported:
- "2019"
_4images_image_id: "6124"
_4images_cat_id: "528"
_4images_user_id: "371"
_4images_image_date: "2006-04-15T23:14:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6124 -->
Model from early 1983.
The Yellow LEDs were for the digital input, the Red LEDs are for two motors (left / Right) and the green LEDs are for two Ft lamps. The output voltage was 5 volt.

I programmed this with basic.