---
layout: "image"
title: "Platine"
date: "2007-02-11T18:18:19"
picture: "cnc1.jpg"
weight: "1"
konstrukteure: 
- "sannchen90"
fotografen:
- "sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/8970
- /details51ea.html
imported:
- "2019"
_4images_image_id: "8970"
_4images_cat_id: "816"
_4images_user_id: "6"
_4images_image_date: "2007-02-11T18:18:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8970 -->
Dies ist ein Spannungswandlerplatine von 24 Volt auf 12 Volt, das hat den Grund, dass die Schrittmotoren mit 24 Volt laufen, aber die Lüfter mit 12 Volt. Leider ist die Aussenkontur nicht so schön geworden, aber die Platine sitzt eh im Gehäuse und ist nicht zu sehen. Momentan ist nur eine Bearbeitung von 40 mal 100mm möglich

Ich hätte gerne einen Betatester. Allerdings brauche ich ein Layout für die Bahnen.