---
layout: "image"
title: "Fräsen"
date: "2007-02-11T18:18:19"
picture: "cnc2.jpg"
weight: "2"
konstrukteure: 
- "sannchen90"
fotografen:
- "sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/8971
- /details8566-2.html
imported:
- "2019"
_4images_image_id: "8971"
_4images_cat_id: "816"
_4images_user_id: "6"
_4images_image_date: "2007-02-11T18:18:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8971 -->
Hier werden kleine Isolationskanäle in die Platine gefräst. Danach werden die Löcher für die Bauteile gebohrt, natürlich auch durch die Maschine. Als Layoutprogramm habe ich Sprint Layout, für CAD Dateien benutze ich Turbo CAD 11 Pro. Gesteuert wird die Maschine mit PCNC.

Damit beschäftige ich mich jetzt schon die letzten 8 Wochen.