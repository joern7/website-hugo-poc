---
layout: "image"
title: "Nachbau der Module aus dem Kasten 'Elektronik'"
date: "2008-08-15T17:00:29"
picture: "Elektronik_front_s.jpg"
weight: "22"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Elektronik", "IC-Spanungsversorgung", "Schwellwertschalter", "Leistungsstufe"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15050
- /detailsf589-3.html
imported:
- "2019"
_4images_image_id: "15050"
_4images_cat_id: "651"
_4images_user_id: "579"
_4images_image_date: "2008-08-15T17:00:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15050 -->
Nachbau der 3 Module aus dem Kasten "Elektronik":

- IC-Spanungsversorgung
- Schwellwertschalter
- Leistungsstufe

Eagle Dateien zum Download gibt es hier:

http://home.arcor.de/uffmann/Electronics.html