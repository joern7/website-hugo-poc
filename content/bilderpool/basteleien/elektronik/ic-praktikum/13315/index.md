---
layout: "image"
title: "IC Steckkonsole mit erweiterten Pin  Version 2"
date: "2008-01-12T23:52:41"
picture: "Digitalsteine_003.jpg"
weight: "16"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/13315
- /details2e67-2.html
imported:
- "2019"
_4images_image_id: "13315"
_4images_cat_id: "651"
_4images_user_id: "473"
_4images_image_date: "2008-01-12T23:52:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13315 -->
