---
layout: "image"
title: "IC14 mit Relaisverstärker für RBII Relaisbaustein"
date: "2008-08-15T17:00:28"
picture: "IC14_RBamp_front_s.jpg"
weight: "20"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15048
- /details6e47.html
imported:
- "2019"
_4images_image_id: "15048"
_4images_cat_id: "651"
_4images_user_id: "579"
_4images_image_date: "2008-08-15T17:00:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15048 -->
Nachbau des IC14 Bausteins mit 4 Relaisverstärkern.

Eagle-Dateien gibt es hier zum Download:

http://home.arcor.de/uffmann/Electronics.html