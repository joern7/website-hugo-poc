---
layout: "image"
title: "Zähler1"
date: "2006-09-03T09:58:17"
picture: "Zhhler1.jpg"
weight: "7"
konstrukteure: 
- "ft"
fotografen:
- "Kurt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- /php/details/6778
- /details2b45.html
imported:
- "2019"
_4images_image_id: "6778"
_4images_cat_id: "651"
_4images_user_id: "71"
_4images_image_date: "2006-09-03T09:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6778 -->
