---
layout: "image"
title: "Taktgeber2"
date: "2006-09-03T09:58:17"
picture: "Takt-rck.jpg"
weight: "6"
konstrukteure: 
- "ft"
fotografen:
- "Kurt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- /php/details/6777
- /details38f0.html
imported:
- "2019"
_4images_image_id: "6777"
_4images_cat_id: "651"
_4images_user_id: "71"
_4images_image_date: "2006-09-03T09:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6777 -->
Leiterbahn