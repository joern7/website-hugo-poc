---
layout: "image"
title: "Detail 'Schraube'"
date: "2013-04-21T08:03:18"
picture: "IMG_9855.jpg"
weight: "7"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36865
- /details5a1e.html
imported:
- "2019"
_4images_image_id: "36865"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36865 -->
hier sieht man, wie ich die Schrauben zur Befestigung des Arduino verklebt habe

in Planung ist später, das komplette Paket in ein altes FT-Powerblockgehäuse zu setzen.