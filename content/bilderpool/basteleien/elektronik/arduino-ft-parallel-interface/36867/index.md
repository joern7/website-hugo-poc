---
layout: "image"
title: "TestLED"
date: "2013-04-21T08:03:18"
picture: "IMG_9857.jpg"
weight: "9"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36867
- /details645d.html
imported:
- "2019"
_4images_image_id: "36867"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36867 -->
diese LED kann mit jedem Arduino-Port verbunden werden (blaues Kabel) - dient zur Fehlersuche während der Programmiersessions, da es kein Display gibt und die fest verbaute LED auf dem Arduino-board schlecht zu sehen ist.

Die Platine über dem Arduino dient eigentlich nur der sauberen Adaptierung vom Arduino zum FT-Interface - aktive Bautile benötigt man keine, nicht mal Pull-Up-Widerstände u.ä. - die hat jeder Arduino-Ausgang schon von Hause aus.