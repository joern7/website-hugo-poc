---
layout: "image"
title: "andere Seite"
date: "2013-04-21T08:03:18"
picture: "IMG_9853.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36863
- /details889c.html
imported:
- "2019"
_4images_image_id: "36863"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36863 -->
Stromversorgung kommt jetzt zur Programmierphase für den Arduino über USB rein, später kann man die Rundbuchse mit entspr. Stecker aus der "normalen" FT-Spannung versorgen (z.B. Trafo oder Akku)