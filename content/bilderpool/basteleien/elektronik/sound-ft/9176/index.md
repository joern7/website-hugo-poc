---
layout: "image"
title: "Skizze 2"
date: "2007-03-01T16:13:13"
picture: "soundft02.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9176
- /detailsc0c4.html
imported:
- "2019"
_4images_image_id: "9176"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9176 -->
Die Skizze des Lautsprechers, der in einem Batteriekasten untergebracht ist. Das weisse viereck ist ein 4.5V Wiederstand.