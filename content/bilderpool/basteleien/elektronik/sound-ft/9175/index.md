---
layout: "image"
title: "Skizze 1"
date: "2007-03-01T16:13:12"
picture: "soundft01.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9175
- /details74d6-2.html
imported:
- "2019"
_4images_image_id: "9175"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9175 -->
Das ist die Skizze eines Umsteckers. Das Rechteck sollte einen Wiederstand darstellen.