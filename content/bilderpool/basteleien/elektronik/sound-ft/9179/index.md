---
layout: "image"
title: "Mikrophon hinten"
date: "2007-03-01T16:13:13"
picture: "soundft05.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9179
- /detailsc43c.html
imported:
- "2019"
_4images_image_id: "9179"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9179 -->
So siet es von hinten aus. Die Lötpunkt haben genau den richtigen Abstand für einen Leuchtstein.