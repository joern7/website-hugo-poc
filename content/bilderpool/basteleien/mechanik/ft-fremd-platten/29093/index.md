---
layout: "image"
title: "FT-Fremdplatten"
date: "2010-10-30T18:28:20"
picture: "FT-Platten-3.jpg"
weight: "3"
konstrukteure: 
- "Harald Krafthöfer"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29093
- /detailse3dd.html
imported:
- "2019"
_4images_image_id: "29093"
_4images_cat_id: "2113"
_4images_user_id: "22"
_4images_image_date: "2010-10-30T18:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29093 -->
FT-Fremd-Platten gibt es bei :
Harald Krafthöfer
www.schmalspurgartenbahn.de