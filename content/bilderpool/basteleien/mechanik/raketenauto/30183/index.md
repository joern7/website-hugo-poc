---
layout: "image"
title: "Rakatenauto"
date: "2011-03-03T15:41:32"
picture: "auto3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30183
- /detailsd9bb.html
imported:
- "2019"
_4images_image_id: "30183"
_4images_cat_id: "2241"
_4images_user_id: "1162"
_4images_image_date: "2011-03-03T15:41:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30183 -->
Windschutzscheibe