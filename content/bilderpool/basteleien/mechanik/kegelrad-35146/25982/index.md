---
layout: "image"
title: "Kegelzahnrad 5mm"
date: "2009-12-26T19:06:07"
picture: "kegelrad2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25982
- /details485c.html
imported:
- "2019"
_4images_image_id: "25982"
_4images_cat_id: "1829"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25982 -->
Hier ein Modifiziertes Zahnrad 35146.
Mit einem Messingeinsatz versehen kann man es auf einem Powermotor mit einer Madenschraube befestigen.