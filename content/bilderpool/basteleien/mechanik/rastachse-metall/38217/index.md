---
layout: "image"
title: "Metallachse mit Rastkupplung"
date: "2014-02-10T17:13:42"
picture: "P1040160.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38217
- /details5b8f.html
imported:
- "2019"
_4images_image_id: "38217"
_4images_cat_id: "2845"
_4images_user_id: "1729"
_4images_image_date: "2014-02-10T17:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38217 -->
Man kann damit auf das Ende der Metallachse alles aufstecken, was es im normalen Rastprogramm gibt. 
Das erweitert die Möglichkeiten doch um einiges:
- Die Teile für die Rastachsen brauchen weniger Platz als das Zubehör für Metallachsen.
- Die Zubehörteile für Rastachsen sind eigentlich recht stabil, bei höheren Kräften geben meistens die Achsen nach. In solchen Fällen kann man mit einer Metallachse das Problem lösen
- Die Metallachsen haben eine niedrigere Reibung als die Kunststoffachsen