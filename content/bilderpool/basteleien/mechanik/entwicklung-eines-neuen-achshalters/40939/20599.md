---
layout: "comment"
hidden: true
title: "20599"
date: "2015-05-05T07:18:05"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo,

ich hätte eine Rückfrage zu Arbeitsschritt 1 "vorsichtig an den Klebestellen biegen um sie zu lösen":
Den Kopf / Fuß irgendwo fest einspannen und solange am Rohr seitlich drücken bis ich das Rohr in der Hand habe?

Und zu Schritt 5 auch noch: Mit was entfernst Du die Klebereste? Abfeilen? Lösemittel?

Gratulation übrigens zu der Idee. Egal was aus defekten Zylindern wird - das ist immer noch sinnvoller als Wegwerfen.