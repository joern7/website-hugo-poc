---
layout: "image"
title: "Entstehung"
date: "2015-05-02T19:51:16"
picture: "Foto_4.jpg"
weight: "4"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40941
- /detailsc4e7.html
imported:
- "2019"
_4images_image_id: "40941"
_4images_cat_id: "3074"
_4images_user_id: "1474"
_4images_image_date: "2015-05-02T19:51:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40941 -->
Entwicklung des neuen Bausteines in Zwischenschritten.