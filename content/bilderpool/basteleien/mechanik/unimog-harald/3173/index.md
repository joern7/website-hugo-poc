---
layout: "image"
title: "Unimog11.JPG"
date: "2004-11-15T19:46:43"
picture: "Unimog11.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3173
- /details5fb9.html
imported:
- "2019"
_4images_image_id: "3173"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T19:46:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3173 -->
Der Power-Mot treibt das Mittendifferenzial über eine pneumatische Kupplung an.

Das Kardangelenk in Bildmitte gehört zur Zapfwelle, die vom hinteren MiniMot (mit schwarzem Getriebe) angetrieben wird. Der zweite MiniMot (mit grauem Getriebe, unten im Bild) gehört zur Lenkung.