---
layout: "image"
title: "Unimog21.JPG"
date: "2004-11-15T19:46:44"
picture: "Unimog21.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Unimog", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3175
- /details3410.html
imported:
- "2019"
_4images_image_id: "3175"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T19:46:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3175 -->
Der Antriebsstrang endet derzeit noch hinter dem Hinterachsdifferenzial, bis zur Radnabe fehlt noch etwas.