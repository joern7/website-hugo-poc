---
layout: "image"
title: "Pneumatische Kupplung"
date: "2015-03-07T18:08:19"
picture: "bastel2.jpg"
weight: "10"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40619
- /details8304.html
imported:
- "2019"
_4images_image_id: "40619"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2015-03-07T18:08:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40619 -->
Das hellblaue ist eine Membran, die aus Zwei-Komponenten-Silikon gegossen wurde. Nach dem Anmischen ist es sehr, sehr dünnfliüssig und erreicht noch den kleinsten Spalt, auch wenn es da gar nicht hin sollte.

Die Plexiglasscheibe hat eine umlaufende Nut außen herum und eine zweite innen herum, d.h. mit Druckluft beaufschlagt, bläst sich die Membran zu einem ringförmigen Wulst auf. Damit klemmt sie dann das Kettenrad Z20 zwischen sich und der ft-Nabe fest und die Kupplung ist geschlossen. Das Ganze gehört natürlich so eng zusammen geschoben, dass im drucklosen Zustand nur ein Luftspalt übrig bleibt.