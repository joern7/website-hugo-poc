---
layout: "image"
title: "Schraube01.JPG"
date: "2007-04-17T21:53:19"
picture: "Schraube01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/10089
- /detailsb636.html
imported:
- "2019"
_4images_image_id: "10089"
_4images_cat_id: "913"
_4images_user_id: "4"
_4images_image_date: "2007-04-17T21:53:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10089 -->
Irgendwie müssen doch die ft-Boote angetrieben werden können...

Das hier ist eine Schraube von Conrad, die aber einen etwas ungewöhnlichen Achsdurchmesser braucht: 2,4 mm. Mitgeliefert sind auch kleine Madenschrauben, aber deren Zweck ist nicht erkennbar.

Die Bohrung in der Schraube ist rund, also kein Gewinde oder Innenvierkant oder dergleichen. Die Wandung hat aber auf der einen Seite zwei Ausnehmungen drin (Bei einem runden Turm würde man von Zinnen reden). Damit kann man die Schraube verstiften, wenn man eine Querbohrung durch die Antriebswelle anbringt.

Die Antriebswelle hier ist ein 4-mm-Messingstab (Baumarkt), der auf Maß abgedreht wurde. Die dünne Spitze stammt von früheren Unternehmungen.