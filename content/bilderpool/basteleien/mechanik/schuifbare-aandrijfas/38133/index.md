---
layout: "image"
title: "In en uitschuifbare as"
date: "2014-01-27T18:35:31"
picture: "P1270084.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38133
- /details43d8.html
imported:
- "2019"
_4images_image_id: "38133"
_4images_cat_id: "2837"
_4images_user_id: "838"
_4images_image_date: "2014-01-27T18:35:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38133 -->
Assen passen perfect en schuiven zeer licht. Lijkt me perfect voor kraanarmen die een uitschuifbare aandrijving vergen. Of voor kraan steunpoten.
Ze zijn ook om te bouwen tot cardanassen in diverse voertuigen.