---
layout: "image"
title: "In en uitschuifbare as"
date: "2014-01-27T18:48:35"
picture: "P1270085.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38136
- /detailsf806.html
imported:
- "2019"
_4images_image_id: "38136"
_4images_cat_id: "2837"
_4images_user_id: "838"
_4images_image_date: "2014-01-27T18:48:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38136 -->
Nog een kleine foto van een toepassing met deze as.
Zal even zien of ik de bestel nummers achterhalen kan zodat iedereen deze assen bestellen kan bij een stihl dealer.