---
layout: "image"
title: "M4 helicoil"
date: "2014-01-27T18:35:31"
picture: "P1270083.jpg"
weight: "2"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38131
- /detailsfb17.html
imported:
- "2019"
_4images_image_id: "38131"
_4images_cat_id: "2837"
_4images_user_id: "838"
_4images_image_date: "2014-01-27T18:35:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38131 -->
De binnenste as is over de gehele lengte hol. In dit gat past precies een m4 helicoil, en na wat opboren tot 6,5mm past er zo'n zwarte rast koppelings huls in.