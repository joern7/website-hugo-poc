---
layout: "image"
title: "In en uitschuifbare as"
date: "2014-01-27T18:35:31"
picture: "P1270081.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38130
- /details9814.html
imported:
- "2019"
_4images_image_id: "38130"
_4images_cat_id: "2837"
_4images_user_id: "838"
_4images_image_date: "2014-01-27T18:35:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38130 -->
Tijdens het repareren van een Stihl HT 131 hoogsnoeier kwam ik erachter dat daar binnenin een perfecte as zit die inschuifbaar is. Het mooie is dat de buiten diameter ook 15 mm is. Deze as past dus precies tussen 2 blokken FT in.