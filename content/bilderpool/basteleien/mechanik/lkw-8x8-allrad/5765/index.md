---
layout: "image"
title: "8x8-121.jpg"
date: "2006-02-13T17:48:31"
picture: "8x8-121.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Eigenbau"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5765
- /details9141.html
imported:
- "2019"
_4images_image_id: "5765"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T17:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5765 -->
Die Vorderachsen und der Antriebsmotor am vorgesehenen Einbauort.