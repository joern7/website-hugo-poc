---
layout: "image"
title: "Snow02.jpg"
date: "2006-02-12T15:53:15"
picture: "Snow02.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5760
- /detailsb624.html
imported:
- "2019"
_4images_image_id: "5760"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-12T15:53:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5760 -->
Die Beschriftung wurde auf Papier gedruckt und dann auf ein Stück Plastik aufgeklebt. Die Wanne ist mit einer schwarzen Plastikfolie ausgelegt, sonst würde das Streumaterial überall durchsickern. Die Messingachse trägt eine ft-Schnecke, die das Salz nach hinten fördert. Unterhalb der Messingachse ist das Loch, aus dem es dann auf die Streuscheiben rieselt.