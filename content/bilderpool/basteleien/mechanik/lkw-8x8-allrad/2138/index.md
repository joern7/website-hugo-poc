---
layout: "image"
title: "8x8-133.jpg"
date: "2004-02-20T12:22:45"
picture: "8x8-133.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2138
- /detailse138.html
imported:
- "2019"
_4images_image_id: "2138"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2138 -->
Die ft-Federn sind mit Silikonschlauch etwas steifer gemacht worden. Die Differenziale sind etwas zu kurz, deswegen wurde mittels Messinghülsen 5 mm (innen 4) angestückelt.