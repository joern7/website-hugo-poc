---
layout: "image"
title: "Aandrijfing"
date: "2012-03-28T19:35:51"
picture: "draaikrans_011.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34705
- /details6da7.html
imported:
- "2019"
_4images_image_id: "34705"
_4images_cat_id: "2562"
_4images_user_id: "838"
_4images_image_date: "2012-03-28T19:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34705 -->
Tandwiel iets kleiner gemaakt dan past deze opzet precies in het FT raster,

Ik hoop dat het allemaal duidelijk is.

Vr gr Ruurd