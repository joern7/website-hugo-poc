---
layout: "comment"
hidden: true
title: "15615"
date: "2011-11-04T07:51:52"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Oh weh, Endlich, da hast Du die gute Lagerhülse umsonst zerstört! ;o)

Ja, der Trick mit den Abstandsringen ist ein wahrer Klassiker und wurde schon oft erfolgreich verbaut, z.B. hier:

http://www.ftcommunity.de/details.php?image_id=23555

http://www.ftcommunity.de/details.php?image_id=9137

Ich habe auch immer etliche dieser Gelenkklauen mit eingepressten Abstandsringen rumliegen. Denn man findet öfter eine elegante Anwendung, als man denkt.

Gruß, Thomas