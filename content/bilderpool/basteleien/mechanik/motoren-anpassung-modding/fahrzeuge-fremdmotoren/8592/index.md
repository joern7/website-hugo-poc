---
layout: "image"
title: "kleines Fahrzeug, unten"
date: "2007-01-21T15:27:21"
picture: "f5.jpg"
weight: "5"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8592
- /detailsc070.html
imported:
- "2019"
_4images_image_id: "8592"
_4images_cat_id: "791"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8592 -->
Das ist ein kleines Fahrzeug mit einem ft-Fremdmotor. Es ist schneller als mit einem 8:1 PM.