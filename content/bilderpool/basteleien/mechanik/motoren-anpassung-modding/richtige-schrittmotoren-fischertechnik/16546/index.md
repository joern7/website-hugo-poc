---
layout: "image"
title: "... und die Anbindung"
date: "2008-12-05T20:19:51"
picture: "richtigeschrittmotorenundfischertechnik3.jpg"
weight: "3"
konstrukteure: 
- "Christian Korn"
fotografen:
- "Christian Korn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16546
- /details1b3f.html
imported:
- "2019"
_4images_image_id: "16546"
_4images_cat_id: "1495"
_4images_user_id: "853"
_4images_image_date: "2008-12-05T20:19:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16546 -->
Dieser Motor, wie die meisten Schrittmotoren, hat eine 6,35 mm-Welle. Darauf habe ich mit einem Spannsatz (innen 6,35, außen 10 mm) ein Modul 1,5-Zahnrad (27 Zähne, Stahl, Breite 15 mm) befestigt (Spannsatz und Zahnrad erhältlich bei Mädler). Der Motor liefert maximal knapp 70 Ncm, das Zahnrad ist bis etwa 180 Ncm einsetzbar - das ist echte Industrietechnik, das Ding liegt mit seinem stolzen Gewicht satt in der Hand.