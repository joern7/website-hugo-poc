---
layout: "image"
title: "Motor"
date: "2008-12-05T20:19:51"
picture: "richtigeschrittmotorenundfischertechnik1.jpg"
weight: "1"
konstrukteure: 
- "Christian Korn"
fotografen:
- "Christian Korn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16544
- /details7f36.html
imported:
- "2019"
_4images_image_id: "16544"
_4images_cat_id: "1495"
_4images_user_id: "853"
_4images_image_date: "2008-12-05T20:19:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16544 -->
Man nehme: Einen Schrittmotor, zum Beispiel diesen hier...