---
layout: "image"
title: "Befestigung Motor"
date: "2008-12-05T20:19:52"
picture: "richtigeschrittmotorenundfischertechnik5.jpg"
weight: "5"
konstrukteure: 
- "Christian Korn"
fotografen:
- "Christian Korn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16548
- /details7755.html
imported:
- "2019"
_4images_image_id: "16548"
_4images_cat_id: "1495"
_4images_user_id: "853"
_4images_image_date: "2008-12-05T20:19:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16548 -->
Erfreulicherweise kann man den Motor bequem genauso befestigen wie die Knobloch-Schrittmotoren: M4-Schraube in die grauen BS15 mit Mutter.
Paßt einwandfrei - nun hat man einen schönen Antrieb, der exakt geht, deutlich schneller läuft als die Knobloch-Motoren (bis weit über 100 U/min kein Problem), und ein mehrfaches an Drehmoment auf die Achse bringt. Einziger Nachteil: Akkubetrieb dürfte kaum möglich sein, dieser Motor zieht ordentlich Strom.