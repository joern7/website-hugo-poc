---
layout: "comment"
hidden: true
title: "21197"
date: "2015-11-03T17:55:19"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
The EGM30 is a 12v motor fully equipped with encoders and a 30:1 reduction gearbox. It is ideal for small or medium robotic applications, providing cost effective drive and feedback for the user.
 
Specification:
Rated voltage 12v 
Rated torque 1.5kg/cm 
Rated speed 170rpm 
Rated current 530mA 
No load speed 216 
No load current 150mA 
Stall current 2.5A 
Rated output 4.22W 
Encoder counts per drive shaft turn 360

Link :
http://www.exp-tech.de/robotik/servos-motoren/emg30-getriebemotor-mit-drehgeber