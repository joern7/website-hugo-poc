---
layout: "image"
title: "Motor mit angebrachter Drehscheibe 2"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke5.jpg"
weight: "5"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- /php/details/40193
- /details4c6a.html
imported:
- "2019"
_4images_image_id: "40193"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40193 -->
Nahaufnahme. Hier sieht man das Stück Schlauch ein wenig.