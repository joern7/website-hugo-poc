---
layout: "image"
title: "Adapterplatte zu Stufengetriebe"
date: "2009-01-09T22:16:25"
picture: "GrundplatteMmotor.jpg"
weight: "6"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16958
- /details39e4.html
imported:
- "2019"
_4images_image_id: "16958"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-09T22:16:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16958 -->
Ich habe hier eine Adapterplatte gemacht womit sich der Powermotor wie ein M-Motor nutzen läßt.