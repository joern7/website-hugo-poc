---
layout: "image"
title: "Anbauplatte XM front"
date: "2012-08-18T17:38:09"
picture: "xmanbauplatte2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/35345
- /details1151.html
imported:
- "2019"
_4images_image_id: "35345"
_4images_cat_id: "2619"
_4images_user_id: "182"
_4images_image_date: "2012-08-18T17:38:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35345 -->
Zur Befestigung diehnt die Schraube mit der der Motor im Gehäuse befestigt ist.
Diese wird durch eine Schraube M3x10 ersetzt.