---
layout: "image"
title: "Anbauplatte XM"
date: "2012-08-18T17:38:09"
picture: "xmanbauplatte3.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/35346
- /details6730-2.html
imported:
- "2019"
_4images_image_id: "35346"
_4images_cat_id: "2619"
_4images_user_id: "182"
_4images_image_date: "2012-08-18T17:38:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35346 -->
Hier ist die Aussparung zu sehen welche die Platte am Motorgehäuse zentriert.