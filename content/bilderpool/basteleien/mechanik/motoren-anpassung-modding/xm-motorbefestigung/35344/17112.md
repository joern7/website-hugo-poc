---
layout: "comment"
hidden: true
title: "17112"
date: "2012-08-18T20:12:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Und wenn die Befesitung vorne mal nicht reichen sollte, kann man hier an den Seiten immer noch Winkelstücke anbringen - perfekt wiedermal!

Gruß,
Stefan, der keinen XM besitzt ;-)