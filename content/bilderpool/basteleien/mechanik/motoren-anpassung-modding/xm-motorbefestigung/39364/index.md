---
layout: "image"
title: "XM Motorplatte am Traktormotor 151178"
date: "2014-09-15T17:57:15"
picture: "xmplatte1_2.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39364
- /detailsee71-2.html
imported:
- "2019"
_4images_image_id: "39364"
_4images_cat_id: "2619"
_4images_user_id: "182"
_4images_image_date: "2014-09-15T17:57:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39364 -->
Damit die Anbauplatte für den XM Motor auch an den neuen Traktormotor 151178 sowie an den Encodermotor 135484 paßt braucht diese eine zusätzliche Bohrung sowie eine andere Schraube. Wer schon so eine Platte hat kann sie zur Convention mitbringen. Ich bohre dann vor Ort ein zusätzliches Loch hinein.