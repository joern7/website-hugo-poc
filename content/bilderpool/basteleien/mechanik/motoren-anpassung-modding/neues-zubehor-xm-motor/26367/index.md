---
layout: "image"
title: "XM Motor mit Getriebehalter"
date: "2010-02-13T22:21:36"
picture: "xmmotor2_2.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26367
- /detailsb5c2-2.html
imported:
- "2019"
_4images_image_id: "26367"
_4images_cat_id: "1876"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T22:21:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26367 -->
Ich habe mal einen Getriebewinkel gebaut. Leider fehlen ja beim XM Motor die Befestigungen an der Stirnseite, deswegen mußte ich auf die seitliche Befestigung zurückgreifen.