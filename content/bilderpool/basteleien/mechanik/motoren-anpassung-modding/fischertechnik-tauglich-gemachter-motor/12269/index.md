---
layout: "image"
title: "Ft-tauglich gemachter Mot. (3)"
date: "2007-10-20T23:49:47"
picture: "fischertechniktauglichgemachtermotor3.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12269
- /details5fec.html
imported:
- "2019"
_4images_image_id: "12269"
_4images_cat_id: "1095"
_4images_user_id: "592"
_4images_image_date: "2007-10-20T23:49:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12269 -->
...und die Platte mit 2 Schrauben an dem Motor befestigt.
Die Platte eignet sich hervorragent als Motorbefestigung.