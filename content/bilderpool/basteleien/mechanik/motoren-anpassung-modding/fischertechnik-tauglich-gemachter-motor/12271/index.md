---
layout: "image"
title: "Ft-tauglich gemachter Mot. (5)"
date: "2007-10-20T23:49:47"
picture: "fischertechniktauglichgemachtermotor5.jpg"
weight: "5"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12271
- /details5eff.html
imported:
- "2019"
_4images_image_id: "12271"
_4images_cat_id: "1095"
_4images_user_id: "592"
_4images_image_date: "2007-10-20T23:49:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12271 -->
Hier sieht man das Powermot.Zahnrad auf dem Motor.