---
layout: "image"
title: "Antriebersatz"
date: "2007-03-12T17:38:08"
picture: "IMG_1239.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: ["Antrieb", "Antriebersatz", "Schnell"]
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9407
- /details1fd0-2.html
imported:
- "2019"
_4images_image_id: "9407"
_4images_cat_id: "868"
_4images_user_id: "558"
_4images_image_date: "2007-03-12T17:38:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9407 -->
Aufgebaut aus einem kapputen Dickiauto und dann mit Heißkleber (was trotzdem sehr gut hält) unter der Bauplatte befestigt. Die Geschwindigkeit übersteigt die des Powermoters bei weitem. Ein Video folgt in kürze.