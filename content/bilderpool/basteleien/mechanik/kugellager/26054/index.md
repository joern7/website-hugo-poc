---
layout: "image"
title: "Kugelgelagerter BS 15 mit Loch [2/2]"
date: "2010-01-10T16:09:48"
picture: "kugellagerpeter2.jpg"
weight: "5"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: ["modding"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/26054
- /detailsa2ce.html
imported:
- "2019"
_4images_image_id: "26054"
_4images_cat_id: "568"
_4images_user_id: "-1"
_4images_image_date: "2010-01-10T16:09:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26054 -->
Nahansicht