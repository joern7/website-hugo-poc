---
layout: "comment"
hidden: true
title: "10512"
date: "2010-01-10T22:45:56"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Manchmal sind die Zapfen der Schneckenmutter im Weg, da kommen die Nuten vom BS15-Loch schon besser hin.

Aber wie hast du da drin Platz gemacht? Gebohrt? Gefräst? Und, wie hast du verhindert, dass die Harz-Soße ins Lager hineinläuft?

Gruß,
Harald