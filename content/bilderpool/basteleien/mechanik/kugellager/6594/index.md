---
layout: "image"
title: "Kugellager für Baustein 37925"
date: "2006-07-02T10:12:19"
picture: "DSCN0838.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6594
- /detailsa927-2.html
imported:
- "2019"
_4images_image_id: "6594"
_4images_cat_id: "568"
_4images_user_id: "184"
_4images_image_date: "2006-07-02T10:12:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6594 -->
leider passen nicht alle Original ft Metallachsen in die 4mm Öffnung des Lagers.