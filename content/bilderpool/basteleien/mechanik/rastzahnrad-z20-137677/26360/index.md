---
layout: "image"
title: "neues Rast-Zahnrad Z 20 rot (Art-Nr.137677)"
date: "2010-02-13T15:24:20"
picture: "neuesrastzahnradzrotartnr1.jpg"
weight: "1"
konstrukteure: 
- "ft"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/26360
- /details80c8.html
imported:
- "2019"
_4images_image_id: "26360"
_4images_cat_id: "1878"
_4images_user_id: "409"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26360 -->
Vorderansicht.
Verglichen mit dem Kettenrad Z 20 schwarz gibt es bis auf die Achsaufnahme und Farbe keinen Unterschied