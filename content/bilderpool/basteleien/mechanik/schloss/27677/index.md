---
layout: "image"
title: "Schloss 4"
date: "2010-07-05T16:39:57"
picture: "schloss4.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27677
- /details1f2d.html
imported:
- "2019"
_4images_image_id: "27677"
_4images_cat_id: "1991"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27677 -->
Die Rückseite habe ich mit dünnem 2mm Holz verschlossen, ich hatte überlegt es noch schwarz zu lackieren, damit es zu Fischertechnik passt. Habe es dann aber gelassen weil es so doch schöner aussieht.