---
layout: "image"
title: "Schloss 1"
date: "2010-07-05T16:39:57"
picture: "schloss1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27674
- /detailse0b7.html
imported:
- "2019"
_4images_image_id: "27674"
_4images_cat_id: "1991"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27674 -->
Hier eine Gesamtansicht des Schlosses. 
Zuerst habe ich an den Seiten unnötige Teile abgesägt, so, dass es ungefähr quadratisch wurde und ein handliches Format bekam. 
Dann habe ich innen drinne Kontakte aus Weißblech angebracht.