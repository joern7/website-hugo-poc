---
layout: "image"
title: "Schloss 3"
date: "2010-07-05T16:39:57"
picture: "schloss3.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27676
- /details06f0.html
imported:
- "2019"
_4images_image_id: "27676"
_4images_cat_id: "1991"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27676 -->
Auf dieser Seite war ein großes Loch in welches ich zwei Steckerbuchsen eingeklebt habe. Dann habe ich den Heißkleber schwarz angemalt. Auf der anderen Seite wo ich keine Buchsen einkleben musste, habe ich einfach einen Halbkreis aus Holz eingeklebt.