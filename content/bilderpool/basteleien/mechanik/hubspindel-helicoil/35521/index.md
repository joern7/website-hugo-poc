---
layout: "image"
title: "helicoil5.jpg"
date: "2012-09-15T13:59:16"
picture: "IMG_7995.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35521
- /details3bda.html
imported:
- "2019"
_4images_image_id: "35521"
_4images_cat_id: "2634"
_4images_user_id: "4"
_4images_image_date: "2012-09-15T13:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35521 -->
Hier das ganze zusammengesetzt. Die Hubspindel kann ziehen und drücken, weil das mittlere Kegelzahnrad weder nach vorn noch nach hinten ausweichen kann. Das Ganze ist gelenkig gelagert und wird durch eins der Gelenke hindurch angetrieben.

Und wem das ganze etwas zu schwachbrüstig vorkommt, der kann sich die große Version von chef8 hier ansehen: http://www.ftcommunity.de/details.php?image_id=35506

Den helicoil-Satz (Marke V-Coil) habe ich von Andreas (TST) bezogen. Die Idee, eine Helicoil überhaupt an ft anzupassen, stammt wohl von Anton Jansen.