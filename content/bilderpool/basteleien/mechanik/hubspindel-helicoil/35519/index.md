---
layout: "image"
title: "helicoil2.jpg"
date: "2012-09-15T13:47:11"
picture: "IMG_7750.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35519
- /details1488.html
imported:
- "2019"
_4images_image_id: "35519"
_4images_cat_id: "2634"
_4images_user_id: "4"
_4images_image_date: "2012-09-15T13:47:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35519 -->
Hier hat ein Rast-Z10 die Prozedur durchgemacht und weist jetzt ein Metall-Innengewinde M4 auf. Gewindestäbe m4 gibt es im Baumarkt, und die passen sauber in die ft-Alus hinein. Mit dieser Anordnung kann man aber die Gewindestange nur in Richtung "aus dem Bild heraus" drücken.