---
layout: "image"
title: "Seilwindentrommel für Metallachse"
date: "2009-12-26T19:06:07"
picture: "trommel1.jpg"
weight: "10"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25983
- /details7cae.html
imported:
- "2019"
_4images_image_id: "25983"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25983 -->
Auf Wunsch von Manu habe ich eine Seilwindetrommel 31998 so geändert das sie mit einer Madenschraube auf einer Metallachse befestigt werden kann.
Eine echt klasse Idee:))