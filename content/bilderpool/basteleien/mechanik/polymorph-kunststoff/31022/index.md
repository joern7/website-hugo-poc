---
layout: "image"
title: "Erste Versuche"
date: "2011-07-11T13:21:46"
picture: "plaast2.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31022
- /details6588.html
imported:
- "2019"
_4images_image_id: "31022"
_4images_cat_id: "491"
_4images_user_id: "453"
_4images_image_date: "2011-07-11T13:21:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31022 -->
