---
layout: "image"
title: "Plastik05.JPG"
date: "2006-02-03T21:52:53"
picture: "Plastik05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5738
- /details4b07.html
imported:
- "2019"
_4images_image_id: "5738"
_4images_cat_id: "491"
_4images_user_id: "4"
_4images_image_date: "2006-02-03T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5738 -->
Das sind die Gußteile aus Bild Nr.3

Passform: gut (für meinen Geschmack ein bisschen zu leichtgängig)

Zugfestigkeit: da reißt wohl eher das ft-Teil. Das ist aber reine Vermutung, denn an der spitzen Form kann man das Teil nicht ordentlich fassen.

Fazit: wer ein ft-Teil braucht, das es nicht gibt, der findet hier die Lösung.