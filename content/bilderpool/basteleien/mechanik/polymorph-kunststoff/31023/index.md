---
layout: "image"
title: "Form"
date: "2011-07-11T19:45:35"
picture: "formen1.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31023
- /detailsdc23.html
imported:
- "2019"
_4images_image_id: "31023"
_4images_cat_id: "491"
_4images_user_id: "453"
_4images_image_date: "2011-07-11T19:45:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31023 -->
Hier seht ihr die offene Form. Das Plaast Material wird reingedrückt, dann kommt der rechte Stein noch oben drauf.