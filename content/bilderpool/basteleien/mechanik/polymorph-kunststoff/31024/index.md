---
layout: "image"
title: "Form"
date: "2011-07-11T19:45:35"
picture: "formen2.jpg"
weight: "9"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31024
- /detailse015.html
imported:
- "2019"
_4images_image_id: "31024"
_4images_cat_id: "491"
_4images_user_id: "453"
_4images_image_date: "2011-07-11T19:45:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31024 -->
Plaast wird als weißes Granulat geliefert, es wird in 60 Grad heißem Wasser durchsichtig und zu einer Knetmasse.

Hier ist das Material schon in die Form gedrückt. Es dauert ein wenig bis das Plastik wieder abgekühlt ist, dann wird es wieder fest. Nun die Form auseinander nehmen und fertig sind die Teile.

