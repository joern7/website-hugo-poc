---
layout: "image"
title: "Plastik03.JPG"
date: "2006-02-03T21:40:11"
picture: "Plastik03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5736
- /details1640-2.html
imported:
- "2019"
_4images_image_id: "5736"
_4images_cat_id: "491"
_4images_user_id: "4"
_4images_image_date: "2006-02-03T21:40:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5736 -->
In heißem Zustand zieht das Material Fäden wie Fondue-Käse oder Kaugummi (es klebt auch so am Werkzeug). Die "Platte" rechts kann man noch ziemlich leicht biegen.

In der Mitte schon mal erste Ansätze, etwas ft-kompatibles zu machen.