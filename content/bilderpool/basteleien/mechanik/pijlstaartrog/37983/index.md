---
layout: "image"
title: "Polystyreen-Gold-Spiegel-Platten"
date: "2014-01-02T20:26:18"
picture: "pijlstaartrog34.jpg"
weight: "34"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37983
- /detailsf25c-2.html
imported:
- "2019"
_4images_image_id: "37983"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:18"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37983 -->
Für meine Pijlstaartrog-Fischertechnik nutze ich eine 20 Euro Polystyreen Platte Gold Spiegel 1000x500x1,5mm.
Dieser und auch andere interessante Sachen gibt es bei :
Kunststofshop.nl ,   Didamseweg 150,    6902 PE  Zevenaar  NL
info@kunststofshop.nl

