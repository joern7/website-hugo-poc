---
layout: "image"
title: "Pijlstaartrog-Fischertechnik"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog19.jpg"
weight: "19"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37968
- /details9553.html
imported:
- "2019"
_4images_image_id: "37968"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37968 -->
Fin Ray Effekt -3