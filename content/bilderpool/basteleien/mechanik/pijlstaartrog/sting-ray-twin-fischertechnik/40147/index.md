---
layout: "image"
title: "Sting-ray Twin Fischertechnik  -unten"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik02.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40147
- /detailsdc8b.html
imported:
- "2019"
_4images_image_id: "40147"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40147 -->
