---
layout: "image"
title: "Stingray-Pijlstaartrog-V2.0"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42710
- /detailsee12.html
imported:
- "2019"
_4images_image_id: "42710"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42710 -->
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel und Schwanz zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel und Schwanz zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.

Video's:
https://www.youtube.com/watch?v=AlHQmpu9C04

https://www.youtube.com/watch?v=vzF9cABscSw

https://www.youtube.com/watch?v=UW5wPpoQAsY

https://www.youtube.com/watch?v=TlITNxtRGBU