---
layout: "image"
title: "Stingray-Pijlstaartrog-V2.0  mit neuer Schwanz"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv08.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42717
- /detailsbedc-2.html
imported:
- "2019"
_4images_image_id: "42717"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42717 -->
