---
layout: "image"
title: "Detail Antrieb Finray Schwanz -1"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv03.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42712
- /details03da-4.html
imported:
- "2019"
_4images_image_id: "42712"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42712 -->
