---
layout: "image"
title: "Detail Antrieb Finray Schwanz-3  mit standard Fischertechnik Motor"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv05.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42714
- /detailsd896-2.html
imported:
- "2019"
_4images_image_id: "42714"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42714 -->
