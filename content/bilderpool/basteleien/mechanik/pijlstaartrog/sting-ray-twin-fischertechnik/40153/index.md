---
layout: "image"
title: "Sting-ray Twin Fischertechnik  -down"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik08.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40153
- /detailsb93d.html
imported:
- "2019"
_4images_image_id: "40153"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40153 -->
