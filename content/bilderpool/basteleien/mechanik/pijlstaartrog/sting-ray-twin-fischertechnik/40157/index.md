---
layout: "image"
title: "Sting-ray Twin Fischertechnik  -unten"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40157
- /details1ecb.html
imported:
- "2019"
_4images_image_id: "40157"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40157 -->
Sting-ray Twin Fischertechnik

Ich habe die Kinematik der Sting-ray Twin in Fischertechnik nachgebaut.
Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + Polystyreen-Gold-Spiegel-Platten.

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.

Links zum Youtube : 
https://www.youtube.com/watch?v=vzF9cABscSw&list=UUvBlHQzqD-ISw8MaTccrfOQ 

https://www.youtube.com/watch?v=UW5wPpoQAsY&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2 

https://www.youtube.com/watch?v=TlITNxtRGBU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1 




Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410


Meine Fischertechnik-Qualle mit Fin-Ray-Effect + pneumatik Muskel (Decke) gibt es unter :
http://www.ftcommunity.de/details.php?image_id=37251


Meine Adaptive Greifer gibt es unter :
http://www.ftcommunity.de/categories.php?cat_id=2775
Der adaptive Greifer mit Fin-Ray-Effect funktioniert auch wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.