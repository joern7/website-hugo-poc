---
layout: "image"
title: "Sting-ray Twin Fischertechnik + Silbermöwe 'Fischertechnik-Smartbird-Earth-Flight'"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40149
- /detailse70a.html
imported:
- "2019"
_4images_image_id: "40149"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40149 -->
Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410

