---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -oben"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog29.jpg"
weight: "29"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37978
- /details5e54-2.html
imported:
- "2019"
_4images_image_id: "37978"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37978 -->
