---
layout: "image"
title: "Pijlstaartrog-Fischertechnik"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog17.jpg"
weight: "17"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37966
- /details8dcc.html
imported:
- "2019"
_4images_image_id: "37966"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37966 -->
Fin Ray Effekt -1