---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -unten"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog20.jpg"
weight: "20"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37969
- /details43b3.html
imported:
- "2019"
_4images_image_id: "37969"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37969 -->
