---
layout: "image"
title: "Achsadapter Clipsachsen/Differential 31500"
date: "2005-04-07T16:17:50"
picture: "Achsadapter.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3960
- /detailsd91f.html
imported:
- "2019"
_4images_image_id: "3960"
_4images_cat_id: "644"
_4images_user_id: "103"
_4images_image_date: "2005-04-07T16:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3960 -->
Hab mir mal Gedanken gemacht wie man die Clipsachsen mit Original-FT-Teilen an das geniale FT-Diff adaptieren könnte.Die Lösung ist bei weitem nicht so professionell wie TST´s Lösung aber diese Teile hat fast jeder in der FT-Kiste.Zur besseren Passung sollten die Statikriegel seitlich jeweils 0,5mm abgeschliffen werden.Man könnte die Riegel auch noch mit einem übergezogenen Schlauch am runterrutschen vom Achsadapter hindern.Durch die verschiedenen Riegellängen können die Achslängen in feinen Abstufungen hergestellt werden.