---
layout: "image"
title: "Antriebsklauen"
date: "2005-03-05T11:26:35"
picture: "Antriebsaklauen.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/3707
- /details1e3d.html
imported:
- "2019"
_4images_image_id: "3707"
_4images_cat_id: "644"
_4images_user_id: "182"
_4images_image_date: "2005-03-05T11:26:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3707 -->
Klauen für die Kardanwellen.Einmal in 4mm für die Standartachsen und in 5mm für den Power Motor.