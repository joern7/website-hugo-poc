---
layout: "image"
title: "Aktor zusammengezogen"
date: "2014-06-10T06:55:41"
picture: "bowdenzug5.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38923
- /details5d78.html
imported:
- "2019"
_4images_image_id: "38923"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38923 -->
Mit so einem Aufbau kann man ziemlich große Hübe realisieren. Der maximale Hub hängt eigentlich nur von der Länge der Metallstangen ab