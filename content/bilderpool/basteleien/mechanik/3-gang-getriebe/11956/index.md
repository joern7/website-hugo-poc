---
layout: "image"
title: "Zahnräder"
date: "2007-09-24T22:40:57"
picture: "Zhne.jpg"
weight: "5"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/11956
- /details3948.html
imported:
- "2019"
_4images_image_id: "11956"
_4images_cat_id: "24"
_4images_user_id: "6"
_4images_image_date: "2007-09-24T22:40:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11956 -->
Diesmal in schwarz, der komplette Satz. Gefertigt mit einem Fräser mit 0,9mm Durchmesser und 30.000 U/min, 1,5mm Zustellung und 8mm/Sec. Vorschub.