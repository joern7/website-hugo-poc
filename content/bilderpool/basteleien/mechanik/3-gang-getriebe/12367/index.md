---
layout: "image"
title: "Dreigang49.JPG"
date: "2007-10-28T13:21:24"
picture: "Dreigang49.JPG"
weight: "7"
konstrukteure: 
- "Michael (sannchen90) / Harald"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12367
- /details8f70.html
imported:
- "2019"
_4images_image_id: "12367"
_4images_cat_id: "24"
_4images_user_id: "4"
_4images_image_date: "2007-10-28T13:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12367 -->
Das Getriebe mit den Zahnrädern von Michael. Die Stahlachse oben wird noch durch eine Messing-Hohlwelle ersetzt, in der dann der Draht mit der Mitnehmernase stecken wird.

Aber mal von vorne (ich bin sicher, dass ich das Getriebe schon mal beschrieben habe, aber die Beschreibung ist nicht mehr auffindbar):

Der Antrieb kommt von oben auf die (als hohl zu betrachtende) Metallachse. Darin ist ein Längsschlitz ausgefeilt, aus dem ein Mitnehmer ("Feder") heraussteht. Dieser greift in die passende Nut vom Z10 oder Z11 oder Z12 ein. Durch Längsverschieben des Mitnehmers wird das Getriebe geschaltet. 
Die Zahnradpaarungen Z10-Z20, Z11-Z19, Z12-Z18 sind ständig im Eingriff, und die Z20/Z19/Z18 sind fest mit dem Differenzialkörper verbunden. Es drehen also immer alle Zahnräder, und von den kleinen Zahnrädern drehen zwei leer, während das dritte per Mitnehmer die Kraft auf sein Gegenüber überträgt.

Das Differenzial kann man sich als Mittendifferenzial eines Allrad-Fahrzeugs vorstellen.