---
layout: "comment"
hidden: true
title: "4463"
date: "2007-10-30T22:01:09"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Jou, genau so war das. Ich hatte auch meine redliche Mühe, die großen Zahnräder von außen einzuspannen, und hab von Filmdöschen bis Sprühdosenkappe alles durchprobiert, bis ich die richtige Umhüllung gefunden hatte. Ich hatte da noch etwas von "geschlitztem Messingrohr zum Spannen von Vieleck-Körpern" im Ohr.

Na ja. Der Fachmann hätte einfach gleich die Außenspannzangen ins Drehfutter montiert, deren abgerundete Form erkannt und sich den Blödsinn gespart, den ich da veranstaltet habe. Ein bisschen mehr Übung tät mir schon gut, glaube ich.

Na egal, Schwamm drüber. Die Nuten für die Mitnehmerfeder sind mit 1 mm Breite und 1 mm Tiefe immer richtig. Ganz ohne Schlüsselfeile wird das kaum im ersten Anlauf zusammenpassen. Wichtig ist aber, dass sich die Nut unter einem Zahn befindet, nicht unter einer Zahnlücke, wo dann ggf. sehr wenig Material stehen bleibt.

@Severin - 
naja, die "Begeisterung" von ft hält sich bei derartigen Vorschlägen in Grenzen. Ein Automatikgetriebe mit ESP, ABS usw., aber mit nur zwei Spritzgussteilen (lieber aber nur einem, und das möglichst aus dem bestehenden Programm), das wäre eher was.

Gruß,
Harald