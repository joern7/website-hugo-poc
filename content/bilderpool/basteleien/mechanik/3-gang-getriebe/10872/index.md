---
layout: "image"
title: "Zahnräder"
date: "2007-06-17T22:19:51"
picture: "IMG_0854.jpg"
weight: "4"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/10872
- /details10eb.html
imported:
- "2019"
_4images_image_id: "10872"
_4images_cat_id: "24"
_4images_user_id: "6"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10872 -->
Auf einer Portalfräse hergestellte Zahnräder.

Harald schick mir bitte mal deine Adresse, hab auch deine Mail nicht mehr