---
layout: "image"
title: "IMG_9515.JPG"
date: "2013-10-19T17:29:31"
picture: "IMG_9515.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37739
- /details1b53.html
imported:
- "2019"
_4images_image_id: "37739"
_4images_cat_id: "2623"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:29:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37739 -->
So richtig überzeugt dieser Antrieb noch nicht (weil er noch nichts antreibt). Aber eine Anwendung für den Baustein 30x15x7,5 ist es doch.