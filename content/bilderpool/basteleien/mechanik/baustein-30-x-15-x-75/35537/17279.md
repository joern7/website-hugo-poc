---
layout: "comment"
hidden: true
title: "17279"
date: "2012-09-23T18:30:39"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Öööey, was heißt hier "aua"? Das ist doch der Ordner für Schnitzereien, und das Ausgangselement ist höchstselbst schon durch spanabhebendes Verfahren aus einer Platte 90x30 entstanden und stellt sozusagen ein Abfallstück dar (der Rest wurde zu einer Motoranbauplatte für den P-Motor).

Lurchi, das sehe ich auch so. Nuten in den Bauteilen sind allemale flexibler als Zapfen. Wenn man einen Zapfen braucht, kann man eine Nut immer mittels Federnocken nachrüsten.Umgekehrt gibt's dann Aua.

Danke Stefan! Und ich möchte noch betonen, dass man mit sowas auch gefederte Fahrzeugachsen homokinetisch antreiben kann. Der Federweg ist nicht berauschend, aber Federung ist eben Federung.

Gruß,
Harald