---
layout: "image"
title: "Interessante HEWA – FLEC  Bauteile im FT-Raster"
date: "2012-01-22T14:41:48"
picture: "HEWA-FlLEC-Bauteile-FT-Raster_005.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33987
- /details0ec0.html
imported:
- "2019"
_4images_image_id: "33987"
_4images_cat_id: "2516"
_4images_user_id: "22"
_4images_image_date: "2012-01-22T14:41:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33987 -->
U-Profile mit Streifen - grau  15x15mm
eckige 4mm Achsen 
Achse mit schroefeind M4
Stel-ringen en -koppelstukken 4mm + M4

Diese Bauteile gibt es bei: 

LPE  und 

http://flecnederlandvo.mamutweb.com/Shop/List/Techniekonderdelen/171/3     und

http://flecnederlandvo.mamutweb.com/Shop/Search?q=U-profiel&page=1