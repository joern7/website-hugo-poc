---
layout: "image"
title: "Interessante HEWA – FLEC  Bauteile im FT-Raster"
date: "2012-01-22T14:41:48"
picture: "HEWA-FlLEC-Bauteile-FT-Raster_003.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33986
- /detailsd6e0.html
imported:
- "2019"
_4images_image_id: "33986"
_4images_cat_id: "2516"
_4images_user_id: "22"
_4images_image_date: "2012-01-22T14:41:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33986 -->
U-Profile mit Streifen - grau  15x15mm
eckige 4mm Achsen 
Achse mit schroefeind M4
Stel-ringen en -koppelstukken 4mm + M4