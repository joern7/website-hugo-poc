---
layout: "image"
title: "HexAdapter 12mm"
date: "2014-04-16T15:10:50"
picture: "HexAdapter12mm.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Metallachse", "4mm", "HexAdapter", "Räder"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38550
- /details5e55-2.html
imported:
- "2019"
_4images_image_id: "38550"
_4images_cat_id: "2879"
_4images_user_id: "1729"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38550 -->
Mit diesen Adapter kann man an eine 4mm Metallachse ein Rad aus dem RC Car Modellbau schrauben. Damit erschliesst sich für fischertechnik eine riesige Auswahl an Rädern unterschiedlichster Größe