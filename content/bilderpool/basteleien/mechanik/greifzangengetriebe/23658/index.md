---
layout: "image"
title: "Greifzange 1. Versuch"
date: "2009-04-10T07:57:22"
picture: "kb11.jpg"
weight: "14"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23658
- /detailsd690.html
imported:
- "2019"
_4images_image_id: "23658"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23658 -->
Hier zum Vergleich noch mein erster Versuch für den Trainingsroboternachbau eine Greifzange mit zusätzlicher Dreh+Schwenkachse zu bauen, leider viel zu klobig.