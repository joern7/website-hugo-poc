---
layout: "image"
title: "Greifzange Antrieb"
date: "2009-04-10T07:57:11"
picture: "kb05.jpg"
weight: "8"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23652
- /details9e8d.html
imported:
- "2019"
_4images_image_id: "23652"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23652 -->
Nochmal geschlossen.