---
layout: "image"
title: "Nachbau Greifzangengetriebe"
date: "2008-02-02T10:03:47"
picture: "Bild1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["m05"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/13508
- /details152b.html
imported:
- "2019"
_4images_image_id: "13508"
_4images_cat_id: "1237"
_4images_user_id: "182"
_4images_image_date: "2008-02-02T10:03:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13508 -->
Ich habe hier mal ein Greifzangengetriebe nach gebaut.Funktioniert einwandfrei. Hier ist das Herzstück zu sehen.