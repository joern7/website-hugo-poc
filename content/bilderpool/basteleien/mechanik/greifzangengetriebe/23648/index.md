---
layout: "image"
title: "Greifzange für Trainigsroboternachbau    -    Aktueller Stand"
date: "2009-04-10T07:57:10"
picture: "kb01.jpg"
weight: "4"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23648
- /detailsf61c.html
imported:
- "2019"
_4images_image_id: "23648"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23648 -->
Für meinen Trainingsroboter-Nachbau tüftle ich schon lange an einer kompakten Greifezange,
hier jetzt die Bilder zum aktuellen Stand.
Die Backen werden durch Drehen des Zahnrad Z16 im modifizierten Baustein 15 mit Loch parallel verfahren.
Dies funktioniert mittels Seilzug/ Feder von hinten. 
