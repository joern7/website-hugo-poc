---
layout: "image"
title: "Greifzangengetriebe komplett"
date: "2008-02-02T10:03:47"
picture: "Bild2.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/13509
- /details9cd3.html
imported:
- "2019"
_4images_image_id: "13509"
_4images_cat_id: "1237"
_4images_user_id: "182"
_4images_image_date: "2008-02-02T10:03:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13509 -->
Ich habe hier mal ein Greifzangengetriebe nachgebaut.Funktioniert einwandfrei.Hier der komplette Zusammenbau.