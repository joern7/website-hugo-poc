---
layout: "image"
title: "Greifzange"
date: "2009-04-10T07:57:11"
picture: "kb08.jpg"
weight: "11"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23655
- /details91e2.html
imported:
- "2019"
_4images_image_id: "23655"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23655 -->
Noch eine Seitenansicht