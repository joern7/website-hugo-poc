---
layout: "image"
title: "Impulserad"
date: "2009-12-26T19:06:07"
picture: "impulsrad1.jpg"
weight: "11"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25984
- /details85f2.html
imported:
- "2019"
_4images_image_id: "25984"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25984 -->
Auf Wunsch von Manu habe ich ein Impulsrad 37157 so geändert das sie mit einer Madenschraube auf einer Metallachse befestigt werden kann.
Eine echt klasse Idee:))