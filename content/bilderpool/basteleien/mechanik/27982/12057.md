---
layout: "comment"
hidden: true
title: "12057"
date: "2010-08-28T16:59:55"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Udo2,

ich bin überzeugt, daß es auch bei Kunststoffachsen funktioniert, nur wird sich die Madenschraube viel leichter in die Achse eindrehen lassen, als bei einer Metallachse. Das könnte eventuell eine (zukünftige) Bruchstelle ergeben. Auch bei Metallachsen kann man die Madenschraube so fest zudrehen, daß man Spuren sieht...
Die Idee von TST gefällt mir sehr gut, da sehr platzsparend.

Viele Grüße
Mirose