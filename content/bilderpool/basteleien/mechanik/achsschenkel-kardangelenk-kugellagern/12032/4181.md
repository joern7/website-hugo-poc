---
layout: "comment"
hidden: true
title: "4181"
date: "2007-09-29T10:20:00"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Eine Metallachse für die "Radseite" habe ich schon. Die ist an meinem Traktor verbaut. Allerdings nicht mit einem Kardangelenk. Fehlt nur noch die passende Länge für die "Antriebsseite". Für eine kompaktere Vorderachse müsste die etwas kürzer sein.
Aber ich kenne da jemanden der das bestimmt machen kann.

;-))

Nicht wahr Andreas ....