---
layout: "overview"
title: "Achsschenkel mit Kardangelenk und Kugellagern"
date: 2020-02-22T07:42:34+01:00
legacy_id:
- /php/categories/1074
- /categoriesf297.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1074 --> 
45mm breiter Achsschenkel mit schwarzem Kardangelenk und Schneckenmuttern mit Kugellagern.