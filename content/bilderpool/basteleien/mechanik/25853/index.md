---
layout: "image"
title: "6mm auf 4mm Adpter"
date: "2009-11-28T21:31:18"
picture: "wellenadapter1.jpg"
weight: "9"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: ["Wellenadapter"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/25853
- /detailsa37c.html
imported:
- "2019"
_4images_image_id: "25853"
_4images_cat_id: "465"
_4images_user_id: "-1"
_4images_image_date: "2009-11-28T21:31:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25853 -->
Mit diesen Teil kann man entweder von einer 6mm Welle von einen Motor auf 4mm Fischertechnik Stange oder von einer 4mm Stange auf eine 6mm gewindestange kommen.
Hinten habe ich eine Längere Stange genommen, damit man mit einem Lichschranken die drehungen zählen kann.

Das Teil habe ich selber gemacht.