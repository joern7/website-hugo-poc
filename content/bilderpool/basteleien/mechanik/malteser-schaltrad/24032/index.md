---
layout: "image"
title: "Malteser Schaltrad 3"
date: "2009-05-17T17:56:42"
picture: "Bild3.jpg"
weight: "13"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/24032
- /detailsfdab-2.html
imported:
- "2019"
_4images_image_id: "24032"
_4images_cat_id: "542"
_4images_user_id: "182"
_4images_image_date: "2009-05-17T17:56:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24032 -->
Hier die 3. Anschicht