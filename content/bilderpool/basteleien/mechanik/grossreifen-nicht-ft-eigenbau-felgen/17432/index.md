---
layout: "image"
title: "Felge komplett"
date: "2009-02-16T19:03:02"
picture: "IMG_7750.jpg"
weight: "16"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/17432
- /details5a88-3.html
imported:
- "2019"
_4images_image_id: "17432"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-02-16T19:03:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17432 -->
Und nun komplett mit Reifen. 
Es gibt nur noch das Problem mit dem Durchrutschen zu lösen, ohne Tricks wird man nicht das notwendige Drehmoment übertragen können...