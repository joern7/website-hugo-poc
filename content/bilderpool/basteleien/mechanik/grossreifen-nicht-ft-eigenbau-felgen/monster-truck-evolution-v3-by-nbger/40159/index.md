---
layout: "image"
title: "Am Anfang war das Rad"
date: "2015-01-04T07:47:31"
picture: "Raeder.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Monstertruck", "Monsterreifen", "Monster"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/40159
- /details8e20.html
imported:
- "2019"
_4images_image_id: "40159"
_4images_cat_id: "3016"
_4images_user_id: "1729"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40159 -->
Die Wahl für das Rad fiel auf das ganz Rechte. Die Gründer erkläre ich im nächsten Bild.
Dieses Bild zeigt einen Größenvergleich:

- ft Standardreifen
- ft großer Reifen
- inzwischen gut bekannter Fremdreifen, der auf die Drehscheibe passt
- ein Reifen von Conrad für RC Cars
- das ft-Männchen, der zukünftige Testpilot