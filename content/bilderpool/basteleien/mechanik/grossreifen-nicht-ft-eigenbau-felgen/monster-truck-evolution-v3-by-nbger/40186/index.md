---
layout: "image"
title: "'Späte Untersetzung' im Reifeninnern"
date: "2015-01-04T07:47:31"
picture: "Untersetzung.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Untersetzung", "Vorgelegegetriebe", "Portalachse"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/40186
- /detailsc8f3.html
imported:
- "2019"
_4images_image_id: "40186"
_4images_cat_id: "3016"
_4images_user_id: "1729"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40186 -->
Den Tipp für die "späte Untersetzung" hat mir ursprünglich Stefan Falk gegeben. Das hatte ich daraufhin schon mal bei den Monsterreifen mit Drehscheibe umgesetzt.
siehe http://www.ftcommunity.de/categories.php?cat_id=2832

Durch die späte Untersetzung möglichst nah am Rad wird der gesamte Antriebsstrang mit höherer Drehzahl, aber im selben Verhältnis geringerem Drehmoment betrieben. Das ist ein deutlicher Vorteil in der Fischertechnik-Welt, die für größere Drehmomente wirklich nicht geeignet ist.
Das Ganze nennt man im Fachjargon "Vorgelegegetriebe".

Hier kommt nun die verbesserte Version. Dies ist eigentlich der Hauptgrund, weswegen ich diese Reifen ausgewählt habe. Die Zahnräder für die Untersetzung verschwinden vollständig im Radinnern (Z10 und Z30). Damit spare ich Baubreite/Spurbreite. Durch diese Anordnung entstehen weitere Vorteile:
Die Radachse und die Antriebsachse sind zwei verschiedene, höhenversetzte Achsen. Dies nennt man Portalachse und wird z.B. im realen Fahrzeugbau beim Unimog und beim Humvee eingesetzt. Durch die Portalachse gewinnt man hauptsächlich Bodenfreiheit, da das Differential weiter oben liegen kann.
Im Vergleich hat der Humvee eine ähnliche Konstruktion. Auch bei ihm ist eine Portalachse und ein Vorgelegegetriebe direkt am Rad kombiniert.

Ein weiterer Vorteil für mich im Modell: Antriebsachse und Lenkgestänge haben genügend Platz und kommen sich nicht mehr in die Quere.