---
layout: "image"
title: "Raupenband-Antrieb.jpg"
date: "2015-03-28T17:04:28"
picture: "IMG_1691.JPG"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40687
- /details0b07-2.html
imported:
- "2019"
_4images_image_id: "40687"
_4images_cat_id: "366"
_4images_user_id: "4"
_4images_image_date: "2015-03-28T17:04:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40687 -->
Eine gescheite Nabe oder Felge für den Antrieb dieses Raupenbands fehlt im ft-Sortiment. 
Mit roten und schwarzen Kettengliedern 1:1, und einem Spurkranz zur seitlichen Führung geht es, ist aber ein ziemlicher Aufwand.