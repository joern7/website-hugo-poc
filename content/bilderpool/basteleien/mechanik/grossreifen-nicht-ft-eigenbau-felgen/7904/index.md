---
layout: "image"
title: "Supermonsterreifen"
date: "2006-12-15T15:28:56"
picture: "Supermonsterreifen.jpg"
weight: "11"
konstrukteure: 
- "Chevyfahrer"
fotografen:
- "Chevyfahrer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/7904
- /details8d0e.html
imported:
- "2019"
_4images_image_id: "7904"
_4images_cat_id: "366"
_4images_user_id: "103"
_4images_image_date: "2006-12-15T15:28:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7904 -->
Wem die Monsterreifen zu popelig sind kann kostengünstig aufrüsten.Durchmesser:furchteinflößende 250mm.Preis 7,50€/Stk(mit ein wenig Glück im Schnäppchenmarkt)
Material Gummi mit Schlauch,Felge Kunststoff Farbe Ähnlich FT-hellrot.Ins Achsloch passt genau ein Standartbaustein oder Alu oder BS15 mit Loch oder....
Herkunft:Bollerwagen aus Schnäppchenmarkt,Komplettpreis 29,99€.
Verwendbarkeit ungewiss,aber den Versuch lohnt´s(Radlader oder so)