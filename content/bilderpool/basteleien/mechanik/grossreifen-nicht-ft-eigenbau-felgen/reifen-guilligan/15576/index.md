---
layout: "image"
title: "2. Montage"
date: "2008-09-24T22:21:36"
picture: "reifen04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/15576
- /details8316.html
imported:
- "2019"
_4images_image_id: "15576"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15576 -->
Dann die nächste Drehscheibe hinein
und auch mit einer Clipsplatte versehen.