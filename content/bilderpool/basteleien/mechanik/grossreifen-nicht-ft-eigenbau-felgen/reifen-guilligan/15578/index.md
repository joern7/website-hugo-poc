---
layout: "image"
title: "4. Montage"
date: "2008-09-24T22:21:36"
picture: "reifen06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/15578
- /detailsf50d.html
imported:
- "2019"
_4images_image_id: "15578"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15578 -->
Von der Außenseite das Zahnrad mit festgezogener Achse einstecken.