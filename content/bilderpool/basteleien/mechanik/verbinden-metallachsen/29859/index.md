---
layout: "image"
title: "Verbinden von Metallachsen"
date: "2011-02-04T15:24:20"
picture: "mmmm1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29859
- /details4c33.html
imported:
- "2019"
_4images_image_id: "29859"
_4images_cat_id: "2199"
_4images_user_id: "1162"
_4images_image_date: "2011-02-04T15:24:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29859 -->
Hier mal ein Tipp, wie man Metallachsen verbinden kann auch ohne ft-Teile.
Ihr braucht eine Lüsterklemme , deren Öffnungen etwas größer als die M-Achsen sind,
Metallachsen,
einen Schraubenzieher