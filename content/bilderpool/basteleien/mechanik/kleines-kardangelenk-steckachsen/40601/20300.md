---
layout: "comment"
hidden: true
title: "20300"
date: "2015-03-05T00:17:19"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Hallo Martin,
willkommen in der ft-Welt. Du moddest dich gerade in unsere ft-Herzen und hoffentlich auch in unsere Kästen. Wann startest du eigentlich die Produktion in größeren Stückzahlen?

Hier noch ein zum Thema Modding passendes Zitat aus einer Inschrift am Dessauer Theater:

Wir ändern gerne mit viel Schwung
wir ändern auch die Änderung
und ist die Änderung gelungen,
dann ändern wir die Änderungen

Gruß, Ralf Geerken