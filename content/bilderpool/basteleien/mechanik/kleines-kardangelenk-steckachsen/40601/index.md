---
layout: "image"
title: "Kardangelenk"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen1.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40601
- /details8b72.html
imported:
- "2019"
_4images_image_id: "40601"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40601 -->
Es gibt ja einige Lösungen für Frontantriebe hier in der ftc, aber irgendwie waren mir alle zu groß. Ursache dafür war eigentlich immer das Kardangelenk, was meistens durch andere Lösungen ersetzt wurde. Im Modellbau habe ich auch kein genügend kleines gefunden (meine Bedingung war ft-Durchmesser von 4mm oder kleiner). Also: Selber bauen. Hier ist das Ergebnis.