---
layout: "image"
title: "Kardangelenk allein"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen3.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40603
- /details3dd8.html
imported:
- "2019"
_4images_image_id: "40603"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40603 -->
Das Messingprofil ist genau so hergestellt wie für die Mini-Steckverbinder http://www.ftcommunity.de/categories.php?cat_id=3032. Durch die kleinen Einkerbungen in den Klauen kann man sie durch solche Mini-Steckverbinder verlängern.