---
layout: "image"
title: "Kardangelenk in gemoddetem Gelenkstein für Frontantrieb"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen6.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40606
- /details12fb.html
imported:
- "2019"
_4images_image_id: "40606"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40606 -->
Nächster Versuch für den Frontantrieb. Klein, aber nur mit Schnitzmesser und Kleber.