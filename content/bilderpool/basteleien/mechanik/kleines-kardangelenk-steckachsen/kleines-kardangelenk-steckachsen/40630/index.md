---
layout: "image"
title: "Herstellung Kardangelenk - feilen"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40630
- /detailscf36.html
imported:
- "2019"
_4images_image_id: "40630"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40630 -->
... fummelige Feilarbeit ...