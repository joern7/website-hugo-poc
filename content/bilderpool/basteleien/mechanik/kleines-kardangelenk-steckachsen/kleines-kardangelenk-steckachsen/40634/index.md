---
layout: "image"
title: "Kardangelenk - Nadel als Stift"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40634
- /detailsee4d.html
imported:
- "2019"
_4images_image_id: "40634"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40634 -->
Für die Stifte habe ich Nadeln verwendet.