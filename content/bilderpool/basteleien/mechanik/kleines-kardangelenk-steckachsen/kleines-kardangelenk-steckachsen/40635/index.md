---
layout: "image"
title: "Kardangelenk-Würfel - Abmessungen"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40635
- /details0546.html
imported:
- "2019"
_4images_image_id: "40635"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40635 -->
Super wäre ein Messingprofil mit 2x2mm, ich hatte nur eins mit 3x3mm. Also: Vor dem Bohren feilen...