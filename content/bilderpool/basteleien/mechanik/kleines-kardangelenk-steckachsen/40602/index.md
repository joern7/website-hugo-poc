---
layout: "image"
title: "Kardangelenk mit Achsen"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40602
- /details0aed.html
imported:
- "2019"
_4images_image_id: "40602"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40602 -->
Wie gesagt, an keiner Stelle dicker als 4mm. Man kann es in die Klauen der Rastachsen stecken, dann sind die beiden Rastachsenenden ca. 5mm voneinender entfernt. Bei Harald's Unimog http://www.ftcommunity.de/categories.php?cat_id=238 gibt es eine ähnliche Schnitzerei, sogar als Gleichlaufgelenk. So viel Mühe habe ich mir nicht gegeben. Dafür ist's noch ein wenig kleiner geworden.