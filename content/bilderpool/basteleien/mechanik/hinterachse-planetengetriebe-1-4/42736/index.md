---
layout: "image"
title: "Innenzahnrad Z40 bei der Nachbearbeitung"
date: "2016-01-24T18:52:37"
picture: "dz2.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42736
- /detailsb847.html
imported:
- "2019"
_4images_image_id: "42736"
_4images_cat_id: "3183"
_4images_user_id: "4"
_4images_image_date: "2016-01-24T18:52:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42736 -->
Das Z40 aus dem 3D-Drucker ist schon mittels zweier Kabelbinder am Speichenrad fixiert und wird hier mit dem Dremel und einem Fräser nachbearbeitet. Trotz allem Rechnen und Messen liefen die Zahnräder zu stramm, und zum Glück war außen herum noch etwas "Fleisch" dran, so dass ich mehr "Luft" machen konnte.