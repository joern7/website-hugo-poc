---
layout: "image"
title: "Untersetzung 1:4 im Monsterreifen"
date: "2016-01-24T18:52:37"
picture: "dz1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42735
- /detailsf50a.html
imported:
- "2019"
_4images_image_id: "42735"
_4images_cat_id: "3183"
_4images_user_id: "4"
_4images_image_date: "2016-01-24T18:52:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42735 -->
Der Showstopper für meinen Fendt-Geräteträger auf der Convention 2015 in Dreieich war ein knatterndes Z10 im ft-Innenzahnrad Z30: das angetriebene Z10 ging direkt auf den Zahnkranz und konnte zur Mitte (durchgehende tragende Stahlachse) hin ausweichen. Dem habe ich in der Zwischenzeit abgeholfen, indem ein Z10 mit Messing-Bund und ohne Madenschraube (von Andreas Tacke) auf der Stahlachse lose mitläuft.

Das Gelbe vom Ei ist das immer noch nicht. Das Innen-Z30 ist sowas von zickig und launisch, dass es irgendwie anders gehen musste. Na, dann probieren wir mal. Außerdem ist das Thema "Planetengetriebe mit ft" noch sehr dünn besetzt, nicht zuletzt weil es eben nur dieses eine Z30 und den einen möglichen Satz mit Z10 (Innen) + Z10 (Planeten) gibt.

hier ist ein Versuch aus dem 3D-Drucker. Der Planetensatz hat eine Übersetzung von 1:4. Die Achsen der Z15 müssen am Fahrwerk fest montiert werden; der Antrieb kommt über die tragende Stahlachse in der Mitte. Das gelbe ist eine Freilaufnabe. 

Das rote Innen-Z40 wird über den inneren Rand zentriert und mittels Kabelbinder drehsteif mit dem Speichenrad verbunden. Das war die sparsamste Art der Befestigung, weil alle anderen betrachteten Lösungen mehr Bauhöhe im Drucker gebraucht hätten, d.h. es wäre großflächig stützendes Material angefallen, das nach dem Druck mühsam abgepopelt werden muss.

Dumm an diesem Aufbau ist der Achsabstand zwischen Z15 und Z10: es gibt keinen halben BS7,5, und genau dieser wäre hier nötig.