---
layout: "image"
title: "Das fertige Innen-Z40"
date: "2016-01-24T18:52:37"
picture: "dz4.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42738
- /detailsc373.html
imported:
- "2019"
_4images_image_id: "42738"
_4images_cat_id: "3183"
_4images_user_id: "4"
_4images_image_date: "2016-01-24T18:52:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42738 -->
Mit einer Reihe von Löchern, die die Ausnehmungen für die Zähne durch den Boden hin fortsetzen, wäre das Nachbearbeiten deutlich einfacher geworden. Man lernt eben immer noch etwas dazu.