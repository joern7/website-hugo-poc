---
layout: "image"
title: "Wunsch und Wirklichkeit"
date: "2016-01-24T18:52:37"
picture: "dz3.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42737
- /detailsb4a6.html
imported:
- "2019"
_4images_image_id: "42737"
_4images_cat_id: "3183"
_4images_user_id: "4"
_4images_image_date: "2016-01-24T18:52:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42737 -->
Das Innen-Z40 hat jetzt noch einen Bund obendrauf bekommen, auf dem der Wulst des Monsterreifens aufliegen soll.

Der Bildschirm hinten zeigt die Vorschau vom Slicer auf die untersten Druckschichten, und da sind die Zähne blitzblank frei stehend. Die Zähne müssen aber allesamt nachbearbeitet werden, weil der Druckkopf beim Weg von Zahn zu Zahn immer einen dünnen Faden nach sich zieht.