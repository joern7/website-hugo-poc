---
layout: "image"
title: "Die eigentliche Fräse"
date: "2005-05-09T22:25:57"
picture: "Frse.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/4116
- /detailscef9.html
imported:
- "2019"
_4images_image_id: "4116"
_4images_cat_id: "350"
_4images_user_id: "115"
_4images_image_date: "2005-05-09T22:25:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4116 -->
