---
layout: "image"
title: "Fixierbackenantieb"
date: "2005-05-09T22:00:49"
picture: "GrennderungDetail_Fixierbackenantrieb.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/4114
- /detailsf8d5.html
imported:
- "2019"
_4images_image_id: "4114"
_4images_cat_id: "350"
_4images_user_id: "115"
_4images_image_date: "2005-05-09T22:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4114 -->
