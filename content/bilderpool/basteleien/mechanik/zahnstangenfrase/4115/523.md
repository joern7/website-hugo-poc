---
layout: "comment"
hidden: true
title: "523"
date: "2005-05-10T22:23:24"
uploadBy:
- "charly"
license: "unknown"
imported:
- "2019"
---
Den Schrittantriebbesogt der Pneumatikzylinder, der am Sperr-
rad Z40 je 2 Zähne weiterschaltet.
Dieses wirkt auf das Z20 am Schlitten, somit wird ein Zahn weitertransportiert.
Die genaue Positionierung besorgen dann wie
gesagt die Fixierbacken.
Das Gewicht wirkt auf den Schlitten als Gegenzug zur Fräsrichtung.