---
layout: "image"
title: "Z 12"
date: "2015-02-22T21:47:09"
picture: "zahnraederausmeinembastelkeller2.jpg"
weight: "2"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/40595
- /detailsdb73.html
imported:
- "2019"
_4images_image_id: "40595"
_4images_cat_id: "3045"
_4images_user_id: "115"
_4images_image_date: "2015-02-22T21:47:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40595 -->
Einmal aufgeklebt auf ein abgedrehtes Z10 und einmal mit Messingnabe