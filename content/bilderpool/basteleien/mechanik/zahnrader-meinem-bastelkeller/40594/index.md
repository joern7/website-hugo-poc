---
layout: "image"
title: "Z 12 mit Messingnabe"
date: "2015-02-22T21:47:09"
picture: "zahnraederausmeinembastelkeller1.jpg"
weight: "1"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/40594
- /details47db.html
imported:
- "2019"
_4images_image_id: "40594"
_4images_cat_id: "3045"
_4images_user_id: "115"
_4images_image_date: "2015-02-22T21:47:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40594 -->
Das Z12 ist perfekt geeignet um mit den großen Zahnstangen und großen (Encoder)Motoren Turbo-Linearantriebe zu bauen. Es greift bei 7,5 mm Abstand genau in die seitliche Verzahnung der großen Zahnstangen.