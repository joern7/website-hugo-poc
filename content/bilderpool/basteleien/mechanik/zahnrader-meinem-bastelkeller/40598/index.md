---
layout: "image"
title: "Z 60"
date: "2015-02-22T21:47:09"
picture: "zahnraederausmeinembastelkeller5.jpg"
weight: "5"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/40598
- /detailse18f-2.html
imported:
- "2019"
_4images_image_id: "40598"
_4images_cat_id: "3045"
_4images_user_id: "115"
_4images_image_date: "2015-02-22T21:47:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40598 -->
Einfach ein schön großes Zahnrad, zur Nabenaufnahme wurde hier ein abgedrehtes Z20 eingeklebt. Bin da jetzt  auf Spurkränze umgestiegen, weil die leichter gerade zu verkleben sind.