---
layout: "image"
title: "Drehstabfederung (1)"
date: "2015-03-07T18:08:19"
picture: "bastel3.jpg"
weight: "16"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40620
- /details3636.html
imported:
- "2019"
_4images_image_id: "40620"
_4images_cat_id: "465"
_4images_user_id: "4"
_4images_image_date: "2015-03-07T18:08:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40620 -->
Beim Herumstöbern habe ich dieses frühe Werk wieder gefunden: der Anfang eines Panzerfahrwerks mit Drehstabfederung. Die Drehstäbe sind die weißen Plastikrohre, die zur Rechten und zur Linken vom Mittelsteg abgehen. Die Messingrohre außen dienen als Führung für die Tragarme, an denen die Wellenstummel (weitere Messingrohre) befestigt sind. 

Das Ganze ist ein Fehlversuch: die Federwirkung ist nicht besonders, und die Tragarme halten nicht lange. So. ist der Tragarm Nr. 2 auf der linken Seite im Loch für den Wellenstummel abgebrochen.

Das Elegante an diesem Entwurf ist, dass die Federelemente leicht getauscht werden können: Links und rechts je einen Bildernagel heraus ziehen, das Röhrchen heraus nehmen und ein anderes einführen.