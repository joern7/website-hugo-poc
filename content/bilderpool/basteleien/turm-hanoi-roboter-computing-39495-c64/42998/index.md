---
layout: "image"
title: "Roboter von oben / senkrecht"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter5.jpg"
weight: "5"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42998
- /details20e7.html
imported:
- "2019"
_4images_image_id: "42998"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42998 -->
Irgendwie "schön.." :-)