---
layout: "comment"
hidden: true
title: "21768"
date: "2016-03-08T09:35:35"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
Moin Martin, ja, das macht wirklich Spaß - Retro-Computing ... :-)
In Kürze werde ich den Plotter in Angriff nehmen - die Teile hab ich jetzt komplett, evtl etwas ander Motoren kommen da zum Einsatz - ich freu mich schon sehr..  Übrigens, an c64 +Floppy etc bekommt man schon für wenig Taler in der Bucht, mit viel Glück auch schon man örtlich in e-Kleinanzeigen oder Fundgrube der Tageszeitung - der finanzielle Aufwand ist jedenfalls überschaubar ;-)