---
layout: "image"
title: "Parameter - hier die Positionen der 3 Türme als Werte des Potientometers"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter4.jpg"
weight: "4"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42997
- /details6bae.html
imported:
- "2019"
_4images_image_id: "42997"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42997 -->
man muss IM Programm die Positionen hinterlegen, die angefahren werden sollen . Diese sind vorher mit dem Diagnose-Programm zu ermitteln. Klar, wenn der Sonntag noch etwas länger wäre, könnte man schon noch eine "teach-in"-Funktion einbauen . erstmal ging es aber darum, mit den original-Programmen loszustarten.