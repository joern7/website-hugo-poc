---
layout: "image"
title: "Anschlussfeld aus dem 'Experimental'-Kasten"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter6.jpg"
weight: "6"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42999
- /details8371.html
imported:
- "2019"
_4images_image_id: "42999"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42999 -->
Ich habe keine feste Verdrahtung mit der Kabelpeitsche aufgebaut, sondern das Anschlussfeld aus dem Experimental-Kasten verwendet.