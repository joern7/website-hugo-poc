---
layout: "image"
title: "Scanner - Rückansicht"
date: "2003-09-27T11:33:05"
picture: "Scanner-Heck.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Scanner", "Optik", "Sensor"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1561
- /details600f.html
imported:
- "2019"
_4images_image_id: "1561"
_4images_cat_id: "183"
_4images_user_id: "46"
_4images_image_date: "2003-09-27T11:33:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1561 -->
Ein einzelner Fototransistor als Sensor, der Rest ist Antriebstechnik