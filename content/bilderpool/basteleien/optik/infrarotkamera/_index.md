---
layout: "overview"
title: "Infrarotkamera"
date: 2020-02-22T07:45:47+01:00
legacy_id:
- /php/categories/183
- /categories7f93.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=183 --> 
Infrarotfotografie mit einem einzigen Sensor.