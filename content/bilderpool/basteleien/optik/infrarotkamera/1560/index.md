---
layout: "image"
title: "Scanner - Frontansicht"
date: "2003-09-27T11:33:05"
picture: "Scanner-Front.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Scanner", "Optik", "Sensor"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1560
- /details9091.html
imported:
- "2019"
_4images_image_id: "1560"
_4images_cat_id: "183"
_4images_user_id: "46"
_4images_image_date: "2003-09-27T11:33:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1560 -->
Prinzipmodell des Scanners mit 85mm Optik und Fototransistor als Sensor. Ein paar feine Details (Verkabelung) fehlen.