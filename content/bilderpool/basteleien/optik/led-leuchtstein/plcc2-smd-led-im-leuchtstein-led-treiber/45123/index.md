---
layout: "image"
title: "Platine"
date: "2017-02-05T15:27:45"
picture: "ledimftleuchtstein3.jpg"
weight: "3"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45123
- /details8d2b.html
imported:
- "2019"
_4images_image_id: "45123"
_4images_cat_id: "3362"
_4images_user_id: "2240"
_4images_image_date: "2017-02-05T15:27:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45123 -->
mit 1-3 LEDs bestückbar