---
layout: "image"
title: "Led-Leuchtstein  mit grüner Kappe (5)"
date: "2007-11-05T19:42:44"
picture: "Led_grn.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Led", "Leuchtstein", "Selbstbau", "Eigenbau", "Licht", "Beleuchtung", "Löten", "Leuchtkappe"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/12514
- /details9246.html
imported:
- "2019"
_4images_image_id: "12514"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-11-05T19:42:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12514 -->
Links: ausgeschaltete Led 
Rechts: eingeschaltete Led