---
layout: "image"
title: "LED Streifen im Baustein"
date: "2017-02-11T19:43:45"
picture: "erledimstatikwinkeltraeger1.jpg"
weight: "1"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45135
- /detailsaf54-2.html
imported:
- "2019"
_4images_image_id: "45135"
_4images_cat_id: "3364"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T19:43:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45135 -->
Vierer LED Platine in einen Statikstein geklebt