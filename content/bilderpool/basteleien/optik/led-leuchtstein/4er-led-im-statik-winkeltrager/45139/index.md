---
layout: "image"
title: "LED Streifen"
date: "2017-02-11T19:43:45"
picture: "erledimstatikwinkeltraeger5.jpg"
weight: "5"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45139
- /details5aa8.html
imported:
- "2019"
_4images_image_id: "45139"
_4images_cat_id: "3364"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T19:43:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45139 -->
Jeweils zwei der LEDs können getrennt angesteuert werden.