---
layout: "image"
title: "LED Streifen Vorderseite"
date: "2017-02-11T19:43:45"
picture: "erledimstatikwinkeltraeger3.jpg"
weight: "3"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45137
- /detailse562.html
imported:
- "2019"
_4images_image_id: "45137"
_4images_cat_id: "3364"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T19:43:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45137 -->
Die LEDs sitzen genau passend im Fischertechnik Raster.