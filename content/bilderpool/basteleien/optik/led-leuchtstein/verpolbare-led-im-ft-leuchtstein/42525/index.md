---
layout: "image"
title: "Helligkeit"
date: "2015-12-19T12:41:27"
picture: "vlifl4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42525
- /detailsb1db.html
imported:
- "2019"
_4images_image_id: "42525"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42525 -->
Die LEDs leuchten sehr hell und man sollte besser nicht hineinschauen. Durch jede LED fließen bei 9,2 Volt etwa 12 mA. Dies macht eine Leistungsaufnahme von 0,11 Watt. Zum Vergleich: Durch herkömmliche ft Linsenlampen fließen bei rund 9 V etwa 120 mA, dies entspricht der zehnfachen Leistungsaufnahme.