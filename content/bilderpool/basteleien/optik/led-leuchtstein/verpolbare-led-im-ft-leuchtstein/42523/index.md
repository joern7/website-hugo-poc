---
layout: "image"
title: "LEDs"
date: "2015-12-19T12:41:27"
picture: "vlifl2.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42523
- /detailsdf7a.html
imported:
- "2019"
_4images_image_id: "42523"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42523 -->
LED Platinen passen dank ihrer Größe (12mm x12mm) in die ft Leuchtsteine