---
layout: "image"
title: "Anleitung zum Bau des Led-Leuchtsteines (3)"
date: "2007-11-05T18:24:55"
picture: "3._Schritt.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Led", "Leuchtstein", "Anleitung", "Löten", "Selbstbau", "Licht", "Beleuchtung", "Eigenbau", "Leuchtdiode"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/12511
- /detailsaa6f.html
imported:
- "2019"
_4images_image_id: "12511"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-11-05T18:24:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12511 -->
Zu guter Letzt werden jeweils auf beide Metallröhren im Leuchstein Lötpunkte gesetzt. Anschließend lötet man den Led-Widerstand Baustein ein. 
Sinnvoll ist, einen der beiden Pole zu makieren, damit es beim Anschließen zu keiner Verpolung kommt. Ich z.B. habe den Pluspol mit einem kleinem Brandzeichen mit dem Lötkolben beidseitig gekennzeichnet. Wer möchte, kann die Led zusätzlich mit Heißkleber oder ähnlichen, isolierenden Kleber fixieren.