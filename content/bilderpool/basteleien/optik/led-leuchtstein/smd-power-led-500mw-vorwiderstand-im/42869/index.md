---
layout: "image"
title: "Ansicht von oben"
date: "2016-02-13T21:30:12"
picture: "LED_top.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["LED", "Power", "Leuchtstein", "Vorwiderstand", "500mW"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42869
- /detailse1aa.html
imported:
- "2019"
_4images_image_id: "42869"
_4images_cat_id: "3188"
_4images_user_id: "579"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42869 -->
Das war mein bisher am besten gelungenes Exemplar