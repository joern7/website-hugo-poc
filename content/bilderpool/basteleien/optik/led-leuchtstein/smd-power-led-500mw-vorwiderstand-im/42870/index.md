---
layout: "image"
title: "Leuchtend von der Seite"
date: "2016-02-13T21:30:12"
picture: "LED_side.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42870
- /details08a6.html
imported:
- "2019"
_4images_image_id: "42870"
_4images_cat_id: "3188"
_4images_user_id: "579"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42870 -->
Aus diesem Bild kann man den Leuchtwinkel abschätzen und bekommt einen Eindruck von der Helligkeit. Man sieht auch, dass die LED tief im Leuchtstein liegt und eine aufgesetzte Leuchtkappe in ihrer ganzen Oberfläche hell erleuchten würde.