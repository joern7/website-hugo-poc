---
layout: "image"
title: "KFZ Beleuchtung mit LEDs für Montage auf 15x30 mm Bauplatte"
date: "2016-05-23T11:08:29"
picture: "KFZ_Beleuchtung.jpg"
weight: "26"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["KFZ", "Auto", "Beleuchtung", "LED"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43414
- /details6088.html
imported:
- "2019"
_4images_image_id: "43414"
_4images_cat_id: "1073"
_4images_user_id: "579"
_4images_image_date: "2016-05-23T11:08:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43414 -->
Für Betrieb an 5V bzw. direkt an den Ausgängen eines Mikrocontrollers.

Außen jeweils 2 LEDs Blinker gelb (in Serie geschaltet mit ca. 100 Ohm Vorwiderstand)

Frontlicht: je 2 weiße LEDs als Frontscheinwerfer (jeweils mit ca. 100 Ohm Vorwiderstánd)
Rücklicht: ganz innen je 2 rote LEDs als Bremslicht, mittig 2 rote LEDs als Rückscheinwerfer

Es fehlt noch: Rückfahrscheinwerfer