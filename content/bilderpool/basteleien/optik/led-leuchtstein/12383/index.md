---
layout: "image"
title: "LED imLeuchtstein"
date: "2007-11-03T12:52:37"
picture: "helleledsimleuchtbaustein2.jpg"
weight: "6"
konstrukteure: 
- "Frank Hanke"
fotografen:
- "Frank Hanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/12383
- /detailsc3b6.html
imported:
- "2019"
_4images_image_id: "12383"
_4images_cat_id: "1073"
_4images_user_id: "666"
_4images_image_date: "2007-11-03T12:52:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12383 -->
eine andere Perspektive