---
layout: "image"
title: "3mm LED Halter 3"
date: "2008-02-05T17:14:32"
picture: "4.jpg"
weight: "12"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13554
- /details47a4-3.html
imported:
- "2019"
_4images_image_id: "13554"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13554 -->
Alles zusammengeschoben.