---
layout: "image"
title: "LEDs"
date: "2007-12-16T00:19:57"
picture: "leds2.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/13084
- /details6481-2.html
imported:
- "2019"
_4images_image_id: "13084"
_4images_cat_id: "699"
_4images_user_id: "453"
_4images_image_date: "2007-12-16T00:19:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13084 -->
Hier mal ein Beitrag von mir zu dem Thema.