---
layout: "image"
title: "1. Baustufe"
date: "2006-11-02T12:59:01"
picture: "DSCN1089.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7303
- /detailsdc2e.html
imported:
- "2019"
_4images_image_id: "7303"
_4images_cat_id: "699"
_4images_user_id: "184"
_4images_image_date: "2006-11-02T12:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7303 -->
zuerst die LED-Anschlüsse von innen her durch ein Loch stecken. Dann mit einem Schraubendreher die LED leicht in das Loch drücken.