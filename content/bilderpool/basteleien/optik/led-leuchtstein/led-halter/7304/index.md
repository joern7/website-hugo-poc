---
layout: "image"
title: "2. Baustufe"
date: "2006-11-02T12:59:01"
picture: "DSCN1092.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7304
- /details9e67.html
imported:
- "2019"
_4images_image_id: "7304"
_4images_cat_id: "699"
_4images_user_id: "184"
_4images_image_date: "2006-11-02T12:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7304 -->
Dann die Lagerhülse von außen aufschieben. Die sitzt so fest das die LED nicht nach hinten rutschen kann. Theroretisch wäre in der Hülse noch Platz für einen Vorwiderstand Das habe ich hier aber nicht gemacht.