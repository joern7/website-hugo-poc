---
layout: "image"
title: "LED Halter 3mm 2"
date: "2008-02-05T17:14:32"
picture: "5.jpg"
weight: "11"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["LED", "Halter", "3mm"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13553
- /details4693-2.html
imported:
- "2019"
_4images_image_id: "13553"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13553 -->
Erst die Hüse überschieben dann den 3mm Klip.
Man kann auch den Widerstand kürzer an die LED anlöten, dann verschwindet er in der Hülse.