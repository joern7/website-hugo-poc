---
layout: "image"
title: "LED halter 5"
date: "2008-02-05T17:14:32"
picture: "2.jpg"
weight: "14"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["LED", "Halter", "3mm"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13556
- /details063d.html
imported:
- "2019"
_4images_image_id: "13556"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13556 -->
Sieht gut aus oder?