---
layout: "image"
title: "3mm diffuse warmweiße Led mit smd Widerstand im Leuchtstein eingelötet"
date: "2015-12-14T10:51:17"
picture: "2015-10-19_21.25.21.jpg"
weight: "22"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/42517
- /details9368.html
imported:
- "2019"
_4images_image_id: "42517"
_4images_cat_id: "1073"
_4images_user_id: "2496"
_4images_image_date: "2015-12-14T10:51:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42517 -->
