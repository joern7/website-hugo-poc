---
layout: "comment"
hidden: true
title: "16979"
date: "2012-07-11T01:00:34"
uploadBy:
- "gggb"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen.

> Eine sehr saubere Lösung!
Danke! Mir ist diese Lösung vor zwei Jahren eingefallen. Mich hat es immer genervt, dass immer mehr von den Lampen dunkel geblieben sind. Das ging so weit, dass ich meine Modelle ohne Beleuchtung gebaut habe. Einfach aus Angst vor dem durchbrennen.

> Optimal wäre es natürlich, wenn diese Lampenfassungen auch separat von ft angeboten würden.
Dies wird wohl nicht geschehen. Aber, was solls… Die Glühleuchtmittel gehen kaputt, garantiert. Und dann kommt eine Platine drauf.
So hatte ich mir das vor gestellt. Die Schaltung war schnell gezeichnet und im Layout Programm erstellt. Da man bei den Platinenherstellern immer mindestens 10cm * 10cm abnehmen muss, sind dabei natürlich einige entstanden. Die Menge, welche ich für mich brauchte, hatte ich mir zurück gelegt. Den Rest wurde in ebay feilgeboten. Innerhalb weniger Wochen (ich hatte nur immer einen Vorgang in ebay eingestellt) waren die LED verkauft. Sogar so, dass sich meine LED´s wieder raus hatte. Ein angenehmer Nebeneffekt. Die Ersteigerer haben sogar noch welche dann ohne ebay bei mir nachbestellt.

Zwei LED´s sind so kräftig das man in einem dunklen Raum von 3 auf 3 Meter, alles wieder erkennen kann. Mehr Power sollte es nicht sein. Wegen der punktuellen Lichtleistung der LED. Aus diesem Grunde kann fischertechnik auch LED´s nicht anbieten. Sie müssten für jede Version und deren Untergruppierung eine eigene technische Untersuchung machen lassen. Das ist einfach nicht realisierbar. (Das ist Originalton aus der Entwicklungsabteilung von ft) Und das stimmt auch so. Diese Probleme kenn ich in meiner Berufswelt auch.
Die LED ist das teuerste. Gerade dann, wenn man eine etwas stärker strahlende haben möchte. Platine, Dioden und Widerstände sind nicht so das Problem. Es sind damals ca. 4 Euro pro System entstanden. Es mach richtig Spaß damit. Einfach anschließen. Keine Polungsgedanken machen. Bisher ist noch keine kaputt gegangen.
Die ft hat die Innenabmessungen der Leuchtkappen verändert. Daher passt meine Version nicht unter die neuen Kappen. Wenn ich die neue mal in die Finger bekomme, werde ich die Abmessung verändern.
Grüße in die Runde