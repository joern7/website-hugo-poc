---
layout: "image"
title: "LED an Trafo"
date: "2012-07-07T09:52:30"
picture: "ftLED1.jpg"
weight: "10"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- /php/details/35101
- /detailsd671.html
imported:
- "2019"
_4images_image_id: "35101"
_4images_cat_id: "1190"
_4images_user_id: "1524"
_4images_image_date: "2012-07-07T09:52:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35101 -->
LED in Funktion an einem ft-Trafo