---
layout: "image"
title: "12V Led mit Vorwiederstand"
date: "2008-07-26T16:23:19"
picture: "Bild_78.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["LED"]
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/14963
- /details2fe7.html
imported:
- "2019"
_4images_image_id: "14963"
_4images_cat_id: "1190"
_4images_user_id: "791"
_4images_image_date: "2008-07-26T16:23:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14963 -->
Hab mir eine 12V LED bei Mayer elektronik besorgt und eingebaut.
Sorry für die schlechte Bildqualität, ich musst die bilder mit meiner Webcam machen weil meine Digicam schrott gegangen ist.