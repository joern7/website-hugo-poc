---
layout: "image"
title: "LED von der Seite"
date: "2013-12-15T21:11:13"
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37928
- /detailsce5b.html
imported:
- "2019"
_4images_image_id: "37928"
_4images_cat_id: "2821"
_4images_user_id: "1624"
_4images_image_date: "2013-12-15T21:11:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37928 -->
Der Leuchtstein wurde ein wenig aufgebohrt, damit die LED schön drin sitzt.