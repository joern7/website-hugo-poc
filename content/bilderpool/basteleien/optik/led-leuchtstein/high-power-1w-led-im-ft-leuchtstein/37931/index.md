---
layout: "image"
title: "Aufnahme ohne Blitz"
date: "2013-12-15T21:11:13"
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37931
- /details0c28-2.html
imported:
- "2019"
_4images_image_id: "37931"
_4images_cat_id: "2821"
_4images_user_id: "1624"
_4images_image_date: "2013-12-15T21:11:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37931 -->
Der 120° Abstrahlwinkel ist schon beeindruckend. Außerdem passt eine Rastleuchtkappe problemlos drauf.