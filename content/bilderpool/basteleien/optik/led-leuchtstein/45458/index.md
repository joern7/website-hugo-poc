---
layout: "image"
title: "LED Leuchtsteine - Serienproduktion fertig"
date: "2017-03-06T21:07:12"
picture: "IMG_20161223_134825.jpg"
weight: "30"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["LED", "leuchtsteiene", "eigenbau"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/45458
- /detailsdc83.html
imported:
- "2019"
_4images_image_id: "45458"
_4images_cat_id: "1073"
_4images_user_id: "2638"
_4images_image_date: "2017-03-06T21:07:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45458 -->
Das ist unsere Ausbeute. 20 LED Steine.
Wie man gut erkennen kann haben wir uns entschieden die LEDs am Baustein mit Heißkleber noch zu befestigen. Das sichert sie nicht nur, sondern erlaubt uns die genaue Positionierung und Ausrichtung zu fixieren. Die Lichter sehen dann gleichmäßiger aus.

Links oben: an diesem Baustein ist schon zu erkennen, dass wir einfach mit Rot und Grün die Polatirär markiert und (stilisiertes "V") den Abstrahlwinkel (siehe nächstes Bild) dargestellt haben.