---
layout: "image"
title: "LED (2)"
date: "2008-02-16T13:52:45"
picture: "ledolli2.jpg"
weight: "21"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13655
- /detailsa384.html
imported:
- "2019"
_4images_image_id: "13655"
_4images_cat_id: "1073"
_4images_user_id: "504"
_4images_image_date: "2008-02-16T13:52:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13655 -->
Aus einer anderen Perspektive. Die LED und den Vorwiderstand habe ich bei PUR-LED (www.pur-led.de) bestellt.