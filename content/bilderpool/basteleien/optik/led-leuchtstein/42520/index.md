---
layout: "image"
title: "Leuchtstein in Action"
date: "2015-12-16T18:14:58"
picture: "IMG_5166.jpg"
weight: "24"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/42520
- /details1470.html
imported:
- "2019"
_4images_image_id: "42520"
_4images_cat_id: "1073"
_4images_user_id: "2496"
_4images_image_date: "2015-12-16T18:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42520 -->
Linke Seite sind Leds.Rechts sind Glühbirne.Zum testen sind Leuchtkappen und Rastleuchtkappen aufgesteckt