---
layout: "image"
title: "LED-Strahler"
date: "2008-02-04T13:59:55"
picture: "LED4.jpg"
weight: "18"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13519
- /detailse394.html
imported:
- "2019"
_4images_image_id: "13519"
_4images_cat_id: "1073"
_4images_user_id: "456"
_4images_image_date: "2008-02-04T13:59:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13519 -->
Das sind 2 superhelle, weiße LEDs (Vorwiderstand ist auch drangelötet) in je einem Stück Plexiglasrohr. Vorne ist ein Papiertrichter mit Alufolie beklebt, das reflektiert anderes Licht und zerstreut das Licht der LED auch ein bischen. Besser aussehen tut's sowieso.