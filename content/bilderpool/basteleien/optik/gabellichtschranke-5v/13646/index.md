---
layout: "image"
title: "Anwendung Gabellichtschranke"
date: "2008-02-13T18:49:21"
picture: "FLB_Appl_s.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Gabellichtschranke", "Segmentscheibe", "Kettenfahrzeug", "Regelung", "Motoren", "Kettenantrieb", "Gleichlauf"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/13646
- /details42cd.html
imported:
- "2019"
_4images_image_id: "13646"
_4images_cat_id: "694"
_4images_user_id: "579"
_4images_image_date: "2008-02-13T18:49:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13646 -->
Einsatz der Gabellichtschranke mit Segmentscheibe in einem Kettenfahrzeug zur Regelung der beiden Motoren für den Kettenantrieb (Gleichlauf)

weitere infos hier:

http://home.arcor.de/uffmann/Electronics.html#Chapter6

Wegen Umzug der Page funktioniert der link unten zu freenet nicht mehr.