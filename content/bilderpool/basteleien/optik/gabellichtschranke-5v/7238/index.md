---
layout: "image"
title: "Blocked signal means 0V"
date: "2006-10-28T08:17:34"
picture: "schanke_006.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/7238
- /details508a.html
imported:
- "2019"
_4images_image_id: "7238"
_4images_cat_id: "694"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7238 -->
