---
layout: "image"
title: "With FT cable and isolation..."
date: "2006-10-28T08:17:34"
picture: "schanke_003.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/7235
- /details9b89.html
imported:
- "2019"
_4images_image_id: "7235"
_4images_cat_id: "694"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7235 -->
yes... more like a cable... no PCB needed.