---
layout: "image"
title: "Schema and components"
date: "2006-10-28T08:17:34"
picture: "schanke_001.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/7233
- /detailsba60.html
imported:
- "2019"
_4images_image_id: "7233"
_4images_cat_id: "694"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7233 -->
How to save the Gabellichtschranke 5V, when connecting it to the RoboPro interface.