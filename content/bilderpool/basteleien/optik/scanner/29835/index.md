---
layout: "image"
title: "Versuch N. 2"
date: "2011-01-30T14:03:27"
picture: "scanner10.jpg"
weight: "10"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29835
- /details6745.html
imported:
- "2019"
_4images_image_id: "29835"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29835 -->
Etwas schwieriger als bei Versuch N. 1