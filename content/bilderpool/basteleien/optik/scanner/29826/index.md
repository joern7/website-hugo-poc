---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-30T14:03:27"
picture: "scanner01.jpg"
weight: "1"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29826
- /details4e44.html
imported:
- "2019"
_4images_image_id: "29826"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29826 -->
Der Scannner hat einen doppelten Boden wo sich das Interface befindet.