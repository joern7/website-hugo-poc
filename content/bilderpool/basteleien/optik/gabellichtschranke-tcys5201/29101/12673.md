---
layout: "comment"
hidden: true
title: "12673"
date: "2010-11-01T17:34:08"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Ich dachte erst für die LED aber das wird wohl intern über eine Stromquelle geregelt wenn der Spannungsbereich so groß ist.

Bei Pollin im Datenblatt ist von einem Lastwiderstand zwischen + und Signal von 1 KOhm die Rede, allerdings auch von einem Kondensator von 0,1µF zwischen + und - zur Kompensierung von Spannungsschwankungen. Für unsere Zwecke kann der vermutlich entfallen, stimmt's Andreas?