---
layout: "image"
title: "Zusammenbau"
date: "2010-10-31T17:32:21"
picture: "Bild2.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["Gabellichtschranke"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29101
- /detailsfb07.html
imported:
- "2019"
_4images_image_id: "29101"
_4images_cat_id: "2115"
_4images_user_id: "182"
_4images_image_date: "2010-10-31T17:32:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29101 -->
In dem Gehäuse läßt sich ohne weiteres der erforderliche Widerstand platzieren. Mehr Elektronik braucht man nicht. Laut Datenblatt arbeitet sie mit bis zu 16 V und hat eine Schmitt-Trigger Schaltung Intergriert. Rechts ist zu sehen wie ich die Lichtschranke auf eine Bauplatte geklebt habe. So paßt sie perfekt ins Raster.