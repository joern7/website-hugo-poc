---
layout: "image"
title: "Lampe mit Reflektor"
date: "2008-03-15T09:11:09"
picture: "lampe1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/13902
- /details330a.html
imported:
- "2019"
_4images_image_id: "13902"
_4images_cat_id: "1276"
_4images_user_id: "453"
_4images_image_date: "2008-03-15T09:11:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13902 -->
Hier eine Nahaufnahme.