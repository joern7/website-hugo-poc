---
layout: "image"
title: "Lasermodul 2"
date: "2006-05-13T17:33:29"
picture: "IMG_2407.jpg"
weight: "2"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/6269
- /details84c7.html
imported:
- "2019"
_4images_image_id: "6269"
_4images_cat_id: "545"
_4images_user_id: "384"
_4images_image_date: "2006-05-13T17:33:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6269 -->
Die Laserdiode ist nicht verklebt, sondern mittels zweier Gummiringe in das Gehäuse eingepresst. Ein Schutzdiode Gegen Verpolung ist im Modul verbaut.
Die Laserdiode sowie den Stepdownwandler gibt es bei Pollin.
!! Achtung, bei eventuellem Nachbau nicht in den Strahl blicken. Auch wenn die Diode nur Schutzklasse IIIA hat kann es zu nachhaltigen Schädigungen der Netzhaut kommen !!