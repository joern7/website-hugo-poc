---
layout: "image"
title: "Lasermodul 1"
date: "2006-05-13T17:33:29"
picture: "IMG_2406.jpg"
weight: "1"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/6268
- /details464c.html
imported:
- "2019"
_4images_image_id: "6268"
_4images_cat_id: "545"
_4images_user_id: "384"
_4images_image_date: "2006-05-13T17:33:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6268 -->
Die Laserdiode wurde in ein Gehäuse des alten Fotowiederstandes eingebaut. Da die Diode mit 3,5V Arbeitet war ein Stepdownwandler von 9V auf 3,5V nötig.
Als Lichtschranke arbeitet der Laser mit dem Fototransistor bestens zusammen.
!! Auch Laser <5mW können langfristige Schäden im Auge hinterlassen. !!