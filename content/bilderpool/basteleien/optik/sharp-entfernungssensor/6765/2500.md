---
layout: "comment"
hidden: true
title: "2500"
date: "2007-02-25T11:28:31"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Bei einem typischen RC-Tiefpass müsste der Widerstand im Signal-Weg vor dem C kommen.

So liegt das Signal bei hohen Frequenzen ja fast auf Masse, da der Wechselstromwiderstand des Kondensators sehr  gering ist...

Da das Signal immer positiv gegenüber der Masse ist, sollte ein gepolter Elko auch gehen. Hab eh grad keinen anderen :)

Thomas