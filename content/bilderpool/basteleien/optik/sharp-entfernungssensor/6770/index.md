---
layout: "image"
title: "Anschluß Sharp Distanzsensor an Robo-Interface"
date: "2006-09-03T09:58:17"
picture: "Anschlu_A1-SignalA2-MasseM1-Motor_rechtslauf_nahaufnahme.jpg"
weight: "4"
konstrukteure: 
- "Remadus hat die Platine entwickelt!!!"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6770
- /details76e6.html
imported:
- "2019"
_4images_image_id: "6770"
_4images_cat_id: "650"
_4images_user_id: "426"
_4images_image_date: "2006-09-03T09:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6770 -->
Hier eine Nahaufnahme!