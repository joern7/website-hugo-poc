---
layout: "image"
title: "Werkstück"
date: "2008-11-11T21:54:20"
picture: "werkstuecke1.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/16256
- /detailsf377.html
imported:
- "2019"
_4images_image_id: "16256"
_4images_cat_id: "1465"
_4images_user_id: "373"
_4images_image_date: "2008-11-11T21:54:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16256 -->
Da ich gerade dabei bin, eine kleine Industrienalage zu bauen, habe ich mal ein paar Werkstücke zum Transportieren gebastelt. Hier sieht man ein Rohstück, 29x29x15mm groß. Das Ganze kommt aus einer Leiste mit 30x15mm Querschnitt, von der ich noch ein Reststück rumstehen hatte. Also Abgeschnitten, mit dem Bandschleifer zurechtgeschliffen, Kanten gefast und mit Dispersionsfarbe zweimal angepinselt. Fertig ist das Ganze.