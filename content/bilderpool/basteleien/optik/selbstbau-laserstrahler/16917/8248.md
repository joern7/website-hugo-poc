---
layout: "comment"
hidden: true
title: "8248"
date: "2009-01-10T20:15:13"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Volker-James,

Auf dem Conrad-Datenblatt fehlt doch die Angabe des Stromes bei einer angelegten Spannung von 3 Volt ?!..... 

Ich habe 0,01 á 0,02 mA gemessen mit einem Laboratorium Power Supply.
Also durchschnitlich etwa 15 mA Strom.

Gruss,

Peter Damen
Poederoyen NL