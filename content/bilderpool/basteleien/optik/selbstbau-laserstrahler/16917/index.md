---
layout: "image"
title: "Laserstrahler 01"
date: "2009-01-07T15:12:37"
picture: "laserstrahler01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16917
- /details19e7.html
imported:
- "2019"
_4images_image_id: "16917"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16917 -->
Will man eine längere Lichtschrankenstrecke in fischertechnik nachbilden, bemerkt man sehr bald, dass die Intensität des ausgesendeten Lichtes schon nach kurzer Entfernung nicht mehr ausreicht, einen Fotowiderstand oder Fototransistor ansprechen zu lassen. Ein Punkt-Laserstrahl als Lichtquelle kann die Lösung sein. Gebündeltes, sehr intensives, punktförmiges Licht kann große Strecken überwinden. Die Abbildung zeigt alle Teile, die für einen Punkt-Laserstrahler erforderlich sind, der in fischertechnik integriert und betrieben werden kann.