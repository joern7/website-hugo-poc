---
layout: "image"
title: "Laserstrahler 11"
date: "2009-01-07T15:12:38"
picture: "laserstrahler12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16928
- /details2b41.html
imported:
- "2019"
_4images_image_id: "16928"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16928 -->
Dieses und das nächste Bild zeigen eine mögliche Anwendung des Punkt-Laserstrahlers zur Positionsmessung des Drehkranzes für den Cube-Solver von MarMac.

Die unterste Lage des Drehkranzes enthält Bausteine 30 mit Bohrung, ft# 32880. Nach jeder 90-Grad Drehung stehen die Bausteine genau gegenüber und die Bohrungen
geben den Weg für den Laserstrahl frei. Dieser "Lichtblitz" wird von einem Fototransistor, ft# 36134, ausgewertet (Fototransistor schaltet durch). Der Fototransistor, ft# 36134, wird durch eine weiße Leuchtkappe, ft# 31320, vor der Zerstörung durch den intensiven Laserstrahl geschützt.

Die eigentlich weiße Leuchtkappe strahlt auf dem Bild, trotz Blitzlicht, "leicht-rosa", da diese Leuchtkappe von dem roten Laserstrahl angeregt wird.

Denkbares Mess- und Auswerteszenario:
Drehkranz dreht los in der gewünschten Drehrichtung. Mit einer Verzögerung von ca. 1 Sekunde wird der Laserstrahl eingeschaltet, um eine weitere Auswertung der letzten Position zu vermeiden. Warten bis der Laserstrahl durchlaufen wurde (Rechteckfunktion), anhalten, Rücklauf mit langsamerer Geschwindigkeit, um den Nachlauf des Motores für den Drehkranz so gering wie möglich zu halten. Warten bis der Fototransistor erneut anspricht (Flankenfunktion). Dann Motor und Laserstrahl aus.
