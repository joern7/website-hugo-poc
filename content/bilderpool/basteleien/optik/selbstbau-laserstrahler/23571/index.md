---
layout: "image"
title: "Kurzer Aufbau"
date: "2009-04-03T08:35:30"
picture: "Profi_Laser_Tech_2_001.jpg"
weight: "15"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/23571
- /details3fcc.html
imported:
- "2019"
_4images_image_id: "23571"
_4images_cat_id: "1523"
_4images_user_id: "473"
_4images_image_date: "2009-04-03T08:35:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23571 -->
Läßt mir jetzt keine Ruhe...Habe mal schnell ein paar Bausteine und eine Zigarre (morgens !) angesteckt....ich hoffe man sieht etwas)

Gruß Micha