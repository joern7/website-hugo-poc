---
layout: "image"
title: "Laserstrahler 09"
date: "2009-01-07T15:12:38"
picture: "laserstrahler10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16926
- /detailsfed5-2.html
imported:
- "2019"
_4images_image_id: "16926"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16926 -->
Der erforderliche Vorwiderstand von 200 Ohm wird in die positive, rote Zuleitung eingelötet und durch ein Stück roten Schrumpfschlauch gesichert und geschützt. Die positive, rote Zuleitung wird mit einem roten Flachstecker, ft# 31337, und die negative, weiße Zuleitung wird mit einem grünen Flachstecker, ft# 31336, versehen. Abschließend werden die vorgesehenen Warnhinweise für die Laserklasse 2 auf den Störlichttubus aufgeklebt. Die schon im letzten Bild auf den Störlichttubus aufgesetzte Störlichtkappe bleibt aufgesetzt, da sie den direkten Blick auf den Laserstrahl erschwert.

Der selbstgebaute Laserstrahler zur Verwendung in fischertechnik Modellen ist nun kein Kinderspielzeug mehr!
