---
layout: "image"
title: "Laserstrahler 06"
date: "2009-01-07T15:12:38"
picture: "laserstrahler07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16923
- /detailsfce2.html
imported:
- "2019"
_4images_image_id: "16923"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16923 -->
Zur Vorgabe der Maße wurde eine Einheit aus Baustein 5, Baustein 15, Leuchtstein und Störlichtkappe gebildet und auf die Klebeschablone aufgebaut.