---
layout: "image"
title: "ft-Teile-Bearbeitung offiziell erlaubt ;)"
date: "2009-07-13T17:57:22"
picture: "ft_modding.jpg"
weight: "11"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["teil", "teile", "bearbeitet", "bearbeiten", "modding"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/24574
- /details871f.html
imported:
- "2019"
_4images_image_id: "24574"
_4images_cat_id: "463"
_4images_user_id: "120"
_4images_image_date: "2009-07-13T17:57:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24574 -->
Gefunden in der Anleitung zu u-t 3 (Version mit dem kleinen Relais) "Handhabung der Bauelemente des Elektromechanikbaukastens" auf Seite 11 

Ein harter Schlag für alle Verfechter der "ft Teile dürfen nicht verändert werden" Fraktion, finde ich :)

Da wird sich Harald aber freuen...