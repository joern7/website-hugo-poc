---
layout: "comment"
hidden: true
title: "9569"
date: "2009-07-17T15:27:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich verstehe das Teilemodding, bringe es aber halt selbst nicht fertig, und das hat einen Grund: Ein ft-Bauteil ist für mich nicht ein Stück Plastik. Es ist mehr. Es hat einen ideellen Wert für mich, trägt jahrzehntelange Erinnerungen bis zurück in die Kindheit und erwarb sich dadurch eine nahezu unantastbare Würde. Niemals würde ich da Hand anlegen. Das ist natürlich schon etwas krank ;-) aber ich kann aus dieser Haut bislang nicht raus.

Die Sache hat aber noch einen anderen Aspekt, ganz ähnlich wie Mathematiker, die sich weigern, Beweise durch Widerspruch als korrekte Beweisform zu akzeptieren - und stattdessen nämlich versuchen, dieselben Sätze direkt zu beweisen. Sie kommen ziemlich weit damit, nur halt nicht überhall hin.

Und so ist es meiner Meinung nach mit dem Teilemodding auch: Wenn ich mir selbst die Beschränkung auferlege, ausschließlich unveränderte ft-Teile zu verwenden, dann muss ich für manche Dinge länger tüfteln, um eine Lösung zu finden, die mit einer Teilemodifikation vielleicht ganz einfach wäre. Viele Dinge gehen aber durchaus damit, man braucht vielleicht nur länger (oder auch nicht). Es bleibt ein - winzig kleiner - Rest, für den man vielleicht tatsächlich keine Lösung mit Originalteilen *findet* (was ja immer noch lange nicht heißt, dass es keine *gibt*). Damit muss ich dann natürlich leben, aber das gelingt ganz gut. :-)

Also jeder halt wie er mag. Ein Gesetz gibt's da ja zum Glück nicht. :-)

Gruß,
Stefan