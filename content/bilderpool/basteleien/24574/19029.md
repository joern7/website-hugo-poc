---
layout: "comment"
hidden: true
title: "19029"
date: "2014-05-07T15:15:56"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
p.s.
ansonsten habe ich beim modden eine relativ geringe Hemmschwelle! Ich würde natürlich keine kostbaren/seltenen Teile verstümmeln, aber wer hat nicht auch Teile im Baukasten, die eh schon Macken haben, wo ein Zapfen abgebrochen ist, bei dem die Klemmung miserabel ist...sowas nehme ich dann immer her für modding-Versuche