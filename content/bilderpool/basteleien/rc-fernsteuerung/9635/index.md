---
layout: "image"
title: "Beispiel Servoeinbau mit Clipachse"
date: "2007-03-20T18:02:59"
picture: "Servoeinbau_mit_Clipachse_1.jpg"
weight: "14"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9635
- /details5a70.html
imported:
- "2019"
_4images_image_id: "9635"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T18:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9635 -->
