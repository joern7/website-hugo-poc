---
layout: "image"
title: "Fertiges Modell 2"
date: "2007-11-22T17:42:54"
picture: "laderaupemitrcfernsteuerung3.jpg"
weight: "3"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12795
- /details4937.html
imported:
- "2019"
_4images_image_id: "12795"
_4images_cat_id: "1153"
_4images_user_id: "672"
_4images_image_date: "2007-11-22T17:42:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12795 -->
Hier sieht man gut, wie die Schaufel per Servo und Schneckenantrieb gehoben wird.

Zur Steuerung verwende ich einen alten 4-Kanal-Sender mit den Kettenantrieben auf den den beiden Steuerknüppeln. Zum Heben und Senken der Schaufel tut es ein Schaltkanal am Sender, wenn keine stufenlose Geschwindigkeitsregelung bei der Schaufelbewegung gewünscht ist.