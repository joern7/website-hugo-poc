---
layout: "overview"
title: "FT Truck (80er) mit RC-Fernsteuerung"
date: 2020-02-22T07:45:22+01:00
legacy_id:
- /php/categories/2878
- /categoriesf4e5.html
- /categories9d92.html
- /categoriesc777.html
- /categories2747.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2878 --> 
einige Komponenten sind Original:
Servo (80er)
Radantrieb & Differntial (80er)
Dazu habe ich denn einen Futaba/Robbe Empfänger
eine Servo-Elektronik aus einem alten Modellbauservo zum Ansteruern des FT-Servos
und einen Leistungsfahrregler (aus Platzgründen Kühlkörper entfernt) in einem leeren Powerblock-Gehäuse gepackt.
Stromversorgung:
1x 9V Akkupack FT für den Fahrantrieb
1x4,8V Akkupack aus dem Modellbaubedarf für Empfänger & Servo

(form follows function) d.h. später mal in Schön ;-)

weitere Kanäle für Motoren / Lampen rüste ich noch nach!