---
layout: "image"
title: "Lenkung / Servo"
date: "2014-04-13T18:18:16"
picture: "IMG_0005.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38547
- /details3cb1.html
imported:
- "2019"
_4images_image_id: "38547"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38547 -->
"klassisch" angelenkt bzw. Aufgebaut nach Anleitung (siehe z.B. Bauanleitung zum Universalfahrzeug)