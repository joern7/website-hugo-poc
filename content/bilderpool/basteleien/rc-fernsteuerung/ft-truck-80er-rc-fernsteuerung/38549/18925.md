---
layout: "comment"
hidden: true
title: "18925"
date: "2014-04-15T15:15:27"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Jetzt verstehe ich die vielen Akkus im Fahrerhaus :)

Ich nehme z.B. 4 Baby-Akkus von dem Typ:
http://www.pollin.de/shop/dt/NTM5ODI3OTk-/Stromversorgung/Akkus/NiMH_Akkus/NiMH_Baby_Akkus_XAVAX_3500_mAh_2_Stueck.html

Zusätzliche 2 Stück habe ich in den Tanks an der Seite versteckt, siehe Bilder zu meinem RC-Truck von der ftc-Convention 2013...

Gruß,
Thomas