---
layout: "image"
title: "Hauptschalter, direkt hinter dem Akku, um alles abzuschalten"
date: "2014-04-27T16:09:00"
picture: "IMG_0111.jpg"
weight: "20"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38712
- /details09d1.html
imported:
- "2019"
_4images_image_id: "38712"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38712 -->
dieser Schalter entstammt dem alten 80er Original RC-Fernsteuerungsset von Fischertechnik, an den Eingang habe ich 2 Stecker montiert, am ausgang 2 Buchsen..