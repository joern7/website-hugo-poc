---
layout: "image"
title: "neue Version mit nur einer Akkuversorgung"
date: "2014-04-27T16:09:00"
picture: "IMG_0106.jpg"
weight: "8"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Fischertechnik", "RC", "Truck", "Fernsteuerung", "mit", "Modellbau-Fernsteuerungen"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38700
- /details6df7.html
imported:
- "2019"
_4images_image_id: "38700"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38700 -->
Der Wagen wird jetzt komplett nur noch durch den FT Akkupack mit Energie versorgt, der Motorfahrregler hat ein sog. "BEC" - damit lässt sich der RC-Empfänger (4,8-6V) aus dem Fahrakku ohne zusätzlichen Verkabelungsaufwand versorgen.