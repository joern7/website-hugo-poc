---
layout: "image"
title: "Shield"
date: "2016-05-06T10:15:54"
picture: "aire7.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43341
- /detailsb0fd.html
imported:
- "2019"
_4images_image_id: "43341"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43341 -->
Die Platine wurde mit einer CNC Fräsmaschine erstellt: Zunächst wurde zur Isolation die Kupferschicht der Hartpapierplatine um die Leiterbahnen herum entfernt, im Anschluss wurden Löcher mit dem Durchmesser 0.85 und 1 mm gefräßt. Abschließend wurde die Platine ausgefräst, bestückt und gelötet.