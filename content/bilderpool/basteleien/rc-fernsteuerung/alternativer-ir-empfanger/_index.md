---
layout: "overview"
title: "Alternativer IR Empfänger"
date: 2020-02-22T07:45:25+01:00
legacy_id:
- /php/categories/3219
- /categories0ffd.html
- /categories8e95-2.html
- /categories531c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3219 --> 
Dies ist ein alternativer Infrarot Empfänger für 38kHz Signale bestehend aus einem Arduino Uno SMD Edition und einem eigens dafür entwickelten Shield. Er kann das Signal des fischertechnik Control Sets demodulieren, grundsätzlich funktioniert dies auch mit Signalen einer beliebigen Universalfernbedienung beispielsweise von TV Geräten. 