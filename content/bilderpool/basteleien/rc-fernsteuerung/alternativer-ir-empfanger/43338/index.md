---
layout: "image"
title: "Gehäuse und Schaltung"
date: "2016-05-06T10:15:54"
picture: "aire4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43338
- /details3961.html
imported:
- "2019"
_4images_image_id: "43338"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43338 -->
Das Gehäuse wurde so gestaltet, dass der Arduino und das Shield durch die Gehäuseinnenseiten und die Ausparungen in der Bodenplatte sowie im Deckel fixiert werden. Ohne zu Schrauben, zu kleben oder die Verwendung ähnlicher Fügetechniken sitzen alle Komponenten fest an ihrem Platz und es kann absolut nichts verrutschen.