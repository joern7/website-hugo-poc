---
layout: "image"
title: "Retro BBC Buggy"
date: "2013-04-13T09:20:23"
picture: "retroBuggy4.jpg"
weight: "8"
konstrukteure: 
- "XBach"
fotografen:
- "XBach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/36826
- /details63b8.html
imported:
- "2019"
_4images_image_id: "36826"
_4images_cat_id: "1143"
_4images_user_id: "427"
_4images_image_date: "2013-04-13T09:20:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36826 -->
Retro Buggy von Economatics 1985 mit IR Steuerung, LiIon-Akkus
und Servos statt Schrittmotoren und ohne Alu ;-).