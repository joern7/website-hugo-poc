---
layout: "image"
title: "Servo-Einbau (oben)"
date: "2015-02-02T17:12:59"
picture: "StdServoEingebautOben.jpg"
weight: "4"
konstrukteure: 
- "Robert Feneberg"
fotografen:
- "Robert Feneberg"
keywords: ["Servo", "RC"]
uploadBy: "rfeneberg"
license: "unknown"
legacy_id:
- /php/details/40435
- /detailsa4db.html
imported:
- "2019"
_4images_image_id: "40435"
_4images_cat_id: "1211"
_4images_user_id: "347"
_4images_image_date: "2015-02-02T17:12:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40435 -->
Einbau eines Standardservos in ein Fahrzeug