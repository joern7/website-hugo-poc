---
layout: "image"
title: "RC Buggy 1"
date: "2007-03-20T16:49:26"
picture: "Buggy1.jpg"
weight: "1"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9622
- /details5789.html
imported:
- "2019"
_4images_image_id: "9622"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9622 -->
Der Buggy aus dem Automobiles Kasten mit 2-Kanal-RC-Steuerung