---
layout: "image"
title: "Screenshot der Android-App"
date: "2015-02-21T21:37:48"
picture: "AppScreenshot.png"
weight: "3"
konstrukteure: 
- "Philipp Kaufmann"
fotografen:
- "Philipp Kaufmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "phk"
license: "unknown"
legacy_id:
- /php/details/40579
- /details4f9f.html
imported:
- "2019"
_4images_image_id: "40579"
_4images_cat_id: "3043"
_4images_user_id: "2379"
_4images_image_date: "2015-02-21T21:37:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40579 -->
