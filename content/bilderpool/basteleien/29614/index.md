---
layout: "image"
title: "3D-Studie Hochseeschlepper, [3/3] Schlepptauwinden"
date: "2011-01-05T19:20:47"
picture: "dstudiehochseeschlepper3.jpg"
weight: "24"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/29614
- /details6481.html
imported:
- "2019"
_4images_image_id: "29614"
_4images_cat_id: "463"
_4images_user_id: "723"
_4images_image_date: "2011-01-05T19:20:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29614 -->
Die beiden Schlepptauwinden auf dem Achterdeck:
Da es auf Deck eng zugeht, habe ich aus Gründen des Arbeitsschutzes :o) die Zahnradgetriebe m0,5 spiegelbildlich nach innen verlegt. Dazu sind die XS-Motoren mit ihren U-Getrieben und die Tauwinden dreidimensional stellbar. Zwei unabhängige Schlepptauwinden ermöglichen z.B. computergesteuert im Schlepp auch ein an Antrieb und Steuerung havariertes Schiff auf Kiellinie des Schleppers zu halten.

Fazit:
Das vorläufige Ergebnis der Studie läßt auf einen mal erfolgreichen praktischen Modellaufbau einschließlich Deckplanken und Reling schließen. Die erforderlichen Akkus (kein ft-Akku) dürften unter Deck ohne Schieflastigkeit des Schiffes verteilbar sein. Die Auswahl der Schiffsmotoren für Antrieb und Steuerung ist konzeptionell zwischen XM- oder Encodermotor noch offen. Passende Schiffsschrauben sind im Internet erhältlich. Der Einsatz einer Fernsteuerung bietet sich zunächst an. Wenn der Platz unter Deck den Einbau eines ROBO TX Controllers bei der Bugschraubenversion ermöglicht, sind hier ähnlich den Fahrrobotern interessante Steuerungen denkbar ...