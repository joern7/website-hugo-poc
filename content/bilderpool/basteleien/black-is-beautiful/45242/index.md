---
layout: "image"
title: "Eine sogenannte Blackbox"
date: "2017-02-16T23:31:26"
picture: "bib3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45242
- /details93fd.html
imported:
- "2019"
_4images_image_id: "45242"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45242 -->
Habe ich so noch nie gesehen und selbst beim FischerFriendsMan nicht gefunden.