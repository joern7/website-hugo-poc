---
layout: "image"
title: "Eine Bodenplatte in schwarz"
date: "2017-02-16T23:31:26"
picture: "bib4.jpg"
weight: "4"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45243
- /details09a6.html
imported:
- "2019"
_4images_image_id: "45243"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45243 -->
Und das alles ohne Unterbodenschutz. Aber trotzdem schwarz.