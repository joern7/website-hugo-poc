---
layout: "image"
title: "Quadratisch, praktisch, schwarz"
date: "2017-02-16T23:31:26"
picture: "bib5.jpg"
weight: "5"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45244
- /details2d1e.html
imported:
- "2019"
_4images_image_id: "45244"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45244 -->
Für das Schachspiel hatte ich mir sie selbstgeschnitzt. Jetzt sind sie auch so da.