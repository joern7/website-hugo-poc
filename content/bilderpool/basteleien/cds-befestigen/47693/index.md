---
layout: "image"
title: "CDs befestigen, Fixierung 1"
date: "2018-06-19T08:26:44"
picture: "2CD_Fixierung_1.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47693
- /details8fe0.html
imported:
- "2019"
_4images_image_id: "47693"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47693 -->
Drehscheibe 60x5,5 (31019) mit 3 Bausteinen 5 auf der Innenseite.
S-Kreuzlasche (31675) mit Riegeln und Riegelscheiben sowie Klemmbuchse.