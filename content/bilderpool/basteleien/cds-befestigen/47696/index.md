---
layout: "image"
title: "CDs befestigen, Jo-Jo"
date: "2018-06-19T08:26:44"
picture: "5Jojo_Aufbau_3.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47696
- /detailse529.html
imported:
- "2019"
_4images_image_id: "47696"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47696 -->
Aus zwei CDs bauen wir nun ein Jo-Jo!