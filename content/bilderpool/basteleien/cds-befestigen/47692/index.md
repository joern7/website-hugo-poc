---
layout: "image"
title: "CDs befestigen, Achsaufnahme"
date: "2018-06-19T08:26:44"
picture: "1Achsaufnahme.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["CD"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47692
- /details855a.html
imported:
- "2019"
_4images_image_id: "47692"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47692 -->
Sie sind einfach da: Jede Menge CDs, die man nicht mehr gebrauchen kann.
Wie wäre es mit der Einbindung in fischertechnik?
Die &#8222;Hülse + Scheibe 15&#8220; rot oder grün transparent (35981, 36701) hat einen Durchmesser, der in das Loch der CD passt. Im Sitz gehalten wird die CD entweder von einer &#8222;Scheibe 4 15&#8220; (105195), die tatsächlich einen Außendurchmesser von 16 mm hat, mit zusätzlicher &#8222;Klemmbuchse&#8220; (37679)  oder einer &#8222;Flachnabe 25&#8220; (31015). Nach Aufschieben der CD muss diese gesichert werden durch ein Abstandselement, wegen der Hülse. Die nächsten Bilder zeigen sechs Vorschläge.