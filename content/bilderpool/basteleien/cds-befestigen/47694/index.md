---
layout: "image"
title: "CDs befestigen, Fixierung 2"
date: "2018-06-19T08:26:44"
picture: "3CD_Fixierung_2.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47694
- /details6bc7.html
imported:
- "2019"
_4images_image_id: "47694"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47694 -->
Ein S-Winkelträger 15 2Z (36304, 36298, 36950 oder 139645), gesichert mit einer Klemmbuchse.
Handkurbel (31026).