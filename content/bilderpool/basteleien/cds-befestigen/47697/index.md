---
layout: "image"
title: "CDs befestigen, Jo-Jo-Aufbau"
date: "2018-06-19T08:26:44"
picture: "6Jojo_Aufbau_2.jpg"
weight: "6"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/47697
- /details9d62.html
imported:
- "2019"
_4images_image_id: "47697"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47697 -->
Das Jo-Jo besteht aus einer Achse 50, Drehscheibe mit 3 BS5, CD, Scheibe mit Hülse, Scheibe, 2 Klemmbuchsen usw. 
Die Schnur wird von einer der beiden Klemmbuchsen so gehalten, dass sie zwischen den beiden Klemmbuchsen nach oben geführt wird.