---
layout: "image"
title: "Mini-Steckverbinder für Rastachsen - Verwendung"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen1.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40428
- /detailsb56a.html
imported:
- "2019"
_4images_image_id: "40428"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40428 -->
Ich wollte euch diese Mini-Steckverbinder vorstellen. Es handelt sich weniger um eine Schnitzerei, sondern vielmehr um eine Feilerei.