---
layout: "image"
title: "Lasche21,3 31668.JPG"
date: "2007-11-27T18:29:22"
picture: "Lasche213_31668.JPG"
weight: "25"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12850
- /details94a5.html
imported:
- "2019"
_4images_image_id: "12850"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12850 -->
Die Spitze der grauen Version taugt prima, um die Lasche in die Lenkklaue 35998 einzufädeln. Das hätte ich früher sehen sollen, denn jetzt sind alle meine grauen mehr oder weniger stark bearbeitet (hier zu sehen am hinteren Ende - da war auch mal eine Spitze dran).