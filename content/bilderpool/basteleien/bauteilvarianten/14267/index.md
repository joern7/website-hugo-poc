---
layout: "image"
title: "Differential"
date: "2008-04-15T18:37:23"
picture: "differentieel2.jpg"
weight: "50"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/14267
- /detailsad6a.html
imported:
- "2019"
_4images_image_id: "14267"
_4images_cat_id: "1119"
_4images_user_id: "764"
_4images_image_date: "2008-04-15T18:37:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14267 -->
Hier sind 3 varianten.
1 rot 120, 1 rot 110 und 1 rot/schwarz 110.