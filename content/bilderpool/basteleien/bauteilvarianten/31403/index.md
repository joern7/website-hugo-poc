---
layout: "image"
title: "Polwendeschalter"
date: "2011-07-28T21:44:15"
picture: "polwendeschalter1.jpg"
weight: "70"
konstrukteure: 
- "ft"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/31403
- /details5cdb.html
imported:
- "2019"
_4images_image_id: "31403"
_4images_cat_id: "1119"
_4images_user_id: "162"
_4images_image_date: "2011-07-28T21:44:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31403 -->
Alte Graue hat nur 2 Riegel, Neue Schwarze hat 3, aber gestern habe ich ein Graue mit 3 Riegel gefunden.