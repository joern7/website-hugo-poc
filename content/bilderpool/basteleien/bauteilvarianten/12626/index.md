---
layout: "image"
title: "Verschiedene Varinaten der Seiltrommel"
date: "2007-11-11T08:33:13"
picture: "IMG_0293.jpg"
weight: "16"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12626
- /details103c.html
imported:
- "2019"
_4images_image_id: "12626"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12626 -->
Verschiedene Varianten der Seiltrommel (von links nach rechts)

1.) 31016.1 Seiltrommel rot, beide Stirnseiten mit großen Zapfen

2.) 31016.2 Seiltrommel rot, beide Stirnseiten mit unterschiedlicher Zapfenform

3.) 31016.3 Seiltrommel neurot, wie 31016.2 jedoch in Neurot, deutlich blasser als 31016.2