---
layout: "image"
title: "Verschiedene Varianten des großen Tasters"
date: "2007-11-05T15:54:02"
picture: "IMG_0061.jpg"
weight: "7"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12486
- /detailsb4eb.html
imported:
- "2019"
_4images_image_id: "12486"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12486 -->
Verschiedene Varianten des großen Tasters (von links nach rechts)

1.) 31332.1 Taster grau/transparent/rot, alte Bauform mit runder Schaltwippe

2.) 31332.2 Taster grau/transparent/rot, neuere Bauform mit eckiger Schaltwippe

Anmerkung: Die neuere Bauform mit eckiger Schaltwippe eignet sich besser im Einsatz bei beidseitig genutzter Wippfunktion (z.B. als Endtaster, wenn die Wippe von beiden Seiten betätigt werden soll, alles klar!? :-) )