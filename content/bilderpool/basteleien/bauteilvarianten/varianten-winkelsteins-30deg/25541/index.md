---
layout: "image"
title: "Nut-Seite"
date: "2009-10-11T18:13:04"
picture: "variantendeswinkelsteins2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25541
- /details63a8.html
imported:
- "2019"
_4images_image_id: "25541"
_4images_cat_id: "1792"
_4images_user_id: "104"
_4images_image_date: "2009-10-11T18:13:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25541 -->
Hier ist rechts der neuere, größere.