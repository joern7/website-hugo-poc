---
layout: "image"
title: "Verschiedene Varianten der Handkurbel"
date: "2007-11-11T08:33:13"
picture: "IMG_0288.jpg"
weight: "15"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12625
- /detailsf589.html
imported:
- "2019"
_4images_image_id: "12625"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12625 -->
Verschiedene Varianten der Handkurbel (von links nach rechts)

1.) 31026.1 Handkurbel rot

2.) 31026.2 Handkurbel neurot, mit langem Kurbelgriff