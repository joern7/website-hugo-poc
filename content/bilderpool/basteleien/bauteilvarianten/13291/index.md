---
layout: "image"
title: "Zahnstangen"
date: "2008-01-07T17:34:12"
picture: "zahnstange1.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "Pesche65"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13291
- /details5501.html
imported:
- "2019"
_4images_image_id: "13291"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-07T17:34:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13291 -->
dunkelrote Variante nur Grösse 30. Hat jemand Grösse 60 in dunkelrot?