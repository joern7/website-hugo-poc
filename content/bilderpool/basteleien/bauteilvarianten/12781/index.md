---
layout: "image"
title: "Schnecke 35977 (mod).JPG"
date: "2007-11-18T22:13:49"
picture: "Schnecke_35977_mod.JPG"
weight: "23"
konstrukteure: 
- "fischertechnik/Harald"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12781
- /details9ed5.html
imported:
- "2019"
_4images_image_id: "12781"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T22:13:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12781 -->
Die Schnecke mit Stummel und ohne Überstand, kombiniert mit einem BS15 Loch (mod., wurde auf 11 mm heruntergeschnipselt) könnte mal ein Wagenheber werden. Die Schneckenmutter kann hier von Anschlag bis Anschlag verfahren werden, die rote Abstandshülse verschwindet dann völlig darin.