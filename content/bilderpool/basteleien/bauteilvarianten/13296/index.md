---
layout: "image"
title: "Getriebehalter mit Welle"
date: "2008-01-09T17:47:17"
picture: "GetriebehalterMitWelle.jpg"
weight: "38"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
keywords: ["Getriebehalter", "mit", "Welle"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13296
- /details19af.html
imported:
- "2019"
_4images_image_id: "13296"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13296 -->
Getriebehalter mit Welle