---
layout: "comment"
hidden: true
title: "6786"
date: "2008-07-24T13:12:22"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die mit den durchgehenden Stegen waren auch zum Aufklipsen, passten aber halt nur auf Grundbausteine oder Ähnliches, nicht auf Statik.

Gruß,
Stefan