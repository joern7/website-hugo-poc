---
layout: "image"
title: "Verschiedene Winkelsteine"
date: "2007-11-07T21:34:07"
picture: "IMG_0141.jpg"
weight: "12"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12531
- /details6d19.html
imported:
- "2019"
_4images_image_id: "12531"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-07T21:34:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12531 -->
Verschiedene Winkelsteine (von links nach rechts)

1.) 32071 Winkelstein 7,5° neurot
2.) 31981 Winkelstein 15° neurot
3.) 31011 Winkelstein 30° rot
4.) 31012 Winkelstein 30° rechtwinkelig rot
5.) 31010 Winkelstein 60° gleichseitig rot
6.) 38309 Winkelstein 15 60° rot
7.) 31918 Winkelstein 60° mit 3 Nuten