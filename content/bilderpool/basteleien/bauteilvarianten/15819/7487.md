---
layout: "comment"
hidden: true
title: "7487"
date: "2008-10-05T20:44:27"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Diese Teile sind auch nur von mir fotografiert, und nicht konstruiert, weshalb ich bei Konstrukteur auch Fischertechnik angegeben habe.

Über einen runden Federnocken habe ich aber auch schon nachgedacht.
Die alten Steine mit runden Nocken konnten sich aber nicht wirklich durchsetzen, weil sie zu selten gebraucht werden, und nicht zu besonders stabilen Konstruktionen führen.

Die Delle im Zapfen hat wohl die gleiche Funktion wie der Schlitz im Zapfen der Grundbausteine.