---
layout: "image"
title: "Es kann mir kein ft-fan erzählen, das er an so einem Kasten vorbeigeht :-)"
date: "2007-12-23T08:22:49"
picture: "iCARUS_003.jpg"
weight: "32"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/13139
- /details1338.html
imported:
- "2019"
_4images_image_id: "13139"
_4images_cat_id: "1119"
_4images_user_id: "473"
_4images_image_date: "2007-12-23T08:22:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13139 -->
