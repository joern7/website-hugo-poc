---
layout: "image"
title: "Verschiedene graue Stebenvarianten"
date: "2007-11-05T15:54:01"
picture: "IMG_0055.jpg"
weight: "4"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12483
- /details3ded.html
imported:
- "2019"
_4images_image_id: "12483"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12483 -->
Verschiedene graue Strebenvarianten (von links nach rechts)

1.) 36316.1 X-Strebe 63,3 grau, alte Bauform mit glatter Rückseite

2.) 36316.2 X-Strebe 63,6 grau, Bauform mit profilförmiger Rückseite

3.) 36316.3 X-Strebe 63,6 grau, letzte Bauform mit beidseitig gesenkten S-Riegelbohrungen (seltene Bauform!)