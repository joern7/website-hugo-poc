---
layout: "image"
title: "I strebe 84,8"
date: "2008-09-14T10:06:09"
picture: "DSC01617.jpg"
weight: "54"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: ["strebe", "fehler"]
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15231
- /details6390.html
imported:
- "2019"
_4images_image_id: "15231"
_4images_cat_id: "1119"
_4images_user_id: "814"
_4images_image_date: "2008-09-14T10:06:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15231 -->
gerade schief