---
layout: "image"
title: "Verschiedene Federn"
date: "2007-11-14T18:06:46"
picture: "IMG_0333.jpg"
weight: "18"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12754
- /details721f.html
imported:
- "2019"
_4images_image_id: "12754"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-14T18:06:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12754 -->
Verschiedene Federn (von links nach rechts)

1.) 31939.1 bzw. 38634.1 Feder 20 schwarz,
alte Bauart mit eckigem Zapfen

2.) 31939.2 bzw. 38634.2 Feder 20 schwarz,
neue Bauart mit rundem Zapfen