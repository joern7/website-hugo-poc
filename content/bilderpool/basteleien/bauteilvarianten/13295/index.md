---
layout: "image"
title: "Batteriestab"
date: "2008-01-09T17:47:17"
picture: "BatterieStab.jpg"
weight: "37"
konstrukteure: 
- "Peter Aeberhard"
fotografen:
- "Peter Aeberhard"
keywords: ["Batteriestab"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13295
- /details8e67.html
imported:
- "2019"
_4images_image_id: "13295"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13295 -->
Batteriestab-Varianten