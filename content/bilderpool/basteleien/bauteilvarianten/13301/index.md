---
layout: "image"
title: "Zahnrad 40 32 Hinterseite"
date: "2008-01-09T17:47:17"
picture: "Zahnrad4032_1.jpg"
weight: "43"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
keywords: ["Zahnrad", "40", "32"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13301
- /details20e1.html
imported:
- "2019"
_4images_image_id: "13301"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13301 -->
Zahnrad 40 32 Hinterseite, man beachte die 'lange Verzahnung' beim seltenen Rad rechts.