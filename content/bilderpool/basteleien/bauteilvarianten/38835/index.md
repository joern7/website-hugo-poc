---
layout: "image"
title: "Stufengetriebe.jpg"
date: "2014-05-24T14:10:22"
picture: "IMG_0636.JPG"
weight: "74"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38835
- /details9f92.html
imported:
- "2019"
_4images_image_id: "38835"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2014-05-24T14:10:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38835 -->
Das rechte Stufengetriebe hat freien Durchgang für eine ft-Achse. Wie das mit dem Motor zusammen gehen soll, ist mir schleierhaft.