---
layout: "comment"
hidden: true
title: "19705"
date: "2014-11-10T12:51:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Oh, stimmt, das Web-UI zeigt diese Datensätze gar nicht. Das müssen aber die Web-Jungs richten, ich hab da keine Aktien drin. Und dann könnte man das natürlich auch ordentlich beschriften. Also: Das steckt zwar in den Daten, wird aber im Web-UI aktuell wohl nicht angezeigt. Wenn einer "bis" wissen will, mir sagen, ich lass' es dann bei mir aus der Datenbank raus.
Gruß, Stefan