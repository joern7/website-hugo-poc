---
layout: "image"
title: "Haken.JPG"
date: "2007-11-18T21:58:23"
picture: "Haken.JPG"
weight: "20"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12778
- /details45cd.html
imported:
- "2019"
_4images_image_id: "12778"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T21:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12778 -->
Haken in groß (alt) und klein (neu). Die neuen haben auch eine Nut, um das Seil einzuklemmen.