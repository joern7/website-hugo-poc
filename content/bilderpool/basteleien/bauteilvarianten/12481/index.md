---
layout: "image"
title: "Verschiedene Lagerböcke"
date: "2007-11-05T15:54:01"
picture: "IMG_0142.jpg"
weight: "2"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12481
- /details8935.html
imported:
- "2019"
_4images_image_id: "12481"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12481 -->
Verschiedene Lagerböcke (von links nach rechts):
1.) 35668 Radhalter rot
2.) 38254 Kupplung 1 rot
3.) 38253 Kupplungsstück 2 rot
4.) 38260 Kupplungsstück 22,5 rot
5.) 31719 Zylinderanschluss 33,3 rot
6.) 38252 Lagerbock 22,5 rot