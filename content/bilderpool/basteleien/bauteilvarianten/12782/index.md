---
layout: "image"
title: "U-Getriebe.JPG"
date: "2007-11-18T22:18:10"
picture: "U-Getriebe.JPG"
weight: "24"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12782
- /detailsb44f-2.html
imported:
- "2019"
_4images_image_id: "12782"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T22:18:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12782 -->
Die (alte) graue Version links hat einen langgezogenen Zapfen und die Achse sitzt deutliche außerhalb vom ft-Raster. Im neuen Getriebe (rechts, grau und schwarz) ist die Achse richtig positioniert.