---
layout: "comment"
hidden: true
title: "7479"
date: "2008-10-05T14:34:42"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
@fischdidel:
Der 15er ohne Zapfen soll den alten auch nicht ersetzen, sondern ergänzen.
An einigen Stellen könnte er wirklich von Vorteil sein.
Der 30er ohne Zapfen ist wirklich nicht so wichtig, es sei denn, jemand findet dafür eine tolle Anwendung.

@Udo2:
Ich habe auch den original Baustein 30 noch einmal nachgemessen.
Der Abastand zwischen den langen Nuten ist zwar 6,5 mm.
Das Lager der Stirnnut ist aber nur 6 mm lang, da es etwas nach innen versetzt ist.