---
layout: "image"
title: "Zahnrad 20"
date: "2008-01-09T17:47:17"
picture: "Zahnrad20.jpg"
weight: "42"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
keywords: ["Zahnrad", "20"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13300
- /details4f56.html
imported:
- "2019"
_4images_image_id: "13300"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13300 -->
Zahnrad 20