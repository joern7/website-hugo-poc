---
layout: "image"
title: "Verschiedene Seilrollen 21"
date: "2007-11-05T15:54:01"
picture: "IMG_0148.jpg"
weight: "3"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12482
- /detailsbd12.html
imported:
- "2019"
_4images_image_id: "12482"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12482 -->
Verschiedene Seilrollen 21  (von links nach rechts):

1.) 35795.1 Seilrolle 21x7 rot, alte Bauform ohne Seilführung

2.) 35795.2 Seilrolle 21x7, neue Bauform mit Seilführung

3.) 35797 Seilrolle 21x7 schwarz