---
layout: "image"
title: "Taster 37780-37783.JPG"
date: "2007-11-27T18:37:31"
picture: "Taster_37780-37783.JPG"
weight: "28"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12853
- /details9e1b.html
imported:
- "2019"
_4images_image_id: "12853"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:37:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12853 -->
Hier hat man einmal neue Teilenummern vergeben: 37780=alt (links), 37783=neu (rechts).