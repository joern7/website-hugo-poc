---
layout: "image"
title: "Verschiedene Hubzahnstangen"
date: "2007-11-07T21:34:07"
picture: "IMG_0145.jpg"
weight: "10"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12529
- /detailsea65.html
imported:
- "2019"
_4images_image_id: "12529"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-07T21:34:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12529 -->
Verschiedene Hubzahnstangen (von links nach rechts)

1.) 37351.1 Hubzahnstange 60 rot, alte Bauform (es fehlen die Verzahnungen neben der Befestigungsnut)

2.) 37351.2 Hubzahnstange 60 rot, neuere Bauform, mit zusätzlicher Verzahnung neben  der Befestigungsnut

3.) 37351.3 Hubzahnstange 60 neurot
Die neurote Farbgebung wirkt deutlich blasser als das klassische Rot