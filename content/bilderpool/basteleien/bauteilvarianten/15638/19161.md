---
layout: "comment"
hidden: true
title: "19161"
date: "2014-06-16T18:51:27"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Den Link gibt es nicht, weil das damalige Forum nicht mehr existiert (und dessen Vorgänger auch nicht mehr). ft hat es auch trotz allerheftigster Bemühungen nicht geschafft, irgendwelche Inhalte über die Foren-Abschaltungen hinweg zu retten. Zum Schluss gab es sogar noch das Argument, dass die Forenschreiber Copyright hätten und dies einem Einspielen auf neue Server entgegen stünde.

Einziger Trost: den Spruch "das Internet vergisst nichts" konnte Tumlingen nicht nur einmal, sondern zweimal widerlegen. Muaaahaahaa.


mit bittersaurem Gruß,
Harald