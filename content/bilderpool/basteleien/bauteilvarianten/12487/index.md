---
layout: "image"
title: "Verschiedene  Varianten von Flachsteinen"
date: "2007-11-05T15:54:02"
picture: "IMG_0164.jpg"
weight: "8"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12487
- /details593e.html
imported:
- "2019"
_4images_image_id: "12487"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12487 -->
Verschiedene Varianten von Flachsteinen (von links nach rechts):

1.) 31013.1 Flachstein 30x30 rot, alte Bauform mit T-förmigen Nuten

2.) 31013.2 Flachstein 30x30 rot, neuere Bauform mit V-förmigen Nuten

3.) 31555.1 Flachstein 30x60 rot, alte Bauform mit T-förmigen Nuten (recht seltene Ausführung!)

4.) 31555.2 Flachstein 30x60 rot, neuere Bauform mit V-förmigen Nuten