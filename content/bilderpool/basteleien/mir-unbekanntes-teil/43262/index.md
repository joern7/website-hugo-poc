---
layout: "image"
title: "Baustein 32227 _grau Bosch Renner"
date: "2016-04-08T21:38:56"
picture: "Bosch_Renner_IMG_5391.jpg"
weight: "8"
konstrukteure: 
- "Roland Enzenhofer"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43262
- /detailsd10d.html
imported:
- "2019"
_4images_image_id: "43262"
_4images_cat_id: "2865"
_4images_user_id: "1688"
_4images_image_date: "2016-04-08T21:38:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43262 -->
Baustein 32227 _grau Bosch Renner