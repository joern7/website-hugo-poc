---
layout: "image"
title: "unbekannter Sensor"
date: "2017-05-15T12:07:26"
picture: "Sensor_unbekannt.jpg"
weight: "12"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/45810
- /details07d1.html
imported:
- "2019"
_4images_image_id: "45810"
_4images_cat_id: "2865"
_4images_user_id: "10"
_4images_image_date: "2017-05-15T12:07:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45810 -->
diesen Sensor habe ich in einem Konvolut gefunden. Graues Lampengehäuse mit einem kleinen Sensor, ähnlich eines LDR Wiederstands.
Wo gehört dieser Sensor hin?