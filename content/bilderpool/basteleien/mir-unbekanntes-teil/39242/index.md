---
layout: "image"
title: "Motor (6V) mit Stift"
date: "2014-08-14T20:14:12"
picture: "Foto.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Richter"
keywords: ["unbekannt", "Motor", "6v"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/39242
- /details8c70.html
imported:
- "2019"
_4images_image_id: "39242"
_4images_cat_id: "2865"
_4images_user_id: "2179"
_4images_image_date: "2014-08-14T20:14:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39242 -->
In meiner Sammlung fand ich diesen Motor. Es befindet sich jedoch keine Schnecke an ihm sondern lediglich dieser Stift.