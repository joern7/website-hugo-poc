---
layout: "image"
title: "Verbindungslasche"
date: "2016-05-01T21:19:44"
picture: "Verw_Strebe_klein.jpg"
weight: "11"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/43333
- /details9f6f.html
imported:
- "2019"
_4images_image_id: "43333"
_4images_cat_id: "2865"
_4images_user_id: "10"
_4images_image_date: "2016-05-01T21:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43333 -->
Als Ergänzung habe ich hier noch mal die Verwendung der 6x60 Strebe dargestellt. Als Verbinder zweier Kreuzknotenplatte im Hobby S Kasten