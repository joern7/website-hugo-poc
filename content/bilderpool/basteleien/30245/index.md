---
layout: "image"
title: "Zahnrad Z20 mit Madenschraube"
date: "2011-03-13T11:15:31"
picture: "zahnradzmitmadenschraube1.jpg"
weight: "26"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30245
- /details0ec4.html
imported:
- "2019"
_4images_image_id: "30245"
_4images_cat_id: "463"
_4images_user_id: "162"
_4images_image_date: "2011-03-13T11:15:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30245 -->
Zahnrad Z20 mit Madenschraube damit eine Feste Antrieb gemacht worden kann.