---
layout: "image"
title: "4"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen04.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27575
- /details7805.html
imported:
- "2019"
_4images_image_id: "27575"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27575 -->
Oben ist der Motor, und über die Ketten kann man die Flasche umdrehen.