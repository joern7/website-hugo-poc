---
layout: "image"
title: "2"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen02.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27573
- /details64f3.html
imported:
- "2019"
_4images_image_id: "27573"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27573 -->
Hier Steuerung mit mehreren Tastern. Man konnte z.b. 180 Grad drehen oder langsam manuell drehen (Die Flasche).