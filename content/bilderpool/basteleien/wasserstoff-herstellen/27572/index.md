---
layout: "image"
title: "1"
date: "2010-06-25T18:21:27"
picture: "wasserstoffherstellen01.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27572
- /details192e.html
imported:
- "2019"
_4images_image_id: "27572"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27572 -->
Hier meine erste Anlage zum Herstellen von Wasserstoff. 
Wenn man zwei blanke Drähte in Salzwasser taucht, entsteht am Pluspol Sauer- und am Minuspol Wasserstoff. Über dem Minuspol habe ich eine kleine Flasche, in der der Wasserstoff aufgefangen wird. Dann kann man sie elektrisch umdrehen und der Wasserstoff kommt in Blasen an die Wasseroberfläche. Wenn man an die Blasen nun ein Feuerzeug hält, platzen sie und der Wasserstoff explodiert mit einem kleinen Buummmm! Man sollte aber ein Stabfeuerzeug verwenden da es sonst heiß werden kann! (Eigene Erfahrung). Mit einer Kerze geht es nicht, weil erstens das Wachs ins Wasser tropft und zweitens weil, wenn man die Kerze schräg nach unten hält, der Docht zuerst die Blase aufsticht und der Wasserstoff nur mit der Luft reagiert und das kaum zu hören ist.