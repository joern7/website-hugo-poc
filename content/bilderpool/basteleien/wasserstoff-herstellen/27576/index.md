---
layout: "image"
title: "5"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen05.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27576
- /details569f.html
imported:
- "2019"
_4images_image_id: "27576"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27576 -->
So sieht es aus wenn der Wasserstoff erzeugt wird. Da die Flasche ein bisschen durchsichtig ist, sieht man wie viel Wasserstoff schon erzeugt wurde.