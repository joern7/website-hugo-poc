---
layout: "overview"
title: "Wasserstoff herstellen"
date: 2020-02-22T07:46:26+01:00
legacy_id:
- /php/categories/1983
- /categoriesdce0.html
- /categoriesa352.html
- /categoriesa47d-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1983 --> 
Vor ungefähr drei Wochen war unsere Schulklasse in der Phänomenta in Flensburg. Vielleicht kennen das ja einige. Es ist eine Ausstellung physikalischer Experimente zum selber machen. Jetzt sollten wir alle über eine Sache vor der Klasse ein Referat halten. Ich hatte mir einen Elektrolyseur ausgesucht, mit dem man Wasserstoff herstellen konnte, der anschließend verbrannt wurde. Den Strom dafür erzeugte man mit einem Dynamo. Den Elektrolyseur konnte ich so schnell nicht nachbauen (möchte ich aber irgendwann mal machen) fand aber nur das Referat ein bisschen langweilig. Deshalb habe ich so etwas ähnliches nachgebaut. Auf den Bilder kann man es sehen. Viel Spaß;-)