---
layout: "image"
title: "3"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen03.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27574
- /details3948-2.html
imported:
- "2019"
_4images_image_id: "27574"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27574 -->
Hier wird angezeigt ob man den alten Fischertechnik-Trafo richtig gepolt eingeschaltet hat. Eigentlich ganz einfach, nur ein paar LEDs.