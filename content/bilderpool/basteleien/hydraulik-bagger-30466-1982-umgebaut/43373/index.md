---
layout: "image"
title: "Lagerung des 1. Geberzylinders"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43373
- /details4736.html
imported:
- "2019"
_4images_image_id: "43373"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43373 -->
in der oberen Strbe habe ich den Lagerpunkt des zylinders im Vergleich zum Original Richtung Fahrerhaus um 1 Loch verschoben.
