---
layout: "image"
title: "Original Bauanleitung (c) Fischerwerke"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43366
- /details120e.html
imported:
- "2019"
_4images_image_id: "43366"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43366 -->
