---
layout: "image"
title: "Der komplette umgebaute Hydraulik-Bagger"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43378
- /detailsf68a.html
imported:
- "2019"
_4images_image_id: "43378"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43378 -->
Hydraulik-Bagger (30466) mit erweiterter Arbeitshöhe - das Problem war, das der Bagger nie in seinem Standard-Aufbau nach Plan den Hydraulik-Kipper oder Anhänger beladen konnte. Durch Erweiterung mit je einem Geber - und Nehmer-Zylinder ist es mir gelungen, die Einsatzfähigkeit herzustellen und den Spielspass für die Jungs zu erhöhen ...