---
layout: "image"
title: "geöffneter Baustein"
date: "2017-04-21T22:10:32"
picture: "doppelrelaisbaustein3.jpg"
weight: "3"
konstrukteure: 
- "Titanschorsch"
fotografen:
- "Titanschorsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Titanschorsch"
license: "unknown"
legacy_id:
- /php/details/45784
- /detailsbd8b.html
imported:
- "2019"
_4images_image_id: "45784"
_4images_cat_id: "3402"
_4images_user_id: "1615"
_4images_image_date: "2017-04-21T22:10:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45784 -->
Offener Baustein