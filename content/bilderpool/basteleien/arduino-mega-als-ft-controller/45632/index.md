---
layout: "image"
title: "Nut"
date: "2017-03-23T11:41:09"
picture: "amafc3_2.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45632
- /details6fc2.html
imported:
- "2019"
_4images_image_id: "45632"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45632 -->
ft Zapfen haben in der nachgeahmten ft Nut einen guten halt, ohne dass sie stark belastet werden
Video: https://www.youtube.com/watch?v=UdzuQfM4sNw