---
layout: "image"
title: "Beschriftete Abdeckung"
date: "2017-03-23T11:41:10"
picture: "amafc6_2.jpg"
weight: "13"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45635
- /details1400.html
imported:
- "2019"
_4images_image_id: "45635"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45635 -->
Gravurplatte als beschriftete Abdeckung des Gehäuses
Das Video https://www.youtube.com/watch?v=UdzuQfM4sNw zeigt in einem kurzen Abschnitt, wie die CNC Maschine die Schrift eingraviert.