---
layout: "image"
title: "Arduino Mega als ft Controller"
date: "2016-07-28T16:54:28"
picture: "amafc1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44038
- /details9efa.html
imported:
- "2019"
_4images_image_id: "44038"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44038 -->
Die Grundidee bestand darin, einen Open Source Controller für Fischertechnik nutzbar zu machen, so dass er nach außen wie ein Fischertechnik Controller aussieht. Die Wahl des Controllers fiel schnell auf den Arduino Mega, da dieser über eine sehr gute Entwicklungsumgebung programmiert werden kann, flexibel ist und eine hohe Anzahl an Pins besitzt.
Das Projekt umfasst die Entwicklung einer für den Mega 2560 R3 abgestimmten Shieldplatine sowie die Produktion eines Fischertechnik kompatiblen Gehäuses. Die Platine soll die Motor- und Aktorsteuerung so übernehmen, wie man es von TX(T) und co. gewöhnt ist. Zusätzlich sollen alle Fischertechnik Sensoren mit dem Controller verbunden werden können.
Enstanden ist ein Controller mit 8 (+2) Motorausgängen, 12 digitalen I/O Pins, 4 analoge Pins mit Pull-Down Widerstand und 12 analog Nutzbare I/O Pins. Über eine Erweiterung kann man zusätzlich 6 Leistungsausgänge und zwei weitere Motoren steuern. Das Shield unterstützt das Serial Parallel Interface (SPI, z.B. für SD Karten), i2c (Two Wire Interface), und serielle Kommunikation über USB mit dem PC. Zusätzlich existiert eine Notabschaltung der Aktoren.

Weshalb der Aufwand? Der Controller zielt darauf ab, größere Industriemodelle zu steuern. Mit Herstellungskosten von unter 50 ? (25? Arduino, rund 20 ? ICs und weitere Kleinteile) ist der Controller eine attraktive Alternative zum TXT Controller.