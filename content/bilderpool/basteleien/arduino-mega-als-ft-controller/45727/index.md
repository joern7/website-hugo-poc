---
layout: "image"
title: "Erweiterungsmodul"
date: "2017-04-10T16:31:32"
picture: "amecg2.jpg"
weight: "16"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45727
- /details0f00.html
imported:
- "2019"
_4images_image_id: "45727"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-04-10T16:31:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45727 -->
8 zusätzliche Ausgänge
2 LEDs