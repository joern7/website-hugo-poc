---
layout: "image"
title: "Arduino Mega"
date: "2016-07-28T16:54:28"
picture: "amafc2.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44039
- /details2c72.html
imported:
- "2019"
_4images_image_id: "44039"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44039 -->
Herzstück ist der Arduino Mega R3 Mikrocontroller. Allerdings ist das Shield zu 90% auch mit dem neueren Arduino Due kompatibel, der deutlich schneller ist und mehr externe Interrupts bietet.