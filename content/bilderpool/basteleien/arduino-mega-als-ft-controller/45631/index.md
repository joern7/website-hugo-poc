---
layout: "image"
title: "Stecker"
date: "2017-03-23T11:41:09"
picture: "amafc2_2.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45631
- /detailsae63.html
imported:
- "2019"
_4images_image_id: "45631"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45631 -->
Die Fischertechnik Stromversorgung und herkömmliche Flachstecker können direkt und einfach angeschlossen werden.
Im Video https://www.youtube.com/watch?v=UdzuQfM4sNw wird die Handhabung des Stecksystems gezeigt.