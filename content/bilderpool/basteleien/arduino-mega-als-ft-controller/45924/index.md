---
layout: "image"
title: "Vergleich Tiny TX - TXC"
date: "2017-05-28T16:45:03"
picture: "tinytx2.jpg"
weight: "19"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45924
- /detailsfa8a.html
imported:
- "2019"
_4images_image_id: "45924"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-05-28T16:45:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45924 -->
Der Tiny TX ist nur 60 x 60 mm groß