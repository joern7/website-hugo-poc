---
layout: "comment"
hidden: true
title: "23342"
date: "2017-04-12T14:24:53"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hello Marspau,

unfortunately, it is not. I have produced the control myself including soldering, milling and 3D printing. It was supposed to be a DIY project, which makes programming easier. If you want to recreate the controller, I can provide some additional information.

Best regards
David