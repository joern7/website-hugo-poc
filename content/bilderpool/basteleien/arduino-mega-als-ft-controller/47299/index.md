---
layout: "image"
title: "Schrittmotortreiber"
date: "2018-02-17T19:05:24"
picture: "step1.jpg"
weight: "21"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47299
- /detailsa04c.html
imported:
- "2019"
_4images_image_id: "47299"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2018-02-17T19:05:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47299 -->
Links: Zweite Version des Arduino Mega Shields
- 30 % kleiner
- Anzahl Eingänge rationalisiert
- verbesserte Motorsteuerung
- RGB LED
- optimiertes Platinenlayout

Rechts: Schrittmotortreiber als Erweiterungsbaustein: Video: https://youtu.be/kjhINmJub4k
- basiert auf zwei A4988 Schrittmotortreibern
- Verbindung zum Arduino über 10poliges Flachbandkabel