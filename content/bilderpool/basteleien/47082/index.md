---
layout: "image"
title: "ftCameraZ30"
date: "2018-01-13T15:59:13"
picture: "ftCameraZ30.jpg"
weight: "39"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Torsten Stuehn"
keywords: ["ftDigiCam", "Z30", "Zahnrad"]
uploadBy: "Torsten"
license: "unknown"
legacy_id:
- /php/details/47082
- /details5253.html
imported:
- "2019"
_4images_image_id: "47082"
_4images_cat_id: "463"
_4images_user_id: "2453"
_4images_image_date: "2018-01-13T15:59:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47082 -->
Zahnrad Z30 zum aufschieben auf die fischertechnik Kamera, gedruckt mit dem fischertechnik 3D-Drucker (z.B. zur Verwendung mit der ftDigiCam aus ft:pedia 1/2016)