---
layout: "image"
title: "Fischergeometric 5000"
date: "2016-05-30T14:53:40"
picture: "Geo_1_klein_2.jpg"
weight: "4"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/43444
- /details9483.html
imported:
- "2019"
_4images_image_id: "43444"
_4images_cat_id: "3171"
_4images_user_id: "10"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43444 -->
