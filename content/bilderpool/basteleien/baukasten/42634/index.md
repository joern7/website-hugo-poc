---
layout: "image"
title: "(Mein) Grundkasten 400 zum 50. ft-Geburtstag"
date: "2015-12-31T21:59:59"
picture: "IMG_3233a.jpg"
weight: "1"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42634
- /detailse420.html
imported:
- "2019"
_4images_image_id: "42634"
_4images_cat_id: "3171"
_4images_user_id: "740"
_4images_image_date: "2015-12-31T21:59:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42634 -->
