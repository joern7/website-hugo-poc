---
layout: "comment"
hidden: true
title: "22052"
date: "2016-05-30T18:18:36"
uploadBy:
- "qincym"
license: "unknown"
imported:
- "2019"
---
@Stefan Falk

Es sieht so aus, als wären die damaligen Geometric Baukästen 1 bis 4 in doppelter Ausführung in dem 5000 Baukasten eingefüllt worden. Das hatte bei unserer gemeinsamen "Elektronische Henne" nicht ausgereicht, um die Henne zu bauen. Ich musste noch Bausteine dazu kaufen.

Gruß
Volker-James