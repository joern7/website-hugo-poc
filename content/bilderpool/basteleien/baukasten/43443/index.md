---
layout: "image"
title: "Fischergeometric 5000_1"
date: "2016-05-30T14:53:40"
picture: "Geo_2_klein.jpg"
weight: "3"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/43443
- /details6868.html
imported:
- "2019"
_4images_image_id: "43443"
_4images_cat_id: "3171"
_4images_user_id: "10"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43443 -->
Ich habe den Kasten mal ein wenig aufgeräumt ;-)