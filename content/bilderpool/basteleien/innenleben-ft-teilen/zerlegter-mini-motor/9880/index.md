---
layout: "image"
title: "Motor im Gehäuse"
date: "2007-04-02T10:19:04"
picture: "motor4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9880
- /detailse5c2.html
imported:
- "2019"
_4images_image_id: "9880"
_4images_cat_id: "893"
_4images_user_id: "453"
_4images_image_date: "2007-04-02T10:19:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9880 -->
Hier ist der Motor wieder in seinem Gehäuse.