---
layout: "image"
title: "Einzellteile"
date: "2007-04-02T10:19:04"
picture: "motor1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9877
- /details1663.html
imported:
- "2019"
_4images_image_id: "9877"
_4images_cat_id: "893"
_4images_user_id: "453"
_4images_image_date: "2007-04-02T10:19:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9877 -->
Hier ist ein Mini-Motor und seine Einzellteile zusehen.