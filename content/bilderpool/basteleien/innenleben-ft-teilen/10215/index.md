---
layout: "image"
title: "Detail Siemens Trafo"
date: "2007-04-29T19:59:11"
picture: "platinen17.jpg"
weight: "18"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10215
- /details23b2.html
imported:
- "2019"
_4images_image_id: "10215"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10215 -->
Den Poti sieht man schlecht, bei ihm führen die Bahnen je zu einem weissen Kasten auf der Seite und eine zum schwarzen vorne.