---
layout: "image"
title: "Rückseite Version 1"
date: "2007-02-26T09:03:37"
picture: "image3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/9156
- /details31cc.html
imported:
- "2019"
_4images_image_id: "9156"
_4images_cat_id: "845"
_4images_user_id: "120"
_4images_image_date: "2007-02-26T09:03:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9156 -->
vom Parallel-Interface für den C64, Leiterzüge sind wohl ursprünglich mal mit Hand gezeichnet worden