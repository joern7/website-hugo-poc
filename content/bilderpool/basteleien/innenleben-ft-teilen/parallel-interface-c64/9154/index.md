---
layout: "image"
title: "Vorderseite Version 2"
date: "2007-02-26T09:03:37"
picture: "image1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/9154
- /details3433.html
imported:
- "2019"
_4images_image_id: "9154"
_4images_cat_id: "845"
_4images_user_id: "120"
_4images_image_date: "2007-02-26T09:03:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9154 -->
vom Parallel-Interface für den C64