---
layout: "comment"
hidden: true
title: "18113"
date: "2013-06-28T09:23:50"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ja, mit Schraubendrehern war ich auch dran. Ganz vorsichtig am Rand und da hat sich der Kunststoff des Deckels schon arg verbogen. Nur runtergehen wollte er nicht. Und ich wollte nicht mit noch mehr Gewalt ran, da ich ihn nicht zerstören wollte - weder den Deckel noch den Motor.

Wie und wo hast Du den Schraubendreher angesetzt? Und dann?