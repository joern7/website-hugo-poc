---
layout: "image"
title: "Mini Taster"
date: "2007-04-29T19:59:11"
picture: "platinen19.jpg"
weight: "20"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10217
- /details1ff0.html
imported:
- "2019"
_4images_image_id: "10217"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10217 -->
Dass man mal sieht wie das es da drin aussieht.