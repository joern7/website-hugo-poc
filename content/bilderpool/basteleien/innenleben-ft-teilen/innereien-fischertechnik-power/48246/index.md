---
layout: "image"
title: "Blick auf die Leistungselektronik"
date: "2018-10-17T21:54:48"
picture: "innereiendesfischertechnikpowercontrollers4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48246
- /detailsfd96.html
imported:
- "2019"
_4images_image_id: "48246"
_4images_cat_id: "3540"
_4images_user_id: "104"
_4images_image_date: "2018-10-17T21:54:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48246 -->
Um dieses Bauteil entspannte sich eine angeregte Diskussion :-)