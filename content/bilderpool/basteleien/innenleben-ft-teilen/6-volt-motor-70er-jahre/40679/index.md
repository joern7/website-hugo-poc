---
layout: "image"
title: "Leergehäuse 6 Volt Motor"
date: "2015-03-22T10:53:43"
picture: "motor02.jpg"
weight: "2"
konstrukteure: 
- "Pim Sturm"
fotografen:
- "Pim Sturm"
keywords: ["Motor"]
uploadBy: "pim_s"
license: "unknown"
legacy_id:
- /php/details/40679
- /details9f5d.html
imported:
- "2019"
_4images_image_id: "40679"
_4images_cat_id: "3055"
_4images_user_id: "2398"
_4images_image_date: "2015-03-22T10:53:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40679 -->
