---
layout: "image"
title: "4 beschädigte Motoren"
date: "2018-07-13T18:04:58"
picture: "M1.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47717
- /detailsbd6a.html
imported:
- "2019"
_4images_image_id: "47717"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47717 -->
Reparatur steht an, Fehlerbeschreibung von links:
1. Motor läuft zwar, aber Stromaufnahme 4 Ampere im Leerlauf
2. Motor läuft gar nicht, Widerstandmessung: hochohmig 
3. Motor läuft sehr schwach, hohe Stromaufnahme
4. Motor läuft gut, Schnecke stark abgenutzt und beschädigt