---
layout: "comment"
hidden: true
title: "20386"
date: "2015-03-22T11:12:59"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da stellt sich mir die Frage, warum der Motor zwar 30 mm breit, aber viel höher ist, wo das innere doch offenbar kreisrund ist?
Gruß,
Stefan