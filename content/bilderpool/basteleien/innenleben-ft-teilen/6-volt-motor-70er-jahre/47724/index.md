---
layout: "image"
title: "Detail der zerrissenen Wicklung und des abgerissenen Graphitblocks am Schleifkontakt"
date: "2018-07-13T18:04:58"
picture: "M8.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47724
- /details84a3.html
imported:
- "2019"
_4images_image_id: "47724"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47724 -->
