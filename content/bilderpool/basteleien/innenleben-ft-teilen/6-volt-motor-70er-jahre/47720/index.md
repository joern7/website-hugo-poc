---
layout: "image"
title: "Leergehäuse"
date: "2018-07-13T18:04:58"
picture: "M4.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47720
- /detailsa082.html
imported:
- "2019"
_4images_image_id: "47720"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47720 -->
Das Gehäuse und die beiden Kontaktstücke werden einem anderen Zweck zugeführt, vielleicht ein Akku oder eine Elektronik.