---
layout: "image"
title: "3 funktionsfähige Motoren ohne Schnecke"
date: "2018-07-13T18:04:58"
picture: "M9.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47725
- /details55a0.html
imported:
- "2019"
_4images_image_id: "47725"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47725 -->
Auf der 4 mm Achse will ich statt der Schnecken Zahnräder Z10 verwenden für schnelle Antriebe bzw. späte Untersetzungen. Das Schneckengetriebe hat ja auch einen schlechten Wirkungsgrad.