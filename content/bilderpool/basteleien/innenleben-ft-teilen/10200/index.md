---
layout: "image"
title: "Robo RF Data Link"
date: "2007-04-29T19:59:11"
picture: "platinen02.jpg"
weight: "3"
konstrukteure: 
- "Knobloch"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10200
- /details650f.html
imported:
- "2019"
_4images_image_id: "10200"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10200 -->
der Datalink unten ist nicht für das Robo Interfache geeignet. Die Platine ist Knobloch GmbH www.knobloch-gmbh.de angeschrieben, also sicher nicht von ft selber.