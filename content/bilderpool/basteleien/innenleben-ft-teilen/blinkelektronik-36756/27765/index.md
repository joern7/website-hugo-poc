---
layout: "image"
title: "Geschlossen"
date: "2010-07-16T23:32:20"
picture: "blinkelektronik1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27765
- /details5820.html
imported:
- "2019"
_4images_image_id: "27765"
_4images_cat_id: "2000"
_4images_user_id: "373"
_4images_image_date: "2010-07-16T23:32:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27765 -->
So dürften sie die Meisten von uns kennen. Ist übrigens 15x75x15 mm groß. An den Seitenkanten sind Buchsen, die auch noch als Ausgang dienen können.