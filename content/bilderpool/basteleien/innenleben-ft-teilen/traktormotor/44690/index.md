---
layout: "image"
title: "Platine"
date: "2016-10-27T15:50:05"
picture: "traktormotor2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44690
- /detailsabed.html
imported:
- "2019"
_4images_image_id: "44690"
_4images_cat_id: "3328"
_4images_user_id: "558"
_4images_image_date: "2016-10-27T15:50:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44690 -->
Vermutlich eine Unbestückte Encoderplatine