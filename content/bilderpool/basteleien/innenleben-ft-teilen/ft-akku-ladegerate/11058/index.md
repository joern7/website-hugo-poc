---
layout: "image"
title: "ft-Akku-Lader ältere Version - Bild 1"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11058
- /detailsaee4.html
imported:
- "2019"
_4images_image_id: "11058"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11058 -->
Elektronik im Adapter für den ft-Akku, dieser hat auch eine grüne LED im Deckel