---
layout: "image"
title: "ft-Akku-Lader aktuelle Version - Bild 4"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11056
- /details3a10.html
imported:
- "2019"
_4images_image_id: "11056"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11056 -->
Strombegrenzung über (vermutlich) PTC mit Aufschrift "ZD2R MEXICO X3O UI85"