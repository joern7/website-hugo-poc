---
layout: "image"
title: "IR Handsender von Ruwido"
date: "2007-04-29T19:59:11"
picture: "platinen11.jpg"
weight: "12"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10209
- /detailsa86c.html
imported:
- "2019"
_4images_image_id: "10209"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10209 -->
Wie im Titel gesagt ist der Handsender weder von ft persönlich noch von knobloch. Aber das ändert nicht dass bei Motor 3 die Pfeile rechtsrum gehen.