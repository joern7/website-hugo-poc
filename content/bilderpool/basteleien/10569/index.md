---
layout: "image"
title: "Adler"
date: "2007-05-31T09:43:06"
picture: "diverse1.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik/Fredy"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10569
- /detailsf999.html
imported:
- "2019"
_4images_image_id: "10569"
_4images_cat_id: "463"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10569 -->
