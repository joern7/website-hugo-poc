---
layout: "image"
title: "Lauflichtmodul"
date: "2017-02-20T17:32:24"
picture: "2017-02-08_09.24.55.jpg"
weight: "33"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45268
- /details4d4b.html
imported:
- "2019"
_4images_image_id: "45268"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-02-20T17:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45268 -->
Links ist ein Lauflichtmodul mit 18 Programmen und rechts ist ein Blitzmodul mit 18 Programmen.An den Ausgängen können Lampen , Leds usw angeschlossen werden.Es kann die Geschwindigkeit und Im Zufallsprogramm die Wechselzeit eingestellt werden.