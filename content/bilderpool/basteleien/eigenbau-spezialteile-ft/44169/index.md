---
layout: "image"
title: "Arduino im Betrieb"
date: "2016-08-03T15:14:05"
picture: "2016-07-31_11.23.18.jpg"
weight: "27"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/44169
- /details803d.html
imported:
- "2019"
_4images_image_id: "44169"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-08-03T15:14:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44169 -->
Es läuft ein Blinkprogramm über Port 13