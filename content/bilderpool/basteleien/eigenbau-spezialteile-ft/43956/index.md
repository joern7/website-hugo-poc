---
layout: "image"
title: "Lichtschrankensensor"
date: "2016-07-25T14:24:24"
picture: "2016-07-17_19.11.331.jpg"
weight: "19"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43956
- /details4761.html
imported:
- "2019"
_4images_image_id: "43956"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43956 -->
Lichtschrankensensor eingebaut.