---
layout: "image"
title: "Fischertechnik Elektronikmodule"
date: "2016-07-28T16:54:28"
picture: "2016-07-24_21.44.371.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/44045
- /details532f.html
imported:
- "2019"
_4images_image_id: "44045"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44045 -->
