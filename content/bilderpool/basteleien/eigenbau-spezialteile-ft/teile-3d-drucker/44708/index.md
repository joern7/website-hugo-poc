---
layout: "image"
title: "TXT Kamera Zahnrad / Bild 1 von 2"
date: "2016-10-30T12:19:35"
picture: "Zahnrad_1.jpg"
weight: "10"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: ["TXT", "Kamera", "Zahnrad"]
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/44708
- /details8a3f-2.html
imported:
- "2019"
_4images_image_id: "44708"
_4images_cat_id: "3272"
_4images_user_id: "1355"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44708 -->
Durch den Beitrag 
https://forum.ftcommunity.de/viewtopic.php?f=6&t=3419&p=23662&hilit=Digitalkamera#p23458

und dem dazu gehörigen Video 
https://www.youtube.com/watch?v=Zgj_jSWz3dk&feature=youtu.be

habe ich ein Zahnrad konstruiert, welches auf die TXT &#8211; Kamera drauf passt.