---
layout: "image"
title: "Grundplatte15x45"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok7.jpg"
weight: "7"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/47580
- /details200d.html
imported:
- "2019"
_4images_image_id: "47580"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47580 -->
Die BSB-Tenderlok besteht auch aus 2 kleinen Grundplatten. Diese werden vorne und hinten an die BSB-Grundplatte durch 2 kleine Stangen und einem 15mm Verbindungsteil befestigt.