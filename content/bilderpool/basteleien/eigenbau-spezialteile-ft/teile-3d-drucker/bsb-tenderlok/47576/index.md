---
layout: "image"
title: "Kesselteil von hinten"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok3.jpg"
weight: "3"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/47576
- /details7606.html
imported:
- "2019"
_4images_image_id: "47576"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47576 -->
Der Kesselteil besteht aus 4 Teile, die wir dann zusammengeklebt haben
.) Kessel
.) Großer Rauchfang
.) 2x kleine Rauchfänge