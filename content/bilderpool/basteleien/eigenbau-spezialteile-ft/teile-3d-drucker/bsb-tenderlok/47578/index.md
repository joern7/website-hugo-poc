---
layout: "image"
title: "Seitenteil von innen"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok5.jpg"
weight: "5"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/47578
- /details7f8b.html
imported:
- "2019"
_4images_image_id: "47578"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47578 -->
Auch den Seitenteil haben wir selber konstruiert und ausgedruckt. 
Hier von Innen