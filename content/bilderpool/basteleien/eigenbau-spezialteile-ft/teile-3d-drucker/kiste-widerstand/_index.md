---
layout: "overview"
title: "Kiste mit Widerstand"
date: 2020-02-22T07:46:35+01:00
legacy_id:
- /php/categories/3348
- /categoriesa6e2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3348 --> 
Mit meinem 3D-Durcker habe ich eine kleine Kiste (30x30x30 mm) konstruiert in denen man elektronische Bauteile (z.b. Widerstände, Dioden, Transistoren, ICs usw.) einbauen kann. Damit sind die elektronischen Teile besser geschützt. Durch die Nuten können auch andere Fischertechnik Bauteile befestigt werden.