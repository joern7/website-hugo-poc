---
layout: "image"
title: "Kiste mit Widerstand 02"
date: "2017-01-07T20:22:20"
picture: "kistemitwiderstand2.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45011
- /detailscf32.html
imported:
- "2019"
_4images_image_id: "45011"
_4images_cat_id: "3348"
_4images_user_id: "1355"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45011 -->
Der Deckel passt genau in die Vertiefung von der Kiste. Dabei gibt es 2 Löcher in denen dann die beiden Buchsen befestiget werden können. Auf den Buchsen habe ich dann den Widerstand draufgelötet.