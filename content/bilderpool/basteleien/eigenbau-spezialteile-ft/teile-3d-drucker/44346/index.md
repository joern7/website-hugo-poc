---
layout: "image"
title: "...doch nicht ganz"
date: "2016-09-10T14:26:54"
picture: "bild3.jpg"
weight: "7"
konstrukteure: 
- "3D-Drucker"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/44346
- /detailsf9ac.html
imported:
- "2019"
_4images_image_id: "44346"
_4images_cat_id: "3272"
_4images_user_id: "1624"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44346 -->
Wenn man genau hinschaut, sieht man jedoch diesen kleinen Spalt. Ich setz mich demnächst mal hin und probiere verschiedene Maße aus und poste die Ergebnisse hier.