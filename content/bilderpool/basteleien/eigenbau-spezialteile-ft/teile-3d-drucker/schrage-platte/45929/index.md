---
layout: "image"
title: "Scheune 02"
date: "2017-06-04T12:09:54"
picture: "schraegeplatte4.jpg"
weight: "4"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45929
- /details8b52.html
imported:
- "2019"
_4images_image_id: "45929"
_4images_cat_id: "3410"
_4images_user_id: "1355"
_4images_image_date: "2017-06-04T12:09:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45929 -->
Scheune von der anderen Seite