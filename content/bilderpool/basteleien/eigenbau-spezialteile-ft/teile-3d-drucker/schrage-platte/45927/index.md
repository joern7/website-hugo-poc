---
layout: "image"
title: "Schräge Platte 02"
date: "2017-06-04T12:09:54"
picture: "schraegeplatte2.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45927
- /details312d-2.html
imported:
- "2019"
_4images_image_id: "45927"
_4images_cat_id: "3410"
_4images_user_id: "1355"
_4images_image_date: "2017-06-04T12:09:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45927 -->
Das Teil von der anderen Seite.
Ein Problem gibt es noch: Die Zapfen gehen leicht herunter. Da muss ich mir was Besseres einfallen lassen.