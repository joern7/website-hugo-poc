---
layout: "image"
title: "Schräge Platte 01"
date: "2017-06-04T12:09:54"
picture: "schraegeplatte1.jpg"
weight: "1"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45926
- /details1fcf.html
imported:
- "2019"
_4images_image_id: "45926"
_4images_cat_id: "3410"
_4images_user_id: "1355"
_4images_image_date: "2017-06-04T12:09:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45926 -->
Das Teil von der Vorderseite
Theoretisch könnte man das Teil auch von einer 30x30-Platte zuschneiden.