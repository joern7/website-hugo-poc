---
layout: "image"
title: "Nut statt Zapfen"
date: "2016-09-10T14:26:54"
picture: "bild4.jpg"
weight: "8"
konstrukteure: 
- "3D-Drucker"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/44347
- /details27f9.html
imported:
- "2019"
_4images_image_id: "44347"
_4images_cat_id: "3272"
_4images_user_id: "1624"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44347 -->
Hier ist das selbe Teil mit einer Nut statt nem Zapfen. Wenn man Teile mit Löchern druckt, werden die aber immer etwas kleiner als das Maß das man am PC eingegeben hat. Deshalb muss man mit den Maßen ein wenig rumspielen.