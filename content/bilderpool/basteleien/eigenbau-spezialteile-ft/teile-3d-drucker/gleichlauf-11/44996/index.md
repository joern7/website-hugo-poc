---
layout: "image"
title: "Prinzipskizze"
date: "2017-01-01T21:43:29"
picture: "gl1.jpg"
weight: "1"
konstrukteure: 
- "Wikipedia"
fotografen:
- "Wikipedia"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44996
- /details9aab.html
imported:
- "2019"
_4images_image_id: "44996"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44996 -->
Screenshot von Wikipedia: https://de.wikipedia.org/wiki/Datei:Skizze-Ueberlagerungsgetriebe-Variante-B.svg

Das Prinzip ist ganz simpel: Wir fangen mit der hellgrünen Welle in der Mitte an: sie treibt die Sonnenräder (auch in hellgrün) der beiden Planetengetriebe links und rechts gleichsinnig an. Deren Außenringe (dunkleres grün) stützen sich unten, auf den dunkelbraunen Elementen ab, die wir zunächst mal als "fest" ansehen. Mit angetriebener Sonne und festem Außenring rollen die Planeten (orange) auf dem Außenring ab, nehmen den Planetenträger (hellbraun) mit und treiben somit die dunkelbraunen Ausgänge ("Abtrieb") links und rechts an. Das Fahrzeug fährt schnurgeradeaus.

Zum Lenken nimmt man jetzt die Abstützung der Außenringe nicht mehr als fest an, sondern lässt diese Elemente ebenfalls antreiben, und zwar gegenläufig. Damit wird auf die beiden Drehungen links und rechts jeweils eine weitere addiert (überlagert), mit unterschiedlichem Vorzeichen, und das Fahrzeug fährt eine Kurve oder dreht auf der Stelle (wenn nämlich die grüne Welle steht und nur der gegenläufige Anteil da ist).