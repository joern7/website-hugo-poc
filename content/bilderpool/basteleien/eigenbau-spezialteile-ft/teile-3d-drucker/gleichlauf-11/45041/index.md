---
layout: "image"
title: "Gleichlauf_11b3.jpg"
date: "2017-01-15T19:45:58"
picture: "Gleichlauf_11b3.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modul1"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45041
- /details5e74.html
imported:
- "2019"
_4images_image_id: "45041"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-15T19:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45041 -->
Den Antrieb habe ich nochmal umgeordnet. Der Motor für gleichsinnigen Antrieb ist jetzt ein Power-Motor, der auch längs in der Wanne liegt. Das schwarze Differenzial wird den unbedarften Zuschauer in die Irre führen - es ist aber innen leer, läuft lose auf der Rastachse mit und dient nur dazu, den Antrieb vom Kegelrad Z14 auf dem Motor um 90° auf das Z20 zu drehen.