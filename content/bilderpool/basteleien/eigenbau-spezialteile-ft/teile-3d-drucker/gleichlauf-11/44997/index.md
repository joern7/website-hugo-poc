---
layout: "image"
title: "CAD-Zeichnung"
date: "2017-01-01T21:43:29"
picture: "gl2.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44997
- /detailsd7ce.html
imported:
- "2019"
_4images_image_id: "44997"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44997 -->
Das blaue Zahnrad Z32 ist auf dem Planetenträger montiert und hat mit dem Planetengetriebe innen drin nichts zu tun. Die Planeten sind mittels ft-Verbindungsstopfen (schwarz) am Planetenträger montiert. 

Das Sonnenrad Z10 modul 1 ist zwar hier auch gezeichnet (grün), kommt aber nicht aus dem 3D-Drucker. Mit den Planeten Z14 modul 1 (cyan) ergibt sich, dass der Außenring (rot) mit 10+14+14 = 38 Zähnen innenverzahnt werden muss. Diese Zahlen wurden auch so gewählt, dass das Getriebe mit drei Planeten überhaupt zusammen gesetzt werden kann, und nicht etwa irgendwo Zahn auf Zahn stößt.

Der gleichsinnige Antrieb kommt über die weiße Welle, das grüne Sonnenrad und die Planeten (cyan) auf den Planetenträger (blau). Der Lenkantrieb wird über das rote Zahnkranz Z30 auf den Außenring übertragen.

Im CAD-Programm zeichne ich bei Zahnrädern immer gleich mit einem dünnen Ring (1 Drucklage hoch) an, wo sich der Wälzkreis befindet. Wenn sich die Wälzkreise miteinander kämmender Zahnräder gerade so berühren, ist alles in Ordnung. Als letzter Bearbeitungsschritt werden diese Ringe ordentlich groß auf gezogen und dienen als Haftrand ("Brim") im 3D-Drucker. Das ist nötig, weil mein Slicer beim automatischen Anfügen des Brim an schräg aufragenden Wänden nichts Gescheites zu Wege bringt.