---
layout: "image"
title: "Raspberry-Halterung.jpg"
date: "2016-01-30T10:55:39"
picture: "Raspi02-Halterung.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Raspi", "Raspberry", "Halterung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42841
- /details6035.html
imported:
- "2019"
_4images_image_id: "42841"
_4images_cat_id: "3272"
_4images_user_id: "4"
_4images_image_date: "2016-01-30T10:55:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42841 -->
Damit mein Raspberry 2 ins Modell hinein passt. Das soll jetzt nicht heißen, dass es dieses Modell schon gäbe. Nuten, Löcher und Statik-Langlöcher sind auf dem 3D-Drucker stabiler herzustellen als Zapfen und Federleisten, und sie verlangen weniger bis gar kein Stützmaterial.