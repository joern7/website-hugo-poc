---
layout: "comment"
hidden: true
title: "22508"
date: "2016-09-12T15:56:31"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Bei meinem Drucker (Velleman K8400 mit 0,35 mm-Düse) muss ich 2,2 mm Radius angeben, damit noch soviel "Fleisch" in der Bohrung bleibt, dass die Reibahle es schön rund und glatt bekommt. 

Für andere Drucker gilt das nicht, und wenn das Loch direkt verwendbar sein soll, macht man am besten ein Teil mit einer Reihe von Löchern in Abstufung von 0,2 mm -- ich verweise mal auf die ft:pedia 2016/01 und darin auf den Teil 3 der "3D"-Artikelreihe.

Gruß,
Harald