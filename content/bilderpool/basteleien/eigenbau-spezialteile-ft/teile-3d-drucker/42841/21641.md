---
layout: "comment"
hidden: true
title: "21641"
date: "2016-02-01T17:46:44"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Gute Arbeit aus der Werkstatt! Und ein neuer Farbimpuls für ft-Modelle.
LG Martin