---
layout: "image"
title: "Mini Powercontroller am Modell"
date: "2016-06-26T11:11:25"
picture: "2016-06-19_17.49.071.jpg"
weight: "5"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43782
- /detailsa805.html
imported:
- "2019"
_4images_image_id: "43782"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43782 -->
Der Controller regelt die Drehzahl am Aufzug 
der Kugelbahn.