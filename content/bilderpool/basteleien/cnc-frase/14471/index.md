---
layout: "image"
title: "Bild 09 - Gefraeste Zahlen in Acryl-Glas"
date: "2008-05-05T16:03:45"
picture: "Bild_09_-_Gefrste_Zahlen_in_Acryl-Glas.jpg"
weight: "2"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14471
- /details3c2d.html
imported:
- "2019"
_4images_image_id: "14471"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14471 -->
Gefräste Zahlen in Acryl-Glas