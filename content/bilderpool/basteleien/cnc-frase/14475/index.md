---
layout: "image"
title: "05 - Detail Antrieb Gewindestange für Z-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_05_-_Detail_Antrieb_Gewindestange_fr_Z-Achse.jpg"
weight: "6"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14475
- /details6c0f.html
imported:
- "2019"
_4images_image_id: "14475"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14475 -->
Detail Antrieb Gewindestange für Z-Achse

Über eine Achs-Kupplung ist der Gewindestab mit dem Schrittmotor verbunden.