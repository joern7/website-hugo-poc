---
layout: "image"
title: "Bild 01 - Fraeser Gesamtansicht"
date: "2008-05-05T16:03:46"
picture: "Bild_01_-_Fraeser_Gesamtansicht.jpg"
weight: "10"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: ["CNC", "Fräse", "Fraese", "Fräser", "Fraeser", "CNC-Fraese", "CNC-Fraeser"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14479
- /details1d83.html
imported:
- "2019"
_4images_image_id: "14479"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14479 -->
CNC-Fräser aus umgebautem Plotter 85

Gesteuert wird er mit VB5 und dem modifizierten Programm ftplot von Ulrich Müller, welches perfekt auf den Plotter 85 abgestimmt und auch recht flott ist. 
Als Interface dient ein leicht modifiziertes Intelligent-Interface (hat jetzt auch Anschluß für Flachbandkabel) und ein Erweiterungs-Modul.

Schwierig war die Ansteuerung der Z-Achse. Denn beim Plotter wird einfach nur der Elektromagnet für den Stift ein- und ausgeschaltet. 
Doch beim Fräser muß man den Schrittmotor solange ansteuern, bis er das Material erreicht hat. 
Am Anfang fährt nun die Z-Achse bis nach oben auf den Referenz-Taster. Dann fährt der Fräser ins Material und dann nur noch 2 mm raus. Denn der Gewindestab ist zwar in Verbindung mit meinem schrittmotor (aus einem alten HP-Drucker) mit 70 Schritten pro mm sehr präzise. Aber leider auch nicht ganz so schnell...

Ich kann im Grunde alle Motive fräsen. Ich exportiere sie dann aus Corel Draw 5 als HPGL-Datei. Anschließend werden die Dateien mit einem Excel-Makro an mein Fräs-Programm angepaßt und direkt in VB5 eingefügt.