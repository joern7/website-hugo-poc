---
layout: "image"
title: "Paraffinum Perliquidum"
date: "2008-09-12T22:45:52"
picture: "anleitungzumoelenvonzylindern3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/15226
- /details3171.html
imported:
- "2019"
_4images_image_id: "15226"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15226 -->
Hier sieht man das Öl mit dem man die Zylinder einölt. Der Name ist Paraffinum Perliquidum medizinisches Weißöl. Besorgen kann man es in der Apotheke bei der 20ml 79 Cent kosten!