---
layout: "image"
title: "Verteilen"
date: "2008-09-12T22:45:53"
picture: "anleitungzumoelenvonzylindern7.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/15230
- /detailsca88.html
imported:
- "2019"
_4images_image_id: "15230"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15230 -->
Anschließend muss man die Zylinder-Stange schnell vor und zurückziehen und ihn auch ein bisschen drehen. Es ist ratsam dabei die andere Seite des Zylinders festzuhalten, was man auf dem Bild nicht sehen kann weil ich ja noch die Kamera halten musste :-).