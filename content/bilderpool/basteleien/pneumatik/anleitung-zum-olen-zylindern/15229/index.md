---
layout: "image"
title: "Öl einziehen"
date: "2008-09-12T22:45:53"
picture: "anleitungzumoelenvonzylindern6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/15229
- /details4aea.html
imported:
- "2019"
_4images_image_id: "15229"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15229 -->
Danach zieht man das ÖL ein, indem man die Zylinder-Stange langsam nach vorne zieht (ausfährt).