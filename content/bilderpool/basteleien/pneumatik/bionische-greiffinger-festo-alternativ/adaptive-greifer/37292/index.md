---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer6.jpg"
weight: "6"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37292
- /details1da0.html
imported:
- "2019"
_4images_image_id: "37292"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37292 -->
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Ein kompaktere Bau der adaptive Greifer in Fischertechnik ist  denke ich nicht möglich...?
