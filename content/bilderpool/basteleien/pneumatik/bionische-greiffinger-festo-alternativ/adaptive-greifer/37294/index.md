---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer8.jpg"
weight: "8"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37294
- /details09ec.html
imported:
- "2019"
_4images_image_id: "37294"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37294 -->
Mit ein pneumatik Zylinder ist eine automatische Schließvorgang einfach möglich.

