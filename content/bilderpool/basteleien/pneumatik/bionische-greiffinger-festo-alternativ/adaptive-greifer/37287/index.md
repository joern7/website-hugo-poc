---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer1.jpg"
weight: "1"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37287
- /details5478.html
imported:
- "2019"
_4images_image_id: "37287"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37287 -->
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Ein kompaktere Bau der adaptive Greifer in Fischertechnik ist  denke ich nicht möglich...?
