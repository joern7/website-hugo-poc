---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer9.jpg"
weight: "9"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37295
- /detailsb9de.html
imported:
- "2019"
_4images_image_id: "37295"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37295 -->
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Ein kompaktere Bau der adaptive Greifer in Fischertechnik ist  denke ich nicht möglich...?

Mit ein pneumatik Zylinder ist eine automatische Schließvorgang einfach möglich.
