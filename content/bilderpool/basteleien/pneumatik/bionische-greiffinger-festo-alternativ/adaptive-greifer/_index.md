---
layout: "overview"
title: "Adaptive Greifer"
date: 2020-02-22T07:42:03+01:00
legacy_id:
- /php/categories/2775
- /categories7893.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2775 --> 
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.
