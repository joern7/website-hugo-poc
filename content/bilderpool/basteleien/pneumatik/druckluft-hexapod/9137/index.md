---
layout: "image"
title: "Gelenke"
date: "2007-02-23T10:51:38"
picture: "03-Hexapod.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/9137
- /details58bf.html
imported:
- "2019"
_4images_image_id: "9137"
_4images_cat_id: "830"
_4images_user_id: "46"
_4images_image_date: "2007-02-23T10:51:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9137 -->
Jetzt weiß ich endlich, wofür diese winzigen, roten Ringe gut sind, die man auf die 4 mm Achsen schieben kann. Sie passen anstelle der schwarzen Hülse in die Gelenkbausteinhälfte. Damit war dann die Konstruktion der Kreuzgelenke einfach.

Allerdings müssen die Gelenke auch eine Rotation zulassen. Das hingegen können die Zylinder schon selber. Diese Konstruktion ist daher völlig spannungsfrei bei jeder Bewegung.