---
layout: "image"
title: "Neuer mini-Kompressor mit Lemo-Solar Membranpumpe"
date: "2006-01-22T11:09:34"
picture: "Draufsicht.jpg"
weight: "2"
konstrukteure: 
- "Reiner"
fotografen:
- "Reiner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "78081"
license: "unknown"
legacy_id:
- /php/details/5638
- /detailse12a.html
imported:
- "2019"
_4images_image_id: "5638"
_4images_cat_id: "487"
_4images_user_id: "405"
_4images_image_date: "2006-01-22T11:09:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5638 -->
Habe nach FischerTechnik Anleitung den mini-Kompressor nachgebaut. Allerdings mit Lemo-Solar Membranpumpe und etwas modifiziert! Die Membranpumpe und den Endschalter lasse ich über ein I/O Extension Modul mit eigener 9V/1000mA Stromversorgung laufen, hat mich zwar etwas gewundert das bei mehrmotor betrieb am Interface die Membranpumpe schwächelt da ich dort ein Netzteil mit 4,2 A nutze aber so wie oben beschrieben funktionierts auch!

P.S. die Druckleitung vom Kompressor habe ich auf beide Druckbehälter aufgeteilt weil, 1. der Kompressor eh mehr luft bringt als durch ein schlauch geht und 2. weil so bei mehrzylinderbetrieb schneller wieder druck aufgebaut ist! Aber bei dem Motor sowieso sehr schwer dem geht nie die Luft aus !!!