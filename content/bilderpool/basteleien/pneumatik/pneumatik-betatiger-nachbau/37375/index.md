---
layout: "image"
title: "Die Rollmembran in ihrem Gehäuse"
date: "2013-09-11T12:40:09"
picture: "Rollmembran_eingezogen.jpg"
weight: "24"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37375
- /detailsc3e5.html
imported:
- "2019"
_4images_image_id: "37375"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37375 -->
So sieht es aus, wenn die Rollmembran in ihrem Gehäuse sitzt. Hier habe ich mit etwas Unterdruck nachgeholfen. Normalerweise wird das von der Kolbenstange erledigt, dann braucht es keinen Unterdruck.