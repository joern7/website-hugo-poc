---
layout: "image"
title: "P-Betätiger 2"
date: "2013-04-18T20:27:44"
picture: "bild2.jpg"
weight: "6"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36846
- /detailsfe34.html
imported:
- "2019"
_4images_image_id: "36846"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36846 -->
Taster drücken. Als Greifer würd ich diesen Betätiger-Nachbau aber nicht verwenden, da verrutscht die Membran.