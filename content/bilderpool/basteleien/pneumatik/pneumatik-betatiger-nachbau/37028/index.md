---
layout: "image"
title: "P-Betätiger klein V2 1"
date: "2013-06-08T20:40:06"
picture: "bild1_3.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37028
- /detailsd51f.html
imported:
- "2019"
_4images_image_id: "37028"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-06-08T20:40:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37028 -->
Noch ein Versuch. Dieser scheint zu funktionieren, vorausgesetzt, es ist genügend Druck drauf (nächstes Bild). Durch Fredys Bild (http://www.ftcommunity.de/details.php?image_id=37027) kam ich darauf, die Gummihandschuh-Fingerspitze irgendwo hineinzustecken. Das nächstbeste greifbare war ein Winkelträger 15 mit zwei Zapfen. Herausgekommen ist etwas, was dem Original-Betätiger wohl ziemlich ähnlich sieht. Die Hülse + Scheibe (35981) ist mit Heißkleber an der Membran festgeklebt, die wiederum mit einem bisschen Tesafilm auf dem Wattestäbchen klebt. Dieses klebt wieder mit Heißkleber in einem der Löcher der Winkelträgers.
Die Grundiedee war, ähnlich wie beim Original-Betätiger, den Gummihandschuh so irgendwo hinein zu packen, dass er, wenn er sich unter Druck ausdehnt, nur in eine Richtung herausbewegen kann. Die Hülse + Scheibe verhindert, dass der Gummihandschuh den Taster umschließt, anstatt ihn zu drücken.