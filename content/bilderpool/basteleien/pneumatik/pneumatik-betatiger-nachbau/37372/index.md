---
layout: "image"
title: "Betätiger drucklos  - aus anderem Blickwinkel"
date: "2013-09-11T12:40:09"
picture: "Bettiger_montiert.jpg"
weight: "21"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37372
- /details1d8b.html
imported:
- "2019"
_4images_image_id: "37372"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37372 -->
Die Rollmembran ist mit zwei O-Ringen auf einen runden Adapter einer Standluftpumpe (so ein Ding zum Luftmatratze aufpusten) aufgezogen.