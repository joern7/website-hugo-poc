---
layout: "comment"
hidden: true
title: "18916"
date: "2014-04-12T20:18:32"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Eine bärenstarke Idee - einfach ne Schraube durch und gut :-)). In der Tat, das Teil gehört ins Programm aufgenommen! 

Wer's gemoddet haben will: ich habe jetzt mal in einen Rastadapter ein ordentliches Gewinde geschnitten und eine helicoil (vgl. http://ftcommunity.de/details.php?image_id=35518 ) eingesetzt. Sieht richtig professionell aus.

Gruß,
Harald