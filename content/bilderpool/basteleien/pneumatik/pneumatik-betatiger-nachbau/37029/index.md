---
layout: "image"
title: "P-Betätiger klein V2 2"
date: "2013-06-08T20:40:06"
picture: "bild2_2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37029
- /details1c75.html
imported:
- "2019"
_4images_image_id: "37029"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-06-08T20:40:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37029 -->
Hier sieht man den Betätiger-Nachbau in Aktion. Das Drücken eines leichtgängigen Tasters scheint kein Problem zu sein, der Druck eines P-Zylinders mit Feder reicht aus. Der BS30 dient dazu da, dass der Gummihandschuh sich nicht an die Seite ausdehnen kann. Es wäre hier vielleicht sinnvoll, ein Stück Pappe oder ähnliches aufzukleben.