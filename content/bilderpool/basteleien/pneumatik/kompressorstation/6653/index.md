---
layout: "image"
title: "Kompressorstation"
date: "2006-07-25T06:55:38"
picture: "M4100019.jpg"
weight: "1"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6653
- /detailsaf9a.html
imported:
- "2019"
_4images_image_id: "6653"
_4images_cat_id: "573"
_4images_user_id: "426"
_4images_image_date: "2006-07-25T06:55:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6653 -->
Da geht einem nie mehr die Luft aus. Der Druckbehälter wird bis auf 2,0 bar aufgepumpt und sobald Druck abnimmt schaltet der zweite Druckwächter bei 1,0 bar den Kompressor wieder ein.Hinter der ganzen Anlage befindet sich noch ein Druckminderer der auf 0,6 bar eingestellt ist. Also bei mir gibt es keine Modelle mehr denen die Luft ausgeht :-)