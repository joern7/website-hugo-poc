---
layout: "image"
title: "Selbstbau Ventil"
date: "2008-04-20T14:11:23"
picture: "selbstbauventil2.jpg"
weight: "2"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/14313
- /details217b.html
imported:
- "2019"
_4images_image_id: "14313"
_4images_cat_id: "1325"
_4images_user_id: "762"
_4images_image_date: "2008-04-20T14:11:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14313 -->
So muss es aussehen wenn ihr alles richtig gemacht habt.