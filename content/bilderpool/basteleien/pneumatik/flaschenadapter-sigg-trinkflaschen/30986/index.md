---
layout: "image"
title: "Kompressorstation"
date: "2011-07-04T12:19:52"
picture: "sigg1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30986
- /detailseb7f.html
imported:
- "2019"
_4images_image_id: "30986"
_4images_cat_id: "2316"
_4images_user_id: "373"
_4images_image_date: "2011-07-04T12:19:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30986 -->
Neuestes Bastel-Projekt: Flaschenadapter für Sigg-Trinkflaschen.
Eigentlich wollte ich mir das passende Gewinde raussuchen und dann einen "ordentlichen" Adapter aus Messing o.Ä. bauen, aber leider ist das Gewinde kein Standard und Sigg rückt "aus betriebstechnischen Gründen" nicht mit dem genauen Maß raus. Naja, kann man ja auch verstehen...

Also habe ich eine Flasche mit Deckel (14 Euro) gekauft, den Griff abgesägt, etwas abgeschliffen und ein 4er Loch reingebohrt. Dann noch einen Schlauchdapter von Landefeld eingesetzt und fertig.

Wer es nachbauen will und noch "alte" Flaschen/Deckel hat: vorher mit Druck drauf(Backpulver und Essig rein, zuschrauben) unter Wasser testen, ob der Gummi wirklich noch dicht ist. Ein Alter bei mir war es nicht mehr, gemerkt habe ich es aber erst hinterher.

Durch das Volumen von 1 Liter hat man Druckspeicher genug. Der verwendete Kompressor (ebay, 10 Euro) bringt 0,45bar. Einmal "vollgetankt" kann man damit einen normalem blauen Zylinder 83 mal ein- oder ausfahren.