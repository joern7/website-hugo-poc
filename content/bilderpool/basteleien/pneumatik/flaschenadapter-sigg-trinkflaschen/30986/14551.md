---
layout: "comment"
hidden: true
title: "14551"
date: "2011-07-04T20:27:45"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Top.

Gegen ein Kaltgetränk könnte ich Dir einen P-Verteiler blau (31646) zuschicken. Dann könntest du eine dickere Zuleitung benutzen, und hättest einen höheren Luftdurchsatz, bzw. weniger Druckabfall während der Zylinderbetätigung. 

Wenn Du eine kleine Vakuumpumpe bzw. einen kleinen Kompressor mit Zuluft-Anschluss hättest, könntest du den auch im Inneren der Flasche anbringen. Das spart Platz, weil Du das Luftvolumen um den Kompressor herum dann besser ausnutzt. 

Mein alter Zwang, alles immer auf die Spitze zu treiben, wird nochmal mein Untergang sein ;-)