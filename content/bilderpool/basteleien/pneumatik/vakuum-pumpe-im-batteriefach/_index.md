---
layout: "overview"
title: "Vakuum-pumpe im Batteriefach"
date: 2020-02-22T07:42:07+01:00
legacy_id:
- /php/categories/2764
- /categories262d.html
- /categories7b46.html
- /categoriesa29b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2764 --> 
Da ich immer auf der suche bin um Luft elektrisch zu pumpen hab ich mir diese 12V   -  0,9 bis 1,1 Bar 16? Pumpe angeschaut und ausprobiert.
Eingebaut ist sie in einem einfachen Batteriefach mit Deckel und zwei Steckerösen.


Mir ist aufgefallen...

...sie ist viel leiser als der blaue FT
...sie ist nur halb so schnell wie der blaue FT
...sie saugt und pustet
...und ist kompakt
...sie hat mehr Kraft

!!! Die FT Pneumatikteile sind NICHT für den Gebrauch von ÜBER 1 Bar gestaltet. Ich benutze einen Betätiger für die Abschaltautomatik der Pumpe !!!
