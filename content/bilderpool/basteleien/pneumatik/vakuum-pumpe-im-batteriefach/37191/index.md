---
layout: "image"
title: "Eins der vielen angesammelten Batteriefächer"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach06.jpg"
weight: "6"
konstrukteure: 
- "Kai"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37191
- /details4d01.html
imported:
- "2019"
_4images_image_id: "37191"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37191 -->
Hier benötige ich leider nur die Steckösen für den späteren Stromanschluss.