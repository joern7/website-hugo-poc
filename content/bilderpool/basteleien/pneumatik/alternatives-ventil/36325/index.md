---
layout: "image"
title: "01 Alternatives Ventil"
date: "2012-12-18T21:38:26"
picture: "ventil1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36325
- /detailsbcfd.html
imported:
- "2019"
_4images_image_id: "36325"
_4images_cat_id: "2695"
_4images_user_id: "860"
_4images_image_date: "2012-12-18T21:38:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36325 -->
Da mir die ft-Ventile für knapp 20€ pro Stück zu teuer sind, habe ich mich mal auf die Suche nach einer Alternative gemacht und siehe da, bei Pollin.de gibt es Ventile für 2,50€ das Stück!

Die Stromaufnahme ist mit 12V und 100mA fast gleich zu dem ft-Ventil, sind dafür aber viel billiger. Nur haben sie den großen Nachteil, dass sie im geschlossenen Zustand beide Seiten blockieren, sodass man zwei Ventile braucht, um eins von ft zu ersetzen.
Wenn kein Strom fließt sind sie geschlossen.

Kurzes Video: http://www.youtube.com/watch?v=NPYvVA3zlxU

Pollin Ventil: http://www.pollin.de/shop/dt/MzQ5OTA2OTk-/Bauelemente_Bauteile/Mechanische_Bauelemente/Sonstige_E_Geraete/Druckluft_Magnetventil_SH_V0829BC_R.html

ft-Ventil: http://www.knobloch-gmbh.de/wbc.php?pid=1170&tpl=pr1510detail.html