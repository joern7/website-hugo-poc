---
layout: "image"
title: "05 Alternatives Ventil"
date: "2012-12-18T21:38:26"
picture: "ventil5.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36329
- /details23fb.html
imported:
- "2019"
_4images_image_id: "36329"
_4images_cat_id: "2695"
_4images_user_id: "860"
_4images_image_date: "2012-12-18T21:38:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36329 -->
Das Magnetventil unverbaut.

Kurzes Video: http://www.youtube.com/watch?v=NPYvVA3zlxU