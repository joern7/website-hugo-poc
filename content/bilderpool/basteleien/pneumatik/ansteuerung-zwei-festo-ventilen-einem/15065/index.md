---
layout: "image"
title: "Direkte Betätigung (2)"
date: "2008-08-22T19:50:13"
picture: "magnetventile2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15065
- /detailse419.html
imported:
- "2019"
_4images_image_id: "15065"
_4images_cat_id: "1370"
_4images_user_id: "104"
_4images_image_date: "2008-08-22T19:50:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15065 -->
So war das ganze in der neuen Uhr eingebaut: Die jeweils oberen Ventilanschlüsse werden mit Druckluft versorgt, die unteren gehen zur Vorder- bzw. Rückseite je eines Zylinders. Damit kann ein E-Magnet einen Zylinder bei eingeschaltetem Magneten ausfahren und bei ausgeschaltetem wieder einfahren.