---
layout: "comment"
hidden: true
title: "2620"
date: "2007-03-07T22:56:05"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Claus,

Jede Dolfijn kann ich nicht nur im End-position, sondern auch in die Mitte (oder 2/3) der Zylinder-länge positionieren. 

Weil meine Magnetventilen 3 Anschlusse haben, statt nur 2 wie beim öriginal FT-Magnetventilen, gibt es mehrere möglichkeiten. Positionierung der Zylinder ist dann leichter.

Gleich wie beim Fussball-Stadion kann ich die Dolfijnen z.b. einen "wave" machen lassen.
Das Robo-pro-programme gibt es beim Downloads.

Also bewegen die Dolfijnen nach oben (+ unten, und auch nach vorne (+ hinten. 

Gruss,

Peter D