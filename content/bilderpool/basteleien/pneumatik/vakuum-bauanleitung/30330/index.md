---
layout: "image"
title: "Der Vakuumgreifer in Aktion"
date: "2011-03-26T21:28:35"
picture: "vakuum09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30330
- /details1f02-2.html
imported:
- "2019"
_4images_image_id: "30330"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30330 -->
Generelle Hinweise zur Verwendung

Der hier vorgestellte Vakuumgreifer ist in erster Linie für die Verwendung in Robotern gedacht, bei denen Objekte von oben gegriffen werden.
Solche Objekte können z.B. auch Kugeln sein, die Oberfläche muss allerdings glatt sein und in diesem Fall der Saugnapf nicht zu groß. 

Wenn sich der Greifer über dem Objekt befindet, fährt der Roboter ihn zunächst nach unten, bis der Taster am Greifer aktiviert wird.
Daraufhin wird das Ventil zum Saugnapf umgeschaltet und damit das Vakuum aktiviert:
Der Unterdruck hält jetzt das Objekt fest.
Während dem Transport des Objekts durch den Roboter muss das Vakuum eingeschaltet bleiben!
Beim Absetzen kann wieder der Taster am Greifer verwendet werden:
Sobald dieser aktiviert wird wird das Ventil wieder zurückgeschaltet und damit das Objekt losgelassen.