---
layout: "image"
title: "Der Vakuumgreifer in Aktion"
date: "2011-03-26T21:28:35"
picture: "vakuum10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30331
- /details01e4.html
imported:
- "2019"
_4images_image_id: "30331"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30331 -->
Positiver Nebeneffekt:
Man kann durch diesen Taster am Greifer Objekte stapeln, ohne die Höhe der Objekte umständlich einprogrammieren zu müssen. 

Hinweise zum Ventil für den Saugnapf:
Man kann hierzu entweder eines der blau/schwarzen Handventile verwenden und ggf. mit einem S-Motor (am Interface) automatisieren, oder man kann die neueren Magnetventile verwenden.
Nicht die alten Festo-Ventile verwenden!