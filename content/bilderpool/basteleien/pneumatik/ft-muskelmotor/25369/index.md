---
layout: "image"
title: "Muskelmotor  Druck-Details"
date: "2009-09-26T17:57:08"
picture: "Muskel-deltail2.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25369
- /detailsb13b.html
imported:
- "2019"
_4images_image_id: "25369"
_4images_cat_id: "1709"
_4images_user_id: "22"
_4images_image_date: "2009-09-26T17:57:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25369 -->
Wichtig is das die original Fischertechnik-Ventilen leider nicht gut funktionieren (wieder schliessen) bei einen hoheren Druck >0,85 bar !
Eine niederige Druck ist kein Problem, doch die Verkürzung ist dann aber geringer.  
 
Für hohere Drucken nutze ich 3-Way-Ventilen 11.12.3.BE.12.Q.8.8   (á 29 Euro/st.), Lieferant Sensortechnics  
http://www.sensortechnics.com/home
 
Mit standard Fahrrad Innenreifen  28/25-622/630, Länge 14 cm und einen Druck von 1,2 Bar gibt es sich eine Verkürzung von ca.: 1,2 x 14 x 0,10 = 1,7 cm.
Meine Muskelmotor funktioniert dann Problemlos !
 
Eine längere Schlaug (>>14 cm), eine niedrige Druck, und original Fischertechnik-Ventielen wäre eine gutes Alternativ.
 
 
Gruss, 

Peter Damen
Poederoyen NL