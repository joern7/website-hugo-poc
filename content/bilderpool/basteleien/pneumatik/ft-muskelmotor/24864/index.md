---
layout: "image"
title: "Muskel-1"
date: "2009-08-30T09:28:51"
picture: "ftmuskelmotor1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24864
- /detailsd92a.html
imported:
- "2019"
_4images_image_id: "24864"
_4images_cat_id: "1709"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24864 -->
Ein Nocken und ein gesperrtes Freilauflager übertragen die lineare Bewegung - erzeugt wie bei einer Muskelkontraktion - auf eine Welle und setzen diese in eine Drehbewegung um. 

Bei Druckentlastung fährt der Pneumatischer Muskel wieder in seine Ursprungslänge zurück. Dank des entriegelten Freilauflagers wird diese Bewegung nicht auf die Welle übertragen. 
Bei ehrere pneumatische Muskeln lässt sich eine gleichmäßige Drehbewegung erzeugen.