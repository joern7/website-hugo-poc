---
layout: "image"
title: "Pneumatik-3D-Slurf    (Rüssel)"
date: "2009-11-13T21:18:50"
picture: "Pneumatik-3D-Slurf_008.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25775
- /details7dd3.html
imported:
- "2019"
_4images_image_id: "25775"
_4images_cat_id: "1695"
_4images_user_id: "22"
_4images_image_date: "2009-11-13T21:18:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25775 -->
