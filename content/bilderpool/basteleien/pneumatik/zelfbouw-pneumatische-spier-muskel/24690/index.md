---
layout: "image"
title: "Pneumatischer Muskel"
date: "2009-08-01T16:35:49"
picture: "zelfbouwpneumatischespierpneumatischermuskel1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
fotografen:
- "Peter Damen, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24690
- /detailsfed9.html
imported:
- "2019"
_4images_image_id: "24690"
_4images_cat_id: "1695"
_4images_user_id: "22"
_4images_image_date: "2009-08-01T16:35:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24690 -->
Zelfbouw pneumatische spier / Pneumatischer Muskel 

.	Bij Fischertechnik- of Lego-  Robot-modellen zijn met een pneumatische spier of pneumatik Muskel aanzienlijk grotere trekkrachten mogelijk t.o.v. gangbare pneumatische cilinders. 
.	De professionele Fluidic Muscle Festo DMSP/MAS kosten gemiddeld tussen 150 á 200 Euro/stuk ! 
.	Zelfbouw voor modellen is interessant bij gewenste grote trek/veerkrachten bij lage drukken:  0,5-1,5 bar.

.	Benodigdheden :
o	Gevlochten slang, polyester HEGPV0X20 ,  Conrad-nr. 543054   á 2,70 Euro/m'
o	Racefiets binnenband 28/25-622/630 (binnendiameter 16mm) á 7,00 Euro of gratis bij rijwielhandelaar
o	TC-Afsluitpluggen 16mm (Middelkoop artnr. 2056100150)  á  3,00 Euro/st
o	1/8"PT-tap t.b.v. binnendraad tappen in TC-Afsluitpluggen 16mm
o	Slangklemmen 11-16mm á 0,70 Euro/st
o	1/8" inschroefkoppelingen t.b.v. pneumatik slang á 1,00 Euro/st

.	Racefiets binnenband 28/25-622/630 (binnendiameter 16mm) geeft reeds bij geringe druk een aantrekkelijke lineaire rek/verkorting van ca. 9% en is goedkoop.  
(10mm siliconenslang (wanddikte: 1mm) reageert pas hogere drukken)
.	Een rek/verkorting van ca. 10% houdt in:  0,10 cm verkorting / (bar x cm slang).
Bijvoorbeeld een 14 cm lange slang geeft bij 1,5 bar een verkorting van ca.:  1,5 x 14 x 0,10 = 2,1 cm. 

.	Interessant zijn : 
o	Website Shadow Robot Company:  http://www.shadowrobot.com/
o	http://www.shadowrobot.com/shop.shtml
o	http://de.manu-systems.com/Shadow.shtml

.	Pneumatik Muskel = Pneumatischer Antrieb ohne Kolbenstange mit Kontraktionsmembran, dynamisch, kraftvoll, stick-slip-frei, einfachwirkend ziehend. 
.	Schau mal unter:   http://www.festo.com/cat/de_de/data/doc_de/PDF/DE/DMSP-MAS_DE.PDF
