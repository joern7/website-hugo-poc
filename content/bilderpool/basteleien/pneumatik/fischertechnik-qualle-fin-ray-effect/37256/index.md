---
layout: "image"
title: "Fischertechnik-Qualle  mit  Fin-Ray-Effect + pneumatik Muskel"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel06.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37256
- /details7cbd.html
imported:
- "2019"
_4images_image_id: "37256"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37256 -->
Der Fluidic Muscle oben der  Fischertechnik Qualle, kombiniert mit dem Fin Ray Effect bei jedem Tentakel bildet die zentrale Vortrieb.