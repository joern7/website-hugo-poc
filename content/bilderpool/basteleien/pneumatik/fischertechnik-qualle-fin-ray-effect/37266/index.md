---
layout: "image"
title: "Fischertechnik-Qualle  mit  Fin-Ray-Effect + pneumatik Muskel"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel16.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37266
- /detailscdb7.html
imported:
- "2019"
_4images_image_id: "37266"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37266 -->
Wenn eine Flanke mit Druck beaufschlagt wird, wölbt sich die geometrische Struktur von selbst entgegen der einwirkenden Kraftrichtung. Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Tentakel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.