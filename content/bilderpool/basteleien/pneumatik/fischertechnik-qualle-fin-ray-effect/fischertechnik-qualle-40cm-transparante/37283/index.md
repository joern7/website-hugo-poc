---
layout: "image"
title: "Fischertechnik-Qualle  mit  40cm transparante Bal (2-delig Ballkit-code MT400TNL)"
date: "2013-08-31T23:18:49"
picture: "qualletransparantebal1.jpg"
weight: "1"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37283
- /detailsd1ac.html
imported:
- "2019"
_4images_image_id: "37283"
_4images_cat_id: "2774"
_4images_user_id: "22"
_4images_image_date: "2013-08-31T23:18:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37283 -->
Die blaue Soffy Beachball habe ich ersetzt durch ein 40cm transparante Bal (2-delig Ballkit-code MT400TNL).