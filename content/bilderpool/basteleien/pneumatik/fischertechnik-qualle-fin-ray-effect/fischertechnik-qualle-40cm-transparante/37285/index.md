---
layout: "image"
title: "Fischertechnik-Qualle  mit  40cm transparante Bal (2-delig Ballkit-code MT400TNL)"
date: "2013-08-31T23:18:49"
picture: "qualletransparantebal3.jpg"
weight: "3"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37285
- /detailsd62d.html
imported:
- "2019"
_4images_image_id: "37285"
_4images_cat_id: "2774"
_4images_user_id: "22"
_4images_image_date: "2013-08-31T23:18:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37285 -->
Mit 8 hell-blaue Leds aus einer alte Taschenlampe gibt es dann noch mehr Effekt !