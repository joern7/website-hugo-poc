---
layout: "image"
title: "Fischertechnik-Qualle  mit  Fin-Ray-Effect + pneumatik Muskel"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37257
- /details746e.html
imported:
- "2019"
_4images_image_id: "37257"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37257 -->
Der Fluidic Muscle oben der  Fischertechnik Qualle an der Decke hebt die ca. 6 kg Qualle beim sliessen der 8 Tentakel. Auch fùr dieser nutze ich 3,8mm Reedsensor. Die Monoflop-Funktion der E-Tech-Module sorgt für genau 2 sekunden 2 Bar in der Fluidic Muscle. 

Die blaue Soffy Beachball ersetze ich nächste Woche in einer 40cm transparante Bal (2-delig Ballkit-code MT400TNL). Mit 8 hell-blaue Leds aus einer alte Taschenlampe gibt es dann noch mehr Effekt !

