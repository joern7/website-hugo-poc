---
layout: "image"
title: "Detail Relaissteuerung"
date: "2017-01-29T14:50:29"
picture: "IMG_1076.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Kompressor", "Selbstbau"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45104
- /detailsdc3e.html
imported:
- "2019"
_4images_image_id: "45104"
_4images_cat_id: "3347"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:50:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45104 -->
Die Steuerung umfasst den Druckschalter, das Relais, um 10A /12V schalten zu können. Am Relais sind 1Kondensator, um etwas Nachlauf bei der Abschaltung zu erzielen, um ein Flattern des Relais zu verhindern.