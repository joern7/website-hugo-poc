---
layout: "image"
title: "Vakuumpumpe"
date: "2004-06-13T17:20:45"
picture: "Vakuum5.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2530
- /details7740.html
imported:
- "2019"
_4images_image_id: "2530"
_4images_cat_id: "280"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2530 -->
Im oberen Deckel ist eine Silikon-
Membrane eingebaut, die über einen Stößel
den Mikroschalter betätigt.
Die Pumpe schaltet bei -0,3 bar ab und kurz
vor 0,2 bar wieder ein.