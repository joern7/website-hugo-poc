---
layout: "image"
title: "ft-Rückschlagventil mit Ansaugstutzen zur Unterdruckerzeugung"
date: "2008-11-07T16:44:36"
picture: "11076113.jpg"
weight: "4"
konstrukteure: 
- "alfred.s"
fotografen:
- "alfred.s"
keywords: ["Pneumatik", "Vakuum", "Rückschlagventil"]
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/16231
- /details8c3d.html
imported:
- "2019"
_4images_image_id: "16231"
_4images_cat_id: "280"
_4images_user_id: "390"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16231 -->
Zu sehen sind die Ausgangselemente, ein Rückschlagventil 36970 und ein Schlauchanschluss 35328 und darunter das Ergebnis, wenn man die beiden Teile mit Superkleber zusammenklebt. Mann sieht beim Ventil schön das Loch, bei dem die Luft im Kompressorbetrieb angesaugt wird. Genau darüber muß der Anschluss angeklebt werden und schon hat man ein Rückschlagventil, mit dem man auch Unterdruck erzeugen kann. Beim Kleben ist daher natürlich darauf zu achten, dass das Loch nicht aus Versehen zugeklebt wird, denn sonst wäre das Ventil danach hinüber. Für ungeübte Bastler ist die Bastelei daher nicht zu empfehlen.