---
layout: "image"
title: "Vakuumpumpe"
date: "2007-07-31T14:04:14"
picture: "vakuum4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11230
- /detailsb3d0.html
imported:
- "2019"
_4images_image_id: "11230"
_4images_cat_id: "1015"
_4images_user_id: "557"
_4images_image_date: "2007-07-31T14:04:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11230 -->
hier musste die kurbel befestigt werden da sie sich immer von der achse löste