---
layout: "image"
title: "Vakuumpumpe"
date: "2007-07-31T14:04:14"
picture: "vakuum3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11229
- /detailsb3ee.html
imported:
- "2019"
_4images_image_id: "11229"
_4images_cat_id: "1015"
_4images_user_id: "557"
_4images_image_date: "2007-07-31T14:04:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11229 -->
und hier die gesamte pupe, das im hintergrund ist der geldscheinzähler