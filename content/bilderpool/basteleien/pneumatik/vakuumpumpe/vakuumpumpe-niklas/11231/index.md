---
layout: "image"
title: "Vakuumpumpe"
date: "2007-07-31T14:04:14"
picture: "vakuum5.jpg"
weight: "5"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11231
- /detailsfb92.html
imported:
- "2019"
_4images_image_id: "11231"
_4images_cat_id: "1015"
_4images_user_id: "557"
_4images_image_date: "2007-07-31T14:04:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11231 -->
und hier das etwas abgeänderte Rückschlagventil, der schlauch wurde durch ein abgekipstes stück eines eckrohres am ventil befestigt, einziges etwas abgeändertes Teil an der pumpe