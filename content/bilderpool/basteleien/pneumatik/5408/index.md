---
layout: "image"
title: "Gewinde"
date: "2005-11-27T18:02:32"
picture: "IMG_1712.jpg"
weight: "3"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5408
- /detailsb44f.html
imported:
- "2019"
_4images_image_id: "5408"
_4images_cat_id: "464"
_4images_user_id: "6"
_4images_image_date: "2005-11-27T18:02:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5408 -->
Der Kolben wird mit einem Zentrierbohrer angebohrt, dann mit einem 3mm Bohrer vorgebohrt und schließlich ein 4mm Gewinde gebohrt. Besonderst hilfreich ist hier eine Drehbank mit electronischer Drehzahlregelung.