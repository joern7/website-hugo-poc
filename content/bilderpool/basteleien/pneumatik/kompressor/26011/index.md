---
layout: "image"
title: "Kompressor mit XM Motor und Druckschalter 2"
date: "2010-01-03T11:41:08"
picture: "kompressor2_2.jpg"
weight: "35"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26011
- /detailsb521.html
imported:
- "2019"
_4images_image_id: "26011"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2010-01-03T11:41:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26011 -->
Hier ist der Zylinder zu sehen.