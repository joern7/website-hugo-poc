---
layout: "image"
title: "Kompressor mit XM Motor 2"
date: "2009-10-11T11:09:16"
picture: "KompresorXM2.jpg"
weight: "33"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25535
- /detailsb30c.html
imported:
- "2019"
_4images_image_id: "25535"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2009-10-11T11:09:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25535 -->
Da der Powermotor ja ausläuft habe ich mir eine Variante mit dem XM Motor überlegt.
Bis auf den Excenter ist alles Original ft.
Wäre eigentlich eine schöne Ergänzung für´s bestehende ft Programm, der aktuelle Kompressor ist ja nicht sehr Leistungsstark.