---
layout: "image"
title: "Kompressor mit XM Motor und Druckschalter 1"
date: "2010-01-03T11:41:07"
picture: "kompressor1_2.jpg"
weight: "34"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26010
- /details20f7.html
imported:
- "2019"
_4images_image_id: "26010"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2010-01-03T11:41:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26010 -->
Ich habe mal den von mir entwickelten Kompressor mit dem neuen XM Motor erweitert.
Dieser Aufbau besitzt nun einen Druckdifferenzschalter.
Über den linken Zylinder und den Taster auf der anderen Seite wird der Motor geschaltet.
Der Druckschalter schaltet bei einer Differenz von ca 0,2 bar.
Bis auf den Excenter sind alle Teile original.