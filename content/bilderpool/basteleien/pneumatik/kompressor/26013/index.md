---
layout: "image"
title: "Kompressor mit XM Motor und Druckschalter 4"
date: "2010-01-03T11:41:08"
picture: "kompressor4_2.jpg"
weight: "37"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26013
- /details2f4a-2.html
imported:
- "2019"
_4images_image_id: "26013"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2010-01-03T11:41:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26013 -->
Hier nun die Ansicht von Oben.
Ich finde den Kompressor recht kompakt.
Vor allen Dingen fördert er genug Luftvolumen.