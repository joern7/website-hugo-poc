---
layout: "image"
title: "abd_mg_2.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00616_resize.jpg"
weight: "15"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5991
- /details5df3.html
imported:
- "2019"
_4images_image_id: "5991"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5991 -->
Überflüssige Öffnungen wurden einfach mit (schwarzen) Schrauben geschlossen. Läuft leider etwas lauter als die von Lemo, aber sehr klein und günstig zu bekommen!