---
layout: "image"
title: "abd_mg_1.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00615_rotate_resize.jpg"
weight: "14"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5990
- /details72be.html
imported:
- "2019"
_4images_image_id: "5990"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5990 -->
Die kleine Membranpumpe stammt aus einem automatischen Blutdruckmeßgerät und schafft am FT-Akku zum Glück nicht mehr als 0,6 bar.