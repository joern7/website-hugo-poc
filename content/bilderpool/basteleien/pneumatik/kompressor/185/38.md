---
layout: "comment"
hidden: true
title: "38"
date: "2003-06-05T18:53:18"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
ErgänzungBlockierstrom bei 24V ist 2A, also etwa 50W hat der Motor. Auf jeden Fall hat der Motor eigentlich viel zu viel Kraft für fischertechnik-Modelle und ist damit erste Wahl für Mechanik und hohe Drehzahlen und Getriebe.

Der Motor läuft extrem leicht, Anlaufspannung etwa 1V und lässt sich noch halbwegs gut ans ft-System anpassen, denn die Welle ist 4mm dick. Er stammt aus einem Sonderangebot eines Wiener Elektronikladens, M1 hat zwei Stück, ich ebenfalls und PVD eigentlich den Rest (es lebe der Postversand).