---
layout: "image"
title: "Kompressor-Schaltung mit 2 Drucksensoren + FT-Etec-Module"
date: "2007-02-16T12:34:18"
picture: "Compressor-schakeling_met_FT-Etech-module.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/9035
- /details5138.html
imported:
- "2019"
_4images_image_id: "9035"
_4images_cat_id: "18"
_4images_user_id: "22"
_4images_image_date: "2007-02-16T12:34:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9035 -->
Schaltung 2 (Conrad-) Drucksensoren mit FT-Etec-Module und richtige Dip-Schalter-Position.