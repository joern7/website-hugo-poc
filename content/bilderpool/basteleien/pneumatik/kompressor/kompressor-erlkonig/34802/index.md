---
layout: "image"
title: "Pollin Twin Kompressor"
date: "2012-04-15T21:55:53"
picture: "Pollin_Twin2.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/34802
- /details5547-2.html
imported:
- "2019"
_4images_image_id: "34802"
_4images_cat_id: "2377"
_4images_user_id: "182"
_4images_image_date: "2012-04-15T21:55:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34802 -->
Ich habe mal aus 2 der Pollin Pumpen einen Twin Kompressor aufgebaut.
Die Werte sind schon beeindruckent.
Er füllt den Tank in 3,4 Sekunden auf 0,6 bar.
Dabei brauchen die beiden Pumpen gerade mal 150 mA.
Mein entwickelter Kompressor aus dem Powermotor schaft dies in der gleichen Zeit.
http://www.ftcommunity.de/details.php?image_id=16943
Braucht aber 600 mA dabei.