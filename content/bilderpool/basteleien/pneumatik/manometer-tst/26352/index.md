---
layout: "image"
title: "Mnometer Rückansicht"
date: "2010-02-13T15:24:20"
picture: "manometer2_2.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26352
- /details1f37.html
imported:
- "2019"
_4images_image_id: "26352"
_4images_cat_id: "1834"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26352 -->
Hier die ansicht von hinten.
Auf den Messinganschluß paßt der ft Schlauch genau drauf.