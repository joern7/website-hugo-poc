---
layout: "image"
title: "Das Ergebnis"
date: "2012-01-25T20:25:44"
picture: "ueberundunterdruckbehaelter3.jpg"
weight: "3"
konstrukteure: 
- "Michael Stumberger"
fotografen:
- "Michael Stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34062
- /details8f92.html
imported:
- "2019"
_4images_image_id: "34062"
_4images_cat_id: "2519"
_4images_user_id: "859"
_4images_image_date: "2012-01-25T20:25:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34062 -->
Ein Druckluftbehälter mit Ein- und Ausgang.