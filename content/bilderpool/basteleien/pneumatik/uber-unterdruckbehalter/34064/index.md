---
layout: "image"
title: "Mehr Volumen"
date: "2012-01-25T20:25:44"
picture: "ueberundunterdruckbehaelter5.jpg"
weight: "5"
konstrukteure: 
- "Michael Stumberger"
fotografen:
- "Michael Stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34064
- /details99df.html
imported:
- "2019"
_4images_image_id: "34064"
_4images_cat_id: "2519"
_4images_user_id: "859"
_4images_image_date: "2012-01-25T20:25:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34064 -->
Ich habe bis heute keine Antwort erhalten, wie ich den Verteiler an einen FT-Tank anschließen soll. Also.....ein Anschluß. Das Loch für den Eingang ist jetzt leider verdeckt. Aber ansonsten 10 mm.