---
layout: "overview"
title: "Mini-Luftdrucktank"
date: 2020-02-22T07:42:02+01:00
legacy_id:
- /php/categories/3066
- /categories0590.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3066 --> 
1x Rohrhülse 58 mm mit Federn klar (31663)
*1x Rohrdeckel mit Loch blau (38851)
1x Rohrdeckel schwarz (36703/31591)
*1x Pneumatik-Schlauch (37003)
*1x Schlauchanschluss weiss(35328)
und etwas Elektrokabelisolierband in Schwarz

Man kann natürlich alles mit einem * Doppelt nehmen, dann kann der Speicher von beiden Seiten benutzt werden. 