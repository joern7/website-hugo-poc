---
layout: "image"
title: "Die Dichtung"
date: "2012-01-25T20:25:44"
picture: "ueberundunterdruckbehaelter4.jpg"
weight: "4"
konstrukteure: 
- "Michael Stumberger"
fotografen:
- "Michael Stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34063
- /details2cd1.html
imported:
- "2019"
_4images_image_id: "34063"
_4images_cat_id: "2519"
_4images_user_id: "859"
_4images_image_date: "2012-01-25T20:25:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34063 -->
Ursprünglich wollte ich den Behälter mit Silikon abdichten. Das wäre aber eine relativ dauerhafte Verbindung geworden. Auch im Hinblick auf den nächsten Deckel. Deshalb entschied ich mich für einen O-Ring. Die Flasche ist selbst unter Druck unter Wasser dicht.