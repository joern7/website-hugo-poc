---
layout: "image"
title: "Pneumatik-Durchführung"
date: "2015-03-14T16:23:37"
picture: "pdurch2.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40667
- /details57a9.html
imported:
- "2019"
_4images_image_id: "40667"
_4images_cat_id: "464"
_4images_user_id: "4"
_4images_image_date: "2015-03-14T16:23:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40667 -->
Der Prototyp. Die Durchführung ist für zwei Messingrohre mit 4 mm und 7 mm Außendurchmesser gedacht und braucht zwei O-Ringe 4*1 und 7*1. Die Maße bei O-Ringen sind Innendurchmesser und Materialstärke. Ein Ring 4*1 braucht deshalb eine Bohrung von 6 mm Durchmesser. Das CAD-Programm erwartet bei runden Sachen nicht den Durchmesser, sondern den Radius. An solchen Sachen kommen ruck-zuck ein paar Fehlversuche zusammen.