---
layout: "image"
title: "offene Strebe 45"
date: "2007-01-15T17:09:29"
picture: "Strebe45_1.jpg"
weight: "5"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8465
- /details9050.html
imported:
- "2019"
_4images_image_id: "8465"
_4images_cat_id: "646"
_4images_user_id: "373"
_4images_image_date: "2007-01-15T17:09:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8465 -->
Diese Strebe war bei einer Knobloch-Bestellung unter ganz vielen Anderen zu finden. Lohnte sich nicht wirklich deswegen den Service zu bemühen. Ich fand aber das ich euch das nicht vorenthalten sollte...