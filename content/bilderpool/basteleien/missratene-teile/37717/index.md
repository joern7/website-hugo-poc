---
layout: "image"
title: "Baustein 15 mit Bohrung"
date: "2013-10-12T17:54:18"
picture: "DSCN0966.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/37717
- /detailsc321.html
imported:
- "2019"
_4images_image_id: "37717"
_4images_cat_id: "646"
_4images_user_id: "502"
_4images_image_date: "2013-10-12T17:54:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37717 -->
