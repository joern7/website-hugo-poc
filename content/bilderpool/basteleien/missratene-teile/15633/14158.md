---
layout: "comment"
hidden: true
title: "14158"
date: "2011-04-21T01:26:49"
uploadBy:
- "rolercoaster1967"
license: "unknown"
imported:
- "2019"
---
Diese Verbindung der Teile ist wohl kein Versehen, sondern Absicht. Ende der 70er Jahre gab es ein Modell "Sägewerk", das Bauteile der Grundkästen 50, kombiniert mit Statik und der Serie "3-6" enthielt, wo diese Bauplatten in der "geklebten Form" als Ladung dargestellt wurden, pro Modell insgesamt 3 Pakete. Da hat wohl jemand ein solches Modell zerlegt und in Teilen verkauft. Bei Bedarf hätte ich ein Foto davon. Best Regards...