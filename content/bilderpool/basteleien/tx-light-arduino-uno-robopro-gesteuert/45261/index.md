---
layout: "image"
title: "TX Light"
date: "2017-02-18T22:45:28"
picture: "txlightarduinounodervonroboprogesteuertwird2.jpg"
weight: "2"
konstrukteure: 
- "H. Howey"
fotografen:
- " 	H. Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/45261
- /detailsbf12.html
imported:
- "2019"
_4images_image_id: "45261"
_4images_cat_id: "3371"
_4images_user_id: "34"
_4images_image_date: "2017-02-18T22:45:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45261 -->
Tx Light Ardunino Uno mit Motorshield 2.3 von Adafruit
Aufgelöteter 20pol Stecker, 10 Widerstände und 18r Buchsen
Die 4 Kabel sich die analogen Eingänge wobei einer (I8) auf Ex geht und die anderen analogen Eingänge auf I5, I6, I7
Stromversorgung ft Netzteil 9V auf den Arduino mit gesetzter Brücke auf dem Motorshield
Die Verkabelung ist unter dem Motorshield.

H. Howey
fishfriend