---
layout: "image"
title: "Gesamtüberblick"
date: "2017-07-16T13:02:19"
picture: "IMG_2701.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Aquarium", "Kühlung", "Peltier", "Arduino"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/46098
- /details6918.html
imported:
- "2019"
_4images_image_id: "46098"
_4images_cat_id: "3423"
_4images_user_id: "1359"
_4images_image_date: "2017-07-16T13:02:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46098 -->
wenig fischertechnik - eigentlich nur als Experimentierplattform..

Ziel: Auqarium kühlen, max 29 Grad sind erlaubt, im Sommer unter dem Dach erwärmt sich das 200 l-Becken aber deutlich über diesen Wert.

Gekühlt wird mit einem PeltierElement, dieses wird bei 12V mit ca 5-6A Stromaufnahme an der einen Seite sehr kalt. (-70 Grad sind zu erreichen) die andere Seite wird dafür sehr heiss. Diese Wärme wird mit dem großen Prozessorkühler abgeführt.
Die Kälte-Seite schließt an einen Alurund-Block an, der mittels Wärmeleitpaste thermisch angebunden ist.
Dieser wird umschlungen von einer Kupferrohr-Spirale.

Durch das Kupeferrohr wird mittels einer Aquariumpumpe Wasser durchgepumpt, abgekühlt und dann durch einen Schlauch durch das Wasserbecken geführt und gelangt zurück usw..

Das Ganze wird mittels 6 Temperatursensoren per Arduino überwacht, die Messwerte zyklisch ausgegeben (auf einer kleinen Website, per serieller Schnittstelle und auf dem kleinen OLED Display)

Sensor 0: im Alukühlblock Kälte Seite
Sensor 1: Warme Seite (am Kühlkörper)
Sensor 2: Vorlauftemp vor der KupferrohrSpule
Sensor 3: Rücklauftemp nach der KupferrohrSpule
Sensor 4: Lufttemperatur
Sensor 5: Wassertemperatur im Aquarium

Zugeschaltet wird das Ganze momentan komplett mittels eines Thermostaten, bei dem Ein - und Ausschaltpunkt unabhängig voneinander gesetzt werden können.

Geplante Erweiterungen:
1. der Arduino übernimmt die komplette Steuerung, inkl Überwachung der Lufttemperatur und schaltet rechtzeitig die Kühlung zu.
2. Kontrolle, ob alle Sensoren innerhalb zulässiger Parameter liegen, 
3. Watchdogschaltung (Hardwarelösung), die neu startet, falls sich das System aufhängt
4. das System soll in einer Kunsstoffwanne stehen, um unzulässige Kühlwasseraustritte zu registrieren / alarmieren
5. Fehlermeldung akustischer Alarm, Klartextanzeige und über Webbrowser
6. DYN DNS Anbindung, um via Internet den Status abzufragen
7. Absetezen von Alarmen und Störungen via Internet, z.B. Per Twitter, SMS, E-Mail..