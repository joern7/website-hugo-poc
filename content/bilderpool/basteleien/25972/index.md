---
layout: "image"
title: "Meine erste Teilemodifikation 1/2"
date: "2009-12-21T19:36:10"
picture: "teilemodifikation1.jpg"
weight: "14"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25972
- /details1397.html
imported:
- "2019"
_4images_image_id: "25972"
_4images_cat_id: "463"
_4images_user_id: "998"
_4images_image_date: "2009-12-21T19:36:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25972 -->
Das war mal eine 60er Stange

Mit einem guten Schraubstock und einen schweren Hammer kann man die sehr gut zurechtbiegen

Und davon habe ich gleich 2 hergestellt (und mache gerne auch mehr

ich verkaufe sie um 1 Euro + Versand