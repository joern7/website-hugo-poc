---
layout: "image"
title: "Rückseite"
date: "2008-10-05T17:50:49"
picture: "selbstgebauterakkukasten3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/15822
- /details644c.html
imported:
- "2019"
_4images_image_id: "15822"
_4images_cat_id: "1444"
_4images_user_id: "445"
_4images_image_date: "2008-10-05T17:50:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15822 -->
Die Metallteile sind in der and mit kleinen Spitzchen verankert, welche hinten heraustreten.