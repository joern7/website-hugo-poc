---
layout: "comment"
hidden: true
title: "7494"
date: "2008-10-06T08:56:49"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
@HLGR
Manchmal reichen auch 1500mAh nicht, kaufen kann man ja schon 3000mAh und mehr. Die höhere Spannung ist auch hin und wieder "besser", man hat dann zum Akku Ende hin etwas mehr Reserven.

@Udo2
Wenn man den Deckel offen lässt gehen auch 8 AA Zellen rein. Da bleibt aber kein Platz für Federkontakte, Akkus mit Lötfahnen wären da besser.