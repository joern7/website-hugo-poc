---
layout: "comment"
hidden: true
title: "7488"
date: "2008-10-05T20:51:55"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Sieben Akkus wären vielleicht ratsamer, dann bleibt noch Platz für eine Polyswitch-Sicherung als Kurzschlußschutz.

Den Original-Akku gibt es jetzt übrigens auch mit NiMH-Akku mit 1500 mAh.