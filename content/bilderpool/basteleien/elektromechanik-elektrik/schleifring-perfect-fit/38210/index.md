---
layout: "image"
title: "Schleifring eingebaut"
date: "2014-02-08T22:06:50"
picture: "Schleifring_eingebaut.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38210
- /detailsfad6.html
imported:
- "2019"
_4images_image_id: "38210"
_4images_cat_id: "2843"
_4images_user_id: "1729"
_4images_image_date: "2014-02-08T22:06:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38210 -->
Der in den Drehkranz eingebaute Schleifring in der Seitenansicht..
nur oben ragt er etwas aus dem Drehkranz heraus. (ca. 7 mm, also weniger als das halbe ft Raster)