---
layout: "image"
title: "IR LED Baustein"
date: "2008-01-06T20:09:55"
picture: "ledbaustein3.jpg"
weight: "12"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13290
- /detailse1a1.html
imported:
- "2019"
_4images_image_id: "13290"
_4images_cat_id: "467"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13290 -->
