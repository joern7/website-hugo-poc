---
layout: "image"
title: "Impulsrad für 5 Takte"
date: "2012-03-02T21:46:49"
picture: "Schaltnocken2.jpg"
weight: "20"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/34514
- /detailsab3e.html
imported:
- "2019"
_4images_image_id: "34514"
_4images_cat_id: "467"
_4images_user_id: "59"
_4images_image_date: "2012-03-02T21:46:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34514 -->
Kompaktes Impulsrad für 5 Impulse/U aus Ritzel Z10, 5 x Klemmbuchse 5 und Heißkleber. Die Drehrichtung ist aber entscheidend, da das Ritzel sich sonst von selbst herausdrehen kann. Die extrem kompakten Anschlüsse am Minitaster (oben) resultieren aus der geforderten engen Verbauung dieser Baugruppe (fürs Foto offen).

Eingebaut war diese Kombination in einem Karussell (Münster 2010).