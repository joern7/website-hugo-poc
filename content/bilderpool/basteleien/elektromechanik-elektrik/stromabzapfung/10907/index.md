---
layout: "image"
title: "Stromabzapfung"
date: "2007-06-24T16:06:46"
picture: "HRL58.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10907
- /details226f.html
imported:
- "2019"
_4images_image_id: "10907"
_4images_cat_id: "987"
_4images_user_id: "456"
_4images_image_date: "2007-06-24T16:06:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10907 -->
Ich will bei meinem Einlagerer meines HRL's kein lästiges kabel vom netzteil herumliegen haben, weil es den einlagerer stört, wenn er sich bewegt. Deswegen habe ich 2 Messingachsen aus dem Baumarkt geholt und parallel zur Schiene gebaut. Auf diese Messingachsen leite ich nun den Strom vom Energy Set. Ich habe schon verschiedenes ausprobiert um den Strom abzuzapfen für das Em, aber bisher hatte alles irgendwie geklemmt oder so. Deswegen hier eine (fast) reibungslose Lösung. 
Ich nehme ein Stück Alufolie und mache daran ein Kabel. Eigentlich wollte ich es löten, aber irgendwie ging das nicht. Die Alufolie verfärbte sich. Dann habe ich halt das Kabel in die Alufolie gewickelt, festgedrückt und dann mit Heißkleber außen festkeklebt, damit das kabel nich rausrutscht. Dann noch Stecker dran und fertig.