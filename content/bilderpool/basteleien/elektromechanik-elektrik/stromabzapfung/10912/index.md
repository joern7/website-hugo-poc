---
layout: "image"
title: "Stromabzapfung"
date: "2007-06-24T16:06:47"
picture: "HRL63.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10912
- /details8789.html
imported:
- "2019"
_4images_image_id: "10912"
_4images_cat_id: "987"
_4images_user_id: "456"
_4images_image_date: "2007-06-24T16:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10912 -->
Jetzt sind es 2 für + und - . Bei - natürlich dasselbe, bloß mit grünem Stecker. Und man darf die beiden Abzapfteile nicht direkt übereinander machen sonst gibt es Kurzschluss.