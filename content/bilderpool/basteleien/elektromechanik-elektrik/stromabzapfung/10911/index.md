---
layout: "image"
title: "Stromabzapfung"
date: "2007-06-24T16:06:47"
picture: "HRL62.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10911
- /details31a9.html
imported:
- "2019"
_4images_image_id: "10911"
_4images_cat_id: "987"
_4images_user_id: "456"
_4images_image_date: "2007-06-24T16:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10911 -->
So kann man das Kabel festmachen.