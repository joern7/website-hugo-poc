---
layout: "image"
title: "Steckdose02.JPG"
date: "2006-07-10T19:02:22"
picture: "Steckdose02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6632
- /details570c.html
imported:
- "2019"
_4images_image_id: "6632"
_4images_cat_id: "467"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T19:02:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6632 -->
