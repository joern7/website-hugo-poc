---
layout: "image"
title: "roter Leuchtstein 2"
date: "2008-09-27T21:15:11"
picture: "dsc01876_resize.jpg"
weight: "15"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/15644
- /detailscab9.html
imported:
- "2019"
_4images_image_id: "15644"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2008-09-27T21:15:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15644 -->
Die Buchsen links sind ein wenig zu kurz, geht aber trotzdem. Beim Einpressen muß man einen 5'er Baustein dazwischen klemmen, die Seitenwände knicken sehr schnell ein wie man noch ein wenig sieht...

Die Buchsen rechts müsste man noch etwas kürzen und eventuell aufweiten damit sie nicht wieder rausrutschen.