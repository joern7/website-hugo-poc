---
layout: "image"
title: "Basic construction plate"
date: "2017-10-27T16:56:00"
picture: "P1017619_800.jpg"
weight: "25"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: ["fotoopa", "kogelbaan", "controller"]
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46847
- /details9761-2.html
imported:
- "2019"
_4images_image_id: "46847"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-10-27T16:56:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46847 -->
Basic construction plate to build a super "Kogelbaan". Home made controller with FPGA module inside, for 16 motors, 32 servo's multiple encoders, detectors, display's etc.