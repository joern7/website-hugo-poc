---
layout: "image"
title: "SR_A03.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_A03.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Eigenbau", "Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4026
- /details318c.html
imported:
- "2019"
_4images_image_id: "4026"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4026 -->
Der bedeutende Vorteil dieser Bauart gegenüber früheren Entwürfen ist, dass die Steckerleiste über Kabel abgesetzt ist. Man kann sie am Modell anbringen, wo es grade passt.