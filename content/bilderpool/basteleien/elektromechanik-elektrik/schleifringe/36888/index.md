---
layout: "image"
title: "SR_CAD2"
date: "2013-04-30T21:02:51"
picture: "SR_CAD2.jpg"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36888
- /details03d5.html
imported:
- "2019"
_4images_image_id: "36888"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2013-04-30T21:02:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36888 -->
Schleifring reloaded: diesmal mit Gehäuseteilen aus dem 3D-Drucker. Noch hab ich selber keinen , aber wer weiß...