---
layout: "image"
title: "Schleifring von iPrototype Oberseite"
date: "2013-10-30T22:51:38"
picture: "sriprototype1.jpg"
weight: "33"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/37776
- /detailsf3f5.html
imported:
- "2019"
_4images_image_id: "37776"
_4images_cat_id: "347"
_4images_user_id: "373"
_4images_image_date: "2013-10-30T22:51:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37776 -->
Bei iprototype.nl (Danke an Peter für den Tip!) gibt es einen 6-Adern-Schleifring, der sehr gut in den ft-Drehkranz reinpasst. Er kostet 17 Euro, ist also durchaus bezahlbar: https://iprototype.nl/products/accessoires/cables-wires/slipring-6-wire
Allerdings muss man ein wenig basteln: Die sich drehenden Kabel sind auf der "falschen" Seite, man muss also den Befestigungsring abdrehen/fräsen/sägen/schleifen. Ich habe das hier mit dem Dremel erledigt. Die neue Befestigung sieht man auf dem nächsten Bild.

Bestellen kann man bei iPrototype auch nach Deutschland, die Versandkosten betragen 3,95. Einfach Belgien als Wohnort auswählen, vor die PLZ ein D dazu und im Bestellungskommentar dazuschreiben, dass man nach DE geliefert haben möchte (Englisch/Holländisch). Zum Bestellen muss man aber halt ein bisschen Holländisch können ;-)