---
layout: "image"
title: "IMG_0467.JPG"
date: "2014-01-28T00:15:34"
picture: "IMG_0467.JPG"
weight: "37"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38137
- /details93b4-2.html
imported:
- "2019"
_4images_image_id: "38137"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2014-01-28T00:15:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38137 -->
Zwei Runden weiter, zwei Nummern größer (6 Bahnen, jeweils mit großzügigen 4 mm Breite), und einige Handgriffe weniger für den Zusammenbau. Hier werden die ft-Flachstecker im Inneren des Innenteils versenkt: drei von oben, drei von von unten. Da, wo die Pinzette liegt, kommt ein Stecker nebst Messinghülse von unten hinein; das Kabel wird durch das Loch zwischen den Pinzettenspitzen nach oben gefädelt. Links daneben (unterste Bahn) kommt der Stecker von oben, und die Messinghülse fällt am kürzesten aus.

Das Messingblech vom Vorläufer mit zwei Bahnen ist jetzt "draußen" (zu steif, zu unhandlich), dafür kommt selbstklebende Alufolie (genaue Angabe fehlt, aber nur die gezeigte Marke hat richtig Materialstärke, die andren haben nur ein paar µm).

Trotz allem ist das Montieren ein Geduldsspiel. Die Messingbuchsen müssen etwas angespitzt werden, damit sie beim Einstecken nicht die Folie wegschieben. Das wird also geändert, und die Lösung geht in Richtung "irgendwas mit einem Konus".