---
layout: "image"
title: "SR_003.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_003.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4020
- /detailsc7bf.html
imported:
- "2019"
_4images_image_id: "4020"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4020 -->
Eins der immer wieder auftauchenden Probleme ist es, Strom auf drehende Teile zu bekommen. Die ft-Schleifringe werden nicht mehr hergestellt und sperrig sind sie auch.

Nach allem was ich sagen kann, geht ein Eigenbau nicht an einer Drehmaschine vorbei. Mit Unterlegscheiben und dergleichen kommt man schnell an alle möglichen Grenzen.

Das Foto zeigt den Grundgedanken: 
1. von einem Messingrohr werden Ringe abgestochen. Daran werden Drähte gelötet.
2. Ein Plexiglasrohr wird so abgedreht, dass die Ringe gerade so daraufgeschoben werden können.
3. ein weiteres Plexiglasrohr (oder was sonst gerade da ist) liefert Distanzringe, die zwischen die Messingringe geschoben werden und später die Schleifer in der Bahn halten. Die Ringe müssen so breit sein, dass sich die Lötstellen an den Messingringen nicht berühren.
4. das innere Rohr wird geschlitzt, um die Kabel herauszuführen. Der Schlitz muss so breit sein, dass die Lötstellen darin Platz haben.