---
layout: "image"
title: "3D-Print-Schleifring.jpg"
date: "2013-04-30T21:06:00"
picture: "IMG_8556.JPG"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36889
- /details1657-2.html
imported:
- "2019"
_4images_image_id: "36889"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2013-04-30T21:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36889 -->
Mit so einem 3D-Drucker kann man dem reinen Bastelstadium vielleicht bald entkommen. Die hier von Hand gedrehten Ringe noch durch handelsübliche Kupfer-DIchtungsringe ersetzen und hier noch dies und da noch jenes ... dann kann es was werden.