---
layout: "image"
title: "SR_D10.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_D10.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4032
- /detailsc680-2.html
imported:
- "2019"
_4images_image_id: "4032"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4032 -->
Modell D ist auch etwas älter und etwas zu aufwändig gebaut. Es wurde im Radar verwendet (http://www.ftcommunity.de/categories.php?cat_id=28 ).