---
layout: "image"
title: "Unterseite"
date: "2012-01-04T08:27:46"
picture: "drehkranzmitschleifring3.jpg"
weight: "30"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/33838
- /details5253-2.html
imported:
- "2019"
_4images_image_id: "33838"
_4images_cat_id: "347"
_4images_user_id: "182"
_4images_image_date: "2012-01-04T08:27:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33838 -->
Hier sieht man wie die Federkontakte innen herauskommen.
Funktioniert echt gut......