---
layout: "image"
title: "Schleif2x_0465.jpg"
date: "2014-01-10T22:30:34"
picture: "Schleif2x_0465.jpg"
weight: "36"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38036
- /detailsec7b-2.html
imported:
- "2019"
_4images_image_id: "38036"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2014-01-10T22:30:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38036 -->
Alle Kontaktierungen erfolgen durch reines Klemmen (nichts ist gelötet). Anstelle der Drähte kommt da besser abisolierte Litze hin, die ist elastischer. Aber dann müssen die hier einfach umgebogenen Enden wieder sauber geklemmt werden, sonst rutscht der Kram womöglich wieder heraus.
Die Schleifbahnen bestehen aus dicker Alufolie (Marke "Joghurtdeckel" oder ähnliches). Edel wird es mit Streifen aus dem dünnsten Messingblech, das der Modellbauhandel aufbieten kann. Die Streifen werden von beiden Seiten in das Durchgangsloch nach innen gebogen und dort mittels Kontakthülse festgeklemmt.