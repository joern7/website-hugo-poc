---
layout: "image"
title: "SR_E07.JPG"
date: "2005-04-20T13:36:07"
picture: "SR_E07.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4034
- /details9a19.html
imported:
- "2019"
_4images_image_id: "4034"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4034 -->
Aus diesem Bild lernt man eines: bei Schleifringen ist die Trommel immer an der ANDEREN Seite des Drehkranzes zu befestigen. Das wurde hier nicht rechtzeitig bedacht, die Hilfskonstruktion läßt sehr zu wünschen übrig. Naja, das Teil stammt noch aus der Zeit V.D.D. (vor der Drehmaschine), da könnte man jetzt noch mal einen Ring drumrum ziehen.

Die Methode, Steckerbuchsen aus Aderendhülsen auf einem  2,6mm-Bohrer zu quetschen und dann mit ordentlich Heißkleber vollzukleistern, liefert auch noch nicht das, was man sich so wünschen würde.