---
layout: "image"
title: "Schleif2x_0460.jpg"
date: "2014-01-10T22:23:53"
picture: "Schleif2x_0460.jpg"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38035
- /detailsf948.html
imported:
- "2019"
_4images_image_id: "38035"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2014-01-10T22:23:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38035 -->
Was lange währt...

Die Kontakthülsen müssen noch gekürzt werden (und sie gehören auf die Unterseite) und an ein paar Stellen muss das Design einmal mehr geändert werden, aber: das ist ein 3D-gedruckter Schleifring mit zwei Bahnen in der Taschengeld-Preisklasse. Große Brüder mit 4 und 6 Bahnen hat er auch schon.