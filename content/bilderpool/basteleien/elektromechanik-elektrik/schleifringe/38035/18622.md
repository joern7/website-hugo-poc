---
layout: "comment"
hidden: true
title: "18622"
date: "2014-01-28T00:21:47"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Befestigung ist da gar nicht vonnöten: das rotierende Innenteil hat Zapfen, um damit über BS7,5 und die alte Seilrolle auf der Achse mit zu drehen. Bei den Steckern außen muss nur das Mitdrehen verhindert werden, was zur Not schon das Kabel übernehmen kann.

Gruß,
Harald