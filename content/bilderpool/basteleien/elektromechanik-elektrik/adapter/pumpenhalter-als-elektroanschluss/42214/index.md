---
layout: "image"
title: "Die Rückseite"
date: "2015-11-06T14:46:04"
picture: "pic2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Hohlstecker", "Hohlsteckbuchse", "Stromanschluß", "Pumpenhalter", "Elektroanschluß", "130770"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42214
- /details71b8.html
imported:
- "2019"
_4images_image_id: "42214"
_4images_cat_id: "3149"
_4images_user_id: "1557"
_4images_image_date: "2015-11-06T14:46:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42214 -->
Die Hohlsteckbuchse paßt auf die Rückseite als wäre sie extra dafür gemacht.

An der Buchse ist noch ein dritter Anschluß für einen Schaltkontakt. Der wird hier nicht benutzt.