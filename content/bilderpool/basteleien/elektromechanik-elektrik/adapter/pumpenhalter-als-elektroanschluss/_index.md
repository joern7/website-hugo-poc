---
layout: "overview"
title: "Pumpenhalter als Elektroanschluß"
date: 2020-02-22T07:44:37+01:00
legacy_id:
- /php/categories/3149
- /categoriesa34a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3149 --> 
Wie man aus einem Pumpenhalter (130770) einen preisgünstigen und stabilen Niedervolt-Anschluß für seine Modelle machen kann.