---
layout: "image"
title: "Schalter im Potihalter"
date: "2018-05-12T10:30:23"
picture: "schalterhalter2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47638
- /details9236.html
imported:
- "2019"
_4images_image_id: "47638"
_4images_cat_id: "3512"
_4images_user_id: "1557"
_4images_image_date: "2018-05-12T10:30:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47638 -->
Die Zahnscheibe liegt zwischen Schaltergehäuse und Deckscheibe. Noch etwas Litze, Lötzinn und hinterher die Anschlüsse mit Schrumpfschlauch überziehen, gibt den perfekten Look zur sicheren Funktion.