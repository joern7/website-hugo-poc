---
layout: "image"
title: "Controller for my Fischertechnik project, rear view."
date: "2017-10-29T18:28:21"
picture: "P7317212_800.jpg"
weight: "29"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: ["fotoopa", "FPGA", "knikkerbaan", "kogelbaan", "controller"]
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46852
- /detailsc2f1.html
imported:
- "2019"
_4images_image_id: "46852"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-10-29T18:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46852 -->
Controller for my Fischertechnik project, rear view.
Photo was taken when the board assembly was not done completely. Shows mainly the rear connectors. There is possibility to expand from 32 to 64 servo motors but then additional connections must be made. Also the NeoLeds, now 128, are no limitation. There are equipped with two loops, each of which may go up to at least 256 color LEDs. The color table for these LEDs is in the FPGA and uses only little memory ram. Neoleds have 24 bit color space. All servo motors have 4 predefined positions:
N: neutral or powerup
L: left position
M: middle position
R: right position
For each servo, these values can be adjusted by the user. Values typically range between 1200 and 1800 usec pulse width.
Central part is the FPGA module. This is the DE0-nano board with 22.000 LE (logic elements). The entire program currently has 3200 LE or 14% of capacity of the FPGA chip. Most of the program is written (verilog code). The rest will be written if all modules are mounted.
Later more info.