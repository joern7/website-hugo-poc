---
layout: "image"
title: "Robo Control 1"
date: "2017-01-25T17:55:42"
picture: "IMG_2290_400.jpg"
weight: "13"
konstrukteure: 
- "DS"
fotografen:
- "DS"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dieschra"
license: "unknown"
legacy_id:
- /php/details/45081
- /details9069-2.html
imported:
- "2019"
_4images_image_id: "45081"
_4images_cat_id: "114"
_4images_user_id: "2704"
_4images_image_date: "2017-01-25T17:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45081 -->
Smartphone steuert Robo Mobil über Bluetooth