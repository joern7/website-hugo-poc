---
layout: "image"
title: "Zugentlastung"
date: "2005-03-04T14:31:33"
picture: "Zugentlastung.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3680
- /details18f2.html
imported:
- "2019"
_4images_image_id: "3680"
_4images_cat_id: "114"
_4images_user_id: "103"
_4images_image_date: "2005-03-04T14:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3680 -->
Um ein ständiges Herausfallen der Stecker zu vermeiden baue ich eine kleine Zugentlastung dran