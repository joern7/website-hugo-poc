---
layout: "image"
title: "IR-Empfänger 1"
date: "2009-01-28T14:43:49"
picture: "schaltplanteile13.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17182
- /detailsa855.html
imported:
- "2019"
_4images_image_id: "17182"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:49"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17182 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.