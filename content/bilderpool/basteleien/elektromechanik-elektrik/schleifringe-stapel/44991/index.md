---
layout: "image"
title: "3 Schleifringe, Achse und Plattkontakt"
date: "2016-12-31T17:34:58"
picture: "aIMG_20161031_123346.jpg"
weight: "1"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["schleifringe", "stromschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44991
- /detailsa6a7.html
imported:
- "2019"
_4images_image_id: "44991"
_4images_cat_id: "3345"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44991 -->
Drei der Schleifringe übereinander. Die Kabel sind durch die kleinen Löcher gezogen worden.
Dazu eine Plattkontakt, an den ich ein Kabel angelötet habe.
Und die Stromschiene als weiteren Kontakt.

= 8 Kontakte

Dieses Monstrum wurde in meinem Klassikkran eingesetzt.