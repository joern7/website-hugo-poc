---
layout: "image"
title: "Kästchen mit Alufolie"
date: "2006-10-23T17:47:13"
picture: "Alu-folien_Sensor_001.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7212
- /detailsfcbb.html
imported:
- "2019"
_4images_image_id: "7212"
_4images_cat_id: "692"
_4images_user_id: "453"
_4images_image_date: "2006-10-23T17:47:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7212 -->
