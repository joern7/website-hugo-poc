---
layout: "image"
title: "Schaltpult"
date: "2008-11-28T18:20:47"
picture: "BILD0419.jpg"
weight: "17"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
keywords: ["Schaltpult", "Steuerpult", "Schalter", "Conrad", "Steuerung"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16518
- /details75d5.html
imported:
- "2019"
_4images_image_id: "16518"
_4images_cat_id: "467"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T18:20:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16518 -->
Dieses schaltpult hab ich mithilfe einer fräse gebaut ich für das ganze schaltpult nur 40 € augegeben , alle teil sind bei Conrad erhältlich, wenn jemand ein ähnliches schaltpult bauen will kann ich ihm gerne die artikelnummern geben