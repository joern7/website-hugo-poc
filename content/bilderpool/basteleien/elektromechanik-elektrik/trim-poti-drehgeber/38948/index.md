---
layout: "image"
title: "Trimmer"
date: "2014-06-13T14:52:54"
picture: "Trim_2.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Trim", "Trimmer", "Poti", "Potentiometer", "Drehgeber"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38948
- /detailsb446.html
imported:
- "2019"
_4images_image_id: "38948"
_4images_cat_id: "2914"
_4images_user_id: "1729"
_4images_image_date: "2014-06-13T14:52:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38948 -->
das Auge für die Potentiometer-Drehachse ist wie geschaffen für Fischertechnik Rastachsen.
Die Achse klemmt gut, es bleibt leider ein bisschen Spiel.
Also, mit ein paar Grad Drehwinkel als Spiel wird man rechnen müssen. Für Anwendungen mit hohen Genauigkeitsanforderungen  eher nicht geeignet.
Aber diese Potentiometer sind Cent-Artikel! So günstig wie damit kann man keinen Drehgeber/Winkelmesser bauen!
Der max. Drehwinkel beträgt ca. 270 Grad.