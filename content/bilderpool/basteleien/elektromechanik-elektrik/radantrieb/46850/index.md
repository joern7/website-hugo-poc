---
layout: "image"
title: "Rad overview"
date: "2017-10-29T15:22:10"
picture: "P1017659.jpg"
weight: "2"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: ["Rad", "fotoopa"]
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46850
- /detailse2b0.html
imported:
- "2019"
_4images_image_id: "46850"
_4images_cat_id: "2179"
_4images_user_id: "2787"
_4images_image_date: "2017-10-29T15:22:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46850 -->
Rad overview. I started with the first module, a rad wheel diametere 510 mm. There are 2 outputs, one full above (1)  and one at the half height at the back (2). The upper output is servo operated (5). This servo can raise the ejector block allowing the passage to the 2nd exit. This servo can be operated remotely or via the control panel. There are 6 neodymium magnets (3), one on each arm for a magnet detector. This information can be used by the controller. The steel ball is pulled through a block with a neodymium magnet (4). To reduce friction, a steel shaft is used (6) on ball bearings (D8xd4xB3). An XM motor is used for the chain drive (7).
The input for the steel balls must still be made.