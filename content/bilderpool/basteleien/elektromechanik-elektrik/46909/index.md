---
layout: "image"
title: "Chain module ball track"
date: "2017-11-08T17:20:12"
picture: "P1017714_800.jpg"
weight: "31"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46909
- /detailsf166.html
imported:
- "2019"
_4images_image_id: "46909"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46909 -->
Chain module ball track. This is a new module that can be added to my project. The steel balls are first pulled upward. Once over the top there are 3 outputs possible. Here you see the highest level output. The servo is fully on the right to catch the ball. The total height is 615mm. The steel shaft of the chain is mounted with ball bearings D8xd4xB3 mm ( inside the gray block ) to reduce friction. Aluminum square profile (15x15x1.5mm) is used for stiffness. Every 30mm, a threaded bore M4 is provided to mount the Fischertechnik components. The aluminum profile is not Fischertechnik part but homebuilt.

The magnetic ball holder (FI-119850) is not always supplied with the same magnetic polarity (N / S). This caused problems with my unipolar hall detector. Fortunately, I was able to recover the magnet using a strong neodymium magnet and then reverse the position.