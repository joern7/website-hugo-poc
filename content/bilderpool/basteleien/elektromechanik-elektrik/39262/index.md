---
layout: "image"
title: "Zugriff verweigert!"
date: "2014-08-20T19:00:35"
picture: "IMG_1396.jpg"
weight: "24"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: ["Schlüsselschalter", "david-ftc", "Schloss", "Schlüssel"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39262
- /detailsa927.html
imported:
- "2019"
_4images_image_id: "39262"
_4images_cat_id: "467"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T19:00:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39262 -->
Hier sieht man einen Schlüsselschalter aus einem alten PC, den ich auf eine Fischertechnik Bauplatte montiert habe. Wenn man damit nicht seine Modelle vor Unbefugten schützen kann..