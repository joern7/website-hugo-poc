---
layout: "image"
title: "Offenes Netzteil von Oben gesehen"
date: "2016-07-03T13:57:21"
picture: "DSC08084.jpg"
weight: "2"
konstrukteure: 
- "serberer"
fotografen:
- "serberer"
keywords: ["Stromversorgung", "PC-Netzteil"]
uploadBy: "serberer"
license: "unknown"
legacy_id:
- /php/details/43831
- /detailseb94.html
imported:
- "2019"
_4images_image_id: "43831"
_4images_cat_id: "3247"
_4images_user_id: "2610"
_4images_image_date: "2016-07-03T13:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43831 -->
Umgebautes PC- Netztteil auf 9 Volt mit Zwei Ausgängen beide bis Max. 2 Ampere, wobei einer der beiden Regelbar ist bis 9V und der andere Fix auf 9V ist.