---
layout: "image"
title: "Controller intern view with FPGA"
date: "2017-10-27T19:37:43"
picture: "P8047230_800.jpg"
weight: "26"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: ["fotoopa", "FPGA", "controller", "hardware"]
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46848
- /details828c.html
imported:
- "2019"
_4images_image_id: "46848"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-10-27T19:37:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46848 -->
Internal view controller fotoopa with FPGA module.