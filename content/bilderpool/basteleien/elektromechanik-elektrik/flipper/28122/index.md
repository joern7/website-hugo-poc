---
layout: "image"
title: "Slingshot 2"
date: "2010-09-14T20:01:16"
picture: "flipper11.jpg"
weight: "11"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28122
- /details4d5b.html
imported:
- "2019"
_4images_image_id: "28122"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28122 -->
Links und Rechts sieht man die Selbstbauschalter. Ich konnte keine originalen Fischertechniktaster verwenden da diese nur sehr schwer schließen.