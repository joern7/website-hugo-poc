---
layout: "image"
title: "flipper09.jpg"
date: "2010-09-14T20:01:15"
picture: "flipper09.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28120
- /details99d7.html
imported:
- "2019"
_4images_image_id: "28120"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28120 -->
Rechts sieht man die Figuren die sich drehen wenn man die Taster davor trifft. Außerdem schaltet sich das Radio ein.