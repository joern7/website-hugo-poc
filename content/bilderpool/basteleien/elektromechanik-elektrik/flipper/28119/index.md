---
layout: "image"
title: "Ball Lock"
date: "2010-09-14T20:01:14"
picture: "flipper08.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28119
- /detailse473.html
imported:
- "2019"
_4images_image_id: "28119"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28119 -->
Hier sieht man den Ball Lock der den Zylinder ausgefahren hat.