---
layout: "image"
title: "Slingshot motor"
date: "2010-09-14T20:01:16"
picture: "flipper18.jpg"
weight: "18"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28129
- /detailsd135.html
imported:
- "2019"
_4images_image_id: "28129"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28129 -->
Anfangs dachte ich nicht das diese Konstruktion wirklich funktioniert, ich wurde jedoch eines besseren belehrt.