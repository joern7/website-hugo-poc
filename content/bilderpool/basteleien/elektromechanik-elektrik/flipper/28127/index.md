---
layout: "image"
title: "Lampe"
date: "2010-09-14T20:01:16"
picture: "flipper16.jpg"
weight: "16"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28127
- /details6d6e.html
imported:
- "2019"
_4images_image_id: "28127"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28127 -->
Lampe mit Reflektor und Abdeckung