---
layout: "image"
title: "Rampe"
date: "2010-09-14T20:01:13"
picture: "flipper04.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28115
- /details59ef.html
imported:
- "2019"
_4images_image_id: "28115"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28115 -->
Hier sieht man die Auffahrt für die Rampe, die selbst gelötet ist. Der Ball überbrückt den Kontakt (orangenes Kabel) und meldet dem Interface einen Rampentreffer.