---
layout: "image"
title: "flipper07.jpg"
date: "2010-09-14T20:01:14"
picture: "flipper07.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28118
- /details2e81.html
imported:
- "2019"
_4images_image_id: "28118"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28118 -->
Links oben sieht man den Ball Lock. Rollt die Kugel hinein so bekommt man noch einen Ball in die Abschussrampe gelegt. Schießt man diese hinaus so wird die Kugel wieder aus dem Ball Lock freigegeben und man hat einen Multiball.