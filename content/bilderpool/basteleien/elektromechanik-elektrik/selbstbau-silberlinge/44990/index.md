---
layout: "image"
title: "Gleichrichter (2 x)"
date: "2016-12-31T17:34:58"
picture: "IMG_20161226_121114.jpg"
weight: "1"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: ["gleichrichter", "brückengleichrichter", "stromversorgung"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44990
- /details75a4.html
imported:
- "2019"
_4images_image_id: "44990"
_4images_cat_id: "3344"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44990 -->
Mein erster Versuch:
2 x Gleichrichter mit großem Kondensator zur Glättung in eines der Kästcen von ft eingebaut.
Damit kann man am Lichtanschluss des klassik Trafos auch Motoren anschließen (mit etwas "Overdrive") oder jede Menge LEDs.

Wurde bei mir zum ersten Mal in der Schrägseilbrücke eingesetzt, da ich über 20 LEDs (mit großer Leistung) betreiben musste.

Leider war das mein erstes Experiment, weswegen folgende Probleme auftraten, die ich nicht ganz so gut gelöst habe (aber man lernt ja):
- die Platine ist nur in das Kästchen gelegt und mit Schaumstoff verklemmt, da man das Nylon der Kästchen nicht kleben kann (und Ameisensäure hate ich nicht da).
- Ich versuchte die Platine und den Deckel mit einer Schraube (siehe Loch in der Mitte) M3 zu verbinden, jedoch ist das Material der Kästchen so dünn, dass es unten ausriss und keine Schraube hält.
- Die Beschriftung mit einem Weißen Lackstift ist klasse - sollte ich aber zukünftig besser planen.

Die Buchsen sind sehr gut!

Mein nächstes Projekt: 2x4 UM Relaisbaustein, sowie Blinkgenerator mit variabler Geschwindigkeit und Puls-Pause. Mal sehen, was ich noch an Bedarf habe....