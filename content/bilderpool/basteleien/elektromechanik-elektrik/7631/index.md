---
layout: "image"
title: "Power-Adapter 1"
date: "2006-11-26T23:08:57"
picture: "dsc00957_resize.jpg"
weight: "5"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["power", "adapter", "netzteil"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/7631
- /details25df.html
imported:
- "2019"
_4images_image_id: "7631"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7631 -->
Mit dem rechten Adapter ist es möglich, den Power-Controller "mobil" mit dem Akku-Pack zu betreiben. Brauchte ich für eine einfache Fernbedienung.

Mit dem linken Adapter kann ein stationäres Modell über das FT-Netzteil mit Strom versorgt werden. Nicht immer ist eine regelbare Ausgangsspannung notwendig bzw. gewünscht.