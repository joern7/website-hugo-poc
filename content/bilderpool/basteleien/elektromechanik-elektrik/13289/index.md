---
layout: "image"
title: "IR LED Baustein"
date: "2008-01-06T20:09:45"
picture: "ledbaustein2.jpg"
weight: "11"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13289
- /details70af-2.html
imported:
- "2019"
_4images_image_id: "13289"
_4images_cat_id: "467"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13289 -->
Hier sieht man noch den eingebauten Widerstand, der vorgängig angelötet wurde.