---
layout: "image"
title: "Scheibe mit 100 Impulsen/Umdrehung für CNY37"
date: "2009-11-26T17:11:26"
picture: "scheibe.jpg"
weight: "18"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/25817
- /details569d.html
imported:
- "2019"
_4images_image_id: "25817"
_4images_cat_id: "467"
_4images_user_id: "-1"
_4images_image_date: "2009-11-26T17:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25817 -->
Gemacht mit Openoffice Calc und Paint