---
layout: "image"
title: "Teile"
date: "2014-12-01T20:00:17"
picture: "P1040510.jpg"
weight: "32"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["unbekannt", "Exoten"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/39888
- /detailse1bb.html
imported:
- "2019"
_4images_image_id: "39888"
_4images_cat_id: "463"
_4images_user_id: "1729"
_4images_image_date: "2014-12-01T20:00:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39888 -->
Ich habe mehrere dieser Teile. Habe keine Ahnung, zu was die gut sind, wozu die passen und was der ursprüngliche Verwendungszweck war.
Ich vermute, die haben irgendwas mit Statik zu tun