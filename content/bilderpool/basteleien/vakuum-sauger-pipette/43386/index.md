---
layout: "image"
title: "Untertasse soll angesaugt werden"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette3.jpg"
weight: "3"
konstrukteure: 
- "lemkajen"
fotografen:
- "lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43386
- /details5256.html
imported:
- "2019"
_4images_image_id: "43386"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43386 -->
Sauger im teilw. vakuumierten Zustand