---
layout: "overview"
title: "Vakuum-Sauger / Pipette"
date: 2020-02-22T07:46:54+01:00
legacy_id:
- /php/categories/3223
- /categories23c5.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3223 --> 
Aus ausgemusterten Teilen eines Agfa-Speicherfolienlesers für belichtete Röntgen-Speicherfolien konnnte ich ein paar Sauger bekommen.
diese habe ich durch leicht modifizierte Magnet-Ventil-Schlauchanschlüsse auf unsere Fischertechnik-Pneumatik-Schläuche adaptiert