---
layout: "comment"
hidden: true
title: "21169"
date: "2015-10-29T22:19:33"
uploadBy:
- "PHabermehl"
license: "unknown"
imported:
- "2019"
---
Grins ... der Herr Roth sagte, er hätte diese Teile noch nie in Natura gesehen... Er empfahl vorsichtiges Abfeilen von normalen 15x45er Bauplatten. Da aber bei meinem Kran nix klemmt und abgefeilte Platten auch nicht original wären, spare ich mir das.