---
layout: "image"
title: "Kupplungsidee 3"
date: "2010-11-24T22:26:24"
picture: "SDC11021.jpg"
weight: "7"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29362
- /details0291.html
imported:
- "2019"
_4images_image_id: "29362"
_4images_cat_id: "2129"
_4images_user_id: "381"
_4images_image_date: "2010-11-24T22:26:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29362 -->
Universalkupplung mit 4 Magneten