---
layout: "image"
title: "Industrieprogramm 6000/1 offen"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37340
- /details2bdd.html
imported:
- "2019"
_4images_image_id: "37340"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37340 -->
Fischertechnik Industrieprogramm 6000/1