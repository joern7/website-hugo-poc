---
layout: "image"
title: "ftmodellsammlung46.jpg"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung46.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37361
- /details5a53.html
imported:
- "2019"
_4images_image_id: "37361"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37361 -->
Service 2 aus 1974 mit Originalbestückung (natürlich alles Neuteile unbespielt)