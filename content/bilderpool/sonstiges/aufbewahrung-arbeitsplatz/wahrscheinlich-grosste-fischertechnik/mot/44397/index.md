---
layout: "image"
title: "mot _erste Serie"
date: "2016-09-25T21:53:05"
picture: "mot01.jpg"
weight: "1"
konstrukteure: 
- "allsystemgmbh"
fotografen:
- "allsystemgmbh"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44397
- /details0d2a.html
imported:
- "2019"
_4images_image_id: "44397"
_4images_cat_id: "3283"
_4images_user_id: "1688"
_4images_image_date: "2016-09-25T21:53:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44397 -->
keine seitlichen Buchsen