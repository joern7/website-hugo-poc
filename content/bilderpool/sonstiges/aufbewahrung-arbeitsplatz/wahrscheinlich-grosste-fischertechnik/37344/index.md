---
layout: "image"
title: "'Mondlandung'"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37344
- /details8efb.html
imported:
- "2019"
_4images_image_id: "37344"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37344 -->
Mondlandung  anno 1969