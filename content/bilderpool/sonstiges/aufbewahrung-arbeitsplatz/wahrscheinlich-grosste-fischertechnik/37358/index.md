---
layout: "image"
title: "Service 1 und 2"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung43.jpg"
weight: "43"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37358
- /details918b-2.html
imported:
- "2019"
_4images_image_id: "37358"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37358 -->
Die "Urbox" Service 1 gemäß Stückliste Clubheft aus 1972 sowie eine weitere Service 1  mit Originaleinsätzen aus 1975 sowie zwei Service 2 aus 1974 mit Originalbestückung (natürlich alles Neuteile unbespielt)