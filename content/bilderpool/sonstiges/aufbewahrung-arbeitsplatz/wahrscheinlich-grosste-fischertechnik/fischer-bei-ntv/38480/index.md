---
layout: "image"
title: "Fischertechnik im Fernsehn"
date: "2014-03-22T10:01:51"
picture: "fischerbeintv5.jpg"
weight: "5"
konstrukteure: 
- "Johanna und Konstantin Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38480
- /details181a.html
imported:
- "2019"
_4images_image_id: "38480"
_4images_cat_id: "2871"
_4images_user_id: "968"
_4images_image_date: "2014-03-22T10:01:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38480 -->
Weißabgleich mit Johanna und Konstantin