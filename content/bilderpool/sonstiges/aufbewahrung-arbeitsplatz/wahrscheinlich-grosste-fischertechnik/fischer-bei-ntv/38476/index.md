---
layout: "image"
title: "Fischertechnik im Fernsehn"
date: "2014-03-22T10:01:51"
picture: "fischerbeintv1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38476
- /detailsd34b.html
imported:
- "2019"
_4images_image_id: "38476"
_4images_cat_id: "2871"
_4images_user_id: "968"
_4images_image_date: "2014-03-22T10:01:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38476 -->
Ich war erstaunt über die Gründlichkeit mit der das Team gearbeitet hat. Was die später aus dem Material zusammenschneiden
wird man sehen :)