---
layout: "image"
title: "Regal aus den 80ern Vorstufe und Geometric"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37320
- /detailsb81e.html
imported:
- "2019"
_4images_image_id: "37320"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37320 -->
Geometric 1-4