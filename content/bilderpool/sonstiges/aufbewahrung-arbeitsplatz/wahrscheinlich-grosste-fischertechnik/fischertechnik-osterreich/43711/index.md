---
layout: "image"
title: "Fischertechnik Sammlung von Heinz aus Graz wechselt den Standort und geht in das Raritätenkabinett"
date: "2016-06-09T11:17:03"
picture: "fischertechnikausoesterreich01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43711
- /details1546.html
imported:
- "2019"
_4images_image_id: "43711"
_4images_cat_id: "3238"
_4images_user_id: "1688"
_4images_image_date: "2016-06-09T11:17:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43711 -->
Perfektes Sortiersystem...