---
layout: "image"
title: "Fischertechnik Sammlung von Heinz aus Graz"
date: "2016-06-09T11:17:03"
picture: "fischertechnikausoesterreich19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43729
- /detailsb3e7.html
imported:
- "2019"
_4images_image_id: "43729"
_4images_cat_id: "3238"
_4images_user_id: "1688"
_4images_image_date: "2016-06-09T11:17:03"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43729 -->
Sortiersystem_3