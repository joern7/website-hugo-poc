---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:49"
picture: "fischertechnikmoebel048.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44244
- /detailsde36-2.html
imported:
- "2019"
_4images_image_id: "44244"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:49"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44244 -->
FT Neuzeit für mich...