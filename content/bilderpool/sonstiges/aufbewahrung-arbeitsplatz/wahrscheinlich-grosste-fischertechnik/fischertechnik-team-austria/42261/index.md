---
layout: "image"
title: "Deko Vitrine mit Saturn Rakete"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett44.jpg"
weight: "44"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42261
- /details994f.html
imported:
- "2019"
_4images_image_id: "42261"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42261 -->
