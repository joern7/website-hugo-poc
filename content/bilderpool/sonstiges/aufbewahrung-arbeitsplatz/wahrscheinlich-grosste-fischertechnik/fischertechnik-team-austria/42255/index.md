---
layout: "image"
title: "Aufbau der Fischertechnik Vitrine"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett38.jpg"
weight: "38"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42255
- /details9a2b-2.html
imported:
- "2019"
_4images_image_id: "42255"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42255 -->
