---
layout: "comment"
hidden: true
title: "21270"
date: "2015-11-10T10:22:48"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Roland, vielen Dank dafür!!! 

Das sind ja echte Raritäten, die Du da aufgetrieben hast. Finde es sehr interessant, was die Briten da für die Schule entwickelt haben. Da wäre ich gerne zur Schule gegangen, wenn es zum Einsatz gekommen wäre.

Es sind ja auch viele noch unbestückte Boards dabei. Dann sollten die Schüler wohl auch das Löten lernen.

Ich habe jetzt drei verschiedene Boards in den Bildern entdeckt:
- eins für den Fischertechnik PIC Buggy, welches 2 Eingänge hat für die Kollisionstaster und 2 Motoren sowie einen Beeper steuern kann.
- den PIC-Logikator, der einen Fotowiderstand, ein Poti und 3 Taster als Eingänge hat und 8 digitale Logik-Ausgänge, deren Zustand mit LEDs angezeigt wird.
- und das Economatics-Board mit 5 Eingängen und 6 Ausgängen

Alle sind für PIC-Mikrocontroller vorgesehen.

Was machst Du mit den ganzen Boards? Willst Du sie in Deinem Museum ausstellen oder kommen auch ein paar zum Einsatz?

Danke und Gruß, Dirk