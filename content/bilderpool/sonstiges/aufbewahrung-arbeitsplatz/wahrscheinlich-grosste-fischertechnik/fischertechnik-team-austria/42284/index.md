---
layout: "image"
title: "Supermodelle aus den 80ern mit Fernsteuerung"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett67.jpg"
weight: "67"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42284
- /details9596.html
imported:
- "2019"
_4images_image_id: "42284"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42284 -->
