---
layout: "image"
title: "FT Baufahrzeuge"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle16.jpg"
weight: "16"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40225
- /detailsb6c2.html
imported:
- "2019"
_4images_image_id: "40225"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40225 -->
Schütte mit Modellen 3-6