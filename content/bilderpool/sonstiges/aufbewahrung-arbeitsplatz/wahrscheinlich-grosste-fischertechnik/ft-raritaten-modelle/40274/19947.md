---
layout: "comment"
hidden: true
title: "19947"
date: "2015-01-08T20:00:08"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
... und sogar noch ohne eine "1" dahinter! Also wohl noch zu der Zeit, als noch niemand wusste, dass das mal ein mot.-1 werden und es einen mot..-2 geben würde!