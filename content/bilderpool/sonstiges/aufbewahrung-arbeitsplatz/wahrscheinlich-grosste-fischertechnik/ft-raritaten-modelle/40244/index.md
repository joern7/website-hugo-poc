---
layout: "image"
title: "FT Vitrine"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle35.jpg"
weight: "35"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40244
- /details82eb.html
imported:
- "2019"
_4images_image_id: "40244"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40244 -->
Modellbaukästen, Grundkästen...