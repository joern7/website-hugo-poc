---
layout: "image"
title: "FT Vitrine"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle41.jpg"
weight: "41"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40250
- /details1a0f.html
imported:
- "2019"
_4images_image_id: "40250"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40250 -->
Grundkästen...