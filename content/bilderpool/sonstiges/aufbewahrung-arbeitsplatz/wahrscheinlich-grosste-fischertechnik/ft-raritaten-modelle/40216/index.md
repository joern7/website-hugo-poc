---
layout: "image"
title: "FT Saturn 5 Rakete"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle07.jpg"
weight: "7"
konstrukteure: 
- "Rupi"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40216
- /details4bce.html
imported:
- "2019"
_4images_image_id: "40216"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40216 -->
Statikmodell (Katalogmodell)