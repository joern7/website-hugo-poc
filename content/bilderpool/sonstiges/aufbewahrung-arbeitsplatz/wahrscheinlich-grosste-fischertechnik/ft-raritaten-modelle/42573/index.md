---
layout: "image"
title: "Pumpstation und Stapler"
date: "2015-12-27T10:20:57"
picture: "20151127_163453.jpg"
weight: "87"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/42573
- /details6367.html
imported:
- "2019"
_4images_image_id: "42573"
_4images_cat_id: "3021"
_4images_user_id: "2496"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42573 -->
