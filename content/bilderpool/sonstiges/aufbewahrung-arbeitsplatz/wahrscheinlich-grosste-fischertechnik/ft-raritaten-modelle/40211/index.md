---
layout: "image"
title: "FT Truck"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle02.jpg"
weight: "2"
konstrukteure: 
- "Rupi"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40211
- /details0d4d.html
imported:
- "2019"
_4images_image_id: "40211"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40211 -->
Modell 1966