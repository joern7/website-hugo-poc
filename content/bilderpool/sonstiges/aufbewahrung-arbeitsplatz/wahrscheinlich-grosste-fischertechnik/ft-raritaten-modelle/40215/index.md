---
layout: "image"
title: "FT Mondlandefähre"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle06.jpg"
weight: "6"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40215
- /details3f83.html
imported:
- "2019"
_4images_image_id: "40215"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40215 -->
Mondlandefähre Clubmodell anlässlich der Mondlandung 1969