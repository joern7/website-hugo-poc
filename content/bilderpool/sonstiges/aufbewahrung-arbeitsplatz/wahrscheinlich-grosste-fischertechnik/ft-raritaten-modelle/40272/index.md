---
layout: "image"
title: "FT 50"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle63.jpg"
weight: "63"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40272
- /details5e16.html
imported:
- "2019"
_4images_image_id: "40272"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40272 -->
Der erste 50er...