---
layout: "image"
title: "Truck"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37321
- /details3470.html
imported:
- "2019"
_4images_image_id: "37321"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37321 -->
Truck mit Fernsteuerung "der riecht noch neu" obwohl bereits 30 Jahre alt