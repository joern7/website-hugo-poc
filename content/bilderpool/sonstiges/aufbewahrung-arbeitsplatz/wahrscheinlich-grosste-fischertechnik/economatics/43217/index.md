---
layout: "image"
title: "Unterseite der Steuerplatine vom Economatics Fischertechnik PIC Buggy"
date: "2016-03-31T17:25:08"
picture: "FTBUG_bottom_s.jpg"
weight: "19"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Steuerplatine", "Economatics", "Fischertechnik", "PIC", "Buggy"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43217
- /detailsc640.html
imported:
- "2019"
_4images_image_id: "43217"
_4images_cat_id: "3152"
_4images_user_id: "579"
_4images_image_date: "2016-03-31T17:25:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43217 -->
Hier sieht man die Unterseite der Steuerplatine vom Economatics Fischertechnik PIC Buggy