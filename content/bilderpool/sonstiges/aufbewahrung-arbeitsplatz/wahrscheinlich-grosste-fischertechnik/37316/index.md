---
layout: "image"
title: "Originalregal aus den 80er Jahren"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37316
- /details6007.html
imported:
- "2019"
_4images_image_id: "37316"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37316 -->
Gefüllt mit Highlights aus den 70ern und 80ern