---
layout: "image"
title: "Lehr- und Demonstrationsprogramm 5000 Grundkasten offen"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung36.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37351
- /details261b.html
imported:
- "2019"
_4images_image_id: "37351"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37351 -->
Lehr- und Demonstrationsprogramm 5000 Grundkasten