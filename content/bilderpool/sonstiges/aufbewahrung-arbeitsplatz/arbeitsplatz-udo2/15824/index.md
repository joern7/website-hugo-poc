---
layout: "image"
title: "[1/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:23"
picture: "arbeitsplatzvonudo1.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15824
- /details8239.html
imported:
- "2019"
_4images_image_id: "15824"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15824 -->
Arbeitstisch:
Mein schon mal aus der Etappe 1 vorgestellter Arbeitstisch 1,20m x 0,60m in der aktuellen Phase. Ja ich weiß, der ist sehr klein aber dafür urgemütlich. Die Beleuchtung ist augenschonend und trotzdem wie sich herausgestellt hat foto- und videotüchtig. Verflixt da fällt mir gerade ein, ich hatte doch bei der Vorstellung meiner BF1 Anfang 2008 versprochen, von ihr noch ein paar ordentliche Fotos und ein Video zu machen .... Hinten rechts grüßen schon wieder etwas gefleddert die Denkanstöße von steffalk. Ja und eigentlich wollte ich das Schwarz-Rot-Gelbe(goldene) Ding da zum Fotografieren wegnehmen. Man wird halt doch alt. Aber so blendet wenigstens etwas aufgelockert die Tischplatte nicht so. Was meint ihr da, was das mit der eckigen Kette soll? Ja daran sind die noch fehlenden Teile schuld. Aber laufen tut das Ding trotzdem schon, der Motor steht ja schließlich auf einem Radialschlitten mit Rückholfederung!