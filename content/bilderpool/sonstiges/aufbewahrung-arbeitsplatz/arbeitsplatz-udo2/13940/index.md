---
layout: "image"
title: "[1/2] ft-Arbeitsplatz, Etappe 1"
date: "2008-03-17T18:09:46"
picture: "arbeitsplatz1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13940
- /details87cb.html
imported:
- "2019"
_4images_image_id: "13940"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-03-17T18:09:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13940 -->
Mein erster ft-Arbeitsplatz nach 15 Monaten Odyssee jetzt auf einem Tisch 1200x600 für 15,- Euro von einer Möbelbörse.