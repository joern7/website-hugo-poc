---
layout: "image"
title: "[4/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:25"
picture: "arbeitsplatzvonudo4.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15827
- /details8b4d.html
imported:
- "2019"
_4images_image_id: "15827"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15827 -->
Teileregal:
Ausschnitt aus dem "Bereich aktuelle Teile"