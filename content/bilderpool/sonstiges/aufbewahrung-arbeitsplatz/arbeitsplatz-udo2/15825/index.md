---
layout: "image"
title: "[2/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:25"
picture: "arbeitsplatzvonudo2.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15825
- /details9c9f.html
imported:
- "2019"
_4images_image_id: "15825"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15825 -->
Teileregal:
Hier befinden sich alle Teile meines verfügbaren ft-Sortimentes, soweit sie mit ihren Abmessungen in die Sortierbehälter passen. Die größeren Teile und Anteile höherer Stückzahlen befinden sich unter dem Tisch in einem Aufnahmevolumen von 12 BOX 1000 sowie 4 Sortimentkästen. Hätte ich bei der Montage des Regals mal gemessen, würden hier 162 statt 158 Sortierbehälter unterkommen. 160 Sortierbehälter sind das Volumen von 20 BOX 1000, hier allerdings in einem kleineren Volumenbedarf untergebracht. Durch die Etiketten mit Rubrik, Art-Nr., Abbildung und Bezeichnung sind alleTeile optisch im Zugriff. Sichtbehälter ohne erforderliche Etiketten wären bequemer, benötigen aber den mir fehlenden Platz. Vor dem unteren Regalboden ist eine Ablage für die Sortierkästen mit einer Beleuchtung der Etikettenfront, wenn in der "blauen Stunde" die kreativen Modellideen kommen. Ich habe mit Hilfe dieser Anordnung schon mehrere größere Modelle auf- und wieder abgebaut. Sie hat sich denke ich mal schon bewährt. Und so ganz nebenbei treibt man wie beabsichtigt durch das Aufstehen seinen gesunderhaltenden Sport ...