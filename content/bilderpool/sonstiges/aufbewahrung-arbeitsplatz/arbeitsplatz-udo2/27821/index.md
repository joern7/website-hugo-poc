---
layout: "image"
title: "[1/3] Etiketten Neuteile 2006/2007"
date: "2010-08-16T10:35:33"
picture: "neuteileetiketten1.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/27821
- /details7041.html
imported:
- "2019"
_4images_image_id: "27821"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2010-08-16T10:35:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27821 -->
Inzwischen ist es für mich zwingend notwendig geworden, meine Ablage zu erweitern. Mein System mit den Sortierkästen und stirnseitigen Etiketten erwies sich inzwischen bis auf die Etiketten als brauchbar. Leider sind die Etiketten von knobloch wenig aktuell und vollständig sowie die Teilelisten von ft ohne die erforderliche Optik bei mehrfarbigen Teilen. Also habe ich zur Selbsthilfe gegriffen und mir Anfang 2010 einen "komplexen" Etikettensatz bestehend aus zunächst 2495 Teilen :o) erstellt ...
Schnelle Datenkommentare meinerseits in den Foren zu Bauteilfragen haben hier u.a. ihre Quellen :o)  
Da in den Sortierkästen bei Kleinteilen bis zu 10 Fächer möglich sind, werden die Etiketten in einer Grösse ausgedruckt, die das stirnseitige Anordnen bis zu 10 Stück ermöglicht. 
Davon hier auf den 3 Fotos [1/3] bis [3/3] Auszüge aus den  A5-Übersichtsblättern zu den 68 Neuteilen aus den Jahren 2006 bis 2009. Diese sind hier noch nach Jahren geordnet, die anderen bereits nach den rechts oben stehenden Rubriken.
Wer hier von mir einen Fehler entdeckt oder Fehlendes sucht kommentiert das bitte hilfreich ...

Diese Anmerkung ist wohl noch notwendig:
Ich kann mir vorstellen, dass die Arbeit mit Etiketten nicht so das breite Interesse findet. Deshalb habe ich Verständnis dafür, dass bei der Fa. knobloch die Aktualisierung ihrer Etiketten nicht die Aufgabe Nr.1 sein kann. Und die Bauteillisten von ft haben ja eine andere Zweckbestimmung, wo dann eben nur eine Teilfarbe in der Abbildung vor der Auflistung erscheint. Dennoch haben sie mir bis in die Gegenwart ihre Dienste geleistet.