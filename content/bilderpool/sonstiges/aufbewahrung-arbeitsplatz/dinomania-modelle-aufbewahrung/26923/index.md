---
layout: "image"
title: "Der Inhalt des Hobby 3 Kastens"
date: "2010-04-11T23:16:17"
picture: "neuebildervonmeinenmodellen01.jpg"
weight: "1"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26923
- /details5640.html
imported:
- "2019"
_4images_image_id: "26923"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26923 -->
Es fehlen auf dem Bild einzelne Teile, wie man sieht.