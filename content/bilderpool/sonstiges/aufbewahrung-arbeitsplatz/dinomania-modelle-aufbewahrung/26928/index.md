---
layout: "image"
title: "Sammlung verschiedener Modelle"
date: "2010-04-11T23:16:18"
picture: "neuebildervonmeinenmodellen06.jpg"
weight: "6"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26928
- /detailsccc9.html
imported:
- "2019"
_4images_image_id: "26928"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26928 -->
Sendemast als Entwurf, die Rutsche und der Fire Truck