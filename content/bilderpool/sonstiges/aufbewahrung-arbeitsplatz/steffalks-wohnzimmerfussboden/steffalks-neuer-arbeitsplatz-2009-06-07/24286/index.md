---
layout: "image"
title: "Gesamtansicht"
date: "2009-06-07T17:22:06"
picture: "steffalk1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24286
- /detailsa1dd.html
imported:
- "2019"
_4images_image_id: "24286"
_4images_cat_id: "1662"
_4images_user_id: "104"
_4images_image_date: "2009-06-07T17:22:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24286 -->
Die zwei weißen "Alex"-Schubladeneinheiten fassen sämtliches ft. Sie haben je 3 flache und drei hohe Schubladen.