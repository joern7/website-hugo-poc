---
layout: "image"
title: "Gesamtansicht"
date: "2006-10-10T19:04:08"
picture: "steffalkswohnzimmerfussboden01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7156
- /detailsea10.html
imported:
- "2019"
_4images_image_id: "7156"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7156 -->
Stand 2006-10-03: 10 gefüllte ft 1000, die 5 hobby-Kästen, Parallel-Interface, RoboInt, Robo-Extension, Netzteile