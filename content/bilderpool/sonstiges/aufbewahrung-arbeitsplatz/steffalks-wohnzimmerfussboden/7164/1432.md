---
layout: "comment"
hidden: true
title: "1432"
date: "2006-10-11T20:28:54"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Also, ich muss das jetzt mal sagen: Stefan, du lagerst deine Teile falsch!

...

Diese Menge an Teilen gehört doch ganz unbestreitbar zu einem Super-Monster-Modell (oder einer Serie davon) verarbeitet.

Wie wär's denn (nur so als Beispiel) mit "Das Audi-Werk in Ingolstadt, von der Blechstanze bis zur Probefahrt, einschließlich aller aktuell erhältlichen Modelle" -- oder so? ;-)

Also im Ernst: ganz schön beeindruckend, diese Sammlung!

Gruß,
Harald