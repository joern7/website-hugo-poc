---
layout: "image"
title: "Schrank 1 Schublade 1 (oben)"
date: "2018-05-07T22:44:05"
picture: "einsortierung07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47603
- /detailscbe1.html
imported:
- "2019"
_4images_image_id: "47603"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47603 -->
Rote Bausteine, Winkelbausteine, Verbinder. Links daneben zwei lange Aluprofile.