---
layout: "image"
title: "Schrank 3 Schublade 1 (oben)"
date: "2018-05-07T22:44:05"
picture: "einsortierung23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47619
- /details656f.html
imported:
- "2019"
_4images_image_id: "47619"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47619 -->
Statikteile wie Riegel, Riegelscheiben, Scharniere, Laschen, Knotenplatten, Aufnahmeplatten, Baggerschaufeln, Kraftmesser, Seiltrommeln, Haken, kleine Seiltrommeln nebst Klemmringen, Greifzangen.