---
layout: "image"
title: "Schrank 3 Schublade 5 verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47625
- /detailsc678.html
imported:
- "2019"
_4images_image_id: "47625"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47625 -->
Weitere Statikträger 120 und Flachträger 120. Im Kasten rechts oben im Bild sind die vier linken Statikträger absichtlich abgetrennt untergebracht - denen fehlt der Zapfen.