---
layout: "image"
title: "Schrank 2 Schublade 6 (unten) sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47617
- /detailsc70b.html
imported:
- "2019"
_4images_image_id: "47617"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47617 -->
Bauplatten 1000 ohne Bodenplatte (nicht staubdicht und tendenziell Zapfenschädigend), der ft:pedia-Wohnzimmer-Dienstreisen-Urlaubs-Kasten incl. eingelegter Staubschutz-Stückliste, Bootskörper.