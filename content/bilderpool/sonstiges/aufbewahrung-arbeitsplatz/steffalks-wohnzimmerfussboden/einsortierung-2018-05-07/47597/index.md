---
layout: "image"
title: "fischertechnik-Gelände"
date: "2018-05-07T22:44:05"
picture: "einsortierung01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47597
- /details1925.html
imported:
- "2019"
_4images_image_id: "47597"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47597 -->
Alles fischertechnik ist in vier Ikea "Alex"-Schränken untergebracht. Die Kabel hängen an von einem Freund aus Holz perfekt gemachten Kabelhaltern. Rechts im Regal sieht man ein paar gedruckte ft:pedia-Ausgaben und niederländische Clubhefte.