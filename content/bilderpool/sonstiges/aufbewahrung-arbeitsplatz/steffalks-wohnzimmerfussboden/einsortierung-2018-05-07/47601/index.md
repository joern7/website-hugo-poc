---
layout: "image"
title: "Schrank 4 - Motoren, Pneumatik, Netzteile, Computing"
date: "2018-05-07T22:44:05"
picture: "einsortierung05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47601
- /details8650.html
imported:
- "2019"
_4images_image_id: "47601"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47601 -->
Im Schrank rechts hinten befinden sich Motoren, Zahnstangen, Pneumatik, fertige Führerhäuser, Netzteile, Akkus, ein Netzschaltgerät, Fernbedienungen, Interfaces, Microcontroller und Werkzeug.