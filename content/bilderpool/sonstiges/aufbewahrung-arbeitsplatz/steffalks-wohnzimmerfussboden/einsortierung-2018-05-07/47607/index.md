---
layout: "image"
title: "Schrank 1 Schublade 4 verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47607
- /detailsed20.html
imported:
- "2019"
_4images_image_id: "47607"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47607 -->
In der unteren Lage liegen weitere Grundbausteine. Bei den drei querliegenden roten Bausteine 15 mit Bohrung fehlt ein Zapfen, aber zum Wegwerfen sind sie mir zu schade.