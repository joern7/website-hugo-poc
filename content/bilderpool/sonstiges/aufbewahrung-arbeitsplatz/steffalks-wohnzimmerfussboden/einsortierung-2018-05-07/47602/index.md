---
layout: "image"
title: "Kabel"
date: "2018-05-07T22:44:05"
picture: "einsortierung06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47602
- /details644b.html
imported:
- "2019"
_4images_image_id: "47602"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47602 -->
Alle Kabel lassen sich ruck-zuck gezielt entnehmen und ratz-fatz wieder an die richtige Stelle einhängen. Die Holzstege sind supergenau parallel, haben abgeschrägte Einlasskanten und bieten noch viel Raum. Von der Stabilität könnten sie auch als Regal durchgehen. (Danke, Andy!)