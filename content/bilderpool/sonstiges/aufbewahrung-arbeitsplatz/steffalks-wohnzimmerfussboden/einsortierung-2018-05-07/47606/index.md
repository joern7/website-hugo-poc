---
layout: "image"
title: "Schrank 1 Schublade 4 sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47606
- /details5b57.html
imported:
- "2019"
_4images_image_id: "47606"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47606 -->
In der ersten doppelt hohen Schublade fanden alle Grundbausteine, Kunststofffedern und die Achsen für die Bausteine 5 mit verbreiterter Bohrung Platz. Links hinten befinden sich die Bausteine mit rotem, rundem Zapfen.