---
layout: "image"
title: "Schrank 2 Schublade 3"
date: "2018-05-07T22:44:05"
picture: "einsortierung18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47614
- /detailsfc6f-2.html
imported:
- "2019"
_4images_image_id: "47614"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47614 -->
1969er Lichtelektronik-Schaltstäbe, Relais, 1970er-Jahre Silberlinge, 1980er IC-Elektronik, Zählwerke, Lautsprecher. Nebendran lange Elektromechanik-Steckachsen.