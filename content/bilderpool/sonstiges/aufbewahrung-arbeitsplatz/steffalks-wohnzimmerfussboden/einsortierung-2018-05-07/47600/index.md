---
layout: "image"
title: "Schrank 3 - Statik, Platten"
date: "2018-05-07T22:44:05"
picture: "einsortierung04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47600
- /detailsb795.html
imported:
- "2019"
_4images_image_id: "47600"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47600 -->
Der Schrank rechts vorne enthält sämtliche Statik, Kranseile, Flexschienen sowie alle Verkleidungs- und kleinere Bauplatten.