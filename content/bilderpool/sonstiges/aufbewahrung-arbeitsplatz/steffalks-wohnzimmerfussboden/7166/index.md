---
layout: "image"
title: "hobby 3 und 4"
date: "2006-10-10T19:04:26"
picture: "steffalkswohnzimmerfussboden11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7166
- /details93a7.html
imported:
- "2019"
_4images_image_id: "7166"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7166 -->
