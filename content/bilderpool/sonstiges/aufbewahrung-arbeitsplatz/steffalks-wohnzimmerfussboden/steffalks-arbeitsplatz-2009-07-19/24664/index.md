---
layout: "image"
title: "21 - Interfaces"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24664
- /details9c49.html
imported:
- "2019"
_4images_image_id: "24664"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24664 -->
Das Parallel-Interface sowie alles zum Robo-Interface.