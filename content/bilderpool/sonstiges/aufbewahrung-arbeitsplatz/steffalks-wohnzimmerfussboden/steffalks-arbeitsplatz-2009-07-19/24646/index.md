---
layout: "image"
title: "04 - Schublade links 2"
date: "2009-07-19T20:07:11"
picture: "steffalksarbeitsplatz04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24646
- /details5352.html
imported:
- "2019"
_4images_image_id: "24646"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24646 -->
Gelochte Grundbausteine (das separate Fach sowie die zwei schwarzen im größeren Fach sind solche mit quadratischer anstatt runder Aussparung), Verbinder, Klemmringe, rote Bausteine, Winkelsteine. Links nebendran kürzere Alustäbe.