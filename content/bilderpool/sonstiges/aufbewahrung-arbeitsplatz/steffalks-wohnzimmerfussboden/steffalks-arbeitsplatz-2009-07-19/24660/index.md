---
layout: "image"
title: "18 - Schublade rechts 5 sichtbare Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24660
- /details9968.html
imported:
- "2019"
_4images_image_id: "24660"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24660 -->
In der sichtbaren Lage liegt alles zur Elektromechanik, Fotowiderstände/-transistoren, einschließlich der älteren Schalter, Taster, Stufenschalter, Lampen, Schleifringe, Schwingfedern, Bimetallstreifen, Zielwände für optische Bänke, und le-Elektronikstäbe. Obenauf liegen die langen geraden Lichtleitstäbe aus hobby-4 bzw. ut-4. Leider waren auf den vier gebrauchten ut-Kastensätzen viele Teile mit Nummern beschriftet.