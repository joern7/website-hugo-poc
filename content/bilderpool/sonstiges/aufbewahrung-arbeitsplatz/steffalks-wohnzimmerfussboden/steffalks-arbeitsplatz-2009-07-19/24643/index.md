---
layout: "image"
title: "01 - Überblick"
date: "2009-07-19T20:07:10"
picture: "steffalksarbeitsplatz01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24643
- /details2688.html
imported:
- "2019"
_4images_image_id: "24643"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24643 -->
Nach über einer Woche Einsortieren ist jetzt alles in zwei Ikea "Alex" verstaut. Ziele waren:

- Kein Teil an mehr als einer Stelle. Bislang gab es viele Teile normal einsortiert, als Teil der hobby-Kästen und als Teil von vier Sätzen von ut-Kästen. Das ist zum Finden und Zurücksortieren umständlich. Nach so langer Zeit habe ich mich also entschlossen, die hobby- und die dieses Jahr dazugekommenen ut-Kästen aufzulösen. Die Originalwannen habe ich aber natürlich noch.

- Wenn irgend möglich, keine zwei verschiedenen Teile, Varianten oder Farben im selben Sortierfach. Z. B. gibt es Streben grau massiv, grau leicht, grau gelocht, gelb gelocht, schwarz gelocht. Die wollte ich auseinanderhalten. Ausnahmen gibt es aber bei Platzmangel oder enormer Verschwendung. Z. B. verlangen X-Streben 169,9 auf jeden Fall einen eigenen ft-1000-Sortiereinsatz. Wegen 4 gelben habe ich keinen neuen angefangen. Ein anderer Grund für eine Ausnahme ist, dass die Teile recht selten verwendet werden, oder dass bei separater Sortierung nicht mehr alle verwandten Teile in einer Schublade landen könnten.

- Möglichst logische Einsortierung in dem Sinne, dass Teile, die etwas miteinander zu tun haben, auch möglichst beieinander liegen sollen.

Links auf dem Tisch steht ein Pari Inhalierboy, der einen prima Kompressor für ft-Pneumatik abgibt, rechts große Bauplatten, Interfaces und schaltbare Vielfachsteckdosen.