---
layout: "comment"
hidden: true
title: "6824"
date: "2008-07-31T18:47:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Große Kinder und zwei Katzen. Letztere klauen all zu gerne Zahnstangen, Reifen, die alten Raupengummis und vor allem Z30, die sie dann entweder unter die Couch pfeffern (vorzugsweise nachts, damit's auch ordentlich gehört wird) oder in meine Schuhe legen (ja, echt!). Aber der ständig aufgebaute ft-Arbeitsplatz rückt endlich in greifbare Nähe. :-)

Gruß,
Stefan