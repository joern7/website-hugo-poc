---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 2"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14820
- /details465c-2.html
imported:
- "2019"
_4images_image_id: "14820"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14820 -->
Standardteile, der gute alte Kraftmesser, die guten alten kleinen Seilrollen, die schönen alten Kurbeln, Raupenbänder, Ketten.