---
layout: "comment"
hidden: true
title: "19119"
date: "2014-06-05T12:36:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein bisschen muss ich Dich enttäuschen, Adlerauge! Der steht absichtlich so, weil das der (einzige) ist, bei dem ein Zapfen fehlt. Der hatte da seinen festen Platz. ;-)
Gruß,
Stefan