---
layout: "image"
title: "Ecke"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14831
- /details815a.html
imported:
- "2019"
_4images_image_id: "14831"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14831 -->
Alte und neue Netzgeräte, Akku-Ladegerät, Alus, ein Parallel-Interface und das Robo-Interface nebst Kollegen. In dem roten Blechkasten rechts befinden sich im Wesentlichen Kabel und Papierrollen von Rechenmaschinen, teilweise vorgemalt mit einer Strecke für das Modell "Autotrainer" aus dem Clubheft 1972-4, an dem meine Kinder immer unheimlich viel Freude hatten. Das gelbe Kästchen mit dem USB-V24-Konverter fand auch hier sein Ehrenplätzchen. Das hatte mir Thomas Kaiser (thkais) netterweise sehr professionell gebaut und geschenkt (Danke Thomas!). Das Teil funktionierte übrigens auf Anhieb mit Windows Vista x64, welches das Chip erkannte und die benötigten Treiber automatisch von Windows Update laden konnte. Ohne den Wandler könnte ich von meinem aktuellen Notebook, der leider keine V24-Schnittstelle mehr hat, nicht aufs Robo-Interface zugreifen, für das leider (noch?) keine x64-Treiber zur Verfügung stehen.