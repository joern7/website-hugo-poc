---
layout: "image"
title: "Arbeitsplatz 1 Schubladen"
date: "2007-02-19T21:26:18"
picture: "Bild6.jpg"
weight: "4"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9069
- /detailsa63a.html
imported:
- "2019"
_4images_image_id: "9069"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9069 -->
Schubladen für besondere Teile + Hefte