---
layout: "image"
title: "Ft-Truck etwas umgebaut mit Fischertechnik Funk"
date: "2007-11-11T08:33:13"
picture: "Truck_mit_FT-Fernsteuerung.jpg"
weight: "26"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12641
- /details0baa.html
imported:
- "2019"
_4images_image_id: "12641"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12641 -->
