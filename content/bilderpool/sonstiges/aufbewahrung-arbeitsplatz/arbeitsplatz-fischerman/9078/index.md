---
layout: "image"
title: "Arbeitsplatz 1 rechts unten"
date: "2007-02-19T21:26:33"
picture: "Bild12.jpg"
weight: "13"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9078
- /details6cce.html
imported:
- "2019"
_4images_image_id: "9078"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9078 -->
und natürlich jede Menge Motoren!