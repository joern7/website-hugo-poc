---
layout: "image"
title: "Nahaufnahme IF1+IF2"
date: "2007-11-11T08:33:13"
picture: "IF1IF2_Nahaufnahme.jpg"
weight: "28"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12643
- /details3d58.html
imported:
- "2019"
_4images_image_id: "12643"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12643 -->
