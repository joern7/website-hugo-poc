---
layout: "image"
title: "änderungen auch an meinem Messplatz"
date: "2007-11-11T08:33:14"
picture: "Arbeitsplatz_Oszi.jpg"
weight: "43"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12658
- /details989e.html
imported:
- "2019"
_4images_image_id: "12658"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12658 -->
So hier ist nun nicht mehr so viel Platz. 
Es haben sich noch zwei Labornetzteile und ein Laptop dazugesellt. Den benutze ich um vom Hameg 205-3 per Interface daten auf den Lap aufzuzeichnen und zu vergleichen. Dank E-Bay habe ich ein Interface ersteigern können das an das Oszi angebaut wird.