---
layout: "image"
title: "Arbeitsplatz 1 rechts"
date: "2007-02-19T21:26:19"
picture: "Bild25.jpg"
weight: "9"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9074
- /details54ee.html
imported:
- "2019"
_4images_image_id: "9074"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9074 -->
Natürlich auch die Sender! :-)