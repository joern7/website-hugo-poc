---
layout: "image"
title: "Arbeitsplatz 1 rechts"
date: "2007-02-19T21:26:19"
picture: "Bild9.jpg"
weight: "10"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9075
- /details0bf8.html
imported:
- "2019"
_4images_image_id: "9075"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9075 -->
oder IR-Sender + Empfänger 1 + 2 und eigenbau Teile