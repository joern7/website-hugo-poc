---
layout: "image"
title: "RoboConnectBox"
date: "2007-11-11T08:33:14"
picture: "RoboConnectBox.jpg"
weight: "45"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12660
- /details370a.html
imported:
- "2019"
_4images_image_id: "12660"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12660 -->
Na die darf ja nicht fehlen wenn man noch alte Intelligente und parallelport Interface hat :-)