---
layout: "image"
title: "neues Regal"
date: "2007-11-11T08:33:14"
picture: "Hubschrauber.jpg"
weight: "42"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12657
- /details2113.html
imported:
- "2019"
_4images_image_id: "12657"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12657 -->
nicht zu vergessen mein kleiner Hubschrauber Landeplatz