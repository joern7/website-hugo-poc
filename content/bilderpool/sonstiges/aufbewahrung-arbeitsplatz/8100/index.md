---
layout: "image"
title: "mein FT-Regal :)"
date: "2006-12-22T23:32:49"
picture: "img_6902_resize.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["Regal"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/8100
- /detailsba66.html
imported:
- "2019"
_4images_image_id: "8100"
_4images_cat_id: "333"
_4images_user_id: "120"
_4images_image_date: "2006-12-22T23:32:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8100 -->
Oben drauf ist ein DVD-Regal quer für die Bauanleitungen. Die Holzplatten sind "einfache" 28mm Küchenarbeitsplatten aus dem Baumarkt auf 2000x540mm zusägen lassen, Seitenteile 1300x540mm. Damit die Boxen hinten schräg stehen, sind die abgesägten 2000x60mm Streifen darunter. Eine Neonleuchte pro Fach sorgen für Licht (fast) auch im hintersten Winkel :) Rechts der RoboMax immer im Blick (damit ich endlich mal weiterbaue...) Aber leider schon wieder zu klein! Die Boxen davor und die Master-Koffer rechts hinten bekommen wohl demnächst einen Regal-Anbau...