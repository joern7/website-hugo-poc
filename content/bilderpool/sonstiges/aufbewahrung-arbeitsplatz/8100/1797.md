---
layout: "comment"
hidden: true
title: "1797"
date: "2006-12-26T01:05:12"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
@Stefan
Das ist leider ein Problem :( Es ist ein Rollo oder Vorhang geplant aber es blieb beim wollen...

@Lothar
Davor auf dem Fußboden oder auf dem Schreibtisch. Wenn ich davor sitze bzw. kniee kann ich bis in's oberste Fach greifen, extra so designed :)

@Guilligan
Meine Liste zeigt 25000 Teile, habe ich seit einem Jahr aber nicht mehr aktualisiert - ich tippe auf etwa das Doppelte jetzt (ink. jedem Kettenglied :)

@Jettaheizer
Habe ich hier im Arbeitszimmer selbst auf dem Boden zusammengeschraubt, schon das aufrichten war allein kaum zu machen! Mir graut vor dem Umzug in unser Haus 03/2007 schon gewaltig :(

Gruß,
Thomas