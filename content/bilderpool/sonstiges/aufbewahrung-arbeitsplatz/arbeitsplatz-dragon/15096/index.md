---
layout: "image"
title: "Schrank ZU"
date: "2008-08-24T16:00:37"
picture: "xfaktor18.jpg"
weight: "1"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15096
- /details4640.html
imported:
- "2019"
_4images_image_id: "15096"
_4images_cat_id: "1376"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15096 -->
Ein nützlicher Schrank