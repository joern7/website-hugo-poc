---
layout: "image"
title: "Sichtlagerkästen"
date: "2008-08-24T16:00:37"
picture: "dragonsarbeitsplatz05.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15101
- /details469f.html
imported:
- "2019"
_4images_image_id: "15101"
_4images_cat_id: "1376"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15101 -->
Sichtlagerkästen von Conrad Electronic