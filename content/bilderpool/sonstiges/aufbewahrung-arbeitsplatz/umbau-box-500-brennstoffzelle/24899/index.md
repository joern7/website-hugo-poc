---
layout: "image"
title: "Umbau 09"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle9.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24899
- /details0193.html
imported:
- "2019"
_4images_image_id: "24899"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24899 -->
Hier sieht man noch einmal deutlich, wie schön sich die Boxen stapeln lassen, obwohl sich in der untersten Box die "unförmige" Brennstoffzelle befindet.