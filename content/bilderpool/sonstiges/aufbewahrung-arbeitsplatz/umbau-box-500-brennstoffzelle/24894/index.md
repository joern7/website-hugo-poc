---
layout: "image"
title: "Umbau 04"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle4.jpg"
weight: "4"
konstrukteure: 
- "kghckzufi"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24894
- /detailsd99c.html
imported:
- "2019"
_4images_image_id: "24894"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24894 -->
Der Boden des mittleren, rechten Faches wird mit einem Stechbeitel entfernt (Holzbrett unterlegen).