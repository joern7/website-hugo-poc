---
layout: "image"
title: "Umbau 01"
date: "2009-09-08T21:04:51"
picture: "umbauboxfuerdiebrennstoffzelle1.jpg"
weight: "1"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24891
- /details7623.html
imported:
- "2019"
_4images_image_id: "24891"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24891 -->
Die neue fischertechnik Brennstoffzelle ft# 136734 aus dem "Profi Hydro Cell Kit", ft# 505285, ist in ihren Abmessungen überhaupt nicht fischertechnik konform. Deshalb passt sie auch nicht so ohne weiteres in das fischertechnik Aufbewahrungssystem, ft# 94828.