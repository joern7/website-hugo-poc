---
layout: "image"
title: "Umbau 08"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24898
- /detailseaab.html
imported:
- "2019"
_4images_image_id: "24898"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24898 -->
Das leider etwas verschwommene Bild zeigt, daß die in die Box 3 hineinragende Brennstoffzelle die gesamte Stapelhöhe nicht überschreitet.

Gleichzeitig dokumentiert die Umbaumaßnahme auch die Zusammengehörigkeit von "Profi Oeco Tech" und "Profi Hydro Cell Kit".