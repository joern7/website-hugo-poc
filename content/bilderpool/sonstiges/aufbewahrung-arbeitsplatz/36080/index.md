---
layout: "image"
title: "Streben&Wasser"
date: "2012-11-03T21:06:31"
picture: "StrebenWasser01_01.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36080
- /details3f94.html
imported:
- "2019"
_4images_image_id: "36080"
_4images_cat_id: "333"
_4images_user_id: "4"
_4images_image_date: "2012-11-03T21:06:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36080 -->
Nachdem ich wieder mal frische gelbe Brösel produziert habe (Strebe I-120 gelb, am "Jumping"-Karusell, das seit letztem Jahr im Bastelzimmer steht), bin ich der Sache nochmal nachgegangen.

Erstens: Das harte Wasser hier produziert weiße Kalkränder an den gelben Streben. Also wird demnächst mit aqua dest. gearbeitet.

Zweitens: ALLE Streben nehmen Wasser auf und geben es wieder ab. Die gelben werden spröde, wenn sie austrocknen. Bei den schwarzen weiß ich's nicht, und bei den grauen passiert es nicht. Zum Beweise dient das Foto: die Streben sind mindestens zwei Tage gebadet worden, sind dabei aufgequollen und jetzt klemmen sie so eng in den Adaptern, dass man sie überkopf hinhängen kann.

Ausblick: Mein ft-Streben-Humidor wird kommen. Die Frage ist nicht "ob", sondern "wann".