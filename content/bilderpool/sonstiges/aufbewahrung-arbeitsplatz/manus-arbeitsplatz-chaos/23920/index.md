---
layout: "image"
title: "einige Sammelkästen im Schrank"
date: "2009-05-08T23:33:18"
picture: "manusarbeitsplatz3.jpg"
weight: "3"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23920
- /details26b3.html
imported:
- "2019"
_4images_image_id: "23920"
_4images_cat_id: "1640"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23920 -->
so sieht mein Schrank aus