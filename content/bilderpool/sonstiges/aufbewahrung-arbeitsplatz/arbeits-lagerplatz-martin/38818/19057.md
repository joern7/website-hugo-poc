---
layout: "comment"
hidden: true
title: "19057"
date: "2014-05-18T21:49:22"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich hatte vor Jahrzehnten mal das ft-Bastelprojekt "alles muss weg" aufgesetzt: soviele Modelle bauen, bis alle Sortierboxen leer sind. Das ist seinerzeit gescheitert, weil die "Allerweltsteile" wie BS30 und Winkelsteine verbaut waren und mit den verbleibenden Teilen nichts mehr zusammen ging. Bei dieser Sammlung käme das Ende so eines Projekts wahrscheinlich durch technisches K.O. mit Sehnenscheidenentzündung in zehn Fingern.

Gruß,
Harald