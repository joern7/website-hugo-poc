---
layout: "image"
title: "Arbeitsplatz"
date: "2008-08-27T14:58:20"
picture: "Arbeitsplatz.jpg"
weight: "1"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["-Michael-", "Michael", "Arbeisplatz"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15111
- /detailsa77d.html
imported:
- "2019"
_4images_image_id: "15111"
_4images_cat_id: "1377"
_4images_user_id: "820"
_4images_image_date: "2008-08-27T14:58:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15111 -->
Gesamtansicht