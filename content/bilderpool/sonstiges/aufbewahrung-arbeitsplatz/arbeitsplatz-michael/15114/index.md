---
layout: "image"
title: "Arbeitsplatz (3)"
date: "2008-08-27T21:17:53"
picture: "Arbeitsplatz_3.jpg"
weight: "4"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["-Michael-", "Michael", "Arbeitsplatz"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15114
- /detailsea5e.html
imported:
- "2019"
_4images_image_id: "15114"
_4images_cat_id: "1377"
_4images_user_id: "820"
_4images_image_date: "2008-08-27T21:17:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15114 -->
Kiste1