---
layout: "image"
title: "RoboPro Standort"
date: "2007-02-11T15:18:39"
picture: "magier5.jpg"
weight: "10"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8943
- /details6143.html
imported:
- "2019"
_4images_image_id: "8943"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8943 -->
Hier arbeite ich an meinen Programmen.