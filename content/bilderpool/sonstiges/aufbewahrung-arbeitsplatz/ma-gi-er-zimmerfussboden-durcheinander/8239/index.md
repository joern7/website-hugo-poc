---
layout: "image"
title: "'Service'"
date: "2006-12-31T18:19:29"
picture: "magierzimmerfussbodendurcheinander4.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8239
- /details0ae9.html
imported:
- "2019"
_4images_image_id: "8239"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2006-12-31T18:19:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8239 -->
Zwei Schraubendreher, zwei alte Fünfräppler, eine alte Japanmesserklinge und zwei überflüssige Kardanwürfel (Rot und Dunkelrot)