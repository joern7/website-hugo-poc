---
layout: "image"
title: "Räder"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14250
- /detailsca95.html
imported:
- "2019"
_4images_image_id: "14250"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14250 -->
Räder, das meiste aus den 60'ern