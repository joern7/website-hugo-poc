---
layout: "image"
title: "Achsen+Ketten"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14252
- /detailsa48c.html
imported:
- "2019"
_4images_image_id: "14252"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14252 -->
Achsen, Ketten, Lagerböcke, das meiste noch aus den 60'ern