---
layout: "image"
title: "Studio von PK-14"
date: "2005-03-04T14:32:20"
picture: "PICT0017.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3694
- /detailsc292-2.html
imported:
- "2019"
_4images_image_id: "3694"
_4images_cat_id: "442"
_4images_user_id: "144"
_4images_image_date: "2005-03-04T14:32:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3694 -->
Inside PK's IKEA IFAR "baukasten" wandmöbel.
Baukasten sind aber nicht drin. Nur die grosseren teilen die nicht in die Racco's passen:
die grossen bauplatten, trafo's, Alu's, Räder, und Statik.

Gruß Peter