---
layout: "image"
title: "Studio von PK-6"
date: "2005-03-04T14:32:00"
picture: "PICT0008.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3686
- /details3f52.html
imported:
- "2019"
_4images_image_id: "3686"
_4images_cat_id: "442"
_4images_user_id: "144"
_4images_image_date: "2005-03-04T14:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3686 -->
Der TEREX Miningtruck auf das IKEA IFAR "baukasten" wandmöbel.