---
layout: "comment"
hidden: true
title: "16777"
date: "2012-04-24T09:57:50"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Inzwischen habe ich eine Lösung gefunden:
Mit PowerStrips von Tesa klebe ich eine Bauplatte 90x30 auf den Schrank, an die Zapfen kann dann die Bauplatte geschoben werden. Funktioniert, hält und kann sogar restlos wieder entfernt werden.
Grüße,
Martin