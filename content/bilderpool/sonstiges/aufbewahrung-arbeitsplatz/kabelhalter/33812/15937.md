---
layout: "comment"
hidden: true
title: "15937"
date: "2011-12-28T14:24:12"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Gut gemacht! Sauber!

Aber schriebst Du nicht erst im August, dass Du für einen Kabelhalter "keine ft-Teile binden wolltest"? ;o)

Siehe hier:

http://www.ftcommunity.de/details.php?image_id=31635

Jaja, irgendwann kommt immer die Einsicht, dass es MIT FT doch am besten und schönsten ist!

Wobei Deine Variante voraussetzt, dass an ALLEN Kabeln Stecker sind. Da die FT-Stecker doch sehr teuer sind, hat sicher nicht jeder FT-Bauer auch überall Stecker dran. Und dann funktioniert Deine Kabelaufbewahrung leider nicht mehr ...

Gruß, Thomas