---
layout: "image"
title: "Kabel_9832.JPG"
date: "2013-10-08T20:13:53"
picture: "Kabel_9832.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37694
- /detailse485.html
imported:
- "2019"
_4images_image_id: "37694"
_4images_cat_id: "2358"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T20:13:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37694 -->
Rezept: 
1. Man nehme einen Kabelschacht 30x30 und entferne den Deckel. Es verbleibt ein U-Profil.
2. Man nehme eine Stichsäge und schneide das U-Profil der Länge nach mitten durch.
3. Man nehme die Stichsäge nochmal zur Hand und schneide mit einem möglichst breiten Blatt viele viele Einschnitte in eine ehemalige Seitenwand des U-Profils.
4. Die Teile schraube man mit der schmalen Seite aufrecht irgendwo an, und hänge dann die ft-Kabel ein.