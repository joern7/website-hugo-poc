---
layout: "image"
title: "Kabelsortierung"
date: "2011-09-26T00:22:25"
picture: "kabelhaltermasked1.jpg"
weight: "5"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/32526
- /details344a.html
imported:
- "2019"
_4images_image_id: "32526"
_4images_cat_id: "2358"
_4images_user_id: "373"
_4images_image_date: "2011-09-26T00:22:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32526 -->
Nochmal eine andere Art der Kabelsortierung. Verwende ich für alle Kabel länger als 20cm. So muss ich wenigstens nicht mehr sooo ewig suchen im Chaos-Kasten