---
layout: "image"
title: "Kabelaufhängung"
date: "2011-12-31T13:13:32"
picture: "endlich1.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33824
- /detailsc676.html
imported:
- "2019"
_4images_image_id: "33824"
_4images_cat_id: "2358"
_4images_user_id: "1162"
_4images_image_date: "2011-12-31T13:13:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33824 -->
Eine schnelle und effektive Kabelaufhängung: 
Einfach ein Stück stärkere Pappe genommen und Schlitze mit der Schere hineingeschnitten und dann die Kabel einfach eingehängt. 
Ist vielleicht nicht die schönste Variante aber sie erfüllt ihren Zweck ;)