---
layout: "image"
title: "Kabelsalat2.jpg"
date: "2004-07-13T13:26:02"
picture: "Kabelsalat2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2546
- /detailsee0f.html
imported:
- "2019"
_4images_image_id: "2546"
_4images_cat_id: "2358"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2546 -->
Die Nägel sollen nur verhindern, dass die Holzwürfelchen (zwischen den Schnittfugen) abbrechen. Einfacher ist es, wenn man eine Hartfaserplatte oder etwas kreuzverleimtes hernimmt.