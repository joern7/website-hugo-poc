---
layout: "image"
title: "Sortierschrank"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish03.jpg"
weight: "3"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- /php/details/39682
- /details1563.html
imported:
- "2019"
_4images_image_id: "39682"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39682 -->
Schubladenschrank mit Vollauszügen.