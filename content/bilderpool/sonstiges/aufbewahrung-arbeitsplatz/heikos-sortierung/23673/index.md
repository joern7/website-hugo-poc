---
layout: "image"
title: "Schublade 3 unten"
date: "2009-04-13T00:20:20"
picture: "Heikos_Sortierung-008.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23673
- /details4099.html
imported:
- "2019"
_4images_image_id: "23673"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23673 -->
Links: Komische Farben.

Rechts: Kraftübertragung.