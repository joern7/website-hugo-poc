---
layout: "image"
title: "Zwei Schubladenkästen"
date: "2009-04-13T00:11:32"
picture: "Heikos_Sortierung-001.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["ikea", "helmer"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23668
- /details4d50.html
imported:
- "2019"
_4images_image_id: "23668"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:11:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23668 -->
Ikea Helmer, ohne dass ich Provision dafür bekomme.

Viel Stauraum für wenig Geld. Remadus hatte die Kästen als erster, das musste ich unbedingt nachmachen.