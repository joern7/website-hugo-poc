---
layout: "image"
title: "Gesamtansicht"
date: "2009-04-13T00:39:25"
picture: "Heikos_Sortierung-014.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23681
- /detailsf070.html
imported:
- "2019"
_4images_image_id: "23681"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23681 -->
Gar nicht mal hässlich. Als Arbeitsplatte dient übrigens mein Schreibtisch, der im Wohn-/Arbeitszimmer steht. 

Deshalb muss das fischertechnik dauerhaft verschwinden können, dank der Rollen werden die beiden Schränke dann auf den Flur verfrachtet.