---
layout: "image"
title: "Schublade 2 oben"
date: "2009-04-13T00:16:57"
picture: "Heikos_Sortierung-005.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23670
- /details2479.html
imported:
- "2019"
_4images_image_id: "23670"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:16:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23670 -->
Links: Antrieb, also Motor, Getriebe und Pneumatik.

Rechts: Die wichtigen roten Kleinteile. Alle auf einen Griff parat, die wichtigsten in den beiden vorderen Kästen oben. Die Schubladen werden nach unten hin immer unwichtiger; hier sind die wichtigsten Teile versammelt.

Um das Volumen gut auszunutzen, habe ich manche Fächer doppelt belegt. Das klappt immer dann, wenn die Teile völlig unterschiedlich sind (dann vergreift man sich nicht), oder wenn man sie jeweils nicht oft braucht und nur wenige davon hat.