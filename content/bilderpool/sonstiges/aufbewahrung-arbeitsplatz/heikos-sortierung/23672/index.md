---
layout: "image"
title: "Schublade 3 oben"
date: "2009-04-13T00:19:30"
picture: "Heikos_Sortierung-007.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23672
- /details5c35.html
imported:
- "2019"
_4images_image_id: "23672"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:19:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23672 -->
Links: Bauplatten. Hinten: Sperrgut. 

Rechts: Achsen, Naben, Kraftübertragung.