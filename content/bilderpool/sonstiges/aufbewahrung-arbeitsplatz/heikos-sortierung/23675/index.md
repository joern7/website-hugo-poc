---
layout: "image"
title: "Schublade 4 unten"
date: "2009-04-13T00:23:49"
picture: "Heikos_Sortierung-010.jpg"
weight: "8"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23675
- /details5b53.html
imported:
- "2019"
_4images_image_id: "23675"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:23:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23675 -->
Rechts: Ketten, Statik-Zubehör.