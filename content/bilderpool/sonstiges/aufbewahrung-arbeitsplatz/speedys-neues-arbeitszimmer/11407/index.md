---
layout: "image"
title: "Ft-Zimmer  1  (leer)"
date: "2007-08-27T17:21:13"
picture: "DSC02984.jpg"
weight: "1"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Aufbewahrung", "Arbeitsplatz", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/11407
- /details6d41.html
imported:
- "2019"
_4images_image_id: "11407"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-27T17:21:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11407 -->
mein neues ft-Zimmer im leeren Zustand