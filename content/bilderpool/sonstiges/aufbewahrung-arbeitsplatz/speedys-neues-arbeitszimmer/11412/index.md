---
layout: "image"
title: "Ft-Zimmer"
date: "2007-08-29T19:19:07"
picture: "DSC03079.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: ["Aufbewahrung", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/11412
- /details1ebd.html
imported:
- "2019"
_4images_image_id: "11412"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-29T19:19:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11412 -->
