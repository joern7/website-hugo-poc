---
layout: "image"
title: "Vitrine offen"
date: "2010-10-28T15:07:32"
picture: "endlich05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29062
- /detailsc700.html
imported:
- "2019"
_4images_image_id: "29062"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29062 -->
Fischertechnik 100S, "Elektroköfferle", Box 1000, 15meter Spiralschlauch