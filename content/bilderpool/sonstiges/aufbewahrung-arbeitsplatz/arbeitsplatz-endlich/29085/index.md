---
layout: "image"
title: "Box 1000 Obere Schicht"
date: "2010-10-28T15:07:33"
picture: "endlich28.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29085
- /detailsc119.html
imported:
- "2019"
_4images_image_id: "29085"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:33"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29085 -->
Taster, schalter, Sensoren, E-Tec Module , Fototransistatoren

Mini Motoren , Summer, Mini Motoren Zubehör

Lampen, Batterie- Halter, Schraubendreher, Stecker, Batterieclips, 

Power Motoren

Ich habe auch noch mehrere Motoren und Sensoren, aber einige sind schon verbaut.