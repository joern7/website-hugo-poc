---
layout: "image"
title: "Vitrine offen"
date: "2010-10-28T15:07:31"
picture: "endlich03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29060
- /details74f5.html
imported:
- "2019"
_4images_image_id: "29060"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29060 -->
So, hier sind: 
(Von links nach rechts)
Bauplatten mit akku, restliche Statikteile 120, Box 500 mit  Werkzeugkoffer, Fischertechnik 100S mit "Elektroköfferle", Box 1000 mit 15 Meter Spiralschlauch

Unten liegen noch Schaumstoffe zum Polstern von TX, IF und Ext.