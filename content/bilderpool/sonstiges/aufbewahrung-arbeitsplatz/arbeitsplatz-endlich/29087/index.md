---
layout: "image"
title: "Koffer"
date: "2010-10-28T15:07:33"
picture: "endlich30.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29087
- /details15a6.html
imported:
- "2019"
_4images_image_id: "29087"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:33"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29087 -->
Das ist der orangene Koffer von BTI.

Den Koffer gibt es auch in verschiedenen Größen.