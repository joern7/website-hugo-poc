---
layout: "overview"
title: "Minimalsammlung 2016"
date: 2020-02-22T09:21:00+01:00
legacy_id:
- /php/categories/3269
- /categories2b3c.html
- /categories0437.html
- /categoriesa832.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3269 --> 
Ab September ziehe ich für meine Masterarbeit um. Da ich mein gesamtes ft definitiv nicht mitnehmen kann, habe ich versucht, eine Minimalsammlung zusammenzustellen.

Es sollte von möglichst allen aktuellen Teilen eine ausreichende Anzahl für "normale" Modelle vorhanden sein. Eigentlich wollte ich mich auf eine Box 1000 beschränken, das stellte sich aber bald als nicht sinnvoll möglich heraus. Deshalb sind es jetzt 2 Boxen 1000 geworden.

Was habe ich vergessen, was haltet ihr für völlig überflüssig, was ist zu viel?