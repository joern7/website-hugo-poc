---
layout: "image"
title: "Box 1.2 oben rechts"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44314
- /detailse0dc.html
imported:
- "2019"
_4images_image_id: "44314"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44314 -->
Haken, Kupplungsstücke mit und ohne Haken, div. Gelenke

U-Träger-Adapter, Achsführungen, Kurbeln schwarz

Baustein 15 schwarz mit Ansenkung, Baustein 15 mit Loch rot (modifiziert), Bauplatte 3,75 (flach mit der einen Nut)

Schnecken mit Schneckenmuttern

Rollenlager, Rollenböcke, Seilrollen 21, 12 und Rast-12

Bauplatten in vielen Größen