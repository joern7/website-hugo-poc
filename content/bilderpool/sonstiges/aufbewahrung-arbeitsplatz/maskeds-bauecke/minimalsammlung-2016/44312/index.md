---
layout: "image"
title: "Box 1.2 unten links"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44312
- /details5ec1.html
imported:
- "2019"
_4images_image_id: "44312"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44312 -->
5 Flachträger, 16 Winkelträger 30, darunter:
16 Winkelträger 60, 24 Winkelträger 15, 12 Winkelträger 15 mit 2 Zapfen

Riegelscheiben, S-Riegel 6, S-Riegel-Werkzeug

5 Knotenplatte Doppel, 4 Kreuzknotenplatte, 5 L-Laschen, div. Winkellaschen

8 Riegelsteine schwarz, 8 Statiksteine