---
layout: "image"
title: "untere Lage Box 1"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44311
- /detailsb859.html
imported:
- "2019"
_4images_image_id: "44311"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44311 -->
jetzt bezeichnet als Box 1.2