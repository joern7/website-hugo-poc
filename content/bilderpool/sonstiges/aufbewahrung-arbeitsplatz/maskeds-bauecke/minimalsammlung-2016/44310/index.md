---
layout: "image"
title: "Box 1.1 unten links"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44310
- /detailsd39e.html
imported:
- "2019"
_4images_image_id: "44310"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44310 -->
Zahnstangen, 2 U-Getriebe

3 weitere U-Getriebe, 2 Encoder-Motoren, 3 Power-Motoren mit Adaptern, 2 Luftschrauben komplett, 2 S-Motoren, Hubgetriebe, 3 XS-Motoren (davon 1 Bausteingröße)

2 Anschlusskabel für Encoder-Motoren, 4 Taster, 1 Polwendeschalter