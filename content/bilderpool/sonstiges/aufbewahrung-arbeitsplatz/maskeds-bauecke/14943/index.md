---
layout: "image"
title: "rechter Tisch"
date: "2008-07-22T20:51:14"
picture: "masked2.jpg"
weight: "2"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/14943
- /details7854.html
imported:
- "2019"
_4images_image_id: "14943"
_4images_cat_id: "1364"
_4images_user_id: "373"
_4images_image_date: "2008-07-22T20:51:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14943 -->
Oben links alles was mit Strom zu tun hat (Motoren gerade alle verbaut - eigentlich 6 Minimotoren und 4 PMs), rechts daneben alle Statikträger in allen möglichen Formen und Farben, untendrunter alles Mögliche an Teilen