---
layout: "image"
title: "linke Seite"
date: "2010-03-28T14:49:50"
picture: "bauecke2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26831
- /details3e37.html
imported:
- "2019"
_4images_image_id: "26831"
_4images_cat_id: "1918"
_4images_user_id: "373"
_4images_image_date: "2010-03-28T14:49:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26831 -->
Wie man sieht, habe ich jetzt deutlich mehr Platz zum Bauen als vorher. Außerdem ist noch ein vierter Aldi-Sortierkasten dazugekommen, der allerdings noch nicht komplett gefüllt ist.