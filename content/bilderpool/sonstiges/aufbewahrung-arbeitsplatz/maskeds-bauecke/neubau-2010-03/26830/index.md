---
layout: "image"
title: "Übersicht"
date: "2010-03-28T14:49:49"
picture: "bauecke1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26830
- /detailsa043.html
imported:
- "2019"
_4images_image_id: "26830"
_4images_cat_id: "1918"
_4images_user_id: "373"
_4images_image_date: "2010-03-28T14:49:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26830 -->
Nach vielen Jahren mit einem eigentlich viel zu kleinen, alten Schreibtisch aus der Grundschulzeit habe ich mir mal einen neuen ft-Tisch gegönnt. Eine Leimholzplatte 75x140 (20 Euro), 4 Füße (20 Euro) und ein Rest Parkettlack (nix) ergeben einen guten Bautisch. Allerdings stand er aufgrund das schiefen Bodens so wackelig, dass ich ihn mit Winkel und Gewindestangen an der Wand festgeschraubt habe. Jetzt wackelt da nix mehr.