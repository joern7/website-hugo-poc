---
layout: "image"
title: "so könnte es sein"
date: "2012-02-18T13:28:18"
picture: "ohne.jpg"
weight: "2"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: ["Sammelbox", "Sammelkasten", "Fischertechnik", "1000", "Überlänge"]
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34224
- /detailsb2b0.html
imported:
- "2019"
_4images_image_id: "34224"
_4images_cat_id: "2535"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34224 -->
das ist doch eine Lösung? Die grauen Bausteine 30 sind mit Doppelklebeband (darauf achten, dass es sich wieder Rückstandslos lösen läßt) eingeklebt. Die Höhe ist die gleiche, wie die von den Sammelboxen. Die meisten langen Bauteile kann man so verschwinden lassen. Gruß und Spaß, Andreas