---
layout: "comment"
hidden: true
title: "12241"
date: "2010-09-25T22:52:42"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

sehr beeindruckend!

Sind die Teile in der untersten Reihe kurzen Metallachsen oder irgend welche Metallbuchsen?
Wie lang (oder kurz) sind denn diese Teile/Achsen?
Gibt es, wenn es Original-ft-Achsen sind, davon eine ft-Art.-Nr.?
Bislang kannte ich nur die Achse 30 als kürzeste Metallachse.

Gruß+Dank

Lurchi