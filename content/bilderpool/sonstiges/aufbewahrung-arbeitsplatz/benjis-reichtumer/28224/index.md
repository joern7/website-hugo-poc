---
layout: "image"
title: "Schublade5"
date: "2010-09-25T12:46:13"
picture: "Vastgelegd_2008-2-2_00010.jpg"
weight: "20"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/28224
- /details82ae.html
imported:
- "2019"
_4images_image_id: "28224"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:46:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28224 -->
