---
layout: "image"
title: "Magneten und Sensoren"
date: "2010-09-25T12:43:25"
picture: "100_0156A.jpg"
weight: "11"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/28215
- /detailsb18a.html
imported:
- "2019"
_4images_image_id: "28215"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28215 -->
Auh viele Leuchtkappen