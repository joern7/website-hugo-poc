---
layout: "image"
title: "Bastelzimmer 2009-05.JPG"
date: "2009-06-27T20:17:12"
picture: "Bastelzimmer69.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/24465
- /detailsf263.html
imported:
- "2019"
_4images_image_id: "24465"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2009-06-27T20:17:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24465 -->
Mai 2009, der bevorstehende Umzug zeichnet sich ab. Elektrik (links) und Mechanik (Stirnseite) sind schon in Kartons verpackt.