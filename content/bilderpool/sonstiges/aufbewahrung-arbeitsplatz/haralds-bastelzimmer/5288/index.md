---
layout: "image"
title: "chez Harald.JPG"
date: "2005-11-10T22:26:23"
picture: "Werkstatt65.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5288
- /details5cdd.html
imported:
- "2019"
_4images_image_id: "5288"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5288 -->
Der Raum ist dreigeteilt:
linke Seite: Elektrik/Elektronik
rechte Seite: fischertechnik
Stirnseite: Mechanik (d.h. spanabhebende Metall/Kunststoffbearbeitung)

Die beiden Tische sind (bis auf den Unterschrank rechts vorne) Eigenbauten aus Kanthölzern und Brettern, oben drauf liegen je 2 Regalbretter 40x200 als Arbeitsplatte.

Derart aufgeräumt sieht der Raum eher selten aus, meistens sind ab Kniehöhe alle horizontalen Flächen mit Rohteilen und Erprobungsmustern belegt.