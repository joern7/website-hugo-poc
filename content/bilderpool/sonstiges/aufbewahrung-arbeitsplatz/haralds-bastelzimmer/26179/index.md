---
layout: "image"
title: "bastel1810"
date: "2010-01-28T21:27:00"
picture: "bastel_1810.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26179
- /details32b5.html
imported:
- "2019"
_4images_image_id: "26179"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2010-01-28T21:27:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26179 -->
Das neue Bastelzimmer.