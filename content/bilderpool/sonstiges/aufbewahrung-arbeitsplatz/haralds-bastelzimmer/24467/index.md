---
layout: "image"
title: "Flieger verpackt 09.JPG"
date: "2009-06-27T20:24:43"
picture: "Flieger_verpackt_09.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/24467
- /details836f.html
imported:
- "2019"
_4images_image_id: "24467"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2009-06-27T20:24:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24467 -->
Da ist jetzt der Transportflieger drin. Ganz unten ein Brett, darüber ein aufblasbarer Sitzball (in blau) als Luftkissen, obendrüber eine Stange zum Anfassen und Tragen.