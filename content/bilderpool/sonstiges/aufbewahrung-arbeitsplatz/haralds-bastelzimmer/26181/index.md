---
layout: "image"
title: "bastel_2118.JPG"
date: "2010-01-28T21:34:14"
picture: "bastel_2118.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26181
- /details4470.html
imported:
- "2019"
_4images_image_id: "26181"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2010-01-28T21:34:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26181 -->
Die Werkstatt fürs Grobe (Mechanik, mit Bohrmaschine, Fräse, Drehbänkli) hat auch ein neues Plätzchen gefunden.