---
layout: "image"
title: "Box 1000 1. Stock"
date: "2006-12-29T15:44:44"
picture: "stefansreich2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8166
- /detailsa03d.html
imported:
- "2019"
_4images_image_id: "8166"
_4images_cat_id: "754"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T15:44:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8166 -->
