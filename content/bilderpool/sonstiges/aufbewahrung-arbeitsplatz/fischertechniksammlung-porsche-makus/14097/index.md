---
layout: "image"
title: "03"
date: "2008-03-24T21:17:24"
picture: "20080125_Fischertechniksammlung_12.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14097
- /details9ba3.html
imported:
- "2019"
_4images_image_id: "14097"
_4images_cat_id: "1293"
_4images_user_id: "327"
_4images_image_date: "2008-03-24T21:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14097 -->
