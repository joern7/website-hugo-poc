---
layout: "image"
title: "Steckers..."
date: "2014-05-29T22:53:45"
picture: "FTc2014_036.jpg"
weight: "5"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/38885
- /details8b8a.html
imported:
- "2019"
_4images_image_id: "38885"
_4images_cat_id: "2905"
_4images_user_id: "371"
_4images_image_date: "2014-05-29T22:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38885 -->
