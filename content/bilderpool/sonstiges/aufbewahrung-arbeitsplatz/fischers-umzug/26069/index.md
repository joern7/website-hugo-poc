---
layout: "image"
title: "Ein Sprinter füllt sich"
date: "2010-01-12T15:48:35"
picture: "Fischertechnik_Umzug_003.jpg"
weight: "3"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/26069
- /details8dc2.html
imported:
- "2019"
_4images_image_id: "26069"
_4images_cat_id: "1842"
_4images_user_id: "473"
_4images_image_date: "2010-01-12T15:48:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26069 -->
