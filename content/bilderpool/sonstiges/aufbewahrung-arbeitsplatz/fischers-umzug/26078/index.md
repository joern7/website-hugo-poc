---
layout: "image"
title: "Ein Sprinter füllt sich"
date: "2010-01-12T15:48:41"
picture: "Fischertechnik_Umzug_011.jpg"
weight: "12"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/26078
- /detailsdeb7.html
imported:
- "2019"
_4images_image_id: "26078"
_4images_cat_id: "1842"
_4images_user_id: "473"
_4images_image_date: "2010-01-12T15:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26078 -->
