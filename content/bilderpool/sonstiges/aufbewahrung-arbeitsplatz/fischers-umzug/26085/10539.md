---
layout: "comment"
hidden: true
title: "10539"
date: "2010-01-15T09:22:55"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
@Harald (...Es soll da gerüchteweise Leute geben...) Bis ich vor ca 5 Jahren wieder anfing zu sammeln hatte ich eine Kiste (genau genommen der uralte Nähkasten meiner Mutter, Maße ca. 60*40*30 cm) zugegebener Maßen proppenvoll mit ft. Eine Lage waren nur Silberlinge. Ich war 30 Jahre stolz drauf, - aber angesichts solcher Mengen wird einem doch ganz anders. Ich find's Klasse. Hätte ich mit 12 eine solche Sammlung gesehen, wäre ich sicherlich vor Neid erblasst. Viele Grüße, Ralf