---
layout: "image"
title: "Der Baubereich"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich05.jpg"
weight: "5"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33759
- /details21c8.html
imported:
- "2019"
_4images_image_id: "33759"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33759 -->
Hier sieht man den Bautisch mit Zugriff auf viele ft-Teile.
Hier lagern natürlich noch jede Menge ft-Teile zu bauen.