---
layout: "image"
title: "Vorrat 4"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich30.jpg"
weight: "30"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33784
- /details5f6c.html
imported:
- "2019"
_4images_image_id: "33784"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33784 -->
Ein Vorrat an verschweißten Tütchen für alle Fälle.