---
layout: "image"
title: "Das hintere Lager"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich04.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33758
- /details2806.html
imported:
- "2019"
_4images_image_id: "33758"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33758 -->
Hier befinden sich noch verpackte fischertechnik Kästen, die ich mehrfach habe.
Und auch die eine und andere Rarität.