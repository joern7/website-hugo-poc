---
layout: "image"
title: "Bausteien"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich07.jpg"
weight: "7"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33761
- /detailsc79a.html
imported:
- "2019"
_4images_image_id: "33761"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33761 -->
BS30 und BS15 mit einem und zwei Zapfen in rauhen Mengen.