---
layout: "image"
title: "Drehkränze"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich19.jpg"
weight: "19"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33773
- /detailsea71.html
imported:
- "2019"
_4images_image_id: "33773"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33773 -->
Eine Schublade voll Drehkränze.