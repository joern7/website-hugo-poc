---
layout: "image"
title: "Kleinteile 1"
date: "2011-12-25T14:16:52"
picture: "ftbaubereich47.jpg"
weight: "47"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33801
- /detailsc63e-2.html
imported:
- "2019"
_4images_image_id: "33801"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:52"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33801 -->
