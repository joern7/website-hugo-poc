---
layout: "image"
title: "Kleinteile 2"
date: "2011-12-25T14:16:52"
picture: "ftbaubereich48.jpg"
weight: "48"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33802
- /details7892.html
imported:
- "2019"
_4images_image_id: "33802"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:52"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33802 -->
