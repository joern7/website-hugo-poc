---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel12.jpg"
weight: "12"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27809
- /details0e98-2.html
imported:
- "2019"
_4images_image_id: "27809"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27809 -->
Schublade 8 , eine 200 mm hohe Schublade für fertiges Zeug,Baugruppen etc.