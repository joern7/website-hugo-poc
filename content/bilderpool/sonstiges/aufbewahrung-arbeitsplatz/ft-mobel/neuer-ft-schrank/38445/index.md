---
layout: "image"
title: "Neuer FT Schubladenschrank"
date: "2014-03-09T17:20:54"
picture: "neuerftschrank2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38445
- /details3adb.html
imported:
- "2019"
_4images_image_id: "38445"
_4images_cat_id: "2866"
_4images_user_id: "968"
_4images_image_date: "2014-03-09T17:20:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38445 -->
Diese mal im FT Rastermaß. Die flachen Schubladen haben jeweils Platz für acht Sortierwannen.