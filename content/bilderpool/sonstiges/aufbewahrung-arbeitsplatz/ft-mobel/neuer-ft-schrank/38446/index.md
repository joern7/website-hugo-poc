---
layout: "image"
title: "Neuer FT Schubladenschrank"
date: "2014-03-09T17:20:54"
picture: "neuerftschrank3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38446
- /details4152.html
imported:
- "2019"
_4images_image_id: "38446"
_4images_cat_id: "2866"
_4images_user_id: "968"
_4images_image_date: "2014-03-09T17:20:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38446 -->
10 Schubladen für zB. 80 Wannen und 2 unten für 4 St.1000er Kästen