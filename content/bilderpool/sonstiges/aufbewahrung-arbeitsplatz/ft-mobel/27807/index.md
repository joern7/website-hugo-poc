---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel10.jpg"
weight: "10"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27807
- /detailscd79.html
imported:
- "2019"
_4images_image_id: "27807"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27807 -->
Schublade 6 ,Reifen,Räder,und noch ein paar Kleinteilemagazine.