---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel06.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27803
- /details3b4a.html
imported:
- "2019"
_4images_image_id: "27803"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27803 -->
Schublade 2 ,Bauplatten,etc