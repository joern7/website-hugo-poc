---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:46"
picture: "ftmoebel02.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27799
- /details8191.html
imported:
- "2019"
_4images_image_id: "27799"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27799 -->
Hier ist meine Lösung. Ein Rollcontainer mit 8 Vollauszugschubladen und klappbarer Arbeitsplatte. 1000*600*900 mm groß,aus Birkensperrholz.
Hier im abgebauten,eingeparkten Zustand.