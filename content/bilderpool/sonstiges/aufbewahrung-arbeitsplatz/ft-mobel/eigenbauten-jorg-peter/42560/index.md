---
layout: "image"
title: "Regal 5"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_05n.jpg"
weight: "4"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- /php/details/42560
- /detailsddf4.html
imported:
- "2019"
_4images_image_id: "42560"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42560 -->
Ganz einfach: Mit einer Rundkopfschraube und einer unterlegten Mutter M5 entsteht ein stabiler Pin, auf den die Sortierkästen mit ihrem überkragenden Rand nur eingeschoben werden.