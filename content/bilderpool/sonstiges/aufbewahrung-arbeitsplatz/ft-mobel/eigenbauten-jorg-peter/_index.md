---
layout: "overview"
title: "Eigenbauten von Jörg-Peter"
date: 2020-02-22T09:21:22+01:00
legacy_id:
- /php/categories/3164
- /categories24b0.html
- /categories2026.html
- /categoriesf744.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3164 --> 
Hier gibt es Bilder von selbst entworfenen und selbst gebauten Möbeln für die Aufbewahrung von Fischertechnik (oder auch anderen Kleinteilesammlungen). Sowie einige Detailaufnahmen von der Erbauung. Pläne zum Nachbau werden im Download-Bereich: http://ftcommunity.de/downloads.php?kategorie=ft%3Apedia+Dateien veröffentlicht. Basis sind jeweils Sortierkästen des Herstellers Allit, Serie Europlus37