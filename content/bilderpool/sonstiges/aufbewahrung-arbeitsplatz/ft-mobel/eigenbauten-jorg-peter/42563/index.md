---
layout: "image"
title: "Rollcontainer leer"
date: "2015-12-22T22:42:30"
picture: "ftc_container_02.jpg"
weight: "7"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- /php/details/42563
- /details3fda.html
imported:
- "2019"
_4images_image_id: "42563"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42563 -->
Das ist handwerklich dann schon etwas anspruchsvoller. Der Rollcontainer ist ganz aus Buche Multiplex in 18 mm gebaut. Es gibt an allen vier Seiten Platz für jeweils sieben Sortierboxen. Sie sind in Nuten geführt, die in die Seitenwände gefräst wurden. Bis auf die Rollen unten wurde keine einzige Schraube verwendet, die Teile sind mit Flachdübeln verbunden und verleimt.