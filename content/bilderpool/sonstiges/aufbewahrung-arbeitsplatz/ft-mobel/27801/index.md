---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:46"
picture: "ftmoebel04.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27801
- /details60d7.html
imported:
- "2019"
_4images_image_id: "27801"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27801 -->
Hier ist nun fertig aufgebaut und ich habe eine 2000*600 mm große Arbeitsplatte .
Die ersten  Schubladen sind 50 mm hoch,die nächsten 3 haben 120 mm.