---
layout: "image"
title: "Arbeitsplatz 2"
date: "2009-10-06T18:48:33"
picture: "AP2.jpg"
weight: "2"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/25495
- /details9526.html
imported:
- "2019"
_4images_image_id: "25495"
_4images_cat_id: "1784"
_4images_user_id: "10"
_4images_image_date: "2009-10-06T18:48:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25495 -->
Im Hintergrund mein Schreibtisch