---
layout: "comment"
hidden: true
title: "5366"
date: "2008-02-20T20:06:33"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Das sind die Vorbereitung für den Kommunikationscomputer, der mit 10 seriellen Schnittstellen ausgerüstet wird.

In dem Fischertechnikgehäuse ist dann der Rechner, ein Floppy-Laufwerk, eine 6-Tasten-Tastatur kommt dran, ein 4x16 LC-Display und alle Anschlüsse (Tastatur, Monitor, Parallelport, serielle Ports,...)
und die Stromaufbereitung.