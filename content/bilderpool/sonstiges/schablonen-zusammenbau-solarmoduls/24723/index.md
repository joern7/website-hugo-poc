---
layout: "image"
title: "Schablone 01"
date: "2009-08-09T23:39:21"
picture: "klebe1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24723
- /detailsf606.html
imported:
- "2019"
_4images_image_id: "24723"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24723 -->
Unter den Messeneuheiten 2009 gibt es Profi "Oeco Tech", ft# 505284, 
und Profi "Hydro Cell Kit", ft# 505285, in welchen jeweils ein 
Solarmudul, ft# 136239, mit zwei Bauplatten 15x60 mit 4 Zapfen, 
ft# 38464, mittels zwei Doppelklebestreifen 14x60, ft# 37034, 
beklebt werden, um diese in fischertechnik verbauen zu können.
Die Bauanleitungen zeigen, wie das zu bewerkstelligen ist.

Wenn die beiden Streifen, wie es die Bauanleitung zeigt, 
"freihändig" aufgeklebt werden, ist die Masshaltigkeit mit dem Raster 
von fischertechnik nicht 100%ig gewährleistet.

Ein "Klebeschablone" soll beim Einhalten der Masshaltigkeit helfen.