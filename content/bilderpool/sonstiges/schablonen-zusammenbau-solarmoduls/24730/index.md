---
layout: "image"
title: "Schablone 08"
date: "2009-08-09T23:39:21"
picture: "klebe8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24730
- /details98af.html
imported:
- "2019"
_4images_image_id: "24730"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24730 -->
Jetzt ist das Solarmodul auf die Klebestreifen aufgedrückt.