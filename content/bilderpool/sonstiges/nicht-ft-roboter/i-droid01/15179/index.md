---
layout: "image"
title: "ziode / Einblick hinten"
date: "2008-09-05T14:13:51"
picture: "Immag120.jpg"
weight: "7"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15179
- /details7bed.html
imported:
- "2019"
_4images_image_id: "15179"
_4images_cat_id: "1106"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T14:13:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15179 -->
Hier noch ein oberflächlicher Einblick von hinten in sein Inneres.

Unter dem schwarzen Punkt auf dem Hinterkopf befindet sich eines der drei Mikrofone.

Auf Halshöhe kann man den Encoder mit Lichtschranke erkennen. Darunter, hinter der Platine befindet sich der Getriebeblock für die links/rechts Drehung des Kopfes.

Links und rechts sieht man die weißen Getriebeblöcke für die Arme.

In der Mitte ist die Zentralplatine, an der alle weiteren Platinen angeschlossen werden, und diese u.a. mit Strom versorgt.