---
layout: "image"
title: "ziode / LCD u. Buttons"
date: "2008-09-05T14:13:51"
picture: "Immag119.jpg"
weight: "6"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15178
- /details19e6.html
imported:
- "2019"
_4images_image_id: "15178"
_4images_cat_id: "1106"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T14:13:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15178 -->
Diese 3 Buttons und das LCD dienen zur konfiguration verschiedener Einstellungen und Modi.