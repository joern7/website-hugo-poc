---
layout: "image"
title: "ziode / LED Ohr"
date: "2008-09-05T14:13:51"
picture: "Immag117_2.jpg"
weight: "4"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15176
- /details5f0e.html
imported:
- "2019"
_4images_image_id: "15176"
_4images_cat_id: "1106"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T14:13:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15176 -->
Das LED im Ohr symbolisiert daß er etwas gehört hat.