---
layout: "image"
title: "Untere Umsetzstelle"
date: "2009-06-15T11:19:44"
picture: "20052009147x.jpg"
weight: "4"
konstrukteure: 
- "OTIS"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/24372
- /details41ed.html
imported:
- "2019"
_4images_image_id: "24372"
_4images_cat_id: "1670"
_4images_user_id: "729"
_4images_image_date: "2009-06-15T11:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24372 -->
Hier sieht man die unteren Führungsbügel der Kabine und die (fettigen) Holzschienen.