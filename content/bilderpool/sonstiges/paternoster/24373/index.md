---
layout: "image"
title: "Antrieb"
date: "2009-06-15T11:19:44"
picture: "20052009145x.jpg"
weight: "5"
konstrukteure: 
- "OTIS"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/24373
- /details45ef.html
imported:
- "2019"
_4images_image_id: "24373"
_4images_cat_id: "1670"
_4images_user_id: "729"
_4images_image_date: "2009-06-15T11:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24373 -->
