---
layout: "image"
title: "So sah der Karton aus als ich ihn abholte"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten01.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37096
- /details18a9.html
imported:
- "2019"
_4images_image_id: "37096"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37096 -->
Voll bis zum Stehkragen!