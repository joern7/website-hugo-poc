---
layout: "image"
title: "Noch nen Statikkasten"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten09.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37104
- /detailsb2d1.html
imported:
- "2019"
_4images_image_id: "37104"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37104 -->
