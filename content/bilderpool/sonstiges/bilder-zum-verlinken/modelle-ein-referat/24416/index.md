---
layout: "image"
title: "Sensor Pult"
date: "2009-06-20T13:34:48"
picture: "fischertechnikreferat1.jpg"
weight: "1"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24416
- /details6f92.html
imported:
- "2019"
_4images_image_id: "24416"
_4images_cat_id: "1673"
_4images_user_id: "430"
_4images_image_date: "2009-06-20T13:34:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24416 -->
Auf dieser Grundplatte hab ich sämtliche Sensoren und elektrische Einheiten montiert. 
Optimal, um das mal in der Klasse rumzureichen.