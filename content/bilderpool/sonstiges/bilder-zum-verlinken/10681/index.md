---
layout: "image"
title: "Antriebsrad waagerechte Achse"
date: "2007-06-03T19:07:32"
picture: "plotter4.jpg"
weight: "18"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/10681
- /detailsaffa.html
imported:
- "2019"
_4images_image_id: "10681"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10681 -->
Dies ist das Antriebsrad der waagerechten Achse - das Stahlseil wird recht kunstvoll um die Ecken gelenkt...