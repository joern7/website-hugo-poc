---
layout: "image"
title: "Robo Mobile Hinderniserkennung Brickly-Programm Teil 1"
date: "2017-01-02T17:14:03"
picture: "Brickly1.jpg"
weight: "92"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45006
- /details7a8f.html
imported:
- "2019"
_4images_image_id: "45006"
_4images_cat_id: "843"
_4images_user_id: "2488"
_4images_image_date: "2017-01-02T17:14:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45006 -->
Hier das Brickly-Programm für die Community-Firmware des TXT
als screenshot in drei Teilen.