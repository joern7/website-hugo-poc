---
layout: "image"
title: "fertige Plakate"
date: "2011-08-11T10:09:32"
picture: "plakatefertig1.jpg"
weight: "62"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/31568
- /detailsbf8c.html
imported:
- "2019"
_4images_image_id: "31568"
_4images_cat_id: "843"
_4images_user_id: "373"
_4images_image_date: "2011-08-11T10:09:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31568 -->
Eben kam der Paketbote von UPS und brachte mir 12,5kg Plakate.
Zum Vergleich: vorne ein Baustein 30, das Bett ist 140cm tief.