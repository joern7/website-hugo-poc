---
layout: "overview"
title: "Sammlung zu verkaufen"
date: 2020-02-22T09:22:43+01:00
legacy_id:
- /php/categories/3113
- /categories64aa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3113 --> 
Mein vor kurzen verstorbener Vater war begeisterter Fischertechniker. Viele Stunden, über viele Jahre, verbrachte er damit Modelle zu bauen, zu entwerfen, umzubauen. Nach Anleitung zu basteln war nicht sein Ding, so brütete er stundenlang über Möglichkeiten nach, seine Ideen umzusetzen. Wenn es nun so gar keine Lösung gab, die mit Fischertechnik zu lösen war, ergänzte, bzw. entwarf ich sie mit Eigenbauten aus Holz. Schweren Herzens muss nun alles weg. Gerne würden wir alles behalten, schon alleine aus emotionalen Gründen, jedoch ist es uns aus Platzgründen, durch die nun geänderten Bedingungen, leider nicht möglich. So möchte ich sie anbieten, in der Hoffnung, dass diese Unikate gefallen, die Sonderanfertigungen so begeistern, das für meine Mutter ein Erlös dabei herauskommt, der sie nun, wo sie „alleine“ da steht, etwas finanziell unterstützt. Über Preisangebote wäre ich sehr dankbar! Bitte anschreiben mit der dementsprechenden Nummer des Modells.
Sicherer für die Modelle wäre es auch, wenn sie abgeholt werden könnten, auf Grund der Höhe oder auch der Länge!
MODELL 1: Bezeichnung: 3 Achser „Scania“ mit 2 Achs Anhänger /  Planenzug // Länge: ca. 40 cm (Motorwagen) / ca. 34cm (Anhänger) / Gesamter Zug ca. 74 cm Breite: ca. 15,5 cm Höhe: ca. 19 / Besonderes: Liftachse am Motorwagen / Falk Stadtplan in Fahrertüre / Leiter aus Holz / Unterlegkeil aus Holz / Kennzeichen // -
MODELL 2: Bezeichnung: 7 – Achser Kranwagen // Länge: ca. 120 cm Breite: ca. 14 cm Höhe: ca. 34 Besonderes: 6 von 7 Achsen über das Lenkrad lenkbar / gelbe ektrische Rundumleuchten / Stützenteller aus Holz / Leitern aus Holz / elektrische Arbeitsscheinwerfer / Kennzeichen // -
MODELL 3: Bezeichnung: 12 Achs Flachbettanhänger // Länge: ca. 90 cm Breite: ca. 12 cm Höhe: ca. Besonderes: alle Achsen werden über die Deichsel angesteuert, lenken also mit, je nach Einschlag / gelbe elektrische Rundumleuchten / Kennzeichen // -
MODELL 4: Bezeichnung: Mercedes Benz Schwerlastsattelzugmaschine mit Flachbettauflieger // Länge: ca. 134 cm Breite: ca. 15 cm Höhe: ca. 18 cm Besonderes: 4 Achs Sattelzugmaschine mit über Lenkrad angesteuerter Doppellenkachse / elektrischer Arbeitsscheinwerfer / gelbe elektrische Rundumleuchten / Falk Stadtplan und Cola Flasche in Fahrertüre / 5 Achs Auflieger mit automatischem Lenkeinschlag der Räder bei Ansteuerung / Kennzeichen // -
MODELL 5: Bezeichnung: MAN 2 Achs Sattelzugmaschine TGA 18.480 mit Kofferkühlauflieger // Länge: ca. 66 cm Breite: ca. 14 cm Höhe: ca. 14 cm Besonderes: Federung der 3 Achsen des Aufliegers, erste Achse ausgelegt als Liftachse / Kühlaggregat an Stirnwand des Aufliegers / Kennzeichen // - 
MODELL 6: Bezeichnung: Kleiner 2 Achs Kranwagen // Länge: ca. 40 cm Breite: ca. 12,5 cm Höhe: ca. 20 Besonderes:       - /-
MODELL 7: Bezeichnung: MB Trac Turbo 900 // Länge: ca. 32 cm Breite: ca. 13,5 cm Höhe: ca. 17 cm Besonderes: Baumzangenaggregat am Heck / Kennzeichen // -	
MODELL 8: Bezeichnung: Tandemachs Anhänger für MB Trac // Länge: ca. 25 cm Breite: ca. 11 cm Höhe: ca. 10 cm Besonderes: Kipperaufbau / Kennzeichen // -
MODELL 9: Bezeichnung: 2 Achs Anhänger für MB Trac // Länge: ca. 25 cm Breite: ca. 11 cm Höhe: ca. 12 cm Besonderes: Kennzeichen // -
MODELL 10: Bezeichnung: 2 Achs Anhänger für MB Trac // Länge: ca. 33 cm Breite: ca. 11 cm Höhe: ca. 11 cm Besonderes: Kipperaufbau / Kennzeichen // -
MODELL 11: Bezeichnung: 4 Achs Anhänger // Länge: ca. 56 cm Breite: ca. 12,5 cm Höhe: ca. 9 cm Besonderes: 3 Achsen gelenkt durch Ansteuerung der Deichsel / gelbe elektrische Rundumleuchte / Kennzeichen // -
MODELL 12: Bezeichnung: US Truck mit Tiefbettauflieger // Länge: ca. 104 cm Breite: ca. 12 cm Höhe: ca. 18,5 cm Besonderes: Gelbe elektrische Rundumleuchten / elektrische Arbeitsscheinwerfer /                       Trollytiefbettauflieger mit gelber elektrischer Rundumleuchte / gelenkte Nachlaufachse durch automatische Ansteuerung bei Einschlag des Lenkrades / Holzleiter / Kennzeichen // -
MODELL 13: Bezeichnung: US Truck 3 Achser Zugmaschine „Peterbilt“ // Länge: ca. 45 cm Breite: ca. 12 cm Höhe: ca. 18,5 cm Besonderes: Ladekran am Heck / bedruckte Plane / elektrischer Arbeitsschweinwerfer / gelbe elektrische Rundumleuchten / bei Lenkungseinschlag automatisch                    angesteuerte Nachlaufachse / Seilwinde vorn / Kennzeichen // -
MODELL 14: Bezeichnung: 4 Achs Anhänger // Länge: ca. 55 cm Breite: ca. 12,5 cm Höhe: ca. 9 cm Besonderes: Automatische bei Deichseleinschlag angesteuerte Nachlaufachse / gelbes elektrisches Rundumlicht // -
MODELL 15: Bezeichnung: MAN Feuerwehr Drehleiterwagen // Länge: ca. 48 cm mit eingefahrener Leiter Breite: ca. 12,5 cm Höhe: ca. 20 cm mit eingefahrener Leiter Besonderes: gelenkte Nachlaufachse durch automatische Ansteuerung bei Einschlag des Lenkrades  / Drehleiter mit Korb (aus Holz) / Wassertank mit Handpumpe / Wasserschlauch / Besatzung / elektrische blaue Rundumleuchten / Kennzeichen // -
MODELL 16: Bezeichnung: Planierraupe // Länge: ca. 28 cm Breite: ca. 18 cm Höhe: ca. 15 cm Besonderes:       - /-
MODELL 17:  Bezeichnung: Traktormit Anhänger „Vatertagstour“ // Länge: ca. 50 cm Breite: ca. 12 cm Höhe: ca. 14 cm Besonderes:       - /-
MODELL 18:  Bezeichnung: Schienendrehkran // Länge: ca. 60 cm Breite: ca. 20 cm Höhe: ca. 82 cm Besonderes: Elektrisch Fahrbar / elektrische Arbeitsscheinwerfer / elektrische Winden für hoch – runter, Neigung, Drehung / Holztreppenaufgang zum Kranstand / elektrische rote Warnlampen // -
MODELL 19: Bezeichnung: Wechselaufbaukranaggregat z.B. für US Truck „Peterbilt“ // Länge: ca. 20 cm Breite: ca. 11 cm Höhe: ca. 18 cm Besonderes:         - /-