---
layout: "image"
title: "MODELL 12: Bezeichnung: US Truck mit Tiefbettauflieger"
date: "2015-08-16T18:51:31"
picture: "Nr._12_Bild_1.jpg"
weight: "43"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41795
- /details841b.html
imported:
- "2019"
_4images_image_id: "41795"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41795 -->
MODELL 12: Bezeichnung: US Truck mit Tiefbettauflieger // Länge: ca. 104 cm Breite: ca. 12 cm Höhe: ca. 18,5 cm Besonderes: Gelbe elektrische Rundumleuchten / elektrische Arbeitsscheinwerfer /                       Trollytiefbettauflieger mit gelber elektrischer Rundumleuchte / gelenkte Nachlaufachse durch automatische Ansteuerung bei Einschlag des Lenkrades / Holzleiter / Kennzeichen // -