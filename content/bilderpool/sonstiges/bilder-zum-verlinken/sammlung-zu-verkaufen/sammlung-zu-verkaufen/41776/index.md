---
layout: "image"
title: "MODELL 5: Bezeichnung: MAN 2 Achs Sattelzugmaschine TGA 18.480 mit Kofferkühlauflieger"
date: "2015-08-16T18:51:31"
picture: "Nr._5_Bild_2.jpg"
weight: "24"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41776
- /detailsa2f2.html
imported:
- "2019"
_4images_image_id: "41776"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41776 -->
