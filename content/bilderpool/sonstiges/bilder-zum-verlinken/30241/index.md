---
layout: "image"
title: "Temperaturumwandlung"
date: "2011-03-08T18:28:51"
picture: "temperatur1.jpg"
weight: "57"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/30241
- /detailsdffd.html
imported:
- "2019"
_4images_image_id: "30241"
_4images_cat_id: "843"
_4images_user_id: "1264"
_4images_image_date: "2011-03-08T18:28:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30241 -->
Unterprogramm um den Wert des  NTC-Wiederstands in Grad Celsius umzurechnen.