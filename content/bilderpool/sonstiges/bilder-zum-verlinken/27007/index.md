---
layout: "image"
title: "x86-Buildkonfiguration"
date: "2010-04-27T22:12:58"
picture: "x1.jpg"
weight: "52"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27007
- /details6cc7.html
imported:
- "2019"
_4images_image_id: "27007"
_4images_cat_id: "843"
_4images_user_id: "104"
_4images_image_date: "2010-04-27T22:12:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27007 -->
Für die Diskussion im Forum, wie man Visual Studio 2008 (ob nun eine der Express-Versionen oder eine höhere) dazu bringt, Assemblies ausdrücklich für x86 zu bauen, so dass sie auch auf einem x64-System ausgeführt 32-Bit-native-code-DLLs ansprechen können: So wie hier gezeigt muss das aussehen. Also sowohl die aktive Konfiguration muss auf x86 stehen, als auch die Konfiguration jedes einzelnen Teilprojektes der Solution. Normalerweise landet dann das Kompilat auch in einem x86-Unterordner des bin-Verzeichnisses, das ist auch noch zu beachten, sonst verwendet man ungewollt evtl. doch wieder noch die für jede CPU kompilierte Fassung.