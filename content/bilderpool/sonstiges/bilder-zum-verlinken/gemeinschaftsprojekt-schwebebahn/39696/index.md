---
layout: "image"
title: "zug04.jpg"
date: "2014-10-10T19:10:48"
picture: "zug04.jpg"
weight: "4"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39696
- /detailsb05e.html
imported:
- "2019"
_4images_image_id: "39696"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39696 -->
Das ist nur eine Testschiene um den Antrieb des Zuges zu testen, kein Entwurf fürs Traggerüst. Dieses muss deutlich Stabiler werden. Der Abstand zwischen den beiden Schienen stimmt, es ist der kleinstmögliche Abstand. Er sollte für die Streckenkonstruktion übernommen werden.