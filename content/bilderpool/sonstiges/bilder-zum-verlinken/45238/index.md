---
layout: "image"
title: "LuckyDokument"
date: "2017-02-16T18:06:43"
picture: "FT_Brief.jpg"
weight: "96"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/45238
- /detailsffca.html
imported:
- "2019"
_4images_image_id: "45238"
_4images_cat_id: "843"
_4images_user_id: "10"
_4images_image_date: "2017-02-16T18:06:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45238 -->
ein über 25 Jahre altes Dokument, das ein ähnliches Problem thematisiert, wie es auch heute noch aktuell ist.