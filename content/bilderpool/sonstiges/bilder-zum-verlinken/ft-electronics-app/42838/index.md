---
layout: "image"
title: "Windows Phone 8.1-App - Detailansicht"
date: "2016-01-30T01:23:15"
picture: "ftelectronicsapp4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42838
- /detailsc1d5.html
imported:
- "2019"
_4images_image_id: "42838"
_4images_cat_id: "3184"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T01:23:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42838 -->
Tippt man auf eine der Schaltflächen, bekommt man Schaltbild und Beschreibungen (ggf. zum Scrollen) angezeigt. In diesem Modus kann man nicht nur zurück zur Übersicht über das jeweilige Modul, sondern auch nach links/rechts durch alle Funktionen des gewählten Moduls durchwischen.