---
layout: "image"
title: "Windows 8.1-App - Startbild"
date: "2016-01-30T01:23:15"
picture: "ftelectronicsapp5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42839
- /detailsfbd9.html
imported:
- "2019"
_4images_image_id: "42839"
_4images_cat_id: "3184"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T01:23:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42839 -->
Dieselbe App soll auch für Windows 8.1 oder höher zur Verfügung stehen.