---
layout: "image"
title: "Rad"
date: "2007-07-17T16:52:56"
picture: "DSC_0529_Kleine_Webansicht.jpg"
weight: "24"
konstrukteure: 
- "Vormann Frederik"
fotografen:
- "Vormann Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11124
- /detailsa4b1.html
imported:
- "2019"
_4images_image_id: "11124"
_4images_cat_id: "843"
_4images_user_id: "453"
_4images_image_date: "2007-07-17T16:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11124 -->
Radaufhängung mit Antrieb und Lenkung.