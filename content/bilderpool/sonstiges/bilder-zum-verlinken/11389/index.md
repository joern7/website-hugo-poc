---
layout: "image"
title: "Schnecke"
date: "2007-08-17T18:41:07"
picture: "schnecke.jpg"
weight: "27"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: ["Schnecke"]
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11389
- /detailsf8d9.html
imported:
- "2019"
_4images_image_id: "11389"
_4images_cat_id: "843"
_4images_user_id: "453"
_4images_image_date: "2007-08-17T18:41:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11389 -->
Hat eigentlich schon mal einer etwas mit dieser Methode gebaut ?