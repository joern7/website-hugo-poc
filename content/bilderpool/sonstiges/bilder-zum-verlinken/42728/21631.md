---
layout: "comment"
hidden: true
title: "21631"
date: "2016-01-31T15:48:56"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Cher,
sehr schön. Meine Messwerte waren (beim VCNL4000) bei weitem nicht so genau... Allerdings habe ich auch mit Störlicht herumexperimentiert. Darauf schien er mir empfindlich zu reagieren.
Aber ich schaue mir das nochmal genau an, vielleicht habe ich einen Fehler gemacht...
Gruß, Dirk