---
layout: "image"
title: "FT Hefte der Frühzeit"
date: "2014-10-02T09:00:35"
picture: "fischertechnikheftederfruehzeit2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39490
- /details8fa6.html
imported:
- "2019"
_4images_image_id: "39490"
_4images_cat_id: "2957"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39490 -->
Hier der Druck auf beiden Front Innenseiten.