---
layout: "image"
title: "FT Hefte der Frühzeit"
date: "2014-10-02T09:00:25"
picture: "fischertechnikheftederfruehzeit1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39489
- /details5c08.html
imported:
- "2019"
_4images_image_id: "39489"
_4images_cat_id: "2957"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39489 -->
Diese Hefte sind relativ aufwendig gemacht. Vor allem die Heftung war,glaub ich, damals noch nicht so üblich.