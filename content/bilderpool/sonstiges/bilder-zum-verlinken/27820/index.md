---
layout: "image"
title: "Encodermotor mit Metallachse"
date: "2010-08-15T11:51:28"
picture: "XM-Achse.jpg"
weight: "53"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["XM", "Encoder", "verbinden"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/27820
- /details7c5a.html
imported:
- "2019"
_4images_image_id: "27820"
_4images_cat_id: "843"
_4images_user_id: "182"
_4images_image_date: "2010-08-15T11:51:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27820 -->
So kann man eine Metallachse mit einem Encoder bzw. XM-Motor verbinden. Durch meinen Verbinder 4mm wird die Metallachse fest verbunden