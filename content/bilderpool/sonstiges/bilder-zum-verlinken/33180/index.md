---
layout: "image"
title: "LKW"
date: "2011-10-16T11:50:14"
picture: "LKW.jpg"
weight: "64"
konstrukteure: 
- "wird noch gesucht"
fotografen:
- "?"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/33180
- /details98d8.html
imported:
- "2019"
_4images_image_id: "33180"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2011-10-16T11:50:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33180 -->
Vor kurzem wurde ein Werbeplakat bei ebay versteigert.
Auf dem Plakat war eben dieser gnadenlos gut aussehende LKW abgebildet.

Ich gehe mal ganz fest davon aus das jemand aus unseren Reihen die Auktion gewonnen hat.
Vielleicht wäre dieser so nett und würde eine Kopie davon anfertigen......?

Falls es jemand anderes war -> wer kann mir zu diesem LKW mehr sagen?
Wer hat Infos darüber.

Bitte endschuldigt das kleine Format. Leider habe ich es nicht anders....

Gruß ludger