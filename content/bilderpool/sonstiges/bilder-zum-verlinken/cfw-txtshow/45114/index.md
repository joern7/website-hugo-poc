---
layout: "image"
title: "TXTShow mit UI gezoomt"
date: "2017-02-03T20:02:53"
picture: "txtshow01a.png"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45114
- /detailse382.html
imported:
- "2019"
_4images_image_id: "45114"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45114 -->
Hier das User-Interface bei gezoomtem Bild.
Mit den Pfeilen kann man jetzt den Bildausschnitt verschieben.