---
layout: "image"
title: "TXTShow mit UI"
date: "2017-02-03T20:02:53"
picture: "txtshow01.png"
weight: "1"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: ["cfw", "TXT", "community", "firmware", "App", "TXTShow"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45113
- /detailsf9f1.html
imported:
- "2019"
_4images_image_id: "45113"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45113 -->
Bild mit eingeblendetem User-Interface. Das User Interface wird durch antippen des Bildschirms ein- und ausgeblendet.

Die Funktionen: 
- oben links: Menu aufrufen
- oben rechts: beenden
- Pfeile links und rechts: vorheriges bzw. nächstes Bild anzeigen
- unten links: Zoom an/aus
- unten rechts: Play/Pause