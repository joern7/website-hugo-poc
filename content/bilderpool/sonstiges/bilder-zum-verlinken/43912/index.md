---
layout: "image"
title: "4x20 LCD Display"
date: "2016-07-19T15:45:56"
picture: "500599_4x20_LCD_Display_2.jpg"
weight: "85"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Wölffel"
keywords: ["4x20", "LCD", "Display", "ftdesigner"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43912
- /details4005-2.html
imported:
- "2019"
_4images_image_id: "43912"
_4images_cat_id: "843"
_4images_user_id: "2303"
_4images_image_date: "2016-07-19T15:45:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43912 -->
Bauteil für ftdesigner aus Downloaddatei ftdesigner_bauteile_teil_2

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_2.zip