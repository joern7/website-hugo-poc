---
layout: "image"
title: "Suchfunktion"
date: "2011-10-08T20:22:42"
picture: "suchfunktion1.jpg"
weight: "63"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/33113
- /details3a42.html
imported:
- "2019"
_4images_image_id: "33113"
_4images_cat_id: "843"
_4images_user_id: "1127"
_4images_image_date: "2011-10-08T20:22:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33113 -->
Eine einfache Suchfunktion, um einen Wert in einer Liste zu scuhen.