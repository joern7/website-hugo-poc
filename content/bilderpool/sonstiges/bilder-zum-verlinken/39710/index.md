---
layout: "image"
title: "Entwurf - längerer Pneumatik-Zylinder"
date: "2014-10-16T17:11:21"
picture: "langer_Zylinder_engltige_Fassung_ftc.jpg"
weight: "76"
konstrukteure: 
- "Julian"
fotografen:
- "Julian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Julian"
license: "unknown"
legacy_id:
- /php/details/39710
- /details4b96.html
imported:
- "2019"
_4images_image_id: "39710"
_4images_cat_id: "843"
_4images_user_id: "2271"
_4images_image_date: "2014-10-16T17:11:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39710 -->
Das besondere an diesem Pneumatik-Zylinder ist, dass es eigentlich zwei Zylinder sind, die ineinander stecken. Dadurch wird ein doppelter Hub Weg möglich. Hier sind  drei mögliche Stellungen gezeigt:
links: komplett eingefahren
mittig: halb ausgefahren
rechts: komplett ausgefahren