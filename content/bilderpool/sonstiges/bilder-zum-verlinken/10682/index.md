---
layout: "image"
title: "Seilzug"
date: "2007-06-03T19:07:32"
picture: "plotter5.jpg"
weight: "19"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/10682
- /detailse2c8.html
imported:
- "2019"
_4images_image_id: "10682"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10682 -->
Hier kann man sehr schön die Feder sehen, die den Seilzug für die waagerechte Achse spannt.