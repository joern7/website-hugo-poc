---
layout: "image"
title: "Übersicht - geöffnet"
date: "2007-06-03T19:07:32"
picture: "plotter3.jpg"
weight: "17"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/10680
- /detailsf902.html
imported:
- "2019"
_4images_image_id: "10680"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10680 -->
Leider etwas unscharf.
Der Stift-Revolver hat ca. 15mm im Durchmesser - ist also recht klein.