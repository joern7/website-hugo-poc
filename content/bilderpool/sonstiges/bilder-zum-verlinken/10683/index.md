---
layout: "image"
title: "Hubmagnet"
date: "2007-06-03T19:07:32"
picture: "plotter6.jpg"
weight: "20"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/10683
- /details7b90.html
imported:
- "2019"
_4images_image_id: "10683"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10683 -->
Dies ist der Hubmagnet, der den Stift an das Papier anpresst. Unten sieht man nochmals die Andruckleiste und das darauf laufende Lager.
Im Hintergrund der Antrieb für die Papierrolle, damit das Papier weniger Schlupf hat, sind kleine Stacheln in der Gummiwalze.