---
layout: "image"
title: "cfw BenoiTxt - Optionsmenu"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt03.jpg"
weight: "3"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45251
- /details6f42.html
imported:
- "2019"
_4images_image_id: "45251"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45251 -->
... in dem man Ein- und auszoomen sowie einige Einstellungen verändern kann, um dann...