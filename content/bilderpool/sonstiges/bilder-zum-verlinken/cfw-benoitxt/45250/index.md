---
layout: "image"
title: "cfw BenoiTxt - das Startbild"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt02.jpg"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45250
- /detailsbdfd.html
imported:
- "2019"
_4images_image_id: "45250"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45250 -->
... die Welt der Fraktale auf dem TXT.

Durch Antippen des Bilds erscheint das Optionsmenu...