---
layout: "image"
title: "i-fischertechnik"
date: "2009-05-08T16:43:04"
picture: "ft_iphone_penguin.jpg"
weight: "14"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23914
- /details8307.html
imported:
- "2019"
_4images_image_id: "23914"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2009-05-08T16:43:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23914 -->
This is a mockup of an iphone application: i-fischertechnik! (This would be a fun side project!)