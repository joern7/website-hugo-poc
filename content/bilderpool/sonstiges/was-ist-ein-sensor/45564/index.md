---
layout: "image"
title: "unbekannter Meß(?)sensor"
date: "2017-03-19T12:20:53"
picture: "Sensor_klein.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/45564
- /details4349.html
imported:
- "2019"
_4images_image_id: "45564"
_4images_cat_id: "1261"
_4images_user_id: "10"
_4images_image_date: "2017-03-19T12:20:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45564 -->
Dieser Sensor viel mir jetzt in einem Konvolut in die Hände. Mit der Aufschrift auf der Tüte konnte ich nichts anfangen. Vielleicht ja jemand sowas schon mal gesehen.
Ich könnte mir vorstellen, das es was ähnliches wie ein Dehnungsstreifen ist, oder zur Gewichtsmessung dient, aber wie?