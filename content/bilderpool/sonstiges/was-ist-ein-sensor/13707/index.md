---
layout: "image"
title: "Vorne kann man eine Spule mit Ferritkern Sehen"
date: "2008-02-19T17:20:51"
picture: "wasistdasfuereinsensor3.jpg"
weight: "3"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13707
- /detailseef9.html
imported:
- "2019"
_4images_image_id: "13707"
_4images_cat_id: "1261"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13707 -->
