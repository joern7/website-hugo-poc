---
layout: "comment"
hidden: true
title: "17342"
date: "2012-10-02T16:31:28"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
das ist unfair gemessen - schließlich muss man einen Vorher-Nachher-Vergleich machen. Und da hätten wir beide wahrscheinlich gut abgeschnitten - die Kuchenkalorien haben nicht annähernd ausgereicht, um unsere Hirne während des stundenlangen Fachsimpelns ausreichend mit Energie zu versorgen. Da ging es also an die Reserven - Kuchen hin oder her...
Gruß, Dirk