---
layout: "image"
title: "Scorpion"
date: "2010-07-09T08:53:48"
picture: "ft-scorpion.jpg"
weight: "23"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27735
- /detailsa9fa.html
imported:
- "2019"
_4images_image_id: "27735"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2010-07-09T08:53:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27735 -->
This is a scorpion I created for the Instructables gift exchange.