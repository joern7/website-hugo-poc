---
layout: "image"
title: "ft Jumpsuit"
date: "2008-06-22T19:55:59"
picture: "ft_mar_2.jpg"
weight: "12"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["jumpsuit"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14758
- /detailsa4e1.html
imported:
- "2019"
_4images_image_id: "14758"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2008-06-22T19:55:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14758 -->
My wife modeling the ft fan jumpsuit.

***google translation
	
Meine Frau Modellierung der FT-Fan Jumpsuit.