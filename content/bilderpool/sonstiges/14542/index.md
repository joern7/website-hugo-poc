---
layout: "image"
title: "August and the Crane"
date: "2008-05-17T10:10:00"
picture: "august_crane2.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["crane", "96778", "Cranes"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14542
- /details0ad5.html
imported:
- "2019"
_4images_image_id: "14542"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2008-05-17T10:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14542 -->
Amelia's brother enjoys the crane too!

***google translation:

Amelia's Bruder genießt der Kran auch!