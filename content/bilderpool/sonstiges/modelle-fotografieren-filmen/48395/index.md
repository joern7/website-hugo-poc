---
layout: "image"
title: "Spiegelreflexkamera EOS"
date: "2018-11-06T11:03:28"
picture: "fotografieren25.jpg"
weight: "25"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48395
- /details6273.html
imported:
- "2019"
_4images_image_id: "48395"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:28"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48395 -->
Kamera Canon EOS 1300 D mit Objektiv

Großer 18-Megapixel-CMOS-Sensor 
TTL-CT-SIR mit CMOS-Sensor, AF-Messwertspeicherung 
7,5 cm (3,0 Zoll) TFT, ca. 920.000 Bildpunkte,
Ladezeit des integrierten Blitzgeräts - ca. 2 Sekunden, automatischer Weißabgleich über Bildsensor

