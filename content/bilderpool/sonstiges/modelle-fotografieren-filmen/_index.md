---
layout: "overview"
title: "Modelle fotografieren und filmen"
date: 2020-02-22T09:23:29+01:00
legacy_id:
- /php/categories/3544
- /categoriesb57a.html
- /categories8c35.html
- /categories4945.html
- /categories6e12.html
- /categoriese26a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3544 --> 
Hallo fischertechniker,

heute möchte ich euch meine Foto- und Filmecke vorstellen.

Da ich, wie viele von euch, im Bastelzimmer immer ein Platzproblem habe, möchte ich euch hier meine Lösung vorstellen.
Der Vorteil dieser Lösung ist, das die Foto- und Filmecke, schnell aufgebaut ist. Ansonsten dient der Platz zum Abstellen der
fertigen Modelle. 