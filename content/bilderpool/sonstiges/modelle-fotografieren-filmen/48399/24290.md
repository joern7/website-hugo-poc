---
layout: "comment"
hidden: true
title: "24290"
date: "2018-11-06T14:09:50"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

danke. Ja der Aufwand ist nicht unerheblich. Wobei beim Zeitfaktor der allergrößte ist.
Ich denke es lohnt sich, da ich ja auch Tage und Monate an Zeit in das Modell stecke.
Das ist nicht ganz günstig, macht aber viel Spaß, wenn du so deine Modelle dokumentieren
kannst. 

Hier die Kosten und Bezugsquellen:

Kamera + Zubehör = 436,- Euro

https://www.amazon.de/Canon-EOS-1300d-18-100EG-schwarz/dp/B01GIL8AJM/ref=sr_1_6?rps=1&ie=UTF8&qid=1541508558&sr=8-6&keywords=canon+eos+1300+d&refinements=p_76%3A419122031

Stativ:
Stativ Hama Star 63 = 29,99,- Euro

https://www.amazon.de/Hama-Leichtes-Einsteiger-Dreibeinstativ-3-Wege-Kopf-Champagner/dp/B0000WXD1G/ref=sr_1_1?ie=UTF8&qid=1541508872&sr=8-1&keywords=stativ+hama+star+63

Fotostudio:

3 x Fotostudio Studioleuchte Set Softbox Studiolampe Stativ Galgenstativ = 78,99,- Euro

https://www.ebay.de/i/382552622210

Hintergrund-Papier:

Hintergrund-Papier für Fotografie, Video und Fernsehen (Weiß) = 30,99,- Euro

https://www.amazon.de/Nahtlose-Faltbare-Hintergrund-Hintergrund-Papier-Fotografie-Fernsehen/dp/B00ZOM7VDS/ref=sr_1_8?ie=UTF8&qid=1541509326&sr=8-8&keywords=Fotostudio+Papierhintergrund

Grüße
Dirk