---
layout: "image"
title: "Rolle Plotterpapier"
date: "2018-11-06T11:03:10"
picture: "fotografieren08.jpg"
weight: "8"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48378
- /details33a8.html
imported:
- "2019"
_4images_image_id: "48378"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48378 -->
Der Vorteil der Papierrolle ist das im Hintergrund keine Kante entsteht.