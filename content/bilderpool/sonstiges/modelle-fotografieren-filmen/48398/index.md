---
layout: "image"
title: "Bildbeispiel 3 Fräse"
date: "2018-11-06T11:03:28"
picture: "fotografieren28.jpg"
weight: "28"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48398
- /details983e.html
imported:
- "2019"
_4images_image_id: "48398"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:28"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48398 -->
