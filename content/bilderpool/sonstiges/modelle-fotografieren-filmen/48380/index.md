---
layout: "image"
title: "Plotterpapier aufgerollt"
date: "2018-11-06T11:03:10"
picture: "fotografieren10.jpg"
weight: "10"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48380
- /details3ed4.html
imported:
- "2019"
_4images_image_id: "48380"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:10"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48380 -->
