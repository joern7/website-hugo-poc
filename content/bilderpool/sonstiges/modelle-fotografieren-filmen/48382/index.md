---
layout: "image"
title: "Fotostudio Softbox mit Stativ"
date: "2018-11-06T11:03:21"
picture: "fotografieren12.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48382
- /details7979.html
imported:
- "2019"
_4images_image_id: "48382"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48382 -->
