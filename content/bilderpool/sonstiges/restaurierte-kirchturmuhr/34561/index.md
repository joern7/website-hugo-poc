---
layout: "image"
title: "Viertelschlagwerk"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34561
- /detailseb98.html
imported:
- "2019"
_4images_image_id: "34561"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34561 -->
Dieses Werk war dafür zuständig, immer zur vollen Viertelstunde ein Mal zu läuten.