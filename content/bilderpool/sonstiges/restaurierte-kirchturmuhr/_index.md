---
layout: "overview"
title: "Restaurierte Kirchturmuhr"
date: 2020-02-22T09:23:27+01:00
legacy_id:
- /php/categories/2553
- /categories85c5.html
- /categories7399.html
- /categories6395.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2553 --> 
Bilder einer Kirchturmuhr, die mittlerweile in einem Treppenhaus gut zu erkunden ist.