---
layout: "image"
title: "Rückansicht, Gewicht und Pendel"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34565
- /details8517.html
imported:
- "2019"
_4images_image_id: "34565"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34565 -->
Von der Rückseite sieht das ganze so aus. Das Gewicht senkt sich durchs Treppenhaus des Hotels ab.