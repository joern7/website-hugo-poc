---
layout: "image"
title: "Antrieb des großen Ziffernblattes"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34567
- /details6fdf-2.html
imported:
- "2019"
_4images_image_id: "34567"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34567 -->
Die senkrechte Welle dreht sich einmal pro Stunde und geht über das Winkelgetriebe und Kardan zum großen Ziffernblatt, welches ein eigenes Getriebe für den Stundenzeiger besitzt.