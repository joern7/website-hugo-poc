---
layout: "image"
title: "Beschreibung"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34560
- /detailsb879.html
imported:
- "2019"
_4images_image_id: "34560"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34560 -->
Text siehe oben ;-)