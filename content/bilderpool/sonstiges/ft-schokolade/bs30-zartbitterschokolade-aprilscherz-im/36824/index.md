---
layout: "image"
title: "Schoko-BS30 (6)"
date: "2013-04-02T15:32:14"
picture: "P1070859_verkleinert.jpg"
weight: "6"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/36824
- /detailsf261.html
imported:
- "2019"
_4images_image_id: "36824"
_4images_cat_id: "2733"
_4images_user_id: "1028"
_4images_image_date: "2013-04-02T15:32:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36824 -->
Und schließlich der komplett befreite und mit einem Messer leicht nachbearbeitete BS30, wie er im Forum veröffentlicht wurde. 
Tatsächlich besteht er aus vielen kleinen Stückchen, die für dieses Bild zusammengepuzzelt wurden. Bei genauem Hinsehen ist auch unten links im Bild eine Lücke eines weggebrochenen Teils erkennbar.