---
layout: "image"
title: "Erster Screenshot einer neuen TXT-App"
date: "2018-06-18T12:39:28"
picture: "Screenshot_1529239133-downscale.png"
weight: "38"
konstrukteure: 
- "Robbi"
fotografen:
- "Robbi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- /php/details/47690
- /detailsa155.html
imported:
- "2019"
_4images_image_id: "47690"
_4images_cat_id: "312"
_4images_user_id: "1442"
_4images_image_date: "2018-06-18T12:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47690 -->
https://forum.ftcommunity.de/viewtopic.php?f=33&t=4825