---
layout: "overview"
title: "Seltsamer Stein"
date: 2020-02-22T09:22:35+01:00
legacy_id:
- /php/categories/829
- /categoriesde66.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=829 --> 
Das ist ein Stein, den ich und mein Freund im Bach gefunden haben. Wir haben schon überall nachgeguckt, aber nix über die Herkunft herausgefunden. Wisst ihr was?