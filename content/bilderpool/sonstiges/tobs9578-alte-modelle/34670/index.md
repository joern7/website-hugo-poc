---
layout: "image"
title: "Kegelbahn 3"
date: "2012-03-20T13:17:08"
picture: "alt6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/34670
- /detailse006.html
imported:
- "2019"
_4images_image_id: "34670"
_4images_cat_id: "2558"
_4images_user_id: "1007"
_4images_image_date: "2012-03-20T13:17:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34670 -->
