---
layout: "image"
title: "Raffael meets Tim&Tom"
date: 2020-05-24T12:48:44+02:00
picture: "TimUndTom6.png"
weight: "5"
konstrukteure: 
- "Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---
Worüber unterhalten sich die wichtigsten Gelehrten der Antike in Raffaels berühmten Gemälde "Die Schule von Athen"? Mathematik, Physik, Philosophie? Weit gefehlt, es geht um die wichtigste Frage der Weltgeschichte...
