---
layout: "image"
title: "Cyclograph   Einstellungen Y Richtung der Saul , und auch X Richtung"
date: "2008-05-03T15:20:28"
picture: "meccanozeichenmaschine01.jpg"
weight: "2"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland)"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/14434
- /details8cea.html
imported:
- "2019"
_4images_image_id: "14434"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:20:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14434 -->
Hier sieht man vorne in der Maschine ,  etwa  Mittelhöhe ,  bemerke das Meccano Schild unten im Foto als Referenz)   Hier sind die Einstellungen für Bewegungen der Y Richtung der Saule .   Auch die Intervalschaltung befindet sich hier .    
Dasselbe  ,  aber für X Richtung befindet sich "um die Ecke ",  und man seht das rechts im Foto . 
überall in der Maschine ist der mechanische Spiel zum miniumum reduziert .  Bemerke auch die starke Zugfeder zB