---
layout: "comment"
hidden: true
title: "6394"
date: "2008-05-03T22:52:28"
uploadBy:
- "pvd"
license: "unknown"
imported:
- "2019"
---
Eine Erklärung der mir der Konstrukteur , Herr Weststrate , gegeben hat war folgende : es darf absolut kein Spiel in die Achslagerungen geben.  Ein Loch in ein Lochband ist zB 4,2 Mm .  Die Achse selbst ist 4 Mm. (ob das nun die absolut genaue Abmessungen sind , ist eigentlich nicht sehr zur Sache.  Es geht ums Prinzip :) Also : 4 Mm innerhalb 4,2 Mm   das lauft wohl leicht , aber es hat sehr viel Spiel in alle Richtungen. 
Ein Trick der verwendet wird is folgender :  es wird nicht 1 Lochband verwendet , sondern zB 3 oder 4 werden zusammengenommen (bundle)  , und die werden miteindander verbunden (Lochbänder mit 5 Löcher sitzen an Gewindestange und 2 Mutter, in Loch 1 und Loch 5 .) In Loch 3 ist dann die eigentliche Lagerung .  
Hier vorne im FOto sieht man eine Gewindestange , mit jedesmal ein Paar Mutter und dazwischen solche Lochbänder. 
Genaue justierung und kleine Verschiebungen der Lochbänder lasst zu das mit mehrere Löcher von 4,2 Mm  man doch eine genaue klemmende 4 Mm Führung bekommt. Ist es gut justiert , dann werden die Lochbäner mit die beiden Mutter geklemmt.
Wenn man genau zuschaut sieht man hier vorne die Gewindestange (Loch1) , in der Mitte  die eigentliche Achslagerung in Loch 3,  und da hinten befindet sich wider eine Gewindestange die durch die Löcher 5 der Löchbander geht ).