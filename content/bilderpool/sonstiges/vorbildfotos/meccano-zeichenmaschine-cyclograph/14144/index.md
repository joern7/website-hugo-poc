---
layout: "image"
title: "Zeichengerät"
date: "2008-03-30T11:38:52"
picture: "meccanozeichenmaschine1.jpg"
weight: "1"
konstrukteure: 
- "unbekannter Holland Meccano Fan"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/14144
- /details27d5.html
imported:
- "2019"
_4images_image_id: "14144"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-03-30T11:38:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14144 -->
Obenansicht der Zeichenmaschine .  Der eigentliche Papiertisch befindet sich so etwas 80Zm hoch .  Der Antriebmotor sitzt ganz unten .