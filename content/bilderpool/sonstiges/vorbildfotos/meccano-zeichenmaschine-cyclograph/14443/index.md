---
layout: "image"
title: "Figuren ..."
date: "2008-05-03T15:20:29"
picture: "meccanozeichenmaschine10.jpg"
weight: "11"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland)"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/14443
- /detailsdcc7-2.html
imported:
- "2019"
_4images_image_id: "14443"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:20:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14443 -->
...