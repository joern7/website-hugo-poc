---
layout: "image"
title: "Andere Seite"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine13.jpg"
weight: "13"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31602
- /details5234.html
imported:
- "2019"
_4images_image_id: "31602"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31602 -->
