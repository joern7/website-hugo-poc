---
layout: "image"
title: "Schmalseite"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine16.jpg"
weight: "16"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31605
- /details227b-2.html
imported:
- "2019"
_4images_image_id: "31605"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31605 -->
Dies ist das der Dampfmaschine zugewandte Ende.