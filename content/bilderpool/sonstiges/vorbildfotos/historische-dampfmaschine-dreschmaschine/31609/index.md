---
layout: "image"
title: "Blick unter die Dreschmaschine"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine20.jpg"
weight: "20"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31609
- /details6af2.html
imported:
- "2019"
_4images_image_id: "31609"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31609 -->
