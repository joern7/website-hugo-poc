---
layout: "image"
title: "Details"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine05.jpg"
weight: "5"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31594
- /details9f53.html
imported:
- "2019"
_4images_image_id: "31594"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31594 -->
Hier kommt von rechts oben die besagte Stange an. Evtl. ist die auf der Schwungradachse exzentrisch gelagert und steuert durch ihre Bewegung hier Ventile für den Zylinder.