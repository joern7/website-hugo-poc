---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine10.jpg"
weight: "10"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31599
- /details124b.html
imported:
- "2019"
_4images_image_id: "31599"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31599 -->
Diese Teile vibrierten vermutlich an den federnden Eisenbändern, um das Dreschgut ins Innere zu befördern. Auf einem späteren Bild sieht man etwas vom Antrieb.