---
layout: "overview"
title: "Historische Dampfmaschine mit Dreschmaschine"
date: 2020-02-22T09:23:24+01:00
legacy_id:
- /php/categories/2354
- /categories7fb1.html
- /categoriesa14e.html
- /categories897a.html
- /categories0428.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2354 --> 
Im Urlaub waren wir im Heimatdorfmuseum in Tittling im bayerischen Wald. Da ist ein komplettes historisches Dorf aufgebaut, und in einer Scheune standen ein Prachtexemplar einer Dampfmaschine, die über einen Riemenantrieb eine mobile Dreschmaschine - einen Vorläufer des Mähdreschers - antrieb. Vielleicht mag's ja jemand nachbauen.