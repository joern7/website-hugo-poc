---
layout: "image"
title: "Ein Quad als Schneemobil 5/5"
date: "2011-02-20T21:26:45"
picture: "a5.jpg"
weight: "5"
konstrukteure: 
- "can-am"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/30100
- /details4b86.html
imported:
- "2019"
_4images_image_id: "30100"
_4images_cat_id: "2224"
_4images_user_id: "389"
_4images_image_date: "2011-02-20T21:26:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30100 -->
Das Quad von can-am ist mit einem Apache Track System ausgerüstet