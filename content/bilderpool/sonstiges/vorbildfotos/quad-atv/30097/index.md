---
layout: "image"
title: "Ein Quad als Schneemobil 2/5"
date: "2011-02-20T21:26:44"
picture: "a2.jpg"
weight: "2"
konstrukteure: 
- "can-am"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/30097
- /details865f.html
imported:
- "2019"
_4images_image_id: "30097"
_4images_cat_id: "2224"
_4images_user_id: "389"
_4images_image_date: "2011-02-20T21:26:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30097 -->
Das Quad von can-am ist mit einem Apache Track System ausgerüstet