---
layout: "image"
title: "Nachwuchs 3"
date: "2011-09-23T20:11:38"
picture: "nachwuchs3.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31911
- /detailsc2fa.html
imported:
- "2019"
_4images_image_id: "31911"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-23T20:11:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31911 -->
Geht denn diese Stebe auch ab ... Oder ist die fest ...