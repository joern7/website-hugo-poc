---
layout: "comment"
hidden: true
title: "13240"
date: "2011-01-18T21:03:15"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Na ja, Winkelträger 60 und 30 untrennbar verklebt? Falls diese Teile ansonsten o.k. sind, kann man sie wenigstens noch als Winkelträger 90 verwenden. Alternativ könnte man versuchen, die Teile (falls der Klebstoff unauflösbar ist) so zu trennen, daß wenigstens eines (i. d. R. das "wertvollere") das procedere heil übersteht. Versuchen würd ich's allemal.

Gruß, Thomas