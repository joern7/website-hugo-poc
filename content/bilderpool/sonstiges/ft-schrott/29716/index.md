---
layout: "image"
title: "FT Schrott"
date: "2011-01-17T21:34:58"
picture: "ftschrott1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29716
- /details9036.html
imported:
- "2019"
_4images_image_id: "29716"
_4images_cat_id: "2182"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29716 -->
Neulich konnte ich ca. 15 Kilo FT erwerben . Das Material kam aus einem Starkraucherhaushalt und war völlig ölig und versifft. In einem Bad mit Spülaschinenwaschpulver habe ich es wieder sauber und geruchsfrei bekommen. Die folgenden Fotos zeigen den Ausschuß.

Mit Lötkolben malträtierte gelbe Box.
