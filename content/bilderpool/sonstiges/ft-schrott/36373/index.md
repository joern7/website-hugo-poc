---
layout: "image"
title: "defekte Fischertechnikteile"
date: "2012-12-31T15:41:40"
picture: "Fischertechnikteile.jpg"
weight: "6"
konstrukteure: 
- "Ft Zerstörer"
fotografen:
- "Norbert"
keywords: ["defekt", "kaputt", "zerstört", "deformiert", "zerteilt", "abgenutzt"]
uploadBy: "gamma"
license: "unknown"
legacy_id:
- /php/details/36373
- /details61b5.html
imported:
- "2019"
_4images_image_id: "36373"
_4images_cat_id: "2182"
_4images_user_id: "1607"
_4images_image_date: "2012-12-31T15:41:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36373 -->
Diese Teile von Fischertechnik wurden durch meine intensive Bespielung leider zerstört. Die Grundbausteine bekamen Risse oder wurden komplett zerteilt. Die Zahnräder der Getriebeteile des großen Motors wurden so stark abgenützt bzw. deformiert dass sie nun nicht mehr einsatzfähig sind. Die abgenutze Schnecke des Motors habe ich nun mit einer Wasserpumenzange gewaltsam entfernt da man sie anscheinend einzeln nachbestellen kann. Die Schnecke des Minimotors von Cartech ist einfach abgebrochen. Andere Teile gingen verloren so dass ich nun keine funktionsfähigen Modelle mehr aufbauen kann.