---
layout: "image"
title: "ft:pedia Werbung"
date: "2011-04-12T07:39:55"
picture: "plakat1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30450
- /details27ef.html
imported:
- "2019"
_4images_image_id: "30450"
_4images_cat_id: "2267"
_4images_user_id: "1162"
_4images_image_date: "2011-04-12T07:39:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30450 -->
So, heute Morgen habe ich mal mal drangesetzt und ein Plakat entworfen, das man evtl. in Schule, Spielwarenläden, Hobbyläden,..... aufhängen könnte. Das Bild hier ist etwas schlecht, aber auf Wunsch kann ich eine bessere PDF-Datei des Plakates auf die ftc laden. Vielleicht fallen euch noch Verbesserungsvorschläge etc. ein? Ich würde mich über eine Rückmeldung egal ob positiv oder negativ freuen.

MfG
Endlich