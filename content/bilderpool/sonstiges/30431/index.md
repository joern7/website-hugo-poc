---
layout: "image"
title: "ft Plans on Barnes and Noble's nook"
date: "2011-04-08T00:27:03"
picture: "amelia_nook_a.jpg"
weight: "27"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["nook"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/30431
- /details1045.html
imported:
- "2019"
_4images_image_id: "30431"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2011-04-08T00:27:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30431 -->
I have been porting my ft construction plans to the nook ereader. Thought to share.