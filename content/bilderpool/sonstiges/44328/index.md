---
layout: "image"
title: "PWM_Speed_Encodermotor"
date: "2016-08-30T13:13:22"
picture: "Kurve.png"
weight: "36"
konstrukteure: 
- "Torsten"
fotografen:
- "Torsten"
keywords: ["Encodermotor", "pwm", "ftrobopy", "setspeed", "Umdrehungsgeschwindigkeit"]
uploadBy: "Torsten"
license: "unknown"
legacy_id:
- /php/details/44328
- /details3554.html
imported:
- "2019"
_4images_image_id: "44328"
_4images_cat_id: "312"
_4images_user_id: "2453"
_4images_image_date: "2016-08-30T13:13:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44328 -->
Zusammenhang zwischen ftrobopy.setSpeed() und der Motordrehzahl des neuen Encodermotors (in Umdrehungen/Sekunde). Es stellte sich die Frage, inwieweit der Zusammenhang linear ist. Ergebnis: im Bereich zwischen 100 und 310 ist die Kurve annähernd linear. Im Bereich zwischen 0 und 100 ist es kaum möglich die Motorgeschwindigkeit kontinuierlich linear zu regeln.