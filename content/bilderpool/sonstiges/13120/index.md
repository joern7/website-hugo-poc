---
layout: "image"
title: "ft tshirt (ft Princess 2)"
date: "2007-12-20T17:36:17"
picture: "tshirt_b.jpg"
weight: "3"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["tshirt", "ft", "princess"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13120
- /details9095.html
imported:
- "2019"
_4images_image_id: "13120"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2007-12-20T17:36:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13120 -->
This is the back of a t-shirt designed by Laura (and posted on Zazzle.com). It is being modelled by my daughter Amelia. She is a ft princess! (google translation-Dies ist die Rückseite ein T-Shirt entworfen von Laura (und auf Zazzle.com). Es wird nach dem Vorbild meiner Tochter Amelia. Sie ist eine m Prinzessin.)