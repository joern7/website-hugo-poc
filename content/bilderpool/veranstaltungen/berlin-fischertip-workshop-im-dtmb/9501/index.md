---
layout: "image"
title: "fischertip22.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9501
- /detailsd729.html
imported:
- "2019"
_4images_image_id: "9501"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9501 -->
