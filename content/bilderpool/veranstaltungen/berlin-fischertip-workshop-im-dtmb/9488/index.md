---
layout: "image"
title: "fischertip09.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9488
- /details4eb6.html
imported:
- "2019"
_4images_image_id: "9488"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9488 -->
