---
layout: "image"
title: "fischertip23.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9502
- /details1e09.html
imported:
- "2019"
_4images_image_id: "9502"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9502 -->
