---
layout: "image"
title: "kvgftmodellschau43.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau43.jpg"
weight: "43"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47807
- /details362e.html
imported:
- "2019"
_4images_image_id: "47807"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47807 -->
