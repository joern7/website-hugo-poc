---
layout: "image"
title: "kvgftmodellschau11.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47775
- /detailsa475.html
imported:
- "2019"
_4images_image_id: "47775"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47775 -->
