---
layout: "image"
title: "kvgftmodellschau09.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47773
- /details992e.html
imported:
- "2019"
_4images_image_id: "47773"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47773 -->
