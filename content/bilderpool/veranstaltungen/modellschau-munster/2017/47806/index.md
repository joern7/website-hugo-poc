---
layout: "image"
title: "kvgftmodellschau42.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau42.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47806
- /detailsba8f.html
imported:
- "2019"
_4images_image_id: "47806"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47806 -->
