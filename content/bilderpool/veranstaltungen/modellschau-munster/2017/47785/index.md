---
layout: "image"
title: "kvgftmodellschau21.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47785
- /details8654-2.html
imported:
- "2019"
_4images_image_id: "47785"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47785 -->
