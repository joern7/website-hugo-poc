---
layout: "image"
title: "kvgftmodellschau34.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau34.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47798
- /details8bf3.html
imported:
- "2019"
_4images_image_id: "47798"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47798 -->
