---
layout: "image"
title: "kvgftmodellschau68.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau68.jpg"
weight: "68"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47832
- /detailsef53-2.html
imported:
- "2019"
_4images_image_id: "47832"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47832 -->
