---
layout: "image"
title: "kvgftmodellschau54.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau54.jpg"
weight: "54"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47818
- /detailse1b2-2.html
imported:
- "2019"
_4images_image_id: "47818"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47818 -->
