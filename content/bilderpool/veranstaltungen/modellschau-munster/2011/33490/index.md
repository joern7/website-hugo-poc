---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau08.jpg"
weight: "8"
konstrukteure: 
- "Thomas"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33490
- /details55a4.html
imported:
- "2019"
_4images_image_id: "33490"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33490 -->
