---
layout: "image"
title: "Blick vom Schwebebahndepot auf die Strecke"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg22.jpg"
weight: "22"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/40360
- /details6ce2.html
imported:
- "2019"
_4images_image_id: "40360"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40360 -->
Blick vom Schwebebahndepot auf die Strecke