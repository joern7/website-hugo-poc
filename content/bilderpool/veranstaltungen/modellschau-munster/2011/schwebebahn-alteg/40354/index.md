---
layout: "image"
title: "Drehscheibe Verriegelung"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg16.jpg"
weight: "16"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/40354
- /details5c48.html
imported:
- "2019"
_4images_image_id: "40354"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40354 -->
Schienenübergang der Drehscheibe in der Endposition