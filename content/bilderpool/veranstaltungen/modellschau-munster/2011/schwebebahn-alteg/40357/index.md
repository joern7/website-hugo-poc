---
layout: "image"
title: "Stützenkonstruktion"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg19.jpg"
weight: "19"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/40357
- /detailsccb2-2.html
imported:
- "2019"
_4images_image_id: "40357"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40357 -->
Detailaufnahme der Stützenkonstruktion