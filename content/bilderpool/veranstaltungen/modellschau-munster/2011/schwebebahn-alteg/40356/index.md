---
layout: "image"
title: "Streckenabschnitt"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg18.jpg"
weight: "18"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/40356
- /details2674-2.html
imported:
- "2019"
_4images_image_id: "40356"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40356 -->
Streckenabschnitt mit Schwebebahnzug