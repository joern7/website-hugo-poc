---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau06.jpg"
weight: "6"
konstrukteure: 
- "Thomas"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33488
- /detailsf836.html
imported:
- "2019"
_4images_image_id: "33488"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33488 -->
