---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:17"
picture: "fischertechnikmodellschau22.jpg"
weight: "22"
konstrukteure: 
- "Andreas"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33504
- /details5b32.html
imported:
- "2019"
_4images_image_id: "33504"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33504 -->
