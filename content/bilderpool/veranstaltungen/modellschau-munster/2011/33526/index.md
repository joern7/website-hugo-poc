---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:17"
picture: "fischertechnikmodellschau44.jpg"
weight: "44"
konstrukteure: 
- "Dirk"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33526
- /detailsd8ad.html
imported:
- "2019"
_4images_image_id: "33526"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:17"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33526 -->
