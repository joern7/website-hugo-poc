---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau07.jpg"
weight: "7"
konstrukteure: 
- "Anselm und Jutta"
fotografen:
- "Fam. Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33489
- /detailscfbb.html
imported:
- "2019"
_4images_image_id: "33489"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33489 -->
