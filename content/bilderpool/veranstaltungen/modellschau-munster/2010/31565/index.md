---
layout: "image"
title: "Doppeldecker1"
date: "2011-08-08T22:20:10"
picture: "IMG_4276.JPG"
weight: "91"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31565
- /details1bdf.html
imported:
- "2019"
_4images_image_id: "31565"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:20:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31565 -->
Super schickes Teil. Den Trick mit den eingeklemmten S-Streben habe ich glatt in eins meiner Modelle übernommen.