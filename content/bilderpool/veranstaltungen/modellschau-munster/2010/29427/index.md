---
layout: "image"
title: "Brückenleger01"
date: "2010-12-05T19:55:58"
picture: "IMG_4266.JPG"
weight: "81"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29427
- /details8cb6.html
imported:
- "2019"
_4images_image_id: "29427"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2010-12-05T19:55:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29427 -->
Das wird mal ein gar mächtiges Teil! Allein die Kräfte, die zu bändigen sind, um die fertige Brücke zu heben...