---
layout: "image"
title: "Octopus"
date: "2010-11-20T22:16:01"
picture: "Octupus.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: ["Karussell"]
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29306
- /details193d.html
imported:
- "2019"
_4images_image_id: "29306"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:16:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29306 -->
