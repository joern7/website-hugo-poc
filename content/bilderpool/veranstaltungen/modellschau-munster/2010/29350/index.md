---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz31.jpg"
weight: "75"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29350
- /details6504-3.html
imported:
- "2019"
_4images_image_id: "29350"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29350 -->
