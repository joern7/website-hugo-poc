---
layout: "image"
title: "Brückenleger03"
date: "2010-12-05T20:05:24"
picture: "IMG_4268.JPG"
weight: "83"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29429
- /detailsffdd.html
imported:
- "2019"
_4images_image_id: "29429"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2010-12-05T20:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29429 -->
Wie im Original: 7 Laufrollen je Seite, und per Drehstab gefedert. Das schwarze mittendrin sind Freilaufnaben. Die Radaufhängung und zugleich Federung besteht aus Schweißdraht, den sich Dirk von einem Kumpel passend biegen ließ. Im Original gehen die Drehstäbe aber bis zur Gegenseite durch (hier nur bis zur Mitte), und deshalb sind beim Leo auch die Aufstandsflächen der Ketten (links/rechts) unterschiedlich angeordnet - die eine Seite liegt etwas weiter hinten auf als die andere.

Die Kette ist High-Tech pur. Jedes Glied hat eine ft-Kufe, die zwischen den Rädern 60 hindurch läuft und damit die Kette in der Spur hält. Insgesamt sieht man sehr schön, warum Kettenfahrzeuge auf Englisch auch "track-laying vehicles" heißen: sie legen also ein Stück Fahrbahn vor sich hin und fahren anschließend darüber hinweg. Was DER hier vor sich hin legt und hinter sich wieder einsammelt, ist wirklich eine Fahrbahn.