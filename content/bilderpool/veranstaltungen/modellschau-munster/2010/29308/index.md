---
layout: "image"
title: "Putzroboter"
date: "2010-11-20T22:29:06"
picture: "Putzroboter.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29308
- /details8c10-2.html
imported:
- "2019"
_4images_image_id: "29308"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:29:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29308 -->
