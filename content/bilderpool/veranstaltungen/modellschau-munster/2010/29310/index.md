---
layout: "image"
title: "Roboterarm01"
date: "2010-11-20T22:57:30"
picture: "Roboterarm01.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29310
- /details1bc9.html
imported:
- "2019"
_4images_image_id: "29310"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:57:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29310 -->
