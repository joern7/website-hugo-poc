---
layout: "image"
title: "Murmelwand"
date: "2010-11-20T22:57:30"
picture: "Rechenmaschinev.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29309
- /details3bb4.html
imported:
- "2019"
_4images_image_id: "29309"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:57:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29309 -->
