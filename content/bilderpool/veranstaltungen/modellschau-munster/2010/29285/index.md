---
layout: "image"
title: "Roboter"
date: "2010-11-17T20:40:34"
picture: "IMGP0167.jpg"
weight: "15"
konstrukteure: 
- "Frederik"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29285
- /detailsf2ed-2.html
imported:
- "2019"
_4images_image_id: "29285"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:40:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29285 -->
