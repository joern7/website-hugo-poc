---
layout: "image"
title: "Roboterarm02"
date: "2010-11-20T22:57:30"
picture: "Roboterarm02.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29311
- /details82ff.html
imported:
- "2019"
_4images_image_id: "29311"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:57:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29311 -->
