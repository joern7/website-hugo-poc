---
layout: "comment"
hidden: true
title: "12917"
date: "2010-12-11T14:38:46"
uploadBy:
- "robo-bac"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen

bei diesem Roboter handelt es sich um einen pick+place-Roboter (ähnlich der FlexPicker von ABB) mit sphärischem Arbeitsraum (Vorbild: Galielo Sphere, gesehen auf der Hannovermesse Industrie). Rotation Achse1 (in meinem Modell) ca. 340° (wenn die Kabellänge reicht); Achse2 schwenkt ca. +/-30°; Achsen 3+4 erledigen die Hubbewegung des Greifers, durch die Kombination aus Achsen3+4 kann der Greifer selbst noch aus einer Horizontal- in eine Vertikallage geschwenkt werden.

Gruß, Claus