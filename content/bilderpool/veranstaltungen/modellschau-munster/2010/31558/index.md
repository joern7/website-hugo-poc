---
layout: "image"
title: "Leiter2"
date: "2011-08-08T22:04:01"
picture: "IMG_4229_Leiter.JPG"
weight: "90"
konstrukteure: 
- "Ludger"
fotografen:
- "Harald Steinhaus"
keywords: ["Leiter", "130925"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31558
- /detailsd98a.html
imported:
- "2019"
_4images_image_id: "31558"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:04:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31558 -->
Man achte auf die Haltemethode mit BS7,5 und Achsen.