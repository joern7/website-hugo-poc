---
layout: "image"
title: "Panzer"
date: "2010-11-20T22:17:22"
picture: "Panzer.jpg"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29307
- /detailsf304.html
imported:
- "2019"
_4images_image_id: "29307"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:17:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29307 -->
