---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz21.jpg"
weight: "65"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29340
- /details5086.html
imported:
- "2019"
_4images_image_id: "29340"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29340 -->
Modelle von Brickwedde