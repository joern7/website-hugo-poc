---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:44"
picture: "hbz02.jpg"
weight: "48"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29321
- /details76fe.html
imported:
- "2019"
_4images_image_id: "29321"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29321 -->
Modelle von Andreas und Tobias Tacke