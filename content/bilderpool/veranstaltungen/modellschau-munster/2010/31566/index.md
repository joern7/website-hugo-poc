---
layout: "image"
title: "Doppeldecker2"
date: "2011-08-08T22:20:48"
picture: "IMG_4277.JPG"
weight: "92"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31566
- /detailsce57.html
imported:
- "2019"
_4images_image_id: "31566"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:20:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31566 -->
