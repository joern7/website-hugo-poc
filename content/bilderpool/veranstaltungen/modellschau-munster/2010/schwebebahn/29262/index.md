---
layout: "image"
title: "Übersicht der gesamten Strecke"
date: "2010-11-15T17:29:11"
picture: "schwebebahn01_2.jpg"
weight: "4"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29262
- /details1582.html
imported:
- "2019"
_4images_image_id: "29262"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29262 -->
