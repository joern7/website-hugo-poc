---
layout: "image"
title: "2. Haltestelle (3)"
date: "2010-11-15T17:29:12"
picture: "schwebebahn06_2.jpg"
weight: "8"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29266
- /details95b0.html
imported:
- "2019"
_4images_image_id: "29266"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29266 -->
Der blaue Zug verlässt die Haltestelle.