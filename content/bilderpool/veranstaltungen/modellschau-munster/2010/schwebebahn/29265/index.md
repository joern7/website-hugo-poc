---
layout: "image"
title: "2. Haltestelle (2)"
date: "2010-11-15T17:29:12"
picture: "schwebebahn05_2.jpg"
weight: "7"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29265
- /detailsfc59.html
imported:
- "2019"
_4images_image_id: "29265"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29265 -->
Der blaue Zug fährt in die Haltestelle ein.