---
layout: "image"
title: "Roter Zug auf der Stecke"
date: "2010-11-15T17:29:12"
picture: "schwebebahn08_2.jpg"
weight: "10"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29268
- /details39a4-2.html
imported:
- "2019"
_4images_image_id: "29268"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29268 -->
