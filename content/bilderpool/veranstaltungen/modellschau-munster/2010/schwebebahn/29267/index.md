---
layout: "image"
title: "2. Haltestelle schräg von oben"
date: "2010-11-15T17:29:12"
picture: "schwebebahn07_2.jpg"
weight: "9"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29267
- /detailsc0c8.html
imported:
- "2019"
_4images_image_id: "29267"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29267 -->
Der blaue Zug verlässt gerade die Haltestelle.