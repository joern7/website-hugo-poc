---
layout: "image"
title: "1. Haltestelle mit blauem Zug"
date: "2010-11-15T17:29:12"
picture: "schwebebahn02_2.jpg"
weight: "5"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29263
- /detailsad80.html
imported:
- "2019"
_4images_image_id: "29263"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29263 -->
