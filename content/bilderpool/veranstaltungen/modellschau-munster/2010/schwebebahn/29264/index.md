---
layout: "image"
title: "2. Haltestelle (1)"
date: "2010-11-15T17:29:12"
picture: "schwebebahn04_2.jpg"
weight: "6"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29264
- /details87f2-2.html
imported:
- "2019"
_4images_image_id: "29264"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29264 -->
Der rote Zug ist gerade abgefahren