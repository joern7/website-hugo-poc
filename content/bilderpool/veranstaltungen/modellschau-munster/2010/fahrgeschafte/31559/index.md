---
layout: "image"
title: "Music Express 1"
date: "2011-08-08T22:06:24"
picture: "IMG_4243.JPG"
weight: "2"
konstrukteure: 
- "Triceratops"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrgeschäft"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31559
- /detailsef06-2.html
imported:
- "2019"
_4images_image_id: "31559"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:06:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31559 -->
Tschuldigung: das waren meine Wenigkeit und mein neuer Fotoapparat im ersten Einsatz - da sind die meisten Bilder nichts gescheites geworden.