---
layout: "image"
title: "Star Flyer 2"
date: "2011-08-08T22:17:04"
picture: "IMG_4255.JPG"
weight: "7"
konstrukteure: 
- "Die Brickweddes"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31564
- /details21c3.html
imported:
- "2019"
_4images_image_id: "31564"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:17:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31564 -->
Hier war die Devise klar: Klotzen,nicht kleckern.