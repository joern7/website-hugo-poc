---
layout: "image"
title: "Schiffsschaukel"
date: "2011-08-08T21:53:35"
picture: "IMG_4215.JPG"
weight: "1"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31551
- /details2278.html
imported:
- "2019"
_4images_image_id: "31551"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31551 -->
