---
layout: "image"
title: "Star Flyer 1"
date: "2011-08-08T22:15:35"
picture: "IMG_4254.JPG"
weight: "6"
konstrukteure: 
- "Die Brickweddes"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31563
- /details7be4-2.html
imported:
- "2019"
_4images_image_id: "31563"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31563 -->
Der Star Flyer ist ein Kettenkarussel, das an einem Turm auf- und ab fährt.

Das Bild ist leider verwaschen, wir üben das noch mit der Schärfe.