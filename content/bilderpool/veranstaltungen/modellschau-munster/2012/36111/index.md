---
layout: "image"
title: "Kugelbahn"
date: "2012-11-20T21:40:42"
picture: "hbz02.jpg"
weight: "2"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36111
- /details6257.html
imported:
- "2019"
_4images_image_id: "36111"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36111 -->
