---
layout: "image"
title: "Riesenrad"
date: "2012-11-20T21:40:43"
picture: "hbz25.jpg"
weight: "25"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36134
- /detailsa034.html
imported:
- "2019"
_4images_image_id: "36134"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36134 -->
