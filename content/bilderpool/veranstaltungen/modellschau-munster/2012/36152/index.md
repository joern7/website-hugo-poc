---
layout: "image"
title: "Kugelbahn"
date: "2012-11-20T21:40:43"
picture: "hbz43.jpg"
weight: "43"
konstrukteure: 
- "Marlon Wünnemann"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36152
- /detailse27e-2.html
imported:
- "2019"
_4images_image_id: "36152"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36152 -->
