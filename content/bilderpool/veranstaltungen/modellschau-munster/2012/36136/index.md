---
layout: "image"
title: "Drucker"
date: "2012-11-20T21:40:43"
picture: "hbz27.jpg"
weight: "27"
konstrukteure: 
- "Maximilian Ammenwerth"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36136
- /details47a8.html
imported:
- "2019"
_4images_image_id: "36136"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36136 -->
