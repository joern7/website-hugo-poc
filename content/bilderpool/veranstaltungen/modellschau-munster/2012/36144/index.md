---
layout: "image"
title: "Kugelbahn"
date: "2012-11-20T21:40:43"
picture: "hbz35.jpg"
weight: "35"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36144
- /details5c4b.html
imported:
- "2019"
_4images_image_id: "36144"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36144 -->
