---
layout: "image"
title: "Bastelecke"
date: "2012-11-20T21:40:43"
picture: "hbz56.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36165
- /details85fe.html
imported:
- "2019"
_4images_image_id: "36165"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36165 -->
