---
layout: "image"
title: "Übersicht"
date: "2012-11-20T21:40:43"
picture: "hbz24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36133
- /details66e2.html
imported:
- "2019"
_4images_image_id: "36133"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36133 -->
