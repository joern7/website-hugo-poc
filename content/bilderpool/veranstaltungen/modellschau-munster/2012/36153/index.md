---
layout: "image"
title: "Roboter"
date: "2012-11-20T21:40:43"
picture: "hbz44.jpg"
weight: "44"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36153
- /detailsaaa5-2.html
imported:
- "2019"
_4images_image_id: "36153"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36153 -->
