---
layout: "image"
title: "Riesenrad beim Aufbau"
date: "2012-11-20T21:40:42"
picture: "hbz04.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36113
- /details35ac.html
imported:
- "2019"
_4images_image_id: "36113"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36113 -->
