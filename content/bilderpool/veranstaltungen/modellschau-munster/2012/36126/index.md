---
layout: "image"
title: "KVG Hiltrup"
date: "2012-11-20T21:40:42"
picture: "hbz17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36126
- /details2b4d.html
imported:
- "2019"
_4images_image_id: "36126"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36126 -->
