---
layout: "image"
title: "SVZ Osnabrück"
date: "2012-11-20T21:40:42"
picture: "hbz20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36129
- /detailsa627.html
imported:
- "2019"
_4images_image_id: "36129"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36129 -->
