---
layout: "image"
title: "Spielecke"
date: "2012-11-20T21:40:43"
picture: "hbz40.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36149
- /details1c7b-3.html
imported:
- "2019"
_4images_image_id: "36149"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36149 -->
