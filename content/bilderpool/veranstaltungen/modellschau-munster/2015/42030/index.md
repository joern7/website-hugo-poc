---
layout: "image"
title: "Flyer zur Modellschau 2015 im HBZ Münster"
date: "2015-10-03T10:40:00"
picture: "flyer1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/42030
- /details68c8.html
imported:
- "2019"
_4images_image_id: "42030"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2015-10-03T10:40:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42030 -->
Hier der Flyer zu unserer Modellschau am 22.11.2015 im HBZ Münster.
Im Foyer des Gebäudes "D" werden in der Zeit von 10 bis 17 Uhr wie üblich eine Vielzahl von Modellen gezeigt.
Ort:
Handwerkskammer
Bildungszentrum
Echelmeyerstraße 1-2
48163 Münster
