---
layout: "image"
title: "fischertechnik Industrie Trainingsmodelle Heft"
date: "2015-11-28T11:42:24"
picture: "muenster08.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42405
- /details7d81.html
imported:
- "2019"
_4images_image_id: "42405"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42405 -->
Bearbeitungszentrum. Heft von Dirk Kutsch