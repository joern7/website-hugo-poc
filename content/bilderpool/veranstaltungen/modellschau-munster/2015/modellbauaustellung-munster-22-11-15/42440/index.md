---
layout: "image"
title: "Bearbeitungszentrum"
date: "2015-11-28T11:42:24"
picture: "muenster43.jpg"
weight: "43"
konstrukteure: 
- "Heinrich Fuchs"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42440
- /details5712-2.html
imported:
- "2019"
_4images_image_id: "42440"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42440 -->
