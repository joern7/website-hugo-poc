---
layout: "image"
title: "Wiener Riesenrad Konstruktion"
date: "2015-11-28T11:42:24"
picture: "muenster61.jpg"
weight: "61"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42458
- /detailsb7b1.html
imported:
- "2019"
_4images_image_id: "42458"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42458 -->
