---
layout: "image"
title: "Glücksspielautomat"
date: "2015-11-28T11:42:24"
picture: "muenster25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42422
- /details3eb1-3.html
imported:
- "2019"
_4images_image_id: "42422"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42422 -->
