---
layout: "image"
title: "6-Achs Roboterarm Turm"
date: "2015-11-28T11:42:24"
picture: "muenster48.jpg"
weight: "48"
konstrukteure: 
- "Frank Linde"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42445
- /detailsc7c7.html
imported:
- "2019"
_4images_image_id: "42445"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42445 -->
