---
layout: "image"
title: "fischertechnik Industrie Trainingsmodelle Heft"
date: "2015-11-28T11:42:24"
picture: "muenster09.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42406
- /details55d1.html
imported:
- "2019"
_4images_image_id: "42406"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42406 -->
Magazin. Heft von Dirk Kutsch