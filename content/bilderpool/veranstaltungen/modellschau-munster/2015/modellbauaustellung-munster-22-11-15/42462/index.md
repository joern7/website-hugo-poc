---
layout: "image"
title: "Robo TXT"
date: "2015-11-28T11:42:24"
picture: "muenster65.jpg"
weight: "65"
konstrukteure: 
- "Lutz Becke"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42462
- /details8fbb.html
imported:
- "2019"
_4images_image_id: "42462"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42462 -->
