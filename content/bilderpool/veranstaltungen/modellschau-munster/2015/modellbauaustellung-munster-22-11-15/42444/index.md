---
layout: "image"
title: "6-Achs Roboterarm Turm"
date: "2015-11-28T11:42:24"
picture: "muenster47.jpg"
weight: "47"
konstrukteure: 
- "Frank Linde"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42444
- /details929d-2.html
imported:
- "2019"
_4images_image_id: "42444"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42444 -->
