---
layout: "image"
title: "fischertechnik Industrie Trainingsmodelle Heft"
date: "2015-11-28T11:42:24"
picture: "muenster11.jpg"
weight: "11"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42408
- /detailsb6e0-2.html
imported:
- "2019"
_4images_image_id: "42408"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42408 -->
Hochregallager. Heft von Dirk Kutsch