---
layout: "image"
title: "Schopf Bergbau Radlader"
date: "2015-11-28T11:42:24"
picture: "muenster05.jpg"
weight: "5"
konstrukteure: 
- "Familie Busch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42402
- /details7bef-2.html
imported:
- "2019"
_4images_image_id: "42402"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42402 -->
