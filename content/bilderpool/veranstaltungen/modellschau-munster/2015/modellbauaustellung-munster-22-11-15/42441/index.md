---
layout: "image"
title: "Bearbeitungszentrum"
date: "2015-11-28T11:42:24"
picture: "muenster44.jpg"
weight: "44"
konstrukteure: 
- "Heinrich Fuchs"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42441
- /detailsf029.html
imported:
- "2019"
_4images_image_id: "42441"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42441 -->
