---
layout: "comment"
hidden: true
title: "21607"
date: "2016-01-27T21:26:16"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Cooles Bild. Es erinnert mich irgendwie an Mini-Steck, wo man aus kleinen bunten Fizzelteilen Bilder zusammensteckt.
LG Martin