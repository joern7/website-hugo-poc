---
layout: "image"
title: "Brockhaus"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44783
- /detailscc1c.html
imported:
- "2019"
_4images_image_id: "44783"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44783 -->
