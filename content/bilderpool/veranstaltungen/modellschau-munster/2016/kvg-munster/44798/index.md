---
layout: "image"
title: "Kardinal-von-Galen Gymnasium"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44798
- /details495e-2.html
imported:
- "2019"
_4images_image_id: "44798"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44798 -->
