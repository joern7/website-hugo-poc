---
layout: "image"
title: "van Baal"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster48.jpg"
weight: "48"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44824
- /details94be.html
imported:
- "2019"
_4images_image_id: "44824"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44824 -->
