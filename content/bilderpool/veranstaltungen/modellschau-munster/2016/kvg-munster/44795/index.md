---
layout: "image"
title: "Kardinal-von-Galen Gymnasium"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44795
- /details7229-2.html
imported:
- "2019"
_4images_image_id: "44795"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44795 -->
