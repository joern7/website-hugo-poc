---
layout: "image"
title: "Kardinal-von-Galen Gymnasium"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44793
- /details39d7-2.html
imported:
- "2019"
_4images_image_id: "44793"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44793 -->
