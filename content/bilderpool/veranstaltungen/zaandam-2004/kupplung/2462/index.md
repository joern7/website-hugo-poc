---
layout: "image"
title: "ZAANDAM_17"
date: "2004-05-31T19:50:20"
picture: "ZAANDAM_17.jpg"
weight: "1"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Andries Tieleman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2462
- /details3204-2.html
imported:
- "2019"
_4images_image_id: "2462"
_4images_cat_id: "589"
_4images_user_id: "144"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2462 -->
Teil der zentrifugalkopflung von der 4 gang getriebe von Max Buiting.