---
layout: "image"
title: "Soccer-Bot Robocup Junior 2015"
date: "2015-03-23T16:30:51"
picture: "SP0_1277-1.jpg"
weight: "1"
konstrukteure: 
- "andreas Kempf"
fotografen:
- "andreas Kempf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "andi1965"
license: "unknown"
legacy_id:
- /php/details/40681
- /details7d4c.html
imported:
- "2019"
_4images_image_id: "40681"
_4images_cat_id: "3056"
_4images_user_id: "2319"
_4images_image_date: "2015-03-23T16:30:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40681 -->
Mit diesen Robotern haben wir,  vom Scheffelgymnasium in Lahr, erfolgreich beim Robocup Junior im Bereich Soccer teilgenommen und uns für die German Open in Magdeburg qualifiziert.
Nähere Infos gerne unter kdu@gmx.de