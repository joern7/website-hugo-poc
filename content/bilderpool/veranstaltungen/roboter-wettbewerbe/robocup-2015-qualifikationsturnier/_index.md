---
layout: "overview"
title: "RoboCup 2015 - Qualifikationsturnier Mannheim"
date: 2020-02-22T09:08:52+01:00
legacy_id:
- /php/categories/3036
- /categoriesd6e9.html
- /categories937b.html
- /categories6e55.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3036 --> 
Im Februar 2015 nahmen zwei Teams der fischertechnik-AG des Bismarck-Gymnasiums Karlsruhe am RoboCup-Qualifikationsturnier in Mannheim teil.
Zwar konnte sich keines für den Deutschland-Cup in Magdeburg qualifizieren - beide erreichten aber respektable Punktzahlen in ihren Wettläufen.