---
layout: "image"
title: "Programmierung des Roboters"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40519
- /details55ab.html
imported:
- "2019"
_4images_image_id: "40519"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40519 -->
Letzte Fehlersuche und Programmoptimierung vor dem zweiten Wettlauf (Team RobCross)