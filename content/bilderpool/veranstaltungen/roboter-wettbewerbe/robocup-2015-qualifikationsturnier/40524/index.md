---
layout: "image"
title: "Roboter-Optimierungen"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40524
- /detailsd1c9.html
imported:
- "2019"
_4images_image_id: "40524"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40524 -->
Komplettzerlegung und Detailoptimierungen... (Team RoBoss)