---
layout: "comment"
hidden: true
title: "20164"
date: "2015-02-11T11:19:37"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich finde das unglaublich, was Jugendliche in diesem Alter heutzutage zustande bringen. Das hätte mal in meiner Kindheit jemand von mir verlangen sollen. Meinen größten Respekt vor sämtlichen Teilnehmern, völlig unabhängig von der Platzierung untereinander. Da überhaupt mitzumischen ist schon eine Auszeichnung. Also: Unbedingt weitermachen!

Viele Grüße,
Stefan