---
layout: "image"
title: "Boise Bot Competion 2010"
date: "2010-10-10T12:29:54"
picture: "sm_bot1.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "Bot", "Competion", "2010", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28965
- /detailsa657.html
imported:
- "2019"
_4images_image_id: "28965"
_4images_cat_id: "2105"
_4images_user_id: "585"
_4images_image_date: "2010-10-10T12:29:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28965 -->
These are images of Amelia and I entry into the Boise Bot Competion 2010. We built several ft bots that use the PCS BRAIN.