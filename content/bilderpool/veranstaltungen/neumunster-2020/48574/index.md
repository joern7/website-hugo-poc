---
layout: "image"
title: "Autoscooter 5"
date: 2020-04-15T09:00:38+02:00
picture: "NeumünsterAutoscooter05.jpg"
weight: "7"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der glänzende Autoscooter mit großer ftc