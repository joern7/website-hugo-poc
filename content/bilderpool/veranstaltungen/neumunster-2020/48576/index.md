---
layout: "image"
title: "Autoscooter 3"
date: 2020-04-15T09:00:40+02:00
picture: "NeumünsterAutoscooter03.jpg"
weight: "5"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Benutzte Fahrbahn