---
layout: "image"
title: "Autoscooter 4"
date: 2020-04-15T09:00:39+02:00
picture: "NeumünsterAutoscooter04.jpg"
weight: "6"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Scheinwerfer sind blendend