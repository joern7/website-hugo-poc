---
layout: "image"
title: "Autoscooter 7"
date: 2020-04-15T09:00:35+02:00
picture: "NeumünsterAutoscooter07.jpg"
weight: "9"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Figürchen inkognito, wenn man sie nicht am Helm erkennt