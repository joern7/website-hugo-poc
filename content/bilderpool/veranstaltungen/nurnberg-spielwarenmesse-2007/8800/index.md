---
layout: "image"
title: "FischerTipp 3"
date: "2007-02-03T12:02:40"
picture: "toyfair12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8800
- /detailsec1a.html
imported:
- "2019"
_4images_image_id: "8800"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T12:02:40"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8800 -->
