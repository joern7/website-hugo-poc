---
layout: "image"
title: "Beispielmodelle"
date: "2007-02-03T00:28:46"
picture: "toyfair04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8792
- /details9e6b.html
imported:
- "2019"
_4images_image_id: "8792"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8792 -->
Eine Wand mit einigen Beispielmodellen