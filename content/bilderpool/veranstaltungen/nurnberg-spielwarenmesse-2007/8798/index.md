---
layout: "image"
title: "FischerTipp 1"
date: "2007-02-03T00:28:46"
picture: "toyfair10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8798
- /details0c1e-2.html
imported:
- "2019"
_4images_image_id: "8798"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8798 -->
Modelle von Fischertipp.
War für was anderes, nur der Vollständigkeit halber dabei.