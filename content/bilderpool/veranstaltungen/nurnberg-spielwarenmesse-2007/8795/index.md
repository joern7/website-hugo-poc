---
layout: "image"
title: "Die Teststrecke 1"
date: "2007-02-03T00:28:46"
picture: "toyfair07.jpg"
weight: "7"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8795
- /details124a.html
imported:
- "2019"
_4images_image_id: "8795"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8795 -->
Im Vordergrund sieht man eine Station, wo mit den Funkmodulen demonstriert wird, dass beide Teile sowohl empfangen als auch senden können. Die Lichter leuten entsprechend auf, wenn der Robo auf den entsprechenden Farbtafeln steht.

Eingebaut war in dem Robo die "Vollausstattung", d.h. Temperatur, Helligkeit, Spurensucher, Abstand und Farbsensor.

Hoffe ich hab nichts vergessen.