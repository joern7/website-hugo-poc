---
layout: "image"
title: "bild14.jpg"
date: "2006-12-10T21:26:21"
picture: "bild14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/7849
- /details0e90.html
imported:
- "2019"
_4images_image_id: "7849"
_4images_cat_id: "737"
_4images_user_id: "1"
_4images_image_date: "2006-12-10T21:26:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7849 -->
