---
layout: "image"
title: "bild20.jpg"
date: "2006-12-10T21:26:21"
picture: "bild20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/7855
- /details5fe7.html
imported:
- "2019"
_4images_image_id: "7855"
_4images_cat_id: "737"
_4images_user_id: "1"
_4images_image_date: "2006-12-10T21:26:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7855 -->
