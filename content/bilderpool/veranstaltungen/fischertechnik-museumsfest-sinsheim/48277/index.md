---
layout: "image"
title: "3D-Scanner (4)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim29.jpg"
weight: "29"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48277
- /detailsa630.html
imported:
- "2019"
_4images_image_id: "48277"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48277 -->
