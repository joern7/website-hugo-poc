---
layout: "image"
title: "Funktionsmodelle zur Schrägseilbrücke - Seillängenausgleich"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim38.jpg"
weight: "38"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48286
- /detailsfa23.html
imported:
- "2019"
_4images_image_id: "48286"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48286 -->
Wenn man auf einer Spule auf- und auf einer anderen gleichzeitig abwickelt (per Drehung an der Kurbel hinten nämlich), die beiden Spulen aber unterschiedlich voll sind, muss man die Gesamt-Seillänge ausgleichen - durch Drehen am Z30 des Gleichlaufgetriebes.