---
layout: "image"
title: "nanoFramework-Uhr"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim54.jpg"
weight: "54"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48302
- /detailsef55-2.html
imported:
- "2019"
_4images_image_id: "48302"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48302 -->
Die hier: https://www.ftcommunity.de/categories.php?cat_id=3515