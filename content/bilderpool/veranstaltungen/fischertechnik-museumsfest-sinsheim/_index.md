---
layout: "overview"
title: "fischertechnik Museumsfest Sinsheim"
date: 2020-02-22T09:11:26+01:00
legacy_id:
- /php/categories/3541
- /categories6069-2.html
- /categories5a74.html
- /categoriesbb72.html
- /categories4dbf.html
- /categories0b10.html
- /categoriesa013.html
- /categories433c.html
- /categories05be.html
- /categoriesb3de.html
- /categoriescaf2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3541 --> 
Am Samstag, 20.10.2018 fand von 10:00 - 17:00 eine fischertechnik-Ausstellung im Erlebnismuseum Fördertechnik (direkt neben dem Technikmuseum) Sinsheim statt. Die jeweiligen Konstrukteure mögen bitte ihre Namen eintragen in den Kommentaren Beschreibungen hinzufügen - Danke!