---
layout: "image"
title: "Funktionsmodelle zur Schrägseilbrücke"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim37.jpg"
weight: "37"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48285
- /detailsb4ba.html
imported:
- "2019"
_4images_image_id: "48285"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48285 -->
Die machen einige technische Details anschaulich.