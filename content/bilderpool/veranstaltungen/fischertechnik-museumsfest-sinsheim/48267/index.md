---
layout: "image"
title: "Quadrokopter - von der Seite"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim19.jpg"
weight: "19"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48267
- /details616a.html
imported:
- "2019"
_4images_image_id: "48267"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48267 -->
