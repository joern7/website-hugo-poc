---
layout: "image"
title: "Urlaubskasten mit Urlaubskasten-Modell"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim52.jpg"
weight: "52"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48300
- /details1738.html
imported:
- "2019"
_4images_image_id: "48300"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48300 -->
Das Urlaubskasten-Modell wird ausführlich in der nächsten ft:pedia beschrieben. Hinten links das kleine Stoßstangen-Auto.