---
layout: "image"
title: "Schrägseilbrücke - Steuerkonsole"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim46.jpg"
weight: "46"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48294
- /details2431.html
imported:
- "2019"
_4images_image_id: "48294"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48294 -->
