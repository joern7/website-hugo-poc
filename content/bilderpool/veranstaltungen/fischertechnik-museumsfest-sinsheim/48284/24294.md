---
layout: "comment"
hidden: true
title: "24294"
date: "2018-11-07T08:45:42"
uploadBy:
- "ThomasW"
license: "unknown"
imported:
- "2019"
---
Der Energiebedarf selber ist deutlich unter 100 Watt, aber es sind 9 Motoren verbaut, und bei einigen muss die Geschwindigkeit ziemlich genau passen.
Im Einsatz sind 5 Powermotoren 50:1 (rote Kappe), 2 rote Encodermotoren und 2 S-Motoren.