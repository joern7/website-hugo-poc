---
layout: "image"
title: "Große Kugelbahn"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim36.jpg"
weight: "36"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48284
- /details8566.html
imported:
- "2019"
_4images_image_id: "48284"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48284 -->
