---
layout: "image"
title: "R2D2"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim08.jpg"
weight: "8"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48256
- /details20a4.html
imported:
- "2019"
_4images_image_id: "48256"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48256 -->
