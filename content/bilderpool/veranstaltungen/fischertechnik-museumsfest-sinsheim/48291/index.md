---
layout: "image"
title: "Schrägseilbrücke - Steuerungszentrum"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim43.jpg"
weight: "43"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48291
- /detailsdcc6.html
imported:
- "2019"
_4images_image_id: "48291"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48291 -->
Die Haube kann geöffnet werden. Nochmal unzählige raffinierte Details.