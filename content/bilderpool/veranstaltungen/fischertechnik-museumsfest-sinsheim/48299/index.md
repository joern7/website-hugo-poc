---
layout: "image"
title: "Industriemodell"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim51.jpg"
weight: "51"
konstrukteure: 
- "Torsten Dechert"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48299
- /details8ad9-2.html
imported:
- "2019"
_4images_image_id: "48299"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48299 -->
Links wird ein Tauchbad simuliert, durch dass die Teile (Vorstuferäder auf Achsen) laufen.