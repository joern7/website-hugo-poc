---
layout: "image"
title: "Achterbahn - Station"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim03.jpg"
weight: "3"
konstrukteure: 
- "Alexander Salameh (EMFT)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48251
- /detailsef79.html
imported:
- "2019"
_4images_image_id: "48251"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48251 -->
In der Station läuft eine Kette mit Magneten darauf, die die Wagen zieht.