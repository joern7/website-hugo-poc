---
layout: "image"
title: "Schrägseilbrücke - Steuerungszentrum"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim44.jpg"
weight: "44"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48292
- /details23a1.html
imported:
- "2019"
_4images_image_id: "48292"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48292 -->
Detail: Der fischerinformic-Taster im Vordergrund (großer Taster mit schwarzem Knopf und roter Seitenfläche) ist ähnlich wie die heutigen Minitaster durchgehen und betätigt also auch den darunter liegenden normalen großen Taster.