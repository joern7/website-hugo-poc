---
layout: "image"
title: "Fußballspiel (2)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim23.jpg"
weight: "23"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48271
- /detailsf1e9.html
imported:
- "2019"
_4images_image_id: "48271"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48271 -->
Hier wird gerade gegeneinander gespielt - ferngesteuert.