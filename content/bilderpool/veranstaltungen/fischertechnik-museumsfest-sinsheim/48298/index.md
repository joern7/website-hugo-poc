---
layout: "image"
title: "Hochregallager (3)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim50.jpg"
weight: "50"
konstrukteure: 
- "David Holtz"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48298
- /details2135.html
imported:
- "2019"
_4images_image_id: "48298"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48298 -->
