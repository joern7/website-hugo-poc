---
layout: "image"
title: "3D-Scanner (3)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim28.jpg"
weight: "28"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48276
- /detailsd554.html
imported:
- "2019"
_4images_image_id: "48276"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48276 -->
Im Vordergrund der Kamera sieht man einen Strich-Laser, dessen Linie durch den Schlitz zwischen den schwarzen Statikträgern 30 nochmal schärfer wird. Auf der anderen Seite sieht man eine helle ft-LED mit zwei ft-Linsen und ebenfalls einem Schlitz, mit dem das Abtasten ebenfalls klappt.