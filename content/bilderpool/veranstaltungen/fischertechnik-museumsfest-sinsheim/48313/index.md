---
layout: "image"
title: "Kettenschaufelbagger"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos14.jpg"
weight: "65"
konstrukteure: 
- "unbekannt"
fotografen:
- "Erlebnismuseum Fördertechnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48313
- /details0dc1-2.html
imported:
- "2019"
_4images_image_id: "48313"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48313 -->
von fischertechnik gesponsort