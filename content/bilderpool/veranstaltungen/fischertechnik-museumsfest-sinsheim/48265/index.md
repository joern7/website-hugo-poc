---
layout: "image"
title: "Quadrokopter - Rahmen"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim17.jpg"
weight: "17"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48265
- /details1652.html
imported:
- "2019"
_4images_image_id: "48265"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48265 -->
