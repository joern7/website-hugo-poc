---
layout: "image"
title: "Energieerhaltung"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim11.jpg"
weight: "11"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48259
- /detailsa73c.html
imported:
- "2019"
_4images_image_id: "48259"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48259 -->
Der rote Hebel rechts wird gedrückt und gibt die vier Kugeln frei. Sie kommen, bedingt durch die unterschiedliche Bahnführung, zwar zu unterschiedlichen Zeitpunkten, aber mit etwa derselben Geschwindigkeit (bis auf Einflüsse durch Rotation und Reibung) bei den Glockenstäben an.