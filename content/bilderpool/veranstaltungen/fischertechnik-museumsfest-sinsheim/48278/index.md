---
layout: "image"
title: "Autofocus-Kamera"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim30.jpg"
weight: "30"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48278
- /detailsc732.html
imported:
- "2019"
_4images_image_id: "48278"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48278 -->
Das neue Zahnrad für die Optikverstellung ist mit einem ft-Drucker gedruckt und also sozusagen ein Original-ft-Teil ;-)