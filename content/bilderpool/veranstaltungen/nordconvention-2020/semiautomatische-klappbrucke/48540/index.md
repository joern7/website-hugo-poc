---
layout: "image"
title: "Klappbrücke Gesamtansicht"
date: 2020-03-31T17:18:54+02:00
picture: "Gesamt.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Klappbrücke basiert auf dem Prinzip einer einspurigen, wechselseitigen Verkehrsführung.
Je Fahrtrichtung besteht für eine festgelegte Dauer eine Grünphase mit einer kurzzeitigen
Rotphase für beide Richtungen. So wird eine angenommene Verkehrssituation weitestgehend
real dargestellt.

Grün 1. Seite - Rot beide (Brücke räumen) - Grün 2. Seite - Rot beide (Brücke räumen) - usw.

Nach 15 Zyklen wechselt die Steuerung in den Modus Brückenhub. In dieser Phase gelten für
beide Fahrtrichtungen dauerhaft Rot. Die pneumatische Einheit wird aktiviert, die Schranken
schließen sich. Und nach einer kurzen Wartezeit setzt sich der Brückenhub in Bewegung. Auch
die Dauer des Brückenhubs ist zeitlich über die Steuerung definiert.

Nachdem die Brücke wieder vollständig geschlossen ist, öffnen sich die Schranken, und eine
Fahrtrichtung erhält Grün; die pneumatische Einheit wird deaktiviert. Der Brückenhub kann
jedoch jederzeit manuell über die separate Ruftaste auf der Zustandsanzeige (roter Kasten)
angefordert werden.

Die zentrale Steuerung besteht aus folgenden ICs:
CD4001, CD4043, CD4017, CD4024, CD4514, NE555, LM358 & SN75469
zzgl. 6 Transistor-Treiber, 1 Relais & 2 Gleichrichter (davon 1x stabilisiert 9V=)

Manuelle Eingriffe: 2x Rückstelltaste (setzt die internen Zähler auf 0), Brückenhub abbrechen
und Brückenhub anfordern

Die Ampelleuchten bestehen aus je 3 Leistungs-LEDs, ebenso die Zustandsanzeige (roter
Kasten - Brückenhub manuell anfordern); die Beleuchtung (Standard-Lampen) besteht aus
je 2 Stromkreisen á 5 Lampen (30 V~)

Versorgung: großer Märklin-Trafo (umgebaut) mit Abgriff 0 - 10 - 30 V~
