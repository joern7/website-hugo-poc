---
layout: "image"
title: "Vorderachse (2)"
date: 2020-03-06T19:44:53+01:00
picture: "2020-01-06 Kleines Fahrwerk mit magnetischer Federung5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor für die Lenkung ist Teil des Fahrzeugrahmens. Damit keine Stecker nach unten herausragen, sind zwei Kabel ohne Stecker mittels je einem 36495 Leuchtstein-Stopfen ec + em im Motor. Der Taster in der Mitte dient zur Definition der Geradeausstellung für die ältere IR-Fernbedienung.