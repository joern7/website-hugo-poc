---
layout: "image"
title: "Bedienstand"
date: 2020-03-06T21:04:47+01:00
picture: "DSC06920.JPG"
weight: "3"
konstrukteure: 
- "PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Bediener am Arbeitsplatz
