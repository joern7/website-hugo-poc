---
layout: "image"
title: "Der Radar-Screen"
date: 2020-03-06T21:04:40+01:00
picture: "screenshot.JPG"
weight: "8"
konstrukteure: 
- "PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Feindliche Objekte im Anflug
