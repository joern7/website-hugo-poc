---
layout: "image"
title: "3D-Druck-Spriegel"
date: 2020-03-06T20:48:00+01:00
picture: "DSC06917.JPG"
weight: "9"
konstrukteure: 
- "PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der 3D-gedruckte Spriegel, lediglich die Verzurrlaschen weisen Abweichungen zum Original auf, die aber beim Modell zu verschmerzen sind.