---
layout: "image"
title: "Unteransicht"
date: 2020-03-06T20:48:01+01:00
picture: "DSC06916.JPG"
weight: "8"
konstrukteure: 
- "PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Man erkennt an der Vorderachse den Fahrschemel des Hydraulik-Anhängers. An der Hinterachse die übliche Konstruktion mit diskret aufgebauten Radaufhängungen.