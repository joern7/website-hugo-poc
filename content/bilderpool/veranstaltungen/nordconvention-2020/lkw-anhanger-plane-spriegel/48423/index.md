---
layout: "image"
title: "Plane geöffnet"
date: 2020-03-06T20:48:03+01:00
picture: "DSC06915.JPG"
weight: "7"
konstrukteure: 
- "PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Plane kann unter der Verschnürung herausgezogen werden, ohne die Schleife zu öffnen. Damit ist ein schneller Zugang möglich.