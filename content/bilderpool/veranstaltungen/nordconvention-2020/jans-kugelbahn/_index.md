---
layout: "overview"
title: "Jans Kugelbahn
"
date: 2020-03-06T22:10:08+01:00
---

Von meinem Sohn (mit etwas Hilfe aus Papas Teilekiste) gebaute Kugelbahn.
Sie hat eine "schnelle" (steiles Gefälle mit Looping) und eine "langsame" Strecke (Taumeltrichter).

Das Highlight ist das verbaute einstellbare Trampolinmodul aus dem Dynamic XM Baukasten. Richtig justiert, springen die Kugeln der langsamen Strecke gegen ein Klangrohr und fallen dann in den Zieltrichter.

Die Kugeln der schnellen Strecke passieren ebenfalls ein Klangrohr, bevor sie in den Zieltrichter laufen.

Video der Bahn [hier auf YouTube.](https://youtu.be/m1zZbwPkPV4)
