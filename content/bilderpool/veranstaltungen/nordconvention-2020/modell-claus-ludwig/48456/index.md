---
layout: "image"
title: "Flipper im Rohbau"
date: 2020-03-09T21:13:19+01:00
picture: "Flipper im Rohbau.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Das Knochengerüst des Flippers