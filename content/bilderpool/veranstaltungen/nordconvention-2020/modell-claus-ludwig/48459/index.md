---
layout: "image"
title: "Flipper bei Tag"
date: 2020-03-09T21:13:22+01:00
picture: "Flipper bei Tag.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Ansicht bei Tag, für eine dunkle Eckkneipe aber recht ungewöhnlich.