---
layout: "image"
title: "Im laufenden Betrieb"
date: 2020-03-08T16:58:29+01:00
picture: "P1000643.JPG"
weight: "2"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man jetzt das laufende Modell. Es passiert nicht viel: die beiden Achsen drehen sich und die Lämpchen blinken. Die Frage ist nur: in welchem Rhythmus?