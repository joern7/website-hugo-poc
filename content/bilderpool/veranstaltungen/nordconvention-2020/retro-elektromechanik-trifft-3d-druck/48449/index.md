---
layout: "image"
title: "Übersetzung"
date: 2020-03-08T16:58:31+01:00
picture: "P1000644.JPG"
weight: "3"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die unrunden Zahnräder bewirken, dass die 2. Achse sich nicht gleichmäßig schnell dreht.