---
layout: "image"
title: "Schalter"
date: 2020-03-08T16:58:33+01:00
picture: "P1000646.JPG"
weight: "4"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der Schleifring wird, wie in der Anleitung zu em1 beschrieben, als Nockenwelle missbraucht und schaltet hier den Taster an und aus.