---
layout: "image"
title: "Zahnräder"
date: 2020-03-08T16:58:34+01:00
picture: "P1000647.JPG"
weight: "5"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man genauer, wie sich die Übersetzung der Zahnräder im Laufe einer Umdrehung ändert.