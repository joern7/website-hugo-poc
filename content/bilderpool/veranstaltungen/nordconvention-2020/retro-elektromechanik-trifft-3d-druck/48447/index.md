---
layout: "image"
title: "Übersicht"
date: 2020-03-08T16:58:28+01:00
picture: "P1000642.JPG"
weight: "1"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Übersicht über das ganze Modell. Alle Teile außer den beiden weißen Zahnrädern stammen aus einem Grundkasten 300 oder aus em1.