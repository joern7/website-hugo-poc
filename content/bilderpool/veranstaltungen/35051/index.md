---
layout: "image"
title: "Fischertechnik Stammtisch Rhein Main"
date: "2012-06-08T22:47:20"
picture: "0001.jpg"
weight: "1"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/35051
- /detailsa0e0.html
imported:
- "2019"
_4images_image_id: "35051"
_4images_cat_id: "1301"
_4images_user_id: "473"
_4images_image_date: "2012-06-08T22:47:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35051 -->
Die Hütte ist endlich fertig. 12-15 Personen gehen gut rein, allerdings nur zum Klnen und was Trinken. Grillen ist gemütlich für bis zu 8 Personen. 

Ich würde mich freuen, wenn wir uns bald in Wiesbaden treffen könnten