---
layout: "image"
title: "Neuer Roboter von unten"
date: "2004-02-11T11:20:29"
picture: "109_0953.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/2105
- /details0c64.html
imported:
- "2019"
_4images_image_id: "2105"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2105 -->
Hier der neue Roboter von unten gesehen (meist sind dort ja die Geheimnisse :-)