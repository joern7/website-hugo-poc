---
layout: "image"
title: "Robo Pro Software"
date: "2004-02-11T11:20:13"
picture: "109_0930.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Robo", "Pro", "Software"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/2096
- /detailse0c3.html
imported:
- "2019"
_4images_image_id: "2096"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2096 -->
Hier einmal die Robo Pro Software. Man sieht die Eigenschaften des Momentan angeklickten Tasters oben rechts. Dort kann man auch die Zuordnung an welchem Interface er ist treffen.