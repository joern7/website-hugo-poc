---
layout: "image"
title: "Robointerface Robo RF Datalink"
date: "2004-02-11T11:20:13"
picture: "109_0927.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Robointerface", "Robo", "RF", "Datalink"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/2095
- /detailse51d.html
imported:
- "2019"
_4images_image_id: "2095"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2095 -->
Robointerface Robo RF Datalink als Zusatzplatine für das Interface