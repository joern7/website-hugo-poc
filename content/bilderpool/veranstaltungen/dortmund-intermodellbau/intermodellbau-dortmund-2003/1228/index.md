---
layout: "image"
title: "Die neuen Modelle(Tracktor mit Anhänger)"
date: "2003-07-08T17:12:53"
picture: "Die_neuen_Modelle.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1228
- /details7a05.html
imported:
- "2019"
_4images_image_id: "1228"
_4images_cat_id: "141"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1228 -->
