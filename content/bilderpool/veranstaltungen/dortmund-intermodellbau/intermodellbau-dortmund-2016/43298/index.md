---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau23.jpg"
weight: "23"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43298
- /details53ca.html
imported:
- "2019"
_4images_image_id: "43298"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43298 -->
Vitrine mit Robotik-Modellen