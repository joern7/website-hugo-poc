---
layout: "overview"
title: "Intermodellbau Dortmund 2016"
date: 2020-02-22T09:01:08+01:00
legacy_id:
- /php/categories/3215
- /categories5fda.html
- /categories0d14.html
- /categories3027.html
- /categories58d5.html
- /categoriesc7ab.html
- /categories684d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3215 --> 
Dieses Jahr waren wir, die Aussteller Markus W., geometer , Friedrich N., rob-van-baal, TST, Dirk K., Friedrich N. und DirkW 
auf der Intermodellbau in Dortmund 20.4-.24.04.2016

Wir hatten eine breite Auswahl von Modellen dabei, die sehr gut beim Publikum ankam. 

Dazu zählten der Wiener Prater und das Atomium als Blickfang in den riesigen Hallen. Hinzu kamen Funktionsmodelle, 
wie den Sattelzug mit Bagger, die Kugeluhr und ein Roboter. Modelle aus dem Buch "Technikgeschichte mit fischertechnik". 
Zum Beispiel das Planetarium, Rechenmaschinen, ein Sextant und eine Uhr mit Schlagwerk. Für die Kinder hatten wir einen 
großen Baukran mit Fernbedienung, den Flipper, (der auch super bei Erwachsenen ankam) und den WALL-E. 
