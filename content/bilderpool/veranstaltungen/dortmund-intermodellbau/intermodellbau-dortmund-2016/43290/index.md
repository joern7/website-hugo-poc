---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau15.jpg"
weight: "15"
konstrukteure: 
- "geometer"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43290
- /detailscc60.html
imported:
- "2019"
_4images_image_id: "43290"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43290 -->
Planetarium