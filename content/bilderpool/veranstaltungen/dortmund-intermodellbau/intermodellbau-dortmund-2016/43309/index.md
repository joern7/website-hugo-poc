---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau34.jpg"
weight: "34"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43309
- /detailsdd80.html
imported:
- "2019"
_4images_image_id: "43309"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43309 -->
Schrittmotor X