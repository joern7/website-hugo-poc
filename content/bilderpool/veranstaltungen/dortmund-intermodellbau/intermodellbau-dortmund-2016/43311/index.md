---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau36.jpg"
weight: "36"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43311
- /details0143.html
imported:
- "2019"
_4images_image_id: "43311"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43311 -->
Schrittmotor Y