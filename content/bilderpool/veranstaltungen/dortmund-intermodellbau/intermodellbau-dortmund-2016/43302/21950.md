---
layout: "comment"
hidden: true
title: "21950"
date: "2016-04-25T20:33:10"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das ist "Repetierhost", die 3D-Drucker-Software. Da kann man Objekte (STL-Dateien) auf die Druckplattform laden, vervielfachen und ausrichten. Hinter dem Knopf "Slicer" kann man (Überraschung) einen Slicer (Cura oder Slic3r) auswählen, vorausgesetzt, dass ft die beiden mitliefert. Wenn nicht, ist das auch nicht so schlimm: das ist alles Freeware. Nur die Voreinstellungen sollten schon an das Druckermodell angepasst sein/werden.