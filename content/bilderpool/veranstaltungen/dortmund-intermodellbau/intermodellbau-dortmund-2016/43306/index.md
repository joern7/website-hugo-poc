---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau31.jpg"
weight: "31"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43306
- /detailsa0b2.html
imported:
- "2019"
_4images_image_id: "43306"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43306 -->
Steuerung.