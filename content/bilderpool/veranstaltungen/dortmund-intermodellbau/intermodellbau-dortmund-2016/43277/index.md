---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43277
- /detailsf8d3.html
imported:
- "2019"
_4images_image_id: "43277"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43277 -->
Brückenkran