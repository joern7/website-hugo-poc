---
layout: "image"
title: "Plotter"
date: "2016-09-21T16:47:03"
picture: "IMG_0468.jpg"
weight: "7"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44392
- /details7984.html
imported:
- "2019"
_4images_image_id: "44392"
_4images_cat_id: "3280"
_4images_user_id: "2638"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44392 -->
