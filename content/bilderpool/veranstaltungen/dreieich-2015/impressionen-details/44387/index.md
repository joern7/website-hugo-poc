---
layout: "image"
title: "Spielen am Classik-Kran"
date: "2016-09-21T16:47:03"
picture: "IMG_0500.jpg"
weight: "2"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44387
- /details9745.html
imported:
- "2019"
_4images_image_id: "44387"
_4images_cat_id: "3280"
_4images_user_id: "2638"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44387 -->
Der Classic Kran ist ca. 2m hoch mit einer Auslage von 150cm und einer Traglast von 400g (bei max. Auslage) Voll bespielbar.