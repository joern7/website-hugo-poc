---
layout: "image"
title: "Papa muss die neue Maschine ausprobieren"
date: "2016-09-21T16:47:03"
picture: "IMG_0507.jpg"
weight: "3"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Jan Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44388
- /detailsb5fb.html
imported:
- "2019"
_4images_image_id: "44388"
_4images_cat_id: "3280"
_4images_user_id: "2638"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44388 -->
(keine Ahnung was das DIng macht, aber Jan hat riesigen Spaß dabei meine Bemühungen zu sehen.)
