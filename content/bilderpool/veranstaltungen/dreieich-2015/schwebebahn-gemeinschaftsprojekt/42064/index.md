---
layout: "image"
title: "Wendeanlage von sjost (Treppenaufgang)"
date: "2015-10-06T18:38:55"
picture: "olagino03.jpg"
weight: "13"
konstrukteure: 
- "sjost"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42064
- /details668d.html
imported:
- "2019"
_4images_image_id: "42064"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42064 -->
Natürlich wurde beim Bau der Stationen auf jedes Detail geachtet. Hier zum Beispiel auf die Fachwerkkonstruktion am Bahnsteig