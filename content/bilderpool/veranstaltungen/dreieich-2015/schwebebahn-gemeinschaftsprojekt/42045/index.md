---
layout: "image"
title: "IMG_2138.JPG"
date: "2015-10-05T21:08:01"
picture: "IMG_2138mit.JPG"
weight: "2"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42045
- /detailse080.html
imported:
- "2019"
_4images_image_id: "42045"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:08:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42045 -->
Das Depot nähert sich der Fertigstellung. Von links kommend, werden die Züge auf eine der parallel hängenden "Parkbuchten" rangiert. Das sind parallel hängende Gleisstücke, von denen auf dem Bild nur die äußeren vorhanden sind. Diese gehen auch hinter dem Depot in einer Kurve ineinander über, so dass die Züge hier wenden können.