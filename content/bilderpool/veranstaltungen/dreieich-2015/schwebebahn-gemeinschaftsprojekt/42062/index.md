---
layout: "image"
title: "Schwebebahn Teilansicht"
date: "2015-10-06T18:38:55"
picture: "olagino01.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42062
- /details7d3c.html
imported:
- "2019"
_4images_image_id: "42062"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42062 -->
Die Schwebebahn war ein Gemeinschaftsprojekt organisiert von Getriebesand, bei dem sjost, harald, steffalk, tobs9578, Volker-James Münchhof, Masked und olagino beteiligt waren, indem sie Gleisabschnitte, Stationen oder Schwebebahnzüge beigesteuert hatten. So entstand eine Anlage mit ungefähr 10,5m Gleislänge