---
layout: "image"
title: "IMG_2146.JPG"
date: "2015-10-05T21:17:33"
picture: "IMG_2146mit.JPG"
weight: "6"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42049
- /details7bcf.html
imported:
- "2019"
_4images_image_id: "42049"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:17:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42049 -->
Die Zugwendestation. Das Gleisstück dreht sich um 180° und verlängert dabei mal die eine und mal die andere Seite vom Gleis. Da kann der Zug einfahren zum Drehen, oder nach Drehung wieder ausfahren.