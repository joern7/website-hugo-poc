---
layout: "image"
title: "sinsheim-4"
date: "2003-09-28T17:25:39"
picture: "sinsh-4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["sinsheim", "ausstellung"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/1748
- /detailsa5c9.html
imported:
- "2019"
_4images_image_id: "1748"
_4images_cat_id: "179"
_4images_user_id: "41"
_4images_image_date: "2003-09-28T17:25:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1748 -->
