---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster24.jpg"
weight: "24"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43056
- /details6772.html
imported:
- "2019"
_4images_image_id: "43056"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43056 -->
Die Kegel hängen an Fäden.Im Kegel befindet sich ein Magnet.
Unter der Plexiglasscheibe sind 9 Reedkontakte verbaut. Vorne die Lichtschranke, welche die Kugeln zählt.