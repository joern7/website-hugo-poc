---
layout: "image"
title: "Laser-Plotter"
date: "2016-03-10T20:29:35"
picture: "neumuenster18.jpg"
weight: "18"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43050
- /details0ff6.html
imported:
- "2019"
_4images_image_id: "43050"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43050 -->
Der Laser-Plotter hat einen 500 mW Laser, angesteuert über einen Robo TXT Controller.
Dieser kann zum Gravieren verwendet werden.