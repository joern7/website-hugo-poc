---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster28.jpg"
weight: "28"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43060
- /details90a6.html
imported:
- "2019"
_4images_image_id: "43060"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43060 -->
Links die Kugelzuführung. Unten der Robo TX Controller. Daneben die 
5V Spannungsversorung für das 8-Kanal Relais und die beiden PCF8574.