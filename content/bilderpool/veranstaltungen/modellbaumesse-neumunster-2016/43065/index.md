---
layout: "image"
title: "Flechtmaschine + Schachspiel"
date: "2016-03-10T20:29:35"
picture: "neumuenster33.jpg"
weight: "33"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43065
- /detailsc5ea.html
imported:
- "2019"
_4images_image_id: "43065"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43065 -->
