---
layout: "comment"
hidden: true
title: "21793"
date: "2016-03-11T16:42:59"
uploadBy:
- "Svefisch"
license: "unknown"
imported:
- "2019"
---
Nein, die Zylinder dienen nicht einfach nur zur Federung. Sie dienen zu Höhenverstellung und zur Nivellierung des Fahrzeugs im Gelände. Im Original kann die Bodenfreiheit zwischen 120 und 1160 mm eingestellt werden, entsprechend können die "Beine" einzeln ausgefahren werden und Neigungswinkel von +/-15° (vorne/hinten) bzw. +/- 25° (links/rechts) ausgeglichen werden. Die Pneumatikzylinder schaffen es tatsächlich das Modell (ohne Kran für das zugehörige Harvesteraggregat) entsprechend in der Höhe zu verstellen (paarweise vorne, hinten, links oder rechts bzw. insgesamt), eine dynamische Fahrregelung ist damit allerdings nicht möglich.
Gruß
Holger