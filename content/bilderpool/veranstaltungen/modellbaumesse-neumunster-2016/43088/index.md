---
layout: "image"
title: "Die Wall-E Brüder"
date: "2016-03-10T20:29:35"
picture: "neumuenster56.jpg"
weight: "56"
konstrukteure: 
- "Frank & Dirk"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43088
- /details8654.html
imported:
- "2019"
_4images_image_id: "43088"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43088 -->
linker Wall-E Frank, rechter Wall-E Dirk