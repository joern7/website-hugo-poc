---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster23.jpg"
weight: "23"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43055
- /details5754.html
imported:
- "2019"
_4images_image_id: "43055"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43055 -->
Kegel sind gestellt