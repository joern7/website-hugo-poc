---
layout: "image"
title: "Training Roboter modified"
date: "2012-10-08T12:22:14"
picture: "DSC01348_2.jpg"
weight: "10"
konstrukteure: 
- "Marspau & R.R.Bidding"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/35844
- /details427d.html
imported:
- "2019"
_4images_image_id: "35844"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2012-10-08T12:22:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35844 -->
Under those black wrappings are the adapper circuits. 

Unter diesen schwarzen Verpackungen gibt es die Adapter Schaltungen