---
layout: "image"
title: "Mopeds08.JPG"
date: "2008-09-23T10:02:29"
picture: "Mopeds08.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/15552
- /details22e2.html
imported:
- "2019"
_4images_image_id: "15552"
_4images_cat_id: "1432"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:02:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15552 -->
