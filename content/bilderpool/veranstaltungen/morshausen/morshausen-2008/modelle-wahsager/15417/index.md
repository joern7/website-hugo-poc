---
layout: "image"
title: "Heiko Engelke mit 10 Kg beladenem Truck und Speedy68"
date: "2008-09-22T07:43:48"
picture: "Heiko_Engelke_mit_10_Kg_beladenem_Truck_und_Speedy68.jpg"
weight: "2"
konstrukteure: 
- "Wahsager"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15417
- /details60b7.html
imported:
- "2019"
_4images_image_id: "15417"
_4images_cat_id: "1418"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15417 -->
