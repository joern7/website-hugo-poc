---
layout: "image"
title: "10 Kg Beladung auf Truck"
date: "2008-09-22T07:43:48"
picture: "10_Kg_Beladung_auf_Truck.jpg"
weight: "3"
konstrukteure: 
- "Wahsager"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15418
- /details0e3c.html
imported:
- "2019"
_4images_image_id: "15418"
_4images_cat_id: "1418"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15418 -->
