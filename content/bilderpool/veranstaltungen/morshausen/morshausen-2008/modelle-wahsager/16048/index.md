---
layout: "image"
title: "Truck"
date: "2008-10-25T14:26:12"
picture: "heiko1.jpg"
weight: "8"
konstrukteure: 
- "Heiko"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16048
- /details3a86.html
imported:
- "2019"
_4images_image_id: "16048"
_4images_cat_id: "1418"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16048 -->
