---
layout: "image"
title: "Truck"
date: "2008-10-25T14:26:12"
picture: "heiko2.jpg"
weight: "9"
konstrukteure: 
- "Heiko"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16049
- /details1667.html
imported:
- "2019"
_4images_image_id: "16049"
_4images_cat_id: "1418"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16049 -->
