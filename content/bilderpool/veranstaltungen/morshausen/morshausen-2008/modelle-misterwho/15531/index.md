---
layout: "image"
title: "Mobile Roboter"
date: "2008-09-23T07:43:24"
picture: "convention63.jpg"
weight: "2"
konstrukteure: 
- "MisterWho ?"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15531
- /details0f07.html
imported:
- "2019"
_4images_image_id: "15531"
_4images_cat_id: "1424"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15531 -->
