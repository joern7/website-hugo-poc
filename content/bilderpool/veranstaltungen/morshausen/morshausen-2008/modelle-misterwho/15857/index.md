---
layout: "image"
title: "Moershausen Models"
date: "2008-10-10T08:58:48"
picture: "m_robots_c.jpg"
weight: "7"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15857
- /details2721-2.html
imported:
- "2019"
_4images_image_id: "15857"
_4images_cat_id: "1424"
_4images_user_id: "585"
_4images_image_date: "2008-10-10T08:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15857 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.