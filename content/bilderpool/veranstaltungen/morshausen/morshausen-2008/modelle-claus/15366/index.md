---
layout: "image"
title: "Drehmaschine"
date: "2008-09-21T21:34:07"
picture: "con1.jpg"
weight: "1"
konstrukteure: 
- "Claus"
fotografen:
- "Limit"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/15366
- /details9c1d.html
imported:
- "2019"
_4images_image_id: "15366"
_4images_cat_id: "1402"
_4images_user_id: "430"
_4images_image_date: "2008-09-21T21:34:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15366 -->
