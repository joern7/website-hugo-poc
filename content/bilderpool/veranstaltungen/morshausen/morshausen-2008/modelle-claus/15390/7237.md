---
layout: "comment"
hidden: true
title: "7237"
date: "2008-09-22T12:45:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Claus,

mächtig, gewaltig, tolles Modell. Es bleibt immer eine Gratwanderung zwischen der Anzahl der modellmässig umzusetzenden Funktionen und der Modellgrösse. In meinem Designer ist ein unvollendeter Entwurf gespeichert zu deinem Modell etwa im Massstab 2:1.

Gruss, Udo2