---
layout: "image"
title: "3D-Roboter"
date: "2008-10-25T14:26:12"
picture: "rob2.jpg"
weight: "23"
konstrukteure: 
- "Rob"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16047
- /details8f59.html
imported:
- "2019"
_4images_image_id: "16047"
_4images_cat_id: "1405"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16047 -->
