---
layout: "image"
title: "Dreharm mit 2 Propellern"
date: "2008-09-21T22:19:53"
picture: "tovenaar1.jpg"
weight: "1"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15386
- /details6682.html
imported:
- "2019"
_4images_image_id: "15386"
_4images_cat_id: "1405"
_4images_user_id: "130"
_4images_image_date: "2008-09-21T22:19:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15386 -->
Trotz schneller Verschlusszeit nicht scharf geworden. Aber ein tolles Modell. Einfach aber gut gebaut.