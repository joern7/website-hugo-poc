---
layout: "image"
title: "Kugelbahn"
date: "2008-10-25T14:26:12"
picture: "rob1.jpg"
weight: "22"
konstrukteure: 
- "Rob"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16046
- /detailse079-2.html
imported:
- "2019"
_4images_image_id: "16046"
_4images_cat_id: "1405"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16046 -->
