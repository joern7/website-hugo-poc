---
layout: "image"
title: "Ball aus Statiksteinen"
date: "2008-09-22T07:43:47"
picture: "Ball_aus_Statiksteinen.jpg"
weight: "4"
konstrukteure: 
- "Tovenaar"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15413
- /detailsa521-3.html
imported:
- "2019"
_4images_image_id: "15413"
_4images_cat_id: "1405"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15413 -->
