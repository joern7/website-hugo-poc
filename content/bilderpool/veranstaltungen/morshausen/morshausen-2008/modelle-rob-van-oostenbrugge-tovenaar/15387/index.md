---
layout: "image"
title: "Dreharm mit 2 Propellern"
date: "2008-09-21T22:19:53"
picture: "tovenaar2.jpg"
weight: "2"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15387
- /details55bc.html
imported:
- "2019"
_4images_image_id: "15387"
_4images_cat_id: "1405"
_4images_user_id: "130"
_4images_image_date: "2008-09-21T22:19:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15387 -->
Nochmal der Arm nur Sekundenbruchteile später.