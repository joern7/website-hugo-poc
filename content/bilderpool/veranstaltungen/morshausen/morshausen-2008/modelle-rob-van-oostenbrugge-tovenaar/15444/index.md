---
layout: "image"
title: "Zwei Taster mit nur ein Hand !"
date: "2008-09-22T15:37:55"
picture: "moershausen20.jpg"
weight: "14"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15444
- /details2c0e.html
imported:
- "2019"
_4images_image_id: "15444"
_4images_cat_id: "1405"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15444 -->
