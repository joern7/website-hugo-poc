---
layout: "image"
title: "Digitaluhr"
date: "2008-09-25T17:47:41"
picture: "conv03.jpg"
weight: "8"
konstrukteure: 
- "Steffalk"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15608
- /detailscc00.html
imported:
- "2019"
_4images_image_id: "15608"
_4images_cat_id: "1414"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15608 -->
