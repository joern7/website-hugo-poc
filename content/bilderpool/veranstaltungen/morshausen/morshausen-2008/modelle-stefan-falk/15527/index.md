---
layout: "image"
title: "Frequenzgesteuerte Uhr..."
date: "2008-09-23T07:43:24"
picture: "convention59.jpg"
weight: "4"
konstrukteure: 
- "steffalk"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15527
- /details83a4.html
imported:
- "2019"
_4images_image_id: "15527"
_4images_cat_id: "1414"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15527 -->
