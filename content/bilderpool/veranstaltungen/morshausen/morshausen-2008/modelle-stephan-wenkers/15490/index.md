---
layout: "image"
title: "Kaiser Wilhelm Brücke"
date: "2008-09-23T07:43:24"
picture: "convention22.jpg"
weight: "4"
konstrukteure: 
- "Stephan"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15490
- /details1710.html
imported:
- "2019"
_4images_image_id: "15490"
_4images_cat_id: "1415"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15490 -->
