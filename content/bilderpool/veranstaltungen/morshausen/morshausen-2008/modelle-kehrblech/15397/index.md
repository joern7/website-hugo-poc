---
layout: "image"
title: "4 Gewinnt Roboter mit Kehrblech"
date: "2008-09-22T07:43:46"
picture: "4_Gewinnt_Roboter_mit_Kehrblech.jpg"
weight: "1"
konstrukteure: 
- "Kehrblech"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15397
- /details71c0.html
imported:
- "2019"
_4images_image_id: "15397"
_4images_cat_id: "1410"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15397 -->
