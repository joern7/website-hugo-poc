---
layout: "image"
title: "Moershausen Models"
date: "2008-10-10T08:58:33"
picture: "m_racecar.jpg"
weight: "15"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15847
- /details6c11.html
imported:
- "2019"
_4images_image_id: "15847"
_4images_cat_id: "1409"
_4images_user_id: "585"
_4images_image_date: "2008-10-10T08:58:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15847 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.