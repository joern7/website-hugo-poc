---
layout: "image"
title: "Hebebühne"
date: "2008-09-23T07:43:23"
picture: "convention14.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15482
- /details4f0d.html
imported:
- "2019"
_4images_image_id: "15482"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15482 -->
