---
layout: "image"
title: "Drehmaschine"
date: "2009-10-01T19:18:52"
picture: "ftconventioninmoehrhausen03.jpg"
weight: "3"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25453
- /details8106.html
imported:
- "2019"
_4images_image_id: "25453"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25453 -->
Aufbau mit Werkzeugträger und Reitstock