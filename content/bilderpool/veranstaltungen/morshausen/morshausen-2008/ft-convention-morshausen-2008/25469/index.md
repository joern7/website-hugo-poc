---
layout: "image"
title: "Flugzeug von Harald"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen19.jpg"
weight: "19"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25469
- /detailsc33a.html
imported:
- "2019"
_4images_image_id: "25469"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25469 -->
Teilansicht von rechts vorne