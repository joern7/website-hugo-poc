---
layout: "image"
title: "Winkelgetriebe von Steffalk"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen27.jpg"
weight: "27"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25477
- /details321f.html
imported:
- "2019"
_4images_image_id: "25477"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25477 -->
