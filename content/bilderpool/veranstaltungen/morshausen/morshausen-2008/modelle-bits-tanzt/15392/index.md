---
layout: "image"
title: "Roboterarm"
date: "2008-09-22T07:43:45"
picture: "Roboterarm_und_Der_mit_den_Bits_tanzt.jpg"
weight: "1"
konstrukteure: 
- "Der mit den Bits tanzt"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15392
- /details2026.html
imported:
- "2019"
_4images_image_id: "15392"
_4images_cat_id: "1406"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15392 -->
