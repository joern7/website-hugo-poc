---
layout: "image"
title: "Roboter 2"
date: "2008-09-22T15:37:55"
picture: "moershausen13.jpg"
weight: "3"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15437
- /details238b.html
imported:
- "2019"
_4images_image_id: "15437"
_4images_cat_id: "1406"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15437 -->
