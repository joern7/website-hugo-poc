---
layout: "comment"
hidden: true
title: "7231"
date: "2008-09-22T10:49:07"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein besonders schönes Detail finde ich die motorische Ansteuerung des Stufenschalters rechts vorne. Wer den nicht kennt: Er stammt aus dem älteren Elektromechanikprogramm von ft, hat 8 Stellungen und ist durch Verkabelung der 8 oberen und unteren Kontakte sehr vielseitig zu verwenden.

Gruß,
Stefan