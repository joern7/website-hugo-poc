---
layout: "image"
title: "Moershausen 2008"
date: "2008-10-07T22:29:13"
picture: "m_clock.jpg"
weight: "7"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15832
- /detailsbd88.html
imported:
- "2019"
_4images_image_id: "15832"
_4images_cat_id: "1417"
_4images_user_id: "585"
_4images_image_date: "2008-10-07T22:29:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15832 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.