---
layout: "image"
title: "Moershausen Model"
date: "2008-10-08T10:48:38"
picture: "m_official_display.jpg"
weight: "5"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15842
- /details2d0a.html
imported:
- "2019"
_4images_image_id: "15842"
_4images_cat_id: "1398"
_4images_user_id: "585"
_4images_image_date: "2008-10-08T10:48:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15842 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.