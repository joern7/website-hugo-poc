---
layout: "image"
title: "Kaffe Chili und Kuchen"
date: "2008-09-22T15:37:55"
picture: "moershausen15.jpg"
weight: "22"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15439
- /details5bd0-2.html
imported:
- "2019"
_4images_image_id: "15439"
_4images_cat_id: "1403"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15439 -->
Danke, war alles sehr lecker.