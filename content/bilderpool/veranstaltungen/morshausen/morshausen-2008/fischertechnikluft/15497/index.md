---
layout: "image"
title: "Convention 2008"
date: "2008-09-23T07:43:24"
picture: "convention29.jpg"
weight: "24"
konstrukteure: 
- "Verschiedene"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15497
- /details97a4-2.html
imported:
- "2019"
_4images_image_id: "15497"
_4images_cat_id: "1403"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15497 -->
