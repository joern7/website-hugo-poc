---
layout: "image"
title: "Diabolo"
date: "2008-09-21T21:34:08"
picture: "conv06.jpg"
weight: "5"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15373
- /detailse079.html
imported:
- "2019"
_4images_image_id: "15373"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15373 -->
Auch ein Diabolo kann man aus ft bauen.