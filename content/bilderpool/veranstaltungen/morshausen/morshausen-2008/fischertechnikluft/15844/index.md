---
layout: "image"
title: "Moershausen 2008"
date: "2008-10-08T10:48:38"
picture: "m_peter.jpg"
weight: "70"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15844
- /details2f49.html
imported:
- "2019"
_4images_image_id: "15844"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-08T10:48:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15844 -->
These are some pics of Moershausen 2008. Thought to share.