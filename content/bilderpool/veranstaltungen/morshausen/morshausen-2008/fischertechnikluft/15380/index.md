---
layout: "image"
title: "Abendessen"
date: "2008-09-21T21:34:08"
picture: "conv13.jpg"
weight: "12"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15380
- /detailsd2e8.html
imported:
- "2019"
_4images_image_id: "15380"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15380 -->
Bauernpfanne, die Zweite. Den ersten Versuch gabs letztes Jahr (wir berichteten) und auch dieses Jahr schlug er wieder fehl...