---
layout: "image"
title: "Fackeln (Ralf)"
date: "2008-09-21T21:34:08"
picture: "conv17.jpg"
weight: "16"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15384
- /detailseaa3.html
imported:
- "2019"
_4images_image_id: "15384"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15384 -->
Und ja, das ist echtes Feuer ;-)