---
layout: "image"
title: "Dinner in Homberg"
date: "2008-10-02T16:37:23"
picture: "sm_hom_4_2.jpg"
weight: "36"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008", "Homberg"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15768
- /details21e5.html
imported:
- "2019"
_4images_image_id: "15768"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-02T16:37:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15768 -->
On the night of the 22nd, a group of ft fans had dinner in Homberg. We were honored to attend.