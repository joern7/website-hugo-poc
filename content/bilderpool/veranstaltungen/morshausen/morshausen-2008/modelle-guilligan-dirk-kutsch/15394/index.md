---
layout: "image"
title: "Containergreifer"
date: "2008-09-22T07:43:45"
picture: "Containergreifer.jpg"
weight: "1"
konstrukteure: 
- "Guilligan"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15394
- /detailsd2ac.html
imported:
- "2019"
_4images_image_id: "15394"
_4images_cat_id: "1408"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15394 -->
