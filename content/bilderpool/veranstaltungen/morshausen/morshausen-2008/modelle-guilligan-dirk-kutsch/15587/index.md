---
layout: "image"
title: "Dirk Kutsch"
date: "2008-09-24T22:21:47"
picture: "FT-Mrshausen-2008-_165.jpg"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/15587
- /detailscb72.html
imported:
- "2019"
_4images_image_id: "15587"
_4images_cat_id: "1408"
_4images_user_id: "22"
_4images_image_date: "2008-09-24T22:21:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15587 -->
