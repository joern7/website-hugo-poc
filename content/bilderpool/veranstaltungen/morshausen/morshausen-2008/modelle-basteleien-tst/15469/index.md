---
layout: "image"
title: "Roboter"
date: "2008-09-23T07:43:23"
picture: "convention01.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15469
- /details4637.html
imported:
- "2019"
_4images_image_id: "15469"
_4images_cat_id: "1400"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15469 -->
