---
layout: "image"
title: "Andreas Tacke TST"
date: "2008-09-22T07:43:45"
picture: "Wink-Roboter_und_Andreas_Tacke.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15391
- /detailsd745.html
imported:
- "2019"
_4images_image_id: "15391"
_4images_cat_id: "1400"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15391 -->
