---
layout: "image"
title: "Kräne"
date: "2008-10-25T14:26:41"
picture: "fitec07.jpg"
weight: "13"
konstrukteure: 
- "fitec"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16064
- /detailsf7cf.html
imported:
- "2019"
_4images_image_id: "16064"
_4images_cat_id: "1407"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16064 -->
