---
layout: "image"
title: "Top Spin von Speedy 68"
date: "2008-09-23T07:43:23"
picture: "ts3.jpg"
weight: "12"
konstrukteure: 
- "Thomas Falkenberg (Speedy 68)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15468
- /details409b.html
imported:
- "2019"
_4images_image_id: "15468"
_4images_cat_id: "1413"
_4images_user_id: "130"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15468 -->
Der Drehkranz ist auch genial gebaut.