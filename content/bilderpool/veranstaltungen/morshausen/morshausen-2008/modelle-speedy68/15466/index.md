---
layout: "image"
title: "Top Spin von Speedy 68"
date: "2008-09-23T07:43:23"
picture: "ts1.jpg"
weight: "10"
konstrukteure: 
- "Thomas Falkenberg (Speedy 68)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15466
- /detailscc64-2.html
imported:
- "2019"
_4images_image_id: "15466"
_4images_cat_id: "1413"
_4images_user_id: "130"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15466 -->
Einfach nur genial gebaut.