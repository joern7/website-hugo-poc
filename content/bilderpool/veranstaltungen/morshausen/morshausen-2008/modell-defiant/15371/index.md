---
layout: "image"
title: "Defiants Spielzeug"
date: "2008-09-21T21:34:08"
picture: "conv04.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15371
- /detailsa145.html
imported:
- "2019"
_4images_image_id: "15371"
_4images_cat_id: "1434"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15371 -->
Der Bruchpilot hat seinem Namen alle Ehre gemacht ;-)