---
layout: "image"
title: "Turn-Up74.JPG"
date: "2008-09-23T10:10:20"
picture: "Turn-Up74.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrgeschäft"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/15560
- /details4757.html
imported:
- "2019"
_4images_image_id: "15560"
_4images_cat_id: "1398"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:10:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15560 -->
