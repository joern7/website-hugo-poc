---
layout: "image"
title: "Quattrocopter - Fluggerät in der Luft"
date: "2008-09-22T15:37:54"
picture: "Quattrocopter_-_Fluggert_in_der_Luft.jpg"
weight: "4"
konstrukteure: 
- "thkais"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15422
- /details7a5e.html
imported:
- "2019"
_4images_image_id: "15422"
_4images_cat_id: "1399"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T15:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15422 -->
Und Fischertechnik fliegt doch!