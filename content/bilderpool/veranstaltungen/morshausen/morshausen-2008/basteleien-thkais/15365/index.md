---
layout: "image"
title: "Luftbild-3"
date: "2008-09-21T20:41:56"
picture: "conv2008-03.jpg"
weight: "3"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/15365
- /details419e.html
imported:
- "2019"
_4images_image_id: "15365"
_4images_cat_id: "1399"
_4images_user_id: "41"
_4images_image_date: "2008-09-21T20:41:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15365 -->
