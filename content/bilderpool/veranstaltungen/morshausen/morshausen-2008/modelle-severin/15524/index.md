---
layout: "image"
title: "Sechsachsroboter"
date: "2008-09-23T07:43:24"
picture: "convention56.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15524
- /detailsf8f0.html
imported:
- "2019"
_4images_image_id: "15524"
_4images_cat_id: "1436"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15524 -->
Punktgenau steuerbar