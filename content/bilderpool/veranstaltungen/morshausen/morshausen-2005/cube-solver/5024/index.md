---
layout: "image"
title: "Cube Solver"
date: "2005-09-26T23:48:51"
picture: "Cube_Solver_von_Marmac.jpg"
weight: "4"
konstrukteure: 
- "MarMac"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5024
- /details93a6.html
imported:
- "2019"
_4images_image_id: "5024"
_4images_cat_id: "391"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5024 -->
