---
layout: "comment"
hidden: true
title: "733"
date: "2005-10-24T15:28:56"
uploadBy:
- "MarMac"
license: "unknown"
imported:
- "2019"
---
AkkuEin 9,6V-3300mAh-Akkupack. Ich werd zur Sicherheit des Interfaces mal schauen, wie ich eine Strombegrenzung hinkrieg, falls die Motor kurzschlusströme produzieren.

Aus meiner ursprünglichen Einzelradaufhängung ist leider nix geworden, da werde ich irgendwann nochmal einen Versuch starten.