---
layout: "image"
title: "conv2005 sven002"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven002.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt"
fotografen:
- "Sven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4939
- /detailsb505.html
imported:
- "2019"
_4images_image_id: "4939"
_4images_cat_id: "398"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4939 -->
