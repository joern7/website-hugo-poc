---
layout: "image"
title: "01"
date: "2005-09-26T23:48:51"
picture: "Conv2005_CJ01_2.jpg"
weight: "1"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5020
- /detailsd15a.html
imported:
- "2019"
_4images_image_id: "5020"
_4images_cat_id: "392"
_4images_user_id: "10"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5020 -->
