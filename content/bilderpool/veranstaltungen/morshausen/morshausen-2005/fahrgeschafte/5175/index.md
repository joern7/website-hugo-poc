---
layout: "image"
title: "Kermis-NN46.JPG"
date: "2005-11-03T14:41:10"
picture: "Kermis-NN46.JPG"
weight: "10"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5175
- /detailsaf80.html
imported:
- "2019"
_4images_image_id: "5175"
_4images_cat_id: "433"
_4images_user_id: "4"
_4images_image_date: "2005-11-03T14:41:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5175 -->
Die Kinematik ist sehr bemerkenswert. Die beiden Z40 im Hintergrund sind festgeklemmt, die zugehörigen Ketten sorgen dafür, dass die Gondeln ihre Neigung auch beim Drehen des rautenförmigen Tragarms beibehalten. Durch Verdrehen der Z40 kann die Neigung verstellt werden.

Die Gondeln mit den Sitzen werden ganz pfiffig angetrieben: die beiden Z40 im Vordergrund sind fest mit dem Tragarm verbunden (siehe Metallachse ganz unten). In ihnen kämmen Zahnräder Z10 (von denen gerade keins zu sehen ist), die unterhalb der Gondeln zur Mitte führen und dort über Kegelräder die Gondeln antreiben. Die Drehung kommt zustande, weil beim Drehen des Tragarms die Z10 auf den Z40 abrollen müssen.