---
layout: "image"
title: "Claas gezogener Mähdrescher 02"
date: "2005-09-26T22:19:55"
picture: "Conv2005_CJ02.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5012
- /detailsf5b4.html
imported:
- "2019"
_4images_image_id: "5012"
_4images_cat_id: "388"
_4images_user_id: "10"
_4images_image_date: "2005-09-26T22:19:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5012 -->
