---
layout: "image"
title: "Der zweite Schlüter Trecker"
date: "2005-09-30T21:20:27"
picture: "Schlter_Trecker_2.jpg"
weight: "12"
konstrukteure: 
- "Besitzer:Claus Ludwig"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5042
- /detailsa077-2.html
imported:
- "2019"
_4images_image_id: "5042"
_4images_cat_id: "388"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5042 -->
