---
layout: "comment"
hidden: true
title: "899"
date: "2006-03-08T02:46:44"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Seilbahn im PendelbetriebSteuerung:
Die elektronische Steuerung basiert auf das klassische Modulsystem
der Elektronikbausteine. Die auslösenden Kontakte sind berührungslos
aus Lichtschranken aufgebaut. Eine Wartezeit ist zwischen jedem Fahr-
zyklus einkalkuliert (ca. 40 sek). Des weiteren sind beide Stützmasten
mit automatischen roten Positionslichtern (blinkend) ausgestattet.

Spannwerke:
Das feststehende Tragseil wird unterhalb der beiden Endstationen mit
jeweils 1 kg vorgespannt. Das separate Zugseil wird in der Primärstation
über zwei parallele Treibscheiben (Drehscheiben) angetrieben und in der
gegenüberliegenden Sekundärstation allein mit 300 g auf Spannung ge-
halten, wodurch das Zugseil äußerst leicht läuft. Beide Stationen werden
darüber hinaus mit jeweils 5 kg auf den Untergrund stabilisiert sowie mit
Klammern fixiert. (Die auftretenden Vibrationen des Motors können beide
Stationen ansonsten in Zugrichtung aufeinander "zuwandern" lassen!!!)

Gesamtmasse aller Spanngewichte: 12,3 kg

Die Seile sind vom Durchmesser her so bemessen, daß sie optimal in
allen relevanten Rollen sowie den Drehscheiben bündig hineinpassen.
Auf diese Weise wird einerseits ein Maximum an Reibung in den Treib-
scheiben erreicht und andererseits ein sicheres Überfahren der Stütz-
masten gewährleistet.

Bei derlei Konstruktionen sollte man immer darauf achten, daß niemals
eine tragende Verbindung mit nur einem Zapfen entsteht!

created by Triceratops 2005