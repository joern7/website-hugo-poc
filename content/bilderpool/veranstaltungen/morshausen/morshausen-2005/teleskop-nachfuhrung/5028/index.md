---
layout: "image"
title: "Martins Webcam Teleskop"
date: "2005-09-27T16:45:40"
picture: "P8252509.jpg"
weight: "4"
konstrukteure: 
- "remadus"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5028
- /details57b0.html
imported:
- "2019"
_4images_image_id: "5028"
_4images_cat_id: "503"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5028 -->
