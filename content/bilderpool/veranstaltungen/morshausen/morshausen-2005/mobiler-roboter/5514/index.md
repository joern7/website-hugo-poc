---
layout: "image"
title: "Mobiler Roboter"
date: "2005-12-22T16:10:07"
picture: "P8252531.jpg"
weight: "2"
konstrukteure: 
- "Joachim Jacobi / MisterWho"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5514
- /detailsfb02.html
imported:
- "2019"
_4images_image_id: "5514"
_4images_cat_id: "399"
_4images_user_id: "8"
_4images_image_date: "2005-12-22T16:10:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5514 -->
Der einzige Roboter, der zum Labyrinth-Wettbewerb angetreten ist und somit gewonnen hat obwohl er wegen Programmfehlern nicht gefahren ist.