---
layout: "image"
title: "Mobiler Roboter im Labyrinth"
date: "2005-12-22T16:10:07"
picture: "P8252550.jpg"
weight: "3"
konstrukteure: 
- "Joachim Jacobi / MisterWho"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5515
- /details229d.html
imported:
- "2019"
_4images_image_id: "5515"
_4images_cat_id: "399"
_4images_user_id: "8"
_4images_image_date: "2005-12-22T16:10:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5515 -->
