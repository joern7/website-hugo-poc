---
layout: "image"
title: "Harvester08.JPG"
date: "2005-10-19T22:06:57"
picture: "Harvester08.jpg"
weight: "13"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5094
- /detailsaf4a.html
imported:
- "2019"
_4images_image_id: "5094"
_4images_cat_id: "389"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5094 -->
Wenn mich nicht alles täuscht, hat da irgendwer etwas mit den Z10 gefrickelt...