---
layout: "image"
title: "Harvester09.JPG"
date: "2005-10-19T22:06:57"
picture: "Harvester09.jpg"
weight: "14"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5095
- /details87a4.html
imported:
- "2019"
_4images_image_id: "5095"
_4images_cat_id: "389"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5095 -->
