---
layout: "comment"
hidden: true
title: "1406"
date: "2006-09-27T20:26:35"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Selbst wenn --- es würde doch nichts nutzen, weil das schwarze ft-Differenzial vorher die Grätsche machen wird. Ohne Untersetzung hinter dem Gelenk sehe ich hier keine Chance, vor allem wenn man bedenkt, dass zu DEM Fahrgestell wohl ein Modell von > 4 kg gehören dürfte.