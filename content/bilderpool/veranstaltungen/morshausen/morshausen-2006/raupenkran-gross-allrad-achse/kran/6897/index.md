---
layout: "image"
title: "Kran_5"
date: "2006-09-24T01:42:50"
picture: "kran05.jpg"
weight: "5"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6897
- /details1c03-2.html
imported:
- "2019"
_4images_image_id: "6897"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6897 -->
