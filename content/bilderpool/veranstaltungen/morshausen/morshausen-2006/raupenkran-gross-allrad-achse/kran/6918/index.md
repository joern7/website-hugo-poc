---
layout: "image"
title: "Kran_26"
date: "2006-09-24T01:43:27"
picture: "kran26.jpg"
weight: "26"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6918
- /detailsa99a.html
imported:
- "2019"
_4images_image_id: "6918"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6918 -->
