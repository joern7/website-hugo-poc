---
layout: "image"
title: "Kran_2"
date: "2006-09-24T01:42:49"
picture: "kran02.jpg"
weight: "2"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6894
- /details9f62.html
imported:
- "2019"
_4images_image_id: "6894"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6894 -->
