---
layout: "image"
title: "Kran_6"
date: "2006-09-24T01:42:50"
picture: "kran06.jpg"
weight: "6"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6898
- /details781c.html
imported:
- "2019"
_4images_image_id: "6898"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6898 -->
