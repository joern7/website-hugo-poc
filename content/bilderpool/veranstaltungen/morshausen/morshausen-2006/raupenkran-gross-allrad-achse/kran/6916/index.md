---
layout: "image"
title: "Kran_24"
date: "2006-09-24T01:43:27"
picture: "kran24.jpg"
weight: "24"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6916
- /detailsfd9c.html
imported:
- "2019"
_4images_image_id: "6916"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6916 -->
