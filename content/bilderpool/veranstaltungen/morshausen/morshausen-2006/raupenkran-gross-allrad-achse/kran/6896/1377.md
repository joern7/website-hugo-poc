---
layout: "comment"
hidden: true
title: "1377"
date: "2006-09-25T13:29:24"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Da bin ich immer wieder froh nur "kleine" Modelle zu haben, Transport und Aufbau bzw. Abbau fordert leider ziemlich Zeit :(

Trotzdem Klasse anzusehen, großartiges Modell!