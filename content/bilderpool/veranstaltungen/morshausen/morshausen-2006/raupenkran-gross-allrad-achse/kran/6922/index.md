---
layout: "image"
title: "Kran_30"
date: "2006-09-24T01:43:27"
picture: "kran30.jpg"
weight: "30"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6922
- /detailsd925.html
imported:
- "2019"
_4images_image_id: "6922"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6922 -->
