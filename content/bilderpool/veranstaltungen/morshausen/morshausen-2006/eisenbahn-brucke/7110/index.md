---
layout: "image"
title: "convention2.jpg"
date: "2006-10-03T11:32:49"
picture: "convention2.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/7110
- /details9fac.html
imported:
- "2019"
_4images_image_id: "7110"
_4images_cat_id: "665"
_4images_user_id: "1"
_4images_image_date: "2006-10-03T11:32:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7110 -->
