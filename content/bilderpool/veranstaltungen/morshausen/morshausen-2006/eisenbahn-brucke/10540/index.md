---
layout: "image"
title: "Güterzug auf Brücke"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke8.jpg"
weight: "14"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10540
- /details4a3c.html
imported:
- "2019"
_4images_image_id: "10540"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10540 -->
Güterzug auf Brücke