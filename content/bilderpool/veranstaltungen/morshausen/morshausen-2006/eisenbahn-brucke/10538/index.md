---
layout: "image"
title: "Güterzug 2"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke6.jpg"
weight: "12"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10538
- /detailsbaca.html
imported:
- "2019"
_4images_image_id: "10538"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10538 -->
Wartet vor Signal