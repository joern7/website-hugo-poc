---
layout: "image"
title: "Zwei Züge auf dem Gleis"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke4.jpg"
weight: "10"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10536
- /detailsb9d4.html
imported:
- "2019"
_4images_image_id: "10536"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10536 -->
Züge werden über Blockschaltung geregelt