---
layout: "image"
title: "Claus Werner mit seinem LKW"
date: "2006-10-29T14:54:37"
picture: "cwl1.jpg"
weight: "6"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7243
- /details4995-2.html
imported:
- "2019"
_4images_image_id: "7243"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7243 -->
