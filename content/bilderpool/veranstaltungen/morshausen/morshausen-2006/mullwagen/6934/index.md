---
layout: "image"
title: "Müllwagen"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_046.jpg"
weight: "3"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6934
- /detailsc7f9.html
imported:
- "2019"
_4images_image_id: "6934"
_4images_cat_id: "672"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6934 -->
