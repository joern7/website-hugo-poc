---
layout: "image"
title: "Labyrinthroboter_2"
date: "2006-09-25T23:17:17"
picture: "remadus2.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6996
- /details6f5c.html
imported:
- "2019"
_4images_image_id: "6996"
_4images_cat_id: "666"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:17:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6996 -->
