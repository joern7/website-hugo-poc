---
layout: "image"
title: "Stacker91.JPG"
date: "2006-09-26T18:19:31"
picture: "Staker91.JPG"
weight: "3"
konstrukteure: 
- "Siegfried Kloster"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7007
- /details9e14.html
imported:
- "2019"
_4images_image_id: "7007"
_4images_cat_id: "669"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:19:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7007 -->
