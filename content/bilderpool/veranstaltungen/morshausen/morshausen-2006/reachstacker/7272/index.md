---
layout: "image"
title: "LKW wo die Reachtaker die Container auf platziert"
date: "2006-10-29T19:01:47"
picture: "sk2.jpg"
weight: "6"
konstrukteure: 
- "Siegfried Kloster"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7272
- /details5165-2.html
imported:
- "2019"
_4images_image_id: "7272"
_4images_cat_id: "669"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7272 -->
