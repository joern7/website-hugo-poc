---
layout: "image"
title: "Karussell in Bewegung"
date: "2007-05-31T09:45:03"
picture: "schwebebahn06.jpg"
weight: "29"
konstrukteure: 
- "Ralf geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10621
- /detailsf2d0.html
imported:
- "2019"
_4images_image_id: "10621"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10621 -->
