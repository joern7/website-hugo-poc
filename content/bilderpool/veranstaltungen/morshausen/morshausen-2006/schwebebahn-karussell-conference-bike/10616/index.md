---
layout: "image"
title: "Karussell"
date: "2007-05-31T09:44:40"
picture: "schwebebahn01.jpg"
weight: "24"
konstrukteure: 
- "Ralf geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10616
- /detailsb6cd.html
imported:
- "2019"
_4images_image_id: "10616"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10616 -->
die angehängten Modelle treiben das Karussell an