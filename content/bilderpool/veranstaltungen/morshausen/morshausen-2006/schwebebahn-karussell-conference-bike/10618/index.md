---
layout: "image"
title: "Propellerflugzeug"
date: "2007-05-31T09:45:03"
picture: "schwebebahn03.jpg"
weight: "26"
konstrukteure: 
- "Ralf geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10618
- /details3bb5.html
imported:
- "2019"
_4images_image_id: "10618"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10618 -->
mit einem Motor