---
layout: "image"
title: "Sonderteil"
date: "2007-05-31T09:45:20"
picture: "schwebebahn13.jpg"
weight: "36"
konstrukteure: 
- "Ralf geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10628
- /detailsf2f5.html
imported:
- "2019"
_4images_image_id: "10628"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10628 -->
Wer erkennt den neuen ft-Baustein ?