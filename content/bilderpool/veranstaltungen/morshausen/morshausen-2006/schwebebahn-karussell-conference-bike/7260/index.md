---
layout: "image"
title: "Karussell"
date: "2006-10-29T19:01:17"
picture: "rg1.jpg"
weight: "11"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7260
- /detailsaf75.html
imported:
- "2019"
_4images_image_id: "7260"
_4images_cat_id: "680"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7260 -->
