---
layout: "image"
title: "Karussell"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_006.jpg"
weight: "22"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9968
- /detailsb95e-2.html
imported:
- "2019"
_4images_image_id: "9968"
_4images_cat_id: "680"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9968 -->
