---
layout: "image"
title: "Sortieranlage03.JPG"
date: "2006-10-02T15:57:27"
picture: "Sortieranlage03.JPG"
weight: "4"
konstrukteure: 
- "Fredy"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7065
- /details62b9.html
imported:
- "2019"
_4images_image_id: "7065"
_4images_cat_id: "678"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T15:57:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7065 -->
Beide Auswurfstationen sind pneumatisch betätigt. Die zweite (links im Bild) verwendet zur Ansteuerung ein P-Ventil von Lego, das man ohne großen Aufwand auch ohne Menschenhand ansteuern kann.