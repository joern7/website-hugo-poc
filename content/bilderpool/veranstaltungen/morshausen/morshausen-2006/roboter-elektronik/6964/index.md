---
layout: "image"
title: "LKW_1"
date: "2006-09-24T22:49:47"
picture: "thkais2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6964
- /detailsc223-2.html
imported:
- "2019"
_4images_image_id: "6964"
_4images_cat_id: "662"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T22:49:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6964 -->
