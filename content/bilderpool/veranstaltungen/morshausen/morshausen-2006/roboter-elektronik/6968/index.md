---
layout: "image"
title: "Finger und Platine"
date: "2006-09-24T22:49:47"
picture: "thkais6.jpg"
weight: "6"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6968
- /details47e3.html
imported:
- "2019"
_4images_image_id: "6968"
_4images_cat_id: "662"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T22:49:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6968 -->
Der Daumen als Größenvergleich zählt nicht wirklich da der Inhaber des Daumens überdurchschnittlich große Finger hat ;-).
