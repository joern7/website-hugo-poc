---
layout: "image"
title: "Labyrinthroboter"
date: "2007-05-31T09:44:39"
picture: "roboterelektronik2.jpg"
weight: "10"
konstrukteure: 
- "Thomas kaiser"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10613
- /details6549.html
imported:
- "2019"
_4images_image_id: "10613"
_4images_cat_id: "662"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10613 -->
mit Abstandsanzeige