---
layout: "image"
title: "Display an RoboPro"
date: "2007-05-31T09:44:39"
picture: "roboterelektronik1.jpg"
weight: "9"
konstrukteure: 
- "Thomas kaiser"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10612
- /details28af.html
imported:
- "2019"
_4images_image_id: "10612"
_4images_cat_id: "662"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10612 -->
mit Textanzeige