---
layout: "image"
title: "Pneumatik Bearbeitungszentrum"
date: "2007-05-31T09:44:07"
picture: "pneuzentrum1.jpg"
weight: "4"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10607
- /details8b22.html
imported:
- "2019"
_4images_image_id: "10607"
_4images_cat_id: "695"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10607 -->
gerade Leer