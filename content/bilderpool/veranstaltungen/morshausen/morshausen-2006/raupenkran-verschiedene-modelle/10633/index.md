---
layout: "image"
title: "Roboter mit Schrittmotoren"
date: "2007-05-31T09:45:20"
picture: "verschmodelle2.jpg"
weight: "29"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10633
- /details94e0.html
imported:
- "2019"
_4images_image_id: "10633"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10633 -->
von Obenrechts