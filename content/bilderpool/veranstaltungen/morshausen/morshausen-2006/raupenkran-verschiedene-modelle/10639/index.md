---
layout: "image"
title: "Krangewichte"
date: "2007-05-31T09:45:33"
picture: "verschmodelle8.jpg"
weight: "35"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10639
- /details8b96-2.html
imported:
- "2019"
_4images_image_id: "10639"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10639 -->
auf ft Platten