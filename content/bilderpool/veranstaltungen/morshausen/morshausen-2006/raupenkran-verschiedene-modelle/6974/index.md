---
layout: "image"
title: "Space Shuttle"
date: "2006-09-25T22:45:19"
picture: "holger4.jpg"
weight: "14"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6974
- /detailsc88e.html
imported:
- "2019"
_4images_image_id: "6974"
_4images_cat_id: "660"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:45:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6974 -->
