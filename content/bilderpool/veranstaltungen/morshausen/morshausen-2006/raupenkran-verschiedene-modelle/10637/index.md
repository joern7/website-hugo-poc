---
layout: "image"
title: "Schwebebahn"
date: "2007-05-31T09:45:33"
picture: "verschmodelle6.jpg"
weight: "33"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10637
- /details616f.html
imported:
- "2019"
_4images_image_id: "10637"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:33"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10637 -->
von Links