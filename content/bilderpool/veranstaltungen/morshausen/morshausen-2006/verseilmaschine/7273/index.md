---
layout: "image"
title: "Verseilmaschine"
date: "2006-10-29T19:01:47"
picture: "se1.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7273
- /details1978.html
imported:
- "2019"
_4images_image_id: "7273"
_4images_cat_id: "676"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7273 -->
