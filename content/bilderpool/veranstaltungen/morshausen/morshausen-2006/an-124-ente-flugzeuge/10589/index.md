---
layout: "image"
title: "Propellerflugzeug"
date: "2007-05-31T09:43:46"
picture: "flugzeuge1.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10589
- /detailsfba8-2.html
imported:
- "2019"
_4images_image_id: "10589"
_4images_cat_id: "664"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10589 -->
mit einziehbaren Fahrwerk