---
layout: "image"
title: "Ente 2CV"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_084.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6960
- /detailsaa39-3.html
imported:
- "2019"
_4images_image_id: "6960"
_4images_cat_id: "664"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6960 -->
