---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:24"
picture: "containerterminal08.jpg"
weight: "17"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10583
- /details9920-3.html
imported:
- "2019"
_4images_image_id: "10583"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10583 -->
Volle Höhe