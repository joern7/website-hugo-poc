---
layout: "image"
title: "Spielen mit ft 2"
date: "2006-10-16T19:01:02"
picture: "P9230088.jpg"
weight: "7"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/7192
- /details493b.html
imported:
- "2019"
_4images_image_id: "7192"
_4images_cat_id: "673"
_4images_user_id: "381"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7192 -->
Auch mein Sohn war sehr begeistert. Auf der Rückfahrt kam die Frage: "Du Papa, warum haben die anderen alle viel mehr fischertechnik als wir?"
Die Antort bin ich bis jetzt noch schuldig.