---
layout: "image"
title: "Containerterminal_3"
date: "2006-09-25T22:52:56"
picture: "warwel3.jpg"
weight: "4"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6982
- /details9c4d.html
imported:
- "2019"
_4images_image_id: "6982"
_4images_cat_id: "673"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:52:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6982 -->
