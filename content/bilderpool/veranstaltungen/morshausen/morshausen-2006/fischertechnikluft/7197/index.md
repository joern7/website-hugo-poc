---
layout: "image"
title: "Der 150m tiefe Brunnen"
date: "2006-10-16T19:01:32"
picture: "P9240136.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/7197
- /detailsf2c0.html
imported:
- "2019"
_4images_image_id: "7197"
_4images_cat_id: "677"
_4images_user_id: "381"
_4images_image_date: "2006-10-16T19:01:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7197 -->
Ein paar Impressionen vom Schlossberg