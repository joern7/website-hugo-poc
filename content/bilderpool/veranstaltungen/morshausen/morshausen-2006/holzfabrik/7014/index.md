---
layout: "image"
title: "Holzfab_06.JPG"
date: "2006-09-26T18:45:41"
picture: "Holzfab_06.JPG"
weight: "9"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7014
- /detailsede4.html
imported:
- "2019"
_4images_image_id: "7014"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7014 -->
Station 4: Sortieren, die kurzen nach hinten, die langen hier raus. 

Die Handarbeit zwischen den Stationen ist gewollt, damit die Kids etwas zu tun haben.