---
layout: "image"
title: "Sägewerk_1"
date: "2006-09-25T22:48:13"
picture: "jansen1.jpg"
weight: "3"
konstrukteure: 
- "Familie Janssen"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6978
- /details6a2d.html
imported:
- "2019"
_4images_image_id: "6978"
_4images_cat_id: "667"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:48:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6978 -->
