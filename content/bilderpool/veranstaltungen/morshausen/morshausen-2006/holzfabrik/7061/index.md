---
layout: "image"
title: "Holzfabrik"
date: "2006-10-02T02:44:55"
picture: "Mrshausen_135.jpg"
weight: "17"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7061
- /details532d.html
imported:
- "2019"
_4images_image_id: "7061"
_4images_cat_id: "667"
_4images_user_id: "130"
_4images_image_date: "2006-10-02T02:44:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7061 -->
Hier wird das Förderband bestückt