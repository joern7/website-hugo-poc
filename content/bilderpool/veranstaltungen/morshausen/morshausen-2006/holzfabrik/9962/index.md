---
layout: "image"
title: "Holzfabrik"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_016.jpg"
weight: "20"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9962
- /detailse4ef.html
imported:
- "2019"
_4images_image_id: "9962"
_4images_cat_id: "667"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9962 -->
