---
layout: "image"
title: "Holzfab_02.JPG"
date: "2006-09-26T18:42:10"
picture: "Holzfab_02.JPG"
weight: "6"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7011
- /detailsed1a-2.html
imported:
- "2019"
_4images_image_id: "7011"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:42:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7011 -->
Station 2: Zersägen