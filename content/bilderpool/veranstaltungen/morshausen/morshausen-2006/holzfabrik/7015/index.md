---
layout: "image"
title: "Holzfab_07.JPG"
date: "2006-09-26T18:46:06"
picture: "Holzfab_07.JPG"
weight: "10"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7015
- /detailsdd8e-2.html
imported:
- "2019"
_4images_image_id: "7015"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:46:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7015 -->
