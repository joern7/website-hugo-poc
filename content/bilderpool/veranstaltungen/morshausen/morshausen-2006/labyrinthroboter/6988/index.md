---
layout: "image"
title: "Roboter"
date: "2006-09-25T23:01:37"
picture: "who1.jpg"
weight: "1"
konstrukteure: 
- "MisterWho"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6988
- /details8a5d.html
imported:
- "2019"
_4images_image_id: "6988"
_4images_cat_id: "671"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:01:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6988 -->
