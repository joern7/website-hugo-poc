---
layout: "image"
title: "Bagger"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_051.jpg"
weight: "1"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6937
- /detailsdd26.html
imported:
- "2019"
_4images_image_id: "6937"
_4images_cat_id: "675"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6937 -->
