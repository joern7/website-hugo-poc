---
layout: "image"
title: "RM2"
date: "2006-10-05T20:05:38"
picture: "RoboMax_002.jpg"
weight: "2"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7136
- /details6454.html
imported:
- "2019"
_4images_image_id: "7136"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7136 -->
