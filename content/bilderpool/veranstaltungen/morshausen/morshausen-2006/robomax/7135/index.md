---
layout: "image"
title: "RM1"
date: "2006-10-05T20:05:37"
picture: "RoboMax_001.jpg"
weight: "1"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7135
- /detailsdd54.html
imported:
- "2019"
_4images_image_id: "7135"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7135 -->
