---
layout: "image"
title: "RM3"
date: "2006-10-05T20:05:38"
picture: "RoboMax_003.jpg"
weight: "3"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7137
- /detailse6cb.html
imported:
- "2019"
_4images_image_id: "7137"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7137 -->
