---
layout: "image"
title: "P9230054.JPG"
date: "2006-10-02T16:14:25"
picture: "P9230054.JPG"
weight: "13"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7079
- /detailsf264.html
imported:
- "2019"
_4images_image_id: "7079"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:14:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7079 -->
