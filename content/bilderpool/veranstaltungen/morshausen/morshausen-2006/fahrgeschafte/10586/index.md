---
layout: "image"
title: "Kettenkarussell"
date: "2007-05-31T09:43:24"
picture: "fahrgeschaefte1.jpg"
weight: "22"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10586
- /details27f5.html
imported:
- "2019"
_4images_image_id: "10586"
_4images_cat_id: "668"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10586 -->
