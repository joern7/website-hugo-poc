---
layout: "overview"
title: "Heikos Dosen-Stapel-Roboter"
date: 2020-02-22T08:53:40+01:00
legacy_id:
- /php/categories/162
- /categories3543.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=162 --> 
Roboterarm mit fünf Bewegungsachsen und Joysticksteuerung.