---
layout: "image"
title: "Bagger"
date: "2003-10-23T20:36:30"
picture: "IMG_0423.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/1840
- /detailseea6-2.html
imported:
- "2019"
_4images_image_id: "1840"
_4images_cat_id: "180"
_4images_user_id: "6"
_4images_image_date: "2003-10-23T20:36:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1840 -->
