---
layout: "image"
title: "Mähdrescher"
date: "2003-09-28T14:48:21"
picture: "scan_5.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/1740
- /details1f2f.html
imported:
- "2019"
_4images_image_id: "1740"
_4images_cat_id: "166"
_4images_user_id: "9"
_4images_image_date: "2003-09-28T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1740 -->
