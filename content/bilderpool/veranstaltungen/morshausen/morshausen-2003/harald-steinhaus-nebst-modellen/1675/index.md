---
layout: "image"
title: "DSCF0019"
date: "2003-09-28T09:56:47"
picture: "DSCF0019.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "NN"
keywords: ["Traktor", "Fendt", "Geräteträger"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1675
- /detailscb86.html
imported:
- "2019"
_4images_image_id: "1675"
_4images_cat_id: "166"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1675 -->
