---
layout: "image"
title: "IMGP3867"
date: "2003-09-28T09:56:47"
picture: "IMGP3867.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "NN"
keywords: ["Bagger", "Pneumatikzylinder", "Eigenbau"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1684
- /details8b9e.html
imported:
- "2019"
_4images_image_id: "1684"
_4images_cat_id: "166"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1684 -->
