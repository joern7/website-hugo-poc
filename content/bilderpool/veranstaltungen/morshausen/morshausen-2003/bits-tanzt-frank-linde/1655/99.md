---
layout: "comment"
hidden: true
title: "99"
date: "2003-09-30T09:01:09"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Nur nicht aufgeben...Hier klemmt der Erbauer das Programmierboard an die Steuerelektronik, in der Hoffnung, er könnte mal eben schnell etwas Software in den Controller hacken, um den Roboterarm doch noch zum Leben zu erwecken. Leider mußte er aber Heiko (aka wahsager) zähneknirschend zustimmen: Während der Convention kann man solche Arbeiten definitiv vergessen.