---
layout: "overview"
title: "Der mit den Bits tanzt: Frank Linde"
date: 2020-02-22T08:53:39+01:00
legacy_id:
- /php/categories/161
- /categories12c2.html
- /categories60f4.html
- /categories73f2.html
- /categories20c0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=161 --> 
Ein fahrbarer Roboterarm mit selbstgebauter Steuerung.