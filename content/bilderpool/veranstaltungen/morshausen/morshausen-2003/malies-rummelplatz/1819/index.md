---
layout: "image"
title: "Schiffschaukel"
date: "2003-10-09T11:40:57"
picture: "Schiffschaukel_von_MaLie_2.jpg"
weight: "22"
konstrukteure: 
- "MaLie"
fotografen:
- "NN"
keywords: ["Fahrgeschäft", "Kirmesmodell"]
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1819
- /details47c5.html
imported:
- "2019"
_4images_image_id: "1819"
_4images_cat_id: "149"
_4images_user_id: "130"
_4images_image_date: "2003-10-09T11:40:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1819 -->
