---
layout: "image"
title: ","
date: "2003-10-08T17:54:41"
picture: "riesenrad_stephan_gegenlicht.jpg"
weight: "13"
konstrukteure: 
- "Holger Howey"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/1814
- /details967f-2.html
imported:
- "2019"
_4images_image_id: "1814"
_4images_cat_id: "154"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:54:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1814 -->
