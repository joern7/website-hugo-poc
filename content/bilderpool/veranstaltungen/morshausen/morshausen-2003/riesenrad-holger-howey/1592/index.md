---
layout: "image"
title: "DSCF0065"
date: "2003-09-28T09:48:23"
picture: "DSCF0065.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1592
- /detailsad92.html
imported:
- "2019"
_4images_image_id: "1592"
_4images_cat_id: "153"
_4images_user_id: "34"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1592 -->
Riesenrad 

Dieses RR hat einen Durchmesser von 2 Metern mit einer Gesammthöhe von ca 2,25 Metern. In den 48 Kabinen a. zwei Sitzen können also 96 ft-Personen mitfahren. 
Würde man das Rad und die Außenträger nachbauen, so braucht man knapp 300 der 120er-Statikträger von fischertechnik. Bei starker Hitze kann man das Modell hervorragend als Ventilator einsetzen. 


Technische Daten (ca.): 
48 Gondeln mit 96 Sitzen 
2 Meter Raddurchmesser 
2,20 Gesamthöhe 
300 120er Statikträger 
170 Statiklaschen 
556 Statikstreben 
mehr als 1100 S-Riegel 
(Wer möchte, kann gerne nachzählen und mir die 
genaue Anzahl sagen J) 
Direktantrieb über einen Conradmotor (244040) 
mit einem Getriebe von 312:1 
mit max. 17 Umdrehungen pro Minute J