---
layout: "image"
title: "n2-0045"
date: "2003-09-28T09:48:23"
picture: "n2-0045.JPG"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1596
- /details335a-2.html
imported:
- "2019"
_4images_image_id: "1596"
_4images_cat_id: "153"
_4images_user_id: "34"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1596 -->
Transport 

Wie transportiert man Großmodelle? Hier sind die zwei Riesenräder und noch unsere Kleinmodelle drinn. Rand - voll der Wagen.