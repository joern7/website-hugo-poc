---
layout: "overview"
title: "Riesenrad von Holger Howey"
date: 2020-02-22T08:53:33+01:00
legacy_id:
- /php/categories/153
- /categoriesade8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=153 --> 
Groß, aber grazil. Und dennoch: "Nie wieder so ein großes Modell"