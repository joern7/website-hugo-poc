---
layout: "image"
title: "Styropor-Fertigungsanlage"
date: "2003-10-03T15:01:46"
picture: "P9200022.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1789
- /details7bdd.html
imported:
- "2019"
_4images_image_id: "1789"
_4images_cat_id: "317"
_4images_user_id: "4"
_4images_image_date: "2003-10-03T15:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1789 -->
