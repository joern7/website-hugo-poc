---
layout: "image"
title: "IMGP3830"
date: "2003-09-28T10:04:38"
picture: "IMGP3830.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/1714
- /details0065-2.html
imported:
- "2019"
_4images_image_id: "1714"
_4images_cat_id: "171"
_4images_user_id: "8"
_4images_image_date: "2003-09-28T10:04:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1714 -->
Gleichlaufkettenfahrzeug und Lowrider von MisterWho 

Gleichlaufkettenfahrzeug: Im Forum wurde mal eine Antriebsidee mit zwei Differentialen für den perfekten Gleichlauf bei mobilen Robotern besprochen. Dabei sorgt ein Motor für den Geradeauslauf und ein anderer für das Drehen. Das Modell hat genau solch einen Antrieb. Aber mehr hat es auch nicht. Gesteuert wird das mit der ft Fernsteuerung. Es ist eher ein Experimentiermodell. 

Lowrider: Eigentlich auch eher ein Experimentiermodell. Alle Räder sind einzeln an der Karosserie aufgehängt und können über Pneumaticzylinder gehoben und gesenkt werden. Das Problem ist hier noch, dass die Räder sich beim Heben nach innen bewegen und dabei am Tisch hängen bleiben. Das "Fahrzeug" wird also beim Heben schmaler. An einer komplett anderen Version wird schon gearbeitet.