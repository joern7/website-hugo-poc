---
layout: "image"
title: "tst031.JPG"
date: "2007-09-23T19:06:41"
picture: "tst031.JPG"
weight: "9"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11928
- /detailsa30f.html
imported:
- "2019"
_4images_image_id: "11928"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11928 -->
Die Vorderachse ist etwas geneigt eingebaut. Die Lenkung erfolgt über einen Modellbau-Servo, der den blauen Hebel unten betätigt.