---
layout: "image"
title: "tst029.JPG"
date: "2007-09-23T19:02:35"
picture: "tst029.JPG"
weight: "7"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11926
- /details0799-2.html
imported:
- "2019"
_4images_image_id: "11926"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11926 -->
Andernorts kann man mit so einem Gerät versuchen, Teddybären zu fischen und in den Ausgabeschacht zu befördern. Hier gibt es etwas viel besseres.