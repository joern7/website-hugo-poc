---
layout: "image"
title: "Roboter"
date: "2007-09-25T09:58:37"
picture: "tst2.jpg"
weight: "15"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12000
- /details2dfc-3.html
imported:
- "2019"
_4images_image_id: "12000"
_4images_cat_id: "1066"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:58:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12000 -->
