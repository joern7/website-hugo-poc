---
layout: "image"
title: "Greifer-Spiel"
date: "2007-09-25T09:58:37"
picture: "tst1.jpg"
weight: "14"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11999
- /details0d03-2.html
imported:
- "2019"
_4images_image_id: "11999"
_4images_cat_id: "1066"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:58:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11999 -->
