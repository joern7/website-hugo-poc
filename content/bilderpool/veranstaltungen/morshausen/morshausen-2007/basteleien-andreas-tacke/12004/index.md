---
layout: "image"
title: "Steuerknüppel"
date: "2007-09-25T09:58:38"
picture: "tst6.jpg"
weight: "19"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12004
- /details0c5b.html
imported:
- "2019"
_4images_image_id: "12004"
_4images_cat_id: "1066"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:58:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12004 -->
