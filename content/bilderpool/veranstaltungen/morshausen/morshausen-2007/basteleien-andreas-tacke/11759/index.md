---
layout: "image"
title: "rrb69.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb69.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11759
- /details7f24.html
imported:
- "2019"
_4images_image_id: "11759"
_4images_cat_id: "1066"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11759 -->
