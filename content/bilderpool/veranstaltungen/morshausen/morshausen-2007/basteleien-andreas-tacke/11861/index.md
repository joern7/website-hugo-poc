---
layout: "image"
title: "Drehscheibe mit Lagerung"
date: "2007-09-18T15:08:28"
picture: "FT-Mrshausen-2007_074.jpg"
weight: "6"
konstrukteure: 
- "Andreas Tacke (FT-Nobelpreise Mörshausen-2007)"
fotografen:
- "Peter Damen (Alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11861
- /details0edc-2.html
imported:
- "2019"
_4images_image_id: "11861"
_4images_cat_id: "1066"
_4images_user_id: "22"
_4images_image_date: "2007-09-18T15:08:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11861 -->
