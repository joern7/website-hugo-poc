---
layout: "image"
title: "Roboter auf großem Fuß"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk066.jpg"
weight: "9"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11647
- /detailsef6e-2.html
imported:
- "2019"
_4images_image_id: "11647"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11647 -->
