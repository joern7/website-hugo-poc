---
layout: "image"
title: "Industrieroboter"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk016.jpg"
weight: "1"
konstrukteure: 
- "marmac"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11597
- /detailsd008.html
imported:
- "2019"
_4images_image_id: "11597"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11597 -->
Wenn ich es richtig verstanden habe, sollte der auch etwas mit dem Projektionssystem zu tun haben.