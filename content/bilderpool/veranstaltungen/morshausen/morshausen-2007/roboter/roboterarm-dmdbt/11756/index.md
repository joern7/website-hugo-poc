---
layout: "image"
title: "rrb66.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb66.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11756
- /detailsd417.html
imported:
- "2019"
_4images_image_id: "11756"
_4images_cat_id: "1035"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11756 -->
