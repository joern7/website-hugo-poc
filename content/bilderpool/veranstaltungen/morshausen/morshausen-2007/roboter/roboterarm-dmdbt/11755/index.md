---
layout: "image"
title: "rrb65.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb65.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11755
- /details9c6e.html
imported:
- "2019"
_4images_image_id: "11755"
_4images_cat_id: "1035"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11755 -->
