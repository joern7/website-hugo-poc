---
layout: "image"
title: "rrb56.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb56.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11746
- /detailsede9-2.html
imported:
- "2019"
_4images_image_id: "11746"
_4images_cat_id: "1035"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11746 -->
