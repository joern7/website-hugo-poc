---
layout: "image"
title: "Greifer"
date: "2007-09-16T16:53:32"
picture: "greifarm1.jpg"
weight: "1"
konstrukteure: 
- "Frank"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11476
- /details7f18.html
imported:
- "2019"
_4images_image_id: "11476"
_4images_cat_id: "1035"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11476 -->
