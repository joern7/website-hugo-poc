---
layout: "image"
title: "Roboterarm mit dezentraler Mikrocontroller-Steuerung"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk073.jpg"
weight: "8"
konstrukteure: 
- "dmdbt"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11654
- /details5b0e.html
imported:
- "2019"
_4images_image_id: "11654"
_4images_cat_id: "1035"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11654 -->
Jeder Motor wird von je einem Prototypen einer zukünftigen dezentralen Steuerung auf Atmel-Basis gesteuert. Frank stellte das am Sonntag ausführlich vor und stellte je nach Abnahmemenge eine kleine professionelle Serienfertigung in Aussicht!