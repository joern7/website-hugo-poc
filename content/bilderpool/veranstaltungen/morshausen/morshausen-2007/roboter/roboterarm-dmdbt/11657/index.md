---
layout: "image"
title: "Roboterarm mit dezentraler Mikrocontroller-Steuerung"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk076.jpg"
weight: "11"
konstrukteure: 
- "dmdbt"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11657
- /details01d1-3.html
imported:
- "2019"
_4images_image_id: "11657"
_4images_cat_id: "1035"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11657 -->
