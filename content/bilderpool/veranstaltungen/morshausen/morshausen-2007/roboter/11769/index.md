---
layout: "image"
title: "rrb79.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb79.jpg"
weight: "23"
konstrukteure: 
- "TST"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11769
- /detailsc8c8-3.html
imported:
- "2019"
_4images_image_id: "11769"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11769 -->
