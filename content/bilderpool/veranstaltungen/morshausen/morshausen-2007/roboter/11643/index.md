---
layout: "image"
title: "Dosen-Einsammel-Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk062.jpg"
weight: "5"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11643
- /detailsc2dc.html
imported:
- "2019"
_4images_image_id: "11643"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11643 -->
Die senkrechten gelben Statikstreben werden nach oben gezogen. Dadurch werden die am unteren Bildrand (hier waagerecht) liegenden  Streben in der Mitte angehoben, wodurch wiederum der Greifer geschlossen wird. Der gewünschte Effekt mit ganz wenigen Teilen erreicht - was will man mehr?