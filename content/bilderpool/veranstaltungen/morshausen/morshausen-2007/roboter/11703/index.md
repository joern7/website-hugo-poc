---
layout: "image"
title: "rrb13.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb13.jpg"
weight: "17"
konstrukteure: 
- "MisterWho"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11703
- /details8511-2.html
imported:
- "2019"
_4images_image_id: "11703"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11703 -->
