---
layout: "image"
title: "rrb59.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb59.jpg"
weight: "16"
konstrukteure: 
- "Severin"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11749
- /details56d4.html
imported:
- "2019"
_4images_image_id: "11749"
_4images_cat_id: "1045"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11749 -->
