---
layout: "image"
title: "rrb61.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb61.jpg"
weight: "18"
konstrukteure: 
- "Severin"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11751
- /details77a4.html
imported:
- "2019"
_4images_image_id: "11751"
_4images_cat_id: "1045"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11751 -->
