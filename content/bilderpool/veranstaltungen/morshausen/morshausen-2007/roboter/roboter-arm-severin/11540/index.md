---
layout: "image"
title: "Greifarm"
date: "2007-09-16T17:09:08"
picture: "roboterarm10.jpg"
weight: "10"
konstrukteure: 
- "Severin"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11540
- /details33f2-2.html
imported:
- "2019"
_4images_image_id: "11540"
_4images_cat_id: "1045"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11540 -->
