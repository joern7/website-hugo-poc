---
layout: "image"
title: "Roboterarm"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk098.jpg"
weight: "13"
konstrukteure: 
- "Severin"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11679
- /details3882-2.html
imported:
- "2019"
_4images_image_id: "11679"
_4images_cat_id: "1045"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11679 -->
