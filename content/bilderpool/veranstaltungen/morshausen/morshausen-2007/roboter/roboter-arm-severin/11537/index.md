---
layout: "image"
title: "Greifarm"
date: "2007-09-16T17:09:08"
picture: "roboterarm07.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11537
- /details9a0f.html
imported:
- "2019"
_4images_image_id: "11537"
_4images_cat_id: "1045"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11537 -->
