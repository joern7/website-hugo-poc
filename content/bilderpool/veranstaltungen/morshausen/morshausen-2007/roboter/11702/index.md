---
layout: "image"
title: "rrb12.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb12.jpg"
weight: "16"
konstrukteure: 
- "MisterWho"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11702
- /details9e46.html
imported:
- "2019"
_4images_image_id: "11702"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11702 -->
