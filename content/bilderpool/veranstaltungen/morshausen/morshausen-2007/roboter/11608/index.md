---
layout: "image"
title: "Thkais Labyrinthroboter"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk027.jpg"
weight: "2"
konstrukteure: 
- "Thkais"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11608
- /details7008.html
imported:
- "2019"
_4images_image_id: "11608"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11608 -->
Incl. LC-Display, programmiert in Thkais Robo-Basic.