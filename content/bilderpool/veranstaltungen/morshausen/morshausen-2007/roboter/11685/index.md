---
layout: "image"
title: "Industrieroboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk104.jpg"
weight: "11"
konstrukteure: 
- "TST"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11685
- /details61f7.html
imported:
- "2019"
_4images_image_id: "11685"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "104"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11685 -->
