---
layout: "image"
title: "rrb78.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb78.jpg"
weight: "22"
konstrukteure: 
- "TST"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11768
- /details087a-2.html
imported:
- "2019"
_4images_image_id: "11768"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11768 -->
