---
layout: "image"
title: "rrb07.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb07.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11697
- /details4d9d.html
imported:
- "2019"
_4images_image_id: "11697"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11697 -->
