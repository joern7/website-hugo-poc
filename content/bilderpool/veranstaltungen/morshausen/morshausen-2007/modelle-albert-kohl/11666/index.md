---
layout: "image"
title: "Gleislege-Zug"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk085.jpg"
weight: "3"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11666
- /details1375.html
imported:
- "2019"
_4images_image_id: "11666"
_4images_cat_id: "1057"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11666 -->
