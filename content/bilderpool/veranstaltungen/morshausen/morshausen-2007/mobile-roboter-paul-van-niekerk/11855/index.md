---
layout: "image"
title: "mobiler roboter"
date: "2007-09-18T12:45:08"
picture: "mobilerroboter1.jpg"
weight: "1"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11855
- /detailsb06d.html
imported:
- "2019"
_4images_image_id: "11855"
_4images_cat_id: "1059"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T12:45:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11855 -->
