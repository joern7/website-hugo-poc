---
layout: "image"
title: "mobiler Roboter"
date: "2007-09-18T12:45:08"
picture: "mobilerroboter2.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11856
- /details35c3.html
imported:
- "2019"
_4images_image_id: "11856"
_4images_cat_id: "1059"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T12:45:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11856 -->
