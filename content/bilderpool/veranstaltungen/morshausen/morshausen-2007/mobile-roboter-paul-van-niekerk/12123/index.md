---
layout: "image"
title: "Sharp Abstandsensoren"
date: "2007-10-03T16:36:25"
picture: "robter6.jpg"
weight: "9"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12123
- /details8e47.html
imported:
- "2019"
_4images_image_id: "12123"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12123 -->
