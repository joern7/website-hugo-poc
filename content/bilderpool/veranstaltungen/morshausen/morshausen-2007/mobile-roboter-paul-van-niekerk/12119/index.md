---
layout: "image"
title: "Display"
date: "2007-10-03T16:36:25"
picture: "robter2.jpg"
weight: "5"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12119
- /details9881.html
imported:
- "2019"
_4images_image_id: "12119"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12119 -->
