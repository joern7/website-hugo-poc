---
layout: "image"
title: "Spurensucher"
date: "2007-10-03T16:36:25"
picture: "robter7.jpg"
weight: "10"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12124
- /details6865-2.html
imported:
- "2019"
_4images_image_id: "12124"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12124 -->
