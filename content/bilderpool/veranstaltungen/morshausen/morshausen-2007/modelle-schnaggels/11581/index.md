---
layout: "image"
title: "Treppe-Robot"
date: "2007-09-16T22:07:26"
picture: "FT-Mrshausen-2007_171.jpg"
weight: "4"
konstrukteure: 
- "Thomas Brestlinger"
fotografen:
- "Peter Damen (Alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11581
- /details7d64.html
imported:
- "2019"
_4images_image_id: "11581"
_4images_cat_id: "1047"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11581 -->
