---
layout: "image"
title: "rrb21.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb21.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11711
- /detailse4d8.html
imported:
- "2019"
_4images_image_id: "11711"
_4images_cat_id: "1601"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11711 -->
