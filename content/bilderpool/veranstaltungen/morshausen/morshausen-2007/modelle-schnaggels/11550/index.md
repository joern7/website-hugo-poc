---
layout: "image"
title: "Robo Max"
date: "2007-09-16T19:38:31"
picture: "robomax4.jpg"
weight: "2"
konstrukteure: 
- "schnaggels"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11550
- /details1161.html
imported:
- "2019"
_4images_image_id: "11550"
_4images_cat_id: "1047"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11550 -->
