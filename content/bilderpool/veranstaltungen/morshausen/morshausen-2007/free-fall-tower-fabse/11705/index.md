---
layout: "image"
title: "rrb15.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb15.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: ["icare", "solarsegler", "flugzeug", "weiß", "weiße", "bausteine"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11705
- /details595e-2.html
imported:
- "2019"
_4images_image_id: "11705"
_4images_cat_id: "1048"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11705 -->
