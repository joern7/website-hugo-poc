---
layout: "image"
title: "Chaos auf 20x30 I"
date: "2007-09-18T10:55:20"
picture: "PICT5636.jpg"
weight: "10"
konstrukteure: 
- "Erik Andresen"
fotografen:
- "Heiko Engelke"
keywords: ["mobiler", "roboter", "defiant"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11798
- /details6bf9-2.html
imported:
- "2019"
_4images_image_id: "11798"
_4images_cat_id: "1034"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T10:55:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11798 -->
Defiant beim Zerlegen seines Modells. Die Innereien ernteten einigen Respekt.