---
layout: "image"
title: "Roboter"
date: "2007-09-16T16:53:32"
picture: "roboter4.jpg"
weight: "4"
konstrukteure: 
- "Defiant"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11470
- /detailsa0fa.html
imported:
- "2019"
_4images_image_id: "11470"
_4images_cat_id: "1034"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11470 -->
