---
layout: "image"
title: "Roboter"
date: "2007-09-25T10:17:07"
picture: "roboter3.jpg"
weight: "11"
konstrukteure: 
- "thkais"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12007
- /details3c01.html
imported:
- "2019"
_4images_image_id: "12007"
_4images_cat_id: "1046"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T10:17:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12007 -->
