---
layout: "image"
title: "Ferngesteuerter Truck"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk005.jpg"
weight: "7"
konstrukteure: 
- "thkais"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11586
- /detailsc968-2.html
imported:
- "2019"
_4images_image_id: "11586"
_4images_cat_id: "1046"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11586 -->
Mit Thomas Kaisers voll ft-kompatibler Proportional-Funk-Lenkung