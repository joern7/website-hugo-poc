---
layout: "image"
title: "Robo Explorer Kasten"
date: "2007-09-16T17:09:08"
picture: "elektronik6.jpg"
weight: "6"
konstrukteure: 
- "thkais"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11546
- /details9b33.html
imported:
- "2019"
_4images_image_id: "11546"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11546 -->
