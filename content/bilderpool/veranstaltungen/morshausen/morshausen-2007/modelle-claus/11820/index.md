---
layout: "image"
title: "Standartenhalter"
date: "2007-09-18T11:20:22"
picture: "PICT5718.jpg"
weight: "11"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11820
- /details9cbd.html
imported:
- "2019"
_4images_image_id: "11820"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11820 -->
;-))