---
layout: "image"
title: "Zugmaschine"
date: "2007-09-18T11:14:57"
picture: "PICT5575.jpg"
weight: "8"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11817
- /details100c.html
imported:
- "2019"
_4images_image_id: "11817"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:14:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11817 -->
