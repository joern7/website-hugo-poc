---
layout: "image"
title: "ccb067.JPG"
date: "2007-09-24T20:48:06"
picture: "ccb067.JPG"
weight: "19"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11950
- /detailse463.html
imported:
- "2019"
_4images_image_id: "11950"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T20:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11950 -->
Der Hafenschlepper auf seinem Container verladen. Zur Schraube hatte Claus dieselbe Idee wie ich: man schneide (oder quäle) ein m4-Gewinde auf eine ft-Clipsachse, und schon geht eine Modellbau-Schiffschraube auch mit fischertechnik zusammen.