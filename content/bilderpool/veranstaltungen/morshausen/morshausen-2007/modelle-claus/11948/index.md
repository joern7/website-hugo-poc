---
layout: "image"
title: "ccb054.JPG"
date: "2007-09-24T20:43:50"
picture: "ccb054.JPG"
weight: "17"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11948
- /detailsf6a3.html
imported:
- "2019"
_4images_image_id: "11948"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T20:43:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11948 -->
Der LKW basiert auf dem Müllabfuhrlaster hier: http://www.ftcommunity.de/categories.php?cat_id=742
hat aber eine Achse mehr bekommen, und die beiden Antriebsmotor treiben jetzt jeder eine davon an. Die hinterste Achse ist nicht angetrieben, wird aber gelenkt.