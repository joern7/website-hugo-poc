---
layout: "image"
title: "Uhr"
date: "2007-09-16T16:53:32"
picture: "uhren06.jpg"
weight: "6"
konstrukteure: 
- "steffalk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11494
- /detailsd17f.html
imported:
- "2019"
_4images_image_id: "11494"
_4images_cat_id: "1037"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11494 -->
