---
layout: "image"
title: "Uhr"
date: "2007-09-16T16:53:32"
picture: "uhren07.jpg"
weight: "7"
konstrukteure: 
- "steffalk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11495
- /details5a38.html
imported:
- "2019"
_4images_image_id: "11495"
_4images_cat_id: "1037"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11495 -->
