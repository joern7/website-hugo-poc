---
layout: "image"
title: "rrb84.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb84.jpg"
weight: "15"
konstrukteure: 
- "Steffalk"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11774
- /details46c5.html
imported:
- "2019"
_4images_image_id: "11774"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11774 -->
