---
layout: "image"
title: "Uhr"
date: "2007-09-16T16:53:32"
picture: "uhren02.jpg"
weight: "2"
konstrukteure: 
- "steffalk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11490
- /details0d52.html
imported:
- "2019"
_4images_image_id: "11490"
_4images_cat_id: "1037"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11490 -->
