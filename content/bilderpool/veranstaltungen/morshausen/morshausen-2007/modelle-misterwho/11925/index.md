---
layout: "image"
title: "radar076.JPG"
date: "2007-09-23T18:51:36"
picture: "radar076.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11925
- /details6645.html
imported:
- "2019"
_4images_image_id: "11925"
_4images_cat_id: "1043"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T18:51:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11925 -->
Blick auf den Vertikalantrieb mit Seilwinde. Der Motor könnte von Faulhaber sein.