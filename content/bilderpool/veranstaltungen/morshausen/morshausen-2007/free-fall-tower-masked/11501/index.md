---
layout: "image"
title: "Plattform"
date: "2007-09-16T16:53:32"
picture: "tower2.jpg"
weight: "2"
konstrukteure: 
- "Masked"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11501
- /detailsff5d.html
imported:
- "2019"
_4images_image_id: "11501"
_4images_cat_id: "1038"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11501 -->
