---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk051.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11632
- /details08b7.html
imported:
- "2019"
_4images_image_id: "11632"
_4images_cat_id: "1069"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11632 -->
Noch mehr Klasse-Detaillösungen: Die lose auf der Achse verschiebliche schwarze Seilrolle sorgt dafür, dass das Seil perfekt auf die Seiltrommel aufgewickelt wird. Das ist für dieses Modell auch wichtig, damit es beim Herunterlassen nicht ruckelt. Außerdem ist da eine Kindersicherung eingebaut: Wenn man mit der Hand an das angehobene Sternrad kommt, wird der fischertechnik-Motor hier durch den zu starken Seilzug etwas nach rechts verdreht und betätigt einen Not-Aus-Taster.