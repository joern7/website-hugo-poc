---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk050.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11631
- /details081c.html
imported:
- "2019"
_4images_image_id: "11631"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11631 -->
Eine Besonderheit des Modells im Vordergrund ist die Führung nicht nur von isolierten Kabeln und es Zugfadens in den Nuten der senkrechten Alustange, sondern die Stromzufuhr durch in die Nuten eingeschobene Metallachsen. Das Alu wurde mit einem Spray oder Lack isoliert. Sehr fein erdacht.