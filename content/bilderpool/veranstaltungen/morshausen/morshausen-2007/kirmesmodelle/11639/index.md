---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk058.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11639
- /detailsed55.html
imported:
- "2019"
_4images_image_id: "11639"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11639 -->
Tolles Detail dieses Modells ist die Übertragung von Druckluft auf ein drehbares Kettenkarussel. Mittel dazu sind Messinghülsen mit Dichtungsring aus einem Feuerzeug und weitere Hülsen, in einem auf 5 mm aufgebohrten roten Baustein 15 mit Loch. Genial und höchst nützlich!