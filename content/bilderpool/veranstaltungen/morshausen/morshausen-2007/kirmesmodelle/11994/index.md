---
layout: "image"
title: "Schaufenstermodell"
date: "2007-09-25T09:41:00"
picture: "schaufenstermodell2.jpg"
weight: "20"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11994
- /detailsffde-2.html
imported:
- "2019"
_4images_image_id: "11994"
_4images_cat_id: "1058"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:41:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11994 -->
