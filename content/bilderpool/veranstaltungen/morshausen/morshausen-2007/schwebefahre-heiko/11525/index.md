---
layout: "image"
title: "Schwebefähre"
date: "2007-09-16T16:59:44"
picture: "schwebefaehre1.jpg"
weight: "1"
konstrukteure: 
- "Heiko"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11525
- /detailsbcf4-3.html
imported:
- "2019"
_4images_image_id: "11525"
_4images_cat_id: "1042"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11525 -->
