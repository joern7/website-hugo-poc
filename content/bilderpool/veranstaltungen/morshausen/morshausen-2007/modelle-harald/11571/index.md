---
layout: "image"
title: "Tarnkappenbomber.JPG"
date: "2007-09-16T20:20:09"
picture: "Tarnkappenbomber.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11571
- /detailsae6b.html
imported:
- "2019"
_4images_image_id: "11571"
_4images_cat_id: "1041"
_4images_user_id: "4"
_4images_image_date: "2007-09-16T20:20:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11571 -->
Für alle, die ein neues Flugzeugmodell vermisst haben. Hier ist es formatfüllend abgebildet (natürlich im aktivierten Tarnkappen-Modus) ;-)