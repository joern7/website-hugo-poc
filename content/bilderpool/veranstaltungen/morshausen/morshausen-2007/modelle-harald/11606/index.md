---
layout: "image"
title: "Hubschraubermechanik, Voith-Schneider-Antrieb"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk025.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11606
- /details832c.html
imported:
- "2019"
_4images_image_id: "11606"
_4images_cat_id: "1041"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11606 -->
