---
layout: "image"
title: "Muldenkipper"
date: "2007-09-16T16:59:44"
picture: "harald4.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11524
- /details8998.html
imported:
- "2019"
_4images_image_id: "11524"
_4images_cat_id: "1041"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11524 -->
