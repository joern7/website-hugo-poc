---
layout: "image"
title: "rrb72.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb72.jpg"
weight: "4"
konstrukteure: 
- "kehrblech"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11762
- /detailsf71c.html
imported:
- "2019"
_4images_image_id: "11762"
_4images_cat_id: "1050"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11762 -->
