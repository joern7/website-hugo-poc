---
layout: "image"
title: "Industriemodell"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk044.jpg"
weight: "4"
konstrukteure: 
- "Fredy"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11625
- /details80d8-2.html
imported:
- "2019"
_4images_image_id: "11625"
_4images_cat_id: "1071"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11625 -->
