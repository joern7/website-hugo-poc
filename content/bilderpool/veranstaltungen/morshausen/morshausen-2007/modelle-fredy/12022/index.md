---
layout: "image"
title: "Programm"
date: "2007-09-27T20:22:12"
picture: "programme1.jpg"
weight: "12"
konstrukteure: 
- "Fredy"
fotografen:
- "Limit"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/12022
- /detailse31c.html
imported:
- "2019"
_4images_image_id: "12022"
_4images_cat_id: "1071"
_4images_user_id: "430"
_4images_image_date: "2007-09-27T20:22:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12022 -->
Das doch sehr komplex wirkende Programm für die Sortiermaschine