---
layout: "image"
title: "Industriemodell"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk043.jpg"
weight: "3"
konstrukteure: 
- "Fredy"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11624
- /details5f78.html
imported:
- "2019"
_4images_image_id: "11624"
_4images_cat_id: "1071"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11624 -->
Gelbe und graue 60x60-Kästchen werden befördert und auf zwei verschiedene Ausgabestationen sortiert. In der Mitte der Kettenvorhang.