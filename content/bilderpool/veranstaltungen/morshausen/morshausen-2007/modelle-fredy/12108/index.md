---
layout: "image"
title: "auto042.JPG"
date: "2007-10-03T08:58:47"
picture: "auto042.JPG"
weight: "16"
konstrukteure: 
- "Fredy"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12108
- /detailseddd.html
imported:
- "2019"
_4images_image_id: "12108"
_4images_cat_id: "1071"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T08:58:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12108 -->
Blick von vorn aufs Vorderachsdifferenzial und die Lenkung.