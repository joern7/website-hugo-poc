---
layout: "image"
title: "Traumkasten"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk106.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11687
- /details1b2d.html
imported:
- "2019"
_4images_image_id: "11687"
_4images_cat_id: "1053"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11687 -->
Viele selten gewordene Spezialteile, und Alus in rauen Mengen. Da das ein Traum ist, ist das Bild auch etwas unscharf ;-)