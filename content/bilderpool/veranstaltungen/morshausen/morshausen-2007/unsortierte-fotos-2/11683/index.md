---
layout: "image"
title: "Truck, Traktor, Förderband und das Greifer-Spiel"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk102.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11683
- /details615d.html
imported:
- "2019"
_4images_image_id: "11683"
_4images_cat_id: "1053"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11683 -->
