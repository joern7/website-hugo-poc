---
layout: "image"
title: "rrb39.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb39.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11729
- /detailsaf18-2.html
imported:
- "2019"
_4images_image_id: "11729"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11729 -->
