---
layout: "image"
title: "rrb33.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb33.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11723
- /detailsb3e0.html
imported:
- "2019"
_4images_image_id: "11723"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11723 -->
