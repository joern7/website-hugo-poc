---
layout: "image"
title: "Faltkran"
date: "2007-09-18T11:07:33"
picture: "PICT5554.jpg"
weight: "37"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11808
- /details47cd.html
imported:
- "2019"
_4images_image_id: "11808"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:07:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11808 -->
