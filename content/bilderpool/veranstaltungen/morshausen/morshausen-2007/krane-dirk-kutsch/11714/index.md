---
layout: "image"
title: "rrb24.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb24.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11714
- /detailsf66d.html
imported:
- "2019"
_4images_image_id: "11714"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11714 -->
