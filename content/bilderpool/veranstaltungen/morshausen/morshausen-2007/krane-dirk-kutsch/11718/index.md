---
layout: "image"
title: "rrb28.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb28.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11718
- /details5996.html
imported:
- "2019"
_4images_image_id: "11718"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11718 -->
