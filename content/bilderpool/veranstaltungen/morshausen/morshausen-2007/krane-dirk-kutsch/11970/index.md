---
layout: "image"
title: "Faltkran"
date: "2007-09-25T09:26:10"
picture: "kran01.jpg"
weight: "45"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11970
- /details2793-3.html
imported:
- "2019"
_4images_image_id: "11970"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11970 -->
