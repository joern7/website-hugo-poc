---
layout: "image"
title: "Faltkran"
date: "2007-09-16T16:59:44"
picture: "kraene8.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11520
- /details76ff.html
imported:
- "2019"
_4images_image_id: "11520"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11520 -->
