---
layout: "image"
title: "Kräne von Dirk Kutsch"
date: "2007-09-16T22:07:25"
picture: "FT-Mrshausen-2007_166.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Damen (Alias Peter Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11577
- /details9717.html
imported:
- "2019"
_4images_image_id: "11577"
_4images_cat_id: "1040"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11577 -->
