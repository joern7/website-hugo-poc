---
layout: "image"
title: "Modell"
date: "2007-09-16T16:53:32"
picture: "industriemodell1.jpg"
weight: "1"
konstrukteure: 
- "sven"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11507
- /details10f5-2.html
imported:
- "2019"
_4images_image_id: "11507"
_4images_cat_id: "1039"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11507 -->
