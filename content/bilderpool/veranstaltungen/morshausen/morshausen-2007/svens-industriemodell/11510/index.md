---
layout: "image"
title: "Greifer"
date: "2007-09-16T16:53:32"
picture: "industriemodell4.jpg"
weight: "4"
konstrukteure: 
- "sven"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11510
- /details983e-2.html
imported:
- "2019"
_4images_image_id: "11510"
_4images_cat_id: "1039"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11510 -->
