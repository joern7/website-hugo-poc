---
layout: "image"
title: "marmacs Projektionssystem"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk014.jpg"
weight: "4"
konstrukteure: 
- "marmac"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11595
- /detailsd826.html
imported:
- "2019"
_4images_image_id: "11595"
_4images_cat_id: "1044"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11595 -->
IR-LEDs strahlen in eine transparente Folie. Wenn man es mit den Fingern berührt, wird das reflektierte Licht von einer Kamera dahinter registriert. Dazu noch Flash-Scripts, die Bilder drehen, die per Projektor (ebenfalls dahinter) auf die Folie projiziert werden. Fertig ist die Microsoft-Konkurrenz ;-) Absolut abgefahren.