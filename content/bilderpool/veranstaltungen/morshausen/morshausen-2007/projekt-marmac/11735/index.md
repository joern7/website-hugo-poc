---
layout: "image"
title: "rrb45.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb45.jpg"
weight: "10"
konstrukteure: 
- "MarMac"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11735
- /detailse3a7-3.html
imported:
- "2019"
_4images_image_id: "11735"
_4images_cat_id: "1044"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11735 -->
