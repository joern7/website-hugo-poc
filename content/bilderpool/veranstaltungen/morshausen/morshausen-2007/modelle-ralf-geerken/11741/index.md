---
layout: "image"
title: "rrb51.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb51.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11741
- /details31d2-2.html
imported:
- "2019"
_4images_image_id: "11741"
_4images_cat_id: "1051"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11741 -->
