---
layout: "image"
title: "rrb49.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb49.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11739
- /details1c99.html
imported:
- "2019"
_4images_image_id: "11739"
_4images_cat_id: "1051"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11739 -->
