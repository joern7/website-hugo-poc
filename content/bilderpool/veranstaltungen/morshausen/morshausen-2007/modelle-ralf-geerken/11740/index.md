---
layout: "image"
title: "rrb50.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb50.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11740
- /detailse8fd.html
imported:
- "2019"
_4images_image_id: "11740"
_4images_cat_id: "1051"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11740 -->
