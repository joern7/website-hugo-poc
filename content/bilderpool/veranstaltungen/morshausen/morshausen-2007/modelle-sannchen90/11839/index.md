---
layout: "image"
title: "Bagger"
date: "2007-09-18T11:40:40"
picture: "PICT5531.jpg"
weight: "6"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11839
- /details5a45-3.html
imported:
- "2019"
_4images_image_id: "11839"
_4images_cat_id: "1049"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11839 -->
