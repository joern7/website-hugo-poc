---
layout: "image"
title: "Bagger 2"
date: "2007-09-18T11:40:55"
picture: "PICT5643.jpg"
weight: "7"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11840
- /details72c0.html
imported:
- "2019"
_4images_image_id: "11840"
_4images_cat_id: "1049"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:40:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11840 -->
