---
layout: "image"
title: "Bagger"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk070.jpg"
weight: "5"
konstrukteure: 
- "Sannchen90"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11651
- /detailsf69f.html
imported:
- "2019"
_4images_image_id: "11651"
_4images_cat_id: "1049"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11651 -->
Interessant sind auch die etwas bearbeiteten Bausteine 7,5 für die Führung der Schläuche am Ansatz des Baggerarms. Wenn jemand viel Pneumatik auf ein Mal sehen will, ist er bei Sannchen90 richtig.