---
layout: "image"
title: "Planetarium"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk037.jpg"
weight: "7"
konstrukteure: 
- "Triceratops"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11618
- /details6058.html
imported:
- "2019"
_4images_image_id: "11618"
_4images_cat_id: "1052"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11618 -->
