---
layout: "image"
title: "Planetarium"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk038.jpg"
weight: "8"
konstrukteure: 
- "Triceratops"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11619
- /detailsd7fd.html
imported:
- "2019"
_4images_image_id: "11619"
_4images_cat_id: "1052"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11619 -->
Das Getriebe bei der Erde. Links oben ist das Z7, etwas unter dem linken gelben Statikträger das Z22 sichtbar.