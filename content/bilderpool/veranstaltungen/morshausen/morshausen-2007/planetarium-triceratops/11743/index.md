---
layout: "image"
title: "rrb53.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb53.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11743
- /details2641.html
imported:
- "2019"
_4images_image_id: "11743"
_4images_cat_id: "1052"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11743 -->
