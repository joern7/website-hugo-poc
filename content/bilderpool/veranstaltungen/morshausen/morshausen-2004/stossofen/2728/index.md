---
layout: "image"
title: "Rummelplatzmodell"
date: "2004-10-20T13:47:28"
picture: "Rummelplatzmodell.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2728
- /detailsb620.html
imported:
- "2019"
_4images_image_id: "2728"
_4images_cat_id: "257"
_4images_user_id: "54"
_4images_image_date: "2004-10-20T13:47:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2728 -->
Ein Rummelplatzmodell von Jos Geurts.