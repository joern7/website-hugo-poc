---
layout: "comment"
hidden: true
title: "315"
date: "2004-11-15T17:02:40"
uploadBy:
- "Uwe Schmejkal"
license: "unknown"
imported:
- "2019"
---
Modell StoßofenBild zeigt das Einfahren der Austragmaschine in den Ofen, mit geöffneter Ofentür. Etwas zu sehen ist, wie die abgesenkten Austragarme zwischen den Rollen am Rollgang vorbei kommen.