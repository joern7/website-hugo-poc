---
layout: "image"
title: "Ultralight-Mobile - Tragschrauber"
date: "2004-09-29T19:20:20"
picture: "Ultra03.jpg"
weight: "7"
konstrukteure: 
- "Holger Howey"
fotografen:
- "-?-"
keywords: ["Mobile", "Ultralight"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2654
- /details70ac.html
imported:
- "2019"
_4images_image_id: "2654"
_4images_cat_id: "261"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2654 -->
Ein Mobile aus ft-Ultralight-Fliegern. Hier der zweite von vier Bestandteilen, der Tragschrauber.

Einfach bezaubernd, wie wenig Teile man braucht, um ein vollständiges Modell hinzukriegen.

Gehört einfach in jedes Kinderzimmer!