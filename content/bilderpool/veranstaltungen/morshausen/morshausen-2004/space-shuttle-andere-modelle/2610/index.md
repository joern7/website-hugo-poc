---
layout: "image"
title: "Holgers Raketenbahnhof2"
date: "2004-09-20T15:35:22"
picture: "Raketenbahnhof_ft02.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2610
- /details793c.html
imported:
- "2019"
_4images_image_id: "2610"
_4images_cat_id: "261"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2610 -->
