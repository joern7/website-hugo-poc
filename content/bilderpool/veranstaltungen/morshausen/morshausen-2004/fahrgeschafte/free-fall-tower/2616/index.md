---
layout: "image"
title: "Der Power Tower"
date: "2004-09-21T13:30:08"
picture: "Der_Power_Tower.jpg"
weight: "5"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2616
- /details387a-2.html
imported:
- "2019"
_4images_image_id: "2616"
_4images_cat_id: "247"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2616 -->
