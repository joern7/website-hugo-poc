---
layout: "image"
title: "Power Tower"
date: "2004-09-20T15:35:22"
picture: "Power_Tower_Brickwedde_ft05.jpg"
weight: "2"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2603
- /details7c50.html
imported:
- "2019"
_4images_image_id: "2603"
_4images_cat_id: "247"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2603 -->
Durch Gegenlicht leider etwas dunkel.