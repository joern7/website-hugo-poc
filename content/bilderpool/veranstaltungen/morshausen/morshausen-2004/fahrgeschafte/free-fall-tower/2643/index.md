---
layout: "image"
title: "DSC00851"
date: "2004-09-29T16:31:27"
picture: "DSC00851.jpg"
weight: "8"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/2643
- /detailsdeef.html
imported:
- "2019"
_4images_image_id: "2643"
_4images_cat_id: "247"
_4images_user_id: "10"
_4images_image_date: "2004-09-29T16:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2643 -->
