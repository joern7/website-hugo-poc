---
layout: "image"
title: "Ein Pack- und Manschaftswagen"
date: "2004-09-21T15:55:05"
picture: "Pack_und_Mannschaftswagen_ft01.jpg"
weight: "2"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2634
- /details76de.html
imported:
- "2019"
_4images_image_id: "2634"
_4images_cat_id: "249"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2634 -->
