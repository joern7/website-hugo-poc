---
layout: "image"
title: "Der Break Dance von Brickweddes"
date: "2004-09-20T15:35:22"
picture: "Break_Dance_ft02.jpg"
weight: "4"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2608
- /details116c.html
imported:
- "2019"
_4images_image_id: "2608"
_4images_cat_id: "246"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2608 -->
Schöne Details, sieht sehr gut aus.