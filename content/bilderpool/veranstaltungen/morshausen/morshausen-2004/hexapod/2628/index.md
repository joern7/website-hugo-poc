---
layout: "image"
title: "Und es bewegt sich nicht"
date: "2004-09-21T13:30:28"
picture: "Hexapod_Fingertest.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2628
- /details40d4.html
imported:
- "2019"
_4images_image_id: "2628"
_4images_cat_id: "259"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2628 -->
