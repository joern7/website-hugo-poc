---
layout: "image"
title: "High Tech im Mondfahrzeug"
date: "2004-09-21T13:30:28"
picture: "Mondfahrzeug_High_Tech.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2624
- /details27b4-3.html
imported:
- "2019"
_4images_image_id: "2624"
_4images_cat_id: "262"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2624 -->
