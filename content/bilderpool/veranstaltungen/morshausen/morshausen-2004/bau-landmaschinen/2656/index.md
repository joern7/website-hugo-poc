---
layout: "image"
title: "Rübenvollernter"
date: "2004-09-29T20:10:33"
picture: "Wwwwm01.jpg"
weight: "8"
konstrukteure: 
- "Claus"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2656
- /details259b.html
imported:
- "2019"
_4images_image_id: "2656"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:10:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2656 -->
Details von der wrritze-wrrohren Wrroiwe-Wrropp-Maschin (for non-Germans: die knall-rote Rüben-Rupf-Maschine)

Die Zapfwelle des Traktors (von links unten) treibt den Rodekopf (verdeckt, in Bildmitte) und die Förderkette für die Rüben (linke Bildhälfte). 
Die restlichen Funktionen treibt der Powermot an, der oben zwischen den Rädern montiert ist.