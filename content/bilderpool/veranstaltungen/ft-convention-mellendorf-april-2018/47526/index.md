---
layout: "image"
title: "Challenge for children"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril055.jpg"
weight: "55"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47526
- /details12d2.html
imported:
- "2019"
_4images_image_id: "47526"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47526 -->
1 button controlled car.
The challenge for  the  driver is to make a slalom around the two green  abstacles.