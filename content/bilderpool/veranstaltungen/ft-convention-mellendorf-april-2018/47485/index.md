---
layout: "image"
title: "The old roboter"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril014.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47485
- /detailse44a.html
imported:
- "2019"
_4images_image_id: "47485"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47485 -->
