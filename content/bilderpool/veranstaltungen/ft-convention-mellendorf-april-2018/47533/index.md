---
layout: "image"
title: "The convention hall"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril062.jpg"
weight: "62"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47533
- /detailsfe27-2.html
imported:
- "2019"
_4images_image_id: "47533"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47533 -->
