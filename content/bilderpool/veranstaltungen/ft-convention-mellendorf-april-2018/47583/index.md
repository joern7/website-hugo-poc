---
layout: "image"
title: "Kugelbahn"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion03.jpg"
weight: "105"
konstrukteure: 
- "-?-"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/47583
- /detailsc1a8.html
imported:
- "2019"
_4images_image_id: "47583"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47583 -->
Schöne Kugelbahn