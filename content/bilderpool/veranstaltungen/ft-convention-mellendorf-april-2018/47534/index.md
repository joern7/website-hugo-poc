---
layout: "image"
title: "ftconventionapril063.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril063.jpg"
weight: "63"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47534
- /details1fa5.html
imported:
- "2019"
_4images_image_id: "47534"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47534 -->
