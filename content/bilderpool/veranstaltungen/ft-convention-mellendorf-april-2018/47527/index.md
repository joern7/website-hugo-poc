---
layout: "image"
title: "ftconventionapril056.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril056.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47527
- /detailsc540-3.html
imported:
- "2019"
_4images_image_id: "47527"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47527 -->
Ralf is ready for his performance.