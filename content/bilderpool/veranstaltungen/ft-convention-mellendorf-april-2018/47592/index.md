---
layout: "image"
title: "Windmühle von Ingwer"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion12.jpg"
weight: "114"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/47592
- /detailsbf9d.html
imported:
- "2019"
_4images_image_id: "47592"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47592 -->
Super stabil gebaute Windmühle von Ingwer Carstens. Ich steh da gerade und bestaune das tolle Modell.