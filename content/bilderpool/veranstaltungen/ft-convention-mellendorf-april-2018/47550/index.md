---
layout: "image"
title: "Friday evening"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril079.jpg"
weight: "79"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47550
- /details6cd5.html
imported:
- "2019"
_4images_image_id: "47550"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47550 -->
Models are just out of their boxes.