---
layout: "comment"
hidden: true
title: "24108"
date: "2018-06-16T15:47:50"
uploadBy:
- "TobiasBrunk3@gmx.de"
license: "unknown"
imported:
- "2019"
---
Für die Hin- und Herbewegung sind zwei XM - Motoren im Rollwagen verbaut.
Für die Drehbewegung ist ein S - Motor verantwortlich.
Gesteuert wird das Modell über einen TX Controller, ebenfalls im Rollwagen verbaut.

Gruß Tecmaster