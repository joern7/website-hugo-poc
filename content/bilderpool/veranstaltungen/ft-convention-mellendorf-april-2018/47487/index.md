---
layout: "image"
title: "ftconventionapril016.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril016.jpg"
weight: "16"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47487
- /details5951-2.html
imported:
- "2019"
_4images_image_id: "47487"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47487 -->
