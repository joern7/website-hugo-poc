---
layout: "image"
title: "Bügelsäge"
date: "2007-03-23T22:21:21"
picture: "162_6244.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9685
- /details163f-2.html
imported:
- "2019"
_4images_image_id: "9685"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9685 -->
