---
layout: "image"
title: "UT Kasten 2"
date: "2007-03-23T22:21:21"
picture: "162_6237.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9692
- /details9638.html
imported:
- "2019"
_4images_image_id: "9692"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9692 -->
