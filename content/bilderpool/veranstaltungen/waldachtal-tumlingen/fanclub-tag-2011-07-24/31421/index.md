---
layout: "image"
title: "FanClubTag2011"
date: "2011-07-28T21:44:15"
picture: "fct18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/31421
- /detailsc491.html
imported:
- "2019"
_4images_image_id: "31421"
_4images_cat_id: "2337"
_4images_user_id: "1162"
_4images_image_date: "2011-07-28T21:44:15"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31421 -->
Stände, ganz vorne der Stand von Majus mit seinem Plotter.