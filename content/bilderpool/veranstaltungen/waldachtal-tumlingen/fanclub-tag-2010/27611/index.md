---
layout: "image"
title: "Neuheiten-Show"
date: "2010-07-04T21:56:31"
picture: "FanClubTag2010_07.jpg"
weight: "5"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27611
- /detailsa95e-2.html
imported:
- "2019"
_4images_image_id: "27611"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T21:56:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27611 -->
Hier war immer viel los. Im Hintergrund die Technik-Info, an der Hartmut Knecht die Stellung hielt und alle Fragen beantwortete.