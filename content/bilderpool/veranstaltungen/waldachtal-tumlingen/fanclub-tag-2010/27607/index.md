---
layout: "image"
title: "Abendessen Samstag"
date: "2010-07-04T21:42:45"
picture: "FanClubTag2010_01.jpg"
weight: "1"
konstrukteure: 
- "."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27607
- /detailsacbf.html
imported:
- "2019"
_4images_image_id: "27607"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T21:42:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27607 -->
Die Aussteller der Großmodelle (und Besucher) waren Samstag Abend noch Alle zusammen Essen. Von Links: TST, Tobias Brezing, Guilligan, Brickwedde Senior, Frau Kohl, (es fehlt Albert Kohl), 3 Holländer, deren Namen ich leider vergessen habe..., Brickwedde Junior, Peter Derks, Rob van Baal, Sven