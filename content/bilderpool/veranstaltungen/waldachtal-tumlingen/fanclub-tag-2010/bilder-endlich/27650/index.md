---
layout: "image"
title: "FT Fan Tag 2010"
date: "2010-07-05T16:39:56"
picture: "ftfanclubtag32.jpg"
weight: "32"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27650
- /details8641.html
imported:
- "2019"
_4images_image_id: "27650"
_4images_cat_id: "1990"
_4images_user_id: "1162"
_4images_image_date: "2010-07-05T16:39:56"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27650 -->
Dominoaufstelltruck