---
layout: "image"
title: "FT Fan Tag 2010"
date: "2010-07-05T16:39:56"
picture: "ftfanclubtag10.jpg"
weight: "10"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27628
- /detailsdbd4.html
imported:
- "2019"
_4images_image_id: "27628"
_4images_cat_id: "1990"
_4images_user_id: "1162"
_4images_image_date: "2010-07-05T16:39:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27628 -->
Tunnelroboter