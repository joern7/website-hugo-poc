---
layout: "image"
title: "Brickwedde Junior"
date: "2010-07-04T22:03:31"
picture: "FanClubTag2010_10.jpg"
weight: "8"
konstrukteure: 
- "Wilhelm Brickwedde Junior"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27614
- /detailscbfd.html
imported:
- "2019"
_4images_image_id: "27614"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T22:03:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27614 -->
Ein neues Karussell, der "SkyFlyer". Im Prinzip ein Kettenkarussell, das während dem Drehen noch am Turm hoch und runter fährt. Leider kein sonderlich gutes Fotos, aber zu mehr hatte ich einfach keine Zeit...