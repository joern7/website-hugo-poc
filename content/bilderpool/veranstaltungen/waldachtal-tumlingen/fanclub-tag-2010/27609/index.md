---
layout: "image"
title: "Überblick Kundencenter"
date: "2010-07-04T21:51:11"
picture: "FanClubTag2010_03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27609
- /details4b8d.html
imported:
- "2019"
_4images_image_id: "27609"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T21:51:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27609 -->
Unten der Stand von Knobloch (Bauteile bestellen mit Rabatt)
An der oberen Kante die fanclub-Mitglieder-Modelle. Teilweise waren da echt schicke Sachen dabei, die auch auf der Convention gut aufgehoben wären.
Und oben rechts fand gerade eine der "Science-Shows" statt. Die waren sehr gut gemacht, mit leuchtenden Gewürzgurken und ähnlichen Dingen.