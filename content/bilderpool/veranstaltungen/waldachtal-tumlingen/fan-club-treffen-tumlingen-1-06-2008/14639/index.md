---
layout: "image"
title: "Waldachtal Fanclubtreffen 1.06.2008"
date: "2008-06-07T19:47:14"
picture: "waldachtalfanclubtreffen11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/14639
- /detailsbc45.html
imported:
- "2019"
_4images_image_id: "14639"
_4images_cat_id: "1345"
_4images_user_id: "409"
_4images_image_date: "2008-06-07T19:47:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14639 -->
Der Andrang bei der Modellschau war groß