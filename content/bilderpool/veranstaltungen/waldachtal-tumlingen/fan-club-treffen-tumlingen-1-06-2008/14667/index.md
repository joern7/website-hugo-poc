---
layout: "image"
title: "Waldachtal Fanclubtreffen 1.06.2008"
date: "2008-06-07T19:47:15"
picture: "waldachtalfanclubtreffen39.jpg"
weight: "39"
konstrukteure: 
- "ft"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/14667
- /detailsc40c.html
imported:
- "2019"
_4images_image_id: "14667"
_4images_cat_id: "1345"
_4images_user_id: "409"
_4images_image_date: "2008-06-07T19:47:15"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14667 -->
Bei diesem Modell vom Baukasten FireTrucks kann man die neuen Winkelträger 120 rot sehen