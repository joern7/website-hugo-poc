---
layout: "image"
title: "Waschanlage"
date: "2009-07-12T17:00:17"
picture: "fanclubtag22.jpg"
weight: "22"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24558
- /detailsabc0.html
imported:
- "2019"
_4images_image_id: "24558"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24558 -->
Dieses sehr professionell gebaute Modell fährt um das stillstehende Auto.