---
layout: "image"
title: "Steuerung"
date: "2009-07-12T17:00:16"
picture: "fanclubtag12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: ["32455"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24548
- /details2b44.html
imported:
- "2019"
_4images_image_id: "24548"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24548 -->
Was das ganz genau machte, weiß ich leider nicht. Interessant sind neben der Steuerung aber die Glas-Schiebetüren, die von den "Führungsplatten für E-Magnete" gehalten werden. Sieht sehr professionell aus. In dem Glaskasten war etwas, was ich als Halogenlampe einschätze, und noch ein Metallteil.