---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag27.jpg"
weight: "27"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24563
- /detailsec37-2.html
imported:
- "2019"
_4images_image_id: "24563"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24563 -->
Das Jubiläumsschild drehte sich.