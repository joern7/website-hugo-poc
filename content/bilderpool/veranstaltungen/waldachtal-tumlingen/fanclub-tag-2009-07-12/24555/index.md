---
layout: "image"
title: "Seilabschaltung"
date: "2009-07-12T17:00:16"
picture: "fanclubtag19.jpg"
weight: "19"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24555
- /detailsaec9-2.html
imported:
- "2019"
_4images_image_id: "24555"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24555 -->
Auf der Seiltrommel in Bildmitte denke man sich ein Seil aufgewickelt. Über eine feine Schraube bewegt sich der schwarze BS15 mit der roten 15x30-Platte darauf zwischen den Tastern, die mittels Diodenschaltung eine Endlagenabschaltung ergeben. Dadurch hält der Motor automatisch an, wenn das Seil ganz auf- oder ganz abgewickelt ist.