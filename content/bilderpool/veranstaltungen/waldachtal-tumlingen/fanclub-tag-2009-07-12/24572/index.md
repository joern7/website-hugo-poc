---
layout: "image"
title: "Bearbeitungsstraße mit 2 Werkzeugautomaten"
date: "2009-07-12T18:57:04"
picture: "tag3.jpg"
weight: "36"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Marius Seider (Limit)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24572
- /detailsd97f.html
imported:
- "2019"
_4images_image_id: "24572"
_4images_cat_id: "1688"
_4images_user_id: "430"
_4images_image_date: "2009-07-12T18:57:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24572 -->
