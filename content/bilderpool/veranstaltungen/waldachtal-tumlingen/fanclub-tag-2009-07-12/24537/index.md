---
layout: "image"
title: "Spurensucher"
date: "2009-07-12T16:59:53"
picture: "fanclubtag01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24537
- /details4cee.html
imported:
- "2019"
_4images_image_id: "24537"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24537 -->
Dieses Fahrzeug kommt gänzlich ohne Computerei aus, sondern wird von zwei Fotozellen und Silberlingen auf der silbrigen Fahrspur gehalten. Gebaut hat das ein ganz junger Mensch, und es funktionierte prima.