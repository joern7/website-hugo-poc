---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag26.jpg"
weight: "26"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24562
- /details0fee.html
imported:
- "2019"
_4images_image_id: "24562"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24562 -->
Man sieht unten einen der Motoren und erahnt die Größe des Wagens.