---
layout: "image"
title: "Sägewerk"
date: "2009-07-12T17:00:17"
picture: "fanclubtag20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24556
- /detailsdcf1.html
imported:
- "2019"
_4images_image_id: "24556"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24556 -->
Fast nur Alus sind verbaut, kaum Grundbausteine. ;-)