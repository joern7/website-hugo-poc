---
layout: "image"
title: "Winkelgetriebe"
date: "2009-07-12T17:00:16"
picture: "fanclubtag16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24552
- /detailsc0a9-2.html
imported:
- "2019"
_4images_image_id: "24552"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24552 -->
Hier stecken die vier Winkelachse, die die Bewegung 90° um die Ecke leiten, in Alustangen. Dieses Modell verkraftet recht hohe Drehzahlen.