---
layout: "image"
title: "Bearbeitungsstraße"
date: "2009-07-12T17:00:16"
picture: "fanclubtag11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24547
- /details57e5-2.html
imported:
- "2019"
_4images_image_id: "24547"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24547 -->
Links legt man das Stückgut auf, welches automatisch auf die Bahn durch alle Stationen geschickt und rechts wieder ausgegeben wird.