---
layout: "image"
title: "Waschanlage"
date: "2009-07-12T17:00:17"
picture: "fanclubtag23.jpg"
weight: "23"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24559
- /details21fd.html
imported:
- "2019"
_4images_image_id: "24559"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24559 -->
Hier die andere Seite.