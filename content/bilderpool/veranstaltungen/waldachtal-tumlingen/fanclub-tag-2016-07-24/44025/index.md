---
layout: "image"
title: "Brücke1"
date: "2016-07-27T18:03:54"
picture: "Brcke01.jpg"
weight: "65"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "FT"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/44025
- /details29f4-2.html
imported:
- "2019"
_4images_image_id: "44025"
_4images_cat_id: "3255"
_4images_user_id: "560"
_4images_image_date: "2016-07-27T18:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44025 -->
