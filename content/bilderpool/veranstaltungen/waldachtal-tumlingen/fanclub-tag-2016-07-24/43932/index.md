---
layout: "image"
title: "fct07.jpg"
date: "2016-07-24T20:10:12"
picture: "fct07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43932
- /details0396.html
imported:
- "2019"
_4images_image_id: "43932"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43932 -->
