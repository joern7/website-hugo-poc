---
layout: "image"
title: "Große Kugelbahn"
date: "2016-07-25T14:24:24"
picture: "ftfct17.jpg"
weight: "42"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43977
- /details401b.html
imported:
- "2019"
_4images_image_id: "43977"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43977 -->
