---
layout: "comment"
hidden: true
title: "22282"
date: "2016-07-25T18:18:57"
uploadBy:
- "ThomasW"
license: "unknown"
imported:
- "2019"
---
Die Anlage ist 2 Meter breit und es waren am Ende etwa 160 Kugeln im Einsatz.
Ungefähr 80 davon jeweils im hier sichtbaren Wartebereich und auf dem Lift, die anderen 80 unterwegs.
Der Wartebereich ist federnd gelagert. Sobald sich genug Kugeln drin befinden, betätigen sie den Taster, der unter dem Wartebereich eingebaut ist. Der Lift startet und läuft eine Umdrehung des Schleifrings lang. In dieser Zeit werden etwa 40 bis 50 Kugeln auf die Reise geschickt.
Die Rundreise besteht aus:
- Wartebereich
- steiler Lift hoch zur Spirale
- Spirale
- Lift rauf zum Binärzähler
- Binärzähler (auf dem nächsten Bild gut zu sehen)
- Schaufelrad
- Wackelbrücke
- Lift für den Rückweg
- Rückweg (die Flexschienen im Hintergrund)