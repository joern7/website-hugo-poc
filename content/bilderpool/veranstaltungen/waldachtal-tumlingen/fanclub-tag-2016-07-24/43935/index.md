---
layout: "image"
title: "Rad-Bagger, designerpreis-verdächtig"
date: "2016-07-24T20:10:12"
picture: "fct10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43935
- /detailse7a6-2.html
imported:
- "2019"
_4images_image_id: "43935"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43935 -->
