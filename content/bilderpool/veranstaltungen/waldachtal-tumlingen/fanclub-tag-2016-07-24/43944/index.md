---
layout: "image"
title: "Fallturm, pfiffig"
date: "2016-07-24T20:10:12"
picture: "fct19.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43944
- /details6e0e.html
imported:
- "2019"
_4images_image_id: "43944"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43944 -->
Oben angekommen, wird die Gondel ausgeklinkt. Da ist kein Seil und keine Bremse, und sie fällt und fällt... bis auf die elastischen Elemente, aka Flexschienen, die sie wieder auf die Reise schicken. Ob das Fahrerlebnis noch unter 4,5 g Maximal-Beschleunigung bleibt, wage ich aber zu bezweifeln.