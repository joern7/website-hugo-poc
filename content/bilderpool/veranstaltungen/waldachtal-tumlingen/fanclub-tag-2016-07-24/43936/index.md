---
layout: "image"
title: "Das Gürteltier unter den Fahrzeugen"
date: "2016-07-24T20:10:12"
picture: "fct11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43936
- /detailsa90c.html
imported:
- "2019"
_4images_image_id: "43936"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43936 -->
Ein Flugzeugschlepper. Hier im Hundegang, aber er kann alles.