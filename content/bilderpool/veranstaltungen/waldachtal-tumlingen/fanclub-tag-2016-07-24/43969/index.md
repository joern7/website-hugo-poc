---
layout: "image"
title: "Rekordversuch Brücke"
date: "2016-07-25T14:24:24"
picture: "ftfct09.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43969
- /details8016-2.html
imported:
- "2019"
_4images_image_id: "43969"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43969 -->
...nicht ganz gerade aber sehr beeindruckend!