---
layout: "image"
title: "Magnetfelder gehen berührungslos..."
date: "2016-07-24T20:10:12"
picture: "fct24.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43949
- /details7fb3-2.html
imported:
- "2019"
_4images_image_id: "43949"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43949 -->
und insbesondere auch durch die Flexschienen hin: innen rotiert der Magnet, außen läuft die Kugel nach oben.