---
layout: "image"
title: "Rekordversuch: die Brücke"
date: "2016-07-24T20:10:12"
picture: "fct01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43926
- /details834c.html
imported:
- "2019"
_4images_image_id: "43926"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43926 -->
