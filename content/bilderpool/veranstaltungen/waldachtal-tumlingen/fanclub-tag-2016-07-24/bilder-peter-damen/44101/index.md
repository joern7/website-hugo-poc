---
layout: "image"
title: "fischertechnikfanclubtagtumlingen13.jpg"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44101
- /details5708.html
imported:
- "2019"
_4images_image_id: "44101"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44101 -->
