---
layout: "image"
title: "Thomas Püttmann + sehr gute Erklärungen"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen11.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann "
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44099
- /details818c-2.html
imported:
- "2019"
_4images_image_id: "44099"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44099 -->
