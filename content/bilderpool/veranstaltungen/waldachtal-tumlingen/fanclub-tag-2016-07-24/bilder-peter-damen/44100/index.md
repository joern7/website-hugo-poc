---
layout: "image"
title: "Thomas Püttmann + sehr gute Erklärungen"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen12.jpg"
weight: "12"
konstrukteure: 
- "Thomas Püttmann "
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44100
- /detailsb95d.html
imported:
- "2019"
_4images_image_id: "44100"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44100 -->
