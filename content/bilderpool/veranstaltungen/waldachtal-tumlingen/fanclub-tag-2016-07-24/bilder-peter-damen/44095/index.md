---
layout: "image"
title: "Bleibt interessant zum anschauen...."
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44095
- /details59fe.html
imported:
- "2019"
_4images_image_id: "44095"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44095 -->
