---
layout: "image"
title: "Wiener Rad"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Püttmann "
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44094
- /detailsbbfc.html
imported:
- "2019"
_4images_image_id: "44094"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44094 -->
