---
layout: "image"
title: "3D-Drucker"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44103
- /detailsadbe-2.html
imported:
- "2019"
_4images_image_id: "44103"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44103 -->
