---
layout: "image"
title: "Harald's  Modell"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen53.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44141
- /detailsa0b2-2.html
imported:
- "2019"
_4images_image_id: "44141"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44141 -->
