---
layout: "image"
title: "RoboPro  ftc-Community  Slide"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44112
- /detailsb6ef.html
imported:
- "2019"
_4images_image_id: "44112"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44112 -->
