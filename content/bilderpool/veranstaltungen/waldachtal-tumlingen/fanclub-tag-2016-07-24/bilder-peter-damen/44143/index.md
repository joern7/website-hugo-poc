---
layout: "image"
title: "Harald's  Modell"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen55.jpg"
weight: "55"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44143
- /detailsce1a.html
imported:
- "2019"
_4images_image_id: "44143"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44143 -->
