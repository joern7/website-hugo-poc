---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix30.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35137
- /detailsc964.html
imported:
- "2019"
_4images_image_id: "35137"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35137 -->
Christian bei der Vorstellung der ft:pedia