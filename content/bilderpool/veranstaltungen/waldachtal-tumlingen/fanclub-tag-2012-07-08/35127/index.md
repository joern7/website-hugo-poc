---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35127
- /detailsd6c4.html
imported:
- "2019"
_4images_image_id: "35127"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35127 -->
Vorstellung der neuen Kästen