---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix49.jpg"
weight: "49"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35156
- /detailsc930.html
imported:
- "2019"
_4images_image_id: "35156"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35156 -->
Im Vordergrund ist ein Teil meines Modells zu sehen. Die Besucher konnten per Joystick den Roboter steuern.