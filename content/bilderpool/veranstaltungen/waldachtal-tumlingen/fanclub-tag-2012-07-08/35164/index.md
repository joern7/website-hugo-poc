---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix57.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35164
- /details6584.html
imported:
- "2019"
_4images_image_id: "35164"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35164 -->
Auf dem Foto kann man nicht nur meinen Bruder, sonder auch unten noch die Alarmananlage von ihm und das Boot in Zusammenarbeit von Tobias und Marius, seinem Kumpel, sehen.