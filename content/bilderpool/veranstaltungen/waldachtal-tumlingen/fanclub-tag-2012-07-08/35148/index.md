---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix41.jpg"
weight: "41"
konstrukteure: 
- "Magnus & Konrad Fox"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35148
- /details0e70-2.html
imported:
- "2019"
_4images_image_id: "35148"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35148 -->
King of the Road