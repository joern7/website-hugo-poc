---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35128
- /details8a2c-2.html
imported:
- "2019"
_4images_image_id: "35128"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35128 -->
Vorstellung der neuen Kästen