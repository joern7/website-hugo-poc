---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35131
- /details6f79-2.html
imported:
- "2019"
_4images_image_id: "35131"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35131 -->
Vorstellung der neuen Kästen