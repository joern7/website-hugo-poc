---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35115
- /details5e45.html
imported:
- "2019"
_4images_image_id: "35115"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35115 -->
Stand, bzw. Modell von uns. Hier sieht man gerade den Traktor mit Tieflader. Mit Hilfe der Zapfwelle kann man die Ladefläche des Tiefladers hoch und runter kippen. Mit dabei hatten wir noch einen Roboter den die Besucher per Joystick steuern durften und mein Bruder hatte außer seinem Schlepper auch noch seine Alarmanlage und ein Boot dabei.