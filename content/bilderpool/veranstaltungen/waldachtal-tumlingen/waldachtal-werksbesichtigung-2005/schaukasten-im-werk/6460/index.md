---
layout: "image"
title: "Schautafeln im Empfangsraum"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung001.jpg"
weight: "1"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6460
- /detailseb8a-2.html
imported:
- "2019"
_4images_image_id: "6460"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6460 -->
In dem großen Empfangssaal, in dem wir begrüßt wurden, hängen Schaukästen an der Wand. Sie enthalten Informationen über den Lebenslauf von Herrn Prof. Dr. Fischer, die fischer-Geschichte, Originaldokumente und Produktbeispiele.