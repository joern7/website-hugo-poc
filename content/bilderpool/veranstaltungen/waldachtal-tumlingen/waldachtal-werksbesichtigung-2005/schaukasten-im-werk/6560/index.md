---
layout: "image"
title: "Schaukasten in der Nähe des Museums"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung101.jpg"
weight: "18"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6560
- /details097d.html
imported:
- "2019"
_4images_image_id: "6560"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "101"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6560 -->
Wenige Schritte vom Museum entfernt stehen öffentliche Schaukäsen.