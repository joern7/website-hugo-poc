---
layout: "image"
title: "Vor dem Zulieferer"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung046.jpg"
weight: "23"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6505
- /details5823.html
imported:
- "2019"
_4images_image_id: "6505"
_4images_cat_id: "355"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6505 -->
