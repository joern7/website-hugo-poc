---
layout: "image"
title: "Im Artur Fischer-Museum"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung061.jpg"
weight: "31"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6520
- /detailsc18c.html
imported:
- "2019"
_4images_image_id: "6520"
_4images_cat_id: "595"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6520 -->
Frühe Varianten des Grundbausteins, die Artur Fischer damals ausprobiert hatte.