---
layout: "image"
title: "Besuch im Artur Fischer Museum"
date: "2005-05-29T10:34:59"
picture: "SANY001046.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4267
- /details94e9.html
imported:
- "2019"
_4images_image_id: "4267"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-29T10:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4267 -->
