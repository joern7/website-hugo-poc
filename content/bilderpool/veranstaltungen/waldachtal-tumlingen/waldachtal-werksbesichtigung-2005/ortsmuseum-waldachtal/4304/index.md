---
layout: "image"
title: "Fischerdübel im Ortsmuseum Waldachtal"
date: "2005-05-30T20:17:49"
picture: "Fischerdbel.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- /php/details/4304
- /details2624.html
imported:
- "2019"
_4images_image_id: "4304"
_4images_cat_id: "596"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4304 -->
