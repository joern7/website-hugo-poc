---
layout: "image"
title: "Fertigungsroboter"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung037.jpg"
weight: "49"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6496
- /details158a.html
imported:
- "2019"
_4images_image_id: "6496"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6496 -->
