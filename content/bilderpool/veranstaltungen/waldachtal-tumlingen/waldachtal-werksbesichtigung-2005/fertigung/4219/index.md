---
layout: "image"
title: "Industrieroboter in der Spritz-Gießerei"
date: "2005-05-28T12:20:09"
picture: "SANY01300.jpg"
weight: "1"
konstrukteure: 
- "---"
fotografen:
- "---"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4219
- /details66ca-2.html
imported:
- "2019"
_4images_image_id: "4219"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T12:20:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4219 -->
Industrieroboter der Bausteine sortiert.