---
layout: "image"
title: "Maschinen in der Spritz-Gießerei"
date: "2005-05-28T22:47:53"
picture: "SANY001034.jpg"
weight: "13"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Marius Moosmann"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4257
- /details4a41.html
imported:
- "2019"
_4images_image_id: "4257"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:47:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4257 -->
