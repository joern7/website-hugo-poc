---
layout: "image"
title: "HESA_Spritzguss"
date: "2005-05-30T20:17:50"
picture: "HESA_Spritzguss_Streben_Schritt_1.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- /php/details/4317
- /details1872.html
imported:
- "2019"
_4images_image_id: "4317"
_4images_cat_id: "594"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4317 -->
