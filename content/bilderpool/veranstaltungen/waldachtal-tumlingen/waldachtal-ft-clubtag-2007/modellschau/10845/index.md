---
layout: "image"
title: "Simba"
date: "2007-06-10T21:09:24"
picture: "ft-Clubtag_-_39.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10845
- /details7459.html
imported:
- "2019"
_4images_image_id: "10845"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:09:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10845 -->
