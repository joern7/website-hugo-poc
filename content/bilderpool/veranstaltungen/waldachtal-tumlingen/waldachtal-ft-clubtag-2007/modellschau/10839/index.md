---
layout: "image"
title: "Karussell"
date: "2007-06-10T21:08:19"
picture: "ft-Clubtag_-_33.jpg"
weight: "7"
konstrukteure: 
- "J.W. Dekker"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10839
- /details4d9f.html
imported:
- "2019"
_4images_image_id: "10839"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:08:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10839 -->
