---
layout: "image"
title: "Sidelifter (Airbus Ground Support)"
date: "2007-06-10T21:10:23"
picture: "ft-Clubtag_-_42.jpg"
weight: "16"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10848
- /details84a5.html
imported:
- "2019"
_4images_image_id: "10848"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:10:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10848 -->
