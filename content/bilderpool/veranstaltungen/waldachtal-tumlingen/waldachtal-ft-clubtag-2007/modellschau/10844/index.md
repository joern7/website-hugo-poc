---
layout: "image"
title: "Karussell"
date: "2007-06-10T21:09:16"
picture: "ft-Clubtag_-_38.jpg"
weight: "12"
konstrukteure: 
- "J.W. Dekker"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10844
- /detailsa6cb-2.html
imported:
- "2019"
_4images_image_id: "10844"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:09:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10844 -->
