---
layout: "image"
title: "Anodentransporter (Aluminiumwerk)"
date: "2007-06-10T21:09:49"
picture: "ft-Clubtag_-_41.jpg"
weight: "15"
konstrukteure: 
- "J.W. Dekker"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10847
- /details7a53.html
imported:
- "2019"
_4images_image_id: "10847"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:09:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10847 -->
