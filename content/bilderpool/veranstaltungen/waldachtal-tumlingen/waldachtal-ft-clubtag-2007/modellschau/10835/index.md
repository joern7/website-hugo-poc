---
layout: "image"
title: "Brücke"
date: "2007-06-10T21:06:54"
picture: "ft-Clubtag_-_29.jpg"
weight: "3"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10835
- /detailsca29.html
imported:
- "2019"
_4images_image_id: "10835"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:06:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10835 -->
