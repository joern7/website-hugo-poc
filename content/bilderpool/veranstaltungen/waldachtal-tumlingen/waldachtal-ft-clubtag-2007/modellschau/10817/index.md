---
layout: "image"
title: "Brücke"
date: "2007-06-10T21:01:49"
picture: "ft-Clubtag_-_10.jpg"
weight: "1"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10817
- /details7ed1.html
imported:
- "2019"
_4images_image_id: "10817"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:01:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10817 -->
