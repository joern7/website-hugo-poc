---
layout: "image"
title: "Harald, Schnaggels, Thkais"
date: "2007-06-10T20:59:52"
picture: "ft-Clubtag_-_02.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10809
- /detailsdcc0.html
imported:
- "2019"
_4images_image_id: "10809"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T20:59:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10809 -->
