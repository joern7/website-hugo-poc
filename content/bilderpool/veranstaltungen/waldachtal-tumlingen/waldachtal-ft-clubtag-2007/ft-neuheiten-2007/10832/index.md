---
layout: "image"
title: "Spuren 2"
date: "2007-06-10T21:06:16"
picture: "ft-Clubtag_-_26.jpg"
weight: "13"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10832
- /details22fd.html
imported:
- "2019"
_4images_image_id: "10832"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:06:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10832 -->
