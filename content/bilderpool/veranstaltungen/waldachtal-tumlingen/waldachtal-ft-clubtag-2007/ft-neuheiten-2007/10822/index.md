---
layout: "image"
title: "Hebebühne"
date: "2007-06-10T21:03:55"
picture: "ft-Clubtag_-_15.jpg"
weight: "3"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10822
- /details4b51.html
imported:
- "2019"
_4images_image_id: "10822"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:03:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10822 -->
