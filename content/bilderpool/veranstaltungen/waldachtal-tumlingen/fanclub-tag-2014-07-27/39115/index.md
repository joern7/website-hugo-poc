---
layout: "image"
title: "Pong-Spiel"
date: "2014-07-31T17:00:42"
picture: "DSC00545_bearb.jpg"
weight: "14"
konstrukteure: 
- "TST"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39115
- /details94bf.html
imported:
- "2019"
_4images_image_id: "39115"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-31T17:00:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39115 -->
