---
layout: "image"
title: "Neue Fernsteuerung via Bluetooth auf Android QRCodes"
date: "2014-07-31T17:00:42"
picture: "Unbenannt.png"
weight: "16"
konstrukteure: 
- "Benedikt"
fotografen:
- "Bennik"
keywords: ["Fan", "Club", "Tag", "QR", "Code", "App", "Android", "Smartphone", "Steuerung"]
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/39117
- /details4c66.html
imported:
- "2019"
_4images_image_id: "39117"
_4images_cat_id: "2924"
_4images_user_id: "1549"
_4images_image_date: "2014-07-31T17:00:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39117 -->
Noch ein größeres Bild der QR Codes.