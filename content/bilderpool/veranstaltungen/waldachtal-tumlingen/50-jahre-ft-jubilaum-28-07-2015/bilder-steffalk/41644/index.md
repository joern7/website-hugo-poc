---
layout: "image"
title: "Haralds Modell"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk161.jpg"
weight: "161"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41644
- /details5b2b.html
imported:
- "2019"
_4images_image_id: "41644"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "161"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41644 -->
Im Vordergrund der Containerträger.