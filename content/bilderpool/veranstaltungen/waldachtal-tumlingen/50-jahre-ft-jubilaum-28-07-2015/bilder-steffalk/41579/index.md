---
layout: "image"
title: "Flip-Flop-Techniken"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk096.jpg"
weight: "96"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41579
- /details6589-2.html
imported:
- "2019"
_4images_image_id: "41579"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41579 -->
mit einer diskret aufgebauten elektronischen Schaltung mit zwei Transistoren aus dem ft-Elektronik-Praktikum