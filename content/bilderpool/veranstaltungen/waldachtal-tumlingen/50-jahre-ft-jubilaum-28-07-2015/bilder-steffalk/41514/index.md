---
layout: "image"
title: "CNC-Fräsmaschine"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk031.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41514
- /detailsd843.html
imported:
- "2019"
_4images_image_id: "41514"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41514 -->
Da ist ein richtiger Bohrer eingespannt, der auch tatsächlich fräsen könnte.