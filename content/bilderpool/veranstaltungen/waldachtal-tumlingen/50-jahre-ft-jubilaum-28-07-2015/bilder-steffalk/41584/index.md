---
layout: "image"
title: "Robs Atomium"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk101.jpg"
weight: "101"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41584
- /details21df-2.html
imported:
- "2019"
_4images_image_id: "41584"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "101"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41584 -->
Ein wunderschön konstruiertes, angenehm ruhiges Modell. Das Flachdach des Pavillons in der Mitte besteht aus 8 grauen (!) Statikplatten 90x180, und es hat sehr viele der frühen 30x30-Fensterbausteine.