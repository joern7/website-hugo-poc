---
layout: "image"
title: "Traktor mit gebogener Statik"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk027.jpg"
weight: "27"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41510
- /details9cf1-2.html
imported:
- "2019"
_4images_image_id: "41510"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41510 -->
