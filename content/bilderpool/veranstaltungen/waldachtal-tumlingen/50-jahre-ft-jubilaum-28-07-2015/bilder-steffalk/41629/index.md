---
layout: "image"
title: "Industrieroboter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk146.jpg"
weight: "146"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41629
- /detailsbdd3.html
imported:
- "2019"
_4images_image_id: "41629"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "146"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41629 -->
