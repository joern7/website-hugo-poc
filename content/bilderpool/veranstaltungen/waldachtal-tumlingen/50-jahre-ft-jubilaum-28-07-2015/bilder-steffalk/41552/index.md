---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk069.jpg"
weight: "69"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41552
- /details5cbc-2.html
imported:
- "2019"
_4images_image_id: "41552"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41552 -->
