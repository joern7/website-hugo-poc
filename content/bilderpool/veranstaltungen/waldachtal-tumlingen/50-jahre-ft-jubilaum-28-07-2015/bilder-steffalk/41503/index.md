---
layout: "image"
title: "Peters Fin-Ray-Greifer"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk020.jpg"
weight: "20"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41503
- /details4a7c-3.html
imported:
- "2019"
_4images_image_id: "41503"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41503 -->
