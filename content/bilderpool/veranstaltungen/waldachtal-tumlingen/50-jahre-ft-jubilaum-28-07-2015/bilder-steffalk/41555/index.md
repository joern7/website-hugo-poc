---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk072.jpg"
weight: "72"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41555
- /detailsb2cc.html
imported:
- "2019"
_4images_image_id: "41555"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41555 -->
Das Firmenlogo ;-) schwenkt hin und her