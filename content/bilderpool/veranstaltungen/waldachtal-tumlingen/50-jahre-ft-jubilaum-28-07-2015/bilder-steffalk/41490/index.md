---
layout: "image"
title: "Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk007.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41490
- /detailsd6c2.html
imported:
- "2019"
_4images_image_id: "41490"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41490 -->
