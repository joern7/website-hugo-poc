---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk070.jpg"
weight: "70"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41553
- /details7e61-3.html
imported:
- "2019"
_4images_image_id: "41553"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41553 -->
Die Murmeln laufen von links oben im Bild nach dem Durchlaufen eines Loopings nach oben (!) in diesen Ring und verlassen ihn in Richtung rechts oben.