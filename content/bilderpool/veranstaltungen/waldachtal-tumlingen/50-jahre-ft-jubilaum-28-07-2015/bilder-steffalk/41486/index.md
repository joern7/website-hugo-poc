---
layout: "image"
title: "Kugelbahn mit besonderer Kugelübergabe"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41486
- /detailse153.html
imported:
- "2019"
_4images_image_id: "41486"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41486 -->
