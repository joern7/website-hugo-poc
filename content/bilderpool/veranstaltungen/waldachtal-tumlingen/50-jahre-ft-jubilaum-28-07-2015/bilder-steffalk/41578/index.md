---
layout: "image"
title: "Flip-Flop-Techniken"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk095.jpg"
weight: "95"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41578
- /details26c2.html
imported:
- "2019"
_4images_image_id: "41578"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41578 -->
mit dem 1990er-Jahre-Flip-Flop-Baustein (rechts oben Knobloch electronics Stromzufuhr-Adapter mit Bundhülsen)