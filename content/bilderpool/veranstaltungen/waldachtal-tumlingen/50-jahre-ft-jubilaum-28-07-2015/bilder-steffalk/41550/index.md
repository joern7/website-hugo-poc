---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk067.jpg"
weight: "67"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41550
- /detailsd778.html
imported:
- "2019"
_4images_image_id: "41550"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41550 -->
Mit einem Trichter in der einzig richtigen Form. Die Kugeln rasen in ihn lange Zeit wie wild im Kreis, bis sie endlich unten herausfallen. Auch sonst jede Menge spannender Details.