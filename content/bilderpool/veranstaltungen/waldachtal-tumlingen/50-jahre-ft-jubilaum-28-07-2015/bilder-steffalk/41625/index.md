---
layout: "image"
title: "Autoscooter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk142.jpg"
weight: "142"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41625
- /detailsd245-2.html
imported:
- "2019"
_4images_image_id: "41625"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "142"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41625 -->
Ein Pol der Stromversorgung kommt von der Metallplatte am Boden...