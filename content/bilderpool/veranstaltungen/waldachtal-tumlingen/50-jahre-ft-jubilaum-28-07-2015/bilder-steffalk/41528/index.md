---
layout: "image"
title: "Geschicklichkeitsspiel"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk045.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41528
- /details2ba5-2.html
imported:
- "2019"
_4images_image_id: "41528"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41528 -->
Dies ist nicht etwa ein automatischer Spurfolger, sondern ein Geschicklichkeitsspiel: Auf dem Fahrzeug kann man durch Drücken je eines Tasters eine Lenkbewegung bewirken. Zwischen den Tastern liegt eine Lichtschranke, in die man den Finger legen kann, um das Fahrzeug zu stoppen. Die Aufgabe ist, selber der Spur entlang bis zum Ziel zu fahren.