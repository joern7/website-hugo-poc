---
layout: "image"
title: "Stefan Roths Exotenkabinett"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk140.jpg"
weight: "140"
konstrukteure: 
- "Stefan Roth Sammlung"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41623
- /details00ac.html
imported:
- "2019"
_4images_image_id: "41623"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "140"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41623 -->
Hier gibt es Prototypen, Vorserienteile und andere Exoten!

Alle unverkäuflich.