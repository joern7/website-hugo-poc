---
layout: "image"
title: "Flip-Flop-Techniken"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk090.jpg"
weight: "90"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41573
- /detailsa569.html
imported:
- "2019"
_4images_image_id: "41573"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41573 -->
mit einem l-e1-Lichtelektronik-Stab von 1969