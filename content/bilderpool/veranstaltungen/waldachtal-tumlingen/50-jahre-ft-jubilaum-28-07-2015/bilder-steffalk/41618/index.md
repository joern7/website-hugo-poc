---
layout: "image"
title: "Modelle von Ralf und seiner Rasselbande"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk135.jpg"
weight: "135"
konstrukteure: 
- "Familie Geerken"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41618
- /details9e50-2.html
imported:
- "2019"
_4images_image_id: "41618"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "135"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41618 -->
Ein grandioses Schachbrett, die wunderbare Turmbergbahn mit Abt'scher Weiche und Silberling-Steuerung, rechts daneben ein handbetriebener Ventilator