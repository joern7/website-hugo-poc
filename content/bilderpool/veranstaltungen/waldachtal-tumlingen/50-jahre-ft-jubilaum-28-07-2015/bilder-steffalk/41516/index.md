---
layout: "image"
title: "Belastungstest (2)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk033.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41516
- /details2976.html
imported:
- "2019"
_4images_image_id: "41516"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41516 -->
In der unteren Modell hälfte werden U-Träger per Motorkraft verschoben...