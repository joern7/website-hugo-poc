---
layout: "image"
title: "Robs Atomium"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk102.jpg"
weight: "102"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41585
- /details9759.html
imported:
- "2019"
_4images_image_id: "41585"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41585 -->
