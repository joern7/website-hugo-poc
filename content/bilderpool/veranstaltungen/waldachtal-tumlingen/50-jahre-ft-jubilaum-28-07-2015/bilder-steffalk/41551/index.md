---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk068.jpg"
weight: "68"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41551
- /details5a19-3.html
imported:
- "2019"
_4images_image_id: "41551"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41551 -->
