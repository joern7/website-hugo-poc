---
layout: "image"
title: "Schiff"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk058.jpg"
weight: "58"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41541
- /details735e.html
imported:
- "2019"
_4images_image_id: "41541"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41541 -->
Ein imposantes Baggerschiff