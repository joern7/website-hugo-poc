---
layout: "image"
title: "Spinne"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk104.jpg"
weight: "104"
konstrukteure: 
- "Tino werner"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41587
- /details55df-2.html
imported:
- "2019"
_4images_image_id: "41587"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "104"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41587 -->
Diese Spinne lief tatsächlich auf ihren Beinen sehr flott umher. Eine sehr raffinierte Mechanik mit einer Kombination aus gebogenen Achsen, die bei Drehung die Beine etwas anheben oder absenken, und Encodermotoren, die die Beine von einem Interface gesteuert geeignet nach vorne oder hinten versetzen, macht das möglich.