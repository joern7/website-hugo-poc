---
layout: "image"
title: "Landwirtschaftliche Fahrzeuge und Maschinen"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk026.jpg"
weight: "26"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41509
- /details9735.html
imported:
- "2019"
_4images_image_id: "41509"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41509 -->
