---
layout: "image"
title: "Brückenbau"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk107.jpg"
weight: "107"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41590
- /detailsfabc.html
imported:
- "2019"
_4images_image_id: "41590"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "107"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41590 -->
Dieses Modell stand im Obergeschoss.

Na? Erinnert sich jemand der alten Hasen an dieses Großmodell? Richtig! Dieses Brückenbaumodell zierte die Titelseite des Clubheftes April 1970! (siehe http://ft-datenbank.de/details.php?ArticleVariantId=f7aecfbd-9643-4916-8abd-f47300a0af38)

Dass ich DAS nochmal in echt sehen durfte!