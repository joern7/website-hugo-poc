---
layout: "image"
title: "Autoscooter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk143.jpg"
weight: "143"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41626
- /details7396.html
imported:
- "2019"
_4images_image_id: "41626"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "143"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41626 -->
... und der andere von einem Stromabnehmer an der Decke.