---
layout: "image"
title: "Svens Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk005.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41488
- /detailse8c8.html
imported:
- "2019"
_4images_image_id: "41488"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41488 -->
