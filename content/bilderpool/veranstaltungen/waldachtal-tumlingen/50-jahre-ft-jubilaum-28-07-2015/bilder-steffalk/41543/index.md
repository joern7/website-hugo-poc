---
layout: "image"
title: "Schiff"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk060.jpg"
weight: "60"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41543
- /details902b-3.html
imported:
- "2019"
_4images_image_id: "41543"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41543 -->
Interessante Kreuzverseilung