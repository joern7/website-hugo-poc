---
layout: "comment"
hidden: true
title: "20915"
date: "2015-07-29T07:41:57"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Die einzig mir bekannte Bezugsquelle in Deutschland:
http://www.shop.mathematikum.de/
=> Auf englische Sprache schalten!! Und dann nach 'Vortex' suchen. Ich finde keinen direkten Link, leider.

Direktimport geht natürlich auch, schaut Euch mal beim Hersteller um:
http://www.divnick.com/
Es ist der 'Vortx Miniature Wishing Well Toy'

Grüße
H.A.R.R.Y.