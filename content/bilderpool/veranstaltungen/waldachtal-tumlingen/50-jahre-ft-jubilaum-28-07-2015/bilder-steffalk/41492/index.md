---
layout: "image"
title: "Alfred Petteras Kirmesmodell (1)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk009.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41492
- /details016a.html
imported:
- "2019"
_4images_image_id: "41492"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41492 -->
Die beiden Arme drehen sich durch ein Getriebe im Antrieb entgegengesetzt...