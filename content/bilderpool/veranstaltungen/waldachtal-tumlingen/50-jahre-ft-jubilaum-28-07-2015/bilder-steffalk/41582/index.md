---
layout: "image"
title: "Riesen-Grundbaustein"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk099.jpg"
weight: "99"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41582
- /detailsb2e2.html
imported:
- "2019"
_4images_image_id: "41582"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41582 -->
Ein riesiger Nachbau des grauen ft-Grundbausteins aus fischergeometric (man vergleiche den echten Grundbaustein obenauf).