---
layout: "image"
title: "Unimog"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk076.jpg"
weight: "76"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41559
- /detailsf812-2.html
imported:
- "2019"
_4images_image_id: "41559"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41559 -->
Blick auf die Portalachskonstruktion