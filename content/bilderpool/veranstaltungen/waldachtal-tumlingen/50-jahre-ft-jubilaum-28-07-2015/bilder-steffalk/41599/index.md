---
layout: "image"
title: "Marcels Industriemodell"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk116.jpg"
weight: "116"
konstrukteure: 
- "Endlich"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41599
- /details5a4d-2.html
imported:
- "2019"
_4images_image_id: "41599"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "116"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41599 -->
In gewohnt perfekt schöner Verkabelung, und stabil auf einer Holzplatte verschraubt.