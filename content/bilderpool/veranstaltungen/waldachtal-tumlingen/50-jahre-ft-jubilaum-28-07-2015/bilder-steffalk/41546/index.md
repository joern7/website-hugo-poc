---
layout: "image"
title: "Flipper"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk063.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41546
- /detailsdb33.html
imported:
- "2019"
_4images_image_id: "41546"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41546 -->
Blick auf die Oberseite