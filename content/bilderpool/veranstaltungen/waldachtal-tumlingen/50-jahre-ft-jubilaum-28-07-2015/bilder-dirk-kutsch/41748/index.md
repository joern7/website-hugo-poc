---
layout: "image"
title: "Schaufelradbagger 289"
date: "2015-08-06T14:28:36"
picture: "dirk22.jpg"
weight: "22"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41748
- /detailsbb97.html
imported:
- "2019"
_4images_image_id: "41748"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41748 -->
