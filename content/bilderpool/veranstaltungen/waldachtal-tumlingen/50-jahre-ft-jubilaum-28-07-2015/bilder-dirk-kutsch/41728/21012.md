---
layout: "comment"
hidden: true
title: "21012"
date: "2015-08-09T08:10:57"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo,

@svefisch: Visualisierst Du das nur aus Deiner Erinnerung oder hast Du eventuell sogar ergänzende Fotos? Dann schick doch einen Scan/Abzug an Guiligan oder stell sie hier dazu.

Was meinst Du mit "45-er Gelenksteinen"? 31008 (http://ft-datenbank.de/details.php?ArticleVariantId=1fed392f-f79d-4dcf-9a7b-0fff14e9eccf)?

Grüße
H.A.R.R.Y.