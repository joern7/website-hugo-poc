---
layout: "image"
title: "Platten-Sortier-Anlage"
date: "2015-08-06T14:28:36"
picture: "dirk16.jpg"
weight: "16"
konstrukteure: 
- "Bennik"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41742
- /details42b9.html
imported:
- "2019"
_4images_image_id: "41742"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41742 -->
