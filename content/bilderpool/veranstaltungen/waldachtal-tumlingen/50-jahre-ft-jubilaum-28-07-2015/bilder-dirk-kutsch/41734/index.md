---
layout: "image"
title: "Detail 2/2"
date: "2015-08-06T14:28:36"
picture: "dirk08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41734
- /detailsf737.html
imported:
- "2019"
_4images_image_id: "41734"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41734 -->
... und selbst die Markierungen der Rollewagen haben ein fischer Symbol.