---
layout: "image"
title: "Zeichenschablone"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim012.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28435
- /details2e5e.html
imported:
- "2019"
_4images_image_id: "28435"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28435 -->
Zeichnet eine Ellipse