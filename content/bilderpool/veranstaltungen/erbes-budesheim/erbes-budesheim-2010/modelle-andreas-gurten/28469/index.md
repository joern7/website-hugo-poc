---
layout: "image"
title: "Andreas Gürten (Laserman)"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim046.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28469
- /details49fd.html
imported:
- "2019"
_4images_image_id: "28469"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28469 -->
