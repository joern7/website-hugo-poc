---
layout: "image"
title: "Pneumatisches Modell"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim151.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten (laserman)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28574
- /details093a-2.html
imported:
- "2019"
_4images_image_id: "28574"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "151"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28574 -->
