---
layout: "image"
title: "how it's done..."
date: "2010-10-03T15:00:56"
picture: "APP-2010-025026.jpg"
weight: "15"
konstrukteure: 
- "Thomas Falkenberg"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28913
- /detailsee2a.html
imported:
- "2019"
_4images_image_id: "28913"
_4images_cat_id: "2077"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28913 -->
