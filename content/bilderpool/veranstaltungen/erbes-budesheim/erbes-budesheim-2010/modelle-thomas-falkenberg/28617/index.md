---
layout: "image"
title: "Bauteile einer Abraumanlage"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim194.jpg"
weight: "10"
konstrukteure: 
- "Thomas Falkenberg"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28617
- /details36c5.html
imported:
- "2019"
_4images_image_id: "28617"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "194"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28617 -->
