---
layout: "image"
title: "Prototyp eines Abräumbaggers"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim014.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28437
- /details169b.html
imported:
- "2019"
_4images_image_id: "28437"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28437 -->
Noch in der Planung