---
layout: "image"
title: "Bausteinschieber"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim090.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28513
- /detailsf836-2.html
imported:
- "2019"
_4images_image_id: "28513"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28513 -->
