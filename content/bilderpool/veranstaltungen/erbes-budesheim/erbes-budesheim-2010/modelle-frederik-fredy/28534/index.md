---
layout: "image"
title: "Bausteinschieber"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim111.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28534
- /detailsf67e-2.html
imported:
- "2019"
_4images_image_id: "28534"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "111"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28534 -->
