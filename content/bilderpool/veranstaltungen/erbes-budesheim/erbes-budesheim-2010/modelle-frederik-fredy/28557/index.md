---
layout: "image"
title: "Boxen-Wechlser"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim134.jpg"
weight: "15"
konstrukteure: 
- "Frederik"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28557
- /details2c43.html
imported:
- "2019"
_4images_image_id: "28557"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "134"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28557 -->
Detail der Anlage