---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:37"
picture: "ssfs10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28339
- /detailsbf6d-3.html
imported:
- "2019"
_4images_image_id: "28339"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28339 -->
