---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:37"
picture: "ssfs02.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28331
- /detailsf7df.html
imported:
- "2019"
_4images_image_id: "28331"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28331 -->
