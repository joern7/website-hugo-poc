---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:41"
picture: "ssfs13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28342
- /details1720-4.html
imported:
- "2019"
_4images_image_id: "28342"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:41"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28342 -->
