---
layout: "image"
title: "Tieflader für Schwertransporte"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim119.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28542
- /details58d0-2.html
imported:
- "2019"
_4images_image_id: "28542"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "119"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28542 -->
