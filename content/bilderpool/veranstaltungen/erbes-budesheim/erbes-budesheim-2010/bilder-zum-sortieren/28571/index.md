---
layout: "image"
title: "Container-LKW"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim148.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28571
- /details5e55-3.html
imported:
- "2019"
_4images_image_id: "28571"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "148"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28571 -->
