---
layout: "image"
title: "Verschiebbares Kardangelenk zweifach"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim096.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28519
- /details3b9e-2.html
imported:
- "2019"
_4images_image_id: "28519"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28519 -->
