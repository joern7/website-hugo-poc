---
layout: "image"
title: "Ballgreifer"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim076.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28499
- /details4b3f.html
imported:
- "2019"
_4images_image_id: "28499"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28499 -->
Übergabes des Balls an einem anderen Greifer