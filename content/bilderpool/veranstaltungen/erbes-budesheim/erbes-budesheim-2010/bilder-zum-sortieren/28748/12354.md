---
layout: "comment"
hidden: true
title: "12354"
date: "2010-09-28T21:57:55"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Link zum Funktionieren: 

http://www.youtube.com/watch?v=6kFnCIemQq8 

Auch Festo hat neue Entwicklungen: 

Schau mal bei "Bionic Learning Network 2010" mit Finray-technic u.s.w.. 
Schau mal unter: 

http://www.festo.com/cms/de_de/13407.htm 

Festo –Bionic hat einige Jahren her eine pneumatischer Yo-Yo entwickelt. 
Ein pneumatischer Muskel is schnell und hat viel Kraft. 

Der Trick einer pneumatischer Muskel ist eine genaue Abstand-, Druck- und Zeit- Regulierung. 

Ich habe versucht mit Fischertechnik ein pneumatischer Yo-Yo zu machen, und es funktioniert !......endlich….. 
Mit der FT-US-Abstandsensor und der Robo-Interface bleibt das Schwungrad / Maxwell-Rad yo-yo-en. 

Link zu meiner Fischertechnik pneumatischer Yo-Yo: 
http://www.youtube.com/watch?v=Kkg8gvdcLYs 

Link zum Festo pneumatischer Yo-Yo: 
http://www.festo.com/ext/en/5893.htm 

Link zum Festo pneumatischer Yo-Yo-Film: 
http://www.festo.com/ext/en/5893_6312.htm#id_6050 

Festo –Bionic hat einige Jahren her eine pneumatischer Yo-Yo entwickelt. 
Ein pneumatischer Muskel is schnell und hat viel Kraft. 

Der Trick einer pneumatischer Muskel ist eine genaue Abstand-, Druck- und Zeit- Regulierung. 

Ich habe versucht mit Fischertechnik ein pneumatischer Yo-Yo zu machen, und es funktioniert !......endlich….. 
Mit der FT-US-Abstandsensor und der Robo-Interface bleibt das Schwungrad / Maxwell-Rad yo-yo-en. 

Link zu meiner Fischertechnik pneumatischer Yo-Yo: 
http://www.youtube.com/watch?v=Kkg8gvdcLYs 

Link zum Festo pneumatischer Yo-Yo: 
http://www.festo.com/ext/en/5893.htm 

Link zum Festo pneumatischer Yo-Yo-Film: 
http://www.festo.com/ext/en/5893_6312.htm#id_6050 

Wichtig is das die original Fischertechnik-Ventilen leider nicht gut funktionieren (wieder schliessen) bei einen hoheren Druck >0,85 bar ! 
Eine niederige Druck ist kein Problem, doch die Verkürzung ist dann aber geringer. 

Für hohere Drucken nutze ich 3-Way-Ventilen 11.12.3.BE.12.Q.8.8 (á 29 Euro/st.), Lieferant Sensortechnics 
http://www.sensortechnics.com/home 

Ich nutze standard Fahrrad Innenreifen 28/25-622/630

Link zum selbstbau Pneumatik Muskel gibt es unter:
http://www.ftcommunity.de/categories.php?cat_id=1695


Grüss, 

Peter Poederoyen NL