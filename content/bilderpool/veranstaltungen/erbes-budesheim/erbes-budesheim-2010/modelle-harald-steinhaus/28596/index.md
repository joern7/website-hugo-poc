---
layout: "image"
title: "Geländegängiges Fahrzeug"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim173.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28596
- /details30e0-3.html
imported:
- "2019"
_4images_image_id: "28596"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "173"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28596 -->
