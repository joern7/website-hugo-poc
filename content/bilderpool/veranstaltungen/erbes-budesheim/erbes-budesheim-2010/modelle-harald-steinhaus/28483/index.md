---
layout: "image"
title: "Harmonic-Drive"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim060.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28483
- /details674a.html
imported:
- "2019"
_4images_image_id: "28483"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28483 -->
