---
layout: "image"
title: "Flieger"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim026.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28449
- /details9203-2.html
imported:
- "2019"
_4images_image_id: "28449"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28449 -->
Konstruiert von Harald Steinhaus