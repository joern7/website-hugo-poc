---
layout: "image"
title: "Flieger Cockpit mit Fahrwerk"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim070.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28493
- /details5783.html
imported:
- "2019"
_4images_image_id: "28493"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28493 -->
