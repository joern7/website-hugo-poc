---
layout: "image"
title: "Panzer (2)"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim106.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28529
- /detailsc2a4-3.html
imported:
- "2019"
_4images_image_id: "28529"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28529 -->
