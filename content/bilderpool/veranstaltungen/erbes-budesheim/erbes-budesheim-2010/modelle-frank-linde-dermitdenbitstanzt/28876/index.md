---
layout: "image"
title: "2010 version of multi axe"
date: "2010-10-03T15:00:54"
picture: "FTconvention2010-20100925-14-00-11-DSC_3135.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28876
- /details7d00.html
imported:
- "2019"
_4images_image_id: "28876"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28876 -->
