---
layout: "image"
title: "stappenmotor"
date: "2010-10-03T15:00:55"
picture: "FTconvention2010-20100925-13-59-12-DSC_3132.jpg"
weight: "15"
konstrukteure: 
- "Frank Linde (DermitdenBitstanzt)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28888
- /details3804.html
imported:
- "2019"
_4images_image_id: "28888"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28888 -->
