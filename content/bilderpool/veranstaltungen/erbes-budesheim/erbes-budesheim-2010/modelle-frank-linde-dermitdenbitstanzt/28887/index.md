---
layout: "image"
title: "Way to use the servo!"
date: "2010-10-03T15:00:55"
picture: "FTconvention2010-20100925-13-08-16-DSC_3099_-_Version_2.jpg"
weight: "14"
konstrukteure: 
- "Frank Linde (DermitdenBitstanzt)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28887
- /detailsa1fd.html
imported:
- "2019"
_4images_image_id: "28887"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28887 -->
