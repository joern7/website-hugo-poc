---
layout: "image"
title: "The French corner (4)"
date: "2010-09-27T23:33:15"
picture: "msrds22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28678
- /details58ca-4.html
imported:
- "2019"
_4images_image_id: "28678"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28678 -->
