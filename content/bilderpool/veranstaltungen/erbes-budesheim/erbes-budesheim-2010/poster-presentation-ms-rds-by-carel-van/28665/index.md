---
layout: "image"
title: "Overview of my table"
date: "2010-09-27T23:33:15"
picture: "msrds09.jpg"
weight: "9"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: ["Robotic", "VPL", "Stepper"]
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28665
- /detailsadf6-2.html
imported:
- "2019"
_4images_image_id: "28665"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28665 -->
At the left the poster presentation of MS-Robotic Developers Studio with in the middle the ROBO Explorer model.

At the right the Stepper controller based on the AMIS 30622 controller