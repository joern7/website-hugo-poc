---
layout: "image"
title: "Poster Presentation 1"
date: "2010-09-27T23:33:15"
picture: "msrds10.jpg"
weight: "10"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28666
- /details0da4-2.html
imported:
- "2019"
_4images_image_id: "28666"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28666 -->
Introduction in MS-RDS.
MS-RDS and RoboPro.
http://web.inter.nl.net/users/Ussel-IntDev/fischertechnik_public/uk/index_MS-RDS.html