---
layout: "image"
title: "I2C based Stepper controller/sequencer"
date: "2010-09-27T23:33:15"
picture: "msrds16.jpg"
weight: "16"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28672
- /details7647.html
imported:
- "2019"
_4images_image_id: "28672"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28672 -->
http://web.inter.nl.net/users/Ussel-IntDev/fischertechnik_public/microcontrollers/Stepper_project.html