---
layout: "image"
title: "Poster Presentation 3"
date: "2010-09-27T23:33:15"
picture: "msrds12.jpg"
weight: "12"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28668
- /details0f28.html
imported:
- "2019"
_4images_image_id: "28668"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28668 -->
Part of the VPL-program to control the ft Explorer RoboInt based.
http://web.inter.nl.net/users/Ussel-IntDev/fischertechnik_public/uk/index_MS-RDS.html