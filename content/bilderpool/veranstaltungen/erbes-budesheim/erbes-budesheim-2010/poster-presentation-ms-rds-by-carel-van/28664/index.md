---
layout: "image"
title: "FT & MS-RDS Poster presentation"
date: "2010-09-27T23:33:15"
picture: "msrds08.jpg"
weight: "8"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28664
- /details66b4.html
imported:
- "2019"
_4images_image_id: "28664"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28664 -->
Fischertechnik under control of Microsoft's Robotic Developers Studio 2008 R3