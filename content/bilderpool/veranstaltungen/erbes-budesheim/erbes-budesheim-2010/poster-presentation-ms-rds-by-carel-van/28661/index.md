---
layout: "image"
title: "Presentation about MS-RDS and fischertechnik interfaces"
date: "2010-09-27T23:33:15"
picture: "msrds05.jpg"
weight: "5"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28661
- /details6ee3-3.html
imported:
- "2019"
_4images_image_id: "28661"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28661 -->
The start sheet.
For the power point 
http://web.inter.nl.net/users/Ussel-IntDev/fischertechnik_public/ft_convention_2010/index.html