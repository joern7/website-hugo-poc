---
layout: "image"
title: "Aufgang zum Fahrgeschäft"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim061.jpg"
weight: "2"
konstrukteure: 
- "Michael Sengstschmid (mirose)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28484
- /detailsa057-3.html
imported:
- "2019"
_4images_image_id: "28484"
_4images_cat_id: "2068"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28484 -->
