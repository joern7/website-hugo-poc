---
layout: "image"
title: "Show me the money...."
date: "2010-10-03T15:00:57"
picture: "APP-2010-025033.jpg"
weight: "13"
konstrukteure: 
- "Michael Sengstschmid (mirose)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28921
- /details805e-2.html
imported:
- "2019"
_4images_image_id: "28921"
_4images_cat_id: "2068"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28921 -->
