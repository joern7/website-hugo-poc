---
layout: "image"
title: "Looping"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim062.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (mirose)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28485
- /details6d23.html
imported:
- "2019"
_4images_image_id: "28485"
_4images_cat_id: "2068"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28485 -->
