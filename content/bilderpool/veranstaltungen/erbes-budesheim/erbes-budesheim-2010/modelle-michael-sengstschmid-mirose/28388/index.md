---
layout: "image"
title: "Achterbahn (Hochschaubahn)"
date: "2010-09-26T22:41:23"
picture: "Achterbahn_-_Michael_Sengstschmid_mirose.jpg"
weight: "1"
konstrukteure: 
- "Michael Sengstschmid (mirose)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28388
- /details5835.html
imported:
- "2019"
_4images_image_id: "28388"
_4images_cat_id: "2068"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T22:41:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28388 -->
Details siehe http://www.ftcommunity.de/categories.php?cat_id=1879