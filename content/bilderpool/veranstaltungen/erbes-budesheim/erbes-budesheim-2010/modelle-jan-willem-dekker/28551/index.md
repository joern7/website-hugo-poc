---
layout: "image"
title: "Kirmesmodelle"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim128.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28551
- /details6bf9.html
imported:
- "2019"
_4images_image_id: "28551"
_4images_cat_id: "2080"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "128"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28551 -->
