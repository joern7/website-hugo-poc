---
layout: "image"
title: "close up..."
date: "2010-10-03T15:00:57"
picture: "APP-2010-025027.jpg"
weight: "8"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28916
- /detailsa19e.html
imported:
- "2019"
_4images_image_id: "28916"
_4images_cat_id: "2080"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28916 -->
