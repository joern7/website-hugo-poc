---
layout: "image"
title: "Dampfmaschine"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim107.jpg"
weight: "3"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28530
- /details71c3.html
imported:
- "2019"
_4images_image_id: "28530"
_4images_cat_id: "2080"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "107"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28530 -->
