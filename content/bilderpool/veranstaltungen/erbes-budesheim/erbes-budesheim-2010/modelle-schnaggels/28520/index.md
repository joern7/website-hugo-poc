---
layout: "image"
title: "Hexapod"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim097.jpg"
weight: "2"
konstrukteure: 
- "Thomas Brestrich"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28520
- /details78d2.html
imported:
- "2019"
_4images_image_id: "28520"
_4images_cat_id: "2083"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28520 -->
