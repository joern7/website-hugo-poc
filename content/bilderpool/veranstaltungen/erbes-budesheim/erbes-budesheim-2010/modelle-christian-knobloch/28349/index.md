---
layout: "image"
title: "Firestorm (Gesamtansicht)"
date: "2010-09-26T18:05:12"
picture: "Firestorm_01_-_Christian_Knobloch.jpg"
weight: "45"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28349
- /details1f48.html
imported:
- "2019"
_4images_image_id: "28349"
_4images_cat_id: "2049"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:05:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28349 -->
Geniales Video unter
http://www.youtube.com/watch?v=5lnYKFNnwdg