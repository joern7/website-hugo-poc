---
layout: "image"
title: "Der Aufstieg"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim199.jpg"
weight: "57"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28622
- /detailsf711-2.html
imported:
- "2019"
_4images_image_id: "28622"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "199"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28622 -->
