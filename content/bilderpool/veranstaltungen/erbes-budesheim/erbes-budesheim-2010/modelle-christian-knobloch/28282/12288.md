---
layout: "comment"
hidden: true
title: "12288"
date: "2010-09-26T18:59:27"
uploadBy:
- "C-Knobloch"
license: "unknown"
imported:
- "2019"
---
Hallo Sebastian

Kannst du bitte noch ein paar Detailbilder von den Zügen machen (Magnete, Bügel usw.)?
- ja, die lade ich bald hoch
Was sind das für Räder und Magnete?
- Kunststoff Kugellager von Conrad und Magnete von supermagnete.de
Wie kriegst du es hin, die Züge in Blockabschnitten zu steuern?
-Mit Variablen (jeder Blockbereich 1 oder 0)
Wird es in absehbarer Zeit wieder die Möglichkeit geben, die Bahn live zu sehen?
-keine Ahnung
Kannst du bitte ein paar detail Bilder von dem Launch Abschnitt hochladen?
-ja, kann ich
Ich bin echt total faßziniert von der Anlage. Ich Arbeite im Moment auch an einem Freifallturm mit Wirbelstrombremse und kann mir vorstellen, wie schwierig die Justierung der Magneten gewesen sein muss.
- das war keinesfalls schwierig, kommt drauf an, wie manns macht ;)
Der Firestorm Megacoaster ist mein neus lieblings Modell!
- Danke!

Gruß, Christian