---
layout: "image"
title: "closer..."
date: "2010-10-03T15:00:56"
picture: "APP-2010-025011.jpg"
weight: "88"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28900
- /details1c72.html
imported:
- "2019"
_4images_image_id: "28900"
_4images_cat_id: "2049"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28900 -->
