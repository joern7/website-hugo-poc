---
layout: "image"
title: "Cristian Knobloch"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim17.jpg"
weight: "75"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28844
- /detailsf5d0.html
imported:
- "2019"
_4images_image_id: "28844"
_4images_cat_id: "2049"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28844 -->
Firestorm Megacoaster