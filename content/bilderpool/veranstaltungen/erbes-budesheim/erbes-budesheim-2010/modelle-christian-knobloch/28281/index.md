---
layout: "image"
title: "Der Eigangsbereich"
date: "2010-09-26T14:33:44"
picture: "firestorm3.jpg"
weight: "20"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/28281
- /details58a1.html
imported:
- "2019"
_4images_image_id: "28281"
_4images_cat_id: "2049"
_4images_user_id: "1007"
_4images_image_date: "2010-09-26T14:33:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28281 -->
