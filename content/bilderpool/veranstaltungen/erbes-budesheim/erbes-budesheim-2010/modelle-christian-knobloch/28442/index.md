---
layout: "image"
title: "Fist Drop/Lifthill"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim019.jpg"
weight: "48"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28442
- /details1e20-2.html
imported:
- "2019"
_4images_image_id: "28442"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28442 -->
