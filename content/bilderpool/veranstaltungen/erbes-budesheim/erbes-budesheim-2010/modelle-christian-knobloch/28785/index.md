---
layout: "image"
title: "Magnetträger"
date: "2010-09-30T19:17:29"
picture: "firestorm_upload02.jpg"
weight: "62"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28785
- /details30d7.html
imported:
- "2019"
_4images_image_id: "28785"
_4images_cat_id: "2049"
_4images_user_id: "997"
_4images_image_date: "2010-09-30T19:17:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28785 -->
jeder Magnet (4Stück pro Träger) hat eine Haftkraft von 8Kilo