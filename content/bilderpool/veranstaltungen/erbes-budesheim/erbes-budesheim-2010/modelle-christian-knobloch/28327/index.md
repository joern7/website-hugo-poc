---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:41"
picture: "achterbahn13.jpg"
weight: "42"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28327
- /detailsbd06-2.html
imported:
- "2019"
_4images_image_id: "28327"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:41"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28327 -->
