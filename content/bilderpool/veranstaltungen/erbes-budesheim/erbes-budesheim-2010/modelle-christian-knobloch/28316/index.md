---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:35"
picture: "achterbahn02.jpg"
weight: "31"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28316
- /details3538-2.html
imported:
- "2019"
_4images_image_id: "28316"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28316 -->
