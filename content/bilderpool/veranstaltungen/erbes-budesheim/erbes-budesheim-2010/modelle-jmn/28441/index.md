---
layout: "image"
title: "Loop"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim018.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28441
- /details5f3f.html
imported:
- "2019"
_4images_image_id: "28441"
_4images_cat_id: "2051"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28441 -->
