---
layout: "image"
title: "[1/5] Standübersicht von rechts"
date: "2010-09-30T14:02:17"
picture: "convention1.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28779
- /details6c0c.html
imported:
- "2019"
_4images_image_id: "28779"
_4images_cat_id: "2064"
_4images_user_id: "723"
_4images_image_date: "2010-09-30T14:02:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28779 -->
Ausgestellte Modelle Portalroboter 3D-XYZ-GES und Leitspindel-Drehmaschine DL1