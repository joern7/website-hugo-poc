---
layout: "image"
title: "Drehmaschine"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim161.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28584
- /detailsc550.html
imported:
- "2019"
_4images_image_id: "28584"
_4images_cat_id: "2064"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "161"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28584 -->
