---
layout: "image"
title: "[2/5] Standübersicht von links"
date: "2010-09-30T14:02:18"
picture: "convention2.jpg"
weight: "9"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28780
- /detailsbfb8-2.html
imported:
- "2019"
_4images_image_id: "28780"
_4images_cat_id: "2064"
_4images_user_id: "723"
_4images_image_date: "2010-09-30T14:02:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28780 -->
Ausgestellte Modelle Portalroboter 3D-XYZ-GES und Leitspindel-Drehmaschine DL1