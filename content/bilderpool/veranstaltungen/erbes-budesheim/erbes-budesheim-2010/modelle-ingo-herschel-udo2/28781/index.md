---
layout: "image"
title: "[3/5] Portalroboter 3D-XYZ-GES, Modellansicht"
date: "2010-09-30T14:02:18"
picture: "convention3.jpg"
weight: "10"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28781
- /details1da7-2.html
imported:
- "2019"
_4images_image_id: "28781"
_4images_cat_id: "2064"
_4images_user_id: "723"
_4images_image_date: "2010-09-30T14:02:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28781 -->
Ausführliche Beschreibung unter http://www.ftcommunity.de/details.php?image_id=28068