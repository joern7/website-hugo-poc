---
layout: "image"
title: "Sendemast dreieckig mit Blinklichtern"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28427
- /details2c88.html
imported:
- "2019"
_4images_image_id: "28427"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28427 -->
Konstrukteur: Dieter Meckel