---
layout: "image"
title: "Radlader und Sortieranlage"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim063.jpg"
weight: "5"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28486
- /detailsd3d6.html
imported:
- "2019"
_4images_image_id: "28486"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28486 -->
Radlader mit Pneumatischer Hebe- und Schaufelvorrichtung