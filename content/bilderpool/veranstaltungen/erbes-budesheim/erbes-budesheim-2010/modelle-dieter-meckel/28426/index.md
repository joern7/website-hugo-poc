---
layout: "image"
title: "Fire Truck mit Blinklicht"
date: "2010-09-27T19:56:18"
picture: "fischertechnikconventioninerbesbuedesheim003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28426
- /detailsfb66.html
imported:
- "2019"
_4images_image_id: "28426"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28426 -->
Konstrukteur: Dieter Meckel