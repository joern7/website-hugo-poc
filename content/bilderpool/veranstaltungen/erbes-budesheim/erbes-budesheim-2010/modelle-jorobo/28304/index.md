---
layout: "image"
title: "Coaster"
date: "2010-09-26T16:07:13"
picture: "eb22.jpg"
weight: "2"
konstrukteure: 
- "jorobo"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28304
- /details7368-2.html
imported:
- "2019"
_4images_image_id: "28304"
_4images_cat_id: "2092"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28304 -->
