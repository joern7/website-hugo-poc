---
layout: "image"
title: "Drehmaschine"
date: "2010-09-26T19:18:18"
picture: "Drehmaschine_-_Claus_Ludwig_claus.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28359
- /details5e46.html
imported:
- "2019"
_4images_image_id: "28359"
_4images_cat_id: "2061"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28359 -->
