---
layout: "image"
title: "Raupenkran"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim052.jpg"
weight: "10"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28475
- /detailsa4e4-2.html
imported:
- "2019"
_4images_image_id: "28475"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28475 -->
