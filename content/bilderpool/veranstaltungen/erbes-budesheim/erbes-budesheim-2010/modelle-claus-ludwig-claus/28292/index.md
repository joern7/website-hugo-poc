---
layout: "image"
title: "Drehbank"
date: "2010-09-26T16:06:48"
picture: "eb10.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28292
- /detailsea55-2.html
imported:
- "2019"
_4images_image_id: "28292"
_4images_cat_id: "2061"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28292 -->
