---
layout: "image"
title: "Volker James"
date: "2010-10-02T23:55:10"
picture: "2010-Erbes-Budesheim_070.jpg"
weight: "4"
konstrukteure: 
- "Volker James"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28808
- /details7170.html
imported:
- "2019"
_4images_image_id: "28808"
_4images_cat_id: "2066"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28808 -->
