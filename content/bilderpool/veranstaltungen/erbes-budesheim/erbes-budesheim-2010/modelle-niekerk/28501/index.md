---
layout: "image"
title: "3D-Drucker"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim078.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28501
- /details080d.html
imported:
- "2019"
_4images_image_id: "28501"
_4images_cat_id: "2102"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28501 -->
