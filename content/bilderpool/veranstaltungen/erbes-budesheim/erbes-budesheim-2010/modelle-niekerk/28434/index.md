---
layout: "image"
title: "3D-Drucker"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim011.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28434
- /detailse1cc.html
imported:
- "2019"
_4images_image_id: "28434"
_4images_cat_id: "2102"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28434 -->
