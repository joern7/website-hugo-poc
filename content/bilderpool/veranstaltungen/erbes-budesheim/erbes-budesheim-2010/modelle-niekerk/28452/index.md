---
layout: "image"
title: "3D-Drucker"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim029.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28452
- /detailsecc6.html
imported:
- "2019"
_4images_image_id: "28452"
_4images_cat_id: "2102"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28452 -->
