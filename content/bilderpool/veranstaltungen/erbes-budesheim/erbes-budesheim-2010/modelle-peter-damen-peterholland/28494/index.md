---
layout: "image"
title: "Traktor"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim071.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28494
- /detailsd1ab-2.html
imported:
- "2019"
_4images_image_id: "28494"
_4images_cat_id: "2059"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28494 -->
