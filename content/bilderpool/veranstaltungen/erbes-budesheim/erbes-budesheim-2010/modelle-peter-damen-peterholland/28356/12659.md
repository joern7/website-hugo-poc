---
layout: "comment"
hidden: true
title: "12659"
date: "2010-10-31T09:30:07"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
- Fischertechnik Inundatie-waaiersluis was sept. t/m okt. 2010 te zien in het Gorcums Museum in het kader van de expositie "Het genie van Gorcum".

- Fischertechnik Inundatie-waaiersluis is  t/m 31 maart 2011 te zien in het Nederlands Watermuseum te Arnhem het kader van het thema "De Hollandse Waterlinie".

Gruss, 

Peter Damen 
Poederoyen NL