---
layout: "image"
title: "Schleuse"
date: "2010-09-26T18:29:38"
picture: "Schleuse_-_Peter_Damen_peterholland.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28356
- /details8837-3.html
imported:
- "2019"
_4images_image_id: "28356"
_4images_cat_id: "2059"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:29:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28356 -->
