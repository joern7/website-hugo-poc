---
layout: "image"
title: "Fahrbarer Kran"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim129.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28552
- /details34e4.html
imported:
- "2019"
_4images_image_id: "28552"
_4images_cat_id: "2078"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "129"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28552 -->
