---
layout: "image"
title: "Mechanische Digitaluhr"
date: "2010-09-26T19:45:15"
picture: "Mechanische_Digitaluhr_-_Stefan_Falk_steffalk.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk (steffalk)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28365
- /details248d.html
imported:
- "2019"
_4images_image_id: "28365"
_4images_cat_id: "2062"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:45:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28365 -->
