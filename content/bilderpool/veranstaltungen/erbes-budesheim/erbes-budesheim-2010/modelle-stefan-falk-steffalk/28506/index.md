---
layout: "image"
title: "Beschreibung der mechanischen Digitaluhr"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim083.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28506
- /details3fd9-2.html
imported:
- "2019"
_4images_image_id: "28506"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28506 -->
