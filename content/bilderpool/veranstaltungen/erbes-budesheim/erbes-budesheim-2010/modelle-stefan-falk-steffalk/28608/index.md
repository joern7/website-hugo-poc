---
layout: "image"
title: "Impulseinheit der mechanischen Digitaluhr"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim185.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28608
- /details02f1.html
imported:
- "2019"
_4images_image_id: "28608"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "185"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28608 -->
