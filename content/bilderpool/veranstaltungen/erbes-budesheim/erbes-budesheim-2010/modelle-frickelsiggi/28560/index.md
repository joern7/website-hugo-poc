---
layout: "image"
title: "Ferngesteuerter Katamaran (2)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim137.jpg"
weight: "3"
konstrukteure: 
- "frickelsiggi  (Sigfried Kloster)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28560
- /details9594-2.html
imported:
- "2019"
_4images_image_id: "28560"
_4images_cat_id: "2086"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "137"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28560 -->
