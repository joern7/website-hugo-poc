---
layout: "image"
title: "fitec"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim038.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28461
- /details00ab-2.html
imported:
- "2019"
_4images_image_id: "28461"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28461 -->
