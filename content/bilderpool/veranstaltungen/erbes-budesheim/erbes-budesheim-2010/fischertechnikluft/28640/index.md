---
layout: "image"
title: "Stefan Falk"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim217.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28640
- /details6e8b.html
imported:
- "2019"
_4images_image_id: "28640"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "217"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28640 -->
