---
layout: "image"
title: "Ausstellerfrühstück"
date: "2010-09-26T19:45:15"
picture: "fischertechnikluft01.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28366
- /details2b5f.html
imported:
- "2019"
_4images_image_id: "28366"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28366 -->
25.09.2010, gegen 9 Uhr