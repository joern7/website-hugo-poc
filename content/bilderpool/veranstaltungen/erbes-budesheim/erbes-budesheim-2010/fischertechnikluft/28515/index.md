---
layout: "image"
title: "Michael Sengschmied (Mirose)"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim092.jpg"
weight: "24"
konstrukteure: 
- "Michael Sengschmied"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28515
- /details2349.html
imported:
- "2019"
_4images_image_id: "28515"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28515 -->
