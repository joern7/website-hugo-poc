---
layout: "image"
title: "Uhr"
date: "2010-09-26T12:10:02"
picture: "uhr1.jpg"
weight: "1"
konstrukteure: 
- "remadus"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28251
- /details668f.html
imported:
- "2019"
_4images_image_id: "28251"
_4images_cat_id: "2050"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:10:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28251 -->
