---
layout: "image"
title: "Pendeluhr"
date: "2010-09-26T16:07:13"
picture: "eb27.jpg"
weight: "13"
konstrukteure: 
- "Remadus"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28309
- /details44aa-2.html
imported:
- "2019"
_4images_image_id: "28309"
_4images_cat_id: "2050"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28309 -->
