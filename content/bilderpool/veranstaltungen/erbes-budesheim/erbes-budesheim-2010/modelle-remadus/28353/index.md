---
layout: "image"
title: "Pendeluhr (Gesamtansicht von vorn)"
date: "2010-09-26T18:25:09"
picture: "Pendeluhr_02_-_Remadus.jpg"
weight: "15"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28353
- /details7c91-3.html
imported:
- "2019"
_4images_image_id: "28353"
_4images_cat_id: "2050"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:25:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28353 -->
