---
layout: "image"
title: "Uhr"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim175.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28598
- /details09f0.html
imported:
- "2019"
_4images_image_id: "28598"
_4images_cat_id: "2050"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "175"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28598 -->
Tragwerk der Uhr