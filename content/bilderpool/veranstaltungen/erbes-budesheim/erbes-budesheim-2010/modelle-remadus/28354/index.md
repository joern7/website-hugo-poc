---
layout: "image"
title: "Pendeluhr (Gesamtansicht von links)"
date: "2010-09-26T18:25:09"
picture: "Pendeluhr_03_-_Remadus.jpg"
weight: "16"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28354
- /details2b11-2.html
imported:
- "2019"
_4images_image_id: "28354"
_4images_cat_id: "2050"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:25:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28354 -->
Detailbeschreibung unter http://www.ftcommunity.de/categories.php?cat_id=1886