---
layout: "image"
title: "Erbsentransportieranlage"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim117.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28540
- /details389c.html
imported:
- "2019"
_4images_image_id: "28540"
_4images_cat_id: "2082"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "117"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28540 -->
