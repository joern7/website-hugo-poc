---
layout: "image"
title: "Hochregal"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim143.jpg"
weight: "8"
konstrukteure: 
- "timtech"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28566
- /details7894.html
imported:
- "2019"
_4images_image_id: "28566"
_4images_cat_id: "2075"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "143"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28566 -->
