---
layout: "image"
title: "Modelle von den Endlich´s Brüdern"
date: "2010-09-27T17:18:36"
picture: "sfd01.jpg"
weight: "1"
konstrukteure: 
- "Endlich und sein Bruder"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28390
- /details52a7.html
imported:
- "2019"
_4images_image_id: "28390"
_4images_cat_id: "2058"
_4images_user_id: "1162"
_4images_image_date: "2010-09-27T17:18:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28390 -->
Hier seht ihr die Modelle von mir und meinem Bruder. Die Bilder entstanden kurz nach dem Aufbau.

Wir hatte folgende Modelle mit:

Maoamspender von meinem Bruder, das Herzlich-Willkommen-Schild von mir, das Industriemodell Pick-up von mir, der vollautomatische Wasserspender von mir.

MfG
Endlich

Ralf sagte zu uns beiden immer: Die Endlich´s Brüder ;)