---
layout: "image"
title: "Greifer des Roboters"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim067.jpg"
weight: "3"
konstrukteure: 
- "manumffilms"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28490
- /detailsc9b9.html
imported:
- "2019"
_4images_image_id: "28490"
_4images_cat_id: "2087"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28490 -->
