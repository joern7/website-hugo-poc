---
layout: "image"
title: "von vorne"
date: "2010-09-29T15:01:11"
picture: "modellevonmanuelankmanumffilms2.jpg"
weight: "7"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28751
- /details4398-2.html
imported:
- "2019"
_4images_image_id: "28751"
_4images_cat_id: "2087"
_4images_user_id: "934"
_4images_image_date: "2010-09-29T15:01:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28751 -->
7-Achs-Roboterarm + Greifer, gesteuert via Webcam - FRONT