---
layout: "image"
title: "Der Spiegel des Teleskops"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim102.jpg"
weight: "7"
konstrukteure: 
- "Fa. Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28525
- /detailsb5f0.html
imported:
- "2019"
_4images_image_id: "28525"
_4images_cat_id: "2053"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28525 -->
