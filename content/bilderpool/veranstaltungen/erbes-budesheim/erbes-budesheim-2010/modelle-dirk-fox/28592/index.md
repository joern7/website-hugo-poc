---
layout: "image"
title: "3-Etagenaufzug"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim169.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28592
- /details6377.html
imported:
- "2019"
_4images_image_id: "28592"
_4images_cat_id: "2089"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "169"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28592 -->
Unterste Etage Tur geöffnet mit Männchen