---
layout: "image"
title: "3-Etagenaufzug"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim167.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28590
- /details5372.html
imported:
- "2019"
_4images_image_id: "28590"
_4images_cat_id: "2089"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "167"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28590 -->
