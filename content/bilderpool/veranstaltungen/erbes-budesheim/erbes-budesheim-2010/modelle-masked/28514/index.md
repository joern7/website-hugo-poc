---
layout: "image"
title: "Riesenrad"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim091.jpg"
weight: "1"
konstrukteure: 
- "Martin Westphal"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28514
- /detailse81d.html
imported:
- "2019"
_4images_image_id: "28514"
_4images_cat_id: "2085"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28514 -->
