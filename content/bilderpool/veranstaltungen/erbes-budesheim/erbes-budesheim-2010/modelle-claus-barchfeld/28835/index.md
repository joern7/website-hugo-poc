---
layout: "image"
title: "Claus Barchfeld"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim08.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28835
- /details448e.html
imported:
- "2019"
_4images_image_id: "28835"
_4images_cat_id: "2052"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28835 -->
Robotermodelle