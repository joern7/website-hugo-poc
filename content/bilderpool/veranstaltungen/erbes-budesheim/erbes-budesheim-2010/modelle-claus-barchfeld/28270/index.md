---
layout: "image"
title: "Roboter"
date: "2010-09-26T12:18:53"
picture: "roboter8.jpg"
weight: "8"
konstrukteure: 
- "Claus"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28270
- /detailsa3a4.html
imported:
- "2019"
_4images_image_id: "28270"
_4images_cat_id: "2052"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:18:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28270 -->
