---
layout: "image"
title: "Roboterarme"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim007.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28430
- /details3382-2.html
imported:
- "2019"
_4images_image_id: "28430"
_4images_cat_id: "2052"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28430 -->
Verschiedene Arten von Roboterarmen