---
layout: "image"
title: "Claus Barchfeld"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim09.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28836
- /detailsc8ea-2.html
imported:
- "2019"
_4images_image_id: "28836"
_4images_cat_id: "2052"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28836 -->
Robotermodelle