---
layout: "image"
title: "Roboterarme"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim006.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28429
- /details8e4a-3.html
imported:
- "2019"
_4images_image_id: "28429"
_4images_cat_id: "2052"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28429 -->
Verschiedene Arten von Roboterarmen
