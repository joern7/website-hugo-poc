---
layout: "image"
title: "Roboterarme"
date: "2010-09-26T16:06:54"
picture: "eb18.jpg"
weight: "9"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28300
- /details1ff7-2.html
imported:
- "2019"
_4images_image_id: "28300"
_4images_cat_id: "2052"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:54"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28300 -->
