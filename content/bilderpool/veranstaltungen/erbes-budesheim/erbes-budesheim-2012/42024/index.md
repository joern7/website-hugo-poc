---
layout: "image"
title: "Link zum Video von der FT-Convention 2012"
date: "2015-10-01T18:18:47"
picture: "Video_von_der_FT-Convention_2012.png"
weight: "20"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42024
- /details7faa.html
imported:
- "2019"
_4images_image_id: "42024"
_4images_cat_id: "2639"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42024 -->
Video von der FT-Convention 2012:
https://www.youtube.com/watch?v=odaqgkquUSE