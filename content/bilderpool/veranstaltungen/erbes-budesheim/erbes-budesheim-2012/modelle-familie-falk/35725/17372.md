---
layout: "comment"
hidden: true
title: "17372"
date: "2012-10-04T19:07:37"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Du meinst, weil's so chaotisch aussieht mit den vielen Schläuchen? Tja, "schön" kann ich ja nicht. Und Pneumatikschläuche mögen halt auch nicht eckig verlegt werden.

Gruß,
Stefan