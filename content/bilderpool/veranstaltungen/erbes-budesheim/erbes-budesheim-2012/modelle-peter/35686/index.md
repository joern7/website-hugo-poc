---
layout: "image"
title: "Lanz Bulldog"
date: "2012-10-01T20:51:00"
picture: "ftconvention63.jpg"
weight: "3"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35686
- /details1b0f.html
imported:
- "2019"
_4images_image_id: "35686"
_4images_cat_id: "2668"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35686 -->
Schönes Modell vom LANZ BULLDOG.