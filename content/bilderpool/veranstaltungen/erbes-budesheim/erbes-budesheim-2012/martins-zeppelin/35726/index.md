---
layout: "image"
title: "Zeppelin von Martin Westphal"
date: "2012-10-03T10:58:59"
picture: "convention07_2.jpg"
weight: "6"
konstrukteure: 
- "Martin Westphal"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35726
- /details8ef1.html
imported:
- "2019"
_4images_image_id: "35726"
_4images_cat_id: "2648"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35726 -->
