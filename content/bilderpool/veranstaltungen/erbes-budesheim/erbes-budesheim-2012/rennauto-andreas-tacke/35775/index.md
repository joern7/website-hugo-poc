---
layout: "image"
title: "TSTs Rennauto 3"
date: "2012-10-03T19:51:54"
picture: "P1040973_verkleinert.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Bernhard Lehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/35775
- /detailsb9bf.html
imported:
- "2019"
_4images_image_id: "35775"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T19:51:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35775 -->
Später wurde das Ganze noch einmal vorgeführt, sodass ich mit veränderten Werten erhoffte, bessere Bilder hinzubekommen (was nur bedingt geglückt ist). 
Zuvor allerdings wurde viel getestet und gerechnet (Vielen Dank an steffalk, Emnet und Severin!); als Ergebnis fotografierte ich die folgenden Bilder mit 60 Bildern/s und 1/320s Belichtungszeit (sowie großer Blende als Ausgleich, damit die Helligkeit gleichbleibt).