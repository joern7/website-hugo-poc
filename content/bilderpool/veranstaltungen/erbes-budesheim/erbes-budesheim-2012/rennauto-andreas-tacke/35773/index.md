---
layout: "image"
title: "TSTs Rennauto 1"
date: "2012-10-03T19:41:01"
picture: "P1040615_verkleinert.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Bernhard Lehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/35773
- /details2c83.html
imported:
- "2019"
_4images_image_id: "35773"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T19:41:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35773 -->
Zum richtigen Zeitpunkt abdrücken ist sowieso unmöglich, also fotografierte ich mit Serienbild, 40 Bilder/s und 1/40s Belichtungszeit. 
(Das oben links ist das Auto.)