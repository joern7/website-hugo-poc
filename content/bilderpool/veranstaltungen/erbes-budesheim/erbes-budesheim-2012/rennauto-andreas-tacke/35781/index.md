---
layout: "image"
title: "TSTs Rennauto 9"
date: "2012-10-03T21:24:35"
picture: "P1050112_verkleinert.jpg"
weight: "6"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Bernhard Lehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/35781
- /detailse8e0-3.html
imported:
- "2019"
_4images_image_id: "35781"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T21:24:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35781 -->
Auch hier bestätigen sich sichtbar die Rechnungen von vorher.