---
layout: "image"
title: "KlappbrückeSystem Strobel 2"
date: "2012-09-29T21:24:59"
picture: "convention14.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35555
- /details4684-2.html
imported:
- "2019"
_4images_image_id: "35555"
_4images_cat_id: "2670"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:59"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35555 -->
Siehe KlappbrückeSystem Strobel1