---
layout: "image"
title: "Klappbrücke von Strobel"
date: "2012-10-03T10:59:00"
picture: "convention36.jpg"
weight: "18"
konstrukteure: 
- "strobel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35755
- /details680c.html
imported:
- "2019"
_4images_image_id: "35755"
_4images_cat_id: "2670"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35755 -->
