---
layout: "image"
title: "KlappbrückeSystem Strobel 1"
date: "2012-09-29T21:24:58"
picture: "convention13.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35554
- /details2eb7.html
imported:
- "2019"
_4images_image_id: "35554"
_4images_cat_id: "2670"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35554 -->
Nach Vorlage aus der Hobby-Buchreihe der 1970 Jahre (Hobby 1 - Band 3) Eine Besonderheit ist die Rollbewegung der Drehachse auf waagerechten Laufbahnen.
(Die muss ich mal nachbauen ;-) )