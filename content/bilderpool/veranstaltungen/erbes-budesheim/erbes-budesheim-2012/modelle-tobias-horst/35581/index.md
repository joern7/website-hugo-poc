---
layout: "image"
title: "Kirmesmodell 'Frisbee'"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim19.jpg"
weight: "1"
konstrukteure: 
- "tobs9578"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35581
- /details9918.html
imported:
- "2019"
_4images_image_id: "35581"
_4images_cat_id: "2669"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35581 -->
Schönes Modell an dem viele Besucher erstaunt anhielten.