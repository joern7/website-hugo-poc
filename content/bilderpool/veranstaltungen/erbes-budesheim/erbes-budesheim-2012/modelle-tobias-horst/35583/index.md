---
layout: "image"
title: "Kirmesmodell 'Frisbee'"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim21.jpg"
weight: "3"
konstrukteure: 
- "tobs9578"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35583
- /details152a-2.html
imported:
- "2019"
_4images_image_id: "35583"
_4images_cat_id: "2669"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35583 -->
Der Antrieb der dem Original in nichts nach stand.