---
layout: "image"
title: "Münzsortierer Detail"
date: "2015-10-01T13:37:10"
picture: "Mnzsortierer_Detail.jpg"
weight: "9"
konstrukteure: 
- "Mirose"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42006
- /details9658.html
imported:
- "2019"
_4images_image_id: "42006"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42006 -->
