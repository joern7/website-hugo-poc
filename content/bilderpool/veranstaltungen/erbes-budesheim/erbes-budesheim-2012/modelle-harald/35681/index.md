---
layout: "image"
title: "Kardangelenk"
date: "2012-10-01T20:51:00"
picture: "ftconvention58.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35681
- /details59b3-3.html
imported:
- "2019"
_4images_image_id: "35681"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35681 -->
Ein Funktionsmodell eines Kardangelenkes