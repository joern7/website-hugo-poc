---
layout: "image"
title: "Kardangelenk"
date: "2012-10-01T20:51:00"
picture: "ftconvention57.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35680
- /details90a3.html
imported:
- "2019"
_4images_image_id: "35680"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35680 -->
Ein Funktionsmodell eines Kardangelenkes