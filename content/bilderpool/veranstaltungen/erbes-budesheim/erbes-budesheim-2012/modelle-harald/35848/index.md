---
layout: "image"
title: "Flugzeug mit Roboter"
date: "2012-10-08T22:35:28"
picture: "ftconeb2.jpg"
weight: "19"
konstrukteure: 
- "Harald & Thanks for the fish"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/35848
- /details8be7.html
imported:
- "2019"
_4images_image_id: "35848"
_4images_cat_id: "2652"
_4images_user_id: "430"
_4images_image_date: "2012-10-08T22:35:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35848 -->
