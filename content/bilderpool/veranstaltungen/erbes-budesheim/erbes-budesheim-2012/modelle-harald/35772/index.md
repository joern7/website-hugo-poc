---
layout: "image"
title: "Unstealthed Stealth Bomber von Harald"
date: "2012-10-03T10:59:01"
picture: "convention53.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35772
- /detailsd718.html
imported:
- "2019"
_4images_image_id: "35772"
_4images_cat_id: "2652"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:01"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35772 -->
