---
layout: "image"
title: "Neuer Bomber"
date: "2012-10-01T20:51:00"
picture: "ftconvention55.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35678
- /details03a2.html
imported:
- "2019"
_4images_image_id: "35678"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35678 -->
Haralds neues Bombermodell. Es fehlen, lt. seiner Aussage noch ein paar Kleinigkeiten.