---
layout: "image"
title: "Rollwagen vom Mega Kran"
date: "2012-10-01T20:50:59"
picture: "ftconvention26.jpg"
weight: "11"
konstrukteure: 
- "W. Starreveld"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35649
- /details1528-2.html
imported:
- "2019"
_4images_image_id: "35649"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35649 -->
