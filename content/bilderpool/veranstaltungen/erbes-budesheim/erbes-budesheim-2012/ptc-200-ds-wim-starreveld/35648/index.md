---
layout: "image"
title: "Rollwagen vom Mega Kran"
date: "2012-10-01T20:50:59"
picture: "ftconvention25.jpg"
weight: "10"
konstrukteure: 
- "W. Starreveld"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35648
- /details3bc3.html
imported:
- "2019"
_4images_image_id: "35648"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35648 -->
