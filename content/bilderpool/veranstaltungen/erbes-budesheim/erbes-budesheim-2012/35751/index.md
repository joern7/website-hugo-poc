---
layout: "image"
title: "Sternenschaukel von David Dinse"
date: "2012-10-03T10:59:00"
picture: "convention32.jpg"
weight: "6"
konstrukteure: 
- "David Dinse"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35751
- /detailsc0bf.html
imported:
- "2019"
_4images_image_id: "35751"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35751 -->
