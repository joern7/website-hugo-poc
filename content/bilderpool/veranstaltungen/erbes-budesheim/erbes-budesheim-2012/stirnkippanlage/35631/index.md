---
layout: "image"
title: "Stirnkippanlage"
date: "2012-10-01T20:50:59"
picture: "ftconvention08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35631
- /details3775.html
imported:
- "2019"
_4images_image_id: "35631"
_4images_cat_id: "2658"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35631 -->
Ganz oben