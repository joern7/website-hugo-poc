---
layout: "image"
title: "Getränkeautomat von Nico Stutz"
date: "2012-10-03T10:58:59"
picture: "convention05_2.jpg"
weight: "2"
konstrukteure: 
- "Nico Stutz"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35724
- /details1a52.html
imported:
- "2019"
_4images_image_id: "35724"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35724 -->
