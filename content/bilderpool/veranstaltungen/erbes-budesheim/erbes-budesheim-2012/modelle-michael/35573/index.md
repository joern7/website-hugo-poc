---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim11.jpg"
weight: "11"
konstrukteure: 
- "DenkMal"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35573
- /detailsa17f.html
imported:
- "2019"
_4images_image_id: "35573"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35573 -->
Das Wellenlager