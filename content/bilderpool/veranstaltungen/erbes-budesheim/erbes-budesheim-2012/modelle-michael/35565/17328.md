---
layout: "comment"
hidden: true
title: "17328"
date: "2012-10-01T15:52:40"
uploadBy:
- "DenkMal"
license: "unknown"
imported:
- "2019"
---
Die Radaufnahme besteht je Stütze aus 3+2 BS15 mit Loch. Die äußeren BS15 bilden über die Stütze eine Art Gegengewicht. Die zuvor verwendete 235mm-Achse hatte das nicht und bog stärker durch, da die 15kg vom Rad die Stützen zu sich hin verzog.