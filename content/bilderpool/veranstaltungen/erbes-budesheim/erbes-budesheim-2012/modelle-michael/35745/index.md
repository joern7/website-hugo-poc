---
layout: "image"
title: "Riesenrad von Michael Stratmann"
date: "2012-10-03T10:59:00"
picture: "convention26.jpg"
weight: "20"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35745
- /detailsdafd.html
imported:
- "2019"
_4images_image_id: "35745"
_4images_cat_id: "2656"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35745 -->
