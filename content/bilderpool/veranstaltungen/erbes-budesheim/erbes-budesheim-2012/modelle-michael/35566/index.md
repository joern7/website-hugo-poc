---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim04.jpg"
weight: "4"
konstrukteure: 
- "DenkMal"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35566
- /details60f2-2.html
imported:
- "2019"
_4images_image_id: "35566"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35566 -->
Der Antrieb