---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim07.jpg"
weight: "7"
konstrukteure: 
- "DenkMal"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35569
- /details5fe7-2.html
imported:
- "2019"
_4images_image_id: "35569"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35569 -->
Nochmal die Gondel