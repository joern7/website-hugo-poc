---
layout: "image"
title: "Bohr- und Fräsmaschine von Ingo - Detailansicht"
date: "2012-10-03T10:59:00"
picture: "convention20.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35739
- /details9b06-2.html
imported:
- "2019"
_4images_image_id: "35739"
_4images_cat_id: "2657"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35739 -->
