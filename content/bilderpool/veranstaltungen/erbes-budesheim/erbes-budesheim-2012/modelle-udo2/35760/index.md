---
layout: "image"
title: "Portalroboter von Ingo - Detailansicht"
date: "2012-10-03T10:59:00"
picture: "convention41.jpg"
weight: "5"
konstrukteure: 
- "Ingo Herschel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35760
- /details6b06.html
imported:
- "2019"
_4images_image_id: "35760"
_4images_cat_id: "2657"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35760 -->
