---
layout: "image"
title: "ft-Plotter 30571 von Volker-James Münchhof"
date: "2012-10-03T10:59:00"
picture: "convention23.jpg"
weight: "4"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35742
- /detailsc203.html
imported:
- "2019"
_4images_image_id: "35742"
_4images_cat_id: "2657"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35742 -->
