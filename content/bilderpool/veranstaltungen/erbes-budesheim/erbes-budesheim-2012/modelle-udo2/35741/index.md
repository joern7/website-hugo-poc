---
layout: "image"
title: "Portalroboter von Ingo"
date: "2012-10-03T10:59:00"
picture: "convention22.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35741
- /detailsccf6.html
imported:
- "2019"
_4images_image_id: "35741"
_4images_cat_id: "2657"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35741 -->
