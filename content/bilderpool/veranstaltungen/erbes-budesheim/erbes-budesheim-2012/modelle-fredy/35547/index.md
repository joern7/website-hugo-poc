---
layout: "image"
title: "Kugelbahn1"
date: "2012-09-29T21:24:41"
picture: "convention06.jpg"
weight: "3"
konstrukteure: 
- "Fredy"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35547
- /details1bf7-2.html
imported:
- "2019"
_4images_image_id: "35547"
_4images_cat_id: "2653"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35547 -->
von Fredy