---
layout: "image"
title: "Alberreien"
date: "2012-10-01T20:51:00"
picture: "ftconvention81.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35704
- /details1649.html
imported:
- "2019"
_4images_image_id: "35704"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35704 -->
Hier haben Ralf Geerken und ich mal etwas rum gealbert und seinen "Trippel-Trappel" Robbi einfach mal auf ein anderes Modell gestellt.