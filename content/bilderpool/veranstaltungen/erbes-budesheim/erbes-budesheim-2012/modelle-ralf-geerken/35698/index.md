---
layout: "image"
title: "Alberreien"
date: "2012-10-01T20:51:00"
picture: "ftconvention75.jpg"
weight: "3"
konstrukteure: 
- "Claus W. Ludwig und Ralf Geerken"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35698
- /details7a20.html
imported:
- "2019"
_4images_image_id: "35698"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35698 -->
Da haben Ralf und ich uns einen Spass draus gemacht den Trippel Trappel Robbi auf einigen Modellen zu fotografieren. Hier nochmal auf dem Unimog.