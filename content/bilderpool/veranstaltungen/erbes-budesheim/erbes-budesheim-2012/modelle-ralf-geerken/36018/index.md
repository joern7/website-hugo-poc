---
layout: "image"
title: "DSC09246"
date: "2012-10-20T23:33:50"
picture: "conv132.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/36018
- /details00b6-2.html
imported:
- "2019"
_4images_image_id: "36018"
_4images_cat_id: "2663"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:50"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36018 -->
