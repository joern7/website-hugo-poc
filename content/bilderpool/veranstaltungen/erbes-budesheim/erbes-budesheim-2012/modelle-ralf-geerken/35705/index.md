---
layout: "image"
title: "Alberreien"
date: "2012-10-01T20:51:00"
picture: "ftconvention82.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35705
- /detailsa5a1.html
imported:
- "2019"
_4images_image_id: "35705"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35705 -->
Hier haben Ralf Geerken und ich mal etwas rumgealbert und seinen "Trippel-Trappel" Robbi einfach mal auf ein anderes Modell gestellt.