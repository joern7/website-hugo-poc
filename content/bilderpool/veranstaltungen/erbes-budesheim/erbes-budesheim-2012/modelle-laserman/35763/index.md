---
layout: "image"
title: "Paternoster von Laserman"
date: "2012-10-03T10:59:00"
picture: "convention44.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35763
- /details3296.html
imported:
- "2019"
_4images_image_id: "35763"
_4images_cat_id: "2659"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35763 -->
