---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-10-01T20:51:00"
picture: "ftconvention38.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35661
- /details3cac.html
imported:
- "2019"
_4images_image_id: "35661"
_4images_cat_id: "2654"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35661 -->
Unterbau mit Treppenauf- bzw. Abgang