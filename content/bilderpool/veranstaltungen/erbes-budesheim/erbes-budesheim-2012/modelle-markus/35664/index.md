---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-10-01T20:51:00"
picture: "ftconvention41.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35664
- /details9be9-2.html
imported:
- "2019"
_4images_image_id: "35664"
_4images_cat_id: "2654"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35664 -->
Noch zu sehen, der Unterbau mit den Treppen und den Rampen zu den einzelnen Waggons/Gondeln..