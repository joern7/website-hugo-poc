---
layout: "image"
title: "Prater von Markus Wolf - Gesamtansicht"
date: "2012-10-03T10:59:00"
picture: "convention16_2.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35735
- /detailsa9ad.html
imported:
- "2019"
_4images_image_id: "35735"
_4images_cat_id: "2654"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35735 -->
