---
layout: "image"
title: "Glücksspielautomat"
date: "2012-09-29T21:24:41"
picture: "convention03.jpg"
weight: "3"
konstrukteure: 
- "lasermann"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35544
- /details071c.html
imported:
- "2019"
_4images_image_id: "35544"
_4images_cat_id: "2651"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35544 -->
Man wirft eine 20ct Münze oben ein und manchmal kommen vorne wieder Münzen raus