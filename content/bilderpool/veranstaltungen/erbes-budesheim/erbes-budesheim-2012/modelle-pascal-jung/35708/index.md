---
layout: "image"
title: "Datenblatt"
date: "2012-10-01T20:51:00"
picture: "ftconvention85.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35708
- /details7e60.html
imported:
- "2019"
_4images_image_id: "35708"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35708 -->
Mit 810000 kg bzw 810 Tonnen nicht gerade ein Leichtgewicht aber dafür fasst die Schaufel ja auch 38 -42 m³.