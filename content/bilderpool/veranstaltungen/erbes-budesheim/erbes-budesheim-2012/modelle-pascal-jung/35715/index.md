---
layout: "image"
title: "Auslegerarm vom riesigen Minenbagger"
date: "2012-10-01T20:51:01"
picture: "ftconvention92.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35715
- /details5ea8-2.html
imported:
- "2019"
_4images_image_id: "35715"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:01"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35715 -->
Filigran aber doch stabil gebaut!