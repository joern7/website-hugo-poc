---
layout: "image"
title: "Kirmesmodell"
date: "2012-10-01T20:50:59"
picture: "ftconvention34.jpg"
weight: "1"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35657
- /details66c8-2.html
imported:
- "2019"
_4images_image_id: "35657"
_4images_cat_id: "2671"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35657 -->
Wird auch "Bratpfanne" genannt.