---
layout: "image"
title: "Kirmesmodell"
date: "2012-10-01T20:50:59"
picture: "ftconvention35.jpg"
weight: "2"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35658
- /detailsda11.html
imported:
- "2019"
_4images_image_id: "35658"
_4images_cat_id: "2671"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35658 -->
Klasse Modell