---
layout: "image"
title: "Getriebe mit zyklisch variables Übersetzungsverhältnis"
date: "2011-09-27T21:57:07"
picture: "zyklischvariablesgetriebe7.jpg"
weight: "10"
konstrukteure: 
- "Wilhelm Klopmeijer"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32922
- /detailsfab1.html
imported:
- "2019"
_4images_image_id: "32922"
_4images_cat_id: "2418"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:57:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32922 -->
