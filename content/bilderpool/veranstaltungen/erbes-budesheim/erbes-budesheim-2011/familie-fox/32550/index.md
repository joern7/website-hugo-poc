---
layout: "image"
title: "conventionerbesbuedesheim023.jpg"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim023.jpg"
weight: "16"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32550
- /details97f9.html
imported:
- "2019"
_4images_image_id: "32550"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32550 -->
