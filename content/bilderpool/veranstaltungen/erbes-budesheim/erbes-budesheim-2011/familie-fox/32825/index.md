---
layout: "image"
title: "Johanns Hubschrauber"
date: "2011-09-27T20:47:57"
picture: "Johanns_Sikorski.jpg"
weight: "24"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/32825
- /details2bba.html
imported:
- "2019"
_4images_image_id: "32825"
_4images_cat_id: "2386"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32825 -->
