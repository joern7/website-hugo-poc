---
layout: "image"
title: "Hubschrauber von Fam. Fox"
date: "2011-09-26T17:47:41"
picture: "dm065.jpg"
weight: "18"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32758
- /detailsfe37.html
imported:
- "2019"
_4images_image_id: "32758"
_4images_cat_id: "2386"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32758 -->
