---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:52:07"
picture: "hubschrauber09.jpg"
weight: "35"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32913
- /detailsce06.html
imported:
- "2019"
_4images_image_id: "32913"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:52:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32913 -->
