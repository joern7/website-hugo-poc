---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:51:59"
picture: "hubschrauber02.jpg"
weight: "28"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32906
- /details6cfb.html
imported:
- "2019"
_4images_image_id: "32906"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32906 -->
