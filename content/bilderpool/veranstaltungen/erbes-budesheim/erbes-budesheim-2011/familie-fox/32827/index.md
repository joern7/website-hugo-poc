---
layout: "image"
title: "Rotorkopf"
date: "2011-09-27T20:47:57"
picture: "Rotorkopf.jpg"
weight: "25"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/32827
- /details9474.html
imported:
- "2019"
_4images_image_id: "32827"
_4images_cat_id: "2386"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32827 -->
