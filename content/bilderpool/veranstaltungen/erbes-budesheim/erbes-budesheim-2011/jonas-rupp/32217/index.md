---
layout: "image"
title: "DSC05917"
date: "2011-09-25T20:36:33"
picture: "modelle043.jpg"
weight: "16"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32217
- /details1464.html
imported:
- "2019"
_4images_image_id: "32217"
_4images_cat_id: "2400"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32217 -->
