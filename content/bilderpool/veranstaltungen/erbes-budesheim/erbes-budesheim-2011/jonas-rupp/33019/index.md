---
layout: "image"
title: "Paketzentrum mit Rundregallager"
date: "2011-09-30T19:49:20"
picture: "paketzentrummitrundregallager11.jpg"
weight: "37"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan/Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/33019
- /details9d2f.html
imported:
- "2019"
_4images_image_id: "33019"
_4images_cat_id: "2400"
_4images_user_id: "1030"
_4images_image_date: "2011-09-30T19:49:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33019 -->
Das Modell ist fertig aufgebaut. Leider ist die Programmierung noch nicht fertig :-(