---
layout: "image"
title: "Umlaufendes Hochregallager"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim137.jpg"
weight: "19"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32664
- /detailsaffc.html
imported:
- "2019"
_4images_image_id: "32664"
_4images_cat_id: "2400"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "137"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32664 -->
Im Hintergrund sieht man viele Holztafeln. Die tragen Adressen und Barcodes. Rechts werden sie aufgelegt, hochtransportiert auf die vordere Ebene. Von dort rollen sie runter auf den Teil vor der steilen Rampe. Das muss so sein, weil die unterschiedlich hoch fährt und ihr unteres Teil also längenverschieblich sein muss (da rutschen die Teile drauf). Von der kleinen Plattform etwas über der Bildmitte werden sie eingelagert. Der Gag ist nun, dass es die Lager sind, die um das Oval umlaufen. Da verlaufen oben und unten ft-Schienen, in denen die Lagertürme eingehängt sind. Jeder Turm hat ein Stück Zahnstange, und in der Mitte oben rechts gibt es einen Antrieb mit zwei Powermotoren und zwei Z10, sodass immer mindestens eines Eingriff in eine Zahnstange hat.