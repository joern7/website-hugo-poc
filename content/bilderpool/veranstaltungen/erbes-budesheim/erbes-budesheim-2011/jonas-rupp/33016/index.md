---
layout: "image"
title: "Paketzentrum mit Rundregallager"
date: "2011-09-30T19:49:20"
picture: "paketzentrummitrundregallager08.jpg"
weight: "34"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan/Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/33016
- /details2baa.html
imported:
- "2019"
_4images_image_id: "33016"
_4images_cat_id: "2400"
_4images_user_id: "1030"
_4images_image_date: "2011-09-30T19:49:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33016 -->
Modell beim Aufbau