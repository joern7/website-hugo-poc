---
layout: "image"
title: "Bewässerungsanlage"
date: "2011-09-26T17:47:41"
picture: "dm093.jpg"
weight: "19"
konstrukteure: 
- "Marcel Endlich"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32786
- /details357d-2.html
imported:
- "2019"
_4images_image_id: "32786"
_4images_cat_id: "2383"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "93"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32786 -->
