---
layout: "image"
title: "Volles Auto"
date: "2011-09-25T15:25:21"
picture: "modelle1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32118
- /detailsbd69.html
imported:
- "2019"
_4images_image_id: "32118"
_4images_cat_id: "2383"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T15:25:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32118 -->
Unter dem hinteren Karton verbirgt sich noch mein "Kassettenbot" und in der vorderen Schachtel der Traktor. Unten im Radkasten waren noch Kabel und Werkzeuge.