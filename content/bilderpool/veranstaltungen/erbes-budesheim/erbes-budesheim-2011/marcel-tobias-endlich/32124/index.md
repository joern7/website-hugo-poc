---
layout: "image"
title: "Geldautomat"
date: "2011-09-25T15:25:22"
picture: "modelle7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32124
- /detailsb83c.html
imported:
- "2019"
_4images_image_id: "32124"
_4images_cat_id: "2383"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T15:25:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32124 -->
