---
layout: "image"
title: "ft Convention 2011"
date: "2011-09-25T20:06:24"
picture: "ftconvention6.jpg"
weight: "10"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/32169
- /details24e6.html
imported:
- "2019"
_4images_image_id: "32169"
_4images_cat_id: "2383"
_4images_user_id: "130"
_4images_image_date: "2011-09-25T20:06:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32169 -->
Sehr schöner "King off the Road".