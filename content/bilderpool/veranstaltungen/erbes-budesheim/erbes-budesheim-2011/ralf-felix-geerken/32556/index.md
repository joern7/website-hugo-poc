---
layout: "image"
title: "Ralf beim Jonglieren"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim029.jpg"
weight: "22"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32556
- /details1980.html
imported:
- "2019"
_4images_image_id: "32556"
_4images_cat_id: "2399"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32556 -->
Es gab mehrere Jonglier- und Balancier-Vorführungen zur Auflockerung. Sehr schöne Ergänzung!