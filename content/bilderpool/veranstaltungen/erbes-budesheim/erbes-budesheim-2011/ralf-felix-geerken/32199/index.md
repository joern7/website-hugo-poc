---
layout: "image"
title: "DSC05880"
date: "2011-09-25T20:36:33"
picture: "modelle025.jpg"
weight: "11"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32199
- /detailsc3d7.html
imported:
- "2019"
_4images_image_id: "32199"
_4images_cat_id: "2399"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32199 -->
