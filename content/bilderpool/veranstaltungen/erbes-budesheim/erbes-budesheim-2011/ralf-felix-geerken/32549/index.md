---
layout: "image"
title: "Fliegerkarussell"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim022.jpg"
weight: "21"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32549
- /details6167.html
imported:
- "2019"
_4images_image_id: "32549"
_4images_cat_id: "2399"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32549 -->
Die drei Flugzeuge haben Propellerantrieb per Akku (in der Mitte). Das Karussell dreht sich so von selbst. Und das Ganze wurde von Ralf dann noch auf einer langen Stange auf dem Kinn balanciert!