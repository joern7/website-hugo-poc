---
layout: "image"
title: "DSC05932"
date: "2011-09-25T20:36:33"
picture: "modelle051.jpg"
weight: "13"
konstrukteure: 
- "Felix Geerken"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32225
- /detailsef21.html
imported:
- "2019"
_4images_image_id: "32225"
_4images_cat_id: "2399"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32225 -->
