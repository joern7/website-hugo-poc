---
layout: "image"
title: "Haralds Flotte I"
date: "2011-09-27T20:47:57"
picture: "Haralds_Flotte_1.jpg"
weight: "38"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/32820
- /details8f87.html
imported:
- "2019"
_4images_image_id: "32820"
_4images_cat_id: "2393"
_4images_user_id: "1126"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32820 -->
Flotte im Teicheinsatz