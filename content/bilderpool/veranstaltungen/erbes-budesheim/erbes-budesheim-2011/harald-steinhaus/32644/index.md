---
layout: "image"
title: "Boot"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim117.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32644
- /details9a7b.html
imported:
- "2019"
_4images_image_id: "32644"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "117"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32644 -->
Draußen gab's einen Swimming Pool, in dem verschiedene Boote fuhren (dieses hier auch).