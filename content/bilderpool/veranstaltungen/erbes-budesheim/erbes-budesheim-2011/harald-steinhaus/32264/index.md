---
layout: "image"
title: "DSC05992"
date: "2011-09-25T20:36:33"
picture: "modelle090.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32264
- /details82e2.html
imported:
- "2019"
_4images_image_id: "32264"
_4images_cat_id: "2393"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32264 -->
