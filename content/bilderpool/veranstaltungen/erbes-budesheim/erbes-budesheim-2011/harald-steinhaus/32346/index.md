---
layout: "image"
title: "DSC06174"
date: "2011-09-25T20:36:34"
picture: "modelle172.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32346
- /detailsfb84-2.html
imported:
- "2019"
_4images_image_id: "32346"
_4images_cat_id: "2393"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "172"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32346 -->
