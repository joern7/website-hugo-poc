---
layout: "image"
title: "Spindelantrieb"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim124.jpg"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32651
- /details96e1.html
imported:
- "2019"
_4images_image_id: "32651"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32651 -->
Wieder ein Einsatz für ganz viele "Hülsen mit Scheibe".