---
layout: "image"
title: "Präzisionspendeluhr"
date: "2011-09-27T21:23:00"
picture: "Remadus_Praezisionsaufbau.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/32855
- /detailsa7f9.html
imported:
- "2019"
_4images_image_id: "32855"
_4images_cat_id: "2403"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T21:23:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32855 -->
