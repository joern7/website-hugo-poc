---
layout: "image"
title: "DSC05848"
date: "2011-09-25T20:36:33"
picture: "modelle004.jpg"
weight: "15"
konstrukteure: 
- "Frederik"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32178
- /detailsa85f.html
imported:
- "2019"
_4images_image_id: "32178"
_4images_cat_id: "2388"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32178 -->
