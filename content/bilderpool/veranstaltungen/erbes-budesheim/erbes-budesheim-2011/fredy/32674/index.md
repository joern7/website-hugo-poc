---
layout: "image"
title: "Murmelmodell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim147.jpg"
weight: "24"
konstrukteure: 
- "Frederik"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32674
- /details1c02.html
imported:
- "2019"
_4images_image_id: "32674"
_4images_cat_id: "2388"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "147"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32674 -->
Die Murmeln fallen in 15*15mm große Lücken des Zentralrades und laufen dann mehr oder weniger zufällig eine der vier Bahnen hinunter.