---
layout: "image"
title: "Hochregelar Stef Dijkstra"
date: "2011-09-28T17:25:57"
picture: "hochregelarstefdijkstra2.jpg"
weight: "12"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32984
- /details7c77-2.html
imported:
- "2019"
_4images_image_id: "32984"
_4images_cat_id: "2422"
_4images_user_id: "22"
_4images_image_date: "2011-09-28T17:25:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32984 -->
