---
layout: "image"
title: "Demonstrationsanlage eines Walzwerkes"
date: "2011-09-26T17:47:41"
picture: "dm018.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32711
- /details61f4.html
imported:
- "2019"
_4images_image_id: "32711"
_4images_cat_id: "2398"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32711 -->
