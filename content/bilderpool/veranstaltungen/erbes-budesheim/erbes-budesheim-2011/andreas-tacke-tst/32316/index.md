---
layout: "image"
title: "DSC06068"
date: "2011-09-25T20:36:34"
picture: "modelle142.jpg"
weight: "11"
konstrukteure: 
- "Andreas Tacke TST"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32316
- /details91a2.html
imported:
- "2019"
_4images_image_id: "32316"
_4images_cat_id: "2395"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "142"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32316 -->
