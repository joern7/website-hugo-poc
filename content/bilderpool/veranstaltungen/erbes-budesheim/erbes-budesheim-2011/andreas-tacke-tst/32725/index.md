---
layout: "image"
title: "Roboter mit Zentrumsspanner"
date: "2011-09-26T17:47:41"
picture: "dm032.jpg"
weight: "26"
konstrukteure: 
- "TST"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32725
- /details8c69.html
imported:
- "2019"
_4images_image_id: "32725"
_4images_cat_id: "2395"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32725 -->
