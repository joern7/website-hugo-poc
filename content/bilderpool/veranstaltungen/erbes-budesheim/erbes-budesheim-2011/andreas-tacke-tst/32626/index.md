---
layout: "image"
title: "Verschiedene Maschinen und Bauteilanwendungen"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim099.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32626
- /detailsbb36.html
imported:
- "2019"
_4images_image_id: "32626"
_4images_cat_id: "2395"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32626 -->
