---
layout: "image"
title: "Roboter mit Zentrumsspanner Details"
date: "2011-09-26T17:47:41"
picture: "dm033.jpg"
weight: "27"
konstrukteure: 
- "Andreas Tacke TST"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32726
- /detailsa9e9-2.html
imported:
- "2019"
_4images_image_id: "32726"
_4images_cat_id: "2395"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32726 -->
