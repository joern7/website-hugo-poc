---
layout: "image"
title: "Roboter mit Zentrumsspanner von Andreas Tacke (TST)"
date: "2011-09-26T17:47:41"
picture: "dm030.jpg"
weight: "24"
konstrukteure: 
- "Andreas Tacke TST"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32723
- /details5adc.html
imported:
- "2019"
_4images_image_id: "32723"
_4images_cat_id: "2395"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32723 -->
