---
layout: "image"
title: "pneumatischer Ballgreifer von Andreas Gürten (laserman)"
date: "2011-09-26T17:47:41"
picture: "dm020.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32713
- /detailsd0d8-2.html
imported:
- "2019"
_4images_image_id: "32713"
_4images_cat_id: "2409"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32713 -->
