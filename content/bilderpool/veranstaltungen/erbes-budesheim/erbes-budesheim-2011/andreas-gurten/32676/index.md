---
layout: "image"
title: "Anzeigetafel"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim149.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32676
- /detailsa0d5.html
imported:
- "2019"
_4images_image_id: "32676"
_4images_cat_id: "2409"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "149"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32676 -->
Je nach Drehung ist ein bestimmtes Zeichen sichtbar, wie auf Flughäfen.