---
layout: "image"
title: "Geldsortiermaschine"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim150.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32677
- /details73b0.html
imported:
- "2019"
_4images_image_id: "32677"
_4images_cat_id: "2409"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "150"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32677 -->
