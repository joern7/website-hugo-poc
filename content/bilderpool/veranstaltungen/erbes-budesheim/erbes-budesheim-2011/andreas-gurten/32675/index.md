---
layout: "image"
title: "3D-Drucker"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim148.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32675
- /detailsf3f3-2.html
imported:
- "2019"
_4images_image_id: "32675"
_4images_cat_id: "2409"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "148"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32675 -->
Hier der andere 3D-Drucker auf der Convention (zwei verschiedene waren vertreten)