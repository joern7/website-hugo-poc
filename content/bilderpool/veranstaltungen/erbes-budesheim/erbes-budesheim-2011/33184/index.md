---
layout: "image"
title: "fischertechnik-Oscar 2011"
date: "2011-10-16T20:34:56"
picture: "Oscar.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33184
- /details0427.html
imported:
- "2019"
_4images_image_id: "33184"
_4images_cat_id: "2380"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T20:34:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33184 -->
Zu Beginn der Convention wurden von Ralf Knobloch erstmalig drei fischertechnik-Oscars (als "Wanderpokal" für ein Jahr) für besonderes Engagement im Jahr 2011 in der fischertechnik-Community verliehen. Ausgezeichnet wurden Sven Engelke (für die ft:c), Stefan Falk und Dirk Fox (für die Herausgabe der ft:pedia).