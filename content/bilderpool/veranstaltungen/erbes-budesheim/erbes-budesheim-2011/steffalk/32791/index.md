---
layout: "image"
title: "Autokran von Stefan Falk"
date: "2011-09-26T17:47:41"
picture: "dm098.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32791
- /detailsf77f.html
imported:
- "2019"
_4images_image_id: "32791"
_4images_cat_id: "2390"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32791 -->
