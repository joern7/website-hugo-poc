---
layout: "image"
title: "Autokran  Stefan Falk"
date: "2011-09-27T23:24:31"
picture: "autokran2.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32933
- /detailsef07.html
imported:
- "2019"
_4images_image_id: "32933"
_4images_cat_id: "2390"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32933 -->
