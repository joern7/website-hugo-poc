---
layout: "image"
title: "Sessellift Michael Sengstschmid"
date: "2011-09-27T20:47:57"
picture: "sesselliftmichaelsengstschmid01.jpg"
weight: "26"
konstrukteure: 
- "Michael Sengstschmid"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32828
- /detailsf36d-4.html
imported:
- "2019"
_4images_image_id: "32828"
_4images_cat_id: "2407"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32828 -->
