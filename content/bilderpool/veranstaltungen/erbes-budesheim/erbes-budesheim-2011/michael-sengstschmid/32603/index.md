---
layout: "image"
title: "Riesen-Seilbahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim076.jpg"
weight: "22"
konstrukteure: 
- "mirose"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32603
- /detailsf0ff.html
imported:
- "2019"
_4images_image_id: "32603"
_4images_cat_id: "2407"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32603 -->
