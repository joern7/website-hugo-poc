---
layout: "image"
title: "DSC05906"
date: "2011-09-25T20:36:33"
picture: "modelle033.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32207
- /details42f3-2.html
imported:
- "2019"
_4images_image_id: "32207"
_4images_cat_id: "2407"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32207 -->
