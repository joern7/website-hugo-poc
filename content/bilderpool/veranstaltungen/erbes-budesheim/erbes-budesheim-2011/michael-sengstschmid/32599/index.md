---
layout: "image"
title: "Riesen-Seilbahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim072.jpg"
weight: "18"
konstrukteure: 
- "mirose"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32599
- /details2744.html
imported:
- "2019"
_4images_image_id: "32599"
_4images_cat_id: "2407"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32599 -->
Dies ist das eine Ende der Seilbahn mit der Antriebsstation (mit *vielen* Powermotoren im Inneren). Das andere Ende stand am anderen Ende der Halle! Die komplette Hallenlänge wurde damit überwunden!