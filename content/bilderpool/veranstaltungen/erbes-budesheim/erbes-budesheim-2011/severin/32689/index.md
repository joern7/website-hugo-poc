---
layout: "image"
title: "Severins Roboterarm"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim162.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32689
- /details508a-2.html
imported:
- "2019"
_4images_image_id: "32689"
_4images_cat_id: "2419"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "162"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32689 -->
