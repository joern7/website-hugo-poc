---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:00"
picture: "schwebebahngereonaltenbeck05.jpg"
weight: "23"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32860
- /detailsf88a.html
imported:
- "2019"
_4images_image_id: "32860"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32860 -->
