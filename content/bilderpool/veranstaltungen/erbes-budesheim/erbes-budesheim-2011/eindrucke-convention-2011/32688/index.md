---
layout: "image"
title: "Spurensucher"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim161.jpg"
weight: "24"
konstrukteure: 
- "André Mohr"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32688
- /details0abe.html
imported:
- "2019"
_4images_image_id: "32688"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "161"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32688 -->
Die Platine selbst entworfen, layoutet, und SMD-Teile darauf verlötet. Den Mikrocontroller selbst programmiert. Wieder ein noch junger fischertechnik-Fan, um dessen Zukunft wir uns wohl keine Sorge machen müssen!