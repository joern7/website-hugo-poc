---
layout: "image"
title: "Carels Modelle mit dem Microsoft Robotics Studio"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim043.jpg"
weight: "15"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32570
- /details61a1.html
imported:
- "2019"
_4images_image_id: "32570"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32570 -->
Mit einer Spielkonsolen-Fernsteuerung gesteuert.