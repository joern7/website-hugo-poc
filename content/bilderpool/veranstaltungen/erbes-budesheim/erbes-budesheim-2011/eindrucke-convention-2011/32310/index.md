---
layout: "image"
title: "DSC06060"
date: "2011-09-25T20:36:34"
picture: "modelle136.jpg"
weight: "10"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32310
- /detailse83c.html
imported:
- "2019"
_4images_image_id: "32310"
_4images_cat_id: "2381"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "136"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32310 -->
