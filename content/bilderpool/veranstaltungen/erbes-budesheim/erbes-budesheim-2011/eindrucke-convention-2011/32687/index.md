---
layout: "image"
title: "Spurensucher"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim160.jpg"
weight: "23"
konstrukteure: 
- "André Mohr"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32687
- /details44f3-2.html
imported:
- "2019"
_4images_image_id: "32687"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "160"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32687 -->
Steuerung siehe nächstes Bild