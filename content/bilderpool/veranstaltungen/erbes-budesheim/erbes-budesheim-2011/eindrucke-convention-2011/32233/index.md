---
layout: "image"
title: "DSC05942"
date: "2011-09-25T20:36:33"
picture: "modelle059.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32233
- /details17f0.html
imported:
- "2019"
_4images_image_id: "32233"
_4images_cat_id: "2381"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32233 -->
