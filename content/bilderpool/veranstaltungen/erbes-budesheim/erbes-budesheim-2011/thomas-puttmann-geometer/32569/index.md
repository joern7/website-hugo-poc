---
layout: "image"
title: "Geometers Planetarium im Dunkeln"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim042.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32569
- /details7222-2.html
imported:
- "2019"
_4images_image_id: "32569"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32569 -->
Thomas Püttmann (geometer) hatte jeweils 10minütige Vorträge über das Planetarium und was man da alles sieht gehalten.