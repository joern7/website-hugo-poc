---
layout: "image"
title: "Geometers Planetarium"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim038.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32565
- /detailsa521-2.html
imported:
- "2019"
_4images_image_id: "32565"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32565 -->
Tipp: Eine ausführliche Beschreibung seines *wunderbaren* Getriebes wird's wohl in der nächsten ft:pedia-Ausgabe geben!