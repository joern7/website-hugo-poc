---
layout: "image"
title: "Geometers 50-Hz-Uhr"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim037.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32564
- /detailsb58c.html
imported:
- "2019"
_4images_image_id: "32564"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32564 -->
Im Vordergrund der kleine 50-Hz-Motor (siehe ftc)