---
layout: "image"
title: "Planetarium + 50Hz-Uhr  Thomas Püttmann"
date: "2011-09-27T21:51:59"
picture: "planetariumhzuhr2.jpg"
weight: "13"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32899
- /details7906.html
imported:
- "2019"
_4images_image_id: "32899"
_4images_cat_id: "2410"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32899 -->
