---
layout: "image"
title: "off road"
date: "2011-09-30T17:29:19"
picture: "mein_off_roader-1.jpg"
weight: "18"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/33006
- /details7e9e.html
imported:
- "2019"
_4images_image_id: "33006"
_4images_cat_id: "2423"
_4images_user_id: "120"
_4images_image_date: "2011-09-30T17:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33006 -->
noch nicht vollendetes Fahrzeug :)
Antrieb und Lenkung noch zu schwach, macht aber jetzt schon Spaß!