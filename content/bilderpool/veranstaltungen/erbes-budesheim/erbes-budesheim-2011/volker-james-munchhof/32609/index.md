---
layout: "image"
title: "Boote und Clubmodell 'Harzer Fahrkunst'"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim082.jpg"
weight: "5"
konstrukteure: 
- "quincym"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32609
- /detailscf9a.html
imported:
- "2019"
_4images_image_id: "32609"
_4images_cat_id: "2427"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32609 -->
