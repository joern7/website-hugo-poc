---
layout: "image"
title: "conventionerbesbuedesheim058.jpg"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim058.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32585
- /details3ccf-2.html
imported:
- "2019"
_4images_image_id: "32585"
_4images_cat_id: "2443"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32585 -->
