---
layout: "image"
title: "DSC05943"
date: "2011-09-25T20:36:33"
picture: "modelle060.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32234
- /details6f24.html
imported:
- "2019"
_4images_image_id: "32234"
_4images_cat_id: "2429"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32234 -->
