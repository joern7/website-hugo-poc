---
layout: "image"
title: "DSC06050"
date: "2011-09-25T20:36:34"
picture: "modelle128.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32302
- /details7dce-2.html
imported:
- "2019"
_4images_image_id: "32302"
_4images_cat_id: "2416"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "128"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32302 -->
