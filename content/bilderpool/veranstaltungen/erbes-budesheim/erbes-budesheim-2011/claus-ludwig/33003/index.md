---
layout: "image"
title: "Oldtimer"
date: "2011-09-30T17:13:47"
picture: "IMG_6317.JPG"
weight: "8"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33003
- /details7788.html
imported:
- "2019"
_4images_image_id: "33003"
_4images_cat_id: "2416"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T17:13:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33003 -->
