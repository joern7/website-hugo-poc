---
layout: "image"
title: "DSC06053"
date: "2011-09-25T20:36:34"
picture: "modelle131.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32305
- /details76f4.html
imported:
- "2019"
_4images_image_id: "32305"
_4images_cat_id: "2406"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32305 -->
