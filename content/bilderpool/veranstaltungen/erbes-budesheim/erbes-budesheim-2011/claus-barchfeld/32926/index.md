---
layout: "image"
title: "Robotermodelle + PVC     Claus Barchfeld"
date: "2011-09-27T23:24:31"
picture: "robotermodellepvc3.jpg"
weight: "15"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32926
- /detailsf665-3.html
imported:
- "2019"
_4images_image_id: "32926"
_4images_cat_id: "2406"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32926 -->
