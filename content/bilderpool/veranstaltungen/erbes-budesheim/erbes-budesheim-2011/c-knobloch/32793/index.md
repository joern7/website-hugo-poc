---
layout: "image"
title: "Firestorm Megacoaster von Christian Knobloch"
date: "2011-09-26T17:47:41"
picture: "dm100.jpg"
weight: "103"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32793
- /details46dd.html
imported:
- "2019"
_4images_image_id: "32793"
_4images_cat_id: "2382"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32793 -->
