---
layout: "image"
title: "Firestorm Megacoaster 2 (12)"
date: "2011-09-25T17:42:28"
picture: "modelle11.jpg"
weight: "62"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32135
- /details8d23-2.html
imported:
- "2019"
_4images_image_id: "32135"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32135 -->
