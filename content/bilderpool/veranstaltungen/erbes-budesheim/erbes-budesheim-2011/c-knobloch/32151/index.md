---
layout: "image"
title: "Firestorm Megacoaster 2 (28)"
date: "2011-09-25T17:42:28"
picture: "modelle27.jpg"
weight: "78"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32151
- /detailsdce0.html
imported:
- "2019"
_4images_image_id: "32151"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32151 -->
