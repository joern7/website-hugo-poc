---
layout: "image"
title: "Firestorm Megacoaster 2 (17)"
date: "2011-09-25T17:42:28"
picture: "modelle16.jpg"
weight: "67"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32140
- /details9c02.html
imported:
- "2019"
_4images_image_id: "32140"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32140 -->
