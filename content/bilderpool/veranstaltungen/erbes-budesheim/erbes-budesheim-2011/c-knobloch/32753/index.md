---
layout: "image"
title: "Firestorm-Megacoaster von Christian Knobloch"
date: "2011-09-26T17:47:41"
picture: "dm060.jpg"
weight: "102"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32753
- /details1aa6-2.html
imported:
- "2019"
_4images_image_id: "32753"
_4images_cat_id: "2382"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32753 -->
