---
layout: "image"
title: "cknoblochfirestormmegacoaster27.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster27.jpg"
weight: "48"
konstrukteure: 
- "C- Knobloch Firestorm Megacoaster"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32114
- /detailsc8c0-3.html
imported:
- "2019"
_4images_image_id: "32114"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32114 -->
