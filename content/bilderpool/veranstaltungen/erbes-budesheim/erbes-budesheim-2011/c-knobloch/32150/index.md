---
layout: "image"
title: "Firestorm Megacoaster 2 (27)"
date: "2011-09-25T17:42:28"
picture: "modelle26.jpg"
weight: "77"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32150
- /detailsf471.html
imported:
- "2019"
_4images_image_id: "32150"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32150 -->
