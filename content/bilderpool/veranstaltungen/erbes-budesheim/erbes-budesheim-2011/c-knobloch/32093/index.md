---
layout: "image"
title: "cknoblochfirestormmegacoaster06.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster06.jpg"
weight: "27"
konstrukteure: 
- "C- Knobloch Firestorm Megacoaster"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32093
- /detailsd1bb.html
imported:
- "2019"
_4images_image_id: "32093"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32093 -->
