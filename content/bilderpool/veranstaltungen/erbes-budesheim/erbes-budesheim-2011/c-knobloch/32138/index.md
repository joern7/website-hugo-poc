---
layout: "image"
title: "Firestorm Megacoaster 2 (15)"
date: "2011-09-25T17:42:28"
picture: "modelle14.jpg"
weight: "65"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32138
- /details73d2.html
imported:
- "2019"
_4images_image_id: "32138"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32138 -->
