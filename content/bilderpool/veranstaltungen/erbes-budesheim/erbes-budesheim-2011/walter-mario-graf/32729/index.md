---
layout: "image"
title: "Fischertechnik Eisenbahn Spur 0 (2)"
date: "2011-09-26T17:47:41"
picture: "dm036.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32729
- /details6252-2.html
imported:
- "2019"
_4images_image_id: "32729"
_4images_cat_id: "2405"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32729 -->
