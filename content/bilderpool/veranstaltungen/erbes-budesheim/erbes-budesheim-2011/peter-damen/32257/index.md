---
layout: "image"
title: "DSC05976"
date: "2011-09-25T20:36:33"
picture: "modelle083.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32257
- /detailse3c1.html
imported:
- "2019"
_4images_image_id: "32257"
_4images_cat_id: "2396"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32257 -->
