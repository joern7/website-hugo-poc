---
layout: "image"
title: "Brückenlegepanzer 'Biber'"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim135.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32662
- /detailsb546.html
imported:
- "2019"
_4images_image_id: "32662"
_4images_cat_id: "2396"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "135"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32662 -->
