---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:29:54"
picture: "brueckenlegepanzerbiber01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32875
- /details6a62-2.html
imported:
- "2019"
_4images_image_id: "32875"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:29:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32875 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg
Massstab ca. 1:10. 

Wegen die zu grösse Kräfte und Momenten  ist meine Aluminium Panzerschnellbrücke nur 2x 1,0 = 2,0m  

Das "Hirschgeweih" das so wichtig für die Zusammenführung der beiden Segmente ist so gestaltet, dass es die Laufschiene umgreift und ein Abrutschen unmöglich macht.

Aluminium Panzerschnellbrücke:
2 x 2,6 kg  = 5,2 kg
2 x 1 m =  2,0 m (statt 2x 1,1 = 2,2m Massstab 1:10).  


Antrieb und Federung
Wie im Original:  7 Laufrollen je Seite, und per Drehstab gefedert.  Die Laufrollen haben schwarze Freilaufnaben. Die FT-Kette hat Kufen die zwischen den Rädern 60 hindurch läuft und damit die Kette in der Spur hält. 
Die Radaufhängung und zugleich "Federung" besteht aus A2-RVS-Achsen die ich pro Achse 2x 90 Grad biegen muss.   Bei mir machen die stahlen Schraubenfedern (nicht-FT) die ganze Arbeit. Die Stäben gehen aber wie beim Original bis zur anderen Seite, funktionel nur damit sich die Seitenteile nicht verbiegen und vorallem die ganze FT-Konstrukion stabiler wird.  Die Drehstäbe reichen bis zur Gegenseite durch und deshalb sind wie beim Leo auch die Aufstandsflächen der Ketten (links/rechts) unterschiedlich angeordnet - die eine Seite liegt etwas weiter hinten auf als die andere. 

Verschluss-mechanismus
Ich habe für meine  FT-Brückenlegepanzer-Biber nur eine einfache Verschluss-mechanismus
Jürgen Warwel hat in 2007 die tolle und zugleich patentierte Verbolzkinematik von Porsche nachgebaut.  Detailierte Zeichnungen könnt Ihr über DEPATSINET (Deutsches Patent und Markenamt) finden. Dokumentiert im US Patent 3,744,075 vom 10.07.1973. Das Deutsche Patent von 1970 ist nicht als pdf greifbar.
 
Links zum Entwicklung 
Bridge Tank TM-5-5420-203-14 :    http://www.ftcommunity.de/categories.php?cat_id=486   + http://www.ftcommunity.de/details.php?image_id=6397#col3     (Peter Poederoyen NL)

FT-Brückenlegepanzer-Biber :
http://www.ftcommunity.de/categories.php?cat_id=820   (Jürgen Warwel DE)
http://www.ftcommunity.de/details.php?image_id=30401#col3     +   http://www.ftcommunity.de/details.php?image_id=29427     (Dirk Kutsch DE)
http://www.ftcommunity.de/categories.php?cat_id=2214    (Peter Poederoyen NL)
