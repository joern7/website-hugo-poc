---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:29:54"
picture: "brueckenlegepanzerbiber07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32881
- /detailsa782.html
imported:
- "2019"
_4images_image_id: "32881"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:29:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32881 -->
