---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:30:04"
picture: "brueckenlegepanzerbiber14.jpg"
weight: "14"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32888
- /details866d.html
imported:
- "2019"
_4images_image_id: "32888"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:30:04"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32888 -->
