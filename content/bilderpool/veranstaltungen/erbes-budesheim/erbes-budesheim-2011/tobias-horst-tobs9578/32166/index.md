---
layout: "image"
title: "ft Convention 2011"
date: "2011-09-25T20:06:24"
picture: "ftconvention3.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/32166
- /details23ec-3.html
imported:
- "2019"
_4images_image_id: "32166"
_4images_cat_id: "2384"
_4images_user_id: "130"
_4images_image_date: "2011-09-25T20:06:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32166 -->
Schön gebaute Gondel vom Top Spin!