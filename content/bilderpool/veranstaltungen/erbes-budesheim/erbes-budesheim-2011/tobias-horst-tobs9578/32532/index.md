---
layout: "image"
title: "Plotter"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim005.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32532
- /details0da6.html
imported:
- "2019"
_4images_image_id: "32532"
_4images_cat_id: "2384"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32532 -->
