---
layout: "image"
title: "DSC05939"
date: "2011-09-25T20:36:33"
picture: "modelle056.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32230
- /detailsce64.html
imported:
- "2019"
_4images_image_id: "32230"
_4images_cat_id: "2430"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32230 -->
