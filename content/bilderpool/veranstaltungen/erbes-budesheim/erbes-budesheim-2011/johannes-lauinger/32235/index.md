---
layout: "image"
title: "DSC05944"
date: "2011-09-25T20:36:33"
picture: "modelle061.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32235
- /details987c.html
imported:
- "2019"
_4images_image_id: "32235"
_4images_cat_id: "2430"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32235 -->
