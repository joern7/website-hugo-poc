---
layout: "image"
title: "DSC05876"
date: "2011-09-25T20:36:33"
picture: "modelle021.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32195
- /detailse4f4.html
imported:
- "2019"
_4images_image_id: "32195"
_4images_cat_id: "2430"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32195 -->
