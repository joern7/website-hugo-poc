---
layout: "image"
title: "DSC05879"
date: "2011-09-25T20:36:33"
picture: "modelle024.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32198
- /details42c1.html
imported:
- "2019"
_4images_image_id: "32198"
_4images_cat_id: "2426"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32198 -->
