---
layout: "image"
title: "Schleifring"
date: "2011-09-30T17:05:17"
picture: "IMG_6283.JPG"
weight: "9"
konstrukteure: 
- "Reiner und Luisa Steg"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33002
- /detailsa94b.html
imported:
- "2019"
_4images_image_id: "33002"
_4images_cat_id: "2424"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T17:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33002 -->
Noch eine Variante, um Strom auf die Schleifringe zu bekommen. Die Andruckskraft kommt von der Schwerkraft plus dem roten Gummiring.

(und meine Kamera hatte einen dicken Fettfleck unten mittig auf der Linse. Das zieht sich quer durch alle Bilder.)