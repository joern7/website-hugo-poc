---
layout: "image"
title: "DSC06066"
date: "2011-09-25T20:36:34"
picture: "modelle141.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32315
- /detailsf977.html
imported:
- "2019"
_4images_image_id: "32315"
_4images_cat_id: "2417"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32315 -->
