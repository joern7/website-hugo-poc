---
layout: "image"
title: "Großer Hafenkran"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim106.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32633
- /details19ce.html
imported:
- "2019"
_4images_image_id: "32633"
_4images_cat_id: "2417"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32633 -->
Hier der Greifer