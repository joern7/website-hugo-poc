---
layout: "image"
title: "Robot Magazine Article"
date: "2010-02-26T21:03:42"
picture: "ft_con1.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Erbes-Budesheim", "convention", "Robot", "Magazine"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26544
- /detailscd6c.html
imported:
- "2019"
_4images_image_id: "26544"
_4images_cat_id: "1720"
_4images_user_id: "585"
_4images_image_date: "2010-02-26T21:03:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26544 -->
A snapshot of the article covering the convention in the May/June 2010 issue in Robot Magazine!