---
layout: "image"
title: "50 Hz Uhr"
date: "2015-10-01T13:37:10"
picture: "50_Hz_Uhr.jpg"
weight: "4"
konstrukteure: 
- "Geometer"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41939
- /details41da.html
imported:
- "2019"
_4images_image_id: "41939"
_4images_cat_id: "3119"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41939 -->
