---
layout: "image"
title: "Pistenbully 2"
date: "2014-10-05T13:51:13"
picture: "modellevonfamiliebusch2.jpg"
weight: "2"
konstrukteure: 
- "Jörg und Erik Busch"
fotografen:
- "Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39662
- /detailsb406.html
imported:
- "2019"
_4images_image_id: "39662"
_4images_cat_id: "2972"
_4images_user_id: "1126"
_4images_image_date: "2014-10-05T13:51:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39662 -->
