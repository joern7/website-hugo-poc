---
layout: "comment"
hidden: true
title: "20607"
date: "2015-05-07T22:16:02"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Danke für die Beschreibung der Inputs beim Programm !

Gruss  

Peter Poederoyen NL

Poti       (Po):      Messwerteingang
MWert   (MW):    Ist der gewünschte Mittelwert
MAbw    (MA):     Mindestabstand vom gewünschten Mittelwert, damit die Abweichung berücksichtigt wird

Die Abweichung ergibt sich aus Poti - MWert.
Bei positiver Abweichung vom Mittelwert wird die Abweichung mit PMult multipliziert, mit PTeiler dividiert und POffset dazuaddiert und ergibt die Variable Änderung.
Bei negativer Abweichung vom Mittelwert wird die Abweichung mit MMult multipliziert, mit MTeiler dividiert und MOffset dazuaddiertund ergibt die Variable Änderung
(Unsymmetrische Auslegung möglich)

Bei Add = 1 wird bei der Winde oder dem Spillkopf verwendet, damit der Geschwindigkeitswert um die Variable Änderung erhöht oder erniedrigt wird.
Bei Add = 0 wird die Variable Änderung als Geschwindigkeitswert verwendet, z.B. direkt für die Drehung der Winde.

PMax beschränkt den Geschwindigkeitswert bei positiven Werten, MMax das Maximum bei negativen Werten.

Offset erhöht die den Geschwindigkeitswerte, sowohl in positiver als auch in negativer Richtung.

Time gibt die Zeit * 10 ms an, in der die Regelschleife wiederholt wird.

Aus gibt die Motoransteuerung aus.