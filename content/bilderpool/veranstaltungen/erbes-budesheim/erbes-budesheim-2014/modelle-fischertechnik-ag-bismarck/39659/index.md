---
layout: "image"
title: "RoboCup-Roboter (in Aktion)"
date: "2014-10-04T23:18:43"
picture: "modellederfischertechnikagdesbismarckgymnasiumska6.jpg"
weight: "6"
konstrukteure: 
- "Schüler der fischertechnik-AG des Bismarck-Gymnasiums Karlsruhe"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39659
- /detailsea76.html
imported:
- "2019"
_4images_image_id: "39659"
_4images_cat_id: "2970"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39659 -->
Näheres im [Wiki der fischertechnik-AG am Bismarck-Gymnasium Karlsruhe]("http://www.fischertechnik-ag.de")