---
layout: "image"
title: "XL Baustein zur Convention 2014"
date: "2014-09-28T21:20:23"
picture: "xlbaustein1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39463
- /details8e5a.html
imported:
- "2019"
_4images_image_id: "39463"
_4images_cat_id: "2953"
_4images_user_id: "182"
_4images_image_date: "2014-09-28T21:20:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39463 -->
Hier noch mal für alle die den Pokal für Ralf nicht aus der Nähe gesehen haben ein Detailbild.
Der Baustein hat die Maße 70mm x 70mm x140mm.
Ich denke es ist ein schönes Andenken für die 6. Convention in Erbes Büdesheim.

Hiermit noch mal von mir und den Ausstellern ein DICKES DANKE an Ralf und sein Team....