---
layout: "image"
title: "Es klappert die Mühle im murmelnden Strom ..."
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn09.jpg"
weight: "20"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Mühlrad", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39560
- /details2394.html
imported:
- "2019"
_4images_image_id: "39560"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39560 -->
Von links kommen die Murmeln eine nach der anderen an und fallen etwas rechts von der Drehachse von oben in eine der Schaufeln. Schon dreht sich das Rad.

---

Marbles arrive from the upper left and fall into the mill wheel. The wheel turns.