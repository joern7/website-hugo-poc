---
layout: "image"
title: "Der Schlepplift - Bergstation"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn15.jpg"
weight: "26"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Schlepplift", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39566
- /details387a.html
imported:
- "2019"
_4images_image_id: "39566"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39566 -->
Sind die Murmeln nach sekundenlanger Fahrt oben angekommen, rollen sie aus dem Schleppschlitten auf die Bahn hinaus; im Bild nach links weg.

---

After several seconds the marbles arrive at the top station, simply rolling out to the tracks; to the left.