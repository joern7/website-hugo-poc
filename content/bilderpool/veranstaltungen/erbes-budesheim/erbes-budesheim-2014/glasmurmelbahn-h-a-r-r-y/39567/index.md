---
layout: "image"
title: "Der Schlepplift - Bergstation"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn16.jpg"
weight: "27"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39567
- /detailsdd41.html
imported:
- "2019"
_4images_image_id: "39567"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39567 -->
Und hier noch ein Blick in den Ausstiegsbereich der Bergstation.

---

This is how the top station looks along the tracks.