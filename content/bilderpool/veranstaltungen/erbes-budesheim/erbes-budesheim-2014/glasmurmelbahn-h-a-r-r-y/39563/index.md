---
layout: "image"
title: "Der Auslauf"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn12.jpg"
weight: "23"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Mühlrad", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39563
- /details879e.html
imported:
- "2019"
_4images_image_id: "39563"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39563 -->
Hier kommen die Murmeln aus der Mühle wieder heraus. Im Gegensatz zur echten Schwarzwaldmühle liegen Eintritt und Austritt auf der gleichen Seite, die "Fluß"richtung wird vom Rad um 180° gedreht.

---

This is where the marbles leave the mill. In contrast to a real blck forrest mill the marbles leave on the same side they enter. The wheel changes the marbles' direction by 180°.