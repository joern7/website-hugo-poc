---
layout: "image"
title: "Die Schaltwarte"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn06.jpg"
weight: "17"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Messwarte", "Meßwarte", "glas", "marble", "rolling", "ball", "marble", "track", "control", "center", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39557
- /details147f.html
imported:
- "2019"
_4images_image_id: "39557"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39557 -->
Dann komm' se mal rein. Hier ist gleich die Schaltwarte. Da gibt es zwei "mächtige" Schalter, und den Techniker. Ein Schalter ist für den Schlepplift und der andere Schalter ist für das Heberad. Das schauen wir uns gleich als nächstes an.

---

This is the switch room. Two huge switches are operated by the technician. One switch controls the drag lift, the other one is dedicated to the lifting wheel. This lifting wheel we visit next.