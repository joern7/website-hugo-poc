---
layout: "image"
title: "Ständer"
date: "2014-10-03T22:04:09"
picture: "modellevonmarkuswolf03.jpg"
weight: "12"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39537
- /details3e23.html
imported:
- "2019"
_4images_image_id: "39537"
_4images_cat_id: "2956"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39537 -->
