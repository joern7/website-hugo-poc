---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad8.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39487
- /detailsfaac.html
imported:
- "2019"
_4images_image_id: "39487"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39487 -->
Hier  ist die Seilspannung gut zu erkennen. Das Problem war, die Länge der Seile zu ermitteln.
Anschließend habe ich 4mm kurze Rohrhülsen gesägt.aufgesenkt und dann verpresst.