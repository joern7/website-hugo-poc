---
layout: "image"
title: "Kabine II"
date: "2014-10-03T22:04:09"
picture: "modellevonmarkuswolf07.jpg"
weight: "16"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39541
- /details3e9f.html
imported:
- "2019"
_4images_image_id: "39541"
_4images_cat_id: "2956"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39541 -->
