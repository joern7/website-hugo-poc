---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39482
- /detailsc6ea.html
imported:
- "2019"
_4images_image_id: "39482"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39482 -->
Hier unser kompletter Stand. Vorne die Kleinmodelle meiner Kinder. Dann die TTballbahn und dahinter das Riesenrad.