---
layout: "image"
title: "Kabine I"
date: "2014-10-03T22:04:09"
picture: "modellevonmarkuswolf06.jpg"
weight: "15"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39540
- /details413e.html
imported:
- "2019"
_4images_image_id: "39540"
_4images_cat_id: "2956"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39540 -->
