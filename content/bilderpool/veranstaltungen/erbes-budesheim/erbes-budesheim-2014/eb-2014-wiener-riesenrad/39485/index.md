---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad6.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39485
- /detailsddfc.html
imported:
- "2019"
_4images_image_id: "39485"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39485 -->
Der Kabineneinstieg.Im Original ist der Bereich "Seilspanner,Einstieg und Antrieb", ummauert bzw. eingehaust.
Ich habe diese Mauer zur besseren Sichtbarkeit weg gelassen.