---
layout: "image"
title: "Von unten beleuchteter Baum"
date: "2014-09-28T13:06:32"
picture: "ebbilderseverin64.jpg"
weight: "49"
konstrukteure: 
- "-?-"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39441
- /detailsc047.html
imported:
- "2019"
_4images_image_id: "39441"
_4images_cat_id: "2951"
_4images_user_id: "558"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39441 -->
Mit Severins ferngesteuertem LED-Strahler