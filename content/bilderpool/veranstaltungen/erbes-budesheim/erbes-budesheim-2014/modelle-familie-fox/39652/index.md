---
layout: "image"
title: "Funktionsmodelle"
date: "2014-10-04T23:18:43"
picture: "modellevonfamiliefox2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39652
- /detailsa5d6-3.html
imported:
- "2019"
_4images_image_id: "39652"
_4images_cat_id: "2969"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39652 -->
[ft:pedia 3/2012]("http://www.ftcommunity.de/categories.php?cat_id=2676"]Dampfmaschine[/url] ([url="http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-4.pdf"]ft:pedia 4/2012[/url]), [url="http://www.ftcommunity.de/categories.php?cat_id=2873"]Selbstbau-Differential[/url] mit Dreigangschaltung, Ewigkeitsmaschine, [url="http://www.ftcommunity.de/categories.php?cat_id=2613"]DCF77-Funkuhr[/url] ([url="http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-3.pdf"))