---
layout: "image"
title: "Pistenwalze"
date: "2014-10-03T16:42:19"
picture: "modellevonharaldsteinhaus4.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39521
- /details4b2b.html
imported:
- "2019"
_4images_image_id: "39521"
_4images_cat_id: "2961"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39521 -->
