---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014   Fischercake"
date: "2014-10-04T12:38:28"
picture: "fischertechniktreffenerbesbudesheim61.jpg"
weight: "61"
konstrukteure: 
- "Archimedes Fischercake"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39629
- /details02d8.html
imported:
- "2019"
_4images_image_id: "39629"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:28"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39629 -->
