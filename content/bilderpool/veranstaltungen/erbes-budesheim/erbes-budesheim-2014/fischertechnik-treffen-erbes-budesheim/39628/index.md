---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014  Fischercake"
date: "2014-10-04T12:38:28"
picture: "fischertechniktreffenerbesbudesheim60.jpg"
weight: "60"
konstrukteure: 
- "Rafael  Fischercake"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39628
- /details79ad-2.html
imported:
- "2019"
_4images_image_id: "39628"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:28"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39628 -->
