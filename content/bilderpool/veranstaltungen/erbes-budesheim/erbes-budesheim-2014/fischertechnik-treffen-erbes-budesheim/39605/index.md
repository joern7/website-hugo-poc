---
layout: "image"
title: "immer interessante + schöne Modelle   (Michael Sengstschmied)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim37.jpg"
weight: "37"
konstrukteure: 
- "Michael Sengstschmied"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39605
- /details34e8.html
imported:
- "2019"
_4images_image_id: "39605"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39605 -->
