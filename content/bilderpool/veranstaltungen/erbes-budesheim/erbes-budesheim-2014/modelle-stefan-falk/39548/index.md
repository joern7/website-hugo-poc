---
layout: "image"
title: "Schwebender Tischtennisball III"
date: "2014-10-03T22:04:09"
picture: "modellestefanfalk3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Gudula Kiehle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39548
- /details323e.html
imported:
- "2019"
_4images_image_id: "39548"
_4images_cat_id: "2964"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39548 -->
