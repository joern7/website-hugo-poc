---
layout: "image"
title: "Schwebender Tischtennisball II"
date: "2014-10-03T22:04:09"
picture: "modellestefanfalk2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Gudula Kiehle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39547
- /detailsdcf6.html
imported:
- "2019"
_4images_image_id: "39547"
_4images_cat_id: "2964"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39547 -->
