---
layout: "image"
title: "Libelle (nach Festo)"
date: "2014-10-03T16:42:19"
picture: "modellevonpeterdamen1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39508
- /details6bc1-2.html
imported:
- "2019"
_4images_image_id: "39508"
_4images_cat_id: "2959"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39508 -->
