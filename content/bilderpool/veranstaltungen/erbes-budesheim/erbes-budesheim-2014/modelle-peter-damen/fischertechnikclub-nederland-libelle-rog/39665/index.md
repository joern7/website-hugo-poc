---
layout: "image"
title: "Libelle + Pijlstaart-rog"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog2.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39665
- /details743e.html
imported:
- "2019"
_4images_image_id: "39665"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39665 -->
Beiden zusammen