---
layout: "image"
title: "Pijlstaart-rog"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog8.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Bert Rook + Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39671
- /details32ce.html
imported:
- "2019"
_4images_image_id: "39671"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39671 -->
Unten.....