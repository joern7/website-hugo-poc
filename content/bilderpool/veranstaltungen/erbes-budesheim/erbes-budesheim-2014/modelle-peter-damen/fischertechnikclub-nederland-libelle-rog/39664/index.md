---
layout: "image"
title: "Fischertechnikclub Nederland in Erbes-Budesheim + Libelle + Pijlstaart-rog"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog1.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Bert Rook + Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39664
- /details46f5-2.html
imported:
- "2019"
_4images_image_id: "39664"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39664 -->
Youtube-link Fischertechnik Libelle : 
https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1