---
layout: "image"
title: "Pijlstaart-rog"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog7.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Bert Rook + Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39670
- /details6fab-2.html
imported:
- "2019"
_4images_image_id: "39670"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39670 -->
Übersicht + Details :

http://www.ftcommunity.de/categories.php?cat_id=2825

http://www.ftcommunity.de/details.php?image_id=37955#col3

Ich habe die Kinematik einer Pijlstaartrog in Fischertechnik nachgebaut. 
Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + 1,5mm Polystyreen-Gold-Spiegel-Platten. 
http://www.kunststofshop.nl/index.php?item=&action=page&group_id=10000046&lang=NL 

http://www.kunststofshop.nl/index.php?action=home&lang=NL 

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.