---
layout: "image"
title: "FT-Convention 2014: Drohne"
date: "2014-09-28T13:06:32"
picture: "margau5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "margau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "margau"
license: "unknown"
legacy_id:
- /php/details/39377
- /details801b.html
imported:
- "2019"
_4images_image_id: "39377"
_4images_cat_id: "2951"
_4images_user_id: "1805"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39377 -->
Flugvorführung