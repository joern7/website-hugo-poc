---
layout: "image"
title: "Ausschnitt"
date: "2014-10-04T23:18:43"
picture: "turmvonmichaelstratmann5.jpg"
weight: "29"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39649
- /details5d7e-2.html
imported:
- "2019"
_4images_image_id: "39649"
_4images_cat_id: "2966"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39649 -->
