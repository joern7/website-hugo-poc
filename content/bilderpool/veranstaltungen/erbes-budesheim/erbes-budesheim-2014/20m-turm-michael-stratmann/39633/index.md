---
layout: "image"
title: "Kontrolle"
date: "2014-10-04T15:59:57"
picture: "turmaufbau4.jpg"
weight: "21"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39633
- /details02ff.html
imported:
- "2019"
_4images_image_id: "39633"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39633 -->
Sind alle S-Riegel im zuletzt angebauten Modul drin? Pro vertikaler Winkelträger-Reihe waren 8 S-Riegel zu setzen, auf dieser Höhe des Turms also etwa 150 Stück.