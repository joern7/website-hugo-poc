---
layout: "image"
title: "Die 'Basis'"
date: "2014-10-04T23:18:43"
picture: "turmvonmichaelstratmann3.jpg"
weight: "27"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39647
- /details7332.html
imported:
- "2019"
_4images_image_id: "39647"
_4images_cat_id: "2966"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39647 -->
