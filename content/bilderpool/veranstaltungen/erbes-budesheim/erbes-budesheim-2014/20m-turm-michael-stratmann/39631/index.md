---
layout: "image"
title: "Aufbau"
date: "2014-10-04T15:59:57"
picture: "turmaufbau2.jpg"
weight: "19"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39631
- /detailsc049-5.html
imported:
- "2019"
_4images_image_id: "39631"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39631 -->
Die Köpfe rauchen, obendrüber stehen schon 8m Spitze.