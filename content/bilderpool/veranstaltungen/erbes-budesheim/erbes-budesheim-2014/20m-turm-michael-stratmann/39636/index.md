---
layout: "image"
title: "Er steht!"
date: "2014-10-04T15:59:57"
picture: "turmaufbau7.jpg"
weight: "24"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39636
- /details3f19.html
imported:
- "2019"
_4images_image_id: "39636"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39636 -->
Und anscheinend hält er auch ;-)