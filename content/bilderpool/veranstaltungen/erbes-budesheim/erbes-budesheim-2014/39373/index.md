---
layout: "image"
title: "FT-Convention 2014: Kran"
date: "2014-09-28T13:06:32"
picture: "margau1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "margau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "margau"
license: "unknown"
legacy_id:
- /php/details/39373
- /details40a3.html
imported:
- "2019"
_4images_image_id: "39373"
_4images_cat_id: "2951"
_4images_user_id: "1805"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39373 -->
Der große Kran.