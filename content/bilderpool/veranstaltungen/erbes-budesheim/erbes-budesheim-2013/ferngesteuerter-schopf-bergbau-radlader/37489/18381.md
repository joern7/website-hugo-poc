---
layout: "comment"
hidden: true
title: "18381"
date: "2013-10-06T20:08:02"
uploadBy:
- "ft-familie"
license: "unknown"
imported:
- "2019"
---
Auf dem unteren BS30 sind zwei Bauplatten mit 3 Nuten (38428).
Auf diesen ist dann, für jede Schalterseite, ein Winkelstein 15° und ein Baustein 7,5 – sozusagen als Rampe mit Verlängerung. Und jeweils abgedeckt mit einer Bauplatte 15x15. Damit passt die Bauhöhe ins 15mm-ft-Raster und die Schalter gleiten weich darüber.
Gerne schreiben wir über dieses Modell noch ein ft-pedia-Artikel.

Gruss  Erik und Jörg