---
layout: "comment"
hidden: true
title: "18413"
date: "2013-10-13T22:40:23"
uploadBy:
- "ft-familie"
license: "unknown"
imported:
- "2019"
---
Hallo Peter, 

die Magnetventile für die Schaufelsteuerung sind direkt an der Fernsteuerung angeschlossen.

Für das Kippen der Schaufel wird ein roter Zylinder mit Feder verwendet, damit die Schaufel immer auskippt. (Alternativ könnte ein blauer Zylinder mit Druck in zwei  Richtungen, d.h. mit 2 Magnetventilen verwendet werden) 

Die 4 Zylinder für das Heben der Schaufel sind bei voller Schaufel notwendig. Der rote Zylinder mit Feder für das Kippen bewirkt aber, dass die Schaufel nicht immer ganz flach, sondern nur vorne an der Kante auf dem Boden aufliegt. Die zwei roten Zylinder für das Heben der Schaufel drücken nun die Schaufel immer flach auf den Boden.

Für richtiges Spielen mit dem Radlader ist das wichtig, damit das Ladegut immer einfach aufgenommen werden kann.   Testfahrer Erik war erst so zufrieden.

Gruß
Erik und Jörg