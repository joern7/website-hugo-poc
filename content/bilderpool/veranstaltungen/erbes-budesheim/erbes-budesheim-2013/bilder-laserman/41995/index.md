---
layout: "image"
title: "Styroporschneider für Fertighaus"
date: "2015-10-01T13:37:10"
picture: "Styroporschneider_fr_Fertighaus.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41995
- /detailse660.html
imported:
- "2019"
_4images_image_id: "41995"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41995 -->
Die Maschine hat einzelne Teile für ein Haus aus Styropor ausgeschnitten.
Die fertigen Teile (6 Stück: 4 Wände und 2 Teile fürs Dach) konnten dann zu einem fertigen Haus zusammengebaut werden.