---
layout: "image"
title: "Bearbeitungsmaschine Antrieb Frässpindel"
date: "2015-10-01T13:37:10"
picture: "Bearbeitungsmaschine_Antrieb_Frsspindel.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41968
- /detailsa5c5-2.html
imported:
- "2019"
_4images_image_id: "41968"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41968 -->
