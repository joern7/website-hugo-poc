---
layout: "image"
title: "Kugelbahn 3"
date: "2015-10-01T13:37:10"
picture: "Kugelbahn_3.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41980
- /details4426.html
imported:
- "2019"
_4images_image_id: "41980"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41980 -->
