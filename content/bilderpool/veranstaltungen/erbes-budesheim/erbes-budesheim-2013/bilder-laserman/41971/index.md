---
layout: "image"
title: "Fischertechnik meets Lego"
date: "2015-10-01T13:37:10"
picture: "Fischertechnik_meets_Lego.jpg"
weight: "6"
konstrukteure: 
- "Tacke"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41971
- /details2c33-2.html
imported:
- "2019"
_4images_image_id: "41971"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41971 -->
