---
layout: "image"
title: "Kugelbahn 4"
date: "2015-10-01T13:37:10"
picture: "Kugelbahn_4.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41981
- /details6664-2.html
imported:
- "2019"
_4images_image_id: "41981"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41981 -->
