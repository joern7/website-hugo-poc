---
layout: "image"
title: "Förderbagger"
date: "2013-09-29T21:54:09"
picture: "convention10.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "lars"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37453
- /detailsb503-2.html
imported:
- "2019"
_4images_image_id: "37453"
_4images_cat_id: "2798"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37453 -->
Bilder von der Convention 2013
Detailaufnahme
Bild 1 von 1
.
Modell:            Förderbagger
-
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.