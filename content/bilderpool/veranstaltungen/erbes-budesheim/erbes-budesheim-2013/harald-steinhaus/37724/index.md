---
layout: "image"
title: "IMG_9975.JPG"
date: "2013-10-19T16:17:26"
picture: "IMG_9975.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37724
- /details1b2f.html
imported:
- "2019"
_4images_image_id: "37724"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:17:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37724 -->
Die Gelenk-Anordnung des 8x8 Allrad so, wie sie sich auf dem Heimweg verschoben und verzogen hat. Das ist noch nichts vernünftiges.