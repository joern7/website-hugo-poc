---
layout: "image"
title: "Münzensortierer"
date: "2013-09-30T23:47:38"
picture: "DSC00259_800x800.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37470
- /details385d.html
imported:
- "2019"
_4images_image_id: "37470"
_4images_cat_id: "2784"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37470 -->
