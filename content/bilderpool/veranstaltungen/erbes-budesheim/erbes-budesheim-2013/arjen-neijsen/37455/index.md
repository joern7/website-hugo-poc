---
layout: "image"
title: "Caterpillar (von Arjen Neijsen)"
date: "2013-09-29T21:54:21"
picture: "convention12.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "lars"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37455
- /details724f.html
imported:
- "2019"
_4images_image_id: "37455"
_4images_cat_id: "2797"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37455 -->
Bilder von der Convention 2013
Gesamtansicht
Bild 1 von 1
.
Modell:            Caterpillar
Konstrukteur:  Arjen Neijsen
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.