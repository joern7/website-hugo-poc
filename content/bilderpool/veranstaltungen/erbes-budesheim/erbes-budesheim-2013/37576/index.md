---
layout: "image"
title: "eb071.jpg"
date: "2013-10-03T09:29:06"
picture: "eb071.jpg"
weight: "58"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel A."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/37576
- /detailsfda9.html
imported:
- "2019"
_4images_image_id: "37576"
_4images_cat_id: "2784"
_4images_user_id: "1"
_4images_image_date: "2013-10-03T09:29:06"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37576 -->
