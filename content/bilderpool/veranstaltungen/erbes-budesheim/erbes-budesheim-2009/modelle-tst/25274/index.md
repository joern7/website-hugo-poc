---
layout: "image"
title: "Give-Away-Maschine"
date: "2009-09-23T20:48:31"
picture: "convention059.jpg"
weight: "18"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25274
- /details0709.html
imported:
- "2019"
_4images_image_id: "25274"
_4images_cat_id: "1746"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25274 -->
