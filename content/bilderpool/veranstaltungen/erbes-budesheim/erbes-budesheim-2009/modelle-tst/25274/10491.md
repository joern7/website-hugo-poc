---
layout: "comment"
hidden: true
title: "10491"
date: "2010-01-08T21:36:23"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Interessante weblink:

http://www.youtube.com/watch?v=p9rFB__oz-E

Ein Overall-Übersicht mehrere interessante TU-Delft-Mechatronica-Projecten mit Fischertechnik gibt es unter:

http://www.youtube.com/results?search_query=mechatronica+tudelft&search_type=&aq=6&oq=Mechatro

Grüss,

Peter Damen
Poederoyen NL