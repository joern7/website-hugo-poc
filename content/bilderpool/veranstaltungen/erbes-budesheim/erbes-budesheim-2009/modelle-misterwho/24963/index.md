---
layout: "image"
title: "Suchroboter"
date: "2009-09-19T22:06:09"
picture: "conv5.jpg"
weight: "8"
konstrukteure: 
- "MisterWho"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24963
- /details11d6.html
imported:
- "2019"
_4images_image_id: "24963"
_4images_cat_id: "1735"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:06:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24963 -->
