---
layout: "image"
title: "Aufräumroboter"
date: "2009-09-23T20:48:31"
picture: "convention086.jpg"
weight: "14"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25301
- /details1f4d.html
imported:
- "2019"
_4images_image_id: "25301"
_4images_cat_id: "1735"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25301 -->
Ich meine, das wäre von den MisterWho-Brüdern