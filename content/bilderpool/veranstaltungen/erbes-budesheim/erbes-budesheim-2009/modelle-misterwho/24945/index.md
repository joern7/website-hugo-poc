---
layout: "image"
title: "Close up Cleaning bot"
date: "2009-09-19T21:55:26"
picture: "DSC_0007.jpg"
weight: "3"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24945
- /details47c0-2.html
imported:
- "2019"
_4images_image_id: "24945"
_4images_cat_id: "1735"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T21:55:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24945 -->
