---
layout: "image"
title: "die ganze Roboter 6 achser"
date: "2009-09-19T23:09:05"
picture: "DSC_0031.jpg"
weight: "2"
konstrukteure: 
- "Manu"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24994
- /details7858.html
imported:
- "2019"
_4images_image_id: "24994"
_4images_cat_id: "1731"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24994 -->
