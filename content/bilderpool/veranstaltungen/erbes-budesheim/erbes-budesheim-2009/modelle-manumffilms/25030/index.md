---
layout: "image"
title: "Manu"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim28.jpg"
weight: "4"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25030
- /detailsb7dc-3.html
imported:
- "2019"
_4images_image_id: "25030"
_4images_cat_id: "1731"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25030 -->
Erbes-Büdesheim-2009