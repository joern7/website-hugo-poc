---
layout: "comment"
hidden: true
title: "10129"
date: "2009-10-25T13:46:44"
uploadBy:
- "Limit"
license: "unknown"
imported:
- "2019"
---
Nein, das System, dass ich angewendet habe nennt sich "Trust". :)
Trotzdem wurde der Wasserstand in der oberen Fahrrinne durch zwei Drähte, die am interface als Schalter eingesteckt waren kontrolliert. Wenn also das Wasser die beiden Drähte erreicht hat, wusste sie Steuerung, dass etwas nicht stimmt und hat die Pumpe über ein Relais abgeschaltet.
Gruß, 
Marius