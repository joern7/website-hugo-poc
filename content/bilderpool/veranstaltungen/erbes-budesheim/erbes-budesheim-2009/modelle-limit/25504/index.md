---
layout: "image"
title: "Limit - Superfish - Wildwasserbahn"
date: "2009-10-08T17:22:54"
picture: "verschiedene07.jpg"
weight: "16"
konstrukteure: 
- "Marius"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25504
- /details74ea.html
imported:
- "2019"
_4images_image_id: "25504"
_4images_cat_id: "1743"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25504 -->
Wirklich gute Idee, eine Wildwasserbahn zu bauen!