---
layout: "image"
title: "Screenshot der Druckersoftware"
date: "2009-09-23T20:48:30"
picture: "convention019.jpg"
weight: "3"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25234
- /details09fb.html
imported:
- "2019"
_4images_image_id: "25234"
_4images_cat_id: "1772"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25234 -->
Da stecken die Anfänge meines geplanten Frameworks drin. Rechts oben der Zeichensatz, rechts unten fertig durchoptimiert, was die Zeichenwege angeht (möglichst jedes Zeichen in einem Zug zu Papier bringen). Links unten arbeitet die tatsächliche (!) Druckersoftware, die gar nichts davon weiß, dass sie nur simulierte Motoren, Taster, Lampen, Fotozellen und E-Magnete hat. Links oben kann man Text eingeben, den der Drucker dann drucken soll - wenn er denn fertig wäre.