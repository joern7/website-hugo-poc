---
layout: "image"
title: "Ein sehr netter Franzose..."
date: "2009-09-23T20:48:31"
picture: "convention032.jpg"
weight: "6"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25247
- /detailscf0b.html
imported:
- "2019"
_4images_image_id: "25247"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25247 -->
aus Paris war sogar da und bot auch einige Kleinteile an.