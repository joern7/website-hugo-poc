---
layout: "image"
title: "ROBOProEntwickler - Raumschiff"
date: "2009-10-08T17:22:55"
picture: "verschiedene32.jpg"
weight: "6"
konstrukteure: 
- "Michael Sögtrop"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25529
- /details5881-2.html
imported:
- "2019"
_4images_image_id: "25529"
_4images_cat_id: "1755"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25529 -->
Auch die Hardware sieht gut aus!