---
layout: "image"
title: "ThanksForTheFish - Taschenbedruckmaschine"
date: "2009-10-08T17:22:55"
picture: "verschiedene20.jpg"
weight: "12"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25517
- /detailsde7d-2.html
imported:
- "2019"
_4images_image_id: "25517"
_4images_cat_id: "1773"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25517 -->
Taschenbedruckmaschine