---
layout: "image"
title: "ThanksForTheFishs Taschenbedruckmaschine"
date: "2009-09-23T20:48:30"
picture: "convention021.jpg"
weight: "6"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25236
- /details507d-2.html
imported:
- "2019"
_4images_image_id: "25236"
_4images_cat_id: "1773"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25236 -->
Leider unscharf, aber ich wollte Euch das Bild doch nicht vorenthalten.