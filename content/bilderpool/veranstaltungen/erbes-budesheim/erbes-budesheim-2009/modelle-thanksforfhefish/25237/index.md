---
layout: "image"
title: "ThanksForTheFishs Taschenbedruckmaschine"
date: "2009-09-23T20:48:30"
picture: "convention022.jpg"
weight: "7"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25237
- /details1c25-2.html
imported:
- "2019"
_4images_image_id: "25237"
_4images_cat_id: "1773"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25237 -->
Aus fertigen Buchstaben konnte man sich seinen Wunschtext zusammenstellen. Dann wurde eine Tragetasche darüber geklappt, die danach einen prima Aufdruck hatten.