---
layout: "image"
title: "ThanksForTheFishs Vierradfahrzeug"
date: "2009-09-23T20:48:30"
picture: "convention025.jpg"
weight: "10"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25240
- /detailsb933.html
imported:
- "2019"
_4images_image_id: "25240"
_4images_cat_id: "1773"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25240 -->
Sehr kräftig, wendig und allradgetrieben.