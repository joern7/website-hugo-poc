---
layout: "image"
title: "Greifroboter"
date: "2009-09-23T20:48:33"
picture: "convention122.jpg"
weight: "24"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25337
- /detailsfe7c.html
imported:
- "2019"
_4images_image_id: "25337"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "122"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25337 -->
