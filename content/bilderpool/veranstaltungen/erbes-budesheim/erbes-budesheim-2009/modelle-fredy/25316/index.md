---
layout: "image"
title: "Fredys mobiler Roboter"
date: "2009-09-23T20:48:31"
picture: "convention101.jpg"
weight: "7"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25316
- /detailsc009.html
imported:
- "2019"
_4images_image_id: "25316"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "101"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25316 -->
