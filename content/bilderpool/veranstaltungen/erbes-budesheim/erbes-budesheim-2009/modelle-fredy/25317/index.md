---
layout: "image"
title: "Fredys Elektromotoren"
date: "2009-09-23T20:48:31"
picture: "convention102.jpg"
weight: "8"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25317
- /detailsb282.html
imported:
- "2019"
_4images_image_id: "25317"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25317 -->
Links ein 50-Hz-Motor (der Magnet ist unten und nicht sichtbar), rechts ein Nachbau des Gleichstrom-Motors aus einem alten ft-Clubheft