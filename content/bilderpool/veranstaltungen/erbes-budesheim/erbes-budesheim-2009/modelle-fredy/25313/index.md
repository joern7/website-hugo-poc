---
layout: "image"
title: "Fahrzeuge"
date: "2009-09-23T20:48:31"
picture: "convention098.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25313
- /detailsebff.html
imported:
- "2019"
_4images_image_id: "25313"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25313 -->
Ganz hinten ein neuer Prototyp von Fredy