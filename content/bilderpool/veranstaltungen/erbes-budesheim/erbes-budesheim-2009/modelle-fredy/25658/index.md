---
layout: "image"
title: "Fredy - Mobiler Roboter"
date: "2009-11-02T21:41:44"
picture: "verschiedene10.jpg"
weight: "13"
konstrukteure: 
- "Frederik"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25658
- /details4dcb.html
imported:
- "2019"
_4images_image_id: "25658"
_4images_cat_id: "1739"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25658 -->
