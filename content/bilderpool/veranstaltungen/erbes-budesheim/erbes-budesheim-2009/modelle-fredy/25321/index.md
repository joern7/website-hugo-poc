---
layout: "image"
title: "Fredys Industriemaschinerie"
date: "2009-09-23T20:48:32"
picture: "convention106.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25321
- /details6b3e.html
imported:
- "2019"
_4images_image_id: "25321"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25321 -->
