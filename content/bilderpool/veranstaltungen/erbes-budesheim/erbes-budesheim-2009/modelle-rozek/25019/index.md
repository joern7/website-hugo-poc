---
layout: "image"
title: "Andreas Rozek"
date: "2009-09-21T20:50:46"
picture: "erbesbuedesheim17.jpg"
weight: "1"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25019
- /detailsc86a-3.html
imported:
- "2019"
_4images_image_id: "25019"
_4images_cat_id: "1751"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:46"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25019 -->
3D-Drucker