---
layout: "image"
title: "DerMitDenBitsTanzt - 6 Achs-Roboter"
date: "2009-11-02T21:41:42"
picture: "verschiedene01.jpg"
weight: "28"
konstrukteure: 
- "Frank Linde"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25649
- /details5258.html
imported:
- "2019"
_4images_image_id: "25649"
_4images_cat_id: "1721"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25649 -->
