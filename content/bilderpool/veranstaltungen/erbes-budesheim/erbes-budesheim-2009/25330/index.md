---
layout: "image"
title: "Carels Robotics Studio-Demo"
date: "2009-09-23T20:48:32"
picture: "convention115.jpg"
weight: "19"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25330
- /details4311.html
imported:
- "2019"
_4images_image_id: "25330"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "115"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25330 -->
Carel van Leeuwen hat sich richtig tief ins Microsoft Robotics Studio eingearbeitet und konnte jedes Detail zeigen und erklären.