---
layout: "image"
title: "thkais - Kugelbahn"
date: "2009-10-08T17:22:54"
picture: "verschiedene15.jpg"
weight: "25"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25512
- /details9af6.html
imported:
- "2019"
_4images_image_id: "25512"
_4images_cat_id: "1741"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25512 -->
Wieviel Uhr ist es?