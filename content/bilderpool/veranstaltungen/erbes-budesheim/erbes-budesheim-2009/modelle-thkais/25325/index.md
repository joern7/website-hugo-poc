---
layout: "image"
title: "Thkais Fernsteuerungs-Laster"
date: "2009-09-23T20:48:32"
picture: "convention110.jpg"
weight: "21"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25325
- /details1317-2.html
imported:
- "2019"
_4images_image_id: "25325"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "110"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25325 -->
