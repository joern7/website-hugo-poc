---
layout: "image"
title: "Kugeluhr"
date: "2009-09-19T22:18:54"
picture: "conv2.jpg"
weight: "2"
konstrukteure: 
- "thkais"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24973
- /details3a25.html
imported:
- "2019"
_4images_image_id: "24973"
_4images_cat_id: "1741"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:18:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24973 -->
