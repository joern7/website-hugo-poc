---
layout: "image"
title: "schnaggels RoboMax"
date: "2009-09-23T20:48:31"
picture: "convention090.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25305
- /details6d0c.html
imported:
- "2019"
_4images_image_id: "25305"
_4images_cat_id: "1737"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25305 -->
