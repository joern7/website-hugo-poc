---
layout: "image"
title: "Work in progress!"
date: "2009-09-19T22:07:23"
picture: "DSC_0023.jpg"
weight: "1"
konstrukteure: 
- "Thomas"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24964
- /detailsade2.html
imported:
- "2019"
_4images_image_id: "24964"
_4images_cat_id: "1737"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:07:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24964 -->
