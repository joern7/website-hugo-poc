---
layout: "image"
title: "3D-Drucker"
date: "2009-09-23T20:48:33"
picture: "convention130.jpg"
weight: "4"
konstrukteure: 
- "niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25345
- /details8bb6.html
imported:
- "2019"
_4images_image_id: "25345"
_4images_cat_id: "1789"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25345 -->
