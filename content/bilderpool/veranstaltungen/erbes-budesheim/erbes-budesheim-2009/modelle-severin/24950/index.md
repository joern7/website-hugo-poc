---
layout: "image"
title: "Säulenroboter"
date: "2009-09-19T21:59:15"
picture: "conv4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24950
- /details2735.html
imported:
- "2019"
_4images_image_id: "24950"
_4images_cat_id: "1723"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:59:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24950 -->
