---
layout: "image"
title: "C-Knobloch - Mobiler Roboter"
date: "2009-10-08T17:22:54"
picture: "verschiedene02.jpg"
weight: "4"
konstrukteure: 
- "Cristian Knobloch"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25499
- /details7b0d.html
imported:
- "2019"
_4images_image_id: "25499"
_4images_cat_id: "1786"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25499 -->
Mit diversen Funktionen