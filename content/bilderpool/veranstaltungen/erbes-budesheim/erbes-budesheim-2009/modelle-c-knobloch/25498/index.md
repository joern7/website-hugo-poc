---
layout: "image"
title: "C-Knobloch - Zimmer-Zugangssystem"
date: "2009-10-08T17:22:53"
picture: "verschiedene01.jpg"
weight: "3"
konstrukteure: 
- "Cristian Knobloch"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25498
- /detailsa54e.html
imported:
- "2019"
_4images_image_id: "25498"
_4images_cat_id: "1786"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25498 -->
Auf einem aufgestellten Monitor im Eingangsbereich stellte man plötzlich fest, dass man selbst im Bild war. Da die Kamera gut getarnt ist, musste man schon suchen, wo sie war.                                               Originaltext: Zugangssystem für eigenes Zimmer (wichtig bei kleinen Geschwistern und nervigen Eltern) mit Videoüberwachung und Aufzeichung - Eltern haben keine Chance...,