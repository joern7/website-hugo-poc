---
layout: "image"
title: "More Looping details!"
date: "2009-09-19T22:23:07"
picture: "DSC_0026.jpg"
weight: "4"
konstrukteure: 
- "Speedy68"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24982
- /details1a8e.html
imported:
- "2019"
_4images_image_id: "24982"
_4images_cat_id: "1724"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24982 -->
