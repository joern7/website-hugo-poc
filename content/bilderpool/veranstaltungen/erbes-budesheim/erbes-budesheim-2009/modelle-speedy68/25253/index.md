---
layout: "image"
title: "speedy68s Achterbahn"
date: "2009-09-23T20:48:31"
picture: "convention038.jpg"
weight: "17"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25253
- /details0bf7-2.html
imported:
- "2019"
_4images_image_id: "25253"
_4images_cat_id: "1724"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25253 -->
