---
layout: "image"
title: "Greifroboter"
date: "2009-11-02T21:41:42"
picture: "verschiedene02.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25650
- /detailsecf9.html
imported:
- "2019"
_4images_image_id: "25650"
_4images_cat_id: "1721"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25650 -->
Bitte Konstrukteur nachtragen, stand auf Tisch 4b (bei Frank Linde)