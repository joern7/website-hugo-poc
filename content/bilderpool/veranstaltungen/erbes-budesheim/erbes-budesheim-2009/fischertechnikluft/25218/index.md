---
layout: "image"
title: "fischertechnik Bastelecke"
date: "2009-09-23T20:48:29"
picture: "convention003.jpg"
weight: "47"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25218
- /detailse975.html
imported:
- "2019"
_4images_image_id: "25218"
_4images_cat_id: "1775"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25218 -->
Hier konnten vor allem Kinder fischertechnik kennenlernen.