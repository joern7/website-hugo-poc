---
layout: "image"
title: "Almost empty hall"
date: "2009-09-22T21:44:14"
picture: "ftconvention06.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/25073
- /details4c42-2.html
imported:
- "2019"
_4images_image_id: "25073"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25073 -->
around 9 o'clock.