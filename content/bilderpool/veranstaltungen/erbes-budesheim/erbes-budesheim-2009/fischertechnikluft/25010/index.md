---
layout: "image"
title: "FT-Ferienwohnung Alte Backstube"
date: "2009-09-21T20:50:45"
picture: "erbesbuedesheim08.jpg"
weight: "5"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25010
- /detailsaf15.html
imported:
- "2019"
_4images_image_id: "25010"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25010 -->
Erbes-Büdesheim-2009