---
layout: "image"
title: "Almost empty hall"
date: "2009-09-22T21:44:14"
picture: "ftconvention03.jpg"
weight: "24"
konstrukteure: 
- "thkais"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/25070
- /details250f-2.html
imported:
- "2019"
_4images_image_id: "25070"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25070 -->
around 9 o'clock.