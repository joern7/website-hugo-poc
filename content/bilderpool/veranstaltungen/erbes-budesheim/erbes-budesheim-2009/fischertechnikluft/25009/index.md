---
layout: "image"
title: "FT-Ferienwohnung Alte Backstube"
date: "2009-09-21T20:50:45"
picture: "erbesbuedesheim07.jpg"
weight: "4"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25009
- /details6f6c.html
imported:
- "2019"
_4images_image_id: "25009"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25009 -->
Erbes-Büdesheim-2009