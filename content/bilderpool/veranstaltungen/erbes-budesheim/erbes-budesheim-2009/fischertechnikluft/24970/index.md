---
layout: "image"
title: "Übersicht"
date: "2009-09-19T22:09:54"
picture: "DSC_0012.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24970
- /detailsb5c6.html
imported:
- "2019"
_4images_image_id: "24970"
_4images_cat_id: "1775"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:09:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24970 -->
:-) neue NIKKOR 10.0-24.0 mm f/3.5-4.5 lens