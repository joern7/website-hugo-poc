---
layout: "image"
title: "close up"
date: "2009-09-19T22:01:56"
picture: "DSC_0009.jpg"
weight: "2"
konstrukteure: 
- "DMDBT"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24955
- /details64cf.html
imported:
- "2019"
_4images_image_id: "24955"
_4images_cat_id: "1748"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:01:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24955 -->
