---
layout: "image"
title: "mirose - Münzsortierer /-zähler"
date: "2009-10-08T17:22:54"
picture: "verschiedene10.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25507
- /details0c92.html
imported:
- "2019"
_4images_image_id: "25507"
_4images_cat_id: "1738"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25507 -->
Mit schwarz - rot wurde die idealere Farbmischung gewählt!