---
layout: "image"
title: "Michael Sengstschmid"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim45.jpg"
weight: "2"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25047
- /details717f.html
imported:
- "2019"
_4images_image_id: "25047"
_4images_cat_id: "1738"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25047 -->
Münzsortierer