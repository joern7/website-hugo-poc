---
layout: "image"
title: "charlie2700 - Container-Terminal"
date: "2009-10-08T17:22:55"
picture: "verschiedene19.jpg"
weight: "12"
konstrukteure: 
- "Karl Tillmetz"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25516
- /details9e22.html
imported:
- "2019"
_4images_image_id: "25516"
_4images_cat_id: "1734"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25516 -->
Container-Terminal