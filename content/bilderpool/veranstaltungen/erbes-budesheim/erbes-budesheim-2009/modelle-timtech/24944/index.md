---
layout: "image"
title: "Sortiermaschine"
date: "2009-09-19T21:55:26"
picture: "conv2.jpg"
weight: "2"
konstrukteure: 
- "timtech"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24944
- /details0218.html
imported:
- "2019"
_4images_image_id: "24944"
_4images_cat_id: "1729"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:55:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24944 -->
