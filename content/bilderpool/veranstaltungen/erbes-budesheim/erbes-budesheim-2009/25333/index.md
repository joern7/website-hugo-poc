---
layout: "image"
title: "Pneumatische Motoren und Förderbändert"
date: "2009-09-23T20:48:33"
picture: "convention118.jpg"
weight: "22"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25333
- /detailsa487.html
imported:
- "2019"
_4images_image_id: "25333"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "118"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25333 -->
Die Förderbänder rechts sind unten mit Platten 15x60 und BS7,5 so gelagert, dass sie überhaupt nicht wackeln oder ruckeln.