---
layout: "image"
title: "kehrblech"
date: "2009-11-02T21:41:44"
picture: "verschiedene15.jpg"
weight: "10"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25663
- /detailsb13a.html
imported:
- "2019"
_4images_image_id: "25663"
_4images_cat_id: "1733"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25663 -->
