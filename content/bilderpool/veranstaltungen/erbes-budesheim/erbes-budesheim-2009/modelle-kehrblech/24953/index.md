---
layout: "image"
title: "Kugelbalancierroboter"
date: "2009-09-19T22:01:55"
picture: "conv1.jpg"
weight: "1"
konstrukteure: 
- "kehrblech"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24953
- /details2e5d.html
imported:
- "2019"
_4images_image_id: "24953"
_4images_cat_id: "1733"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:01:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24953 -->
