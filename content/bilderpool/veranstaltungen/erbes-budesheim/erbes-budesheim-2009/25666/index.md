---
layout: "image"
title: "schnaggels - RoboMax"
date: "2009-11-02T21:41:44"
picture: "verschiedene18.jpg"
weight: "34"
konstrukteure: 
- "Thomas Brestrich"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25666
- /detailscd8a.html
imported:
- "2019"
_4images_image_id: "25666"
_4images_cat_id: "1721"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25666 -->
