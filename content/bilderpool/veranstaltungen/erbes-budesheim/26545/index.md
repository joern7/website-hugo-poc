---
layout: "image"
title: "Robot Magazine Article"
date: "2010-02-26T21:03:43"
picture: "ft_con2.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Erbes-Budesheim", "convention", "Robot", "magazine"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26545
- /detailseb18.html
imported:
- "2019"
_4images_image_id: "26545"
_4images_cat_id: "1720"
_4images_user_id: "585"
_4images_image_date: "2010-02-26T21:03:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26545 -->
A snapshot of the article covering the convention in the May/June 2010 issue in Robot Magazine!