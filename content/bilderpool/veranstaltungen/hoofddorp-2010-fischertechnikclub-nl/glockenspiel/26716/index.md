---
layout: "image"
title: "Überblick mit Marcel and model..."
date: "2010-03-15T19:09:50"
picture: "tingel3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26716
- /details5265.html
imported:
- "2019"
_4images_image_id: "26716"
_4images_cat_id: "1904"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26716 -->
