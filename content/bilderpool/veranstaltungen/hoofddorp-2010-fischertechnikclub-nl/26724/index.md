---
layout: "image"
title: "Überblick über die Halle"
date: "2010-03-15T19:09:51"
picture: "Hoofddorp.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik Club NL"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26724
- /details9ccb.html
imported:
- "2019"
_4images_image_id: "26724"
_4images_cat_id: "1902"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26724 -->
