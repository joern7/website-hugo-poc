---
layout: "image"
title: "Logic analyser trace of the RS485 communication between a Master and 5 Slaves"
date: "2010-03-21T18:38:00"
picture: "ad1.jpg"
weight: "8"
konstrukteure: 
- "AD"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26786
- /details58ca.html
imported:
- "2019"
_4images_image_id: "26786"
_4images_cat_id: "1903"
_4images_user_id: "716"
_4images_image_date: "2010-03-21T18:38:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26786 -->
In the period between the A and B bars, the master interrogates all 5 slaves. As you can see in the bar above the ruler, this takes approx. 14ms or about 2.8ms/slave