---
layout: "image"
title: "TX test software recognizes the 6 pieces...."
date: "2010-03-15T19:09:50"
picture: "TX5.jpg"
weight: "5"
konstrukteure: 
- "Ad, Richard and Paul"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26711
- /detailsc2bd.html
imported:
- "2019"
_4images_image_id: "26711"
_4images_cat_id: "1903"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26711 -->
