---
layout: "image"
title: ":-)"
date: "2010-03-15T19:09:51"
picture: "keermis3.jpg"
weight: "2"
konstrukteure: 
- "Jan Willem Dekker und Clemens Jansen"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26726
- /details1406.html
imported:
- "2019"
_4images_image_id: "26726"
_4images_cat_id: "1907"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26726 -->
