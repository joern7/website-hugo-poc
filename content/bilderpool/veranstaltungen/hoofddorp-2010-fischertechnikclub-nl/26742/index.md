---
layout: "image"
title: "Star Heavy Lifter 1:40"
date: "2010-03-15T19:09:51"
picture: "DSC_1182.jpg"
weight: "3"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26742
- /detailsdf07.html
imported:
- "2019"
_4images_image_id: "26742"
_4images_cat_id: "1902"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26742 -->
