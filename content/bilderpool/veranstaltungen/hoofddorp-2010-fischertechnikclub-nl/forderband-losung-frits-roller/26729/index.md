---
layout: "image"
title: "Förderband mit Rutschkupplung"
date: "2010-03-15T19:09:51"
picture: "folderband.jpg"
weight: "1"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26729
- /details6589.html
imported:
- "2019"
_4images_image_id: "26729"
_4images_cat_id: "1908"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26729 -->
