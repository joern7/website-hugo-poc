---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen5.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38144
- /details75e4.html
imported:
- "2019"
_4images_image_id: "38144"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38144 -->
Kleinstmodelle von unserer Tochter Johanna,und mir :) .