---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage07.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/40444
- /detailse1be.html
imported:
- "2019"
_4images_image_id: "40444"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40444 -->
Hier der Antrieb der "Achterbahn"