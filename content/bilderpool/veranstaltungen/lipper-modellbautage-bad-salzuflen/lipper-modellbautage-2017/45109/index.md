---
layout: "image"
title: "Lipper Modellbautage 2017"
date: "2017-01-30T17:08:11"
picture: "lippermodellbautage5.jpg"
weight: "5"
konstrukteure: 
- "Fam. Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/45109
- /detailsc3f6.html
imported:
- "2019"
_4images_image_id: "45109"
_4images_cat_id: "3359"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T17:08:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45109 -->
Reges Interesse den ganzen Tag über.