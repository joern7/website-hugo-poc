---
layout: "image"
title: "Lipper Modellbautage 2017"
date: "2017-01-30T17:08:11"
picture: "lippermodellbautage4.jpg"
weight: "4"
konstrukteure: 
- "Fam. Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/45108
- /details597a.html
imported:
- "2019"
_4images_image_id: "45108"
_4images_cat_id: "3359"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T17:08:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45108 -->
Unser Ft Stand kurz vor dem Opening.