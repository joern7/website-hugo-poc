---
layout: "image"
title: "Lipper Modellbautage 2017"
date: "2017-01-30T17:08:11"
picture: "lippermodellbautage1.jpg"
weight: "1"
konstrukteure: 
- "Fam. Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/45105
- /detailsff76.html
imported:
- "2019"
_4images_image_id: "45105"
_4images_cat_id: "3359"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T17:08:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45105 -->
Familie Wolf zum dritten mal bei den Lipper Modellbautagen in Bad Salzuflen.