---
layout: "image"
title: "Lipper Modedllbautage 2018"
date: "2018-01-25T16:43:57"
picture: "lippermodellbautage5.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/47194
- /detailsf834.html
imported:
- "2019"
_4images_image_id: "47194"
_4images_cat_id: "3491"
_4images_user_id: "968"
_4images_image_date: "2018-01-25T16:43:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47194 -->
