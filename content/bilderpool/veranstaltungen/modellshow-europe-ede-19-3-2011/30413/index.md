---
layout: "image"
title: "EDE 24"
date: "2011-04-02T23:50:38"
picture: "ede24.jpg"
weight: "24"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30413
- /details620e.html
imported:
- "2019"
_4images_image_id: "30413"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30413 -->
Details der Manitowoc: Ballastträger. Die beide stutsen sind ausfahrbar