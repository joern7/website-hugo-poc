---
layout: "image"
title: "Beim Start"
date: "2012-03-18T20:23:50"
picture: "renner2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/34658
- /detailse006-2.html
imported:
- "2019"
_4images_image_id: "34658"
_4images_cat_id: "2557"
_4images_user_id: "182"
_4images_image_date: "2012-03-18T20:23:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34658 -->
Hier zu sehen neben einem Original Boliden.
Leider bin ich wegen des hohen Gewichtes nur 2. geworden :((