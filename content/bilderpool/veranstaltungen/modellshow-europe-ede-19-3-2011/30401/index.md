---
layout: "image"
title: "EDE 12"
date: "2011-04-02T23:50:38"
picture: "ede12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30401
- /details9b12-2.html
imported:
- "2019"
_4images_image_id: "30401"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30401 -->
Brückenlegerpantser Bieber. Der version von Dirk Kutsch.