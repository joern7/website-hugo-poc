---
layout: "image"
title: "EDE 03"
date: "2011-04-02T23:50:37"
picture: "ede03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30392
- /details115f-2.html
imported:
- "2019"
_4images_image_id: "30392"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30392 -->
Demag CC4800 II maßstab 1:16