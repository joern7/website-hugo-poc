---
layout: "comment"
hidden: true
title: "14027"
date: "2011-04-04T18:52:58"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

kannst bitte du noch weitere Bilder zeigen?

Wie willst du den Derrickausleger mit Ballast steuern, wenn kein Gewicht am Haken ist? Baust du unsichtbare kleine Reifen in den Ballastaufleger hinein?

Gruß Jürgen