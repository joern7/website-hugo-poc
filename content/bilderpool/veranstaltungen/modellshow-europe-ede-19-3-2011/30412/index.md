---
layout: "image"
title: "EDE 23"
date: "2011-04-02T23:50:38"
picture: "ede23.jpg"
weight: "23"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30412
- /detailse779.html
imported:
- "2019"
_4images_image_id: "30412"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30412 -->
Details der Manitowoc: der drehantrieb mittels 4 Power-motoren