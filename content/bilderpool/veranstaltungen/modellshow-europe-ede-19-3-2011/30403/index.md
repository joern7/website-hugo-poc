---
layout: "image"
title: "EDE 14"
date: "2011-04-02T23:50:38"
picture: "ede14.jpg"
weight: "14"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30403
- /detailsdfb5.html
imported:
- "2019"
_4images_image_id: "30403"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30403 -->
Der Haken von der Manitowoc