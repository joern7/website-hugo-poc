---
layout: "image"
title: "EDE 27"
date: "2011-04-02T23:50:38"
picture: "ede27.jpg"
weight: "27"
konstrukteure: 
- "Dennis Bosman"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30416
- /detailsda46.html
imported:
- "2019"
_4images_image_id: "30416"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30416 -->
Lego: Scania mit tiefbet Auflieger mit jeep-dolly und einen Menck Löffelbagger