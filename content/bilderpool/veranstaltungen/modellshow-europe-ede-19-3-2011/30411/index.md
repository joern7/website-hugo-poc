---
layout: "image"
title: "EDE 22"
date: "2011-04-02T23:50:38"
picture: "ede22.jpg"
weight: "22"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30411
- /details2cb5.html
imported:
- "2019"
_4images_image_id: "30411"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30411 -->
Details der Manitowoc: Obwol der Kran 4 Raupenschiffen hat, functioniert das als ob er nur 2 Raupenschiffen hat.