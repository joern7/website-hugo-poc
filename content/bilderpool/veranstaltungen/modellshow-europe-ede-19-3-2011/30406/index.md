---
layout: "image"
title: "EDE 17"
date: "2011-04-02T23:50:38"
picture: "ede17.jpg"
weight: "17"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30406
- /detailsd167-2.html
imported:
- "2019"
_4images_image_id: "30406"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30406 -->
Details der Manitowoc: 2 Seilwinden neben einander. Angetrieben von je einen XM motor