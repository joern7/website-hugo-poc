---
layout: "image"
title: "EDE 21"
date: "2011-04-02T23:50:38"
picture: "ede21.jpg"
weight: "21"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30410
- /detailsf78d-2.html
imported:
- "2019"
_4images_image_id: "30410"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30410 -->
Details der Manitowoc: Der Kran dreht sich auf ein Ring. An der ineseiten diesen Ring sind auch noch die Zahnstangen m1,5 angebaut