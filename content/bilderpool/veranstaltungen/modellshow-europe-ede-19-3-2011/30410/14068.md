---
layout: "comment"
hidden: true
title: "14068"
date: "2011-04-13T15:52:26"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Würde mich auch interessieren :)

Da sich die Zahnstangen ja nicht biegen lassen, muß wohl die Gegenseite federnd gelagert sein. Bei dem Gewicht muß ja ein ordentlicher Anpressdruck zwischen Zahnstange und Zahnrad sein, sonst springt das ja ständig.

Oder erfolgt der Antrieb vom Oberteil nicht über die Zahnstangen?