---
layout: "image"
title: "DSC00848"
date: "2006-06-01T21:54:51"
picture: "orgel1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Benny Hamers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6358
- /details8847.html
imported:
- "2019"
_4images_image_id: "6358"
_4images_cat_id: "555"
_4images_user_id: "5"
_4images_image_date: "2006-06-01T21:54:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6358 -->
Clemens Jansen, Arthur Fischer en Marcel Bosch met de Orgelspelende robot