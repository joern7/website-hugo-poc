---
layout: "image"
title: "Fam. Dijkstra - Fokker 50 vliegtuigmodel"
date: "2009-02-28T19:51:04"
picture: "2009-Febr-FT-Apeldoorn_012.jpg"
weight: "12"
konstrukteure: 
- "Fam. Dijkstra - Fokker 50 vliegtuigmodel"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23254
- /details099f.html
imported:
- "2019"
_4images_image_id: "23254"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23254 -->
