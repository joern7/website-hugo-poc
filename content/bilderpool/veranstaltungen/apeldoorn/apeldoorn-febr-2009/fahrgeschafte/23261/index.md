---
layout: "image"
title: "Jan Willem Dekker"
date: "2009-02-28T19:51:04"
picture: "2009-Febr-FT-Apeldoorn_020.jpg"
weight: "4"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23261
- /detailsbac4.html
imported:
- "2019"
_4images_image_id: "23261"
_4images_cat_id: "2012"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23261 -->
