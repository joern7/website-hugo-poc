---
layout: "image"
title: "Heftruck"
date: "2009-02-28T21:47:57"
picture: "2009-Febr-FT-Apeldoorn_048.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23279
- /detailsfd7b.html
imported:
- "2019"
_4images_image_id: "23279"
_4images_cat_id: "2013"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T21:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23279 -->
