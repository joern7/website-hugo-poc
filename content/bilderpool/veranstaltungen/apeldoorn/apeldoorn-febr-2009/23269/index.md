---
layout: "image"
title: "Marcel Bosch"
date: "2009-02-28T19:51:04"
picture: "2009-Febr-FT-Apeldoorn_032.jpg"
weight: "20"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23269
- /details00b5.html
imported:
- "2019"
_4images_image_id: "23269"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23269 -->
