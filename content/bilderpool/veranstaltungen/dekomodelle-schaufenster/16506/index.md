---
layout: "image"
title: "Mein Robbi im Schaufenster"
date: "2008-11-25T20:40:51"
picture: "2008_1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16506
- /detailsa711.html
imported:
- "2019"
_4images_image_id: "16506"
_4images_cat_id: "1491"
_4images_user_id: "182"
_4images_image_date: "2008-11-25T20:40:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16506 -->
Auch dieses Jahr habe ich wieder bei der Firma Peppinghaus in Münster-Wolbeck ein Modell im Schaufenster stehen. Mein Robbi läßt sich über einen Sensor an der Scheibe starten und bewegt dann Arme, Hände und den Kopf.