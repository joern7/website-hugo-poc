---
layout: "image"
title: "Gear Train"
date: "2009-06-27T19:56:56"
picture: "ft_geartrain.jpg"
weight: "1"
konstrukteure: 
- "Student"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Arkansas", "gear", "train"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24458
- /detailsb040.html
imported:
- "2019"
_4images_image_id: "24458"
_4images_cat_id: "1678"
_4images_user_id: "585"
_4images_image_date: "2009-06-27T19:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24458 -->
This student experimented with constructing and hooking together four 10-30 toothed gear pairs with a fifth gear train built by an instructor.