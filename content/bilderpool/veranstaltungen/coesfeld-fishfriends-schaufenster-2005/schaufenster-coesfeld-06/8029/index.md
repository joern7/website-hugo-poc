---
layout: "image"
title: "Schaufenster 25"
date: "2006-12-20T21:50:26"
picture: "157_5786.jpg"
weight: "23"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8029
- /detailsc4fd.html
imported:
- "2019"
_4images_image_id: "8029"
_4images_cat_id: "747"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8029 -->
