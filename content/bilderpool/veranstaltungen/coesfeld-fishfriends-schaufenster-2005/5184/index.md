---
layout: "image"
title: "Schaufenster 5"
date: "2005-11-05T15:49:19"
picture: "142_4281.jpg"
weight: "5"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Schaufenster", "Riesenrad"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/5184
- /details0717.html
imported:
- "2019"
_4images_image_id: "5184"
_4images_cat_id: "434"
_4images_user_id: "34"
_4images_image_date: "2005-11-05T15:49:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5184 -->
Schaufenster von Außen