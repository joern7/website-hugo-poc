---
layout: "image"
title: "Die jungen Fischertechniker aus Österreich..."
date: "2016-05-15T10:46:08"
picture: "diejungenfischertechnikerausoesterreich09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43354
- /details2454.html
imported:
- "2019"
_4images_image_id: "43354"
_4images_cat_id: "3220"
_4images_user_id: "1688"
_4images_image_date: "2016-05-15T10:46:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43354 -->
