---
layout: "image"
title: "Artur Fischer......."
date: "2009-05-10T12:05:08"
picture: "2009-RidderkerkArtur-Fischer_102.jpg"
weight: "44"
konstrukteure: 
- "Artur Fischer"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23992
- /details1367.html
imported:
- "2019"
_4images_image_id: "23992"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T12:05:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23992 -->
