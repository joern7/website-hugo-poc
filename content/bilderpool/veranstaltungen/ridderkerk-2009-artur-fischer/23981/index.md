---
layout: "image"
title: "Alles fertig um Besuch Artur Fischer"
date: "2009-05-10T12:04:49"
picture: "2009-RidderkerkArtur-Fischer_086.jpg"
weight: "33"
konstrukteure: 
- "FT-Expert Wim Starreveld"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23981
- /details32c6.html
imported:
- "2019"
_4images_image_id: "23981"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T12:04:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23981 -->
