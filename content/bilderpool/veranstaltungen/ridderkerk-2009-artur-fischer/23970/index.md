---
layout: "image"
title: "3D-Printer Paul van Niekerk"
date: "2009-05-10T11:37:28"
picture: "2009-RidderkerkArtur-Fischer_063.jpg"
weight: "22"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23970
- /detailse01b.html
imported:
- "2019"
_4images_image_id: "23970"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23970 -->
