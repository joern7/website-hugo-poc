---
layout: "image"
title: "Musik-Generator und andere Modellen"
date: "2009-05-10T11:33:49"
picture: "2009-RidderkerkArtur-Fischer_027.jpg"
weight: "4"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Peter Damen (PoederoyenNL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23952
- /detailsa9c3.html
imported:
- "2019"
_4images_image_id: "23952"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23952 -->
