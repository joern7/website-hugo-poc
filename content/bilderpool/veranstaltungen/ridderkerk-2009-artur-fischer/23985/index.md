---
layout: "image"
title: "Container-Terminal"
date: "2009-05-10T12:04:49"
picture: "2009-RidderkerkArtur-Fischer_092.jpg"
weight: "37"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23985
- /details351c.html
imported:
- "2019"
_4images_image_id: "23985"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T12:04:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23985 -->
