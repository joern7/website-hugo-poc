---
layout: "image"
title: "Schloß Sigmaringen (I)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt01.jpg"
weight: "1"
konstrukteure: 
- "Verschiedene Architekten"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43679
- /details2e5b.html
imported:
- "2019"
_4images_image_id: "43679"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43679 -->
Für die Historie und alle weiteren Details:
https://de.wikipedia.org/wiki/Schloss_Sigmaringen