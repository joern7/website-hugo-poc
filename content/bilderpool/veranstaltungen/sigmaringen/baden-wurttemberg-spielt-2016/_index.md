---
layout: "overview"
title: "Baden-Württemberg spielt 2016"
date: 2020-02-22T09:09:46+01:00
legacy_id:
- /php/categories/3235
- /categories76f4-2.html
- /categories1c37.html
- /categories032f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3235 --> 
Am 5. Juni 2016 fand in Sigmaringen die Veranstaltung "Baden-Württemberg spielt" statt. An vielen Spielstationen hatten Kinder jeden Alters ;) viel Spaß am Mitmachen, Ausprobieren und Experimentieren.

Pro Spielstation waren eine Betreuerin oder auch ein Betreuer im Einsatz. Große wie kleine Besucher konnten so gleich mit ein bißchen Hilfe neue Spielsachen kennenlernen oder sich einfach mit den gewohnten Spielen austoben.

Eine prima Gelegenheit also fischertechnik dem Publikum wieder etwas näher zu bringen. Auf Einladung von fischertechnik (https://forum.ftcommunity.de/viewtopic.php?f=21&t=3398) hat H.A.R.R.Y. beim Event mitgemacht, das Ganze wurde organisiert von 101Entertainment (http://www.bwspielt.de/wp/).

Die Veranstaltung war sowohl in der Innenstadt als auch in der Stadthalle und im Rathaus untergebracht. Für Euch gibt es einige wenige Impressionen vom Vorabend und unmittelbar vor Veranstaltungsbeginn. Die Veranstaltung selbst war deutlich größer und trotz Regenwetter gut besucht.