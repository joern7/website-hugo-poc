---
layout: "image"
title: "Location (Schlechtwetter)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt08.jpg"
weight: "8"
konstrukteure: 
- "Verschiedene Architekten"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43686
- /details14b5.html
imported:
- "2019"
_4images_image_id: "43686"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43686 -->
Nun war für den Sonntag Regen angekündigt und so ist ein Teil der Veranstaltung ins Rathaus und die Stadthalle verlegt worden. Man sieht den Wolken schon an, daß das eine weise Entscheidung war.