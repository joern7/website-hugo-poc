---
layout: "image"
title: "Spielstation 'fischertechnik Oeco Tech'"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt13.jpg"
weight: "13"
konstrukteure: 
- "101Entertainment"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43691
- /details5326.html
imported:
- "2019"
_4images_image_id: "43691"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43691 -->
Oeco Tech als Schüttgut. Hier war etwas erfahreneres Personal gefragt, denn manche Solarzelle hatte erst unter der Kunstsonne (rechts ausserhalb des Bildes) genug Energie für das Modell. Auch die Sache mit dem Stromkreis war nicht jedem vertraut. Aber ein oder zwei Erklärungen und etwas Know-How verhalfen fast jedem Mitspieler zu einem Erfolgserlebnis.

(Entschuldigt die Bildqualität, die Lichtverhältnisse waren etwas schwierig am Sonntag früh)