---
layout: "image"
title: "Brunnen"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt07.jpg"
weight: "7"
konstrukteure: 
- "Jemand der das gut kann"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43685
- /details8bd8.html
imported:
- "2019"
_4images_image_id: "43685"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43685 -->
Brunnen gibt es reichlich in Sigmaringens Fußgängerzone. Die Inschrift hat der Fotograf allerdings vergessen ...