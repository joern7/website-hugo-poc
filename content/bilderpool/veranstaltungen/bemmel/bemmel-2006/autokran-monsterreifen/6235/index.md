---
layout: "image"
title: "Kran 4"
date: "2006-05-07T15:16:33"
picture: "IMG_2522_1.jpg"
weight: "6"
konstrukteure: 
- "Anton Jansen - NL"
fotografen:
- "Rob  van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/6235
- /details6c23.html
imported:
- "2019"
_4images_image_id: "6235"
_4images_cat_id: "581"
_4images_user_id: "379"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6235 -->
