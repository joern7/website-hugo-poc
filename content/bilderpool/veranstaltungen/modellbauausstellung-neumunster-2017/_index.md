---
layout: "overview"
title: "Modellbauausstellung Neumünster 2017"
date: 2020-02-22T09:10:09+01:00
legacy_id:
- /php/categories/3381
- /categoriesa1a3.html
- /categories71c5.html
- /categorieseb90.html
- /categories7fb0.html
- /categoriesba09.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3381 --> 
Unser Stand war diesmal 16 meter lang und 5 meter breit.
Dadurch hatten wir mehr Platz für unsere Modelle und die Besucher haben sich besser verteilt.

Die Stimmung im Team war super. Jeder hatte seine Modelle mitgebracht.

Bei uns war Mitmachen angesagt. Bulldozer fahren, Flippern, Anhänger angeln,
mit dem WALL-E spielen, eine Verlosung  und Freundschaftsbänder stricken.