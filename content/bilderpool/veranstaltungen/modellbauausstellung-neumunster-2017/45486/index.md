---
layout: "image"
title: "Kugelbahn"
date: "2017-03-08T17:29:59"
picture: "neumuenster27.jpg"
weight: "27"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45486
- /details5d2c-2.html
imported:
- "2019"
_4images_image_id: "45486"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45486 -->
