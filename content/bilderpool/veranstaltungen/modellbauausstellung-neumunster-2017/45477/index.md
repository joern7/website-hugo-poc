---
layout: "image"
title: "Bulldozer"
date: "2017-03-08T17:29:42"
picture: "neumuenster18.jpg"
weight: "18"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45477
- /details3a63.html
imported:
- "2019"
_4images_image_id: "45477"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:42"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45477 -->
