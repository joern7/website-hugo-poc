---
layout: "image"
title: "Ballübergabemaschine"
date: "2017-03-08T17:29:59"
picture: "neumuenster23.jpg"
weight: "23"
konstrukteure: 
- "svefisch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45482
- /details76fa.html
imported:
- "2019"
_4images_image_id: "45482"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45482 -->
