---
layout: "image"
title: "Kirmesmodell 2"
date: "2005-12-16T16:02:17"
picture: "Bild1988.jpg"
weight: "4"
konstrukteure: 
- "Florian Lammering"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5497
- /details85ce.html
imported:
- "2019"
_4images_image_id: "5497"
_4images_cat_id: "480"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5497 -->
