---
layout: "image"
title: "Der sonntag in Muenster"
date: "2006-03-06T09:00:25"
picture: "MSinMuenster.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Scan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/5839
- /detailsfc5b.html
imported:
- "2019"
_4images_image_id: "5839"
_4images_cat_id: "502"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5839 -->
Vorabartikel in der Zeitung Der Sonntag in Muenster