---
layout: "image"
title: "Uebersicht3"
date: "2006-03-06T09:00:25"
picture: "Uebersicht3.jpg"
weight: "11"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Westfälische Nachrichten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/5836
- /details1681.html
imported:
- "2019"
_4images_image_id: "5836"
_4images_cat_id: "472"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5836 -->
