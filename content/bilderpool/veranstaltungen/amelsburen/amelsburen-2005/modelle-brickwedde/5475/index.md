---
layout: "image"
title: "Kreissäge"
date: "2005-12-16T16:01:58"
picture: "Bild1990.jpg"
weight: "8"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5475
- /details9349.html
imported:
- "2019"
_4images_image_id: "5475"
_4images_cat_id: "473"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5475 -->
