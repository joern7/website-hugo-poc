---
layout: "image"
title: "Bohrmaschine"
date: "2005-12-16T16:01:46"
picture: "Bild1959.jpg"
weight: "1"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5468
- /detailsee9a.html
imported:
- "2019"
_4images_image_id: "5468"
_4images_cat_id: "473"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5468 -->
