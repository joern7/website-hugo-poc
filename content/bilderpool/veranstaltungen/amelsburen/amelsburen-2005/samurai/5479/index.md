---
layout: "image"
title: "Modifizierte Version des Samurai"
date: "2005-12-16T16:01:58"
picture: "Bild1965.jpg"
weight: "1"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5479
- /detailsa3c3.html
imported:
- "2019"
_4images_image_id: "5479"
_4images_cat_id: "475"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5479 -->
