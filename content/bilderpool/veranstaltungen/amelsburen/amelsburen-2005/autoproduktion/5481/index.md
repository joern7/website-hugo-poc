---
layout: "image"
title: "Das Ende der Produktionsstraße"
date: "2005-12-16T16:02:07"
picture: "Bild1970.jpg"
weight: "2"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5481
- /detailsad81.html
imported:
- "2019"
_4images_image_id: "5481"
_4images_cat_id: "476"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5481 -->
