---
layout: "image"
title: "4-Takt-Motor 1"
date: "2005-12-16T16:02:07"
picture: "Bild1980.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5489
- /detailsb1b0.html
imported:
- "2019"
_4images_image_id: "5489"
_4images_cat_id: "478"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5489 -->
