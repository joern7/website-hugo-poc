---
layout: "image"
title: "Ein Flugzeug macht loopings"
date: "2005-12-16T16:02:17"
picture: "Bild1991.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5491
- /detailsd246.html
imported:
- "2019"
_4images_image_id: "5491"
_4images_cat_id: "478"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5491 -->
