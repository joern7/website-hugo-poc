---
layout: "image"
title: "Ansicht2"
date: "2006-03-06T09:00:25"
picture: "Tacke2.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas E."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/5838
- /details193a.html
imported:
- "2019"
_4images_image_id: "5838"
_4images_cat_id: "478"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5838 -->
