---
layout: "image"
title: "Amelsbüren 1"
date: "2005-12-17T13:44:46"
picture: "Amelsbren_01.jpg"
weight: "9"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5498
- /details53a4.html
imported:
- "2019"
_4images_image_id: "5498"
_4images_cat_id: "472"
_4images_user_id: "10"
_4images_image_date: "2005-12-17T13:44:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5498 -->
Vor lauter Zuschauern war Manfred Busch kaum zu sehen