---
layout: "image"
title: "Kettenkarussell"
date: "2005-12-16T16:02:17"
picture: "Bild1982.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5493
- /details591d.html
imported:
- "2019"
_4images_image_id: "5493"
_4images_cat_id: "479"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5493 -->
