---
layout: "image"
title: "Kirmesmodell Autokarussell"
date: "2005-12-16T16:02:17"
picture: "Bild1981.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5492
- /details11b9.html
imported:
- "2019"
_4images_image_id: "5492"
_4images_cat_id: "479"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5492 -->
