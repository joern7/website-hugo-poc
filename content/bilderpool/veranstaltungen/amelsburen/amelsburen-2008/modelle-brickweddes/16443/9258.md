---
layout: "comment"
hidden: true
title: "9258"
date: "2009-05-12T21:55:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Lange ist's her. Ich versuch's mal als Anstoß was zusammen zu bekommen:
Der Hydraulik-Axialkolbenmotor ist eine verstellbare Hydraulik-Druckpumpe. Elektrisch angetrieben kann über die verstellbare Taumelscheibe der Axialkolbenhub und damit die Fördermenge des zu pumpenden Öls variiert werden. Der Druck wird mit einem "Pilotventil" (Druckbegrenzer) eingestellt.
Brickweddes bewegen hier vom Rotor abgenommen - soweit ich das erkennen kann - schaltscheibengesteuert mit Druckluft die als Axialkolben eingesetzten ft-Hydraulikzylinder und über eine raffinierte Taumelscheibe in ft wiederum den Rotor des Motors.
Gruß, Udo2