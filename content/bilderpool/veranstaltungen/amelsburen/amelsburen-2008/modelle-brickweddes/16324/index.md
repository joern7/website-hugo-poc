---
layout: "image"
title: "ftamel08_0082"
date: "2008-11-17T21:09:19"
picture: "amel23.jpg"
weight: "2"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16324
- /details81b3.html
imported:
- "2019"
_4images_image_id: "16324"
_4images_cat_id: "1476"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:19"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16324 -->
