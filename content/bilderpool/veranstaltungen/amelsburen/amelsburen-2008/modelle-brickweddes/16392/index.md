---
layout: "image"
title: "Das Riesenrad..."
date: "2008-11-21T16:54:59"
picture: "ftausstellungamelsbueren32.jpg"
weight: "8"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16392
- /details3bba.html
imported:
- "2019"
_4images_image_id: "16392"
_4images_cat_id: "1476"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:59"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16392 -->
...ist inzwischen auch schon in Einzelteile zerlegt und verpackt.