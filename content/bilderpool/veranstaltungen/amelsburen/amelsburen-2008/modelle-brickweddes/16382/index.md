---
layout: "image"
title: "Nabe vom Riesenrad"
date: "2008-11-21T16:54:44"
picture: "ftausstellungamelsbueren22.jpg"
weight: "4"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16382
- /details6d82.html
imported:
- "2019"
_4images_image_id: "16382"
_4images_cat_id: "1476"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:44"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16382 -->
Ganz schön gross...