---
layout: "image"
title: "kessel16107.jpg"
date: "2008-11-23T13:56:32"
picture: "Amelsb16107.JPG"
weight: "5"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16478
- /detailsa7eb.html
imported:
- "2019"
_4images_image_id: "16478"
_4images_cat_id: "1481"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T13:56:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16478 -->
Der hintere Wagen, Teleskopauszug ist draußen.