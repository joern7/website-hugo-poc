---
layout: "image"
title: "kessel160110"
date: "2008-11-23T13:57:09"
picture: "Amelsb16110.JPG"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16479
- /details0d21.html
imported:
- "2019"
_4images_image_id: "16479"
_4images_cat_id: "1481"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T13:57:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16479 -->
Teleskopauszug eingefahren.