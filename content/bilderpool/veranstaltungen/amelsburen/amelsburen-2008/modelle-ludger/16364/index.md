---
layout: "image"
title: "Industrea Dozer von Ludger"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren04.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16364
- /detailsa17d.html
imported:
- "2019"
_4images_image_id: "16364"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16364 -->
Wieder mal ein Supermodell von Ludger.