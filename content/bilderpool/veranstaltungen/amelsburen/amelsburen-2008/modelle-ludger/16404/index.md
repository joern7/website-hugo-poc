---
layout: "image"
title: "Bulldozer-Spiel"
date: "2008-11-21T17:41:36"
picture: "ft12.jpg"
weight: "13"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16404
- /details5de3-4.html
imported:
- "2019"
_4images_image_id: "16404"
_4images_cat_id: "1473"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16404 -->
