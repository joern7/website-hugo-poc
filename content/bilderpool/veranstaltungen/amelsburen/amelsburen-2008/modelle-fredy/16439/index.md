---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:42:29"
picture: "ft47.jpg"
weight: "2"
konstrukteure: 
- "Fredy"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16439
- /details67c0-2.html
imported:
- "2019"
_4images_image_id: "16439"
_4images_cat_id: "1482"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16439 -->
