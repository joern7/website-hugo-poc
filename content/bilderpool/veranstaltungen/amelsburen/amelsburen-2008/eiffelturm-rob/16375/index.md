---
layout: "image"
title: "Der Eiffelturm"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren15.jpg"
weight: "4"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16375
- /details1d5b.html
imported:
- "2019"
_4images_image_id: "16375"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16375 -->
Sehr schönes und funktionelles Modell von Rob van Baal.