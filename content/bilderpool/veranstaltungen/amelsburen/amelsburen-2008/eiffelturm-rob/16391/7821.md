---
layout: "comment"
hidden: true
title: "7821"
date: "2008-11-21T19:51:02"
uploadBy:
- "robvanbaal"
license: "unknown"
imported:
- "2019"
---
45-60 minuten; alleine. Ist eigentlich nicht so schwierig. Beim bauen war immer die Frage: Wie geht das später in mein Wagen. Wenn mann das nicht tut, kommt das Modell nicht vom Dachboden!