---
layout: "image"
title: "Erste Aussichtsplattform.."
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren17.jpg"
weight: "6"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16377
- /detailsa838-2.html
imported:
- "2019"
_4images_image_id: "16377"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16377 -->
mit vielen Touristen.