---
layout: "image"
title: "TST s Modelle"
date: "2008-11-21T17:42:29"
picture: "ft57.jpg"
weight: "7"
konstrukteure: 
- "TST"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16449
- /detailsa47e.html
imported:
- "2019"
_4images_image_id: "16449"
_4images_cat_id: "1474"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16449 -->
