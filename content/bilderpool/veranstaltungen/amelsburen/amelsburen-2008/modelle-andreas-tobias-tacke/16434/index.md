---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:42:29"
picture: "ft42.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16434
- /details4991.html
imported:
- "2019"
_4images_image_id: "16434"
_4images_cat_id: "1474"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16434 -->
