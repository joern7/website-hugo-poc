---
layout: "image"
title: "Amelsb16121.JPG"
date: "2008-11-23T14:09:14"
picture: "Amelsb16121.JPG"
weight: "2"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16484
- /details8a35.html
imported:
- "2019"
_4images_image_id: "16484"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:09:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16484 -->
Gabellichtschranke mit Elektronik und zwei LEDs im Sockel (eine rote sitzt gegenüber der gelben, die hier zu sehen ist). Der Würfel ist eine aufgebohrte Schneckenmutter.