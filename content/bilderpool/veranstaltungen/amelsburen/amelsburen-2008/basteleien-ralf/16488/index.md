---
layout: "image"
title: "Amelsb16126.JPG"
date: "2008-11-23T14:16:05"
picture: "Amelsb16126.JPG"
weight: "6"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16488
- /detailsc63e.html
imported:
- "2019"
_4images_image_id: "16488"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:16:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16488 -->
Zwei P-Betätiger Marke Eigenbau. Die Membran wird von einem O-Ring gehalten, der sie in die Nut einer ft-Seilscheibe klemmt. Der rote Ring aus der ft-Nabe ist nur Zierde. Von der Rückseite her ist eine P-Düse als Anschluss angeklebt.