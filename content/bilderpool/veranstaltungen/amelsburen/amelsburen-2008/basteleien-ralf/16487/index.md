---
layout: "image"
title: "Amelsb16125.JPG"
date: "2008-11-23T14:13:19"
picture: "Amelsb16125.JPG"
weight: "5"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16487
- /detailse818.html
imported:
- "2019"
_4images_image_id: "16487"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:13:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16487 -->
Der Vakuum-Greifer bei der Arbeit.