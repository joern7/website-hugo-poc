---
layout: "image"
title: "Amelsb16124.JPG"
date: "2008-11-23T14:12:52"
picture: "Amelsb16124.JPG"
weight: "4"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16486
- /details0cce.html
imported:
- "2019"
_4images_image_id: "16486"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16486 -->
Ein Vakuum-Sauger.