---
layout: "image"
title: "Amelsb16308.JPG"
date: "2008-11-23T15:06:03"
picture: "Amelsb16308.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16499
- /details357b.html
imported:
- "2019"
_4images_image_id: "16499"
_4images_cat_id: "1477"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T15:06:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16499 -->
Der Flieger in Draufsicht. Die Heckflosse hat sich einen Transportschaden zugezogen, nicht mal 1 m von der Wohnungstür entfernt. Das Modell war, in seine Luftmatratze halb eingewickelt, vor der Tür abgelegt worden. Der werte Herr Konstrukteur meinte, nach Abschließen der Tür über selbiges drüber steigen zu müssen, und traf es mit dem Absatz.