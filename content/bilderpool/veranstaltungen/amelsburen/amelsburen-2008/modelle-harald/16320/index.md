---
layout: "image"
title: "Flieger"
date: "2008-11-17T21:09:19"
picture: "amel19.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16320
- /detailsdcef-2.html
imported:
- "2019"
_4images_image_id: "16320"
_4images_cat_id: "1477"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:19"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16320 -->
