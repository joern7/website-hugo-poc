---
layout: "image"
title: "LKW"
date: "2008-11-21T17:42:29"
picture: "ft55.jpg"
weight: "6"
konstrukteure: 
- "Siegfried"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16447
- /detailsc9e9.html
imported:
- "2019"
_4images_image_id: "16447"
_4images_cat_id: "1479"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16447 -->
