---
layout: "image"
title: "Industriemodell"
date: "2008-11-21T17:42:29"
picture: "ft21.jpg"
weight: "3"
konstrukteure: 
- "Olli"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16413
- /details1c76.html
imported:
- "2019"
_4images_image_id: "16413"
_4images_cat_id: "1485"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16413 -->
