---
layout: "image"
title: "Mal-Maschine"
date: "2008-11-21T17:41:15"
picture: "ft07.jpg"
weight: "6"
konstrukteure: 
- "MisterWho"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16399
- /detailsf734.html
imported:
- "2019"
_4images_image_id: "16399"
_4images_cat_id: "1486"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16399 -->
