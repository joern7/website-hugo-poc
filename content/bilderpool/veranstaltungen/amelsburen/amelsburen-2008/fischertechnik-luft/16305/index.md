---
layout: "image"
title: "Roboter"
date: "2008-11-17T21:08:46"
picture: "amel04.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16305
- /detailsa43e.html
imported:
- "2019"
_4images_image_id: "16305"
_4images_cat_id: "1480"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16305 -->
Roboter zum Selbersteuern.