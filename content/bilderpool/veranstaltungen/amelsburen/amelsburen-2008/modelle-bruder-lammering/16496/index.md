---
layout: "image"
title: "Amelsb16304.JPG"
date: "2008-11-23T14:28:23"
picture: "Amelsb16304.JPG"
weight: "9"
konstrukteure: 
- "Lammering"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16496
- /details6c18.html
imported:
- "2019"
_4images_image_id: "16496"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:28:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16496 -->
Und noch mehr Kassenhäuschen für Kirmesmodelle.