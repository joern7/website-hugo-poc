---
layout: "image"
title: "Amelsb15118.JPG"
date: "2008-11-23T14:24:03"
picture: "Amelsb15118.JPG"
weight: "4"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16491
- /details8994.html
imported:
- "2019"
_4images_image_id: "16491"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:24:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16491 -->
