---
layout: "image"
title: "Amelsb16300.JPG"
date: "2008-11-23T14:29:10"
picture: "Amelsb16300.JPG"
weight: "10"
konstrukteure: 
- "Lammering"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16497
- /details8e38.html
imported:
- "2019"
_4images_image_id: "16497"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:29:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16497 -->
Der Kassenhäuschen dritter Teil.