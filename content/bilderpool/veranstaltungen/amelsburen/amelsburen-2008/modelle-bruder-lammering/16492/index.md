---
layout: "image"
title: "Amelsb15119.JPG"
date: "2008-11-23T14:24:40"
picture: "Amelsb15119.JPG"
weight: "5"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16492
- /details5f37.html
imported:
- "2019"
_4images_image_id: "16492"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:24:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16492 -->
Ein Radioteleskop, in allen Achsen schwenkbar.