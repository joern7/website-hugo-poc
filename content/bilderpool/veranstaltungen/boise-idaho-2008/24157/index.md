---
layout: "image"
title: "Amelia Programs the Carnival Ride"
date: "2009-06-03T16:41:08"
picture: "sm_amelia_swing.jpg"
weight: "106"
konstrukteure: 
- "Amelia"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Amelia", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24157
- /details6ae0.html
imported:
- "2019"
_4images_image_id: "24157"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-06-03T16:41:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24157 -->
My daughter programmed the Rotational Swing using the PCS BRAIN!