---
layout: "image"
title: "Motor Test"
date: "2009-05-29T15:30:25"
picture: "motor_test_4.jpg"
weight: "97"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24124
- /details062b.html
imported:
- "2019"
_4images_image_id: "24124"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-29T15:30:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24124 -->
Testing the PCS BRAIN with a motor under a small load, using a single 9v rechargeable battery.