---
layout: "image"
title: "LED Guessing Gamer"
date: "2009-05-19T17:24:06"
picture: "led_game_a1.jpg"
weight: "91"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["LED", "Guessing", "Game", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24058
- /detailsb3c7-3.html
imported:
- "2019"
_4images_image_id: "24058"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-19T17:24:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24058 -->
This is a simple LED Game I built for the LED contest at www.instructables.com. The BRAIN random selects one of two LEDs hidden behind a ft plate. You guess which one is lit by pressing one of the buttons. Should you guess correct, a happy tune plays.

Thought to share.