---
layout: "image"
title: "Rendered Walker"
date: "2008-12-12T06:36:27"
picture: "sm_hexapod.jpg"
weight: "69"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Walker"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16580
- /details342f.html
imported:
- "2019"
_4images_image_id: "16580"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-12-12T06:36:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16580 -->
Something I have been working on. Thought to share.