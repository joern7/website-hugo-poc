---
layout: "image"
title: "Building"
date: "2009-01-31T00:06:29"
picture: "sm_build_a.jpg"
weight: "74"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/17215
- /details07ed.html
imported:
- "2019"
_4images_image_id: "17215"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17215 -->
Building with ft and the PCS BRAIN. Thought to share.