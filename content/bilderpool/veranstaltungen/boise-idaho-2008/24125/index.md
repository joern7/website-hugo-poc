---
layout: "image"
title: "Battery Test"
date: "2009-05-29T15:30:25"
picture: "motor_test_3.jpg"
weight: "98"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24125
- /details4363.html
imported:
- "2019"
_4images_image_id: "24125"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-29T15:30:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24125 -->
Testing the PCS BRAIN with a motor under a small load, using a single 9v rechargeable battery.