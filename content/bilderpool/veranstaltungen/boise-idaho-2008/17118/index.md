---
layout: "image"
title: "Snow Sled"
date: "2009-01-20T22:36:24"
picture: "sm_snow_sled.jpg"
weight: "72"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Snow", "Sled"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/17118
- /details583e.html
imported:
- "2019"
_4images_image_id: "17118"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-01-20T22:36:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17118 -->
Three members of the development team (and families) rented a cabin in McCall Idaho, and built a snow sled!