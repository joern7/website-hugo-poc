---
layout: "image"
title: "Having fun with motors"
date: "2009-05-11T22:45:24"
picture: "sm_motor_testing.jpg"
weight: "84"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24000
- /details4fcb.html
imported:
- "2019"
_4images_image_id: "24000"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-11T22:45:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24000 -->
Using ft motors with the PCS BRAIN.