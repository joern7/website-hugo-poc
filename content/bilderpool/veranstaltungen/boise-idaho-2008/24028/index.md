---
layout: "image"
title: "PCS offices"
date: "2009-05-14T23:15:31"
picture: "meeting_1.jpg"
weight: "88"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "office"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24028
- /detailsb1bc.html
imported:
- "2019"
_4images_image_id: "24028"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-14T23:15:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24028 -->
Meeting on the development table at PCS in Boise Idaho. Thought to share.