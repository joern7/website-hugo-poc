---
layout: "image"
title: "News Story Coverage"
date: "2008-02-16T08:06:45"
picture: "ktrv_story_1.jpg"
weight: "35"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "ERC", "News", "Story", "models"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13650
- /details9b45.html
imported:
- "2019"
_4images_image_id: "13650"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-02-16T08:06:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13650 -->
A local news station interviewed us about the PCS ERC robotics competition. (Visit http://www.ktrv.com/ and cycle through today's stories on the flash menu in the middle of the screen). Here are several screen captures. Thought to share.