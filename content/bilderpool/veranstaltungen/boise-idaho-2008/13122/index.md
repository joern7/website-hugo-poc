---
layout: "image"
title: "BSU_Team_1"
date: "2007-12-20T17:36:17"
picture: "bsu_g1c.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "State", "University", "2007"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13122
- /detailsdb08.html
imported:
- "2019"
_4images_image_id: "13122"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2007-12-20T17:36:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13122 -->
These robots were designed, created and built at Boise State University in Boise Idaho, Dec 2007.