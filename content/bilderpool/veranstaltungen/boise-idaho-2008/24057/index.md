---
layout: "image"
title: "ft in Boise Idaho"
date: "2009-05-18T20:11:07"
picture: "ft_hobbytown.jpg"
weight: "90"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Hobbytown", "USA"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24057
- /detailsc584-2.html
imported:
- "2019"
_4images_image_id: "24057"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-18T20:11:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24057 -->
Howdy all. Our local science toy/hobby store is carrying ft! Very exciting. Thought to share. 

How does this compare to other local displays?