---
layout: "image"
title: "Worm Gear Model with PCS Brain"
date: "2009-10-30T21:05:03"
picture: "ort_model_2.jpg"
weight: "114"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Worm", "Gear", "PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25584
- /detailsffc6-2.html
imported:
- "2019"
_4images_image_id: "25584"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25584 -->
Worm Gear Model integrating the PCS Brain and sensors for a training session.