---
layout: "image"
title: "ft Walker"
date: "2008-11-21T19:43:22"
picture: "ft_walker_1.jpg"
weight: "59"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["walking", "robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16464
- /detailsc4a2-2.html
imported:
- "2019"
_4images_image_id: "16464"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-11-21T19:43:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16464 -->
These are various walkers I built over the year. Thought to share.