---
layout: "image"
title: "Laura and the Carnival Ride"
date: "2009-11-20T17:55:37"
picture: "ft_ride_a.jpg"
weight: "116"
konstrukteure: 
- "Harrison"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "PCS", "Carnival"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25804
- /details7bef-3.html
imported:
- "2019"
_4images_image_id: "25804"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-11-20T17:55:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25804 -->
Laura is playing with a variation of one of the carnival rides.