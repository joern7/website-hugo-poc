---
layout: "image"
title: "Amelia Constructing the Rotating Swing"
date: "2009-06-02T18:45:23"
picture: "amelia_ft_swing3.jpg"
weight: "104"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Amelia", "PCS", "Edventures"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24153
- /detailse1f8.html
imported:
- "2019"
_4images_image_id: "24153"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-06-02T18:45:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24153 -->
Mhy daughter spent the day with me at the office. We build ft models of course! Thought to share.