---
layout: "image"
title: "Worm Gear Model with PCS Brain"
date: "2009-10-30T21:05:02"
picture: "ort_model_1.jpg"
weight: "113"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Worm", "Gear", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25583
- /details7ebb.html
imported:
- "2019"
_4images_image_id: "25583"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25583 -->
Worm Gear Model integrating the PCS Brain and sensors for a training session.