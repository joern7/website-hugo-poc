---
layout: "image"
title: "ft Building"
date: "2009-01-31T00:06:29"
picture: "sm_build_c.jpg"
weight: "75"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/17216
- /details84cc.html
imported:
- "2019"
_4images_image_id: "17216"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17216 -->
Building with ft and the PCS BRAIN. Thought to share.