---
layout: "image"
title: "Working on the Snow Sled 2009"
date: "2009-01-20T22:36:25"
picture: "sm_c_ss.jpg"
weight: "73"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Snow", "Sled"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/17119
- /details6393.html
imported:
- "2019"
_4images_image_id: "17119"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-01-20T22:36:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17119 -->
Three members of the development team (and families) rented a cabin in McCall Idaho, and built a snow sled! (2009) This is one of the young people who assisted in its construction. 

Video is here: http://www.youtube.com/watch?v=hv5LyR8RCfY