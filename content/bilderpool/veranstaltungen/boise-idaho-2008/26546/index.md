---
layout: "image"
title: "Robot Magazine Article"
date: "2010-02-26T21:03:43"
picture: "brain_review.jpg"
weight: "120"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain", "carousel", "Robot", "Magazine"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26546
- /detailsb1fb.html
imported:
- "2019"
_4images_image_id: "26546"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-02-26T21:03:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26546 -->
A snapshot of the article reviewing the PCS Brain and the ft carousel in the May/June 2010 issue in Robot Magazine!