---
layout: "image"
title: "elevator"
date: "2009-05-16T09:26:19"
picture: "ft-elevator_a.jpg"
weight: "89"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24029
- /details7ccf-2.html
imported:
- "2019"
_4images_image_id: "24029"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-16T09:26:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24029 -->
Simple fischertechnik elevator that integrates the PCS BRAIN