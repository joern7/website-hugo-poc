---
layout: "image"
title: "Laura and the Model"
date: "2009-11-20T17:55:36"
picture: "ft_ride_b.jpg"
weight: "115"
konstrukteure: 
- "Harrison"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "PCS", "carnival", "ride"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25803
- /details9d28.html
imported:
- "2019"
_4images_image_id: "25803"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-11-20T17:55:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25803 -->
Laura is playing with a variation of one of the carnival rides.