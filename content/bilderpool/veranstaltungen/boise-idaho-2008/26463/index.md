---
layout: "image"
title: "Counter Wheel Model"
date: "2010-02-18T17:54:21"
picture: "counter_wheel2.jpg"
weight: "119"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26463
- /detailsa754-2.html
imported:
- "2019"
_4images_image_id: "26463"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-02-18T17:54:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26463 -->
Simple counter wheel integrating an IR sensor. Thought to share.