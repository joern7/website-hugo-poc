---
layout: "image"
title: "Sandon's Dragster"
date: "2009-05-29T15:30:24"
picture: "sm_ft_car.jpg"
weight: "95"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["dragster", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24120
- /detailsf985.html
imported:
- "2019"
_4images_image_id: "24120"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-29T15:30:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24120 -->
Laura and her son Sandon built this awesome dragster. Thought to share.