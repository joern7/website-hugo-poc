---
layout: "image"
title: "ft Robot"
date: "2008-12-15T20:21:18"
picture: "sm_ft_robot.jpg"
weight: "70"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16623
- /details934f.html
imported:
- "2019"
_4images_image_id: "16623"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-12-15T20:21:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16623 -->
Robot using the PCS Brain.