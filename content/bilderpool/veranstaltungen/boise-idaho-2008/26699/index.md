---
layout: "image"
title: "Sunday School Robotics"
date: "2010-03-15T16:56:39"
picture: "sm_ft_ucc1.jpg"
weight: "122"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26699
- /detailsbff0.html
imported:
- "2019"
_4images_image_id: "26699"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-03-15T16:56:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26699 -->
Taught a robotics class in Sunday School at my church.