---
layout: "image"
title: "Governor's Innovation Summit"
date: "2009-09-24T23:27:27"
picture: "ft_booth.jpg"
weight: "110"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Idaho", "Governor's", "Innovation", "Summit"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25355
- /detailsa93f-2.html
imported:
- "2019"
_4images_image_id: "25355"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-09-24T23:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25355 -->
The PCS Booth at the Governor's Innovation Summit ... featuring fischertechnik and the the PCS BRAIN!