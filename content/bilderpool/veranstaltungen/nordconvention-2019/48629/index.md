---
layout: "image"
title: "dsc00015"
date: 2020-04-22T15:33:29+02:00
picture: "DSC00015.JPG"
weight: "10"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Kleiner dürfte sie wirklich nicht sein (lt. Ingwer jedenfalls)