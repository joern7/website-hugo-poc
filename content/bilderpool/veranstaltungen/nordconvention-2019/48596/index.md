---
layout: "image"
title: "dsc00080"
date: 2020-04-22T15:32:48+02:00
picture: "DSC00080.JPG"
weight: "43"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Rot-Gelb ist die fischertechnik-Welt