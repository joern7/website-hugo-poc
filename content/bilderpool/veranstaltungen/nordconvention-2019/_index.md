---
layout: "overview"
title: "Nordconvention 2019"
date: 2019-04-28T15:32:29+02:00
---

Die Nordconvention 2019 fand am Samstag, 27.04.2019 von 10-17 Uhr im Schulzentrum in Mellendorf/ Wedemark statt.

Der Ausrichter war der Verein zur Förderung des Richard-Brandt-Heimatmuseums Wedemark e.V. in Kooperation mit dem Verein ftc Modellbau e.V. .
