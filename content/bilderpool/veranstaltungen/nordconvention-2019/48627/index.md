---
layout: "image"
title: "dsc00018"
date: 2020-04-22T15:33:26+02:00
picture: "DSC00018.JPG"
weight: "12"
konstrukteure: 
- "chrischan"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kugelbahn mit Durchblick