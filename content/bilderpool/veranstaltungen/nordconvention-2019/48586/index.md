---
layout: "image"
title: "dsc00098"
date: 2020-04-22T15:32:36+02:00
picture: "DSC00098.JPG"
weight: "53"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die fantastische Mühle von Ingwer für Edel