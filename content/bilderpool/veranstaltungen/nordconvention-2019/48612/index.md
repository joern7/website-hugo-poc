---
layout: "image"
title: "dsc00045"
date: 2020-04-22T15:33:07+02:00
picture: "DSC00045.JPG"
weight: "27"
konstrukteure: 
- "Etliche"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Die hinteren Reihen kann man kaum erkennen, so groß ist die Halle