---
layout: "image"
title: "dsc00027"
date: 2020-04-22T15:33:18+02:00
picture: "DSC00027.JPG"
weight: "18"
konstrukteure: 
- "DirkW"
fotografen:
- "Silke Glauberstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Website-Team"
license: "unknown"
---

Der 3D-Drucker 2.0 in Aktion