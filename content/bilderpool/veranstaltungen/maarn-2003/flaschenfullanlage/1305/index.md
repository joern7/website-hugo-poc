---
layout: "image"
title: "Flaschen Abfüllanlage6"
date: "2003-08-04T09:17:36"
picture: "abfuellanlage6.jpg"
weight: "16"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1305
- /detailsd708.html
imported:
- "2019"
_4images_image_id: "1305"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1305 -->
Die Befüllstation