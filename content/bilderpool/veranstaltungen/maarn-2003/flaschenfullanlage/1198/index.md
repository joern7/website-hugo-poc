---
layout: "image"
title: "flaschenUndTank"
date: "2003-06-22T14:23:50"
picture: "flesfabriektank.jpg"
weight: "1"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: ["Flaschenanlage", "Tank"]
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/1198
- /details1e89.html
imported:
- "2019"
_4images_image_id: "1198"
_4images_cat_id: "443"
_4images_user_id: "7"
_4images_image_date: "2003-06-22T14:23:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1198 -->
Endstationen der Flaschenanlage , Modell vom Herrn Leurs . Die ganz Anlage ist  elektromechanisch und mit Silberlinge gesteuert .
Rechts seht man der Tank vom Herrn Lankheet (IR gesteuert)