---
layout: "image"
title: "Eine Stütze vom Mobilkran"
date: "2003-07-08T16:05:25"
picture: "Sttze_vom_kran.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1222
- /details2638-2.html
imported:
- "2019"
_4images_image_id: "1222"
_4images_cat_id: "445"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1222 -->
