---
layout: "image"
title: "Ampel mit Straße"
date: "2003-07-10T14:03:57"
picture: "ampel.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1241
- /detailsefbb.html
imported:
- "2019"
_4images_image_id: "1241"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1241 -->
Hier ein Modell einer Ampel mit Straße. Die Autos fahren auf der Starße entlang und halten dann, wenn die Fußgängerampel betätigt wurde, dort an.