---
layout: "image"
title: "Panzer2"
date: "2003-07-31T18:41:47"
picture: "panzer2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Panzer", "Robi", "Ketten"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1274
- /details7724.html
imported:
- "2019"
_4images_image_id: "1274"
_4images_cat_id: "446"
_4images_user_id: "34"
_4images_image_date: "2003-07-31T18:41:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1274 -->
Der Panzer hat zwei Motore zum fahren und zwei für den Turm. Einen zum drehen und den anderen für rauf runter vom Rohr.
Der Panzer ist sehr wendig und man kann ihn auch sehr gut als einen Kettenrobi umbauen.