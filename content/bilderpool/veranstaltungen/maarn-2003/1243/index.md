---
layout: "image"
title: "Boxermotor"
date: "2003-07-10T14:03:57"
picture: "boxermotor.jpg"
weight: "7"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1243
- /detailsae3c.html
imported:
- "2019"
_4images_image_id: "1243"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1243 -->
Ein weiteres in Maarn ausgestelltes Funktionsmodell von einem Motor in der Anordnung eines Boxermotors