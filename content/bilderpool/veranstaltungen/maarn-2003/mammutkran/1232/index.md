---
layout: "image"
title: "Mammoetkran"
date: "2003-07-08T17:12:53"
picture: "Mammutkran_2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1232
- /details2a5c.html
imported:
- "2019"
_4images_image_id: "1232"
_4images_cat_id: "447"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1232 -->
