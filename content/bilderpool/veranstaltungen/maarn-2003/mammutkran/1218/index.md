---
layout: "image"
title: "Mammoetkran"
date: "2003-07-08T16:05:24"
picture: "Mammutkran.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1218
- /details2b04.html
imported:
- "2019"
_4images_image_id: "1218"
_4images_cat_id: "447"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1218 -->
