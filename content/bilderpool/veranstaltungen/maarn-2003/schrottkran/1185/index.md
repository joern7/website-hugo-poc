---
layout: "image"
title: "greiferglobal"
date: "2003-06-15T05:03:36"
picture: "grijpermaarn01.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/1185
- /detailsefa7.html
imported:
- "2019"
_4images_image_id: "1185"
_4images_cat_id: "448"
_4images_user_id: "7"
_4images_image_date: "2003-06-15T05:03:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1185 -->
Obenansicht  auf einem ziemlich speziellen Krangreifer