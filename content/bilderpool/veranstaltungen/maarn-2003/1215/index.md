---
layout: "image"
title: "King of the Road mit größeren Rädern"
date: "2003-07-08T16:05:24"
picture: "King_of_the_Road.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1215
- /detailsd87c.html
imported:
- "2019"
_4images_image_id: "1215"
_4images_cat_id: "138"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1215 -->
