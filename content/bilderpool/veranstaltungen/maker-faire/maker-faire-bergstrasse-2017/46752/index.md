---
layout: "image"
title: "Die Keksfabrik wird bestaunt"
date: "2017-10-02T21:00:58"
picture: "klein-0014.jpg"
weight: "25"
konstrukteure: 
- "Die Elektrofuzzies"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46752
- /details325c-2.html
imported:
- "2019"
_4images_image_id: "46752"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T21:00:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46752 -->
