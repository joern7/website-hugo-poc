---
layout: "image"
title: "3D-Drucker"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse04.jpg"
weight: "4"
konstrukteure: 
- "Til Harbaum"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46275
- /details94ab.html
imported:
- "2019"
_4images_image_id: "46275"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46275 -->
... und dann auf Tils 3D-Drucker ausgedruckt. Gesteuert vom TXT mit Community-Firmware, versteht sich.