---
layout: "image"
title: "Fachgespräche"
date: "2017-10-02T21:00:58"
picture: "klein-0013.jpg"
weight: "26"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46753
- /detailsc55c.html
imported:
- "2019"
_4images_image_id: "46753"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T21:00:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46753 -->
