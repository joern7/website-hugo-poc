---
layout: "image"
title: "Unser Stand - etwas später..."
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse02.jpg"
weight: "2"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46273
- /details4942.html
imported:
- "2019"
_4images_image_id: "46273"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46273 -->
Zwar war die Maker Faire in Bensheim lange nicht so gut besucht wie die in Hannover - aber am fischertechnik-Stand bildeten sich dennoch immer wieder Besuchertrauben.