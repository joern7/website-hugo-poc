---
layout: "image"
title: "Keks-Trockenlager"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46280
- /details4a23.html
imported:
- "2019"
_4images_image_id: "46280"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46280 -->
Im Hochregallager wurden die bedruckten Kekse zum Trocknen abgelegt.