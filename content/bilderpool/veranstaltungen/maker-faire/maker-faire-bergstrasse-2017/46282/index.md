---
layout: "image"
title: "Robotersteuerung mit Blockly (via Tablet)"
date: "2017-09-21T17:04:44"
picture: "makerfairebergstrasse11.jpg"
weight: "11"
konstrukteure: 
- "EstherM"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46282
- /details0bcd.html
imported:
- "2019"
_4images_image_id: "46282"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:44"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46282 -->
In der "Mitmachecke" konnten die Besucher fischertechnik-Modelle konstruieren oder Roboter mit Community-Firmware vom Tablet via Blockly steuern.