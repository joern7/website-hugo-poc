---
layout: "image"
title: "Und noch mal ein Teil des Teams"
date: "2017-10-02T20:30:44"
picture: "klein-003.jpg"
weight: "18"
konstrukteure: 
- "Was man sieht: die Elektrofuzzies"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46745
- /detailsbb57.html
imported:
- "2019"
_4images_image_id: "46745"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46745 -->
