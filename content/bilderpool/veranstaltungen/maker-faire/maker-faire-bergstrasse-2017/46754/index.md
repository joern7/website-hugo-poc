---
layout: "image"
title: "Nachdenken"
date: "2017-10-02T21:00:58"
picture: "klein-0012.jpg"
weight: "27"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46754
- /details47ab.html
imported:
- "2019"
_4images_image_id: "46754"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T21:00:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46754 -->
Hier wird eher nachgedacht als diskutiert!