---
layout: "image"
title: "Wartungsarbeiten"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46281
- /detailsb235.html
imported:
- "2019"
_4images_image_id: "46281"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46281 -->
Gelegentlich bedurfte der Drucker des einen oder anderen Wartungseingriffs - vor allem, wenn sich zu viel der süßlich-klebrigen Schriftfarbe neben dem Keks auf dem Transportband verteilt hatte oder die Farb-Spritzen nachgefüllt werden mussten.