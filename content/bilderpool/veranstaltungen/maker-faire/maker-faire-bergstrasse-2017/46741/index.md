---
layout: "image"
title: "Flugobjekt"
date: "2017-10-02T20:30:44"
picture: "klein-007.jpg"
weight: "14"
konstrukteure: 
- "Magnus"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46741
- /details77eb.html
imported:
- "2019"
_4images_image_id: "46741"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46741 -->
Leider ist der Hintergrund des Fotos etwas unschön, aber auch außerhalb der Halle konnte das Ding fliegen.