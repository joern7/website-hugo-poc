---
layout: "image"
title: "makerfairehannover4.jpg"
date: "2017-08-28T14:50:33"
picture: "makerfairehannover4.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46215
- /details5d3f.html
imported:
- "2019"
_4images_image_id: "46215"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-28T14:50:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46215 -->
