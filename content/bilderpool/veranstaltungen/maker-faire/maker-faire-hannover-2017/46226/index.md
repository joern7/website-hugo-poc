---
layout: "image"
title: "3D Printer Teile"
date: "2017-09-01T16:27:17"
picture: "makerfaire2.jpg"
weight: "14"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46226
- /details5158.html
imported:
- "2019"
_4images_image_id: "46226"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T16:27:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46226 -->
Links Hexapod (Rot) Mittig LPE Servo Halter (Gelb rechts darunter)
Rechts (Grau) Dreibackenfutter, Rest Diverses