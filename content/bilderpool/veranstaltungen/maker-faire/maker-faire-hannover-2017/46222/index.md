---
layout: "image"
title: "makerfaireb3.jpg"
date: "2017-08-29T19:05:31"
picture: "makerfaireb3.jpg"
weight: "11"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46222
- /detailsbe12.html
imported:
- "2019"
_4images_image_id: "46222"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-29T19:05:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46222 -->
