---
layout: "image"
title: "fischertechnik Pinball"
date: "2017-09-01T16:27:17"
picture: "makerfaire1.jpg"
weight: "13"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46225
- /detailsc65a.html
imported:
- "2019"
_4images_image_id: "46225"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T16:27:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46225 -->
