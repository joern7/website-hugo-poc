---
layout: "image"
title: "makerfairehannover1.jpg"
date: "2017-08-28T14:50:33"
picture: "makerfairehannover1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46212
- /detailsfe61.html
imported:
- "2019"
_4images_image_id: "46212"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-28T14:50:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46212 -->
Ralf mailte mir die Bilder zur Veröffentlichung.