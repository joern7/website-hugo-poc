---
layout: "comment"
hidden: true
title: "23600"
date: "2017-08-29T17:11:55"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hier wird gerade ein filigraner Totenkopf gedruckt.

Ralf hatte vorab mit einem Studenten den G-Code bearbeitet (Temperatur), 
damit der Druck gelingt.

Kompliment, der Totenkopf schaute echt super aus.

Gruß
Dirk