---
layout: "image"
title: "Solarrad"
date: "2017-11-20T19:26:36"
picture: "makerfairkiel31.jpg"
weight: "31"
konstrukteure: 
- "Grau"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46958
- /details6c71.html
imported:
- "2019"
_4images_image_id: "46958"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:36"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46958 -->
