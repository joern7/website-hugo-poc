---
layout: "image"
title: "Flipper"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel05.jpg"
weight: "5"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46932
- /details185b.html
imported:
- "2019"
_4images_image_id: "46932"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46932 -->
