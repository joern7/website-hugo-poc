---
layout: "image"
title: "Bilder von ftc-Ausstellungen"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel29.jpg"
weight: "29"
konstrukteure: 
- "Grau"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46956
- /detailsda22.html
imported:
- "2019"
_4images_image_id: "46956"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46956 -->
