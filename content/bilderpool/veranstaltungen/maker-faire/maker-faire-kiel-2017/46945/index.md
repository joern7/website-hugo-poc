---
layout: "image"
title: "Truck mit Anhänger"
date: "2017-11-20T19:26:24"
picture: "makerfairkiel18.jpg"
weight: "18"
konstrukteure: 
- "Christian"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46945
- /details94e4.html
imported:
- "2019"
_4images_image_id: "46945"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46945 -->
