---
layout: "comment"
hidden: true
title: "23834"
date: "2017-11-29T00:31:38"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Hallo meine Herren,
wie das Modell nach Kiel kam ist rel. einfach und schnell erzählt.
Der Christian aus der Nähe von Lübeck hat den Roland ganz einfach mal besucht und dort von dem Original wohl (natürlich mit der Erlaubnis von Roland) ein paar Fotos gemacht, weil er so fasziniert von dem Modell war. Da er ein ft-Fan von frühester Stunde ist und ein ganz toller Bastler obendrein ist, hat er das Modell einfach nachgebaut und auf der Maker-Faire Kiel ausgestellt.

Ich habe ja versucht ihn zu überreden, dass er es mir gleich für die nächste Nordconvention mitgibt, aber er wollte es lieber selber hierher bringen.

Da er hier eher selten reinschaut, habe ich mal für ihn geantwortet ;-)