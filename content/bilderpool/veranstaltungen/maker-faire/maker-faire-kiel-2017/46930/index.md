---
layout: "image"
title: "Stand"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel03.jpg"
weight: "3"
konstrukteure: 
- "ftc"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46930
- /details09e3.html
imported:
- "2019"
_4images_image_id: "46930"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46930 -->
