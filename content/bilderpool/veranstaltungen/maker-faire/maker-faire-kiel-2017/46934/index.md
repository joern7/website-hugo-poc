---
layout: "image"
title: "Fräse Detail"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel07.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46934
- /detailsf6b0.html
imported:
- "2019"
_4images_image_id: "46934"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46934 -->
