---
layout: "image"
title: "Drehbank"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel08.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46935
- /detailsae4e.html
imported:
- "2019"
_4images_image_id: "46935"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46935 -->
