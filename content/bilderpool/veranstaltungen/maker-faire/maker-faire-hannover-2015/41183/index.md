---
layout: "image"
title: "makerfaire010"
date: "2015-06-08T21:30:57"
picture: "makerfaire10_2.jpg"
weight: "63"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/41183
- /details8a1e.html
imported:
- "2019"
_4images_image_id: "41183"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41183 -->
Ruhe vor dem Ansturm.