---
layout: "image"
title: "makerfaire13.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41110
- /details8f37.html
imported:
- "2019"
_4images_image_id: "41110"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41110 -->
