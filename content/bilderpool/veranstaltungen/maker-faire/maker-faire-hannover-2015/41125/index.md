---
layout: "image"
title: "makerfaire28.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire28.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41125
- /details29c9.html
imported:
- "2019"
_4images_image_id: "41125"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41125 -->
