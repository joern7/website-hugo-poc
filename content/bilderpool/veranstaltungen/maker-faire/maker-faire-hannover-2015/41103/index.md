---
layout: "image"
title: "makerfaire06.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41103
- /details464a.html
imported:
- "2019"
_4images_image_id: "41103"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41103 -->
