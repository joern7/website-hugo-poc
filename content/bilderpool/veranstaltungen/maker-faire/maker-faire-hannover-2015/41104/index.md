---
layout: "image"
title: "makerfaire07.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41104
- /details5087.html
imported:
- "2019"
_4images_image_id: "41104"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41104 -->
