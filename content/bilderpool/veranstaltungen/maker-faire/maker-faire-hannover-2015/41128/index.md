---
layout: "image"
title: "makerfaire31.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire31.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41128
- /details9f3a-2.html
imported:
- "2019"
_4images_image_id: "41128"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41128 -->
