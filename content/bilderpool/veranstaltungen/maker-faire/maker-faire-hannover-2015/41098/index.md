---
layout: "image"
title: "makerfaire01.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41098
- /details44a8.html
imported:
- "2019"
_4images_image_id: "41098"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41098 -->
