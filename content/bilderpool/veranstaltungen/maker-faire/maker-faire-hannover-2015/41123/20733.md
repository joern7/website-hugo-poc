---
layout: "comment"
hidden: true
title: "20733"
date: "2015-06-09T21:22:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was für Stückzahlen... Aber sind die beiden schwarzen Träger nicht etwas arg ungesichert gegen Auslenkung nach (im Bild) links und rechts? Da ist so gar keine Schrägverstrebung nach außen hin dran?
Gruß,
Stefan