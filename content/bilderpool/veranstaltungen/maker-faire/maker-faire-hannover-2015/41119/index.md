---
layout: "image"
title: "makerfaire22.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41119
- /details6bc3.html
imported:
- "2019"
_4images_image_id: "41119"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41119 -->
