---
layout: "image"
title: "makerfaire20.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41117
- /details3d6b.html
imported:
- "2019"
_4images_image_id: "41117"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41117 -->
