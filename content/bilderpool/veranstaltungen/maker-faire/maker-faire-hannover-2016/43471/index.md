---
layout: "image"
title: "makerfaire017.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire017.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43471
- /detailscb11.html
imported:
- "2019"
_4images_image_id: "43471"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43471 -->
