---
layout: "image"
title: "makerfaire161.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire161.jpg"
weight: "158"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43615
- /details2cff-2.html
imported:
- "2019"
_4images_image_id: "43615"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "161"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43615 -->
