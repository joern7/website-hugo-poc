---
layout: "image"
title: "makerfaire053.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire053.jpg"
weight: "50"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43507
- /details35f2.html
imported:
- "2019"
_4images_image_id: "43507"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43507 -->
