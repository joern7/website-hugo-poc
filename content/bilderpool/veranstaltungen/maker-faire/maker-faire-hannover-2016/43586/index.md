---
layout: "image"
title: "makerfaire132.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire132.jpg"
weight: "129"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43586
- /details594b.html
imported:
- "2019"
_4images_image_id: "43586"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43586 -->
