---
layout: "image"
title: "makerfaire105.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire105.jpg"
weight: "102"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43559
- /details99ff.html
imported:
- "2019"
_4images_image_id: "43559"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43559 -->
