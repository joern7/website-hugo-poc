---
layout: "image"
title: "makerfaire002.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire002.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43456
- /detailsc656.html
imported:
- "2019"
_4images_image_id: "43456"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43456 -->
