---
layout: "image"
title: "makerfaire003.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire003.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43457
- /details0c7f.html
imported:
- "2019"
_4images_image_id: "43457"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43457 -->
