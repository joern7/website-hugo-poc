---
layout: "image"
title: "makerfaire183.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire183.jpg"
weight: "180"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43637
- /details69de.html
imported:
- "2019"
_4images_image_id: "43637"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "183"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43637 -->
