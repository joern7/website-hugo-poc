---
layout: "image"
title: "makerfaire098.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire098.jpg"
weight: "95"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43552
- /details7aad-2.html
imported:
- "2019"
_4images_image_id: "43552"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43552 -->
