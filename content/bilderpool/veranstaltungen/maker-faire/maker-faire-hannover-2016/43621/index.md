---
layout: "image"
title: "makerfaire167.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire167.jpg"
weight: "164"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43621
- /detailsfd57.html
imported:
- "2019"
_4images_image_id: "43621"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "167"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43621 -->
