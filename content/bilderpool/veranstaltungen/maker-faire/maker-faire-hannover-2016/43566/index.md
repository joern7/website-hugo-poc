---
layout: "image"
title: "makerfaire112.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire112.jpg"
weight: "109"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43566
- /detailsd9ea-2.html
imported:
- "2019"
_4images_image_id: "43566"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "112"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43566 -->
