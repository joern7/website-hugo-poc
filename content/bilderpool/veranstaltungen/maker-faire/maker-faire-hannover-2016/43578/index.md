---
layout: "image"
title: "makerfaire124.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire124.jpg"
weight: "121"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43578
- /details6285-4.html
imported:
- "2019"
_4images_image_id: "43578"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43578 -->
