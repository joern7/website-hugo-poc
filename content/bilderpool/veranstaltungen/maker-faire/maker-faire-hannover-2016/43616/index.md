---
layout: "image"
title: "makerfaire162.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire162.jpg"
weight: "159"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43616
- /details1e9f.html
imported:
- "2019"
_4images_image_id: "43616"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "162"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43616 -->
