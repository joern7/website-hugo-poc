---
layout: "image"
title: "makerfaire110.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire110.jpg"
weight: "107"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43564
- /details1df3-2.html
imported:
- "2019"
_4images_image_id: "43564"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "110"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43564 -->
