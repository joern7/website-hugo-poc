---
layout: "comment"
hidden: true
title: "4593"
date: "2007-11-14T13:16:05"
uploadBy:
- "sulu007"
license: "unknown"
imported:
- "2019"
---
Hallo,
nein so eine Welle wollte ich nicht, wollte nur darauf hinweisen das das mit dem Publischer doch kein Problem ist den Konstrukteur zu ändern und man halt dadurch ein wenig Mühe hat.
Ich versuche das immer ordentlich hinzubekommen, das dauert dann halt seine Zeit.
Nur Bilder hochladen kann man schließlich auch auf anderem Wege.

@Ludger
stimmt, neue Schaufel sieht doch prima aus, um Fehler entdecken zu können müsste das Bild aber heller sein. So wäre es keinem aufgefallen ;-).