---
layout: "image"
title: "Sägewerk"
date: "2007-11-10T15:28:05"
picture: "wilhelmbrickwedde01.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12578
- /detailsae96.html
imported:
- "2019"
_4images_image_id: "12578"
_4images_cat_id: "1134"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12578 -->
