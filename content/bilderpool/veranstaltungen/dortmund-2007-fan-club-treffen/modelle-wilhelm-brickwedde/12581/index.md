---
layout: "image"
title: "Sägewerk"
date: "2007-11-10T15:28:05"
picture: "wilhelmbrickwedde04.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12581
- /details0702.html
imported:
- "2019"
_4images_image_id: "12581"
_4images_cat_id: "1134"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12581 -->
