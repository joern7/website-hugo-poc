---
layout: "image"
title: "Robo Explorer"
date: "2007-11-10T15:10:47"
picture: "roboexplorer6.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12570
- /details837e.html
imported:
- "2019"
_4images_image_id: "12570"
_4images_cat_id: "1132"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:10:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12570 -->
