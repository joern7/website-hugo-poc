---
layout: "image"
title: "Tim, Tom & Co machen Pause"
date: "2017-09-07T13:37:32"
picture: "Convention_klein.jpg"
weight: "3"
konstrukteure: 
- "Christian und Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Convention", "Südconvention", "2017"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46238
- /details064e.html
imported:
- "2019"
_4images_image_id: "46238"
_4images_cat_id: "1301"
_4images_user_id: "2611"
_4images_image_date: "2017-09-07T13:37:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46238 -->
Beim Bau der Modelle für die Convention legen Tim, Tom & Co erst mal eine Pause ein.