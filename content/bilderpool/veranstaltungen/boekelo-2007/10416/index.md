---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:49:48"
picture: "boekelo07.jpg"
weight: "10"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10416
- /details590e.html
imported:
- "2019"
_4images_image_id: "10416"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10416 -->
fichertechnikclub event 2007 Boekelo