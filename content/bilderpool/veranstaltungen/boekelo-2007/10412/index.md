---
layout: "image"
title: "fam Tielemans"
date: "2007-05-15T14:49:48"
picture: "boekelo03.jpg"
weight: "6"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10412
- /details4601.html
imported:
- "2019"
_4images_image_id: "10412"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10412 -->
the first who arrived in Boekelo