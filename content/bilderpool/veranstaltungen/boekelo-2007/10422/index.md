---
layout: "image"
title: "Franc is helping the children in the TiP/fischertechnik workcorner"
date: "2007-05-15T14:50:01"
picture: "boekelo13.jpg"
weight: "16"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10422
- /details2085.html
imported:
- "2019"
_4images_image_id: "10422"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10422 -->
fichertechnikclub event 2007 Boekelo