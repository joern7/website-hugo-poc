---
layout: "image"
title: "Poster Boekelo2007 de"
date: "2007-05-06T21:37:12"
picture: "pooster-de_001.jpg"
weight: "1"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: ["clubdag", "fischeretchnikclub", "Boekelo", "event", "treffen"]
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10331
- /detailsf064.html
imported:
- "2019"
_4images_image_id: "10331"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-06T21:37:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10331 -->
