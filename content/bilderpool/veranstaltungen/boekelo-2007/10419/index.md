---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:49:48"
picture: "boekelo10.jpg"
weight: "13"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10419
- /detailsf8b1-2.html
imported:
- "2019"
_4images_image_id: "10419"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10419 -->
fichertechnikclub event 2007 Boekelo