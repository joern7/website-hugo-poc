---
layout: "overview"
title: "Boekelo 2007"
date: 2020-02-22T09:00:13+01:00
legacy_id:
- /php/categories/950
- /categories64d9.html
- /categories6d89.html
- /categories3d98.html
- /categoriesb2b9.html
- /categories970c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=950 --> 
Event fischertechnikclub Nederland on the 12th of May in Boekelo near Enschede Netherlands.

At the hall of Cafe de Buren



Organisation commity :

Carel van Leeuwen, Franc Pots, Alex Schelfhorst, Lucas Weel



With the help from

Harold Jaarsma (fischertechnik Nederland), Johan Lankheet

Family Jansen and  family Tielemans (manifestatiecommisie fischertechnikclub Nederland)

Rob van Baal en Dave Gabeler (edactie clubblad fischertechnikclub Nederland)

and other members.