---
layout: "image"
title: "Carel van Leeuwen"
date: "2007-05-15T14:49:48"
picture: "boekelo04.jpg"
weight: "7"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Eric Bernhard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10413
- /detailsbfca.html
imported:
- "2019"
_4images_image_id: "10413"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10413 -->
Organisation commity