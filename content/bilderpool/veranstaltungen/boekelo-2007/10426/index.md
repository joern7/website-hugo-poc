---
layout: "image"
title: "sawmill (C. Jansen)"
date: "2007-05-15T14:50:01"
picture: "boekelo17.jpg"
weight: "20"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10426
- /details5bbf.html
imported:
- "2019"
_4images_image_id: "10426"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10426 -->
fichertechnikclub event 2007 Boekelo