---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:01"
picture: "boekelo18.jpg"
weight: "21"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10427
- /detailsad68.html
imported:
- "2019"
_4images_image_id: "10427"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10427 -->
fichertechnikclub event 2007 Boekelo