---
layout: "image"
title: "Industy robot"
date: "2007-05-15T14:50:12"
picture: "boekelo21.jpg"
weight: "24"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10430
- /detailse9e3.html
imported:
- "2019"
_4images_image_id: "10430"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10430 -->
fischertechnik Nederland (Freetime)