---
layout: "image"
title: "fischertechnikschoonh51.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh51.jpg"
weight: "29"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7367
- /detailsffb1-2.html
imported:
- "2019"
_4images_image_id: "7367"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7367 -->
