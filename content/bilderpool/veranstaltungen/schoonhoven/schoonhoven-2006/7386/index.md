---
layout: "image"
title: "fischertechnikschoonh70.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh70.jpg"
weight: "38"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7386
- /details22cb.html
imported:
- "2019"
_4images_image_id: "7386"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7386 -->
