---
layout: "image"
title: "fischertechnikschoonh03.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh03.jpg"
weight: "3"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7319
- /detailsfda0.html
imported:
- "2019"
_4images_image_id: "7319"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7319 -->
