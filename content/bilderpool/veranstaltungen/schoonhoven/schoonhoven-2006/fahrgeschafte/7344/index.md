---
layout: "image"
title: "fischertechnikschoonh28.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh28.jpg"
weight: "11"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7344
- /detailsd9a4-2.html
imported:
- "2019"
_4images_image_id: "7344"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7344 -->
