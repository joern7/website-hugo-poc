---
layout: "image"
title: "fischertechnikschoonh74.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh74.jpg"
weight: "17"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7390
- /detailsb4c1-2.html
imported:
- "2019"
_4images_image_id: "7390"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7390 -->
