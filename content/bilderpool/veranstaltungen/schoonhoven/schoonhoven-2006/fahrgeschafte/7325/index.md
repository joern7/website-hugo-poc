---
layout: "image"
title: "fischertechnikschoonh09.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh09.jpg"
weight: "2"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7325
- /details0715.html
imported:
- "2019"
_4images_image_id: "7325"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7325 -->
