---
layout: "image"
title: "fischertechnikschoonh10.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh10.jpg"
weight: "3"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7326
- /detailsafba.html
imported:
- "2019"
_4images_image_id: "7326"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7326 -->
