---
layout: "image"
title: "fischertechnikschoonh81.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh81.jpg"
weight: "47"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7397
- /details6072-2.html
imported:
- "2019"
_4images_image_id: "7397"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7397 -->
