---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:52"
picture: "schoonhovenmw01.jpg"
weight: "49"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7411
- /details6f9e.html
imported:
- "2019"
_4images_image_id: "7411"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7411 -->
