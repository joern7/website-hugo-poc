---
layout: "image"
title: "fischertechnikschoonh85.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh85.jpg"
weight: "6"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7401
- /details1349-2.html
imported:
- "2019"
_4images_image_id: "7401"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7401 -->
