---
layout: "image"
title: "fischertechnikschoonh83.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh83.jpg"
weight: "4"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7399
- /details5b4e.html
imported:
- "2019"
_4images_image_id: "7399"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7399 -->
