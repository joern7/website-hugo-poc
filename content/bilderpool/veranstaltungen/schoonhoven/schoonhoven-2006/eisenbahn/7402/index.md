---
layout: "image"
title: "fischertechnikschoonh86.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh86.jpg"
weight: "7"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7402
- /detailsd654.html
imported:
- "2019"
_4images_image_id: "7402"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7402 -->
