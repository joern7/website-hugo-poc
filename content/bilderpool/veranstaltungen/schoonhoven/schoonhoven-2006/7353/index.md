---
layout: "image"
title: "fischertechnikschoonh37.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh37.jpg"
weight: "17"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7353
- /details4993.html
imported:
- "2019"
_4images_image_id: "7353"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7353 -->
