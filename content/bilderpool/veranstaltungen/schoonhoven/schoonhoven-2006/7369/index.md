---
layout: "image"
title: "fischertechnikschoonh53.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh53.jpg"
weight: "31"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7369
- /detailsf700.html
imported:
- "2019"
_4images_image_id: "7369"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7369 -->
