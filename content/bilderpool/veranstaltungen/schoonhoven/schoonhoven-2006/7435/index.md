---
layout: "image"
title: "ft-Luft"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw25.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7435
- /detailsd766-2.html
imported:
- "2019"
_4images_image_id: "7435"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7435 -->
