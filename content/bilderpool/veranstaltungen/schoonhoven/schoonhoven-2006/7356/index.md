---
layout: "image"
title: "fischertechnikschoonh40.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh40.jpg"
weight: "19"
konstrukteure: 
- "Fritz Roller"
fotografen:
- "Peter Damen"
keywords: ["Eigenbau", "Pneumatik"]
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7356
- /detailsa8a9.html
imported:
- "2019"
_4images_image_id: "7356"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7356 -->
