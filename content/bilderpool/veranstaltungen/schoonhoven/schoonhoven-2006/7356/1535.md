---
layout: "comment"
hidden: true
title: "1535"
date: "2006-11-06T20:36:40"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Festo hat auch ein "standard"-Alternativ: 

CN-M5-PK2 FES 19.521 (1 á 2 Euro/St) 

LCN-M5-PK2 FES 19.523 (2 á 3 Euro/St) 

FT-Baustein 15 mit Bohrung 32.064 

Gruss, 

Peter Damen 
Poederoyen, Holland