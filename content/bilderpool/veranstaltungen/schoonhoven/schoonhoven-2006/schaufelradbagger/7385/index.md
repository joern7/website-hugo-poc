---
layout: "image"
title: "fischertechnikschoonh69.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh69.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7385
- /detailsffa2.html
imported:
- "2019"
_4images_image_id: "7385"
_4images_cat_id: "1128"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7385 -->
