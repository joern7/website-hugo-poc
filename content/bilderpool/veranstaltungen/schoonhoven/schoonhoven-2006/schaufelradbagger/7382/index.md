---
layout: "image"
title: "fischertechnikschoonh66.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh66.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7382
- /details2706.html
imported:
- "2019"
_4images_image_id: "7382"
_4images_cat_id: "1128"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7382 -->
