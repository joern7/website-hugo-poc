---
layout: "image"
title: "fischertechnikschoonh42.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh42.jpg"
weight: "20"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7358
- /detailsc255.html
imported:
- "2019"
_4images_image_id: "7358"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7358 -->
