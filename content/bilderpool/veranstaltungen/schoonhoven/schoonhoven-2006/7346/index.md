---
layout: "image"
title: "fischertechnikschoonh30.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh30.jpg"
weight: "11"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7346
- /details2231.html
imported:
- "2019"
_4images_image_id: "7346"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7346 -->
