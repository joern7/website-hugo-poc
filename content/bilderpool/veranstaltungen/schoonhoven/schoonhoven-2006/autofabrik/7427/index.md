---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw17.jpg"
weight: "9"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7427
- /details0cf0.html
imported:
- "2019"
_4images_image_id: "7427"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7427 -->
8.Staion: Einbau des Sitzes