---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw11.jpg"
weight: "3"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7421
- /detailsff96-2.html
imported:
- "2019"
_4images_image_id: "7421"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7421 -->
2. Station: Motor wird eingebaut und befestigt