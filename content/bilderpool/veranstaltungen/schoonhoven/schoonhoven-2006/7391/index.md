---
layout: "image"
title: "fischertechnikschoonh75.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh75.jpg"
weight: "41"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7391
- /detailsd81d.html
imported:
- "2019"
_4images_image_id: "7391"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7391 -->
