---
layout: "image"
title: "Turmdrehkran"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw09.jpg"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7419
- /detailsee8f.html
imported:
- "2019"
_4images_image_id: "7419"
_4images_cat_id: "1124"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7419 -->
Eine Betonplatte als Gegengewicht