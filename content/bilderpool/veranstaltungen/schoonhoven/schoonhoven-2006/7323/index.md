---
layout: "image"
title: "fischertechnikschoonh07.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh07.jpg"
weight: "7"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7323
- /detailsb592.html
imported:
- "2019"
_4images_image_id: "7323"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7323 -->
