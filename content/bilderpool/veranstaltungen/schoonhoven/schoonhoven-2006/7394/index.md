---
layout: "image"
title: "fischertechnikschoonh78.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh78.jpg"
weight: "44"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7394
- /details9732.html
imported:
- "2019"
_4images_image_id: "7394"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7394 -->
