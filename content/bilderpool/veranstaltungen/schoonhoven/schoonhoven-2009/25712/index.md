---
layout: "image"
title: "Rob van Oostenbrugge"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25712
- /details4569.html
imported:
- "2019"
_4images_image_id: "25712"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25712 -->
FT-Heftruck   (Forklift)