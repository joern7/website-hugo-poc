---
layout: "image"
title: "Herman Mels"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven06.jpg"
weight: "6"
konstrukteure: 
- "Herman Mels"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25697
- /detailsd04a.html
imported:
- "2019"
_4images_image_id: "25697"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25697 -->
Bearbeitungsstation