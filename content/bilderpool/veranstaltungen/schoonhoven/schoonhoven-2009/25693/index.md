---
layout: "image"
title: "Herman Mels"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25693
- /details1f09.html
imported:
- "2019"
_4images_image_id: "25693"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25693 -->
Bearbeitungsstation