---
layout: "image"
title: "Imgp2289"
date: "2003-04-26T15:55:55"
picture: "Imgp2289.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
keywords: ["Marionette", "Handpuppe"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/663
- /detailsfc83.html
imported:
- "2019"
_4images_image_id: "663"
_4images_cat_id: "75"
_4images_user_id: "1"
_4images_image_date: "2003-04-26T15:55:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=663 -->
