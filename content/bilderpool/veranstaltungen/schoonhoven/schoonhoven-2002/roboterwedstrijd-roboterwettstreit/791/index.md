---
layout: "image"
title: "DCP 2480"
date: "2003-04-27T13:07:39"
picture: "DCP_2480.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/791
- /details9b19.html
imported:
- "2019"
_4images_image_id: "791"
_4images_cat_id: "85"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T13:07:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=791 -->
