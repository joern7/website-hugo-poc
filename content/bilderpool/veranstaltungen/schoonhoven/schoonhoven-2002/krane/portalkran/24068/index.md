---
layout: "image"
title: "Portalkran"
date: "2009-05-21T22:29:48"
picture: "Krane_1_006.jpg"
weight: "18"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24068
- /details3915.html
imported:
- "2019"
_4images_image_id: "24068"
_4images_cat_id: "84"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24068 -->
