---
layout: "image"
title: "Portalkran"
date: "2009-05-21T22:29:48"
picture: "Krane_1_007_2.jpg"
weight: "20"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24070
- /details83f1-2.html
imported:
- "2019"
_4images_image_id: "24070"
_4images_cat_id: "84"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24070 -->
View of the left side,showing the carriage
pulling  chain around the sproket wheel.

Blick auf der linken Seite, zeigt der Beförderung
ziehen um das Sproket-Rad-Kette.