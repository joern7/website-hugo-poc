---
layout: "image"
title: "PortalKran"
date: "2009-05-21T22:29:48"
picture: "Krane_1_008.jpg"
weight: "21"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24071
- /detailsf52b-2.html
imported:
- "2019"
_4images_image_id: "24071"
_4images_cat_id: "84"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24071 -->
View of the bottom of the kran,showing
the power and transmission to move the
 whole krane back and forth.

Ansicht des unteren Rand der Kran, anzeigen
die Macht und Übertragung zum Verschieben der
ganze Krane hin-und her.