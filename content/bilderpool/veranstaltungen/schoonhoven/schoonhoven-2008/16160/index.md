---
layout: "image"
title: "Marcel Bosch   Treinen"
date: "2008-11-01T22:33:45"
picture: "FT-Schoonhoven-2008_082.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16160
- /details914a.html
imported:
- "2019"
_4images_image_id: "16160"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16160 -->
