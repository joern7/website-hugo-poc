---
layout: "image"
title: "Peter Krijnen  Nelcon-Containerkraan"
date: "2008-11-01T22:33:44"
picture: "FT-Schoonhoven-2008_048.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16136
- /detailsd3e9.html
imported:
- "2019"
_4images_image_id: "16136"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16136 -->
