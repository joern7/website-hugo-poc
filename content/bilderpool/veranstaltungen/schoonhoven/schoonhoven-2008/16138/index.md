---
layout: "image"
title: "Peter Krijnen  Nelcon-Containerkraan"
date: "2008-11-01T22:33:44"
picture: "FT-Schoonhoven-2008_051.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16138
- /details913c.html
imported:
- "2019"
_4images_image_id: "16138"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16138 -->
