---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:07"
picture: "fischertechnikbijeenkomstschoonhovennov70.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29176
- /details3741-2.html
imported:
- "2019"
_4images_image_id: "29176"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:07"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29176 -->
FT-Treffen-Schoonhoven-Thema