---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:07"
picture: "fischertechnikbijeenkomstschoonhovennov68.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29174
- /details894e.html
imported:
- "2019"
_4images_image_id: "29174"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:07"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29174 -->
FT-Treffen-Schoonhoven-Thema