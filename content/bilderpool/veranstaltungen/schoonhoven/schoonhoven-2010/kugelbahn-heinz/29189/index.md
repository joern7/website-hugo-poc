---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:23"
picture: "fischertechnikbijeenkomstschoonhovennov83.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29189
- /details28ac.html
imported:
- "2019"
_4images_image_id: "29189"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:23"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29189 -->
FT-Treffen-Schoonhoven-Thema