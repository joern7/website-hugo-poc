---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov09.jpg"
weight: "9"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29115
- /details81ba.html
imported:
- "2019"
_4images_image_id: "29115"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29115 -->
Peter Krijnen + Anton Janssen