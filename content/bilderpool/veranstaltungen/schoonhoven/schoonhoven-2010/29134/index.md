---
layout: "image"
title: "Rob Tovenaar"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov28.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29134
- /detailsc922-2.html
imported:
- "2019"
_4images_image_id: "29134"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29134 -->
Rob Tovenaar