---
layout: "image"
title: "Rob Tovenaar"
date: "2010-11-06T23:39:27"
picture: "fischertechnikbijeenkomstschoonhovennov31.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29137
- /detailsba19.html
imported:
- "2019"
_4images_image_id: "29137"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:27"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29137 -->
Rob Tovenaar