---
layout: "image"
title: "Grijper-Robots"
date: "2010-11-06T23:40:07"
picture: "fischertechnikbijeenkomstschoonhovennov65.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29171
- /details0087.html
imported:
- "2019"
_4images_image_id: "29171"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:07"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29171 -->
Grijper-Robots