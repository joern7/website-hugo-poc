---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:27"
picture: "fischertechnikbijeenkomstschoonhovennov34.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29140
- /details2365.html
imported:
- "2019"
_4images_image_id: "29140"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:27"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29140 -->
Kubus Rob Tovenaar