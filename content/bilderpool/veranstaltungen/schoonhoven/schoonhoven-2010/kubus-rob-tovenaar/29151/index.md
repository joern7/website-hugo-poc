---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:45"
picture: "fischertechnikbijeenkomstschoonhovennov45.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29151
- /detailsb780.html
imported:
- "2019"
_4images_image_id: "29151"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:45"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29151 -->
Kubus Rob Tovenaar