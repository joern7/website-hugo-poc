---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:45"
picture: "fischertechnikbijeenkomstschoonhovennov48.jpg"
weight: "17"
konstrukteure: 
- "Rob Tovenaar"
fotografen:
- "peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29154
- /details4e77-2.html
imported:
- "2019"
_4images_image_id: "29154"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:45"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29154 -->
Kubus Rob Tovenaar