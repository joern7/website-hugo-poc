---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:28"
picture: "fischertechnikbijeenkomstschoonhovennov36.jpg"
weight: "5"
konstrukteure: 
- "Rob Tovenaar"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29142
- /detailsc4f9.html
imported:
- "2019"
_4images_image_id: "29142"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:28"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29142 -->
Kubus Rob Tovenaar