---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29112
- /details9bea.html
imported:
- "2019"
_4images_image_id: "29112"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29112 -->
Peter Krijnen + Anton Janssen