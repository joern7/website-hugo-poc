---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:39:09"
picture: "fischertechnikbijeenkomstschoonhovennov14.jpg"
weight: "14"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29120
- /details3c56.html
imported:
- "2019"
_4images_image_id: "29120"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:09"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29120 -->
Peter Krijnen + Anton Janssen