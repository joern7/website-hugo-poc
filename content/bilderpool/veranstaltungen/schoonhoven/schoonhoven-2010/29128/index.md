---
layout: "image"
title: "Herman Mels"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29128
- /detailsf19f.html
imported:
- "2019"
_4images_image_id: "29128"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29128 -->
Herman Mels