---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:39:09"
picture: "fischertechnikbijeenkomstschoonhovennov15.jpg"
weight: "15"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29121
- /details0aa5.html
imported:
- "2019"
_4images_image_id: "29121"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:09"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29121 -->
Peter Krijnen + Anton Janssen