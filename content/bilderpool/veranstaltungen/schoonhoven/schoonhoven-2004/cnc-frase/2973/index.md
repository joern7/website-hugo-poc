---
layout: "image"
title: "DSC01048"
date: "2004-11-08T20:48:48"
picture: "DSC01048.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/2973
- /details5757-2.html
imported:
- "2019"
_4images_image_id: "2973"
_4images_cat_id: "303"
_4images_user_id: "9"
_4images_image_date: "2004-11-08T20:48:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2973 -->
