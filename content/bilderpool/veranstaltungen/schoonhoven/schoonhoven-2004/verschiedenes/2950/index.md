---
layout: "image"
title: "DSC01012"
date: "2004-11-08T20:48:48"
picture: "DSC01012.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
keywords: ["Uhr"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/2950
- /detailsa66e.html
imported:
- "2019"
_4images_image_id: "2950"
_4images_cat_id: "309"
_4images_user_id: "9"
_4images_image_date: "2004-11-08T20:48:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2950 -->
