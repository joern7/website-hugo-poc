---
layout: "image"
title: "IM000828"
date: "2003-04-22T17:43:28"
picture: "IM000828.jpg"
weight: "1"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/486
- /details9d97.html
imported:
- "2019"
_4images_image_id: "486"
_4images_cat_id: "302"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T17:43:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=486 -->
