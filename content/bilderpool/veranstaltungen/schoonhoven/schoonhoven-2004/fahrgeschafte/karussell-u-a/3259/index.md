---
layout: "image"
title: "frank - 38"
date: "2004-11-18T17:20:27"
picture: "frank - 38.jpg"
weight: "4"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/3259
- /detailscc51-2.html
imported:
- "2019"
_4images_image_id: "3259"
_4images_cat_id: "306"
_4images_user_id: "9"
_4images_image_date: "2004-11-18T17:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3259 -->
