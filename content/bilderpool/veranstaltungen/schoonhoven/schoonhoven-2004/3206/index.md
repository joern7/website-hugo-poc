---
layout: "image"
title: "photo61"
date: "2004-11-16T09:16:34"
picture: "photo61.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen (?)"
keywords: ["Schnecke", "35977"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/3206
- /details3e83.html
imported:
- "2019"
_4images_image_id: "3206"
_4images_cat_id: "277"
_4images_user_id: "1"
_4images_image_date: "2004-11-16T09:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3206 -->
