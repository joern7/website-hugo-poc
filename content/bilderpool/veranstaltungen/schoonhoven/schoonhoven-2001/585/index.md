---
layout: "image"
title: "IM000928"
date: "2003-04-22T17:43:29"
picture: "IM000928.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
keywords: ["Brettspiel", "Solitaire"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/585
- /details94b2.html
imported:
- "2019"
_4images_image_id: "585"
_4images_cat_id: "5"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T17:43:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=585 -->
