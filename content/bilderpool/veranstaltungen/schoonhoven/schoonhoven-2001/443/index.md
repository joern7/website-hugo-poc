---
layout: "image"
title: "IM000785"
date: "2003-04-22T17:43:28"
picture: "IM000785.jpg"
weight: "19"
konstrukteure: 
- "fischertechnik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/443
- /detailsd9f5.html
imported:
- "2019"
_4images_image_id: "443"
_4images_cat_id: "5"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T17:43:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=443 -->
