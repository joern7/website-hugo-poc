---
layout: "image"
title: "HopperDredge07.JPG"
date: "2005-11-10T22:11:11"
picture: "HopperDredge07.JPG"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5285
- /details0406.html
imported:
- "2019"
_4images_image_id: "5285"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:11:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5285 -->
Ein Blick unter den Schiffsboden. Die Kamera musste so schräg gehalten werden, damit Blitz und Objektiv zueinander fanden.