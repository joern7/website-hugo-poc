---
layout: "image"
title: "Hopper-kran mit Drehscheibe-Potmeter-Positionierung"
date: "2005-11-06T11:02:55"
picture: "Schoonhoven-2005_053.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Andries Tieleman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5224
- /details3bbf.html
imported:
- "2019"
_4images_image_id: "5224"
_4images_cat_id: "453"
_4images_user_id: "22"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5224 -->
Hopper-kran mit Robo-pro-Drehscheibe-Potmeter-Positionierung