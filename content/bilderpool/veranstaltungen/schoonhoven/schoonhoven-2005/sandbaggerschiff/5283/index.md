---
layout: "image"
title: "HopperDredge05.JPG"
date: "2005-11-10T22:07:57"
picture: "HopperDredge05.JPG"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5283
- /detailsd4d7.html
imported:
- "2019"
_4images_image_id: "5283"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:07:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5283 -->
Die schwenkbaren Antriebe (Schottel-Antriebe?)