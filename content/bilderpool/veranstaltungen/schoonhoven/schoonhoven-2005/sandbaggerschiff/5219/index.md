---
layout: "image"
title: "Hopper"
date: "2005-11-06T11:02:55"
picture: "Schoonhoven-2005_007_2.jpg"
weight: "2"
konstrukteure: 
- "Andries Tieleman"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5219
- /details6019.html
imported:
- "2019"
_4images_image_id: "5219"
_4images_cat_id: "453"
_4images_user_id: "22"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5219 -->
