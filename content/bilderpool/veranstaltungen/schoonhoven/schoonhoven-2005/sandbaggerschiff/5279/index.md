---
layout: "image"
title: "HopperDredge01.JPG"
date: "2005-11-10T21:59:58"
picture: "HopperDredge01.JPG"
weight: "8"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Harald Steinhaus"
keywords: ["Hopper", "Dredge", "Sandbaggerschiff", "Baggerschiff"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5279
- /details0a8f.html
imported:
- "2019"
_4images_image_id: "5279"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T21:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5279 -->
Ein Schiff, das Sand von einer Stelle im Flachwasser aufsaugt und an anderer Stelle wieder anspült oder einfach durch die Bodenluke auskippt. Mit so etwas wird z.B. in Singapur Land gewonnen (Doku-Sendung auf N24: "Vasco da Gama") oder es werden Flughäfen ins Meer hinaus gebaut.

Die Schläuche sind aus dem Aquarien-Zubehörhandel.