---
layout: "image"
title: "Hopper"
date: "2005-11-06T11:02:55"
picture: "Schoonhoven-2005_018.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Andries Tieleman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5222
- /detailsae76-2.html
imported:
- "2019"
_4images_image_id: "5222"
_4images_cat_id: "453"
_4images_user_id: "22"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5222 -->
