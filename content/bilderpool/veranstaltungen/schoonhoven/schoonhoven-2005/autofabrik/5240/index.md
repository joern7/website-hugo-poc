---
layout: "image"
title: "Autofabrik03.JPG"
date: "2005-11-06T20:53:06"
picture: "Autofabrik03.JPG"
weight: "3"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5240
- /details668e.html
imported:
- "2019"
_4images_image_id: "5240"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T20:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5240 -->
Eine Laser-Lichtschranke in Aktion (Bildmitte). Links davon wird die Codierung der Palette ausgelesen; das passiert mit drei roten LED-Lichtschranken.