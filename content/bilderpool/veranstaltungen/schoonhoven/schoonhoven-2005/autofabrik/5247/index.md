---
layout: "image"
title: "Autofabrik10.JPG"
date: "2005-11-06T21:04:24"
picture: "Autofabrik10.JPG"
weight: "10"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5247
- /details8f43.html
imported:
- "2019"
_4images_image_id: "5247"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:04:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5247 -->
