---
layout: "image"
title: "Waage123.JPG"
date: "2005-11-28T19:13:36"
picture: "Waage123.JPG"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5422
- /detailsd3ec.html
imported:
- "2019"
_4images_image_id: "5422"
_4images_cat_id: "436"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T19:13:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5422 -->
Beim Auflegen (oder Wegnehmen) eines Gewichts tariert sich die Waage automatisch. Gesteuert wird das durch Federkontakte, die sich am Fuß des vertikal hängenden Arms befinden und den M-Motor (links oben) ansteuern, wenn der Arm aus der lotrechten Lage kommt. Der M-Motor verschiebt den Gewichtsblock und damit auch den Skalenzeiger, bis alles im Lot ist.