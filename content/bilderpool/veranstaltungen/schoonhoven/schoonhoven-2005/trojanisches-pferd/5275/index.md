---
layout: "image"
title: "Troja04.JPG"
date: "2005-11-07T20:17:37"
picture: "Troja04.JPG"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5275
- /details5b8b.html
imported:
- "2019"
_4images_image_id: "5275"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:17:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5275 -->
