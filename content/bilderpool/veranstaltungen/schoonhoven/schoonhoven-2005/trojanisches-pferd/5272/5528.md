---
layout: "comment"
hidden: true
title: "5528"
date: "2008-03-11T19:33:58"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Nur um Missverständnisse zu vermeiden: das Pferd hier und den ganzen Rest habe ich nur als Besucher des Treffens bewundert und fotografiert. Das Lob gebührt der Entwicklerin (ich meine, so wär's gewesen), deren Namen ich leider nicht festgehalten habe.

Gruß,
Harald