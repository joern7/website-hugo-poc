---
layout: "image"
title: "Capriolo54.JPG"
date: "2005-11-06T19:33:22"
picture: "Capriolo54.JPG"
weight: "3"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5236
- /detailsc750.html
imported:
- "2019"
_4images_image_id: "5236"
_4images_cat_id: "439"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T19:33:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5236 -->
Der Capriolo von Jan-Willem Dekker, noch nicht ganz fertig. Deswegen ist auch der Monteur noch auf dem Gondelträger zu sehen.