---
layout: "image"
title: "Schoonhoven 07"
date: "2005-11-06T19:14:54"
picture: "Schoon_07.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5232
- /detailsfae4.html
imported:
- "2019"
_4images_image_id: "5232"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T19:14:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5232 -->
