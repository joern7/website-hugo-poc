---
layout: "image"
title: "Schoonhoven 03"
date: "2005-11-06T11:02:55"
picture: "Schoon_03.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5226
- /details98d2.html
imported:
- "2019"
_4images_image_id: "5226"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5226 -->
