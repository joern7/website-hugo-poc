---
layout: "image"
title: "Photo interruptor with cables"
date: "2005-11-25T11:59:22"
picture: "DSCF0035a.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5402
- /details07be-2.html
imported:
- "2019"
_4images_image_id: "5402"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T11:59:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5402 -->
Two resistors were added here: one for the transmitting LED, and one for the receiving transistor. This makes it very flexible. Here it is used with an Atmel ATmega8 input, but with different values it can be used for different interfaces and different voltages.  For the RoboInterface, only the resistor on the transmitting side is required.