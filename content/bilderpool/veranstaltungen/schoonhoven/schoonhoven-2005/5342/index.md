---
layout: "image"
title: "Mobile Robot  TNO 2005"
date: "2005-11-15T19:15:15"
picture: "RobotTNO.jpg"
weight: "13"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5342
- /details7c01-2.html
imported:
- "2019"
_4images_image_id: "5342"
_4images_cat_id: "436"
_4images_user_id: "385"
_4images_image_date: "2005-11-15T19:15:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5342 -->
Mobile robot constructed for the TNO robot competition 2005. It features 4 distance sensors, bumper switches, and does odometry on the wheels. The camera module on top is still not functional :-( We ranked 6th out of 13 :-).