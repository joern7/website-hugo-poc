---
layout: "image"
title: "Schoonhoven 01"
date: "2005-11-06T11:02:55"
picture: "Schoon_01.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5225
- /detailsa596.html
imported:
- "2019"
_4images_image_id: "5225"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5225 -->
