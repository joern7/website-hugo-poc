---
layout: "image"
title: "OelBohr03.JPG"
date: "2003-11-10T21:00:25"
picture: "OelBohr03.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1934
- /detailsf4d5.html
imported:
- "2019"
_4images_image_id: "1934"
_4images_cat_id: "202"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1934 -->
