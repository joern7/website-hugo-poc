---
layout: "image"
title: "Prop02.JPG"
date: "2003-11-10T21:00:41"
picture: "Prop02.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1940
- /details028e.html
imported:
- "2019"
_4images_image_id: "1940"
_4images_cat_id: "202"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1940 -->
