---
layout: "image"
title: "Starlift07.JPG"
date: "2003-11-11T20:17:16"
picture: "Starlift07_2.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1989
- /detailsf107-3.html
imported:
- "2019"
_4images_image_id: "1989"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:17:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1989 -->
Die Gondel kurz vor der untersten Position. Die Haltebügel werden beim vollen Absenken automatisch geöffnet (siehe nächstes Bild), dazu dienen die schwarzen Lochstreben, die in Bild 6 nach unten raushängend sichtbar sind.