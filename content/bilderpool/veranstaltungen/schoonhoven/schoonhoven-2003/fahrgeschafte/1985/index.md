---
layout: "image"
title: "Starlift02.JPG"
date: "2003-11-11T20:15:27"
picture: "Starlift02.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1985
- /details90d0.html
imported:
- "2019"
_4images_image_id: "1985"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1985 -->
Der Antrieb erfolgt über zwei identisch aufgebaute Stränge (Powermot, Differenzial, Kupplung, Seilwinde), die über die Z30 auf der rechten Seite gekoppelt sind. Die Seile heben die linke bzw. rechte Seite der Gondel.