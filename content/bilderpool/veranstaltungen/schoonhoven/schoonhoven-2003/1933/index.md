---
layout: "image"
title: "OelBohr02.JPG"
date: "2003-11-10T21:00:24"
picture: "OelBohr02.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1933
- /details0f81.html
imported:
- "2019"
_4images_image_id: "1933"
_4images_cat_id: "202"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1933 -->
