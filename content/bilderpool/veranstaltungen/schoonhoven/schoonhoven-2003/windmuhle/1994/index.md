---
layout: "image"
title: "WindM03.JPG"
date: "2003-11-11T20:17:16"
picture: "WindM03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1994
- /detailseda7.html
imported:
- "2019"
_4images_image_id: "1994"
_4images_cat_id: "409"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:17:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1994 -->
