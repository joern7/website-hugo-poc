---
layout: "image"
title: "S-Bagg07.JPG"
date: "2003-11-10T21:10:18"
picture: "S-Bagg07.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1948
- /details7cef.html
imported:
- "2019"
_4images_image_id: "1948"
_4images_cat_id: "413"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:10:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1948 -->
Der Lagerzapfen liegt hinten im Bild, vorne ist die Gleitführung für den Lenkhebel.