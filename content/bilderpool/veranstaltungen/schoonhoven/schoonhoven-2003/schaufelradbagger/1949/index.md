---
layout: "image"
title: "S-Bagg08.JPG"
date: "2003-11-10T21:10:18"
picture: "S-Bagg08.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1949
- /detailsa91e-2.html
imported:
- "2019"
_4images_image_id: "1949"
_4images_cat_id: "413"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:10:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1949 -->
Das Förderband, das zum Mitläufer führt. Das runde Teil wird sonst in Dachrinnen verwendet.