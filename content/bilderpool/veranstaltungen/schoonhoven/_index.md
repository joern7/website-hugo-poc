---
layout: "overview"
title: "Schoonhoven"
date: 2020-02-22T08:56:40+01:00
legacy_id:
- /php/categories/4
- /categories040b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=4 --> 
im holländischen Schoonhoven wird seit über 10 Jahren ein Treffen von Technikmodellbauern abgehalten.

Die Modelle sind dort so zahlreich wie verblüffend, [a href="http://utopia.knoware.nl/users/cdeweerd/n18.html"]der FCNL[/a] gibt weitere Informationen.