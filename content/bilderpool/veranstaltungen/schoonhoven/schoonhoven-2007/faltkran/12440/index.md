---
layout: "image"
title: "Faltkran 1"
date: "2007-11-04T20:05:56"
picture: "faltkran1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12440
- /details486e.html
imported:
- "2019"
_4images_image_id: "12440"
_4images_cat_id: "1111"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:05:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12440 -->
