---
layout: "image"
title: "3"
date: "2007-11-25T15:18:05"
picture: "autofabrikvonralph3.jpg"
weight: "3"
konstrukteure: 
- "Ralph Roetman"
fotografen:
- "Ralph Roetman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ralph_ft"
license: "unknown"
legacy_id:
- /php/details/12831
- /details6ad3.html
imported:
- "2019"
_4images_image_id: "12831"
_4images_cat_id: "1157"
_4images_user_id: "575"
_4images_image_date: "2007-11-25T15:18:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12831 -->
