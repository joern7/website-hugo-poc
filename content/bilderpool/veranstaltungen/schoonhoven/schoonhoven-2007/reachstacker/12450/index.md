---
layout: "image"
title: "Rad"
date: "2007-11-04T20:22:26"
picture: "reachstacker3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12450
- /detailsba9c.html
imported:
- "2019"
_4images_image_id: "12450"
_4images_cat_id: "1115"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12450 -->
Das Rad stammt aus dem Baumarkt. Offenbar für Rollwagen und -schränke gedacht, ich hätte es eher den Einkaufs-Karren für Senioren zugeordnet. Preis um 3,50 Euro.