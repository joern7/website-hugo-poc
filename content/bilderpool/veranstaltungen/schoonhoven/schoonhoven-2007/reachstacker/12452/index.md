---
layout: "image"
title: "Greifer"
date: "2007-11-04T20:22:26"
picture: "reachstacker5.jpg"
weight: "5"
konstrukteure: 
- "jmn"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12452
- /detailsaef1-2.html
imported:
- "2019"
_4images_image_id: "12452"
_4images_cat_id: "1115"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12452 -->
Das Original gehört zur Firma Kalmar. Dort ist der Greifer drehbar, der Reachstacker kann also auch schräg zum Containerstapel anfahren.