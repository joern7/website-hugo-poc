---
layout: "image"
title: "Seerose 1"
date: "2007-11-04T20:23:27"
picture: "verschiedenes04.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12465
- /detailsab64-2.html
imported:
- "2019"
_4images_image_id: "12465"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12465 -->
