---
layout: "image"
title: "Sortier- und Lageranlage Detail"
date: "2007-11-04T20:23:27"
picture: "verschiedenes08.jpg"
weight: "9"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12469
- /details3b9f-2.html
imported:
- "2019"
_4images_image_id: "12469"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12469 -->
