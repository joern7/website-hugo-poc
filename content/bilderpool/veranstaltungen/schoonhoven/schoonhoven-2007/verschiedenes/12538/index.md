---
layout: "image"
title: "Band045.JPG"
date: "2007-11-08T19:15:01"
picture: "Band045.JPG"
weight: "14"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12538
- /details7330-3.html
imported:
- "2019"
_4images_image_id: "12538"
_4images_cat_id: "1117"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T19:15:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12538 -->
