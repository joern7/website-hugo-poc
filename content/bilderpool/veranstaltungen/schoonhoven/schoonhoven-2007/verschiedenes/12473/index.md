---
layout: "image"
title: "SPS"
date: "2007-11-04T20:23:27"
picture: "verschiedenes12.jpg"
weight: "13"
konstrukteure: 
- "Herr Brickwedde"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12473
- /detailsb012-2.html
imported:
- "2019"
_4images_image_id: "12473"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12473 -->
Das ist die Steuerung des schnellen Trainingsroboters der Brickweddes. Der ist im Video zu sehen.