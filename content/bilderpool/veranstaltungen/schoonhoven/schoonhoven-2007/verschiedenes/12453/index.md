---
layout: "image"
title: "Tadano GR-300E Teleskop 2"
date: "2007-11-04T20:22:26"
picture: "reachstacker6.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12453
- /details9d1a.html
imported:
- "2019"
_4images_image_id: "12453"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12453 -->
(hoppla, ist das etwa schon das nächste Modell?)