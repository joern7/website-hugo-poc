---
layout: "image"
title: "Gitterspitze, Lagerung"
date: "2007-11-04T19:45:04"
picture: "raupenkran18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12410
- /details7fe4.html
imported:
- "2019"
_4images_image_id: "12410"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12410 -->
