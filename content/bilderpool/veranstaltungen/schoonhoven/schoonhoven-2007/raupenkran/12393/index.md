---
layout: "image"
title: "Raupenkran"
date: "2007-11-04T19:45:04"
picture: "raupenkran01.jpg"
weight: "1"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12393
- /detailsaf4b.html
imported:
- "2019"
_4images_image_id: "12393"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12393 -->
