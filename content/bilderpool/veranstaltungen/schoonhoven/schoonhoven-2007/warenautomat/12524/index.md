---
layout: "image"
title: "Warenautomat06.JPG"
date: "2007-11-05T21:56:36"
picture: "Warenautomat06.JPG"
weight: "6"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12524
- /details7245-2.html
imported:
- "2019"
_4images_image_id: "12524"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:56:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12524 -->
Das ganze ist professionell geplant und entwickelt worden. Die Zeichnungsmappe liegt auf der heruntergeklappten Rückwand.