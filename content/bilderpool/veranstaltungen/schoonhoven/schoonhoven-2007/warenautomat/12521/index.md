---
layout: "image"
title: "Warenautomat03.JPG"
date: "2007-11-05T21:49:27"
picture: "Warenautomat03.JPG"
weight: "3"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12521
- /detailsf9ea-2.html
imported:
- "2019"
_4images_image_id: "12521"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:49:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12521 -->
Das ist der Münzprüfer. Geprüft werden (wahrscheinlich) Größe, Gewicht und magnetische Eigenschaften. Rechts am Rand befindet sich die Stromversorgung für den ganzen Automaten.