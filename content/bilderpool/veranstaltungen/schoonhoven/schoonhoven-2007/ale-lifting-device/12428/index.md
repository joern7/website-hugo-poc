---
layout: "image"
title: "Moved by Mammoet"
date: "2007-11-04T20:02:18"
picture: "aleliftingdevice14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12428
- /detailsbd09.html
imported:
- "2019"
_4images_image_id: "12428"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12428 -->
