---
layout: "image"
title: "Winden, Derrick-Ausleger"
date: "2007-11-04T20:02:17"
picture: "aleliftingdevice03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12417
- /details6646.html
imported:
- "2019"
_4images_image_id: "12417"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12417 -->
