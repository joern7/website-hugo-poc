---
layout: "image"
title: "Starlifter 2"
date: "2003-04-22T16:40:07"
picture: "Starlifter 2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/362
- /details71b4-2.html
imported:
- "2019"
_4images_image_id: "362"
_4images_cat_id: "47"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=362 -->
