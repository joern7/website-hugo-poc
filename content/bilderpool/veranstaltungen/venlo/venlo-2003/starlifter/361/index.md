---
layout: "image"
title: "Starlifter 1"
date: "2003-04-22T16:40:07"
picture: "Starlifter 1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "n/a"
keywords: ["Fallturm", "Fahrgeschäft", "Kirmes"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/361
- /detailscc23-2.html
imported:
- "2019"
_4images_image_id: "361"
_4images_cat_id: "47"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=361 -->
