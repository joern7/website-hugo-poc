---
layout: "image"
title: "Mobiler Roboter 5"
date: "2003-04-22T16:40:07"
picture: "Mobiler Roboter 5.jpg"
weight: "5"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/341
- /detailsca1d.html
imported:
- "2019"
_4images_image_id: "341"
_4images_cat_id: "43"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=341 -->
