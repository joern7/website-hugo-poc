---
layout: "image"
title: "Bagger 1"
date: "2003-04-22T16:40:07"
picture: "Bagger 1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/317
- /detailsdf62.html
imported:
- "2019"
_4images_image_id: "317"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=317 -->
