---
layout: "image"
title: "Bionic Robot"
date: "2003-04-22T16:40:07"
picture: "Bionic Robot.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/319
- /detailsf57f.html
imported:
- "2019"
_4images_image_id: "319"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=319 -->
