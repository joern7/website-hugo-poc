---
layout: "image"
title: "Kran"
date: "2007-03-14T20:14:56"
picture: "162_6279.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9522
- /detailsfa1b.html
imported:
- "2019"
_4images_image_id: "9522"
_4images_cat_id: "855"
_4images_user_id: "34"
_4images_image_date: "2007-03-14T20:14:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9522 -->
