---
layout: "image"
title: "FT-Treffen Vlissingen-2008"
date: "2008-02-24T14:33:02"
picture: "Fischertechnik-Vlissingen-2008_002.jpg"
weight: "10"
konstrukteure: 
- "Fam. van Baal"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13742
- /details3708.html
imported:
- "2019"
_4images_image_id: "13742"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T14:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13742 -->
