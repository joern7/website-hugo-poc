---
layout: "image"
title: "H.A.R.R.Y.s Schmuddelecke"
date: "2018-06-19T08:26:44"
picture: "mintka12.jpg"
weight: "13"
konstrukteure: 
- "Sisyphos + H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47709
- /details1fb3.html
imported:
- "2019"
_4images_image_id: "47709"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47709 -->
Was zeigt man passend für ein MINT Feriencamp?
Qgelbahn (https://www.youtube.com/watch?v=M0GUgUuPjos) - und wie werden die Bälle nach Farbe sortiert?
Balkenwaage (https://www.ftcommunity.de/categories.php?cat_id=3502, https://forum.ftcommunity.de/viewtopic.php?f=4&t=4757)
Gummibandauto (https://www.ftcommunity.de/details.php?image_id=47467)
Das Ding mit dem Dreh (eins weiter)
KEM1 (https://www.ftcommunity.de/details.php?image_id=47687)
und ein Jojo (https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-1.pdf)