---
layout: "image"
title: "Das Ding mit dem Dreh"
date: "2018-06-19T08:26:44"
picture: "mintka13.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47710
- /details861d-2.html
imported:
- "2019"
_4images_image_id: "47710"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47710 -->
Spielerei an Differentialgetriebe. Wer am geschicktesten kurbelt, gewinnt. Die Kugel wird per Magnet und Kette bewegt. Einzige Besonderheit: Der Antrieb in der Mitte der Kette (naja, etwas links davon).