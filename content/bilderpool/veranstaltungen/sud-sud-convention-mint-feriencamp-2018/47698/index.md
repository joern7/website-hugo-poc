---
layout: "image"
title: "Von Kugeln und von Bahnen ..."
date: "2018-06-19T08:26:44"
picture: "mintka01.jpg"
weight: "2"
konstrukteure: 
- "ThomasW"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47698
- /details4269.html
imported:
- "2019"
_4images_image_id: "47698"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47698 -->
Eine Hälfte der großen Kugelbahn mit vielerlei Mechanismen.