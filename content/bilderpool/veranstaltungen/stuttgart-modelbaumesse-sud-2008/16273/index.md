---
layout: "image"
title: "ft Messestand"
date: "2008-11-14T21:31:57"
picture: "modellsuedbaubahn3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/16273
- /detailsaf2d.html
imported:
- "2019"
_4images_image_id: "16273"
_4images_cat_id: "1470"
_4images_user_id: "409"
_4images_image_date: "2008-11-14T21:31:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16273 -->
Messe Modell Süd Bau & Bahn