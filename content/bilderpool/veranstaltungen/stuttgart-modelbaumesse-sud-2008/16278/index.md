---
layout: "image"
title: "Boats Übungsbecken ?"
date: "2008-11-14T21:31:58"
picture: "modellsuedbaubahn8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/16278
- /details8011.html
imported:
- "2019"
_4images_image_id: "16278"
_4images_cat_id: "1470"
_4images_user_id: "409"
_4images_image_date: "2008-11-14T21:31:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16278 -->
Das müsste als Übungsbecken reichen, um Boote zu testen.