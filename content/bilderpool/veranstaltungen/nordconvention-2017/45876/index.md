---
layout: "image"
title: "Ballweitergabemaschine"
date: "2017-05-15T12:07:51"
picture: "nordconvention66.jpg"
weight: "66"
konstrukteure: 
- "Holger"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45876
- /detailscf5c.html
imported:
- "2019"
_4images_image_id: "45876"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:51"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45876 -->
