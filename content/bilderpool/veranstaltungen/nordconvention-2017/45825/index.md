---
layout: "image"
title: "Sägewerk Kreissäge"
date: "2017-05-15T12:07:32"
picture: "nordconvention15.jpg"
weight: "15"
konstrukteure: 
- "Ingwer"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45825
- /detailsfcbc-2.html
imported:
- "2019"
_4images_image_id: "45825"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:32"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45825 -->
