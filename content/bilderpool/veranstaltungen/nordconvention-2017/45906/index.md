---
layout: "image"
title: "Der klasse Flipper von Dirk"
date: "2017-05-17T16:39:47"
picture: "nordc19.jpg"
weight: "93"
konstrukteure: 
- "Dirk W."
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45906
- /details1371-2.html
imported:
- "2019"
_4images_image_id: "45906"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45906 -->
