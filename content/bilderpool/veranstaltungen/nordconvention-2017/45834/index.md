---
layout: "image"
title: "Flipper in Aktion"
date: "2017-05-15T12:07:36"
picture: "nordconvention24.jpg"
weight: "24"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45834
- /details0cd1-2.html
imported:
- "2019"
_4images_image_id: "45834"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:36"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45834 -->
