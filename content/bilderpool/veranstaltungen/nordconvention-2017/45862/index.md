---
layout: "image"
title: "ft Jumbo Jet"
date: "2017-05-15T12:07:47"
picture: "nordconvention52.jpg"
weight: "52"
konstrukteure: 
- "Frank"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45862
- /details7829-2.html
imported:
- "2019"
_4images_image_id: "45862"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45862 -->
