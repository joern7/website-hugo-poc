---
layout: "image"
title: "Flipper"
date: "2017-05-15T12:07:47"
picture: "nordconvention50.jpg"
weight: "50"
konstrukteure: 
- "Frank"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45860
- /details6ca5-2.html
imported:
- "2019"
_4images_image_id: "45860"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45860 -->
