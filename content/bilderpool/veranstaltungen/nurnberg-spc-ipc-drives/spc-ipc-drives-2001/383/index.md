---
layout: "image"
title: "DSC00091"
date: "2003-04-22T16:46:00"
picture: "DSC00091.jpg"
weight: "12"
konstrukteure: 
- "Staudinger"
fotografen:
- "Lothar Vogt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/383
- /details7dbc.html
imported:
- "2019"
_4images_image_id: "383"
_4images_cat_id: "8"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:46:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=383 -->
