---
layout: "image"
title: "IPC 2002 4"
date: "2003-04-22T16:47:59"
picture: "IPC 2002 4.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/396
- /details69bf.html
imported:
- "2019"
_4images_image_id: "396"
_4images_cat_id: "9"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:47:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=396 -->
