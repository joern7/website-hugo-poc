---
layout: "image"
title: "7"
date: "2007-12-01T09:10:42"
picture: "Neuer_Ordner_007.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12955
- /details87f8.html
imported:
- "2019"
_4images_image_id: "12955"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:10:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12955 -->
SPS/IPC Drives Nürnberg 2007