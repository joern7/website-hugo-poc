---
layout: "image"
title: "14"
date: "2007-12-01T09:10:52"
picture: "Neuer_Ordner_014.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12963
- /details9fa2-2.html
imported:
- "2019"
_4images_image_id: "12963"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:10:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12963 -->
SPS/IPC Drives Nürnberg 2007