---
layout: "image"
title: "21"
date: "2007-12-01T09:11:01"
picture: "Neuer_Ordner_021.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12970
- /details90bf.html
imported:
- "2019"
_4images_image_id: "12970"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:11:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12970 -->
SPS/IPC Drives Nürnberg 2007