---
layout: "image"
title: "12"
date: "2007-12-01T09:10:52"
picture: "Neuer_Ordner_012.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12961
- /details9a01.html
imported:
- "2019"
_4images_image_id: "12961"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:10:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12961 -->
SPS/IPC Drives Nürnberg 2007