---
layout: "image"
title: "Modelbaumesse Wien_Saturnrakete und Mondlandefähre"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien07.jpg"
weight: "9"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42165
- /details848b.html
imported:
- "2019"
_4images_image_id: "42165"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42165 -->
FISCHERTECHNIK TEAM AUSTRIA