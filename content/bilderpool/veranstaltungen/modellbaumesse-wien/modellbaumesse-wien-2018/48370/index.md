---
layout: "image"
title: "Modellbaumesse Wien 2018"
date: "2018-11-02T16:38:59"
picture: "modellbaumessewien7.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48370
- /detailscf06.html
imported:
- "2019"
_4images_image_id: "48370"
_4images_cat_id: "3543"
_4images_user_id: "968"
_4images_image_date: "2018-11-02T16:38:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48370 -->
...an so einem Tag macht das Rad ca. 400 Umdrehungen bei 1:45 min pro Umdrehung.