---
layout: "image"
title: "Modelbaumesse Wien_Das Wiener Riesenrad"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien09.jpg"
weight: "11"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42167
- /detailsc98b-2.html
imported:
- "2019"
_4images_image_id: "42167"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42167 -->
