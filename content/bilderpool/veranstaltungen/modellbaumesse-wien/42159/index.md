---
layout: "image"
title: "Modelbaumesse Wien_Saturnrakete und Mondlandefähre"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien01.jpg"
weight: "3"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42159
- /details124f.html
imported:
- "2019"
_4images_image_id: "42159"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42159 -->
FISCHERTECHNIK TEAM AUSTRIA