---
layout: "image"
title: "Riesenrad in Wien"
date: "2015-10-28T08:58:30"
picture: "IMG_0118.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Haizmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/42157
- /details33a7.html
imported:
- "2019"
_4images_image_id: "42157"
_4images_cat_id: "3142"
_4images_user_id: "560"
_4images_image_date: "2015-10-28T08:58:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42157 -->
