---
layout: "image"
title: "Riesenrad"
date: "2008-02-17T19:31:02"
picture: "greven6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13679
- /detailsd3ef.html
imported:
- "2019"
_4images_image_id: "13679"
_4images_cat_id: "1258"
_4images_user_id: "504"
_4images_image_date: "2008-02-17T19:31:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13679 -->
Mit den drehenden Gondeln.