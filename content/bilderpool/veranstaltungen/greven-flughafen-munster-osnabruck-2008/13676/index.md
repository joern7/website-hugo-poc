---
layout: "image"
title: "Roboter"
date: "2008-02-17T19:31:02"
picture: "greven3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13676
- /detailsae84.html
imported:
- "2019"
_4images_image_id: "13676"
_4images_cat_id: "1258"
_4images_user_id: "504"
_4images_image_date: "2008-02-17T19:31:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13676 -->
Mit schnelleren Motoren als der Normale.