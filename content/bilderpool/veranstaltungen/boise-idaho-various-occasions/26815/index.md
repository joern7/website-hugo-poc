---
layout: "image"
title: "ft in Egypt"
date: "2010-03-23T21:06:12"
picture: "encerc6.jpg"
weight: "48"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain", "Egypt"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26815
- /details9286.html
imported:
- "2019"
_4images_image_id: "26815"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-03-23T21:06:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26815 -->
These are images of a ft robotics competition in Egypt.