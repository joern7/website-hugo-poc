---
layout: "image"
title: "Community Schools Class"
date: "2010-02-13T15:23:27"
picture: "sm_am_ft.jpg"
weight: "30"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Community", "Schools"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26341
- /detailsdad9.html
imported:
- "2019"
_4images_image_id: "26341"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-02-13T15:23:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26341 -->
These are images of a "Mechanical Engineering with fischertechnik" class I am teaching at Community Schools!