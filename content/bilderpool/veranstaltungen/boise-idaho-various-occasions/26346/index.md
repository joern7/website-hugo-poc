---
layout: "image"
title: "Community Schools Class"
date: "2010-02-13T15:23:28"
picture: "sm_jaiden_2.jpg"
weight: "35"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Community", "Schools", "Boise", "PCS"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26346
- /details2419.html
imported:
- "2019"
_4images_image_id: "26346"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-02-13T15:23:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26346 -->
These are images of a "Mechanical Engineering with fischertechnik" class I am teaching at Community Schools!