---
layout: "image"
title: "Mechatronics at BSU 2009"
date: "2009-01-09T22:16:25"
picture: "DSC01245.jpg"
weight: "2"
konstrukteure: 
- "BSU students"
fotografen:
- "BSU"
keywords: ["BSU", "Mechatronics", "PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16957
- /detailsb008.html
imported:
- "2019"
_4images_image_id: "16957"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-01-09T22:16:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16957 -->
Photos from the Design and Analysis of Mechatronics Class (Robotics Module) from Boise State University. This project combined ft and the PCS Brain controller.  

***google translation***

Fotos von der Konzeption und Analyse der Mechatronik-Klasse (Robotik-Modul) aus Boise State University. Dieses Projekt kombiniert ft und der PCS-Brain-Controller.