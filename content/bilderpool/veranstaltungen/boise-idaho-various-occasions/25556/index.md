---
layout: "image"
title: "BrickCON Seattle WA 2009"
date: "2009-10-14T00:35:14"
picture: "sm_ft_brickon4.jpg"
weight: "16"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["BrickCON"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25556
- /details22e5.html
imported:
- "2019"
_4images_image_id: "25556"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-10-14T00:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25556 -->
These are images of a fischertechnik session I conducted at BrickCON in Seattle Washington, 2009. Many had never seen ft before, and attendees loved it!