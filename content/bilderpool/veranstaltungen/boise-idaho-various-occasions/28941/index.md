---
layout: "image"
title: "Teacher Training"
date: "2010-10-08T14:21:04"
picture: "sm_ft_trainA.jpg"
weight: "57"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["in-service", "teacher", "training"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28941
- /detailsabe6.html
imported:
- "2019"
_4images_image_id: "28941"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-10-08T14:21:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28941 -->
Images from a recent ft teacher in-service session.