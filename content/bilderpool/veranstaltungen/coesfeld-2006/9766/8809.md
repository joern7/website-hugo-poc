---
layout: "comment"
hidden: true
title: "8809"
date: "2009-03-22T09:41:33"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Hallo...
Oh ha, ein bisschen lange her.
Ich vermute mal das es von einem aus Werne war (Es muss noch eine Liste mit den Ausstellern geben). Die haben da eine art von Elektronikclub. Sehr tolle Sachen haben die gemacht, wie z.B. die S5 SPS auf einer Platine mit IOS, Analogeingänge usw...
Was mich sehr beeindruckt hat, war die Lernfunktion. Man konnte frei jede Taste für einen beliebigen Kolben belegen.

Es war einfach das IC rausgenommen worden und durch die Adapterplatine ersetzt worden. So hat man zugriff auf die digitalen I/Os des ft Interfaces. Ich meine es waren 6 Interfaces hintereinander geschaltet.

Man könnte es auch anders machen. Z.B. mit der AVR-Platine von Pollin und dort den seriellen Eingang nutzen. Dann hätte man auch die analogen Eingänge. Oder den AVR in Kombination mit der BOX von Knobloch. So hätte man den AVR an RoboPro angekoppelt.
Gruß
Holger Howey