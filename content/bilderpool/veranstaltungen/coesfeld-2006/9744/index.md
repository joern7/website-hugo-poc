---
layout: "image"
title: "IR Auto"
date: "2007-03-23T23:43:25"
picture: "158_5895.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9744
- /details5f25-2.html
imported:
- "2019"
_4images_image_id: "9744"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9744 -->
