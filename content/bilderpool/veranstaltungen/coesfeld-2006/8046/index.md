---
layout: "image"
title: "COE 16"
date: "2006-12-20T21:50:26"
picture: "158_5802_2.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8046
- /detailse9ac.html
imported:
- "2019"
_4images_image_id: "8046"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8046 -->
