---
layout: "image"
title: "Übersicht 1"
date: "2006-11-21T17:43:16"
picture: "Coesfeld_001.jpg"
weight: "1"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7519
- /details4f1c.html
imported:
- "2019"
_4images_image_id: "7519"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7519 -->
Ein Blick auf die Zuschauermenge