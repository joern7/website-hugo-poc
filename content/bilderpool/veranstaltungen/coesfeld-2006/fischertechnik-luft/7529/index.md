---
layout: "image"
title: "Übersicht 4"
date: "2006-11-21T18:08:52"
picture: "Coesfeld_030.jpg"
weight: "5"
konstrukteure: 
- "diverse"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7529
- /details8bce.html
imported:
- "2019"
_4images_image_id: "7529"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7529 -->
mit Blick auf die Riesenräder