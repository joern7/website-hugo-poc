---
layout: "image"
title: "Übersicht 2"
date: "2006-11-21T17:43:16"
picture: "Coesfeld_002.jpg"
weight: "2"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7520
- /detailsd54c.html
imported:
- "2019"
_4images_image_id: "7520"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7520 -->
Ein zweiter Blick auf die Zuschauermenge