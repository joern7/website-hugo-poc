---
layout: "image"
title: "Herr Jansen, Herr Roller und Stephan am schauen"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_140.jpg"
weight: "8"
konstrukteure: 
- "niemand"
fotografen:
- "Stephan's Frau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7591
- /details93f1.html
imported:
- "2019"
_4images_image_id: "7591"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7591 -->
