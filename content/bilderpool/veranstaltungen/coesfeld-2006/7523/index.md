---
layout: "image"
title: "ft Männchen in Lebensgröße"
date: "2006-11-21T17:43:16"
picture: "Coesfeld_007.jpg"
weight: "1"
konstrukteure: 
- "ft"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7523
- /details32b7.html
imported:
- "2019"
_4images_image_id: "7523"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7523 -->
netter Kerl der am Eingang stand und um eine kleine Spende für die Organisatoren bat