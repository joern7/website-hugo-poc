---
layout: "image"
title: "Ludgers Truck"
date: "2006-11-21T18:08:59"
picture: "Coesfeld_049.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7537
- /details273d.html
imported:
- "2019"
_4images_image_id: "7537"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7537 -->
Draufsicht