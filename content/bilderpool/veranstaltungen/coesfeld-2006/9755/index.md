---
layout: "image"
title: "Detail"
date: "2007-03-23T23:43:25"
picture: "159_5907.jpg"
weight: "68"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9755
- /details0b14.html
imported:
- "2019"
_4images_image_id: "9755"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9755 -->
