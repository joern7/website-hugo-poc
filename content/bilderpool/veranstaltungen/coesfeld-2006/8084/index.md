---
layout: "image"
title: "COE 59"
date: "2006-12-20T21:50:26"
picture: "158_5845.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8084
- /detailsde2f.html
imported:
- "2019"
_4images_image_id: "8084"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8084 -->
