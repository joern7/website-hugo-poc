---
layout: "image"
title: "Kolben"
date: "2007-03-23T23:43:25"
picture: "159_5916.jpg"
weight: "77"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9764
- /details08f6-3.html
imported:
- "2019"
_4images_image_id: "9764"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9764 -->
Dies ist eine sehr besindere Ansteuerung.
Diese besteht aus einer SPS Step 5 aus dem Bergbau mit 168mA Stromaufnahme und u.a. mit analogen Eingängen... und mehreren  (6 ?) parallelen ft Interfaces