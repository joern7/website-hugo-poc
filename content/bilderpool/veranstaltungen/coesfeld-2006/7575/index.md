---
layout: "image"
title: "fliegendes Flugzeug"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_120.jpg"
weight: "16"
konstrukteure: 
- "TST"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7575
- /details8198.html
imported:
- "2019"
_4images_image_id: "7575"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7575 -->
