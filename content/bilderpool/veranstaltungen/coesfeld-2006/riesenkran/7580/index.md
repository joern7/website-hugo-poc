---
layout: "image"
title: "Laufkatze vom Kran en gros"
date: "2006-12-20T21:50:26"
picture: "Coesfeld_128.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7580
- /details5242.html
imported:
- "2019"
_4images_image_id: "7580"
_4images_cat_id: "1296"
_4images_user_id: "130"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7580 -->
