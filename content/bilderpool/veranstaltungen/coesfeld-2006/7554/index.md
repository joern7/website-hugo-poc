---
layout: "image"
title: "Planierraupe auf Tieflader"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_083.jpg"
weight: "9"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7554
- /details1865.html
imported:
- "2019"
_4images_image_id: "7554"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7554 -->
