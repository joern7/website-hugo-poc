---
layout: "image"
title: "Motor von Brickwedde"
date: "2007-01-07T14:41:08"
picture: "coesfeld1.jpg"
weight: "20"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/8321
- /detailsa75e.html
imported:
- "2019"
_4images_image_id: "8321"
_4images_cat_id: "1563"
_4images_user_id: "130"
_4images_image_date: "2007-01-07T14:41:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8321 -->
Schleifring für einen Motor