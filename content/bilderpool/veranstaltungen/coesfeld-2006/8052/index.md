---
layout: "image"
title: "COE 22"
date: "2006-12-20T21:50:26"
picture: "158_5808.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8052
- /detailsa2a1.html
imported:
- "2019"
_4images_image_id: "8052"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8052 -->
