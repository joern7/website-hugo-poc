---
layout: "image"
title: "COE 45"
date: "2006-12-20T21:50:26"
picture: "158_5831.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8077
- /detailsbd9e.html
imported:
- "2019"
_4images_image_id: "8077"
_4images_cat_id: "1298"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8077 -->
