---
layout: "comment"
hidden: true
title: "7867"
date: "2008-11-26T17:34:12"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Freunden,

Schau mal unter:

http://www.ftcommunity.de/details.php?image_id=7356

Festo hat auch ein "standard"-Alternativ: 

CN-M5-PK2 FES 19.521 (1 á 2 Euro/St) 

LCN-M5-PK2 FES 19.523 (2 á 3 Euro/St) 

FT-Baustein 15 mit Bohrung 32.064 

Gruss, 

Peter Damen 
Poederoyen, Holland