---
layout: "comment"
hidden: true
title: "2781"
date: "2007-03-24T14:23:00"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Hallo...
Das hat leider einen tieferen Grund. Ich habe hier noch ein paar hundert Fotos die auf das Hochladen warten. Ich muß zugeben das ich nur auf "einigermaßen" Scharf geachtet habe und nicht auf die Ausrichtung. Ich habe diese Fotos hauptsächlich gemacht um für mich gute Ideen zu sammeln. Ich muß mal sehen wann ich die Zeit dafür finde.
Gruß
Holger Howey