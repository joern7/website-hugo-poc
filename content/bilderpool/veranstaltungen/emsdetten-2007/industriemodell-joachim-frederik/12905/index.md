---
layout: "image"
title: "Industriemaschine"
date: "2007-11-29T17:35:21"
picture: "olli32.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12905
- /detailsbfac.html
imported:
- "2019"
_4images_image_id: "12905"
_4images_cat_id: "1169"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:21"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12905 -->
Von Fredy und Joachim