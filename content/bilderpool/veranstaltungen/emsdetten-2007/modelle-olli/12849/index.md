---
layout: "image"
title: "Dampfwalze"
date: "2007-11-26T18:39:37"
picture: "olli2.jpg"
weight: "2"
konstrukteure: 
- "Olli"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12849
- /details34e3-2.html
imported:
- "2019"
_4images_image_id: "12849"
_4images_cat_id: "1161"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T18:39:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12849 -->
