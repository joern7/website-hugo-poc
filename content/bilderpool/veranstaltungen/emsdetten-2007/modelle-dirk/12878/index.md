---
layout: "image"
title: "Mobilkran"
date: "2007-11-29T17:35:20"
picture: "olli05.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12878
- /details4497.html
imported:
- "2019"
_4images_image_id: "12878"
_4images_cat_id: "1163"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12878 -->
Teil des Kranes von Dirk Kutsch.Leider nicht ganz im Bild.