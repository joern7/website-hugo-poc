---
layout: "image"
title: "Monsterkran"
date: "2007-11-29T17:35:20"
picture: "olli25.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12898
- /details177b.html
imported:
- "2019"
_4images_image_id: "12898"
_4images_cat_id: "1168"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12898 -->
mhm