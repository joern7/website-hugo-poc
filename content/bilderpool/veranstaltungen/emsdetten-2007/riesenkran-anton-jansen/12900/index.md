---
layout: "image"
title: "Kran"
date: "2007-11-29T17:35:20"
picture: "olli27.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12900
- /detailsea34-2.html
imported:
- "2019"
_4images_image_id: "12900"
_4images_cat_id: "1168"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12900 -->
