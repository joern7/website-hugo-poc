---
layout: "image"
title: "Traktor"
date: "2007-11-29T17:35:20"
picture: "olli15.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12888
- /details9ae1.html
imported:
- "2019"
_4images_image_id: "12888"
_4images_cat_id: "1167"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12888 -->
Auch vom Herrn Brickwedde