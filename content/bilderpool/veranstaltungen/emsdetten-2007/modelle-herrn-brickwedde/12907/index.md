---
layout: "image"
title: "Nabe"
date: "2007-11-29T17:35:21"
picture: "olli34.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12907
- /details9375-2.html
imported:
- "2019"
_4images_image_id: "12907"
_4images_cat_id: "1167"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:21"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12907 -->
Vom Riesenrad