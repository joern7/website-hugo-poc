---
layout: "image"
title: "Spielbrett"
date: "2007-11-30T12:30:19"
picture: "brickwedde2.jpg"
weight: "13"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12937
- /detailse25f.html
imported:
- "2019"
_4images_image_id: "12937"
_4images_cat_id: "1167"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:30:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12937 -->
