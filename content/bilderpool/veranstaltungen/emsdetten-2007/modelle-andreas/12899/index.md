---
layout: "image"
title: "Schild"
date: "2007-11-29T17:35:20"
picture: "olli26.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12899
- /details7f0b.html
imported:
- "2019"
_4images_image_id: "12899"
_4images_cat_id: "1164"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12899 -->
....von Tobias Tacke