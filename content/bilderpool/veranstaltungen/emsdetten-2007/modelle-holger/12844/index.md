---
layout: "image"
title: "Steuerung"
date: "2007-11-26T16:28:11"
picture: "modellevonholger2.jpg"
weight: "2"
konstrukteure: 
- "Holger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12844
- /details84f1.html
imported:
- "2019"
_4images_image_id: "12844"
_4images_cat_id: "1162"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12844 -->
