---
layout: "image"
title: "Radlader"
date: "2007-11-26T16:28:11"
picture: "modellevonholger3.jpg"
weight: "3"
konstrukteure: 
- "Holger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12845
- /details38c3.html
imported:
- "2019"
_4images_image_id: "12845"
_4images_cat_id: "1162"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12845 -->
