---
layout: "image"
title: "Lastaufnahme"
date: "2007-11-29T19:56:16"
picture: "puetter3.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/12917
- /details8499.html
imported:
- "2019"
_4images_image_id: "12917"
_4images_cat_id: "1166"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:56:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12917 -->
Hier wird gerade eine Kiste angehoben