---
layout: "image"
title: "Laufkatze"
date: "2007-11-29T17:35:20"
picture: "olli03.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12876
- /detailse9ed.html
imported:
- "2019"
_4images_image_id: "12876"
_4images_cat_id: "1166"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12876 -->
Vom Kran