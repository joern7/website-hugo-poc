---
layout: "image"
title: "Modell"
date: "2007-11-29T17:35:20"
picture: "olli13.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12886
- /details705c.html
imported:
- "2019"
_4images_image_id: "12886"
_4images_cat_id: "1170"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12886 -->
Kirmesding