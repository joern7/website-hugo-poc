---
layout: "image"
title: "Kran"
date: "2007-11-30T18:56:14"
picture: "fischer1.jpg"
weight: "6"
konstrukteure: 
- "Stefan Fasterman"
fotografen:
- "Jürgen Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12940
- /detailse555.html
imported:
- "2019"
_4images_image_id: "12940"
_4images_cat_id: "1158"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T18:56:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12940 -->
