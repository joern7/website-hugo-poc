---
layout: "image"
title: "Fernlenkauto"
date: "2007-11-08T23:07:48"
picture: "seevetalhittfeld12.jpg"
weight: "12"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12552
- /details0e8e.html
imported:
- "2019"
_4images_image_id: "12552"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:07:48"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12552 -->
Die Kinder haben sich viel mit den Ferngelenkten Autos beschäftigt.
Drei davon waren im Einsatz, war schon lustig anzusehen wenn alle drei Autos in eine Richtung fuhren und keiner der Fahrer wusste wer den nun die Autos steuert.