---
layout: "image"
title: "Der Schnäppchentisch"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld10.jpg"
weight: "10"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12550
- /details47f6.html
imported:
- "2019"
_4images_image_id: "12550"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12550 -->
Überaschungsboxen, Silberlinge und Beutel mit allerlei ft.