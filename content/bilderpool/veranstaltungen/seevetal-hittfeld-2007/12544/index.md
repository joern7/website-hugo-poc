---
layout: "image"
title: "Packstation"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld04.jpg"
weight: "4"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12544
- /details2be8.html
imported:
- "2019"
_4images_image_id: "12544"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12544 -->
In den Trichter wurden die fertig einsortierten ft-Teile geschüttet.