---
layout: "image"
title: "Packstation"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld06.jpg"
weight: "6"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12546
- /detailsf291.html
imported:
- "2019"
_4images_image_id: "12546"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12546 -->
Hier sieht man die Technik zum halten der Beutel.