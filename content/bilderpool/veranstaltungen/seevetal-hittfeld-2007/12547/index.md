---
layout: "image"
title: "Robo Explorer"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld07.jpg"
weight: "7"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12547
- /details7646.html
imported:
- "2019"
_4images_image_id: "12547"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12547 -->
Fuhr Stundenlang immer der schwarzen Linie nach.