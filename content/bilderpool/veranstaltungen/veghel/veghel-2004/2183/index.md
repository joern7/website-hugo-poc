---
layout: "image"
title: "Mani02.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani02_2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2183
- /details3f62.html
imported:
- "2019"
_4images_image_id: "2183"
_4images_cat_id: "204"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2183 -->
Manitowac 21000, ein Mordsteil von Kran.

Detailaufnahmen gibts unter Modelle/Scale-Modelle/Manitowac 21000, 
http://www.ftcommunity.de/categories.php?cat_id=210