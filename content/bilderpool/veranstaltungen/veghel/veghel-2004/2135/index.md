---
layout: "image"
title: "Bagger01.JPG"
date: "2004-02-20T12:21:29"
picture: "Bagger01.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2135
- /details7b41.html
imported:
- "2019"
_4images_image_id: "2135"
_4images_cat_id: "204"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2135 -->
Die interessanten Sachen sind unter der Motorhaube versteckt, aber das hab ich nicht fotografiert :-(

Ein Blick in den Löffel beweist, dass meine Digicam sehr empfindlich auf Staub reagiert. Jedes Körnchen gibt ein helles Pixel.