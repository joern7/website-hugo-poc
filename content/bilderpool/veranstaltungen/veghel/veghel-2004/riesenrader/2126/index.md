---
layout: "image"
title: "RR04.JPG"
date: "2004-02-20T12:21:10"
picture: "RR04.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2126
- /details3c62.html
imported:
- "2019"
_4images_image_id: "2126"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2126 -->
Veghel 2004, hier in der Totalen.
Das Tragwerk im Rad besteht nur aus Streben und sieht sehr filigran aus, aber es ist erstaunlich stabil.

Unten drunter ist ein Radfahrgestell samt Zugmaschine.