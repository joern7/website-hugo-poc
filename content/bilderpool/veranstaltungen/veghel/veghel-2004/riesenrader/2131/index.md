---
layout: "image"
title: "RRBrick02.JPG"
date: "2004-02-20T12:21:29"
picture: "RRBrick02.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2131
- /detailsd00a.html
imported:
- "2019"
_4images_image_id: "2131"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2131 -->
Veghel 2004, das 'andere' von zwei Riesenrädern.

Auch hier keine Spur vom Antrieb zu sehen, und gedreht hat es sich doch!