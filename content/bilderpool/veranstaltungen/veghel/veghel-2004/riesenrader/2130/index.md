---
layout: "image"
title: "RRBrick01.JPG"
date: "2004-02-20T12:21:29"
picture: "RRBrick01.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2130
- /details09aa.html
imported:
- "2019"
_4images_image_id: "2130"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2130 -->
Veghel 2004, das 'andere' von den beiden Riesenrädern.

Irgendwo hab ich da nicht aufgepasst - der Antrieb muss wohl in der Nabe versteckt sein.