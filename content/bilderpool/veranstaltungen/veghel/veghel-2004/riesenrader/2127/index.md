---
layout: "image"
title: "RR08.JPG"
date: "2004-02-20T12:21:29"
picture: "RR08.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2127
- /details0806-2.html
imported:
- "2019"
_4images_image_id: "2127"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2127 -->
Veghel 2004, eins von zwei Riesenrädern.

Hier nochmal das Tragwerk im Rad, alles nur aus Statik-Streben!