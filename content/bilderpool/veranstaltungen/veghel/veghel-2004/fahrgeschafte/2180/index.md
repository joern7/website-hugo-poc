---
layout: "image"
title: "Karussel01.JPG"
date: "2004-02-23T00:24:26"
picture: "Karussel01.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2180
- /detailsfa72-2.html
imported:
- "2019"
_4images_image_id: "2180"
_4images_cat_id: "426"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2180 -->
Zu diesem Karussel gibts unter 'Schoonhoven 2003' noch mehr Bilder.