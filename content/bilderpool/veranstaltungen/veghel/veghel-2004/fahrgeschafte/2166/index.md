---
layout: "image"
title: "Riesenrad 2"
date: "2004-02-20T12:22:46"
picture: "Riesenrad_2.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kirmes", "Riesenrad"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2166
- /details34d0.html
imported:
- "2019"
_4images_image_id: "2166"
_4images_cat_id: "426"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2166 -->
