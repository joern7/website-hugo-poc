---
layout: "image"
title: "The_Spin02.JPG"
date: "2004-02-23T00:24:27"
picture: "The_Spin02.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2197
- /details8b56.html
imported:
- "2019"
_4images_image_id: "2197"
_4images_cat_id: "426"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2197 -->
Der obere Kranz dreht gegenläufig zum Rest, durch die exzentrische Lagerung werden die Gondeln gehoben und gesenkt.