---
layout: "image"
title: "Kitzelrob02.JPG"
date: "2004-02-20T12:22:45"
picture: "Kitzelrob02.jpg"
weight: "2"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2141
- /detailse1f3.html
imported:
- "2019"
_4images_image_id: "2141"
_4images_cat_id: "427"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2141 -->
Die Gegengewichte bestehen aus etwa 3,5 kg schweren Stahlplatten!