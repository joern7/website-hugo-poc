---
layout: "image"
title: "Kitzelroboter 1"
date: "2004-02-20T12:22:45"
picture: "Kitzelroboter_2.jpg"
weight: "5"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
keywords: ["Roboter", "Knickarm"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2144
- /details6fed-2.html
imported:
- "2019"
_4images_image_id: "2144"
_4images_cat_id: "427"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2144 -->
