---
layout: "image"
title: "Pianoroboter 6"
date: "2004-02-20T12:22:45"
picture: "Pianoroboter_6.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2159
- /details5003.html
imported:
- "2019"
_4images_image_id: "2159"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2159 -->
Detail Schultergelenk zur Seitwärtsbewegung des Armes (hier linke Seite).