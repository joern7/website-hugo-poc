---
layout: "image"
title: "Pianoroboter 10"
date: "2004-02-20T12:22:45"
picture: "Pianoroboter_10.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2154
- /details65fc.html
imported:
- "2019"
_4images_image_id: "2154"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2154 -->
Das Instrument, der Meister und sein Meister (Marcel Bosch)