---
layout: "image"
title: "Veghel_042.jpg"
date: "2006-03-26T15:44:48"
picture: "Veghel_042.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen"
keywords: ["Piano", "Roboter"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5968
- /details04c9.html
imported:
- "2019"
_4images_image_id: "5968"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:44:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5968 -->
