---
layout: "image"
title: "Veghel_044.jpg"
date: "2006-03-26T15:45:41"
picture: "Veghel_044.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen"
keywords: ["LKW"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5969
- /details50ae.html
imported:
- "2019"
_4images_image_id: "5969"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5969 -->
