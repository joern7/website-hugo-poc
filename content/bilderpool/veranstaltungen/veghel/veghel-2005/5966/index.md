---
layout: "image"
title: "Veghel_038.jpg"
date: "2006-03-26T15:43:53"
picture: "Veghel_038.jpg"
weight: "13"
konstrukteure: 
- "---"
fotografen:
- "Peter Damen"
keywords: ["Piano", "Roboter"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5966
- /details6dc6.html
imported:
- "2019"
_4images_image_id: "5966"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:43:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5966 -->
