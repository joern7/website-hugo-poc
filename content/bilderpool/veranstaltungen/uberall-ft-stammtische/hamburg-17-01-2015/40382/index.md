---
layout: "image"
title: "Laser"
date: "2015-01-17T22:00:11"
picture: "stammtischhamburg5.jpg"
weight: "5"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/40382
- /details37e1.html
imported:
- "2019"
_4images_image_id: "40382"
_4images_cat_id: "3026"
_4images_user_id: "373"
_4images_image_date: "2015-01-17T22:00:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40382 -->
von Dirk Wölfel, reicht wohl zum Gravieren von Holz...Nicht ganz ungefährlich. Daneben ein Anschlusskabel für den Aufzug von Dirk und ein weiterer kleiner Laser im Minimotoer-Gehäuse von sulu007(glaube ich...).