---
layout: "image"
title: "vereinsgruendung1.jpg"
date: "2013-08-31T15:49:52"
picture: "vereinsgruendung1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/37282
- /details3637.html
imported:
- "2019"
_4images_image_id: "37282"
_4images_cat_id: "1303"
_4images_user_id: "373"
_4images_image_date: "2013-08-31T15:49:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37282 -->
Gruppenbild der Vereinsgründung am 31.08.2013

Damit ist der "ftc Modellbau e.V." gegründet.