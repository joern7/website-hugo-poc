---
layout: "image"
title: "Dmdbt und masked"
date: "2007-04-29T22:38:25"
picture: "dsc01102_resize.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/10222
- /detailse246.html
imported:
- "2019"
_4images_image_id: "10222"
_4images_cat_id: "925"
_4images_user_id: "120"
_4images_image_date: "2007-04-29T22:38:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10222 -->
Die andere Tischseite war auch nicht so begeistert, beim nächsten mal brauchen wir 2 Kuchen :)