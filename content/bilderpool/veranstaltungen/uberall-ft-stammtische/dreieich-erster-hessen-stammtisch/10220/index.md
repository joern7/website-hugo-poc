---
layout: "image"
title: "Farbdisplay"
date: "2007-04-29T19:59:11"
picture: "hessen3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/10220
- /detailsda53.html
imported:
- "2019"
_4images_image_id: "10220"
_4images_cat_id: "925"
_4images_user_id: "373"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10220 -->
Ein Farbdisplay das Thkais aufgetrieben hat, allerdings noch im Entwicklungsstadium. Das Teil passt exakt in eine 60x60-Kassette rein und sieht recht vielversprechend aus.