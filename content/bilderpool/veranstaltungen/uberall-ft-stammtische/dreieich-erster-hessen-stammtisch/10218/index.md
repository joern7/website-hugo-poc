---
layout: "image"
title: "Thomas 1"
date: "2007-04-29T19:59:11"
picture: "hessen1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/10218
- /details331e.html
imported:
- "2019"
_4images_image_id: "10218"
_4images_cat_id: "925"
_4images_user_id: "373"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10218 -->
Thomas beim demonstrieren seines neuen Farbdisplays während dem ersten Hessen-Stammtisch in Dreieich. Anwesend waren Thomas (ThKais), Thomas (schnaggels), Frank (dmdbt), Martin (Masked) und ein Apfelkuchen. Der hielt aber nicht lange....