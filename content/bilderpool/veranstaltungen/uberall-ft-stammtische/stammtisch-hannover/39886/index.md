---
layout: "image"
title: "Stammtisch Hannover"
date: "2014-11-23T19:12:24"
picture: "stammtischhannover2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Rioman Rose (Nordhannoversche Zeitung)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/39886
- /detailsc78e.html
imported:
- "2019"
_4images_image_id: "39886"
_4images_cat_id: "2994"
_4images_user_id: "1"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39886 -->
