---
layout: "image"
title: "Stammtisch Hannover"
date: "2014-11-23T19:12:24"
picture: "stammtischhannover1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Rioman Rose (Nordhannoversche Zeitung)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/39885
- /details5b52.html
imported:
- "2019"
_4images_image_id: "39885"
_4images_cat_id: "2994"
_4images_user_id: "1"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39885 -->
