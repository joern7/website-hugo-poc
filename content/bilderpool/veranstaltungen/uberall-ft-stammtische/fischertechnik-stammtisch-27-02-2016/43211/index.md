---
layout: "image"
title: "ft-kompatible Halterung für µC-Boards"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/43211
- /details76d9.html
imported:
- "2019"
_4images_image_id: "43211"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43211 -->
Und hier die Rückseite mit Löchern für S-Riegel.