---
layout: "image"
title: "Digitalkamera mit AutoFokus"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/43206
- /details4c77.html
imported:
- "2019"
_4images_image_id: "43206"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43206 -->
Siehe die ft:pedia 2016-1!