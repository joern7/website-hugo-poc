---
layout: "image"
title: "Zugmaschine mit Sound"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover05.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/40971
- /detailsfb47-2.html
imported:
- "2019"
_4images_image_id: "40971"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40971 -->
Hier in ganzer Schönheit