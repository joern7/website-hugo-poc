---
layout: "image"
title: "Zugmaschine mit Sound(!!)"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover04.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/40970
- /details7828.html
imported:
- "2019"
_4images_image_id: "40970"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40970 -->
Imposante Zugmaschine von Claus hier mit freigelegter Technik