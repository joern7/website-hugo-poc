---
layout: "image"
title: "Laufroboter"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover11.jpg"
weight: "11"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/40977
- /details839e.html
imported:
- "2019"
_4images_image_id: "40977"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40977 -->
Ralf's Laufroboter