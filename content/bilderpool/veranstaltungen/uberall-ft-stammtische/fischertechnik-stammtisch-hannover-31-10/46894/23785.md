---
layout: "comment"
hidden: true
title: "23785"
date: "2017-11-06T22:20:48"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Ein Ikosaeder, wie er auch schon auf der Nord-Convention zu sehen war und ein vor kurzem erst entstandener Dodekaeder. Beide aus der Feder von meinem mittleren Sohn Florian.
Beide (Spiel)Würfel sind mit einem Zahlencode auf den flachen Seiten ausgestattet, damit man den Würfel auch ablesen kann. Die gegenüberliegenden Summen ergeben immer die selbe Summe - also 21 beim Ikosaeder und 13 beim Dodekaeder.