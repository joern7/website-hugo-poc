---
layout: "image"
title: "Platine"
date: "2017-11-06T16:08:42"
picture: "stammtisch15.jpg"
weight: "15"
konstrukteure: 
- "Thomas"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46901
- /details6d7b.html
imported:
- "2019"
_4images_image_id: "46901"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46901 -->
