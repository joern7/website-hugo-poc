---
layout: "image"
title: "Fachgesimpel"
date: "2017-11-06T16:08:24"
picture: "stammtisch02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46888
- /detailsf1e4.html
imported:
- "2019"
_4images_image_id: "46888"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46888 -->
