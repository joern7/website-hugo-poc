---
layout: "image"
title: "Magnetmotor"
date: "2017-11-06T16:08:42"
picture: "stammtisch11.jpg"
weight: "11"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46897
- /details87f1-2.html
imported:
- "2019"
_4images_image_id: "46897"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46897 -->
