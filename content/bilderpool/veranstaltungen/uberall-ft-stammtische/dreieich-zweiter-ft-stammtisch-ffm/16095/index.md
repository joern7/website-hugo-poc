---
layout: "image"
title: "Das muss doch..."
date: "2008-10-28T18:08:56"
picture: "DSC01804A.jpg"
weight: "5"
konstrukteure: 
- "/"
fotografen:
- "Volker-James Muenchhof"
keywords: ["Stammtisch"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/16095
- /detailsbc0d.html
imported:
- "2019"
_4images_image_id: "16095"
_4images_cat_id: "1459"
_4images_user_id: "41"
_4images_image_date: "2008-10-28T18:08:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16095 -->
... irgendwie funktionieren, nur wie?