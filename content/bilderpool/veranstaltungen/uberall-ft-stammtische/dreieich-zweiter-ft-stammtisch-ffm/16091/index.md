---
layout: "image"
title: "Alle"
date: "2008-10-28T18:08:55"
picture: "DSC01799A.jpg"
weight: "1"
konstrukteure: 
- "/"
fotografen:
- "Michaels Frauchen"
keywords: ["Stammtisch"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/16091
- /detailsf4d2.html
imported:
- "2019"
_4images_image_id: "16091"
_4images_cat_id: "1459"
_4images_user_id: "41"
_4images_image_date: "2008-10-28T18:08:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16091 -->
Übersicht über den zweiten ft-Stammtisch FFM, bei dem sogar eine Frau anwesend war, die sich aber geschickt hinter dem Fotoapparat versteckt hat ;-)