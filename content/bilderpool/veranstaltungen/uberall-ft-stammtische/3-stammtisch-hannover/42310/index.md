---
layout: "image"
title: "stammtisch13.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch13.jpg"
weight: "13"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42310
- /details2a79.html
imported:
- "2019"
_4images_image_id: "42310"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42310 -->
