---
layout: "image"
title: "stammtisch57.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch57.jpg"
weight: "57"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42354
- /details530f.html
imported:
- "2019"
_4images_image_id: "42354"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42354 -->
