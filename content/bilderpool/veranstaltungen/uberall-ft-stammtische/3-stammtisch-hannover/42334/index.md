---
layout: "image"
title: "stammtisch37.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch37.jpg"
weight: "37"
konstrukteure: 
- "Triceratops"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42334
- /detailsef88.html
imported:
- "2019"
_4images_image_id: "42334"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42334 -->
