---
layout: "image"
title: "stammtisch28.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch28.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42325
- /details461c.html
imported:
- "2019"
_4images_image_id: "42325"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42325 -->
