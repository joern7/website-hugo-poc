---
layout: "image"
title: "stammtisch33.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch33.jpg"
weight: "33"
konstrukteure: 
- "giliprimero"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42330
- /details768e.html
imported:
- "2019"
_4images_image_id: "42330"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42330 -->
