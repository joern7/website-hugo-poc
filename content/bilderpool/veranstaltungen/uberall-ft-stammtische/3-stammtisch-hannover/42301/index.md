---
layout: "image"
title: "stammtisch04.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch04.jpg"
weight: "4"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42301
- /detailsa8cc-2.html
imported:
- "2019"
_4images_image_id: "42301"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42301 -->
