---
layout: "image"
title: "stammtisch34.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch34.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42331
- /details78c5-2.html
imported:
- "2019"
_4images_image_id: "42331"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42331 -->
