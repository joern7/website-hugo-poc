---
layout: "image"
title: "stammtisch14.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch14.jpg"
weight: "14"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42311
- /detailsc049-4.html
imported:
- "2019"
_4images_image_id: "42311"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42311 -->
