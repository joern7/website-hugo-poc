---
layout: "image"
title: "stammtisch48.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch48.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42345
- /details568a.html
imported:
- "2019"
_4images_image_id: "42345"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42345 -->
