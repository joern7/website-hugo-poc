---
layout: "image"
title: "stammtisch46.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch46.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42343
- /details2c77.html
imported:
- "2019"
_4images_image_id: "42343"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42343 -->
