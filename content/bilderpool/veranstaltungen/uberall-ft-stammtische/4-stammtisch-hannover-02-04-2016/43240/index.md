---
layout: "image"
title: "stammtisch13.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43240
- /detailsef15.html
imported:
- "2019"
_4images_image_id: "43240"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43240 -->
