---
layout: "image"
title: "stammtisch03.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43230
- /details3093.html
imported:
- "2019"
_4images_image_id: "43230"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43230 -->
