---
layout: "image"
title: "Riesenrad"
date: "2016-04-05T11:46:23"
picture: "stammtisch7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "ThanksForTheFish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/43259
- /detailsef8f.html
imported:
- "2019"
_4images_image_id: "43259"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43259 -->
Hereinspaziert! Alle weiblichen fischertechnik-Figuren bekommen freien Eintritt!