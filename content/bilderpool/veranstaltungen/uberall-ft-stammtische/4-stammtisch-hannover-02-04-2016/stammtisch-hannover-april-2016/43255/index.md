---
layout: "image"
title: "Kugelbahn für Tischtennisbälle"
date: "2016-04-05T11:46:23"
picture: "stammtisch3.jpg"
weight: "3"
konstrukteure: 
- "Triceratops"
fotografen:
- "ThanksForTheFish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/43255
- /details9753.html
imported:
- "2019"
_4images_image_id: "43255"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43255 -->
Hier wurden nur gelbe und rote Teile verbaut. (Einzige Ausnahme Elektromagnet. Den gibt's nur in grau.)