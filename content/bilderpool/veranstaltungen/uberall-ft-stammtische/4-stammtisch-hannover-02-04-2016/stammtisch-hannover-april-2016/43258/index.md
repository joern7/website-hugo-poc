---
layout: "image"
title: "Freihand- und Stand-Mikrofon"
date: "2016-04-05T11:46:23"
picture: "stammtisch6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "ThanksForTheFish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/43258
- /details47c4.html
imported:
- "2019"
_4images_image_id: "43258"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43258 -->
Unabdingbar für eine moderate Moderation!