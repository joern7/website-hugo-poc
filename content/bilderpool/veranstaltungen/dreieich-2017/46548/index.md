---
layout: "image"
title: "Ballsammler"
date: "2017-09-30T13:03:40"
picture: "ftconvs06.jpg"
weight: "5"
konstrukteure: 
- "Herrmann"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46548
- /detailsf1a0.html
imported:
- "2019"
_4images_image_id: "46548"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46548 -->
Was dem Landwirt beim Strohballen-Ernten recht ist, kann dem Tischtennissspieler billig sein: diese Maschine sucht die Bälle per Kamera, befördert sie zum Aufzug und schafft sie weg.