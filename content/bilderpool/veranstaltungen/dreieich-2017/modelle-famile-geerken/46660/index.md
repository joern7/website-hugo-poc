---
layout: "image"
title: "Gravitationsexperiment"
date: "2017-10-02T17:32:28"
picture: "modellederfamilegeerken2.jpg"
weight: "2"
konstrukteure: 
- "Familie Geerken"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46660
- /details03ef.html
imported:
- "2019"
_4images_image_id: "46660"
_4images_cat_id: "3446"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46660 -->
Welches der beiden (unterschiedlich schweren) Fahrzeuge kommt zuerst unten an?