---
layout: "image"
title: "Formel 1 - Red Bull"
date: "2017-10-02T17:32:52"
picture: "modellehanswijnsouw2.jpg"
weight: "2"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46697
- /details2562-2.html
imported:
- "2019"
_4images_image_id: "46697"
_4images_cat_id: "3454"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46697 -->
