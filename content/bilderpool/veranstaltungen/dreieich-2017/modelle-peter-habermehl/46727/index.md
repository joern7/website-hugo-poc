---
layout: "image"
title: "Maoam-Automat"
date: "2017-10-02T17:32:53"
picture: "modellepeterhabermehl2.jpg"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46727
- /details3655.html
imported:
- "2019"
_4images_image_id: "46727"
_4images_cat_id: "3460"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46727 -->
Kontrollierte Versorgung der Sprösslinge mit Süßigkeiten... RFID sei Dank.