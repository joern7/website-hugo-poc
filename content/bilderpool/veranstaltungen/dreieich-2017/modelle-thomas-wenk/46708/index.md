---
layout: "image"
title: "Kugeltreppe"
date: "2017-10-02T17:32:52"
picture: "modellethomaswenk2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46708
- /detailsa0cf.html
imported:
- "2019"
_4images_image_id: "46708"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46708 -->
