---
layout: "image"
title: "Kugelhebewerk"
date: "2017-10-02T17:32:53"
picture: "modellethomaswenk7.jpg"
weight: "7"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46713
- /details5974.html
imported:
- "2019"
_4images_image_id: "46713"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46713 -->
