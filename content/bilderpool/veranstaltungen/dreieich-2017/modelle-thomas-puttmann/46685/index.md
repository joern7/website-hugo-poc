---
layout: "image"
title: "Wallingford-Hemmung"
date: "2017-10-02T17:32:52"
picture: "modellevonthomaspuettmann4.jpg"
weight: "4"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46685
- /detailse2f7.html
imported:
- "2019"
_4images_image_id: "46685"
_4images_cat_id: "3452"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46685 -->
Waag mit zwei "Zahnrädern"