---
layout: "image"
title: "Magische Mal-Maschine (die Mädels)"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch5.jpg"
weight: "5"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46640
- /details16fd.html
imported:
- "2019"
_4images_image_id: "46640"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46640 -->
Freie Pendelbewegung zeichnet Bilder