---
layout: "image"
title: "Magische Mal-Maschine (die Mädels), Detail"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch6.jpg"
weight: "6"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46641
- /detailsc82b.html
imported:
- "2019"
_4images_image_id: "46641"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46641 -->
Pendelbilder