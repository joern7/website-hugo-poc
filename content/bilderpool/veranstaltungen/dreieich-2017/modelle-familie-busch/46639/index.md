---
layout: "image"
title: "Flugzeugschlepper (Jörg)"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch4.jpg"
weight: "4"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46639
- /details30d8.html
imported:
- "2019"
_4images_image_id: "46639"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46639 -->
Lenkung mit Antrieb