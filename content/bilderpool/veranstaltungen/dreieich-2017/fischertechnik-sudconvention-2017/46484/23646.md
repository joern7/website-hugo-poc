---
layout: "comment"
hidden: true
title: "23646"
date: "2017-09-28T10:56:45"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Gratuliere, Dirk (Fox),
zu deinem Jacobi Motor! Wunderschön kompakt.
Gibt es Details dazu? Wie hast du das mit den Steckern an den Magneten gelöst?
Die Kommutierung müsstest du aber etwas richten.
Gruß
Rüdiger