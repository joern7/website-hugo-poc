---
layout: "image"
title: "Bild aus der Malmaschine"
date: "2017-09-27T18:24:30"
picture: "dreieich31.jpg"
weight: "31"
konstrukteure: 
- "Fam.Busch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46455
- /detailsdcd5.html
imported:
- "2019"
_4images_image_id: "46455"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46455 -->
