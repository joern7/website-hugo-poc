---
layout: "image"
title: "XXL-Express / Schrägseilbrücke"
date: "2017-09-30T11:52:18"
picture: "aIMG_2867.jpg"
weight: "84"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46521
- /details32c7-2.html
imported:
- "2019"
_4images_image_id: "46521"
_4images_cat_id: "3437"
_4images_user_id: "2638"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46521 -->
Spannweite: 3,60m
Länge über alles: 5,25m
Turmhöhe: 1,30m
Bahnhöhe über Grund: 40cm
Automatische Fahrt, Seilspannung, Seillängen-Regelung, Kabelfernbedienung, LED Beleuchtung