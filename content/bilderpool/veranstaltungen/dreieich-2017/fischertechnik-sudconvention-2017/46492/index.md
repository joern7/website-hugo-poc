---
layout: "image"
title: "Uhr"
date: "2017-09-27T18:24:51"
picture: "dreieich68.jpg"
weight: "68"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46492
- /detailsa0af.html
imported:
- "2019"
_4images_image_id: "46492"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:51"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46492 -->
