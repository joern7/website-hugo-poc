---
layout: "image"
title: "Kugelbahn"
date: "2017-09-27T18:24:25"
picture: "dreieich14.jpg"
weight: "14"
konstrukteure: 
- "Franz Santjohannser"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46438
- /detailsa37f.html
imported:
- "2019"
_4images_image_id: "46438"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:25"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46438 -->
