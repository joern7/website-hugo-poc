---
layout: "image"
title: "Keksmaschine"
date: "2017-09-27T18:24:30"
picture: "dreieich24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46448
- /details4162.html
imported:
- "2019"
_4images_image_id: "46448"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46448 -->
