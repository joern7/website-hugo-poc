---
layout: "image"
title: "Eiffelturm"
date: "2017-10-02T17:32:28"
picture: "modelledirkwoelffel1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46631
- /detailsa99b.html
imported:
- "2019"
_4images_image_id: "46631"
_4images_cat_id: "3442"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46631 -->
