---
layout: "image"
title: "Servo-Krabbe/Spinne"
date: "2017-10-02T17:32:28"
picture: "modelledirkwoelffel4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46634
- /details0ede.html
imported:
- "2019"
_4images_image_id: "46634"
_4images_cat_id: "3442"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46634 -->
Ausgedruckt mit dem fischertechnik-3D-Drucker