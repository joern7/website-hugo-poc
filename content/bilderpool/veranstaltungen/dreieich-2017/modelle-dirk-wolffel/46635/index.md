---
layout: "image"
title: "Wall-E"
date: "2017-10-02T17:32:28"
picture: "modelledirkwoelffel5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46635
- /details656c.html
imported:
- "2019"
_4images_image_id: "46635"
_4images_cat_id: "3442"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46635 -->
Wall-E (mit Pixy-Kamera) beim Ball-Verfolgen...