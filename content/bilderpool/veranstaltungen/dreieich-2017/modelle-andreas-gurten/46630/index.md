---
layout: "image"
title: "Mal-Maschine"
date: "2017-10-02T17:32:28"
picture: "modelleandreasguerten3.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46630
- /details4d6e.html
imported:
- "2019"
_4images_image_id: "46630"
_4images_cat_id: "3441"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46630 -->
Zeichenmaschine mit Kurbelantrieb