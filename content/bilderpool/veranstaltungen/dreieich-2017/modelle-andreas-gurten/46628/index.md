---
layout: "image"
title: "3D-Uhr"
date: "2017-10-02T17:32:28"
picture: "modelleandreasguerten1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46628
- /details0414.html
imported:
- "2019"
_4images_image_id: "46628"
_4images_cat_id: "3441"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46628 -->
Portalkran, der die Ziffern der Digitaluhr via Elektromagnet stellt