---
layout: "image"
title: "Waagbalken"
date: "2017-09-30T11:52:18"
picture: "Waagbalken.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Rüdiger Riedel"
keywords: ["Waagbalken", "Uhr"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46520
- /detailsd970.html
imported:
- "2019"
_4images_image_id: "46520"
_4images_cat_id: "3434"
_4images_user_id: "2635"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46520 -->
Von wem stammt diese schöne Waagbalken-Demonstration in Dreieich 2017? Geometer?