---
layout: "image"
title: "Auto-Herstellung"
date: "2017-09-30T13:03:40"
picture: "ftconvs10.jpg"
weight: "9"
konstrukteure: 
- "Holtz"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46552
- /details3404.html
imported:
- "2019"
_4images_image_id: "46552"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46552 -->
Die Taktstraße "schweißt" Halterungen an und baut dann (nach Materialprüfung) die Motorhaube an.