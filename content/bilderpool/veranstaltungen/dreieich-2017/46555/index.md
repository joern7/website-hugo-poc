---
layout: "image"
title: "Ballweitergabe"
date: "2017-09-30T18:49:56"
picture: "ftconvs13.jpg"
weight: "12"
konstrukteure: 
- "NN"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46555
- /detailsfe95.html
imported:
- "2019"
_4images_image_id: "46555"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T18:49:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46555 -->
Die Paddel (ft-Seilkurbeln) arbeiten paarweise synchron und schaufeln die Bälle nach oben.