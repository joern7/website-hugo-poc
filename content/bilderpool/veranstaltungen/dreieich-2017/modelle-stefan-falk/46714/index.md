---
layout: "image"
title: "Synchronuhr"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46714
- /details1916.html
imported:
- "2019"
_4images_image_id: "46714"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46714 -->
