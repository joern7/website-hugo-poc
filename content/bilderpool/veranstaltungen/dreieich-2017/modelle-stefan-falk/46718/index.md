---
layout: "image"
title: "Gesteuerter Schrägaufzug"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46718
- /detailsd876.html
imported:
- "2019"
_4images_image_id: "46718"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46718 -->
Endlich mal wieder was für Stefans Silberlinge...