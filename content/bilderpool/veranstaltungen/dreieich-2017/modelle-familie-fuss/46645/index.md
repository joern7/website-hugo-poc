---
layout: "image"
title: "Greifautomat"
date: "2017-10-02T17:32:28"
picture: "modellefamiliefuss1.jpg"
weight: "1"
konstrukteure: 
- "Fabian, Max, Christian & Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46645
- /detailsc294.html
imported:
- "2019"
_4images_image_id: "46645"
_4images_cat_id: "3444"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46645 -->
War bereits der Publikumsrenner auf der Maker Faire Bergstraße in Bensheim (2017)