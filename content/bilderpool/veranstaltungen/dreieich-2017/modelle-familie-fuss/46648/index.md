---
layout: "image"
title: "Kekstrockner"
date: "2017-10-02T17:32:28"
picture: "modellefamiliefuss4.jpg"
weight: "4"
konstrukteure: 
- "Fabian, Max, Christian & Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46648
- /details4005.html
imported:
- "2019"
_4images_image_id: "46648"
_4images_cat_id: "3444"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46648 -->
Hochregallager zum Trocknen der bedruckten Kekse