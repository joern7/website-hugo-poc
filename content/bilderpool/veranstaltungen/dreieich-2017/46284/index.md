---
layout: "image"
title: "Ausstellergeschenke von ft"
date: "2017-09-24T16:37:01"
picture: "IMG_3058.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/46284
- /detailsf23a.html
imported:
- "2019"
_4images_image_id: "46284"
_4images_cat_id: "3434"
_4images_user_id: "1359"
_4images_image_date: "2017-09-24T16:37:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46284 -->
Also, für Anfänger ein super Startpaket, für Profis die ultimative Ergänzung. Dankeschön! :-)