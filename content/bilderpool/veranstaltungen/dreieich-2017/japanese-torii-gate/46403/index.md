---
layout: "image"
title: "Saturday morning, almost ready"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch13.jpg"
weight: "13"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46403
- /details8c43.html
imported:
- "2019"
_4images_image_id: "46403"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46403 -->
