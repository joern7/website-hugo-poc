---
layout: "image"
title: "ftconventionchinesestriumphalarch16.jpg"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch16.jpg"
weight: "16"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46406
- /detailsc686.html
imported:
- "2019"
_4images_image_id: "46406"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46406 -->
