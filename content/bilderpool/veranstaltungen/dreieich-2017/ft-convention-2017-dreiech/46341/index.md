---
layout: "image"
title: "ftconventiondreiech057.jpg"
date: "2017-09-25T13:47:43"
picture: "ftconventiondreiech057.jpg"
weight: "57"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46341
- /details8cf7.html
imported:
- "2019"
_4images_image_id: "46341"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:43"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46341 -->
