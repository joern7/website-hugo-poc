---
layout: "image"
title: "Steinhaus-Werkstatt"
date: "2017-10-02T17:32:39"
picture: "modelleharaldsteinhaus8.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46670
- /detailsf155-2.html
imported:
- "2019"
_4images_image_id: "46670"
_4images_cat_id: "3448"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46670 -->
