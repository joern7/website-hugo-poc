---
layout: "image"
title: "Uhr mit Sekunden-Regler"
date: "2017-10-02T17:32:28"
picture: "modellegerhardbirkenstock1.jpg"
weight: "1"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46662
- /details6a2a.html
imported:
- "2019"
_4images_image_id: "46662"
_4images_cat_id: "3447"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46662 -->
Schöne Uhr mit Plexiglas-Zahnrädern und einer Synchron-Regelung.
Ausführliche Erläuterung in ft:pedia 3/2017