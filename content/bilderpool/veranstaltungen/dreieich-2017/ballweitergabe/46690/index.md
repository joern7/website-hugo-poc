---
layout: "image"
title: "Ballverschieber"
date: "2017-10-02T17:32:52"
picture: "ballweitergabemaschine4.jpg"
weight: "19"
konstrukteure: 
- "Diverse"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46690
- /details865e.html
imported:
- "2019"
_4images_image_id: "46690"
_4images_cat_id: "3439"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46690 -->
