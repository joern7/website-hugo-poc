---
layout: "image"
title: "Balldrehschieber"
date: "2017-10-02T17:32:52"
picture: "ballweitergabemaschine3.jpg"
weight: "18"
konstrukteure: 
- "Diverse"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46689
- /details881e.html
imported:
- "2019"
_4images_image_id: "46689"
_4images_cat_id: "3439"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46689 -->
