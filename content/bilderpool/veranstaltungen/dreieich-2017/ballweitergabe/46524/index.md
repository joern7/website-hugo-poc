---
layout: "image"
title: "Tischtennisball-Weitergabemaschine Station: Ballansaugung"
date: "2017-09-30T11:52:18"
picture: "aIMG_2876.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "ClassicMan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46524
- /details9a92.html
imported:
- "2019"
_4images_image_id: "46524"
_4images_cat_id: "3439"
_4images_user_id: "2638"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46524 -->
Faszienierender Anblick