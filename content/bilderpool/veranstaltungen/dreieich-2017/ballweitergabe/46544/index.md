---
layout: "image"
title: "Ballweitergabe"
date: "2017-09-30T13:03:40"
picture: "ftconvs02.jpg"
weight: "12"
konstrukteure: 
- "NN"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46544
- /detailsb7ae-2.html
imported:
- "2019"
_4images_image_id: "46544"
_4images_cat_id: "3439"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46544 -->
Links und rechts am wippenden Balken befindet sich je ein Saugkopf, der mittels Propeller einen Ball aus der Rinne ansaugt und im durchsichtigen Trichter (ex-Flaschenhals) nach oben transportiert. Wenn der Balken zu einer Seite kippt, taucht der eine Saugkopf in die Wanne ein und holt einen Ball (der Propeller wird über den oberen Kippschalter eingeschaltet), während der andere Saugkopf angehoben wird. 
Unten am Balken sind die grünen Kugelbahnen befestigt, Wenn der Saugkopf oben ist, dreht sich das zugehörige Kugelbahnstück darunter, so dass der Ball losgelassen werden kann und dann nach hinten weg rollen/fallen kann.
Der Balken wird über den unteren Kippschalter gesteuert, der einen schwarzen M-Motor links und rechts umpolt. Der zweite Kippschalter polt die Spannung für die Saugköpfe um, mit zeitlichem Vorlauf gegenüber der Umkehr der Wippe, damit der Unterdruck schon da ist, wenn der Saugkopf eintaucht. Die Saugmotoren haben je eine Diode im Strang, die dafür sorgt, dass der Motor bei Abwärtsfahrt steht.