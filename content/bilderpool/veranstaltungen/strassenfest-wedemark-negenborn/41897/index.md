---
layout: "image"
title: "Spaghettiboot"
date: "2015-09-23T09:52:30"
picture: "strassenfest04.jpg"
weight: "4"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/41897
- /detailsc3c3.html
imported:
- "2019"
_4images_image_id: "41897"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41897 -->
Als ich Philip fragte was er denn da gebaut hätte, antwortete er lapidar:
Aber Papa das ist doch klar, da werden auf einem Boot die Spaghettis getrocknet.
Genial!