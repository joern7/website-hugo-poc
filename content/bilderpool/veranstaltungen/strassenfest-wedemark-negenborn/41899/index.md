---
layout: "image"
title: "Turmbergbahn"
date: "2015-09-23T09:52:30"
picture: "strassenfest06.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/41899
- /detailsa7a1.html
imported:
- "2019"
_4images_image_id: "41899"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41899 -->
Die Turmbergbahn nach dem Original aus Karlsruhe Durlach