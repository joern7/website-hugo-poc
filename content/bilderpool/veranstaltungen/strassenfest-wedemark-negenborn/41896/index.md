---
layout: "image"
title: "Kabelsalat ade"
date: "2015-09-23T09:52:30"
picture: "strassenfest03.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/41896
- /details4477.html
imported:
- "2019"
_4images_image_id: "41896"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41896 -->
Endlich Ordnung im Kabelsalat durch eine gänzlich andere Verwendung der Flexschienen