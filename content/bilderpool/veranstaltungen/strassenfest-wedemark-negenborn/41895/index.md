---
layout: "image"
title: "Kabelsalat ade"
date: "2015-09-23T09:52:30"
picture: "strassenfest02.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/41895
- /detailse8e5.html
imported:
- "2019"
_4images_image_id: "41895"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41895 -->
Endlich Ordnung im Kabelsalat durch eine gänzlich andere Verwendung der Flexschienen