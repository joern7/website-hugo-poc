---
layout: "image"
title: "Fast die gesamte Länge"
date: "2016-10-25T14:48:43"
picture: "ingwershaengebruecketeil2.jpg"
weight: "2"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/44682
- /details3d42.html
imported:
- "2019"
_4images_image_id: "44682"
_4images_cat_id: "3326"
_4images_user_id: "381"
_4images_image_date: "2016-10-25T14:48:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44682 -->
Nur so zur Erinnerung: Ingwer kann mit seinen 2% Sehkraft das Ende der Brücke nur mit einer Art Fernrohr erkennen.