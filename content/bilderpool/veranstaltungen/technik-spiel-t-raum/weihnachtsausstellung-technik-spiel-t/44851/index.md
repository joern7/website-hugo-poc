---
layout: "image"
title: "Alte Baukästen"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44851
- /details39e4-2.html
imported:
- "2019"
_4images_image_id: "44851"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44851 -->
