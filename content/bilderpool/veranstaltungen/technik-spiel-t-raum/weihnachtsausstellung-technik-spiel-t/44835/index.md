---
layout: "image"
title: "Die Baufahrzeuge"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum03.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44835
- /detailsd86a.html
imported:
- "2019"
_4images_image_id: "44835"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44835 -->
