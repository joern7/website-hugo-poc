---
layout: "image"
title: "gebautes Modell von Besuchern"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum30.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44862
- /detailsd03a.html
imported:
- "2019"
_4images_image_id: "44862"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44862 -->
