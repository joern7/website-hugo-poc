---
layout: "image"
title: "Das untere Fach"
date: "2016-10-08T13:56:22"
picture: "technikspieltraum2.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/44562
- /details1ad9.html
imported:
- "2019"
_4images_image_id: "44562"
_4images_cat_id: "3314"
_4images_user_id: "381"
_4images_image_date: "2016-10-08T13:56:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44562 -->
Intelligent Interface
Lucky Logic Software
Buch "Robotik mit dem Homecomputer"
Trafo
Rennwagen aus dem 200er Kasten