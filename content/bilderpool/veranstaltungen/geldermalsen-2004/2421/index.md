---
layout: "image"
title: "rwepower bagger 293_1"
date: "2004-04-29T21:13:53"
picture: "rwepower_bagger_293_1.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2421
- /details9b0c.html
imported:
- "2019"
_4images_image_id: "2421"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2421 -->
Braunkohle Schaufelradbagger 293 der RWE POWER, Tagebau Hambach, hergesteld von M.A.N. TAKRAFT.
Hier das model von Peter Krijnen in maßstab 1:80. "Da mein auto schon vol war mit die ausleger teilen der M21000
konte ich die baggerbrucke und das baggerabgabegerät nicht mehr mit nehmen".