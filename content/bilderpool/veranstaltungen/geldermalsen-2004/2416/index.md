---
layout: "image"
title: "manitowoc m21000_1"
date: "2004-04-29T21:13:52"
picture: "manitowoc_m21000_1.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2416
- /detailsdeee.html
imported:
- "2019"
_4images_image_id: "2416"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2416 -->
Der Manitowoc von Anton Jansen und Peter Krijnen: maßstab 1:20 , haupausleger 4 meter , hilfsausleger 3,5 meter.
Wie hier zu sehen ist biegt der hilfsauleger gefährlich durch. Nachdem diesen foto gemacht worden war, ist der kran wegen dieses biegen eingesturtst.