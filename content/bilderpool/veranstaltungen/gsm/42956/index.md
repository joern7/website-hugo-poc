---
layout: "image"
title: "Hanna"
date: "2016-02-29T21:42:47"
picture: "xxx2.jpg"
weight: "3"
konstrukteure: 
- "Hanna"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- /php/details/42956
- /details3ed3.html
imported:
- "2019"
_4images_image_id: "42956"
_4images_cat_id: "3193"
_4images_user_id: "2439"
_4images_image_date: "2016-02-29T21:42:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42956 -->
Rettungshubschrauber der GP-Serie