---
layout: "image"
title: "Eelde 2005_10"
date: "2005-06-15T21:03:41"
picture: "EELDE_2005_10.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/4454
- /detailsffe6.html
imported:
- "2019"
_4images_image_id: "4454"
_4images_cat_id: "449"
_4images_user_id: "144"
_4images_image_date: "2005-06-15T21:03:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4454 -->
Detail vom auseren auslegerteil.
Um alle teilen besser zusammen zu halten, musten auch noch gewindestäben eingebaut worden.