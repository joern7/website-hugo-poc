---
layout: "image"
title: "Plakat Convention 2018"
date: "2018-07-22T19:39:59"
picture: "plakat1.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/47760
- /detailsf8f9.html
imported:
- "2019"
_4images_image_id: "47760"
_4images_cat_id: "3526"
_4images_user_id: "373"
_4images_image_date: "2018-07-22T19:39:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47760 -->
Da ist es, das Plakat zur Südconvention 2018 in Dreieich.
pdfs zum Ausdrucken und fleißig Verteilen gibt es hier:
DIN A4, 15MB: https://www.ftcommunity.de/data/downloads/conventionplakate/plakat2018_a4.pdf
DIN A3, 23MB: https://www.ftcommunity.de/data/downloads/conventionplakate/plakat2018_a3.pdf