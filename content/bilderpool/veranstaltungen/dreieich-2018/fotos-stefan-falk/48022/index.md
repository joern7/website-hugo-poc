---
layout: "image"
title: "Miniatur-LKW"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention103.jpg"
weight: "103"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48022
- /detailsf1aa.html
imported:
- "2019"
_4images_image_id: "48022"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48022 -->
