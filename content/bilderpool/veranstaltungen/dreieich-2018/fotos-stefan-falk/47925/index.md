---
layout: "image"
title: "Zahnstangenuhr"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention006.jpg"
weight: "6"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47925
- /detailsa3d5.html
imported:
- "2019"
_4images_image_id: "47925"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47925 -->
