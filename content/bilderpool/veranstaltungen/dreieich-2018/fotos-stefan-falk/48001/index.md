---
layout: "image"
title: "Flipper, Solarhubschrauber"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention082.jpg"
weight: "82"
konstrukteure: 
- "Joel Gundermann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48001
- /details0698.html
imported:
- "2019"
_4images_image_id: "48001"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48001 -->
