---
layout: "image"
title: "Seilantrieb des Portalkrans"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention107.jpg"
weight: "107"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48026
- /details88ab.html
imported:
- "2019"
_4images_image_id: "48026"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "107"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48026 -->
