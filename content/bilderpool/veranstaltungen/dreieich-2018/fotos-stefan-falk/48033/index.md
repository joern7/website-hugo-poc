---
layout: "image"
title: "Hebemechanik-Kugelbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention114.jpg"
weight: "114"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48033
- /details2f5f.html
imported:
- "2019"
_4images_image_id: "48033"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "114"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48033 -->
