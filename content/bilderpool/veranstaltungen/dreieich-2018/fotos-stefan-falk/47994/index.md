---
layout: "image"
title: "Baustein-Sortier-Anlage"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention075.jpg"
weight: "75"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47994
- /detailse5d1.html
imported:
- "2019"
_4images_image_id: "47994"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47994 -->
Man kippt einen Haufen unsortierter fischertechnik-Teile auf die Maschine. Die werden vereinzelt und per Kamera und trainierter KI erkannd und in die richtigen Fächer einsortiert.