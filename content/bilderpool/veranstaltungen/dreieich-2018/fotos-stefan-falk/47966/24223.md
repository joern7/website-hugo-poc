---
layout: "comment"
hidden: true
title: "24223"
date: "2018-10-06T10:28:45"
uploadBy:
- "vleeuwen"
license: "unknown"
imported:
- "2019"
---
My stepper project:
Original ft plotter with two steppers.

The controller is a Raspberry Pi 3 with motor/stepper head with as OS Windows 10 IoT and as integrated development environment MS=Visual Studio 2017 community (C#, VB.NET, Python.NET C/C++). Behind this model the old FT Roboter and a more recent FT Robot.
At the left the FT-Bot (design C.van Leeuwen) that has been used during the Robot skills in Canada some years ago. Here is in use to demonstrate the use of a SLI. An SLI is an in C/C++ writtenlibrary that contains several RoboPro elements.
See also:  https://github.com/fischertechnik/txt_demo_ROBOPro_SLI
Demo1.