---
layout: "image"
title: "Tischtennisball-Schuss"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention131.jpg"
weight: "131"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48050
- /details3dea.html
imported:
- "2019"
_4images_image_id: "48050"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48050 -->
... der dann "feuert"...