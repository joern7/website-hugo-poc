---
layout: "image"
title: "Rennwagen-Transporter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention164.jpg"
weight: "164"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48083
- /detailsecb7-2.html
imported:
- "2019"
_4images_image_id: "48083"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "164"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48083 -->
Mit unzähligen Details.