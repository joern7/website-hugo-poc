---
layout: "image"
title: "Zukünftiges Kirmesmodell"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention093.jpg"
weight: "93"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48012
- /details4d05.html
imported:
- "2019"
_4images_image_id: "48012"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "93"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48012 -->
Dieser Turm kommt später noch auf das fertige Kirmesmodell (siehe vorheriges Bilder).