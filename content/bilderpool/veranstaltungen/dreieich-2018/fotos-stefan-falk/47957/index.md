---
layout: "image"
title: "Computerisiertes Fahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention038.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47957
- /details28b9.html
imported:
- "2019"
_4images_image_id: "47957"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47957 -->
