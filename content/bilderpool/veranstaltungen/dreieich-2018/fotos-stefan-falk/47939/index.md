---
layout: "image"
title: "Kugelbahnmodell-Strecke"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention020.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47939
- /detailsa4a5.html
imported:
- "2019"
_4images_image_id: "47939"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47939 -->
