---
layout: "comment"
hidden: true
title: "24187"
date: "2018-09-26T00:07:11"
uploadBy:
- "Claus"
license: "unknown"
imported:
- "2019"
---
LKW`s der 80er Jahre weiterentwickelt und mit RC Fernsteuerung auf 2,4 GHz Basis ferngesteuert. Fernsteuerung Multiplex SMART SX FLEXX; weitere Steuermodule von CTI und RC-bruder.