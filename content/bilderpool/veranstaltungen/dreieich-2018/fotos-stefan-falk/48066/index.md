---
layout: "image"
title: "RGB-Spiel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention147.jpg"
weight: "147"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48066
- /details18cb.html
imported:
- "2019"
_4images_image_id: "48066"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "147"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48066 -->
Die Optik: Drei farbige Lampen nebst Spiegeln, oberhalb derer die Pergamentpapier-Projektionsfolie sitzt.