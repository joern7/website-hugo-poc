---
layout: "image"
title: "Leichtflieger mit dem 'etwas anderen' Motor"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention012.jpg"
weight: "12"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47931
- /detailscf0d.html
imported:
- "2019"
_4images_image_id: "47931"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47931 -->
