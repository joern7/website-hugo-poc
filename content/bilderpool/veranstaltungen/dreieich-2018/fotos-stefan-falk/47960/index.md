---
layout: "image"
title: "Roboterhand"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention041.jpg"
weight: "41"
konstrukteure: 
- "Huub van NIekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47960
- /detailsaccb.html
imported:
- "2019"
_4images_image_id: "47960"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47960 -->
