---
layout: "image"
title: "Präziser Polarkoordinaten-Plotter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention182.jpg"
weight: "182"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48101
- /details588b.html
imported:
- "2019"
_4images_image_id: "48101"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "182"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48101 -->
Der Löwe wurde fünf Mal übereinander gezeichnen - das spricht für die Wiederholgenauigkeit.