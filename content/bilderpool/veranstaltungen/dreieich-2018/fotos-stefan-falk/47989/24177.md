---
layout: "comment"
hidden: true
title: "24177"
date: "2018-09-23T19:23:17"
uploadBy:
- "PHabermehl"
license: "unknown"
imported:
- "2019"
---
Brickly...

...ist auf dem Foto nirgendwo zu sehen.

Es handelt sich um mehrere TX-PIs, auf denen startIDE läuft

Von älteren Modellen wie der RoboLT-Schiebetür und einer Verkehrsampel mit Fußgänger-Bedarfsampel an der Robo-IO-Extension bis hin zum wichtigsten Modell, der Dynamic S³ Kugelbahn mit Touchscreen-Steuerung, links am Bildrand zu erahnen, wird hier demonstriert, wie leistungsfähig die App startIDE für TXT mit community firmware und TX-Pi mittlerweile ist.