---
layout: "image"
title: "Tischtennisball-Schuss"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention132.jpg"
weight: "132"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48051
- /detailse186.html
imported:
- "2019"
_4images_image_id: "48051"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48051 -->
... und den Ball so schnell hochschießt, dass er sauber unterhalb des Bogens entlang rast und links unten wieder ankommt.