---
layout: "image"
title: "Digitalisierte Bau-Spiel-Bahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention043.jpg"
weight: "43"
konstrukteure: 
- "Paul van NIekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47962
- /details4292.html
imported:
- "2019"
_4images_image_id: "47962"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47962 -->
