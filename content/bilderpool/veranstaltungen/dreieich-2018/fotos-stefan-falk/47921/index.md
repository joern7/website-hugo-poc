---
layout: "image"
title: "Rennwagen, 3D-Drucker"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47921
- /detailsb0ba.html
imported:
- "2019"
_4images_image_id: "47921"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47921 -->
