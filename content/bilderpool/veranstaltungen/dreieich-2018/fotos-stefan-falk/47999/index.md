---
layout: "image"
title: "Baustein-Sortier-Anlage"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention080.jpg"
weight: "80"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47999
- /details8bdf.html
imported:
- "2019"
_4images_image_id: "47999"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47999 -->
Beschreibung