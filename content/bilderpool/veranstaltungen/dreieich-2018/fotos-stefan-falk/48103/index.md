---
layout: "image"
title: "Menschenschlange..."
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention184.jpg"
weight: "184"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48103
- /details570f.html
imported:
- "2019"
_4images_image_id: "48103"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "184"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48103 -->
... vorm sehr leckeren Abendessen-Buffet!