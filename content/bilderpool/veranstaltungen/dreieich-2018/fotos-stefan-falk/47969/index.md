---
layout: "image"
title: "Robotersport"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention050.jpg"
weight: "50"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47969
- /details18ed-2.html
imported:
- "2019"
_4images_image_id: "47969"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47969 -->
