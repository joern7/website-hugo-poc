---
layout: "image"
title: "Analog-Elektronik-Kleinfahrzeuge"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention063.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47982
- /details3cef.html
imported:
- "2019"
_4images_image_id: "47982"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47982 -->
Die Dinger fahren butterweich um Kurven. Da sitzen keine Computerdrin, sondern nur einfache Analogelektronik.