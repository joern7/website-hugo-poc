---
layout: "image"
title: "Kugelbahn-Xylophon, Kugelbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention025.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47944
- /detailse236.html
imported:
- "2019"
_4images_image_id: "47944"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47944 -->
