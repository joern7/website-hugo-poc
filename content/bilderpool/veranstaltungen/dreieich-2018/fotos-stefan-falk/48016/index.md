---
layout: "image"
title: "Industrieanlage"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention097.jpg"
weight: "97"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48016
- /details0c28.html
imported:
- "2019"
_4images_image_id: "48016"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48016 -->
