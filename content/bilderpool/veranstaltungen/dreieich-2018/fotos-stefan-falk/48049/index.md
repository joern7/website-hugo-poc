---
layout: "image"
title: "Tischtennisball-Schuss"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention130.jpg"
weight: "130"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48049
- /detailsb7da-2.html
imported:
- "2019"
_4images_image_id: "48049"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48049 -->
Man legt vorne ins rote Fach einen weiteren Ball. Der hinterste wird auf den waagerechten Statikträger verschoben...