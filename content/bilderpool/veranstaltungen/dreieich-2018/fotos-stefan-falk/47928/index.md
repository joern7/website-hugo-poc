---
layout: "image"
title: "Verschiedenes"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention009.jpg"
weight: "9"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47928
- /details0ab9.html
imported:
- "2019"
_4images_image_id: "47928"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47928 -->
