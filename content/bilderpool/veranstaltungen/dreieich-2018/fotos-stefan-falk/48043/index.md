---
layout: "image"
title: "Traubenzucker-Spender"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention124.jpg"
weight: "124"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48043
- /detailsf772.html
imported:
- "2019"
_4images_image_id: "48043"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48043 -->
Das Statikrad läuft auf Reifen45, die es drehen, und so die Traubenzucker hochtransportieren.