---
layout: "image"
title: "RGB-Spiel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention145.jpg"
weight: "145"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48064
- /details4e71.html
imported:
- "2019"
_4images_image_id: "48064"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "145"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48064 -->
