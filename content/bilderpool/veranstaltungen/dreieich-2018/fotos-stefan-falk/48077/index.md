---
layout: "image"
title: "Ablaufbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention158.jpg"
weight: "158"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48077
- /details6b53-3.html
imported:
- "2019"
_4images_image_id: "48077"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "158"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48077 -->
Das Rahrzeug auf der zweituntersten Schine (etwas links von den rechten Trägern) kann von oben bis unten ablaufen.