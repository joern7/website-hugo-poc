---
layout: "image"
title: "Portalkran"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention072.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47991
- /details062f.html
imported:
- "2019"
_4images_image_id: "47991"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47991 -->
