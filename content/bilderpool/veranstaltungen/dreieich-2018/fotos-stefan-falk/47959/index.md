---
layout: "image"
title: "Roboterhand"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention040.jpg"
weight: "40"
konstrukteure: 
- "Huub van NIekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47959
- /details580a.html
imported:
- "2019"
_4images_image_id: "47959"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47959 -->
