---
layout: "image"
title: "Geländegängige Raupe"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention178.jpg"
weight: "178"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48097
- /detailsdfcb-2.html
imported:
- "2019"
_4images_image_id: "48097"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "178"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48097 -->
So kann es locker über Unebenheiten fahren.