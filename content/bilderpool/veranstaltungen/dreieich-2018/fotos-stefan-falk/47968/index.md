---
layout: "image"
title: "Schiffe, Kräne"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention049.jpg"
weight: "49"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47968
- /details993d.html
imported:
- "2019"
_4images_image_id: "47968"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47968 -->
