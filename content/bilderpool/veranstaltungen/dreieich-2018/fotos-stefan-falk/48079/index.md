---
layout: "image"
title: "Kugelbahn, LKW"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention160.jpg"
weight: "160"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48079
- /detailsc7c3.html
imported:
- "2019"
_4images_image_id: "48079"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "160"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48079 -->
