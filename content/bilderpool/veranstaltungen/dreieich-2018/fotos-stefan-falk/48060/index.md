---
layout: "image"
title: "Große Kugelbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention141.jpg"
weight: "141"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48060
- /details0eb8.html
imported:
- "2019"
_4images_image_id: "48060"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48060 -->
