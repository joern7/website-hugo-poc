---
layout: "image"
title: "Digitalisierte Bau-Spiel-Bahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention042.jpg"
weight: "42"
konstrukteure: 
- "Paul van NIekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47961
- /details10ba.html
imported:
- "2019"
_4images_image_id: "47961"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47961 -->
