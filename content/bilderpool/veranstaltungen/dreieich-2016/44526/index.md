---
layout: "image"
title: "2_Rad_Flitzer"
date: "2016-10-03T10:59:01"
picture: "konstrukteurunbekannt3.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44526
- /detailsb5a1.html
imported:
- "2019"
_4images_image_id: "44526"
_4images_cat_id: "3281"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44526 -->
Kann sich um sich selbst drehen.
Und vorwärts und rückwärts fahren.

Video-Link:
https://www.youtube.com/watch?v=XD0yWyetD8E