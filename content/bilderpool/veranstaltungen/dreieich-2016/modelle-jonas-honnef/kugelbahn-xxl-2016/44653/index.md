---
layout: "image"
title: "Kugelbahn XXL 2016"
date: "2016-10-23T20:57:10"
picture: "kugelbahnxxl08.jpg"
weight: "8"
konstrukteure: 
- "Jonas Honnef"
fotografen:
- "Jonas Honnef"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jonas Honnef"
license: "unknown"
legacy_id:
- /php/details/44653
- /details2861.html
imported:
- "2019"
_4images_image_id: "44653"
_4images_cat_id: "3323"
_4images_user_id: "2649"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44653 -->
Kugelbahn XXL 2016 "Weg zum Kugel-Trichter"