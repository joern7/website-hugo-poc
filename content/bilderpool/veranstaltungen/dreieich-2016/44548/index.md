---
layout: "image"
title: "Blick in die Halle 1/3"
date: "2016-10-03T19:55:15"
picture: "dreieich02.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44548
- /detailsb264.html
imported:
- "2019"
_4images_image_id: "44548"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44548 -->
In der geräumigen Halle ...