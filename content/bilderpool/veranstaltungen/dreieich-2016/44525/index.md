---
layout: "image"
title: "Arthur_Fischer"
date: "2016-10-03T10:59:01"
picture: "konstrukteurunbekannt2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44525
- /detailsc435.html
imported:
- "2019"
_4images_image_id: "44525"
_4images_cat_id: "3281"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44525 -->
In Memorium...