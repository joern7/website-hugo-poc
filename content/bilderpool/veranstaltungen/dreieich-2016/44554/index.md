---
layout: "image"
title: "Kommunales Entsorgungsfahrzeug"
date: "2016-10-03T19:55:15"
picture: "dreieich08.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44554
- /detailsd723.html
imported:
- "2019"
_4images_image_id: "44554"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44554 -->
Vulgo: Müll-Laster. Der Abräumer, sozusagen. So einer in echt kam montags bestimmt abholen was wir am Samstag übrig gelassen haben.

Das Modell wurde auf einer Drehscheibe rundum präsentiert.