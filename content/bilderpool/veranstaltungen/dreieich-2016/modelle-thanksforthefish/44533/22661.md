---
layout: "comment"
hidden: true
title: "22661"
date: "2016-10-22T23:46:20"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Hallo Ralf,
wenn ich mich recht erinnere müssten beide Wagen gleich schnell sein. Zumindest wäre es im freien Fall der Fall.

Auf Youtube habe ich eine Doku über Gravitation mit Bowling Kugel vs. Büste, Melone usw..
In einem anderen Video über Holz-Rennwagen mit einer ähnlichen Rampe, konnte man ein Fahrzeug "tunen" so dass es schneller war.

Bitte um Aufklärung.

k.G Kai