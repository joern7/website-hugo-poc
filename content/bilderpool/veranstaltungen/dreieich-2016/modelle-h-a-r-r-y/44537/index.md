---
layout: "image"
title: "Kugelbahn"
date: "2016-10-03T10:59:01"
picture: "harry2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44537
- /details4c94.html
imported:
- "2019"
_4images_image_id: "44537"
_4images_cat_id: "3309"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44537 -->
Heberad (links) und Hebekunst(vorne)

Video-Link:
https://www.youtube.com/watch?v=CR_v-tVpJjE