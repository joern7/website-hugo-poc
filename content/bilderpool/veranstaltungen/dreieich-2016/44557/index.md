---
layout: "image"
title: "Bauspielbahn-Lok"
date: "2016-10-03T19:55:15"
picture: "dreieich11.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44557
- /details5946.html
imported:
- "2019"
_4images_image_id: "44557"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44557 -->
Man beachte das doppelte Kupplungssystem, das sowohl für ft-Züge als auch NEM-Kupplungen ausgelegt ist. Der Haupteinsatz der Lok dürfte daher wohl im Übergabebetrieb zwischen der Modell-DB und der ft-Lokalbahn liegen ;)