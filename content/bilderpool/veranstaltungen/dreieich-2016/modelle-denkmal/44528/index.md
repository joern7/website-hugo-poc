---
layout: "image"
title: "Die_Brücke_im_Aufbau"
date: "2016-10-03T10:59:01"
picture: "denkmal2.jpg"
weight: "2"
konstrukteure: 
- "DenkMal"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44528
- /details9770.html
imported:
- "2019"
_4images_image_id: "44528"
_4images_cat_id: "3307"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44528 -->
