---
layout: "image"
title: "Teile_für_die_Brücke"
date: "2016-10-03T10:59:01"
picture: "denkmal1.jpg"
weight: "1"
konstrukteure: 
- "DenkMal"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44527
- /detailsb84b.html
imported:
- "2019"
_4images_image_id: "44527"
_4images_cat_id: "3307"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44527 -->
