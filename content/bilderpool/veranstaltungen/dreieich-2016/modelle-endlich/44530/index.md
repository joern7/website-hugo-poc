---
layout: "image"
title: "Steilhang_Traubenernter"
date: "2016-10-03T10:59:01"
picture: "endlich1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44530
- /details7583.html
imported:
- "2019"
_4images_image_id: "44530"
_4images_cat_id: "3308"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44530 -->
