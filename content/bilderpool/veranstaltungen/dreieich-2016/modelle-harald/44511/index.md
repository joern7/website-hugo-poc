---
layout: "image"
title: "Chassis"
date: "2016-10-02T17:43:47"
picture: "modellevonharald2.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44511
- /detailsf428.html
imported:
- "2019"
_4images_image_id: "44511"
_4images_cat_id: "3300"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44511 -->
Ganz fertig ist er noch nicht, dem Vernehmen nach soll es ein Frontkippertraktor werden.