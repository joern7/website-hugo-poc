---
layout: "image"
title: "fischertechnikconventiondreieich48.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich48.jpg"
weight: "48"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44454
- /details5964.html
imported:
- "2019"
_4images_image_id: "44454"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44454 -->
