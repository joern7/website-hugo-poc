---
layout: "image"
title: "fischertechnikconventiondreieich52.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich52.jpg"
weight: "52"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44458
- /details4b13.html
imported:
- "2019"
_4images_image_id: "44458"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44458 -->
