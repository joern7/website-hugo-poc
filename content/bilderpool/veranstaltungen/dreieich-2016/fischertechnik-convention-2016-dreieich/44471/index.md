---
layout: "image"
title: "fischertechnikconventiondreieich65.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich65.jpg"
weight: "65"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44471
- /details995a-2.html
imported:
- "2019"
_4images_image_id: "44471"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44471 -->
