---
layout: "image"
title: "fischertechnikconventiondreieich07.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich07.jpg"
weight: "7"
konstrukteure: 
- "Marcel Endlich"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44413
- /detailse8a8.html
imported:
- "2019"
_4images_image_id: "44413"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44413 -->
