---
layout: "image"
title: "fischertechnikconventiondreieich09.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich09.jpg"
weight: "9"
konstrukteure: 
- "Andreas Gurten"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44415
- /details85b3.html
imported:
- "2019"
_4images_image_id: "44415"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44415 -->
