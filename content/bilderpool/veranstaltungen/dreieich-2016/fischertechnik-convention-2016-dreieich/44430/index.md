---
layout: "image"
title: "fischertechnikconventiondreieich24.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich24.jpg"
weight: "24"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44430
- /detailscc48.html
imported:
- "2019"
_4images_image_id: "44430"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44430 -->
