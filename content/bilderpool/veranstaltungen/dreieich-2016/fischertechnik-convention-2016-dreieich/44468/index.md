---
layout: "image"
title: "PDR-Robotiker Entwicklung der I2C-Motortreiber (> 12V, 2A)  für die TXT"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich62.jpg"
weight: "62"
konstrukteure: 
- "Fabian"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44468
- /details8a1b.html
imported:
- "2019"
_4images_image_id: "44468"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44468 -->
