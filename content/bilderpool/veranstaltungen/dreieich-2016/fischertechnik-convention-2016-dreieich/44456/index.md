---
layout: "image"
title: "fischertechnikconventiondreieich50.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich50.jpg"
weight: "50"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44456
- /detailscdbb.html
imported:
- "2019"
_4images_image_id: "44456"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44456 -->
