---
layout: "image"
title: "fischertechnikconventiondreieich59.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich59.jpg"
weight: "59"
konstrukteure: 
- "sjost"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44465
- /detailscf32-2.html
imported:
- "2019"
_4images_image_id: "44465"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44465 -->
