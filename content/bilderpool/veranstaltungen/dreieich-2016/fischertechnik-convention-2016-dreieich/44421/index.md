---
layout: "image"
title: "fischertechnikconventiondreieich15.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44421
- /detailsc2f6.html
imported:
- "2019"
_4images_image_id: "44421"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44421 -->
