---
layout: "comment"
hidden: true
title: "22571"
date: "2016-10-03T12:48:15"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Man beachte auch das maßgeschneiderte (d.h. vom Profi-Sattler gefertigte) Verdeck mit Fenstern in der Rückwand!