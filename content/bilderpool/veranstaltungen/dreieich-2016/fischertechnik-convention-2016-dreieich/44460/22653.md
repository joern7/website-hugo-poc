---
layout: "comment"
hidden: true
title: "22653"
date: "2016-10-22T21:48:20"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Ein großes ...Respekt... an die Mitwirkenden des Gemeinschaftsprojekts Schwebebahn.

Es ist sehr spannend und Interessant wie sich das Projekt entwickelt. Wenn ich das richtig wahrgenommen habe, ist der "Projektmanager" noch nicht Volljährig bzw. ein junger Mann. Ich finde es äußerst beeindruckend, ein so großes und langjähriges Projekt anzugehen, durch zu ziehen und bestimmt "Fertig" zu stellen.
Dickes Lob.

k.G Kai