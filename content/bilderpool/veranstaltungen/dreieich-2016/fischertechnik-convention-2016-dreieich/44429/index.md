---
layout: "image"
title: "fischertechnikconventiondreieich23.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich23.jpg"
weight: "23"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44429
- /detailsff61.html
imported:
- "2019"
_4images_image_id: "44429"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44429 -->
