---
layout: "image"
title: "fischertechnikconventiondreieich18.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich18.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44424
- /detailsdc6b.html
imported:
- "2019"
_4images_image_id: "44424"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44424 -->
