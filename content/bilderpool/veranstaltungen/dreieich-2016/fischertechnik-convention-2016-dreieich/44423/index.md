---
layout: "image"
title: "fischertechnikconventiondreieich17.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44423
- /detailsa5bb.html
imported:
- "2019"
_4images_image_id: "44423"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44423 -->
