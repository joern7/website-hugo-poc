---
layout: "image"
title: "fischertechnikconventiondreieich11.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich11.jpg"
weight: "11"
konstrukteure: 
- "Andreas Gurten"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44417
- /detailsd0eb.html
imported:
- "2019"
_4images_image_id: "44417"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44417 -->
