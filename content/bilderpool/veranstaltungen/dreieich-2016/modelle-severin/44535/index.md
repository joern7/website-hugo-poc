---
layout: "image"
title: "3D_Drucker_von_Severin"
date: "2016-10-03T10:59:01"
picture: "severin1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44535
- /details5d37.html
imported:
- "2019"
_4images_image_id: "44535"
_4images_cat_id: "3294"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44535 -->
Der Drucker hat eine beachtliche Höhe von etwa 1 Meter