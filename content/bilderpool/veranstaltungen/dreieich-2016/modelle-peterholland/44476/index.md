---
layout: "image"
title: "Unimog"
date: "2016-10-01T15:02:06"
picture: "modellevonpeterholland2.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44476
- /details85f3.html
imported:
- "2019"
_4images_image_id: "44476"
_4images_cat_id: "3285"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44476 -->
