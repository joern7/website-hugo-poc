---
layout: "image"
title: "Schwebebahnstrecke 4"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Station"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44501
- /details210b.html
imported:
- "2019"
_4images_image_id: "44501"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44501 -->
Ein Haltepunkt mit Aufzügen.