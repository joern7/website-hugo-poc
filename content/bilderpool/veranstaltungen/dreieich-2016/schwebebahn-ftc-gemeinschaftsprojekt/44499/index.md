---
layout: "image"
title: "Schwebebahnstrecke 2"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44499
- /details017a.html
imported:
- "2019"
_4images_image_id: "44499"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44499 -->
Von der Station geht es auf die Strecke. Die Schienen werden von filigranen Stützen getragen.