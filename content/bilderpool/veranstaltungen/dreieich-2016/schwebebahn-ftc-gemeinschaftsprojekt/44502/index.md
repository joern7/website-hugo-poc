---
layout: "image"
title: "Schwebebahnstrecke 5"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44502
- /details5a45-2.html
imported:
- "2019"
_4images_image_id: "44502"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44502 -->
Die Kurve.