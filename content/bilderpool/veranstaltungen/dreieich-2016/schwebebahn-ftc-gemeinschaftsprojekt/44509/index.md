---
layout: "image"
title: "Zugwendeanlage - Detail"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Zugwendeanlage", "Drehscheibe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44509
- /detailsd3e1.html
imported:
- "2019"
_4images_image_id: "44509"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44509 -->
Ein Reedkontakt erkennt die Sollposition der Drehscheibe. Eine interessante Möglichkeit die Kettenmagnete als Ersatz für Magnetbausteine zu verwenden!