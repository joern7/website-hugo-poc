---
layout: "image"
title: "Etikettier_Maschine"
date: "2016-10-03T10:59:01"
picture: "konstrukteurunbekannt1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44524
- /details7d78.html
imported:
- "2019"
_4images_image_id: "44524"
_4images_cat_id: "3281"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44524 -->
Im Vordergrund sieht man die Pneumatik-Ventile