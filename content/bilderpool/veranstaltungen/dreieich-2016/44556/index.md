---
layout: "image"
title: "Bauspielbahn"
date: "2016-10-03T19:55:15"
picture: "dreieich10.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44556
- /details392a.html
imported:
- "2019"
_4images_image_id: "44556"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44556 -->
Auf d'r schwäb'schen Eisebahne ...