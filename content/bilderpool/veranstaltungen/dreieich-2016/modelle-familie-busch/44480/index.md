---
layout: "image"
title: "Das Karussell"
date: "2016-10-01T15:02:07"
picture: "modellevonfamiliebusch1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44480
- /detailsd0f7.html
imported:
- "2019"
_4images_image_id: "44480"
_4images_cat_id: "3289"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44480 -->
Kirmes in grauer Vorzeit.