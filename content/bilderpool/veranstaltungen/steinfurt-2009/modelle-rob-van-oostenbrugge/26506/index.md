---
layout: "image"
title: "Kugelbahn"
date: "2010-02-22T20:23:47"
picture: "S6002668.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Bernhard Lehner"
keywords: ["Kugelbahn", "Turm", "Tischtennisball"]
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/26506
- /details954d.html
imported:
- "2019"
_4images_image_id: "26506"
_4images_cat_id: "1864"
_4images_user_id: "1028"
_4images_image_date: "2010-02-22T20:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26506 -->
