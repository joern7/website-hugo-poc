---
layout: "file"
hidden: true
title: "C-Programme für den TX"
date: "2017-10-13T00:00:00"
file: "ftmsccdemo_v1.3.zip"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/ftmsccdemo_v1.3.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftmsccdemo_v1.3.zip -->
Dieses Packet enthält den GNU ARM C-Compiler, den PSPad-Editor und 5 Beispielprogramme in der Sprache C. Der Compiler erzeugt die bin-Datei, die in den Speicher des TX übertragen wird.
Durch die Implementierung der wichtigsten TX-Befehle in einer Include-Datei sind die Beispielprogramme einfacher als die original C-Programme von fischertechnik. Ein Beispiel zeigt, wie die Pixy - CMUcam5 am TX-Gabelstapler (aus ROBO TX Training Lab) verwendet werden kann.
In ft:pedia 2/2017 habe ich einen Artikel über die TX-Programmierung veröffentlicht.