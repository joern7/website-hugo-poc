---
layout: "file"
hidden: true
title: "I²C-Treiber für PWM-Servo-Driver von Adafruit (PCA9685) v1.1"
date: "2017-06-17T00:00:00"
file: "servodriverpca9685v1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/servodriverpca9685v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/servodriverpca9685v1.1.zip -->
ROBO Pro-Treiber für den 12-bit-PWM Servo Driver von Adafruit (PCA9685) für die Ansteuerung von bis zu 16 Servos, v1.1. Ausführliche Erläuterungen und Beispielprogramm in ft:pedia 2/2017.