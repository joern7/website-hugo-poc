---
hidden: true
layout: "file"
title: "Kreisringe aus ft Winkelbausteinen"
date: 2017-03-16T00:00:00
file: "kreisringeausftwinkelbausteinen.xlsx"
konstrukteure:
- "Rüdiger Riedel"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/kreisringeausftwinkelbausteinen.xlsx
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/kreisringeausftwinkelbausteinen.xlsx -->
Die Liste der ft-Winkelsteinkreise ist in diesem Excel-File zu finden.
Darin habe ich auch die Summen der Kantenlängen innen und außen aufgeführt.
