---
hidden: true
layout: "file"
title: "ft-Ringe mit Winkelsumme nahe 360°"
date: 2017-03-16T00:00:00
file: "ftringemitwinkelsummenahe360.xlsx"
konstrukteure:
- "Rüdiger Riedel"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftringemitwinkelsummenahe360.xlsx
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftringemitwinkelsummenahe360.xlsx -->
In dieser Excel-Datei habe ich alle die Ringe aufgelistet, die die
Winkelsumme 360° knapp verfehlen.
Als Grenzwert habe ich +/- 1° gesetzt, bezogen auf den einzelnen Baustein.
