---
hidden: true
layout: "file"
title: "Die Uhrzeit im Klartext"
date: 2018-02-25T00:00:00
file: "uhrzeitimklartext.zip"
konstrukteure:
- "Thomas Püttmann"
- "Ludger Mäsing"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/uhrzeitimklartext.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/uhrzeitimklartext.zip -->
ft:pedia 1 / 2017: ft-Designer-Datei für eine der Klartextuhren und die
Anzeigetexte für beide Uhren
