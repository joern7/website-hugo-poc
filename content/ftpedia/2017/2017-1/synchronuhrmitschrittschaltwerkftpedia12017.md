---
hidden: true
layout: "file"
title: "Synchronuhr mit Schrittschaltwerk"
date: 2017-06-12T00:00:00
file: "synchronuhrmitschrittschaltwerkftpedia12017.ftm"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/synchronuhrmitschrittschaltwerkftpedia12017.ftm
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/synchronuhrmitschrittschaltwerkftpedia12017.ftm -->
fischertechnik-designer-Datei der Synchronuhr mit Schrittschaltwerk,
vorgestellt in Ausgabe 1/2017 der ft:pedia.
