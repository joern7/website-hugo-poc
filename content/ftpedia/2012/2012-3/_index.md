---
hidden: true
layout: "issue"
title: "3 / 2012"
file: "ftpedia-2012-3.pdf"
publishDate: 2012-09-29T00:00:00
date: 2012-09-29T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2012-3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-3.pdf -->
