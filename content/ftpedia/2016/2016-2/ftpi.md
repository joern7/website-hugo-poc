---
hidden: true
layout: "file"
title: "Steuerprogramme für den ftPI"
date: 2016-07-19T00:00:00
file: "ftpi.zip"
konstrukteure:
- "Christian Bergschneider"
- "Stefan Fuss"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftpi.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftpi.zip -->
Bibliothek und Beispielprogramm für den ftPi
