---
hidden: true
layout: "issue"
title: "1 / 2016"
file: "ftpedia-2016-1.pdf"
publishDate: 2016-03-26T00:00:00
date: 2016-03-26T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2016-1.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-1.pdf -->
