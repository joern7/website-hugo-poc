---
layout: "file"
hidden: true
title: "I²C-Treiber für Farbsensor ISL29125 v1.0"
date: "2017-02-03T00:00:00"
file: "colorisl29125v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/colorisl29125v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/colorisl29125v1.0.zip -->
RoboPro-I²C-Teiber für den Farbsensor ISL29125 am TXT Controller. Nähere Erläuterungen in ft:pedia 1/2016, S. 79-89.