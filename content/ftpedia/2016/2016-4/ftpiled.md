---
hidden: true
layout: "file"
title: "ftpiLED rpp Bibliothek und Fontgenerator"
date: 2016-12-21T00:00:00
file: "ftpiled.zip"
konstrukteure:
- "Christian Bergschneider"
- "Stefan Fuss"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftpiled.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftpiled.zip -->
Das Zip enthält die ROBO Pro Software zur Ansteuerung von LED-Matrixen inkl. einem Fontgenerator zum einfachen Erstellen von 8x8-Zeichensätzen über Excel.

Hardware: HT16K33 über I2C-Bus, z.B. adafruit Bi-Color 8x8 Matrix.
