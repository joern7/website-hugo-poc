---
hidden: true
layout: "issue"
title: "3 / 2016"
file: "ftpedia-2016-3.pdf"
publishDate: 2016-09-23T00:00:00
date: 2016-09-23T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2016-3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-3.pdf -->
