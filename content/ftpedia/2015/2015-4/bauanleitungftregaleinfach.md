---
hidden: true
layout: "file"
title: "einfaches Regal für fischertechnik"
date: 2015-12-22T00:00:00
file: "bauanleitungftregaleinfach.pdf"
konstrukteure:
- "Jörg-Peter"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/bauanleitungftregaleinfach.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/bauanleitungftregaleinfach.pdf -->
Dies ist der Bauplan für eines der beiden in der ft:pedia 4/2015 vorgestellten Möbelstücke für die Aufbewahrung von größeren oder kleineren fischertechnik-Sammlungen.
Der Plan stellt mit Schritt-für-Schritt-Bildern vor, wie man mit wenig und
preiswertem Material und ohne großen Maschinenpark ein Möbelstück bauen kann,
das sich leicht in die eigene Wohnsituation einpassen lässt.
Alle benötigten Teile sollte man in jedem Baumarkt bekommen, und der komplette
Nachbau ist mit einigen Stunden Zeitaufwand auch für Anfäger zu schaffen.
