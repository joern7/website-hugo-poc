---
hidden: true
layout: "file"
title: "C-Code zum Ansteuern des ft-Universal-Interface"
date: 2018-09-25T00:00:00
file: "ftui_pwm.zip"
konstrukteure:
- "Dirk Uffmann"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftui_pwm.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftui_pwm.zip -->
Siehe den Artikel in der ft_pedia 2015/1: "PWM-Motorsteuerung am fischertechnik-
Universal-Interface"
