---
hidden: true
layout: "issue"
title: "2 / 2015"
file: "ftpedia-2015-2.pdf"
publishDate: 2015-06-26T00:00:00
date: 2015-06-26T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2015-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2015-2.pdf -->
