---
layout: "file"
hidden: true
title: "Seilcomputer Kelvin"
date: "2015-11-13T00:00:00"
file: "seilcomputer.ftm"
konstrukteure: 
- "Andreas Gürten"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/seilcomputer.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/seilcomputer.ftm -->
Ergänzend zum ft:pedia-Artikel aus Heft 2014-2