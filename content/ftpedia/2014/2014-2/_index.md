---
hidden: true
layout: "issue"
title: "2 / 2014"
file: "ftpedia-2014-2.pdf"
publishDate: 2014-06-27T00:00:00
date: 2014-06-27T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2014-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2014-2.pdf -->
