---
layout: "file"
hidden: true
title: "I²C-Treiber für Pixy-Kamera v1.0"
date: "2017-01-12T00:00:00"
file: "treiberpixykamerav1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberpixykamerav1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberpixykamerav1.0.zip -->
RoboPro-I²C-Teiber für die CMUcam5 (Pixy) für den Betrieb am TX Controller. Funktionen zur Farberkennung, Pan/Tilt-Steuerung, Erkennung mehrerer Objekte und Abstandsberechnung. Nähere Erläuterungen in ft:pedia 4/2014, S. 43-51-