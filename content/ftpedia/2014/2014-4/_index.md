---
hidden: true
layout: "issue"
title: "4 / 2014"
file: "ftpedia-2014-4.pdf"
publishDate: 2014-12-23T00:00:00
date: 2014-12-23T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2014-4.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2014-4.pdf -->
