---
layout: "file"
hidden: true
title: "Funkuhr mit RTC (DS3231) und LCD (LCD2005) v1.0"
date: "2017-02-03T00:00:00"
file: "ds3231mitdcf77undlcd2004v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ds3231mitdcf77undlcd2004v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ds3231mitdcf77undlcd2004v1.0.zip -->
Das Programm erweitert die Funkuhr (DCF77) um eine Echtzeituhr (RTC DS3231) und ein LC-Display (LCD2005). Näheres in ft:pedia 1/2014.