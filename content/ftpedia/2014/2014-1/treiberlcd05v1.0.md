---
layout: "file"
hidden: true
title: "I²C-Treiber für LCD05"
date: "2017-02-03T00:00:00"
file: "treiberlcd05v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberlcd05v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberlcd05v1.0.zip -->
Treiber für das I²C-LC-Display LCD05. Der Treiber enthält 18 Funktionen, mit denen sich das Display sehr komfortabel ansteuern lässt. Näheres in der ft:pedia 1/2014.