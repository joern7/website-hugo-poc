---
layout: "file"
hidden: true
title: "I²C-Treiber für LCD05-Keypad"
date: "2017-02-03T00:00:00"
file: "treiberlcd05keypadv1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberlcd05keypadv1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberlcd05keypadv1.0.zip -->
RoboPro-I²C-Treiber für das am LC-Display LCD05 anschließbare Keypad (Nummerntastatur). Näheres in Ausgabe 3/2014 der ft:pedia.