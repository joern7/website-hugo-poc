---
layout: "file"
hidden: true
title: "Initialienschreiber von Stefan Falk"
date: "2013-04-01T00:00:00"
file: "bauanleitung_initialienschreiber.pdf"
konstrukteure: 
- "Sven Engelke"
uploadBy:
- "Sven Engelke"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/bauanleitung_initialienschreiber.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/bauanleitung_initialienschreiber.pdf -->
In der ft:pedia 1/2013 hat Stefan Falk ein tolles Modell vorgestellt.
Einen Initialienschreiber.
Allerdings gibt es dort keine Anleitung uns somit ist das Modell von vielen Leuten nicht nach den Fotos baubar.
Thomas Kaiser war so freundlich das Modell im ftDesigner zu bauen und ich habe dann davon eine Anleitung erstellt.