---
layout: "file"
hidden: true
title: "I2C-Treiber für Luftdrucksensor BMP085"
date: "2017-01-12T00:00:00"
file: "BMP085_treiber_v12.zip"
konstrukteure: 
- "Georg Stiegler (fantogerch)"
uploadBy:
- "Georg Stiegler (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/BMP085_treiber_v12.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/BMP085_treiber_v12.zip -->
Treiber und Beispielprogramme für den I²C-Drucksensor BMP085 (siehe Beitrag in ft:pedia 1/2013).