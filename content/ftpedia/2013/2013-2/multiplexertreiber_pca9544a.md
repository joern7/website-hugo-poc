---
layout: "file"
hidden: true
title: "I2C-Treiber für Multiplexer PCA9544A"
date: "2017-01-12T00:00:00"
file: "multiplexertreiber_pca9544a.zip"
konstrukteure: 
- "Georg Stiegler (fantogerch)"
uploadBy:
- "Georg Stiegler (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/multiplexertreiber_pca9544a.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/multiplexertreiber_pca9544a.zip -->
RoboPro-Treiber, um den I2C-Bus-Multiplexer PCA9544A am I2C-Port des TX zu nutzen. Ermöglicht z.B. Ansteuerung von bis zu 4 I2C-Komponenten mit gleicher Adresse. Näheres in ft:pedia 2/2013.