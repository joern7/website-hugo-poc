---
layout: "file"
hidden: true
title: "I²C-Treiber für Real Time Clock (RTC) DS1307"
date: "2017-02-03T00:00:00"
file: "rtctreiberds1307v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/rtctreiberds1307v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/rtctreiberds1307v1.0.zip -->
Verbesserter I²C-Treiber für RTC DS1307 inklusive Beispielprogramm - Stellen der RTC mit dem DCF77-Decoder (siehe ft:pedia 3/2012) und Ausgabe auf TX-Display sowie (falls angeschlossen) bis zu drei 7-Segment-LED-Anzeigen (SAA1064, siehe ft:pedia 4/2012).