---
layout: "file"
hidden: true
title: "I2C-Treiber für Nunchuk v2.0"
date: "2017-01-12T00:00:00"
file: "nunchuktreiberv2.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/nunchuktreiberv2.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/nunchuktreiberv2.0.zip -->
RoboPro-Treiber, um den Nunchuk (Nintendo-Wii-Steuergerät) am I2C-Port des TX zu nutzen. Funktioniert ab Firmware 1.30.1 mit dem Original-Nunchuk und dem kabellosen Logic3-Nachbau (Busgeschwindigkeit 100 kHz). Näheres in ft:pedia 2/2013.