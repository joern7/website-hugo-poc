---
hidden: true
layout: "issue"
title: "2 / 2013"
file: "ftpedia-2013-2.pdf"
publishDate: 2013-06-28T00:00:00
date: 2013-06-28T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2013-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2013-2.pdf -->
