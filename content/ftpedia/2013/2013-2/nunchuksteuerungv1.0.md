---
layout: "file"
hidden: true
title: "Nunchuk-Steuerung für fischertechnik-Modelle v1.0"
date: "2013-06-24T00:00:00"
file: "nunchuksteuerungv1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/nunchuksteuerungv1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/nunchuksteuerungv1.0.zip -->
I²C-Nunchuk-Steuerung für ft-Modelle (Fahrzeuge, Raupenantriebe etc.). Siehe Beitrag in ft:pedia 2/2013. Beispielanwendung für RoboPro-Nunchuk-Treiber (v2.0).