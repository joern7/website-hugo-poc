---
layout: "file"
hidden: true
title: "I²C-Treiber für RTC DS1307 und DS3231 v1.1"
date: "2017-02-03T00:00:00"
file: "rtctreiberds1307undds3231v1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/rtctreiberds1307undds3231v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/rtctreiberds1307undds3231v1.1.zip -->
I²C-Treiber für die Real Time Clock (RTC) DS1307 und  DS3231 inklusive Beispielprogrammen (Stoppuhr, Stellen der RTC mit dem DCF77-Decoder aus ft:pedia 3/2012). Näheres im entsprechenden Beitrag der ft:pedia 4/2013.