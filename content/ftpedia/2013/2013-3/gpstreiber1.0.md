---
layout: "file"
hidden: true
title: "I²C-Treiber v1.0 für GPS-Sensor (Navigatron v2)"
date: "2017-02-03T00:00:00"
file: "gpstreiber1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/gpstreiber1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/gpstreiber1.0.zip -->
Robo-Pro-Treiber für I²C-GPS-Sensor Navigatron v2 von [Flytron](http://www.flytron.com/sensors/180-i2c-gps-for-multiwii-and-others.html) mit Beispielprogramm zur Positionsbestimmung. Siehe Beitrag in ft:pedia 3/2013.
