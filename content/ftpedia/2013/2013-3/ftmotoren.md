---
layout: "file"
hidden: true
title: "Rechenhilfe für Elektromotoren"
date: "2013-09-13T00:00:00"
file: "ftmotoren.xls"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/ftmotoren.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/ftmotoren.xls -->
Praktische Excel-Sheets mit / für
+ Motordaten einiger ft-Motoren (siehe Hinweise in der Datei!)
+ Vergleichshilfe
+ Vorlage f. Kennlinien

Zur Unterstützung des ft:pedia Artikels über Kenndaten der ft-Motoren in Heft 3/2013. Das Hintergrundwissen ist dort zu finden.

Die Sheets kommen ohne Makros aus.