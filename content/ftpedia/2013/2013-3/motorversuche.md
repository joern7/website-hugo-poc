---
layout: "file"
hidden: true
title: "Hilfe für Versuchsauswertung - ft:pedia 3/2013"
date: "2013-09-13T00:00:00"
file: "motorversuche.xls"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/motorversuche.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/motorversuche.xls -->
Excel-Sheet zur Ermittlung der Motormodellparameter.

Für das nötige Hintergundwissen dazu siehe Artikel "Kenndaten der ft-Motoren" in ft:pedia Heft 3/2013.