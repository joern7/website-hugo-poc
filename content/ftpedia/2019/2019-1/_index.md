---
hidden: true
layout: "issue"
title: "1 / 2019"
file: "ftpedia-2019-1.pdf"
publishDate: 2019-03-30T00:00:00
date: 2019-03-30T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2019-1.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2019-1.pdf -->
