---
layout: "file"
hidden: true
title: "Essay_LED"
date: "2011-08-17T00:00:00"
file: "essay_led.pdf"
konstrukteure: 
- "Thomas Habig"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/essay_led.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/essay_led.pdf -->
Elektronik Grundlagen
Essay über LEDs