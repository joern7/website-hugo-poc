---
title: "ft-Datenbank"
weight: 200
date: 2020-03-29T00:00:00
uploadBy:
- "Website-Team"
---

Hinweise zur Benutzung der fischertechnik-Datenbank!

Die Entwicklung der Datenbank beruht ausschließlich auf ehrenamtlicher Tätigkeit.
Falls also irgendwo beim Stöbern ein Fehler auftritt oder sonstwo das Fehlerteufelchen zugeschlagen hat, bitten wir dies zu berücksichtigen und zu entschuldigen. Bei gravierenderen Unstimmigkeiten könnt ihr das gerne [im Unterforum "ft-Datenbank"](https://forum.ftcommunity.de/viewforum.php?f=32) loswerden.

Wir wünschen euch jedenfalls viel Spaß mit der fischertechnik-Datenbank:

[Link zur Datenbank](http://ft-datenbank.de)

Der Vorstand des ftc Modellbau e.V.
