---
Title: "Start"
ordersectionsby: "weight"
---

# ![fischertechnik community](images/logo-full.png)

Du baust gerne mit fischertechnik? Hier gibt es für dich ....

* eine [Bildersammlung](./bilderpool) (schau dir Fotos anderer fischertechnik-Freunde an)
* einen [Servicebereich](./knowhow) (lade Programme, Bauanleitungen und weitere nützliche Dinge herunter)
* ein [Forum](https://forum.ftcommunity.de/) (tritt in Kontakt mit Gleichgesinnten)
* die kostenlose Zeitschrift [ft:pedia](./ftpedia) (mit Artikeln und Modellvorstellungen)
* [Veranstaltungen](fans/veranstaltungen), bei denen du vor Ort erlebst, was mit fischertechnik alles möglich ist.
  Weitere aktuelle Infos dazu auch im [Forum](https://forum.ftcommunity.de/viewforum.php?f=39).
* eine [Teile-Datenbank](./ftdatenbank) (nutze die umfangreichen Informationen über aktuelle oder frühere Bauteile und Baukästen)


